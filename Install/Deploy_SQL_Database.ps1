#-------------------------------------------------------------------------------------------------#
# Call the PowerShell script and pass the arguments
# .\Deploy_SQL_Database.ps1 "C:\WorkArea\Scripts\ServerConfig.xml"
# .\Deploy_SQL_Database.ps1 -ConfigPath "C:\WorkArea\Scripts\ServerConfig.xml"
#-------------------------------------------------------------------------------------------------#

#-------------------------------------------------------------------------------------------------#
# Script parameters
#-------------------------------------------------------------------------------------------------#
[CmdletBinding()]
param (
	[Parameter(Mandatory = $true)]
	[string]$ConfigPath = $null,
	[Parameter(Mandatory = $true)]
	[boolean]$CheckOnly = $false
)

#-------------------------------------------------------------------------------------------------#
# Function Write-Log
#-------------------------------------------------------------------------------------------------#
Function Write-Log
{
	Param (
		[string]$logOutput
	)
	$OperationDateTime = $(Get-Date -Format o).Substring(0,24);
	$OperationLog = "$OperationDateTime : $logOutput";
	Write-Output $OperationLog;
	Add-content $logFile -value $OperationLog;
}

#-------------------------------------------------------------------------------------------------#
# Variables
#-------------------------------------------------------------------------------------------------#
$scriptHome = Split-Path $MyInvocation.MyCommand.Path
$scriptName = $MyInvocation.MyCommand.Name
$scriptName = $scriptName.Substring(0, ($scriptName.Length - 4))

$currentLogDate = Get-Date -format yyyyMMddHHmmss
$logFile = "$($scriptHome)\$($scriptName)_$($currentLogDate).log"

#-------------------------------------------------------------------------------------------------#
# Start script
#-------------------------------------------------------------------------------------------------#
Write-Log "Starting $($scriptName) ..."
Write-Log "Log available in $($logFile)"

#-------------------------------------------------------------------------------------------------#
# Initialize Settings from Config file
#-------------------------------------------------------------------------------------------------#
Write-Log "Reading configuration file $($ConfigPath)"
[xml]$ConfigFile = Get-Content "$($ConfigPath)"
$SQL_INSTANCE = $ConfigFile.Settings.SQLSettings.MSQL_SERVER_NAME
$SQL_DB_NAME = $ConfigFile.Settings.SQLProject.DatabaseName
$SQL_PROJECT_DACPAC = $ConfigFile.Settings.SQLProject.DacpacName
$SQL_PROJECT_PROFILE = $ConfigFile.Settings.SQLProject.ProfileName
Write-Log "SQL_INSTANCE        = $SQL_INSTANCE"
Write-Log "SQL_DB_NAME         = $SQL_DB_NAME"
Write-Log "SQL_PROJECT_DACPAC  = $SQL_PROJECT_DACPAC"
Write-Log "SQL_PROJECT_PROFILE = $SQL_PROJECT_PROFILE"

$SQL_DB_DEPLOY_SCRIPT = "$($scriptHome)\$($scriptName)_$($SQL_DB_NAME).sql"

# Store the Dac Assembly namespace to avoid typing it every time
$DACNamespace = "Microsoft.SqlServer.Management.Dac"

# Loading Dac Assembly .........
Write-Log "Loading Dac Assembly ..."
[System.Reflection.Assembly]::LoadWithPartialName("$DACNamespace")
add-type -path "C:\Program Files (x86)\Microsoft SQL Server\130\DAC\bin\Microsoft.SqlServer.Dac.dll"

#-------------------------------------------------------------------------------------------------#
# Connect to the instance
#-------------------------------------------------------------------------------------------------#
try
{
	# Create a DacServices object, which needs a connection string
	Write-Log "Creating DacServices object ..."
	$DacServices = new-object Microsoft.SqlServer.Dac.DacServices "server=$SQL_INSTANCE;Integrated Security=True;"

	# Load dacpac from file
	Write-Log "Loading dacpac from file $SQL_PROJECT_DACPAC ..."
	$DacPackage = [Microsoft.SqlServer.Dac.DacPackage]::Load("$($scriptHome)\$($SQL_PROJECT_DACPAC)")

	# Read a publish profile XML to get the deployment options
	Write-Log "Loading profile from file $SQL_PROJECT_PROFILE ..."
	$DacProfile = [Microsoft.SqlServer.Dac.DacProfile]::Load("$($scriptHome)\$($SQL_PROJECT_PROFILE)")

	# Update SQLCMD Variables
	Write-Log "Updating SQLCMD variables in profile ..."
	foreach ($CurrentVariable in $ConfigFile.Settings.SQLProject.SourceProjectVariables.Variable)
	{
		Write-Log "  $($CurrentVariable.Name) = $($CurrentVariable.Value)"
		$DacProfile.DeployOptions.SqlCommandVariableValues["$($CurrentVariable.Name)"] = "$($CurrentVariable.Value)"

		if ( "$($CurrentVariable.Name)" -eq "FilestreamDirectoryName" )
		{
			$FilestreamDirectoryName = $($CurrentVariable.Value)
		}
	}

	# Deploy database
	Write-Log "Generating deploy script for database $SQL_DB_NAME ..."
	$TSqlScript = $DacServices.GenerateDeployScript($DacPackage, $SQL_DB_NAME, $DacProfile.DeployOptions)
	$TSqlScript -Replace "SET FILESTREAM\((.*)DIRECTORY_NAME = N'[A-Za-z0-9_]+'(.*)\)", "SET FILESTREAM(`$1DIRECTORY_NAME = N'$($FilestreamDirectoryName)'`$2)" |  Out-File "$SQL_DB_DEPLOY_SCRIPT"

	if(-not $CheckOnly){
		Write-Log "Deploying database $SQL_DB_NAME ...";
		Invoke-Sqlcmd -inputfile "$SQL_DB_DEPLOY_SCRIPT" -serverinstance "$SQL_INSTANCE" -querytimeout 3600 -verbose *>&1 | Add-Content -path $logFile -encoding ASCII -passthru;
	}
}
catch
{
	Write-Log "Error : $($Error[0].exception.GetBaseException().Message)"
	exit 1
}
