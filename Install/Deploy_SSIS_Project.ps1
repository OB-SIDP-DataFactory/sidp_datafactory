#-------------------------------------------------------------------------------------------------#
# Call the PowerShell script and pass the arguments
# .\Setup_SSIS_Project.ps1 "C:\WorkArea\Scripts\ServerConfig.xml"
# .\Setup_SSIS_Project.ps1 -ConfigPath "C:\WorkArea\Scripts\ServerConfig.xml"
#-------------------------------------------------------------------------------------------------#

#-------------------------------------------------------------------------------------------------#
# Script parameters
#-------------------------------------------------------------------------------------------------#
[CmdletBinding()]
param (
	[Parameter(Mandatory = $true)]
	[string]$ConfigPath = $null
)

#-------------------------------------------------------------------------------------------------#
# Function Write-Log
#-------------------------------------------------------------------------------------------------#
Function Write-Log
{
	Param (
		[string]$logOutput
	)
	$OperationDateTime = $(Get-Date -Format o).Substring(0,24);
	$OperationLog = "$OperationDateTime : $logOutput";
	Write-Output $OperationLog;
	Add-content $logFile -value $OperationLog;
}

#-------------------------------------------------------------------------------------------------#
# Variables
#-------------------------------------------------------------------------------------------------#
$scriptHome = Split-Path $MyInvocation.MyCommand.Path
$scriptName = $MyInvocation.MyCommand.Name
$scriptName = $scriptName.Substring(0, ($scriptName.Length - 4))

$currentLogDate = Get-Date -format yyyyMMddHHmmss
$logFile = "$($scriptHome)\$($scriptName)_$($currentLogDate).log"

#-------------------------------------------------------------------------------------------------#
# Start script
#-------------------------------------------------------------------------------------------------#
Write-Log "Starting $($scriptName) ..."
Write-Log "Log available in $($logFile)"

#-------------------------------------------------------------------------------------------------#
# Initialize Settings from Config file
#-------------------------------------------------------------------------------------------------#
Write-Log "Reading configuration file $($ConfigPath)"
[xml]$ConfigFile = Get-Content "$($ConfigPath)"
$SSIS_SQL_INSTANCE = $ConfigFile.Settings.SSISSettings.MSQL_SERVER_NAME
$SSIS_PROJECT_FOLDER = $ConfigFile.Settings.SSISProject.FolderName
$SSIS_PROJECT_NAME = $ConfigFile.Settings.SSISProject.ProjectName
$SSIS_PROJECT_ISPAC = $ConfigFile.Settings.SSISProject.IspacName
$SSIS_PROJECT_ASSOCIATED_ENVIRONMENTS = $ConfigFile.Settings.AssociatedEnvironments.Environment

Write-Log "SSIS_SQL_INSTANCE   = $SSIS_SQL_INSTANCE"
Write-Log "SSIS_PROJECT_FOLDER = $SSIS_PROJECT_FOLDER"
Write-Log "SSIS_PROJECT_NAME   = $SSIS_PROJECT_NAME"
Write-Log "SSIS_PROJECT_ISPAC  = $SSIS_PROJECT_ISPAC"

# Store the IntegrationServices Assembly namespace to avoid typing it every time
$SSISNamespace = "Microsoft.SqlServer.Management.IntegrationServices"

# Loading IntegrationServices Assembly .........
Write-Log "Loading IntegrationServices Assembly ..."
[System.Reflection.Assembly]::LoadWithPartialName("$($SSISNamespace)")

#-------------------------------------------------------------------------------------------------#
# Deploy and setup the SSIS projec
#-------------------------------------------------------------------------------------------------#
try
{
	$SQLConnectionString = "Data Source=$SSIS_SQL_INSTANCE;Initial Catalog=master;Integrated Security = True;"
	$SQLConnection = New-Object System.Data.SqlClient.SqlConnection $SQLConnectionString
	$IntegrationServices = New-Object "$SSISNamespace.IntegrationServices" $SQLConnection
	$IntegrationServicesCatalog = $IntegrationServices.Catalogs["SSISDB"]

	Write-Log "Trying to retrieve SSIS project folder"
	$IntegrationServicesProjectFolder = $IntegrationServicesCatalog.Folders[$SSIS_PROJECT_FOLDER]

	#Check If the destination SSIS folder does not exists
	if($IntegrationServicesProjectFolder -eq $null)
	{
		Write-Log "Trying to create SSIS project folder ..."
		$IntegrationServicesProjectFolder = New-Object "$SSISNamespace.CatalogFolder" ($IntegrationServicesCatalog, $SSIS_PROJECT_FOLDER, $SSIS_PROJECT_FOLDER)
		$IntegrationServicesProjectFolder.Create()
		Write-Log "SSIS project folder has been created successfully"

		#Get the SSIS folder
		$IntegrationServicesProjectFolder = $IntegrationServicesCatalog.Folders[$SSIS_PROJECT_FOLDER]
	}

	#Check if project is already deployed or not, if deployed drop it
	if($IntegrationServicesProjectFolder.Projects.Item($SSIS_PROJECT_NAME))
	{
		Write-Log "Trying to drop SSIS project before deploying it ..."
		$IntegrationServicesProjectFolder.Projects.Item($SSIS_PROJECT_NAME).Drop()
		Write-Log "SSIS project has been dropped successfully"
	}

	# Deploy the SSIS project
	Write-Log "Trying to deploy SSIS project ..."
	[byte[]] $byte_ispac_file = [System.IO.File]::ReadAllBytes("$($scriptHome)\$($SSIS_PROJECT_ISPAC)")
	$IntegrationServicesProject = $IntegrationServicesProjectFolder.DeployProject($SSIS_PROJECT_NAME, $byte_ispac_file)
	Write-Log "SSIS project has been deployed successfully"


	# Associate environments with SSIS Project
	Write-Log "Trying to associate environments with SSIS Project"

	$IntegrationServicesProjectFolder.Refresh()
	$IntegrationServicesProject = $IntegrationServicesProjectFolder.Projects.Item($SSIS_PROJECT_NAME)

	foreach($currentEnvironmentToAssociate in $SSIS_PROJECT_ASSOCIATED_ENVIRONMENTS)
	{
		Write-Log "  - $($currentEnvironmentToAssociate.Name)"
		$IntegrationServicesProject.References.Add($currentEnvironmentToAssociate.Name, $IntegrationServicesProjectFolder.Name)
		$lastEnvironmentName = $currentEnvironmentToAssociate.Name
	}
	$IntegrationServicesProject.Alter()

	Write-Log "Environments have been associated successfully with SSIS Project"

	# Associate Project parameters to Environment variables
	Write-Log "Trying to associate environment variables with project parameters"

	$IntegrationServicesProjectFolder.Refresh()
	$IntegrationServicesEnvironment = $IntegrationServicesProjectFolder.Environments[$lastEnvironmentName]

	foreach($EnvironmentVariable in $IntegrationServicesEnvironment.Variables)
	{
		Write-Log "  - $($EnvironmentVariable.Name)"
		$IntegrationServicesProject.Parameters[$EnvironmentVariable.Name].Set("Referenced",$EnvironmentVariable.Name)
	}
	$IntegrationServicesProject.Alter()

	Write-Log "Environment variables have been associated successfully with project parameters"

	$SQLConnection.Close()
}
catch
{
	Write-Log "Error : $($Error[0].exception.GetBaseException().Message)"
	exit 1
}