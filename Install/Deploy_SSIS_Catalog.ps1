#-------------------------------------------------------------------------------------------------#
# Call the PowerShell script and pass the arguments
# .\Create_SSIS_Catalog.ps1 "C:\WorkArea\Scripts\ServerConfig.xml" "PW0Rd4sSsIsCAt10g"
# .\Create_SSIS_Catalog.ps1 -ConfigPath "C:\WorkArea\Scripts\ServerConfig.xml" -SSISCatalogPassword "PW0Rd4sSsIsCAt10g"
#-------------------------------------------------------------------------------------------------#

#-------------------------------------------------------------------------------------------------#
# Script parameters
#-------------------------------------------------------------------------------------------------#
[CmdletBinding()]
param (
	[Parameter(Mandatory = $true)]
	[string]$ConfigPath = $null,
	[string]$SSISCatalogPassword = $null
)

#-------------------------------------------------------------------------------------------------#
# Function Write-Log
#-------------------------------------------------------------------------------------------------#
Function Write-Log
{
	Param (
		[string]$logOutput
	)
	$OperationDateTime = $(Get-Date -Format o).Substring(0,24);
	$OperationLog = "$OperationDateTime : $logOutput";
	Write-Output $OperationLog;
	Add-content $logFile -value $OperationLog;
}

#-------------------------------------------------------------------------------------------------#
# Variables
#-------------------------------------------------------------------------------------------------#
$scriptHome = Split-Path $MyInvocation.MyCommand.Path
$scriptName = $MyInvocation.MyCommand.Name
$scriptName = $scriptName.Substring(0, ($scriptName.Length - 4))

$currentLogDate = Get-Date -format yyyyMMddHHmmss
$logFile = "$($scriptHome)\$($scriptName)_$($currentLogDate).log"

#-------------------------------------------------------------------------------------------------#
# Start script
#-------------------------------------------------------------------------------------------------#
Write-Log "Starting $($scriptName) ..."
Write-Log "Log available in $($logFile)"

#-------------------------------------------------------------------------------------------------#
# Initialize Settings from Config file
#-------------------------------------------------------------------------------------------------#
Write-Log "Reading configuration file $($ConfigPath)"
[xml]$ConfigFile = Get-Content "$($ConfigPath)"
$SSIS_SQL_INSTANCE = $ConfigFile.Settings.SSISSettings.MSQL_SERVER_NAME
Write-Log "SSIS_SQL_INSTANCE = $SSIS_SQL_INSTANCE"

# Store the IntegrationServices Assembly namespace to avoid typing it every time
$SSISNamespace = "Microsoft.SqlServer.Management.IntegrationServices"

# Loading IntegrationServices Assembly .........
Write-Log "Loading IntegrationServices Assembly ..."
[System.Reflection.Assembly]::LoadWithPartialName("$($SSISNamespace)")

Write-Log "Loading Microsoft.SQLServer.SMO Assembly ..."
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SQLServer.SMO")

#-------------------------------------------------------------------------------------------------#
# Pre-Requisite - Enable CLR before creating catalog
#-------------------------------------------------------------------------------------------------#
try
{
	Write-Log "Checking CLR on the SQL Server ..."
	$SQLServer = New-Object Microsoft.SQLServer.Management.SMO.Server $SSIS_SQL_INSTANCE
	$SQLServerConfiguration = $SQLServer.Configuration
	$CLRValue = $SQLServerConfiguration.IsSqlClrEnabled
}
catch
{
	# Error connecting to database
	Write-Log "Error checking CLR on the SQL Server $SSIS_SQL_INSTANCE"
	Write-Log "Error : $($Error[0].exception.GetBaseException().Message)"
	exit 1
}

If ($CLRValue.ConfigValue -eq 0)
{
	Write-Log "Enabling CLR on the server $SSIS_SQL_INSTANCE"
	$CLRValue.ConfigValue = 1
	$SQLServerConfiguration.Alter()
	Write-Log "CLR enabled on the server $SSIS_SQL_INSTANCE successfully"
}
Else
{
	Write-Log "CLR is already enabled on the server $SSIS_SQL_INSTANCE"
}

#-------------------------------------------------------------------------------------------------#
# Create the SSIS catalog
#-------------------------------------------------------------------------------------------------#
$SQLConnectionString = "Data Source=$SSIS_SQL_INSTANCE;Initial Catalog=master;Integrated Security = True;"

# Create SQL Server connection based on the connection string
try
{
	Write-Log "Trying to connect SQL Server $($SSIS_SQL_INSTANCE) ..."
	$SQLConnection = New-Object System.Data.SqlClient.SqlConnection $SQLConnectionString
	Write-Log "SQL Server connection has been enabled successfully"
}
catch
{
	# Error connecting to database
	Write-Log "Error connecting to database"
	Write-Log "Error : $($Error[0].exception.GetBaseException().Message)"
	exit 1
}


# Create Integration Services object based on the SQL Server connection
try
{
	Write-Log "Trying to create Integration Services object based on the SQL Server connection"
	$IntegrationServices = New-Object "$SSISNamespace.IntegrationServices" $SQLConnection
	Write-Log "Integration Services object has been created successfully"
}
catch
{
	# Error creating Integration Services object based on the SQL Server connection
	Write-Log "Error creating Integration Services object based on the SQL Server connection"
	Write-Log "Error : $($Error[0].exception.GetBaseException().Message)"
	exit 1
}

# Create SSIS catalog
if(!$IntegrationServices.Catalogs["SSISDB"])
{
	if ( !$SSISCatalogPassword )
	{
		Write-Log "SSISCatalogPassword not provided"
		$SSISCatalogPassword = Read-Host -Prompt "Enter catalog password"# -AsSecureString
	}

	try
	{
		Write-Log "Trying to create SSIS catalog ..."
		$SSISCatalog = New-Object "$SSISNamespace.Catalog" ($IntegrationServices, "SSISDB", $SSISCatalogPassword)
		$SSISCatalog.Create()
		Write-Log "Integration Services catalog has been created successfully"
	}
	catch
	{
		# Error creating Integration Services object based on the SQL Server connection
		Write-Log "Error creating Integration Services catalog"
		Write-Log "Error : $($Error[0].exception.GetBaseException().Message)"
		exit 1
	}
}
else
{
	Write-Log "Integration Services catalog already exists"
}
