#-------------------------------------------------------------------------------------------------#
# Call the PowerShell script and pass the arguments
# .\Deploy_SSRS.ps1 "C:\WorkArea\Scripts\Deploy_SSRS.config"
# .\Deploy_SSRS.ps1 -ConfigPath ".\Deploy_SSRS.config" -ReportServerUrl "http://OBKSVWCR05/ReportServer_Development" -SourcePath "\OB_SIDP_Solution\SIDPSolutions\SSRS_Solution" -InstallMode I
#-------------------------------------------------------------------------------------------------#

#-------------------------------------------------------------------------------------------------#
# Script parameters
#-------------------------------------------------------------------------------------------------#
[CmdletBinding()]
param (
	[Parameter(Mandatory = $true)]
	[string]$ConfigPath = $null,
	[Parameter(Mandatory = $true)]
	[string]$ReportServerUrl = $null,
	[Parameter(Mandatory = $true)]
	[string]$SourcePath = $null,
	[string]$InstallMode = "I",
	[string]$RootFolder = "/"
)

#-------------------------------------------------------------------------------------------------#
# Function Write-Log
#-------------------------------------------------------------------------------------------------#
Function Write-Log
{
	Param (
		[string]$logOutput
	)
	$OperationDateTime = $(Get-Date -Format o).Substring(0,24);
	$OperationLog = "$OperationDateTime : $logOutput";
	Write-Output $OperationLog;
	Add-content $logFile -value $OperationLog;
}

#-------------------------------------------------------------------------------------------------#
# Function DeployFolders
#-------------------------------------------------------------------------------------------------#
Function DeployFolders
{
	Write-Log "--------------------------------------------------------------------------------"
	Write-Log "Deploying Folders ..."
	Write-Log "--------------------------------------------------------------------------------"

	foreach ($CurrentFolderXML in $ConfigFile.Deploy_SSRS.Folders.Folder)
	{
		if ($InstallMode -eq "I")
		{
			Write-Log "Deploy '$($CurrentFolderXML.Path)' ..."
			CreateFolder $CurrentFolderXML.GetAttribute("Path") $CurrentFolderXML.SelectSingleNode("Properties")
		}

		ApplyItemPolicies $CurrentFolderXML.GetAttribute("Path") "Folder" $CurrentFolderXML.SelectSingleNode("Policies")
	}
}

#-------------------------------------------------------------------------------------------------#
# Function CreateFolder
#-------------------------------------------------------------------------------------------------#
Function CreateFolder
{
	Param ([string]$FolderPath, [Xml.XmlElement]$FolderPropetiesXML)

	# Initialize $ParentPath
	$ParentPath = "/"

	# Loop to create the complete path
	foreach ($CurrentFolderName in $FolderPath.Split("/", [System.StringSplitOptions]::RemoveEmptyEntries))
	{
		if ($ParentPath -eq "/")
		{
			$CurrentFolderPath = "/$($CurrentFolderName)"
		}
		else
		{
			$CurrentFolderPath = "$($ParentPath)/$($CurrentFolderName)"
		}

		if ($(ItemExistsInRS $ParentPath $CurrentFolderName "Folder") -eq $false)
		{
			Write-Log "Create '$($FolderPath)' ..."
			$FolderProperties = $(CreateProperties $FolderPropetiesXML)
			$ReportServerWS.CreateFolder($CurrentFolderName, $ParentPath, $FolderProperties) *>&1 | Add-Content -path $logFile -encoding ASCII -passthru
		}

		$ParentPath = "$($CurrentFolderPath)"
	}
}

#-------------------------------------------------------------------------------------------------#
# Function DeployDatasources
#-------------------------------------------------------------------------------------------------#
Function DeployDatasources
{
	Write-Log "--------------------------------------------------------------------------------"
	Write-Log "Deploying DataSources ..."
	Write-Log "--------------------------------------------------------------------------------"

	foreach ($CurrentDatasourceXML in $ConfigFile.Deploy_SSRS.DataSources.DataSource)
	{
		if ($InstallMode -eq "I")
		{
			# Create the folder
			if ($CurrentDatasourceXML.Path -ne "/")
			{
				CreateFolder $CurrentDatasourceXML.Path
			}
			
			# Publish the Datasource if it does not already exists
			if ($(ItemExistsInRS $CurrentDatasourceXML.Path $CurrentDatasourceXML.Name "DataSource") -eq $false)
			{
				Write-Log "Deploy '$($CurrentDatasourceXML.Path)/$($CurrentDatasourceXML.Name)' ..."
				PublishDatasource $CurrentDatasourceXML
			}
			else
			{
				Write-Log "'$($CurrentDatasourceXML.Path)/$($CurrentDatasourceXML.Name)' already exists"
			}
		}

		ApplyItemPolicies "$($CurrentDatasourceXML.Path)/$($CurrentDatasourceXML.Name)" "DataSource" $CurrentDatasourceXML.SelectSingleNode("Policies")
	}
}

#-------------------------------------------------------------------------------------------------#
# Function PublishDatasource
#-------------------------------------------------------------------------------------------------#
Function PublishDatasource
{
	Param ([Xml.XmlElement]$DatasourceXML)

	$CredentialRetrieval = New-Object ($CredentialRetrievalEnumNameSpace)
	$DataSourceDefinition = New-Object ($DataSourceDefinitionNameSpace)
	$DataSourceDefinition.Enabled = $true
	$DataSourceDefinition.EnabledSpecified = $true
	$DataSourceDefinition.Extension = $DatasourceXML.Extension
	$DataSourceDefinition.ConnectString = $DatasourceXML.ConnectionString
	$DataSourceDefinition.CredentialRetrieval = [System.Enum]::Parse($CredentialRetrieval.GetType(), $DatasourceXML.Credential.Retrieval)

	foreach ($CredentialOption in $DatasourceXML.Credential.Option)
	{
		switch($CredentialOption.Name)
		{
			"Prompt"
			{
				if ($DatasourceXML.Credential.Retrieval -eq "Prompt")
				{
					$DataSourceDefinition.Prompt = $CredentialOption.Value
				}
			}
			"UserName"
			{
				if ($DatasourceXML.Credential.Retrieval -eq "Store")
				{
					$DataSourceDefinition.UserName = $CredentialOption.Value
				}
			}
			"Password"
			{
				if ($DatasourceXML.Credential.Retrieval -eq "Store")
				{
					$DataSourceDefinition.Password = $CredentialOption.Value
				}
			}
			"WindowsCredentials"
			{
				if (($DatasourceXML.Credential.Retrieval -eq "Store") -or ($DatasourceXML.Credential.Retrieval -eq "Prompt"))
				{
					$DataSourceDefinition.WindowsCredentials = [boolean]$CredentialOption.Value
				}
			}
			"ImpersonateUser"
			{
				 if ($DatasourceXML.Credential.Retrieval -eq "Store")
				{
					$DataSourceDefinition.ImpersonateUser = [boolean]$CredentialOption.Value
					$DataSourceDefinition.ImpersonateUserSpecified = [boolean]$CredentialOption.Value
				}
			}
		}
	}

	Write-Log "Create '$($DatasourceXML.Path)/$($DatasourceXML.Name)' ..."
	$DatasourceProperties = $(CreateProperties $DatasourceXML.SelectSingleNode("Properties"))
	$ReportServerWS.CreateDataSource($DatasourceXML.Name, $DatasourceXML.Path, $true, $DataSourceDefinition, $DatasourceProperties) *>&1 | Add-Content -path $logFile -encoding ASCII -passthru
}

#-------------------------------------------------------------------------------------------------#
# Function DeployItems
#-------------------------------------------------------------------------------------------------#
Function DeployItems
{
	Param ([string]$ItemType)

	Write-Log "--------------------------------------------------------------------------------"
	Write-Log "Deploying $($ItemType)s ..."
	Write-Log "--------------------------------------------------------------------------------"

	foreach ($CurrentItemXML in $ConfigFile.Deploy_SSRS.SelectSingleNode("$($ItemType)s").SelectNodes($($ItemType)))
	{
		if ($InstallMode -eq "I")
		{
			# Create the folder
			if ($CurrentItemXML.Path -ne "/")
			{
				CreateFolder $CurrentItemXML.Path
			}

			switch($ItemType)
			{
				"Report" { $ItemFileExtension = ".rdl" }
				"DataSet" { $ItemFileExtension = ".rsd" }
			}

			$ItemFilePath = "$($SourcePath)\$($CurrentItemXML.Name)$($ItemFileExtension)";

			$fileExists = [System.IO.File]::Exists($ItemFilePath);

			if($fileExists -eq $true)
			{
				# Remove Item first if it already exists
				if ($(ItemExistsInRS $CurrentItemXML.Path $CurrentItemXML.Name $ItemType) -eq $true)
				{
					Write-Log "Delete '$($CurrentItemXML.Path)/$($CurrentItemXML.Name)' ..."
					$ReportServerWS.DeleteItem("$($CurrentItemXML.Path)/$($CurrentItemXML.Name)")  *>&1 | Add-Content -path $logFile -encoding ASCII -passthru
				}

				# Publish the Item
				PublishItem $ItemType $CurrentItemXML

				# Update External Datasources references for reports
				if (($ItemType -eq "Report") -or ($ItemType -eq "DataSet"))
				{
					UpdateItemDataSources $ItemType $CurrentItemXML
				}

				if (($ItemType -eq "Report"))
				{
					UpdateItemReferences $ItemType "DataSet" $CurrentItemXML
					CreateReportSubscriptions $CurrentItemXML
				}
			}
		}

		ApplyItemPolicies "$($CurrentItemXML.Path)/$($CurrentItemXML.Name)" $ItemType $CurrentItemXML.SelectSingleNode("Policies")
	}
}

#-------------------------------------------------------------------------------------------------#
# Function PublishItem
#-------------------------------------------------------------------------------------------------#
Function PublishItem
{
	Param (
		[string]$ItemType,
		[Xml.XmlElement]$ItemXML
	)

	switch($ItemType)
	{
		"Report" { $ItemFileExtension = ".rdl" }
		"DataSet" { $ItemFileExtension = ".rsd" }
	}

	$ItemFilePath = "$($SourcePath)\$($ItemXML.Name)$($ItemFileExtension)"

	Write-Log "Create '$($ItemXML.Path)/$($ItemXML.Name)' ..."
	$ItemFileStream = Get-Content $ItemFilePath -Encoding byte
	$ItemProperties = $(CreateProperties $ItemXML.SelectSingleNode("Properties"))
	$ItemWarnings = @()
	$ReportServerWS.CreateCatalogItem($ItemType, $ItemXML.Name, $ItemXML.Path, $true, $ItemFileStream, $ItemProperties, [ref]$ItemWarnings)  *>&1 | Add-Content -path $logFile -encoding ASCII -passthru
	Write-Log "$ItemType '$($ItemXML.Path)/$($ItemXML.Name)' successfully published with $($ItemWarnings.Length) warning(s)"
}

#-------------------------------------------------------------------------------------------------#
# Function ApplyItemPolicies
#-------------------------------------------------------------------------------------------------#
Function ApplyItemPolicies
{
	Param ([string]$ItemPath, [string]$ItemType, [Xml.XmlElement]$PoliciesXML)

	$ItemPathArray = $ItemPath.Split("/", [System.StringSplitOptions]::RemoveEmptyEntries)
	$ItemName = $ItemPathArray[$ItemPathArray.Count - 1]
	$ItemParentPath = "/" + [System.String]::Join("/", $ItemPathArray, 0, $ItemPathArray.Count - 1)

	$ItemPolicies = $(CreatePolicies $PoliciesXML)

	# Apply policies if item exists
	if (($(ItemExistsInRS $ItemParentPath $ItemName $ItemType) -eq $true) -and ($ItemPolicies -ne $null))
	{
		Write-Log "Apply policies for '$($ItemPath)' ..."
		$ReportServerWS.SetPolicies("$($ItemPath)", $ItemPolicies) *>&1 | Add-Content -path $logFile -encoding ASCII -passthru
	}
}

#-------------------------------------------------------------------------------------------------#
# Function UpdateItemDataSources
#-------------------------------------------------------------------------------------------------#
Function UpdateItemDataSources
{
	Param ([string]$ItemType, [Xml.XmlElement]$ItemXML)

	Write-Log "Update Datasources references for '$($ItemXML.Path)/$($ItemXML.Name)' ..."

	$ItemDataSources = $ReportServerWS.GetItemDataSources("$($ItemXML.Path)/$($ItemXML.Name)")

	foreach ($ItemDataSource in $ItemDataSources)
	{
		if ($ItemDataSource.Item.GetType().Name -eq "InvalidDataSourceReference")
		{
			$ReferenceDataSourceName = ""

			# Look for the Datasource
			foreach ($DataSourceOption in $ItemXML.SelectSingleNode("Options/DataSource[@Name='$($ItemDataSource.Name)']"))
			{
				$ReferenceDataSourceName = $DataSourceOption.GetAttribute("ReferenceDataSourceName")
			}

			if ($ReferenceDataSourceName -eq "")
			{
				$ReferenceDataSourceName = $ItemDataSource.Name
			}

			$BooleanOperatorEnum = New-Object ($BooleanOperatorEnumNameSpace)
			$Conditions = $(CreateSearchConditions "Name=$($ReferenceDataSourceName)")
			$PropertiesXML = [xml]"<Properties><Property Name='Resursive' Value='True'/></Properties>"
			$Properties = $(CreateProperties $PropertiesXML.SelectSingleNode("Properties"))

			foreach ($CurrentItem in $ReportServerWS.FindItems($RootFolder, [System.Enum]::Parse($BooleanOperatorEnum.GetType(), "And"), $Properties, $Conditions))
			{
				if ($CurrentItem.TypeName -eq "DataSource")
				{
					Write-Log "  - $ReferenceDataSourceName"
					$ItemDataSourceReference = New-Object ($DataSourceReferenceNameSpace)
					$ItemDataSourceReference.Reference = $CurrentItem.Path
					$ItemDataSource.Item = $ItemDataSourceReference
					break
				}
			}
		}
	}

	$ReportServerWS.SetItemDataSources("$($ItemXML.Path)/$($ItemXML.Name)", $ItemDataSources) *>&1 | Add-Content -path $logFile -encoding ASCII -passthru
}

#-------------------------------------------------------------------------------------------------#
# Function UpdateItemReferences
#-------------------------------------------------------------------------------------------------#
Function UpdateItemReferences
{
	Param ([string]$ItemType, [string]$ItemReferenceType, [Xml.XmlElement]$ItemXML)

	Write-Log "Update $($ItemReferenceType) references for '$($ItemXML.Path)/$($ItemXML.Name)' ..."

	$ItemReferences = $ReportServerWS.GetItemReferences("$($ItemXML.Path)/$($ItemXML.Name)", $ItemReferenceType)
	$NewItemReferences = New-Object ($ItemReferenceNameSpace + '[]')0

	foreach ($ItemReference in $ItemReferences)
	{
		$NewItemReference = New-Object ($ItemReferenceNameSpace)
		$NewItemReference.Name = $ItemReference.Name

		if ($ItemReference.Reference -eq $null)
		{
			$ReferenceItemName = ""

			# Look for the Item
			foreach ($ItemOption in $ItemXML.SelectSingleNode("Options/Reference[@Name='$($ItemReference.Name)']"))
			{
				$ReferenceItemName = $ItemOption.GetAttribute("ReferenceItemName")
			}

			if ($ReferenceItemName -eq "")
			{
				$ReferenceItemName = $ItemReference.Name
			}

			$BooleanOperatorEnum = New-Object ($BooleanOperatorEnumNameSpace)
			$Conditions = $(CreateSearchConditions "Name=$($ReferenceItemName)")
			$PropertiesXML = [xml]"<Properties><Property Name='Resursive' Value='True'/></Properties>"
			$Properties = $(CreateProperties $PropertiesXML.SelectSingleNode("Properties"))

			foreach ($CurrentItem in $ReportServerWS.FindItems($RootFolder, [System.Enum]::Parse($BooleanOperatorEnum.GetType(), "And"), $Properties, $Conditions))
			{
				if ($CurrentItem.TypeName -eq $ItemReferenceType)
				{
					Write-Log "  - $ReferenceItemName"
					$NewItemReference.Reference = $CurrentItem.Path
					break
				}
			}
		}
		else
		{
			$NewItemReference.Reference = $ItemReference.Reference
		}
		$NewItemReferences += $NewItemReference
	}

	$ReportServerWS.SetItemReferences("$($ItemXML.Path)/$($ItemXML.Name)", $NewItemReferences) *>&1 | Add-Content -path $logFile -encoding ASCII -passthru
}

#-------------------------------------------------------------------------------------------------#
# Function CreateReportSubscriptions
#-------------------------------------------------------------------------------------------------#
Function CreateReportSubscriptions
{
	Param ([Xml.XmlElement]$ItemXML)
	Write-Log "Create subscriptions for '$($ItemXML.Path)/$($ItemXML.Name)' ..."

	foreach ($CurrentSubscriptionDefinition in $ItemXML.SelectSingleNode("Subscriptions").SelectNodes("Subscription"))
	{
		Write-Log "  - $($CurrentSubscriptionDefinition.Name)"
		$Settings = New-Object ($ExtensionSettingsNameSpace)
		$Settings.Extension = $CurrentSubscriptionDefinition.Extension.Type

		# Set the extension parameter values
		$ExtensionParams = New-Object ($ParameterValueOrFieldReferenceNameSpace + '[]')0
		foreach ($ExtensionParamDefinition in $CurrentSubscriptionDefinition.Extension.SelectSingleNode("ExtensionParameters").SelectNodes("ExtensionParameter"))
		{
			if ($ExtensionParamDefinition.Type -eq "FieldAlias")
			{
				$ExtensionParam = New-Object ($ParameterFieldReferenceNameSpace)
				$ExtensionParam.ParameterName = "$($ExtensionParamDefinition.Name)"
				$ExtensionParam.FieldAlias = "$($ExtensionParamDefinition.Value)"
			}
			else
			{
				$ExtensionParam = New-Object ($ParameterValueNameSpace)
				$ExtensionParam.Name = "$($ExtensionParamDefinition.Name)"
				$ExtensionParam.Value = "$($ExtensionParamDefinition.Value)"
			}
			$ExtensionParams += $ExtensionParam
		}
		$Settings.ParameterValues = $ExtensionParams

		# Create the data source for the delivery query.
		$DataSource = New-Object ($DataSourceNameSpace)

		$DataSourceReference = New-Object ($DataSourceReferenceNameSpace)
		$DataSourceReference.Reference = "$($CurrentSubscriptionDefinition.DataSet.DataSourcePath)/$($CurrentSubscriptionDefinition.DataSet.DataSourceName)"
		$DataSource.Name = "$($CurrentSubscriptionDefinition.DataSet.DataSourceName)"
		$DataSource.Item = $DataSourceReference

		# Create the dataset for the delivery query
		$DataSetDefinition = New-Object ($DataSetDefinitionNameSpace)
		$DataSetDefinition.AccentSensitivitySpecified = $false
		$DataSetDefinition.CaseSensitivitySpecified = $false
		$DataSetDefinition.KanatypeSensitivitySpecified = $false
		$DataSetDefinition.WidthSensitivitySpecified = $false

		# Create the fields list for the dataset
		$DataSetFieldsList = New-Object ($DataSetFieldNameSpace + '[]')0
		foreach ($DataSetFieldDefinition in $CurrentSubscriptionDefinition.DataSet.SelectSingleNode("DataSetFields").SelectNodes("DataSetField"))
		{
			$DataSetField = New-Object ($DataSetFieldNameSpace)
			$DataSetField.Name = "$($DataSetFieldDefinition.Name)"
			$DataSetField.Alias = "$($DataSetFieldDefinition.Alias)"
			$DataSetFieldsList += $DataSetField
		}
		$DataSetDefinition.Fields = $DataSetFieldsList

		# Create the query for the dataset
		$QueryDefinition = New-Object ($QueryDefinitionNameSpace)
		$QueryDefinition.CommandText = "$($CurrentSubscriptionDefinition.DataSet.Query)"
		$QueryDefinition.CommandType = "Text"
		$QueryDefinition.Timeout = 30
		$QueryDefinition.TimeoutSpecified = $true
		$DataSetDefinition.Query = $QueryDefinition

		$Results = New-Object ($DataSetDefinitionNameSpace)
		$Changed = New-Object ('Boolean')
		$ParamNames = New-Object ('String[]')0
		$Results = $ReportServerWS.PrepareQuery($DataSource, $DataSetDefinition, [ref]$Changed, [ref]$ParamNames)

		$DataRetrieval = New-Object ($DataRetrievalPlanNameSpace)
		$DataRetrieval.DataSet = $Results
		$DataRetrieval.Item = $DataSourceReference

		# Set the report parameter values
		$ReportParams = New-Object ($ParameterValueOrFieldReferenceNameSpace + '[]')0
		foreach ($ReportParamDefinition in $CurrentSubscriptionDefinition.SelectSingleNode("ReportParameters").SelectNodes("ReportParameter"))
		{
			if ($ReportParamDefinition.Type -eq "FieldAlias")
			{
				$ReportParam = New-Object ($ParameterFieldReferenceNameSpace)
				$ReportParam.ParameterName = "$($ReportParamDefinition.Name)"
				$ReportParam.FieldAlias = "$($ReportParamDefinition.Value)"
			}
			else
			{
				$ReportParam = New-Object ($ParameterValueNameSpace)
				$ReportParam.Name = "$($ReportParamDefinition.Name)"
				$ReportParam.Value = "$($ReportParamDefinition.Value)"
			}
			$ReportParams += $ReportParam
		}

		$EventType = "TimedSubscription"
		$MatchData = $CurrentSubscriptionDefinition.MatchData.Value
		$SubscriptionId = $ReportServerWS.CreateDataDrivenSubscription($ItemXML.Path + "/" + $ItemXML.Name, $Settings, $DataRetrieval, $CurrentSubscriptionDefinition.Description, $EventType, $MatchData, $ReportParams)
		Write-Log "    Subscription '$($SubscriptionId)' successfully created"
		Write-Log "DEBUG " + $ReportServerNameSpace
		$SettingsTest = New-Object ($ExtensionSettingsNameSpace)
		$DataRetrievalTest = New-Object ($DataRetrievalPlanNameSpace)
		$S1Test = ""
		$Active = New-Object ($ActiveStateNameSpace)
		$S2Test = ""
		$S3Test = ""
		$S4Test = ""
		$ReportParamsTest = New-Object ($ParameterValueOrFieldReferenceNameSpace + '[]')0

		$SubscriptionOwner = $ReportServerWS.GetDataDrivenSubscriptionProperties($SubscriptionId, [ref]$SettingsTest, [ref]$DataRetrievalTest, [ref]$S1Test, [ref]$Active, [ref]$S2Test, [ref]$S3Test, [ref]$S4Test, [ref]$ReportParamsTest)

		foreach ($test in $SettingsTest.ParameterValues)
		{
			if ("$($test.ParameterName)" -eq "")
			{
				Write-Log "    Static Value : $($test.Name) - $($test.Value)"
			}
			else
			{
				Write-Log "    Parameter : $($test.ParameterName) - $($test.FieldAlias)"
			}
		}

	}
}

#-------------------------------------------------------------------------------------------------#
# Function RemoveItems
#-------------------------------------------------------------------------------------------------#
Function RemoveItems
{
	Param ([string]$ItemType)

	Write-Log "--------------------------------------------------------------------------------"
	Write-Log "Removing $($ItemType)s ..."
	Write-Log "--------------------------------------------------------------------------------"

	foreach ($CurrentItemXML in $ConfigFile.Deploy_SSRS.SelectSingleNode("$($ItemType)s").SelectNodes($($ItemType)))
	{
		if ($ItemType -eq "Folder")
		{
			$SubFolders = $CurrentItemXML.Path.Split("/", [System.StringSplitOptions]::RemoveEmptyEntries)

			$ItemPath = $CurrentItemXML.Path
			$ItemName = $SubFolders[$SubFolders.Count - 1]
			$ItemParentPath = "/" + [System.String]::Join("/", $SubFolders, 0, $SubFolders.Count - 1)
		}
		else
		{
			$ItemPath = "$($CurrentItemXML.Path)/$($CurrentItemXML.Name)"
			$ItemName = $CurrentItemXML.Name
			$ItemParentPath = $CurrentItemXML.Path
		}

		if ($(ItemExistsInRS $ItemParentPath $ItemName $ItemType) -eq $true)
		{
			Write-Log "Deleting '$($ItemPath)' ..."
			$ReportServerWS.DeleteItem($ItemPath)  *>&1 | Add-Content -path $logFile -encoding ASCII -passthru
		}
	}
}

#-------------------------------------------------------------------------------------------------#
# Function CreateProperties
#-------------------------------------------------------------------------------------------------#
Function CreateProperties
{
	Param ([Xml.XmlElement]$PropertiesXML)

	$Properties = New-Object ($PropertyNameSpace + '[]')0

	foreach ($PropertyXML in $PropertiesXML.Property)
	{
		$NewPropertyFlag = $true

		foreach ($Property in $Properties)
		{
			if ($Property.Name -eq $PropertyName)
			{
				$Property.Value = $PropertyValue
				$NewPropertyFlag = $false
			}
		}

		if ($NewPropertyFlag -eq $true)
		{
			$Property = New-Object ($PropertyNameSpace)
			$Property.Name = $PropertyXML.Name
			$Property.Value = $PropertyXML.Value
			$Properties += $Property
		}
	}

	return $Properties
}

#-------------------------------------------------------------------------------------------------#
# Function CreatePolicies
#-------------------------------------------------------------------------------------------------#
Function CreatePolicies
{
	Param ([Xml.XmlElement]$PoliciesXML)

	$PoliciesDict = New-Object ('System.Collections.Generic.Dictionary[String, String]')

	foreach ($RoleXML in $PoliciesXML.Role)
	{
		foreach ($RoleMemberXML in $RoleXML.Member)
		{
			if ($PoliciesDict.ContainsKey($RoleMemberXML.Name) -eq $true)
			{
				$PoliciesDict[$RoleMemberXML.Name] = $PoliciesDict[$RoleMemberXML.Name] + "|" + $RoleXML.Name
			}
			else
			{
				$PoliciesDict.Add($RoleMemberXML.Name, $RoleXML.Name)
			}
		}
	}

	$Policies = New-Object ($PolicyNameSpace + '[]')0

	foreach ($GroupUserName in $PoliciesDict.Keys)
	{
		$Policy = New-Object ($PolicyNameSpace)
		$Policy.GroupUserName = $GroupUserName
		$Roles = New-Object ($RoleNameSpace + '[]')0

		foreach ($RoleName in $PoliciesDict[$GroupUserName].Split("|", [System.StringSplitOptions]::RemoveEmptyEntries))
		{
			$Role = New-Object ($RoleNameSpace)
			$Role.Name = $RoleName
			$Roles += $Role
		}

		$Policy.Roles = $Roles
		$Policies += $Policy
	}

	return $Policies
}

#-------------------------------------------------------------------------------------------------#
# Function CreateSearchConditions
#-------------------------------------------------------------------------------------------------#
Function CreateSearchConditions
{
	Param ([string]$SearchConditionStr)

	$ConditionEnum = New-Object ($ConditionEnumNameSpace)
	$SearchConditions = New-Object ($SearchConditionNameSpace + '[]')0

	foreach ($SearchConditionsDefinition in $SearchConditionStr.Split(";", [System.StringSplitOptions]::RemoveEmptyEntries))
	{
		$SearchCondition = New-Object ($SearchConditionNameSpace)

		$SearchCondition.Condition = [System.Enum]::Parse($ConditionEnum.GetType(), "Equals")
		$SearchCondition.ConditionSpecified = $true
		$SearchCondition.Name = $SearchConditionsDefinition.Split("=")[0]
		$SearchCondition.Values = $SearchConditionsDefinition.Split("=")[1].Split("|")

		$SearchConditions += $SearchCondition
	}

	return $SearchConditions
}

#-------------------------------------------------------------------------------------------------#
# Function ItemExistsInRS
#-------------------------------------------------------------------------------------------------#
Function ItemExistsInRS
{
	Param (
		[string]$ItemPath,
		[string]$ItemName,
		[string]$ItemType
	)

	$ItemExists = $false
	$FolderItems = $ReportServerWS.ListChildren($ItemPath, $false)

	foreach ($FolderItem in $FolderItems)
	{
		if (($FolderItem.TypeName -eq $ItemType) -and ($FolderItem.Name -eq $ItemName))
		{
			$ItemExists = $true
		}
	}

	return [boolean]$ItemExists
}

#-------------------------------------------------------------------------------------------------#
# Variables
#-------------------------------------------------------------------------------------------------#
$scriptHome = Split-Path $MyInvocation.MyCommand.Path
$scriptName = $MyInvocation.MyCommand.Name
$scriptName = $scriptName.Substring(0, ($scriptName.Length - 4))

$currentLogDate = Get-Date -format yyyyMMddHHmmss
$logFile = "$($scriptHome)\$($scriptName)_$($currentLogDate).log"

#-------------------------------------------------------------------------------------------------#
# Start script
#-------------------------------------------------------------------------------------------------#
Write-Log "Starting $($scriptName) ..."
Write-Log "Log available in $($logFile)"

#-------------------------------------------------------------------------------------------------#
# Initialize Settings from Config file
#-------------------------------------------------------------------------------------------------#
Write-Log "Reading configuration file $($ConfigPath)"
[xml]$ConfigFile = Get-Content "$($ConfigPath)"

#-------------------------------------------------------------------------------------------------#
# Start
#-------------------------------------------------------------------------------------------------#
try
{
	# Create a WebServiceProxy object
	Write-Log "Connecting to $($ReportServerUrl) ..."
	$ReportServerUri = "$($ReportServerUrl)/ReportService2010.asmx?wsdl"
	$ReportServerWS = New-WebServiceProxy -Uri $ReportServerUri -UseDefaultCredential
	$ReportServerNameSpace = ($ReportServerWS.GetType().Namespace)
	$PropertyNameSpace = ($ReportServerWS.GetType().Namespace + '.Property')
	$PolicyNameSpace = ($ReportServerWS.GetType().Namespace + '.Policy')
	$RoleNameSpace = ($ReportServerWS.GetType().Namespace + '.Role')
	$DataSourceDefinitionNameSpace = ($ReportServerWS.GetType().Namespace + '.DataSourceDefinition')
	$DataSourceReferenceNameSpace = ($ReportServerWS.GetType().Namespace + '.DataSourceReference')
	$CredentialRetrievalEnumNameSpace = ($ReportServerWS.GetType().Namespace + '.CredentialRetrievalEnum')
	$SearchConditionNameSpace = ($ReportServerWS.GetType().Namespace + '.SearchCondition')
	$ConditionEnumNameSpace = ($ReportServerWS.GetType().Namespace + '.ConditionEnum')
	$BooleanOperatorEnumNameSpace = ($ReportServerWS.GetType().Namespace + '.BooleanOperatorEnum')
	$ItemReferenceNameSpace = ($ReportServerWS.GetType().Namespace + '.ItemReference')
	$ExtensionSettingsNameSpace = ($ReportServerWS.GetType().Namespace + '.ExtensionSettings')
	$DataRetrievalPlanNameSpace = ($ReportServerWS.GetType().Namespace + '.DataRetrievalPlan')
	$ParameterValueOrFieldReferenceNameSpace = ($ReportServerWS.GetType().Namespace + '.ParameterValueOrFieldReference')
	$ParameterValueNameSpace = ($ReportServerWS.GetType().Namespace + '.ParameterValue')
	$ParameterFieldReferenceNameSpace = ($ReportServerWS.GetType().Namespace + '.ParameterFieldReference')
	$DataSourceNameSpace = ($ReportServerWS.GetType().Namespace + '.DataSource')
	$DataSetDefinitionNameSpace = ($ReportServerWS.GetType().Namespace + '.DataSetDefinition')
	$DataSetFieldNameSpace = ($ReportServerWS.GetType().Namespace + '.Field')
	$QueryDefinitionNameSpace = ($ReportServerWS.GetType().Namespace + '.QueryDefinition')
	$ActiveStateNameSpace = ($ReportServerWS.GetType().Namespace + '.ActiveState')

	# Format Source Path
	if ($SourcePath -ne "")
	{
		if ($SourcePath.Substring($SourcePath.Length-1, 1) -ne "\")
		{
			$SourcePath = "$($SourcePath)\"
		}
	}

	# Install SSRS solution / Apply Security / Uninstall SSRS reports
	if (($InstallMode -eq "I") -or ($InstallMode -eq "S"))
	{
		Write-Log "--------------------------------------------------------------------------------"
		Write-Log "Deployment started"
		Write-Log "--------------------------------------------------------------------------------"

		DeployFolders
		DeployDatasources
		DeployItems "Resource"
		DeployItems "DataSet"
		DeployItems "Report"

		Write-Log "--------------------------------------------------------------------------------"
		Write-Log "Deployment ended"
		Write-Log "--------------------------------------------------------------------------------"
	}
	elseif ($InstallMode -eq "U")
	{
		Write-Log "--------------------------------------------------------------------------------"
		Write-Log "Uninstall started"
		Write-Log "--------------------------------------------------------------------------------"

		RemoveItems "Report"
		RemoveItems "DataSet"
		RemoveItems "Resource"
		RemoveItems "DataSource"
		RemoveItems "Folder"

		Write-Log "--------------------------------------------------------------------------------"
		Write-Log "Uninstall ended"
		Write-Log "--------------------------------------------------------------------------------"
	}
}
catch
{
	Write-Log "Error : $($Error[0].exception.GetBaseException().Message)"
	exit 1
}
