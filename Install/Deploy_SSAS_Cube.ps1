#-------------------------------------------------------------------------------------------------#
# Call the PowerShell script and pass the arguments
# .\Deploy_SSAS_Cube.ps1 "C:\WorkArea\Scripts\ServerConfig.xml"
# .\Deploy_SSAS_Cube.ps1 -ConfigPath "C:\WorkArea\Scripts\ServerConfig.xml"
#-------------------------------------------------------------------------------------------------#

#-------------------------------------------------------------------------------------------------#
# Script parameters
#-------------------------------------------------------------------------------------------------#
[CmdletBinding()]
param (
	[Parameter(Mandatory = $true)]
	[string]$ConfigPath = $null
)

#-------------------------------------------------------------------------------------------------#
# Function Write-Log
#-------------------------------------------------------------------------------------------------#
Function Write-Log
{
	Param (
		[string]$logOutput
	)
	$OperationDateTime = $(Get-Date -Format o).Substring(0,24);
	$OperationLog = "$OperationDateTime : $logOutput";
	Write-Output $OperationLog;
	Add-content $logFile -value $OperationLog;
}

#-------------------------------------------------------------------------------------------------#
# Variables
#-------------------------------------------------------------------------------------------------#
$scriptHome = Split-Path $MyInvocation.MyCommand.Path
$scriptName = $MyInvocation.MyCommand.Name
$scriptName = $scriptName.Substring(0, ($scriptName.Length - 4))

$currentLogDate = Get-Date -format yyyyMMddHHmmss
$logFile = "$($scriptHome)\$($scriptName)_$($currentLogDate).log"

#-------------------------------------------------------------------------------------------------#
# Start script
#-------------------------------------------------------------------------------------------------#
Write-Log "Starting $($scriptName) ..."
Write-Log "Log available in $($logFile)"

#-------------------------------------------------------------------------------------------------#
# Initialize Settings from Config file
#-------------------------------------------------------------------------------------------------#
Write-Log "Reading configuration file $($ConfigPath)"
[xml]$ConfigFile = Get-Content "$($ConfigPath)"
$SSAS_INSTANCE = $ConfigFile.Settings.SSASSettings.SSAS_SERVER_NAME
$SSAS_DB_NAME = $ConfigFile.Settings.SSASProject.DatabaseName
$SSAS_XMLA_FILE = $ConfigFile.Settings.SSASProject.XmlaName
$SSAS_DROP_DB = $ConfigFile.Settings.SSASProject.DropBeforeInstall
Write-Log "SSAS_INSTANCE   = $SSAS_INSTANCE"
Write-Log "SSAS_DB_NAME    = $SSAS_DB_NAME"
Write-Log "SSAS_XMLA_FILE  = $SSAS_XMLA_FILE"
Write-Log "SSAS_DROP_DB    = $SSAS_DROP_DB"

# Store the IntegrationServices Assembly namespace to avoid typing it every time
$SSASNamespace = "Microsoft.AnalysisServices"

# Loading IntegrationServices Assembly .........
Write-Log "Loading IntegrationServices Assembly ..."
[System.Reflection.Assembly]::LoadWithPartialName("$($SSASNamespace)")

#-------------------------------------------------------------------------------------------------#
# Connect to the instance
#-------------------------------------------------------------------------------------------------#
try
{
	# Delete database if it exists
	if ( "$SSAS_DROP_DB" -eq "True" )
	{
		$ASServer = New-Object Microsoft.AnalysisServices.Server
		$ASServer.Connect("$($SSAS_INSTANCE)")
		if ( $ASServer.Databases.Contains("$($SSAS_DB_NAME)") )
		{
			Write-Log "Removing database $($SSAS_DB_NAME)"
			$ASServer.Databases.GetByName("$($SSAS_DB_NAME)").Drop()
		}
		$ASServer.Disconnect()
	}

	# Update Database Name in XMLA
	Write-Log "Updating $($SSAS_XMLA_FILE)"
	$CurrentCubeXmla = [xml] (Get-Content "$($scriptHome)\$($SSAS_XMLA_FILE)")

	# XmlNamespaceManager to be able to SelectSingleNode
	$ASNsMgr = New-Object System.Xml.XmlNamespaceManager $CurrentCubeXmla.NameTable
	$ASNsMgr.AddNamespace("as", "http://schemas.microsoft.com/analysisservices/2003/engine")

	$CurrentCubeXmla.Batch.Alter.Object.DatabaseID = $SSAS_DB_NAME
	$CurrentCubeXmla.Batch.Alter.ObjectDefinition.Database.ID = $SSAS_DB_NAME
	$CurrentCubeXmla.Batch.Alter.ObjectDefinition.Database.Name = $SSAS_DB_NAME

	# Update Datasources
	foreach ($CurrentDataSource in $ConfigFile.Settings.SSASProject.DataSources.DataSource)
	{
		$CurrentCubeXmla.Batch.Alter.ObjectDefinition.Database.DataSources.SelectSingleNode("as:DataSource[as:Name='$($CurrentDataSource.Name)']", $ASNsMgr).ConnectionString = $CurrentDataSource.ConnectionString
	}

	$CurrentCubeXmla.Save("$($scriptHome)\$($SSAS_XMLA_FILE)")

	# Deploy cube
	Write-Log "Deploying database $SSAS_DB_NAME : Invoke-ASCmd -InputFile `"$($scriptHome)\$($SSAS_XMLA_FILE)`" -Server `"$($SSAS_INSTANCE)`""
	Invoke-ASCmd -InputFile "$($scriptHome)\$($SSAS_XMLA_FILE)" -Server "$($SSAS_INSTANCE)"

	# Update Passwords
	Write-Log "Update passwords for datasources with ImpersonationMode = ImpersonateAccount ..."
	$ASServer = New-Object Microsoft.AnalysisServices.Server
	$ASServer.Connect("$($SSAS_INSTANCE)")

	# Update Datasources Passwords
	foreach ($CurrentDataSource in $ConfigFile.Settings.SSASProject.DataSources.DataSource)
	{
		if ( "$($CurrentDataSource.ImpersonationInfo.ImpersonationMode)" -eq "ImpersonateAccount" )
		{
			Write-Log "$($CurrentDataSource.Name)"
			$CurrentImpersonationInfo = New-Object Microsoft.AnalysisServices.ImpersonationInfo("ImpersonateAccount", $CurrentDataSource.ImpersonationInfo.Account, $CurrentDataSource.ImpersonationInfo.Password)
			$ASServer.Databases.GetByName("$($SSAS_DB_NAME)").DataSources.GetByName("$($CurrentDataSource.Name)").ImpersonationInfo = $CurrentImpersonationInfo
			$ASServer.Databases.GetByName("$($SSAS_DB_NAME)").DataSources.GetByName("$($CurrentDataSource.Name)").Update()
		}
	}
	
	$ASServer.Disconnect()

	# Update Roles Memberships
	Write-Log "Update Roles Memberships ..."
	foreach ($CurrentRole in $CurrentCube.Roles.Role )
	{
		Write-Log "Adding members to role `"$($CurrentRole.Name)`" :"
		foreach ($CurrentMember in $CurrentRole.Member )
		{
			Write-Log "  - $($CurrentMember.Name)"
			Add-RoleMember -MemberName "$($CurrentMember.Name)" -Server "$($SSAS_INSTANCE)" -Database "$($SSAS_DB_NAME)" -RoleName "$($CurrentRole.Name)"
		}
	}
}
catch
{
	Write-Log "Error : $($Error[0].exception.GetBaseException().Message)"
	exit 1
}
