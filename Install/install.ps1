#-------------------------------------------------------------------------------------------------#
# Call the PowerShell script and pass the arguments
# .\install.ps1 "E:\delivery\Install\installConfig.xml" "E:\delivery\Install\SIDP.zip" -LocalSolution -Databases -IS -AS -RS
# .\install.ps1 -ConfigPath "E:\delivery\Install\installConfig.xml" -PackagePath "E:\delivery\Install\SIDP.zip" -LocalSolution -Databases -IS -AS -RS
#-------------------------------------------------------------------------------------------------#

#-------------------------------------------------------------------------------------------------#
# Script parameters
#-------------------------------------------------------------------------------------------------#
[CmdletBinding()]
param (
	[Parameter(Mandatory = $true)]
	[string]$ConfigPath = $null,
	[Parameter(Mandatory = $true)]
	[string]$PackagePath = $null,
	[switch]$Databases,
	[switch]$IS,
	[switch]$AS,
	[switch]$RS,
	[switch]$SA,
	[Parameter(Mandatory = $false)]
	[string]$IsAppAccount = $null,
	[Parameter(Mandatory = $false)]
	[string]$IsAppPassword = $null,
	[Parameter(Mandatory = $false)]
	[string]$AsAppAccount = $null,
	[Parameter(Mandatory = $false)]
	[string]$AsAppPassword = $null,
	[Parameter(Mandatory = $false)]
	[string]$RsAppAccount = $null,
	[Parameter(Mandatory = $false)]
	[string]$RsAppPassword = $null,
	[Parameter(Mandatory = $false)]
	[string]$SasSqlAccount = $null,
	[Parameter(Mandatory = $false)]
	[string]$SasSqlAccountPwd = $null,
	[Parameter(Mandatory = $true)]
	[boolean]$CheckOnly = $false
)

#-------------------------------------------------------------------------------------------------#
# Function Write-Log
#-------------------------------------------------------------------------------------------------#
Function Write-Log
{
	Param (
		[string]$logOutput
	)
	$OperationDateTime = $(Get-Date -Format o).Substring(0,24);
	$OperationLog = "$OperationDateTime : $logOutput";
	Write-Output $OperationLog;
	Add-content $logFile -value $OperationLog;
}

#-------------------------------------------------------------------------------------------------#
# Variables
#-------------------------------------------------------------------------------------------------#
$scriptHome = Split-Path $MyInvocation.MyCommand.Path
$scriptName = $MyInvocation.MyCommand.Name
$scriptName = $scriptName.Substring(0, ($scriptName.Length - 4))

$currentLogDate = Get-Date -format yyyyMMddHHmmss
$logFile = "$($scriptHome)\$($scriptName)_$($currentLogDate).log"

#-------------------------------------------------------------------------------------------------#
# Start script
#-------------------------------------------------------------------------------------------------#
Write-Log "Starting $($scriptName) ..."
Write-Log "Log available in $($logFile)"

try
{
	#-------------------------------------------------------------------------------------------------#
	# Set $InstallAll
	#-------------------------------------------------------------------------------------------------#
	$InstallAll = (($Databases -eq $false ) -and ($IS -eq $false) -and ($AS -eq $false) -and ($RS -eq $false) -and ($SA -eq $false));

	#-------------------------------------------------------------------------------------------------#
	# Initialize Settings from Config file
	#-------------------------------------------------------------------------------------------------#
	Write-Log "Reading configuration file $($ConfigPath)"
	$ConfigFileStr = Get-Content "$($ConfigPath)"

	if ( "$IsAppAccount" -ne "" ) { $ConfigFileStr = $ConfigFileStr.Replace("##IsAppAccount##", "$IsAppAccount") }
	elseif ( "$env:IsAppAccount" -ne "" ) { $ConfigFileStr = $ConfigFileStr.Replace("##IsAppAccount##", "$env:IsAppAccount") }
	if ( "$IsAppPassword" -ne "" ) { $ConfigFileStr = $ConfigFileStr.Replace("##IsAppPassword##", "$IsAppPassword") }
	elseif ( "$env:IsAppPassword" -ne "" ) { $ConfigFileStr = $ConfigFileStr.Replace("##IsAppPassword##", "$env:IsAppPassword") }

	if ( "$AsAppAccount" -ne "" ) { $ConfigFileStr = $ConfigFileStr.Replace("##AsAppAccount##", "$AsAppAccount") }
	elseif ( "$env:AsAppAccount" -ne "" ) { $ConfigFileStr = $ConfigFileStr.Replace("##AsAppAccount##", "$env:AsAppAccount") }
	if ( "$AsAppPassword" -ne "" ) { $ConfigFileStr = $ConfigFileStr.Replace("##AsAppPassword##", "$AsAppPassword") }
	elseif ( "$env:AsAppPassword" -ne "" ) { $ConfigFileStr = $ConfigFileStr.Replace("##AsAppPassword##", "$env:AsAppPassword") }

	if ( "$RsAppAccount" -ne "" ) { $ConfigFileStr = $ConfigFileStr.Replace("##RsAppAccount##", "$RsAppAccount") }
	elseif ( "$env:RsAppAccount" -ne "" ) { $ConfigFileStr = $ConfigFileStr.Replace("##RsAppAccount##", "$env:RsAppAccount") }
	if ( "$RsAppPassword" -ne "" ) { $ConfigFileStr = $ConfigFileStr.Replace("##RsAppPassword##", "$RsAppPassword") }
	elseif ( "$env:RsAppPassword" -ne "" ) { $ConfigFileStr = $ConfigFileStr.Replace("##RsAppPassword##", "$env:RsAppPassword") }

	if ( "$SasSqlAccount" -ne "" ) { $ConfigFileStr = $ConfigFileStr.Replace("##SasSqlAccount##", "$SasSqlAccount") }
	elseif ( "$env:SasSqlAccount" -ne "" ) { $ConfigFileStr = $ConfigFileStr.Replace("##SasSqlAccount##", "$env:SasSqlAccount") }
	if ( "$SasSqlAccountPwd" -ne "" ) { $ConfigFileStr = $ConfigFileStr.Replace("##SasSqlAccountPwd##", "$SasSqlAccountPwd") }
	elseif ( "$env:SasSqlAccountPwd" -ne "" ) { $ConfigFileStr = $ConfigFileStr.Replace("##SasSqlAccountPwd##", "$env:SasSqlAccountPwd") }

	[xml]$ConfigFile = $ConfigFileStr

	#-------------------------------------------------------------------------------------------------#
	# Unzip package
	#-------------------------------------------------------------------------------------------------#
	$PackageName = Split-Path $PackagePath -Leaf
	$PackageName = $PackageName.Substring(0, ($PackageName.Length - 4));
	$Version = $ConfigFile.Settings.Version.Value;
	$VersionParameterInstance = $($ConfigFile.Settings.SSISSettings.Environments.Environment.EnvironmentVariables.Variable  | where {$_.Name -eq "CNX_DATAFACTORY_ServerName"}).Value;
	$VersionParameterDatabase = $($ConfigFile.Settings.SSISSettings.Environments.Environment.EnvironmentVariables.Variable  | where {$_.Name -eq "CNX_DATAFACTORY_InitialCatalog"}).Value;
	$TargetSolutionDir = $ConfigFile.Settings.LocalSolutionSettings.SolutionFolder.Name;

	$WorkDir = "$scriptHome\work"
	$PackageDir = "$WorkDir\$PackageName"
	$PackageSolutionDir = "$PackageDir\Solution"
	$PackageInstallDir = "$PackageDir\Install"

	Write-Log "Extracting $($PackagePath) to $($PackageDir)"
	Remove-Item $PackageDir -Force -Recurse -ErrorAction SilentlyContinue
	Add-Type -AssemblyName System.IO.Compression.FileSystem | out-null
	[System.IO.Compression.ZipFile]::ExtractToDirectory($PackagePath, $PackageDir)

	#-------------------------------------------------------------------------------------------------#
	# Local Solution
	#-------------------------------------------------------------------------------------------------#
	if (($InstallAll -eq $true) -or ($IS -eq $true))
	{
		# Deploy files
		Write-Log "####################################################################################################"
		Write-Log "Deploying solution to $($TargetSolutionDir)"

		# Loop through subfolders to deploy
		foreach ($CurrentSolutionSubFolder in $ConfigFile.Settings.LocalSolutionSettings.SolutionSubFolders.SolutionSubFolder)
		{
			Write-Log "Copying $($CurrentSolutionSubFolder.Name) to $($TargetSolutionDir)"
			Remove-Item "$($TargetSolutionDir)\$($CurrentSolutionSubFolder.Name)" -Force -Recurse -ErrorAction SilentlyContinue
			Copy-Item "$($PackageSolutionDir)\$($CurrentSolutionSubFolder.Name)" "$($TargetSolutionDir)" -Force -Recurse
		}

		# Loop through configuration files to update
		foreach ($CurrentSolutionCfgFile in $ConfigFile.Settings.LocalSolutionSettings.SolutionConfigFiles.SolutionConfigFile)
		{
			Write-Log "Updating $($CurrentSolutionCfgFile.Name) ($($CurrentSolutionCfgFile.Type))"
			if ( "$($CurrentSolutionCfgFile.Type)" -eq "xml" )
			{
				$CurrentSolutionCfgFileXml = [xml] (Get-Content "$($TargetSolutionDir)\$($CurrentSolutionCfgFile.Name)")
				foreach ($CurrentVariable in $CurrentSolutionCfgFile.Variable)
				{
					Write-Log "  Updating `"$($CurrentVariable.Id)`" --> `"$($CurrentVariable.Value)`""
					$CurrentVariableXml = Select-Xml -Xml $CurrentSolutionCfgFileXml -XPath "$($CurrentVariable.XPath)" | Select-Object -ExpandProperty Node
					if ( "$($CurrentVariable.Type)" -eq "Attribute" )
					{
						$CurrentVariableXml.SetAttribute("$($CurrentVariable.Name)", "$($CurrentVariable.Value)")
					}
				}
				$CurrentSolutionCfgFileXml.Save("$($TargetSolutionDir)\$($CurrentSolutionCfgFile.Name)")
			}
			if ( "$($CurrentSolutionCfgFile.Type)" -eq "bat" )
			{
				$CurrentSolutionCfgFileBat = (Get-Content "$($TargetSolutionDir)\$($CurrentSolutionCfgFile.Name)")
				foreach ($CurrentVariable in $CurrentSolutionCfgFile.Variable)
				{
					Write-Log "  Updating `"$($CurrentVariable.Name)`" --> `"$($CurrentVariable.Value)`""
					$CurrentSolutionCfgFileBat = ( $CurrentSolutionCfgFileBat | ForEach-Object { $_ -replace "^$($CurrentVariable.Name)=.*", "$($CurrentVariable.Name)=$($CurrentVariable.Value)" } )
				}
				$CurrentSolutionCfgFileBat | Set-Content -Path "$($TargetSolutionDir)\$($CurrentSolutionCfgFile.Name)"
			}
		}
	}

	#-------------------------------------------------------------------------------------------------#
	# MSSQLSettings
	#-------------------------------------------------------------------------------------------------#
	if ($Databases -eq $true -or $InstallAll -eq $true)
	{
		# Loop through databases instances to deploy
		foreach ($CurrentInstance in $ConfigFile.Settings.MSSQLSettings.MSSQLSetting)
		{
			if($CurrentInstance.Enabled -eq "True")
			{
				# Get SQL Server Instance
				$TargetInstance = $CurrentInstance.Instance.Name;

				if(-not $CheckOnly){
					$InstanceConnectionString = "Data Source=$($TargetInstance); Database=master; Trusted_Connection=True;";
					$DisablingInstanceQuery = "";
					$EnablingInstanceQuery = "";

					try {
						$AccountsToDisable = @{};
						$InstanceConnection = New-Object System.Data.SqlClient.SqlConnection $InstanceConnectionString;

						$SqlCommand = New-Object System.Data.SqlClient.SqlCommand;
						$SqlCommand.Connection = $InstanceConnection;
						$Query = "SELECT QUOTENAME(sp.name) AS Name, sp.type AS UserType FROM sys.server_principals sp WHERE 1 = 1 AND is_member(sp.name) <> 1 AND sp.principal_id > 100 AND sp.is_disabled = 0 AND sp.name NOT LIKE 'NT%' AND sp.name NOT LIKE '%$%' AND sp.name NOT LIKE '%S_U%' AND sp.name NOT LIKE '%S_S%' AND sp.type NOT IN ('C', 'K', 'R')";
						$SqlCommand.CommandText = $Query;
						$SqlCommand.CommandTimeout = 180;

						$InstanceConnection.Open();
						$reader = $SqlCommand.ExecuteReader();
						$RowCount = 0;

						# Initialze the array that hold the values
						$array = @()
						for ( $i = 0 ; $i -lt $reader.FieldCount; $i++ )
						{
							$array += @($i)
						}
						Write-Log $array.Length

						while ($reader.Read())
						{
							$AccountsToDisable[$RowCount] = @{}

							# get the values
							for ($i = 0; $i -lt $array.Length; $i++)
							{
								$AccountsToDisable[$RowCount]["$($reader.GetName($i))"] = $reader.GetValue($i)
							}

							if($AccountsToDisable[$RowCount]["UserType"] -eq "G"){
								$DisablingInstanceQuery += "REVOKE CONNECT SQL TO $($AccountsToDisable[$RowCount]["Name"]);`r`n";
								$EnablingInstanceQuery += "GRANT CONNECT SQL TO $($AccountsToDisable[$RowCount]["Name"]);`r`n";
							}
							else{
								$DisablingInstanceQuery += "ALTER LOGIN $($AccountsToDisable[$RowCount]["Name"]) DISABLE;`r`n";
								$EnablingInstanceQuery += "ALTER LOGIN $($AccountsToDisable[$RowCount]["Name"]) ENABLE;`r`n";
							}

							$RowCount += 1;
						}
					}
					catch {
						Write-Log "Error : $($Error[0].exception.GetBaseException().Message)";
					}
					finally {
						$reader.Close();
						$InstanceConnection.Close();
					}

					try {
						$InstanceConnection = New-Object System.Data.SqlClient.SqlConnection $InstanceConnectionString;
						$DisablingCommand = New-Object System.Data.SqlClient.SqlCommand
						$DisablingCommand.Connection = $InstanceConnection

						# The disabling query
						Write-Log "$DisablingInstanceQuery";
						$DisablingCommand.CommandText = $DisablingInstanceQuery;

						$InstanceConnection.Open();

						# Run the query
						if(-not($TargetInstance -like "*CR05*")){
							$DisablingCommand.ExecuteNonQuery() | Out-Null
						}

						Write-Log "Accounts And Groups Are Successfully Disabled.";

						$DisablingCommand.Dispose()
					}
					catch {
						Write-Log "Error : $($Error[0].exception.GetBaseException().Message)";
					}
					finally {
						$InstanceConnection.Close();
					}

					$KillInstanceSessionQuery = "";

					try {
						$SessionsToKill = @{};
						$InstanceConnection = New-Object System.Data.SqlClient.SqlConnection $InstanceConnectionString;

						$SqlCommand = New-Object System.Data.SqlClient.SqlCommand;
						$SqlCommand.Connection = $InstanceConnection;
						$Query = "SELECT DISTINCT spid FROM sys.sysprocesses WITH(NOLOCK) WHERE UPPER(nt_username) LIKE 'OB%' OR UPPER(nt_username) LIKE 'A_GB%' OR UPPER(loginame) LIKE 'SAS%' OR UPPER(loginame) LIKE 'S_ADMIN_TAB%'";
						$SqlCommand.CommandText = $Query;
						$SqlCommand.CommandTimeout = 180;

						$InstanceConnection.Open();
						$reader = $SqlCommand.ExecuteReader();
						$SessionsToKillRowCount = 0;

						# Initialze the array that hold the values
						$array = @()
						for ( $i = 0 ; $i -lt $reader.FieldCount; $i++ )
						{
							$array += @($i)
						}
						Write-Log $array.Length

						while ($reader.Read())
						{
							$SessionsToKill[$SessionsToKillRowCount] = @{}

							# get the values
							for ($i = 0; $i -lt $array.Length; $i++)
							{
								$SessionsToKill[$SessionsToKillRowCount]["$($reader.GetName($i))"] = $reader.GetValue($i)
							}

							$KillInstanceSessionQuery += "KILL $($SessionsToKill[$SessionsToKillRowCount]["spid"]);`r`n";

							$SessionsToKillRowCount += 1;
						}
					}
					catch {
						Write-Log "Error : $($Error[0].exception.GetBaseException().Message)";
					}
					finally {
						$reader.Close();
						$InstanceConnection.Close();
					}

					try {
						$InstanceConnection = New-Object System.Data.SqlClient.SqlConnection $InstanceConnectionString;

						# Run the query
						if(-not($TargetInstance -like "*CR*" -and ($SessionsToKillRowCount -gt 0) -and -not([string]::IsNullOrWhiteSpace($KillInstanceSessionQuery)))){
							$KillSessionCommand = New-Object System.Data.SqlClient.SqlCommand
							$KillSessionCommand.Connection = $InstanceConnection

							# The disabling query
							Write-Log "$KillInstanceSessionQuery";
							$KillSessionCommand.CommandText = $KillInstanceSessionQuery;
							$InstanceConnection.Open();

							$KillSessionCommand.ExecuteNonQuery() | Out-Null
							Write-Log "Non-System Sessions Are Successfully Killed.";
							$KillSessionCommand.Dispose()
						}
					}
					catch {
						Write-Log "Error : $($Error[0].exception.GetBaseException().Message)";
					}
					finally {
						$InstanceConnection.Close();
					}
				}

				# Deploy databases
				Write-Log "####################################################################################################"
				Write-Log "Deploying databases to $($TargetInstance)"

				# Loop through databases to deploy
				foreach ($CurrentDatabase in $($CurrentInstance.Databases.Database | Sort-Object -Property Rank))
				{
					if($CurrentDatabase.Enabled -eq "True")
					{
						if(-not $CheckOnly){
							$DatabaseConnectionString = "Data Source=$($TargetInstance); Database=$($CurrentDatabase.Name); Trusted_Connection=True;";
							$DisablingDatabaseQuery = "USE $($CurrentDatabase.Name);`r`n";
							$EnablingDatabaseQuery = "USE $($CurrentDatabase.Name);`r`n";

							try {
								$AccountsToDisable = @{};
								$DatabaseConnection = New-Object System.Data.SqlClient.SqlConnection $DatabaseConnectionString;

								$SqlCommand = New-Object System.Data.SqlClient.SqlCommand;
								$SqlCommand.Connection = $DatabaseConnection;
								$Query = "SELECT QUOTENAME(SP.name) AS Name, SP.type AS UserType FROM sys.database_principals AS DP WITH(NOLOCK) JOIN sys.server_principals AS SP WITH(NOLOCK) ON SP.name = DP.name WHERE 1 = 1 AND is_member(SP.name) <> 1 AND SP.principal_id > 100 AND SP.is_disabled = 0 AND SP.name NOT LIKE 'NT%' AND SP.name NOT LIKE '%$%' AND SP.name NOT LIKE '%S_U%' AND SP.name NOT LIKE '%S_S%' AND SP.type NOT IN ('C', 'K', 'R')";
								$SqlCommand.CommandText = $Query;
								$SqlCommand.CommandTimeout = 180;

								$DatabaseConnection.Open();
								$reader = $SqlCommand.ExecuteReader();
								$RowCount = 0;

								# Initialze the array that hold the values
								$array = @()
								for ( $i = 0 ; $i -lt $reader.FieldCount; $i++ )
								{
									$array += @($i)
								}
								Write-Log $array.Length

								while ($reader.Read())
								{
									$AccountsToDisable[$RowCount] = @{}

									# get the values
									for ($i = 0; $i -lt $array.Length; $i++)
									{
										$AccountsToDisable[$RowCount]["$($reader.GetName($i))"] = $reader.GetValue($i)
									}

									if($AccountsToDisable[$RowCount]["UserType"] -eq "G"){
										$DisablingDatabaseQuery += "REVOKE CONNECT ON DATABASE::$($CurrentDatabase.Name) TO $($AccountsToDisable[$RowCount]["Name"]);`r`n";
										$EnablingDatabaseQuery += "GRANT CONNECT ON DATABASE::$($CurrentDatabase.Name) TO $($AccountsToDisable[$RowCount]["Name"]);`r`n";
									}
									else{
										$DisablingDatabaseQuery += "ALTER LOGIN $($AccountsToDisable[$RowCount]["Name"]) DISABLE;`r`n";
										$EnablingDatabaseQuery += "ALTER LOGIN $($AccountsToDisable[$RowCount]["Name"]) ENABLE;`r`n";
									}

									$RowCount += 1;
								}
							}
							catch {
								Write-Log "Error : $($Error[0].exception.GetBaseException().Message)";
							}
							finally {
								$reader.Close();
								$DatabaseConnection.Close();
							}

							try {
								$DatabaseConnection = New-Object System.Data.SqlClient.SqlConnection $DatabaseConnectionString;
								$DisablingCommand = New-Object System.Data.SqlClient.SqlCommand
								$DisablingCommand.Connection = $DatabaseConnection

								# The disabling query
								Write-Log "$($DisablingDatabaseQuery)";
								$DisablingCommand.CommandText = $DisablingDatabaseQuery

								$DatabaseConnection.Open();

								# Run the query
								$DisablingCommand.ExecuteNonQuery() | Out-Null;

								Write-Log "Accounts And Groups Are Successfully Disabled.";

								$DisablingCommand.Dispose()
							}
							catch {
								Write-Log "Error : $($Error[0].exception.GetBaseException().Message)";
							}
							finally {
								$DatabaseConnection.Close();
							}

							$KillDatabaseSessionQuery = "USE $($CurrentDatabase.Name);`r`n";

							try {
								$SessionsToKill = @{};
								$DatabaseConnection = New-Object System.Data.SqlClient.SqlConnection $DatabaseConnectionString;

								$SqlCommand = New-Object System.Data.SqlClient.SqlCommand;
								$SqlCommand.Connection = $DatabaseConnection;
								$Query = "SELECT DISTINCT spid FROM sys.sysprocesses AS SP WITH(NOLOCK) JOIN sys.databases AS D WITH(NOLOCK) ON D.database_id = SP.dbid WHERE D.name = '$($CurrentDatabase.Name)' AND (UPPER(nt_username) LIKE 'OB%' OR UPPER(nt_username) LIKE 'A_GB%' OR UPPER(loginame) LIKE 'SAS%' OR UPPER(loginame) LIKE 'S_ADMIN_TAB%')";
								$SqlCommand.CommandText = $Query;
								$SqlCommand.CommandTimeout = 180;

								$DatabaseConnection.Open();
								$reader = $SqlCommand.ExecuteReader();
								$SessionsToKillRowCount = 0;

								# Initialze the array that hold the values
								$array = @()
								for ( $i = 0 ; $i -lt $reader.FieldCount; $i++ )
								{
									$array += @($i)
								}
								Write-Log $array.Length

								while ($reader.Read())
								{
									$SessionsToKill[$SessionsToKillRowCount] = @{}

									# get the values
									for ($i = 0; $i -lt $array.Length; $i++)
									{
										$SessionsToKill[$SessionsToKillRowCount]["$($reader.GetName($i))"] = $reader.GetValue($i)
									}

									$KillDatabaseSessionQuery += "KILL $($SessionsToKill[$SessionsToKillRowCount]["spid"]);`r`n";

									$SessionsToKillRowCount += 1;
								}
							}
							catch {
								Write-Log "Error : $($Error[0].exception.GetBaseException().Message)";
							}
							finally {
								$reader.Close();
								$DatabaseConnection.Close();
							}

							try {
								$DatabaseConnection = New-Object System.Data.SqlClient.SqlConnection $DatabaseConnectionString;

								# Run the query
								if (($SessionsToKillRowCount -gt 0) -and -not([string]::IsNullOrWhiteSpace($KillDatabaseSessionQuery))) {
									$KillSessionCommand = New-Object System.Data.SqlClient.SqlCommand
									$KillSessionCommand.Connection = $DatabaseConnection

									# The disabling query
									Write-Log "$($KillDatabaseSessionQuery)";
									$KillSessionCommand.CommandText = $KillDatabaseSessionQuery;
									$DatabaseConnection.Open();

									$KillSessionCommand.ExecuteNonQuery() | Out-Null
									Write-Log "Non-System Sessions Are Successfully Killed.";
									$KillSessionCommand.Dispose()
								}
							}
							catch {
								Write-Log "Error : $($Error[0].exception.GetBaseException().Message)";
							}
							finally {
								$DatabaseConnection.Close();
							}
						}

						Write-Log "----------------------------------------------------------------------------------------------------"
						Write-Log "Deploying $($CurrentDatabase.Name) on $($TargetInstance)"
						$CurrentDatabaseDacpac = "$PackageInstallDir\SQL\$($CurrentDatabase.SourceProject.Name).dacpac"
						$CurrentDeploymentProfile = "$PackageInstallDir\SQL\$($CurrentDatabase.SourceProject.PublishProfile)"

						Write-Log "  - Dacpac file  : $CurrentDatabaseDacpac"
						Write-Log "  - Profile file : $CurrentDeploymentProfile"

						# Update XML config file for Deploy_SQL_Database.ps1
						$DeployDbConfigPath = "$($PackageInstallDir)\SQL\Deploy_SQL_Database.config"
						$DeployDbConfigUpdPath = "$($PackageInstallDir)\SQL\Deploy_SQL_Database.$($CurrentDatabase.Name).config"

						Write-Log "Generating $($DeployDbConfigUpdPath)"
						$DeployDbConfig = [xml] (Get-Content "$($DeployDbConfigPath)")
						$DeployDbConfig.Settings.SQLSettings.SetAttribute("MSQL_SERVER_NAME", "$($TargetInstance)")
						$DeployDbConfig.Settings.SQLProject.SetAttribute("DatabaseName", "$($CurrentDatabase.Name)")
						$DeployDbConfig.Settings.SQLProject.SetAttribute("DacpacName", "$($CurrentDatabase.SourceProject.Name).dacpac")
						$DeployDbConfig.Settings.SQLProject.SetAttribute("ProfileName", "$($CurrentDatabase.SourceProject.PublishProfile)")

						foreach ($CurrentVariable in $DeployDbConfig.Settings.SQLProject.SourceProjectVariables.Variable)
						{
							$DeployDbConfig.Settings.SQLProject.SourceProjectVariables.RemoveChild($CurrentVariable) | Out-Null
						}

						foreach ($ProjectVariable in $CurrentDatabase.SourceProject.SourceProjectVariables.Variable)
						{
							$NewProjectVariable = $DeployDbConfig.CreateElement("Variable")
							$NewProjectVariable.SetAttribute("Name", "$($ProjectVariable.Name)")
							$NewProjectVariable.SetAttribute("Value", "$($ProjectVariable.Value)")
							$DeployDbConfig.Settings.SQLProject.SelectSingleNode("SourceProjectVariables").AppendChild($NewProjectVariable) | Out-Null
						}

						$DeployDbConfig.Save("$($DeployDbConfigUpdPath)")

						# Run Deploy_SQL_Database.ps1
						Write-Log "Executing $PackageInstallDir\SQL\Deploy_SQL_Database.ps1 -ConfigPath `"$($DeployDbConfigUpdPath)`" -CheckOnly $CheckOnly";
						& $PackageInstallDir\SQL\Deploy_SQL_Database.ps1 -ConfigPath "$($DeployDbConfigUpdPath)" -CheckOnly $CheckOnly *>&1 | Add-Content -path $logFile -encoding ASCII -passthru;

						if ( $? -ne $true )
						{
							Write-Error "Error while executing $PackageInstallDir\SQL\Deploy_SQL_Database.ps1 `"$($DeployDbConfigUpdPath)`"";
						}

						if(-not $CheckOnly){
							try {
								$DatabaseConnection = New-Object System.Data.SqlClient.SqlConnection $DatabaseConnectionString;
								$EnablingDatabaseCommand = New-Object System.Data.SqlClient.SqlCommand
								$EnablingDatabaseCommand.Connection = $DatabaseConnection

								# The enabling query
								Write-Log "$($EnablingDatabaseQuery)";
								$EnablingDatabaseCommand.CommandText = $EnablingDatabaseQuery;

								$DatabaseConnection.Open();

								# Run the query
								$EnablingDatabaseCommand.ExecuteNonQuery() | Out-Null

								Write-Log "Accounts And Groups Are Are Successfully Enabled.";

								$EnablingDatabaseCommand.Dispose()
							}
							catch {
								Write-Log "Error : $($Error[0].exception.GetBaseException().Message)";
							}
							finally {
								$DatabaseConnection.Close();
							}
						}
					}
				}

				if(-not $CheckOnly){
					try {
						$InstanceConnection = New-Object System.Data.SqlClient.SqlConnection $InstanceConnectionString;
						$EnablingInstanceCommand = New-Object System.Data.SqlClient.SqlCommand
						$EnablingInstanceCommand.Connection = $InstanceConnection

						# The enabling query
						Write-Log "$EnablingInstanceQuery";
						$EnablingInstanceCommand.CommandText = $EnablingInstanceQuery;

						$InstanceConnection.Open();

						# Run the query
						if(-not($TargetInstance -like "*CR*")){
							$EnablingInstanceCommand.ExecuteNonQuery() | Out-Null
						}

						Write-Log "Accounts And Groups Are Are Successfully Enabled.";

						$EnablingInstanceCommand.Dispose()
					}
					catch {
						Write-Log "Error : $($Error[0].exception.GetBaseException().Message)";
					}
					finally {
						$InstanceConnection.Close();
					}
				}
			}
		}

		if(-not $CheckOnly){
			try {
				#-------------------------------------------------------------------------------------------------#
				# Update SIDP DB Deployed Version
				#-------------------------------------------------------------------------------------------------#
				Write-Log "Updating SIDP DB Deployed Version..."
				Invoke-Sqlcmd -ServerInstance "$VersionParameterInstance" -Database "$VersionParameterDatabase" -Query "Exec dbo.UpdateSIDPVersion @Application = 'DB', @Version = '$($Version)'" -verbose *>&1 | Add-Content -path $logFile -encoding ASCII -passthru
			}
			catch {
				Write-Error "Error : $($Error[0].exception.GetBaseException().Message)";
			}
		}
	}

	#-------------------------------------------------------------------------------------------------#
	# SQL Agent Settings
	#-------------------------------------------------------------------------------------------------#
	if (($SA -eq $true) -or ($InstallAll -eq $true))
	{
		Write-Log "Loading Microsoft.SQLServer.SMO Assembly ...";
		[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SQLServer.SMO");

		foreach($SASetting in $ConfigFile.Settings.SASettings.SASetting){
			$TargetInstance = $SASetting.Instance.Name;

			# Check SQL Agant Jobs

			# Loop through Jobs to schedule
			foreach ($Job in $SASetting.Jobs.Job)
			{
				$JobName = $Job.AlphabeticalName;
				$JobOwner = $Job.owner;
				$Drop = [System.Convert]::ToBoolean($Job.dropIfExists);
				Write-Log "----------------------------------------------------------------------------------------------------"
				Write-Log "Scheduling $JobName on $TargetInstance"

				$SqlServer = New-Object -TypeName  Microsoft.SQLServer.Management.Smo.Server($TargetInstance)
				$SqlServerJobs = $SqlServer.JobServer.Jobs

				$SQLJob = $SqlServerJobs[$JobName]

				if ($SQLJob -and !($Drop))
				{
					 Write-Log "Job $JobName already exists."
				}
				elseif ($SQLJob -and $Drop)
				{
					Write-Log "Dropping Job $JobName started...";
					$SQLJob.Drop();
					Write-Log "Dropping Job $JobName finished.";
				}

				if (!$SQLJob -or $Drop)
				{
					#Creating Job on SQL Agent
					Write-Log "Creating Job $JobName on $TargetInstance..."

					$SQLJob = New-Object -TypeName Microsoft.SqlServer.Management.SMO.Agent.Job -argumentlist $SqlServer.JobServer, $JobName;
					$SQLJob.OwnerLoginName = $JobOwner;
					$SQLJob.Category = "Data Collector";
					$SQLJob.Description = $Job.Description;
					$SQLJob.Create();
					$SQLJob.ApplyToTargetServer($TargetInstance);
					$SQLJob.Alter();

					Write-Log "Creating Job $JobName on $TargetInstance Finished.";

					$JobStepsCount = $Job.JobStep.Count;
					$JobStepCount = 0;

					foreach ($JobStep in $Job.JobStep | Sort-Object -Property AllJobStepId)
					{
						$JobStepName = $JobStep.AlphabeticalName;
						Write-Log "Creating Step $JobStepName for Job $JobName on $TargetInstance ($($JobStepCount + 1) / $JobStepsCount)...";

						$SQLJobStep = New-Object -TypeName Microsoft.SqlServer.Management.SMO.Agent.JobStep -argumentlist $SQLJob, $JobStepName
						$SQLJobStep.SubSystem = $JobStep.subSystem;
						$SQLJobStep.JobStepFlags = $JobStep.jobStepFlags;
						$SQLJobStep.ProxyName = $JobStep.proxyName;
						#$SQLJobStep.Description = $JobStep.Description;
						$SQLJobStep.OnFailAction = [Microsoft.SqlServer.Management.Smo.Agent.StepCompletionAction]::QuitWithFailure;
						if ($($JobStepCount + 1) -lt $JobStepsCount){
							$SQLJobStep.OnSuccessAction = [Microsoft.SqlServer.Management.Smo.Agent.StepCompletionAction]::GoToNextStep;
						}
						else{
							$SQLJobStep.OnSuccessAction = [Microsoft.SqlServer.Management.Smo.Agent.StepCompletionAction]::QuitWithSuccess;
						}

						$JobStepCommand = "";
						foreach($JobStepCommandLine in $JobStep.command.Split(";")){
							$JobStepCommand += "`r`n$JobStepCommandLine";
						}

						$SQLJobStep.command = $JobStepCommand;

						$SQLJobStep.Create();
						$SQLJob.Alter();
						$JobStepCount += 1;
					}

					$ScheduledJob = [System.Convert]::ToBoolean($Job.ScheduledJob);
					if ($ScheduledJob)
					{
						Write-Log "Creating Schedule for Job $JobName on $TargetInstance...";
						$JobStartDate = Get-Date

						$JobStartHour = $Job.scheduleStartHour
						$JobStartMinute = $Job.scheduleStartMinute
						$JobStartTime = New-TimeSpan -Hour $JobStartHour -Minute $JobStartMinute;

						$JobFrequencyType = $Job.FrequencyType;
						$JobFrequencyTypeInterval = $Job.FrequencyTypeInterval;
						$JobFrequencyRecurrenceFactor = $Job.FrequencyRecurrenceFactor;

						$SQLJobSchedule = New-Object -TypeName Microsoft.SqlServer.Management.Smo.Agent.JobSchedule -ArgumentList $SQLJob, "$JobFrequencyType Schedule";
						$SQLJobSchedule.IsEnabled = [System.Convert]::ToBoolean($ScheduledJob);

						$SQLJobSchedule.FrequencyTypes = [Microsoft.SqlServer.Management.SMO.Agent.FrequencyTypes]::$JobFrequencyType;
						$SQLJobSchedule.FrequencyInterval = $JobFrequencyTypeInterval;
						$SQLJobSchedule.FrequencyRecurrenceFactor = $JobFrequencyRecurrenceFactor;

						$SQLJobSchedule.ActiveStartDate = $JobStartDate
						$SQLJobSchedule.ActiveStartTimeofDay = $JobStartTime
						$SQLJobSchedule.Create()
						$SQLJob.Alter()

						Write-Log "Creating Schedule for Job $JobName on $TargetInstance Finished.";
					}

					$SQLJob.IsEnabled = [System.Convert]::ToBoolean($Job.Enabled);
					$SQLJob.Alter();
				}
			}
		}
	}

	#-------------------------------------------------------------------------------------------------#
	# SSASSettings
	#-------------------------------------------------------------------------------------------------#
	if ($AS -eq $true)
	{
		$TargetInstance = $ConfigFile.Settings.SSASSettings.Instance.Name

		# Deploy databases
		Write-Log "####################################################################################################"
		Write-Log "Deploying cubes to $($TargetInstance)"

		# Loop through databases to deploy
		foreach ($CurrentCube in $ConfigFile.Settings.SSASSettings.Databases.Database)
		{
			Write-Log "----------------------------------------------------------------------------------------------------"
			Write-Log "Deploying $($CurrentCube.Name) on $($TargetInstance)"
			$CurrentCubeXmlaPath = "$PackageInstallDir\SSAS\$($CurrentCube.SourceProject.Name).xmla"

			Write-Log "  - XMLA file  : $CurrentCubeXmlaPath"

			# Update XML config file for Deploy_SSAS_Cube.ps1
			$DeployAsConfigPath = "$($PackageInstallDir)\SSAS\Deploy_SSAS_Cube.config"
			$DeployAsConfigUpdPath = "$($PackageInstallDir)\SSAS\Deploy_SSAS_Cube.$($CurrentCube.Name).config"

			Write-Log "Generating $($DeployAsConfigUpdPath)"
			$DeployAsConfig = [xml] (Get-Content "$($DeployAsConfigPath)")
			$DeployAsConfig.Settings.SSASSettings.SetAttribute("SSAS_SERVER_NAME", "$($TargetInstance)")
			$DeployAsConfig.Settings.SSASProject.SetAttribute("DatabaseName", "$($CurrentCube.Name)")
			$DeployAsConfig.Settings.SSASProject.SetAttribute("XmlaName", "$($CurrentCube.SourceProject.Name).xmla")
			$DeployAsConfig.Settings.SSASProject.SetAttribute("DropBeforeInstall", "$($CurrentCube.DropBeforeInstall)")

			foreach ($CurrentDataSource in $DeployAsConfig.Settings.SSASProject.DataSources.DataSource)
			{
				$DeployAsConfig.Settings.SSASProject.DataSources.RemoveChild($CurrentDataSource) | Out-Null
			}

			foreach ($CurrentDataSource in $CurrentCube.SourceProject.DataSources.DataSource )
			{
				$NewDataSource = $DeployAsConfig.CreateElement("DataSource")
				$NewDataSource.SetAttribute("Name", "$($CurrentDataSource.Name)")
				$NewDataSource.SetAttribute("ConnectionString", "$($CurrentDataSource.ConnectionString)")

				$NewImpersonationInfo = $DeployAsConfig.CreateElement("ImpersonationInfo")
				$NewImpersonationInfo.SetAttribute("ImpersonationMode", "$($CurrentDataSource.ImpersonationInfo.ImpersonationMode)")

				if ( "$($CurrentDataSource.ImpersonationInfo.ImpersonationMode)" -eq "ImpersonateAccount" )
				{
					$NewImpersonationInfo.SetAttribute("Account", "$($CurrentDataSource.ImpersonationInfo.Account)")
					$NewImpersonationInfo.SetAttribute("Password", "$($CurrentDataSource.ImpersonationInfo.Password)")
				}

				$NewDataSource.AppendChild($NewImpersonationInfo) | Out-Null
				$DeployAsConfig.Settings.SSASProject.SelectSingleNode("DataSources").AppendChild($NewDataSource) | Out-Null
			}

			foreach ($CurrentRole in $CurrentCube.Roles.Role )
			{
				$NewRole = $DeployAsConfig.CreateElement("Role")
				$NewRole.SetAttribute("Name", "$($CurrentRole.Name)")

				foreach ($CurrentMember in $CurrentRole.Member )
				{
					$NewMember = $DeployAsConfig.CreateElement("Member")
					$NewMember.SetAttribute("Name", "$($CurrentMember.Name)")
					$NewRole.AppendChild($NewMember)
				}

				$DeployAsConfig.Settings.SSASProject.SelectSingleNode("Roles").AppendChild($NewRole) | Out-Null
			}

			$DeployAsConfig.Save("$($DeployAsConfigUpdPath)")

			# Run Deploy_SQL_Database.ps1
			Write-Log "Executing $PackageInstallDir\SSAS\Deploy_SSAS_Cube.ps1 `"$($DeployAsConfigUpdPath)`""
			& $PackageInstallDir\SSAS\Deploy_SSAS_Cube.ps1 "$($DeployAsConfigUpdPath)" *>&1 | Add-Content -path $logFile -encoding ASCII -passthru
			if ( $? -ne $true )
			{
				Write-Log "Error while executing $PackageInstallDir\SSAS\Deploy_SSAS_Cube.ps1 `"$($DeployAsConfigUpdPath)`""
				Exit 1
			}
		}

		if(-not $CheckOnly){
			try {
				#-------------------------------------------------------------------------------------------------#
				# Update SIDP SSAS Deployed Version
				#-------------------------------------------------------------------------------------------------#
				Write-Log "Updating SIDP SSAS Deployed Version..."
				Invoke-Sqlcmd -ServerInstance "$VersionParameterInstance" -Database "$VersionParameterDatabase" -Query "Exec dbo.UpdateSIDPVersion @Application = 'SSAS', @Version = '$($Version)'" -verbose *>&1 | Add-Content -path $logFile -encoding ASCII -passthru
			}
			catch {
				Write-Log "Error : $($Error[0].exception.GetBaseException().Message)";
			}
		}
	}

	#-------------------------------------------------------------------------------------------------#
	# SSISSettings
	#-------------------------------------------------------------------------------------------------#
	if ($IS -eq $true -or $InstallAll -eq $true)
	{
		$TargetInstance = $ConfigFile.Settings.SSISSettings.Catalog.InstanceName;
		$TargetCatalog = $ConfigFile.Settings.SSISSettings.Catalog.Name;

		# Deploy SSIS
		Write-Log "####################################################################################################"
		Write-Log "Deploying SSIS projects to $($TargetCatalog) on $($TargetInstance)"

		# Create catalog if it does not exist
		Write-Log "----------------------------------------------------------------------------------------------------"
		Write-Log "Creating catalog $($TargetCatalog) on $($TargetInstance)"

		# Update XML config file for Deploy_SSIS_Catalog.ps1
		$CreateSSISDBConfigPath = "$($PackageInstallDir)\SSIS\Deploy_SSIS_Catalog.config"
		$CreateSSISDBConfigUpdPath = "$($PackageInstallDir)\SSIS\Deploy_SSIS_Catalog.$($TargetInstance.replace('\', '_')).config"

		Write-Log "Generating $($CreateSSISDBConfigUpdPath)"
		$CreateSSISDBConfig = [xml] (Get-Content "$($CreateSSISDBConfigPath)")
		$CreateSSISDBConfig.Settings.SSISSettings.SetAttribute("MSQL_SERVER_NAME", "$($TargetInstance)")
		$CreateSSISDBConfig.Save("$($CreateSSISDBConfigUpdPath)")

		# Run Deploy_SSIS_Catalog.ps1
		Write-Log "Executing $PackageInstallDir\SSIS\Deploy_SSIS_Catalog.ps1 `"$($CreateSSISDBConfigUpdPath)`""
		& $PackageInstallDir\SSIS\Deploy_SSIS_Catalog.ps1 "$($CreateSSISDBConfigUpdPath)" *>&1 | Add-Content -path $logFile -encoding ASCII -passthru
		if ($? -ne $true)
		{
			Write-Log "Error while executing $PackageInstallDir\SSIS\Deploy_SSIS_Catalog.ps1 `"$($CreateSSISDBConfigUpdPath)`""
			Exit 1
		}

		# Deploy custom procedures in SSIS catalog
		Write-Log "Deploying custom scripts in SSIS catalog $($TargetCatalog) on $($TargetInstance)"

		foreach ( $CustomScript in $ConfigFile.Settings.SSISSettings.Catalog.CustomScripts.CustomScript )
		{
			$CustomScriptVariables = New-Object ('System.String[]')0
			foreach ( $CustomScriptVar in $CustomScript.SelectNodes("Variable") )
			{
				$CustomScriptVariables += "$($CustomScriptVar.Name) = $($CustomScriptVar.Value)"
			}
			Write-Log "Executing $($PackageInstallDir)\SSIS\$($CustomScript.Name)"

			if(-not $CheckOnly){
				Invoke-Sqlcmd -inputfile "$($PackageInstallDir)\SSIS\$($CustomScript.Name)" -serverinstance "$($TargetInstance)" -database "$($TargetCatalog)" -Variable $CustomScriptVariables -verbose *>&1 | Add-Content -path $logFile -encoding ASCII -passthru;
			}
		}

		# Create environments
		Write-Log "----------------------------------------------------------------------------------------------------"
		Write-Log "Creating environments in SSIS catalog"

		# Loop through environments to create
		foreach ($CurrentEnvironment in $ConfigFile.Settings.SSISSettings.Environments.Environment)
		{
			Write-Log "Creating $($CurrentEnvironment.Name) in SSIS catalog"

			# Update XML config file for Deploy_SSIS_Environment.ps1
			$CreateSSISEnvConfigPath = "$($PackageInstallDir)\SSIS\Deploy_SSIS_Environment.config"
			$CreateSSISEnvConfigUpdPath = "$($PackageInstallDir)\SSIS\Deploy_SSIS_Environment.$($CurrentEnvironment.Name).config"

			Write-Log "Generating $($CreateSSISEnvConfigUpdPath)"
			$CreateSSISEnvConfig = [xml] (Get-Content "$($CreateSSISEnvConfigPath)")
			$CreateSSISEnvConfig.Settings.SSISSettings.SetAttribute("MSQL_SERVER_NAME", "$($TargetInstance)")
			$CreateSSISEnvConfig.Settings.SSISFolder.SetAttribute("FolderName", "$($CurrentEnvironment.Folder)")

			$EnvNode = $CreateSSISEnvConfig.Settings.SSISEnvironments.Environment | where {$_.Name -eq "Template"}
			$EnvNode.SetAttribute("Name", "$($CurrentEnvironment.Name)")

			foreach ($CurrentVariable in $CurrentEnvironment.EnvironmentVariables.Variable)
			{
				$EnvVarNode = $EnvNode.Variable | where {$_.Name -eq $CurrentVariable.Name}
				$EnvVarNode.SetAttribute("Value", "$($CurrentVariable.Value)")
			}

			$CreateSSISEnvConfig.Save("$($CreateSSISEnvConfigUpdPath)")

			# Run Deploy_SSIS_Environment.ps1
			Write-Log "Executing $PackageInstallDir\SSIS\Deploy_SSIS_Environment.ps1 `"$($CreateSSISEnvConfigUpdPath)`""
			& $PackageInstallDir\SSIS\Deploy_SSIS_Environment.ps1 "$($CreateSSISEnvConfigUpdPath)" *>&1 | Add-Content -path $logFile -encoding ASCII -passthru
			if ( $? -ne $true )
			{
				Write-Log "Error while executing $PackageInstallDir\SSIS\Deploy_SSIS_Environment.ps1 `"$($CreateSSISEnvConfigUpdPath)`""
				Exit 1
			}
		}

		# Deploy projects
		Write-Log "----------------------------------------------------------------------------------------------------"
		Write-Log "Deploying projects in SSIS catalog"

		# Loop through projects to deploy
		foreach ($CurrentProject in $ConfigFile.Settings.SSISSettings.Projects.Project)
		{
			Write-Log "Deploying $($CurrentProject.Name) in SSIS catalog"

			# Update XML config file for Deploy_SSIS_Project.ps1
			$CreateSSISProjConfigPath = "$($PackageInstallDir)\SSIS\Deploy_SSIS_Project.config"
			$CreateSSISProjConfigUpdPath = "$($PackageInstallDir)\SSIS\Deploy_SSIS_Project.$($CurrentProject.Name).config"

			Write-Log "Generating $($CreateSSISProjConfigUpdPath)"
			$CreateSSISProjConfig = [xml] (Get-Content "$($CreateSSISProjConfigPath)")
			$CreateSSISProjConfig.Settings.SSISSettings.SetAttribute("MSQL_SERVER_NAME", "$($TargetInstance)")
			$CreateSSISProjConfig.Settings.SSISProject.SetAttribute("FolderName", "$($CurrentProject.Folder)")
			$CreateSSISProjConfig.Settings.SSISProject.SetAttribute("ProjectName", "$($CurrentProject.Name)")
			$CreateSSISProjConfig.Settings.SSISProject.SetAttribute("IspacName", "$($CurrentProject.SourceProject.Name).ispac")

			foreach ($CurrentAssociatedEnvironment in $CreateSSISProjConfig.Settings.AssociatedEnvironments.Environment)
			{
				$CreateSSISProjConfig.Settings.AssociatedEnvironments.RemoveChild($CurrentAssociatedEnvironment) | Out-Null
			}

			foreach ($AssociatedEnvironment in $CurrentProject.AssociatedEnvironments.Environment)
			{
				$NewAssociatedEnvironment = $CreateSSISProjConfig.CreateElement("Environment")
				$NewAssociatedEnvironment.SetAttribute("Name", "$($AssociatedEnvironment.Name)")
				$CreateSSISProjConfig.Settings.SelectSingleNode("AssociatedEnvironments").AppendChild($NewAssociatedEnvironment) | Out-Null
			}

			$CreateSSISProjConfig.Save("$($CreateSSISProjConfigUpdPath)")

			# Run Deploy_SSIS_Project.ps1
			Write-Log "Executing $PackageInstallDir\SSIS\Deploy_SSIS_Project.ps1 `"$($CreateSSISProjConfigUpdPath)`""
			& $PackageInstallDir\SSIS\Deploy_SSIS_Project.ps1 "$($CreateSSISProjConfigUpdPath)" *>&1 | Add-Content -path $logFile -encoding ASCII -passthru
			if ( $? -ne $true )
			{
				Write-Log "Error while executing $PackageInstallDir\SSIS\Deploy_SSIS_Project.ps1 `"$($CreateSSISProjConfigUpdPath)`""
				Exit 1
			}
		}

		if(-not $CheckOnly){
			try {
				#-------------------------------------------------------------------------------------------------#
				# Update SIDP SSIS Deployed Version
				#-------------------------------------------------------------------------------------------------#
				Write-Log "Updating SIDP SSIS Deployed Version..."
				Invoke-Sqlcmd -ServerInstance "$VersionParameterInstance" -Database "$VersionParameterDatabase" -Query "Exec dbo.UpdateSIDPVersion @Application = 'SSIS', @Version = '$($Version)'" -verbose *>&1 | Add-Content -path $logFile -encoding ASCII -passthru
			}
			catch {
				Write-Log "Error : $($Error[0].exception.GetBaseException().Message)";
			}
		}

		try {
			#-------------------------------------------------------------------------------------------------#
			# Update json Config File of SIDP_EXTRACT
			#-------------------------------------------------------------------------------------------------#
			$SIDPExtractToolsConfigurationDirectory = "$($TargetSolutionDir)\Tools\ExtractData\Config\";
			Write-Log $SIDPExtractToolsConfigurationDirectory;
			$EnvironementName = $ConfigFile.Settings.SSISSettings.Environments.Environment.Name;
			Write-Log $EnvironementName;
			Get-ChildItem -Path "$SIDPExtractToolsConfigurationDirectory" | %{If(!($_.Name -like "*.$EnvironementName.*")){Write-Log "Removing $($_.FullName)"; Remove-Item -Path $_.FullName;}};
			Get-ChildItem -Path "$SIDPExtractToolsConfigurationDirectory" | %{Write-Log "Renaming $($_.FullName) to $($_.FullName.Replace(".$EnvironementName.json",".json"))"; Rename-Item $_.FullName $_.FullName.Replace(".$EnvironementName.json",".json");};
		}
		catch {
			Write-Log "Error : $($Error[0].exception.GetBaseException().Message)";
		}

		try {
			#-------------------------------------------------------------------------------------------------#
			# Update json Config File of SalesForce Objects
			#-------------------------------------------------------------------------------------------------#
			$SalesForceObjectsConfigurationDirectory = "$($TargetSolutionDir)\Tools\SalesForceObjects\Config\";
			Write-Log $SalesForceObjectsConfigurationDirectory;
			$EnvironementName = $ConfigFile.Settings.SSISSettings.Environments.Environment.Name;
			Write-Log $EnvironementName;
			Get-ChildItem -Path "$SalesForceObjectsConfigurationDirectory" | %{If(!($_.Name -like "*.$EnvironementName.*")){Write-Log "Removing $($_.FullName)"; Remove-Item -Path $_.FullName;}};
			Get-ChildItem -Path "$SalesForceObjectsConfigurationDirectory" | %{Write-Log "Renaming $($_.FullName) to $($_.FullName.Replace(".$EnvironementName.json",".json"))"; Rename-Item $_.FullName $_.FullName.Replace(".$EnvironementName.json",".json");};
		}
		catch {
			Write-Log "Error : $($Error[0].exception.GetBaseException().Message)";
		}

		try {
			#-------------------------------------------------------------------------------------------------#
			# Update json Config File of FrontEnd Objects
			#-------------------------------------------------------------------------------------------------#
			$FrontEndObjectsConfigurationDirectory = "$($TargetSolutionDir)\Tools\FrontEndObjects\Config\";
			Write-Log $FrontEndObjectsConfigurationDirectory;
			$EnvironementName = $ConfigFile.Settings.SSISSettings.Environments.Environment.Name;
			Write-Log $EnvironementName;
			Get-ChildItem -Path "$FrontEndObjectsConfigurationDirectory" | %{If(!($_.Name -like "*.$EnvironementName.*")){Write-Log "Removing $($_.FullName)"; Remove-Item -Path $_.FullName;}};
			Get-ChildItem -Path "$FrontEndObjectsConfigurationDirectory" | %{Write-Log "Renaming $($_.FullName) to $($_.FullName.Replace(".$EnvironementName.json",".json"))"; Rename-Item $_.FullName $_.FullName.Replace(".$EnvironementName.json",".json");};
		}
		catch {
			Write-Log "Error : $($Error[0].exception.GetBaseException().Message)";
		}

		try {
			#-------------------------------------------------------------------------------------------------#
			# Update json Config File of Tableau Refresh Extracts
			#-------------------------------------------------------------------------------------------------#
			$TableauRefreshExtractsConfigurationDirectory = "$($TargetSolutionDir)\Tools\TableauServer\Config\";
			Write-Log $TableauRefreshExtractsConfigurationDirectory;
			$EnvironementName = $ConfigFile.Settings.SSISSettings.Environments.Environment.Name;
			Write-Log $EnvironementName;
			Get-ChildItem -Path "$TableauRefreshExtractsConfigurationDirectory" | %{If(!($_.Name -like "*.$EnvironementName.*")){Write-Log "Removing $($_.FullName)"; Remove-Item -Path $_.FullName;}};
			Get-ChildItem -Path "$TableauRefreshExtractsConfigurationDirectory" | %{Write-Log "Renaming $($_.FullName) to $($_.FullName.Replace(".$EnvironementName.json",".json"))"; Rename-Item $_.FullName $_.FullName.Replace(".$EnvironementName.json",".json");};
		}
		catch {
			Write-Log "Error : $($Error[0].exception.GetBaseException().Message)";
		}
	}

	#-------------------------------------------------------------------------------------------------#
	# SSRSSettings
	#-------------------------------------------------------------------------------------------------#
	if ($RS -eq $true)
	{
		# Deploy on portal
		Write-Log "####################################################################################################"
		Write-Log "Deploying items on Reporting Services portal ($($ConfigFile.Settings.SSRSSettings.ReportServer.URL))"

		foreach ( $CurrentDeployment in $ConfigFile.Settings.SSRSSettings.Deployment )
		{
			Write-Log "----------------------------------------------------------------------------------------------------"
			if ($CurrentDeployment.Source -eq "Embedded")
			{
				# Build XML config file for Deploy_SSRS.ps1
				$DeploySSRSConfigPath = "$($PackageInstallDir)\SSRS\Deploy_SSRS.config"
				$DeploySSRSConfig = [xml]"<?xml version=`"1.0`" encoding=`"utf-8`" ?><Deploy_SSRS><Folders/><DataSources/><DataSets/><Reports/><Resources/></Deploy_SSRS>"
				Write-Log "Generating $($DeploySSRSConfigPath)"

				# Loop through folders
				foreach ($CurrentFolder in $CurrentDeployment.Folders.Folder)
				{
					$NewFolder = $DeploySSRSConfig.CreateElement("Folder")

					# Retrieve attributes
					foreach ( $AttributeName in ("Path").Split(";", [System.StringSplitOptions]::RemoveEmptyEntries) )
					{
						 $NewFolder.SetAttribute($AttributeName, "$($CurrentFolder.GetAttribute($AttributeName))")
					}

					# Retrieve properties
					$NewFolderProperties = $DeploySSRSConfig.CreateElement("Properties")
					$NewFolder.AppendChild($NewFolderProperties) | Out-Null

					foreach ( $PropertyName in ("Description;Hidden").Split(";", [System.StringSplitOptions]::RemoveEmptyEntries) )
					{
						if ( $CurrentFolder.HasAttribute($PropertyName) -eq $true )
						{
							$NewProperty = $DeploySSRSConfig.CreateElement("Property")
							$NewProperty.SetAttribute("Name", $PropertyName)
							$NewProperty.SetAttribute("Value", "$($CurrentFolder.GetAttribute($PropertyName))")
							$NewFolder.SelectSingleNode("Properties").AppendChild($NewProperty) | Out-Null
						}
					}

					# Get declared policies or init policies
					if ( $CurrentFolder.SelectSingleNode("Policies").Count -ne 0 )
					{
						$NewFolder.AppendChild($DeploySSRSConfig.ImportNode($CurrentFolder.SelectSingleNode("Policies"), $true)) | Out-Null
					}
					else
					{
						$NewFolder.AppendChild($DeploySSRSConfig.CreateElement("Policies")) | Out-Null
					}

					$DeploySSRSConfig.Deploy_SSRS.SelectSingleNode("Folders").AppendChild($NewFolder) | Out-Null
				}

				# Loop through datasources
				foreach ($CurrentDataSource in $CurrentDeployment.DataSources.DataSource)
				{
					$NewDataSource = $DeploySSRSConfig.CreateElement("DataSource")

					# Retrieve attributes
					foreach ( $AttributeName in ("Path;Name;Extension;ConnectionString").Split(";", [System.StringSplitOptions]::RemoveEmptyEntries) )
					{
						 $NewDataSource.SetAttribute($AttributeName, "$($CurrentDataSource.GetAttribute($AttributeName))")
					}

					# Retrieve credential
					$NewDataSourceCredential = $DeploySSRSConfig.CreateElement("Credential")
					$NewDataSourceCredential.SetAttribute("Retrieval", "$($CurrentDataSource.CredentialRetrieval)")
					$NewDataSource.AppendChild($NewDataSourceCredential) | Out-Null

					foreach ($OptionName in ("Prompt;UserName;Password;WindowsCredentials;ImpersonateUser").Split(";", [System.StringSplitOptions]::RemoveEmptyEntries))
					{
						if ( $CurrentDataSource.HasAttribute($OptionName) -eq $true )
						{
							$NewOption = $DeploySSRSConfig.CreateElement("Option")
							$NewOption.SetAttribute("Name", $OptionName)
							$NewOption.SetAttribute("Value", "$($CurrentDataSource.GetAttribute($OptionName))")
							$NewDataSource.SelectSingleNode("Credential").AppendChild($NewOption) | Out-Null
						}
					}

					# Retrieve properties
					$NewDataSourceProperties = $DeploySSRSConfig.CreateElement("Properties")
					$NewDataSource.AppendChild($NewDataSourceProperties) | Out-Null

					foreach ( $PropertyName in ("Description;Hidden").Split(";", [System.StringSplitOptions]::RemoveEmptyEntries) )
					{
						if ( $CurrentDataSource.HasAttribute($PropertyName) -eq $true )
						{
							$NewProperty = $DeploySSRSConfig.CreateElement("Property")
							$NewProperty.SetAttribute("Name", $PropertyName)
							$NewProperty.SetAttribute("Value", "$($CurrentDataSource.GetAttribute($PropertyName))")
							$NewDataSource.SelectSingleNode("Properties").AppendChild($NewProperty) | Out-Null
						}
					}

					# Get declared policies or init policies
					if ( $CurrentDataSource.SelectSingleNode("Policies").Count -ne 0 )
					{
						$NewDataSource.AppendChild($DeploySSRSConfig.ImportNode($CurrentDataSource.SelectSingleNode("Policies"), $true)) | Out-Null
					}
					else
					{
						$NewDataSource.AppendChild($DeploySSRSConfig.CreateElement("Policies")) | Out-Null
					}

					$DeploySSRSConfig.Deploy_SSRS.SelectSingleNode("DataSources").AppendChild($NewDataSource) | Out-Null
				}

				# Loop through datasets
				foreach ($CurrentDataSet in $CurrentDeployment.DataSets.DataSet)
				{
					$NewDataSet = $DeploySSRSConfig.CreateElement("DataSet")

					# Retrieve attributes
					foreach ( $AttributeName in ("Path;Name").Split(";", [System.StringSplitOptions]::RemoveEmptyEntries) )
					{
						 $NewDataSet.SetAttribute($AttributeName, "$($CurrentDataSet.GetAttribute($AttributeName))")
					}

					# Retrieve properties
					$NewDataSetProperties = $DeploySSRSConfig.CreateElement("Properties")
					$NewDataSet.AppendChild($NewDataSetProperties) | Out-Null

					foreach ( $PropertyName in ("Description;Hidden").Split(";", [System.StringSplitOptions]::RemoveEmptyEntries) )
					{
						if ( $CurrentDataSet.HasAttribute($PropertyName) -eq $true )
						{
							$NewProperty = $DeploySSRSConfig.CreateElement("Property")
							$NewProperty.SetAttribute("Name", $PropertyName)
							$NewProperty.SetAttribute("Value", "$($CurrentDataSet.GetAttribute($PropertyName))")
							$NewDataSet.SelectSingleNode("Properties").AppendChild($NewProperty) | Out-Null
						}
					}

					# Retrieve options
					$NewDataSetOptions = $DeploySSRSConfig.CreateElement("Options")
					$NewDataSet.AppendChild($NewDataSetOptions) | Out-Null

					if ($CurrentDataSet.HasAttribute("DataSourceName") -eq $true)
					{
						$NewOption = $DeploySSRSConfig.CreateElement("DataSource")
						$NewOption.SetAttribute("Name", "DataSetDataSource")
						$NewOption.SetAttribute("ReferenceDataSourceName", "$($CurrentDataSet.DataSourceName)")
						$NewDataSet.SelectSingleNode("Options").AppendChild($NewOption) | Out-Null
					}

					# Get declared policies or init policies
					if ( $CurrentDataSet.SelectSingleNode("Policies").Count -ne 0 )
					{
						$NewDataSet.AppendChild($DeploySSRSConfig.ImportNode($CurrentDataSet.SelectSingleNode("Policies"), $true)) | Out-Null
					}
					else
					{
						$NewDataSet.AppendChild($DeploySSRSConfig.CreateElement("Policies")) | Out-Null
					}

					$DeploySSRSConfig.Deploy_SSRS.SelectSingleNode("DataSets").AppendChild($NewDataSet) | Out-Null
				}

				# Loop through reports
				foreach ($CurrentReport in $CurrentDeployment.Reports.Report)
				{
					$NewReport = $DeploySSRSConfig.CreateElement("Report")

					# Retrieve attributes
					foreach ( $AttributeName in ("Path;Name").Split(";", [System.StringSplitOptions]::RemoveEmptyEntries) )
					{
						 $NewReport.SetAttribute($AttributeName, "$($CurrentReport.GetAttribute($AttributeName))")
					}

					# Retrieve properties
					$NewReportProperties = $DeploySSRSConfig.CreateElement("Properties")
					$NewReport.AppendChild($NewReportProperties) | Out-Null

					foreach ( $PropertyName in ("Description;Hidden").Split(";", [System.StringSplitOptions]::RemoveEmptyEntries) )
					{
						if ( $CurrentReport.HasAttribute($PropertyName) -eq $true )
						{
							$NewProperty = $DeploySSRSConfig.CreateElement("Property")
							$NewProperty.SetAttribute("Name", $PropertyName)
							$NewProperty.SetAttribute("Value", "$($CurrentReport.GetAttribute($PropertyName))")
							$NewReport.SelectSingleNode("Properties").AppendChild($NewProperty) | Out-Null
						}
					}

					# Retrieve options
					$NewReportOptions = $DeploySSRSConfig.CreateElement("Options")
					$NewReport.AppendChild($NewReportOptions) | Out-Null

					foreach ( $SharedDataSet in $CurrentReport.SharedDataSets.SharedDataSet )
					{
						$NewOption = $DeploySSRSConfig.CreateElement("Reference")
						$NewOption.SetAttribute("Name", $SharedDataSet.Name)
						$NewOption.SetAttribute("ReferenceItemName", $SharedDataSet.Reference)
						$NewReport.SelectSingleNode("Options").AppendChild($NewOption) | Out-Null
					}

					# Get declared policies or init policies
					if ( $CurrentReport.SelectSingleNode("Policies").Count -ne 0 )
					{
						$NewReport.AppendChild($DeploySSRSConfig.ImportNode($CurrentReport.SelectSingleNode("Policies"), $true)) | Out-Null
					}
					else
					{
						$NewReport.AppendChild($DeploySSRSConfig.CreateElement("Policies")) | Out-Null
					}

					# Get declared Subscriptions or init Subscriptions
					if ( $CurrentReport.SelectSingleNode("Subscriptions").Count -ne 0 )
					{
						$NewReport.AppendChild($DeploySSRSConfig.ImportNode($CurrentReport.SelectSingleNode("Subscriptions"), $true)) | Out-Null
					}
					else
					{
						$NewReport.AppendChild($DeploySSRSConfig.CreateElement("Subscriptions")) | Out-Null
					}

					$DeploySSRSConfig.Deploy_SSRS.SelectSingleNode("Reports").AppendChild($NewReport) | Out-Null
				}

				# Loop through resources
				foreach ($CurrentResource in $CurrentDeployment.Resources.Resource)
				{
					$NewResource = $DeploySSRSConfig.CreateElement("Resource")

					# Retrieve attributes
					foreach ( $AttributeName in ("Path;Name").Split(";", [System.StringSplitOptions]::RemoveEmptyEntries) )
					{
						 $NewResource.SetAttribute($AttributeName, "$($CurrentResource.GetAttribute($AttributeName))")
					}

					# Retrieve properties
					$NewResourceProperties = $DeploySSRSConfig.CreateElement("Properties")
					$NewResource.AppendChild($NewResourceProperties) | Out-Null

					foreach ( $PropertyName in ("Description;MimeType;Hidden").Split(";", [System.StringSplitOptions]::RemoveEmptyEntries) )
					{
						if ( $CurrentResource.HasAttribute($PropertyName) -eq $true )
						{
							$NewProperty = $DeploySSRSConfig.CreateElement("Property")
							$NewProperty.SetAttribute("Name", $PropertyName)
							$NewProperty.SetAttribute("Value", "$($CurrentResource.GetAttribute($PropertyName))")
							$NewResource.SelectSingleNode("Properties").AppendChild($NewProperty) | Out-Null
						}
					}

					# Get declared policies or init policies
					if ( $CurrentResource.SelectSingleNode("Policies").Count -ne 0 )
					{
						$NewResource.AppendChild($DeploySSRSConfig.ImportNode($CurrentResource.SelectSingleNode("Policies"), $true)) | Out-Null
					}
					else
					{
						$NewResource.AppendChild($DeploySSRSConfig.CreateElement("Policies")) | Out-Null
					}

					$DeploySSRSConfig.Deploy_SSRS.SelectSingleNode("Resources").AppendChild($NewResource) | Out-Null
				}

				$DeploySSRSConfig.Save("$($DeploySSRSConfigPath)")
			}
			else
			{
				$DeploySSRSConfigPath = "$($PackageInstallDir)\SSRS\$($CurrentDeployment.Source)"
				Write-Log "Using $($DeploySSRSConfigPath)"
			}

			# Run Deploy_SSRS.ps1
			Write-Log "Executing $PackageInstallDir\SSRS\Deploy_SSRS.ps1 -ConfigPath `"$($DeploySSRSConfigPath)`" -ReportServerUrl `"$($ConfigFile.Settings.SSRSSettings.ReportServer.URL)`" -SourcePath `"$($PackageInstallDir)\SSRS`" -InstallMode `"$($CurrentDeployment.InstallMode)`" -RootFolder `"$($CurrentDeployment.RootFolder)`""
			& $PackageInstallDir\SSRS\Deploy_SSRS.ps1 -ConfigPath "$($DeploySSRSConfigPath)" -ReportServerUrl "$($ConfigFile.Settings.SSRSSettings.ReportServer.URL)" -SourcePath "$($PackageInstallDir)\SSRS" -InstallMode "$($CurrentDeployment.InstallMode)" -RootFolder "$($CurrentDeployment.RootFolder)" *>&1 | Add-Content -path $logFile -encoding ASCII -passthru
			if ( $? -ne $true )
			{
				Write-Log "Error while executing $PackageInstallDir\SSRS\Deploy_SSRS.ps1 -ConfigPath `"$($DeploySSRSConfigPath)`" -ReportServerUrl `"$($ConfigFile.Settings.SSRSSettings.ReportServer.URL)`" -SourcePath `"$($PackageInstallDir)\SSRS`" -InstallMode `"$($CurrentDeployment.InstallMode)`" -RootFolder `"$($CurrentDeployment.RootFolder)`""
				Exit 1
			}
		}

		if(-not $CheckOnly){
			try {
				#-------------------------------------------------------------------------------------------------#
				# Update SIDP SSRS Deployed Version
				#-------------------------------------------------------------------------------------------------#
				Write-Log "Updating SIDP SSRS Deployed Version..."
				Invoke-Sqlcmd -ServerInstance "$VersionParameterInstance" -Database "$VersionParameterDatabase" -Query "Exec dbo.UpdateSIDPVersion @Application = 'SSRS', @Version = '$($Version)'" -verbose *>&1 | Add-Content -path $logFile -encoding ASCII -passthru
			}
			catch {
				Write-Log "Error : $($Error[0].exception.GetBaseException().Message)";
			}
		}
	}
	#-------------------------------------------------------------------------------------------------#
	# Remove $PackageDir
	#-------------------------------------------------------------------------------------------------#
	Write-Log "####################################################################################################"
	Write-Log "Removing $($PackageDir)"
	Remove-Item $PackageDir -Force -Recurse -ErrorAction SilentlyContinue
}
catch
{
	Write-Log "Error : $($Error[0].exception.GetBaseException().Message)"
	exit 1
}
