#-------------------------------------------------------------------------------------------------#
# Call the PowerShell script and pass the arguments
# .\Setup_SSIS_Project.ps1 "C:\WorkArea\Scripts\ServerConfig.xml"
# .\Setup_SSIS_Project.ps1 -ConfigPath "C:\WorkArea\Scripts\ServerConfig.xml"
#-------------------------------------------------------------------------------------------------#

#-------------------------------------------------------------------------------------------------#
# Script parameters
#-------------------------------------------------------------------------------------------------#
[CmdletBinding()]
param (
	[Parameter(Mandatory = $true)]
	[string]$ConfigPath = $null
)

#-------------------------------------------------------------------------------------------------#
# Function Write-Log
#-------------------------------------------------------------------------------------------------#
Function Write-Log
{
	Param (
		[string]$logOutput
	)
	$OperationDateTime = $(Get-Date -Format o).Substring(0,24);
	$OperationLog = "$OperationDateTime : $logOutput";
	Write-Output $OperationLog;
	Add-content $logFile -value $OperationLog;
}

#-------------------------------------------------------------------------------------------------#
# Variables
#-------------------------------------------------------------------------------------------------#
$scriptHome = Split-Path $MyInvocation.MyCommand.Path
$scriptName = $MyInvocation.MyCommand.Name
$scriptName = $scriptName.Substring(0, ($scriptName.Length - 4))

$currentLogDate = Get-Date -format yyyyMMddHHmmss
$logFile = "$($scriptHome)\$($scriptName)_$($currentLogDate).log"

#-------------------------------------------------------------------------------------------------#
# Start script
#-------------------------------------------------------------------------------------------------#
Write-Log "Starting $($scriptName) ..."
Write-Log "Log available in $($logFile)"

#-------------------------------------------------------------------------------------------------#
# Initialize Settings from Config file
#-------------------------------------------------------------------------------------------------#
Write-Log "Reading configuration file $($ConfigPath)"
[xml]$ConfigFile = Get-Content "$($ConfigPath)"
$SSIS_SQL_INSTANCE = $ConfigFile.Settings.SSISSettings.MSQL_SERVER_NAME
$SSIS_PROJECT_FOLDER = $ConfigFile.Settings.SSISFolder.FolderName
$SSIS_ENVIRONMENTS = $ConfigFile.Settings.SSISEnvironments.Environment

Write-Log "SSIS_SQL_INSTANCE   = $SSIS_SQL_INSTANCE"
Write-Log "SSIS_PROJECT_FOLDER = $SSIS_PROJECT_FOLDER"

# Store the IntegrationServices Assembly namespace to avoid typing it every time
$SSISNamespace = "Microsoft.SqlServer.Management.IntegrationServices"

# Loading IntegrationServices Assembly .........
Write-Log "Loading IntegrationServices Assembly ..."
[System.Reflection.Assembly]::LoadWithPartialName("$($SSISNamespace)")

#-------------------------------------------------------------------------------------------------#
# Deploy and setup the SSIS project
#-------------------------------------------------------------------------------------------------#
try
{
	$SQLConnectionString = "Data Source=$SSIS_SQL_INSTANCE;Initial Catalog=master;Integrated Security = True;"
	$SQLConnection = New-Object System.Data.SqlClient.SqlConnection $SQLConnectionString
	$IntegrationServices = New-Object "$SSISNamespace.IntegrationServices" $SQLConnection
	$IntegrationServicesCatalog = $IntegrationServices.Catalogs["SSISDB"]

	Write-Log "Trying to retrieve SSIS project folder"
	$IntegrationServicesProjectFolder = $IntegrationServicesCatalog.Folders[$SSIS_PROJECT_FOLDER]

	#Check If the destination SSIS folder doesnt exists
	if($IntegrationServicesProjectFolder -eq $null )
	{
		Write-Log "Trying to create SSIS project folder ..."
		$IntegrationServicesProjectFolder = New-Object "$SSISNamespace.CatalogFolder" ($IntegrationServicesCatalog, $SSIS_PROJECT_FOLDER, $SSIS_PROJECT_FOLDER)
		$IntegrationServicesProjectFolder.Create()
		Write-Log "SSIS project folder has been created successfully"

		#Get the SSIS folder
		$IntegrationServicesProjectFolder = $IntegrationServicesCatalog.Folders[$SSIS_PROJECT_FOLDER]
	}
	
	#List all the Installed Environments
	$InstalledEnvironments = $IntegrationServicesProjectFolder.Environments;

	foreach ($InstalledEnvironmentConfig in $InstalledEnvironments)
	{
		$Found = $false;
		# Get the installed Environment
		$InstalledEnvironment = $InstalledEnvironments[$InstalledEnvironmentConfig.Name];

		# if the environment exists
		if($InstalledEnvironment -ne $null)
		{
			foreach ($CurrentEnvironmentConfig in $SSIS_ENVIRONMENTS)
			{
				if($CurrentEnvironmentConfig.Name -eq $InstalledEnvironment.Name) { $Found = $true; }
			}

			# Drop the environment if there is no mapped configuration
			if(-not $Found)
			{
				Write-Log "Trying to drop SSIS environment $($InstalledEnvironment.Name) because it does not have a mapped configuration ..."
				$InstalledEnvironment.Drop()
				Write-Log "SSIS environment $($InstalledEnvironment.Name) has been dropped successfully."
			}
		}
	}

	#List all the Environments from the configuration file
	foreach ($CurrentEnvironmentConfig in $SSIS_ENVIRONMENTS)
	{
		# Check if current environment must be deployed
		if($CurrentEnvironmentConfig.ToBeDeployed -eq "True")
		{
			# Check if current Environment already exists
			$InstalledEnvironment = $IntegrationServicesProjectFolder.Environments[$CurrentEnvironmentConfig.Name]
			
			# if the environment exists
			if($InstalledEnvironment -ne $null)
			{
				foreach($InstalledEnvironmentVariable in $InstalledEnvironment.Variables)
				{
					$InstalledVariable_name = $InstalledEnvironmentVariable.Name
					$InstalledVariable_type = $InstalledEnvironmentVariable.Type
					$InstalledVariable_description = $InstalledEnvironmentVariable.Description
					$InstalledVariable_value = $InstalledEnvironmentVariable.Value
					$InstalledVariable_sensitive = [System.Convert]::ToBoolean($InstalledEnvironmentVariable.Sensitive)
				
					$Found = $false;
				
					foreach($CurrentVariableConfig in $currentEnvironmentConfig.Variable)
					{
						$CurrentVariable_name = $CurrentVariableConfig.Name
						$CurrentVariable_type = $CurrentVariableConfig.Type
						$CurrentVariable_description = $CurrentVariableConfig.Description
						$CurrentVariable_value = $CurrentVariableConfig.Value
						$CurrentVariable_sensitive = [System.Convert]::ToBoolean($CurrentVariableConfig.Sensitive)

						if($CurrentVariable_name -eq $InstalledVariable_name)
						{
							$Found = $true;
							if(($CurrentVariable_type -eq $InstalledVariable_type) -and ($CurrentVariable_description -eq $InstalledVariable_description) -and ($CurrentVariable_value -eq $InstalledVariable_value) -and ($CurrentVariable_sensitive -eq $InstalledVariable_sensitive))
							{
								Write-Log "Variable value Unchanged : $InstalledVariable_name ...";
							}
							else
							{
								$InstalledEnvironmentVariable.Type = $CurrentVariable_type;
								$InstalledEnvironmentVariable.Description = $CurrentVariable_description;
								$InstalledEnvironmentVariable.Value = $CurrentVariable_value;
								$InstalledEnvironmentVariable.Sensitive = [System.Convert]::ToBoolean($CurrentVariable_sensitive);
								$InstalledEnvironment.Alter();
							}
						}
					}

					if(-not $Found)
					{
						$InstalledEnvironment.Variables.Remove($InstalledVariable_name);
						$InstalledEnvironment.Alter();
					}
				}

				$InstalledEnvironment = $IntegrationServicesProjectFolder.Environments[$CurrentEnvironmentConfig.Name];

				# All the variables to create
				foreach($CurrentVariableConfig in $currentEnvironmentConfig.Variable)
				{
					$Found = $false;
				
					foreach($InstalledEnvironmentVariable in $InstalledEnvironment.Variables)
					{
						if($CurrentVariableConfig.Name -eq $InstalledEnvironmentVariable.Name)
						{
							$Found = $true;
						}
					}

					if(-not $Found)
					{
						$CurrentVariable_name = $CurrentVariableConfig.Name
						$CurrentVariable_type = $CurrentVariableConfig.Type
						$CurrentVariable_description = $CurrentVariableConfig.Description
						$CurrentVariable_value = $CurrentVariableConfig.Value
						$CurrentVariable_sensitive = [System.Convert]::ToBoolean($CurrentVariableConfig.Sensitive)

						Write-Log "Adding $CurrentVariable_name variable to $($CurrentEnvironmentConfig.Name) environment ..."
						$InstalledEnvironment.Variables.Add($CurrentVariable_name, $CurrentVariable_type, $CurrentVariable_value, $CurrentVariable_sensitive, $CurrentVariable_description);
						$InstalledEnvironment.Alter();
					}
				}

				Write-Log "SSIS environment $($CurrentEnvironmentConfig.Name) has been updated successfully"
			}
			else # If the environment does not exist
			{
				#Init the current environment
				Write-Log "Trying to create SSIS environment $($CurrentEnvironmentConfig.Name) in SSIS Folder $($SSIS_PROJECT_FOLDER) ..."
				$CurrentEnvironment = New-Object "$SSISNamespace.EnvironmentInfo" ($IntegrationServicesProjectFolder, $CurrentEnvironmentConfig.Name, "Environment $($CurrentEnvironmentConfig.Name)")

				# All the variables to create
				foreach($CurrentVariableConfig in $currentEnvironmentConfig.Variable)
				{
					$CurrentVariable_name = $CurrentVariableConfig.Name
					$CurrentVariable_type = $CurrentVariableConfig.Type
					$CurrentVariable_description = $CurrentVariableConfig.Description
					$CurrentVariable_value = $CurrentVariableConfig.Value
					$CurrentVariable_sensitive = [System.Convert]::ToBoolean($CurrentVariableConfig.Sensitive)

					Write-Log "Adding $CurrentVariable_name variable to $($CurrentEnvironmentConfig.Name) environment ..."
					$CurrentEnvironment.Variables.Add($CurrentVariable_name, $CurrentVariable_type, $CurrentVariable_value, $CurrentVariable_sensitive, $CurrentVariable_description)
				}

				#Create the current environment
				$CurrentEnvironment.Create()
				Write-Log "SSIS environment $($CurrentEnvironmentConfig.Name) has been created successfully"
			}
		}
	}

	$SQLConnection.Close()
}
catch
{
	Write-Log "Error : $($Error[0].exception.GetBaseException().Message)"
	exit 1
}
