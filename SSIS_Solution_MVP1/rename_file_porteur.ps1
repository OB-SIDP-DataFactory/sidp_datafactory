Get-ChildItem -Filter MTPSID002*.csv |
%{
    $header_date = (Get-Content $_ -First 1).substring(25,8);
    $NewFileName = "X1_MTPSID002_" + $header_date + ".csv";
    $FileContent = Get-Content -Path $_.FullName;
    $_.FullName;
    Set-Content -Path $NewFileName -Value $FileContent;
   
}