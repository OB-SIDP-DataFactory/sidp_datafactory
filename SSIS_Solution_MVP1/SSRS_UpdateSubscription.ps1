#-------------------------------------------------------------------------------------------------#
# Script parameters
#-------------------------------------------------------------------------------------------------#
[CmdletBinding()]
param (
    [Parameter(Mandatory = $true)]
    [xml]$ConfigXML = $null
)

#-------------------------------------------------------------------------------------------------#
# Function Write-Log
#-------------------------------------------------------------------------------------------------#
Function Write-Log
{
    Param ( [string]$logOutput )

    Write-Output $logOutput
    Add-content $logFile -value $logOutput
}

#-------------------------------------------------------------------------------------------------#
# Function UpdateReportSubscriptions
#-------------------------------------------------------------------------------------------------#
Function UpdateReportSubscriptions
{
    Param ( [Xml.XmlElement]$ItemXML )
    Write-Log "Update subscription for '$($ItemXML.Path)/$($ItemXML.Name)' ..."

	$CurrentSubscriptionDefinition = $ItemXML.SelectSingleNode("Subscription")

    foreach ( $CurrentSubscription in $ReportServerWS.ListSubscriptions("$($ItemXML.Path)/$($ItemXML.Name)") )
    {
        if ( $CurrentSubscription.Description -eq $CurrentSubscriptionDefinition.Name )
		{
		    $SubscriptionId = $CurrentSubscription.SubscriptionID

			# Retrieve subscription properties
            $Settings = New-Object ($ExtensionSettingsNameSpace)
            $DataRetrieval = New-Object ($DataRetrievalPlanNameSpace)
            $Description = ""
            $Active = New-Object ($ActiveStateNameSpace)
            $Status = ""
            $EventType = ""
            $MatchData = ""
            $ReportParams = New-Object ($ParameterValueOrFieldReferenceNameSpace + '[]')0
            $SubscriptionOwner = $ReportServerWS.GetDataDrivenSubscriptionProperties($SubscriptionId, [ref]$Settings, [ref]$DataRetrieval, [ref]$Description, [ref]$Active, [ref]$Status, [ref]$EventType, [ref]$MatchData, [ref]$ReportParams)	

			# Update subscription properties
            $UpdatedExtensionParams = New-Object ($ParameterValueOrFieldReferenceNameSpace + '[]')0

			foreach ( $ExtensionParam in $Settings.ParameterValues )
			{
			    if ( "$($ExtensionParam.Name)$($ExtensionParam.ParameterName)" -ne "USERNAME" -and "$($ExtensionParam.Name)$($ExtensionParam.ParameterName)" -ne "PASSWORD" -and "$($ExtensionParam.Name)$($ExtensionParam.ParameterName)" -ne "RENDER_FORMAT" )
				{
				     $UpdatedExtensionParams += $ExtensionParam
				}
			}

            foreach ( $ExtensionParamDefinition in $CurrentSubscriptionDefinition.SelectSingleNode("ExtensionParameters").SelectNodes("ExtensionParameter") )
            {
                $ExtensionParam = New-Object ($ParameterValueNameSpace)
                $ExtensionParam.Name = "$($ExtensionParamDefinition.Name)"
                $ExtensionParam.Value = "$($ExtensionParamDefinition.Value)"
                $UpdatedExtensionParams += $ExtensionParam
            }

			$Settings.ParameterValues = $UpdatedExtensionParams

            $DataRetrieval.DataSet.Query.CommandText = "$($CurrentSubscriptionDefinition.DataSet.Query)"

			$ScheduleStartDateTime = Get-Date -date $(Get-Date).AddMinutes(2) -format "yyyy-MM-dd HH:mm"
			$ScheduleEndDate = Get-Date -date $(Get-Date) -format "yyyy-MM-dd"
            $MatchData = "<ScheduleDefinition><StartDateTime>" + $ScheduleStartDateTime + "</StartDateTime><DailyRecurrence><DaysInterval>1</DaysInterval></DailyRecurrence><EndDate>" + $ScheduleEndDate + "</EndDate></ScheduleDefinition>"

			$ReportServerWS.SetDataDrivenSubscriptionProperties($SubscriptionId, $Settings, $DataRetrieval, $Description, $EventType, $MatchData, $ReportParams)
		}
    }
}

#-------------------------------------------------------------------------------------------------#
# Variables
#-------------------------------------------------------------------------------------------------#
$scriptHome = Split-Path $MyInvocation.MyCommand.Path
$scriptName = $MyInvocation.MyCommand.Name
$scriptName = $scriptName.Substring(0, ($scriptName.Length - 4))

$currentLogDate = Get-Date -format yyyyMMddHHmmss
$logFile = "$($scriptHome)\$($scriptName)_$($currentLogDate).log"

#-------------------------------------------------------------------------------------------------#
# Start script
#-------------------------------------------------------------------------------------------------#
Write-Log "Starting $($scriptName) ..."
Write-Log "Log available in $($logFile)"

#-------------------------------------------------------------------------------------------------#
# Start
#-------------------------------------------------------------------------------------------------#
try
{
    # Create a WebServiceProxy object
	$ReportServerUrl = $ConfigXML.SSRSSettings.ReportServer.URL
    Write-Log "Connecting to $($ReportServerUrl) ..."
    $ReportServerUri = "$($ReportServerUrl)/ReportService2010.asmx?wsdl"
    $ReportServerWS = New-WebServiceProxy -Uri $ReportServerUri -UseDefaultCredential
    $ReportServerNameSpace = ( $ReportServerWS.GetType().Namespace )
    $PropertyNameSpace = ( $ReportServerWS.GetType().Namespace + '.Property')
    $PolicyNameSpace = ( $ReportServerWS.GetType().Namespace + '.Policy')
    $RoleNameSpace = ( $ReportServerWS.GetType().Namespace + '.Role')
    $DataSourceDefinitionNameSpace = ( $ReportServerWS.GetType().Namespace + '.DataSourceDefinition')
    $DataSourceReferenceNameSpace = ( $ReportServerWS.GetType().Namespace + '.DataSourceReference')
    $CredentialRetrievalEnumNameSpace = ( $ReportServerWS.GetType().Namespace + '.CredentialRetrievalEnum')
    $SearchConditionNameSpace = ( $ReportServerWS.GetType().Namespace + '.SearchCondition')
    $ConditionEnumNameSpace = ( $ReportServerWS.GetType().Namespace + '.ConditionEnum')
    $BooleanOperatorEnumNameSpace = ( $ReportServerWS.GetType().Namespace + '.BooleanOperatorEnum')
    $ItemReferenceNameSpace = ( $ReportServerWS.GetType().Namespace + '.ItemReference')
    $ExtensionSettingsNameSpace = ( $ReportServerWS.GetType().Namespace + '.ExtensionSettings')
    $DataRetrievalPlanNameSpace = ( $ReportServerWS.GetType().Namespace + '.DataRetrievalPlan')
    $ParameterValueOrFieldReferenceNameSpace = ( $ReportServerWS.GetType().Namespace + '.ParameterValueOrFieldReference')
    $ParameterValueNameSpace = ( $ReportServerWS.GetType().Namespace + '.ParameterValue')
    $ParameterFieldReferenceNameSpace = ( $ReportServerWS.GetType().Namespace + '.ParameterFieldReference')
    $DataSourceNameSpace = ( $ReportServerWS.GetType().Namespace + '.DataSource')
    $DataSetDefinitionNameSpace = ( $ReportServerWS.GetType().Namespace + '.DataSetDefinition')
    $DataSetFieldNameSpace = ( $ReportServerWS.GetType().Namespace + '.Field')
    $QueryDefinitionNameSpace = ( $ReportServerWS.GetType().Namespace + '.QueryDefinition')
    $ActiveStateNameSpace = ( $ReportServerWS.GetType().Namespace + '.ActiveState')

	UpdateReportSubscriptions $ConfigXML.SSRSSettings.SelectSingleNode("Report")
}
catch
{
    Write-Log "Error : $($Error[0].exception.GetBaseException().Message)"
    exit 1
}
