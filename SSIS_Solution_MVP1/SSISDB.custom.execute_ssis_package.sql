/* Select the SSISDB database */
USE SSISDB
GO
/* Create a parameter (variable) named @SQL */
DECLARE @SQL VARCHAR(2000)

/* Create the Custom schema if it does not already exist */
PRINT 'Custom Schema'
IF NOT EXISTS(SELECT NAME FROM SYS.SCHEMAS WHERE NAME = 'custom')
BEGIN
    /* Create Schema statements must occur first in a batch */
    PRINT ' - Creating custom schema'
    SET @SQL = 'CREATE SCHEMA custom'
    EXEC(@SQL)
    PRINT ' - custom schema created'
END
ELSE
BEGIN
    PRINT ' - custom Schema already exists.'
END

/* Drop the custom.execute_ssis_package Stored Procedure if it already exists */
PRINT 'Custom.execute_ssis_package Stored Procedure'
IF EXISTS( SELECT S.NAME + '.' +  P.NAME
             FROM SYS.PROCEDURES P
             JOIN SYS.SCHEMAS S ON S.SCHEMA_ID = P.SCHEMA_ID
            WHERE S.NAME = 'custom'
              AND P.NAME = 'execute_ssis_package')
BEGIN
    PRINT ' - Dropping custom.execute_ssis_package'
    DROP PROCEDURE custom.execute_ssis_package
    PRINT ' - Custom.execute_ssis_package dropped'
END

/* Create the Custom.execute_ssis_package Stored Procedure */
PRINT ' - Creating custom.execute_ssis_package'
GO
/*
     Stored Procedure: custom.execute_ssis_package
     Description: Creates a wrapper around the SSISDB Catalog procedures used to execute the SSIS Packages.
                  Packages in the SSIS Catalog are referenced by a multi-part identifier - or path - that consists of the following hierarchy:
                      Catalog Name: Implied by the database name in Integration Server 2016
                      |-Folder Name: A folder created before or at Deployment to contain the SSIS project
                      |-Project Name: The name of the SSIS Project deployed
                      |-Package Name: The name(s) of the SSIS Package(s) deployed

        Parameters:
        @ExecutionID [bigint] {Output} – 
          Output parameter (variable) passed back to the caller
        @ExecutionStatus [bigint] {Output} – 
          Output parameter (variable) passed back to the caller
        @LoggingLevel [varchar(16)] {Default} – 
          Contains the (case-insensitive) name of the logging level to apply to this execution instance
        @Use32BitRunTime [bit] {Default} – 
          1 == Use 64-bit run-time
          0 == Use 32-bit run-time
        @ReferenceID [bigint] {Default} –
          contains a reference to an Execution Environment
*/
CREATE PROCEDURE custom.execute_ssis_package
  @FolderName nvarchar(128) = N''
 ,@ProjectName nvarchar(128) = N''
 ,@PackageName nvarchar(128) = N''
 ,@EnvironmentName nvarchar(128) = N''
 ,@RunDate nvarchar(32) = N''
 ,@LoggingLevel varchar(16) = 'Verbose'
 ,@Use32BitRunTime bit = 0
AS
BEGIN
    SET NOCOUNT ON
    SET IMPLICIT_TRANSACTIONS OFF

    DECLARE @reference_id BIGINT
    DECLARE @execution_id BIGINT
    DECLARE @execution_status BIGINT = 1
    DECLARE @logging_level SMALLINT
    DECLARE @run_date NVARCHAR(32)

    /* Retrieve the reference_id */
    SELECT @reference_id = reference_id 
     FROM catalog.folders AS fld 
     JOIN catalog.projects AS prj ON prj.folder_id = fld.folder_id
     JOIN catalog.environment_references AS env_ref ON env_ref.project_id = prj.project_id AND env_ref.environment_folder_name = fld.name
    WHERE fld.name = @FolderName
      AND prj.name = @ProjectName
      AND env_ref.environment_name = @EnvironmentName

    /* Set run_date to current date */
    SELECT @run_date = CASE WHEN @RunDate <> '' THEN @RunDate ELSE CONVERT(NVARCHAR(32), GETDATE(), 120) END

    /* Call the catalog.create_execution stored procedure to initialize execution location and parameters */
    EXEC [SSISDB].[catalog].[create_execution]
      @package_name = @PackageName,
      @execution_id = @execution_id OUTPUT,
      @folder_name = @FolderName,
      @project_name = @ProjectName,
      @use32bitruntime = @Use32BitRunTime,
      @reference_id = @reference_id

    /* Call the catalog.set_execution_parameter_value stored procedure to set package parameters
       For the following parameters, set object_type to 50
         - LOGGING_LEVEL
         - CUSTOMIZED_LOGGING_LEVEL
         - DUMP_ON_ERROR
         - DUMP_ON_EVENT
         - DUMP_EVENT_CODE
         - CALLER_INFO
         - SYNCHRONIZED
       Use the value 20 to indicate a project parameter
       Use the value 30 to indicate a package parameter
    */
    EXEC [SSISDB].[catalog].[set_execution_parameter_value]
      @execution_id,
      @object_type = 20,
      @parameter_name = N'RUN_DATE',
      @parameter_value = @run_date

    /* Decode the Logging Level */
    SELECT @logging_level = CASE WHEN UPPER(@LoggingLevel) = 'BASIC'       THEN 1
                                 WHEN UPPER(@LoggingLevel) = 'PERFORMANCE' THEN 2
                                 WHEN UPPER(@LoggingLevel) = 'VERBOSE'     THEN 3
                                 ELSE 0                      /* 'None' */
                            END

    EXEC [SSISDB].[catalog].[set_execution_parameter_value]
      @execution_id,
      @object_type = 50,
      @parameter_name = N'LOGGING_LEVEL',
      @parameter_value = @logging_level

    /* Call the catalog.start_execution (self-explanatory) */
    EXEC [SSISDB].[catalog].[start_execution]
     @execution_id

    /* Loop while the package is running
       The possible status values are :
         - created (1)
         - running (2)
         - canceled (3)
         - failed (4)
         - pending (5)
         - ended unexpectedly (6)
         - succeeded (7)
         - stopping (8)
         - completed (9)
       https://msdn.microsoft.com/en-us/library/ff878089.aspx
    */
    WHILE @execution_status NOT IN (3, 4, 6, 7, 9)
    BEGIN
        --Pause for 1 second.
        WAITFOR DELAY '00:00:05'

        --Refresh the status.
        SET @execution_status = (SELECT [executions].[status] FROM [SSISDB].[catalog].[executions] WHERE [executions].[execution_id] = @execution_id)
    END

    /* Populate parameters for OUTPUT */
    --SET @ExecutionID = @execution_id
    --SET @ExecutionStatus = @execution_status

    SELECT @execution_id, @execution_status, CASE WHEN @execution_status = 7 THEN 0 ELSE 1 END
END
GO
PRINT ' - custom.execute_ssis_package created.' 
GO
