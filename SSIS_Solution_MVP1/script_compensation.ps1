Clear-Host;

Get-ChildItem -Filter MTPSID001*.csv |
%{
    $header_date = (Get-Content $_ -First 1).substring(8,8);
    $NewFileName = "NEW_MTPSID001_" + $header_date + ".csv";
    $FullPath = Join-Path $_.DirectoryName $NewFileName
    $streamWr = New-Object System.IO.StreamWriter $FullPath, $true, ([System.Text.Encoding]::GetEncoding(1252))
    try {
        Get-Content $_  | Select-Object -Skip 1 |
        %{
            $streamWr.WriteLine($_ + ' ' * (770 - ($_.Length +1)));
         }
     }
	 catch {
		 Write-Host "impossible d'ecrire";
	 }
	finally {
		$streamWr.Close();
	}
     
     $streamWr.Close();
          
}
