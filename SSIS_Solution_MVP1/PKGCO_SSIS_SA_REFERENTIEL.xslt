<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:csv="csv:csv">
	<xsl:output method="text" encoding="UTF-8"/>
	<xsl:strip-space elements="*"/>  

	<xsl:param name="FIELD_DELIMITER" select="'|'"/>
	<xsl:param name="FILLER_ZDWH0010" select="'|||||||||||||||||||'"/>   
	<xsl:param name="FILLER_ZDWH0020" select="'|||||||||||||||||||'"/>
	<xsl:param name="FILLER_ZDWH0030" select="'|||||||||||||||||||'"/>
	<xsl:param name="FILLER_ZDWH0040" select="'||||||||||||||||||'"/>
	<xsl:param name="FILLER_ZDWH0050" select="'||||||||||||||||||'"/> 
  <xsl:param name="FILLER_ZDWH0060" select="'|||||||||||||'"/>
  <xsl:param name="FILLER_ZDWH0070" select="'|||||||||||||||||||'"/>
  <xsl:param name="FILLER_ZDWH0080" select="'||||||||||||||||||'"/>
  <xsl:param name="FILLER_ZDWH0090" select="'||||||||||||||||||||'"/>
  <xsl:param name="FILLER_ZDWH0100" select="'||||||||||||||||||'"/>
  <xsl:param name="FILLER_ZDWH0120" select="'|||||||||||||||||||'"/>
  <xsl:param name="FILLER_ZDWH0140" select="'|||||||||||||||||||'"/>
  <xsl:param name="FILLER_ZDWH0150" select="'|||||||||||||||||||'"/>
  <xsl:param name="FILLER_ZDWH0170" select="'|||||||||||||||||||'"/>
  <xsl:param name="FILLER_ZDWH0180" select="'|||||||||||||||||||'"/>
  <xsl:param name="FILLER_ZDWH0200" select="'|||||||||||||||||||'"/>
  <xsl:param name="FILLER_ZDWH0210" select="'||||||||||||||||||||'"/>
  <xsl:param name="FILLER_ZDWH0230" select="'|||||||||||||||||||'"/>
  <xsl:param name="FILLER_ZDWH0240" select="'|||||||||||||||||||'"/>
  <xsl:param name="FILLER_ZDWH0320" select="'|||||||||||||||||||'"/>
  <xsl:param name="FILLER_ZDWH0440" select="'|||||||||||||||||||'"/>
  <xsl:param name="FILLER_ZDWH0580" select="'||||||||||||||||||'"/>
  <xsl:param name="FILLER_ZDWH0790" select="'||||||||||||||||||||'"/>
  <xsl:param name="FILLER_ZDWH1210" select="'||||||||||||||||||||'"/>
  <xsl:param name="FILLER_ZDWH2350" select="'||||||||||||||||||||'"/>
  <xsl:param name="FILLER_ZDWH3060" select="'||||||||||||||||||||'"/>
  <xsl:param name="FILLER_ZDWHETB0" select="'|||||||||||||||||||||'"/>
  <xsl:param name="FILLER_ZDWHGAG0" select="'||||||||||||||||||||'"/>
  <xsl:param name="FILLER_ZDWHAGE0" select="'|||||||||||||||||'"/>
  <xsl:param name="FILLER_ZDWHSER0" select="'|||||||||||||||||||'"/>
  <xsl:param name="FILLER_ZDWHSES0" select="'||||||||||||||||||'"/>
  <xsl:param name="FILLER_ZDWHTAU0" select="'|||'"/>
  <xsl:param name="FILLER_ZDWHDEV0" select="'|||||||||||||||'"/>
  <xsl:param name="FILLER_ZPLAN0" select="'|||||||||||||'"/>
  <xsl:param name="ROW_DELIMITER" select="'&#13;&#10;'"/>

  <xsl:template match="/Referentiel">
		<xsl:apply-templates select="Table[@donnees='Codes qualité']/Qualite"/>
		<xsl:apply-templates select="Table[@donnees='Codes APE']/APE"/>
		<xsl:apply-templates select="Table[@donnees='Options de débit']/OptionsDebit"/>
<!--
    <xsl:apply-templates select="Table[@donnees='Secteurs géographiques']/XXX"/>
-->
    <xsl:apply-templates select="Table[@donnees='Codes état clients']/EtatClient"/>
    <xsl:apply-templates select="Table[@donnees='Responsables commerciaux']/ResponsableCommercial"/>
		<xsl:apply-templates select="Table[@donnees='Cotations internes']/CotationInterne"/>
    <xsl:apply-templates select="Table[@donnees='Catégories clients']/CategorieClient"/>
    <xsl:apply-templates select="Table[@donnees='Entrées en litigieux']/EntreeEnLitigieux"/>
    <xsl:apply-templates select="Table[@donnees='Segments']/Segment"/>
    <xsl:apply-templates select="Table[@donnees='Liens']/Lien"/>
    <xsl:apply-templates select="Table[@donnees='Types de compte']/TypeCompte"/>
    <xsl:apply-templates select="Table[@donnees='Prestations']/Prestation"/>
    <xsl:apply-templates select="Table[@donnees='Utilisations']/Utilisation"/>
    <xsl:apply-templates select="Table[@donnees='Services conventions']/ServiceConvention"/>
    <xsl:apply-templates select="Table[@donnees='Conventions']/Convention"/>
    <xsl:apply-templates select="Table[@donnees='Motifs de résiliation']/MotifResiliation"/>
    <xsl:apply-templates select="Table[@donnees='Opérations']/Operation"/>
    <xsl:apply-templates select="Table[@donnees='Types de porteur CB']/TypesPorteurCB"/>
    <xsl:apply-templates select="Table[@donnees='Codes résidents']/Resident"/>
    <xsl:apply-templates select="Table[@donnees='Commissions']/Commission"/>
    <xsl:apply-templates select="Table[@donnees='Natures opérations']/NatureOperation"/>
    <xsl:apply-templates select="Table[@donnees='Codes analytiques']/CodeAnalytique"/>
    <xsl:apply-templates select="Table[@donnees='Catégories professionnelles']/CategorieProfessionnelle"/>
    <xsl:apply-templates select="Table[@donnees='Professions']/Profession"/>
    <xsl:apply-templates select="Table[@donnees='Motifs de clôture']/MotifCloture"/>
    <xsl:apply-templates select="Table[@donnees='Établissements']/Etablissement"/>
    <xsl:apply-templates select="Table[@donnees='Groupes agence']/GroupeAgence"/>
    <xsl:apply-templates select="Table[@donnees='Agences']/Agence"/>
    <xsl:apply-templates select="Table[@donnees='Services']/Service"/>
    <xsl:apply-templates select="Table[@donnees='Sous-Services']/SousService"/>
    <xsl:apply-templates select="Table[@donnees='Historique mensuel taux']/Taux"/>
    <xsl:apply-templates select="Table[@donnees='Historique mensuel devises']/Devise"/>
    <xsl:apply-templates select="Table[@donnees='Plan de comptes']/PlanCompte"/>
  </xsl:template>

	<xsl:template match="Table[@donnees='Codes qualité']/Qualite">
		<xsl:value-of select="'ZDWH0010'"/><xsl:value-of select="$FIELD_DELIMITER"/>
		<xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
		<xsl:value-of select="Identifiant"/><xsl:value-of select="$FIELD_DELIMITER"/>
		<xsl:value-of select="CodeQualite"/><xsl:value-of select="$FIELD_DELIMITER"/>
		<xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
		<xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
		<xsl:value-of select="$FILLER_ZDWH0010"/>
		<xsl:value-of select="$ROW_DELIMITER"/>
	</xsl:template>

	<xsl:template match="Table[@donnees='Codes APE']/APE">
		<xsl:value-of select="'ZDWH0020'"/><xsl:value-of select="$FIELD_DELIMITER"/>
		<xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
		<xsl:value-of select="Identifiant"/><xsl:value-of select="$FIELD_DELIMITER"/>
		<xsl:value-of select="CodeAPE"/><xsl:value-of select="$FIELD_DELIMITER"/>
		<xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
		<xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
		<xsl:value-of select="$FILLER_ZDWH0020"/>
		<xsl:value-of select="$ROW_DELIMITER"/>
	</xsl:template>

	<xsl:template match="Table[@donnees='Options de débit']/OptionsDebit">
		<xsl:value-of select="'ZDWH0030'"/><xsl:value-of select="$FIELD_DELIMITER"/>
		<xsl:value-of select="DateAnalyse"/><xsl:value-of select="$FIELD_DELIMITER"/>
		<xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
		<xsl:value-of select="TypeDebit"/><xsl:value-of select="$FIELD_DELIMITER"/>
		<xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
		<xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
		<xsl:value-of select="$FILLER_ZDWH0030"/>
		<xsl:value-of select="$ROW_DELIMITER"/>
	</xsl:template>

<!--
	<xsl:template match="Table[@donnees='Secteurs géographiques']/XXX">
		<xsl:value-of select="'ZDWH0040'"/><xsl:value-of select="$FIELD_DELIMITER"/>
		<xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
		<xsl:value-of select="Identifiant"/><xsl:value-of select="$FIELD_DELIMITER"/>
		<xsl:value-of select="XXX"/><xsl:value-of select="$FIELD_DELIMITER"/>
		<xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
		<xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
		<xsl:value-of select="$FILLER_ZDWH0040"/>
		<xsl:value-of select="$ROW_DELIMITER"/>
	</xsl:template>
-->
  
  <xsl:template match="Table[@donnees='Codes état clients']/EtatClient">
    <xsl:value-of select="'ZDWH0050'"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Identifiant"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeEtat"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Libelle2"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="$FILLER_ZDWH0050"/>
    <xsl:value-of select="$ROW_DELIMITER"/>
  </xsl:template>

  <xsl:template match="Table[@donnees='Responsables commerciaux']/ResponsableCommercial">
    <xsl:value-of select="'ZDWH0060'"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="DateAnalyse"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Identifiant"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeResponsable"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeAgence"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeGroupeAgence"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeUtilisateur"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Profil"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="ResponsableHierarchique"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="$FILLER_ZDWH0060"/>
    <xsl:value-of select="$ROW_DELIMITER"/>
  </xsl:template>
 
  <xsl:template match="Table[@donnees='Cotations internes']/CotationInterne">
		<xsl:value-of select="'ZDWH0070'"/><xsl:value-of select="$FIELD_DELIMITER"/>
		<xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
		<xsl:value-of select="Identifiant"/><xsl:value-of select="$FIELD_DELIMITER"/>
		<xsl:value-of select="Cotation"/><xsl:value-of select="$FIELD_DELIMITER"/>
		<xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
		<xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
		<xsl:value-of select="$FILLER_ZDWH0070"/>
		<xsl:value-of select="$ROW_DELIMITER"/>
	</xsl:template>



  <xsl:template match="Table[@donnees='Catégories clients']/CategorieClient">
    <xsl:value-of select="'ZDWH0080'"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Identifiant"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeCategorieClient"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Type"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="$FILLER_ZDWH0080"/>
    <xsl:value-of select="$ROW_DELIMITER"/>
  </xsl:template>




  <xsl:template match="Table[@donnees='Entrées en litigieux']/EntreeEnLitigieux">
    <xsl:value-of select="'ZDWH0090'"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeMotif"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="$FILLER_ZDWH0090"/>
    <xsl:value-of select="$ROW_DELIMITER"/>
  </xsl:template>



  <xsl:template match="Table[@donnees='Segments']/Segment">
    <xsl:value-of select="'ZDWH0100'"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeSegment"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodePropagation"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Priorite"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodePotentiel"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="$FILLER_ZDWH0100"/>
    <xsl:value-of select="$ROW_DELIMITER"/>
  </xsl:template>




 <xsl:template match="Table[@donnees='Liens']/Lien">
    <xsl:value-of select="'ZDWH0120'"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Identifiant"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeLien"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="$FILLER_ZDWH0120"/>
    <xsl:value-of select="$ROW_DELIMITER"/>
  </xsl:template>





 <xsl:template match="Table[@donnees='Types de compte']/TypeCompte">
    <xsl:value-of select="'ZDWH0140'"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeProduit"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="TypeEpargne"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="$FILLER_ZDWH0140"/>
    <xsl:value-of select="$ROW_DELIMITER"/>
  </xsl:template>




 <xsl:template match="Table[@donnees='Prestations']/Prestation">
    <xsl:value-of select="'ZDWH0150'"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodePrestation"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="NumeroSequence"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="$FILLER_ZDWH0150"/>
    <xsl:value-of select="$ROW_DELIMITER"/>
  </xsl:template>



  <xsl:template match="Table[@donnees='Utilisations']/Utilisation">
    <xsl:value-of select="'ZDWH0170'"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="DateAnalyse"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="TypeUtilisation"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="$FILLER_ZDWH0170"/>
    <xsl:value-of select="$ROW_DELIMITER"/>
  </xsl:template>




  <xsl:template match="Table[@donnees='Services conventions']/ServiceConvention">
    <xsl:value-of select="'ZDWH0180'"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeService"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="NumeroSequence"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="$FILLER_ZDWH0180"/>
    <xsl:value-of select="$ROW_DELIMITER"/>
  </xsl:template>






 <xsl:template match="Table[@donnees='Conventions']/Convention">
    <xsl:value-of select="'ZDWH0200'"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="DateAnalyse"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeConvention"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="$FILLER_ZDWH0200"/>
    <xsl:value-of select="$ROW_DELIMITER"/>
  </xsl:template>






  <xsl:template match="Table[@donnees='Motifs de résiliation']/MotifResiliation">
    <xsl:value-of select="'ZDWH0210'"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeMotif"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="$FILLER_ZDWH0210"/>
    <xsl:value-of select="$ROW_DELIMITER"/>
  </xsl:template>


 <xsl:template match="Table[@donnees='Opérations']/Operation">
    <xsl:value-of select="'ZDWH0230'"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeOperation"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeApplication"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="$FILLER_ZDWH0230"/>
    <xsl:value-of select="$ROW_DELIMITER"/>
  </xsl:template>


 <xsl:template match="Table[@donnees='Types de porteur CB']/TypesPorteurCB">
    <xsl:value-of select="'ZDWH0240'"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="DateAnalyse"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="TypePorteur"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="$FILLER_ZDWH0240"/>
    <xsl:value-of select="$ROW_DELIMITER"/>
 </xsl:template>




   <xsl:template match="Table[@donnees='Codes résidents']/Resident">
    <xsl:value-of select="'ZDWH0320'"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Identifiant"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeResident"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="$FILLER_ZDWH0320"/>
    <xsl:value-of select="$ROW_DELIMITER"/>
  </xsl:template>


  <xsl:template match="Table[@donnees='Commissions']/Commission">
    <xsl:value-of select="'ZDWH0440'"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeCommission"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Application"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="$FILLER_ZDWH0440"/>
    <xsl:value-of select="$ROW_DELIMITER"/>
  </xsl:template>
  
  
  
  <xsl:template match="Table[@donnees='Natures opérations']/NatureOperation">
    <xsl:value-of select="'ZDWH0580'"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeOperation"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeNature"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
   <xsl:value-of select="SureteGarantie"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="$FILLER_ZDWH0580"/>
    <xsl:value-of select="$ROW_DELIMITER"/>
  </xsl:template>
  
  
 <xsl:template match="Table[@donnees='Codes analytiques']/CodeAnalytique">
    <xsl:value-of select="'ZDWH0790'"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Code"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="$FILLER_ZDWH0790"/>
    <xsl:value-of select="$ROW_DELIMITER"/>
  </xsl:template>




 <xsl:template match="Table[@donnees='Catégories professionnelles']/CategorieProfessionnelle">
    <xsl:value-of select="'ZDWH1210'"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeCategorie"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="$FILLER_ZDWH1210"/>
    <xsl:value-of select="$ROW_DELIMITER"/>
  </xsl:template>

  


 <xsl:template match="Table[@donnees='Professions']/Profession">
    <xsl:value-of select="'ZDWH2350'"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeProfession"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="$FILLER_ZDWH2350"/>
    <xsl:value-of select="$ROW_DELIMITER"/>
  </xsl:template>



 <xsl:template match="Table[@donnees='Motifs de clôture']/MotifCloture">
    <xsl:value-of select="'ZDWH3060'"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeMotif"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="$FILLER_ZDWH3060"/>
    <xsl:value-of select="$ROW_DELIMITER"/>
  </xsl:template>



 <xsl:template match="Table[@donnees='Établissements']/Etablissement">
    <xsl:value-of select="'ZDWHETB0'"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="$FILLER_ZDWHETB0"/>
    <xsl:value-of select="$ROW_DELIMITER"/>
  </xsl:template>



  <xsl:template match="Table[@donnees='Groupes agence']/GroupeAgence">
    <xsl:value-of select="'ZDWHGAG0'"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeGroupeAgence"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="$FILLER_ZDWHGAG0"/>
    <xsl:value-of select="$ROW_DELIMITER"/>
  </xsl:template>



  <xsl:template match="Table[@donnees='Agences']/Agence">
    <xsl:value-of select="'ZDWHAGE0'"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeAgence"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeGroupeAgence"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeBanque"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeGuichet"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="$FILLER_ZDWHAGE0"/>
    <xsl:value-of select="$ROW_DELIMITER"/>
  </xsl:template>
  


 <xsl:template match="Table[@donnees='Services']/Service">
    <xsl:value-of select="'ZDWHSER0'"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeAgence"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeService"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="$FILLER_ZDWHSER0"/>
    <xsl:value-of select="$ROW_DELIMITER"/>
  </xsl:template>



 <xsl:template match="Table[@donnees='Sous-Services']/SousService">
    <xsl:value-of select="'ZDWHSES0'"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeAgence"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeService"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeSousService"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="$FILLER_ZDWHSES0"/>
    <xsl:value-of select="$ROW_DELIMITER"/>
  </xsl:template>



 <xsl:template match="Table[@donnees='Historique mensuel taux']/Taux">
    <xsl:value-of select="'ZDWHTAU0'"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="DateAnalyse"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeTaux"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeDevise"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="TypeTaux"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="PeriodiciteTaux"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="SigneSensDelai"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="DelaiUsance"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="TypePeriode"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="TypeJours"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="TypeCalcul"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Arrondi"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="TypeArrondi"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="DeviseCotation"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="DebutValidite"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="FinValidite"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="TauxSubstitution"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="DeviseTauxSubstitution"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="ValeurTaux"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="$FILLER_ZDWHTAU0"/>
    <xsl:value-of select="$ROW_DELIMITER"/>
  </xsl:template>



 <xsl:template match="Table[@donnees='Historique mensuel devises']/Devise">
    <xsl:value-of select="'ZDWHDEV0'"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="DateExtraction"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeDevise"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Abrege"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Libelle"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="AvecCentimes"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="BaseCalcul"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="UniteCotation"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="ValeurFixing"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="$FILLER_ZDWHDEV0"/>
    <xsl:value-of select="$ROW_DELIMITER"/>
  </xsl:template>




 <xsl:template match="Table[@donnees='Plan de comptes']/PlanCompte">
    <xsl:value-of select="'ZPLAN0'"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Etablissement"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="NumeroPlan"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CompteObligatoire"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Intitule"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CodeProduit"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="ClasseSecurite"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="Sens"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="GestionDepassement"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CompteTiers"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="CompteDeClientele"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="IntituleExtraitCompte"/><xsl:value-of select="$FIELD_DELIMITER"/>
    <xsl:value-of select="$FILLER_ZPLAN0"/>
    <xsl:value-of select="$ROW_DELIMITER"/>
  </xsl:template>

</xsl:stylesheet>