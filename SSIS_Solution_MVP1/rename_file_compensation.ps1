Get-ChildItem -Filter MTPSID001*.csv |
%{
    $header_date = (Get-Content $_ -First 1).substring(8,8);
    $NewFileName = "X1_MTPSID001_" + $header_date + ".csv";
    $FileContent = Get-Content -Path $_.FullName;
    $_.FullName;
    Set-Content -Path (Join-Path $_.DirectoryName $NewFileName) -Value $FileContent;
   
}