﻿Get-ChildItem -Filter ORANGE_BANK_-_Reporting_du*.csv | %{

$newFileName = $_.FullName -replace "ORANGE_BANK" ,"X1_ORANGE_BANK";
Get-Content $_.FullName | Where-Object {$_ -match 'ORANGE BANK'} | Set-Content $newFileName;

}