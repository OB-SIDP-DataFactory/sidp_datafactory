@ECHO OFF
SETLOCAL ENABLEEXTENSIONS
SETLOCAL ENABLEDELAYEDEXPANSION

rem # --------------------------------------------------
rem # Nom du script + repertoire
rem # --------------------------------------------------
FOR %%i IN (%0) DO SET BASEDIR_Dollar0=%%~dpi
FOR %%i IN (%0) DO SET BASENAME_Dollar0=%%~ni

SET ERROR_VAL=0

rem # --------------------------------------------------
rem # Env variables
rem # --------------------------------------------------
FOR /F "delims=" %%A IN (!BASEDIR_Dollar0!\CFGRUN_SSIS.cfg) DO SET "%%A"

rem # --------------------------------------------------
rem # Parameters
rem # --------------------------------------------------
SET ENV_SSIS_PACKAGE=%~1

rem # --------------------------------------------------
rem # Executing package
rem # --------------------------------------------------

SET SQLCMD_OUTPUT=!BASEDIR_Dollar0!\!BASENAME_Dollar0!_!ENV_SSIS_PACKAGE!.out
DEL !SQLCMD_OUTPUT! > nul 2>&1

ECHO SQLCMD -S "!ENV_SSIS_SERVER!" -E -d "!ENV_SSIS_CATALOG!" -Q "EXECUTE [custom].[execute_ssis_package] @FolderName='$(folder_name)', @ProjectName='$(project_name)', @PackageName='$(package_name)', @EnvironmentName='$(environment_name)', @LoggingLevel = 'Basic', @Use32BitRunTime=1" -v folder_name="!ENV_SSIS_FOLDER!" -v project_name="!ENV_SSIS_PROJECT!" -v package_name="!ENV_SSIS_PACKAGE!.dtsx" -v environment_name="!ENV_SSIS_ENVIRONMENT!" -s ";" -W -h-1 -o "!SQLCMD_OUTPUT!"
ECHO Started: %time%

SQLCMD -S "!ENV_SSIS_SERVER!" -E -d "!ENV_SSIS_CATALOG!" -Q "EXECUTE [custom].[execute_ssis_package] @FolderName='$(folder_name)', @ProjectName='$(project_name)', @PackageName='$(package_name)', @EnvironmentName='$(environment_name)', @LoggingLevel = 'Basic', @Use32BitRunTime=1" -v folder_name="!ENV_SSIS_FOLDER!" -v project_name="!ENV_SSIS_PROJECT!" -v package_name="!ENV_SSIS_PACKAGE!.dtsx" -v environment_name="!ENV_SSIS_ENVIRONMENT!" -s ";" -W -h-1 -o "!SQLCMD_OUTPUT!"
SET ERROR_VAL=!ERRORLEVEL!

ECHO Finished: %time%

FOR /F "delims=" %%A IN (!SQLCMD_OUTPUT!) DO (
    SET CURRENT_OUTPUT_LINE=%%A
    SET /A CURRENT_OUTPUT_LINE_CTRL=!CURRENT_OUTPUT_LINE:;=! > nul 2>&1

    IF "!CURRENT_OUTPUT_LINE_CTRL!" == "0" (
        ECHO !CURRENT_OUTPUT_LINE!
    )

    IF NOT "!CURRENT_OUTPUT_LINE_CTRL!" == "0" (
        FOR /F "tokens=1,2,3 delims=;" %%A IN ('ECHO !CURRENT_OUTPUT_LINE!') DO (
            SET ExecutionID=%%A
            SET ExecutionStatus=%%B
            SET ReturnCode=%%C
        )
    )
)

DEL !SQLCMD_OUTPUT! > nul 2>&1

IF "!ReturnCode!" == "0" (
    ECHO Execution ID: !ExecutionID!.
    ECHO Execution Status: !ExecutionStatus!.
    ECHO To view the details for the execution, right-click on the Integration Services Catalog, and open the [All Executions] report
)

IF "!ReturnCode!" == "1" (
    SET SSIS_ERRORS=!BASEDIR_Dollar0!\!BASENAME_Dollar0!_!ENV_SSIS_PACKAGE!.err
    DEL !SSIS_ERRORS! > nul 2>&1

    ECHO SQLCMD -S "!ENV_SSIS_SERVER!" -E -d "!ENV_SSIS_CATALOG!" -Q "EXECUTE [custom].[get_ssis_execution_errors] @ExecutionId = $(execution_id)" -v execution_id="!ExecutionID!" -s ";" -W -h-1 -o "!SSIS_ERRORS!"    
    SQLCMD -S "!ENV_SSIS_SERVER!" -E -d "!ENV_SSIS_CATALOG!" -Q "EXECUTE [custom].[get_ssis_execution_errors] @ExecutionId = $(execution_id)" -v execution_id="!ExecutionID!" -s ";" -W -h-1 -o "!SSIS_ERRORS!"
    
    ECHO Execution ID: !ExecutionID!.
    ECHO Execution Status: !ExecutionStatus!.
    TYPE !SSIS_ERRORS! 2>nul
    ECHO To view the details for the execution, right-click on the Integration Services Catalog, and open the [All Executions] report

    DEL !SSIS_ERRORS! > nul 2>&1
)

IF "!ReturnCode!" == "" (
    SET ReturnCode=1
)

ECHO ReturnCode: !ReturnCode!
EXIT /B !ReturnCode!
