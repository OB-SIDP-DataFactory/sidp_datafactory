#-------------------------------------------------------------------------------------------------#
# Script parameters
#-------------------------------------------------------------------------------------------------#
[CmdletBinding()]
param (
    [Parameter(Mandatory = $true)]
    [String]$ProcessConfPath = $null,
    [Parameter(Mandatory = $true)]
    [String]$ProcessId = $null,
    [Parameter(Mandatory = $true)]
    [String]$LastModifiedDateMin = $null,
    [Parameter(Mandatory = $true)]
    [String]$LastModifiedDateMax = $null
)

#-------------------------------------------------------------------------------------------------#
# Function Write-Log
#-------------------------------------------------------------------------------------------------#
Function Write-Log
{
    Param ( [string]$logOutput )
    Write-Output $logOutput
    Add-content $logFile -value $logOutput
}

#-------------------------------------------------------------------------------------------------#
# Variables
#-------------------------------------------------------------------------------------------------#
$scriptHome = Split-Path $MyInvocation.MyCommand.Path
$scriptName = $MyInvocation.MyCommand.Name
$scriptName = $scriptName.Substring(0, ($scriptName.Length - 4))

$currentLogDate = Get-Date -format yyyyMMddHHmmss
$logFile = "$($scriptHome)\$($scriptName)_$($currentLogDate).log"

#-------------------------------------------------------------------------------------------------#
# Start script
#-------------------------------------------------------------------------------------------------#
Write-Log "Starting $($scriptName) ..."
Write-Log "Log available in $($logFile)"

#-------------------------------------------------------------------------------------------------#
# Start
#-------------------------------------------------------------------------------------------------#
try
{
    $ProcessConfXml = [xml] (Get-Content "$($ProcessConfPath)")

    Write-Log "  Retrieving SOQL query for `"$($ProcessId)`""
    $sfdcExtractionSOQLNode = Select-Xml -Xml $ProcessConfXml -XPath "/beans/bean[@id='$($ProcessId)']/property[@name='configOverrideMap']/map/entry[@key='sfdc.extractionSOQL']" | Select-Object -ExpandProperty Node

    $sfdcExtractionSOQL = $sfdcExtractionSOQLNode.GetAttribute("value") -replace "LastModifiedDate > [^ ]*", "LastModifiedDate > $($LastModifiedDateMin)" -replace "LastModifiedDate <= [^ ]*", "LastModifiedDate <= $($LastModifiedDateMax)" -replace "CreatedDate > [^ ]*", "CreatedDate > $($LastModifiedDateMin)" -replace "CreatedDate <= [^ ]*", "CreatedDate <= $($LastModifiedDateMax)"

    Write-Log "  Updating SOQL query for `"$($ProcessId)`" --> `"$($sfdcExtractionSOQL)`""
    $sfdcExtractionSOQLNode.SetAttribute("value", "$($sfdcExtractionSOQL)")

    $ProcessConfXml.Save("$($ProcessConfPath)")
}
catch
{
    Write-Log "Error : $($Error[0].exception.GetBaseException().Message)"
    exit 1
}
