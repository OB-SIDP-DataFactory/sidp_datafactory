﻿/* Select the SSISDB database */
USE SSISDB
GO
/* Create a parameter (variable) named @SQL */
DECLARE @SQL VARCHAR(2000)

/* Create the custom schema if it does not already exist */
PRINT 'custom Schema'
IF NOT EXISTS(SELECT NAME FROM SYS.SCHEMAS WHERE NAME = 'custom')
BEGIN
    /* Create Schema statements must occur first in a batch */
    PRINT ' - Creating custom schema'
    SET @SQL = 'CREATE SCHEMA custom'
    EXEC(@SQL)
    PRINT ' - custom schema created'
END
ELSE
BEGIN
    PRINT ' - custom Schema already exists.'
END

/* Drop the custom.get_ssis_execution_errors Stored Procedure if it already exists */
PRINT 'custom.get_ssis_execution_errors Stored Procedure'
IF EXISTS( SELECT S.NAME + '.' +  P.NAME
             FROM SYS.PROCEDURES P
             JOIN SYS.SCHEMAS S ON S.SCHEMA_ID = P.SCHEMA_ID
            WHERE S.NAME = 'custom'
              AND P.NAME = 'get_ssis_execution_errors')
BEGIN
    PRINT ' - Dropping custom.get_ssis_execution_errors'
    DROP PROCEDURE custom.get_ssis_execution_errors
    PRINT ' - custom.get_ssis_execution_errors dropped'
END

/* Create the custom.get_ssis_execution_errors Stored Procedure */
PRINT ' - Creating custom.get_ssis_execution_errors'
GO
CREATE PROCEDURE custom.get_ssis_execution_errors
  @ExecutionId bigint
AS
BEGIN
    SET NOCOUNT ON
    SET IMPLICIT_TRANSACTIONS OFF

    SELECT [message]
      FROM [catalog].[event_messages]
     WHERE [operation_id] = @ExecutionId AND [event_name] IN ('OnWarning', 'OnError', 'OnTaskFailed')
    ORDER BY [message_time]
END
GO
PRINT ' - custom.get_ssis_execution_errors created.' 
GO
