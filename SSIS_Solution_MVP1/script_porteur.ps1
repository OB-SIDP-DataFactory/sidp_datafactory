Clear-Host;
Get-ChildItem -Filter MTPSID002*.csv |
%{
	$header_date = (Get-Content $_ -First 1).substring(25,8);
    $NewFileName = "NEW_MTPSID002_" + $header_date + ".csv";
    $FileContent = "";

    Get-Content $_ -ReadCount 1  | Select -Skip 2 |
    %{
        $Line = $_;
        $LineLength = $_.Length;
        if($LineLength -eq 1839){
            $FileContent += $Line;
            $FileContent += "`r`n";

        }
        else {
            if($LineLength_prec -eq 1839){
                $FileContent += $Line;
                $FileContent += "`r`n";
            }
            else {
                $FileContent = $FileContent.Trim();
                #$FileContent += " ";
                $FileContent += " " * (1839 - $LineLength - $LineLength_prec)    
                $FileContent += $Line;
                $FileContent += "`r`n";
                }
        }

        $LineLength_prec = $LineLength



     }
     Set-Content -Path "$NewFileName" -Value $FileContent;
}
