@ECHO OFF

SETLOCAL ENABLEEXTENSIONS
SETLOCAL ENABLEDELAYEDEXPANSION

rem # --------------------------------------------------
rem # Nom du script + repertoire
rem # --------------------------------------------------
FOR %%i IN (%0) DO SET BASEDIR_Dollar0=%%~dpi
FOR %%i IN (%0) DO SET BASENAME_Dollar0=%%~ni

rem # --------------------------------------------------
rem # Env variables
rem # --------------------------------------------------
FOR /F "delims=" %%A IN (!BASEDIR_Dollar0!\CFGRUN_SSIS.cfg) DO SET "%%A"
SET racine_log="!REP_LOG!"
SET rep_log=%racine_log%"\DataCollected\OAV\"
SET date_heure=%date:~6,4%%date:~3,2%%date:~0,2%_%time:~0,2%%time:~3,2%%time:~6,2%
SET date_heure=%date_heure: =0%
SET fichier_log=%rep_log%OAV_DATAHUB_COLLECT_%date_heure%.log 
echo "====================================================================================================" > %fichier_log%
echo [%date%-%time%] "=======================BEGIN=============================================="   >> %fichier_log%
echo "====================================================================================================" >> %fichier_log%
echo .
SQLCMD -S "!ENV_SQL_SERVER!"  -E -Q "use SIDP_DataHub; exec dbo.OAV_DATAHUB_ALIMENTATION" -fo:1252 >> %fichier_log%
echo .
echo "====================================================================================================" >> %fichier_log%
echo [%date%-%time%] "=======================FIN=============================================="   >> %fichier_log%
echo "====================================================================================================" >> %fichier_log%
