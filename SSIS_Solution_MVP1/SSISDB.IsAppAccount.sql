﻿/* Reporting Services Applicative Account needs to be granted acces to database to perform data loads */

/* Create the login if it does not exist */
USE [master]
GO
IF NOT EXISTS ( SELECT name FROM master.sys.server_principals WHERE name = '$(IsAppAccount)')
BEGIN
    CREATE LOGIN [$(IsAppAccount)] FROM WINDOWS WITH DEFAULT_DATABASE=[master]
END
GO
/* Create the user if it does not exist */
USE [SSISDB]
GO
IF NOT EXISTS ( SELECT name FROM sys.database_principals WHERE name = '$(IsAppAccount)')
BEGIN
    CREATE USER [$(IsAppAccount)] FOR LOGIN [$(IsAppAccount)] 
END
GO
/* Grant access to user */
ALTER USER [$(IsAppAccount)] WITH DEFAULT_SCHEMA=[catalog]
GO
ALTER ROLE [db_owner] ADD MEMBER [$(IsAppAccount)]
GO
ALTER ROLE [ssis_admin] ADD MEMBER [$(IsAppAccount)]
GO