﻿CREATE VIEW [bfr].[V_DIM_INDICATEUR]
AS
SELECT
	[I].[CODE_INDICATEUR],
	[I].[LIBELLE_INDICATEUR],
	[I].[CODE_FAMILLE_INDICATEUR],
	[I].[LIBELLE_FAMILLE_INDICATEUR]
FROM
	bfr.T_DIM_INDICATEUR	AS	I	WITH(NOLOCK)
GO