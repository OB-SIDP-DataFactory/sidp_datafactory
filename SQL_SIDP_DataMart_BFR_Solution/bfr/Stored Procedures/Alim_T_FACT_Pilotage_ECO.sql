﻿
-- =============================================
-- Author:		<Ali Fassahi>
-- Create date: <25/04/2017>
-- Description:	<Alimentation de la table de dimension T_FACT_PILOTAGE par les indicateurs Portefeuille Client>
-- =============================================
CREATE PROCEDURE [bfr].[Alim_T_FACT_Pilotage_ECO] 
AS
BEGIN

SET NOCOUNT ON;

DECLARE @CurrentDate DATE;

-- if target table is not empty, reload last month
IF EXISTS ( SELECT 1 FROM [bfr].[T_FACT_PILOTAGE_ECO] )
BEGIN
    SELECT @CurrentDate = MAX(DATE_ACTION) FROM [$(DataFactoryBFRDatabaseName)].[dbo].[BF_COMPTES];
    DELETE FROM [bfr].[T_FACT_PILOTAGE_ECO] WHERE CODE_DATE = EOMONTH(@CurrentDate, -1);
END

exec bfr.Alim_T_FACT_Pilotage_ECO_CLI @CurrentDate;

exec bfr.Alim_T_FACT_Pilotage_ECO_CC @CurrentDate;

exec bfr.Alim_T_FACT_Pilotage_ECO_CR @CurrentDate;

exec bfr.Alim_T_FACT_Pilotage_ECO_EB @CurrentDate;

exec bfr.Alim_T_FACT_Pilotage_ECO_CMP @CurrentDate;

exec bfr.Alim_T_FACT_Pilotage_ECO_ASS @CurrentDate;

END