﻿CREATE PROCEDURE [bfr].[Alim_T_DIM_Temps] 
AS
BEGIN
	SET NOCOUNT ON;

	TRUNCATE TABLE bfr.T_DIM_TEMPS;

	INSERT INTO
		bfr.T_DIM_TEMPS
	SELECT
		CONVERT(VARCHAR(10), [PK_ID]),
		CONVERT(DATE, [Date]),
		CONVERT(INT, [Year]),
		CONVERT(INT, [Month]),
		[MonthName]
	FROM
		[$(DataFactoryDatabaseName)].[dbo].[DIM_TEMPS]	WITH(NOLOCK)
END
GO