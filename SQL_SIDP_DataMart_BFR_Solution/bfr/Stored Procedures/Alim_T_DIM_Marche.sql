﻿CREATE PROCEDURE [bfr].[Alim_T_DIM_Marche] 
AS
BEGIN

	SET NOCOUNT ON;
 
	------------------------------///////////////// Step 1
	--insertion dans la table cible 

	truncate table bfr.T_DIM_MARCHE;

	INSERT INTO bfr.T_DIM_MARCHE
	(
		CODE_MARCHE_N4,
		LIBELLE_MARCHE_N4,
		CODE_MARCHE_N1,
		LIBELLE_MARCHE_N1,
		CODE_MARCHE_N2,
		LIBELLE_MARCHE_N2,
		CODE_MARCHE_N3,
		LIBELLE_MARCHE_N3
	)
	SELECT DISTINCT
		CONVERT(VARCHAR(100) ,[code_marche]),
		CONVERT(NVARCHAR(100) ,[libelle_marche]),
		CONVERT(VARCHAR(100) ,[code_marche_n1]),
		CONVERT(NVARCHAR(100) ,[libelle_marche_n1]),
		CONVERT(VARCHAR(100) ,[code_marche_n2]),
		CONVERT(NVARCHAR(100) ,[libelle_marche_n2]),
		CONVERT(VARCHAR(100) ,[code_marche_n3]),
		CONVERT(NVARCHAR(100) ,[libelle_marche_n3])
	FROM
		[$(DataHubDatabaseName)].[dbo].[REF_TRANSCO_MARCHE]	WITH(NOLOCK);

	INSERT INTO [bfr].[T_DIM_MARCHE]
	(
		CODE_MARCHE_N4,
		LIBELLE_MARCHE_N4
	)
	VALUES
	(
		'99999',
		'Valeur non renseignée'
	);
END