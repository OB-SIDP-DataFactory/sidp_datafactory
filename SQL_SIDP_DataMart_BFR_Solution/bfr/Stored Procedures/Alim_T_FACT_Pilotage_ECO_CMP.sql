﻿
CREATE PROCEDURE [bfr].[Alim_T_FACT_Pilotage_ECO_CMP] 
(
   @CurrentDate DATE
)
AS
BEGIN

SET NOCOUNT ON;

--with ind_cmp as (
--select f.[CODE_MARQUE]
--     , f.[CODE_MARCHE]
--     , f.[CODE_PRODUIT]
--     , f.[CODE_RESEAU]
--     , f.[CODE_DATE]
--     , case -- Valeur de la production brute de crédit
--            when ( p.[CODE_PRODUIT_N1] = 'N1CRE' and f.[CODE_INDICATEUR] = 'PCR03' ) then 'PCR30'
--			-- Nombre de la production brute de crédit
--            when ( p.[CODE_PRODUIT_N1] = 'N1CRE' and f.[CODE_INDICATEUR] = 'PCR01' ) then 'PCR31'
--            -- Nombre de crédit en stock
--            when ( p.[CODE_PRODUIT_N1] = 'N1CRE' and f.[CODE_INDICATEUR] = 'SCR01' ) or ( p.[CODE_PRODUIT_N1] = 'N1CAV' and f.[CODE_INDICATEUR] = 'SCC25' ) then 'SCR30'
--            -- Encours de crédit en stock
--            when ( p.[CODE_PRODUIT_N1] = 'N1CRE' and f.[CODE_INDICATEUR] = 'SCR03' ) or ( p.[CODE_PRODUIT_N1] = 'N1CAV' and f.[CODE_INDICATEUR] = 'SCC21' ) then 'SCR31'
--            -- Encours moyen de crédit en stock
--            when ( p.[CODE_PRODUIT_N1] = 'N1CRE' and f.[CODE_INDICATEUR] = 'SCR07' ) or ( p.[CODE_PRODUIT_N1] = 'N1CAV' and f.[CODE_INDICATEUR] = 'SCC23' ) then 'SCR32'
--            -- Somme des intérêts client du stock de crédit
--            when ( p.[CODE_PRODUIT_N1] = 'N1CRE' and f.[CODE_INDICATEUR] = 'SCR17' ) or ( p.[CODE_PRODUIT_N1] = 'N1CAV' and f.[CODE_INDICATEUR] = 'SCC27' ) then 'SCR33'
--            -- Production brute sur épargne bilancielle
--            when ( p.[CODE_PRODUIT_N1] = 'N1EPA' and f.[CODE_INDICATEUR] = 'PEB01' ) or ( p.[CODE_PRODUIT_N1] = 'N1CAV' and f.[CODE_INDICATEUR] = 'PCC16' ) then 'PEB30'
--            -- Valeur de la production brute sur épargne bilancielle
--            when ( p.[CODE_PRODUIT_N1] = 'N1EPA' and f.[CODE_INDICATEUR] = 'PEB03' ) or ( p.[CODE_PRODUIT_N1] = 'N1CAV' and f.[CODE_INDICATEUR] = 'PCC23' ) then 'PEB31'			
--            -- Nombre du stock sur épargne bilancielle
--            when ( p.[CODE_PRODUIT_N1] = 'N1EPA' and f.[CODE_INDICATEUR] = 'SEB01' ) or ( p.[CODE_PRODUIT_N1] = 'N1CAV' and f.[CODE_INDICATEUR] = 'SCC30' ) then 'SEB30'
--            -- Encours du stock sur épargne bilancielle
--            when ( p.[CODE_PRODUIT_N1] = 'N1EPA' and f.[CODE_INDICATEUR] = 'SEB03' ) or ( p.[CODE_PRODUIT_N1] = 'N1CAV' and f.[CODE_INDICATEUR] = 'SCC31' ) then 'SEB31'
--            -- Encours moyen du stock sur épargne bilancielle
--            when ( p.[CODE_PRODUIT_N1] = 'N1EPA' and f.[CODE_INDICATEUR] = 'SEB07' ) or ( p.[CODE_PRODUIT_N1] = 'N1CAV' and f.[CODE_INDICATEUR] = 'SCC32' ) then 'SEB32'
--            -- Somme des intérêts client du stock sur Epargne bilancielle
--            when ( p.[CODE_PRODUIT_N1] = 'N1EPA' and f.[CODE_INDICATEUR] = 'SEB13' ) or ( p.[CODE_PRODUIT_N1] = 'N1CAV' and f.[CODE_INDICATEUR] = 'SCC28' ) then 'SEB33'
--            else ''
--       end as [CODE_INDICATEUR]
--     , f.[VALEUR_INDICATEUR]
--     , getdate() as DATE_ACTION
--  from [bfr].[T_FACT_PILOTAGE_ECO] f
--  join [bfr].[T_DIM_PRODUIT] p
--    on f.CODE_PRODUIT = p.CODE_PRODUIT_N5
-- where @CurrentDate is null or f.CODE_DATE = EOMONTH(@CurrentDate, -1)
--)

--insert into [bfr].[T_FACT_PILOTAGE_ECO]
--select [CODE_MARQUE]
--     , [CODE_MARCHE]
--     , [CODE_PRODUIT]
--     , [CODE_RESEAU]
--     , [CODE_DATE]
--     , [CODE_INDICATEUR]
--     , [VALEUR_INDICATEUR]
--     , [DATE_ACTION]
--  from ind_cmp
-- where [CODE_INDICATEUR] <> ''

END
GO