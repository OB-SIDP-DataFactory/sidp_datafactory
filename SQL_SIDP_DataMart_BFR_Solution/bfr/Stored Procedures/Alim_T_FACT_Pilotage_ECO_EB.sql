﻿
CREATE PROCEDURE [bfr].[Alim_T_FACT_Pilotage_ECO_EB] 
(
   @CurrentDate DATE
)
AS
BEGIN

SET NOCOUNT ON;

---- préparation de la table temporaire incluant tous les comptes y compris les comptes joints
IF OBJECT_ID('tempdb..#COMPTES') IS NOT NULL
drop table #COMPTES;

--select *
--  into #COMPTES
--  from ( select co.NUMERO_CLIENT
--              , co.NUMERO_COMPTE
--              , co.DATE_OUVERTURE
--              , co.DATE_CLOTURE
--              , co.CODE_PRODUIT
--              , co.CODE_MARQUE
--              , co.CODE_MARCHE
--              , co.ACTIF
--			  , co.SOLDE_DATE_COMPTABLE
--              , co.DATE_ACTION
--           from [$(DataFactoryBFRDatabaseName)].dbo.BF_COMPTES co
--          where @CurrentDate is null 
--             or ( dateadd(month, datediff(month, 0, co.DATE_ACTION), 0) = dateadd(month, datediff(month, 0, @CurrentDate), 0) )
--) COMPTES
--;

--with ind_pvt as (
--select co.CODE_MARQUE as cd_mrq
--     , co.CODE_MARCHE as cd_mrc
--     , co.CODE_PRODUIT as cd_pdt
--     , '99999' as cd_rsx
--     , t.CODE_DATE as cd_dt
--       -- Interets client sur Epargne bilancielle
--     , sum( CASE WHEN dateadd(month, datediff(month, 0, co.DATE_CLOTURE), 0) = dateadd(month, datediff(month, 0, co.DATE_ACTION) - 1, 0) and
--                      p.CODE_PRODUIT_N1 = 'N1EPA'
--                 THEN -e.INTERETS_EPRGN_CREDITEURS_DEVISE_BASE-e.INTERETS_EPRGN_DEBITEURS_DEVISE_BASE ELSE 0 END ) as PEB18
--       -- Interets client du stock sur Epargne bilancielle
--     , sum( CASE WHEN ( co.DATE_CLOTURE is null OR co.DATE_CLOTURE >= dateadd(month, datediff(month, 0, co.DATE_ACTION), 0) ) and
--                      p.CODE_PRODUIT_N1 = 'N1EPA'
--                 THEN -e.INTERETS_EPRGN_CREDITEURS_DEVISE_BASE-e.INTERETS_EPRGN_DEBITEURS_DEVISE_BASE ELSE 0 END ) as SEB13
--	   -- Taux contractuel EPRGN x Valeur prod brute épargne bilancielle
--     , sum(CASE WHEN dateadd(month, datediff(month, 0, co.DATE_OUVERTURE), 0) = dateadd(month, datediff(month, 0, co.DATE_ACTION) - 1, 0) 
--				AND p.CODE_PRODUIT_N1 = 'N1EPA' 
--            THEN -e.TAUX_INTERET_CONTRACTUEL_EPRGN * co.SOLDE_DATE_COMPTABLE ELSE 0 END) as PEB20
--  from [$(DataFactoryBFRDatabaseName)].dbo.BF_EPARGNE e
--  left join #COMPTES co
--    on e.NUMERO_COMPTE = co.NUMERO_COMPTE and e.DATE_ACTION = co.DATE_ACTION
--  left join bfr.T_DIM_PRODUIT p
--    on co.CODE_PRODUIT = p.CODE_PRODUIT_N5
--  left join bfr.T_DIM_TEMPS t
--    on t.DATE_ANNEE_MOIS_JOUR = EOMONTH(e.DATE_ACTION, -1)
-- where ( @CurrentDate is null or ( dateadd(month, datediff(month, 0, e.DATE_ACTION), 0) = dateadd(month, datediff(month, 0, @CurrentDate), 0) ) )
--   and co.CODE_MARQUE is not null
--   and co.CODE_MARCHE is not null
--   and co.CODE_PRODUIT is not null
--group by co.CODE_MARQUE
--       , co.CODE_MARCHE
--       , co.CODE_PRODUIT
--       , t.CODE_DATE	
--),

----Unpivot the table.
--ind_unpvt as (
--SELECT cd_mrq, cd_mrc, cd_pdt, cd_rsx, cd_dt
--     , cd_ind, vl_ind
--  FROM ( SELECT cd_mrq, cd_mrc, cd_pdt, cd_rsx, cd_dt, 
--                cast(PEB18 as float) as PEB18, cast(SEB13 as float) as SEB13, cast(PEB20 as float) as PEB20
--           FROM ind_pvt ) p
--         UNPIVOT ( vl_ind FOR cd_ind IN (PEB18, SEB13, PEB20)
--       ) AS unpvt
--)

--insert into [bfr].[T_FACT_PILOTAGE_ECO]
--select *
--     , getdate() as dt_act
--  from ind_unpvt
-- where vl_ind <> 0

END
GO