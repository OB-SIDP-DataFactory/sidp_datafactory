﻿
CREATE PROCEDURE [bfr].[Alim_T_FACT_Pilotage_ECO_CC] 
(
   @CurrentDate DATE
)
AS
BEGIN

SET NOCOUNT ON;

---- préparation de la table temporaire incluant tous les comptes y compris les comptes joints
--IF OBJECT_ID('tempdb..#COMPTES') IS NOT NULL
--drop table #COMPTES;

--select *
--into #COMPTES
--from ( select 'COMPTES' as SRC
--               ,co.NUMERO_CLIENT as NUMERO_CLIENT_PARENT
--               ,co.NUMERO_CLIENT
--               ,co.NUMERO_COMPTE
--			   ,rr.NUMERO_COMPTE as NUMERO_COMPTE_PREC  --modification 2019-04-12
--               ,co.DATE_OUVERTURE
--               ,co.DATE_CLOTURE
--               ,co.CODE_MARQUE
--               ,co.CODE_MARCHE
--               ,co.CODE_PRODUIT
--               ,rr.CODE_PRODUIT as CODE_PRODUIT_SORTIE
--			   ,co.CODE_PRODUIT_SAB as CODE_PRODUIT_SAB
--			   ,rr.CODE_PRODUIT_SAB as CODE_PRODUIT_SAB_SORTIE
--			   ,co.CODE_RUBRIQUE_COMPTABLE
--			   ,rr.CODE_RUBRIQUE_COMPTABLE as CODE_RUBRIQUE_COMPTABLE_SORTIE
--               ,-co.SOLDE_DATE_COMPTABLE as SOLDE_DATE_COMPTABLE
--               ,-co.CONTREVALEUR_SOLDE_MOYEN_CREDITEUR as CONTREVALEUR_SOLDE_MOYEN_CREDITEUR
--               ,-co.CONTREVALEUR_SOLDE_MOYEN_DEBITEUR as CONTREVALEUR_SOLDE_MOYEN_DEBITEUR
--               ,-rr.SOLDE_DATE_COMPTABLE AS SOLDE_DATE_COMPTABLE_PREC
--               ,cav.MONTANT_DECOUVERT_AUTORISE
--               ,-cav.INTERET_CAV_DEBITEURS_DEVISE_BASE as INTERET_CAV_DEBITEURS_DEVISE_BASE
--               ,-cav.INTERET_CAV_CREDITEURS_DEVISE_BASE as INTERET_CAV_CREDITEURS_DEVISE_BASE
--               ,co.ACTIF
--               ,co.DATE_ACTION
--			   ,CASE WHEN co.CODE_PRODUIT_SAB in ('CW8','LW8') and co.PRESENCE_FE = 0 THEN 0
--			         ELSE 1 END as FLAG_FE  --modification 2019-04-12
--			   ,CASE WHEN dateadd(month, datediff(month, 0, co.DATE_OUVERTURE), 0) = dateadd(month, datediff(month, 0, co.DATE_ACTION) - 1, 0)
--                     THEN 1 ELSE 0 END as FLAG_OUVERTURE
--               ,CASE WHEN dateadd(month, datediff(month, 0, co.DATE_OUVERTURE), 0) = dateadd(month, datediff(month, 0, co.DATE_ACTION) - 4, 0)
--                     THEN 1 ELSE 0 END as FLAG_PROD_ACTIF
--			   ,CASE WHEN dateadd(month, datediff(month, 0, co.DATE_CLOTURE), 0) = dateadd(month, datediff(month, 0, co.DATE_ACTION) - 1, 0)
--                     THEN 1 ELSE 0 END as FLAG_CLOTURE
--			   ,CASE WHEN co.CODE_PRODUIT <> rr.CODE_PRODUIT and co.CODE_PRODUIT_SAB != 'CCO' and rr.CODE_PRODUIT_SAB != 'CCO' THEN 1
--			         WHEN (co.CODE_PRODUIT_SAB = 'CCO' and rr.CODE_PRODUIT_SAB != 'CCO') or (co.CODE_PRODUIT_SAB != 'CCO' and rr.CODE_PRODUIT_SAB = 'CCO') THEN 1 
--					 ELSE 0 
--					 END as FLAG_ENTREE
--			   ,CASE WHEN co.CODE_PRODUIT <> rr.CODE_PRODUIT and co.CODE_PRODUIT_SAB != 'CCO' and rr.CODE_PRODUIT_SAB != 'CCO' THEN 1
--			         WHEN (co.CODE_PRODUIT_SAB = 'CCO' and rr.CODE_PRODUIT_SAB != 'CCO') or (co.CODE_PRODUIT_SAB != 'CCO' and rr.CODE_PRODUIT_SAB = 'CCO') THEN 1  
--					 ELSE 0 
--					 END as FLAG_SORTIE
--               ,CASE WHEN ( co.DATE_CLOTURE is null or co.DATE_CLOTURE >= co.DATE_ACTION ) 
--                      AND ( co.DATE_OUVERTURE < co.DATE_ACTION ) THEN 1 ELSE 0 END as FLAG_STOCK
--       from [$(DataFactoryBFRDatabaseName)].dbo.BF_COMPTES co
--       left join [$(DataFactoryBFRDatabaseName)].dbo.BF_COMPTES rr
--              on rr.NUMERO_COMPTE = co.NUMERO_COMPTE and dateadd(month, datediff(month, 0, rr.DATE_ACTION), 0) = dateadd(month, datediff(month, 0, co.DATE_ACTION) - 1, 0)
--       left join [$(DataFactoryBFRDatabaseName)].dbo.BF_CAV cav
--              on co.NUMERO_COMPTE = cav.NUMERO_COMPTE and co.DATE_ACTION = cav.DATE_ACTION
--	   where @CurrentDate is null 
--           or ( dateadd(month, datediff(month, 0, co.DATE_ACTION), 0) = dateadd(month, datediff(month, 0, @CurrentDate), 0) )
--) COMPTES
--;

--with ind_pvt as (
--select co.CODE_MARQUE as cd_mrq
--      ,co.CODE_MARCHE as cd_mrc
--      ,co.CODE_PRODUIT as cd_pdt
--      ,'99999' as cd_rsx
--      ,t.CODE_DATE as cd_dt
     
---- Production brute de CAV
--      ,count( CASE WHEN co.FLAG_OUVERTURE = 1
--                    AND p.CODE_PRODUIT_N1 like '%CAV%' 
--				    AND co.CODE_PRODUIT <> 'CAV011' --exclusion des surendettements 
--					AND co.CODE_PRODUIT not in ('CAV035','CAV037','CAV039','CAV041','CAV042','CAV043','CAV045','CAV049','CAV051','CAV055','CAV068','CAV069') -- exclusion des comptes de contrepartie
--					AND co.CODE_RUBRIQUE_COMPTABLE not in ('251149','251501','251502','251504','251505','251507','251517','251519','251522','251523') --exclusion des CAV EDG
--					AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--                   THEN co.NUMERO_COMPTE ELSE NULL END ) as PCC01
	 
---- Production brute de CAV EPGN													
--		,count( CASE WHEN co.FLAG_OUVERTURE = 1													
--		  		  	  AND p.CODE_PRODUIT_N1 like '%CAV%' 												
--					  AND co.CODE_PRODUIT <> 'CAV011' --exclusion des surendettements 	
--	 				  AND co.CODE_PRODUIT not in ('CAV035','CAV037','CAV039','CAV041','CAV042','CAV043','CAV045','CAV049','CAV051','CAV055','CAV068','CAV069') -- exclusion des comptes de contrepartie											
--					  AND co.CODE_RUBRIQUE_COMPTABLE not in ('251149','251501','251502','251504','251505','251507','251517','251519','251522','251523') --exclusion des CAV EDG												
--					  AND co.CODE_RUBRIQUE_COMPTABLE not in ( '205115','203121','203194','204192','205200') --exclusion des CCO CREDITS : modification 20190212 exclusion 205200 - crédit en syndication												
--				      AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--					 THEN co.NUMERO_COMPTE ELSE NULL END ) as PCC16		

---- Production brute de CAV créditeurs
--       ,count( CASE WHEN co.FLAG_OUVERTURE = 1
--                    AND p.CODE_PRODUIT_N1 like '%CAV%'
--					AND	co.SOLDE_DATE_COMPTABLE > 0 
--					AND co.CODE_PRODUIT <> 'CAV011' --exclusion des CAV surendettement
--	 		 	    AND co.CODE_PRODUIT not in ('CAV035','CAV037','CAV039','CAV041','CAV042','CAV043','CAV045','CAV049','CAV051','CAV055','CAV068','CAV069') -- exclusion des comptes de contrepartie											
--					AND co.CODE_RUBRIQUE_COMPTABLE not in ('251149','251501','251502','251504','251505','251507','251517','251519','251522','251523') --exclusion des CAV EDG
--					AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--                   THEN co.NUMERO_COMPTE ELSE NULL END ) as PCC21
	 
---- Valeur de la production brute de CAV débiteurs
--     , sum( CASE WHEN co.FLAG_OUVERTURE = 1 
--                  AND p.CODE_PRODUIT_N1 like '%CAV%' 
--				  AND co.CODE_PRODUIT <> 'CAV011' --exclusion des CAV surendettement
--	 			  AND co.CODE_PRODUIT not in ('CAV035','CAV037','CAV039','CAV041','CAV042','CAV043','CAV045','CAV049','CAV051','CAV055','CAV068','CAV069') -- exclusion des comptes de contrepartie											
--				  AND co.CODE_RUBRIQUE_COMPTABLE not in ('251149','251501','251502','251504','251505','251507','251517','251519','251522','251523') --exclusion des CAV EDG
--				  AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--                 THEN co.MONTANT_DECOUVERT_AUTORISE ELSE 0 END ) as PCC22
	 
---- Valeur de la production brute de CAV créditeurs
--     , sum( CASE WHEN co.FLAG_OUVERTURE = 1 
--                  AND p.CODE_PRODUIT_N1 like '%CAV%'
--				  AND co.SOLDE_DATE_COMPTABLE > 0 
--      			  AND co.CODE_PRODUIT <> 'CAV011' -- exclusion des CAV surendettement
--	 			  AND co.CODE_PRODUIT not in ('CAV035','CAV037','CAV039','CAV041','CAV042','CAV043','CAV045','CAV049','CAV051','CAV055','CAV068','CAV069') -- exclusion des comptes de contrepartie											
--				  AND co.CODE_RUBRIQUE_COMPTABLE not in ('251149','251501','251502','251504','251505','251507','251517','251519','251522','251523') --exclusion des CAV EDG
--				  AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--                 THEN co.SOLDE_DATE_COMPTABLE ELSE 0 END ) as PCC23
     
---- Nombre de CAV actifs 
--     , count( CASE WHEN co.FLAG_PROD_ACTIF = 1 
--                    AND p.CODE_PRODUIT_N1 like '%CAV%'
--                    AND co.ACTIF = 1 
--					AND co.CODE_PRODUIT <> 'CAV011' --exclusion des CAV surendettement
--	 				AND co.CODE_PRODUIT not in ('CAV035','CAV037','CAV039','CAV041','CAV042','CAV043','CAV045','CAV049','CAV051','CAV055','CAV068','CAV069') -- exclusion des comptes de contrepartie											
--					AND co.CODE_RUBRIQUE_COMPTABLE not in ('251149','251501','251502','251504','251505','251507','251517','251519','251522','251523') --exclusion des CAV EDG
-- 				    AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--                   THEN co.NUMERO_COMPTE ELSE NULL END ) as PCC20
---- entrée
--     , count( CASE WHEN co.FLAG_ENTREE = 1 
--                    AND p.CODE_PRODUIT_N1 like '%CAV%' 
--					AND co.CODE_PRODUIT <> 'CAV011' -- exclusion des CAV surendettement
--	 				AND co.CODE_PRODUIT not in ('CAV035','CAV037','CAV039','CAV041','CAV042','CAV043','CAV045','CAV049','CAV051','CAV055','CAV068','CAV069') -- exclusion des comptes de contrepartie											
--					AND co.CODE_RUBRIQUE_COMPTABLE not in ('251149','251501','251502','251504','251505','251507','251517','251519','251522','251523') --exclusion des CAV EDG
--					AND co.CODE_RUBRIQUE_COMPTABLE not like '2519%' --exclusion des contentieux et douteux
--                    AND co.CODE_RUBRIQUE_COMPTABLE not like '291%'  --exclusion des contentieux et douteux
--					AND CODE_RUBRIQUE_COMPTABLE_SORTIE not like '2519%' --exclusion des contentieux et douteux
--				    AND CODE_RUBRIQUE_COMPTABLE_SORTIE not like '291%'  --exclusion des contentieux et douteux
--				    AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--                   THEN co.NUMERO_COMPTE ELSE NULL END ) as PCC04
     
---- sortie
--     , 0 as PCC06
     
---- Nombre de cloture mensuelle de CAV
--     , count( CASE WHEN co.FLAG_CLOTURE = 1  
--                    AND p.CODE_PRODUIT_N1 like '%CAV%' 
--					AND co.CODE_PRODUIT <> 'CAV011' --exclusion des CAV surendettement
--	 				AND co.CODE_PRODUIT not in ('CAV035','CAV037','CAV039','CAV041','CAV042','CAV043','CAV045','CAV049','CAV051','CAV055','CAV068','CAV069') -- exclusion des comptes de contrepartie
--					AND co.CODE_RUBRIQUE_COMPTABLE not in ('251149','251501','251502','251504','251505','251507','251517','251519','251522','251523') --exclusion des CAV EDG
--				    AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--                   THEN co.NUMERO_COMPTE ELSE NULL END ) as PCC08
     
---- Production brute sur Epargne Bilancielle
--     , count( CASE WHEN co.FLAG_OUVERTURE = 1
--                    AND p.CODE_PRODUIT_N1 like '%EPA%'
--				    AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--                   THEN co.NUMERO_COMPTE ELSE NULL END ) as PEB01
     
---- Valeur de la production brute sur Epargne Bilancielle
--     , sum( CASE WHEN co.FLAG_OUVERTURE = 1 
--                  AND p.CODE_PRODUIT_N1 like '%EPA%'
--				  AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--                 THEN co.SOLDE_DATE_COMPTABLE ELSE 0 END ) as PEB03
     
---- Nombre de tombées sur Epargne bilancielle
--     , count( CASE WHEN co.SOLDE_DATE_COMPTABLE < co.SOLDE_DATE_COMPTABLE_PREC
--                    AND p.CODE_PRODUIT_N1 like '%EPA%'
--				    AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--                   THEN co.NUMERO_COMPTE ELSE NULL END ) as PEB08
     
---- Valeur des tombées sur Epargne bilancielle
--     , sum( CASE WHEN co.SOLDE_DATE_COMPTABLE < co.SOLDE_DATE_COMPTABLE_PREC
--                  AND p.CODE_PRODUIT_N1 like '%EPA%'
--				  AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--                 THEN (co.SOLDE_DATE_COMPTABLE_PREC-co.SOLDE_DATE_COMPTABLE) ELSE 0 END ) as PEB10 -- modification 2019-02-12

---- Valeur moyenne de la production brute sur Epargne Bilancielle
--	 , sum( CASE WHEN co.FLAG_OUVERTURE = 1
--                  AND p.CODE_PRODUIT_N1 like '%EPA%'
--				  AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--                 THEN (co.CONTREVALEUR_SOLDE_MOYEN_CREDITEUR + co.CONTREVALEUR_SOLDE_MOYEN_DEBITEUR)  ELSE 0 END ) as PEB19
     
---- Nombre de CAV en stock
--     , count( CASE WHEN co.FLAG_STOCK = 1 
--                    AND p.CODE_PRODUIT_N1 like '%CAV%' 
--					AND co.CODE_PRODUIT <> 'CAV011' -- exclusion des CAV surendettement
--	 				AND co.CODE_PRODUIT not in ('CAV035','CAV037','CAV039','CAV041','CAV042','CAV043','CAV045','CAV049','CAV051','CAV055','CAV068','CAV069') -- exclusion des comptes de contrepartie											
--					AND co.CODE_RUBRIQUE_COMPTABLE not in ('251149','251501','251502','251504','251505','251507','251517','251519','251522','251523') --exclusion des CAV EDG
-- 				    AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--                   THEN co.NUMERO_COMPTE ELSE NULL END ) as SCC01
     
---- Nombre de CAV EPGN en stock													
--		, count( CASE WHEN co.FLAG_STOCK = 1 													
--					   AND p.CODE_PRODUIT_N1 like '%CAV%' 												
--					   AND co.CODE_PRODUIT <> 'CAV011' --exclusion des CAV surendettements
--	 				   AND co.CODE_PRODUIT not in ('CAV035','CAV037','CAV039','CAV041','CAV042','CAV043','CAV045','CAV049','CAV051','CAV055','CAV068','CAV069') -- exclusion des comptes de contrepartie																   												
--					   AND co.CODE_RUBRIQUE_COMPTABLE not in ('251149','251501','251502','251504','251505','251507','251517','251519','251522','251523') --exclusion des CAV EDG												
--					   AND co.CODE_RUBRIQUE_COMPTABLE not in ( '205115','203121','203194','204192','205200') --exclusion des CCO CREDITS : modification 20190212 exclusion 205200 - crédit en syndication												
-- 				       AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--					  THEN co.NUMERO_COMPTE ELSE NULL END ) as SCC30	

---- Nombre de CAV actifs en stock
--     , count( CASE WHEN co.FLAG_STOCK = 1
--                    AND p.CODE_PRODUIT_N1 like '%CAV%'
--                    AND co.ACTIF = 1 
--					AND co.CODE_PRODUIT <> 'CAV011' -- exclusion des CAV surendettement
--	 				AND co.CODE_PRODUIT not in ('CAV035','CAV037','CAV039','CAV041','CAV042','CAV043','CAV045','CAV049','CAV051','CAV055','CAV068','CAV069') -- exclusion des comptes de contrepartie
--					AND co.CODE_RUBRIQUE_COMPTABLE not in ('251149','251501','251502','251504','251505','251507','251517','251519','251522','251523') --exclusion des CAV EDG
-- 				    AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--                   THEN co.NUMERO_COMPTE ELSE NULL END ) as SCC05
     
---- Nombre de CAV distanciés en stock
--     , count( CASE WHEN co.FLAG_STOCK = 1
--                    AND p.CODE_PRODUIT_N1 like '%CAV%'
--                    AND co.ACTIF = 0 
--					AND co.CODE_PRODUIT <> 'CAV011' -- exclusion des CAV surendettement
--	 				AND co.CODE_PRODUIT not in ('CAV035','CAV037','CAV039','CAV041','CAV042','CAV043','CAV045','CAV049','CAV051','CAV055','CAV068','CAV069') -- exclusion des comptes de contrepartie											
--					AND co.CODE_RUBRIQUE_COMPTABLE not in ('251149','251501','251502','251504','251505','251507','251517','251519','251522','251523') --exclusion des CAV EDG
-- 				    AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--                   THEN co.NUMERO_COMPTE ELSE NULL END ) as SCC08
     
---- Somme des encours moyens des CAV en stock  
--     , sum( CASE WHEN co.FLAG_STOCK = 1
--                  AND p.CODE_PRODUIT_N1 like '%CAV%' 
--				  AND co.CODE_PRODUIT <> 'CAV011' -- exclusion des CAV surendettement
--	 			  AND co.CODE_PRODUIT not in ('CAV035','CAV037','CAV039','CAV041','CAV042','CAV043','CAV045','CAV049','CAV051','CAV055','CAV068','CAV069') -- exclusion des comptes de contrepartie											
--				  AND co.CODE_RUBRIQUE_COMPTABLE not in ('251149','251501','251502','251504','251505','251507','251517','251519','251522','251523') --exclusion des CAV EDG
-- 				  AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--				 THEN co.CONTREVALEUR_SOLDE_MOYEN_CREDITEUR+co.CONTREVALEUR_SOLDE_MOYEN_DEBITEUR ELSE 0 END ) as SCC18
     
---- Somme des encours moyens des CAV EPGN en stock  													
--		, sum( CASE WHEN co.FLAG_STOCK = 1													
--					 AND p.CODE_PRODUIT_N1 like '%CAV%' 												
--					 AND co.CODE_PRODUIT <> 'CAV011' --exclusion des CAV surendettement
--	 				 AND co.CODE_PRODUIT not in ('CAV035','CAV037','CAV039','CAV041','CAV042','CAV043','CAV045','CAV049','CAV051','CAV055','CAV068','CAV069') -- exclusion des comptes de contrepartie											
--					 AND co.CODE_RUBRIQUE_COMPTABLE not in ('251149','251501','251502','251504','251505','251507','251517','251519','251522','251523') --exclusion des CAV EDG												
--					 AND co.CODE_RUBRIQUE_COMPTABLE not in ( '205115','203121','203194','204192','205200') --exclusion des CCO CREDITS : modification 20190212 exclusion 205200 - crédit en syndication												
-- 				     AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--					THEN co.CONTREVALEUR_SOLDE_MOYEN_CREDITEUR+co.CONTREVALEUR_SOLDE_MOYEN_DEBITEUR ELSE 0 END ) as SCC32	

---- Somme des encours moyens des CAV actifs en stock  
--     , sum( CASE WHEN co.FLAG_STOCK = 1
--                  AND p.CODE_PRODUIT_N1 like '%CAV%'
--                  AND co.ACTIF = 1 
--				  AND co.CODE_PRODUIT <> 'CAV011' -- exclusion des CAV surendettement
--	 			  AND co.CODE_PRODUIT not in ('CAV035','CAV037','CAV039','CAV041','CAV042','CAV043','CAV045','CAV049','CAV051','CAV055','CAV068','CAV069') -- exclusion des comptes de contrepartie											
--				  AND co.CODE_RUBRIQUE_COMPTABLE not in ('251149','251501','251502','251504','251505','251507','251517','251519','251522','251523') --exclusion des CAV EDG
-- 				  AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--                 THEN co.CONTREVALEUR_SOLDE_MOYEN_CREDITEUR+co.CONTREVALEUR_SOLDE_MOYEN_DEBITEUR ELSE 0 END ) as SCC19											

---- Somme des encours moyens des CAV distanciés en stock  
--     , sum( CASE WHEN co.FLAG_STOCK = 1
--                  AND p.CODE_PRODUIT_N1 like '%CAV%'
--                  AND co.ACTIF = 0 
--				  AND co.CODE_PRODUIT <> 'CAV011' -- exclusion des CAV surendettement
--	 			  AND co.CODE_PRODUIT not in ('CAV035','CAV037','CAV039','CAV041','CAV042','CAV043','CAV045','CAV049','CAV051','CAV055','CAV068','CAV069') -- exclusion des comptes de contrepartie											
--				  AND co.CODE_RUBRIQUE_COMPTABLE not in ('251149','251501','251502','251504','251505','251507','251517','251519','251522','251523') --exclusion des CAV EDG
-- 				  AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--                 THEN co.CONTREVALEUR_SOLDE_MOYEN_CREDITEUR+co.CONTREVALEUR_SOLDE_MOYEN_DEBITEUR ELSE 0 END ) as SCC20
	 
---- Somme des encours du stock de CAV débiteurs  
--     , sum( CASE WHEN co.FLAG_STOCK = 1 
--                  AND p.CODE_PRODUIT_N1 like '%CAV%'
--                  AND co.SOLDE_DATE_COMPTABLE < 0 
--  				  AND co.CODE_PRODUIT <> 'CAV011' -- exclusion des CAV surendettement
--	 			  AND co.CODE_PRODUIT not in ('CAV035','CAV037','CAV039','CAV041','CAV042','CAV043','CAV045','CAV049','CAV051','CAV055','CAV068','CAV069') -- exclusion des comptes de contrepartie											
--				  AND co.CODE_RUBRIQUE_COMPTABLE not in ('251149','251501','251502','251504','251505','251507','251517','251519','251522','251523') --exclusion des CAV EDG
-- 				  AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--                 THEN -co.SOLDE_DATE_COMPTABLE ELSE 0 END ) as SCC21
	 
---- Somme des encours du stock de CAV créditeurs  
--     , sum( CASE WHEN co.FLAG_STOCK = 1
--                  AND p.CODE_PRODUIT_N1 like '%CAV%' 
--                  AND co.SOLDE_DATE_COMPTABLE > 0 
--				  AND co.CODE_PRODUIT <> 'CAV011' -- exclusion des CAV surendettement
--	 			  AND co.CODE_PRODUIT not in ('CAV035','CAV037','CAV039','CAV041','CAV042','CAV043','CAV045','CAV049','CAV051','CAV055','CAV068','CAV069') -- exclusion des comptes de contrepartie											
--				  AND co.CODE_RUBRIQUE_COMPTABLE not in ('251149','251501','251502','251504','251505','251507','251517','251519','251522','251523') --exclusion des CAV EDG
-- 				  AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--                 THEN co.SOLDE_DATE_COMPTABLE ELSE 0 END ) as SCC22

---- Somme des encours du stock de CAV EPGN													
--	, sum( CASE WHEN co.FLAG_STOCK = 1													
--				 AND p.CODE_PRODUIT_N1 like '%CAV%' 												
--				 AND co.CODE_PRODUIT <> 'CAV011' --exclusion des CAV surendettement
--	 			 AND co.CODE_PRODUIT not in ('CAV035','CAV037','CAV039','CAV041','CAV042','CAV043','CAV045','CAV049','CAV051','CAV055','CAV068','CAV069') -- exclusion des comptes de contrepartie											
--				 AND co.CODE_RUBRIQUE_COMPTABLE not in ('251149','251501','251502','251504','251505','251507','251517','251519','251522','251523') --exclusion des CAV EDG												
--				 AND co.CODE_RUBRIQUE_COMPTABLE not in ( '205115','203121','203194','204192','205200') --exclusion des CCO CREDITS : modification 20190212 exclusion 205200 - crédit en syndication												
-- 				 AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--				THEN co.SOLDE_DATE_COMPTABLE ELSE 0 END ) as SCC31													

---- Somme des encours moyens du stock de CAV débiteurs  
--     , sum( CASE WHEN co.FLAG_STOCK = 1
--                  AND p.CODE_PRODUIT_N1 like '%CAV%'
--                  AND co.SOLDE_DATE_COMPTABLE < 0 
--				  AND co.CODE_PRODUIT <> 'CAV011' -- exclusion des CAV surendettement
--	 			  AND co.CODE_PRODUIT not in ('CAV035','CAV037','CAV039','CAV041','CAV042','CAV043','CAV045','CAV049','CAV051','CAV055','CAV068','CAV069') -- exclusion des comptes de contrepartie											
--			      AND co.CODE_RUBRIQUE_COMPTABLE not in ('251149','251501','251502','251504','251505','251507','251517','251519','251522','251523') --exclusion des CAV EDG
-- 				  AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--                 THEN -(co.CONTREVALEUR_SOLDE_MOYEN_CREDITEUR+co.CONTREVALEUR_SOLDE_MOYEN_DEBITEUR) ELSE 0 END ) as SCC23
	 
---- Somme des encours moyens du stock de CAV créditeurs  
--     , sum( CASE WHEN co.FLAG_STOCK = 1
--                  AND p.CODE_PRODUIT_N1 like '%CAV%' 
--				  AND co.SOLDE_DATE_COMPTABLE > 0 
--				  AND co.CODE_PRODUIT <> 'CAV011' -- exclusion des CAV surendettement
--	 			  AND co.CODE_PRODUIT not in ('CAV035','CAV037','CAV039','CAV041','CAV042','CAV043','CAV045','CAV049','CAV051','CAV055','CAV068','CAV069') -- exclusion des comptes de contrepartie											
--				  AND co.CODE_RUBRIQUE_COMPTABLE not in ('251149','251501','251502','251504','251505','251507','251517','251519','251522','251523') --exclusion des CAV EDG
-- 				  AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--                 THEN co.CONTREVALEUR_SOLDE_MOYEN_CREDITEUR+co.CONTREVALEUR_SOLDE_MOYEN_DEBITEUR ELSE 0 END ) as SCC24
     
---- Nombre de CAV débiteurs en stock
--     , count( CASE WHEN co.FLAG_STOCK = 1 
--	                AND p.CODE_PRODUIT_N1 like '%CAV%'
--					AND	co.SOLDE_DATE_COMPTABLE < 0 
--					AND co.CODE_PRODUIT <> 'CAV011' -- exclusion des CAV surendettement
--	 			    AND co.CODE_PRODUIT not in ('CAV035','CAV037','CAV039','CAV041','CAV042','CAV043','CAV045','CAV049','CAV051','CAV055','CAV068','CAV069') -- exclusion des comptes de contrepartie											
--					AND co.CODE_RUBRIQUE_COMPTABLE not in ('251149','251501','251502','251504','251505','251507','251517','251519','251522','251523') --exclusion des CAV EDG
--  				    AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--                   THEN co.NUMERO_COMPTE ELSE NULL END ) as SCC25
     
---- Nombre de CAV créditeurs en stock
--     , count( CASE WHEN co.FLAG_STOCK = 1
--                    AND p.CODE_PRODUIT_N1 like '%CAV%' 
--					AND	co.SOLDE_DATE_COMPTABLE > 0 
--					AND co.CODE_PRODUIT <> 'CAV011' -- exclusion des CAV surendettement
--	 			    AND co.CODE_PRODUIT not in ('CAV035','CAV037','CAV039','CAV041','CAV042','CAV043','CAV045','CAV049','CAV051','CAV055','CAV068','CAV069') -- exclusion des comptes de contrepartie											
--					AND co.CODE_RUBRIQUE_COMPTABLE not in ('251149','251501','251502','251504','251505','251507','251517','251519','251522','251523') --exclusion des CAV EDG
-- 				    AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--                   THEN co.NUMERO_COMPTE ELSE NULL END ) as SCC26
	 
---- Somme des interets du stock de CAV débiteurs  
--     , sum( CASE WHEN co.FLAG_STOCK = 1 
--                  AND p.CODE_PRODUIT_N1 like '%CAV%'
--                  AND co.SOLDE_DATE_COMPTABLE < 0 
--				  AND co.CODE_PRODUIT <> 'CAV011' -- exclusion des CAV surendettement
--	 			  AND co.CODE_PRODUIT not in ('CAV035','CAV037','CAV039','CAV041','CAV042','CAV043','CAV045','CAV049','CAV051','CAV055','CAV068','CAV069') -- exclusion des comptes de contrepartie											
--				  AND co.CODE_RUBRIQUE_COMPTABLE not in ('251149','251501','251502','251504','251505','251507','251517','251519','251522','251523') --exclusion des CAV EDG
-- 				  AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--                 THEN -co.INTERET_CAV_DEBITEURS_DEVISE_BASE ELSE 0 END ) as SCC27
	 
---- Somme des interets du stock de CAV créditeurs  
--     , sum( CASE WHEN co.FLAG_STOCK = 1
--                  AND p.CODE_PRODUIT_N1 like '%CAV%'
--                  AND co.SOLDE_DATE_COMPTABLE > 0 
--   				  AND co.CODE_PRODUIT <> 'CAV011' -- exclusion des CAV surendettement
--	 			  AND co.CODE_PRODUIT not in ('CAV035','CAV037','CAV039','CAV041','CAV042','CAV043','CAV045','CAV049','CAV051','CAV055','CAV068','CAV069') -- exclusion des comptes de contrepartie											
--				  AND co.CODE_RUBRIQUE_COMPTABLE not in ('251149','251501','251502','251504','251505','251507','251517','251519','251522','251523') --exclusion des CAV EDG
-- 				  AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--                 THEN co.INTERET_CAV_CREDITEURS_DEVISE_BASE ELSE 0 END ) as SCC28
     
---- Stock sur Epargne Bilancielle
--     , count( CASE WHEN co.FLAG_STOCK = 1 
--                    AND p.CODE_PRODUIT_N1 like '%EPA%'
-- 				    AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--                   THEN co.NUMERO_COMPTE ELSE NULL END ) as SEB01
     
---- Encours du stock sur Epargne Bilancielle
--     , sum( CASE WHEN co.FLAG_STOCK = 1
--                  AND p.CODE_PRODUIT_N1 like '%EPA%'
--  				  AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--                 THEN co.SOLDE_DATE_COMPTABLE ELSE 0 END ) as SEB03
     
---- Encours moyen du stock sur Epargne bilancielle
--     , sum( CASE WHEN co.FLAG_STOCK = 1
--                  AND p.CODE_PRODUIT_N1 like '%EPA%'
--  				  AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--                 THEN co.CONTREVALEUR_SOLDE_MOYEN_CREDITEUR+co.CONTREVALEUR_SOLDE_MOYEN_DEBITEUR ELSE 0 END ) as SEB07
     
---- Nombre équipement
--     ,count( CASE WHEN (c.TIERS_COLLECTIF <> 1
--	               AND (p.CODE_PRODUIT_N1 like '%CAV%' or p.CODE_PRODUIT_N1 like '%EPA%')
--				   AND  co.CODE_PRODUIT <> 'CAV011' --exclusion des CAV surendettement
--	 			   AND  co.CODE_PRODUIT not in ('CAV035','CAV037','CAV039','CAV041','CAV042','CAV043','CAV045','CAV049','CAV051','CAV055','CAV068','CAV069') -- exclusion des comptes de contrepartie											
--				   AND  co.CODE_RUBRIQUE_COMPTABLE not in ('251149','251501','251502','251504','251505','251507','251517','251519','251522','251523') --exclusion des CAV EDG
--	               AND (c.DATE_FIN_RELATION_CLIENT is null or c.DATE_FIN_RELATION_CLIENT >= c.DATE_ACTION )
--                   AND  c.DATE_CREATION_CLIENT < DATEADD(MONTH, DATEDIFF(MONTH, 0, c.DATE_ACTION), 0))
-- 				   AND  co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--                   THEN co.NUMERO_COMPTE ELSE NULL END ) as CLI30
--from #COMPTES co
--left join [$(DataFactoryBFRDatabaseName)].dbo.BF_CLIENTS c
--       on co.NUMERO_CLIENT = c.NUMERO_CLIENT and co.DATE_ACTION = c.DATE_ACTION
--left join bfr.T_DIM_PRODUIT p
--       on co.CODE_PRODUIT = p.CODE_PRODUIT_N5
--left join bfr.T_DIM_TEMPS t
--       on t.DATE_ANNEE_MOIS_JOUR = EOMONTH(co.DATE_ACTION, -1)
--where co.CODE_MARQUE is not null
--  and co.CODE_MARCHE is not null
--  and co.CODE_PRODUIT is not null
--group by co.CODE_MARQUE
--       , co.CODE_MARCHE
--       , co.CODE_PRODUIT
--       , t.CODE_DATE

--union all

--select co.CODE_MARQUE as cd_mrq
--     , co.CODE_MARCHE as cd_mrc
--     , co.CODE_PRODUIT_SORTIE as cd_pdt
--     , '99999' as cd_rsx
--     , t.CODE_DATE as cd_dt
     
---- Production brute de CAV
--     , 0 as PCC01

---- Production brute de CAV EPGN
--	 , 0 as PCC16

---- Production brute de CAV créditeurs
--     , 0 as PCC21
	 
---- Valeur de la production brute de CAV débiteurs
--     , 0 as PCC22
	 
---- Valeur de la production brute de CAV créditeurs
--     , 0 as PCC23
     
---- Nombre de CAV actifs 
--     , 0 as PCC20
     
---- entrée
--     , 0 as PCC04
     
---- sortie
--     ,count( CASE WHEN co.FLAG_SORTIE = 1
--                   AND p.CODE_PRODUIT_N1 like '%CAV%' 
--				   AND co.CODE_PRODUIT <> 'CAV011' -- exclusion des CAV surendettement
--	 			   AND co.CODE_PRODUIT not in ('CAV035','CAV037','CAV039','CAV041','CAV042','CAV043','CAV045','CAV049','CAV051','CAV055','CAV068','CAV069') -- exclusion des comptes de contrepartie											
--				   AND co.CODE_RUBRIQUE_COMPTABLE not in ('251149','251501','251502','251504','251505','251507','251517','251519','251522','251523') --exclusion des CAV EDG
--				   AND CODE_RUBRIQUE_COMPTABLE_SORTIE not like '2519%' --exclusion des contentieux et douteux
--				   AND CODE_RUBRIQUE_COMPTABLE_SORTIE not like '291%'  --exclusion des contentieux et douteux
--				   AND CODE_RUBRIQUE_COMPTABLE not like '2519%' --exclusion des contentieux et douteux
--				   AND CODE_RUBRIQUE_COMPTABLE not like '291%'  --exclusion des contentieux et douteux
-- 				   AND co.FLAG_FE = 1  --modification 2019-04-12 : suppression des comptes CW8 et LW8 non présents dans FE
--                  THEN co.NUMERO_COMPTE ELSE NULL END )  as PCC06
     
---- Nombre de cloture mensuelle de CAV
--     , 0 as PCC08
     
---- Production brute sur Epargne Bilancielle
--     , 0 as PEB01
     
---- Valeur de la production brute sur Epargne Bilancielle
--     , 0 as PEB03
     
---- Nombre de tombées sur Epargne bilancielle
--     , 0 as PEB08
     
---- Valeur des tombées sur Epargne bilancielle
--     , 0 as PEB10
	 
---- Encours moyens production brute épargne bilancielle
--	 , 0 as PEB19
     
---- Nombre de CAV en stock
--     , 0 as SCC01

---- Nombre de CAV EPGN en stock	
--     , 0 as SCC30
     
---- Nombre de CAV actifs en stock
--     , 0 as SCC05
     
---- Nombre de CAV distanciés en stock
--     , 0 as SCC08
     
---- Somme des encours moyens des CAV en stock  
--     , 0 as SCC18

---- Somme des encours moyens des CAV EPGN en stock
--	  , 0 as SCC32
     
---- Somme des encours moyens des CAV actifs en stock  
--     , 0 as SCC19
     
---- Somme des encours moyens des CAV distanciés en stock  
--     , 0 as SCC20
	 
---- Somme des encours du stock de CAV débiteurs  
--     , 0 as SCC21
	 
---- Somme des encours du stock de CAV créditeurs  
--     , 0 as SCC22

---- Somme des encours du stock de CAV EPGN
--	  , 0 as SCC31
	 
---- Somme des encours moyens du stock de CAV débiteurs  
--     , 0 as SCC23
	 
---- Somme des encours moyens du stock de CAV créditeurs  
--     , 0 as SCC24
     
---- Nombre de CAV débiteurs en stock
--     , 0 as SCC25
     
---- Nombre de CAV créditeurs en stock
--     , 0 as SCC26
	 
---- Somme des interets du stock de CAV débiteurs  
--     , 0 as SCC27
	 
---- Somme des interets du stock de CAV créditeurs  
--     , 0 as SCC28
     
---- Stock sur Epargne Bilancielle
--     , 0 as SEB01
     
---- Encours du stock sur Epargne Bilancielle
--     , 0 as SEB03
     
---- Encours moyen du stock sur Epargne bilancielle
--     , 0 as SEB07
     
---- Nombre équipement
--     , 0 as CLI30

--from #COMPTES co
--left join bfr.T_DIM_PRODUIT p
--       on co.CODE_PRODUIT_SORTIE = p.CODE_PRODUIT_N5
--left join bfr.T_DIM_TEMPS t
--       on t.DATE_ANNEE_MOIS_JOUR = EOMONTH(co.DATE_ACTION, -1)
--where co.CODE_MARQUE is not null
--  and co.CODE_MARCHE is not null
--  and co.CODE_PRODUIT_SORTIE is not null
--group by co.CODE_MARQUE
--       , co.CODE_MARCHE
--	   , co.CODE_PRODUIT_SORTIE
--       , t.CODE_DATE
--),
----Unpivot the table.
--ind_unpvt as (
--SELECT cd_mrq, cd_mrc, cd_pdt, cd_rsx, cd_dt
--     , cd_ind, vl_ind
--  FROM ( SELECT cd_mrq, cd_mrc, cd_pdt, cd_rsx, cd_dt, 
--                cast(PCC01 as float) as PCC01, cast(PCC20 as float) as PCC20, cast(PCC04 as float) as PCC04, cast(PCC06 as float) as PCC06, cast(PCC08 as float) as PCC08, 
--                cast(PEB01 as float) as PEB01, cast(PEB03 as float) as PEB03, cast(PEB08 as float) as PEB08, cast(PEB10 as float) as PEB10, cast(PEB19 as float) as PEB19,
--                cast(SCC01 as float) as SCC01, cast(SCC05 as float) as SCC05, cast(SCC08 as float) as SCC08, cast(SCC18 as float) as SCC18, cast(SCC19 as float) as SCC19, 
--				cast(SCC20 as float) as SCC20, cast(SEB01 as float) as SEB01, cast(SEB03 as float) as SEB03, cast(SEB07 as float) as SEB07, cast(CLI30 as float) as CLI30, 
--				cast(PCC21 as float) as PCC21, cast(PCC22 as float) as PCC22, cast(PCC23 as float) as PCC23, cast(SCC21 as float) as SCC21, cast(SCC22 as float) as SCC22, 
--				cast(SCC23 as float) as SCC23, cast(SCC24 as float) as SCC24, cast(SCC25 as float) as SCC25, cast(SCC26 as float) as SCC26, cast(SCC27 as float) as SCC27, 
--				cast(SCC28 as float) as SCC28, cast(PCC16 as float) as PCC16, cast(SCC30 as float) as SCC30, cast(SCC32 as float) as SCC32, cast(SCC31 as float) as SCC31

--           FROM ind_pvt ) p
--         UNPIVOT ( vl_ind FOR cd_ind IN (PCC01, PCC20, PCC04, PCC06, PCC08, PEB01, PEB03, PEB08, PEB10, PEB19, SCC01, SCC05, SCC08, SCC18, SCC19, SCC20, SEB01, SEB03, SEB07, CLI30, PCC21, PCC22, PCC23, SCC21, SCC22, SCC23, SCC24, SCC25, SCC26, SCC27, SCC28,
--											PCC16, SCC30, SCC32, SCC31)
--       ) AS unpvt
--)

--insert into [bfr].[T_FACT_PILOTAGE_ECO]
--select *
--     , getdate() as dt_act
--  from ind_unpvt
-- where vl_ind <> 0

END
GO