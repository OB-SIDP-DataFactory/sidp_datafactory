﻿
CREATE PROCEDURE [bfr].[Alim_T_FACT_Pilotage_ECO_ASS] 
(
   @CurrentDate DATE
)
AS
BEGIN

SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#ASSURANCES') IS NOT NULL
drop table #ASSURANCES;

--select *
--  into #ASSURANCES
--  from ( 
--         select  ass.NUMERO_CLIENT
--			   , ass.[NUMERO_COMPTE]
--			   , ass.[CODE_CONVENTION]
--			   , ass.[DATE_ADHESION_CONVENTION_DEBUT]
--			   , ass.[DATE_ADHESION_CONVENTION_FIN]
--			   , ass.[CODE_TYPE_ASSURANCE]
--			   , ass.[CODE_SERVICE]
--			   , ass.[DATE_ADHESION_SERVICE_DEBUT]
--			   , ass.[DATE_ADHESION_SERVICE_FIN]
--			   , ass.[DATE_RESILIATION_SERVICE]
--			   , ass.[DATE_RESILIATION_CONVENTION]
--			   , ass.[DATE_ACTION]
--           from [$(DataFactoryBFRDatabaseName)].dbo.BF_ASSURANCES ass
--          where @CurrentDate is null 
--             or ( dateadd(month, datediff(month, 0, ass.DATE_ACTION), 0) = dateadd(month, datediff(month, 0, @CurrentDate), 0) )
--) ASSURANCES;

--with ind_pvt as (
--select c.CODE_MARQUE as cd_mrq
--     , c.CODE_MARCHE as cd_mrc
--     , ass.[CODE_TYPE_ASSURANCE] as cd_pdt
--     , '99999' as cd_rsx
--     , t.CODE_DATE as cd_dt
--       -- Calcul du nombre d'equipement pour les assurances
--     , count( CASE WHEN 
--	               (
--	                c.TIERS_COLLECTIF <> 1
--					and  (c.DATE_FIN_RELATION_CLIENT is null or c.DATE_FIN_RELATION_CLIENT >= c.DATE_ACTION )
--                    and  c.DATE_CREATION_CLIENT < DATEADD(MONTH, DATEDIFF(MONTH, 0, c.DATE_ACTION), 0)
--					and
--						(([DATE_RESILIATION_SERVICE] is  null or [DATE_ADHESION_SERVICE_FIN] is null)
--					          or
--						([DATE_RESILIATION_SERVICE] >= ass.DATE_ACTION or [DATE_ADHESION_SERVICE_FIN] >= ass.DATE_ACTION))
--					)
--                   THEN ass.NUMERO_COMPTE ELSE NULL END ) as CLI30
--from #ASSURANCES ass
--left join [$(DataFactoryBFRDatabaseName)].dbo.BF_CLIENTS c
--       on ass.NUMERO_CLIENT = c.NUMERO_CLIENT and ass.DATE_ACTION = c.DATE_ACTION
--left join bfr.T_DIM_PRODUIT p
--       on ass.[CODE_TYPE_ASSURANCE] = p.CODE_PRODUIT_N5
--left join bfr.T_DIM_TEMPS t
--       on t.DATE_ANNEE_MOIS_JOUR = EOMONTH(ass.DATE_ACTION, -1)
--where c.CODE_MARQUE is not null
--  and c.CODE_MARCHE is not null
--  and ass.[CODE_TYPE_ASSURANCE] is not null
--group by c.CODE_MARQUE
--       ,c.CODE_MARCHE
--       ,ass.[CODE_TYPE_ASSURANCE]
--       ,t.CODE_DATE
--),

----Unpivot the table.
--ind_unpvt as (
--SELECT cd_mrq, cd_mrc, cd_pdt, cd_rsx, cd_dt
--     , cd_ind, vl_ind
--  FROM ( SELECT cd_mrq, cd_mrc, cd_pdt, cd_rsx, cd_dt, 
--                cast(CLI30 as float) as CLI30
--           FROM ind_pvt ) p
--         UNPIVOT ( vl_ind FOR cd_ind IN (CLI30)
--       ) AS unpvt
--)

--insert into [bfr].[T_FACT_PILOTAGE_ECO]
--select *
--     , getdate() as dt_act
--  from ind_unpvt
-- where vl_ind <> 0

END
GO