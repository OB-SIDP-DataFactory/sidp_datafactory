﻿-- =============================================
-- Author:		<Ali Fassahi>
-- Create date: <25/04/2017>
-- Description:	<Alimentation de la table de dimension T_DIM_PRODUIT>
-- =============================================
CREATE PROCEDURE [bfr].[Alim_T_DIM_Produit] 
AS
BEGIN

  SET NOCOUNT ON;

------------------------------///////////////// Step 1
--insertion dans la table cible

truncate table bfr.T_DIM_PRODUIT

insert into bfr.T_DIM_PRODUIT (CODE_PRODUIT_N5, LIBELLE_PRODUIT_N5, CODE_PRODUIT_N1, LIBELLE_PRODUIT_N1,
                                                   CODE_PRODUIT_N2, LIBELLE_PRODUIT_N2, CODE_PRODUIT_N3, LIBELLE_PRODUIT_N3,
												   CODE_PRODUIT_N4, LIBELLE_PRODUIT_N4)
select distinct convert(varchar(10) ,[code_produit_SIDP])
      ,convert(nvarchar(100) ,[libelle_code_produit_SIDP])
      ,convert(varchar(10) ,[code_produit_n1])
      ,convert(nvarchar(100) ,[libelle_produit_n1])
      ,convert(varchar(10) ,[code_produit_n2])
      ,convert(nvarchar(100) ,[libelle_produit_n2])
      ,convert(varchar(10) ,[code_produit_n3])
      ,convert(nvarchar(100) ,[libelle_produit_n3])
	  ,convert(varchar(10) ,[code_produit_n4])
      ,convert(nvarchar(100) ,[libelle_produit_n4])
      
from [$(DataHubDatabaseName)].[dbo].[REF_TRANSCO_PRODUIT] 

 union all

select distinct convert(varchar(10) ,[code_assurance])
      ,convert(nvarchar(100) ,[libelle_assurance])
      ,convert(varchar(10) ,[code_assurance_n1])
      ,convert(nvarchar(100) ,[libelle_assurance_n1])
      ,convert(varchar(10) ,[code_assurance_n2])
      ,convert(nvarchar(100) ,[libelle_assurance_n2])
      ,convert(varchar(10) ,[code_assurance_n3])
      ,convert(nvarchar(100) ,[libelle_assurance_n3])
	  ,convert(varchar(10) ,[code_assurance_n4])
      ,convert(nvarchar(100) ,[libelle_assurance_n4])
      
from [$(DataHubDatabaseName)].[dbo].[REF_TRANSCO_ASSURANCE] 

insert into [bfr].[T_DIM_PRODUIT] (CODE_PRODUIT_N5,LIBELLE_PRODUIT_N5) values('99999','Valeur non renseignée')

END