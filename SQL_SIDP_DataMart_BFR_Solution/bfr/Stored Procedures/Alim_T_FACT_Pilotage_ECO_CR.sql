﻿
CREATE PROCEDURE [bfr].[Alim_T_FACT_Pilotage_ECO_CR] 
(
   @CurrentDate DATE
)
AS
BEGIN

SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#CREDITS') IS NOT NULL
drop table #CREDITS;

--select *
--  into #CREDITS
--  from ( 

----Crédits Amortissable
--       select  cr.NUMERO_CLIENT as NUMERO_CLIENT_PARENT
--              ,cr.NUMERO_CLIENT
--              ,cra.NUMERO_DOSSIER
--              ,cr.CODE_PRODUIT
--			  ,cra.FFQ_DATE_DERNIER_DEBLOCAGE as DATE_OUVERTURE
--              ,cr.DATE_CLOTURE
--              ,cr.CTVL_CRD as CONTREVALEUR_MONTANT_ENCOURS
--              ,cr.CTVL_MONTANT_CREDIT_DECAISSE as CONTREVALEUR_MONTANT_CREDIT_DECAISSE
--	          ,cr.CTVL_ENCOURS_MOYEN as CONTREVALEUR_MONTANT_ENCOURS_MOYEN
--              ,cr.DUREE_CREDIT
--              ,cr.MONTANT_INTERETS as INTERETS_TOTAUX_MOIS_DEVISE_BASE
--			  ,cra.FFQ_TAUX_INTERET_PALIER as TAUX_INTERET_CONTRACTUEL
--              ,cr.DATE_ACTION
--              ,'CR' as SRC
--              ,case when dateadd(month, datediff(month, 0, cra.FFQ_DATE_DERNIER_DEBLOCAGE), 0) = dateadd(month, datediff(month, 0, cr.DATE_ACTION) - 1, 0)
--			         and (cra.FFQ_DATE_1ERE_ECHEANCE >= cra.FFQ_DATE_DERNIER_DEBLOCAGE or cra.FFQ_DATE_1ERE_ECHEANCE is NULL)   
--                    then 1 else 0 end as FLAG_OUVERTURE
--               ,case when dateadd(month, datediff(month, 0, cr.DATE_CLOTURE), 0) = dateadd(month, datediff(month, 0, cr.DATE_ACTION) - 1, 0)
--			         and cr.CTVL_CRD <= 0                    
--                    then 1 else 0 end as FLAG_CLOTURE
--               ,case when cr.CTVL_CRD > 0
--                    then 1 else 0 end as FLAG_STOCK
--       from [$(DataFactoryBFRDatabaseName)].dbo.BF_CREDIT_AMORTISSABLE cra
--       join [$(DataFactoryBFRDatabaseName)].dbo.BF_CREDITS cr
--         on cra.NUMERO_DOSSIER = cr.NUMERO_DOSSIER and cra.DATE_ACTION = cr.DATE_ACTION
--      where @CurrentDate is null 
--             or ( dateadd(month, datediff(month, 0, cra.DATE_ACTION), 0) = dateadd(month, datediff(month, 0, @CurrentDate), 0) )
--union all

----Crédits Equipement Immo
--       select  cr.NUMERO_CLIENT as NUMERO_CLIENT_PARENT
--              ,cr.NUMERO_CLIENT
--			  ,crei.NUMERO_DOSSIER
--              ,cr.CODE_PRODUIT
--              ,crei.DATE_ENGAGEMENT as DATE_OUVERTURE
--              ,cr.DATE_CLOTURE as DATE_CLOTURE
--              ,cr.CTVL_CRD as CONTREVALEUR_MONTANT_ENCOURS
--              ,cr.CTVL_MONTANT_CREDIT_DECAISSE as CONTREVALEUR_MONTANT_CREDIT_DECAISSE
--              ,cr.CTVL_ENCOURS_MOYEN as CONTREVALEUR_MONTANT_ENCOURS_MOYEN
--              ,cr.DUREE_CREDIT
--              ,cr.MONTANT_INTERETS as INTERETS_TOTAUX_MOIS_DEVISE_BASE
--	          ,crei.TAUX_APPLIQUE as TAUX_INTERET_CONTRACTUEL
--              ,cr.DATE_ACTION
--              ,'CREI' as SRC
--              ,case when dateadd(month, datediff(month, 0, crei.DATE_ENGAGEMENT), 0) = dateadd(month, datediff(month, 0, crei.DATE_ACTION) - 1, 0)
--		             and cr.CTVL_MONTANT_CREDIT_DECAISSE > 0 
--		            then 1 else 0 end as FLAG_OUVERTURE
--              ,case when dateadd(month, datediff(month, 0, cr.DATE_CLOTURE), 0) = dateadd(month, datediff(month, 0, cr.DATE_ACTION) - 1, 0)
--	                 and cr.CTVL_CRD <= 0 
--                    then 1 else 0 end as FLAG_CLOTURE
--              ,case when cr.CTVL_CRD > 0 
--                    then 1 else 0 end as FLAG_STOCK
--       from [$(DataFactoryBFRDatabaseName)].dbo.BF_CREDIT_EQUIPEMENT_IMMO crei
--       join [$(DataFactoryBFRDatabaseName)].dbo.BF_CREDITS cr
--         on crei.NUMERO_DOSSIER = cr.NUMERO_DOSSIER and crei.DATE_ACTION = cr.DATE_ACTION
--      where @CurrentDate is null 
--         or ( dateadd(month, datediff(month, 0, crei.DATE_ACTION), 0) = dateadd(month, datediff(month, 0, @CurrentDate), 0) ) 

--union all

----Crédits Renouvelable
--       select  cr.NUMERO_CLIENT as NUMERO_CLIENT_PARENT
--              ,cr.NUMERO_CLIENT
--              ,crr.NUMERO_DOSSIER
--              ,cr.CODE_PRODUIT
--              ,cr.DATE_OUVERTURE 
--              ,cr.DATE_CLOTURE
--              ,cr.CTVL_CRD as CONTREVALEUR_MONTANT_ENCOURS
--              ,crr.CPT_MONTANT_TOTAL_UTILISE as CONTREVALEUR_MONTANT_CREDIT_DECAISSE -- recuperation du montant du credit utilise du flux quotidien
--              ,cr.CTVL_ENCOURS_MOYEN as CONTREVALEUR_MONTANT_ENCOURS_MOYEN
--              ,cr.DUREE_CREDIT
--	          ,cr.MONTANT_INTERETS as INTERETS_TOTAUX_MOIS_DEVISE_BASE
--	          ,crr.FFM_TAUX_MOYEN as TAUX_INTERET_CONTRACTUEL
--              ,cr.DATE_ACTION
--              ,'CR' as SRC
--	          ,case when dateadd(month, datediff(month, 0, cr.DATE_OUVERTURE), 0) = dateadd(month, datediff(month, 0, cr.DATE_ACTION) - 1, 0)
--	                 and crr.CPT_MONTANT_TOTAL_UTILISE > 0 
--                    then 1 ELSE 0 end as FLAG_OUVERTURE
--	          ,case when dateadd(month, datediff(month, 0, cr.DATE_CLOTURE), 0) = dateadd(month, datediff(month, 0, cr.DATE_ACTION) - 1, 0)
--	                 and cr.CTVL_CRD <= 0
--                    then 1 else 0 end as FLAG_CLOTURE
--              ,case when cr.CTVL_CRD > 0 
--                    then 1 else 0 end as FLAG_STOCK
--       from [$(DataFactoryBFRDatabaseName)].dbo.BF_CREDIT_RENOUVELABLE crr
--       join [$(DataFactoryBFRDatabaseName)].dbo.BF_CREDITS cr
--         on crr.NUMERO_DOSSIER = cr.NUMERO_DOSSIER and crr.DATE_ACTION = cr.DATE_ACTION
--      where @CurrentDate is null 
--         or ( dateadd(month, datediff(month, 0, crr.DATE_ACTION), 0) = dateadd(month, datediff(month, 0, @CurrentDate), 0) )     
--) CREDITS;

--with ind_pvt as (
--select c.CODE_MARQUE as cd_mrq
--      ,c.CODE_MARCHE as cd_mrc
--      ,cr.CODE_PRODUIT as cd_pdt
--      ,'99999' as cd_rsx
--      ,t.CODE_DATE as cd_dt
---- Production brute de crédit 
--      ,count( case when cr.FLAG_OUVERTURE = 1 AND cr.SRC IN ('CREI','CR')
--                   then cr.NUMERO_DOSSIER else NULL end ) as PCR01
---- Valeur de la production brute de crédit
--      ,sum( case when cr.FLAG_OUVERTURE = 1 AND cr.SRC IN ('CREI','CR')
--                 then cr.CONTREVALEUR_MONTANT_CREDIT_DECAISSE else 0 end ) as PCR03
---- Somme des encours moyens de la production brute de crédit
--      ,sum( case when cr.FLAG_OUVERTURE = 1 AND cr.SRC IN ('CREI','CR')
--                 then cr.CONTREVALEUR_MONTANT_ENCOURS_MOYEN else 0 end ) as PCR20
---- Somme de la durée des crédits produits
--      ,sum( case when cr.FLAG_OUVERTURE = 1 AND cr.SRC IN ('CREI','CR')
--                 then cr.DUREE_CREDIT else 0 end ) as PCR21
---- Interet client de la production brute de crédit
--      ,sum( case when cr.FLAG_OUVERTURE = 1 AND cr.SRC IN ('CREI','CR')
--                 then cr.INTERETS_TOTAUX_MOIS_DEVISE_BASE else 0 end ) as PCR22
---- Valeur des tombées de crédit
--      ,sum( case when cr.FLAG_CLOTURE = 1 AND cr.SRC IN ('CREI','CR')
--                 then cr.CONTREVALEUR_MONTANT_ENCOURS else 0 end ) as PCR12
---- Nombre de crédit en stock
--      ,count( case when cr.FLAG_STOCK = 1 AND cr.SRC IN ('CREI','CR')
--                   then cr.NUMERO_DOSSIER else NULL end ) as SCR01
---- Encours de crédit en stock
--      ,sum( case when cr.FLAG_STOCK = 1 AND cr.SRC IN ('CREI','CR')
--                 then cr.CONTREVALEUR_MONTANT_ENCOURS else 0 end ) as SCR03
---- Encours moyen de crédit en stock
--      ,sum( case when cr.FLAG_STOCK = 1 AND cr.SRC IN ('CREI','CR')
--                 then cr.CONTREVALEUR_MONTANT_ENCOURS_MOYEN else 0 end ) as SCR07
---- Encours moyen de crédit en stock
--      ,sum( case when cr.FLAG_STOCK = 1 AND cr.SRC IN ('CREI','CR')
--                 then cr.DUREE_CREDIT else NULL end ) as SCR16
---- Somme des interets client du stock de crédit
--      ,sum( case when cr.FLAG_STOCK = 1 AND cr.SRC IN ('CREI','CR')
--                 then cr.INTERETS_TOTAUX_MOIS_DEVISE_BASE else 0 end ) as SCR17
---- Taux contractuel  x Valeur prod brute de crédit
--     ,sum( case when cr.FLAG_OUVERTURE = 1 AND cr.SRC IN ('CREI','CR')
--				then  cr.TAUX_INTERET_CONTRACTUEL * cr.CONTREVALEUR_MONTANT_CREDIT_DECAISSE else 0 end) as PCR23
---- Calcul du nombre d'equipements pour les crédits
--     ,count( case when ( c.TIERS_COLLECTIF <> 1 and
--                         c.DATE_FIN_RELATION_CLIENT is null or c.DATE_FIN_RELATION_CLIENT >= c.DATE_ACTION ) and
--                         c.DATE_CREATION_CLIENT < DATEADD(MONTH, DATEDIFF(MONTH, 0, c.DATE_ACTION), 0)
--                  then  cr.NUMERO_DOSSIER else NULL end ) as CLI30
--from #CREDITS cr
--left join [$(DataFactoryBFRDatabaseName)].dbo.BF_CLIENTS c
--       on cr.NUMERO_CLIENT = c.NUMERO_CLIENT and cr.DATE_ACTION = c.DATE_ACTION
--left join bfr.T_DIM_PRODUIT p
--       on cr.CODE_PRODUIT = p.CODE_PRODUIT_N5
--left join bfr.T_DIM_TEMPS t
--       on t.DATE_ANNEE_MOIS_JOUR = EOMONTH(cr.DATE_ACTION, -1)
--where c.CODE_MARQUE is not null
--  and c.CODE_MARCHE is not null
--  and cr.CODE_PRODUIT is not null
--group by c.CODE_MARQUE
--        ,c.CODE_MARCHE
--        ,cr.CODE_PRODUIT
--        ,t.CODE_DATE
--),

----Unpivot the table.
--ind_unpvt as (
--select cd_mrq, cd_mrc, cd_pdt, cd_rsx, cd_dt
--     , cd_ind, vl_ind
--  from ( select cd_mrq, cd_mrc, cd_pdt, cd_rsx, cd_dt, 
--                cast(PCR01 as float) as PCR01, cast(PCR03 as float) as PCR03, cast(PCR20 as float) as PCR20, cast(PCR21 as float) as PCR21, cast(PCR22 as float) as PCR22, cast(PCR12 as float) as PCR12,
--                cast(SCR01 as float) as SCR01, cast(SCR03 as float) as SCR03, cast(SCR07 as float) as SCR07, cast(SCR16 as float) as SCR16, cast(SCR17 as float) as SCR17,
--				cast(PCR23 as float) as PCR23,
--                cast(CLI30 as float) as CLI30
--           from ind_pvt ) p
--         UNPIVOT ( vl_ind for cd_ind IN (PCR01, PCR03, PCR20, PCR21, PCR22, PCR12, SCR01, SCR03, SCR07, SCR16, SCR17, PCR23, CLI30)
--       ) as unpvt
--)

--insert into [bfr].[T_FACT_PILOTAGE_ECO]
--select *
--     , getdate() as dt_act
--  from ind_unpvt
-- where vl_ind <> 0

END
GO