﻿CREATE PROCEDURE [bfr].[Alim_T_FACT_Pilotage_ECO_CLI] 
(
   @CurrentDate DATE
)
AS
BEGIN

SET NOCOUNT ON;

----Création de la table temporaire, comportant les clients ayant au moins un compte ou un crédit
--IF OBJECT_ID('tempdb..#CLIENTS') IS NOT NULL
--drop table #CLIENTS;

--select cl.* 
--     , CASE WHEN dateadd(month, datediff(month, 0, cl.DATE_CREATION_CLIENT), 0) = dateadd(month, datediff(month, 0, cl.DATE_ACTION) - 1, 0)
--            THEN 1 ELSE 0 END as FLAG_ENTREE_RELATION
--     , CASE WHEN dateadd(month, datediff(month, 0, cl.DATE_FIN_RELATION_CLIENT), 0) = dateadd(month, datediff(month, 0, cl.DATE_ACTION) - 1, 0)
--            THEN 1 ELSE 0 END as FLAG_FIN_RELATION
--     , CASE WHEN cl.DATE_FIN_RELATION_CLIENT is null or cl.DATE_FIN_RELATION_CLIENT >= cl.DATE_ACTION THEN 1 ELSE 0 END as FLAG_STOCK
--     , convert(varchar(8), DATE_ACTION, 112) + '###' + NUMERO_CLIENT as NUMERO_CLIENT_C
--into #CLIENTS
--from [$(DataFactoryBFRDatabaseName)].dbo.BF_CLIENTS cl
 
--/*------------------------------------------------------------------------------------------------------------------------------------------------*/

--select * 
--into #CLIENTS2
--from #CLIENTS cl1
--where ( @CurrentDate is null or ( dateadd(month, datediff(month, 0, cl1.DATE_ACTION), 0) = dateadd(month, datediff(month, 0, @CurrentDate), 0) ) )
    
--	and cl1.NUMERO_CLIENT_C in ( 

---- Client ayant un compte ou un crédit FF : cas tiers collectif = 0

--select NUMERO_CLIENT_C
--from 
--	  (
--	  select cl1.NUMERO_CLIENT_C,                 
--			 ROW_NUMBER() OVER(PARTITION BY NUMERO_CLIENT_C ORDER BY co_hcj.NUMERO_COMPTE) rang               
--      from #CLIENTS cl1                        
--      inner join [$(DataFactoryBFRDatabaseName)].dbo.BF_COMPTES co_hcj
--               on co_hcj.TITULAIRE_PRINCIPAL = cl1.NUMERO_CLIENT 
--	 	      and co_hcj.DATE_ACTION = cl1.DATE_ACTION
--		      and co_hcj.DATE_CLOTURE is NULL
--		      and (
--                  (co_hcj.CODE_PRODUIT like 'CAV%' and co_hcj.CODE_PRODUIT not in ('CAV035','CAV037','CAV039','CAV041','CAV042','CAV043','CAV045','CAV049','CAV051','CAV055','CAV068','CAV069')) or
--		           --co_hcj.CODE_PRODUIT like 'CAV%' or
--		           co_hcj.CODE_PRODUIT like 'EPA%' or
--			       co_hcj.CODE_PRODUIT like 'CTP%' or
--			       co_hcj.CODE_PRODUIT = 'AUC001' or
--			       co_hcj.CODE_PRODUIT = 'AUC002' or
--			       co_hcj.CODE_PRODUIT = 'AUC003' or
--			       co_hcj.CODE_PRODUIT = 'AUC004' or
--			       co_hcj.CODE_PRODUIT = 'AUC005' or
--			       co_hcj.CODE_PRODUIT = 'AUC006'
--			      )
--      where (@CurrentDate is null or (dateadd(month,datediff(month,0,cl1.DATE_ACTION),0)=dateadd(month,datediff(month,0,@CurrentDate),0)))
--	    and cl1.TIERS_COLLECTIF = 0 
--		) as rq1
--	  where rq1.rang = 1

--      union

---- Client ayant un compte ou un crédit FF : cas tiers collectif = 1

--select NUMERO_CLIENT_C 
--from 
--	  (
--	  select convert(varchar(8), cl1.DATE_ACTION, 112) + '###' + cl_co.NUMERO_CLIENT_FILS as NUMERO_CLIENT_C
--	  ,ROW_NUMBER() OVER(PARTITION BY (convert(varchar(8), cl1.DATE_ACTION, 112) + '###' + cl_co.NUMERO_CLIENT_FILS) ORDER BY cl_co.NUMERO_CLIENT_PARENT) rang
--      from #CLIENTS cl1               
--      inner join [$(DataFactoryBFRDatabaseName)].dbo.BF_COMPTES co_hcj
--              on co_hcj.TITULAIRE_PRINCIPAL = cl1.NUMERO_CLIENT 
--  		     and co_hcj.DATE_ACTION = cl1.DATE_ACTION
--		     and co_hcj.DATE_CLOTURE is NULL
--		     and (
--			     (co_hcj.CODE_PRODUIT like 'CAV%' and co_hcj.CODE_PRODUIT not in ('CAV035','CAV037','CAV039','CAV041','CAV042','CAV043','CAV045','CAV049','CAV051','CAV055','CAV068','CAV069')) or
--		          co_hcj.CODE_PRODUIT like 'EPA%' or
--			      co_hcj.CODE_PRODUIT like 'CTP%' or
--			      co_hcj.CODE_PRODUIT = 'AUC001' or
--			      co_hcj.CODE_PRODUIT = 'AUC002' or
--			      co_hcj.CODE_PRODUIT = 'AUC003' or
--			      co_hcj.CODE_PRODUIT = 'AUC004' or
--			      co_hcj.CODE_PRODUIT = 'AUC005' or
--			      co_hcj.CODE_PRODUIT = 'AUC006'
--				 ) 
--	  inner join [$(DataFactoryBFRDatabaseName)].dbo.BF_CLIENTS_COMPTES cl_co
--		       on co_hcj.NUMERO_CLIENT = cl_co.NUMERO_CLIENT_PARENT   
--		      and co_hcj.DATE_ACTION = cl_co.DATE_ACTION              /* ajout DATE_ACTION */
--	        join [$(DataFactoryBFRDatabaseName)].dbo.BF_CLIENTS cl2        
--               on cl_co.NUMERO_CLIENT_FILS = cl2.NUMERO_CLIENT  
--		      and cl_co.DATE_ACTION = cl2.DATE_ACTION                 /* ajout DATE_ACTION */
--      where (@CurrentDate is null or (dateadd(month, datediff(month,0,cl1.DATE_ACTION),0) = dateadd(month, datediff(month,0,@CurrentDate),0)))
--		and cl1.TIERS_COLLECTIF = 1 
--      ) as rq2
--	  where rq2.rang = 1

--      union

---- Client ayant un crédit SAB : cas tiers collectif = 0

--select NUMERO_CLIENT_C
--from 
--	 (    
--	 select cl1.NUMERO_CLIENT_C, 
--	        ROW_NUMBER() OVER(PARTITION BY cl1.NUMERO_CLIENT_C ORDER BY cr_hcj.[NUMERO_DOSSIER]) rang 
--     from #CLIENTS cl1      
--     inner join [$(DataFactoryBFRDatabaseName)].dbo.BF_CREDITS cr_hcj
--              on cr_hcj.NUMERO_CLIENT = cl1.NUMERO_CLIENT
--             and cr_hcj.DATE_CLOTURE > cl1.DATE_EXPLOITATION
--	         and cr_hcj.CODE_PRODUIT not like 'DES%'
--	         and cr_hcj.CODE_PRODUIT not like 'COM%'
--	         and cr_hcj.DATE_ACTION = cl1.DATE_ACTION   
--     where (@CurrentDate is null or (dateadd(month,datediff(month, 0,cl1.DATE_ACTION),0) = dateadd(month,datediff(month,0,@CurrentDate),0)))
--	   and cl1.TIERS_COLLECTIF = 0 
--	 ) as rq3
--     where rq3.rang = 1
      
--     union

---- Client ayant un crédit SAB : cas tiers collectif = 1

--select NUMERO_CLIENT_C 
--from 
--	 ( 
--     select convert(varchar(8),cl1.DATE_ACTION, 112)+'###'+cl_co.NUMERO_CLIENT_FILS as NUMERO_CLIENT_C,
--		    ROW_NUMBER() OVER(PARTITION BY (convert(varchar(8),cl1.DATE_ACTION, 112)+'###'+cl_co.NUMERO_CLIENT_FILS) ORDER BY cl_co.NUMERO_CLIENT_PARENT) rang 
--     from #CLIENTS cl1
--     inner join [$(DataFactoryBFRDatabaseName)].dbo.BF_CREDITS cr_hcj
--              on cr_hcj.NUMERO_CLIENT = cl1.NUMERO_CLIENT 
--             and cr_hcj.DATE_CLOTURE > cl1.DATE_EXPLOITATION
--	         and cr_hcj.CODE_PRODUIT not like 'DES%'
--	         and cr_hcj.CODE_PRODUIT not like 'COM%'
--	         and cr_hcj.DATE_ACTION = cl1.DATE_ACTION
--	 inner join [$(DataFactoryBFRDatabaseName)].dbo.BF_CLIENTS_COMPTES cl_co
--	          on cr_hcj.NUMERO_CLIENT = cl_co.NUMERO_CLIENT_PARENT 
--	         and cr_hcj.DATE_ACTION = cl_co.DATE_ACTION              /* ajout DATE_ACTION */
--	  left join [$(DataFactoryBFRDatabaseName)].dbo.BF_CLIENTS cl4
--              on cl_co.NUMERO_CLIENT_FILS = cl4.NUMERO_CLIENT
--             and cl_co.DATE_ACTION = cl4.DATE_ACTION                 /* ajout DATE_ACTION */
--     where (@CurrentDate is null or (dateadd(month,datediff(month,0,cl1.DATE_ACTION),0) = dateadd(month,datediff(month,0,@CurrentDate),0)))
--	   and cl1.TIERS_COLLECTIF = 1 
--	 ) as rq4
--	 where rq4.rang = 1
--)
--;

--with ind_pvt as (
--select CODE_MARQUE as cd_mrq
--     , CODE_MARCHE as cd_mrc
--     , '99999' as cd_pdt
--     , '99999' as cd_rsx
--     , t.CODE_DATE as cd_dt
--       -- Nombre de client en stock
--     , count(CASE WHEN c.FLAG_STOCK = 1 and
--                       c.DATE_CREATION_CLIENT < DATEADD(MONTH, DATEDIFF(MONTH, 0, c.DATE_ACTION), 0)
--                  THEN NUMERO_CLIENT ELSE NULL END) as CLI01
--       -- Nombre d'entrée en relation
--     , count(CASE WHEN c.FLAG_ENTREE_RELATION = 1
--                  THEN NUMERO_CLIENT ELSE NULL END) as CLI02
--       -- Nombre de fin de relation
--     , count(CASE WHEN c.FLAG_FIN_RELATION = 1
--                  THEN NUMERO_CLIENT ELSE NULL END) as CLI05
--       -- Nombre clients actifs en stock
--     , count(CASE WHEN c.FLAG_STOCK = 1 and
--                       c.DATE_CREATION_CLIENT < DATEADD(MONTH, DATEDIFF(MONTH, 0, c.DATE_ACTION), 0) and
--                       c.ACTIF = 1
--                  THEN NUMERO_CLIENT ELSE NULL END) as CLI28
--       -- Somme ancienneté à la fin de relation (par le niveau le plus fin)
--     , sum(CASE WHEN c.FLAG_FIN_RELATION = 1
--                THEN year(c.DATE_FIN_RELATION_CLIENT)-YEAR(c.DATE_CREATION_CLIENT) ELSE 0 END) as CLI29
--from #CLIENTS2 c
--left join bfr.T_DIM_TEMPS t
--    on t.DATE_ANNEE_MOIS_JOUR = EOMONTH(c.DATE_ACTION, -1)
-- where c.CODE_MARQUE is not null
--   and c.CODE_MARCHE is not null
--group by CODE_MARQUE  
--       , CODE_MARCHE 
--       , t.CODE_DATE
--),

----Unpivot the table.
--ind_unpvt as (
--SELECT cd_mrq, cd_mrc, cd_pdt, cd_rsx, cd_dt
--     , cd_ind, vl_ind
--  FROM ( SELECT cd_mrq, cd_mrc, cd_pdt, cd_rsx, cd_dt, CLI01, CLI02, CLI05, CLI28, CLI29 FROM ind_pvt ) p
--         UNPIVOT ( vl_ind FOR cd_ind IN (CLI01, CLI02, CLI05, CLI28, CLI29)
--       ) AS unpvt
--)

--insert into [bfr].[T_FACT_PILOTAGE_ECO]
--select *
--     , getdate() as dt_act
--  from ind_unpvt
-- where vl_ind <> 0

END
GO