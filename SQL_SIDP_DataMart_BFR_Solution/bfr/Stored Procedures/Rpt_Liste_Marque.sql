﻿CREATE PROCEDURE [bfr].[Rpt_Liste_Marque] (
    @p_ReportName VARCHAR(255)
  , @p_ReportDate DATE
  , @p_ReportServerShare VARCHAR(255)
)
AS
BEGIN

SET LANGUAGE FRENCH;

DECLARE @ReportDate DATE = COALESCE(@p_ReportDate, dateadd(month, -1, getdate()));

SELECT 'CODE MARQUE N3 : ' + [CODE_MARQUE_N3] AS Source
     , '{ [MARQUE].[CODE MARQUE N3].[CODE MARQUE N3].&[' + [CODE_MARQUE_N3] + '] }' AS P_Filtre_Marque
     , '[TEMPS].[Hierarchy].[DATE MOIS].&[' + cast(year(@ReportDate) as varchar(4)) + ']&[' + cast(month(@ReportDate) as CHAR(2)) + ']' AS P_Periode
     , @p_ReportServerShare as Path
     , @p_ReportName + '_' + replace(replace([LIBELLE_MARQUE_N3], ' ', '_'), '''', '_') + '_' + datename(year, @ReportDate) + '_' + datename(month, @ReportDate) AS FileName
  FROM [bfr].[T_DIM_MARQUE]
 WHERE [CODE_MARQUE_N3] IN ('MGAG001', 'MGAM001', 'MGAS001', 'MGAP001', 'MGCA001', 'MGCM001', 'MGGE001', 'MGLB001', 'MGNE001', 'MGOC001', 'MGOI001', 'MGPR001', 'MGPV001', 'MGRA001')
UNION
SELECT '' AS Source
     , '-{ [MARQUE].[CODE MARQUE N1].[CODE MARQUE N1].&[] }' AS P_Filtre_Marque
     , '[TEMPS].[Hierarchy].[DATE MOIS].&[' + cast(year(@ReportDate) as varchar(4)) + ']&[' + cast(month(@ReportDate) as CHAR(2)) + ']' AS P_Periode
     , @p_ReportServerShare as Path
     , @p_ReportName + '_Toutes_marques_' + datename(year, @ReportDate) + '_' + datename(month, @ReportDate) AS FileName
ORDER BY 1;

END