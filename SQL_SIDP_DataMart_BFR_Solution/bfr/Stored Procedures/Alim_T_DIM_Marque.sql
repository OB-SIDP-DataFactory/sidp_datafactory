﻿

-- =============================================
-- Author:		<Ali Fassahi>
-- Create date: <25/04/2017>
-- Description:	<Alimentation de la table de dimension T_DIM_MARQUE>
-- =============================================
CREATE PROCEDURE [bfr].[Alim_T_DIM_Marque] 
AS
BEGIN

  SET NOCOUNT ON;

------------------------------///////////////// Step 1
--insertino dans la table cible

truncate table bfr.T_DIM_MARQUE

insert into bfr.T_DIM_MARQUE (CODE_MARQUE_N3, LIBELLE_MARQUE_N3, CODE_MARQUE_N1, LIBELLE_MARQUE_N1, 
                                                  CODE_MARQUE_N2, LIBELLE_MARQUE_N2)
select distinct convert(varchar(10) ,[code_marque])
      ,convert(nvarchar(30) ,[libelle_marque])
      ,convert(varchar(10) ,[code_marque_n1])
      ,convert(nvarchar(30) ,[libelle_marque_n1])
      ,convert(varchar(10) ,[code_marque_n2])
      ,convert(nvarchar(30) ,[libelle_marque_n2])
      
from [$(DataHubDatabaseName)].[dbo].[REF_TRANSCO_MARQUE]

insert into [bfr].[T_DIM_MARQUE](code_marque_n3,libelle_marque_n3) values('99999','Valeur non renseignée')
END
