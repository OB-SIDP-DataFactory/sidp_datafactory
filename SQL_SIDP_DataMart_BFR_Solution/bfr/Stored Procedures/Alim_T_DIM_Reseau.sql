﻿CREATE PROCEDURE [bfr].[Alim_T_DIM_Reseau] 
AS
BEGIN
	SET NOCOUNT ON;
	------------------------------///////////////// Step 1
	--insertion dans la table cible

	TRUNCATE TABLE bfr.T_DIM_RESEAU

	INSERT INTO bfr.T_DIM_RESEAU
	(
		CODE_RESEAU_N3,
		LIBELLE_RESEAU_N3,
		CODE_RESEAU_N2,
		LIBELLE_RESEAU_N2,
		CODE_RESEAU_N1,
		LIBELLE_RESEAU_N1
	)
	SELECT DISTINCT
		CONVERT(VARCHAR(10) ,[code_reseau]),
		CONVERT(NVARCHAR(30) ,[libelle_reseau]),
		CONVERT(VARCHAR(10) ,[code_reseau_n2]),
		CONVERT(NVARCHAR(30) ,[libelle_reseau_n2]),
		CONVERT(VARCHAR(10) ,[code_reseau_n1]),
		CONVERT(NVARCHAR(30) ,[libelle_reseau_n1])
	from
		[$(DataHubDatabaseName)].[dbo].[REF_TRANSCO_RESEAU]

	INSERT INTO [bfr].[T_DIM_RESEAU]
	(
		CODE_RESEAU_N3,
		LIBELLE_RESEAU_N3
	)
	VALUES
	(
		'99999',
		'Valeur non renseignée'
	)
END