﻿CREATE TABLE [bfr].[T_FACT_CLIENT_SCORING] (
    [NUMERO_CLIENT]           VARCHAR (7)     NOT NULL,
    [DATE_ACTION]             DATE            NOT NULL,
    [VERSION_SCORE]           DECIMAL (3)     NOT NULL,
    [AGE]                     DECIMAL (3)     NULL,
    [TAUX_UTILISATION]        DECIMAL (3)     NULL,
    [NB_JOUR_DEP_12M]         DECIMAL (3)     NULL,
    [MIN_FLUX_CREDITEUR_6M]   DECIMAL (18, 3) NULL,
    [EPARGNE_BILANCIELLE]     DECIMAL (18, 3) NULL,
    [CUMUL_FLUX_CREDITEUR_3M] DECIMAL (18, 3) NULL,
    [SOLDE_MOYEN_DEBIT]       DECIMAL (18, 3) NULL,
    [NOTE_SCORE]              DECIMAL (3)     NULL,
    [ID_CR]                   BIGINT          NULL,
    [SCORE_ER]                VARCHAR (3)     NULL,
    [ID_SCORE_COMP]           BIGINT          NULL,
    [ID_SEG_RISK]             BIGINT          NULL,
    [CANAL_DISTRIBUTION]      NVARCHAR (255)  NULL,
    [DAT_CRTN_ENRG]           DATETIME        NULL,
    [DAT_DERN_MODF_ENRG]      DATETIME        NULL,
    [FLG_ACTIF]               BIT             NULL,
    CONSTRAINT [PK_T_FACT_CLIENT_SCORING] PRIMARY KEY CLUSTERED ([DATE_ACTION] DESC, [VERSION_SCORE] ASC, [NUMERO_CLIENT] ASC)
);

