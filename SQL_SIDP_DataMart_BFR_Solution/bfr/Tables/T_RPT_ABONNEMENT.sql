﻿CREATE TABLE [bfr].[T_RPT_ABONNEMENT] (
    [ReportName]              VARCHAR (100) NOT NULL,
    [ReportPath]              VARCHAR (100) NOT NULL,
    [SubscriptionName]        VARCHAR (100) NOT NULL,
    [SubscriptionSP]          VARCHAR (100) NOT NULL,
    [RenderFormat]            VARCHAR (10)  NOT NULL,
    [ReportDate]              VARCHAR (10)  NULL,
    [RunSubscription]         TINYINT       NOT NULL,
    [SubscriptionLastRunDate] DATETIME      NULL
);

