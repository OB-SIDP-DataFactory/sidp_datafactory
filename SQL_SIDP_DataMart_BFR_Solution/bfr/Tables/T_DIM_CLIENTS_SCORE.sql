﻿CREATE TABLE [bfr].[T_DIM_CLIENTS_SCORE] (
    [NUMERO_CLIENT]          VARCHAR (7) NOT NULL,
    [DATE_ACTION]            DATE        NOT NULL,
    [INDC_CONTENTIEUX]       BIT         DEFAULT ((0)) NULL,
    [INDC_IMPAYE_AN]         BIT         DEFAULT ((0)) NULL,
    [INDC_PP]                BIT         DEFAULT ((0)) NULL,
    [INDC_BANQUE_PRIVEE]     BIT         DEFAULT ((0)) NULL,
    [INDC_DECEDE]            BIT         DEFAULT ((0)) NULL,
    [INDC_COTATION_SCORE]    BIT         DEFAULT ((0)) NULL,
    [INDC_CAPABLE_JURIDIQUE] BIT         DEFAULT ((0)) NULL,
    [INDC_ANCIENNETE_6M]     BIT         DEFAULT ((0)) NULL,
    [INDC_PRET_PERSONNEL]    BIT         DEFAULT ((0)) NULL,
    [INDC_PRET_RECENT]       BIT         DEFAULT ((0)) NULL,
    [INDC_PRET_DECAISSE]     BIT         DEFAULT ((0)) NULL,
    [INDC_REVOLVING]         BIT         DEFAULT ((0)) NULL,
    [DAT_CRTN_ENRG]          DATETIME    NULL,
    [DAT_DERN_MODF_ENRG]     DATETIME    NULL,
    [FLG_ACTIF]              BIT         NULL,
    CONSTRAINT [PK_T_DIM_CR] PRIMARY KEY CLUSTERED ([DATE_ACTION] DESC, [NUMERO_CLIENT] DESC)
);

