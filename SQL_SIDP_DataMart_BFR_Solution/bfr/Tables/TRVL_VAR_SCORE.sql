﻿CREATE TABLE [bfr].[TRVL_VAR_SCORE] (
    [NUMERO_CLIENT]                 VARCHAR (7)     NOT NULL,
    [DATE_ACTION]                   DATE            NOT NULL,
    [AGE]                           DECIMAL (3)     NULL,
    [TAUX_UTILISATION]              DECIMAL (3)     NULL,
    [DATE_CREATION_CLIENT]          DATE            NULL,
    [DATE_FIN_RELATION_CLIENT]      DATE            NULL,
    [NB_JOUR_DEP_12M]               DECIMAL (3)     NULL,
    [MIN_FLUX_CREDITEUR_6M]         DECIMAL (18, 3) NULL,
    [CUMUL_FLUX_CREDITEUR_3M]       DECIMAL (18, 3) NULL,
    [EPARGNE_BILANCIELLE]           DECIMAL (18, 3) NULL,
    [SOLDE_MOYEN_DEBIT]             DECIMAL (18, 3) NULL,
    [SITUATION_FAMILIALE]           CHAR (2)     NULL,
    [CODE_MARCHE]                   VARCHAR (25)    NULL,
    [CANAL_ORIGINE]                 VARCHAR (25)    NULL,
    [RESEAU_DISTRIBUTEUR]           NVARCHAR (255)  NULL,
    [CANAL_DISTRIBUTION]            NVARCHAR (255)  NULL,
    [SCORE_PREATTRIBUTION_ORANGE]   NVARCHAR (255)  NULL,
    [SCORE_PREATTRIBUTION_GROUPAMA] NVARCHAR (255)  NULL,
    [SCORE_ER]                      NVARCHAR (255)  NULL,
    [COTATION_RISQUE]               CHAR (2)     NULL,
    CONSTRAINT [PK_TRVL_VAR_SCORE] PRIMARY KEY CLUSTERED ([DATE_ACTION] DESC, [NUMERO_CLIENT] ASC)
);

