﻿CREATE TABLE [bfr].[T_FACT_PILOTAGE_ECO] (
    [CODE_MARQUE]       VARCHAR (10) NOT NULL,
    [CODE_MARCHE]       VARCHAR (10) NOT NULL,
    [CODE_PRODUIT]      VARCHAR (10) NOT NULL,
    [CODE_RESEAU]       VARCHAR (10) NOT NULL,
    [CODE_DATE]         VARCHAR (10) NOT NULL,
    [CODE_INDICATEUR]   VARCHAR (10) NOT NULL,
    [VALEUR_INDICATEUR] FLOAT (53)   NOT NULL,
    [DATE_ACTION]       DATE         NOT NULL,
    CONSTRAINT [PK_T_FACT_PILOTAGE_ECO1] PRIMARY KEY CLUSTERED ([CODE_MARQUE] ASC, [CODE_MARCHE] ASC, [CODE_PRODUIT] ASC, [CODE_RESEAU] ASC, [CODE_DATE] ASC, [CODE_INDICATEUR] ASC)
);


