CREATE TABLE [dbo].[DWH_M_EQPM_COMP_ACTV] (
    [DAT_OBSR]           DATE         NULL,
    [NUMR_COMP]          VARCHAR (20) NULL,
    [COD_RUBR]           VARCHAR (10) NULL,
    [COD_ACTV_COMP]      VARCHAR (2)  NULL,
    [LIBL_ACTV_COMP]     VARCHAR (50) NULL,
    [DAT_CRTN_ENRG]      DATETIME     NULL,
    [DAT_DERN_MODF_ENRG] DATETIME     NULL,
    [FLG_ENRG_COUR]      BIT          NULL
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_M_EQPM_COMP_ACTV', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_M_EQPM_COMP_ACTV', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Activité Compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_M_EQPM_COMP_ACTV', @level2type = N'COLUMN', @level2name = N'LIBL_ACTV_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Activité Compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_M_EQPM_COMP_ACTV', @level2type = N'COLUMN', @level2name = N'COD_ACTV_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_M_EQPM_COMP_ACTV', @level2type = N'COLUMN', @level2name = N'NUMR_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Rubrique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_M_EQPM_COMP_ACTV', @level2type = N'COLUMN', @level2name = N'COD_RUBR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_M_EQPM_COMP_ACTV', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Courant Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_M_EQPM_COMP_ACTV', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';

