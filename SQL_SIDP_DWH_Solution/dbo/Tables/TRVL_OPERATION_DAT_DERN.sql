﻿CREATE TABLE [dbo].[TRVL_OPERATION_DAT_DERN] (
    [NUMR_COMP]               VARCHAR (20) NOT NULL,
    [DAT_DERN_OPRT]           DATE         NULL,
    [IDNT_TYP_DERN_OPRT]      INT          NULL,
    [DAT_DERN_UTLS_PAIE_MOBL] DATE         NULL,
    [DAT_DERN_UTLS_PAIE_CB]   DATE         NULL,
    [DAT_DERN_OPRT_DEBT]      DATE         NULL,
    [DAT_DERN_OPRT_CRDT]      DATE         NULL,
    [DAT_CRTN_ENRG]           DATETIME     NULL,
    [DAT_DERN_MODF_ENRG]      DATETIME     NULL
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Dernière Modification de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRVL_OPERATION_DAT_DERN', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Création de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRVL_OPERATION_DAT_DERN', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Dernière Opération Créditrice', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRVL_OPERATION_DAT_DERN', @level2type = N'COLUMN', @level2name = N'DAT_DERN_OPRT_CRDT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Dernière Opération Débitrice', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRVL_OPERATION_DAT_DERN', @level2type = N'COLUMN', @level2name = N'DAT_DERN_OPRT_DEBT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Dernière Utilisation Paiement CB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRVL_OPERATION_DAT_DERN', @level2type = N'COLUMN', @level2name = N'DAT_DERN_UTLS_PAIE_CB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Dernière Utilisation Paiement Mobile', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRVL_OPERATION_DAT_DERN', @level2type = N'COLUMN', @level2name = N'DAT_DERN_UTLS_PAIE_MOBL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Type Dernière Opération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRVL_OPERATION_DAT_DERN', @level2type = N'COLUMN', @level2name = N'IDNT_TYP_DERN_OPRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Dernière Opération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRVL_OPERATION_DAT_DERN', @level2type = N'COLUMN', @level2name = N'DAT_DERN_OPRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro de compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRVL_OPERATION_DAT_DERN', @level2type = N'COLUMN', @level2name = N'NUMR_COMP';

