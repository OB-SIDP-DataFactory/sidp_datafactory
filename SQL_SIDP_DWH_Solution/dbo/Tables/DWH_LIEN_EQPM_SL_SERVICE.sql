﻿CREATE TABLE [dbo].[DWH_LIEN_EQPM_SL_SERVICE] (
    [DAT_OBSR]           DATE            NOT NULL,
    [IDNT_EQPM]          INT             NOT NULL,
    [IDNT_SERV]          INT             NULL,
    [IDNT_STTS]          INT             NULL,
    [IDNT_CLNT]          NUMERIC (38)    NULL,
    [NUMR_AGRM]          INT             NULL,
    [TYP_RISQ_ASSR_1]    NVARCHAR (255)  NULL,
    [TAUX_RISQ_ASSR_1]   DECIMAL (19, 7) NULL,
    [TYP_RISQ_ASSR_2]    NVARCHAR (255)  NULL,
    [TAUX_RISQ_ASSR_2]   DECIMAL (19, 7) NULL,
    [LIST_GARN]          NVARCHAR (4000) NULL,
    [DAT_CRTN]           DATETIME2 (6)   NULL,
    [IDNT_CRTN]          NVARCHAR (255)  NULL,
    [DAT_DERN_MODF]      DATETIME2 (6)   NULL,
    [IDNT_DERN_MODF]     NVARCHAR (255)  NULL,
    [TYP_UTLS]           NVARCHAR (255)  NULL,
    [DAT_CRTN_ENRG]      DATETIME        NULL,
    [DAT_DERN_MODF_ENRG] DATETIME        NULL,
    [FLG_ENRG_COUR]      BIT             NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere modification enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_EQPM_SL_SERVICE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date creation enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_EQPM_SL_SERVICE', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type utilisateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_EQPM_SL_SERVICE', @level2type = N'COLUMN', @level2name = N'TYP_UTLS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_EQPM_SL_SERVICE', @level2type = N'COLUMN', @level2name = N'IDNT_DERN_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_EQPM_SL_SERVICE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_EQPM_SL_SERVICE', @level2type = N'COLUMN', @level2name = N'IDNT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_EQPM_SL_SERVICE', @level2type = N'COLUMN', @level2name = N'DAT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Liste de garantie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_EQPM_SL_SERVICE', @level2type = N'COLUMN', @level2name = N'LIST_GARN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux de risque d''assurance 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_EQPM_SL_SERVICE', @level2type = N'COLUMN', @level2name = N'TAUX_RISQ_ASSR_2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type de risque d''assurance 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_EQPM_SL_SERVICE', @level2type = N'COLUMN', @level2name = N'TYP_RISQ_ASSR_2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux de risque d''assurance 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_EQPM_SL_SERVICE', @level2type = N'COLUMN', @level2name = N'TAUX_RISQ_ASSR_1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type de risque d''assurance 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_EQPM_SL_SERVICE', @level2type = N'COLUMN', @level2name = N'TYP_RISQ_ASSR_1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro agrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_EQPM_SL_SERVICE', @level2type = N'COLUMN', @level2name = N'NUMR_AGRM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_EQPM_SL_SERVICE', @level2type = N'COLUMN', @level2name = N'IDNT_CLNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant status', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_EQPM_SL_SERVICE', @level2type = N'COLUMN', @level2name = N'IDNT_STTS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant service', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_EQPM_SL_SERVICE', @level2type = N'COLUMN', @level2name = N'IDNT_SERV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant equipement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_EQPM_SL_SERVICE', @level2type = N'COLUMN', @level2name = N'IDNT_EQPM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_EQPM_SL_SERVICE', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Courant Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_EQPM_SL_SERVICE', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';

