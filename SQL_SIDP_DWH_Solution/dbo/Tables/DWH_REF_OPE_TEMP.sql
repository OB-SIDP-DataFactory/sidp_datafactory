﻿CREATE TABLE [dbo].[DWH_REF_OPE_TEMP] (
    [IDNT_TYP_OPRT]      INT           NULL,
    [IDNT_SOUS_TYP_OPRT] INT           NULL,
    [LIBL_TYP_OPRT]      VARCHAR (255) NULL,
    [LIBL_SOUS_TYP_OPRT] VARCHAR (255) NULL,
    [FLG_INTT_CLNT]      VARCHAR (3)   NULL,
    [DAT_DEB_VALD]       DATE          NULL,
    [DAT_FIN_VALD]       DATE          NULL,
    [FLG_ENRG_COUR]      INT           NULL
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Fin Validité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_OPE_TEMP', @level2type = N'COLUMN', @level2name = N'DAT_FIN_VALD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Début Validité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_OPE_TEMP', @level2type = N'COLUMN', @level2name = N'DAT_DEB_VALD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Initiative Client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_OPE_TEMP', @level2type = N'COLUMN', @level2name = N'FLG_INTT_CLNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Sous-Type Opération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_OPE_TEMP', @level2type = N'COLUMN', @level2name = N'LIBL_SOUS_TYP_OPRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Type Opération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_OPE_TEMP', @level2type = N'COLUMN', @level2name = N'LIBL_TYP_OPRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Sous-Type Opération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_OPE_TEMP', @level2type = N'COLUMN', @level2name = N'IDNT_SOUS_TYP_OPRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Type Opération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_OPE_TEMP', @level2type = N'COLUMN', @level2name = N'IDNT_TYP_OPRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Courant Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_OPE_TEMP', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';

