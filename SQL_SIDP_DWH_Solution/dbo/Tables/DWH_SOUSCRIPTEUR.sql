﻿CREATE TABLE [dbo].[DWH_SOUSCRIPTEUR] (
    [DAT_OBSR]                 DATE             NOT NULL,
    [IDNT_SOUS]                NVARCHAR (18)    NOT NULL,
    [IDNT_PERS_SF]             NVARCHAR (18)    NULL,
    [NUMR_PERS_SF]             NVARCHAR (11)    NULL,
    [NUMR_CLNT_SAB]            NVARCHAR (15)    NULL,
    [IDNT_TYP_ENRG]            NVARCHAR (18)    NULL,
    [FLG_SUPP]                 BIT              NULL,
    [IDNT_ENRG_PRNC]           NVARCHAR (18)    NULL,
    [FLG_PERS]                 BIT              NULL,
    [CIVL]                     NVARCHAR (40)    NULL,
    [NOM]                      NVARCHAR (80)    NULL,
    [PRNM]                     NVARCHAR (40)    NULL,
    [NOM_COMP]                 NVARCHAR (121)   NULL,
    [NOM_PHNT]                 NVARCHAR (255)   NULL,
    [PRNM_PHNT]                NVARCHAR (255)   NULL,
    [NOM_NAIS]                 NVARCHAR (80)    NULL,
    [NOM_NAIS_PHNT]            NVARCHAR (255)   NULL,
    [DAT_NAIS]                 DATE             NULL,
    [COD_POST_NAIS]            NVARCHAR (5)     NULL,
    [COMM_NAIS]                NVARCHAR (255)   NULL,
    [DEPR_NAIS]                NVARCHAR (80)    NULL,
    [PAYS_NAIS]                NVARCHAR (255)   NULL,
    [NATN]                     NVARCHAR (255)   NULL,
    [NATN_AMRC]                NVARCHAR (255)   NULL,
    [FLG_INDC_AMRC]            BIT              NULL,
    [RUE_ENV_POST]             NVARCHAR (255)   NULL,
    [VILL_ENV_POST]            NVARCHAR (40)    NULL,
    [COD_POST_ENV_POST]        NVARCHAR (20)    NULL,
    [COD_PAYS_ENV_POST]        NVARCHAR (10)    NULL,
    [PAYS_ENV_POST]            NVARCHAR (80)    NULL,
    [LATT_ENV_POST]            DECIMAL (18, 15) NULL,
    [LONG_ENV_POST]            DECIMAL (18, 15) NULL,
    [GEOC_PRCS_ENV_POST]       NVARCHAR (40)    NULL,
    [COMP_ADRS]                NVARCHAR (255)   NULL,
    [FLG_INVL_ADRS]            BIT              NULL,
    [TELP]                     NVARCHAR (40)    NULL,
    [FAX]                      NVARCHAR (40)    NULL,
    [TELP_MOBL]                NVARCHAR (40)    NULL,
    [TELP_DOMC]                NVARCHAR (40)    NULL,
    [AUTR_TELP]                NVARCHAR (40)    NULL,
    [TELP_FAVR]                NVARCHAR (255)   NULL,
    [TELP_ASSS]                NVARCHAR (40)    NULL,
    [IDNT_RESP_HIER]           NVARCHAR (18)    NULL,
    [ADRS_MAIL]                NVARCHAR (80)    NULL,
    [ADRS_MAIL_SECN]           NVARCHAR (80)    NULL,
    [FLG_DESN_MAIL]            BIT              NULL,
    [FLG_DESN_TELC]            BIT              NULL,
    [FLG_NE_PAS_APPL]          BIT              NULL,
    [FLG_AUTR_AUTN_PORT_CLNT]  BIT              NULL,
    [DAT_CRTN]                 DATETIME         NULL,
    [IDNT_CRTN]                NVARCHAR (18)    NULL,
    [IDNT_PRPR]                NVARCHAR (18)    NULL,
    [DAT_DERN_MODF]            DATETIME         NULL,
    [IDNT_DERN_MODF]           NVARCHAR (18)    NULL,
    [HORD_MODF_SYST]           DATETIME         NULL,
    [DAT_DERN_ACTV]            DATETIME         NULL,
    [DAT_DERN_AFFC]            DATETIME         NULL,
    [DAT_DERN_REFR]            DATETIME         NULL,
    [RAIS_RETR_MAIL]           NVARCHAR (255)   NULL,
    [DAT_RETR_MAIL]            DATETIME         NULL,
    [FLG_RETR_MAIL]            BIT              NULL,
    [FLG_IDNT_CRTN_CLNT]       BIT              NULL,
    [DAT_DERN_MODF_PRFL_CLNT]  DATETIME         NULL,
    [DAT_DECS]                 DATE             NULL,
    [DAT_DEBT_CONT_TRVL]       DATE             NULL,
    [DAT_FIN_CONT_TRVL]        DATE             NULL,
    [TYP_CONT_TRVL]            NVARCHAR (255)   NULL,
    [DAT_CONF_ADHS]            DATE             NULL,
    [PRFL_INVS]                NVARCHAR (255)   NULL,
    [SITT_FAML]                NVARCHAR (255)   NULL,
    [REGM_MATR]                NVARCHAR (255)   NULL,
    [NOMB_ENFN_CHRG]           DECIMAL (2)      NULL,
    [REVN_JUST]                DECIMAL (18, 2)  NULL,
    [DAT_COLL_REVN]            DATE             NULL,
    [REVN_FONC_MENS]           DECIMAL (18, 2)  NULL,
    [REVN_MOBL_MENS]           DECIMAL (18, 2)  NULL,
    [REVN_MENS_NETS]           DECIMAL (18, 2)  NULL,
    [AUTR_REVN_MENS]           DECIMAL (18, 2)  NULL,
    [TOTL_REVN]                DECIMAL (18, 2)  NULL,
    [TOTL_REVN_DISP]           DECIMAL (18, 2)  NULL,
    [CHRG_MENS]                DECIMAL (18, 2)  NULL,
    [AUTR_CHRG_MENS]           DECIMAL (18, 2)  NULL,
    [TOTL_CHRG]                DECIMAL (18, 2)  NULL,
    [MENS_AUTR_CRDT_COUR]      DECIMAL (18, 2)  NULL,
    [PENS_ALMN_MENS_VERS]      DECIMAL (18, 2)  NULL,
    [PENS_ALMN_MENS_REC]       DECIMAL (18, 2)  NULL,
    [PRST_SOCL_MENS]           DECIMAL (18, 2)  NULL,
    [IMPT_MENS]                DECIMAL (18, 2)  NULL,
    [PRFS_PCS_PRNC]            NVARCHAR (255)   NULL,
    [PRFS_PCS_SECN]            NVARCHAR (255)   NULL,
    [PRFS]                     NVARCHAR (255)   NULL,
    [TYP_OCCP_RESD_PRNC]       NVARCHAR (255)   NULL,
    [DAT_ENTR_RESD_PRNC]       DATE             NULL,
    [PATR_MOBL]                DECIMAL (18, 2)  NULL,
    [PATR_IMMB]                DECIMAL (18, 2)  NULL,
    [DRT_OPPS]                 NVARCHAR (255)   NULL,
    [MOTF_RETR_COUR]           NVARCHAR (255)   NULL,
    [IMPT_ISF]                 NVARCHAR (255)   NULL,
    [SECT_ACTV]                NVARCHAR (255)   NULL,
    [QUAL_EMPL]                NVARCHAR (255)   NULL,
    [CAPC_JURD]                NVARCHAR (255)   NULL,
    [FLG_REFS_APPL_TELP]       BIT              NULL,
    [FLG_REFS_CNTC_COUR]       BIT              NULL,
    [FLG_REFS_MAIL]            BIT              NULL,
    [FLG_REFS_SMS]             BIT              NULL,
    [FLG_REFS_MAIL_PART]       BIT              NULL,
    [FLG_REFS_SMS_PART]        BIT              NULL,
    [ELGB_MOBL]                BIT              NULL,
    [FLG_EXCT_CONT_UNCT]       BIT              NULL,
    [CLNT_VIP_FIRS_PARN]       NVARCHAR (255)   NULL,
    [DAT_DEMN_ADHS]            DATE             NULL,
    [CRDT_COUR_HORS_ORNG_BANK] DECIMAL (18, 2)  NULL,
    [RES_DIST]                 NVARCHAR (255)   NULL,
    [ALLC_FAML]                DECIMAL (18)     NULL,
    [ALLC_LOGM]                DECIMAL (18)     NULL,
    [NUMR_CLNT_DIST]           NVARCHAR (15)    NULL,
    [FLG_CERT_AMF]             BIT              NULL,
    [FLG_SYNC_CNTC]            BIT              NULL,
    [FLG_HABL_IOBSP_TECH]      BIT              NULL,
    [CERT_IOBSP]               NVARCHAR (255)   NULL,
    [FLG_HABL_TA3C]            BIT              NULL,
    [INFR_REVN_JUST]           NVARCHAR (90)    NULL,
    [GERN]                     NVARCHAR (255)   NULL,
    [SCR_PRTT]                 DECIMAL (4)      NULL,
    [PRSC]                     NVARCHAR (255)   NULL,
    [NUMR_RCS]                 NVARCHAR (255)   NULL,
    [FLG_RESD_FISC_FRNC]       BIT              NULL,
    [COD_PAYS_FISC]            NVARCHAR (10)    NULL,
    [SEGM_GEOL]                NVARCHAR (100)   NULL,
    [IDNT_TCH]                 NVARCHAR (15)    NULL,
    [DAT_INSC_KIOS]            DATE             NULL,
    [DAT_MOBL]                 DATE             NULL,
    [FLG_REFS_NOTF_SOLD]       BIT              NULL,
    [FLG_REFS_NOTF_PAIE]       BIT              NULL,
    [ACTN_FLX]                 NVARCHAR (255)   NULL,
    [SCR_CONF_IDNT_DIGT]       DECIMAL (4)      NULL,
    [DAT_ENTR_RELT_OF]         DATE             NULL,
    [DAT_CRTN_ENRG]            DATETIME         NULL,
    [DAT_DERN_MODF_ENRG]       DATETIME         NULL,
    [FLG_ENRG_COUR]            BIT              NULL
)
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_SOUS_FLG_ENR_COU]
ON [dbo].[DWH_SOUSCRIPTEUR] ([FLG_ENRG_COUR])
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_SOUS_DAT_OBS]
ON [dbo].[DWH_SOUSCRIPTEUR] ([DAT_OBSR])
GO