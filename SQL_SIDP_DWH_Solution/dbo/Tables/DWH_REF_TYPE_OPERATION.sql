﻿CREATE TABLE [dbo].[DWH_REF_TYPE_OPERATION] (
    [IDNT_TYP_OPRT]                  FLOAT (53)     NULL,
    [FAMILLE_N0]                     NVARCHAR (255) NULL,
    [FAMILLE_N1]                     NVARCHAR (255) NULL,
    [FAMILLE_N2]                     NVARCHAR (255) NULL,
    [FAMILLE_N3]                     NVARCHAR (255) NULL,
    [TYP_COMP]                       NVARCHAR (255) NULL,
    [FLG_INTT_CLNT]                  FLOAT (53)     NULL,
    [SENS]                           NVARCHAR (255) NULL,
    [OPT_DEBIT]                      NVARCHAR (255) NULL,
    [TYP]                            NVARCHAR (255) NULL,
    [COD_OPE]                        NVARCHAR (255) NULL,
    [COD_EVE]                        NVARCHAR (255) NULL,
    [COD_ANA]                        NVARCHAR (255) NULL,
    [NAT_OPE]                        NVARCHAR (255) NULL,
    [SIGN_MONTANT]                   NVARCHAR (255) NULL,
    [BIC_CTP]                        NVARCHAR (255) NULL,
    [COD_COMMISION]                  NVARCHAR (255) NULL,
    [TYP_CART]                       NVARCHAR (255) NULL,
    [NAT_TRANS]                      NVARCHAR (255) NULL,
    [COMP_PASSAGE]                   NVARCHAR (255) NULL,
    [REQUETE_POUR_TESTER_LA_REGLE  ] NVARCHAR (MAX) NULL,
    [VERIFICATION_REGLE]             NVARCHAR (255) NULL,
    [EXPR_TECH]                      NVARCHAR (MAX) NULL
);


