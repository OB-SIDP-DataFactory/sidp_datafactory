﻿CREATE TABLE [dbo].[DWH_INTERACTION] (
    [DAT_OBSR]            DATE            NOT NULL,
    [IDNT_INTR]           NVARCHAR (18)   NULL,
    [NUMR_TACH]           NVARCHAR (30)   NULL,
    [NUMR_INTR]           NVARCHAR (36)   NULL,
    [IDNT_SF_PERS]        NVARCHAR (18)   NULL,
    [NUMR_PERS_SF]        NVARCHAR (11)   NULL,
    [NUMR_CLNT_SAB]       NVARCHAR (15)   NULL,
    [IDNT_TYP_ENRG]       NVARCHAR (18)   NULL,
    [OBJT]                NVARCHAR (255)  NULL,
    [TYP]                 NVARCHAR (40)   NULL,
    [SOUS_TYP_TACH]       NVARCHAR (40)   NULL,
    [FAML]                NVARCHAR (255)  NULL,
    [SOUS_FAML]           NVARCHAR (255)  NULL,
    [STTT]                NVARCHAR (40)   NULL,
    [PRRT]                NVARCHAR (40)   NULL,
    [DESC]                NVARCHAR (MAX)  NULL,
    [FLG_PRRT_ELV]        BIT             NULL,
    [IDNT_ASSC_A]         NVARCHAR (18)   NULL,
    [IDNT_NOM]            NVARCHAR (18)   NULL,
    [NUMR_CONV_IBM]       NVARCHAR (255)  NULL,
    [IDNT_PRPR]           NVARCHAR (18)   NULL,
    [DAT_CRTN]            DATETIME        NULL,
    [IDNT_CRTN]           NVARCHAR (18)   NULL,
    [NOMB_MOIS_DEPS_CRTN] DECIMAL (18)    NULL,
    [DAT_INTR]            DATETIME        NULL,
    [NOT_INTR]            DECIMAL (2)     NULL,
    [DAT_ECHN]            DATE            NULL,
    [DAT_TACH]            DATETIME        NULL,
    [FLG_FERM]            BIT             NULL,
    [TYP_APPL]            NVARCHAR (40)   NULL,
    [TYP_APPL_2]          NVARCHAR (1300) NULL,
    [IDNT_OBJT_APPL]      NVARCHAR (255)  NULL,
    [DUR_APPL_SECN]       INT             NULL,
    [RESL_APPL]           NVARCHAR (255)  NULL,
    [DAT_HEUR_RAPP]       DATETIME        NULL,
    [FLG_DEFN_RAPP]       BIT             NULL,
    [NIV_SATS]            NVARCHAR (1300) NULL,
    [IDNT_REPN]           NVARCHAR (10)   NULL,
    [LIEN_REPN]           NVARCHAR (255)  NULL,
    [FLG_SUPP]            BIT             NULL,
    [DAT_DERN_MODF]       DATETIME        NULL,
    [IDNT_DERN_MODF]      NVARCHAR (18)   NULL,
    [HORD_MODF_SYST]      DATETIME        NULL,
    [FLG_ARCH]            BIT             NULL,
    [FLG_PUBL]            BIT             NULL,
    [IDNT_ACTV_RECR]      NVARCHAR (18)   NULL,
    [FLG_RECR]            BIT             NULL,
    [DAT_DEBT_RECR]       DATE            NULL,
    [DAT_FIN_RECR]        DATE            NULL,
    [FUS_HORR_RECR]       NVARCHAR (40)   NULL,
    [TYP_RECR]            NVARCHAR (40)   NULL,
    [INTRV_RECR]          INT             NULL,
    [MASQ_JOUR_SEMN_RECR] INT             NULL,
    [JOUR_MOIS_RECR]      INT             NULL,
    [INST_RECR]           NVARCHAR (40)   NULL,
    [MOIS_ANN_RECR]       NVARCHAR (40)   NULL,
    [TYP_REPT_RECR]       NVARCHAR (40)   NULL,
    [INFR_COMP]           NVARCHAR (255)  NULL,
    [CHMP_TEST_WDE]       NVARCHAR (255)  NULL,
    [URL_DOCM]            NVARCHAR (255)  NULL,
    [MOTS_INTR]           NVARCHAR (255)  NULL,
    [FLG_MOTS_INTR]       BIT             NULL,
    [ADRS_LIVR]           NVARCHAR (255)  NULL,
    [DAT_ACHV]            DATETIME        NULL,
    [DAT_CRTN_ENRG]       DATETIME        NULL,
    [DAT_DERN_MODF_ENRG]  DATETIME        NULL,
    [FLG_ENRG_COUR]       BIT             NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Enregistrement Courant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere modification enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date creation enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Adresse de livraisoN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'ADRS_LIVR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Mots Interdits', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'FLG_MOTS_INTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mots interdits', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'MOTS_INTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'URL du document	', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'URL_DOCM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Champs test WDE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'CHMP_TEST_WDE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Tâche', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'NUMR_TACH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Tâche', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'DAT_TACH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de la réponse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'IDNT_REPN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre mois depuis créatioN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'NOMB_MOIS_DEPS_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Informations Complémentaires', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'INFR_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type répétition de récurrence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'TYP_REPT_RECR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mois de l''année de récurrence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'MOIS_ANN_RECR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Instance de récurrence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'INST_RECR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Jour du mois de récurrence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'JOUR_MOIS_RECR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Masque du jour de la semaine de récurrence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'MASQ_JOUR_SEMN_RECR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Intervalle de récurrence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'INTRV_RECR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type de récurrence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'TYP_RECR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fuseau horaire de récurrence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'FUS_HORR_RECR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date fin de la récurrence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'DAT_FIN_RECR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date début de la récurrence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'DAT_DEBT_RECR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag de récurrence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'FLG_RECR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant d''l''activité de récurrence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'IDNT_ACTV_RECR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag définition du rappel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'FLG_DEFN_RAPP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date et heure de rappel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'DAT_HEUR_RAPP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de l''objet de l''appel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'IDNT_OBJT_APPL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Résultat de l''appel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'RESL_APPL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type d''appel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'TYP_APPL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Durée de l''appel en seconde', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'DUR_APPL_SECN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag public', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'FLG_PUBL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag archivé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'FLG_ARCH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Horodateur modification systeme', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'HORD_MODF_SYST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de derniere modificatioN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'IDNT_DERN_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag fermée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'FLG_FERM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag supprimé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'FLG_SUPP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'TYP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag priorité élevée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'FLG_PRRT_ELV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro de conversation IBM', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'NUMR_CONV_IBM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date échéance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'DAT_ECHN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Lien de réponse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'LIEN_REPN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Niveau de satisfactioN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'NIV_SATS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date dinteraction', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'DAT_INTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Sous famille', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'SOUS_FAML';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Famille', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'FAML';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Statut', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'STTT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Priorité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'PRRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Sous type de tâche', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'SOUS_TYP_TACH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de derniere modificatioN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de créatioN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'DAT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de creatioN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'IDNT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro InteractioN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'NUMR_INTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Objet', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'OBJT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de nom', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'IDNT_NOM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DescriptioN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'DESC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant du proprietaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'IDNT_PRPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant d''associé à', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'IDNT_ASSC_A';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant type enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'IDNT_TYP_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant SF personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'IDNT_SF_PERS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de l interactioN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'IDNT_INTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date ObservatioN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'NUMR_PERS_SF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Client Sab', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'NUMR_CLNT_SAB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'	Note de l''interaction', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'NOT_INTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type d''appel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'TYP_APPL_2';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date d''achèvement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_INTERACTION', @level2type = N'COLUMN', @level2name = N'DAT_ACHV';

