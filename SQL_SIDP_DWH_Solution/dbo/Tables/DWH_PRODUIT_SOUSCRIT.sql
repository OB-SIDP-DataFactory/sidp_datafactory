﻿CREATE TABLE [dbo].[DWH_PRODUIT_SOUSCRIT] (
    [DAT_OBSR]             DATE           NOT NULL,
    [IDNT_PRDT_SOUS]       DECIMAL (19)   NULL,
    [IDNT_DOSS_SOUS]       DECIMAL (19)   NULL,
    [IDNT_PRDT_COMM]       DECIMAL (19)   NULL,
    [COD_PRDT_COMM]        NVARCHAR (255) NULL,
    [LIBL_PRDT_COMM]       NVARCHAR (255) NULL,
    [DISC]                 NVARCHAR (31)  NULL,
    [DAT_CRTN]             DATETIME2 (6)  NULL,
    [IDNT_CRTN]            NVARCHAR (255) NULL,
    [IDNT_DERN_MODF]       NVARCHAR (255) NULL,
    [TYP_UTLS]             NVARCHAR (255) NULL,
    [FLG_SECR_BANC_OPT_IN] BIT            NULL,
    [DAT_DERN_MODF]        DATETIME2 (6)  NULL,
    [DAT_CRTN_ENRG]        DATETIME       NULL,
    [DAT_DERN_MODF_ENRG]   DATETIME       NULL,
    [FLG_ENRG_COUR]        BIT            NULL
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere modification enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date creation enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Discriminateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'DISC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant produit commercial', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'IDNT_PRDT_COMM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant dossier souscription', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'IDNT_DOSS_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'DAT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant produit souscrit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'IDNT_PRDT_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé produit commercial', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'LIBL_PRDT_COMM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code produit commercial', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'COD_PRDT_COMM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Courant Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type utilisateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'TYP_UTLS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'IDNT_DERN_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'IDNT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag secret bancaire Opt In', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'FLG_SECR_BANC_OPT_IN';

