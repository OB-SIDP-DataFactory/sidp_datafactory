﻿CREATE TABLE [dbo].[DWH_CONVENTION] (
    [DAT_OBSR]           DATE         NULL,
    [NUMR_ABNN]          INT          NULL,
    [NUMR_COMP]          VARCHAR (20) NULL,
    [NUMR_CLNT_SAB]      VARCHAR (7)  NULL,
    [COD_ETBL]           VARCHAR (4)  NULL,
    [COD_AGNC]           VARCHAR (4)  NULL,
    [COD_SERV]           VARCHAR (2)  NULL,
    [COD_SOUS]           VARCHAR (2)  NULL,
    [COD_PRDT]           VARCHAR (3)  NULL,
    [COD_ETT]            VARCHAR (1)  NULL,
    [COMP_FACT]          VARCHAR (20) NULL,
    [DAT_ADHS]           DATE         NULL,
    [DAT_FIN]            DATE         NULL,
    [DAT_RENV]           DATE         NULL,
    [UTLS_1]             INT          NULL,
    [UTLS_2]             INT          NULL,
    [DAT_RESL]           DATE         NULL,
    [MOTF_RESL]          VARCHAR (6)  NULL,
    [DAT_CRTN]           DATE         NULL,
    [DAT_VALDTN]         DATE         NULL,
    [DAT_ANNL]           DATE         NULL,
    [MOD_ENV_RELV]       VARCHAR (1)  NULL,
    [DAT_CRTN_ENRG]      DATETIME     NULL,
    [DAT_DERN_MODF_ENRG] DATETIME     NULL,
    [FLG_ENRG_COUR]      BIT          NULL
)
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CONVENTION', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CONVENTION', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mode Envoi Relevés', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CONVENTION', @level2type = N'COLUMN', @level2name = N'MOD_ENV_RELV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Annulation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CONVENTION', @level2type = N'COLUMN', @level2name = N'DAT_ANNL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Validation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CONVENTION', @level2type = N'COLUMN', @level2name = N'DAT_VALDTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CONVENTION', @level2type = N'COLUMN', @level2name = N'DAT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motif Résiliation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CONVENTION', @level2type = N'COLUMN', @level2name = N'MOTF_RESL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Résiliation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CONVENTION', @level2type = N'COLUMN', @level2name = N'DAT_RESL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Utilisateur 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CONVENTION', @level2type = N'COLUMN', @level2name = N'UTLS_2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Utilisateur 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CONVENTION', @level2type = N'COLUMN', @level2name = N'UTLS_1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Compte Facturation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CONVENTION', @level2type = N'COLUMN', @level2name = N'COMP_FACT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Etat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CONVENTION', @level2type = N'COLUMN', @level2name = N'COD_ETT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date renouvellement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CONVENTION', @level2type = N'COLUMN', @level2name = N'DAT_RENV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Fin', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CONVENTION', @level2type = N'COLUMN', @level2name = N'DAT_FIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Adhésion', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CONVENTION', @level2type = N'COLUMN', @level2name = N'DAT_ADHS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Produit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CONVENTION', @level2type = N'COLUMN', @level2name = N'COD_PRDT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Client SAB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CONVENTION', @level2type = N'COLUMN', @level2name = N'NUMR_CLNT_SAB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Sous-Service', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CONVENTION', @level2type = N'COLUMN', @level2name = N'COD_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Service', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CONVENTION', @level2type = N'COLUMN', @level2name = N'COD_SERV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Agence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CONVENTION', @level2type = N'COLUMN', @level2name = N'COD_AGNC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Etablissement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CONVENTION', @level2type = N'COLUMN', @level2name = N'COD_ETBL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CONVENTION', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Courant Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CONVENTION', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CONVENTION', @level2type = N'COLUMN', @level2name = N'NUMR_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Abonnement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CONVENTION', @level2type = N'COLUMN', @level2name = N'NUMR_ABNN';
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_CNV_FLG_ENR_COU]
ON [dbo].[DWH_CONVENTION] ([FLG_ENRG_COUR])
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_CNV_DAT_OBS]
ON [dbo].[DWH_CONVENTION] ([DAT_OBSR])
GO