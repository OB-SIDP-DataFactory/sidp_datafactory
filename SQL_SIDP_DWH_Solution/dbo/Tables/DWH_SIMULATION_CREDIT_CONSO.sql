﻿CREATE TABLE [dbo].[DWH_SIMULATION_CREDIT_CONSO] (
    [DAT_OBSR]                 DATE            NOT NULL,
    [IDNT_SIML_CRDT_CONS]      DECIMAL (38)    NOT NULL,
    [IDNT_PRDT_SOUS_CRDT_CONS] DECIMAL (38)    NULL,
    [IDNT_TYP_CONT_EMPR]       DECIMAL (38)    NULL,
    [IDNT_TYP_CONT_CO_EMPR]    DECIMAL (38)    NULL,
    [IDNT_STTT_PLN_FINN]       DECIMAL (38)    NULL,
    [DAT_CRTN]                 DATETIME2 (6)   NULL,
    [IDNT_CRTN]                NVARCHAR (255)  NULL,
    [IDNT_TYP_FINN]            DECIMAL (38)    NULL,
    [MONT_PRT]                 DECIMAL (19, 2) NOT NULL,
    [DUR_PRT_MOIS]             DECIMAL (38)    NOT NULL,
    [TAUX_NOMN_PRT_CLNT]       DECIMAL (19, 7) NOT NULL,
    [DAT_FIN_VALD_TAUX]        DATE            NOT NULL,
    [REVN_MENS_NET_EMPR]       DECIMAL (19, 2) NULL,
    [REVN_MENS_NET_CO_EMPR]    DECIMAL (19, 2) NULL,
    [LIST_GARN_ASSR_EMPR]      NVARCHAR (4000) NULL,
    [LIST_GARN_ASSR_CO_EMPR]   NVARCHAR (4000) NULL,
    [TAUX_RISQ_ASSR_1_SOUS]    DECIMAL (19, 7) NULL,
    [TAUX_RISQ_ASSR_1_CO_SOUS] DECIMAL (19, 7) NULL,
    [TAUX_RISQ_ASSR_2_SOUS]    DECIMAL (19, 7) NULL,
    [TAUX_RISQ_ASSR_2_CO_SOUS] DECIMAL (19, 7) NULL,
    [TYP_RISQ_ASSR_1_SOUS]     NVARCHAR (255)  NULL,
    [TYP_RISQ_ASSR_1_CO_SOUS]  NVARCHAR (255)  NULL,
    [TYP_RISQ_ASSR_2_SOUS]     NVARCHAR (255)  NULL,
    [TYP_RISQ_ASSR_2_CO_SOUS]  NVARCHAR (255)  NULL,
    [PRM_ASSR_EMPR]            DECIMAL (19, 2) NULL,
    [PRM_ASSR_CO_EMPR]         DECIMAL (19, 2) NULL,
    [PRM_ASSR_RECM]            DECIMAL (19, 2) NULL,
    [PRM_ASSR]                 DECIMAL (19, 2) NULL,
    [COUT_TOTL_ASSR]           DECIMAL (19, 2) NULL,
    [FRS_INSC]                 DECIMAL (19, 2) NOT NULL,
    [TAUX_TAEA_PRT_RECM]       DECIMAL (19, 7) NULL,
    [TAUX_ANNL_EFFC_ASSR]      DECIMAL (19, 7) NULL,
    [TAUX_ANNL_EFFC_GLBL]      DECIMAL (19, 7) NOT NULL,
    [MONT_TOTL_INTR]           DECIMAL (19, 2) NOT NULL,
    [VERS_MENS_AVC_ASSR]       DECIMAL (19, 2) NULL,
    [VERS_MENS_SANS_ASSR]      DECIMAL (19, 2) NOT NULL,
    [MONT_PRMR_VERS]           DECIMAL (19, 2) NOT NULL,
    [VERS_PEND_PERD_DIFF]      DECIMAL (19, 2) NULL,
    [DAT_DERN_MODF]            DATETIME2 (6)   NULL,
    [IDNT_DERN_MODF]           NVARCHAR (255)  NULL,
    [TOTL_PERD_DIFF_MOIS]      DECIMAL (38)    NULL,
    [TYP_UTLS]                 NVARCHAR (255)  NULL,
    [DEPT_PRT_CART]            DECIMAL (19, 7) NULL,
    [DEPT_PRT_AUTR]            DECIMAL (19, 7) NULL,
    [TAUX_NOMN_PRT_TOTL]       DECIMAL (19, 7) NULL,
    [TAUX_DEDC_PRT]            DECIMAL (19, 7) NULL,
    [COD_TAUX_DEDC_PRT]        NVARCHAR (32)   NULL,
    [TAUX_ANNL_EFFC_GLBL_PRV]  DECIMAL (19, 7) NULL,
    [TAUX_NOMN_PRT_PRV]        DECIMAL (19, 7) NULL,
    [DAT_CRTN_ENRG]            DATETIME        NULL,
    [DAT_DERN_MODF_ENRG]       DATETIME        NULL,
    [FLG_ENRG_COUR]            BIT             NULL
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date création de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Versement pendant periode differee', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'VERS_PEND_PERD_DIFF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Versement mensuel sans assurance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'VERS_MENS_SANS_ASSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Versement mensuel avec assurance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'VERS_MENS_AVC_ASSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type utilisateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TYP_UTLS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type de risque d''assurance 2 souscripteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TYP_RISQ_ASSR_2_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type de risque d''assurance 1 souscripteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TYP_RISQ_ASSR_1_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux TAEA de prêt recommandé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TAUX_TAEA_PRT_RECM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux de risque d''assurance 2 souscripteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TAUX_RISQ_ASSR_2_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux de risque d''assurance 1 souscripteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TAUX_RISQ_ASSR_1_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux annuel effectif global', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TAUX_ANNL_EFFC_GLBL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux annuel effectif d''assurance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TAUX_ANNL_EFFC_ASSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Revenu mensuel net de l''emprunteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'REVN_MENS_NET_EMPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prime d''assurance recommandee', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'PRM_ASSR_RECM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prime d''assurance emprunteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'PRM_ASSR_EMPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prime d''assurance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'PRM_ASSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant total de l''interet', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_TOTL_INTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant du pret', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_PRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant du premier versement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_PRMR_VERS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Liste de garantie d''assurance emprunteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'LIST_GARN_ASSR_EMPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant type de contrat emprunteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'IDNT_TYP_CONT_EMPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant statut plan de financement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'IDNT_STTT_PLN_FINN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Frais inscription', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'FRS_INSC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Duree du pret en mois', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DUR_PRT_MOIS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de fin de validite du taux', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_FIN_VALD_TAUX';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cout total de l''assurance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'COUT_TOTL_ASSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type de risque d''assurance 2 co souscripteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TYP_RISQ_ASSR_2_CO_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type de risque d''assurance 1 co souscripteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TYP_RISQ_ASSR_1_CO_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Total periode differee en mois', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TOTL_PERD_DIFF_MOIS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux de risque d''assurance 2 co souscripteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TAUX_RISQ_ASSR_2_CO_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux de risque d''assurance 1 co souscripteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TAUX_RISQ_ASSR_1_CO_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Revenu mensuel net du co emprunteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'REVN_MENS_NET_CO_EMPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prime d''assurance co emprunteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'PRM_ASSR_CO_EMPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Liste de garantie d''assurance co emprunteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'LIST_GARN_ASSR_CO_EMPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifant type de financement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'IDNT_TYP_FINN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant simulation credit consommation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'IDNT_SIML_CRDT_CONS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant produit souscrit credit consommation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'IDNT_PRDT_SOUS_CRDT_CONS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifant derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'IDNT_DERN_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'IDNT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant type de contrat coemprunteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'IDNT_TYP_CONT_CO_EMPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Courant Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux nominal du prêt total', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TAUX_NOMN_PRT_TOTL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux nominal du prêt prev', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TAUX_NOMN_PRT_PRV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux nominal du prêt client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TAUX_NOMN_PRT_CLNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux déduction du prêt', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TAUX_DEDC_PRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux annuel effectif global prev', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TAUX_ANNL_EFFC_GLBL_PRV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dépôt du prêt par carte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DEPT_PRT_CART';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dépôt du prêt autre', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DEPT_PRT_AUTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code taux déduction du prêt', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'COD_TAUX_DEDC_PRT';

