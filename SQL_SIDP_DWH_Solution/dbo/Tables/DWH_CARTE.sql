﻿CREATE TABLE [dbo].[DWH_CARTE] (
    [DAT_OBSR]           DATE            NOT NULL,
    [COD_ETBL]           VARCHAR (4)     NULL,
    [COD_CART]           VARCHAR (6)     NULL,
    [LIBL_CART]          VARCHAR (42)    NULL,
    [NUMR_COMP]          VARCHAR (20)    NULL,
    [NUMR_SEQN]          INT             NULL,
    [NUMR_CART]          VARCHAR (19)    NULL,
    [NUMR_PORT]          VARCHAR (7)     NULL,
    [TYP_PORT]           VARCHAR (1)     NULL,
    [NATR_CART]          VARCHAR (1)     NULL,
    [TYP_CART]           VARCHAR (1)     NULL,
    [COD_ETT_CART]       VARCHAR (3)     NULL,
    [DAT_COMM_CART]      DATE            NULL,
    [DAT_RECP_CART]      DATE            NULL,
    [DAT_REMS_CART]      DATE            NULL,
    [DAT_INVL]           DATE            NULL,
    [COD_OPTN_DEBIT]     VARCHAR (1)     NULL,
    [UTLS]               VARCHAR (1)     NULL,
    [DAT_VALD_CART]      DATE            NULL,
    [DUR_VALD_CART]      INT             NULL,
    [AGNC_GEST]          INT             NULL,
    [FLG_MIS_HORS_SERV]  BIT             NULL,
    [FLG_CART_CR_MOIS]   BIT             NULL,
    [FLG_CART_RENV_MOIS] BIT             NULL,
    [FLG_CART_RESL_MOIS] BIT             NULL,
    [DAT_RENV]           DATE            NULL,
    [DAT_RESL]           DATE            NULL,
    [DAT_ENV_FACN]       DATE            NULL,
    [FLG_RENV_AUTM]      BIT             NULL,
    [COD_LIVR]           VARCHAR (5)     NULL,
    [MONT_PLFN_ACHT]     DECIMAL (18, 3) NULL,
    [MONT_PLFN_RETR]     DECIMAL (18, 3) NULL,
    [TYP_VISL]           VARCHAR (3)     NULL,
    [NUMR_ANCN_CART]     VARCHAR (19)    NULL,
    [FLG_CRTN_AUTM]      VARCHAR (1)     NULL,
    [DAT_CRTN_ENRG]      DATETIME        NULL,
    [DAT_DERN_MODF_ENRG] DATETIME        NULL,
    [FLG_ENRG_COUR]      BIT             NULL
)
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Invalidité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'DAT_INVL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag  Création Automatique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'FLG_CRTN_AUTM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Ancienne Carte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'NUMR_ANCN_CART';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type Visuel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'TYP_VISL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Etat Carte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'COD_ETT_CART';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant Plafond Retrait', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'MONT_PLFN_RETR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant Plafond Achat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'MONT_PLFN_ACHT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Livraison', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'COD_LIVR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Renouvellement Automatique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'FLG_RENV_AUTM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Envoi au façonnier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'DAT_ENV_FACN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Résiliant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'DAT_RESL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Renouvellement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'DAT_RENV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Carte résilie dans mois', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'FLG_CART_RESL_MOIS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Carte renouvelé mois', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'FLG_CART_RENV_MOIS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Carte crée mois', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'FLG_CART_CR_MOIS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Remise Carte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'DAT_REMS_CART';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date réception Carte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'DAT_RECP_CART';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Commande Carte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'DAT_COMM_CART';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Mise Hors Service', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'FLG_MIS_HORS_SERV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Agence Gestionnaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'AGNC_GEST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Durée Validité Carte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'DUR_VALD_CART';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Validité Carte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'DAT_VALD_CART';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Utilisation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'UTLS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Option Débit (I:Immédiat / D:Différé)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'COD_OPTN_DEBIT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type Carte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'TYP_CART';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nature Carte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'NATR_CART';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Carte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'LIBL_CART';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type Porteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'TYP_PORT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Carte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'NUMR_CART';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Séquence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'NUMR_SEQN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'NUMR_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Carte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'COD_CART';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Etablissement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'COD_ETBL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Porteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'NUMR_PORT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Enregistrement Courant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CARTE', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR'
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_CAR_FLG_ENR_COU]
ON [dbo].[DWH_CARTE] ([FLG_ENRG_COUR])
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_CAR_DAT_OBS]
ON [dbo].[DWH_CARTE] ([DAT_OBSR])
GO