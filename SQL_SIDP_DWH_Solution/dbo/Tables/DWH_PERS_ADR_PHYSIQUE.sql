﻿CREATE TABLE [dbo].[DWH_PERS_ADRESSE_PHYSIQUE]
(
	DAT_OBSR date,
	IDNT_PERS_SF nvarchar(18),
	TYP_ADRS NVARCHAR(100),
	RUE	NVARCHAR(255),
	CMPL_ADRS NVARCHAR(255),
	COD_POST	NVARCHAR(20),	
	VILL NVARCHAR(40),
	PAYS	NVARCHAR(80),
	[DAT_CRTN_ENRG] datetime,
	[DAT_DERN_MODF_ENRG] datetime ,
	[FLG_ENRG_COUR] bit 
)
GO
EXEC sp_addextendedproperty @name=N'MS_Description', @value=N'Date Observation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_PERS_ADRESSE_PHYSIQUE', @level2type=N'COLUMN',@level2name=N'DAT_OBSR'
GO
EXEC sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant Personne SF' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_PERS_ADRESSE_PHYSIQUE', @level2type=N'COLUMN',@level2name=N'IDNT_PERS_SF'
GO
EXEC sp_addextendedproperty @name=N'MS_Description', @value=N'Type Adresse' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_PERS_ADRESSE_PHYSIQUE', @level2type=N'COLUMN',@level2name=N'TYP_ADRS'
GO
EXEC sp_addextendedproperty @name=N'MS_Description', @value=N'Rue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_PERS_ADRESSE_PHYSIQUE', @level2type=N'COLUMN',@level2name=N'RUE'
GO
EXEC sp_addextendedproperty @name=N'MS_Description', @value=N'Complément Adresse' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_PERS_ADRESSE_PHYSIQUE', @level2type=N'COLUMN',@level2name=N'CMPL_ADRS'
GO
EXEC sp_addextendedproperty @name=N'MS_Description', @value=N'Code Postal' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_PERS_ADRESSE_PHYSIQUE', @level2type=N'COLUMN',@level2name=N'COD_POST'
GO
EXEC sp_addextendedproperty @name=N'MS_Description', @value=N'Ville' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_PERS_ADRESSE_PHYSIQUE', @level2type=N'COLUMN',@level2name=N'VILL'
GO
EXEC sp_addextendedproperty @name=N'MS_Description', @value=N'Pays' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_PERS_ADRESSE_PHYSIQUE', @level2type=N'COLUMN',@level2name=N'PAYS'
GO
EXEC sp_addextendedproperty @name=N'MS_Description', @value=N'Date de création de l''enregistrement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_PERS_ADRESSE_PHYSIQUE', @level2type=N'COLUMN',@level2name=N'DAT_CRTN_ENRG'
GO
EXEC sp_addextendedproperty @name=N'MS_Description', @value=N'Date de dernière modification de l''enregistrement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_PERS_ADRESSE_PHYSIQUE', @level2type=N'COLUMN',@level2name=N'DAT_DERN_MODF_ENRG'
GO
EXEC sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Enregistrement Courant' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_PERS_ADRESSE_PHYSIQUE', @level2type=N'COLUMN',@level2name=N'FLG_ENRG_COUR'
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_PERS_ADR_PHY_FLG_ENR_COU]
ON [dbo].[DWH_PERS_ADRESSE_PHYSIQUE] ([FLG_ENRG_COUR])
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_PERS_ADR_PHY_DAT_OBS]
ON [dbo].[DWH_PERS_ADRESSE_PHYSIQUE] ([DAT_OBSR])
GO