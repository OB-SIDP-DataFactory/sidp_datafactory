﻿CREATE TABLE [dbo].[DWH_OPPORTUNITE] (
    [DAT_OBSR]             DATE            NOT NULL,
    [IDNT_OPPR]            NVARCHAR (18)   NOT NULL,
    [NUMR_OPPR]            NVARCHAR (30)   NULL,
    [IDNT_SF_PERS]         NVARCHAR (18)   NULL,
    [NUMR_PERS_SF]         NVARCHAR (11)   NULL,
    [NUMR_CLNT_SAB]        NVARCHAR (15)   NULL,
    [NOM_OPPR]             NVARCHAR (120)  NULL,
    [COD_OFFR_COMM]        NVARCHAR (80)   NULL,
    [NOM_OFFR_COMM]        NVARCHAR (80)   NULL,
    [IDNT_TYP_ENRG]        NVARCHAR (18)   NULL,
    [COD_FAML_PRDT]        NVARCHAR (80)   NULL,
    [NOM_FAML_PRDT]        NVARCHAR (80)   NULL,
    [PRDT]                 NVARCHAR (255)  NULL,
    [STD_VENT]             NVARCHAR (40)   NULL,
    [DAT_CRTN_OPPR]        DATETIME        NULL,
    [DAT_CLTR_OPPR]        DATE            NULL,
    [AGE_OPPR]             INT             NULL,
    [EVNM_ORGN]            NVARCHAR (255)  NULL,
    [DAT_DERN_MODF]        DATETIME        NULL,
    [IDNT_DERN_MODF]       NVARCHAR (18)   NULL,
    [FLG_FERM]             NVARCHAR (10)   NULL,
    [FLG_GAGN]             NVARCHAR (10)   NULL,
    [MOTF_AFFR_REFS]       NVARCHAR (255)  NULL,
    [MOTF_REFS_ENGG]       NVARCHAR (255)  NULL,
    [MOTF_SANS_SUIT]       NVARCHAR (255)  NULL,
    [CANL_INTR_ORGN]       NVARCHAR (255)  NULL,
    [CANL_INTR]            NVARCHAR (40)   NULL,
    [CANL_INTR_FINL]       NVARCHAR (80)   NULL,
    [CANL_INTR_FINL_2]     NVARCHAR (255)  NULL,
    [CANL_DIST_ORGN]       NVARCHAR (255)  NULL,
    [CANL_DIST]            NVARCHAR (255)  NULL,
    [RES_DIST]             NVARCHAR (255)  NULL,
    [ENTT_DIST]            NVARCHAR (255)  NULL,
    [ENTT_DIST_FINL]       NVARCHAR (255)  NULL,
    [COD_POIN_VENT]        NVARCHAR (80)   NULL,
    [IDNT_SOUR]            NVARCHAR (80)   NULL,
    [COD_CONS]             NVARCHAR (80)   NULL,
    [IDNT_CONS]            NVARCHAR (80)   NULL,
    [FLG_INDC]             NVARCHAR (80)   NULL,
    [NUMR_INDC_PRCS]       NVARCHAR (80)   NULL,
    [INDC_DIGT]            NVARCHAR (80)   NULL,
    [INDC_CANL_INTR_ORGN]  NVARCHAR (255)  NULL,
    [FLG_DOSS_COMP]        NVARCHAR (255)  NULL,
    [MOTF_DOSS_INCM]       NVARCHAR (4000) NULL,
    [IDNT_PRCS_SOUS]       NVARCHAR (80)   NULL,
    [IDNT_CAMP]            NVARCHAR (18)   NULL,
    [CAMP]                 NVARCHAR (80)   NULL,
    [DERG]                 NVARCHAR (255)  NULL,
    [REAL_PAR]             NVARCHAR (50)   NULL,
    [FINL_PAR]             NVARCHAR (18)   NULL,
    [OBJT_FINN]            NVARCHAR (255)  NULL,
    [MONT_PRT]             DECIMAL (18, 2) NULL,
    [MONT_DEBL]            DECIMAL (18, 2) NULL,
    [MONT_VERS]            DECIMAL (18, 2) NULL,
    [MONT_SOUS]            DECIMAL (18, 2) NULL,
    [TAUX_OFFR]            DECIMAL (18, 2) NULL,
    [DAT_SIGN]             DATE            NULL,
    [DAT_ACCR_CRDT]        DATETIME        NULL,
    [DEL_ACCR_CRDT]        INT             NULL,
    [DAT_RETR]             DATE            NULL,
    [DAT_DEBL_CRDT]        DATETIME        NULL,
    [DEL_DEBL_CRDT]        INT             NULL,
    [DAT_DEBT]             DATE            NULL,
    [JOUR_PRLV]            NVARCHAR (255)  NULL,
    [PRMR_ECHN]            NVARCHAR (255)  NULL,
    [CATG_PRVS]            NVARCHAR (40)   NULL,
    [NOM_CATG_PRVS]        NVARCHAR (40)   NULL,
    [SCR_ENTR_RELT]        NVARCHAR (255)  NULL,
    [COD_PR_SCR]           NVARCHAR (80)   NULL,
    [MOTF_PR_SCR]          NVARCHAR (4000) NULL,
    [COUL_PR_SCR]          NVARCHAR (1300) NULL,
    [COUL_PR_SCR_TCH]      NVARCHAR (255)  NULL,
    [DECS_FINL]            NVARCHAR (1300) NULL,
    [DECS_FINL_TCH]        NVARCHAR (255)  NULL,
    [TAUX_ENDT]            DECIMAL (18, 2) NULL,
    [FLG_DONN_TELC_OPT_IN] NVARCHAR (10)   NULL,
    [FLG_FACT_TELC_OPT_IN] NVARCHAR (10)   NULL,
    [NUMR_CONT_DIST]       NVARCHAR (80)   NULL,
    [FLG_SOUS_OU_TITL]     NVARCHAR (10)   NULL,
    [IDNT_DOSS_FE]         NVARCHAR (36)   NULL,
    [IDNT_CRTN]            NVARCHAR (18)   NULL,
    [CRTR_OPPR]            NVARCHAR (18)   NULL,
    [FLG_SUPP]             NVARCHAR (10)   NULL,
    [FLG_PRV]              NVARCHAR (10)   NULL,
    [MONT]                 DECIMAL (18, 2) NULL,
    [PRBB]                 DECIMAL (18, 2) NULL,
    [REVN_ATTN]            DECIMAL (18, 2) NULL,
    [QUAN_TOTL_OPPR]       DECIMAL (18, 2) NULL,
    [TYP_OPPR]             NVARCHAR (40)   NULL,
    [ETP_SUIV]             NVARCHAR (255)  NULL,
    [FLG_LIGN_COMM_ATTR]   NVARCHAR (10)   NULL,
    [IDNT_CATL_PRX]        NVARCHAR (18)   NULL,
    [HORD_MODF_SYST]       DATETIME        NULL,
    [DAT_DERN_ACTV]        DATE            NULL,
    [TRMS_FISC]            INT             NULL,
    [ANN_FISC]             INT             NULL,
    [TYP_EXRC_FISC]        NVARCHAR (6)    NULL,
    [DAT_DERN_AFFC]        DATETIME        NULL,
    [DAT_DERN_REFR]        DATETIME        NULL,
    [IDNT_CONT]            NVARCHAR (18)   NULL,
    [FLG_ACTV_COUR]        NVARCHAR (10)   NULL,
    [FLG_TACH_RETR]        NVARCHAR (10)   NULL,
    [FLG_FICH_COMP]        NVARCHAR (10)   NULL,
    [FLG_DEPS_MARK_CAS]    NVARCHAR (10)   NULL,
    [DAT_VALD_FORM]        DATETIME        NULL,
    [DAT_ENV_INFR]         DATETIME        NULL,
    [FLG_NOTF_DUPL_PRSP]   NVARCHAR (10)   NULL,
    [LIEN_REPR]            NVARCHAR (1300) NULL,
    [FLG_NOMB_INDC_EXPR]   NVARCHAR (10)   NULL,
    [FLG_NOMB_SOUS_EXPR]   NVARCHAR (10)   NULL,
    [FLG_NOMB_ANLS_MANL]   NVARCHAR (10)   NULL,
    [FLG_NOMB_CRTN_OPPR]   NVARCHAR (10)   NULL,
    [FLG_A_SUPP]           NVARCHAR (10)   NULL,
    [RESP_ENTT]            NVARCHAR (18)   NULL,
    [IDNT_TCH]             NVARCHAR (18)   NULL,
    [MOTS_INTR]            NVARCHAR (255)  NULL,
    [FLG_MOTS_INTR]        NVARCHAR (10)   NULL,
    [FLG_ENTT_APPR]        NVARCHAR (10)   NULL,
    [PRSC]                 NVARCHAR (18)   NULL,
    [CATG]                 NVARCHAR (1300) NULL,
    [FLG_SYNC]             NVARCHAR (10)   NULL,
    [SOUM_APPR]            NVARCHAR (18)   NULL,
    [FLG_ENRL_CHCK_GO]     NVARCHAR (10)   NULL,
    [COMM]                 NVARCHAR (4000) NULL,
    [FLG_CLNT_OF]          NVARCHAR (10)   NULL,
    [DAT_APPR_MANG]        DATE            NULL,
    [NOM_DEVL_TYP_ENRG]    NVARCHAR (1300) NULL,
    [PUSH_TOPC_DESR_TCH]   NVARCHAR (10)   NULL,
    [FLG_ASSR_CO_EMPR]     NVARCHAR (10)   NULL,
    [FLG_ASSR_EMPR_PRNC]   NVARCHAR (10)   NULL,
    [FIL_COMM]             NVARCHAR (MAX)  NULL,
    [FLG_OAV_ACCR]         NVARCHAR (10)   NULL,
    [COD_CAMP]             NVARCHAR (MAX)  NULL,
    [DAT_CRTN_LEAD]        DATE            NULL,
    [DAT_PRMR_VIST]        DATE            NULL,
    [COD_SALN]             NVARCHAR (MAX)  NULL,
    [CANL_INTR_LEAD]       NVARCHAR (255)  NULL,
    [IDNT_LEAD]            NVARCHAR (20)   NULL,
    [FAML_PRDT_LEAD]       NVARCHAR (255)  NULL,
    [NOMB_VIST]            DECIMAL (18, 2) NULL,
    [FLG_ANCN_LEAD]        NVARCHAR (10)   NULL,
    [IDNT_CNTC]            NVARCHAR (18)   NULL,
    [RES_DIST_INTL]        NVARCHAR (2)    NULL,
    [DAT_CRTN_ENRG]        DATETIME        NULL,
    [DAT_DERN_MODF_ENRG]   DATETIME        NULL,
    [FLG_ENRG_COUR]        BIT             NULL,
    [LIEN_REPR_RIO]        NVARCHAR (255)  NULL,
    [FLG_OPT_IN_PRTT_TELC] BIT             NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag synchronisation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_SYNC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date retractation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DAT_RETR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Produit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'PRDT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Categorie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'CATG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date debut', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DAT_DEBT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prescripteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'PRSC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux offre', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'TAUX_OFFR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motif dossier incomplet', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'MOTF_DOSS_INCM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag mots interdits', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_MOTS_INTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Objet de financement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'OBJT_FINN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mots interdits', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'MOTS_INTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Premiere echeance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'PRMR_ECHN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Couleur pre score', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'COUL_PR_SCR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Decision finale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DECS_FINL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Derogation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DERG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Jour de prelevement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'JOUR_PRLV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant conseiller', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'IDNT_CONS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numero contrat distributeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'NUMR_CONT_DIST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant TCH', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'IDNT_TCH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Indicateur digitale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'INDC_DIGT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Responsable entite', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'RESP_ENTT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant source', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'IDNT_SOUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Lien de reprise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'LIEN_REPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant processus souscription', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'IDNT_PRCS_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant dossier FE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'IDNT_DOSS_FE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de signature', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DAT_SIGN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date envoi information', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DAT_ENV_INFR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code point de vente', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'COD_POIN_VENT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Evenement origine', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'EVNM_ORGN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numero indication processus', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'NUMR_INDC_PRCS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date validation formulaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DAT_VALD_FORM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Finalise par', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FINL_PAR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Realise par', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'REAL_PAR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Campagne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'CAMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag tache en retard', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_TACH_RETR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag activite en cours', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_ACTV_COUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant contrat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'IDNT_CONT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere reference', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_REFR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date dernier affichage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_AFFC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type exercice fiscal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'TYP_EXRC_FISC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Annee fiscale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'ANN_FISC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Trimestre fiscal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'TRMS_FISC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere activite', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_ACTV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Horodateur modification systeme', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'HORD_MODF_SYST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'IDNT_DERN_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant du catalogue de prix', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'IDNT_CATL_PRX';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant campagne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'IDNT_CAMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom categorie de prevision', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'NOM_CATG_PRVS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Categorie de prevision', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'CATG_PRVS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag gagnee', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_GAGN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag fermee', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_FERM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Etape suivante', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'ETP_SUIV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type opportunite', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'TYP_OPPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Quantite total opportunite', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'QUAN_TOTL_OPPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Revenue attendu', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'REVN_ATTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Probabilite', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'PRBB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'MONT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag privé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_PRV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag supprime', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_SUPP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant souscrit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'MONT_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant debloque', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'MONT_DEBL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant verse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'MONT_VERS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant du pret', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'MONT_PRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date deblocage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DAT_DEBL_CRDT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date accord du credit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DAT_ACCR_CRDT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motif sans suite', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'MOTF_SANS_SUIT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motif affaire refusee', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'MOTF_AFFR_REFS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom famille produit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'NOM_FAML_PRDT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code famille produit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'COD_FAML_PRDT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Canal interaction', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'CANL_INTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Indication canal interaction origine', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'INDC_CANL_INTR_ORGN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Canal interaction origine', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'CANL_INTR_ORGN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Entite distributrice', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'ENTT_DIST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Reseau distributeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'RES_DIST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Canal de distribution origine', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'CANL_DIST_ORGN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Score entree en relation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'SCR_ENTR_RELT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom offre commerciale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'NOM_OFFR_COMM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code offre commerciale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'COD_OFFR_COMM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code conseiller', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'COD_CONS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Createur opportunite', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'CRTR_OPPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date cloture opportunite', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DAT_CLTR_OPPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de creation opportunite', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_OPPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'IDNT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Indication', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_INDC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Stade de vente', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'STD_VENT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom opportunite', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'NOM_OPPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant type enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'IDNT_TYP_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant opportunite', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'IDNT_OPPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere modification enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date creation enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Delai deblocage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DEL_DEBL_CRDT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Delai accord', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DEL_ACCR_CRDT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Age opportunité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'AGE_OPPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Entite distributrice de finalisation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'ENTT_DIST_FINL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Canal interaction de finalisation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'CANL_INTR_FINL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Canal de distribution', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'CANL_DIST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numero opportunite', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'NUMR_OPPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux d''endettement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'TAUX_ENDT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Soumetteur de l''approbation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'SOUM_APPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Push Topic Desirio TCH', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'PUSH_TOPC_DESR_TCH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom developpeur type d''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'NOM_DEVL_TYP_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motif Refus Engagement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'MOTF_REFS_ENGG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motif pré score', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'MOTF_PR_SCR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'IDNT_SF_PERS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Client OF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_CLNT_OF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Décision finale TCH', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DECS_FINL_TCH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date d''approbation manager', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DAT_APPR_MANG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Couleur pré score TCH', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'COUL_PR_SCR_TCH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Commentaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'COMM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code pré score', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'COD_PR_SCR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Canal d''interaction de finalisation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'CANL_INTR_FINL_2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Courant Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Souscripteur ou titulaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_SOUS_OU_TITL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Ligne de commande attribuee', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_LIGN_COMM_ATTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Facture Telco Opt in', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_FACT_TELC_OPT_IN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Dossier complet', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_DOSS_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Donnees telco Opt in', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_DONN_TELC_OPT_IN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag A supprimer', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_A_SUPP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Notification duplicate prospect', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_NOTF_DUPL_PRSP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Nombre souscription expiree', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_NOMB_SOUS_EXPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Nombre indication expiree', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_NOMB_INDC_EXPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Nombre creation opportunite', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_NOMB_CRTN_OPPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Nombre analyse manuelle', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_NOMB_ANLS_MANL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Fichier complete', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_FICH_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Entite en approbation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_ENTT_APPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Enrôlement Check Go', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_ENRL_CHCK_GO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Depuis marketing case', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_DEPS_MARK_CAS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag OAV Accordé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_OAV_ACCR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Assurance emprunteur principal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_ASSR_EMPR_PRNC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Assurance co emprunteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_ASSR_CO_EMPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fil des commentaires', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FIL_COMM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'NUMR_PERS_SF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Client SAB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'NUMR_CLNT_SAB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Reseau de distribution initial', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'RES_DIST_INTL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre de visites', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'NOMB_VIST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Lead', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'IDNT_LEAD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant du contact', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'IDNT_CNTC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Ancien Lead', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_ANCN_LEAD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Famille Produit Lead', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FAML_PRDT_LEAD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de première visite', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DAT_PRMR_VIST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création du Lead', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_LEAD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Salon', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'COD_SALN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Campagne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'COD_CAMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Canal d''interaction Lead', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'CANL_INTR_LEAD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Lien de reprise RIO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'LIEN_REPR_RIO';
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_OPP_DAT_OBS]
ON [dbo].[DWH_OPPORTUNITE] ([DAT_OBSR])
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_OPP_FLG_ENR_COU]
ON [dbo].[DWH_OPPORTUNITE] ([FLG_ENRG_COUR])
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Opt-in Préattribution Telco', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_OPT_IN_PRTT_TELC';

