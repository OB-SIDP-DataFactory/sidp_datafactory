﻿CREATE TABLE [dbo].[DWH_REF_PERS_REFR_EXTR]
(
	[NUMR_CLNT_SAB]			[varchar](7) NULL,
	[NUMR_CLNT_REFR_EXTR]	[varchar](15) NULL,
	[COD_REFR_EXTR]			[varchar](2) NULL,
	[COD_ETBL]				[int] NULL,
	[DAT_CRTN_ENRG]			[datetime] NULL,
	[DAT_DERN_MODF_ENRG]	[datetime] NULL,
	[FLG_ENRG_COUR]			[bit] NULL
)
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numéro Client SAB' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_REF_PERS_REFR_EXTR', @level2type=N'COLUMN',@level2name=N'NUMR_CLNT_SAB'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numéro Client Référence Externe' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_REF_PERS_REFR_EXTR', @level2type=N'COLUMN',@level2name=N'NUMR_CLNT_REFR_EXTR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Référence Externe' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_REF_PERS_REFR_EXTR', @level2type=N'COLUMN',@level2name=N'COD_REFR_EXTR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Etablissement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_REF_PERS_REFR_EXTR', @level2type=N'COLUMN',@level2name=N'COD_ETBL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date de création de l''enregistrement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_REF_PERS_REFR_EXTR', @level2type=N'COLUMN',@level2name=N'DAT_CRTN_ENRG'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date de dernière modification de l''enregistrement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_REF_PERS_REFR_EXTR', @level2type=N'COLUMN',@level2name=N'DAT_DERN_MODF_ENRG'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Enregistrement Courant' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_REF_PERS_REFR_EXTR', @level2type=N'COLUMN',@level2name=N'FLG_ENRG_COUR'
GO