CREATE TABLE [dbo].[DWH_PERS_SCR_PRTT] (
    [DAT_OBSR]           DATE          NOT NULL,
    [SF_NUMR_PERS]       NVARCHAR (11) NULL,
    [IDNT_SF_PERS]       NVARCHAR (18) NOT NULL,
    [COD_RES_DIST]       VARCHAR (2)   NULL,
    [LIBL_RES_DIST]      VARCHAR (80)  NULL,
    [SCR_PRTT_ORANGE]    VARCHAR (10)  NULL,
    [SCR_PRTT_GROUPAMA]  VARCHAR (10)  NULL,
    [COD_RES_DIST_PRNC]  VARCHAR (2)   NULL,
    [LIBL_RES_DIST_PRNC] VARCHAR (80)  NULL,
    [DAT_CRTN_ENRG]      DATETIME      NOT NULL,
    [DAT_DERN_MODF_ENRG] DATETIME      NOT NULL,
    [FLG_ENRG_COUR]      BIT           NOT NULL
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERS_SCR_PRTT', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERS_SCR_PRTT', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Réseau distributeur principal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERS_SCR_PRTT', @level2type = N'COLUMN', @level2name = N'LIBL_RES_DIST_PRNC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Réseau distributeur principal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERS_SCR_PRTT', @level2type = N'COLUMN', @level2name = N'COD_RES_DIST_PRNC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Score de pré-attribution Groupama', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERS_SCR_PRTT', @level2type = N'COLUMN', @level2name = N'SCR_PRTT_GROUPAMA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Score de pré-attribution Orange', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERS_SCR_PRTT', @level2type = N'COLUMN', @level2name = N'SCR_PRTT_ORANGE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Réseau de distribution', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERS_SCR_PRTT', @level2type = N'COLUMN', @level2name = N'LIBL_RES_DIST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Réseau de distribution', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERS_SCR_PRTT', @level2type = N'COLUMN', @level2name = N'COD_RES_DIST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant SF de la personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERS_SCR_PRTT', @level2type = N'COLUMN', @level2name = N'IDNT_SF_PERS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERS_SCR_PRTT', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERS_SCR_PRTT', @level2type = N'COLUMN', @level2name = N'SF_NUMR_PERS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Courant Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERS_SCR_PRTT', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';

