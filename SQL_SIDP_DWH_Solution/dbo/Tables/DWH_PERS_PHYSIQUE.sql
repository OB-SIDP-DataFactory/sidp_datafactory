﻿CREATE TABLE [dbo].[DWH_PERSONNE_PHYSIQUE] (
    [DAT_OBSR]                DATE            NULL,
    [IDNT_PERS_SF]            NVARCHAR (18)   NULL,
    [COD_CIVL]                NVARCHAR (40)   NULL,
    [NOM_USG]                 NVARCHAR (80)   NULL,
    [NOM_NAIS]                NVARCHAR (80)   NULL,
    [PRNM]                    NVARCHAR (40)   NULL,
    [NOM_PHNT]                NVARCHAR (255)  NULL,
    [NOM_NAIS_PHNT]           NVARCHAR (255)  NULL,
    [PRNM_PHNT]               NVARCHAR (255)  NULL,
    [DAT_NAIS]                DATE            NULL,
    [COD_POST_NAIS]           NVARCHAR (5)    NULL,
    [COMM_NAIS]               NVARCHAR (255)  NULL,
    [DEPR_NAIS]               NVARCHAR (80)   NULL,
    [PAYS_NAIS]               NVARCHAR (255)  NULL,
	COD_INSEE				  NVARCHAR(5),
    [DAT_DECS]                DATE            NULL,
    [NATN]                    NVARCHAR (255)  NULL,
    [NATN_AMRC]               NVARCHAR (255)  NULL,
    [COD_SITT_FAML]           NVARCHAR (255)  NULL,
    [REGM_MATR]               NVARCHAR (255)  NULL,
    [NOMB_ENFN_CHRG]          INT             NULL,
    [SEGM_GEOL]               NVARCHAR (100)  NULL,
    [REVN_MENS_NETS]          DECIMAL (18, 2) NULL,
    [DAT_COLL_REVN]           DATE            NULL,
    [REVN_JUST]               DECIMAL (18, 2) NULL,
    [DAT_DERN_MODF_REVN_JUST] DATE            NULL,
    [REVN_MOBL_MENS]          DECIMAL (18, 2) NULL,
    [REVN_FONC_MENS]          DECIMAL (18, 2) NULL,
    [AUTR_REVN_MENS]          DECIMAL (18, 2) NULL,
    [TOTL_REVN_DISP]          DECIMAL (18, 2) NULL,
    [TOTL_REVN]               DECIMAL (18, 2) NULL,
    [CHRG_MENS]               DECIMAL (18, 3) NULL,
    [AUTR_CHRG_MENS]          DECIMAL (18, 2) NULL,
    [TOTL_CHRG]               DECIMAL (18, 3) NULL,
    [IMPT_MENS]               DECIMAL (18, 3) NULL,
    [PENS_ALMN_MENS_VERS]     DECIMAL (18, 2) NULL,
    [PENS_ALMN_MENS_REC]      DECIMAL (18, 2) NULL,
    [CRDT_COUR_HORS_OB]       DECIMAL (18, 3) NULL,
    [MENS_AUTR_CRDT_COUR]     DECIMAL (18, 2) NULL,
    [PRST_SOCL_MENS]          DECIMAL (18, 2) NULL,
    [ALLC_LOGM]               DECIMAL (18, 3) NULL,
    [ALLC_FAML]               DECIMAL (18, 3) NULL,
    [PATR_IMMB]               DECIMAL (18, 3) NULL,
    [PATR_MOBL]               DECIMAL (18, 3) NULL,
    [ISF]                     NVARCHAR (255)  NULL,
    [AGNC_COMM]               NVARCHAR (255)  NULL,
    [SYST]                    NVARCHAR (255)  NULL,
    [DAT_MOBL]                DATE            NULL,
    [REFS_MAIL_PRDT]          NVARCHAR (4000) NULL,
    [REFS_SOUS]               NVARCHAR (1300) NULL,
    [ANML_NON_CFRM]           NVARCHAR (4000) NULL,
    [REAL_PAR]                NVARCHAR (18)   NULL,
    [PRFS]                    NVARCHAR (255)  NULL,
    [PRFS_PCS_PRNC]           NVARCHAR (255)  NULL,
    [PRFS_PCS_SECN]           NVARCHAR (255)  NULL,
    [TYP_CONT_TRVL]           NVARCHAR (255)  NULL,
    [DAT_DEBT_CONT_TRVL]      DATE            NULL,
    [DAT_FIN_CONT_TRVL]       DATE            NULL,
    [QUAL_EMPL]               NVARCHAR (255)  NULL,
    [DAT_REFS_6_MOIS]         DATE            NULL,
    [DAT_REFS_5_ANS]          DATE            NULL,
    [DAT_LIMT_INTR]           DATE            NULL,
    [DAT_RETR_MAIL]           DATETIME        NULL,
    [RAIS_RETR_MAIL]          NVARCHAR (255)  NULL,
    [CLNT_PHY_VIP_FIRS_PARN]  NVARCHAR (255)  NULL,
    [DAT_ENTR_RELT_OF]        DATETIME        NULL,
    [ACTN_FLX]                NVARCHAR (255)  NULL,
    [DAT_INSC_KIOS]           DATE            NULL,
    [PRFL_INVS]               NVARCHAR (255)  NULL,
    [DAT_MIS_JOUR_PRFL_INVS]  DATE            NULL,
    [SCR_CONF_IDNT_DIGT]      DECIMAL (4)     NULL,
    [SCR_COMP]                NVARCHAR (255)  NULL,
    [COTT_BANQ]               NVARCHAR (1)    NULL,
    [NIV_ACTV_BANC]           NVARCHAR (255)  NULL,
    [WEBS]                    NVARCHAR (255)  NULL,
    [SEGM_EPRG]               NVARCHAR (255)  NULL,
    [COD_TYP_OCCP_RESD_PRNC]  NVARCHAR (255)  NULL,
    [DAT_ENTR_RESD_PRNC]      DATE            NULL,
    [CAPC_JURD]               NVARCHAR (255)  NULL,
    [COD_PAYS_ENV_POST]       NVARCHAR (10)   NULL,
    [PAYS_ENV_POST]           NVARCHAR (80)   NULL,
    [AUTR_COD_PAYS]           NVARCHAR (10)   NULL,
    [AUTR_PAYS]               NVARCHAR (80)   NULL,
    [PRDT_BETA]               NVARCHAR (255)  NULL,
    [FLG_ACTL_DONN_CLNT]      BIT             NULL,
    [DAT_MODF_DONN_CLNT]      DATETIME        NULL,
    [STTT_KYC]                NVARCHAR (255)  NULL,
    [DAT_VALD_KYC]            DATE            NULL,
    [FLG_TCH_CLNT_DOUT]       BIT             NULL,
    [FLG_IDNT_CRTN_CLNT]      BIT             NULL,
    [DAT_CRTN_ENRG]           DATETIME        NULL,
    [DAT_DERN_MODF_ENRG]      DATETIME        NULL,
    [FLG_ENRG_COUR]           BIT             NULL
)
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Website', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'WEBS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type de contrat de travail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'TYP_CONT_TRVL'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Total revenus disponibles', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'TOTL_REVN_DISP'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Total revenus', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'TOTL_REVN'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Total Charges', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'TOTL_CHRG'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Système', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'SYST'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Statut KYC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'STTT_KYC'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Segment Geolife', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'SEGM_GEOL'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Segmentation Epargne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'SEGM_EPRG'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Score de confiance sur Identité digitale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'SCR_CONF_IDNT_DIGT'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Score de comportement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'SCR_COMP'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Revenus mobiliers mensuels', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'REVN_MOBL_MENS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Revenus mensuels nets', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'REVN_MENS_NETS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Revenus justifiés', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'REVN_JUST'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Revenus fonciers mensuels', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'REVN_FONC_MENS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Régime matrimonial', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'REGM_MATR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus Souscription', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'REFS_SOUS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus E-mail Produits', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'REFS_MAIL_PRDT'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Réalisé par', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'REAL_PAR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Raison Retour Mail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'RAIS_RETR_MAIL'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Qualification employé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'QUAL_EMPL'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prestations sociales mensuelles', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'PRST_SOCL_MENS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prénom phonétique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'PRNM_PHNT'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prénom', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'PRNM'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Profession PCS secondaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'PRFS_PCS_SECN'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Profession PCS principale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'PRFS_PCS_PRNC'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Profession', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'PRFS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Profil investisseur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'PRFL_INVS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Produit Beta', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'PRDT_BETA'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Pension alimentaire mensuelle versée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'PENS_ALMN_MENS_VERS'
GO
EXEC sp_addextendedproperty @name=N'MS_Description', @value=N'Code INSEE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_PERSONNE_PHYSIQUE', @level2type=N'COLUMN',@level2name=N'COD_INSEE'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Pension alimentaire mensuelle reçue', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'PENS_ALMN_MENS_REC'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Pays de naissance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'PAYS_NAIS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Pays Envoi Postal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'PAYS_ENV_POST'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Patrimoine mobilier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'PATR_MOBL'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Patrimoine immobilier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'PATR_IMMB'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre d''enfants à charge', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'NOMB_ENFN_CHRG'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom d''usage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'NOM_USG'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom phonétique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'NOM_PHNT'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom de naissance phonétique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'NOM_NAIS_PHNT'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom de naissance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'NOM_NAIS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Niveau d''activité bancaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'NIV_ACTV_BANC'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nationalité américaine', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'NATN_AMRC'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nationalité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'NATN'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mensualité Autre crédit en cours', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'MENS_AUTR_CRDT_COUR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ISF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'ISF'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Impôts mensuels', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'IMPT_MENS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Personne SF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'IDNT_PERS_SF'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag TCH Client douteux', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'FLG_TCH_CLNT_DOUT'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Identifiant Création Client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'FLG_IDNT_CRTN_CLNT'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Enregistrement Courant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Actualisation des données client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'FLG_ACTL_DONN_CLNT'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Département de naissance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'DEPR_NAIS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de validation KYC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'DAT_VALD_KYC'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Retour Mail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'DAT_RETR_MAIL'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Refus 6 mois', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'DAT_REFS_6_MOIS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Refus 5 ans', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'DAT_REFS_5_ANS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'DAT_OBSR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de naissance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'DAT_NAIS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de modification des données client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'DAT_MODF_DONN_CLNT'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de mobilité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'DAT_MOBL'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de mise à jour Profil investisseur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'DAT_MIS_JOUR_PRFL_INVS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date limite d''interdit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'DAT_LIMT_INTR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date d''inscription Kiosque', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'DAT_INSC_KIOS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de fin du contrat de travail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'DAT_FIN_CONT_TRVL'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date d''entrée Résidence Principale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'DAT_ENTR_RESD_PRNC'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date d''entrée en relation avec OF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'DAT_ENTR_RELT_OF'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification du revenu justifié', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_REVN_JUST'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de décès', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'DAT_DECS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de début du contrat de travail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'DAT_DEBT_CONT_TRVL'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Collecte Revenus', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'DAT_COLL_REVN'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Crédit en cours hors OB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'CRDT_COUR_HORS_OB'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cotation Banque', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'COTT_BANQ'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Commune de naissance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'COMM_NAIS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Type Occupation Résidence principale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'COD_TYP_OCCP_RESD_PRNC'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Situation familiale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'COD_SITT_FAML'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code postal de naissance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'COD_POST_NAIS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Pays Envoi Postal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'COD_PAYS_ENV_POST'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Civilité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'COD_CIVL'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Client VIP / First / Parnasse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'CLNT_PHY_VIP_FIRS_PARN'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Charges mensuelles', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'CHRG_MENS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Capacité juridique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'CAPC_JURD'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autres revenus mensuels', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'AUTR_REVN_MENS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autre pays', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'AUTR_PAYS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autre code pays', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'AUTR_COD_PAYS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autres charges mensuelles', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'AUTR_CHRG_MENS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Anomalie Non Coformité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'ANML_NON_CFRM'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Allocation logement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'ALLC_LOGM'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Allocations familiales', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'ALLC_FAML'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Agence commerciale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'AGNC_COMM'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Action Flux', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_PHYSIQUE', @level2type = N'COLUMN', @level2name = N'ACTN_FLX'
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_PERS_PHY_FLG_ENR_COU]
ON [dbo].[DWH_PERSONNE_PHYSIQUE] ([FLG_ENRG_COUR])
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_PERS_PHY_DAT_OBS]
ON [dbo].[DWH_PERSONNE_PHYSIQUE] ([DAT_OBSR])
GO