﻿CREATE TABLE  [dbo].[DWH_PERSONNE] (
    [DAT_OBSR]             DATE            NULL,
    [IDNT_PERS_SF]         NVARCHAR (18)   NULL,
    [NUMR_PERS_SF]         NVARCHAR (11)   NULL,
    [NUMR_CLNT_SAB]        NVARCHAR (18)   NULL,
	[IDNT_CLNT_FE]		   NUMERIC(19,0)   NULL,
    [NUMR_CLNT_GRC]        NVARCHAR (11)   NULL,
    [NOM]                  NVARCHAR (255)  NULL,
    [DAT_ENTR_RELT_OB]     DATE            NULL,
    [DAT_FIN_RELT_OB]      DATE            NULL,
    [ANCNT_OB]             INT             NULL,
	[DAT_CLTR]			   date		       NULL,
    [COD_AGNC]             INT             NULL,
    [COD_CATG_PERS]        NVARCHAR (40)   NULL,
    [IDNT_TYP_ENRG]        NVARCHAR (18)   NULL,
    [COD_RES_DIST_PRNC]    NVARCHAR (255)  NULL,
    [RESX_DIST]            NVARCHAR (4000) NULL,
    [COD_PAYS_LIVR]        NVARCHAR (10)   NULL,
    [PAYS_LIVR]            NVARCHAR (80)   NULL,
    [PAYS_RESD_FISC]       NVARCHAR (255)  NULL,
    [IDNT_CNTC]            NVARCHAR (18)   NULL,
    [IDNT_TCH]             NVARCHAR (18)   NULL,
    [IDNT_INWEBO]          NVARCHAR (1300) NULL,
    [NUMR_SIRET]           NVARCHAR (14)   NULL,
    [COD_SIREN]            VARCHAR (9)     NULL,
    [LIBL_NAF_REV2]        NVARCHAR (80)   NULL,
    [COD_NAF_REV2]         NVARCHAR (5)    NULL,
    [FLG_INCD_COUR]        BIT             NULL,
    [LIEN_SPNS]            DECIMAL (18, 2) NULL,
    [MARC]                 NVARCHAR (255)  NULL,
    [CHFF_AFFR]            DECIMAL (18, 2) NULL,
    [ANN_CHFF_AFFR]        DATE            NULL,
    [SECR_BANC]            NVARCHAR (255)  NULL,
	[DRT_OPPS]             NVARCHAR (255)  NULL,
    [SOUS_RES_BOUT]        NVARCHAR (255)  NULL,
    [SECT_ACTV]            NVARCHAR (255)  NULL,
    [NUMR_CLNT_DIST]       NVARCHAR (15)   NULL,
    [DAT_DERN_ACTV]        DATE            NULL,
    [FLG_DIST_TCH]         BIT             NULL,
    [FLG_RGPD]             BIT             NULL,
    [FLG_NON_CFRM_PJ]      BIT             NULL,
    [FLG_HABL_IOBSP_TECH]  BIT             NULL,
    [CERT_IOBSP]           NVARCHAR (255)  NULL,
    [FLG_HABL_TA3C]        BIT             NULL,
    [FLG_CERT_AMF]         BIT             NULL,
    [SEGM_DIST]            NVARCHAR (255)  NULL,
    [FLG_CLNT_PRMM]        BIT             NULL,
    [FLG_CLNT_CLUB]        BIT             NULL,
    [COTT_RISQ]            NVARCHAR (255)  NULL,
	[COTT_INTR]			   VARCHAR(3)	   NULL,
    [FLG_CLNT_MIGR]        BIT             NULL,
    [FLG_ADRS_INCR]        BIT             NULL,
    [FLG_ADRS_INVL]        BIT             NULL,
    [FLG_COMP_SIM]         BIT             NULL,
    [FLG_COMP_TELP]        BIT             NULL,
    [FLG_DESN_MAIL]        BIT             NULL,
    [FLG_DESN_TELC]        BIT             NULL,
    [FLG_FATCA]            BIT             NULL,
    [FLG_NE_PAS_APPL]      BIT             NULL,
    [FLG_RECV]             BIT             NULL,
    [FLG_RESD_FISC_FRNC]   BIT             NULL,
    [FLG_SUPP]             BIT             NULL,
    [FLG_CLNT_AUTH]        BIT             NULL,
    [FLG_COMP_PORT_CLNT]   BIT             NULL,
    [ETT_ADRS]             NVARCHAR (1300) NULL,
    [FLG_COMP_BLQ]         BIT             NULL,
    [FLG_DEPS_DECV]        BIT             NULL,
    [FLG_EXCT_CTRL_UNCT]   BIT             NULL,
    [FLG_PERS]             BIT             NULL,
    [FLG_SOUS_OU_TITL]     BIT             NULL,
    [FLG_SYNC_COMP_CNTC]   BIT             NULL,
    [FLG_TAX_RESD_FRNC]    BIT             NULL,
    [FLG_VISB_PART_TCH]    BIT             NULL,
    [FLG_DESN_MOBL]        BIT             NULL,
    [FLG_REFS_RECM]        BIT             NULL,
    [FLG_REFS_APPL_TELP]   BIT             NULL,
    [FLG_REFS_CNTC_COUR]   BIT             NULL,
    [FLG_REFS_MAIL]        BIT             NULL,
    [FLG_REFS_MAIL_PART]   BIT             NULL,
    [FLG_REFS_SMS]         BIT             NULL,
    [FLG_REFS_SMS_PART]    BIT             NULL,
    [FLG_REFS_NOTF_PAIE]   BIT             NULL,
    [FLG_REFS_NOTF_SOLD]   BIT             NULL,
    [FLG_PRMR_ENRL_TCH]    BIT             NULL,
    [FLG_OPT_IN_DONN_TELC] BIT             NULL,
    [FLG_OPT_IN_FACT_TELC] BIT             NULL,
    [COD_RESP_EXPL]        VARCHAR (3)     NULL,
    [COD_QUAL_CLNT]        VARCHAR (3)     NULL,
    [ZON_RUBR_2]           VARCHAR (15)    NULL,
    [SECT_ACTV_REGL]       VARCHAR (6)     NULL,
    [COD_DOUT_REGL]        VARCHAR (1)     NULL,
    [COD_CLNT_DOUT]        VARCHAR (2)     NULL,
    [TYP_INTR]             VARCHAR (2)     NULL,
    [MOTF_RETR_COUR]       NVARCHAR (255)  NULL,
    [DAT_PRPG]             DATETIME        NULL,
    [FLG_INTR_CHQR]        BIT             NULL,
    [TCH_ORGN_INTR_CHQR]   NVARCHAR (255)  NULL,
    [TCH_INTR_CHQR]        NVARCHAR (255)  NULL,
    [IDNT_PRPR]            NVARCHAR (18)   NULL,
    [DAT_CRTN]             DATETIME        NULL,
    [IDNT_UTLS_CRTN]       NVARCHAR (18)   NULL,
    [DAT_DERN_MODF]        DATETIME        NULL,
    [IDNT_UTLS_MODF]       NVARCHAR (18)   NULL,
    [FLG_CLNT]             BIT             NULL,
    [FLG_ANC_CLNT]         BIT             NULL,
    [DAT_CRTN_ENRG]        DATETIME        NULL,
    [DAT_DERN_MODF_ENRG]   DATETIME        NULL,
    [FLG_ENRG_COUR]        BIT             NULL
)
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Zone Rubrique 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'ZON_RUBR_2'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type Interdit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'TYP_INTR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TCH Origine Interdiction Chéquier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'TCH_ORGN_INTR_CHQR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TCH Interdiction Chéquier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'TCH_INTR_CHQR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Sous Réseau Boutique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'SOUS_RES_BOUT'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Segmentation distributive', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'SEGM_DIST'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Secteur d''activité réglementaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'SECT_ACTV_REGL'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Secteur d''activité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'SECT_ACTV'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Secret bancaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'SECR_BANC'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Réseaux distributeurs', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'RESX_DIST'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Pays Résidence fiscale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'PAYS_RESD_FISC'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Pays Livraison', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'PAYS_LIVR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro SIRET', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'NUMR_SIRET'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Personne SF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'NUMR_PERS_SF'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Client SAB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'NUMR_CLNT_SAB'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Client GRC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'NUMR_CLNT_GRC'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Client Distributeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'NUMR_CLNT_DIST'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'NOM'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motifs Retours Courriers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'MOTF_RETR_COUR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Marché', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'MARC'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Lien Sponsor', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'LIEN_SPNS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé NAF Rev2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'LIBL_NAF_REV2'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Utilisateur Modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'IDNT_UTLS_MODF'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Utilisateur Création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'IDNT_UTLS_CRTN'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Type Enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'IDNT_TYP_ENRG'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant TCH', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'IDNT_TCH'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Proprietaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'IDNT_PRPR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Personne SF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'IDNT_PERS_SF'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Inwebo', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'IDNT_INWEBO'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Contact ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'IDNT_CNTC'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Visibilité Partenaires TCH', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_VISB_PART_TCH'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Taxe Résidence en France', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_TAX_RESD_FRNC'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Synchronisation Compte Contact', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_SYNC_COMP_CNTC'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Supprimé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_SUPP'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Souscripteur ou Titulaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_SOUS_OU_TITL'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag RGPD', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_RGPD'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Résidence fiscal en France', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_RESD_FISC_FRNC'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Refus SMS Partenaires', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_REFS_SMS_PART'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Refus SMS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_REFS_SMS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Refus Recommandation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_REFS_RECM'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Refus notifications solde', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_REFS_NOTF_SOLD'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Refus notification paiement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_REFS_NOTF_PAIE'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Refus Mail Partenaires', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_REFS_MAIL_PART'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Refus Mail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_REFS_MAIL'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Refus Contact Courrier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_REFS_CNTC_COUR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Refus appels téléphoniques', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_REFS_APPL_TELP'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Recouvrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_RECV'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Premier Enrôlement TCH', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_PRMR_ENRL_TCH'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_PERS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Opt In Facture Telco', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_OPT_IN_FACT_TELC'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Opt In Données Telco', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_OPT_IN_DONN_TELC'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Non Conformité PJ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_NON_CFRM_PJ'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Ne pas appeler', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_NE_PAS_APPL'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Interdiction Chéquier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_INTR_CHQR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Incident en cours', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_INCD_COUR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Habilitation TA3C', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_HABL_TA3C'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Tech Temp Habilitation IOBSP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_HABL_IOBSP_TECH'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag FATCA', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_FATCA'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Exécution Contrôle Unicite', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_EXCT_CTRL_UNCT'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Enregistrement Courant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Distributeur TCH', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_DIST_TCH'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Désinscription Télécopie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_DESN_TELC'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Désinscription Mobile', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_DESN_MOBL'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Désinscription Mail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_DESN_MAIL'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Dépassement de découvert', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_DEPS_DECV'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Compatibilité Téléphone', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_COMP_TELP'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Compatibilité SIM', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_COMP_SIM'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Compte du portail client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_COMP_PORT_CLNT'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Compte bloqué', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_COMP_BLQ'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Client Premium', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_CLNT_PRMM'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Client migré', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_CLNT_MIGR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Client Club', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_CLNT_CLUB'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Client authentifié', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_CLNT_AUTH'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_CLNT'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Certification AMF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_CERT_AMF'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Ancien Client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_ANC_CLNT'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Adresse invalide', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_ADRS_INVL'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Adresse incorrecte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_ADRS_INCR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Etat de l''adresse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'ETT_ADRS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de propagation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'DAT_PRPG'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'DAT_OBSR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de fin de relation OB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'DAT_FIN_RELT_OB'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de clôture', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'DAT_CLTR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date d''entrée en relation OB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'DAT_ENTR_RELT_OB'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière activité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_ACTV'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'DAT_CRTN'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cotation Risque', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'COTT_RISQ'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cotation interne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'COTT_INTR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code SIREN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'COD_SIREN'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Responsable Exploitation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'COD_RESP_EXPL'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Réseau distributeur principal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'COD_RES_DIST_PRNC'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Qualité Client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'COD_QUAL_CLNT'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Pays Livraison', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'COD_PAYS_LIVR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code NAF Rev2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'COD_NAF_REV2'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Douteux réglementaire ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'COD_DOUT_REGL'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Droit d''opposition', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'DRT_OPPS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Client douteux', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'COD_CLNT_DOUT'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Catégorie Personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'COD_CATG_PERS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Agence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'COD_AGNC'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Chiffre d''affaires', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'CHFF_AFFR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Certification IOBSP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'CERT_IOBSP'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Année Chiffre d''affaires', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'ANN_CHFF_AFFR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ancienneté OB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE', @level2type = N'COLUMN', @level2name = N'ANCNT_OB'
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_PERS_FLG_ENR_COU]
ON [dbo].[DWH_PERSONNE] ([FLG_ENRG_COUR])
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_PERS_DAT_OBS]
ON [dbo].[DWH_PERSONNE] ([DAT_OBSR])
GO