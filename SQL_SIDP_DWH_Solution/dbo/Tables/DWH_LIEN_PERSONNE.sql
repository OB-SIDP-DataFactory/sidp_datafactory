﻿CREATE TABLE [dbo].[DWH_LIEN_PERSONNE] (
    [DAT_OBSR]           DATE          NULL,
    [NUMR_LIEN]          VARCHAR (80)  NULL,
    [IDNT_SF_LIEN]       NVARCHAR (18) NULL,
    [IDNT_SF_TYP_ENRG]   NVARCHAR (18) NULL,
    [COD_NATR_LIEN]      VARCHAR (20)  NULL,
    [IDNT_SF_PERS1]      NVARCHAR (18) NULL,
    [NUMR_PERS1]         NVARCHAR (11) NULL,
    [COD_ROL_PERS1]      VARCHAR (20)  NULL,
    [IDNT_SF_PERS2]      NVARCHAR (18) NULL,
    [NUMR_PERS2]         NVARCHAR (11) NULL,
    [COD_ROL_PERS2]      VARCHAR (20)  NULL,
    [DAT_CRTN_LIEN]      DATETIME      NULL,
    [DAT_DERN_MODF_LIEN] DATETIME      NULL,
    [DAT_EXPR]           DATETIME      NULL,
    [FLG_LIEN_ACTF]      BIT           NULL,
    [FLG_LIEN_A_VERF]    BIT           NULL,
    [DAT_CRTN_ENRG]      DATETIME      NULL,
    [DAT_DERN_MODF_ENRG] DATETIME      NULL,
    [FLG_ENRG_COUR]      BIT           NULL
)
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PERSONNE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PERSONNE', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Lien à vérifier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_LIEN_A_VERF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Lien Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_LIEN_ACTF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date d''expiration', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PERSONNE', @level2type = N'COLUMN', @level2name = N'DAT_EXPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification du lien', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PERSONNE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_LIEN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création du lien', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PERSONNE', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_LIEN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Rôle Personne 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PERSONNE', @level2type = N'COLUMN', @level2name = N'COD_ROL_PERS2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Personne 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PERSONNE', @level2type = N'COLUMN', @level2name = N'NUMR_PERS2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Rôle Personne 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PERSONNE', @level2type = N'COLUMN', @level2name = N'COD_ROL_PERS1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Personne 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PERSONNE', @level2type = N'COLUMN', @level2name = N'NUMR_PERS1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Nature Lien', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PERSONNE', @level2type = N'COLUMN', @level2name = N'COD_NATR_LIEN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant SF LIEN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PERSONNE', @level2type = N'COLUMN', @level2name = N'IDNT_SF_LIEN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro du lien', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PERSONNE', @level2type = N'COLUMN', @level2name = N'NUMR_LIEN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PERSONNE', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant SF Type Enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PERSONNE', @level2type = N'COLUMN', @level2name = N'IDNT_SF_TYP_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Courant Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PERSONNE', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant SF de la personne 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PERSONNE', @level2type = N'COLUMN', @level2name = N'IDNT_SF_PERS2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant SF de la personne 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PERSONNE', @level2type = N'COLUMN', @level2name = N'IDNT_SF_PERS1';
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_LIE_PER_FLG_ENR_COU]
ON [dbo].[DWH_LIEN_PERSONNE] ([FLG_ENRG_COUR])
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_LIE_PER_DAT_OBS]
ON [dbo].[DWH_LIEN_PERSONNE] ([DAT_OBSR])
GO