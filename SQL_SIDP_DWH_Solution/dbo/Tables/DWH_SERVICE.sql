﻿CREATE TABLE [dbo].[DWH_SERVICE] (
    [DAT_OBSR]           DATE        NOT NULL,
    [NUMR_ABNN]          INT         NULL,
    [COD_SERV_SAB]       VARCHAR (6) NULL,
    [COD_ETBL]           VARCHAR (4) NULL,
    [COD_AGNC]           VARCHAR (4) NULL,
    [COD_SERV]           VARCHAR (2) NULL,
    [COD_SOUS]           VARCHAR (2) NULL,
    [COD_ETT]            VARCHAR (1) NULL,
    [DAT_ADHS]           DATE        NULL,
    [DAT_FIN]            DATE        NULL,
    [DAT_RENV]           DATE        NULL,
    [DAT_RESL]           DATE        NULL,
    [MOTF_RESL]          VARCHAR (6) NULL,
    [DAT_CRTN]           DATE        NULL,
    [DAT_VALDTN]         DATE        NULL,
    [DAT_CRTN_ENRG]      DATETIME    NULL,
    [DAT_DERN_MODF_ENRG] DATETIME    NULL,
    [FLG_ENRG_COUR]      BIT         NULL
)
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_SVC_FLG_ENR_COU]
ON [dbo].[DWH_SERVICE] ([FLG_ENRG_COUR])
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_SVC_DAT_OBS]
ON [dbo].[DWH_SERVICE] ([DAT_OBSR])
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SERVICE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SERVICE', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Validation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SERVICE', @level2type = N'COLUMN', @level2name = N'DAT_VALDTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SERVICE', @level2type = N'COLUMN', @level2name = N'DAT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motif Résiliation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SERVICE', @level2type = N'COLUMN', @level2name = N'MOTF_RESL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Résiliation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SERVICE', @level2type = N'COLUMN', @level2name = N'DAT_RESL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Etat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SERVICE', @level2type = N'COLUMN', @level2name = N'COD_ETT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date renouvellement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SERVICE', @level2type = N'COLUMN', @level2name = N'DAT_RENV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Fin', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SERVICE', @level2type = N'COLUMN', @level2name = N'DAT_FIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Adhésion', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SERVICE', @level2type = N'COLUMN', @level2name = N'DAT_ADHS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Service SAB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SERVICE', @level2type = N'COLUMN', @level2name = N'COD_SERV_SAB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Sous-Service', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SERVICE', @level2type = N'COLUMN', @level2name = N'COD_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Service', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SERVICE', @level2type = N'COLUMN', @level2name = N'COD_SERV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Agence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SERVICE', @level2type = N'COLUMN', @level2name = N'COD_AGNC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Etablissement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SERVICE', @level2type = N'COLUMN', @level2name = N'COD_ETBL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SERVICE', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Courant Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SERVICE', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Abonnement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_SERVICE', @level2type = N'COLUMN', @level2name = N'NUMR_ABNN'
GO