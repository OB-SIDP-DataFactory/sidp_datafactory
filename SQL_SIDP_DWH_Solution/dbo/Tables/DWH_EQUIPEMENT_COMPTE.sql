﻿CREATE TABLE [dbo].[DWH_EQUIPEMENT_COMPTE] (
    [DAT_OBSR]                       DATE            NULL,
    [COD_ETBL]                       VARCHAR (4)     NULL,
    [NUMR_PLAN]                      INT             NULL,
    [COD_AGNC]                       VARCHAR (4)     NULL,
    [NUMR_COMP]                      VARCHAR (20)    NULL,
    [COD_RUBR]                       VARCHAR (10)    NULL,
    [TYP_COMP]                       VARCHAR (3)     NULL,
    [COD_PRDT_SF]                    VARCHAR (3)     NULL,
    [INTITULE]                       VARCHAR (32)    NULL,
    [NOMB_TITL]                      INT             NULL,
    [NUMR_CLNT_SAB_TITL_PRCP]        VARCHAR (7)     NULL,
    [DAT_OUVR]                       DATE            NULL,
    [DAT_CLTR]                       DATE            NULL,
    [MOTF_CLTR]                      VARCHAR (6)     NULL,
    [COD_DEVS]                       VARCHAR (3)     NULL,
    [SOLD_DAT_OPE]                   DECIMAL (18, 3) NULL,
    [SOLD_DAT_VALR]                  DECIMAL (18, 3) NULL,
    [SOLD_DAT_COMP]                  DECIMAL (18, 3) NULL,
    [SOLD_DAT_COMP_DEVS_COMP]        DECIMAL (18, 3) NULL,
    [DUR_REST_COUR_JOUR]             INT             NULL,
    [DUR_INTL_JOUR]                  INT             NULL,
    [GARN_DEVS_OPE]                  DECIMAL (18, 3) NULL,
    [GARN_DEVS_BASE]                 DECIMAL (18, 3) NULL,
    [COD_GARN]                       VARCHAR (6)     NULL,
    [GARANT]                         VARCHAR (7)     NULL,
    [COD_QUAL_GARN]                  VARCHAR (3)     NULL,
    [COD_RESD_GARN]                  VARCHAR (3)     NULL,
    [NON_UTLS]                       VARCHAR (3)     NULL,
    [SOMM_FLX_DEBIT_MOIS]            DECIMAL (18, 3) NULL,
    [SOMM_FLX_CRDT_MOIS]             DECIMAL (18, 3) NULL,
    [SOLD_MOYE_COMP_DEBIT]           DECIMAL (18, 3) NULL,
    [SOLD_MOYE_COMP_CRDT]            DECIMAL (18, 3) NULL,
    [ECHL_COD_SELC]                  VARCHAR (3)     NULL,
    [ECHL_DAT_DERN_ARRT]             DATE            NULL,
    [ECHL_UNT_PERD]                  VARCHAR (1)     NULL,
    [ECHL_NOMB_UNTS_PERD]            INT             NULL,
    [MONT_AUTR]                      DECIMAL (18, 3) NULL,
    [DAT_DEBT_DECV]                  DATE            NULL,
    [MONT_UTLS]                      DECIMAL (18, 3) NULL,
    [MONT_DEPS]                      DECIMAL (18, 3) NULL,
    [NOMB_JOUR_DEPS]                 INT             NULL,
    [FLG_DEPS_FACL_CAIS]             BIT             NULL,
    [FLG_MOBL_BANC]                  BIT             NULL,
    [IEDOM_MONT_REFN]                DECIMAL (18, 3) NULL,
    [IEDOM_MONT_NON_REFN]            DECIMAL (18, 3) NULL,
    [IEDOM_MONT_SENS_RESR]           DECIMAL (18, 3) NULL,
    [CONTVAL_SOLD_MOYE_DEBIT]        DECIMAL (18, 3) NULL,
    [CONTVAL_SOLD_MOYE_CRDT]         DECIMAL (18, 3) NULL,
    [CONTVAL_MONT_DEBIT]             DECIMAL (18, 3) NULL,
    [CONTVAL_MONT_CRDT]              DECIMAL (18, 3) NULL,
    [MONT_INTR_DEBIT_CUML]           DECIMAL (18, 3) NULL,
    [MONT_INTR_DEBIT_CUML_DEVS_BASE] DECIMAL (18, 3) NULL,
    [PART_BANQ_TRSR]                 DECIMAL (18, 3) NULL,
    [PART_BANQ_TRSR_DEVS_BASE]       DECIMAL (18, 3) NULL,
    [FLG_COMP_MAIT]                  BIT             NULL,
    [NUMR_COMP_MAIT]                 VARCHAR (20)    NULL,
    [CUML_MOUV_CRDT_DEVS_BASE]       DECIMAL (18, 3) NULL,
    [COMM_PRVS_ECHL_DEVS_COMP]       DECIMAL (18, 3) NULL,
    [COMM_PRVS_ECHL_DEVS_BASE]       DECIMAL (18, 3) NULL,
    [CLSS_SECR]                      INT             NULL,
    [FLG_UTLS_MOIS]                  BIT             NULL,
    [TAUX_NOMN]                      DECIMAL (14, 9) NULL,
    [REVN_COTITL]                    DECIMAL (18, 3) NULL,
    [COD_SURT]                       VARCHAR (1)     NULL,
    [FRQN]                           VARCHAR (30)    NULL,
    [FLG_PERM_RISQ]                  BIT             NULL,
    [FLG_COMP_PRDT]                  BIT             NULL,
    [DAT_CRTN_ENRG]                  DATETIME        NULL,
    [DAT_DERN_MODF_ENRG]             DATETIME        NULL,
    [FLG_ENRG_COUR]                  BIT             NULL,
    [COD_APPOR]                      VARCHAR (3)     NULL
)
GO
CREATE NONCLUSTERED INDEX IX_NC_DWH_EQU_CPT_NUM_FLG
ON [dbo].[DWH_EQUIPEMENT_COMPTE] ([NUMR_COMP], [FLG_ENRG_COUR])
INCLUDE ([COD_RUBR], [NUMR_CLNT_SAB_TITL_PRCP])
GO
CREATE NONCLUSTERED INDEX IX_NC_EQP_CPT_DTE_NUM_CPT
ON [dbo].[DWH_EQUIPEMENT_COMPTE] ([DAT_OBSR], [NUMR_COMP], [NUMR_CLNT_SAB_TITL_PRCP])
GO
CREATE NONCLUSTERED INDEX [IX_NC_EQP_CPT_RUB_FLG_DTE]
ON [dbo].[DWH_EQUIPEMENT_COMPTE] ([COD_RUBR], [FLG_ENRG_COUR], [DAT_OUVR])
INCLUDE([NUMR_COMP], [NUMR_CLNT_SAB_TITL_PRCP])
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Compte Produit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'FLG_COMP_PRDT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Périmètre Risque', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'FLG_PERM_RISQ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fréquence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'FRQN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Sûreté', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'COD_SURT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'REVENUS COTITULAIRE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'REVN_COTITL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TX NOMINAL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'TAUX_NOMN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Utilisation dans le mois', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'FLG_UTLS_MOIS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Classe de sécurité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'CLSS_SECR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Commission provisionnelle échelles en devise de base', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'COMM_PRVS_ECHL_DEVS_BASE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Commission provisionnelle échelles en devise du compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'COMM_PRVS_ECHL_DEVS_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cumul des mouvements créditeurs en devise de base', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'CUML_MOUV_CRDT_DEVS_BASE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Compte Maître', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'NUMR_COMP_MAIT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Compte Maître', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'FLG_COMP_MAIT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre de jours en dépassement ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'NOMB_JOUR_DEPS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Début Découvert ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'DAT_DEBT_DECV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Part de la banque en trésorerie devise de base', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'PART_BANQ_TRSR_DEVS_BASE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Part de la banque en trésorerie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'PART_BANQ_TRSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant des intérêts provisionnels débiteurs cumulés en devise de base ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'MONT_INTR_DEBIT_CUML_DEVS_BASE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant des intérêts provisionnels débiteurs cumulés', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'MONT_INTR_DEBIT_CUML';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Ouverture', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'DAT_OUVR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Clôture', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'DAT_CLTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motif Clôture', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'MOTF_CLTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type Compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'TYP_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Contrevaleur Montant Créditeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'CONTVAL_MONT_CRDT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Contrevaleur Montant Débiteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'CONTVAL_MONT_DEBIT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Contrevaleur Solde Moyen Créditeur ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'CONTVAL_SOLD_MOYE_CRDT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Contrevaleur Solde Moyen Débiteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'CONTVAL_SOLD_MOYE_DEBIT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IEDOM Montant sens réservé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'IEDOM_MONT_SENS_RESR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IEDOM Montant non refinancé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'IEDOM_MONT_NON_REFN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IEDOM Montant refinancé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'IEDOM_MONT_REFN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Mobilité Bancaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'FLG_MOBL_BANC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Dépassement Facilité de caisse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'FLG_DEPS_FACL_CAIS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant en dépassement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'MONT_DEPS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant utilisé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'MONT_UTLS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant autorisé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'MONT_AUTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Echelles Nombre Unités de période', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'ECHL_NOMB_UNTS_PERD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Echelles Unité de période', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'ECHL_UNT_PERD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Echelles Date Dernier Arrêté', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'ECHL_DAT_DERN_ARRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Echelles Code Sélection ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'ECHL_COD_SELC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Solde moyen comptable  créditeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'SOLD_MOYE_COMP_CRDT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Solde moyen comptable débiteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'SOLD_MOYE_COMP_DEBIT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Somme Flux créditeurs du mois (date comptable)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'SOMM_FLX_CRDT_MOIS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Somme Flux débiteurs du mois (date comptable)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'SOMM_FLX_DEBIT_MOIS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Non utilisé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'NON_UTLS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Résidence Garant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'COD_RESD_GARN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Qualité Garant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'COD_QUAL_GARN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Garant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'GARANT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Garantie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'COD_GARN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Garantie en devise de base', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'GARN_DEVS_BASE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Durée initiale en jours', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'DUR_INTL_JOUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Durée restante à courir en jours', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'DUR_REST_COUR_JOUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Solde Date Comptable en devise du compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'SOLD_DAT_COMP_DEVS_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Solde Date Comptable', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'SOLD_DAT_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Solde Date Valeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'SOLD_DAT_VALR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Devise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'COD_DEVS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Agence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'COD_AGNC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Client SAB Titulaire Pricipal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'NUMR_CLNT_SAB_TITL_PRCP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre Titulaires', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'NOMB_TITL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Intitulé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'INTITULE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Rubrique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'COD_RUBR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'NUMR_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Plan ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'NUMR_PLAN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Etablissement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'COD_ETBL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Produit SF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'COD_PRDT_SF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Courant Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Solde Date Opération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'SOLD_DAT_OPE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Garantie en devise d’opération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'GARN_DEVS_OPE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code apporteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_COMPTE', @level2type = N'COLUMN', @level2name = N'COD_APPOR';
GO