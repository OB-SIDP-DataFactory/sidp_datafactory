﻿CREATE TABLE [dbo].[DWH_DOSSIER_SOUSCRIPTION] (
    [DAT_OBSR]              DATE            NOT NULL,
    [IDNT_DOSS_SOUS]        DECIMAL (19)    NOT NULL,
    [NUMR_OPPR]             NVARCHAR (30)   NULL,
    [IDNT_OPPR]             NVARCHAR (255)  NOT NULL,
    [IDNT_OFFR_COMM]        DECIMAL (19)    NULL,
    [COD_OFFR_COMM]         NVARCHAR (255)  NULL,
    [LIBL_OFFR_COMM]        NVARCHAR (255)  NULL,
    [IDNT_SOUS_STTT]        DECIMAL (19)    NOT NULL,
    [IDNT_SUSP_CONT_STTT]   DECIMAL (19)    NULL,
    [IDNT_PRSP]             DECIMAL (38)    NULL,
    [IDNT_DOSS_JUST]        DECIMAL (38)    NULL,
    [IDNT_RELT_PERS]        DECIMAL (38)    NULL,
    [DAT_CRTN]              DATETIME2 (6)   NULL,
    [IDNT_CRTN]             NVARCHAR (255)  NULL,
    [DAT_DERN_MODF]         DATETIME2 (6)   NOT NULL,
    [IDNT_DERN_MODF]        NVARCHAR (255)  NULL,
    [TYP_UTLS]              NVARCHAR (255)  NULL,
    [ALS]                   NVARCHAR (255)  NOT NULL,
    [ALS_FACT_RES_DIST]     NVARCHAR (255)  NULL,
    [DAT_FIN]               DATE            NULL,
    [FLG_ENV_EML_PRMR_DEPT] BIT             NULL,
    [FLG_DOSS_COMP]         BIT             NULL,
    [DAT_ACCP_GC]           DATE            NULL,
    [FLG_VERF_IDNT_VEND]    BIT             NULL,
    [DAT_VERF_IDNT]         DATETIME2 (6)   NULL,
    [SCR_COMP_APPL]         DECIMAL (10)    NOT NULL,
    [COD_ERRR_COR_BANK]     NVARCHAR (255)  NULL,
    [MESS_ERRR_COR_BANK]    NVARCHAR (4000) NULL,
    [FLG_CONT_ENV]          BIT             NULL,
    [FLG_DOCM_PRCN_ENV]     BIT             NULL,
    [PRMR_DEPT_TRNS_IBAN]   NVARCHAR (255)  NULL,
    [DAT_CRTN_ENRG]         DATETIME        NULL,
    [DAT_DERN_MODF_ENRG]    DATETIME        NULL,
    [FLG_ENRG_COUR]         BIT             NULL,
    [FLG_CONT_RIO]          BIT             NULL,
    [FLG_CONS_VIE]          BIT             NULL
)
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_DOS_SOUS_FLG_ENR_COU]
ON [dbo].[DWH_DOSSIER_SOUSCRIPTION] ([FLG_ENRG_COUR])
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_DOS_SOUS_DAT_OBS]
ON [dbo].[DWH_DOSSIER_SOUSCRIPTION] ([DAT_OBSR])
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date création de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag document precontractuel envoye', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'FLG_DOCM_PRCN_ENV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag contrat envoye', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'FLG_CONT_ENV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Message erreur core banking', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'MESS_ERRR_COR_BANK';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code erreur core banking', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'COD_ERRR_COR_BANK';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Score compteur appel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'SCR_COMP_APPL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date verification identite', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'DAT_VERF_IDNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date acceptation GC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'DAT_ACCP_GC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag dossier complet', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'FLG_DOSS_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag envoie email premier depot', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'FLG_ENV_EML_PRMR_DEPT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date fin', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'DAT_FIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Alias facture reseau distribution', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'ALS_FACT_RES_DIST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Alias', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'ALS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type utilisateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'TYP_UTLS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'IDNT_DERN_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'IDNT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'DAT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant relation personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'IDNT_RELT_PERS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant dossier justificatif ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'IDNT_DOSS_JUST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant prospect', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'IDNT_PRSP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant suspension controle statut', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'IDNT_SUSP_CONT_STTT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant sous statut', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'IDNT_SOUS_STTT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant offre commercial', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'IDNT_OFFR_COMM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant opportunite', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'IDNT_OPPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant dossier souscription', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'IDNT_DOSS_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro opportunite', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'NUMR_OPPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé offre commercial', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'LIBL_OFFR_COMM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code offre commercial', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'COD_OFFR_COMM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Courant Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag vérification identification vendeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'FLG_VERF_IDNT_VEND';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Premier dépôt par transfert IBAN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'PRMR_DEPT_TRNS_IBAN';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag contexte RIO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'FLG_CONT_RIO';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag consentement de vie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'FLG_CONS_VIE';

