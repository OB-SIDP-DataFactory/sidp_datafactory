﻿CREATE TABLE [dbo].[DWH_AGG_OPERATION_MOIS] (
    [DAT_OBSR]           DATE            NOT NULL,
    [MOIS_OBSR]          INT             NULL,
    [MOIS_OPE]           INT             NULL,
    [NUMR_COMP]          VARCHAR (20)    NOT NULL,
    [IDNT_TYP_OPE]       INT             NULL,
    [COD_PAYS_TRNS]      VARCHAR (2)     NULL,
    [NOMB_OPE]           INT             NULL,
    [MONT_OPE]           DECIMAL (18, 2) NULL,
    [DAT_CRTN_ENRG]      DATETIME        NULL,
    [DAT_DERN_MODF_ENRG] DATETIME        NULL
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_AGG_OPERATION_MOIS', @level2type = N'COLUMN', @level2name = N'NUMR_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre Opération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_AGG_OPERATION_MOIS', @level2type = N'COLUMN', @level2name = N'NOMB_OPE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant Opération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_AGG_OPERATION_MOIS', @level2type = N'COLUMN', @level2name = N'MONT_OPE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Type Opération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_AGG_OPERATION_MOIS', @level2type = N'COLUMN', @level2name = N'IDNT_TYP_OPE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_AGG_OPERATION_MOIS', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_AGG_OPERATION_MOIS', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_AGG_OPERATION_MOIS', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Pays de transaction', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_AGG_OPERATION_MOIS', @level2type = N'COLUMN', @level2name = N'COD_PAYS_TRNS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mois Opération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_AGG_OPERATION_MOIS', @level2type = N'COLUMN', @level2name = N'MOIS_OPE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mois Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_AGG_OPERATION_MOIS', @level2type = N'COLUMN', @level2name = N'MOIS_OBSR';

