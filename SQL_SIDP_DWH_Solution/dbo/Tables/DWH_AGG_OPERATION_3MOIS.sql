﻿CREATE TABLE [dbo].[DWH_AGG_OPERATION_3MOIS] (
    [DAT_OBSR]           DATE            NULL,
    [MOIS_TRTM]          INT             NULL,
    [MOIS_TRAITES]       VARCHAR (100)   NULL,
    [NUMR_COMP]          VARCHAR (20)    NULL,
    [NOMB_OPE_DEBIT]     INT             NULL,
    [NOMB_OPE_CRDT]      INT             NULL,
    [MONT_OPE_DEBIT]     DECIMAL (18, 2) NULL,
    [MONT_OPE_CRDT]      DECIMAL (18, 2) NULL,
    [DAT_CRTN_ENRG]      DATETIME        NULL,
    [DAT_DERN_MODF_ENRG] DATETIME        NULL
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_AGG_OPERATION_3MOIS', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_AGG_OPERATION_3MOIS', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_AGG_OPERATION_3MOIS', @level2type = N'COLUMN', @level2name = N'NUMR_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mois Traitement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_AGG_OPERATION_3MOIS', @level2type = N'COLUMN', @level2name = N'MOIS_TRTM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre Opération Débitrice', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_AGG_OPERATION_3MOIS', @level2type = N'COLUMN', @level2name = N'NOMB_OPE_DEBIT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre Opération Créditrice', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_AGG_OPERATION_3MOIS', @level2type = N'COLUMN', @level2name = N'NOMB_OPE_CRDT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant Opération Débitrice', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_AGG_OPERATION_3MOIS', @level2type = N'COLUMN', @level2name = N'MONT_OPE_DEBIT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant Opération Créditrice', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_AGG_OPERATION_3MOIS', @level2type = N'COLUMN', @level2name = N'MONT_OPE_CRDT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Les 3 mois traités', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_AGG_OPERATION_3MOIS', @level2type = N'COLUMN', @level2name = N'MOIS_TRAITES';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_AGG_OPERATION_3MOIS', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';

