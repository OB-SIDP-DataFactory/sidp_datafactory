﻿CREATE TABLE  [dbo].[DWH_PERSONNE_MORALE] (
    [DAT_OBSR]           DATE           NULL,
    [IDNT_PERS_SF]       NVARCHAR (18)  NULL,
    [FORM_JURD]          NVARCHAR (255) NULL,
    [RAIS_SOCL]          NVARCHAR (80)  NULL,
    [FLG_IDNT_CRTN_CLNT] BIT            NULL,
    [CLNT_VIP_FIRS_PARN] NVARCHAR (255) NULL,
    [DAT_CRTN_ENRG]      DATETIME       NULL,
    [DAT_DERN_MODF_ENRG] DATETIME       NULL,
    [FLG_ENRG_COUR]      BIT            NULL
)
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Raison sociale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_MORALE', @level2type = N'COLUMN', @level2name = N'RAIS_SOCL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Personne SF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_MORALE', @level2type = N'COLUMN', @level2name = N'IDNT_PERS_SF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Identifiant Création Client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_MORALE', @level2type = N'COLUMN', @level2name = N'FLG_IDNT_CRTN_CLNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Enregistrement Courant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_MORALE', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_MORALE', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_MORALE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_MORALE', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Client VIP / First / Parnasse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_MORALE', @level2type = N'COLUMN', @level2name = N'CLNT_VIP_FIRS_PARN';
GO
EXEC sp_addextendedproperty @name = N'MS_Description', @value = N'Forme juridique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PERSONNE_MORALE', @level2type = N'COLUMN', @level2name = N'FORM_JURD'
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_PERS_MOR_FLG_ENR_COU]
ON [dbo].[DWH_PERSONNE_MORALE] ([FLG_ENRG_COUR])
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_PERS_MOR_DAT_OBS]
ON [dbo].[DWH_PERSONNE_MORALE] ([DAT_OBSR])
GO