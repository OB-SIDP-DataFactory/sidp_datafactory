﻿CREATE TABLE [dbo].[DWH_EQPM_CREDIT_CONSO_SL] (
    [DAT_OBSR]                      DATE            NULL,
    [IDNT_EQPM]                     DECIMAL (38)    NOT NULL,
    [FRS_INSC]                      DECIMAL (19, 2) NOT NULL,
    [DAT_FIN_CONT]                  DATETIME        NULL,
    [IDNT_TYP_FINN]                 DECIMAL (38)    NOT NULL,
    [DAT_PRMR_ECHN]                 DATE            NULL,
    [DAT_FIN_PRV_PRT]               DATE            NULL,
    [NUMR_APPL_FI]                  NVARCHAR (7)    NOT NULL,
    [NUMR_CONT_FI]                  NVARCHAR (11)   NOT NULL,
    [MONT_CRDT_SOUS]                DECIMAL (19, 2) NOT NULL,
    [PRM_ASSR]                      DECIMAL (19, 2) NOT NULL,
    [COUT_TOTL_ASSR]                DECIMAL (19, 2) NOT NULL,
    [MONT_DERN_ACMP_PAYE_AVC_ASSR]  DECIMAL (19, 2) NULL,
    [MONT_DERN_ACMP_PAYE_SANS_ASSR] DECIMAL (19, 2) NULL,
    [DERN_DAT_ECHN_PAYE]            DATE            NULL,
    [DAT_EXPR_ACTV_PRT]             DATE            NULL,
    [DAT_DEBT_ACTV_PRT]             DATE            NOT NULL,
    [MONT_DECS_PRT]                 DECIMAL (19, 2) NULL,
    [DAT_DECS_PRT]                  DATE            NULL,
    [TAUX_NOMN_PRT]                 DECIMAL (19, 7) NOT NULL,
    [NOMB_ECHN_PRT]                 DECIMAL (38)    NULL,
    [PRT_EXCP]                      DECIMAL (19, 2) NULL,
    [IDNT_SITT_PRT_CONS]            DECIMAL (38)    NULL,
    [IDNT_STTT_PRT_CONS]            DECIMAL (38)    NOT NULL,
    [TAUX_ANNL_EFFC_ASSR]           DECIMAL (19, 7) NOT NULL,
    [TAUX_ANNL_EFFC_GLBL]           DECIMAL (19, 7) NOT NULL,
    [VERS_MENS_AVC_ASSR]            DECIMAL (19, 2) NOT NULL,
    [VERS_MENS_SANS_ASSR]           DECIMAL (19, 2) NOT NULL,
    [NOMB_VERS_ECHS]                DECIMAL (38)    NULL,
    [NOMB_VERS_REPR]                DECIMAL (38)    NULL,
    [DAT_FIN_PERD_RETR]             DATE            NOT NULL,
    [PERD_TOTL_MOIS_DIFF]           DECIMAL (38)    NULL,
    [CAPT_REST_DU]                  DECIMAL (19, 2) NULL,
    [MONT_TOTL_INTR]                DECIMAL (19, 2) NOT NULL,
    [NOMB_TOTL_VERS]                DECIMAL (38)    NOT NULL,
    [VERS_VENR_AVC_ASSR]            DECIMAL (19, 2) NULL,
    [VERS_VENR_SANS_ASSR]           DECIMAL (19, 2) NULL,
    [DAT_VERS_VENR]                 DATE            NULL,
    [IDNT_RAIS_PRT_TERM]            DECIMAL (38)    NULL,
    [DAT_APPR]                      DATE            NULL,
    [JOUR_RETR]                     DECIMAL (38)    NULL,
    [FLG_MAIL_DISP_DECS_ENV]        DECIMAL (1)     NULL,
    [IDNT_PRLV_AUTM_PAYS]           DECIMAL (38)    NULL,
    [PRLV_AUTM_CL_CTRL]             NVARCHAR (2)    NULL,
    [PRLV_AUTM_BBN]                 NVARCHAR (30)   NULL,
    [TAUX_DEDC_PRT]                 DECIMAL (19, 7) NULL,
    [COD_TAUX_DEDC_PRT]             NVARCHAR (32)   NULL,
    [TAUX_ANNL_EFFC_GLBL_PRV]       DECIMAL (19, 7) NULL,
    [TAUX_NOMN_PRT_PRV]             DECIMAL (19, 7) NULL,
    [TAUX_NOMN_PRT_TOTL]            DECIMAL (19, 7) NULL,
    [DEPT_PRT_CART]                 DECIMAL (19, 7) NULL,
    [DEPT_PRT_AUTR]                 DECIMAL (19, 7) NULL,
    [DAT_DECS_AUTM]                 DATE            NULL,
    [TAUX_NOMN_PRT_CLNT]            DECIMAL (19, 7) NULL,
    [DAT_CRTN_ENRG]                 DATETIME        NULL,
    [DAT_DERN_MODF_ENRG]            DATETIME        NULL,
    [FLG_ENRG_COUR]                 BIT             NULL,
    [DAT_SIGN]                      DATE            NULL
)
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere modification enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date creation enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Versements a venir sans assurance ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'VERS_VENR_SANS_ASSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Versements a venir avec assurance ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'VERS_VENR_AVC_ASSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Versement mensuel sans assur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'VERS_MENS_SANS_ASSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Versement mensuel avec assur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'VERS_MENS_AVC_ASSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux annuel effectif global', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'TAUX_ANNL_EFFC_GLBL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux annuel effectif d assurance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'TAUX_ANNL_EFFC_ASSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Pret exceptionnel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'PRT_EXCP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prime d assurance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'PRM_ASSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prelevement automatique BBAN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'PRLV_AUTM_BBN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Periode totale en mois differee', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'PERD_TOTL_MOIS_DIFF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre des versements reportes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'NOMB_VERS_REPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre de versements echus', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'NOMB_VERS_ECHS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre total des versements', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'NOMB_TOTL_VERS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre d echeances du pret', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'NOMB_ECHN_PRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant total des interets', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'MONT_TOTL_INTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant du dernier acompte paye sans assurance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'MONT_DERN_ACMP_PAYE_SANS_ASSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant du dernier acompte paye avec assurance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'MONT_DERN_ACMP_PAYE_AVC_ASSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant du decaissement du pret', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'MONT_DECS_PRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant du credit souscrit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'MONT_CRDT_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant statut prêt à la consommation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'IDNT_STTT_PRT_CONS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant situation pret a la consommation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'IDNT_SITT_PRT_CONS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant raison du pret a terme', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'IDNT_RAIS_PRT_TERM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Frais d inscription', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'FRS_INSC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Derniere date d echeance payee', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'DERN_DAT_ECHN_PAYE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date des versements a venir', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'DAT_VERS_VENR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de fin contrat ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'DAT_FIN_CONT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date d expiration de l activation du pret', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'DAT_EXPR_ACTV_PRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de decaissement du pret', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'DAT_DECS_PRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de début de l activation du pret', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'DAT_DEBT_ACTV_PRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date d approbation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'DAT_APPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cout total de l assurance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'COUT_TOTL_ASSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Capital restant du ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'CAPT_REST_DU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numero de contrat FI', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'NUMR_CONT_FI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numero application FI', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'NUMR_APPL_FI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Jour de retrait', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'JOUR_RETR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant type de financement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'IDNT_TYP_FINN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Première date d échéance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'DAT_PRMR_ECHN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de fin prevue du pret ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'DAT_FIN_PRV_PRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de fin période rétraction', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'DAT_FIN_PERD_RETR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Courant Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant prelevement automatique pays', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'IDNT_PRLV_AUTM_PAYS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux nominal du prêt prev', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'TAUX_NOMN_PRT_PRV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux nominal du prêt client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'TAUX_NOMN_PRT_CLNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux déduction du prêt', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'TAUX_DEDC_PRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux annuel effectif global prev', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'TAUX_ANNL_EFFC_GLBL_PRV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dépôt du prêt par carte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'DEPT_PRT_CART';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dépôt du prêt autre', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'DEPT_PRT_AUTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date décaissement automatique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'DAT_DECS_AUTM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code taux déduction du prêt', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'COD_TAUX_DEDC_PRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux nominal du pret', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'TAUX_NOMN_PRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux nominal du prêt total', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'TAUX_NOMN_PRT_TOTL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de signature', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'DAT_SIGN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prelevement automatique cle de controle', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'PRLV_AUTM_CL_CTRL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant equipement du produit credit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'IDNT_EQPM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag email de disponibilité du decaissement envoyé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'FLG_MAIL_DISP_DECS_ENV';
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_EQU_CRE_CONS_SL_DAT_OBS]
ON [dbo].[DWH_EQPM_CREDIT_CONSO_SL] ([DAT_OBSR])
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_EQU_CRE_CONS_SL_FLG_ENR_COU]
ON [dbo].[DWH_EQPM_CREDIT_CONSO_SL] ([FLG_ENRG_COUR])
GO