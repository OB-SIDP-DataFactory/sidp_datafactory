﻿CREATE TABLE [dbo].[TRVL_OPERATION_DAT_PRMR] (
    [NUMR_COMP]               VARCHAR (20)    NOT NULL,
    [DAT_VERS_PRM_BIEN]       DATE            NULL,
    [DAT_PRMR_UTLS_PAIE_MOBL] DATE            NULL,
    [DAT_PRMR_UTLS_PAIE_CB]   DATE            NULL,
    [DAT_PRMR_OPRT_DEBT]      DATE            NULL,
    [DAT_PRMR_OPRT_CRDT]      DATE            NULL,
    [DAT_PREM_VERS]           DATE            NULL,
    [MONT_PRMR_VERS]          DECIMAL (19, 2) NULL,
    [DAT_CRTN_ENRG]           DATETIME        NULL,
    [DAT_DERN_MODF_ENRG]      DATETIME        NULL
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Dernière Modification de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRVL_OPERATION_DAT_PRMR', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Création de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRVL_OPERATION_DAT_PRMR', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant Premier Versement CAV ou CSL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRVL_OPERATION_DAT_PRMR', @level2type = N'COLUMN', @level2name = N'MONT_PRMR_VERS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Premier Versement CAV ou CSL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRVL_OPERATION_DAT_PRMR', @level2type = N'COLUMN', @level2name = N'DAT_PREM_VERS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Première Opération Créditrice', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRVL_OPERATION_DAT_PRMR', @level2type = N'COLUMN', @level2name = N'DAT_PRMR_OPRT_CRDT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Première Opération Débitrice', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRVL_OPERATION_DAT_PRMR', @level2type = N'COLUMN', @level2name = N'DAT_PRMR_OPRT_DEBT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Première Utilisation Paiement CB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRVL_OPERATION_DAT_PRMR', @level2type = N'COLUMN', @level2name = N'DAT_PRMR_UTLS_PAIE_CB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Première Utilisation Paiement Mobile', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRVL_OPERATION_DAT_PRMR', @level2type = N'COLUMN', @level2name = N'DAT_PRMR_UTLS_PAIE_MOBL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Versement Prime Bienvenue', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRVL_OPERATION_DAT_PRMR', @level2type = N'COLUMN', @level2name = N'DAT_VERS_PRM_BIEN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro de compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TRVL_OPERATION_DAT_PRMR', @level2type = N'COLUMN', @level2name = N'NUMR_COMP';
GO