﻿CREATE TABLE [dbo].[DWH_LIEN_EQUIPEMENT] (
    [DAT_OBSR]           DATE          NULL,
    [NUMR_COMP]          VARCHAR (20)  NULL,
    [NUMR_CLNT_SAB]      VARCHAR (7)   NULL,
    [NUMR_CLNT_COLL]     VARCHAR (7)   NULL,
    [NATR_RELT]          VARCHAR (3)   NULL,
    [NUMR_OPPR]          NVARCHAR (30) NULL,
    [FLG_TITL_PRCP]      BIT           NULL,
    [FLG_TIER_COLL]      BIT           NULL,
    [DAT_CRTN_ENRG]      DATETIME      NULL,
    [DAT_DERN_MODF_ENRG] DATETIME      NULL,
    [FLG_ENRG_COUR]      BIT           NULL
)
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Dernière Modification de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_EQUIPEMENT', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Création de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_EQUIPEMENT', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Titulaire Pricipal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_EQUIPEMENT', @level2type = N'COLUMN', @level2name = N'FLG_TITL_PRCP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Client SAB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_EQUIPEMENT', @level2type = N'COLUMN', @level2name = N'NUMR_CLNT_SAB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_EQUIPEMENT', @level2type = N'COLUMN', @level2name = N'NUMR_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Opportunité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_EQUIPEMENT', @level2type = N'COLUMN', @level2name = N'NUMR_OPPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Client Collectif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_EQUIPEMENT', @level2type = N'COLUMN', @level2name = N'NUMR_CLNT_COLL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Tiers Collectif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_EQUIPEMENT', @level2type = N'COLUMN', @level2name = N'FLG_TIER_COLL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_EQUIPEMENT', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Courant Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_EQUIPEMENT', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nature Relation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_EQUIPEMENT', @level2type = N'COLUMN', @level2name = N'NATR_RELT';
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_LIE_EQU_DAT_OBS]
ON [dbo].[DWH_LIEN_EQUIPEMENT] ([DAT_OBSR])
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_LIE_EQU_FLG_ENR_COU]
ON [dbo].[DWH_LIEN_EQUIPEMENT] ([FLG_ENRG_COUR])
GO