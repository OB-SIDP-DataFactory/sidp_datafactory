﻿CREATE TABLE [dbo].[DWH_REQUETE] (
    [DAT_OBSR]                    DATE            NOT NULL,
    [IDNT_REQT]                   NVARCHAR (50)   NULL,
    [NUMR_REQT]                   NVARCHAR (30)   NULL,
    [IDNT_TYP_ENRG]               NVARCHAR (18)   NULL,
    [IDNT_SF_PERS]                NVARCHAR (18)   NULL,
    [NUMR_PERS_SF]                NVARCHAR (11)   NULL,
    [NUMR_CLNT_SAB]               NVARCHAR (15)   NULL,
    [IDNT_OPPR]                   NVARCHAR (18)   NULL,
    [NUMR_OPPR]                   NVARCHAR (30)   NULL,
    [IDNT_REQT_PRNC]              NVARCHAR (18)   NULL,
    [IDNT_REQT_TCH]               NVARCHAR (20)   NULL,
    [IDNT_ENRG_PRNC]              NVARCHAR (18)   NULL,
    [REQT_ORGN]                   NVARCHAR (18)   NULL,
    [CANL_ORGN]                   NVARCHAR (40)   NULL,
    [STTT]                        NVARCHAR (40)   NULL,
    [FLG_REQT_TRT]                BIT             NULL,
    [FLG_REQT_NON_TRT]            BIT             NULL,
    [PRMR_RECL]                   NVARCHAR (255)  NULL,
    [RECL_NIV]                    NVARCHAR (255)  NULL,
    [OBJT_PRNC]                   NVARCHAR (255)  NULL,
    [OBJT_SECN]                   NVARCHAR (255)  NULL,
    [TYP_REQT]                    NVARCHAR (40)   NULL,
    [SOUS_TYP]                    NVARCHAR (255)  NULL,
    [ACT_GEST]                    NVARCHAR (255)  NULL,
    [UNVR]                        NVARCHAR (255)  NULL,
    [SOUS_UNVR]                   NVARCHAR (255)  NULL,
    [PRRT]                        NVARCHAR (40)   NULL,
    [PRCS]                        NVARCHAR (255)  NULL,
    [DAT_CRTN]                    DATETIME        NULL,
    [IDNT_CRTN]                   NVARCHAR (18)   NULL,
    [IDNT_PRPR]                   NVARCHAR (18)   NULL,
    [DAT_FERM]                    DATETIME        NULL,
    [FLG_DAT_FERM]                BIT             NULL,
    [DAT_ACCS_RECP]               DATETIME        NULL,
    [CANL_REPN]                   NVARCHAR (255)  NULL,
    [DAT_PRCH_ACTN]               DATETIME2 (7)   NULL,
    [DAT_REPN]                    DATETIME        NULL,
    [JALN_DEPS]                   NVARCHAR (10)   NULL,
    [IDNT_DERN_MODF]              NVARCHAR (18)   NULL,
    [DAT_DERN_MODF]               DATETIME        NULL,
    [CONS_TECH]                   NVARCHAR (18)   NULL,
    [REAL_PAR]                    NVARCHAR (18)   NULL,
    [RESP]                        NVARCHAR (1300) NULL,
    [METR]                        NVARCHAR (255)  NULL,
    [PRPR_PRCD]                   NVARCHAR (255)  NULL,
    [FIL_ATTN]                    NVARCHAR (255)  NULL,
    [DAT_TRTM]                    DATETIME        NULL,
    [DEL_TRTM]                    DECIMAL (18)    NULL,
    [DUR]                         NVARCHAR (80)   NULL,
    [DUR_TRTM]                    DECIMAL (18)    NULL,
    [DUR_TRTM_HEUR]               DECIMAL (18)    NULL,
    [MOTF_TRTM]                   NVARCHAR (255)  NULL,
    [DUT]                         DECIMAL (18)    NULL,
    [DAT_PLNF]                    DATETIME        NULL,
    [DAT_ATTR_REQT]               DATETIME2 (7)   NULL,
    [STTT_JALN]                   NVARCHAR (30)   NULL,
    [DECS]                        NVARCHAR (255)  NULL,
    [INDM]                        NVARCHAR (255)  NULL,
    [MONT_GEST_COMM]              MONEY           NULL,
    [MONT_PERT]                   MONEY           NULL,
    [MONT_RETR]                   MONEY           NULL,
    [MONT]                        MONEY           NULL,
    [STTT_APPL]                   NVARCHAR (255)  NULL,
    [DETL_STTT_APPL]              NVARCHAR (255)  NULL,
    [IDNT_CAMP]                   NVARCHAR (100)  NULL,
    [PLN_ACTN]                    NVARCHAR (255)  NULL,
    [TYPL]                        NVARCHAR (255)  NULL,
    [TYP_CLNT]                    NVARCHAR (255)  NULL,
    [NOM_VEND]                    NVARCHAR (18)   NULL,
    [NOM_BOUT]                    NVARCHAR (18)   NULL,
    [HABL]                        NVARCHAR (10)   NULL,
    [DESC]                        NVARCHAR (MAX)  NULL,
    [DETL]                        NVARCHAR (MAX)  NULL,
    [PRCS_DETL]                   NVARCHAR (MAX)  NULL,
    [REPN]                        NVARCHAR (255)  NULL,
    [QUAL_REPN_OBJT_PRNC]         NVARCHAR (255)  NULL,
    [QUAL_REPN_OBJT_SECN]         NVARCHAR (255)  NULL,
    [COMM]                        NVARCHAR (MAX)  NULL,
    [COMM_INTR]                   VARCHAR (MAX)   NULL,
    [FLG_LIBR_SERV_COMM]          BIT             NULL,
    [FLG_NOUV_COMM_LIBR_SERV]     BIT             NULL,
    [MOTF_REVR]                   NVARCHAR (255)  NULL,
    [OBJT]                        NVARCHAR (255)  NULL,
    [NOM]                         NVARCHAR (80)   NULL,
    [ADRS_EML]                    NVARCHAR (255)  NULL,
    [TELP]                        NVARCHAR (40)   NULL,
    [SOCT]                        NVARCHAR (80)   NULL,
    [IDNT_CNTC]                   NVARCHAR (18)   NULL,
    [NUMR_TELP_CNTC]              NVARCHAR (40)   NULL,
    [TELP_MOBL_CNTC]              NVARCHAR (40)   NULL,
    [ADRS_EML_CNTC]               NVARCHAR (255)  NULL,
    [CNTC_TELC]                   NVARCHAR (40)   NULL,
    [TYP_DOCM]                    NVARCHAR (MAX)  NULL,
    [PRSN_PJ]                     NVARCHAR (80)   NULL,
    [VALD_PJ]                     NVARCHAR (255)  NULL,
    [DAT_EXMN_EXPR]               NVARCHAR (10)   NULL,
    [DAT_DEBT_PRCS_AUTR]          DATETIME2 (7)   NULL,
    [DAT_FIN_PRCS_AUTR]           DATETIME2 (7)   NULL,
    [DAT_DEBT_ARRT]               DATETIME2 (7)   NULL,
    [DAT_DERN_AFFC]               DATETIME2 (7)   NULL,
    [DAT_DERN_REFR]               DATETIME2 (7)   NULL,
    [DAT_REVR]                    DATETIME2 (7)   NULL,
    [DAT_LETT_ATTN]               DATETIME        NULL,
    [FLG_SUPP]                    BIT             NULL,
    [FLG_FERM]                    BIT             NULL,
    [FLG_ESCL]                    BIT             NULL,
    [FLG_VISB_PORT_LIBR_SERV]     BIT             NULL,
    [FLG_FERM_UTLS_LIBR_SERV]     BIT             NULL,
    [FLG_FERM_LORS_CRTN]          BIT             NULL,
    [FLG_ARRT]                    BIT             NULL,
    [FLG_EMPC_ASSG]               BIT             NULL,
    [FLG_FIL_ATTN]                BIT             NULL,
    [FLG_CONF_ABND]               BIT             NULL,
    [FLG_REQT_TRT_AVC_JALN_DEPS]  BIT             NULL,
    [FLG_REQT_TRTS_AVC_JALN_RESP] BIT             NULL,
    [FLG_NON_TRT_AVC_JALN_DEPS]   BIT             NULL,
    [FLG_NON_TRT_AVC_JALN_RESP]   BIT             NULL,
    [IDNT_ACTF]                   NVARCHAR (18)   NULL,
    [IDNT_PRDT]                   NVARCHAR (18)   NULL,
    [IDNT_AUTR]                   NVARCHAR (18)   NULL,
    [IDNT_QUES]                   NVARCHAR (18)   NULL,
    [IDNT_SOUR]                   NVARCHAR (18)   NULL,
    [IDNT_ZON]                    NVARCHAR (18)   NULL,
    [IDNT_HEUR_OUVR]              NVARCHAR (18)   NULL,
    [EQPM]                        NVARCHAR (200)  NULL,
    [PIST_AMLR]                   NVARCHAR (255)  NULL,
    [RES]                         NVARCHAR (255)  NULL,
    [ORGN]                        NVARCHAR (255)  NULL,
    [MOTF_REQT]                   NVARCHAR (40)   NULL,
    [EN_ATTN_DE]                  NVARCHAR (255)  NULL,
    [HORD_MODF_SYST]              DATETIME2 (7)   NULL,
    [URL_PHOTO_PRFL_CRTR]         NVARCHAR (255)  NULL,
    [URL_PHOTO_MINT_CRTR]         NVARCHAR (255)  NULL,
    [NOM_CRTR]                    NVARCHAR (121)  NULL,
    [AUTH_MANL]                   NVARCHAR (10)   NULL,
    [IDNT_DIX_HUIT_CHRS]          NVARCHAR (1300) NULL,
    [NOMB_JALN_FERM]              DECIMAL (18)    NULL,
    [RESL_AUTH]                   NVARCHAR (255)  NULL,
    [DYSF_BANQ]                   NVARCHAR (255)  NULL,
    [TAUX_CONF]                   NVARCHAR (10)   NULL,
    [PLFN_PAIE]                   MONEY           NULL,
    [PLFN_RETR]                   MONEY           NULL,
    [MOTS_INTR]                   NVARCHAR (255)  NULL,
    [VISB_CLNT]                   NVARCHAR (255)  NULL,
    [FLG_MOTS_INTR]               BIT             NULL,
    [AUTH]                        NVARCHAR (255)  NULL,
    [COMP_MESS]                   DECIMAL (18)    NULL,
    [NOUV_MESS]                   NVARCHAR (1300) NULL,
    [PURG]                        DECIMAL (18)    NULL,
    [TRGG_PASS]                   NVARCHAR (10)   NULL,
    [REFR_TICK_EXTR]              NVARCHAR (255)  NULL,
    [SPCF]                        NVARCHAR (255)  NULL,
    [GRVT_INCD]                   NVARCHAR (255)  NULL,
    [EML_SOUR]                    NVARCHAR (255)  NULL,
    [ECHN_CORR_PRV]               DATETIME        NULL,
    [MOTF_ANML_TA3C]              NVARCHAR (MAX)  NULL,
    [SUIV_EVLT_STTT]              NVARCHAR (10)   NULL,
    [RELN_CLNT]                   NVARCHAR (255)  NULL,
    [PERM]                        NVARCHAR (255)  NULL,
    [IDNT_OPPR_DIST_ASSC_TCH]     NVARCHAR (1300) NULL,
    [RES_DIST_OPPR_ASSC_TCH]      NVARCHAR (1300) NULL,
    [DAT_CRTN_ENRG]               DATETIME        NULL,
    [DAT_DERN_MODF_ENRG]          DATETIME        NULL,
    [FLG_ENRG_COUR]               BIT             NULL,
    [INCD_COUR]                   NVARCHAR (1300) NULL,
    [FLG_RECL_COUR_APPR]          BIT             NULL,
    [SCR_EXPR]                    DECIMAL (18)    NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Enregistrement Courant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere modification enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date creation enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Périmètre', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'PERM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Relance client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'RELN_CLNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Suivi Evolution du Statut', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'SUIV_EVLT_STTT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motifs Anomalie TA3C', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'MOTF_ANML_TA3C';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Echéance de correction prévue', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'ECHN_CORR_PRV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Email source', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'EML_SOUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gravité de l''incident', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'GRVT_INCD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Spécification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'SPCF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Référence Ticket Externe', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'REFR_TICK_EXTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Trigger Pass', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'TRGG_PASS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Purge', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'PURG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nouveau Message', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'NOUV_MESS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Compteur de message', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'COMP_MESS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Authentification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'AUTH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Mots Interdits', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_MOTS_INTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Visibilité Client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'VISB_CLNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mots interdits', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'MOTS_INTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Plafond Retrait', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'PLFN_RETR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Plafond Paiement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'PLFN_PAIE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux de confiance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'TAUX_CONF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dysfonctionnement Banque', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'DYSF_BANQ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Résultat de l''authentification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'RESL_AUTH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre jalons fermés', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'NOMB_JALN_FERM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Dix huit Chars ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_DIX_HUIT_CHRS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Authentification Manuelle', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'AUTH_MANL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom du créateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'NOM_CRTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'URL de la photo miniature du créateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'URL_PHOTO_MINT_CRTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'URL de la photo de profil du créateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'URL_PHOTO_PRFL_CRTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Horodateur modification systeme', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'HORD_MODF_SYST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'En Attente de', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'EN_ATTN_DE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motif de la requête', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'MOTF_REQT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Origine', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'ORGN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Réseau', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'RES';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Piste d''amélioration', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'PIST_AMLR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Equipement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'EQPM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant d''heures d''ouverture', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_HEUR_OUVR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de zone', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_ZON';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant source', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_SOUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de question', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_QUES';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant d''autorisation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_AUTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant produit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_PRDT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de l''actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_ACTF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Non traité avec jalon respecté', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_NON_TRT_AVC_JALN_RESP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Non traité avec jalon dépassé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_NON_TRT_AVC_JALN_DEPS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Requetes traitees avec jalon respecté', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_REQT_TRTS_AVC_JALN_RESP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Requête traitée avec jalon dépassé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_REQT_TRT_AVC_JALN_DEPS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Confirmer l''abandon', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_CONF_ABND';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag File d''attente', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_FIL_ATTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Empêche Assignation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_EMPC_ASSG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag arrêt', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_ARRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag fermeture lors de la création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_FERM_LORS_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag fermeture par l''utilisateur du libre service', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_FERM_UTLS_LIBR_SERV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Visible dans le portail libre service', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_VISB_PORT_LIBR_SERV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Escaladée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_ESCL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag fermée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_FERM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag supprimé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_SUPP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date lettre d''attente', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_LETT_ATTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date à revoir', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_REVR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere reference', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_REFR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernier affichage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_AFFC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date debut arrêt', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_DEBT_ARRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de fin du processus d''autorisation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_FIN_PRCS_AUTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de début du processus d''autorisation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_DEBT_PRCS_AUTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date examen expiré', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_EXMN_EXPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Validation PJ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'VALD_PJ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Présence PJ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'PRSN_PJ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type Document', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'TYP_DOCM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Société', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'SOCT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Téléphone', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'TELP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Adresse email', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'ADRS_EML';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'NOM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Objet', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'OBJT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motif à revoir', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'MOTF_REVR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Nouveau commentaire Libre service', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_NOUV_COMM_LIBR_SERV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Libre service commenté', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_LIBR_SERV_COMM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Commentaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'COMM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Qualification réponse objet secondaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'QUAL_REPN_OBJT_SECN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Qualification réponse objet principal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'QUAL_REPN_OBJT_PRNC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Reponse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'REPN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Détails', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'DETL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Description', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'DESC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Habilitation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'HABL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom de la boutique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'NOM_BOUT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom vendeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'NOM_VEND';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type de client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'TYP_CLNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Typologie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'TYPL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Plan d''action', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'PLN_ACTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de la Campagne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_CAMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Détail Statut Appel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'DETL_STTT_APPL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Statut de l''appel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'STTT_APPL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'MONT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant rétrocession', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'MONT_RETR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant perte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'MONT_PERT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant geste commercial', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'MONT_GEST_COMM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Indemnisation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'INDM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Décision', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'DECS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Statut du jalon', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'STTT_JALN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date d''attribution de la requête', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_ATTR_REQT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de planification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_PLNF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DUT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'DUT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motif de traitement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'MOTF_TRTM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Durée traitement Heures', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'DUR_TRTM_HEUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Durée de traitement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'DUR_TRTM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Durée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'DUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Délai de traitement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'DEL_TRTM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de traitement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_TRTM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'File d''attente', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'FIL_ATTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Propriétaire précédent', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'PRPR_PRCD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Métier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'METR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Responsable', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'RESP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Réalisé par', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'REAL_PAR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Conseiller technique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'CONS_TECH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_DERN_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Jalon dépassé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'JALN_DEPS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de la réponse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_REPN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de prochaine action', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_PRCH_ACTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Canal réponse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'CANL_REPN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Accusé Réception', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_ACCS_RECP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date fermeture', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_FERM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant du propriétaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_PRPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Processus', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'PRCS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Priorité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'PRRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Sous Univers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'SOUS_UNVR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Univers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'UNVR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Acte de Gestion', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'ACT_GEST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Sous Type', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'SOUS_TYP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type de requête', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'TYP_REQT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Objet secondaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'OBJT_SECN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Objet principal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'OBJT_PRNC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Réclamation niveau', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'RECL_NIV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Première réclamation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'PRMR_RECL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Requete Non Traitee', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_REQT_NON_TRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Requete Traitee', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_REQT_TRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Statut', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'STTT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Canal d''origine', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'CANL_ORGN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Requête d''origine', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'REQT_ORGN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Requête TCH', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_REQT_TCH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de la requête principale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_REQT_PRNC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro de l''opportunité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'NUMR_OPPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de l''opportunité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_OPPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Client SAB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'NUMR_CLNT_SAB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'NUMR_PERS_SF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant SF personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_SF_PERS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant du type d''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_TYP_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro de la requête', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'NUMR_REQT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de la requête', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_REQT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Précision détails', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'PRCS_DETL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag date fermeture', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_DAT_FERM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TCHOptDistributorNetwork', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'RES_DIST_OPPR_ASSC_TCH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TCHOppIdProcessSous', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_OPPR_DIST_ASSC_TCH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant d''enregistrement principal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_ENRG_PRNC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Commentaires internes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'COMM_INTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Incident en Cours', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'INCD_COUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag réclamation en cours d''approbation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_RECL_COUR_APPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Téléphone mobile du contact', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'TELP_MOBL_CNTC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro de téléphone du contact', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'NUMR_TELP_CNTC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant du contact', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_CNTC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Contact Télécopie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'CNTC_TELC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Adresse email du contact', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'ADRS_EML_CNTC';
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_REQ_DAT_OBS]
ON [dbo].[DWH_REQUETE] ([DAT_OBSR])
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_REQ_FLG_ENR_COU]
ON [dbo].[DWH_REQUETE] ([FLG_ENRG_COUR])
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Score Expérian', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REQUETE', @level2type = N'COLUMN', @level2name = N'SCR_EXPR';

