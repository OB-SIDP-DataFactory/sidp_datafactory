﻿CREATE TABLE [dbo].[DWH_EQUIPEMENT_CREDIT_CONSO] (
    [DAT_OBSR]                DATE            NOT NULL,
    [REFR_CONT]               NVARCHAR (11)   NOT NULL,
    [NUMR_COMP_SAB]           VARCHAR (20)    NULL,
    [DAT_ACCR_TOKS]           DATE            NULL,
    [DAT_ACCP_CLNT]           DATE            NULL,
    [DAT_DECS]                DATE            NULL,
    [DAT_DERN_DEBL]           DATE            NULL,
    [DEL_DECS]                INT             NULL,
    [DAT_FIN_PRVS_DOSS]       DATE            NULL,
    [RES_DIST]                NVARCHAR (3)    NULL,
    [NUMR_COMP_PRLV]          NVARCHAR (11)   NULL,
    [COD_BANQ_PRLV]           NVARCHAR (5)    NULL,
    [COD_GUIC_PRLV]           NVARCHAR (5)    NULL,
    [LIBL_BANQ_GUIC_PRLV]     NVARCHAR (24)   NULL,
    [LIBL_TITL_COMP_PRLV]     NVARCHAR (26)   NULL,
    [MONT_NOMN]               DECIMAL (11, 2) NULL,
    [MONT_TOTL_DEBL]          DECIMAL (11, 2) NULL,
    [MONT_TOTL_DU]            DECIMAL (11, 2) NULL,
    [CAPT_REST_DU]            DECIMAL (11, 2) NULL,
    [CATG_PRT]                NVARCHAR (9)    NULL,
    [COD_OBJT_FINN]           NVARCHAR (5)    NULL,
    [COD_DEVS]                NVARCHAR (3)    NULL,
    [DUR_LEGL]                DECIMAL (22)    NULL,
    [TAUX_VEND]               DECIMAL (11, 2) NULL,
    [TAUX_INTR_PALR]          DECIMAL (11, 2) NULL,
    [NATR_ASSR_EMPR]          NVARCHAR (2)    NULL,
    [NATR_ASSR_COEM]          NVARCHAR (2)    NULL,
    [NATR_ASSR_CAUT_1]        NVARCHAR (2)    NULL,
    [NATR_ASSR_CAUT_2]        NVARCHAR (2)    NULL,
    [FLG_ASSR_SOUS]           NVARCHAR (1)    NULL,
    [COD_SITT_TOKS]           NVARCHAR (1)    NULL,
    [FRS_DOSS]                DECIMAL (11, 2) NULL,
    [MONT_AUTR_FRS]           DECIMAL (11, 2) NULL,
    [DAT_DERN_MODF]           DATE            NULL,
    [SITT_CONT]               NVARCHAR (1)    NULL,
    [COD_FIN_DOSS]            NVARCHAR (1)    NULL,
    [DAT_MODF_RIB]            DATE            NULL,
    [DAT_MODF_REGL]           DATE            NULL,
    [COD_MOD_REGL]            NVARCHAR (1)    NULL,
    [PRT_MUTL]                NVARCHAR (1)    NULL,
    [COD_MOD_DEBL_FOND]       NVARCHAR (1)    NULL,
    [DAT_PRMR_ECHN]           DATE            NULL,
    [DAT_DERN_ECHN_TRT]       DATE            NULL,
    [NUMR_PALR]               DECIMAL (22)    NULL,
    [NOMB_ECHN_TOTL]          DECIMAL (22)    NULL,
    [NOMB_ECHN_ECHS]          DECIMAL (22)    NULL,
    [CAPT_REST_DERN_ECHN]     DECIMAL (11, 2) NULL,
    [INTR_DERN_ECHN]          DECIMAL (11, 2) NULL,
    [ACCS_DERN_ECHN]          DECIMAL (11, 2) NULL,
    [ASSR_DERN_ECHN]          DECIMAL (11, 2) NULL,
    [DAT_PRCH_ECHN]           DATE            NULL,
    [CAPT_PRCH_ECHN]          DECIMAL (11, 2) NULL,
    [INTR_PRCH_ECHN]          DECIMAL (11, 2) NULL,
    [ACCS_PRCH_ECHN]          DECIMAL (11, 2) NULL,
    [ASSR_PRCH_ECHN]          DECIMAL (11, 2) NULL,
    [DAT_PRCH_PERC_ECHN]      DATE            NULL,
    [DAT_PRCH_ANTC]           DATE            NULL,
    [FLG_EXST_PLN_REAM]       NVARCHAR (1)    NULL,
    [MONT_GLBL_IMP]           DECIMAL (11, 2) NULL,
    [MONT_GLBL_CAPT_IMP]      DECIMAL (11, 2) NULL,
    [MONT_GLBL_INTR_IMP]      DECIMAL (11, 2) NULL,
    [MONT_GLBL_ASSR_IMP]      DECIMAL (11, 2) NULL,
    [MONT_GLBL_ACCS_IMP]      DECIMAL (11, 2) NULL,
    [MONT_GLBL_IR_IMP]        DECIMAL (11, 2) NULL,
    [MONT_GLBL_IL_IMP]        DECIMAL (11, 2) NULL,
    [TAUX_INTR_RETR]          DECIMAL (11, 2)  NULL,
    [DAT_PRMR_IMP]            DATE            NULL,
    [DAT_DERN_IMP]            DATE            NULL,
    [DAT_PRCH_ECHN_RATT]      DATE            NULL,
    [MONT_IMP_DECL_FICP]      DECIMAL (11, 2) NULL,
    [NOMB_IMP_AVNT_TRTM]      DECIMAL (11, 2) NULL,
    [NUMR_PLN_RATT_TOKS]      NVARCHAR (3)    NULL,
    [MONT_IMP_AVNT_TRTM]      DECIMAL (11, 2) NULL,
    [MONT_IMP_COUR_RE_EMS]    DECIMAL (11, 2) NULL,
    [MONT_IMP_PLN_RATT]       DECIMAL (11, 2) NULL,
    [MONT_IMP_NON_REEM]       DECIMAL (11, 2) NULL,
    [FLG_EXST_PLN_RATT]       NVARCHAR (1)    NULL,
    [NOMB_ECHN_IMPS]          DECIMAL (11, 2) NULL,
    [DERN_COD_REJT_TOKS]      NVARCHAR (2)    NULL,
    [DAT_ECHN_REPR]           DATE            NULL,
    [DAT_PRCH_REPR]           DATE            NULL,
    [FLG_INDC_BALG]           NVARCHAR (1)    NULL,
    [FLG_CONT_TITR]           NVARCHAR (1)    NULL,
    [DAT_CESS_CONT_TITR]      DATE            NULL,
    [NUMR_FOND_COMM_CRNC]     NVARCHAR (2)    NULL,
    [NUMR_CESS_FCC]           DECIMAL (22)    NULL,
    [DAT_SORT_POUR_CONT_TITR] DATE            NULL,
    [COD_AGS]                 NVARCHAR (1)    NULL,
    [NOMB_JOUR_DIFF]          DECIMAL (22)    NULL,
    [TYP_CONT]                NVARCHAR (2)    NULL,
    [FLG_INDC_REPR]           NVARCHAR (1)    NULL,
    [ORGN_SIGN_CLNT]          NVARCHAR (1)    NULL,
    [PERD_AMRT]               DECIMAL (22)    NULL,
    [DAT_PASS_CNTX]           DATE            NULL,
    [REFR_CONT_REAM_CP]       NVARCHAR (11)   NULL,
    [DUR_TOTL_REPR_ECHN]      DECIMAL (22)    NULL,
    [INTR_REPR_REST_DU]       DECIMAL (11, 2) NULL,
    [DAT_DERN_IMPT_COMP]      DATE            NULL,
    [DAT_DERN_ACTL]           DATE            NULL,
    [DAT_DERN_REGL]           DATE            NULL,
    [DAT_DERN_REPR]           DATE            NULL,
    [DAT_BASC_COMP_EUR]       DATE            NULL,
    [INTR_CAPT_REST_DU]       DECIMAL (11, 2) NULL,
    [INTR_DIFF_REST_DU]       DECIMAL (11, 2) NULL,
    [COD_SOCT_TECH]           NVARCHAR (3)    NULL,
    [DAT_MODF]                DATE            NULL,
    [DAT_CRTN]                DATE            NULL,
    [TYP_AGNT]                NVARCHAR (1)    NULL,
    [IDNT_GEST]               NVARCHAR (8)    NULL,
    [DAT_CRTN_ENRG]           DATETIME        NULL,
    [DAT_DERN_MODF_ENRG]      DATETIME        NULL,
    [FLG_ENRG_COUR]           BIT             NULL
)
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Reseau de distribution', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'RES_DIST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Interet differe restant du', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'INTR_DIFF_REST_DU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Interet capitalise restant du', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'INTR_CAPT_REST_DU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date bascule compte en euro', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_BASC_COMP_EUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date prochaine representation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_PRCH_REPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date echeance representee', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_ECHN_REPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere representation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_DERN_REPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date dernier reglement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_DERN_REGL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant impaye non reemis', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_IMP_NON_REEM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant impaye plan rattrapage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_IMP_PLN_RATT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant impaye en cours re emis', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_IMP_COUR_RE_EMS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernier code rejet tokos', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DERN_COD_REJT_TOKS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant impaye avant traitement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_IMP_AVNT_TRTM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numero plan rattrapage tokos', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NUMR_PLN_RATT_TOKS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre impaye avant traitement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NOMB_IMP_AVNT_TRTM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant impaye declaration FICP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_IMP_DECL_FICP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date prochaine echeance rattrapage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_PRCH_ECHN_RATT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date du dernier impaye', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_DERN_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date du premier impaye', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_PRMR_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux interets de retard', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TAUX_INTR_RETR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant global IL impaye', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_GLBL_IL_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant global IR impaye', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_GLBL_IR_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant global accessoire impaye', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_GLBL_ACCS_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant global assurance impaye', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_GLBL_ASSR_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant global interet impaye', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_GLBL_INTR_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant global capital impaye', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_GLBL_CAPT_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant global impaye', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_GLBL_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre echeances impayees', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NOMB_ECHN_IMPS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere actualisation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_DERN_ACTL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nature assurance caution 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NATR_ASSR_CAUT_2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nature assurance caution 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NATR_ASSR_CAUT_1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nature assurance coemprunteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NATR_ASSR_COEM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nature assurance emprunteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NATR_ASSR_EMPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag assurance souscrite', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'FLG_ASSR_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code mode deblocage des fonds', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'COD_MOD_DEBL_FOND';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Interet reporte restant du', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'INTR_REPR_REST_DU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux vendeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TAUX_VEND';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Duree totale reports echeance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DUR_TOTL_REPR_ECHN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Reference contrat reamenage CP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'REFR_CONT_REAM_CP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Situation contrat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'SITT_CONT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant autres frais', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_AUTR_FRS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Frais de dossier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'FRS_DOSS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date modification reglement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_MODF_REGL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date modification du RIB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_MODF_RIB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Periodicite amortissement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'PERD_AMRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Pret mutualise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'PRT_MUTL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Origine signature client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'ORGN_SIGN_CLNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type contrat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TYP_CONT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre de jours differes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NOMB_JOUR_DIFF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code agios', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'COD_AGS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code fin de dossier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'COD_FIN_DOSS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date sortie pour contrat titrise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_SORT_POUR_CONT_TITR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numero cession au FCC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NUMR_CESS_FCC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numero fond commun de creance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NUMR_FOND_COMM_CRNC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date cession contrat titrise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_CESS_CONT_TITR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date prochaine anticipation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_PRCH_ANTC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux interet palier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TAUX_INTR_PALR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numero palier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NUMR_PALR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Assurance prochaine echeance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'ASSR_PRCH_ECHN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Accessoire prochaine echeance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'ACCS_PRCH_ECHN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Interet prochaine echeance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'INTR_PRCH_ECHN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Capital prochaine echeance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'CAPT_PRCH_ECHN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date prochaine echeance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_PRCH_ECHN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Assurance derniere echeance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'ASSR_DERN_ECHN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Accessoire derniere echeance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'ACCS_DERN_ECHN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Interet derniere echeance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'INTR_DERN_ECHN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Capital restant du derniere echeance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'CAPT_REST_DERN_ECHN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere echeance traitee', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_DERN_ECHN_TRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre echeances echues', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NOMB_ECHN_ECHS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre echeances totales', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NOMB_ECHN_TOTL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date fin previsionnelle dossier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_FIN_PRVS_DOSS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code situation tokos', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'COD_SITT_TOKS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date premiere echeance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_PRMR_ECHN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Duree legale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DUR_LEGL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code devise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'COD_DEVS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code objet finance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'COD_OBJT_FINN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Categorie pret', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'CATG_PRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date acceptation client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_ACCP_CLNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code mode reglement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'COD_MOD_REGL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant gestionnaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'IDNT_GEST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type agent', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TYP_AGNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code societe technique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'COD_SOCT_TECH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Capital restant du', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'CAPT_REST_DU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant total debloque', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_TOTL_DEBL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant nominal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_NOMN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date dernier deblocage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_DERN_DEBL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date accord tokos', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_ACCR_TOKS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numero compte SAB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NUMR_COMP_SAB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Reference contrat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'REFR_CONT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere modification enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date creation enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Délai décaissement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DEL_DECS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Courant Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prochaine perception echeance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_PRCH_PERC_ECHN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Indicateur de reprise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'FLG_INDC_REPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag xistence plan reamenagement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'FLG_EXST_PLN_REAM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Existence plan rattrapage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'FLG_EXST_PLN_RATT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro de compte prélèvement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NUMR_COMP_PRLV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant total dû', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_TOTL_DU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libelle titulaire compte prélèvement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'LIBL_TITL_COMP_PRLV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libelle banque et guichet prélèvement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'LIBL_BANQ_GUIC_PRLV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière imputation comptable', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_DERN_IMPT_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de décaissement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_DECS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code guichet prélèvement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'COD_GUIC_PRLV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code banque prélèvement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'COD_BANQ_PRLV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Indicateur balayage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'FLG_INDC_BALG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag contrat titrise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'FLG_CONT_TITR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date passage en contentieux', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_PASS_CNTX';
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_EQU_CRE_CONS_DAT_OBS]
ON [dbo].[DWH_EQUIPEMENT_CREDIT_CONSO] ([DAT_OBSR])
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_EQU_CRE_CONS_FLG_ENR_COU]
ON [dbo].[DWH_EQUIPEMENT_CREDIT_CONSO] ([FLG_ENRG_COUR])
GO