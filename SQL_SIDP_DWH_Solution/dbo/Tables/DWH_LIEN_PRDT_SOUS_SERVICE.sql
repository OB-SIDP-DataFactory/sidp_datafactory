﻿CREATE TABLE [dbo].[DWH_LIEN_PRDT_SOUS_SERVICE] (
    [DAT_OBSR]                 DATE           NOT NULL,
    [IDNT_LIEN_PRDT_SOUS_SERV] NUMERIC (38)   NULL,
    [IDNT_PRDT_SOUS]           DECIMAL (19)   NULL,
    [IDNT_SERV]                DECIMAL (19)   NULL,
    [IDNT_PERS_FE]                NUMERIC (38)   NULL,
    [IDNT_PERS_SF]             NVARCHAR (255) NULL,
    [DAT_CRTN]                 DATETIME2 (6)  NULL,
    [IDNT_CRTN]                NVARCHAR (255) NULL,
    [DAT_DERN_MODF]            DATETIME2 (6)  NULL,
    [IDNT_DERN_MODF]           NVARCHAR (255) NULL,
    [TYP_UTLS]                 NVARCHAR (255) NULL,
    [DAT_CRTN_ENRG]            DATETIME       NULL,
    [DAT_DERN_MODF_ENRG]       DATETIME       NULL,
    [FLG_ENRG_COUR]            BIT            NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere modification enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PRDT_SOUS_SERVICE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date creation enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PRDT_SOUS_SERVICE', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type utilisateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PRDT_SOUS_SERVICE', @level2type = N'COLUMN', @level2name = N'TYP_UTLS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PRDT_SOUS_SERVICE', @level2type = N'COLUMN', @level2name = N'IDNT_DERN_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PRDT_SOUS_SERVICE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PRDT_SOUS_SERVICE', @level2type = N'COLUMN', @level2name = N'IDNT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PRDT_SOUS_SERVICE', @level2type = N'COLUMN', @level2name = N'DAT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Personne SF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PRDT_SOUS_SERVICE', @level2type = N'COLUMN', @level2name = N'IDNT_PERS_SF';
GO

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant service', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PRDT_SOUS_SERVICE', @level2type = N'COLUMN', @level2name = N'IDNT_SERV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant produit souscrit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PRDT_SOUS_SERVICE', @level2type = N'COLUMN', @level2name = N'IDNT_PRDT_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant lien produit souscrit service', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PRDT_SOUS_SERVICE', @level2type = N'COLUMN', @level2name = N'IDNT_LIEN_PRDT_SOUS_SERV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PRDT_SOUS_SERVICE', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Courant Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PRDT_SOUS_SERVICE', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_PRDT_SOUS_SERVICE', @level2type = N'COLUMN', @level2name = N'IDNT_PERS_FE';

