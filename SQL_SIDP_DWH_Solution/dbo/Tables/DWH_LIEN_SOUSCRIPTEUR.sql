﻿CREATE TABLE [dbo].[DWH_LIEN_SOUSCRIPTEUR] (
    [DAT_OBSR]           DATE          NOT NULL,
    [IDNT_LIEN_SOUS]     NVARCHAR (50) NOT NULL,
    [NUMR_OPPR]          NVARCHAR (30) NULL,
    [IDNT_OPPR]          NVARCHAR (18) NULL,
    [IDNT_SOUS]          NVARCHAR (18) NULL,
    [ROL]                NVARCHAR (40) NULL,
    [FLG_PRNC]           BIT           NULL,
    [DAT_CRTN]           DATETIME      NULL,
    [IDNT_CRTN]          NVARCHAR (18) NULL,
    [DAT_DERN_MODF]      DATETIME      NULL,
    [IDNT_DERN_MODF]     NVARCHAR (18) NULL,
    [FLG_SUPP]           BIT           NULL,
    [DAT_CRTN_ENRG]      DATETIME      NULL,
    [DAT_DERN_MODF_ENRG] DATETIME      NULL,
    [FLG_ENRG_COUR]      BIT           NULL
)
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag supprime', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_SOUSCRIPTEUR', @level2type = N'COLUMN', @level2name = N'FLG_SUPP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_SOUSCRIPTEUR', @level2type = N'COLUMN', @level2name = N'IDNT_DERN_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_SOUSCRIPTEUR', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_SOUSCRIPTEUR', @level2type = N'COLUMN', @level2name = N'IDNT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_SOUSCRIPTEUR', @level2type = N'COLUMN', @level2name = N'DAT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag principal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_SOUSCRIPTEUR', @level2type = N'COLUMN', @level2name = N'FLG_PRNC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Role', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_SOUSCRIPTEUR', @level2type = N'COLUMN', @level2name = N'ROL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant souscripteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_SOUSCRIPTEUR', @level2type = N'COLUMN', @level2name = N'IDNT_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant opportunite ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_SOUSCRIPTEUR', @level2type = N'COLUMN', @level2name = N'IDNT_OPPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant lien souscripteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_SOUSCRIPTEUR', @level2type = N'COLUMN', @level2name = N'IDNT_LIEN_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro opportunite ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_SOUSCRIPTEUR', @level2type = N'COLUMN', @level2name = N'NUMR_OPPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_SOUSCRIPTEUR', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_SOUSCRIPTEUR', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date création de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_SOUSCRIPTEUR', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Courant Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_LIEN_SOUSCRIPTEUR', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_LIE_SOU_DAT_OBS]
ON [dbo].[DWH_LIEN_SOUSCRIPTEUR] ([DAT_OBSR])
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_LIE_SOU_FLG_ENR_COU]
ON [dbo].[DWH_LIEN_SOUSCRIPTEUR] ([FLG_ENRG_COUR])
GO