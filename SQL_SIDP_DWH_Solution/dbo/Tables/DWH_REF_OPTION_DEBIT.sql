﻿CREATE TABLE [dbo].[DWH_REF_OPTION_DEBIT] (
    [COD_OPTN_DEBIT]       VARCHAR (1)  NOT NULL,
    [LIBL_COUR_OPTN_DEBIT] VARCHAR (20) NULL,
    [LIBL_LONG_OPTN_DEBIT] VARCHAR (50) NULL,
    [DAT_CRTN_ENRG]        DATETIME     NOT NULL,
    [DAT_DERN_MODF_ENRG]   DATETIME     NOT NULL,
    [DAT_DEBT_VALD]        DATE         NOT NULL,
    [DAT_FIN_VALD]         DATE         NOT NULL,
    [FLG_ENRG_COUR]        BIT          NOT NULL
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de fin de validité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_OPTION_DEBIT', @level2type = N'COLUMN', @level2name = N'DAT_FIN_VALD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de début de validité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_OPTION_DEBIT', @level2type = N'COLUMN', @level2name = N'DAT_DEBT_VALD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_OPTION_DEBIT', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_OPTION_DEBIT', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé long Option Débit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_OPTION_DEBIT', @level2type = N'COLUMN', @level2name = N'LIBL_LONG_OPTN_DEBIT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé court Option Débit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_OPTION_DEBIT', @level2type = N'COLUMN', @level2name = N'LIBL_COUR_OPTN_DEBIT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Option Débit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_OPTION_DEBIT', @level2type = N'COLUMN', @level2name = N'COD_OPTN_DEBIT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Courant Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_OPTION_DEBIT', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';

