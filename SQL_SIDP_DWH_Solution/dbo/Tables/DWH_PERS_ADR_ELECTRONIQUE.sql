﻿CREATE TABLE  [dbo].[DWH_PERS_ADRESSE_ELECTRONIQUE] (
    [DAT_OBSR]           DATE          NULL,
    [IDNT_PERS_SF]       NVARCHAR (18) NULL,
    [TYP_ADRS]           NVARCHAR (255) NULL,
    [VALR_ADRS]          NVARCHAR (255) NULL,
    [FLG_TELP_FAVR]			bit  NULL,
    [DAT_CRTN_ENRG]      DATETIME      NULL,
    [DAT_DERN_MODF_ENRG] DATETIME      NULL,
	[FLG_ENRG_COUR] bit null
)
GO
EXEC sp_addextendedproperty @name=N'MS_Description', @value=N'Date Observation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_PERS_ADRESSE_ELECTRONIQUE', @level2type=N'COLUMN',@level2name=N'DAT_OBSR'
GO
EXEC sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant Personne SF' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_PERS_ADRESSE_ELECTRONIQUE', @level2type=N'COLUMN',@level2name=N'IDNT_PERS_SF'
GO
EXEC sp_addextendedproperty @name=N'MS_Description', @value=N'Type Adresse' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_PERS_ADRESSE_ELECTRONIQUE', @level2type=N'COLUMN',@level2name=N'TYP_ADRS'
GO
EXEC sp_addextendedproperty @name=N'MS_Description', @value=N'Valeur Adresse' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_PERS_ADRESSE_ELECTRONIQUE', @level2type=N'COLUMN',@level2name=N'VALR_ADRS'
GO
EXEC sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Téléphone favori' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_PERS_ADRESSE_ELECTRONIQUE', @level2type=N'COLUMN',@level2name=N'FLG_TELP_FAVR'
GO
EXEC sp_addextendedproperty @name=N'MS_Description', @value=N'Date de création de l''enregistrement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_PERS_ADRESSE_ELECTRONIQUE', @level2type=N'COLUMN',@level2name=N'DAT_CRTN_ENRG'
GO
EXEC sp_addextendedproperty @name=N'MS_Description', @value=N'Date de dernière modification de l''enregistrement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_PERS_ADRESSE_ELECTRONIQUE', @level2type=N'COLUMN',@level2name=N'DAT_DERN_MODF_ENRG'
GO
EXEC sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Enregistrement Courant' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_PERS_ADRESSE_ELECTRONIQUE', @level2type=N'COLUMN',@level2name=N'FLG_ENRG_COUR'
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_PERS_ADR_ELE_FLG_ENR_COU]
ON [dbo].[DWH_PERS_ADRESSE_ELECTRONIQUE] ([FLG_ENRG_COUR])
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_PERS_ADR_ELE_DAT_OBS]
ON [dbo].[DWH_PERS_ADRESSE_ELECTRONIQUE] ([DAT_OBSR])
GO