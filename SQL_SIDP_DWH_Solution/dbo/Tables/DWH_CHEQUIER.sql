CREATE TABLE [dbo].[DWH_CHEQUIER] (
    [DAT_OBSR]           DATE         NOT NULL,
    [COD_ETBL]           VARCHAR (4)  NULL,
    [COD_AGNC_STCK]      VARCHAR (4)  NULL,
    [COD_AGNC]           VARCHAR (4)  NULL,
    [NUMR_COMP]          VARCHAR (20) NULL,
    [TYP_CHQR]           VARCHAR (6)  NULL,
    [DAT_DEMN]           DATE         NULL,
    [NUMR_SEQN]          INT          NULL,
    [DAT_RECP_CHQR]      DATE         NULL,
    [DAT_REMS_CHQR]      DATE         NULL,
    [COD_LIVR]           VARCHAR (3)  NULL,
    [DAT_SUPP]           DATE         NULL,
    [COD_ETT_RENV]       VARCHAR (1)  NULL,
    [DAT_RENV]           DATE         NULL,
    [ORGN_CHQR]          VARCHAR (1)  NULL,
    [DAT_CRTN_ENRG]      DATETIME     NULL,
    [DAT_DERN_MODF_ENRG] DATETIME     NULL,
    [FLG_ENRG_COUR]      BIT          NULL
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CHEQUIER', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CHEQUIER', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Origine Chéquier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CHEQUIER', @level2type = N'COLUMN', @level2name = N'ORGN_CHQR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Renouvellement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CHEQUIER', @level2type = N'COLUMN', @level2name = N'DAT_RENV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Etat Renouvellement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CHEQUIER', @level2type = N'COLUMN', @level2name = N'COD_ETT_RENV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Suppression', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CHEQUIER', @level2type = N'COLUMN', @level2name = N'DAT_SUPP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Livraison', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CHEQUIER', @level2type = N'COLUMN', @level2name = N'COD_LIVR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Remise Chéquier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CHEQUIER', @level2type = N'COLUMN', @level2name = N'DAT_REMS_CHQR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Réception Chéquier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CHEQUIER', @level2type = N'COLUMN', @level2name = N'DAT_RECP_CHQR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Séquence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CHEQUIER', @level2type = N'COLUMN', @level2name = N'NUMR_SEQN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Demande', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CHEQUIER', @level2type = N'COLUMN', @level2name = N'DAT_DEMN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type Chéquier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CHEQUIER', @level2type = N'COLUMN', @level2name = N'TYP_CHQR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CHEQUIER', @level2type = N'COLUMN', @level2name = N'NUMR_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Agence Compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CHEQUIER', @level2type = N'COLUMN', @level2name = N'COD_AGNC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Agence Stock', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CHEQUIER', @level2type = N'COLUMN', @level2name = N'COD_AGNC_STCK';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Etablissement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CHEQUIER', @level2type = N'COLUMN', @level2name = N'COD_ETBL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CHEQUIER', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Courant Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_CHEQUIER', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';

