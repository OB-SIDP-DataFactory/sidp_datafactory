﻿CREATE TABLE [dbo].[DWH_PRDT_SOUS_CREDIT_CONSO] (
    [DAT_OBSR]                 DATE            NOT NULL,
    [IDNT_PRDT_SOUS_CRDT_CONS] DECIMAL (38)    NOT NULL,
    [NUMR_COMP]                NVARCHAR (11)   NULL,
    [NUMR_APPL_FI]             NVARCHAR (7)    NULL,
    [NUMR_CONT_FI]             NVARCHAR (11)   NULL,
    [NUMR_MAND_SEP]            NVARCHAR (255)  NULL,
    [DAT_FIN_PERD_RETR]        DATE            NULL,
    [DAT_FIN_DEBR_PRT]         DATE            NULL,
    [DAT_DEBT_DEBR_PRT]        DATE            NULL,
    [DRT_RETR_RENN]            DECIMAL (1)     NULL,
    [DAT_ACCP_ENGG_CFRM]       DATE            NULL,
    [DAT_APPR]                 DATE            NULL,
    [IDNT_COUL_SCR_CRDT]       DECIMAL (38)    NULL,
    [VALR_SCR_CRDT]            DECIMAL (38)    NULL,
    [TAUX_ENDT]                DECIMAL (19, 7) NULL,
    [IDNT_COUL_TAUX_ENDT]      DECIMAL (38)    NULL,
    [IDNT_TYP_COMP_PRLV_AUTM]  DECIMAL (38)    NULL,
    [IDNT_COUL_SCR_FI]         DECIMAL (38)    NULL,
    [REFS_EMPR_COUV_ASSR]      DECIMAL (1)     NOT NULL,
    [REFS_CO_EMPR_COUV_ASSR]   DECIMAL (1)     NOT NULL,
    [IDNT_PRLV_AUTM_PAYS]      DECIMAL (38)    NULL,
    [PRLV_AUTM_CL_CONT]        NVARCHAR (2)    NULL,
    [PRLV_AUTM_BBN]            NVARCHAR (30)   NULL,
    [MONT_EXCP_EMPR]           DECIMAL (19, 2) NULL,
    [NUMR_MAND_SEP_RES_DIST]   NVARCHAR (255)  NULL,
    [DAT_CRTN_ENRG]            DATETIME        NULL,
    [DAT_DERN_MODF_ENRG]       DATETIME        NULL,
    [FLG_ENRG_COUR]            BIT             NULL
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere modification enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRDT_SOUS_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date creation enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRDT_SOUS_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prelevement automatique BBAN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRDT_SOUS_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'PRLV_AUTM_BBN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prelevement automatique cle de controle', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRDT_SOUS_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'PRLV_AUTM_CL_CONT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant type de compte prelevement automatique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRDT_SOUS_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'IDNT_TYP_COMP_PRLV_AUTM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Valeur score credit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRDT_SOUS_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'VALR_SCR_CRDT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant couleur score credit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRDT_SOUS_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'IDNT_COUL_SCR_CRDT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date acceptation de engagement conformite', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRDT_SOUS_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_ACCP_ENGG_CFRM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Droit de retractation renonce', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRDT_SOUS_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DRT_RETR_RENN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de debut deboursement de pret', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRDT_SOUS_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_DEBT_DEBR_PRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de fin deboursement de pret', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRDT_SOUS_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_FIN_DEBR_PRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de fin de periode de retractation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRDT_SOUS_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_FIN_PERD_RETR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numero de mandat SEPA', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRDT_SOUS_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NUMR_MAND_SEP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant produit souscrit credit consommation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRDT_SOUS_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'IDNT_PRDT_SOUS_CRDT_CONS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux endettement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRDT_SOUS_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TAUX_ENDT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus emprunteur de la couverture assurance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRDT_SOUS_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'REFS_EMPR_COUV_ASSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numero de contrat FI', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRDT_SOUS_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NUMR_CONT_FI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numero compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRDT_SOUS_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NUMR_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numero application FI', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRDT_SOUS_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NUMR_APPL_FI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant couleur du taux endettement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRDT_SOUS_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'IDNT_COUL_TAUX_ENDT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant couleur score FI', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRDT_SOUS_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'IDNT_COUL_SCR_FI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRDT_SOUS_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date approbation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRDT_SOUS_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_APPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus du co emprunteur de la couverture assurance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRDT_SOUS_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'REFS_CO_EMPR_COUV_ASSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Courant Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRDT_SOUS_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant prelevement automatique pays', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRDT_SOUS_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'IDNT_PRLV_AUTM_PAYS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numero de mandat SEPA du réseau de distribution', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRDT_SOUS_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NUMR_MAND_SEP_RES_DIST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant exceptionnel emprunteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PRDT_SOUS_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_EXCP_EMPR';

