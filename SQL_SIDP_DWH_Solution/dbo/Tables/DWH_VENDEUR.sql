﻿CREATE TABLE [dbo].[DWH_VENDEUR] (
    [DAT_OBSR]                DATE             NOT NULL,
    [IDNT_VEND]               NVARCHAR (18)    NOT NULL,
    [CIVL]                    NVARCHAR (40)    NULL,
    [NOM_USG]                 NVARCHAR (80)    NULL,
    [PRNM]                    NVARCHAR (40)    NULL,
    [NOM]                     NVARCHAR (121)   NULL,
    [SOUS_RES_BOUT]           NVARCHAR (255)   NULL,
    [FLG_SUPP]                BIT              NULL,
    [FLG_PERS_ACCN]           BIT              NULL,
    [AUTR_RUE]                NVARCHAR (255)   NULL,
    [AUTR_VILL]               NVARCHAR (40)    NULL,
    [AUTR_ETT]                NVARCHAR (80)    NULL,
    [AUTR_COD_POST]           NVARCHAR (20)    NULL,
    [AUTR_PAYS]               NVARCHAR (80)    NULL,
    [AUTR_COD_ETT]            NVARCHAR (10)    NULL,
    [AUTR_COD_PAYS]           NVARCHAR (10)    NULL,
    [AUTR_LATT]               DECIMAL (18, 15) NULL,
    [AUTR_LONG]               DECIMAL (18, 15) NULL,
    [AUTR_GEOC_PRCS]          NVARCHAR (40)    NULL,
    [RUE_ENV_POST]            NVARCHAR (255)   NULL,
    [VILL_ENV_POST]           NVARCHAR (40)    NULL,
    [ETT_ENV_POST]            NVARCHAR (80)    NULL,
    [COD_POST_ENV_POST]       NVARCHAR (20)    NULL,
    [PAYS_ENV_POST]           NVARCHAR (80)    NULL,
    [COD_ETT_ENV_POST]        NVARCHAR (10)    NULL,
    [COD_PAYS_ENV_POST]       NVARCHAR (10)    NULL,
    [LATT_ENV_POST]           DECIMAL (18, 15) NULL,
    [LONG_ENV_POST]           DECIMAL (18, 15) NULL,
    [GEOC_PRCS_ENV_POST]      NVARCHAR (40)    NULL,
    [TELP]                    NVARCHAR (40)    NULL,
    [ADRS_MAIL]               NVARCHAR (80)    NULL,
    [IDNT_PRPR]               NVARCHAR (18)    NULL,
    [FLG_DESN_MAIL]           BIT              NULL,
    [FLG_DESN_TELC]           BIT              NULL,
    [FLG_NE_PAS_APPL]         BIT              NULL,
    [FLG_AUTR_AUTN_PORT_CLNT] BIT              NULL,
    [DAT_CRTN]                DATETIME         NULL,
    [IDNT_CRTN]               NVARCHAR (18)    NULL,
    [DAT_DERN_MODF]           DATETIME         NULL,
    [IDNT_DERN_MODF]          NVARCHAR (18)    NULL,
    [HORD_MODF_SYST]          DATETIME         NULL,
    [DAT_DERN_ACTV]           DATETIME         NULL,
    [RAIS_RETR_MAIL]          NVARCHAR (255)   NULL,
    [DAT_RETR_MAIL]           DATETIME         NULL,
    [FLG_RETR_MAIL]           BIT              NULL,
    [FLG_IDNT_CRTN_CLNT]      BIT              NULL,
    [TOTL_REVN_DISP]          DECIMAL (18, 2)  NULL,
    [TOTL_CHRG]               DECIMAL (18, 2)  NULL,
    [TOTL_REVN]               DECIMAL (18, 2)  NULL,
    [FLG_INVL_ADRS]           BIT              NULL,
    [FLG_REFS_APPL_TELP]      BIT              NULL,
    [FLG_REFS_CNTC_COUR]      BIT              NULL,
    [FLG_REFS_MAIL]           BIT              NULL,
    [FLG_REFS_SMS]            BIT              NULL,
    [FLG_REFS_MAIL_PART]      BIT              NULL,
    [FLG_REFS_SMS_PART]       BIT              NULL,
    [FLG_EXCT_CONT_UNCT]      BIT              NULL,
    [CUID_ENCD]               NVARCHAR (64)    NULL,
    [FLG_INDC_AMRC]           BIT              NULL,
    [RES_DIST]                NVARCHAR (4000)  NULL,
    [ALLC_FAML]               DECIMAL (18)     NULL,
    [ALLC_LOGM]               DECIMAL (18)     NULL,
    [FLG_CERT_AMF]            BIT              NULL,
    [FLG_SYNC_CNTC]           BIT              NULL,
    [CERT_IOBSP]              NVARCHAR (255)   NULL,
    [FLG_HABL_TA3C]           BIT              NULL,
    [FLG_RESD_FISC_FRNC]      BIT              NULL,
    [FLG_HABL_IOBSP_TECH]     BIT              NULL,
    [FLG_REFS_NOTF_SOLD]      BIT              NULL,
    [FLG_REFS_NOTF_PAIE]      BIT              NULL,
    [DAT_CRTN_ENRG]           DATETIME         NULL,
    [DAT_DERN_MODF_ENRG]      DATETIME         NULL,
    [FLG_ENRG_COUR]           BIT              NULL
);











GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Observation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'DAT_OBSR'
GO


GO


GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag supprime' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'FLG_SUPP'
GO


GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag person account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'FLG_PERS_ACCN'
GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom complet', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_VENDEUR', @level2type = N'COLUMN', @level2name = N'NOM';


GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Prenom' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'PRNM'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Civilite' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'CIVL'
GO


GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Autre rue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'AUTR_RUE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Autre ville' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'AUTR_VILL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Autre etat' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'AUTR_ETT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Autre code postal' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'AUTR_COD_POST'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Autre pays' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'AUTR_PAYS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Autre code etat' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'AUTR_COD_ETT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Autre code pays' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'AUTR_COD_PAYS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Autre latitude' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'AUTR_LATT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Autre longitude' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'AUTR_LONG'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Autre geocode precision' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'AUTR_GEOC_PRCS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Rue envoi postal' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'RUE_ENV_POST'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ville envoi postal' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'VILL_ENV_POST'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Etat  envoi postal' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'ETT_ENV_POST'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code postal envoi postal' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'COD_POST_ENV_POST'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pays envoi postal' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'PAYS_ENV_POST'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code etat envoi postal' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'COD_ETT_ENV_POST'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code pays envoi postal' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'COD_PAYS_ENV_POST'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Latitude envoi postal' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'LATT_ENV_POST'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Longitude envoi postal' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'LONG_ENV_POST'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Geocode precision envoi postal' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'GEOC_PRCS_ENV_POST'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Telephone' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'TELP'
GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant du proprietaire' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'IDNT_PRPR'
GO


GO


GO


GO


GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date de creation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'DAT_CRTN'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant de creation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'IDNT_CRTN'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date de derniere modification' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'DAT_DERN_MODF'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant de derniere modification' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'IDNT_DERN_MODF'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Horodateur modification systeme' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'HORD_MODF_SYST'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date derniere activite' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'DAT_DERN_ACTV'
GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total revenus disponibles' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'TOTL_REVN_DISP'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total charges' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'TOTL_CHRG'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total revenus' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'TOTL_REVN'
GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cuid encode' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'CUID_ENCD'
GO


GO


GO


GO


GO


GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reseau de distribution' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'RES_DIST'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Allocation familiale' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'ALLC_FAML'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Allocation logement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'ALLC_LOGM'
GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO


GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tech Temp Habilitation IOBSP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'FLG_HABL_IOBSP_TECH'
GO


GO


GO


GO


GO


GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date creation enregistrement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'DAT_CRTN_ENRG'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date derniere modification enregistrement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'DAT_DERN_MODF_ENRG'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Courant Actif' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DWH_VENDEUR', @level2type=N'COLUMN',@level2name=N'FLG_ENRG_COUR'
GO



GO



GO



GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Raison retour email', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_VENDEUR', @level2type = N'COLUMN', @level2name = N'RAIS_RETR_MAIL';


GO



GO



GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom d''usage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_VENDEUR', @level2type = N'COLUMN', @level2name = N'NOM_USG';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag contact account synchronisation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_VENDEUR', @level2type = N'COLUMN', @level2name = N'FLG_SYNC_CNTC';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag retour email', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_VENDEUR', @level2type = N'COLUMN', @level2name = N'FLG_RETR_MAIL';


GO



GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date retour email', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_VENDEUR', @level2type = N'COLUMN', @level2name = N'DAT_RETR_MAIL';


GO



GO



GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Adresse email', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_VENDEUR', @level2type = N'COLUMN', @level2name = N'ADRS_MAIL';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Residence fiscale en france', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_VENDEUR', @level2type = N'COLUMN', @level2name = N'FLG_RESD_FISC_FRNC';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus sms partenaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_VENDEUR', @level2type = N'COLUMN', @level2name = N'FLG_REFS_SMS_PART';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus sms', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_VENDEUR', @level2type = N'COLUMN', @level2name = N'FLG_REFS_SMS';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus notifications solde', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_VENDEUR', @level2type = N'COLUMN', @level2name = N'FLG_REFS_NOTF_SOLD';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus notification paiement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_VENDEUR', @level2type = N'COLUMN', @level2name = N'FLG_REFS_NOTF_PAIE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus email partenaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_VENDEUR', @level2type = N'COLUMN', @level2name = N'FLG_REFS_MAIL_PART';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus email', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_VENDEUR', @level2type = N'COLUMN', @level2name = N'FLG_REFS_MAIL';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus contact courrier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_VENDEUR', @level2type = N'COLUMN', @level2name = N'FLG_REFS_CNTC_COUR';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus appel telephonique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_VENDEUR', @level2type = N'COLUMN', @level2name = N'FLG_REFS_APPL_TELP';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ne pas appeler', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_VENDEUR', @level2type = N'COLUMN', @level2name = N'FLG_NE_PAS_APPL';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Invalide adresse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_VENDEUR', @level2type = N'COLUMN', @level2name = N'FLG_INVL_ADRS';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Indice americanite', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_VENDEUR', @level2type = N'COLUMN', @level2name = N'FLG_INDC_AMRC';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant creation client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_VENDEUR', @level2type = N'COLUMN', @level2name = N'FLG_IDNT_CRTN_CLNT';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Habilitation TA3C', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_VENDEUR', @level2type = N'COLUMN', @level2name = N'FLG_HABL_TA3C';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Execution contrôle unicite', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_VENDEUR', @level2type = N'COLUMN', @level2name = N'FLG_EXCT_CONT_UNCT';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Desinscription telecopie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_VENDEUR', @level2type = N'COLUMN', @level2name = N'FLG_DESN_TELC';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Desinscription email', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_VENDEUR', @level2type = N'COLUMN', @level2name = N'FLG_DESN_MAIL';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Certification AMF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_VENDEUR', @level2type = N'COLUMN', @level2name = N'FLG_CERT_AMF';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autoriser autoinscription portail client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_VENDEUR', @level2type = N'COLUMN', @level2name = N'FLG_AUTR_AUTN_PORT_CLNT';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Habilitation IOBSP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_VENDEUR', @level2type = N'COLUMN', @level2name = N'CERT_IOBSP';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant VENDEUR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_VENDEUR', @level2type = N'COLUMN', @level2name = N'IDNT_VEND';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Sous Réseau Boutique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_VENDEUR', @level2type = N'COLUMN', @level2name = N'SOUS_RES_BOUT';

