﻿CREATE TABLE [dbo].[DWH_EQUIPEMENT_SL] (
    [DAT_OBSR]             DATE           NULL,
    [IDNT_EQPM]            DECIMAL (19)   NULL,
    [NUMR_EQPM]            NVARCHAR (255) NULL,
    [DISC]                 NVARCHAR (50)  NULL,
    [IDNT_DOSS_SOUS]       DECIMAL (19)   NULL,
    [IDNT_PRDT]            DECIMAL (19)   NULL,
    [COD_PRDT]             NVARCHAR (255) NULL,
    [LIBL_PRDT]            NVARCHAR (255) NULL,
    [DAT_CRTN]             DATETIME2 (6)  NULL,
    [DAT_DERN_MIS_JOUR]    DATETIME2 (6)  NULL,
    [IDNT_CRTN]            NVARCHAR (255) NULL,
    [IDNT_DERN_MODF]       NVARCHAR (255) NULL,
    [TYP_UTLS]             NVARCHAR (255) NULL,
    [CL_PFM]               NVARCHAR (255) NULL,
    [IDNT_RES_DIST]        DECIMAL (19)   NULL,
    [IDNT_MARQ]            DECIMAL (38)   NULL,
    [FLG_SECR_BANC_OPT_IN] BIT            NULL,
    [FLG_VERF_IDNT_VEND]   BIT            NULL,
    [DAT_CRTN_ENRG]        DATETIME       NULL,
    [DAT_DERN_MODF_ENRG]   DATETIME       NULL,
    [FLG_ENRG_COUR]        BIT            NULL
)
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere modification enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date creation enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant produit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'IDNT_PRDT'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant equipement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'IDNT_EQPM'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant dossier souscription', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'IDNT_DOSS_SOUS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Discriminant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'DISC'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere mise a jour', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MIS_JOUR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'DAT_CRTN'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numero equipement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'NUMR_EQPM'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'DAT_OBSR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé produit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'LIBL_PRDT'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code produit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'COD_PRDT'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Courant Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type utilisateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'TYP_UTLS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Réseau de distribution', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'IDNT_RES_DIST'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant marque', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'IDNT_MARQ'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifant derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'IDNT_DERN_MODF'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'IDNT_CRTN'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag vérification identification vendeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'FLG_VERF_IDNT_VEND'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag secret bancaire Opt In', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'FLG_SECR_BANC_OPT_IN'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Clé PFM', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'CL_PFM'
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_EQU_SL_DAT_OBS_IDNT_DOSS]
ON [dbo].[DWH_EQUIPEMENT_SL] ([DAT_OBSR], [IDNT_DOSS_SOUS])
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_EQU_SL_FLG_ENR_COU]
ON [dbo].[DWH_EQUIPEMENT_SL] ([FLG_ENRG_COUR])
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_EQU_SL_DAT_OBS]
ON [dbo].[DWH_EQUIPEMENT_SL] ([DAT_OBSR])
GO