﻿CREATE TABLE [dbo].[DWH_REF_POST_DONN_ANLT_OPRT] (
    [COD_OPRT]                   VARCHAR (3) NOT NULL,
    [TYP_TRNS_POST_DEBT]         INT         NULL,
    [TYP_TRNS_POST_FIN]          INT         NULL,
    [EVNM_DECL_POST_DEBT]        INT         NULL,
    [EVNM_DECL_POST_FIN]         INT         NULL,
    [COD_OPRT_CART_BL_POST_DEBT] INT         NULL,
    [COD_OPRT_CART_BL_POST_FIN]  INT         NULL,
    [TYP_CART_POST_DEBT]         INT         NULL,
    [TYP_CART_POST_FIN]          INT         NULL,
    [NATR_OPRT_POST_DEBT]        INT         NULL,
    [NATR_OPRT_POST_FIN]         INT         NULL,
    [RES_COMP_POST_DEBT]         INT         NULL,
    [RES_COMP_POST_FIN]          INT         NULL,
    [TYP_RES_POST_DEBT]          INT         NULL,
    [TYP_RES_POST_FIN]           INT         NULL,
    [COD_FOUR_OPRT_POST_DEBT]    INT         NULL,
    [COD_FOUR_OPRT_POST_FIN]     INT         NULL,
    [COD_COMM_POST_DEBT]         INT         NULL,
    [COD_COMM_POST_FIN]          INT         NULL,
    [DAT_CRTN_ENRG]              DATETIME    NOT NULL,
    [DAT_DERN_MODF_ENRG]         DATETIME    NOT NULL,
    [DAT_DEBT_VALD]              DATE        NOT NULL,
    [DAT_FIN_VALD]               DATE        NOT NULL,
    [FLG_ENRG_COUR]              BIT         NOT NULL
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de fin de validité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'DAT_FIN_VALD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de début de validité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'DAT_DEBT_VALD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Commission (Position Fin)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'COD_COMM_POST_FIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Commission (Position Début)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'COD_COMM_POST_DEBT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Fournisseur Opération (Position Fin)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'COD_FOUR_OPRT_POST_FIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Fournisseur Opération (Position Début)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'COD_FOUR_OPRT_POST_DEBT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type Réseau (Position Fin)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'TYP_RES_POST_FIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type Réseau (Position Début)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'TYP_RES_POST_DEBT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Réseau de compensation (Position Fin)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'RES_COMP_POST_FIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Réseau de compensation (Position Début)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'RES_COMP_POST_DEBT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nature Opération (Position Fin)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'NATR_OPRT_POST_FIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nature Opération (Position Début)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'NATR_OPRT_POST_DEBT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type Carte (Position Fin)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'TYP_CART_POST_FIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type Carte (Position Début)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'TYP_CART_POST_DEBT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Opération Carte Bleue (Position Fin)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'COD_OPRT_CART_BL_POST_FIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Opération Carte Bleue (Position Début)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'COD_OPRT_CART_BL_POST_DEBT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Evènement Déclencheur (Position Fin)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'EVNM_DECL_POST_FIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Evènement Déclencheur (Position Début)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'EVNM_DECL_POST_DEBT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type Transaction (Position Fin)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'TYP_TRNS_POST_FIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type Transaction (Position Début)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'TYP_TRNS_POST_DEBT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Opération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'COD_OPRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Courant Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';

