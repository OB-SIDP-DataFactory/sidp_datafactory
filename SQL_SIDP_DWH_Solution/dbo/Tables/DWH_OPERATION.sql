﻿CREATE TABLE [dbo].[DWH_OPERATION] (
    [DAT_TRTM]           DATE            NULL,
    [DAT_OPE]            DATE            NULL,
    [DAT_CTBL]           DATE            NULL,
    [DAT_VALR]           DATE            NULL,
    [NUMR_COMP]          VARCHAR (20)    NULL,
    [COD_OPE]            VARCHAR (3)     NULL,
    [NUMR_OPE]           INT             NULL,
    [COD_EVNM]           VARCHAR (3)     NULL,
    [COD_SCHM]           INT             NULL,
    [NUMR_PIEC]          INT             NULL,
    [NUMR_ECRT]          INT             NULL,
    [COD_ANLT]           VARCHAR (6)     NULL,
    [STRC_ANLT]          VARCHAR (6)     NULL,
    [COD_EXNR]           VARCHAR (1)     NULL,
    [COD_BDF]            VARCHAR (3)     NULL,
    [NATR_OPE]           VARCHAR (3)     NULL,
    [MONT_OPE_DEVS]      DECIMAL (16, 2) NULL,
    [MONT_OPE_EUR]       DECIMAL (16, 2) NULL,
    [DEVS]               VARCHAR (3)     NULL,
    [SENS_OPE]           VARCHAR (1)     NULL,
    [COD_UTLS]           VARCHAR (4)     NULL,
    [COD_AGNC]           VARCHAR (4)     NULL,
    [COD_SERV]           VARCHAR (2)     NULL,
    [COD_SOUS_SERV]      VARCHAR (2)     NULL,
    [COD_ANNL]           VARCHAR (1)     NULL,
    [COD_BANQ_EIC]       VARCHAR (5)     NULL,
    [LIBL_MOUV_1]        VARCHAR (30)    NULL,
    [LIBL_MOUV_2]        VARCHAR (30)    NULL,
    [LIBL_MOUV_3]        VARCHAR (30)    NULL,
    [COD_EMTT]           VARCHAR (6)     NULL,
    [DAT_TRTM_MAD]       DATE            NULL,
    [DAT_REFR_MAD]       DATE            NULL,
    [DAT_LIMT_REJT_MAD]  DATE            NULL,
    [COD_UTLS_INTT_MAD]  VARCHAR (4)     NULL,
    [DAT_CRTN_MAD]       DATE            NULL,
    [DAT_UTLS_MAD]       DATE            NULL,
    [COD_UTLS_MAD]       VARCHAR (4)     NULL,
    [REJT_ACCP_MAD]      VARCHAR (1)     NULL,
    [DONN_ANLT]          VARCHAR (80)    NULL,
    [LIBL_CMPL_1]        VARCHAR (70)    NULL,
    [LIBL_CMPL_2]        VARCHAR (70)    NULL,
    [NUMR_REMS]          VARCHAR (7)     NULL,
    [NUMR_CHQ]           VARCHAR (7)     NULL,
    [IDNT_DO]            VARCHAR (7)     NULL,
    [IDNT_CTPR]          VARCHAR (7)     NULL,
    [NOM_RAIS_SOCL_CTPR] VARCHAR (70)    NULL,
    [NUMR_COMP_CTPR]     VARCHAR (35)    NULL,
    [BIC_ETBL_CTPR]      VARCHAR (12)    NULL,
    [COD_BANQ_CTPR]      VARCHAR (12)    NULL,
    [COD_PAYS_CTPR]      VARCHAR (5)     NULL,
    [FLG_COMP_CLNT]      VARCHAR (1)     NULL,
    [FIXN_DEVS]          DECIMAL (14, 9) NULL,
    [COD_AGNC_COMP]      VARCHAR (6)     NULL,
    [DAT_CRTN_ENRG]      DATETIME        NULL,
    [DAT_DERN_MODF_ENRG] DATETIME        NULL,
    [FLG_ENRG_COUR]      BIT             NULL
)
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Enregistrement Courant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Agence Compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'COD_AGNC_COMP'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fixing Devise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'FIXN_DEVS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Compte Client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'FLG_COMP_CLNT'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Pays Contrepartie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'COD_PAYS_CTPR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Banque Contrepartie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'COD_BANQ_CTPR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'BIC Etablissement Contrepartie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'BIC_ETBL_CTPR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Compte Contrepartie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'NUMR_COMP_CTPR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom/Raison Sociale Contrepartie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'NOM_RAIS_SOCL_CTPR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Contrepartie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'IDNT_CTPR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant DO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'IDNT_DO'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Chèque', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'NUMR_CHQ'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Remise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'NUMR_REMS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Complémentaire 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'LIBL_CMPL_2'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Complémentaire 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'LIBL_CMPL_1'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Donnée Analytique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'DONN_ANLT'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Rejet Acceptation MAD', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'REJT_ACCP_MAD'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Utilisateur MAD', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'COD_UTLS_MAD'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Utilisation MAD', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'DAT_UTLS_MAD'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Création MAD', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_MAD'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Utilisateur Initiateur MAD', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'COD_UTLS_INTT_MAD'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Limite Rejet MAD', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'DAT_LIMT_REJT_MAD'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date référence  MAD', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'DAT_REFR_MAD'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Traitement MAD', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'DAT_TRTM_MAD'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Emetteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'COD_EMTT'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Mouvement 3', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'LIBL_MOUV_3'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Mouvement 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'LIBL_MOUV_2'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Mouvement 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'LIBL_MOUV_1'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Banque EIC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'COD_BANQ_EIC'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Annulation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'COD_ANNL'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Sous-Service', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'COD_SOUS_SERV'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Service', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'COD_SERV'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Agence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'COD_AGNC'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Utilisateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'COD_UTLS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Sens Opération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'SENS_OPE'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Devise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'DEVS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant Opération Euro', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'MONT_OPE_EUR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant Opération Devise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'MONT_OPE_DEVS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nature Opération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'NATR_OPE'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code BDF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'COD_BDF'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Exonération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'COD_EXNR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Structure Analytique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'STRC_ANLT'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Analytique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'COD_ANLT'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Ecriture', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'NUMR_ECRT'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro pièce', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'NUMR_PIEC'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Schéma', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'COD_SCHM'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Evénement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'COD_EVNM'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Opération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'NUMR_OPE'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Opération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'COD_OPE'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'NUMR_COMP'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Valeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'DAT_VALR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Comptable', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'DAT_CTBL'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Opération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'DAT_OPE'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Traitement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_OPERATION', @level2type = N'COLUMN', @level2name = N'DAT_TRTM'
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_OPE_DAT_TRT]
ON [dbo].[DWH_OPERATION] ([DAT_TRTM])
GO
CREATE NONCLUSTERED INDEX [IX_NC_DWH_OPE_FLG_ENR_COU]
ON [dbo].[DWH_OPERATION] ([FLG_ENRG_COUR])
GO