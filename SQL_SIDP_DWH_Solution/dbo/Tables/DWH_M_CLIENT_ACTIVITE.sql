CREATE TABLE [dbo].[DWH_M_CLIENT_ACTIVITE] (
    [DAT_OBSR]           DATE         NOT NULL,
    [NUMR_CLNT_SAB]      VARCHAR (7)  NOT NULL,
    [ACTV_CAV]           VARCHAR (50) NULL,
    [ACTV_CSL]           VARCHAR (50) NULL,
    [ACTV_GLBL]          VARCHAR (50) NULL,
    [DAT_CRTN_ENRG]      DATETIME     NOT NULL,
    [DAT_DERN_MODF_ENRG] DATETIME     NOT NULL,
    [FLG_ENRG_COUR]      BIT          NOT NULL
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_M_CLIENT_ACTIVITE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_M_CLIENT_ACTIVITE', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Activité Globale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_M_CLIENT_ACTIVITE', @level2type = N'COLUMN', @level2name = N'ACTV_GLBL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Activité CSL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_M_CLIENT_ACTIVITE', @level2type = N'COLUMN', @level2name = N'ACTV_CSL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Activité CAV', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_M_CLIENT_ACTIVITE', @level2type = N'COLUMN', @level2name = N'ACTV_CAV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Client SAB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_M_CLIENT_ACTIVITE', @level2type = N'COLUMN', @level2name = N'NUMR_CLNT_SAB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_M_CLIENT_ACTIVITE', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Courant Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_M_CLIENT_ACTIVITE', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';

