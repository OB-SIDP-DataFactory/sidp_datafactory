﻿CREATE TABLE [dbo].[DWH_PISTE_AUDIT_OPPORTUNITE] (
    [IDNT_SF]            NVARCHAR (50)  NOT NULL,
    [NUMR_OPPR]          NVARCHAR (30)  NULL,
    [IDNT_OPPR]          NVARCHAR (50)  NULL,
    [IDNT_CRTN]          NVARCHAR (50)  NULL,
    [DAT_MODF]           DATETIME2 (7)  NOT NULL,
    [CHMP_MODF]          NVARCHAR (255) NULL,
    [FLG_SUPP]           NVARCHAR (10)  NULL,
    [NOUV_VALR]          NVARCHAR (MAX) NULL,
    [ANCN_VALR]          NVARCHAR (MAX) NULL,
    [DAT_DEBT_VALD]      DATETIME2 (7)  NULL,
    [DAT_FIN_VALD]       DATETIME2 (7)  NULL,
    [DAT_CRTN_ENRG]      DATETIME       NULL,
    [DAT_DERN_MODF_ENRG] DATETIME       NULL,
    [FLG_ENRG_COUR]      BIT            NOT NULL
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification de l enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PISTE_AUDIT_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date création de l enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PISTE_AUDIT_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ancienne valeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PISTE_AUDIT_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'ANCN_VALR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nouvelle valeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PISTE_AUDIT_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'NOUV_VALR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag supprime', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PISTE_AUDIT_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_SUPP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Champ modifie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PISTE_AUDIT_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'CHMP_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PISTE_AUDIT_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DAT_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PISTE_AUDIT_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'IDNT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant opportunite', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PISTE_AUDIT_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'IDNT_OPPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant SF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PISTE_AUDIT_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'IDNT_SF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro opportunite', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PISTE_AUDIT_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'NUMR_OPPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de fin de validité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PISTE_AUDIT_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DAT_FIN_VALD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de début de validité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PISTE_AUDIT_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DAT_DEBT_VALD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Courant Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DWH_PISTE_AUDIT_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';

