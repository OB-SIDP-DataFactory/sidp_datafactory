﻿
CREATE PROC [dbo].[PKGDWH_PROC_ALIM_DWH_AGG_OPERATION_3MOIS]  
    @DATE_ALIM  DATE  
  , @mois_histo INT         -- valeur 0 par défaut = traitement nominal par défaut
  , @nbRows     INT OUTPUT  -- nb lignes processées

AS  
BEGIN 

IF @DATE_ALIM IS NULL 
  SET @DATE_ALIM = dateadd(dd,-1,cast(GETDATE() as date));
 
IF @mois_histo is null 
  SET @mois_histo = 0; --valeur 0 par défaut = traitement nominal par défaut

Declare @DTETIME_ALIM DATETIME = getdate();

--calcul de la période de chargement

IF OBJECT_ID('tempdb..#WK_TEMPS_ACCOUNT') IS NOT NULL
BEGIN DROP TABLE #WK_TEMPS_ACCOUNT END
 
select distinct eomonth(StandardDate) as StandardDate
  into #WK_TEMPS_ACCOUNT
  from [dbo].[DWH_REF_TEMPS]
 where eomonth(StandardDate) <  eomonth(@DATE_ALIM)
   and eomonth(StandardDate) >= dateadd(month, -@mois_histo, eomonth(@DATE_ALIM))

 --select * from #WK_TEMPS_ACCOUNT order by 1 desc
 --SELECT format(cast(dateadd(mm,-3,'2018-10-31') as date),'yyyyMM') AS min_mois,format(cast('2018-10-31' as date),'yyyyMM') AS max_mois

/* Table Intermédiaire*/

IF OBJECT_ID('tempdb..#DWH_AGG_OPERATION_3MOIS') IS NOT NULL
BEGIN DROP TABLE #DWH_AGG_OPERATION_3MOIS END

CREATE TABLE #DWH_AGG_OPERATION_3MOIS(
	[DAT_VALD] [date] NULL,
	[MOIS_TRTM] [varchar](20) NULL,
	[NUMR_COMP] [varchar](20) NULL,
	[NOMB_OPRT_DEBIT] [int] NULL,
	[NOMB_OPRT_CRDT] [int] NULL,
	[MONT_OPRT_DEBIT] [decimal](18, 2) NULL,
	[MONT_OPRT_CRDT] [decimal](18, 2) NULL
)

/*Précalcul des Opérations*/

declare @Mois_Traitement int ;
declare @Standardate date ;
declare Mois_traitement_Cursor CURSOR for select format(StandardDate,'yyyyMM') from #WK_TEMPS_ACCOUNT

OPEN Mois_traitement_Cursor;
FETCH NEXT FROM Mois_traitement_Cursor INTO @Mois_Traitement;
WHILE @@FETCH_STATUS = 0
   BEGIN
   set @Standardate = (select StandardDate from #WK_TEMPS_ACCOUNT where format(StandardDate,'yyyyMM') = @Mois_Traitement);
   
   Insert into #DWH_AGG_OPERATION_3MOIS
   (
    [DAT_VALD] 
   ,[MOIS_TRTM]
   ,[NUMR_COMP]
   ,[NOMB_OPRT_DEBIT]
   ,[NOMB_OPRT_CRDT]
   ,[MONT_OPRT_DEBIT]
   ,[MONT_OPRT_CRDT]
   )
   select @Standardate,
          @Mois_Traitement,
          t.[NUMR_COMP],  
	      sum(case when ref.SENS = 'Débit' and Ref.FLG_INTT_CLNT = 1 then t.[NOMB_OPE] else 0 END) - sum(case when Ref.FLG_INTT_CLNT= -1 and ref.SENS='Annulation débit' then  t.[NOMB_OPE] else 0 END  ) as NB_OP_DEB ,
	      sum(case when ref.SENS = 'Crédit' and Ref.FLG_INTT_CLNT = 1 then t.[NOMB_OPE] else 0 END) - sum(case when Ref.FLG_INTT_CLNT= -1 and ref.SENS='Annulation crédit' then t.[NOMB_OPE] else 0 END ) as NB_OP_CRE,
	      sum(case when ref.SENS = 'Débit' and Ref.FLG_INTT_CLNT = 1 then ABS(t.[MONT_OPE]) else 0 END) - sum(case when Ref.FLG_INTT_CLNT= -1 and ref.SENS='Annulation débit' then  ABS(t.[MONT_OPE]) else 0 END  ) as MTT_OP_DEB ,
	      sum(case when ref.SENS = 'Crédit' and Ref.FLG_INTT_CLNT = 1 then ABS(t.[MONT_OPE]) else 0 END) - sum(case when Ref.FLG_INTT_CLNT= -1 and ref.SENS='Annulation crédit' then ABS(t.[MONT_OPE]) else 0 END ) as MTT_OP_CRE
   from [dbo].[DWH_AGG_OPERATION_MOIS] t
   left join DWH_REF_TYPE_OPERATION Ref on  t.IDNT_TYP_OPE = Ref.IDNT_TYP_OPRT
   where t.[MOIS_OBSR] > format(cast(dateadd(mm,-3,@Standardate) as date),'yyyyMM') and t.[MOIS_OBSR] <= format(cast(@Standardate as date),'yyyyMM')
   group by  t.[NUMR_COMP],ref.SENS,Ref.FLG_INTT_CLNT
      FETCH NEXT FROM Mois_traitement_Cursor INTO @Mois_Traitement;
   END;
CLOSE Mois_traitement_Cursor;
DEALLOCATE Mois_traitement_Cursor;

/*Delete en cas de rechargement*/

DELETE FROM DWH_AGG_OPERATION_3MOIS
where [DAT_OBSR] IN (select StandardDate from #WK_TEMPS_ACCOUNT)
/* Insertion dans la table [DWH_AGG_OPERATION_3MOIS] */

INSERT INTO [dbo].[DWH_AGG_OPERATION_3MOIS]
           ([DAT_OBSR]
           ,[MOIS_TRTM]
           ,[MOIS_TRAITES]
           ,[NUMR_COMP]
           ,[NOMB_OPE_DEBIT]
           ,[NOMB_OPE_CRDT]
           ,[MONT_OPE_DEBIT]
           ,[MONT_OPE_CRDT]
           ,[DAT_CRTN_ENRG]
           ,[DAT_DERN_MODF_ENRG])

SELECT 
           [DAT_VALD] 
          ,[MOIS_TRTM]
		  ,CONCAT(format(cast(dateadd(mm,0,[DAT_VALD]) as date),'yyyyMM'),' | ',format(cast(dateadd(mm,-1,[DAT_VALD]) as date),'yyyyMM'),' | ',format(cast(dateadd(mm,-2,[DAT_VALD]) as date),'yyyyMM'))
          ,[NUMR_COMP]
          ,SUM([NOMB_OPRT_DEBIT]) AS [NOMB_OPE_DEBIT]
          ,SUM([NOMB_OPRT_CRDT])  AS [NOMB_OPE_CRDT]
          ,SUM([MONT_OPRT_DEBIT]) AS [MONT_OPE_DEBIT]
          ,SUM([MONT_OPRT_CRDT])  AS [MONT_OPE_CRDT]
          ,@DTETIME_ALIM
          ,@DTETIME_ALIM
FROM #DWH_AGG_OPERATION_3MOIS
GROUp BY 
[DAT_VALD] 
,[MOIS_TRTM]
,[NUMR_COMP]
SELECT @nbRows = @@ROWCOUNT;

END

/***  Test ****/
/*
select * from #DWH_AGG_OPERATION_3MOIS  order by NUMR_COMP
select * from [dbo].[DWH_AGG_OPERATION_3MOIS] order by NUMR_COMP

select * from  [dbo].[DWH_AGG_OPERATION_MOIS] where NUMR_COMP IN('75000182843','75000182836' )

--select * from DWH_REF_TYPE_OPERATION
*/