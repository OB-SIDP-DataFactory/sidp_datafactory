﻿
CREATE PROC [dbo].[PKGDWH_PROC_ALIM_TRVL_PREM_OPRT_INIT] 
    @nbRows     INT OUTPUT  -- nb lignes processées
AS  
BEGIN 
--INITIALISATION  COMPTE OB_GB 
 IF OBJECT_ID('tempdb..#COMPTE_OB_GB') IS NOT NULL
BEGIN DROP TABLE #COMPTE_OB_GB END

Select distinct c.DWHCPTCOM
INTO #COMPTE_OB_GB
FROM [$(DatabaseInstanceName)].[$(DataHubDatabaseName)].[dbo].VW_PV_SA_Q_COMPTE c
INNER JOIN [dbo].[DWH_VUE_RUBRIQUE_COMPTABLE_GB_OB]e on c.DWHCPTRUB = e.[COD_RUBR_COMP]
--Pré Calcul des premières opérations 
 IF OBJECT_ID('tempdb..#RES_PRMR_OPE_INIT') IS NOT NULL
BEGIN DROP TABLE #RES_PRMR_OPE_INIT  END

select CASE WHEN COD_OPE = 'CTB' and  NUM_CPT like '203130EUR%' THEN substring(DON_ANA,8,11) ELSE NUM_CPT END   as NUM_CPT
     , min(case WHEN COD_OPE IN ('CTB') AND SUBSTRING(DON_ANA,TYP_CART_POST_DEBT,TYP_CART_POST_FIN-TYP_CART_POST_DEBT+1) IN ('WMI') then DTE_OPE else null end) as [DT_PRE_UTI_PM]
	 , Min(CASE WHEN COD_OPE IN ('CTB') AND COD_EVE IN ('IMM')  AND SUBSTRING(DON_ANA,TYP_CART_POST_DEBT,TYP_CART_POST_FIN-TYP_CART_POST_DEBT+1) IN ('CBC','CBI','CBP','CE2','CE9','CEJ','CPC','CPM','CPP','CRK','CVE','MGD','MGI') then DTE_OPE else null end) as [DT_PRE_UTI_CB]
     , min(case when MTT_EUR >= 0                                          then DTE_OPE else null end) as [DT_PRE_OPE_DEB]
     , min(case when MTT_EUR < 0                                          then DTE_OPE else null end) as [DT_PRE_OPE_CRE]
     , min(case when COD_OPE in ('*OF') and COD_EVE in ('PBC')            then DTE_OPE else null end) as [DT_VRS_PRI_BVN]
into #RES_PRMR_OPE_INIT 
from [$(DatabaseInstanceName)].[$(DataHubDatabaseName)].[dbo].[PV_SA_Q_MOUVEMENT] MO
INNER JOIN #COMPTE_OB_GB CPT ON CPT.DWHCPTCOM  = CASE WHEN COD_OPE = 'CTB' and  NUM_CPT like '203130EUR%' THEN substring(DON_ANA,8,11) ELSE NUM_CPT END  
LEFT OUTER JOIN dbo.DWH_REF_POST_DONN_ANLT_OPRT ON COD_OPE = dbo.DWH_REF_POST_DONN_ANLT_OPRT.COD_OPRT AND 
                  DTE_TRT >= dbo.DWH_REF_POST_DONN_ANLT_OPRT.DAT_DEBT_VALD AND DTE_TRT < dbo.DWH_REF_POST_DONN_ANLT_OPRT.DAT_FIN_VALD
group by CASE WHEN COD_OPE = 'CTB' and  NUM_CPT like '203130EUR%' THEN substring(DON_ANA,8,11) ELSE NUM_CPT END

--select * from #RES_PRMR_OPE_INIT where NUM_CPT = '75000039557'
--select * from [$(DatabaseInstanceName)].[$(DataHubDatabaseName)].[dbo].[PV_SA_Q_MOUVEMENT] where NUM_CPT = '75000039557' order by DTE_OPE asc       

  --Pré Calcul premier versement (CAV et CSL) 
 IF OBJECT_ID('tempdb..#RES_PRMR_VER_INIT') IS NOT NULL
BEGIN DROP TABLE #RES_PRMR_VER_INIT  END

Select NUM_CPT,[DAT_PREM_VERS],[MONT_PRMR_VERS]
INTO #RES_PRMR_VER_INIT
FROM (
SELECT  CASE WHEN COD_OPE = 'CTB' and  NUM_CPT like '203130EUR%' THEN substring(DON_ANA,8,11) ELSE NUM_CPT END   as NUM_CPT,
        DTE_OPE as [DAT_PREM_VERS] ,
		MTT_EUR as [MONT_PRMR_VERS],
		row_number() over (partition by NUM_CPT order by DTE_OPE asc) as FLG_OPE
from [$(DatabaseInstanceName)].[$(DataHubDatabaseName)].[dbo].[PV_SA_Q_MOUVEMENT] MO
  INNER JOIN #COMPTE_OB_GB CPT ON CPT.DWHCPTCOM  = CASE WHEN COD_OPE = 'CTB' and  NUM_CPT like '203130EUR%' THEN substring(DON_ANA,8,11) ELSE NUM_CPT END  
where 
(COD_OPE IN ('ERE') AND COD_EVE IN ('OUV') AND COD_ANA IN ('VERPA'))
OR (COD_OPE IN ('A0V') AND NAT_OPE in ('OUI'))
OR (COD_OPE IN ('ERE') AND COD_EVE IN ('OUV') AND COD_ANA NOT IN ('VERPA')) 
)c where FLG_OPE = 1

--select count(NUM_CPT),count(distinct NUM_CPT) from #RES_PRMR_VER_INIT
--select * from #RES_PRMR_VER_INIT
    

-- LEFT JOIN #RES_PRMR_OPE et #RES_PRMR_VER
 IF OBJECT_ID('tempdb..#RES_PRMR') IS NOT NULL
BEGIN DROP TABLE #RES_PRMR  END

SELECT   M.NUM_CPT
	   , M.[DT_PRE_UTI_PM]
	   , M.[DT_PRE_UTI_CB]
	   , M.[DT_PRE_OPE_DEB]
	   , M.[DT_PRE_OPE_CRE]
	   , M.[DT_VRS_PRI_BVN]
	   , C.[DAT_PREM_VERS]
	   , C.[MONT_PRMR_VERS] 
INTO   #RES_PRMR
FROM   #RES_PRMR_OPE_INIT  M
LEFT JOIN #RES_PRMR_VER_INIT C ON M.NUM_CPT=C.NUM_CPT

--select * from #RES_PRMR

/*Merge dans la table TRVL_OPERATION_DAT_PRMR */

INSERT [dbo].[TRVL_OPERATION_DAT_PRMR]
([NUMR_COMP]
,[DAT_VERS_PRM_BIEN]
,[DAT_PRMR_UTLS_PAIE_MOBL]
,[DAT_PRMR_UTLS_PAIE_CB]
,[DAT_PRMR_OPRT_DEBT]
,[DAT_PRMR_OPRT_CRDT]
,[DAT_PREM_VERS]
,[MONT_PRMR_VERS] 
,[DAT_CRTN_ENRG]
,[DAT_DERN_MODF_ENRG])
SELECT 
 NUM_CPT 
,DT_VRS_PRI_BVN
,DT_PRE_UTI_PM          
,DT_PRE_UTI_CB     
,DT_PRE_OPE_DEB     
,DT_PRE_OPE_CRE
,[DAT_PREM_VERS]
,[MONT_PRMR_VERS] 
,getdate() 
,getdate() 
from #RES_PRMR
;

SELECT @nbRows = @@ROWCOUNT

END