﻿CREATE PROC dbo.PKGDWH_PROC_ALIM_DWH_AGG_OPERATION_MOIS  
    @DATE_ALIM  DATE  
  , @mois_histo INT         -- Valeur 0 par défaut = traitement nominal par défaut
  , @nbRows     INT OUTPUT  -- Nb lignes processées

AS  
BEGIN 

IF @DATE_ALIM IS NULL 
  SET @DATE_ALIM = DATEADD(dd,-1,CAST(GETDATE() AS DATE));
 
IF @mois_histo IS NULL 
  SET @mois_histo = 0 ; -- Valeur 0 par défaut = traitement nominal par défaut

DECLARE @DTETIME_ALIM DATETIME = getdate();

-- Calcul de la période de chargement
IF OBJECT_ID('tempdb..#WK_TEMPS_ACCOUNT') IS NOT NULL
BEGIN DROP TABLE #WK_TEMPS_ACCOUNT END
 
SELECT DISTINCT EOMONTH(StandardDate) AS StandardDate
  INTO #WK_TEMPS_ACCOUNT
  FROM dbo.DWH_REF_TEMPS
 WHERE StandardDate <= eomonth(@DATE_ALIM)
   AND StandardDate >= DATEADD(MONTH, -@mois_histo, eomonth(@DATE_ALIM))

-- Nettoyage de la table cible (suppréssion des lignes a recharger)
DELETE FROM dbo.DWH_AGG_OPERATION_MOIS
WHERE MOIS_OBSR IN (SELECT FORMAT(CAST(StandardDate AS DATE),'yyyyMM') FROM #WK_TEMPS_ACCOUNT)

-- Alimentation de la table cible DWH
INSERT INTO dbo.DWH_AGG_OPERATION_MOIS
           (DAT_OBSR,
		    MOIS_OBSR,
            MOIS_OPE,
            NUMR_COMP,
            IDNT_TYP_OPE,
		    COD_PAYS_TRNS,
            NOMB_OPE,
            MONT_OPE,
            DAT_CRTN_ENRG,
            DAT_DERN_MODF_ENRG)
SELECT StandardDate,
       FORMAT(CAST(StandardDate AS DATE),'yyyyMM') AS MOIS_OBSR,
	   FORMAT(DWH_OPE.DAT_OPE, 'yyyyMM') AS MOIS_OPE,
       DWH_OPE.NUMR_COMP,
       DWH_OPE.IDNT_TYP_OPE,
	   DWH_OPE.COD_PAYS_TRNS,
       SUM(DWH_OPE.NOMB_OPE) AS NOMB_OPE_MOIS ,
	   SUM(DWH_OPE.MONT_OPE) AS MONT_OPE_MOIS ,
	   @DTETIME_ALIM ,
	   @DTETIME_ALIM 
FROM dbo.DWH_OPERATION DWH_OPE
JOIN #WK_TEMPS_ACCOUNT t ON t.StandardDate = EOMONTH(DWH_OPE.DAT_OBSR)
GROUP BY StandardDate, FORMAT(DWH_OPE.DAT_OPE, 'yyyyMM'), DWH_OPE.NUMR_COMP, DWH_OPE.IDNT_TYP_OPE, DWH_OPE.COD_PAYS_TRNS
SELECT @nbRows = @@ROWCOUNT;

END