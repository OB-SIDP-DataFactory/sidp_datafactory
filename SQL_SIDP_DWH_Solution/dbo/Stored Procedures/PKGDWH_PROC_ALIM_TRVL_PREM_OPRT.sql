﻿
CREATE PROC [dbo].[PKGDWH_PROC_ALIM_TRVL_PREM_OPRT] 
    @nbRows     INT OUTPUT  -- nb lignes processées
AS  
BEGIN 

--Pré Calcul des premières opérations 
 IF OBJECT_ID('tempdb..#RES_PRMR_OPE') IS NOT NULL
BEGIN DROP TABLE #RES_PRMR_OPE  END

select MO.NUM_CPT
     , min(case when [COD_REF_OPE] = 1135                              then DTE_OPE else null end) as [DT_PRE_UTI_PM]
	 , Min(case when [COD_REF_OPE] >= 1004 and [COD_REF_OPE] <= 1133   then DTE_OPE else null end) as [DT_PRE_UTI_CB]
     , min(case when MTT_EUR > 0                                       then DTE_OPE else null end) as [DT_PRE_OPE_DEB]
     , min(case when MTT_EUR < 0                                       then DTE_OPE else null end) as [DT_PRE_OPE_CRE]
     , min(case when [COD_REF_OPE] = 1136                              then DTE_OPE else null end) as [DT_VRS_PRI_BVN]
  into #RES_PRMR_OPE 
  from [$(DatabaseInstanceName)].[$(DataHubDatabaseName)].[dbo].[PV_SA_Q_MOUVEMENT_JOUR] MO
 inner join [dbo].[DWH_EQUIPEMENT_COMPTE] EQCPT ON MO.NUM_CPT = EQCPT.NUMR_COMP and [FLG_COMP_PRDT] = 1 and FLG_ENRG_COUR = 1
 group by MO.NUM_CPT

--Select * from #RES_PRMR_OPE

 --Pré Calcul premier versement (CAV et CSL) 
 IF OBJECT_ID('tempdb..#RES_PRMR_VER') IS NOT NULL
BEGIN DROP TABLE #RES_PRMR_VER  END

Select NUM_CPT,[DAT_PREM_VERS],[MONT_PRMR_VERS]
INTO #RES_PRMR_VER
FROM(
SELECT  NUM_CPT,
        DTE_OPE as [DAT_PREM_VERS] ,
		MTT_EUR as [MONT_PRMR_VERS],
		row_number() over (partition by NUM_CPT order by DTE_OPE asc) as FLG_OPE
FROM [$(DatabaseInstanceName)].[$(DataHubDatabaseName)].dbo.PV_SA_Q_MOUVEMENT_JOUR MO 
 inner join [dbo].[DWH_EQUIPEMENT_COMPTE] EQCPT ON MO.NUM_CPT = EQCPT.NUMR_COMP and [FLG_COMP_PRDT] = 1 and FLG_ENRG_COUR = 1
where [COD_REF_OPE] IN (1149,1153,1160)
)c where FLG_OPE = 1

--Select * from #RES_PRMR_VER

-- LEFT JOIN #RES_PRMR_OPE et #RES_PRMR_VER
 IF OBJECT_ID('tempdb..#RES_PRMR') IS NOT NULL
BEGIN DROP TABLE #RES_PRMR  END

SELECT   M.NUM_CPT
	   , M.[DT_PRE_UTI_PM]
	   , M.[DT_PRE_UTI_CB]
	   , M.[DT_PRE_OPE_DEB]
	   , M.[DT_PRE_OPE_CRE]
	   , M.[DT_VRS_PRI_BVN]
	   , C.[DAT_PREM_VERS]
	   , C.[MONT_PRMR_VERS] 
INTO   #RES_PRMR
FROM   #RES_PRMR_OPE M
LEFT JOIN #RES_PRMR_VER C ON M.NUM_CPT=C.NUM_CPT

--select * from #RES_PRMR

/*Merge dans la table TRVL_OPERATION_DAT_PRMR */

MERGE [dbo].[TRVL_OPERATION_DAT_PRMR] AS TARGET
USING #RES_PRMR  AS SOURCE 
ON (TARGET.NUMR_COMP = SOURCE.NUM_CPT) 

when matched And 
(TARGET.[DAT_VERS_PRM_BIEN] != SOURCE.DT_VRS_PRI_BVN
or TARGET.[DAT_PRMR_UTLS_PAIE_MOBL] != SOURCE.DT_PRE_UTI_PM
or TARGET.[DAT_PRMR_UTLS_PAIE_CB] != SOURCE.DT_PRE_UTI_CB
or TARGET.[DAT_PRMR_OPRT_DEBT] != SOURCE.DT_PRE_OPE_DEB
or TARGET.[DAT_PRMR_OPRT_CRDT] != SOURCE.DT_PRE_OPE_CRE
OR TARGET.[DAT_PREM_VERS] != SOURCE.[DAT_PREM_VERS]
OR TARGET.[MONT_PRMR_VERS] != SOURCE.[MONT_PRMR_VERS]
)
then
Update 

set  [DAT_VERS_PRM_BIEN] = ISNULL(TARGET.[DAT_VERS_PRM_BIEN],SOURCE.DT_VRS_PRI_BVN)
    ,[DAT_PRMR_UTLS_PAIE_MOBL] = ISNULL(TARGET.[DAT_PRMR_UTLS_PAIE_MOBL],SOURCE.DT_PRE_UTI_PM ) 
    ,[DAT_PRMR_UTLS_PAIE_CB] = ISNULL(TARGET.[DAT_PRMR_UTLS_PAIE_CB],SOURCE.DT_PRE_UTI_CB)
    ,[DAT_PRMR_OPRT_DEBT] = ISNULL(TARGET.[DAT_PRMR_OPRT_DEBT],SOURCE.DT_PRE_OPE_DEB )
    ,[DAT_PRMR_OPRT_CRDT] = ISNULL(TARGET.[DAT_PRMR_OPRT_CRDT],SOURCE.DT_PRE_OPE_CRE)
	,[DAT_PREM_VERS] = ISNULL(TARGET.[DAT_PREM_VERS],SOURCE.[DAT_PREM_VERS])
	,[MONT_PRMR_VERS] = ISNULL(TARGET.[MONT_PRMR_VERS],SOURCE.[MONT_PRMR_VERS])
    ,[DAT_DERN_MODF_ENRG] = getdate()
WHEN NOT MATCHED BY TARGET THEN 
INSERT
([NUMR_COMP]
,[DAT_VERS_PRM_BIEN]
,[DAT_PRMR_UTLS_PAIE_MOBL]
,[DAT_PRMR_UTLS_PAIE_CB]
,[DAT_PRMR_OPRT_DEBT]
,[DAT_PRMR_OPRT_CRDT]
,[DAT_PREM_VERS] 
,[MONT_PRMR_VERS]
,[DAT_CRTN_ENRG]
,[DAT_DERN_MODF_ENRG])
VALUES (
 SOURCE.NUM_CPT 
,SOURCE.DT_VRS_PRI_BVN
,SOURCE.DT_PRE_UTI_PM          
,SOURCE.DT_PRE_UTI_CB     
,SOURCE.DT_PRE_OPE_DEB     
,SOURCE.DT_PRE_OPE_CRE
,SOURCE.[DAT_PREM_VERS]
,SOURCE.[MONT_PRMR_VERS]
,getdate() 
,getdate()
);
SELECT @nbRows = @@ROWCOUNT

END