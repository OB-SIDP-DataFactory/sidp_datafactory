﻿CREATE PROC [dbo].[PKGDWH_PROC_ALIM_DWH_EQUIPEMENT_COMPTE]  @DATE_ALIM DATE  
                                   ,@jours_histo INT    --valeur 1 par défaut = traitement nominal par défaut
								   ,@nbRows INT OUTPUT -- nb lignes processées
AS  
BEGIN 

declare  @DATE_EXEC DATETIME= getdate()
				
IF @DATE_ALIM IS NULL 
  SET @DATE_ALIM = dateadd(dd,-1,cast(GETDATE() as date));
 
IF @jours_histo IS NULL 
  SET @jours_histo = 1; --valeur 1 par défaut = traitement nominal par défaut  

--------------------------------------///////////////// Step 1
--Création d'un sous-ensemble de la table dimension temps

-- PURGE AVANT INSERT
IF OBJECT_ID('tempdb..#WK_TEMPS_ACCOUNT') IS NOT NULL
BEGIN DROP TABLE #WK_TEMPS_ACCOUNT END

SELECT DISTINCT 
       CASE WHEN datediff(DAY,StandardDate,@DATE_ALIM) < 90 THEN StandardDate
		    ELSE EOMONTH(StandardDate) END AS StandardDate
into #WK_TEMPS_ACCOUNT
from dbo.DWH_REF_TEMPS 
WHERE dateadd(DAY,- @jours_histo, @DATE_ALIM) < StandardDate AND StandardDate <= cast(@DATE_ALIM as date);

--select * from #WK_TEMPS_ACCOUNT
--------------------------------------///////////////// Step 2
--Merge de la table _Compte && _Compte_History et insertion dans une table work

-- PURGE AVANT INSERT
IF OBJECT_ID('tempdb..#WK_SAB_COMPTES') IS NOT NULL
BEGIN Drop TABLE #WK_SAB_COMPTES END

-- INSERTION DANS TABLE WORK
SELECT
       c.DWHCPTETA
      ,c.DWHCPTPLA
      ,c.DWHCPTCOM
      ,c.DWHCPTRUB
      ,c.DWHCPTINT
      ,c.DWHCPTNBR
      ,c.DWHCPTPPAL
      ,c.DWHCPTAGE
      ,c.DWHCPTDEV
      ,c.DWHCPTOPR
      ,c.DWHCPTVAL
      ,c.DWHCPTCPT
      ,c.DWHCPTCPC
      ,c.DWHCPTRES
      ,c.DWHCPTDUR
      ,c.DWHCPTMG1
      ,c.DWHCPTMG2
      ,c.DWHCPTGAR
      ,c.DWHCPTGA1
      ,c.DWHCPTQUA
      ,c.DWHCPTGA2
      ,c.DWHCPTGA3
      ,c.DWHCPTSMOD
      ,c.DWHCPTSMOC
      ,c.DWHCPTSMVD
      ,c.DWHCPTSMVC
      ,c.DWHCPTCDS
      ,c.DWHCPTDAA
      ,c.DWHCPTPER
      ,c.DWHCPTNPR
      ,c.DWHCPTMTA
      ,c.DWHCPTMTU
      ,c.DWHCPTMTD
      ,c.DWHCPTMTR
      ,c.DWHCPTMTNR
      ,c.DWHCPTMTNS
      ,c.DWHCPTSMD
      ,c.DWHCPTSMC
      ,c.DWHCPTML1
      ,c.DWHCPTML2
      ,c.DWHCPTCLO
      ,c.DWHCPTDAC
      ,c.DWHCPTDAO
      ,c.DWHCPTMIP
      ,c.DWHCPTCIP
      ,c.DWHCPTPBC
      ,c.DWHCPTCPB
      ,c.DWHCPTDDD
      ,c.DWHCPTNJD
      ,c.DWHCPTFLG
      ,c.DWHCPTCPM
      ,c.DWHCPTMVC
      ,c.DWHCPTCOC
      ,c.DWHCPTCOB
      ,c.DWHCPTCLA
      ,c.DWHCPTUTI
      ,c.DWHCPTTXN
      ,c.DWHCPTREV
      ,c.DWHCPTSUR
       ,case when c.DWHCPTFRE = 1 then 'Mensuelle'
	        when c.DWHCPTFRE = 2 then 'Quotidienne'
	   END as DWHCPTFRE
      ,c.DWHCPTRIS
	  ,0 as FLG_COMP_PRDT
	  ,c.Validity_StartDate
	  ,c.Validity_EndDate
INTO #WK_SAB_COMPTES	
FROM [$(DatabaseInstanceName)].[$(DataHubDatabaseName)].[dbo].[VW_PV_SA_Q_COMPTE] c
WHERE c.DWHCPTAGE  NOT IN (51,52) and c.Validity_EndDate > DATEADD(DAY,- @jours_histo,@DATE_ALIM);

--select count(*) from #WK_SAB_COMPTES
--------------------------------------///////////////// Step 3
--Photo sur la période requise et insertion dans une table res

-- PURGE AVANT INSERT
IF OBJECT_ID('tempdb..#RES_COMPTES') IS NOT NULL
BEGIN Drop TABLE #RES_COMPTES END

-- INSERTION DANS TABLE WORK
SELECT
	[DTE_VALID], [DWHCPTETA], [DWHCPTPLA], [DWHCPTCOM], [DWHCPTRUB], [DWHCPTINT], [DWHCPTNBR], [DWHCPTPPAL], [DWHCPTAGE], [DWHCPTDEV], [DWHCPTOPR], [DWHCPTVAL], [DWHCPTCPT], [DWHCPTCPC], [DWHCPTRES], [DWHCPTDUR], [DWHCPTMG1], [DWHCPTMG2], [DWHCPTGAR], [DWHCPTGA1], [DWHCPTQUA], [DWHCPTGA2], [DWHCPTGA3], [DWHCPTSMOD], [DWHCPTSMOC], [DWHCPTSMVD], [DWHCPTSMVC], [DWHCPTCDS], [DWHCPTDAA], [DWHCPTPER], [DWHCPTNPR], [DWHCPTMTA], [DWHCPTMTU], [DWHCPTMTD], [DWHCPTMTR], [DWHCPTMTNR], [DWHCPTMTNS], [DWHCPTSMD], [DWHCPTSMC], [DWHCPTML1], [DWHCPTML2], [DWHCPTCLO], [DWHCPTDAC], [DWHCPTDAO], [DWHCPTMIP], [DWHCPTCIP], [DWHCPTPBC], [DWHCPTCPB], [DWHCPTDDD], [DWHCPTNJD], [DWHCPTFLG], [DWHCPTCPM], [DWHCPTMVC], [DWHCPTCOC], [DWHCPTCOB], [DWHCPTCLA], [DWHCPTUTI], [DWHCPTTXN], [DWHCPTREV], [DWHCPTSUR], [DWHCPTFRE], [DWHCPTRIS], [FLG_COMP_PRDT], [Validity_StartDate], [Validity_EndDate]
INTO
	#RES_COMPTES
FROM
(
SELECT d.StandardDate AS DTE_VALID
      ,c.DWHCPTETA
      ,c.DWHCPTPLA
      ,c.DWHCPTCOM
      ,c.DWHCPTRUB
      ,c.DWHCPTINT
      ,c.DWHCPTNBR
      ,c.DWHCPTPPAL
      ,c.DWHCPTAGE
      ,c.DWHCPTDEV
      ,c.DWHCPTOPR
      ,c.DWHCPTVAL
      ,c.DWHCPTCPT
      ,c.DWHCPTCPC
      ,c.DWHCPTRES
      ,c.DWHCPTDUR
      ,c.DWHCPTMG1
      ,c.DWHCPTMG2
      ,c.DWHCPTGAR
      ,c.DWHCPTGA1
      ,c.DWHCPTQUA
      ,c.DWHCPTGA2
      ,c.DWHCPTGA3
      ,c.DWHCPTSMOD
      ,c.DWHCPTSMOC
      ,c.DWHCPTSMVD
      ,c.DWHCPTSMVC
      ,c.DWHCPTCDS
      ,c.DWHCPTDAA
      ,c.DWHCPTPER
      ,c.DWHCPTNPR
      ,c.DWHCPTMTA
      ,c.DWHCPTMTU
      ,c.DWHCPTMTD
      ,c.DWHCPTMTR
      ,c.DWHCPTMTNR
      ,c.DWHCPTMTNS
      ,c.DWHCPTSMD
      ,c.DWHCPTSMC
      ,c.DWHCPTML1
      ,c.DWHCPTML2
      ,c.DWHCPTCLO
      ,c.DWHCPTDAC
      ,c.DWHCPTDAO
      ,c.DWHCPTMIP
      ,c.DWHCPTCIP
      ,c.DWHCPTPBC
      ,c.DWHCPTCPB
      ,c.DWHCPTDDD
      ,c.DWHCPTNJD
      ,c.DWHCPTFLG
      ,c.DWHCPTCPM
      ,c.DWHCPTMVC
      ,c.DWHCPTCOC
      ,c.DWHCPTCOB
      ,c.DWHCPTCLA
      ,c.DWHCPTUTI
      ,c.DWHCPTTXN
      ,c.DWHCPTREV
      ,c.DWHCPTSUR
      ,c.DWHCPTFRE
      ,c.DWHCPTRIS
	  ,c.FLG_COMP_PRDT
	  ,c.Validity_StartDate
	  ,c.Validity_EndDate
FROM #WK_TEMPS_ACCOUNT d
outer apply 
	(
	select 
	    DWHCPTETA
      , DWHCPTPLA
      , DWHCPTCOM
      , DWHCPTRUB
      , DWHCPTINT
      , DWHCPTNBR
      , DWHCPTPPAL
      , DWHCPTAGE
      , DWHCPTDEV
      , DWHCPTOPR
      , DWHCPTVAL
      , DWHCPTCPT
      , DWHCPTCPC
      , DWHCPTRES
      , DWHCPTDUR
      , DWHCPTMG1
      , DWHCPTMG2
      , DWHCPTGAR
      , DWHCPTGA1
      , DWHCPTQUA
      , DWHCPTGA2
      , DWHCPTGA3
      , DWHCPTSMOD
      , DWHCPTSMOC
      , DWHCPTSMVD
      , DWHCPTSMVC
      , DWHCPTCDS
      , DWHCPTDAA
      , DWHCPTPER
      , DWHCPTNPR
      , DWHCPTMTA
      , DWHCPTMTU
      , DWHCPTMTD
      , DWHCPTMTR
      , DWHCPTMTNR
      , DWHCPTMTNS
      , DWHCPTSMD
      , DWHCPTSMC
      , DWHCPTML1
      , DWHCPTML2
      , DWHCPTCLO
      , DWHCPTDAC
      , DWHCPTDAO
      , DWHCPTMIP
      , DWHCPTCIP
      , DWHCPTPBC
      , DWHCPTCPB
      , DWHCPTDDD
      , DWHCPTNJD
      , DWHCPTFLG
      , DWHCPTCPM
      , DWHCPTMVC
      , DWHCPTCOC
      , DWHCPTCOB
      , DWHCPTCLA
      , DWHCPTUTI
      , DWHCPTTXN
      , DWHCPTREV
      , DWHCPTSUR
      , DWHCPTFRE
      , DWHCPTRIS
	  , FLG_COMP_PRDT
	  ,Validity_StartDate
	  ,Validity_EndDate
	from #WK_SAB_COMPTES
	where (cast(Validity_StartDate as date) <= d.StandardDate and d.StandardDate < cast(Validity_EndDate as date) )  
	) c
)r;
-- MAJ du flag COMP PRDT pour les comptes nouvelle offre
UPDATE #RES_COMPTES
SET [FLG_COMP_PRDT] = 1
FROM #RES_COMPTES CPT
WHERE EXISTS (SELECT '1'
			   FROM [$(DatabaseInstanceName)].[$(DataHubDatabaseName)].[dbo].PV_FE_EQUIPMENT_V2 FE_V2
			   WHERE CPT.DWHCPTCOM = FE_V2.EQUIPMENT_NUMBER)
 AND CPT.DWHCPTRUB != '203120' -- Modif : Homologation
 AND CPT.FLG_COMP_PRDT = 0;

-- MAJ du flag COMP PRDT pour les comptes ancienne offre 
UPDATE #RES_COMPTES
SET [FLG_COMP_PRDT] = 1
FROM #RES_COMPTES c
INNER JOIN [dbo].[DWH_VUE_RUBRIQUE_COMPTABLE_GB] e on c.DWHCPTRUB = e.[COD_RUBR_COMP]
where [FLG_COMP_PRDT] = 0;

--------------------------------------///////////////// Step 4
-- Nettoyage + insertion dans la table cible
-- Nettoyage de la table cible (suppréssion des lignes a recharger)
DELETE FROM [dbo].[DWH_EQUIPEMENT_COMPTE]
WHERE [DAT_OBSR] IN (SELECT StandardDate FROM #WK_TEMPS_ACCOUNT);

-- UPDATE FLG_ENRG_COUR
UPDATE [dbo].[DWH_EQUIPEMENT_COMPTE]
Set FLG_ENRG_COUR = 0 
WHERE FLG_ENRG_COUR = 1;

-- ALIMENTATION DE LA TABLE CIBLE [dbo].[DWH_EQUIPEMENT_COMPTE]
INSERT INTO [dbo].[DWH_EQUIPEMENT_COMPTE]
           ([DAT_OBSR]
           ,[COD_ETBL]
           ,[NUMR_PLAN]
           ,[COD_AGNC]
           ,[NUMR_COMP]
           ,[COD_RUBR]
           ,[TYP_COMP]
		   ,[COD_PRDT_SF]
           ,[INTITULE]
           ,[NOMB_TITL]
           ,[NUMR_CLNT_SAB_TITL_PRCP]
           ,[DAT_OUVR]
           ,[DAT_CLTR]
           ,[MOTF_CLTR]
           ,[COD_DEVS]
           ,[SOLD_DAT_OPE]
           ,[SOLD_DAT_VALR]
           ,[SOLD_DAT_COMP]
           ,[SOLD_DAT_COMP_DEVS_COMP]
           ,[DUR_REST_COUR_JOUR]
           ,[DUR_INTL_JOUR]
           ,[GARN_DEVS_OPE]
           ,[GARN_DEVS_BASE]
           ,[COD_GARN]
           ,[GARANT]
           ,[COD_QUAL_GARN]
           ,[COD_RESD_GARN]
           ,[NON_UTLS]
           ,[SOMM_FLX_DEBIT_MOIS]
           ,[SOMM_FLX_CRDT_MOIS]
           ,[SOLD_MOYE_COMP_DEBIT]
           ,[SOLD_MOYE_COMP_CRDT]
           ,[ECHL_COD_SELC]
           ,[ECHL_DAT_DERN_ARRT]
           ,[ECHL_UNT_PERD]
           ,[ECHL_NOMB_UNTS_PERD]
           ,[MONT_AUTR]
           ,[DAT_DEBT_DECV]
           ,[MONT_UTLS]
           ,[MONT_DEPS]
           ,[NOMB_JOUR_DEPS]
           ,[FLG_DEPS_FACL_CAIS]
           ,[FLG_MOBL_BANC]
           ,[IEDOM_MONT_REFN]
           ,[IEDOM_MONT_NON_REFN]
           ,[IEDOM_MONT_SENS_RESR]
           ,[CONTVAL_SOLD_MOYE_DEBIT]
           ,[CONTVAL_SOLD_MOYE_CRDT]
           ,[CONTVAL_MONT_DEBIT]
           ,[CONTVAL_MONT_CRDT]
           ,[MONT_INTR_DEBIT_CUML]
           ,[MONT_INTR_DEBIT_CUML_DEVS_BASE]
           ,[PART_BANQ_TRSR]
           ,[PART_BANQ_TRSR_DEVS_BASE]
           ,[FLG_COMP_MAIT]
           ,[NUMR_COMP_MAIT]
           ,[CUML_MOUV_CRDT_DEVS_BASE]
           ,[COMM_PRVS_ECHL_DEVS_COMP]
           ,[COMM_PRVS_ECHL_DEVS_BASE]
           ,[CLSS_SECR]
           ,[FLG_UTLS_MOIS]
           ,[TAUX_NOMN]
           ,[REVN_COTITL]
           ,[COD_SURT]
           ,[FRQN]
           ,[FLG_PERM_RISQ]
           ,[FLG_COMP_PRDT]
		   ,[COD_APPOR]
           ,[DAT_CRTN_ENRG]
           ,[DAT_DERN_MODF_ENRG]
           ,[FLG_ENRG_COUR])
SELECT 
		 CPT_SAB.DTE_VALID
		,CPT_SAB.DWHCPTETA
		,CPT_SAB.DWHCPTPLA
		,CPT_SAB.DWHCPTAGE
		,CPT_SAB.DWHCPTCOM
		,CPT_SAB.DWHCPTRUB
		,PC.PLAN_DE_COMPTES_CODE_PRODUIT
		,CPTBIS.CPTBISCOD
		,CPT_SAB.DWHCPTINT
		,CPT_SAB.DWHCPTNBR
		,CPT_SAB.DWHCPTPPAL
		,CPT_SAB.DWHCPTDAO
		,CPT_SAB.DWHCPTDAC
		,CPT_SAB.DWHCPTCLO
		,CPT_SAB.DWHCPTDEV
		,CPT_SAB.DWHCPTOPR
		,CPT_SAB.DWHCPTVAL
		,CPT_SAB.DWHCPTCPT
		,CPT_SAB.DWHCPTCPC
		,CPT_SAB.DWHCPTRES
		,CPT_SAB.DWHCPTDUR
		,CPT_SAB.DWHCPTMG1
		,CPT_SAB.DWHCPTMG2
		,CPT_SAB.DWHCPTGAR
		,CPT_SAB.DWHCPTGA1
		,CPT_SAB.DWHCPTQUA
		,CPT_SAB.DWHCPTGA2
		,CPT_SAB.DWHCPTGA3
		,CPT_SAB.DWHCPTSMOD
		,CPT_SAB.DWHCPTSMOC
		,CPT_SAB.DWHCPTSMVD
		,CPT_SAB.DWHCPTSMVC
		,CPT_SAB.DWHCPTCDS
		,CPT_SAB.DWHCPTDAA
		,CPT_SAB.DWHCPTPER
		,CPT_SAB.DWHCPTNPR
		,CPT_SAB.DWHCPTMTA
		,CPT_SAB.DWHCPTDDD
		,CPT_SAB.DWHCPTMTU
		,CPT_SAB.DWHCPTMTD
		,CPT_SAB.DWHCPTNJD
		,CASE WHEN CPT_SAB.DWHCPTMTD > 0 THEN 1 ELSE 0 END AS FLG_DEPS_FACL_CAIS
		,CASE WHEN SBDCHKACCOUNT.BANK_DOMICILIATION > 0 THEN 1 ELSE 0 END AS BANK_DOMICILIATION
		,CPT_SAB.DWHCPTMTR
		,CPT_SAB.DWHCPTMTNR
		,CPT_SAB.DWHCPTMTNS
		,CPT_SAB.DWHCPTSMD
		,CPT_SAB.DWHCPTSMC
		,CPT_SAB.DWHCPTML1
		,CPT_SAB.DWHCPTML2
		,CPT_SAB.DWHCPTMIP
		,CPT_SAB.DWHCPTCIP
		,CPT_SAB.DWHCPTPBC
		,CPT_SAB.DWHCPTCPB
		,CPT_SAB.DWHCPTFLG
		,CPT_SAB.DWHCPTCPM
		,CPT_SAB.DWHCPTMVC
		,CPT_SAB.DWHCPTCOC
		,CPT_SAB.DWHCPTCOB
		,CPT_SAB.DWHCPTCLA
		,CPT_SAB.DWHCPTUTI
		,CPT_SAB.DWHCPTTXN
		,CPT_SAB.DWHCPTREV
		,CPT_SAB.DWHCPTSUR
		,CPT_SAB.DWHCPTFRE
		,CPT_SAB.DWHCPTRIS
		,CPT_SAB.FLG_COMP_PRDT
		,CPTBIS.CPTBISNAT
		,@DATE_EXEC as DAT_CRTN_ENRG
		,@DATE_EXEC as DAT_DERN_MODF_ENRG
		,CASE CPT_SAB.DTE_VALID WHEN @DATE_ALIM THEN 1 ELSE 0 END AS FLG_ENRG_COUR
FROM #RES_COMPTES CPT_SAB
LEFT JOIN [$(DatabaseInstanceName)].[$(DataHubDatabaseName)].[dbo].REF_PLAN_DE_COMPTES PC
		  ON PC.PLAN_DE_COMPTES_COMPTE_OBLIGATOIRE = CPT_SAB.DWHCPTRUB
LEFT JOIN [$(DatabaseInstanceName)].[$(DataHubDatabaseName)].[dbo].[PV_SA_Q_CPTBIS] CPTBIS
          ON CPT_SAB.DWHCPTCOM = CPTBIS.CPTBISCOM
LEFT JOIN [$(DatabaseInstanceName)].[$(DataHubDatabaseName)].[dbo].PV_FE_SBDCHKACCOUNT_V2 SBDCHKACCOUNT
          ON CAST(SBDCHKACCOUNT.ACCOUNT_NUMBER as varchar) = CPT_SAB.DWHCPTCOM AND
             CAST(SBDCHKACCOUNT.Validity_StartDate AS DATE) <= CPT_SAB.DTE_VALID AND
			 CAST(SBDCHKACCOUNT.Validity_EndDate AS DATE) > CPT_SAB.DTE_VALID

SELECT @nbRows = @@ROWCOUNT;

-- SUPPRESSION DES ENREGISTREMENTS > 90 JRS 
DELETE FROM [dbo].[DWH_EQUIPEMENT_COMPTE]
  WHERE (DATEDIFF(DAY,[DAT_OBSR],@DATE_ALIM) > 90 AND [DAT_OBSR] !=EOMONTH([DAT_OBSR]));

END
