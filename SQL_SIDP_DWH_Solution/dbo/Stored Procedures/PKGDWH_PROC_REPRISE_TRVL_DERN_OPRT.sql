﻿
CREATE  PROC [dbo].[PKGDWH_PROC_REPRISE_TRVL_DERN_OPRT] 
AS  
BEGIN 

--Pré Calcul de la dernière opération + libellé
 IF OBJECT_ID('tempdb..#RES_DERN_OPE_1') IS NOT NULL
BEGIN DROP TABLE #RES_DERN_OPE_1  END

select m.NUM_CPT
     , m.DTE_OPE as DT_DER_MVT 		
	 , m.COD_REF_OPE
     , Lead(DTE_OPE, 1, '9999-12-31') over (partition by NUM_CPT order by DTE_OPE) as NEXT_DTE_OPE -- à utiliser pour identifier la dernière opération des jours précédents	
	 , m.FLG_OPE_PIE 	
	 --,row_number() over (partition by m.NUM_CPT order by m.DTE_OPE desc) as FLG_DER_OP
  into #RES_DERN_OPE_1 		
  from ( select MO.NUM_CPT, MO.DTE_OPE, MO.NUM_PIE, 
                MO.COD_OPE, MO.DON_ANA, MO.COD_EVE, 
				MO.BIC_CTP,MO.NAT_OPE, MO.LIB_MVT_3,
				MO.COD_ANA,MO.COD_REF_OPE,
				REF.IDNT_TYP_OPRT,	
				row_number() over (partition by MO.NUM_CPT order by MO.NUM_PIE) as FLG_OPE_PIE -- flag pour identifier la dernière opération de chaque jours		
           from [$(DatabaseInstanceName)].[$(DataHubDatabaseName)].[dbo].[PV_SA_Q_MOUVEMENT_JOUR] MO	
           inner join [dbo].[TRVL_COMPTE_OB_GB] CPT ON MO.NUM_CPT = CPT.DWHCPTCOM
		   LEFT JOIN [dbo].[DWH_REF_TYPE_OPERATION] REF ON MO.COD_REF_OPE = REF.[IDNT_TYP_OPRT]
		   ) m
           where m.FLG_OPE_PIE = 1	

		  -- select count(*),count(distinct NUM_CPT ) from #RES_DERN_OPE_1

--Pré Calcul de la dernière opération par sous-type d'opération
IF OBJECT_ID('tempdb..#RES_DERN_OPE_2') IS NOT NULL
BEGIN DROP TABLE #RES_DERN_OPE_2  END

select MO.NUM_CPT
     , max(case when [COD_REF_OPE] = 1135                            then DTE_OPE else null end) as [DT_DER_UTI_PM]
     , max(case when [COD_REF_OPE] >= 1004 and [COD_REF_OPE] <= 1133 then DTE_OPE else null end) as [DT_DER_UTI_CB]
     , max(case when MTT_EUR > 0                                     then DTE_OPE else null end) as [DT_DER_OPE_DEB]
     , max(case when MTT_EUR < 0                                     then DTE_OPE else null end) as [DT_DER_OPE_CRE]
  into #RES_DERN_OPE_2
  from [$(DatabaseInstanceName)].[$(DataHubDatabaseName)].[dbo].[PV_SA_Q_MOUVEMENT_JOUR] MO
 inner join dbo.TRVL_COMPTE_OB_GB CPT ON MO.NUM_CPT = CPT.DWHCPTCOM
 group by MO.NUM_CPT 

 
 --Insertion dans la table TRVL
 IF OBJECT_ID('tempdb..#TRVL_OPERATION_DAT_DERN') IS NOT NULL
BEGIN DROP TABLE #TRVL_OPERATION_DAT_DERN  END

 SELECT D1.NUM_CPT,
        D1.DT_DER_MVT         as DAT_DERN_OPRT          ,
		D1.COD_REF_OPE        as IDNT_TYP_DERN_OPRT     ,
		D2.DT_DER_UTI_PM      as DAT_DERN_UTLS_PAIE_MOBL,
		D2.DT_DER_UTI_CB      as DAT_DERN_UTLS_PAIE_CB  ,
		D2.DT_DER_OPE_DEB     as DAT_DERN_OPRT_DEBT     ,
		D2.DT_DER_OPE_CRE     as DAT_DERN_OPRT_CRDT     ,
		getdate()  as DAT_CRTN_ENRG,
		getdate()  as DAT_DERN_MODF_ENRG
 INTO   #TRVL_OPERATION_DAT_DERN
 FROM   #RES_DERN_OPE_1 D1
 LEFT JOIN #RES_DERN_OPE_2 D2 ON D1.NUM_CPT = D2.NUM_CPT

/**/
--Merge

MERGE [dbo].[TRVL_OPERATION_DAT_DERN] AS TARGET
USING #TRVL_OPERATION_DAT_DERN AS SOURCE 
ON (TARGET.NUMR_COMP = SOURCE.NUM_CPT) 

WHEN MATCHED AND 

(TARGET.DAT_DERN_OPRT             != SOURCE.DAT_DERN_OPRT
Or TARGET.IDNT_TYP_DERN_OPRT      != SOURCE.IDNT_TYP_DERN_OPRT  
Or TARGET.DAT_DERN_UTLS_PAIE_MOBL != SOURCE.DAT_DERN_UTLS_PAIE_MOBL
Or TARGET.DAT_DERN_UTLS_PAIE_CB   != SOURCE.DAT_DERN_UTLS_PAIE_CB  
Or TARGET.DAT_DERN_OPRT_DEBT      != SOURCE.DAT_DERN_OPRT_DEBT     
Or TARGET.DAT_DERN_OPRT_CRDT      != SOURCE.DAT_DERN_OPRT_CRDT   )  

THEN

UPDATE 
SET TARGET.DAT_DERN_OPRT = SOURCE.DAT_DERN_OPRT, 
        
TARGET.IDNT_TYP_DERN_OPRT      = SOURCE.IDNT_TYP_DERN_OPRT  ,    
TARGET.DAT_DERN_UTLS_PAIE_MOBL = ISNULL(SOURCE.DAT_DERN_UTLS_PAIE_MOBL,	TARGET.DAT_DERN_UTLS_PAIE_MOBL ),
TARGET.DAT_DERN_UTLS_PAIE_CB   = ISNULL(SOURCE.DAT_DERN_UTLS_PAIE_CB  ,	TARGET.DAT_DERN_UTLS_PAIE_CB   ),
TARGET.DAT_DERN_OPRT_DEBT      = ISNULL(SOURCE.DAT_DERN_OPRT_DEBT     ,	TARGET.DAT_DERN_OPRT_DEBT      ),
TARGET.DAT_DERN_OPRT_CRDT      = ISNULL(SOURCE.DAT_DERN_OPRT_CRDT     ,	TARGET.DAT_DERN_OPRT_CRDT      ),
TARGET.DAT_DERN_MODF_ENRG	   = SOURCE.DAT_DERN_MODF_ENRG	   

WHEN NOT MATCHED BY TARGET THEN 
INSERT (
 NUMR_COMP
,DAT_DERN_OPRT          
,IDNT_TYP_DERN_OPRT     
,DAT_DERN_UTLS_PAIE_MOBL
,DAT_DERN_UTLS_PAIE_CB  
,DAT_DERN_OPRT_DEBT     
,DAT_DERN_OPRT_CRDT     
,DAT_CRTN_ENRG
,DAT_DERN_MODF_ENRG
) 
VALUES (
 SOURCE.NUM_CPT 
,SOURCE.DAT_DERN_OPRT          
,SOURCE.IDNT_TYP_DERN_OPRT     
,SOURCE.DAT_DERN_UTLS_PAIE_MOBL
,SOURCE.DAT_DERN_UTLS_PAIE_CB  
,SOURCE.DAT_DERN_OPRT_DEBT     
,SOURCE.DAT_DERN_OPRT_CRDT     
,SOURCE.DAT_CRTN_ENRG
,SOURCE.DAT_DERN_MODF_ENRG
);

END