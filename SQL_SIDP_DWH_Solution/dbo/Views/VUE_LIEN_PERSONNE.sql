﻿CREATE VIEW [dbo].[VUE_LIEN_PERSONNE]
AS SELECT 
	DAT_OBSR,
	NUMR_LIEN,
	REF_TYP_ENRG.COD_SF AS COD_TYP_ENRG,
	REF_TYP_ENRG.NOM_SF AS LIBL_TYP_ENRG,
	COD_NATR_LIEN,
	CASE COD_NATR_LIEN  
      WHEN '01' THEN 'Marié'
      WHEN '02' THEN 'Vie maritale'
      WHEN '03' THEN 'PACS'
      WHEN '04' THEN 'Représentant Légal'
      WHEN '05' THEN 'Parenté'
      WHEN '06' THEN 'Tiers'
      WHEN '07' THEN 'Gérant'
      WHEN '08' THEN 'Actionnaire'
      WHEN '09' THEN 'Mandataire'
      WHEN '10' THEN 'Autre'
      WHEN '11' THEN 'Doublon'
    END AS LIBL_NATR_LIEN,
	IDNT_SF_PERS1,
	NUMR_PERS1,
	COD_ROL_PERS1,
	CASE COD_ROL_PERS1
      WHEN '01' THEN 'Conjoint'
      WHEN '02' THEN 'Représentant'
      WHEN '03' THEN 'Représenté'
      WHEN '06' THEN 'Parent'
      WHEN '07' THEN 'Enfant'
      WHEN '08' THEN 'Colocataire'
      WHEN '09' THEN 'Parrain'
      WHEN '10' THEN 'Filleul'
      WHEN '11' THEN 'Entreprise'
      WHEN '12' THEN 'Gérant'
      WHEN '13' THEN 'Actionnaire'
      WHEN '14' THEN 'Mandataire'
      WHEN '15' THEN 'Client ancienne offre'
      WHEN '16' THEN 'Client nouvelle offre'
    END AS LIBL_ROL_PERS1,
	IDNT_SF_PERS2,	
	NUMR_PERS2,
	COD_ROL_PERS2,
	CASE COD_ROL_PERS2
      WHEN '01' THEN 'Conjoint'
      WHEN '02' THEN 'Représentant'
      WHEN '03' THEN 'Représenté'
      WHEN '06' THEN 'Parent'
      WHEN '07' THEN 'Enfant'
      WHEN '08' THEN 'Colocataire'
      WHEN '09' THEN 'Parrain'
      WHEN '10' THEN 'Filleul'
      WHEN '11' THEN 'Entreprise'
      WHEN '12' THEN 'Gérant'
      WHEN '13' THEN 'Actionnaire'
      WHEN '14' THEN 'Mandataire'
      WHEN '15' THEN 'Client ancienne offre'
      WHEN '16' THEN 'Client nouvelle offre'
    END AS LIBL_ROL_PERS2,
	DAT_CRTN_LIEN AS DAT_CRTN_LIEN_HMS,
	DAT_DERN_MODF_LIEN AS DAT_DERN_MODF_LIEN_HMS,
	DAT_EXPR,	
	CASE FLG_LIEN_ACTF WHEN 1 THEN 'Oui' WHEN 0 THEN 'Non' END as FLG_LIEN_ACTF,
	CASE FLG_LIEN_A_VERF WHEN 1 THEN 'Oui' WHEN 0 THEN 'Non' END as FLG_LIEN_A_VERF,
	FLG_ENRG_COUR
FROM  dbo.DWH_LIEN_PERSONNE LIEN_PERS
LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REF_TYPE_ENREGISTREMENT REF_TYP_ENRG
  ON LIEN_PERS.IDNT_SF_TYP_ENRG = REF_TYP_ENRG.IDNT_SF
  AND REF_TYP_ENRG.TYP_OBJT_SF = 'AccountLink__c'
  AND LIEN_PERS.DAT_OBSR >= REF_TYP_ENRG.DAT_DEBT_VALD 
  AND LIEN_PERS.DAT_OBSR < REF_TYP_ENRG.DAT_FIN_VALD
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Observation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PERSONNE', @level2type=N'COLUMN',@level2name=N'DAT_OBSR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numéro du lien' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PERSONNE', @level2type=N'COLUMN',@level2name=N'NUMR_LIEN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Type Enregistrement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PERSONNE', @level2type=N'COLUMN',@level2name=N'COD_TYP_ENRG'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé Type Enregistrement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PERSONNE', @level2type=N'COLUMN',@level2name=N'LIBL_TYP_ENRG'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Nature Lien' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PERSONNE', @level2type=N'COLUMN',@level2name=N'COD_NATR_LIEN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé Nature Lien' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PERSONNE', @level2type=N'COLUMN',@level2name=N'LIBL_NATR_LIEN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant SF de la personne 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PERSONNE', @level2type=N'COLUMN',@level2name=N'IDNT_SF_PERS1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numéro Personne 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PERSONNE', @level2type=N'COLUMN',@level2name=N'NUMR_PERS1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Rôle Personne 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PERSONNE', @level2type=N'COLUMN',@level2name=N'COD_ROL_PERS1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé Rôle Personne 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PERSONNE', @level2type=N'COLUMN',@level2name=N'LIBL_ROL_PERS1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant SF de la personne 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PERSONNE', @level2type=N'COLUMN',@level2name=N'IDNT_SF_PERS2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numéro Personne 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PERSONNE', @level2type=N'COLUMN',@level2name=N'NUMR_PERS2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Rôle Personne 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PERSONNE', @level2type=N'COLUMN',@level2name=N'COD_ROL_PERS2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé Rôle Personne 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PERSONNE', @level2type=N'COLUMN',@level2name=N'LIBL_ROL_PERS2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date de création du lien' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PERSONNE', @level2type=N'COLUMN',@level2name=N'DAT_CRTN_LIEN_HMS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date de dernière modification du lien' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PERSONNE', @level2type=N'COLUMN',@level2name=N'DAT_DERN_MODF_LIEN_HMS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date d''expiration' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PERSONNE', @level2type=N'COLUMN',@level2name=N'DAT_EXPR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Lien Actif' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PERSONNE', @level2type=N'COLUMN',@level2name=N'FLG_LIEN_ACTF'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Lien à vérifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PERSONNE', @level2type=N'COLUMN',@level2name=N'FLG_LIEN_A_VERF'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Enregistrement Courant' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PERSONNE', @level2type=N'COLUMN',@level2name=N'FLG_ENRG_COUR'
GO