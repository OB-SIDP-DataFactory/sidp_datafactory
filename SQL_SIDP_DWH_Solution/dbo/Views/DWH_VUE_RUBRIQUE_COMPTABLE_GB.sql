﻿CREATE VIEW [dbo].[DWH_VUE_RUBRIQUE_COMPTABLE_GB] AS 
( 
SELECT DISTINCT [code_rubrique_comptable_SAB] AS [COD_RUBR_COMP] 
FROM [$(DatabaseInstanceName)].[$(DataHubDatabaseName)].[dbo].[REF_PRODUIT]	WITH(NOLOCK)
 WHERE code_regroupement_SIDP IN ('CV','EL','EP') AND [code_rubrique_comptable_SAB] NOT IN ('251180','291228','254181','254181','254111'))
GO