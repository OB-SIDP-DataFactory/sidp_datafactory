﻿


CREATE VIEW [dbo].[VUE_EQUIPEMENT_SL] AS
SELECT  DAT_OBSR,
        IDNT_EQPM,
		NUMR_EQPM,
        DISC,
        IDNT_DOSS_SOUS,
        IDNT_PRDT,
        COD_PRDT,
        LIBL_PRDT,
		CAST(DAT_CRTN AS DATE) AS DAT_CRTN,
        DAT_CRTN AS DAT_CRTN_HMS,
		CAST(DAT_DERN_MIS_JOUR AS DATE) AS DAT_DERN_MIS_JOUR,
        DAT_DERN_MIS_JOUR AS DAT_DERN_MIS_JOUR_HMS,
		IDNT_CRTN,
		IDNT_DERN_MODF,
		TYP_UTLS,
		CL_PFM,
		IDNT_RES_DIST,
		DistributionNetwork.CODE AS LIBL_RES_DIST,
		IDNT_MARQ,
		Brand.CODE AS LIBL_MARQ,
        CASE WHEN FLG_SECR_BANC_OPT_IN = 1 THEN 'Oui'
        	 WHEN FLG_SECR_BANC_OPT_IN = 0 THEN 'Non'
        END AS FLG_SECR_BANC_OPT_IN,
        CASE WHEN FLG_VERF_IDNT_VEND = 1 THEN 'Oui'
        	 WHEN FLG_VERF_IDNT_VEND = 0 THEN 'Non'
        END AS FLG_VERF_IDNT_VEND,		
	    FLG_ENRG_COUR
FROM dbo.DWH_EQUIPEMENT_SL equi
LEFT JOIN [$(DatabaseInstanceName)].[$(DataHubDatabaseName)].[dbo].[REF_CODE] DistributionNetwork on equi.IDNT_RES_DIST = DistributionNetwork.CODE_ID and DistributionNetwork.REF_FAMILY_ID = 6
LEFT JOIN [$(DatabaseInstanceName)].[$(DataHubDatabaseName)].[dbo].[REF_CODE] Brand on equi.IDNT_MARQ = Brand.CODE_ID and Brand.REF_FAMILY_ID = 91
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Discriminant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'DISC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numero equipement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'NUMR_EQPM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé produit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'LIBL_PRDT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code produit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'COD_PRDT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant produit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'IDNT_PRDT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant dossier souscription', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'IDNT_DOSS_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant equipement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'IDNT_EQPM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Enregistrement Courant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere mise a jour Heure Minute Seconde', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MIS_JOUR_HMS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date creation Heure Minute Seconde', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_HMS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere mise a jour', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MIS_JOUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'DAT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type Utilisateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'TYP_UTLS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Réseau de distribution', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'IDNT_RES_DIST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Marque', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'IDNT_MARQ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifant Dernière modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'IDNT_DERN_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'IDNT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Vérification Identification Vendeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'FLG_VERF_IDNT_VEND';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Secret bancaire Opt In', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'FLG_SECR_BANC_OPT_IN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Clé PFM', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'CL_PFM';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Réseau de distribution', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'LIBL_RES_DIST';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Marque', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_SL', @level2type = N'COLUMN', @level2name = N'LIBL_MARQ';

