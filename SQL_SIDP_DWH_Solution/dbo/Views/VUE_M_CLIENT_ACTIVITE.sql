﻿
CREATE VIEW [dbo].[VUE_M_CLIENT_ACTIVITE] as
SELECT
	[DAT_OBSR],
	[NUMR_CLNT_SAB],
	[ACTV_CAV],
	[ACTV_CSL],
	[ACTV_GLBL]
FROM dbo.DWH_M_CLIENT_ACTIVITE
;
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Activité Globale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_M_CLIENT_ACTIVITE', @level2type = N'COLUMN', @level2name = N'ACTV_GLBL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Activité CSL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_M_CLIENT_ACTIVITE', @level2type = N'COLUMN', @level2name = N'ACTV_CSL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Activité CAV', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_M_CLIENT_ACTIVITE', @level2type = N'COLUMN', @level2name = N'ACTV_CAV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Client SAB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_M_CLIENT_ACTIVITE', @level2type = N'COLUMN', @level2name = N'NUMR_CLNT_SAB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_M_CLIENT_ACTIVITE', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';

