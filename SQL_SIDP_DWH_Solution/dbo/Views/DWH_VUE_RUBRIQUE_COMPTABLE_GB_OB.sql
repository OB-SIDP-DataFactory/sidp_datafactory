﻿CREATE VIEW [dbo].[DWH_VUE_RUBRIQUE_COMPTABLE_GB_OB]
AS 
SELECT DISTINCT
	[code_rubrique_comptable_SAB] AS [COD_RUBR_COMP]
FROM
	[$(DatabaseInstanceName)].[$(DataHubDatabaseName)].[dbo].[REF_PRODUIT]	WITH(NOLOCK)
WHERE
	1 = 1
	AND	code_regroupement_SIDP IN ('CV','EL','EP')
GO