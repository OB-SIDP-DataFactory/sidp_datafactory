﻿



CREATE VIEW [dbo].[VUE_DOSSIER_SOUSCRIPTION] AS 
SELECT [DAT_OBSR]
      ,[IDNT_DOSS_SOUS]
      ,[NUMR_OPPR]
      ,[IDNT_OPPR]
      ,[IDNT_OFFR_COMM]
	  ,[COD_OFFR_COMM]
	  ,[LIBL_OFFR_COMM]
      ,[IDNT_SOUS_STTT]
	  ,EnrolmentFolderSubStatus.CODE as LIBL_SOUS_STTT
      ,[IDNT_SUSP_CONT_STTT]
	  ,BankingSuspensionControlStatus.CODE as LIBL_SUSP_CONT_STTT
      ,[IDNT_PRSP]
      ,[IDNT_DOSS_JUST]
      ,[IDNT_RELT_PERS]
	  ,PersonRelationship.CODE as LIBL_RELT_PERS
	  ,CAST([DAT_CRTN] AS DATE) AS DAT_CRTN
      ,[DAT_CRTN] AS DAT_CRTN_HMS
      ,[IDNT_CRTN]
	  ,CAST([DAT_DERN_MODF] AS DATE) AS DAT_DERN_MODF
      ,[DAT_DERN_MODF] AS DAT_DERN_MODF_HMS
      ,[IDNT_DERN_MODF]
      ,[TYP_UTLS]
      ,[ALS]
      ,[ALS_FACT_RES_DIST]
      ,[DAT_FIN]
      ,CASE WHEN FLG_ENV_EML_PRMR_DEPT=1 THEN 'Oui' 
			WHEN FLG_ENV_EML_PRMR_DEPT=0 THEN 'Non'
		END AS FLG_ENV_EML_PRMR_DEPT
      ,CASE WHEN FLG_DOSS_COMP=1 THEN 'Oui' 
			WHEN FLG_DOSS_COMP=0 THEN 'Non'
		END AS FLG_DOSS_COMP
      ,[DAT_ACCP_GC]
      ,CASE WHEN FLG_VERF_IDNT_VEND=1 THEN 'Oui' 
			WHEN FLG_VERF_IDNT_VEND=0 THEN 'Non'
		END AS FLG_VERF_IDNT_VEND
      ,[DAT_VERF_IDNT]
      ,[SCR_COMP_APPL]
      ,[COD_ERRR_COR_BANK]
      ,[MESS_ERRR_COR_BANK]
	  ,[PRMR_DEPT_TRNS_IBAN]
      ,CASE WHEN FLG_CONT_ENV=1 THEN 'Oui' 
			WHEN FLG_CONT_ENV=0 THEN 'Non'
		END AS FLG_CONT_ENV
      ,CASE WHEN FLG_DOCM_PRCN_ENV=1 THEN 'Oui' 
			WHEN FLG_DOCM_PRCN_ENV=0 THEN 'Non'
		END AS FLG_DOCM_PRCN_ENV
      ,CASE WHEN FLG_CONT_RIO=1 THEN 'Oui' 
			WHEN FLG_CONT_RIO=0 THEN 'Non'
		END AS FLG_CONT_RIO
      ,CASE WHEN FLG_CONS_VIE=1 THEN 'Oui' 
			WHEN FLG_CONS_VIE=0 THEN 'Non'
		END AS FLG_CONS_VIE
	  ,FLG_ENRG_COUR
  FROM [dbo].[DWH_DOSSIER_SOUSCRIPTION] ds
  LEFT JOIN [$(DatabaseInstanceName)].[$(DataHubDatabaseName)].[dbo].[REF_CODE] EnrolmentFolderSubStatus on  EnrolmentFolderSubStatus.REF_FAMILY_ID = 46 and ds.IDNT_SOUS_STTT = EnrolmentFolderSubStatus.CODE_ID 
  LEFT JOIN [$(DatabaseInstanceName)].[$(DataHubDatabaseName)].[dbo].[REF_CODE] BankingSuspensionControlStatus on  BankingSuspensionControlStatus.REF_FAMILY_ID = 20 and BankingSuspensionControlStatus.CODE_ID = ds.IDNT_SUSP_CONT_STTT 
  LEFT JOIN [$(DatabaseInstanceName)].[$(DataHubDatabaseName)].[dbo].[REF_CODE] PersonRelationship on PersonRelationship.REF_FAMILY_ID = 85 and PersonRelationship.CODE_ID = ds.IDNT_RELT_PERS  
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag document précontractuel envoyé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'FLG_DOCM_PRCN_ENV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag contrat envoyé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'FLG_CONT_ENV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Message erreur core banking', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'MESS_ERRR_COR_BANK';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code erreur core banking', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'COD_ERRR_COR_BANK';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Score compteur appel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'SCR_COMP_APPL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date vérification identité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'DAT_VERF_IDNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date acceptation GC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'DAT_ACCP_GC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag dossier complet', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'FLG_DOSS_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag envoie email premier dépôt', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'FLG_ENV_EML_PRMR_DEPT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date fin', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'DAT_FIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Alias facture réseau distribution', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'ALS_FACT_RES_DIST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Alias', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'ALS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type utilisateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'TYP_UTLS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant dernière modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'IDNT_DERN_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'IDNT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant relation personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'IDNT_RELT_PERS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant dossier justificatif ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'IDNT_DOSS_JUST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant prospect', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'IDNT_PRSP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant suspension contrôle statut', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'IDNT_SUSP_CONT_STTT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant sous statut', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'IDNT_SOUS_STTT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant offre commercial', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'IDNT_OFFR_COMM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant opportunité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'IDNT_OPPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro opportunité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'NUMR_OPPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant dossier souscription', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'IDNT_DOSS_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé offre commercial', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'LIBL_OFFR_COMM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code offre commercial', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'COD_OFFR_COMM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Enregistrement Courant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date dernière modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_HMS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_HMS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag vérification identification vendeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'FLG_VERF_IDNT_VEND';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'DAT_CRTN';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libéllé suspension contrôle statut', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'LIBL_SUSP_CONT_STTT';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé sous statut', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'LIBL_SOUS_STTT';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé relation personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_DOSSIER_SOUSCRIPTION', @level2type = N'COLUMN', @level2name = N'LIBL_RELT_PERS';

