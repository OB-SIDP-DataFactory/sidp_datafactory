CREATE VIEW [dbo].[VUE_PRDT_SOUS_CREDIT_CONSO] AS
SELECT DAT_OBSR
      ,IDNT_PRDT_SOUS_CRDT_CONS
	  ,IDNT_COUL_TAUX_ENDT
	  ,ref_coul_taux_endt.CODE AS LIBL_COUL_TAUX_ENDT
	  --,ref_coul_taux_endt.MESSAGE AS LIBL_COUL_TAUX_ENDT
	  ,TAUX_ENDT
	  ,IDNT_TYP_COMP_PRLV_AUTM
	  ,ref_typ_comp_prlv_autm.CODE AS LIBL_TYP_COMP_PRLV_AUTM
	  --,ref_typ_comp_prlv_autm.MESSAGE AS LIBL_TYP_COMP_PRLV_AUTM
	  ,IDNT_COUL_SCR_FI
	  ,ref_coul_scr_fi.CODE AS LIBL_COUL_SCR_FI
	  --,ref_coul_scr_fi.MESSAGE AS LIBL_COUL_SCR_FI
	  ,IDNT_COUL_SCR_CRDT
	  ,ref_coul_scr_crdt.CODE AS LIBL_COUL_SCR_CRDT
	  --,ref_coul_scr_crdt.MESSAGE AS LIBL_COUL_SCR_CRDT
	  ,IDNT_PRLV_AUTM_PAYS
	  ,Country.CODE AS COD_PRLV_AUTM_PAYS
      ,NUMR_COMP
      ,NUMR_APPL_FI
      ,NUMR_CONT_FI
      ,NUMR_MAND_SEP
      ,DAT_FIN_PERD_RETR
      ,DAT_FIN_DEBR_PRT
      ,DAT_DEBT_DEBR_PRT
      ,CASE WHEN DRT_RETR_RENN=1 THEN 'Oui'
			WHEN DRT_RETR_RENN=0 THEN 'Non'
		END AS DRT_RETR_RENN
      ,DAT_ACCP_ENGG_CFRM
      ,DAT_APPR
      ,VALR_SCR_CRDT
      ,CASE WHEN REFS_EMPR_COUV_ASSR=1 THEN 'Oui'
			WHEN REFS_EMPR_COUV_ASSR=0 THEN 'Non'
		END AS REFS_EMPR_COUV_ASSR
      ,CASE WHEN REFS_CO_EMPR_COUV_ASSR=1 THEN 'Oui'
			WHEN REFS_CO_EMPR_COUV_ASSR=0 THEN 'Non'
		END AS REFS_CO_EMPR_COUV_ASSR
      ,PRLV_AUTM_CL_CONT
      ,PRLV_AUTM_BBN
	  ,MONT_EXCP_EMPR
	  ,NUMR_MAND_SEP_RES_DIST
	  ,FLG_ENRG_COUR
 FROM dbo.DWH_PRDT_SOUS_CREDIT_CONSO pdt
 LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REF_CONSUMER_LOAN_SCORE_COLOR ref_coul_taux_endt
 ON pdt.IDNT_COUL_TAUX_ENDT = ref_coul_taux_endt.SCORE_ID AND ref_coul_taux_endt.DAT_DEBT_VALD <= pdt.DAT_OBSR AND pdt.DAT_OBSR < ref_coul_taux_endt.DAT_FIN_VALD
 LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REF_CHECKING_ACCOUNT_TYPE ref_typ_comp_prlv_autm 
 ON pdt.IDNT_TYP_COMP_PRLV_AUTM = ref_typ_comp_prlv_autm.ACCOUNT_TYPE_ID AND ref_typ_comp_prlv_autm.DAT_DEBT_VALD <= pdt.DAT_OBSR AND pdt.DAT_OBSR < ref_typ_comp_prlv_autm.DAT_FIN_VALD
 LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REF_CONSUMER_LOAN_SCORE_COLOR ref_coul_scr_fi
 ON pdt.IDNT_COUL_SCR_FI = ref_coul_scr_fi.SCORE_ID AND ref_coul_scr_fi.DAT_DEBT_VALD <= pdt.DAT_OBSR AND pdt.DAT_OBSR < ref_coul_scr_fi.DAT_FIN_VALD
 LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REF_CONSUMER_LOAN_SCORE_COLOR ref_coul_scr_crdt
 ON pdt.IDNT_COUL_SCR_CRDT = ref_coul_scr_crdt.SCORE_ID AND ref_coul_scr_crdt.DAT_DEBT_VALD <= pdt.DAT_OBSR AND pdt.DAT_OBSR < ref_coul_scr_crdt.DAT_FIN_VALD
 LEFT JOIN [$(DatabaseInstanceName)].[$(DataHubDatabaseName)].dbo.REF_CODE Country
 ON pdt.IDNT_PRLV_AUTM_PAYS = Country.CODE_ID AND Country.REF_FAMILY_ID = 1  
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Observation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_PRDT_SOUS_CREDIT_CONSO', @level2type=N'COLUMN',@level2name=N'DAT_OBSR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant produit souscrit credit consommation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_PRDT_SOUS_CREDIT_CONSO', @level2type=N'COLUMN',@level2name=N'IDNT_PRDT_SOUS_CRDT_CONS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant couleur du taux endettement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_PRDT_SOUS_CREDIT_CONSO', @level2type=N'COLUMN',@level2name=N'IDNT_COUL_TAUX_ENDT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé couleur du taux endettement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_PRDT_SOUS_CREDIT_CONSO', @level2type=N'COLUMN',@level2name=N'LIBL_COUL_TAUX_ENDT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Taux endettement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_PRDT_SOUS_CREDIT_CONSO', @level2type=N'COLUMN',@level2name=N'TAUX_ENDT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant type de compte prelevement automatique' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_PRDT_SOUS_CREDIT_CONSO', @level2type=N'COLUMN',@level2name=N'IDNT_TYP_COMP_PRLV_AUTM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé type de compte prelevement automatique' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_PRDT_SOUS_CREDIT_CONSO', @level2type=N'COLUMN',@level2name=N'LIBL_TYP_COMP_PRLV_AUTM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant couleur score FI' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_PRDT_SOUS_CREDIT_CONSO', @level2type=N'COLUMN',@level2name=N'IDNT_COUL_SCR_FI'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé couleur score FI' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_PRDT_SOUS_CREDIT_CONSO', @level2type=N'COLUMN',@level2name=N'LIBL_COUL_SCR_FI'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant couleur score credit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_PRDT_SOUS_CREDIT_CONSO', @level2type=N'COLUMN',@level2name=N'IDNT_COUL_SCR_CRDT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé couleur score credit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_PRDT_SOUS_CREDIT_CONSO', @level2type=N'COLUMN',@level2name=N'LIBL_COUL_SCR_CRDT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant prélèvement automatique pays' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_PRDT_SOUS_CREDIT_CONSO', @level2type=N'COLUMN',@level2name=N'IDNT_PRLV_AUTM_PAYS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numero compte' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_PRDT_SOUS_CREDIT_CONSO', @level2type=N'COLUMN',@level2name=N'NUMR_COMP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numero application FI' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_PRDT_SOUS_CREDIT_CONSO', @level2type=N'COLUMN',@level2name=N'NUMR_APPL_FI'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numero de contrat FI' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_PRDT_SOUS_CREDIT_CONSO', @level2type=N'COLUMN',@level2name=N'NUMR_CONT_FI'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numero de mandat SEPA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_PRDT_SOUS_CREDIT_CONSO', @level2type=N'COLUMN',@level2name=N'NUMR_MAND_SEP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date de fin de periode de retractation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_PRDT_SOUS_CREDIT_CONSO', @level2type=N'COLUMN',@level2name=N'DAT_FIN_PERD_RETR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date de fin deboursement de pret' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_PRDT_SOUS_CREDIT_CONSO', @level2type=N'COLUMN',@level2name=N'DAT_FIN_DEBR_PRT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date de debut deboursement de pret' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_PRDT_SOUS_CREDIT_CONSO', @level2type=N'COLUMN',@level2name=N'DAT_DEBT_DEBR_PRT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Droit de retractation renonce' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_PRDT_SOUS_CREDIT_CONSO', @level2type=N'COLUMN',@level2name=N'DRT_RETR_RENN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date acceptation de engagement conformite' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_PRDT_SOUS_CREDIT_CONSO', @level2type=N'COLUMN',@level2name=N'DAT_ACCP_ENGG_CFRM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date approbation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_PRDT_SOUS_CREDIT_CONSO', @level2type=N'COLUMN',@level2name=N'DAT_APPR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valeur score credit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_PRDT_SOUS_CREDIT_CONSO', @level2type=N'COLUMN',@level2name=N'VALR_SCR_CRDT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Refus emprunteur de la couverture assurance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_PRDT_SOUS_CREDIT_CONSO', @level2type=N'COLUMN',@level2name=N'REFS_EMPR_COUV_ASSR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Refus du co emprunteur de la couverture assurance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_PRDT_SOUS_CREDIT_CONSO', @level2type=N'COLUMN',@level2name=N'REFS_CO_EMPR_COUV_ASSR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Prelevement automatique cle de controle' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_PRDT_SOUS_CREDIT_CONSO', @level2type=N'COLUMN',@level2name=N'PRLV_AUTM_CL_CONT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Prelevement automatique BBAN' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_PRDT_SOUS_CREDIT_CONSO', @level2type=N'COLUMN',@level2name=N'PRLV_AUTM_BBN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Montant exceptionnel emprunteur' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_PRDT_SOUS_CREDIT_CONSO', @level2type=N'COLUMN',@level2name=N'MONT_EXCP_EMPR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numero de mandat SEPA du réseau de distribution' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_PRDT_SOUS_CREDIT_CONSO', @level2type=N'COLUMN',@level2name=N'NUMR_MAND_SEP_RES_DIST'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Enregistrement Courant' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_PRDT_SOUS_CREDIT_CONSO', @level2type=N'COLUMN',@level2name=N'FLG_ENRG_COUR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code prélèvement automatique pays', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PRDT_SOUS_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'COD_PRLV_AUTM_PAYS';

