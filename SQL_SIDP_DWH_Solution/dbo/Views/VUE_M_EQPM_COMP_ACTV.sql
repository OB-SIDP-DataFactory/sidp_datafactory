﻿CREATE VIEW dbo.VUE_M_EQPM_COMP_ACTV as
SELECT 
	DAT_OBSR,
	NUMR_COMP,
	COD_RUBR,
	COD_ACTV_COMP,
	LIBL_ACTV_COMP
FROM dbo.DWH_M_EQPM_COMP_ACTV
;
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Activité Compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_M_EQPM_COMP_ACTV', @level2type = N'COLUMN', @level2name = N'LIBL_ACTV_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Activité Compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_M_EQPM_COMP_ACTV', @level2type = N'COLUMN', @level2name = N'COD_ACTV_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Rubrique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_M_EQPM_COMP_ACTV', @level2type = N'COLUMN', @level2name = N'COD_RUBR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_M_EQPM_COMP_ACTV', @level2type = N'COLUMN', @level2name = N'NUMR_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_M_EQPM_COMP_ACTV', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';

