﻿
CREATE VIEW [dbo].[VUE_SIMULATION_CREDIT_CONSO] AS 
SELECT DAT_OBSR
	  ,IDNT_SIML_CRDT_CONS
	  ,IDNT_PRDT_SOUS_CRDT_CONS
	  ,IDNT_TYP_CONT_EMPR
	  ,ref_typ_cont_empr.CODE AS LIBL_TYP_CONT_EMPR
	  ,IDNT_TYP_CONT_CO_EMPR
	  ,ref_typ_cont_co_empr.CODE AS LIBL_TYP_CONT_CO_EMPR
	  ,IDNT_TYP_FINN
	  ,ref_typ_finn.CODE AS LIBL_TYP_FINN
	  ,IDNT_STTT_PLN_FINN
	  ,ref_sttt_pln_finn.CODE AS LIBL_STTT_PLN_FINN
	  ,CAST(DAT_CRTN AS date) AS DAT_CRTN
      ,DAT_CRTN AS DAT_CRTN_HMS
	  ,IDNT_CRTN
	  ,MONT_PRT
	  ,DUR_PRT_MOIS
	  ,TAUX_NOMN_PRT_CLNT
	  ,DAT_FIN_VALD_TAUX
	  ,TAUX_TAEA_PRT_RECM
	  ,TAUX_ANNL_EFFC_ASSR
	  ,TAUX_ANNL_EFFC_GLBL
	  ,TAUX_ANNL_EFFC_GLBL_PRV
	  ,TAUX_DEDC_PRT
	  ,COD_TAUX_DEDC_PRT
	  ,TAUX_NOMN_PRT_PRV
	  ,TAUX_NOMN_PRT_TOTL
	  ,MONT_TOTL_INTR
	  ,VERS_MENS_AVC_ASSR
	  ,VERS_MENS_SANS_ASSR
	  ,MONT_PRMR_VERS
	  ,VERS_PEND_PERD_DIFF
	  ,TOTL_PERD_DIFF_MOIS
	  ,REVN_MENS_NET_EMPR
	  ,REVN_MENS_NET_CO_EMPR
	  ,TYP_UTLS
	  ,LIST_GARN_ASSR_EMPR
	  ,LIST_GARN_ASSR_CO_EMPR
	  ,TAUX_RISQ_ASSR_1_SOUS
	  ,TAUX_RISQ_ASSR_1_CO_SOUS
	  ,TAUX_RISQ_ASSR_2_SOUS
	  ,TAUX_RISQ_ASSR_2_CO_SOUS
	  ,TYP_RISQ_ASSR_1_SOUS
	  ,TYP_RISQ_ASSR_1_CO_SOUS
	  ,TYP_RISQ_ASSR_2_SOUS
	  ,TYP_RISQ_ASSR_2_CO_SOUS
	  ,PRM_ASSR_EMPR
	  ,PRM_ASSR_CO_EMPR
	  ,PRM_ASSR_RECM
	  ,PRM_ASSR
	  ,COUT_TOTL_ASSR
	  ,FRS_INSC
	  ,DEPT_PRT_CART
	  ,DEPT_PRT_AUTR
	  ,CAST(DAT_DERN_MODF as date) AS DAT_DERN_MODF
      ,DAT_DERN_MODF AS DAT_DERN_MODF_HMS
	  ,IDNT_DERN_MODF
	  ,FLG_ENRG_COUR
  FROM dbo.DWH_SIMULATION_CREDIT_CONSO sim
  LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REF_EMPLOYMENT_CONTRACT ref_typ_cont_empr
  ON sim.IDNT_TYP_CONT_EMPR = ref_typ_cont_empr.EMPLOYMENT_CONTRACT_ID AND ref_typ_cont_empr.DAT_DEBT_VALD <= sim.DAT_OBSR AND sim.DAT_OBSR < ref_typ_cont_empr.DAT_FIN_VALD
  LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REF_EMPLOYMENT_CONTRACT ref_typ_cont_co_empr
  ON sim.IDNT_TYP_CONT_CO_EMPR = ref_typ_cont_co_empr.EMPLOYMENT_CONTRACT_ID AND ref_typ_cont_co_empr.DAT_DEBT_VALD <= sim.DAT_OBSR AND sim.DAT_OBSR < ref_typ_cont_co_empr.DAT_FIN_VALD
  LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REF_CONSU_LOAN_FIN_PLAN_STATUS ref_sttt_pln_finn
  ON sim.IDNT_STTT_PLN_FINN = ref_sttt_pln_finn.STATUS_ID AND ref_sttt_pln_finn.DAT_DEBT_VALD <= sim.DAT_OBSR AND sim.DAT_OBSR < ref_sttt_pln_finn.DAT_FIN_VALD
  LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REF_FINANCING_TYPE ref_typ_finn
  ON sim.IDNT_TYP_FINN = ref_typ_finn.FINANCING_TYPE_ID AND ref_typ_finn.DAT_DEBT_VALD <= sim.DAT_OBSR AND sim.DAT_OBSR < ref_typ_finn.DAT_FIN_VALD
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Enregistrement Courant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type utilisateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TYP_UTLS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Total periode differee en mois', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TOTL_PERD_DIFF_MOIS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifant derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'IDNT_DERN_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Versement pendant periode differee', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'VERS_PEND_PERD_DIFF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant du premier versement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_PRMR_VERS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Versement mensuel sans assurance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'VERS_MENS_SANS_ASSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Versement mensuel avec assurance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'VERS_MENS_AVC_ASSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant total de l''interet', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_TOTL_INTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux annuel effectif global', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TAUX_ANNL_EFFC_GLBL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux annuel effectif d''assurance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TAUX_ANNL_EFFC_ASSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux TAEA de prêt recommandé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TAUX_TAEA_PRT_RECM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Frais inscription', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'FRS_INSC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cout total de l''assurance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'COUT_TOTL_ASSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prime d''assurance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'PRM_ASSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prime d''assurance recommandee', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'PRM_ASSR_RECM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prime d''assurance co emprunteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'PRM_ASSR_CO_EMPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prime d''assurance emprunteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'PRM_ASSR_EMPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type de risque d''assurance 2 co souscripteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TYP_RISQ_ASSR_2_CO_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type de risque d''assurance 2 souscripteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TYP_RISQ_ASSR_2_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type de risque d''assurance 1 co souscripteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TYP_RISQ_ASSR_1_CO_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type de risque d''assurance 1 souscripteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TYP_RISQ_ASSR_1_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux de risque d''assurance 2 co souscripteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TAUX_RISQ_ASSR_2_CO_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux de risque d''assurance 2 souscripteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TAUX_RISQ_ASSR_2_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux de risque d''assurance 1 co souscripteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TAUX_RISQ_ASSR_1_CO_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux de risque d''assurance 1 souscripteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TAUX_RISQ_ASSR_1_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Liste de garantie d''assurance co emprunteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'LIST_GARN_ASSR_CO_EMPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Liste de garantie d''assurance emprunteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'LIST_GARN_ASSR_EMPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Revenu mensuel net du co emprunteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'REVN_MENS_NET_CO_EMPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Revenu mensuel net de l''emprunteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'REVN_MENS_NET_EMPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de fin de validite du taux', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_FIN_VALD_TAUX';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux nominal du pret', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TAUX_NOMN_PRT_CLNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Duree du pret en mois', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DUR_PRT_MOIS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant du pret', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_PRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'IDNT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé type de financement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'LIBL_TYP_FINN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifant type de financement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'IDNT_TYP_FINN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé statut plan de financement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'LIBL_STTT_PLN_FINN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant statut plan de financement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'IDNT_STTT_PLN_FINN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé type de contrat coemprunteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'LIBL_TYP_CONT_CO_EMPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant type de contrat coemprunteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'IDNT_TYP_CONT_CO_EMPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé type de contrat emprunteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'LIBL_TYP_CONT_EMPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant type de contrat emprunteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'IDNT_TYP_CONT_EMPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant produit souscrit credit consommation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'IDNT_PRDT_SOUS_CRDT_CONS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant simulation credit consommation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'IDNT_SIML_CRDT_CONS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux nominal du prêt total', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TAUX_NOMN_PRT_TOTL';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux nominal du prêt prev', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TAUX_NOMN_PRT_PRV';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux déduction du prêt', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TAUX_DEDC_PRT';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux annuel effectif global prev', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TAUX_ANNL_EFFC_GLBL_PRV';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dépôt du prêt par carte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DEPT_PRT_CART';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dépôt du prêt autre', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DEPT_PRT_AUTR';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code taux déduction du prêt', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SIMULATION_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'COD_TAUX_DEDC_PRT';

