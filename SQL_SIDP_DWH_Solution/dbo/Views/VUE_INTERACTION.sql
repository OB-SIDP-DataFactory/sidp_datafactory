﻿

CREATE VIEW [dbo].[VUE_INTERACTION] AS
SELECT INTR.DAT_OBSR,
       INTR.IDNT_INTR,
       INTR.NUMR_TACH,
       INTR.NUMR_INTR,
       INTR.IDNT_SF_PERS,
       INTR.NUMR_PERS_SF,
       INTR.NUMR_CLNT_SAB,
       INTR.IDNT_TYP_ENRG,
       INTR.OBJT,
       INTR.TYP,
       INTR.SOUS_TYP_TACH,
       INTR.FAML as COD_FAML,	   
	   REF_FAML.LIBL_SF as LIBL_FAML,	   
       INTR.SOUS_FAML as COD_SOUS_FAML,	   
	   REF_SOUS_FAML.LIBL_SF as LIBL_SOUS_FAML,	   
       INTR.STTT as COD_STTT,	   
	   REF_STTT.LIBL_SF as LIBL_STTT,	   
       INTR.PRRT as COD_PRRT,	   
	   REF_PRRT.LIBL_SF as LIBL_PRRT,	   
       INTR.[DESC],
       CASE WHEN INTR.FLG_PRRT_ELV = 1 THEN 'Oui'
            WHEN INTR.FLG_PRRT_ELV = 0 THEN 'Non'
       END AS FLG_PRRT_ELV,	   	   
       INTR.IDNT_ASSC_A,
       INTR.IDNT_NOM,
       INTR.NUMR_CONV_IBM,
       INTR.IDNT_PRPR,
       INTR.DAT_CRTN as DAT_CRTN_HMS,
	   CAST(INTR.DAT_CRTN AS DATE) AS DAT_CRTN,	   
       INTR.IDNT_CRTN,
       INTR.NOMB_MOIS_DEPS_CRTN,
       INTR.DAT_INTR,
       INTR.NOT_INTR,
       INTR.DAT_ECHN,
       INTR.DAT_TACH,
       CASE WHEN INTR.FLG_FERM = 1 THEN 'Oui'
            WHEN INTR.FLG_FERM = 0 THEN 'Non'
       END AS FLG_FERM,  
       INTR.TYP_APPL,
       INTR.TYP_APPL_2,
       INTR.IDNT_OBJT_APPL,
       INTR.DUR_APPL_SECN,
       INTR.RESL_APPL,
       INTR.DAT_HEUR_RAPP,	   
       CASE WHEN INTR.FLG_DEFN_RAPP = 1 THEN 'Oui'
            WHEN INTR.FLG_DEFN_RAPP = 0 THEN 'Non'
       END AS FLG_DEFN_RAPP,	   
       INTR.NIV_SATS,
       INTR.IDNT_REPN,
       INTR.LIEN_REPN,	   
       CASE WHEN INTR.FLG_SUPP = 1 THEN 'Oui'
            WHEN INTR.FLG_SUPP = 0 THEN 'Non'
       END AS FLG_SUPP,	      
       INTR.DAT_DERN_MODF,
       INTR.IDNT_DERN_MODF,
       INTR.HORD_MODF_SYST,	   
       CASE WHEN INTR.FLG_ARCH = 1 THEN 'Oui'
            WHEN INTR.FLG_ARCH = 0 THEN 'Non'
       END AS FLG_ARCH,	 	  
       CASE WHEN INTR.FLG_PUBL = 1 THEN 'Oui'
            WHEN INTR.FLG_PUBL = 0 THEN 'Non'
       END AS FLG_PUBL,	 
       INTR.IDNT_ACTV_RECR,	   
       CASE WHEN INTR.FLG_RECR = 1 THEN 'Oui'
            WHEN INTR.FLG_RECR = 0 THEN 'Non'
       END AS FLG_RECR,  
       INTR.DAT_DEBT_RECR,
       INTR.DAT_FIN_RECR,
       INTR.FUS_HORR_RECR,
       INTR.TYP_RECR,
       INTR.INTRV_RECR,
       INTR.MASQ_JOUR_SEMN_RECR,
       INTR.JOUR_MOIS_RECR,
       INTR.INST_RECR,
       INTR.MOIS_ANN_RECR,
       INTR.TYP_REPT_RECR,
       INTR.INFR_COMP,
       INTR.CHMP_TEST_WDE,
       INTR.URL_DOCM,
       INTR.MOTS_INTR,	   
       CASE WHEN INTR.FLG_MOTS_INTR = 1 THEN 'Oui'
            WHEN INTR.FLG_MOTS_INTR = 0 THEN 'Non'
       END AS FLG_MOTS_INTR,  	  	   
       INTR.ADRS_LIVR,
	   INTR.DAT_ACHV,
       INTR.FLG_ENRG_COUR
FROM dbo.DWH_INTERACTION INTR
LEFT JOIN dbo.REFERENTIEL_SF REF_STTT
  ON INTR.STTT = REF_STTT.COD_SF
  AND REF_STTT.COD_OBJ_SF = 'task' AND REF_STTT.COD_CHMP_SF = 'Status' AND REF_STTT.FLG_ACTIF = 1
LEFT JOIN dbo.REFERENTIEL_SF REF_FAML
  ON INTR.FAML = REF_FAML.COD_SF
  AND REF_FAML.COD_OBJ_SF = 'task' AND REF_FAML.COD_CHMP_SF = 'Family__c' AND REF_FAML.FLG_ACTIF = 1
LEFT JOIN dbo.REFERENTIEL_SF REF_SOUS_FAML
  ON INTR.SOUS_FAML = REF_SOUS_FAML.COD_SF
  AND REF_SOUS_FAML.COD_OBJ_SF = 'task' AND REF_SOUS_FAML.COD_CHMP_SF = 'SubFamily__c' AND REF_SOUS_FAML.FLG_ACTIF = 1
LEFT JOIN dbo.REFERENTIEL_SF REF_PRRT
  ON INTR.PRRT = REF_PRRT.COD_SF
  AND REF_PRRT.COD_OBJ_SF = 'task' AND REF_PRRT.COD_CHMP_SF = 'Priority' AND REF_PRRT.FLG_ACTIF = 1
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Enregistrement Courant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Adresse de livraison', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'ADRS_LIVR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Mots Interdits', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'FLG_MOTS_INTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mots interdits', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'MOTS_INTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'URL du document	', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'URL_DOCM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Champs test WDE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'CHMP_TEST_WDE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Informations Complémentaires', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'INFR_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type répétition de récurrence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'TYP_REPT_RECR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mois de l''année de récurrence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'MOIS_ANN_RECR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Instance de récurrence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'INST_RECR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Jour du mois de récurrence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'JOUR_MOIS_RECR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Masque du jour de la semaine de récurrence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'MASQ_JOUR_SEMN_RECR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Intervalle de récurrence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'INTRV_RECR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type de récurrence	', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'TYP_RECR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fuseau horaire de récurrence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'FUS_HORR_RECR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date fin de la récurrence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'DAT_FIN_RECR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date début de la récurrence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'DAT_DEBT_RECR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag de récurrence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'FLG_RECR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant d''activité de récurrence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'IDNT_ACTV_RECR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag public', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'FLG_PUBL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag archivé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'FLG_ARCH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Horodateur modification systeme', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'HORD_MODF_SYST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'IDNT_DERN_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag supprimé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'FLG_SUPP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Lien de réponse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'LIEN_REPN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de la réponse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'IDNT_REPN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Niveau de satisfaction', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'NIV_SATS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag définition du rappel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'FLG_DEFN_RAPP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date et heure de rappel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'DAT_HEUR_RAPP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Résultat de l''appel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'RESL_APPL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Durée de l''appel en seconde', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'DUR_APPL_SECN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de l''objet de l''appel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'IDNT_OBJT_APPL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type d''appel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'TYP_APPL_2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type d''appel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'TYP_APPL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag fermée	', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'FLG_FERM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Tâche', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'DAT_TACH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date échéance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'DAT_ECHN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Note de l''interaction', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'NOT_INTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date d''interaction', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'DAT_INTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre mois depuis création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'NOMB_MOIS_DEPS_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'IDNT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'DAT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date et heure de création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_HMS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant du proprietaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'IDNT_PRPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro de conversation IBM', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'NUMR_CONV_IBM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de nom', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'IDNT_NOM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant d''associé à', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'IDNT_ASSC_A';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag priorité élevée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'FLG_PRRT_ELV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Description', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'DESC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Priorité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'LIBL_PRRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Priorité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'COD_PRRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Statut', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'LIBL_STTT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Statut', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'COD_STTT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Sous famille', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'LIBL_SOUS_FAML';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Sous famille', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'COD_SOUS_FAML';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Famille', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'LIBL_FAML';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Famille', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'COD_FAML';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Sous type de tâche', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'SOUS_TYP_TACH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'TYP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Objet', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'OBJT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant type enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'IDNT_TYP_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Client SAB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'NUMR_CLNT_SAB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'NUMR_PERS_SF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant SF personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'IDNT_SF_PERS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Interaction', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'NUMR_INTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Tâche', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'NUMR_TACH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Interaction', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'IDNT_INTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date d''achèvement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_INTERACTION', @level2type = N'COLUMN', @level2name = N'DAT_ACHV';

