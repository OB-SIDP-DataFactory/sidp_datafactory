﻿

CREATE VIEW [dbo].[VUE_EQUIPEMENT_CREDIT_CONSO] AS
SELECT [DAT_OBSR]
      ,[REFR_CONT]
      ,[NUMR_COMP_SAB]
	  ,CASE WHEN NUMR_COMP_SAB IS NOT NULL OR NUMR_COMP_SAB !='' THEN 'Oui'
		ELSE 'Non'
		END AS FLG_NUMR_COMP_SAB
      ,[DAT_ACCR_TOKS]
      ,[DAT_ACCP_CLNT]
      ,[DAT_DECS]
      ,[DAT_DERN_DEBL]
      ,[DEL_DECS]
      ,[DAT_FIN_PRVS_DOSS]
      ,[RES_DIST]
      ,[NUMR_COMP_PRLV]
      ,[COD_BANQ_PRLV]
      ,[COD_GUIC_PRLV]
      ,[LIBL_BANQ_GUIC_PRLV]
      ,[LIBL_TITL_COMP_PRLV]
      ,[MONT_NOMN]
      ,[MONT_TOTL_DEBL]
      ,[MONT_TOTL_DU]
      ,[CAPT_REST_DU]
      ,[CATG_PRT]
      ,[COD_OBJT_FINN]
      ,[COD_DEVS]
      ,[DUR_LEGL]
      ,[TAUX_VEND]
      ,[TAUX_INTR_PALR]
      ,[NATR_ASSR_EMPR]
      ,[NATR_ASSR_COEM]
      ,[NATR_ASSR_CAUT_1]
      ,[NATR_ASSR_CAUT_2]
      ,CASE WHEN FLG_ASSR_SOUS='O' THEN 'Oui' 
			WHEN FLG_ASSR_SOUS='N' THEN 'Non'
		END AS FLG_ASSR_SOUS
      ,[COD_SITT_TOKS]
      ,[FRS_DOSS]
      ,[MONT_AUTR_FRS]
      ,[DAT_DERN_MODF]
      ,[SITT_CONT]
      ,[COD_FIN_DOSS]
      ,[DAT_MODF_RIB]
      ,[DAT_MODF_REGL]
      ,[COD_MOD_REGL]
      ,[PRT_MUTL]
      ,[COD_MOD_DEBL_FOND]
      ,[DAT_PRMR_ECHN]
      ,[DAT_DERN_ECHN_TRT]
      ,[NUMR_PALR]
      ,[NOMB_ECHN_TOTL]
      ,[NOMB_ECHN_ECHS]
      ,[CAPT_REST_DERN_ECHN]
      ,[INTR_DERN_ECHN]
      ,[ACCS_DERN_ECHN]
      ,[ASSR_DERN_ECHN]
      ,[DAT_PRCH_ECHN]
      ,[CAPT_PRCH_ECHN]
      ,[INTR_PRCH_ECHN]
      ,[ACCS_PRCH_ECHN]
      ,[ASSR_PRCH_ECHN]
      ,[DAT_PRCH_PERC_ECHN]
      ,[DAT_PRCH_ANTC]
      ,CASE WHEN [FLG_EXST_PLN_REAM]='O' THEN 'Oui' 
			WHEN [FLG_EXST_PLN_REAM]='N' THEN 'Non'
		END AS [FLG_EXST_PLN_REAM]
      ,[MONT_GLBL_IMP]
      ,[MONT_GLBL_CAPT_IMP]
      ,[MONT_GLBL_INTR_IMP]
      ,[MONT_GLBL_ASSR_IMP]
      ,[MONT_GLBL_ACCS_IMP]
      ,[MONT_GLBL_IR_IMP]
      ,[MONT_GLBL_IL_IMP]
      ,[TAUX_INTR_RETR]
      ,[DAT_PRMR_IMP]
      ,[DAT_DERN_IMP]
      ,[DAT_PRCH_ECHN_RATT]
      ,[MONT_IMP_DECL_FICP]
      ,[NOMB_IMP_AVNT_TRTM]
      ,[NUMR_PLN_RATT_TOKS]
      ,[MONT_IMP_AVNT_TRTM]
      ,[MONT_IMP_COUR_RE_EMS]
      ,[MONT_IMP_PLN_RATT]
      ,[MONT_IMP_NON_REEM]
      ,CASE WHEN [FLG_EXST_PLN_RATT]='O' THEN 'Oui' 
			WHEN [FLG_EXST_PLN_RATT]='N' THEN 'Non'
		END AS [FLG_EXST_PLN_RATT]
      ,[NOMB_ECHN_IMPS]
      ,[DERN_COD_REJT_TOKS]
      ,[DAT_ECHN_REPR]
      ,[DAT_PRCH_REPR]
      ,CASE WHEN [FLG_INDC_BALG]='O' THEN 'Oui' 
			WHEN [FLG_INDC_BALG]='N' THEN 'Non'
		END AS [FLG_INDC_BALG]
      ,CASE WHEN [FLG_CONT_TITR]='O' THEN 'Oui' 
			WHEN [FLG_CONT_TITR]='N' THEN 'Non'
		END AS [FLG_CONT_TITR]
      ,[DAT_CESS_CONT_TITR]
      ,[NUMR_FOND_COMM_CRNC]
      ,[NUMR_CESS_FCC]
      ,[DAT_SORT_POUR_CONT_TITR]
      ,[COD_AGS]
      ,[NOMB_JOUR_DIFF]
      ,[TYP_CONT]
      ,CASE WHEN [FLG_INDC_REPR]='O' THEN 'Oui' 
			WHEN [FLG_INDC_REPR]='N' THEN 'Non'
		END AS [FLG_INDC_REPR]
      ,[ORGN_SIGN_CLNT]
      ,[PERD_AMRT]
      ,[DAT_PASS_CNTX]
      ,[REFR_CONT_REAM_CP]
      ,[DUR_TOTL_REPR_ECHN]
      ,[INTR_REPR_REST_DU]
      ,[DAT_DERN_IMPT_COMP]
      ,[DAT_DERN_ACTL]
      ,[DAT_DERN_REGL]
      ,[DAT_DERN_REPR]
      ,[DAT_BASC_COMP_EUR]
      ,[INTR_CAPT_REST_DU]
      ,[INTR_DIFF_REST_DU]
      ,[COD_SOCT_TECH]
      ,[DAT_MODF]
      ,[DAT_CRTN]
      ,[TYP_AGNT]
      ,[IDNT_GEST]
      ,[DAT_CRTN_ENRG]
      ,[DAT_DERN_MODF_ENRG]
      ,[FLG_ENRG_COUR]
  FROM [dbo].[DWH_EQUIPEMENT_CREDIT_CONSO]
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Enregistrement Courant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant gestionnaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'IDNT_GEST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type agent', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TYP_AGNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code societe technique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'COD_SOCT_TECH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Interet differe restant du', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'INTR_DIFF_REST_DU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Interet capitalise restant du', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'INTR_CAPT_REST_DU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date bascule compte en euro', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_BASC_COMP_EUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere representation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_DERN_REPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date dernier reglement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_DERN_REGL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere actualisation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_DERN_ACTL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière imputation comptable', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_DERN_IMPT_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Interet reporte restant du', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'INTR_REPR_REST_DU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Duree totale reports echeance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DUR_TOTL_REPR_ECHN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Reference contrat reamenage CP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'REFR_CONT_REAM_CP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date passage en contentieux', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_PASS_CNTX';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Periodicite amortissement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'PERD_AMRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Origine signature client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'ORGN_SIGN_CLNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Indicateur de reprise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'FLG_INDC_REPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type contrat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TYP_CONT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre de jours differes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NOMB_JOUR_DIFF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code agios', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'COD_AGS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date sortie pour contrat titrise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_SORT_POUR_CONT_TITR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numero cession au FCC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NUMR_CESS_FCC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numero fond commun de creance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NUMR_FOND_COMM_CRNC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date cession contrat titrise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_CESS_CONT_TITR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Contrat titrise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'FLG_CONT_TITR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Indicateur balayage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'FLG_INDC_BALG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date prochaine representation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_PRCH_REPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date echeance representee', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_ECHN_REPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernier code rejet tokos', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DERN_COD_REJT_TOKS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre echeances impayees', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NOMB_ECHN_IMPS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Existence plan rattrapage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'FLG_EXST_PLN_RATT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant impaye non reemis', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_IMP_NON_REEM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant impaye plan rattrapage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_IMP_PLN_RATT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant impaye en cours re emis', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_IMP_COUR_RE_EMS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant impaye avant traitement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_IMP_AVNT_TRTM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numero plan rattrapage tokos', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NUMR_PLN_RATT_TOKS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre impaye avant traitement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NOMB_IMP_AVNT_TRTM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant impaye declaration FICP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_IMP_DECL_FICP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date prochaine echeance rattrapage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_PRCH_ECHN_RATT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date du dernier impaye', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_DERN_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date du premier impaye', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_PRMR_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux interets de retard', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TAUX_INTR_RETR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant global IL impaye', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_GLBL_IL_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant global IR impaye', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_GLBL_IR_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant global accessoire impaye', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_GLBL_ACCS_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant global assurance impaye', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_GLBL_ASSR_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant global interet impaye', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_GLBL_INTR_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant global capital impaye', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_GLBL_CAPT_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant global impaye', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_GLBL_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Existence plan reamenagement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'FLG_EXST_PLN_REAM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date prochaine anticipation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_PRCH_ANTC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date prochaine perception echeance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_PRCH_PERC_ECHN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Assurance prochaine echeance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'ASSR_PRCH_ECHN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Accessoire prochaine echeance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'ACCS_PRCH_ECHN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Interet prochaine echeance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'INTR_PRCH_ECHN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Capital prochaine echeance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'CAPT_PRCH_ECHN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date prochaine echeance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_PRCH_ECHN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Assurance derniere echeance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'ASSR_DERN_ECHN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Accessoire derniere echeance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'ACCS_DERN_ECHN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Interet derniere echeance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'INTR_DERN_ECHN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Capital restant du derniere echeance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'CAPT_REST_DERN_ECHN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre echeances echues', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NOMB_ECHN_ECHS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre echeances totales', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NOMB_ECHN_TOTL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numero palier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NUMR_PALR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere echeance traitee', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_DERN_ECHN_TRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date premiere echeance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_PRMR_ECHN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code mode deblocage des fonds', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'COD_MOD_DEBL_FOND';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Pret mutualise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'PRT_MUTL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code mode reglement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'COD_MOD_REGL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date modification reglement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_MODF_REGL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date modification du RIB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_MODF_RIB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code fin de dossier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'COD_FIN_DOSS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Situation contrat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'SITT_CONT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant autres frais', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_AUTR_FRS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Frais de dossier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'FRS_DOSS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code situation tokos', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'COD_SITT_TOKS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag assurance souscrite', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'FLG_ASSR_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nature assurance caution 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NATR_ASSR_CAUT_2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nature assurance caution 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NATR_ASSR_CAUT_1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nature assurance coemprunteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NATR_ASSR_COEM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nature assurance emprunteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NATR_ASSR_EMPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux interet palier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TAUX_INTR_PALR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux vendeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'TAUX_VEND';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Duree legale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DUR_LEGL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code devise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'COD_DEVS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code objet finance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'COD_OBJT_FINN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Categorie pret', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'CATG_PRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Capital restant du', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'CAPT_REST_DU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant total dû', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_TOTL_DU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant total debloque', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_TOTL_DEBL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant nominal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'MONT_NOMN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libelle titulaire compte prélèvement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'LIBL_TITL_COMP_PRLV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libelle banque et guichet prélèvement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'LIBL_BANQ_GUIC_PRLV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code guichet prélèvement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'COD_GUIC_PRLV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code banque prélèvement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'COD_BANQ_PRLV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro de compte prélèvement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NUMR_COMP_PRLV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Reseau de distribution', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'RES_DIST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date fin previsionnelle dossier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_FIN_PRVS_DOSS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Délai décaissement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DEL_DECS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date dernier deblocage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_DERN_DEBL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de décaissement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_DECS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date acceptation client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_ACCP_CLNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date accord tokos', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_ACCR_TOKS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numero compte SAB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NUMR_COMP_SAB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Reference contrat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'REFR_CONT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Numero compte SAB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'FLG_NUMR_COMP_SAB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date création de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';

