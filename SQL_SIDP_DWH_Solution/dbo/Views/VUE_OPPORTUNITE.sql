﻿CREATE VIEW dbo.VUE_OPPORTUNITE AS
SELECT
	opp.DAT_OBSR,
	opp.IDNT_OPPR,
	opp.NUMR_OPPR,
	opp.IDNT_SF_PERS,
	opp.NUMR_PERS_SF,
	opp.NUMR_CLNT_SAB,
	opp.NOM_OPPR,
	opp.COD_OFFR_COMM,
	opp.NOM_OFFR_COMM,
	opp.IDNT_TYP_ENRG,
	ref_RecordTypeId.COD_SF AS COD_TYP_ENRG,
	ref_RecordTypeId.NOM_SF AS LIBL_TYP_ENRG,
	opp.COD_FAML_PRDT,
	opp.NOM_FAML_PRDT,
	opp.PRDT,
	opp.STD_VENT,
	ref_StageName.LIBL_SF AS LIBL_STD_VENT,
	pa.ANCN_VALR AS STD_VENT_PRCD,
	ref_StageName_prec.LIBL_SF AS LIBL_STD_VENT_PRCD,
	CASE
	WHEN pa.ANCN_VALR IS NOT NULL and opp.COD_OFFR_COMM IN ('OPAP','OPTR','OPVN','OPVO') 
		AND opp.STD_VENT NOT IN ('10','11','12','15','16') 
	THEN DATEDIFF(DD, CAST(pa.DAT_MODF AS DATE), opp.DAT_OBSR)
	WHEN pa.ANCN_VALR IS NOT NULL and (opp.COD_OFFR_COMM  NOT IN ('OPAP','OPTR','OPVN','OPVO') OR opp.COD_OFFR_COMM IS NULL) 
		AND opp.STD_VENT NOT IN ('09','10','11','13','15','22','29')
	THEN DATEDIFF(DD, CAST(pa.DAT_MODF AS DATE), opp.DAT_OBSR)
	WHEN pa.ANCN_VALR IS NULL and opp.COD_OFFR_COMM IN ('OPAP','OPTR','OPVN','OPVO') 
		AND opp.STD_VENT NOT IN ('10','11','12','15','16') 
	THEN DATEDIFF(DD, CAST(opp.DAT_CRTN_OPPR AS DATE), opp.DAT_OBSR) 	
	WHEN pa.ANCN_VALR IS NULL and (opp.COD_OFFR_COMM  NOT IN ('OPAP','OPTR','OPVN','OPVO') OR opp.COD_OFFR_COMM IS NULL) 
		AND opp.STD_VENT NOT IN ('09','10','11','13','15','22','29')
	THEN DATEDIFF(DD, CAST(opp.DAT_CRTN_OPPR AS DATE), opp.DAT_OBSR) 	
	ELSE NULL
	END AS DUR_STD_VENT,
	CAST(opp.DAT_CRTN_OPPR AS DATE) AS DAT_CRTN_OPPR,
	CAST(opp.DAT_CLTR_OPPR AS DATE) AS DAT_CLTR_OPPR,
	CAST(pa.DAT_MODF AS DATE) AS DAT_CHNG_DERN_STD_VENT,
	opp.EVNM_ORGN,
	ref_OriginalEvent__c.LIBL_SF AS LIBL_EVNM_ORGN,
	opp.IDNT_DERN_MODF,
	opp.AGE_OPPR,
	CASE WHEN opp.FLG_FERM='true' THEN 'Oui' 
	WHEN opp.FLG_FERM='false' THEN 'Non'
	END AS FLG_FERM,
	CASE WHEN opp.FLG_GAGN='true' THEN 'Oui' 
	WHEN opp.FLG_GAGN='false' THEN 'Non'
	END AS FLG_GAGN,
	opp.MOTF_AFFR_REFS,
	ref_DeniedOpportunityReason__c.LIBL_SF AS LIBL_MOTF_AFFR_REFS,
	--Motif Refus Engagement R5
	opp.MOTF_REFS_ENGG,
	ref_CommitmentRefusalReason__c.LIBL_SF AS LIBL_MOTF_REFS_ENGG,
	opp.MOTF_SANS_SUIT,
	ref_RejectReason__c.LIBL_SF AS LIBL_MOTF_SANS_SUIT,
	opp.CANL_INTR_ORGN,
	ref_StartedChannel__c.LIBL_SF AS LIBL_CANL_INTR_ORGN,
	opp.CANL_DIST_ORGN,
	ref_OriginDistributionChannel__c.LIBL_SF AS LIBL_CANL_DIST_ORGN,
	opp.CANL_INTR,
	ref_LeadSource.LIBL_SF AS LIBL_CANL_INTR,
	opp.CANL_DIST,
	ref_DistributionChannel__c.LIBL_SF AS LIBL_CANL_DIST,
	opp.RES_DIST,
	ref_DistributorNetwork__c.LIBL_SF AS LIBL_RES_DIST,
	opp.ENTT_DIST,
	ref_DistributorEntity__c.LIBL_SF AS LIBL_ENTT_DIST
	--Canal Apporteur 
	--OF_Souscription_boutique
	,CASE WHEN opp.COD_OFFR_COMM='OC80' AND opp.STD_VENT='09' AND opp.RES_DIST='01' AND opp.FLG_DOSS_COMP='1' THEN 'Souscription boutique'
	--OF_Repli_Selfcare
	WHEN opp.COD_OFFR_COMM='OC80' AND opp.STD_VENT='09' AND opp.RES_DIST='01' AND (opp.FLG_DOSS_COMP<>'1' OR opp.FLG_DOSS_COMP IS NULL) AND opp.IDNT_PRCS_SOUS IS NOT NULL THEN 'Repli selfcare'
	--OF_Indication_boutique
	WHEN opp.COD_OFFR_COMM='OC80' AND opp.STD_VENT='09' AND opp.RES_DIST='01' AND opp.IDNT_PRCS_SOUS IS NULL AND opp.FLG_INDC='Oui' AND opp.ENTT_DIST IN ('01','02') THEN 'Indication boutique'
	--OF_Indication_Digitale
	WHEN opp.COD_OFFR_COMM='OC80' AND opp.STD_VENT='09' AND opp.RES_DIST='01' AND opp.IDNT_PRCS_SOUS IS NULL AND opp.FLG_INDC='Oui' AND opp.ENTT_DIST='04' THEN 'Indication digitale'
	--OF_Renvoi_trafic
	WHEN opp.COD_OFFR_COMM='OC80' AND opp.STD_VENT='09' AND opp.RES_DIST='01' AND opp.ENTT_DIST IN ('04') AND opp.CANL_INTR_ORGN='11' THEN 'Renvoi trafic'
	-- OB_selfcare
	WHEN opp.COD_OFFR_COMM='OC80' AND opp.STD_VENT='09' AND opp.RES_DIST='02' AND opp.CANL_INTR_ORGN IN ('10','11') THEN 'OB selfcare'
	--OB_CRC
	WHEN opp.COD_OFFR_COMM='OC80' AND opp.STD_VENT='09' AND opp.RES_DIST='02' AND opp.CANL_INTR_ORGN IN ('12','13') THEN 'OB CRC'
	--Si  Réseau Distributeur = 'Groupama / GAN' alors le canal apporteur est valorisé à 'GMA - Caisse'
	WHEN opp.COD_OFFR_COMM='OC80' AND opp.STD_VENT='09' AND opp.RES_DIST='03' THEN 'GMA - Caisse'
	END AS TYP_DIST,
	opp.IDNT_SOUR,
	opp.COD_POIN_VENT,
	opp.COD_CONS,
	opp.IDNT_CONS,
	--Nom du conseiller
	opp.CANL_INTR_FINL,
	ref_LeadSource_finl.LIBL_SF AS LIBL_CANL_INTR_FINL,
	--Ajout Canal d'interaction de finalisation 2
	opp.CANL_INTR_FINL_2,
	ref_EndingChannel__c.LIBL_SF AS LIBL_CANL_INTR_FINL_2,
	opp.FLG_INDC,
	opp.NUMR_INDC_PRCS,
	opp.INDC_DIGT,
	opp.INDC_CANL_INTR_ORGN,
	ref_StartedChannelForIndication__c.LIBL_SF AS LIBL_INDC_CANL_INTR_ORGN,
	CASE WHEN opp.FLG_DOSS_COMP='1' THEN 'Oui' 
	WHEN opp.FLG_DOSS_COMP='0' THEN 'Non'
	END AS FLG_DOSS_COMP,
	opp.MOTF_DOSS_INCM,
	ref_IncompleteFileReason__c.LIBL_SF AS LIBL_MOTF_DOSS_INCM,
	opp.IDNT_PRCS_SOUS,
	opp.CAMP,
	opp.DERG,
	ref_Derogation__c.LIBL_SF AS LIBL_DERG,
	opp.REAL_PAR,
	opp.FINL_PAR,
	opp.OBJT_FINN,
	ref_FundingSubject__c.LIBL_SF AS LIBL_OBJT_FINN,
	opp.MONT_PRT,
	opp.MONT_DEBL,
	opp.TAUX_OFFR,
	opp.DAT_SIGN,
	CAST(opp.DAT_ACCR_CRDT AS DATE) AS DAT_ACCR_CRDT,
	opp.DEL_ACCR_CRDT,
	opp.DAT_RETR,
	CAST(opp.DAT_DEBL_CRDT AS DATE) AS DAT_DEBL_CRDT,
	opp.DEL_DEBL_CRDT,
	opp.DAT_DEBT,
	opp.JOUR_PRLV,
	ref_DebitDay__c.LIBL_SF AS LIBL_JOUR_PRLV,
	opp.PRMR_ECHN,
	ref_FirstDueDate__c.LIBL_SF AS LIBL_PRMR_ECHN,
	opp.CATG,
	opp.SCR_ENTR_RELT,
	ref_RelationEntryScore__c.LIBL_SF AS LIBL_SCR_ENTR_RELT,
	--Code pré-score R5
	opp.COD_PR_SCR,
	--Motif Pré-score R5
	opp.MOTF_PR_SCR,
	opp.COUL_PR_SCR,
	--Ajout Couleur pré score TCH R5
	opp.COUL_PR_SCR_TCH,
	ref_TCHPreScoreColor__c.LIBL_SF AS LIBL_COUL_PR_SCR_TCH,
	opp.DECS_FINL,
	--Ajout Décision finale TCH R5
	opp.DECS_FINL_TCH,
	ref_TCHFinalDecision__c.LIBL_SF AS LIBL_DECS_FINL_TCH,
	--Taux d'endettement R5
	opp.TAUX_ENDT,
	CASE WHEN opp.FLG_DONN_TELC_OPT_IN='true' THEN 'Oui' 
	WHEN opp.FLG_DONN_TELC_OPT_IN='false' THEN 'Non'
	END AS FLG_DONN_TELC_OPT_IN,
	--Opt-in facture Telco R5
	CASE WHEN opp.FLG_FACT_TELC_OPT_IN='true' THEN 'Oui' 
	WHEN opp.FLG_FACT_TELC_OPT_IN='false' THEN 'Non'
	END AS FLG_FACT_TELC_OPT_IN,
	opp.NUMR_CONT_DIST,
	CASE WHEN opp.FLG_SOUS_OU_TITL='true' THEN 'Oui' 
	WHEN opp.FLG_SOUS_OU_TITL='false' THEN 'Non'
	END AS FLG_SOUS_OU_TITL,
	opp.FLG_ENRG_COUR
FROM
	dbo.DWH_OPPORTUNITE		opp
	LEFT JOIN (SELECT IDNT_SF, NUMR_OPPR, IDNT_OPPR, DAT_MODF, CHMP_MODF, NOUV_VALR, ANCN_VALR, DAT_DEBT_VALD, DAT_FIN_VALD FROM DWH_PISTE_AUDIT_OPPORTUNITE WHERE CHMP_MODF='StageName') pa 
		ON opp.IDNT_OPPR = pa.IDNT_OPPR AND opp.STD_VENT = pa.NOUV_VALR AND CAST(pa.DAT_DEBT_VALD AS DATE) <= opp.DAT_OBSR and opp.DAT_OBSR < CAST(pa.DAT_FIN_VALD AS DATE)
	LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REF_TYPE_ENREGISTREMENT AS ref_RecordTypeId
		ON opp.IDNT_TYP_ENRG =ref_RecordTypeId.IDNT_SF AND ref_RecordTypeId.TYP_OBJT_SF='Opportunity' AND ref_RecordTypeId.DAT_DEBT_VALD <= opp.DAT_OBSR AND opp.DAT_OBSR < ref_RecordTypeId.DAT_FIN_VALD
	LEFT JOIN (SELECT COD_SF, LIBL_SF, DAT_DEBT_VALD, DAT_FIN_VALD FROM [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF WHERE COD_OBJ_SF='opportunity' AND COD_CHMP_SF='StageName') ref_StageName 
		ON opp.STD_VENT = ref_StageName.COD_SF AND ref_StageName.DAT_DEBT_VALD <= opp.DAT_OBSR AND opp.DAT_OBSR < ref_StageName.DAT_FIN_VALD
	LEFT JOIN (SELECT COD_SF, LIBL_SF, DAT_DEBT_VALD, DAT_FIN_VALD FROM [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF WHERE COD_OBJ_SF='opportunity' AND COD_CHMP_SF='StageName') ref_StageName_prec 
		ON pa.ANCN_VALR = ref_StageName_prec.COD_SF AND ref_StageName_prec.DAT_DEBT_VALD <= opp.DAT_OBSR AND opp.DAT_OBSR < ref_StageName_prec.DAT_FIN_VALD
	LEFT JOIN (SELECT COD_SF, LIBL_SF, DAT_DEBT_VALD, DAT_FIN_VALD FROM [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF WHERE COD_OBJ_SF='opportunity' AND COD_CHMP_SF='OriginalEvent__c') ref_OriginalEvent__c 
		ON opp.EVNM_ORGN = ref_OriginalEvent__c.COD_SF AND ref_OriginalEvent__c.DAT_DEBT_VALD <= opp.DAT_OBSR AND opp.DAT_OBSR < ref_OriginalEvent__c.DAT_FIN_VALD
	LEFT JOIN (SELECT COD_SF, LIBL_SF, DAT_DEBT_VALD, DAT_FIN_VALD FROM [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF WHERE COD_OBJ_SF='opportunity' AND COD_CHMP_SF='DeniedOpportunityReason__c') ref_DeniedOpportunityReason__c 
		ON opp.MOTF_AFFR_REFS = ref_DeniedOpportunityReason__c.COD_SF AND ref_DeniedOpportunityReason__c.DAT_DEBT_VALD <= opp.DAT_OBSR AND opp.DAT_OBSR < ref_DeniedOpportunityReason__c.DAT_FIN_VALD
	LEFT JOIN (SELECT COD_SF, LIBL_SF, DAT_DEBT_VALD, DAT_FIN_VALD FROM [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF WHERE COD_OBJ_SF='opportunity' AND COD_CHMP_SF='CommitmentRefusalReason__c') ref_CommitmentRefusalReason__c 
		ON opp.MOTF_REFS_ENGG = ref_CommitmentRefusalReason__c.COD_SF AND ref_CommitmentRefusalReason__c.DAT_DEBT_VALD <= opp.DAT_OBSR AND opp.DAT_OBSR < ref_CommitmentRefusalReason__c.DAT_FIN_VALD
	LEFT JOIN (SELECT COD_SF, LIBL_SF, DAT_DEBT_VALD, DAT_FIN_VALD FROM [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF WHERE COD_OBJ_SF='opportunity' AND COD_CHMP_SF='RejectReason__c') ref_RejectReason__c 
		ON opp.MOTF_SANS_SUIT = ref_RejectReason__c.COD_SF AND ref_RejectReason__c.DAT_DEBT_VALD <= opp.DAT_OBSR AND opp.DAT_OBSR < ref_RejectReason__c.DAT_FIN_VALD
	LEFT JOIN (SELECT COD_SF, LIBL_SF, DAT_DEBT_VALD, DAT_FIN_VALD FROM [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF WHERE COD_OBJ_SF='opportunity' AND COD_CHMP_SF='StartedChannel__c') ref_StartedChannel__c 
		ON opp.CANL_INTR_ORGN = ref_StartedChannel__c.COD_SF AND ref_StartedChannel__c.DAT_DEBT_VALD <= opp.DAT_OBSR AND opp.DAT_OBSR < ref_StartedChannel__c.DAT_FIN_VALD
	LEFT JOIN (SELECT COD_SF, LIBL_SF, DAT_DEBT_VALD, DAT_FIN_VALD FROM [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF WHERE COD_OBJ_SF='opportunity' AND COD_CHMP_SF='OriginDistributionChannel__c') ref_OriginDistributionChannel__c
		ON opp.CANL_DIST_ORGN = ref_OriginDistributionChannel__c.COD_SF AND ref_OriginDistributionChannel__c.DAT_DEBT_VALD <= opp.DAT_OBSR AND opp.DAT_OBSR < ref_OriginDistributionChannel__c.DAT_FIN_VALD
	LEFT JOIN (SELECT COD_SF, LIBL_SF, DAT_DEBT_VALD, DAT_FIN_VALD FROM [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF WHERE COD_OBJ_SF='opportunity' AND COD_CHMP_SF='LeadSource') ref_LeadSource
		ON opp.CANL_INTR = ref_LeadSource.COD_SF AND ref_LeadSource.DAT_DEBT_VALD <= opp.DAT_OBSR AND opp.DAT_OBSR < ref_LeadSource.DAT_FIN_VALD
	LEFT JOIN (SELECT COD_SF, LIBL_SF, DAT_DEBT_VALD, DAT_FIN_VALD FROM [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF WHERE COD_OBJ_SF='opportunity' AND COD_CHMP_SF='DistributionChannel__c') ref_DistributionChannel__c
		ON opp.CANL_DIST = ref_DistributionChannel__c.COD_SF AND ref_DistributionChannel__c.DAT_DEBT_VALD <= opp.DAT_OBSR AND opp.DAT_OBSR < ref_DistributionChannel__c.DAT_FIN_VALD
	LEFT JOIN (SELECT COD_SF, LIBL_SF, DAT_DEBT_VALD, DAT_FIN_VALD FROM [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF WHERE COD_OBJ_SF='opportunity' AND COD_CHMP_SF='DistributorNetwork__c') ref_DistributorNetwork__c
		ON opp.RES_DIST = ref_DistributorNetwork__c.COD_SF AND ref_DistributorNetwork__c.DAT_DEBT_VALD <= opp.DAT_OBSR AND opp.DAT_OBSR < ref_DistributorNetwork__c.DAT_FIN_VALD
	LEFT JOIN (SELECT COD_SF, LIBL_SF, DAT_DEBT_VALD, DAT_FIN_VALD FROM [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF WHERE COD_OBJ_SF='opportunity' AND COD_CHMP_SF='DistributorEntity__c') ref_DistributorEntity__c
		ON opp.ENTT_DIST = ref_DistributorEntity__c.COD_SF AND ref_DistributorEntity__c.DAT_DEBT_VALD <= opp.DAT_OBSR AND opp.DAT_OBSR < ref_DistributorEntity__c.DAT_FIN_VALD
	LEFT JOIN (SELECT COD_SF, LIBL_SF, DAT_DEBT_VALD, DAT_FIN_VALD FROM [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF WHERE COD_OBJ_SF='opportunity' AND COD_CHMP_SF='LeadSource') ref_LeadSource_finl
		ON opp.CANL_INTR_FINL = ref_LeadSource_finl.COD_SF AND ref_LeadSource_finl.DAT_DEBT_VALD <= opp.DAT_OBSR AND opp.DAT_OBSR < ref_LeadSource_finl.DAT_FIN_VALD
	LEFT JOIN (SELECT COD_SF, LIBL_SF, DAT_DEBT_VALD, DAT_FIN_VALD FROM [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF WHERE COD_OBJ_SF='opportunity' AND COD_CHMP_SF='StartedChannelForIndication__c') ref_StartedChannelForIndication__c
		ON opp.INDC_CANL_INTR_ORGN = ref_StartedChannelForIndication__c.COD_SF AND ref_StartedChannelForIndication__c.DAT_DEBT_VALD <= opp.DAT_OBSR AND opp.DAT_OBSR < ref_StartedChannelForIndication__c.DAT_FIN_VALD
	LEFT JOIN (SELECT COD_SF, LIBL_SF, DAT_DEBT_VALD, DAT_FIN_VALD FROM [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF WHERE COD_OBJ_SF='opportunity' AND COD_CHMP_SF='IncompleteFileReason__c') ref_IncompleteFileReason__c
		ON opp.MOTF_DOSS_INCM = ref_IncompleteFileReason__c.COD_SF AND ref_IncompleteFileReason__c.DAT_DEBT_VALD <= opp.DAT_OBSR AND opp.DAT_OBSR < ref_IncompleteFileReason__c.DAT_FIN_VALD
	LEFT JOIN (SELECT COD_SF, LIBL_SF, DAT_DEBT_VALD, DAT_FIN_VALD FROM [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF WHERE COD_OBJ_SF='opportunity' AND COD_CHMP_SF='Derogation__c') ref_Derogation__c
		ON opp.DERG = ref_Derogation__c.COD_SF AND ref_Derogation__c.DAT_DEBT_VALD <= opp.DAT_OBSR AND opp.DAT_OBSR < ref_Derogation__c.DAT_FIN_VALD
	LEFT JOIN (SELECT COD_SF, LIBL_SF, DAT_DEBT_VALD, DAT_FIN_VALD FROM [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF WHERE COD_OBJ_SF='opportunity' AND COD_CHMP_SF='FundingSubject__c') ref_FundingSubject__c
		ON opp.OBJT_FINN = ref_FundingSubject__c.COD_SF AND ref_FundingSubject__c.DAT_DEBT_VALD <= opp.DAT_OBSR AND opp.DAT_OBSR < ref_FundingSubject__c.DAT_FIN_VALD
	LEFT JOIN (SELECT COD_SF, LIBL_SF, DAT_DEBT_VALD, DAT_FIN_VALD FROM [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF WHERE COD_OBJ_SF='opportunity' AND COD_CHMP_SF='DebitDay__c') ref_DebitDay__c
		ON opp.JOUR_PRLV = ref_DebitDay__c.COD_SF AND ref_DebitDay__c.DAT_DEBT_VALD <= opp.DAT_OBSR AND opp.DAT_OBSR < ref_DebitDay__c.DAT_FIN_VALD
	LEFT JOIN (SELECT COD_SF, LIBL_SF, DAT_DEBT_VALD, DAT_FIN_VALD FROM [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF WHERE COD_OBJ_SF='opportunity' AND COD_CHMP_SF='FirstDueDate__c') AS ref_FirstDueDate__c
		ON opp.PRMR_ECHN = ref_FirstDueDate__c.COD_SF AND ref_FirstDueDate__c.DAT_DEBT_VALD <= opp.DAT_OBSR AND opp.DAT_OBSR < ref_FirstDueDate__c.DAT_FIN_VALD
	LEFT JOIN (SELECT COD_SF, LIBL_SF, DAT_DEBT_VALD, DAT_FIN_VALD FROM [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF WHERE COD_OBJ_SF='opportunity' AND COD_CHMP_SF='RelationEntryScore__c') AS ref_RelationEntryScore__c
		ON opp.SCR_ENTR_RELT = ref_RelationEntryScore__c.COD_SF AND ref_RelationEntryScore__c.DAT_DEBT_VALD <= opp.DAT_OBSR AND opp.DAT_OBSR < ref_RelationEntryScore__c.DAT_FIN_VALD
	LEFT JOIN (SELECT COD_SF, LIBL_SF, DAT_DEBT_VALD, DAT_FIN_VALD FROM [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF WHERE COD_OBJ_SF='opportunity' AND COD_CHMP_SF='TCHFinalDecision__c') AS ref_TCHFinalDecision__c
		ON opp.DECS_FINL_TCH = ref_TCHFinalDecision__c.COD_SF AND ref_TCHFinalDecision__c.DAT_DEBT_VALD <= opp.DAT_OBSR AND opp.DAT_OBSR < ref_TCHFinalDecision__c.DAT_FIN_VALD
	LEFT JOIN (SELECT COD_SF, LIBL_SF, DAT_DEBT_VALD, DAT_FIN_VALD FROM [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF WHERE COD_OBJ_SF='opportunity' AND COD_CHMP_SF='TCHPreScoreColor__c') AS ref_TCHPreScoreColor__c
		ON opp.COUL_PR_SCR_TCH = ref_TCHPreScoreColor__c.COD_SF AND ref_TCHPreScoreColor__c.DAT_DEBT_VALD <= opp.DAT_OBSR AND opp.DAT_OBSR < ref_TCHPreScoreColor__c.DAT_FIN_VALD
	LEFT JOIN (SELECT COD_SF, LIBL_SF, DAT_DEBT_VALD, DAT_FIN_VALD FROM [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF WHERE COD_OBJ_SF='opportunity' AND COD_CHMP_SF='EndingChannel__c') AS ref_EndingChannel__c
		ON opp.CANL_INTR_FINL_2 = ref_EndingChannel__c.COD_SF AND ref_EndingChannel__c.DAT_DEBT_VALD <= opp.DAT_OBSR AND opp.DAT_OBSR < ref_EndingChannel__c.DAT_FIN_VALD
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Observation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'DAT_OBSR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant de l''Opportunité' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'NUMR_OPPR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant technique client SF' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'IDNT_SF_PERS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant fonctionel client SF' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'NUMR_PERS_SF'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant client SAB' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'NUMR_CLNT_SAB'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nom de l''opportunité' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'NOM_OPPR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code de l''offre commerciale' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'COD_OFFR_COMM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nom de l''offre commerciale' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'NOM_OFFR_COMM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type d''enregistrement de l''opportunité' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'IDNT_TYP_ENRG'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code du type d''enregistrement de l''opportunité' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'COD_TYP_ENRG'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé du type d''enregistrement de l''opportunité' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'LIBL_TYP_ENRG'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code famille produit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'COD_FAML_PRDT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé famille produit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'NOM_FAML_PRDT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Produit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'PRDT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Étape' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'STD_VENT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nom de l''étape' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'LIBL_STD_VENT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Étape précédente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'STD_VENT_PRCD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nom de l''étape précédente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'LIBL_STD_VENT_PRCD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Durée de l''étape' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'DUR_STD_VENT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date de création' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'DAT_CRTN_OPPR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date de clôture' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'DAT_CLTR_OPPR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date de changement de la dernière Étape' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'DAT_CHNG_DERN_STD_VENT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Évènement d''origine' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'EVNM_ORGN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé de l''évènement d''origine' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'LIBL_EVNM_ORGN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dernière modification par' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'IDNT_DERN_MODF'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Age' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'AGE_OPPR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fermée' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'FLG_FERM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Gagnée' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'FLG_GAGN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Motif affaire refusée' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'MOTF_AFFR_REFS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé du motif affaire refusée' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'LIBL_MOTF_AFFR_REFS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Motif Refus Engagement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'MOTF_REFS_ENGG'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé du Motif Refus Engagement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'LIBL_MOTF_REFS_ENGG'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Motif sans suite' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'MOTF_SANS_SUIT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé du motif sans suite' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'LIBL_MOTF_SANS_SUIT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Canal d''interaction d''origine' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'CANL_INTR_ORGN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé du canal d''interaction d''origine' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'LIBL_CANL_INTR_ORGN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Canal de distribution d''origine' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'CANL_DIST_ORGN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé du canal de distribution d''origine' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'LIBL_CANL_DIST_ORGN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Canal d''interaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'CANL_INTR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé du canal d''interaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'LIBL_CANL_INTR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Canal de distribution' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'CANL_DIST'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé du canal de distribution' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'LIBL_CANL_DIST'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Réseau distributeur' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'RES_DIST'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé du réseau distributeur' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'LIBL_RES_DIST'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entité distributrice' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'ENTT_DIST'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé de l''entité distributrice' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'LIBL_ENTT_DIST'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type de distribution' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'TYP_DIST'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Source' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'IDNT_SOUR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Point de vente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'COD_POIN_VENT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Conseiller' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'COD_CONS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant conseiller' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'IDNT_CONS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Canal d''interaction de finalisation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'CANL_INTR_FINL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé du canal d''interaction de finalisation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'LIBL_CANL_INTR_FINL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Canal d''interaction de finalisation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'CANL_INTR_FINL_2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé du canal d''interaction de finalisation 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'LIBL_CANL_INTR_FINL_2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indication' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'FLG_INDC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id process indication' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'NUMR_INDC_PRCS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicateur indication digitale' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'INDC_DIGT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Canal d''intéraction d''origine indication' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'INDC_CANL_INTR_ORGN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé du canal d''intéraction d''origine indication' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'LIBL_INDC_CANL_INTR_ORGN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Dossier Complet' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'FLG_DOSS_COMP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Motif dossier incomplet' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'MOTF_DOSS_INCM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé du motif cossier incomplet' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'LIBL_MOTF_DOSS_INCM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant opportunité distributeur' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'IDNT_PRCS_SOUS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Campagne' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'CAMP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dérogation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'DERG'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé de la dérogation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'LIBL_DERG'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Réalisé par' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'REAL_PAR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Finalisé par' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'FINL_PAR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Objet de financement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'OBJT_FINN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé de l''objet de financement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'LIBL_OBJT_FINN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Montant du prêt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'MONT_PRT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Montant débloqué' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'MONT_DEBL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Taux de l''offre' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'TAUX_OFFR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date de signature' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'DAT_SIGN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date de l''accord du crédit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'DAT_ACCR_CRDT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Délai d''accord' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'DEL_ACCR_CRDT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date de la rétractation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'DAT_RETR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date du déblocage' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'DAT_DEBL_CRDT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Délai de déblocage' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'DEL_DEBL_CRDT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date de démarrage' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'DAT_DEBT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Jour de prélèvement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'JOUR_PRLV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé du jour de prélèvement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'LIBL_JOUR_PRLV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1ère échéance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'PRMR_ECHN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé de la 1ère échéance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'LIBL_PRMR_ECHN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Categorie' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'CATG'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Score d''entrée en relation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'SCR_ENTR_RELT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé du score d''entrée en relation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'LIBL_SCR_ENTR_RELT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code pré score' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'COD_PR_SCR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Motif pré score' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'MOTF_PR_SCR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Couleur pré-score' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'COUL_PR_SCR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Couleur pré score TCH' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'COUL_PR_SCR_TCH'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé de couleur pré score TCH' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'LIBL_COUL_PR_SCR_TCH'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Décision finale calculer pour la couleur' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'DECS_FINL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Décision finale TCH' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'DECS_FINL_TCH'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé de décision finale TCH' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'LIBL_DECS_FINL_TCH'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Taux d''endettement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'TAUX_ENDT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Opt-in données Telco' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'FLG_DONN_TELC_OPT_IN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Facture Telco Opt in' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'FLG_FACT_TELC_OPT_IN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'N° contrat distributeur' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'NUMR_CONT_DIST'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Souscripteur/Titulaire' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'FLG_SOUS_OU_TITL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag enregistrement Courant' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_OPPORTUNITE', @level2type=N'COLUMN',@level2name=N'FLG_ENRG_COUR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de l''opportunité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'IDNT_OPPR';

