﻿CREATE VIEW [dbo].[VUE_CONVENTION]
AS
	SELECT
		CNVT.DAT_OBSR,
		CNVT.NUMR_ABNN,
		CNVT.NUMR_COMP,
		CNVT.NUMR_CLNT_SAB,
		CNVT.COD_ETBL,
		CNVT.COD_AGNC,
		REF_AGENCE.Col5		AS	LIBL_AGNC,
		CNVT.COD_SERV,
		REF_SERV.Col5		AS	LIBL_SERV,
		CNVT.COD_SOUS		AS	COD_SOUS_SERV,
		REF_SOUS_SERV.Col5	AS	LIBL_SOUS_SERV,
		CNVT.COD_PRDT,
		CNVT.COD_ETT,
		CNVT.COMP_FACT,
		CNVT.DAT_ADHS,
		CNVT.DAT_FIN,
		CNVT.DAT_RENV,
		CNVT.UTLS_1,
		CNVT.UTLS_2,
		CNVT.DAT_RESL,
		CNVT.MOTF_RESL		AS	COD_MOTF_RESL,
		REF_MOTF_RESL.Col5	AS	LIBL_MOTF_RESL,
		CNVT.DAT_CRTN,
		CNVT.DAT_VALDTN,
		CNVT.DAT_ANNL,
		CNVT.MOD_ENV_RELV,
		FLG_ENRG_COUR
	FROM
		dbo.DWH_CONVENTION	AS	CNVT	WITH(NOLOCK)
		LEFT JOIN
		(
			SELECT DISTINCT
				Col2,
				Col5,
				DAT_DEBT_VALD,
				DAT_FIN_VALD
			FROM
				dbo.REFERENTIEL_SA	WITH(NOLOCK)
			WHERE
				1 = 1
				AND	SOUR_DONN = 'ZDWHAGE0'
		)	AS	REF_AGENCE
			ON	CNVT.COD_AGNC = REF_AGENCE.Col2
				AND	REF_AGENCE.DAT_DEBT_VALD <= CNVT.DAT_OBSR
				AND	CNVT.DAT_OBSR < REF_AGENCE.DAT_FIN_VALD
		LEFT JOIN
		(
			SELECT DISTINCT
				Col3,
				Col5,
				DAT_DEBT_VALD,
				DAT_FIN_VALD
			FROM
				dbo.REFERENTIEL_SA	WITH(NOLOCK)
			WHERE
				1 = 1
				AND	SOUR_DONN = 'ZDWHSER0'
		)	AS	REF_SERV
			ON	CNVT.COD_SERV = REF_SERV.Col3
				AND	REF_SERV.DAT_DEBT_VALD <= CNVT.DAT_OBSR
				AND	CNVT.DAT_OBSR < REF_SERV.DAT_FIN_VALD
		LEFT JOIN
		(
			SELECT DISTINCT
				Col3,
				Col5,
				DAT_DEBT_VALD,
				DAT_FIN_VALD
			FROM
				dbo.REFERENTIEL_SA	WITH(NOLOCK)
			WHERE
				1 = 1
				AND	SOUR_DONN = 'ZDWHSES0'
		)	AS	REF_SOUS_SERV
			ON	CNVT.COD_SOUS = REF_SOUS_SERV.Col3
				AND	REF_SOUS_SERV.DAT_DEBT_VALD <= CNVT.DAT_OBSR
				AND	CNVT.DAT_OBSR < REF_SOUS_SERV.DAT_FIN_VALD
		LEFT JOIN
		(
			SELECT DISTINCT
				Col3,
				Col5,
				DAT_DEBT_VALD,
				DAT_FIN_VALD
			FROM
				dbo.REFERENTIEL_SA	WITH(NOLOCK)
			WHERE
				1 = 1
				AND	SOUR_DONN = 'ZDWH0210'
		)	AS	REF_MOTF_RESL
			ON	CNVT.MOTF_RESL = REF_MOTF_RESL.Col3
				AND	REF_MOTF_RESL.DAT_DEBT_VALD <= CNVT.DAT_OBSR
				AND	CNVT.DAT_OBSR < REF_MOTF_RESL.DAT_FIN_VALD   
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Observation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CONVENTION', @level2type=N'COLUMN',@level2name=N'DAT_OBSR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Etablissement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CONVENTION', @level2type=N'COLUMN',@level2name=N'COD_ETBL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Agence' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CONVENTION', @level2type=N'COLUMN',@level2name=N'COD_AGNC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé Agence' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CONVENTION', @level2type=N'COLUMN',@level2name=N'LIBL_AGNC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CONVENTION', @level2type=N'COLUMN',@level2name=N'COD_SERV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CONVENTION', @level2type=N'COLUMN',@level2name=N'LIBL_SERV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Sous-Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CONVENTION', @level2type=N'COLUMN',@level2name=N'COD_SOUS_SERV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé Sous-Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CONVENTION', @level2type=N'COLUMN',@level2name=N'LIBL_SOUS_SERV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numéro Client SAB' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CONVENTION', @level2type=N'COLUMN',@level2name=N'NUMR_CLNT_SAB'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numéro Compte' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CONVENTION', @level2type=N'COLUMN',@level2name=N'NUMR_COMP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Produit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CONVENTION', @level2type=N'COLUMN',@level2name=N'COD_PRDT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Adhésion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CONVENTION', @level2type=N'COLUMN',@level2name=N'DAT_ADHS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Fin' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CONVENTION', @level2type=N'COLUMN',@level2name=N'DAT_FIN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date renouvellement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CONVENTION', @level2type=N'COLUMN',@level2name=N'DAT_RENV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Etat' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CONVENTION', @level2type=N'COLUMN',@level2name=N'COD_ETT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Compte Facturation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CONVENTION', @level2type=N'COLUMN',@level2name=N'COMP_FACT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Utilisateur 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CONVENTION', @level2type=N'COLUMN',@level2name=N'UTLS_1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Utilisateur 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CONVENTION', @level2type=N'COLUMN',@level2name=N'UTLS_2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Résiliation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CONVENTION', @level2type=N'COLUMN',@level2name=N'DAT_RESL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Motif Résiliation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CONVENTION', @level2type=N'COLUMN',@level2name=N'COD_MOTF_RESL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé Motif Résiliation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CONVENTION', @level2type=N'COLUMN',@level2name=N'LIBL_MOTF_RESL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Création' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CONVENTION', @level2type=N'COLUMN',@level2name=N'DAT_CRTN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Validation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CONVENTION', @level2type=N'COLUMN',@level2name=N'DAT_VALDTN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Annulation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CONVENTION', @level2type=N'COLUMN',@level2name=N'DAT_ANNL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Mode Envoi Relevés' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CONVENTION', @level2type=N'COLUMN',@level2name=N'MOD_ENV_RELV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Enregistrement Courant' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CONVENTION', @level2type=N'COLUMN',@level2name=N'FLG_ENRG_COUR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Abonnement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_CONVENTION', @level2type = N'COLUMN', @level2name = N'NUMR_ABNN';
GO