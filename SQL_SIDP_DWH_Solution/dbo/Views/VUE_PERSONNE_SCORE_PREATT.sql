﻿
CREATE VIEW [dbo].[VUE_PERSONNE_SCORE_PREATT] AS 
  SELECT
	DAT_OBSR,
	SF_NUMR_PERS,
	IDNT_SF_PERS,
	COD_RES_DIST,
	LIBL_RES_DIST,
	SCR_PRTT_ORANGE,
	SCR_PRTT_GROUPAMA,
	COD_RES_DIST_PRNC,
	LIBL_RES_DIST_PRNC,
	FLG_ENRG_COUR
FROM dbo.DWH_PERS_SCR_PRTT;
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Réseau distributeur principal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PERSONNE_SCORE_PREATT', @level2type = N'COLUMN', @level2name = N'LIBL_RES_DIST_PRNC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Réseau distributeur principal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PERSONNE_SCORE_PREATT', @level2type = N'COLUMN', @level2name = N'COD_RES_DIST_PRNC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Score de pré-attribution Groupama', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PERSONNE_SCORE_PREATT', @level2type = N'COLUMN', @level2name = N'SCR_PRTT_GROUPAMA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Score de pré-attribution Orange', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PERSONNE_SCORE_PREATT', @level2type = N'COLUMN', @level2name = N'SCR_PRTT_ORANGE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Réseau de distribution', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PERSONNE_SCORE_PREATT', @level2type = N'COLUMN', @level2name = N'LIBL_RES_DIST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Réseau de distribution', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PERSONNE_SCORE_PREATT', @level2type = N'COLUMN', @level2name = N'COD_RES_DIST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant SF de la personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PERSONNE_SCORE_PREATT', @level2type = N'COLUMN', @level2name = N'IDNT_SF_PERS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PERSONNE_SCORE_PREATT', @level2type = N'COLUMN', @level2name = N'SF_NUMR_PERS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PERSONNE_SCORE_PREATT', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Enregistrement Courant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PERSONNE_SCORE_PREATT', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';

