﻿
CREATE VIEW [dbo].[VUE_LIEN_EQPM_SL_SERVICE] AS 
SELECT lien.DAT_OBSR,
       lien.IDNT_EQPM,
       lien.IDNT_SERV,
	   ref.COD AS COD_SERV,
	   ref.LABL AS LIBL_SERV,
       lien.IDNT_STTS,
	   ServiceStatus.CODE AS LIBL_STTS,
       lien.IDNT_CLNT,
       lien.NUMR_AGRM,
       lien.TYP_RISQ_ASSR_1,
       lien.TAUX_RISQ_ASSR_1,
       lien.TYP_RISQ_ASSR_2,
       lien.TAUX_RISQ_ASSR_2,
       lien.LIST_GARN,
	   CAST(lien.DAT_CRTN AS date) AS DAT_CRTN,
       lien.DAT_CRTN AS DAT_CRTN_HMS,
       lien.IDNT_CRTN,
	   CAST(lien.DAT_DERN_MODF AS date) AS DAT_DERN_MODF,
       lien.DAT_DERN_MODF AS DAT_DERN_MODF_HMS,
       lien.IDNT_DERN_MODF,
       lien.TYP_UTLS,
	   lien.FLG_ENRG_COUR
  FROM [dbo].[DWH_LIEN_EQPM_SL_SERVICE] AS lien
  LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REF_SERVICE_SL AS ref
  ON lien.IDNT_SERV = ref.IDNT_SERV_SL AND ref.DAT_DEBT_VALD <= lien.DAT_OBSR AND lien.DAT_OBSR < ref.DAT_FIN_VALD
  LEFT JOIN [$(DatabaseInstanceName)].[$(DataHubDatabaseName)].dbo.REF_CODE ServiceStatus on lien.IDNT_STTS = ServiceStatus.CODE_ID and REF_FAMILY_ID = 39
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Observation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_EQPM_SL_SERVICE', @level2type=N'COLUMN',@level2name=N'DAT_OBSR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant equipement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_EQPM_SL_SERVICE', @level2type=N'COLUMN',@level2name=N'IDNT_EQPM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_EQPM_SL_SERVICE', @level2type=N'COLUMN',@level2name=N'IDNT_SERV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_EQPM_SL_SERVICE', @level2type=N'COLUMN',@level2name=N'COD_SERV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_EQPM_SL_SERVICE', @level2type=N'COLUMN',@level2name=N'LIBL_SERV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_EQPM_SL_SERVICE', @level2type=N'COLUMN',@level2name=N'IDNT_STTS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant Client' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_EQPM_SL_SERVICE', @level2type=N'COLUMN',@level2name=N'IDNT_CLNT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numéro agrement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_EQPM_SL_SERVICE', @level2type=N'COLUMN',@level2name=N'NUMR_AGRM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type de risque d''assurance 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_EQPM_SL_SERVICE', @level2type=N'COLUMN',@level2name=N'TYP_RISQ_ASSR_1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Taux de risque d''assurance 1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_EQPM_SL_SERVICE', @level2type=N'COLUMN',@level2name=N'TAUX_RISQ_ASSR_1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type de risque d''assurance 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_EQPM_SL_SERVICE', @level2type=N'COLUMN',@level2name=N'TYP_RISQ_ASSR_2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Taux de risque d''assurance 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_EQPM_SL_SERVICE', @level2type=N'COLUMN',@level2name=N'TAUX_RISQ_ASSR_2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Liste de garantie' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_EQPM_SL_SERVICE', @level2type=N'COLUMN',@level2name=N'LIST_GARN'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date heure et minute de creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_EQPM_SL_SERVICE', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_HMS';


GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant creation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_EQPM_SL_SERVICE', @level2type=N'COLUMN',@level2name=N'IDNT_CRTN'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date heure et minute de derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_EQPM_SL_SERVICE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_HMS';


GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant derniere modification' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_EQPM_SL_SERVICE', @level2type=N'COLUMN',@level2name=N'IDNT_DERN_MODF'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type utilisateur' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_EQPM_SL_SERVICE', @level2type=N'COLUMN',@level2name=N'TYP_UTLS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Enregistrement Courant' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_EQPM_SL_SERVICE', @level2type=N'COLUMN',@level2name=N'FLG_ENRG_COUR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIBL status', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_EQPM_SL_SERVICE', @level2type = N'COLUMN', @level2name = N'LIBL_STTS';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_EQPM_SL_SERVICE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_EQPM_SL_SERVICE', @level2type = N'COLUMN', @level2name = N'DAT_CRTN';

