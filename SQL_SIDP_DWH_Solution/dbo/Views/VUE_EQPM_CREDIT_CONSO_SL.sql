﻿

CREATE VIEW [dbo].[VUE_EQPM_CREDIT_CONSO_SL] AS 
SELECT  DAT_OBSR
      ,IDNT_EQPM
	  ,IDNT_TYP_FINN
	  ,ref_typ_finn.CODE AS LIBL_TYP_FINN
	  ,IDNT_RAIS_PRT_TERM
	  ,ref_rais_prt_term.CODE AS LIBL_RAIS_PRT_TERM
	  ,IDNT_STTT_PRT_CONS
	  ,ref_sttt_prt_cons.CODE AS LIBL_STTT_PRT_CONS
      ,IDNT_SITT_PRT_CONS
	  ,ref_sitt_prt_cons.CODE AS LIBL_SITT_PRT_CONS
      ,FRS_INSC
      ,DAT_FIN_CONT
      ,DAT_PRMR_ECHN
      ,DAT_FIN_PRV_PRT
      ,NUMR_APPL_FI
      ,NUMR_CONT_FI
      ,MONT_CRDT_SOUS
      ,PRM_ASSR
      ,COUT_TOTL_ASSR
      ,MONT_DERN_ACMP_PAYE_AVC_ASSR
      ,MONT_DERN_ACMP_PAYE_SANS_ASSR
      ,DERN_DAT_ECHN_PAYE
      ,DAT_EXPR_ACTV_PRT
      ,DAT_DEBT_ACTV_PRT
      ,MONT_DECS_PRT
      ,DAT_DECS_PRT
      ,TAUX_NOMN_PRT
      ,NOMB_ECHN_PRT
      ,PRT_EXCP
      ,TAUX_ANNL_EFFC_ASSR
      ,TAUX_ANNL_EFFC_GLBL
      ,VERS_MENS_AVC_ASSR
      ,VERS_MENS_SANS_ASSR
      ,NOMB_VERS_ECHS
      ,NOMB_VERS_REPR
      ,DAT_FIN_PERD_RETR
      ,PERD_TOTL_MOIS_DIFF
      ,CAPT_REST_DU
      ,MONT_TOTL_INTR
      ,NOMB_TOTL_VERS
      ,VERS_VENR_AVC_ASSR
      ,VERS_VENR_SANS_ASSR
      ,DAT_VERS_VENR
      ,DAT_APPR
      ,JOUR_RETR
	  ,CASE WHEN FLG_MAIL_DISP_DECS_ENV=1 THEN 'Oui'
		WHEN FLG_MAIL_DISP_DECS_ENV=0 THEN 'Non'
		END AS FLG_MAIL_DISP_DECS_ENV
  --    ,CASE WHEN FLG_DECS_AUTM=1 THEN 'Oui'
		--WHEN FLG_DECS_AUTM=0 THEN 'Non'
		--END FLG_DECS_AUTM
      ,IDNT_PRLV_AUTM_PAYS
	  ,Country.CODE AS COD_PRLV_AUTM_PAYS
      ,PRLV_AUTM_CL_CTRL
      ,PRLV_AUTM_BBN
	  ,TAUX_DEDC_PRT
      ,COD_TAUX_DEDC_PRT
      ,TAUX_ANNL_EFFC_GLBL_PRV
      ,TAUX_NOMN_PRT_PRV
      ,TAUX_NOMN_PRT_TOTL
      ,DEPT_PRT_CART
      ,DEPT_PRT_AUTR
      ,DAT_DECS_AUTM
      ,TAUX_NOMN_PRT_CLNT
	  ,DAT_SIGN
	  ,FLG_ENRG_COUR
  FROM dbo.DWH_EQPM_CREDIT_CONSO_SL eqpm_cre
  LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REF_FINANCING_TYPE ref_typ_finn
  ON eqpm_cre.IDNT_TYP_FINN = ref_typ_finn.FINANCING_TYPE_ID AND ref_typ_finn.DAT_DEBT_VALD <= eqpm_cre.DAT_OBSR AND eqpm_cre.DAT_OBSR < ref_typ_finn.DAT_FIN_VALD
  LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REF_CONSUMER_LOAN_TERM_REASON ref_rais_prt_term
  ON eqpm_cre.IDNT_RAIS_PRT_TERM = ref_rais_prt_term.REASON_ID AND ref_rais_prt_term.DAT_DEBT_VALD <= eqpm_cre.DAT_OBSR AND eqpm_cre.DAT_OBSR < ref_rais_prt_term.DAT_FIN_VALD
  LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REF_CONSUMER_LOAN_STATUS ref_sttt_prt_cons
  ON eqpm_cre.IDNT_STTT_PRT_CONS = ref_sttt_prt_cons.STATUS_ID AND ref_sttt_prt_cons.DAT_DEBT_VALD <= eqpm_cre.DAT_OBSR AND eqpm_cre.DAT_OBSR < ref_sttt_prt_cons.DAT_FIN_VALD
  LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REF_CONSUMER_LOAN_SITUATION ref_sitt_prt_cons
  ON eqpm_cre.IDNT_SITT_PRT_CONS = ref_sitt_prt_cons.SITUATION_ID AND ref_sitt_prt_cons.DAT_DEBT_VALD <= eqpm_cre.DAT_OBSR AND eqpm_cre.DAT_OBSR < ref_sitt_prt_cons.DAT_FIN_VALD
  LEFT JOIN [$(DatabaseInstanceName)].[$(DataHubDatabaseName)].[dbo].[REF_CODE] Country 
  ON eqpm_cre.IDNT_PRLV_AUTM_PAYS = Country.CODE_ID AND Country.REF_FAMILY_ID = 1
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Observation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'DAT_OBSR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant equipement du produit credit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'IDNT_EQPM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant type de financement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'IDNT_TYP_FINN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé type de financement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'LIBL_TYP_FINN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant raison du pret a terme' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'IDNT_RAIS_PRT_TERM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé raison du pret a terme' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'LIBL_RAIS_PRT_TERM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant statut prêt à la consommation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'IDNT_STTT_PRT_CONS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé statut prêt à la consommation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'LIBL_STTT_PRT_CONS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant situation pret à la consommation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'IDNT_SITT_PRT_CONS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé situation pret à la consommation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'LIBL_SITT_PRT_CONS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Frais d inscription' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'FRS_INSC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date de fin contrat ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'DAT_FIN_CONT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Première date d échéance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'DAT_PRMR_ECHN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date de fin prevue du pret ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'DAT_FIN_PRV_PRT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numero application FI' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'NUMR_APPL_FI'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numero de contrat FI' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'NUMR_CONT_FI'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Montant du credit souscrit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'MONT_CRDT_SOUS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Prime d assurance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'PRM_ASSR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cout total de l assurance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'COUT_TOTL_ASSR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Montant du dernier acompte paye avec assurance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'MONT_DERN_ACMP_PAYE_AVC_ASSR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Montant du dernier acompte paye sans assurance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'MONT_DERN_ACMP_PAYE_SANS_ASSR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Derniere date d echeance payee' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'DERN_DAT_ECHN_PAYE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date d expiration de l activation du pret' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'DAT_EXPR_ACTV_PRT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date de début de l activation du pret' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'DAT_DEBT_ACTV_PRT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Montant du decaissement du pret' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'MONT_DECS_PRT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date de decaissement du pret' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'DAT_DECS_PRT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Taux nominal du pret' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'TAUX_NOMN_PRT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre d echeances du pret' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'NOMB_ECHN_PRT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pret exceptionnel' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'PRT_EXCP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Taux annuel effectif d assurance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'TAUX_ANNL_EFFC_ASSR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Taux annuel effectif global' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'TAUX_ANNL_EFFC_GLBL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Versement mensuel avec assur' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'VERS_MENS_AVC_ASSR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Versement mensuel sans assur' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'VERS_MENS_SANS_ASSR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre de versements echus' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'NOMB_VERS_ECHS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre des versements reportes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'NOMB_VERS_REPR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date de fin période rétraction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'DAT_FIN_PERD_RETR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Periode totale en mois differee' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'PERD_TOTL_MOIS_DIFF'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Capital restant du ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'CAPT_REST_DU'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Montant total des interets' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'MONT_TOTL_INTR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre total des versements' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'NOMB_TOTL_VERS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Versements a venir avec assurance ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'VERS_VENR_AVC_ASSR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Versements a venir sans assurance ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'VERS_VENR_SANS_ASSR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date des versements a venir' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'DAT_VERS_VENR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date d approbation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'DAT_APPR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Jour de retrait' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'JOUR_RETR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag email de disponibilité du decaissement envoyé' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'FLG_MAIL_DISP_DECS_ENV'
GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag décaissement automatique' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'FLG_DECS_AUTM'
--GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant prelevement automatique territoire' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'IDNT_PRLV_AUTM_PAYS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value = N'Code prelevement automatique territoire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_EQPM_CREDIT_CONSO_SL', @level2type = N'COLUMN', @level2name = N'COD_PRLV_AUTM_PAYS';
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Prelevement automatique cle de controle' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'PRLV_AUTM_CL_CTRL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Prelevement automatique BBAN' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'PRLV_AUTM_BBN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Taux déduction du prêt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'TAUX_DEDC_PRT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code taux déduction du prêt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'COD_TAUX_DEDC_PRT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Taux annuel effectif global prev' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'TAUX_ANNL_EFFC_GLBL_PRV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Taux nominal du prêt prev' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'TAUX_NOMN_PRT_PRV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Taux nominal du prêt total' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'TAUX_NOMN_PRT_TOTL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dépôt du prêt par carte' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'DEPT_PRT_CART'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dépôt du prêt autre' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'DEPT_PRT_AUTR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date décaissement automatique' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'DAT_DECS_AUTM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Taux nominal du prêt client' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'TAUX_NOMN_PRT_CLNT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date de signature' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'DAT_SIGN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Enregistrement Courant' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQPM_CREDIT_CONSO_SL', @level2type=N'COLUMN',@level2name=N'FLG_ENRG_COUR'
GO
