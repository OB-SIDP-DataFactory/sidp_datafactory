﻿CREATE VIEW [dbo].[VUE_REQUETE] AS
SELECT
REQ.DAT_OBSR,
REQ.IDNT_REQT,
REQ.NUMR_REQT,
REQ.IDNT_TYP_ENRG,
REQ.IDNT_SF_PERS,
REQ.NUMR_PERS_SF,
REQ.NUMR_CLNT_SAB,
REQ.IDNT_OPPR,
REQ.NUMR_OPPR,
REQ.IDNT_REQT_PRNC,
REQ.IDNT_REQT_TCH,
REQ.IDNT_ENRG_PRNC,
REQ.REQT_ORGN,
REQ.CANL_ORGN as COD_CANL_ORGN,
REF_CANL_ORGN.LIBL_SF as LIBL_CANL_ORGN,
REQ.STTT as COD_STTT,	   
REF_STTT.LIBL_SF as LIBL_STTT,
CASE WHEN REQ.FLG_REQT_TRT = 1 THEN 'Oui'
     WHEN REQ.FLG_REQT_TRT = 0 THEN 'Non'
END AS FLG_REQT_TRT,
CASE WHEN REQ.FLG_REQT_NON_TRT = 1 THEN 'Oui'
     WHEN REQ.FLG_REQT_NON_TRT = 0 THEN 'Non'
END AS FLG_REQT_NON_TRT,
REQ.PRMR_RECL as COD_PRMR_RECL,
REF_PRMR_RECL.LIBL_SF as LIBL_PRMR_RECL,
REQ.RECL_NIV as COD_RECL_NIV,
REF_RECL_NIV.LIBL_SF as LIBL_RECL_NIV,
REQ.OBJT_PRNC as COD_OBJT_PRNC,
REF_OBJT_PRNC.LIBL_SF as LIBL_OBJT_PRNC,
REQ.OBJT_SECN as COD_OBJT_SECN,
REF_OBJT_SECN.LIBL_SF as LIBL_OBJT_SECN,
REQ.TYP_REQT as COD_TYP_REQT,
REF_TYP_REQT.LIBL_SF as LIBL_TYP_REQT,
REQ.SOUS_TYP as COD_SOUS_TYP,
REF_SOUS_TYP.LIBL_SF as LIBL_SOUS_TYP,
REQ.ACT_GEST as COD_ACT_GEST,
REF_ACT_GEST.LIBL_SF as LIBL_ACT_GEST,
REQ.UNVR as COD_UNVR,
REF_UNVR.LIBL_SF as LIBL_UNVR,
REQ.SOUS_UNVR as COD_SOUS_UNVR,
REF_SOUS_UNVR.LIBL_SF as LIBL_SOUS_UNVR,
REQ.PRRT as COD_PRRT,	   
REF_PRRT.LIBL_SF as LIBL_PRRT,
REQ.PRCS as COD_PRCS,
REF_PRCS.LIBL_SF as LIBL_PRCS,
REQ.DAT_CRTN as DAT_CRTN_HMS,
CAST(REQ.DAT_CRTN AS DATE) AS DAT_CRTN,	 
REQ.IDNT_CRTN,
REQ.IDNT_PRPR,
REQ.DAT_FERM as DAT_FERM_HMS,
CAST(REQ.DAT_FERM AS DATE) AS DAT_FERM,
CASE WHEN REQ.FLG_DAT_FERM = 1 THEN 'Oui'
     WHEN REQ.FLG_DAT_FERM = 0 THEN 'Non'
END AS FLG_DAT_FERM,
REQ.DAT_ACCS_RECP,
REQ.CANL_REPN as COD_CANL_REPN,
REF_CANL_REPN.LIBL_SF as LIBL_CANL_REPN,
REQ.DAT_PRCH_ACTN,
REQ.DAT_REPN,
REQ.JALN_DEPS,
REQ.IDNT_DERN_MODF,
REQ.DAT_DERN_MODF,
REQ.CONS_TECH,
REQ.REAL_PAR,
REQ.RESP,
REQ.METR as COD_METR,
REF_METR.LIBL_SF as LIBL_METR,
REQ.PRPR_PRCD,
REQ.FIL_ATTN,
REQ.DAT_TRTM,
REQ.DEL_TRTM,
REQ.DUR,
REQ.DUR_TRTM,
REQ.DUR_TRTM_HEUR,
REQ.MOTF_TRTM as COD_MOTF_TRTM,
REF_MOTF_TRTM.LIBL_SF as LIBL_MOTF_TRTM,
REQ.DUT,
REQ.DAT_PLNF,
REQ.DAT_ATTR_REQT,
REQ.STTT_JALN,
REQ.DECS as COD_DECS,
REF_DECS.LIBL_SF as LIBL_DECS,
REQ.INDM as COD_INDM,
REF_INDM.LIBL_SF as LIBL_INDM,
REQ.MONT_GEST_COMM,
REQ.MONT_PERT,
REQ.MONT_RETR,
REQ.MONT,
REQ.STTT_APPL as COD_STTT_APPL,	   
REF_STTT_APPL.LIBL_SF as LIBL_STTT_APPL,
REQ.DETL_STTT_APPL as COD_DETL_STTT_APPL,	   
REF_DETL_STTT_APPL.LIBL_SF as LIBL_DETL_STTT_APPL,
REQ.IDNT_CAMP,
REQ.PLN_ACTN as COD_PLN_ACTN,
REF_PLN_ACTN.LIBL_SF as LIBL_PLN_ACTN,
REQ.TYPL,
REQ.TYP_CLNT as COD_TYP_CLNT,
REF_TYP_CLNT.LIBL_SF as LIBL_TYP_CLNT,
REQ.NOM_VEND,
REQ.NOM_BOUT,
REQ.HABL,
REQ.[DESC],
REQ.DETL,
REQ.PRCS_DETL,
REQ.REPN,
REQ.QUAL_REPN_OBJT_PRNC as COD_QUAL_REPN_OBJT_PRNC,
REF_QUAL_REPN_OBJT_PRNC.LIBL_SF as LIBL_QUAL_REPN_OBJT_PRNC,
REQ.QUAL_REPN_OBJT_SECN as COD_QUAL_REPN_OBJT_SECN,
REF_QUAL_REPN_OBJT_SECN.LIBL_SF as LIBL_QUAL_REPN_OBJT_SECN,
REQ.COMM,
REQ.COMM_INTR,
CASE WHEN REQ.FLG_LIBR_SERV_COMM = 1 THEN 'Oui'
     WHEN REQ.FLG_LIBR_SERV_COMM = 0 THEN 'Non'
END AS FLG_LIBR_SERV_COMM,
CASE WHEN REQ.FLG_NOUV_COMM_LIBR_SERV = 1 THEN 'Oui'
     WHEN REQ.FLG_NOUV_COMM_LIBR_SERV = 0 THEN 'Non'
END AS FLG_NOUV_COMM_LIBR_SERV,
REQ.MOTF_REVR,
REQ.OBJT,
REQ.NOM,
REQ.ADRS_EML,
REQ.TELP,
REQ.SOCT,
REQ.IDNT_CNTC,
REQ.NUMR_TELP_CNTC,
REQ.TELP_MOBL_CNTC,
REQ.ADRS_EML_CNTC,
REQ.CNTC_TELC,
REQ.TYP_DOCM as COD_TYP_DOCM,
REF_TYP_DOCM.LIBL_SF as LIBL_TYP_DOCM,
REQ.PRSN_PJ,
REQ.VALD_PJ,
REQ.DAT_EXMN_EXPR,
REQ.DAT_DEBT_PRCS_AUTR,
REQ.DAT_FIN_PRCS_AUTR,
REQ.DAT_DEBT_ARRT,
REQ.DAT_DERN_AFFC,
REQ.DAT_DERN_REFR,
REQ.DAT_REVR,
REQ.DAT_LETT_ATTN,
CASE WHEN REQ.FLG_SUPP = 1 THEN 'Oui'
     WHEN REQ.FLG_SUPP = 0 THEN 'Non'
END AS FLG_SUPP,
CASE WHEN REQ.FLG_FERM = 1 THEN 'Oui'
     WHEN REQ.FLG_FERM = 0 THEN 'Non'
END AS FLG_FERM,
CASE WHEN REQ.FLG_ESCL = 1 THEN 'Oui'
     WHEN REQ.FLG_ESCL = 0 THEN 'Non'
END AS FLG_ESCL,
CASE WHEN REQ.FLG_VISB_PORT_LIBR_SERV = 1 THEN 'Oui'
     WHEN REQ.FLG_VISB_PORT_LIBR_SERV = 0 THEN 'Non'
END AS FLG_VISB_PORT_LIBR_SERV,
CASE WHEN REQ.FLG_FERM_UTLS_LIBR_SERV = 1 THEN 'Oui'
     WHEN REQ.FLG_FERM_UTLS_LIBR_SERV = 0 THEN 'Non'
END AS FLG_FERM_UTLS_LIBR_SERV,
CASE WHEN REQ.FLG_FERM_LORS_CRTN = 1 THEN 'Oui'
     WHEN REQ.FLG_FERM_LORS_CRTN = 0 THEN 'Non'
END AS FLG_FERM_LORS_CRTN,
CASE WHEN REQ.FLG_ARRT = 1 THEN 'Oui'
     WHEN REQ.FLG_ARRT = 0 THEN 'Non'
END AS FLG_ARRT,
CASE WHEN REQ.FLG_EMPC_ASSG = 1 THEN 'Oui'
     WHEN REQ.FLG_EMPC_ASSG = 0 THEN 'Non'
END AS FLG_EMPC_ASSG,
CASE WHEN REQ.FLG_FIL_ATTN = 1 THEN 'Oui'
     WHEN REQ.FLG_FIL_ATTN = 0 THEN 'Non'
END AS FLG_FIL_ATTN,
CASE WHEN REQ.FLG_CONF_ABND = 1 THEN 'Oui'
     WHEN REQ.FLG_CONF_ABND = 0 THEN 'Non'
END AS FLG_CONF_ABND,
CASE WHEN REQ.FLG_REQT_TRT_AVC_JALN_DEPS = 1 THEN 'Oui'
     WHEN REQ.FLG_REQT_TRT_AVC_JALN_DEPS = 0 THEN 'Non'
END AS FLG_REQT_TRT_AVC_JALN_DEPS,
CASE WHEN REQ.FLG_REQT_TRTS_AVC_JALN_RESP = 1 THEN 'Oui'
     WHEN REQ.FLG_REQT_TRTS_AVC_JALN_RESP = 0 THEN 'Non'
END AS FLG_REQT_TRTS_AVC_JALN_RESP,
CASE WHEN REQ.FLG_NON_TRT_AVC_JALN_DEPS = 1 THEN 'Oui'
     WHEN REQ.FLG_NON_TRT_AVC_JALN_DEPS = 0 THEN 'Non'
END AS FLG_NON_TRT_AVC_JALN_DEPS,
CASE WHEN REQ.FLG_NON_TRT_AVC_JALN_RESP = 1 THEN 'Oui'
     WHEN REQ.FLG_NON_TRT_AVC_JALN_RESP = 0 THEN 'Non'
END AS FLG_NON_TRT_AVC_JALN_RESP,
REQ.IDNT_ACTF,
REQ.IDNT_PRDT,
REQ.IDNT_AUTR,
REQ.IDNT_QUES,
REQ.IDNT_SOUR,
REQ.IDNT_ZON,
REQ.IDNT_HEUR_OUVR,
REQ.EQPM,
REQ.PIST_AMLR,
REQ.RES,
REQ.ORGN as COD_ORGN,
REF_ORGN.LIBL_SF as LIBL_ORGN,
REQ.MOTF_REQT as COD_MOTF_REQT,	   
REF_MOTF_REQT.LIBL_SF as LIBL_MOTF_REQT,
REQ.EN_ATTN_DE as COD_EN_ATTN_DE,
REF_EN_ATTN_DE.LIBL_SF as LIBL_EN_ATTN_DE,
REQ.HORD_MODF_SYST,
REQ.URL_PHOTO_PRFL_CRTR,
REQ.URL_PHOTO_MINT_CRTR,
REQ.NOM_CRTR,
REQ.AUTH_MANL,
REQ.IDNT_DIX_HUIT_CHRS,
REQ.NOMB_JALN_FERM,
REQ.RESL_AUTH,
REQ.DYSF_BANQ as COD_DYSF_BANQ,
REF_DYSF_BANQ.LIBL_SF as LIBL_DYSF_BANQ,
REQ.TAUX_CONF,
REQ.PLFN_PAIE,
REQ.PLFN_RETR,
REQ.MOTS_INTR,
REQ.VISB_CLNT as COD_VISB_CLNT,
REF_VISB_CLNT.LIBL_SF as LIBL_VISB_CLNT,
CASE WHEN REQ.FLG_MOTS_INTR = 1 THEN 'Oui'
     WHEN REQ.FLG_MOTS_INTR = 0 THEN 'Non'
END AS FLG_MOTS_INTR,
REQ.AUTH,
REQ.COMP_MESS,
REQ.NOUV_MESS,
REQ.PURG,
REQ.TRGG_PASS,
REQ.REFR_TICK_EXTR,
REQ.SPCF as COD_SPCF,
REF_SPCF.LIBL_SF as LIBL_SPCF,
REQ.GRVT_INCD as COD_GRVT_INCD,
REF_GRVT_INCD.LIBL_SF as LIBL_GRVT_INCD,
REQ.EML_SOUR,
REQ.ECHN_CORR_PRV,
REQ.MOTF_ANML_TA3C as COD_MOTF_ANML_TA3C,
REF_MOTF_ANML_TA3C.LIBL_SF as LIBL_MOTF_ANML_TA3C,
REQ.SUIV_EVLT_STTT,
REQ.RELN_CLNT as COD_RELN_CLNT,
REF_RELN_CLNT.LIBL_SF as LIBL_RELN_CLNT,
REQ.PERM as COD_PERM,
REF_PERM.LIBL_SF as LIBL_PERM,
REQ.IDNT_OPPR_DIST_ASSC_TCH,
REQ.RES_DIST_OPPR_ASSC_TCH,
REQ.INCD_COUR,
CASE WHEN REQ.FLG_RECL_COUR_APPR = 1 THEN 'Oui'
     WHEN REQ.FLG_RECL_COUR_APPR = 0 THEN 'Non'
END AS FLG_RECL_COUR_APPR,
REQ.FLG_ENRG_COUR
FROM dbo.DWH_REQUETE REQ
LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF REF_STTT
  ON REQ.STTT = REF_STTT.COD_SF
  AND REF_STTT.COD_OBJ_SF = 'case' AND REF_STTT.COD_CHMP_SF = 'Status' AND REF_STTT.FLG_ACTIF = 1  
LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF REF_STTT_APPL
  ON REQ.STTT_APPL = REF_STTT_APPL.COD_SF
  AND REF_STTT_APPL.COD_OBJ_SF = 'case' AND REF_STTT_APPL.COD_CHMP_SF = 'CallStatus__c' AND REF_STTT_APPL.FLG_ACTIF = 1
LEFT JOIN (SELECT DISTINCT COD_SF, LIBL_SF FROM [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF 
           WHERE COD_OBJ_SF = 'case' AND COD_CHMP_SF = 'CallDetail__c' AND FLG_ACTIF = 1) REF_DETL_STTT_APPL
  ON REQ.DETL_STTT_APPL = REF_DETL_STTT_APPL.COD_SF  
LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF REF_PRRT
  ON REQ.PRRT = REF_PRRT.COD_SF
  AND REF_PRRT.COD_OBJ_SF = 'case' AND REF_PRRT.COD_CHMP_SF = 'Priority' AND REF_PRRT.FLG_ACTIF = 1
LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF REF_CANL_ORGN
  ON REQ.CANL_ORGN = REF_CANL_ORGN.COD_SF
  AND REF_CANL_ORGN.COD_OBJ_SF = 'case' AND REF_CANL_ORGN.COD_CHMP_SF = 'Origin' AND REF_CANL_ORGN.FLG_ACTIF = 1
LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF REF_MOTF_REQT
  ON REQ.MOTF_REQT = REF_MOTF_REQT.COD_SF
  AND REF_MOTF_REQT.COD_OBJ_SF = 'case' AND REF_MOTF_REQT.COD_CHMP_SF = 'Reason' AND REF_MOTF_REQT.FLG_ACTIF = 1
LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF REF_PRMR_RECL
  ON REQ.PRMR_RECL = REF_PRMR_RECL.COD_SF
  AND REF_PRMR_RECL.COD_OBJ_SF = 'case' AND REF_PRMR_RECL.COD_CHMP_SF = 'FirstClaim__c' AND REF_PRMR_RECL.FLG_ACTIF = 1
LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF REF_RECL_NIV
  ON REQ.RECL_NIV = REF_RECL_NIV.COD_SF
  AND REF_RECL_NIV.COD_OBJ_SF = 'case' AND REF_RECL_NIV.COD_CHMP_SF = 'LevelClaim__c' AND REF_RECL_NIV.FLG_ACTIF = 1
LEFT JOIN (SELECT DISTINCT COD_SF, LIBL_SF FROM [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF 
           WHERE COD_OBJ_SF = 'case' AND COD_CHMP_SF = 'PrimarySubject__c' AND FLG_ACTIF = 1) REF_OBJT_PRNC
  ON REQ.OBJT_PRNC = REF_OBJT_PRNC.COD_SF
LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF REF_OBJT_SECN
  ON REQ.OBJT_SECN = REF_OBJT_SECN.COD_SF
  AND REF_OBJT_SECN.COD_OBJ_SF = 'case' AND REF_OBJT_SECN.COD_CHMP_SF = 'SecondarySubject__c' AND REF_OBJT_SECN.FLG_ACTIF = 1
LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF REF_TYP_REQT
  ON REQ.TYP_REQT = REF_TYP_REQT.COD_SF
  AND REF_TYP_REQT.COD_OBJ_SF = 'case' AND REF_TYP_REQT.COD_CHMP_SF = 'Type' AND REF_TYP_REQT.FLG_ACTIF = 1  
LEFT JOIN (SELECT DISTINCT COD_SF, LIBL_SF FROM [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF 
           WHERE COD_OBJ_SF = 'case' AND COD_CHMP_SF = 'SubType__c' AND FLG_ACTIF = 1) REF_SOUS_TYP
  ON REQ.SOUS_TYP = REF_SOUS_TYP.COD_SF  
LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF REF_ACT_GEST
  ON REQ.ACT_GEST = REF_ACT_GEST.COD_SF
  AND REF_ACT_GEST.COD_OBJ_SF = 'case' AND REF_ACT_GEST.COD_CHMP_SF = 'ManagerialAct__c' AND REF_ACT_GEST.FLG_ACTIF = 1
LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF REF_UNVR
  ON REQ.UNVR = REF_UNVR.COD_SF
  AND REF_UNVR.COD_OBJ_SF = 'case' AND REF_UNVR.COD_CHMP_SF = 'Universe__c' AND REF_UNVR.FLG_ACTIF = 1
LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF REF_SOUS_UNVR
  ON REQ.SOUS_UNVR = REF_SOUS_UNVR.COD_SF
  AND REF_SOUS_UNVR.COD_OBJ_SF = 'case' AND REF_SOUS_UNVR.COD_CHMP_SF = 'SubUniverse__c' AND REF_SOUS_UNVR.FLG_ACTIF = 1
LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF REF_PRCS
  ON REQ.PRCS = REF_PRCS.COD_SF
  AND REF_PRCS.COD_OBJ_SF = 'case' AND REF_PRCS.COD_CHMP_SF = 'Process__c' AND REF_PRCS.FLG_ACTIF = 1
LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF REF_CANL_REPN
  ON REQ.CANL_REPN = REF_CANL_REPN.COD_SF
  AND REF_CANL_REPN.COD_OBJ_SF = 'case' AND REF_CANL_REPN.COD_CHMP_SF = 'ResponseChannel__c' AND REF_CANL_REPN.FLG_ACTIF = 1
LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF REF_METR
  ON REQ.METR = REF_METR.COD_SF
  AND REF_METR.COD_OBJ_SF = 'case' AND REF_METR.COD_CHMP_SF = 'Metier__c' AND REF_METR.FLG_ACTIF = 1
LEFT JOIN (SELECT DISTINCT COD_SF, LIBL_SF FROM [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF
           WHERE COD_OBJ_SF = 'case' AND COD_CHMP_SF = 'TreatementReason__c' AND FLG_ACTIF = 1) REF_MOTF_TRTM
  ON REQ.MOTF_TRTM = REF_MOTF_TRTM.COD_SF
LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF REF_INDM
  ON REQ.INDM = REF_INDM.COD_SF
  AND REF_INDM.COD_OBJ_SF = 'case' AND REF_INDM.COD_CHMP_SF = 'Indemnity__c' AND REF_INDM.FLG_ACTIF = 1
LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF REF_PLN_ACTN
  ON REQ.PLN_ACTN = REF_PLN_ACTN.COD_SF
  AND REF_PLN_ACTN.COD_OBJ_SF = 'case' AND REF_PLN_ACTN.COD_CHMP_SF = 'ActionPlan__c' AND REF_PLN_ACTN.FLG_ACTIF = 1
LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF REF_TYP_CLNT
  ON REQ.TYP_CLNT = REF_TYP_CLNT.COD_SF
  AND REF_TYP_CLNT.COD_OBJ_SF = 'case' AND REF_TYP_CLNT.COD_CHMP_SF = 'CustomerType__c' AND REF_TYP_CLNT.FLG_ACTIF = 1
LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF REF_SPCF
  ON REQ.SPCF = REF_SPCF.COD_SF
  AND REF_SPCF.COD_OBJ_SF = 'case' AND REF_SPCF.COD_CHMP_SF = 'Specification__c' AND REF_SPCF.FLG_ACTIF = 1
LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF REF_RELN_CLNT
  ON REQ.RELN_CLNT = REF_RELN_CLNT.COD_SF
  AND REF_RELN_CLNT.COD_OBJ_SF = 'case' AND REF_RELN_CLNT.COD_CHMP_SF = 'Relance_client__c' AND REF_RELN_CLNT.FLG_ACTIF = 1
LEFT JOIN (SELECT DISTINCT COD_SF, LIBL_SF FROM [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF 
           WHERE COD_OBJ_SF = 'case' AND COD_CHMP_SF = 'Decision__c' AND FLG_ACTIF = 1) REF_DECS
  ON REQ.DECS = REF_DECS.COD_SF  
LEFT JOIN (SELECT DISTINCT COD_SF, LIBL_SF FROM [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF 
           WHERE COD_OBJ_SF = 'case' AND COD_CHMP_SF = 'StandBy__c' AND FLG_ACTIF = 1) REF_EN_ATTN_DE
  ON REQ.EN_ATTN_DE = REF_EN_ATTN_DE.COD_SF  
LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF REF_TYP_DOCM
  ON REQ.TYP_DOCM = REF_TYP_DOCM.COD_SF
  AND REF_TYP_DOCM.COD_OBJ_SF = 'case' AND REF_TYP_DOCM.COD_CHMP_SF = 'DocumentType__c' AND REF_TYP_DOCM.FLG_ACTIF = 1
LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF REF_ORGN
  ON REQ.ORGN = REF_ORGN.COD_SF
  AND REF_ORGN.COD_OBJ_SF = 'case' AND REF_ORGN.COD_CHMP_SF = 'Origin__c' AND REF_ORGN.FLG_ACTIF = 1
LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF REF_VISB_CLNT
  ON REQ.VISB_CLNT = REF_VISB_CLNT.COD_SF
  AND REF_VISB_CLNT.COD_OBJ_SF = 'case' AND REF_VISB_CLNT.COD_CHMP_SF = 'ClientVisibility__c' AND REF_VISB_CLNT.FLG_ACTIF = 1
LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF REF_PERM
  ON REQ.PERM = REF_PERM.COD_SF
  AND REF_PERM.COD_OBJ_SF = 'case' AND REF_PERM.COD_CHMP_SF = 'Perimeter__c' AND REF_PERM.FLG_ACTIF = 1
LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF REF_QUAL_REPN_OBJT_PRNC
  ON REQ.QUAL_REPN_OBJT_PRNC = REF_QUAL_REPN_OBJT_PRNC.COD_SF
  AND REF_QUAL_REPN_OBJT_PRNC.COD_OBJ_SF = 'case' AND REF_QUAL_REPN_OBJT_PRNC.COD_CHMP_SF = 'PrimaryQualification__c' AND REF_QUAL_REPN_OBJT_PRNC.FLG_ACTIF = 1
LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF REF_MOTF_ANML_TA3C
  ON REQ.MOTF_ANML_TA3C = REF_MOTF_ANML_TA3C.COD_SF
  AND REF_MOTF_ANML_TA3C.COD_OBJ_SF = 'case' AND REF_MOTF_ANML_TA3C.COD_CHMP_SF = 'ReasonAnomalyTA3C__c' AND REF_MOTF_ANML_TA3C.FLG_ACTIF = 1
LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF REF_GRVT_INCD
  ON REQ.GRVT_INCD = REF_GRVT_INCD.COD_SF
  AND REF_GRVT_INCD.COD_OBJ_SF = 'case' AND REF_GRVT_INCD.COD_CHMP_SF = 'IncidentGravity__c' AND REF_GRVT_INCD.FLG_ACTIF = 1
LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF REF_QUAL_REPN_OBJT_SECN
  ON REQ.QUAL_REPN_OBJT_SECN = REF_QUAL_REPN_OBJT_SECN.COD_SF
  AND REF_QUAL_REPN_OBJT_SECN.COD_OBJ_SF = 'case' AND REF_QUAL_REPN_OBJT_SECN.COD_CHMP_SF = 'SecondQualification__c' AND REF_QUAL_REPN_OBJT_SECN.FLG_ACTIF = 1
LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SF REF_DYSF_BANQ
  ON REQ.DYSF_BANQ = REF_DYSF_BANQ.COD_SF
  AND REF_DYSF_BANQ.COD_OBJ_SF = 'case' AND REF_DYSF_BANQ.COD_CHMP_SF = 'Dysfunction__c' AND REF_DYSF_BANQ.FLG_ACTIF = 1
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Enregistrement Courant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Périmètre', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_PERM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Périmètre', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_PERM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Relance client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_RELN_CLNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Relance client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_RELN_CLNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Suivi Evolution du Statut', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'SUIV_EVLT_STTT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Motifs Anomalie TA3C', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_MOTF_ANML_TA3C';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Motifs Anomalie TA3C', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_MOTF_ANML_TA3C';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Echéance de correction prévue', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'ECHN_CORR_PRV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Email source', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'EML_SOUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Gravité de l''incident', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_GRVT_INCD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Gravité de l''incident', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_GRVT_INCD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Spécification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_SPCF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Spécification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_SPCF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Référence Ticket Externe', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'REFR_TICK_EXTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Trigger Pass', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'TRGG_PASS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Purge', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'PURG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nouveau Message', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'NOUV_MESS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Compteur de message', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COMP_MESS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Authentification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'AUTH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Mots Interdits', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_MOTS_INTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Visibilité Client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_VISB_CLNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Visibilité Client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_VISB_CLNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mots interdits', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'MOTS_INTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Plafond Retrait', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'PLFN_RETR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Plafond Paiement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'PLFN_PAIE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux de confiance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'TAUX_CONF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Dysfonctionnement Banque', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_DYSF_BANQ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Dysfonctionnement Banque', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_DYSF_BANQ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Résultat de l''authentification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'RESL_AUTH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre jalons fermés', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'NOMB_JALN_FERM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Dix huit Chars ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_DIX_HUIT_CHRS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Authentification Manuelle', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'AUTH_MANL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom du créateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'NOM_CRTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'URL de la photo miniature du créateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'URL_PHOTO_MINT_CRTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'URL de la photo de profil du créateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'URL_PHOTO_PRFL_CRTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Horodateur modification systeme', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'HORD_MODF_SYST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé En Attente de', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_EN_ATTN_DE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code En Attente de', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_EN_ATTN_DE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Motif de la requête', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_MOTF_REQT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Motif de la requête', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_MOTF_REQT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Origine', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_ORGN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Origine', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_ORGN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Réseau', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'RES';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Piste d''amélioration', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'PIST_AMLR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Equipement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'EQPM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant d''heures d''ouverture', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_HEUR_OUVR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de zone', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_ZON';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant source', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_SOUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de question', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_QUES';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant d''autorisation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_AUTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant produit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_PRDT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de l''actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_ACTF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Non traité avec jalon respecté', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_NON_TRT_AVC_JALN_RESP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Non traité avec jalon dépassé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_NON_TRT_AVC_JALN_DEPS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Requetes traitees avec jalon respecté', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_REQT_TRTS_AVC_JALN_RESP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Requête traitée avec jalon dépassé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_REQT_TRT_AVC_JALN_DEPS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Confirmer l''abandon', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_CONF_ABND';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag File d''attente', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_FIL_ATTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Empêche Assignation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_EMPC_ASSG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag arrêt', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_ARRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag fermeture lors de la création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_FERM_LORS_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag fermeture par l''utilisateur du libre service', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_FERM_UTLS_LIBR_SERV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Visible dans le portail libre service', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_VISB_PORT_LIBR_SERV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Escaladée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_ESCL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag fermée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_FERM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag supprimé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_SUPP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date lettre d''attente', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_LETT_ATTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date à revoir', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_REVR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere reference', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_REFR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernier affichage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_AFFC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date debut arrêt', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_DEBT_ARRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de fin du processus d''autorisation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_FIN_PRCS_AUTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de début du processus d''autorisation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_DEBT_PRCS_AUTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date examen expiré', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_EXMN_EXPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Validation PJ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'VALD_PJ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Présence PJ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'PRSN_PJ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Type Document', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_TYP_DOCM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Type Document', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_TYP_DOCM';
GO

GO

GO

GO

GO

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Société', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'SOCT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Téléphone', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'TELP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Adresse email', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'ADRS_EML';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'NOM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Objet', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'OBJT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motif à revoir', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'MOTF_REVR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Nouveau commentaire Libre service', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_NOUV_COMM_LIBR_SERV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Libre service commenté', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_LIBR_SERV_COMM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Commentaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COMM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Qualification réponse objet secondaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_QUAL_REPN_OBJT_SECN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Qualification réponse objet secondaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_QUAL_REPN_OBJT_SECN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Qualification réponse objet principal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_QUAL_REPN_OBJT_PRNC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Qualification réponse objet principal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_QUAL_REPN_OBJT_PRNC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Reponse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'REPN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Détails', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'DETL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Description', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'DESC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Habilitation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'HABL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom de la boutique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'NOM_BOUT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom vendeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'NOM_VEND';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Type de client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_TYP_CLNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Type de client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_TYP_CLNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Typologie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'TYPL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Plan d''action', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_PLN_ACTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Plan d''action', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_PLN_ACTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de la Campagne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_CAMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Détail Statut Appel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_DETL_STTT_APPL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Détail Statut Appel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_DETL_STTT_APPL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Statut de l''appel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_STTT_APPL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Statut de l''appel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_STTT_APPL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'MONT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant rétrocession', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'MONT_RETR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant perte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'MONT_PERT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant geste commercial', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'MONT_GEST_COMM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Indemnisation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_INDM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Indemnisation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_INDM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Décision', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_DECS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Décision', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_DECS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Statut du jalon', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'STTT_JALN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date d''attribution de la requête', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_ATTR_REQT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de planification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_PLNF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DUT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'DUT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Motif de traitement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_MOTF_TRTM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Motif de traitement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_MOTF_TRTM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Durée traitement Heures', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'DUR_TRTM_HEUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Durée de traitement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'DUR_TRTM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Durée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'DUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Délai de traitement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'DEL_TRTM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de traitement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_TRTM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'File d''attente', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'FIL_ATTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Propriétaire précédent', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'PRPR_PRCD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Métier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_METR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Métier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_METR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Responsable', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'RESP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Réalisé par', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'REAL_PAR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Conseiller technique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'CONS_TECH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_DERN_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Jalon dépassé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'JALN_DEPS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de la réponse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_REPN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de prochaine action', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_PRCH_ACTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Canal réponse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_CANL_REPN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Canal réponse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_CANL_REPN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Accusé Réception', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_ACCS_RECP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de fermeture', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_FERM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant du propriétaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_PRPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Processus', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_PRCS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Processus', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_PRCS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Priorité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_PRRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Priorité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_PRRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Sous Univers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_SOUS_UNVR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Sous Univers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_SOUS_UNVR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Univers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_UNVR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Univers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_UNVR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Acte de Gestion', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_ACT_GEST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Acte de Gestion', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_ACT_GEST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Sous Type', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_SOUS_TYP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Sous Type', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_SOUS_TYP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Type de requête', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_TYP_REQT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Type de requête', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_TYP_REQT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Objet secondaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_OBJT_SECN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Objet secondaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_OBJT_SECN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Objet principal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_OBJT_PRNC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Objet principal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_OBJT_PRNC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Réclamation niveau', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_RECL_NIV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Réclamation niveau', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_RECL_NIV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Première réclamation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_PRMR_RECL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Première réclamation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_PRMR_RECL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Requete Non Traitee', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_REQT_NON_TRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Requete Traitee', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_REQT_TRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Statut', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_STTT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Statut', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_STTT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Canal d''origine', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'LIBL_CANL_ORGN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Canal d''origine', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COD_CANL_ORGN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Requête d''origine', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'REQT_ORGN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Requête TCH', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_REQT_TCH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de la requête principale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_REQT_PRNC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro de l''opportunité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'NUMR_OPPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de l''opportunité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_OPPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Client SAB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'NUMR_CLNT_SAB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'NUMR_PERS_SF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant SF personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_SF_PERS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant du type d''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_TYP_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro de la requête', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'NUMR_REQT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de la requête', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_REQT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date et heure de fermeture', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_FERM_HMS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date et heure de création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_HMS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Précision détails', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'PRCS_DETL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag date fermeture', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_DAT_FERM';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TCHOptDistributorNetwork', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'RES_DIST_OPPR_ASSC_TCH';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TCHOppIdProcessSous', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_OPPR_DIST_ASSC_TCH';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant d''enregistrement principal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_ENRG_PRNC';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Commentaires internes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'COMM_INTR';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Incident en Cours', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'INCD_COUR';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag réclamation en cours d''approbation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'FLG_RECL_COUR_APPR';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Téléphone mobile du contact', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'TELP_MOBL_CNTC';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro de téléphone du contact', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'NUMR_TELP_CNTC';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant du contact', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'IDNT_CNTC';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Contact Télécopie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'CNTC_TELC';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Adresse email du contact', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_REQUETE', @level2type = N'COLUMN', @level2name = N'ADRS_EML_CNTC';

