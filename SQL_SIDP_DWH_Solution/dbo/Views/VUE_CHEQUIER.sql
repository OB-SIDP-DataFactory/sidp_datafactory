﻿CREATE VIEW [dbo].[VUE_CHEQUIER]
AS
SELECT
	chq.DAT_OBSR,
	chq.COD_ETBL,
	REF_ETBL.Col2 AS LIBL_ETBL,
	chq.COD_AGNC_STCK,
	chq.COD_AGNC,
	REF_AGENCE.Col5 AS LIBL_AGNC,
	chq.NUMR_COMP,
	chq.TYP_CHQR,
	chq.DAT_DEMN,
	chq.NUMR_SEQN,
	chq.DAT_RECP_CHQR,
	chq.DAT_REMS_CHQR,
	chq.COD_LIVR,
	chq.DAT_SUPP,
	chq.COD_ETT_RENV,
	chq.DAT_RENV,
	chq.ORGN_CHQR,
	chq.FLG_ENRG_COUR
FROM
	dbo.DWH_CHEQUIER chq	WITH(NOLOCK)
	LEFT JOIN
	(
		SELECT
			[ID_REFERENTIEL_SA], [SOUR_DONN], [LIBL_SOUR_DONN], [Col1], [Col2], [Col3], [Col4], [Col5], [DAT_CRTN_ENRG], [DAT_DERN_MODF_ENRG], [DAT_DEBT_VALD], [DAT_FIN_VALD], [FLG_ACTIF]
		FROM
			dbo.REFERENTIEL_SA WITH(NOLOCK)
		WHERE
			SOUR_DONN = 'ZDWHETB0'
	) AS REF_ETBL
		ON	chq.COD_ETBL = REF_ETBL.Col1
			AND	REF_ETBL.DAT_DEBT_VALD <= chq.DAT_OBSR
			AND	chq.DAT_OBSR < REF_ETBL.DAT_FIN_VALD
	LEFT JOIN
	(
		SELECT
			[ID_REFERENTIEL_SA], [SOUR_DONN], [LIBL_SOUR_DONN], [Col1], [Col2], [Col3], [Col4], [Col5], [DAT_CRTN_ENRG], [DAT_DERN_MODF_ENRG], [DAT_DEBT_VALD], [DAT_FIN_VALD], [FLG_ACTIF]
		FROM
			dbo.REFERENTIEL_SA WITH(NOLOCK)
		WHERE
			SOUR_DONN = 'ZDWHAGE0'
	) AS REF_AGENCE
		ON
			chq.COD_AGNC = REF_AGENCE.Col2
			AND	REF_AGENCE.DAT_DEBT_VALD <= chq.DAT_OBSR
			AND	chq.DAT_OBSR < REF_AGENCE.DAT_FIN_VALD
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Observation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CHEQUIER', @level2type=N'COLUMN',@level2name=N'DAT_OBSR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Etablissement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CHEQUIER', @level2type=N'COLUMN',@level2name=N'COD_ETBL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé Etablissement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CHEQUIER', @level2type=N'COLUMN',@level2name=N'LIBL_ETBL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Agence Stock' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CHEQUIER', @level2type=N'COLUMN',@level2name=N'COD_AGNC_STCK'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Agence Compte' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CHEQUIER', @level2type=N'COLUMN',@level2name=N'COD_AGNC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numéro Compte' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CHEQUIER', @level2type=N'COLUMN',@level2name=N'NUMR_COMP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type Chéquier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CHEQUIER', @level2type=N'COLUMN',@level2name=N'TYP_CHQR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Demande' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CHEQUIER', @level2type=N'COLUMN',@level2name=N'DAT_DEMN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numéro Séquence' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CHEQUIER', @level2type=N'COLUMN',@level2name=N'NUMR_SEQN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Réception Chéquier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CHEQUIER', @level2type=N'COLUMN',@level2name=N'DAT_RECP_CHQR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Remise Chéquier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CHEQUIER', @level2type=N'COLUMN',@level2name=N'DAT_REMS_CHQR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Livraison' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CHEQUIER', @level2type=N'COLUMN',@level2name=N'COD_LIVR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Suppression' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CHEQUIER', @level2type=N'COLUMN',@level2name=N'DAT_SUPP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Etat Renouvellement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CHEQUIER', @level2type=N'COLUMN',@level2name=N'COD_ETT_RENV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Renouvellement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CHEQUIER', @level2type=N'COLUMN',@level2name=N'DAT_RENV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Origine Chéquier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CHEQUIER', @level2type=N'COLUMN',@level2name=N'ORGN_CHQR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Enregistrement Courant' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CHEQUIER', @level2type=N'COLUMN',@level2name=N'FLG_ENRG_COUR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Code Agence Compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_CHEQUIER', @level2type = N'COLUMN', @level2name = N'LIBL_AGNC'
GO