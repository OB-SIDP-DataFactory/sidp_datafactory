﻿CREATE VIEW [dbo].[VUE_SERVICE] AS
 SELECT SERV.DAT_OBSR,
        SERV.NUMR_ABNN,
		SERV.COD_SERV_SAB,
        REF_SERV_SAB.Col5 as LIBL_SERV_SAB,
        SERV.COD_ETBL,
        SERV.COD_AGNC,
        REF_AGENCE.Col5 AS LIBL_AGNC,
        SERV.COD_SERV,
		REF_SERV.Col5 as LIBL_SERV,
        SERV.COD_SOUS as COD_SOUS_SERV,
		REF_SOUS_SERV.Col5 as LIBL_SOUS_SERV,		
		SERV.COD_ETT,
        SERV.DAT_ADHS,
        SERV.DAT_FIN,
        SERV.DAT_RENV,        
        SERV.DAT_RESL,
        SERV.MOTF_RESL as COD_MOTF_RESL,
        REF_MOTF_RESL.Col5 as LIBL_MOTF_RESL,
        SERV.DAT_CRTN,
        SERV.DAT_VALDTN,
		SERV.FLG_ENRG_COUR
 FROM  dbo.DWH_SERVICE SERV	WITH(NOLOCK)
 LEFT JOIN (SELECT distinct Col2, Col5, DAT_DEBT_VALD, DAT_FIN_VALD FROM dbo.REFERENTIEL_SA	WITH(NOLOCK) where SOUR_DONN = 'ZDWHAGE0') AS REF_AGENCE
   ON SERV.COD_AGNC = REF_AGENCE.Col2 AND REF_AGENCE.DAT_DEBT_VALD <= SERV.DAT_OBSR AND SERV.DAT_OBSR < REF_AGENCE.DAT_FIN_VALD
 LEFT JOIN (SELECT distinct Col2, Col5, DAT_DEBT_VALD, DAT_FIN_VALD FROM dbo.REFERENTIEL_SA	WITH(NOLOCK) where SOUR_DONN = 'ZDWH0180' and Col3 = '000') AS REF_SERV_SAB
   ON SERV.COD_SERV_SAB = REF_SERV_SAB.Col2 AND REF_SERV_SAB.DAT_DEBT_VALD <= SERV.DAT_OBSR AND SERV.DAT_OBSR < REF_SERV_SAB.DAT_FIN_VALD
 LEFT JOIN (SELECT distinct Col3, Col5, DAT_DEBT_VALD, DAT_FIN_VALD FROM dbo.REFERENTIEL_SA	WITH(NOLOCK) where SOUR_DONN = 'ZDWH0210') AS REF_MOTF_RESL
   ON SERV.MOTF_RESL = REF_MOTF_RESL.Col3 AND REF_MOTF_RESL.DAT_DEBT_VALD <= SERV.DAT_OBSR AND SERV.DAT_OBSR < REF_MOTF_RESL.DAT_FIN_VALD
 LEFT JOIN (SELECT distinct Col3, Col5, DAT_DEBT_VALD, DAT_FIN_VALD FROM dbo.REFERENTIEL_SA	WITH(NOLOCK) where SOUR_DONN = 'ZDWHSER0') AS REF_SERV
   ON SERV.COD_SERV = REF_SERV.Col3 AND REF_SERV.DAT_DEBT_VALD <= SERV.DAT_OBSR AND SERV.DAT_OBSR < REF_SERV.DAT_FIN_VALD
 LEFT JOIN (SELECT distinct Col3, Col5, DAT_DEBT_VALD, DAT_FIN_VALD FROM dbo.REFERENTIEL_SA	WITH(NOLOCK) where SOUR_DONN = 'ZDWHSES0') AS REF_SOUS_SERV
   ON SERV.COD_SOUS = REF_SOUS_SERV.Col3 AND REF_SOUS_SERV.DAT_DEBT_VALD <= SERV.DAT_OBSR AND SERV.DAT_OBSR < REF_SOUS_SERV.DAT_FIN_VALD
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Observation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_SERVICE', @level2type=N'COLUMN',@level2name=N'DAT_OBSR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Etablissement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_SERVICE', @level2type=N'COLUMN',@level2name=N'COD_ETBL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Agence' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_SERVICE', @level2type=N'COLUMN',@level2name=N'COD_AGNC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé Agence' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_SERVICE', @level2type=N'COLUMN',@level2name=N'LIBL_AGNC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_SERVICE', @level2type=N'COLUMN',@level2name=N'COD_SERV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_SERVICE', @level2type=N'COLUMN',@level2name=N'LIBL_SERV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Sous-Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_SERVICE', @level2type=N'COLUMN',@level2name=N'COD_SOUS_SERV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé Sous-Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_SERVICE', @level2type=N'COLUMN',@level2name=N'LIBL_SOUS_SERV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Service SAB' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_SERVICE', @level2type=N'COLUMN',@level2name=N'COD_SERV_SAB'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé Service SAB' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_SERVICE', @level2type=N'COLUMN',@level2name=N'LIBL_SERV_SAB'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Adhésion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_SERVICE', @level2type=N'COLUMN',@level2name=N'DAT_ADHS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Fin' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_SERVICE', @level2type=N'COLUMN',@level2name=N'DAT_FIN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date renouvellement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_SERVICE', @level2type=N'COLUMN',@level2name=N'DAT_RENV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Etat' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_SERVICE', @level2type=N'COLUMN',@level2name=N'COD_ETT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Résiliation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_SERVICE', @level2type=N'COLUMN',@level2name=N'DAT_RESL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Motif Résiliation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_SERVICE', @level2type=N'COLUMN',@level2name=N'COD_MOTF_RESL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé Motif Résiliation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_SERVICE', @level2type=N'COLUMN',@level2name=N'LIBL_MOTF_RESL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Création' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_SERVICE', @level2type=N'COLUMN',@level2name=N'DAT_CRTN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Validation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_SERVICE', @level2type=N'COLUMN',@level2name=N'DAT_VALDTN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Enregistrement Courant' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_SERVICE', @level2type=N'COLUMN',@level2name=N'FLG_ENRG_COUR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Abonnement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_SERVICE', @level2type = N'COLUMN', @level2name = N'NUMR_ABNN';
GO