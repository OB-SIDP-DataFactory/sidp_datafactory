﻿CREATE VIEW [dbo].[VUE_CARTE]
AS
SELECT
	car.DAT_OBSR
	,car.COD_ETBL
	,car.COD_CART
	,car.LIBL_CART
	,car.NUMR_COMP
	,car.NUMR_SEQN
	,car.NUMR_PORT
	,car.TYP_PORT
	,car.NATR_CART
	,car.TYP_CART	  
	,car.COD_ETT_CART
	,ref_ett_cart.LIB_CARTE_CODE_ETAT as LIBL_ETT_CART	  
	,car.DAT_COMM_CART
	,car.DAT_RECP_CART
	,car.DAT_REMS_CART	  
	,car.DAT_INVL	  	  
	,car.COD_OPTN_DEBIT
	,ref_optn_debit.COL5 AS LIBL_OPTN_DEBIT
	,car.UTLS
	,car.DAT_VALD_CART
	,car.DUR_VALD_CART
	,car.AGNC_GEST
	,CASE WHEN car.FLG_MIS_HORS_SERV = 1 THEN 'Oui'
	WHEN car.FLG_MIS_HORS_SERV = 0 THEN 'Non'
	END AS FLG_MIS_HORS_SERV
	,CASE WHEN car.FLG_CART_CR_MOIS = 1 THEN 'Oui'
	WHEN car.FLG_CART_CR_MOIS = 0 THEN 'Non'
	END AS FLG_CART_CR_MOIS    
	,CASE WHEN car.FLG_CART_RENV_MOIS = 1 THEN 'Oui'
	WHEN car.FLG_CART_RENV_MOIS = 0 THEN 'Non'
	END AS FLG_CART_RENV_MOIS
	,CASE WHEN car.FLG_CART_RESL_MOIS = 1 THEN 'Oui'
	WHEN car.FLG_CART_RESL_MOIS = 0 THEN 'Non'
	END AS FLG_CART_RESL_MOIS  
	,car.DAT_RENV
	,car.DAT_RESL
	,car.DAT_ENV_FACN	  
	,CASE WHEN car.FLG_RENV_AUTM = 1 THEN 'Oui'
	WHEN car.FLG_RENV_AUTM = 0 THEN 'Non'
	END AS FLG_RENV_AUTM	  
	,car.COD_LIVR
	,car.MONT_PLFN_ACHT
	,car.MONT_PLFN_RETR
	,car.TYP_VISL
	--,NUMR_ANCN_CART  
	,CASE WHEN car.FLG_CRTN_AUTM = 1 THEN 'Oui'
	WHEN car.FLG_CRTN_AUTM = 0 THEN 'Non'
	END AS FLG_CRTN_AUTM  
	,car.FLG_ENRG_COUR
FROM
	dbo.DWH_CARTE car	WITH(NOLOCK)
	LEFT JOIN
	(
		SELECT
			[ID_REFERENTIEL_SA],
			[SOUR_DONN],
			[LIBL_SOUR_DONN],
			[COL1],
			[COL2],
			[COL3],
			[COL4],
			[COL5],
			[DAT_CRTN_ENRG],
			[DAT_DERN_MODF_ENRG],
			[DAT_DEBT_VALD],
			[DAT_FIN_VALD],
			[FLG_ACTIF]
		FROM
			[$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REFERENTIEL_SA	WITH(NOLOCK)
		WHERE
			SOUR_DONN = 'ZDWH0030'
	) AS ref_optn_debit
		ON car.COD_OPTN_DEBIT = ref_optn_debit.COL3
		AND ref_optn_debit.DAT_DEBT_VALD <= car.DAT_OBSR
		AND car.DAT_OBSR < ref_optn_debit.DAT_FIN_VALD
	LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REF_CARTE_CODE_ETAT ref_ett_cart	WITH(NOLOCK)
		ON car.COD_ETT_CART = ref_ett_cart.CARTE_CODE_ETAT
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Observation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'DAT_OBSR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Etablissement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'COD_ETBL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Carte' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'COD_CART'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numéro Compte' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'NUMR_COMP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numéro Séquence' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'NUMR_SEQN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numéro Porteur' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'NUMR_PORT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type Porteur' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'TYP_PORT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé Carte' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'LIBL_CART'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nature Carte' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'NATR_CART'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type Carte' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'TYP_CART'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Option Débit (I:Immédiat / D:Différé)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'COD_OPTN_DEBIT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé Option Débit (I:Immédiat / D:Différé)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'LIBL_OPTN_DEBIT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Utilisation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'UTLS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Validité Carte' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'DAT_VALD_CART'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Durée Validité Carte' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'DUR_VALD_CART'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Agence Gestionnaire' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'AGNC_GEST'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Mise Hors Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'FLG_MIS_HORS_SERV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Commande Carte' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'DAT_COMM_CART'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date réception Carte' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'DAT_RECP_CART'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Remise Carte' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'DAT_REMS_CART'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Carte crée mois' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'FLG_CART_CR_MOIS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Carte renouvelé mois' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'FLG_CART_RENV_MOIS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Carte résilie dans mois' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'FLG_CART_RESL_MOIS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Renouvellement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'DAT_RENV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Résiliant' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'DAT_RESL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Envoi au façonnier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'DAT_ENV_FACN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Renouvellement Automatique' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'FLG_RENV_AUTM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Livraison' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'COD_LIVR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Montant Plafond Achat' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'MONT_PLFN_ACHT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Montant Plafond Retrait' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'MONT_PLFN_RETR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Etat Carte' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'COD_ETT_CART'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé Etat Carte' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'LIBL_ETT_CART'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type Visuel' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'TYP_VISL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag  Création Automatique' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'FLG_CRTN_AUTM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Invalidité' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'DAT_INVL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Enregistrement Courant' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_CARTE', @level2type=N'COLUMN',@level2name=N'FLG_ENRG_COUR'
GO