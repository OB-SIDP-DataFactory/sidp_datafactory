﻿

CREATE VIEW [dbo].[VUE_LIEN_EQUIPEMENT_CREDIT_CONSO] AS
  SELECT DAT_OBSR,
         NUMR_COMP_SAB,
         REFR_CONT,
         NUMR_CLNT_SAB,
         NUMR_CLNT_COLL,
         NUMR_OPPR,
		 CASE FLG_TITL_PRCP WHEN 1 THEN 'Oui' WHEN 0 THEN 'Non' END AS FLG_TITL_PRCP,
		 CASE FLG_TIER_COLL WHEN 1 THEN 'Oui' WHEN 0 THEN 'Non' END AS FLG_TIER_COLL,
		 FLG_ENRG_COUR
  FROM dbo.DWH_LIEN_EQUIPEMENT_CREDIT_CONSO
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Tiers Collectif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'FLG_TIER_COLL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Titulaire Pricipal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'FLG_TITL_PRCP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Opportunité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NUMR_OPPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Client Collectif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NUMR_CLNT_COLL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Client SAB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NUMR_CLNT_SAB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Reference contrat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'REFR_CONT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Compte SAB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'NUMR_COMP_SAB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Enregistrement Courant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_EQUIPEMENT_CREDIT_CONSO', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';

