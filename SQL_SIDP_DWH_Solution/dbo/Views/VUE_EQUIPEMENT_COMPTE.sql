﻿CREATE VIEW [dbo].[VUE_EQUIPEMENT_COMPTE] AS 
  SELECT
	   DAT_OBSR,
	   COD_ETBL,
	   NUMR_PLAN,
	   COD_AGNC,
	   REF_AGNC.AGENCE_LIBELLE as LIBL_AGNC,
	   NUMR_COMP,
	   COD_RUBR,
	   TYP_COMP,
	   COD_PRDT_SF,
	   COD_APPOR,
	   INTITULE,
	   NOMB_TITL,
	   NUMR_CLNT_SAB_TITL_PRCP as NUMR_CLNT_COLL,
	   DAT_OUVR,
	   DAT_CLTR,
	   MOTF_CLTR,
	   COD_DEVS,
	   SOLD_DAT_OPE,
	   SOLD_DAT_VALR,
	   SOLD_DAT_COMP,
	   SOLD_DAT_COMP_DEVS_COMP,
	   DUR_REST_COUR_JOUR,
	   DUR_INTL_JOUR,
	   GARN_DEVS_OPE,
	   GARN_DEVS_BASE,
	   COD_GARN,
	   GARANT,
	   COD_QUAL_GARN,
	   COD_RESD_GARN,
	   NON_UTLS,
	   SOMM_FLX_DEBIT_MOIS,
	   SOMM_FLX_CRDT_MOIS,
	   SOLD_MOYE_COMP_DEBIT,
	   SOLD_MOYE_COMP_CRDT,
	   ECHL_COD_SELC,
	   ECHL_DAT_DERN_ARRT,
	   ECHL_UNT_PERD,
	   ECHL_NOMB_UNTS_PERD,
	   MONT_AUTR,
	   DAT_DEBT_DECV,
	   MONT_UTLS,
	   MONT_DEPS,
	   NOMB_JOUR_DEPS,	
	   CASE FLG_DEPS_FACL_CAIS WHEN 1 THEN 'Oui' WHEN 0 THEN 'Non' END as FLG_DEPS_FACL_CAIS,
	   CASE FLG_MOBL_BANC WHEN 1 THEN 'Oui' WHEN 0 THEN 'Non' END as FLG_MOBL_BANC,	
	   IEDOM_MONT_REFN,
	   IEDOM_MONT_NON_REFN,
	   IEDOM_MONT_SENS_RESR,
	   CONTVAL_SOLD_MOYE_DEBIT,
	   CONTVAL_SOLD_MOYE_CRDT,
	   CONTVAL_MONT_DEBIT,
	   CONTVAL_MONT_CRDT,
	   MONT_INTR_DEBIT_CUML,
	   MONT_INTR_DEBIT_CUML_DEVS_BASE,
	   PART_BANQ_TRSR,
	   PART_BANQ_TRSR_DEVS_BASE,
	   CASE FLG_COMP_MAIT WHEN 1 THEN 'Oui' WHEN 0 THEN 'Non' END as FLG_COMP_MAIT,
	   NUMR_COMP_MAIT,
	   CUML_MOUV_CRDT_DEVS_BASE,
	   COMM_PRVS_ECHL_DEVS_COMP,
	   COMM_PRVS_ECHL_DEVS_BASE,
	   CLSS_SECR,
	   CASE FLG_UTLS_MOIS WHEN 1 THEN 'Oui' WHEN 0 THEN 'Non' END as FLG_UTLS_MOIS,
	   TAUX_NOMN,
	   REVN_COTITL,
	   COD_SURT,
	   FRQN,
	   CASE FLG_PERM_RISQ WHEN 1 THEN 'Oui' WHEN 0 THEN 'Non' END as FLG_PERM_RISQ,
	   FLG_ENRG_COUR
FROM dbo.DWH_EQUIPEMENT_COMPTE EQPM_COMP
LEFT JOIN [$(DatabaseInstanceName)].[$(DataHubDatabaseName)].dbo.REF_AGENCE REF_AGNC
  ON REF_AGNC.AGENCE_CODE_AGENCE = EQPM_COMP.COD_AGNC
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date observation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'DAT_OBSR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Etablissement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'COD_ETBL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numéro Plan ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'NUMR_PLAN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Agence' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'COD_AGNC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé Agence' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'LIBL_AGNC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numéro Compte' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'NUMR_COMP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Rubrique' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'COD_RUBR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type Compte' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'TYP_COMP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Produit SF' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'COD_PRDT_SF'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Intitulé' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'INTITULE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre Titulaires' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'NOMB_TITL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numéro Client Collectif' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'NUMR_CLNT_COLL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Ouverture' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'DAT_OUVR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Clôture' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'DAT_CLTR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Motif Clôture' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'MOTF_CLTR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Devise' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'COD_DEVS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Solde Date Opération' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'SOLD_DAT_OPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Solde Date Valeur' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'SOLD_DAT_VALR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Solde Date Comptable' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'SOLD_DAT_COMP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Solde Date Comptable en devise du compte' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'SOLD_DAT_COMP_DEVS_COMP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Durée restante à courir en jours' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'DUR_REST_COUR_JOUR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Durée initiale en jours' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'DUR_INTL_JOUR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Garantie en devise d’opération ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'GARN_DEVS_OPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Garantie en devise de base' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'GARN_DEVS_BASE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Garantie' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'COD_GARN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Garant' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'GARANT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Qualité Garant' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'COD_QUAL_GARN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Résidence Garant' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'COD_RESD_GARN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Non utilisé' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'NON_UTLS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Somme Flux débiteurs du mois (date comptable)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'SOMM_FLX_DEBIT_MOIS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Somme Flux créditeurs du mois (date comptable)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'SOMM_FLX_CRDT_MOIS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Solde moyen comptable débiteur' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'SOLD_MOYE_COMP_DEBIT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Solde moyen comptable  créditeur' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'SOLD_MOYE_COMP_CRDT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Echelles Code Sélection ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'ECHL_COD_SELC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Echelles Date Dernier Arrêté' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'ECHL_DAT_DERN_ARRT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Echelles Unité de période' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'ECHL_UNT_PERD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Echelles Nombre Unités de période' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'ECHL_NOMB_UNTS_PERD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Montant autorisé' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'MONT_AUTR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Début Découvert ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'DAT_DEBT_DECV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Montant utilisé' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'MONT_UTLS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Montant en dépassement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'MONT_DEPS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre de jours en dépassement ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'NOMB_JOUR_DEPS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Dépassement Facilité de caisse' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'FLG_DEPS_FACL_CAIS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Mobilité Bancaire' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'FLG_MOBL_BANC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IEDOM Montant refinancé' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'IEDOM_MONT_REFN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IEDOM Montant non refinancé' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'IEDOM_MONT_NON_REFN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IEDOM Montant sens réservé' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'IEDOM_MONT_SENS_RESR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contrevaleur Solde Moyen Débiteur' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'CONTVAL_SOLD_MOYE_DEBIT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contrevaleur Solde Moyen Créditeur ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'CONTVAL_SOLD_MOYE_CRDT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contrevaleur Montant Débiteur' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'CONTVAL_MONT_DEBIT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contrevaleur Montant Créditeur' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'CONTVAL_MONT_CRDT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Montant des intérêts provisionnels débiteurs cumulés' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'MONT_INTR_DEBIT_CUML'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Montant des intérêts provisionnels débiteurs cumulés en devise de base ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'MONT_INTR_DEBIT_CUML_DEVS_BASE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Part de la banque en trésorerie' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'PART_BANQ_TRSR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Part de la banque en trésorerie devise de base' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'PART_BANQ_TRSR_DEVS_BASE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Compte Maître' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'FLG_COMP_MAIT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Numéro Compte Maître' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'NUMR_COMP_MAIT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cumul des mouvements créditeurs en devise de base' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'CUML_MOUV_CRDT_DEVS_BASE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Commission provisionnelle échelles en devise du compte' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'COMM_PRVS_ECHL_DEVS_COMP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Commission provisionnelle échelles en devise de base' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'COMM_PRVS_ECHL_DEVS_BASE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Classe de sécurité' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'CLSS_SECR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Utilisation dans le mois' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'FLG_UTLS_MOIS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TX NOMINAL' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'TAUX_NOMN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'REVENUS COTITULAIRE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'REVN_COTITL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Sûreté' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'COD_SURT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fréquence' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'FRQN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Périmètre Risque' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'FLG_PERM_RISQ'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code apporteur' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'COD_APPOR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Enregistrement Courant' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_EQUIPEMENT_COMPTE', @level2type=N'COLUMN',@level2name=N'FLG_ENRG_COUR'
GO