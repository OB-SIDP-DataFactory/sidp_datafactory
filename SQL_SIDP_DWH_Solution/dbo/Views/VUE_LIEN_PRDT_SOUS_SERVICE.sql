﻿CREATE VIEW [dbo].[VUE_LIEN_PRDT_SOUS_SERVICE] AS 
SELECT lien.DAT_OBSR,
       lien.IDNT_LIEN_PRDT_SOUS_SERV,
       lien.IDNT_PRDT_SOUS,
       lien.IDNT_SERV,
	   ref.COD AS COD_SERV,
	   ref.LABL AS LIBL_SERV,
       lien.[IDNT_PERS_FE],
       lien.IDNT_PERS_SF,
	   CAST(lien.DAT_CRTN as date) AS DAT_CRTN,
       lien.DAT_CRTN AS DAT_CRTN_HMS,
       lien.IDNT_CRTN,
	   CAST(lien.DAT_DERN_MODF as date) AS DAT_DERN_MODF,
       lien.DAT_DERN_MODF AS DAT_DERN_MODF_HMS,
       lien.IDNT_DERN_MODF,
       lien.TYP_UTLS,
	   lien.FLG_ENRG_COUR
  FROM [dbo].[DWH_LIEN_PRDT_SOUS_SERVICE] AS lien
  LEFT JOIN [$(DatabaseInstanceName)].[$(DataFactoryDatabaseName)].dbo.REF_SERVICE_SL AS ref
  ON lien.IDNT_SERV = ref.IDNT_SERV_SL AND ref.DAT_DEBT_VALD <= lien.DAT_OBSR AND lien.DAT_OBSR < ref.DAT_FIN_VALD
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date Observation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PRDT_SOUS_SERVICE', @level2type=N'COLUMN',@level2name=N'DAT_OBSR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant lien produit souscrit service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PRDT_SOUS_SERVICE', @level2type=N'COLUMN',@level2name=N'IDNT_LIEN_PRDT_SOUS_SERV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant produit souscrit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PRDT_SOUS_SERVICE', @level2type=N'COLUMN',@level2name=N'IDNT_PRDT_SOUS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PRDT_SOUS_SERVICE', @level2type=N'COLUMN',@level2name=N'IDNT_SERV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PRDT_SOUS_SERVICE', @level2type=N'COLUMN',@level2name=N'COD_SERV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PRDT_SOUS_SERVICE', @level2type=N'COLUMN',@level2name=N'LIBL_SERV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant Personne SF' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PRDT_SOUS_SERVICE', @level2type=N'COLUMN',@level2name=N'IDNT_PERS_SF'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date creation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PRDT_SOUS_SERVICE', @level2type=N'COLUMN',@level2name=N'DAT_CRTN_HMS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant creation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PRDT_SOUS_SERVICE', @level2type=N'COLUMN',@level2name=N'IDNT_CRTN'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date heure et minute de derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_PRDT_SOUS_SERVICE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_HMS';
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant derniere modification' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PRDT_SOUS_SERVICE', @level2type=N'COLUMN',@level2name=N'IDNT_DERN_MODF'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type utilisateur' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PRDT_SOUS_SERVICE', @level2type=N'COLUMN',@level2name=N'TYP_UTLS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Enregistrement Courant' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VUE_LIEN_PRDT_SOUS_SERVICE', @level2type=N'COLUMN',@level2name=N'FLG_ENRG_COUR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_PRDT_SOUS_SERVICE', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date heure et minute de creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_PRDT_SOUS_SERVICE', @level2type = N'COLUMN', @level2name = N'DAT_CRTN';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_PRDT_SOUS_SERVICE', @level2type = N'COLUMN', @level2name = N'IDNT_PERS_FE';

