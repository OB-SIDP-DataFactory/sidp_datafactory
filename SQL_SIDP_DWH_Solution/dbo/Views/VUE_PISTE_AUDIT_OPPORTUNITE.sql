﻿

CREATE VIEW [dbo].[VUE_PISTE_AUDIT_OPPORTUNITE] AS 
  SELECT IDNT_SF,
         NUMR_OPPR,
         IDNT_OPPR,
         IDNT_CRTN,
         DAT_MODF,
         CHMP_MODF,
		 CASE FLG_SUPP WHEN 'true' THEN 'Oui' WHEN 'false' THEN 'Non' END AS FLG_SUPP,
         NOUV_VALR,
         ANCN_VALR,
  	     DAT_DEBT_VALD,
  	     DAT_FIN_VALD,
  	     FLG_ENRG_COUR
  FROM [dbo].[DWH_PISTE_AUDIT_OPPORTUNITE]
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ancienne valeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PISTE_AUDIT_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'ANCN_VALR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nouvelle valeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PISTE_AUDIT_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'NOUV_VALR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag supprime', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PISTE_AUDIT_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_SUPP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Champ modifie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PISTE_AUDIT_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'CHMP_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PISTE_AUDIT_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DAT_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PISTE_AUDIT_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'IDNT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant opportunite', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PISTE_AUDIT_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'IDNT_OPPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro opportunite', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PISTE_AUDIT_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'NUMR_OPPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant SF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PISTE_AUDIT_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'IDNT_SF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de fin de validité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PISTE_AUDIT_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DAT_FIN_VALD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de début de validité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PISTE_AUDIT_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'DAT_DEBT_VALD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Enregistrement Courant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PISTE_AUDIT_OPPORTUNITE', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';

