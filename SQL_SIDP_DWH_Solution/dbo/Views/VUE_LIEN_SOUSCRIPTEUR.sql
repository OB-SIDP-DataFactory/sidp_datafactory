﻿

CREATE VIEW [dbo].[VUE_LIEN_SOUSCRIPTEUR] AS
SELECT LIEN_SOUS.DAT_OBSR,
LIEN_SOUS.IDNT_LIEN_SOUS,
LIEN_SOUS.NUMR_OPPR,
LIEN_SOUS.IDNT_OPPR,
LIEN_SOUS.IDNT_SOUS,
LIEN_SOUS.ROL as COD_ROL,
CASE LIEN_SOUS.ROL WHEN '01' THEN 'Titulaire' WHEN '02' THEN 'Co-titulaire' END as LIBL_ROL,
CASE LIEN_SOUS.FLG_PRNC WHEN 1 THEN 'Oui' WHEN 0 THEN 'Non' END as FLG_PRNC,
CAST(LIEN_SOUS.DAT_CRTN AS DATE) AS DAT_CRTN,	
LIEN_SOUS.DAT_CRTN as DAT_CRTN_HMS,
LIEN_SOUS.IDNT_CRTN,
CAST(LIEN_SOUS.DAT_DERN_MODF AS DATE) AS DAT_DERN_MODF,
LIEN_SOUS.DAT_DERN_MODF as DAT_DERN_MODF_HMS,
LIEN_SOUS.IDNT_DERN_MODF,
CASE LIEN_SOUS.FLG_SUPP WHEN 1 THEN 'Oui' WHEN 0 THEN 'Non' END as FLG_SUPP,
LIEN_SOUS.FLG_ENRG_COUR
FROM dbo.DWH_LIEN_SOUSCRIPTEUR LIEN_SOUS
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Courant Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_SOUSCRIPTEUR', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag supprime', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_SOUSCRIPTEUR', @level2type = N'COLUMN', @level2name = N'FLG_SUPP';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de dernière modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_SOUSCRIPTEUR', @level2type = N'COLUMN', @level2name = N'IDNT_DERN_MODF';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_SOUSCRIPTEUR', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date et heure de dernière modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_SOUSCRIPTEUR', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_HMS';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_SOUSCRIPTEUR', @level2type = N'COLUMN', @level2name = N'IDNT_CRTN';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_SOUSCRIPTEUR', @level2type = N'COLUMN', @level2name = N'DAT_CRTN';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date et heure de création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_SOUSCRIPTEUR', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_HMS';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag principal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_SOUSCRIPTEUR', @level2type = N'COLUMN', @level2name = N'FLG_PRNC';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé Role', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_SOUSCRIPTEUR', @level2type = N'COLUMN', @level2name = N'LIBL_ROL';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Role', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_SOUSCRIPTEUR', @level2type = N'COLUMN', @level2name = N'COD_ROL';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant souscripteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_SOUSCRIPTEUR', @level2type = N'COLUMN', @level2name = N'IDNT_SOUS';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant opportunite ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_SOUSCRIPTEUR', @level2type = N'COLUMN', @level2name = N'IDNT_OPPR';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro opportunite ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_SOUSCRIPTEUR', @level2type = N'COLUMN', @level2name = N'NUMR_OPPR';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant lien souscripteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_SOUSCRIPTEUR', @level2type = N'COLUMN', @level2name = N'IDNT_LIEN_SOUS';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_LIEN_SOUSCRIPTEUR', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';

