﻿

CREATE VIEW [dbo].[VUE_PRODUIT_SOUSCRIT] as 
SELECT  DAT_OBSR,
        IDNT_PRDT_SOUS,
        IDNT_DOSS_SOUS,
        IDNT_PRDT_COMM,
        COD_PRDT_COMM,
        LIBL_PRDT_COMM,
        DISC,
		CAST(DAT_CRTN AS date) AS DAT_CRTN,
		DAT_CRTN AS DAT_CRTN_HMS,
		IDNT_CRTN,
		IDNT_DERN_MODF,
		TYP_UTLS,
		FLG_SECR_BANC_OPT_IN,
		CAST(DAT_DERN_MODF AS date) AS DAT_DERN_MODF,
		DAT_DERN_MODF AS DAT_DERN_MODF_HMS,
		DAT_CRTN_ENRG,
		DAT_DERN_MODF_ENRG,
	    FLG_ENRG_COUR
  FROM dbo.DWH_PRODUIT_SOUSCRIT
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Discriminateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'DISC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé produit commercial', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'LIBL_PRDT_COMM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code produit commercial', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'COD_PRDT_COMM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant produit commercial', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'IDNT_PRDT_COMM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant dossier souscription', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'IDNT_DOSS_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant produit souscrit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'IDNT_PRDT_SOUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Enregistrement Courant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type utilisateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'TYP_UTLS';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'IDNT_DERN_MODF';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'IDNT_CRTN';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag secret bancaire Opt In', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'FLG_SECR_BANC_OPT_IN';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date et heure de derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_HMS';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere modification enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date et heure de création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_HMS';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date creation enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_PRODUIT_SOUSCRIT', @level2type = N'COLUMN', @level2name = N'DAT_CRTN';

