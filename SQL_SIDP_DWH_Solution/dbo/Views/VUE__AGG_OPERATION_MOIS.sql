﻿CREATE VIEW [dbo].[VUE_AGG_OPERATION_MOIS] AS 
SELECT DAOM.DAT_OBSR        AS DAT_OBSR,
       DAOM.MOIS_OBSR       AS MOIS_OBSR,
	   DAOM.MOIS_OPE        AS MOIS_OPE,
       DAOM.NUMR_COMP       AS NUMR_COMP,
       DAOM.IDNT_TYP_OPE    AS IDNT_TYP_OPE,
       RTO.FAMILLE_N0       AS FAMILLE,
       RTO.FAMILLE_N1       AS SOUS_FAMILLE_N1,
       RTO.FAMILLE_N2       AS SOUS_FAMILLE_N2,
       RTO.FAMILLE_N3       AS SOUS_FAMILLE_N3,
       DAOM.COD_PAYS_TRNS   AS COD_PAYS_TRNS,
       DAOM.NOMB_OPE        AS NOMB_OPE,
       ABS(DAOM.MONT_OPE)   AS MONT_OPE,
       RTO.SENS             AS SENS_OPE,
       RTO.TYP              AS TYP
FROM [dbo].[DWH_AGG_OPERATION_MOIS] DAOM 
LEFT JOIN DWH_REF_TYPE_OPERATION RTO ON DAOM.IDNT_TYP_OPE = RTO.IDNT_TYP_OPRT
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Sous Famille Opération N3', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_AGG_OPERATION_MOIS', @level2type = N'COLUMN', @level2name = N'SOUS_FAMILLE_N3';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Sous Famille Opération N2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_AGG_OPERATION_MOIS', @level2type = N'COLUMN', @level2name = N'SOUS_FAMILLE_N2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Sous Famille Opération N1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_AGG_OPERATION_MOIS', @level2type = N'COLUMN', @level2name = N'SOUS_FAMILLE_N1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Sens Opération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_AGG_OPERATION_MOIS', @level2type = N'COLUMN', @level2name = N'SENS_OPE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_AGG_OPERATION_MOIS', @level2type = N'COLUMN', @level2name = N'NUMR_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre Opération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_AGG_OPERATION_MOIS', @level2type = N'COLUMN', @level2name = N'NOMB_OPE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant Opération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_AGG_OPERATION_MOIS', @level2type = N'COLUMN', @level2name = N'MONT_OPE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mois Opération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_AGG_OPERATION_MOIS', @level2type = N'COLUMN', @level2name = N'MOIS_OPE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Type Opération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_AGG_OPERATION_MOIS', @level2type = N'COLUMN', @level2name = N'IDNT_TYP_OPE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Famille Opération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_AGG_OPERATION_MOIS', @level2type = N'COLUMN', @level2name = N'FAMILLE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_AGG_OPERATION_MOIS', @level2type = N'COLUMN', @level2name = N'DAT_OBSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Pays de transaction', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_AGG_OPERATION_MOIS', @level2type = N'COLUMN', @level2name = N'COD_PAYS_TRNS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_AGG_OPERATION_MOIS', @level2type = N'COLUMN', @level2name = N'TYP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mois Observation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VUE_AGG_OPERATION_MOIS', @level2type = N'COLUMN', @level2name = N'MOIS_OBSR';
GO