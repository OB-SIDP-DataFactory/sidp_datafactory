﻿CREATE TABLE [dbo].[JobSteps]
(
	[Id]			BIGINT			IDENTITY(1,1) NOT NULL,
	[Name]			NVARCHAR(10)	NOT NULL,
	[JobName]		NVARCHAR(10)	NOT NULL,
	[Description]	NVARCHAR(255)	NULL
)
GO