﻿CREATE TABLE [dbo].[Jobs]
(
	[Id]			BIGINT			IDENTITY(1,1) NOT NULL,
	[Name]			NVARCHAR(10)	NOT NULL,
	[Description]	NVARCHAR(255)	NULL
)
GO