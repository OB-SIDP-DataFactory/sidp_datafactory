﻿CREATE TABLE [dbo].[BF_INCIDENTS_ARCHIVE] (
    [COD_ETABLISSEMENT]                INT             NOT NULL,
    [TYP_INCIDENT]                     VARCHAR (2)     NOT NULL,
    [NUMERO_INCIDENT]                  INT             NOT NULL,
    [NUMERO_CLIENT]                    VARCHAR (7)     NOT NULL,
    [NUMERO_PLAN]                      INT             NOT NULL,
    [NUMERO_COMPTE]                    VARCHAR (20)    NOT NULL,
    [NUMERO_CHEQUE]                    INT             NULL,
    [NUMERO_SEQUENCE]                  INT             NULL,
    [DAT_EVENEMENT]                    DATE            NULL,
    [DEVISE]                           VARCHAR (3)     NULL,
    [MONT_GLOBAL_INCIDENT_DEVISE_CPT]  DECIMAL (18, 3) NULL,
    [MONT_GLOBAL_INCIDENT_DEVISE_BASE] DECIMAL (18, 3) NULL,
    [DATE_EXPLOITATION]                DATE            NULL,
    [DATE_ACTION]                      DATE            NOT NULL,
    CONSTRAINT [PK_BF_INCIDENTS_ARCHIVE] PRIMARY KEY CLUSTERED ([COD_ETABLISSEMENT] ASC, [TYP_INCIDENT] ASC, [NUMERO_INCIDENT] ASC, [NUMERO_CLIENT] ASC, [NUMERO_PLAN] ASC, [NUMERO_COMPTE] ASC, [DATE_ACTION] ASC)
);

