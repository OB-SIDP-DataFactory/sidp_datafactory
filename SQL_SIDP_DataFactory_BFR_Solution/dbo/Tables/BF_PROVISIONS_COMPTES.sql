﻿CREATE TABLE [dbo].[BF_PROVISIONS_COMPTES] (
    [ETABLISSEMENT]                          INT             NOT NULL,
    [NUMERO_COMPTE]                          VARCHAR (20)    NOT NULL,
    [CODE_PRODUIT]                           VARCHAR (3)     NULL,
    [RUBRIQUE_COMPTABLE]                     VARCHAR (10)    NULL,
    [DEVISE]                                 VARCHAR (3)     NULL,
    [MONTANT_PROVISIONS]                     DECIMAL (17, 2) NULL,
    [MONTANT_PROVISIONS_CVL]                 DECIMAL (17, 2) NULL,
    [MONTANT_DOTATIONS]                      DECIMAL (17, 2) NULL,
    [MONTANT_DOTATIONS_CVL]                  DECIMAL (17, 2) NULL,
    [MONTANT_REPRISES]                       DECIMAL (17, 2) NULL,
    [MONTANT_REPRISES_CVL]                   DECIMAL (17, 2) NULL,
    [MONTANT_PERTES]                         DECIMAL (17, 2) NULL,
    [MONTANT_PERTES_CVL]                     DECIMAL (17, 2) NULL,
    [MONTANT_STOCK_PROVISIONS_DOTATIONS]     DECIMAL (17, 2) NULL,
    [MONTANT_STOCK_PROVISIONS_DOTATIONS_CVL] DECIMAL (17, 2) NULL,
    [MONTANT_STOCK_REPRISES]                 DECIMAL (17, 2) NULL,
    [MONTANT_STOCK_REPRISES_CVL]             DECIMAL (17, 2) NULL,
    [ORIGINE_PROVISIONS]                     VARCHAR (1)     NULL,
    [PERIMETRE_RISQUE]                       VARCHAR (1)     NULL,
    [FREQUENCE]                              VARCHAR (1)     NULL,
    [DOMAINE]                                VARCHAR (12)    NULL,
    [DATE_EXPLOITATION]                      DATE            NULL,
    [DATE_ACTION]                            DATE            NOT NULL,
    CONSTRAINT [PK_BF_PROVISIONS_COMPTES] PRIMARY KEY CLUSTERED ([ETABLISSEMENT] ASC, [NUMERO_COMPTE] ASC, [DATE_ACTION] ASC)
);



