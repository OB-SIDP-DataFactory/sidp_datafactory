﻿CREATE TABLE [dbo].[BF_COMMISSIONS_ECH_ARCHIVE] (
    [COD_ETABLISSEMENT]                  INT             NOT NULL,
    [NUMERO_CLIENT]                      VARCHAR (7)     NOT NULL,
    [NUMERO_PLAN]                        INT             NOT NULL,
    [NUMERO_COMPTE]                      VARCHAR (20)    NOT NULL,
    [COD_OPERATION]                      VARCHAR (6)     NOT NULL,
    [COD_ANALYTIQUE]                     VARCHAR (6)     NOT NULL,
    [DAT_DEBUT_SITUATION]                DATE            NULL,
    [MONT_TOTAL]                         DECIMAL (18, 3) NULL,
    [NOMB_COMMISSION]                    INT             NULL,
    [DEVISE]                             VARCHAR (3)     NULL,
    [MONT_TOTAL_DEVISE_BASE]             DECIMAL (18, 3) NULL,
    [NUMERO_LIGNE_COMMISSION_DEBITRICE]  INT             NULL,
    [NUMERO_LIGNE_COMMISSION_CREDITRICE] INT             NULL,
    [DAT_SYSTEME]                        DATE            NULL,
    [DATE_EXPLOITATION]                  DATE            NULL,
    [DATE_ACTION]                        DATE            NOT NULL,
    CONSTRAINT [PK_BF_COMMISSIONS_ECH_ARCHIVE] PRIMARY KEY CLUSTERED ([COD_ETABLISSEMENT] ASC, [NUMERO_CLIENT] ASC, [NUMERO_PLAN] ASC, [NUMERO_COMPTE] ASC, [COD_OPERATION] ASC, [COD_ANALYTIQUE] ASC, [DATE_ACTION] ASC)
);

