﻿CREATE TABLE [dbo].[TRVL_SEGMENT_Q] (
    [DATE_ACTION]                   DATE          NOT NULL,
    [NUMERO_CLIENT]                 VARCHAR (7)   NOT NULL,
    [CR_2]                          VARCHAR (15)  NULL,
    [AGE]                           DECIMAL (3)   NULL,
    [INDC_PP]                       BIT           DEFAULT ((0)) NULL,
    [INDC_BANQUE_PRIVEE]            BIT           DEFAULT ((0)) NULL,
    [INDC_DECEDE]                   BIT           DEFAULT ((0)) NULL,
    [INDC_CAPABLE_JURIDIQUE]        BIT           DEFAULT ((0)) NULL,
    [INDC_ANCIENNETE_6M]            BIT           DEFAULT ((0)) NULL,
    [SCORE_ER]                      VARCHAR (255) NULL,
    [COTATION_RISQUE]               VARCHAR (2)   NULL,
    [CANAL_DISTRIBUTION]            VARCHAR (255) NULL,
    [SCORE_PREATTRIBUTION_ORANGE]   VARCHAR (255) NULL,
    [SCORE_PREATTRIBUTION_GROUPAMA] VARCHAR (255) NULL,
    [LIB_MARC]                      VARCHAR (50)  NULL,
    [DAT_CRTN_ENRG]                 DATETIME      NULL,
    CONSTRAINT [PK_TRVL_SEGMENT_Q] PRIMARY KEY CLUSTERED ([DATE_ACTION] DESC, [NUMERO_CLIENT] ASC)
);

