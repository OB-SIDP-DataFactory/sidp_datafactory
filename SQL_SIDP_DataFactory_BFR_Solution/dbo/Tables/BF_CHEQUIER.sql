﻿CREATE TABLE [dbo].[BF_CHEQUIER] (
    [NUMERO_COMPTE]           VARCHAR (20) NOT NULL,
    [COD_ETABLISSEMENT]       INT          NOT NULL,
    [COD_AGENCE_STOCK]        INT          NOT NULL,
    [COD_AGENCE_COMPTE]       INT          NOT NULL,
    [TYP_CHEQUIER]            VARCHAR (6)  NOT NULL,
    [DAT_DEMANDE]             DATE         NOT NULL,
    [NUMERO_SEQUENCE]         INT          NULL,
    [DAT_RECEPTION_CHEQUIER]  DATE         NULL,
    [DATE_REMISE_CHEQUIER]    DATE         NULL,
    [COD_LIVRAISON]           VARCHAR (3)  NULL,
    [DAT_SUPPRESSION]         DATE         NULL,
    [COD_ETAT_RENOUVELLEMENT] INT          NULL,
    [DAT_RENOUVELLEMENT]      DATE         NULL,
    [ORIGINE_CHEQUIER]        VARCHAR (1)  NULL,
    [DATE_EXPLOITATION]       DATE         NULL,
    [DATE_ACTION]             DATE         NOT NULL
);

