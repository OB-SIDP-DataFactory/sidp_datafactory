﻿CREATE TABLE [dbo].[T_DIM_CLI_SCORE_COMP_M] (
    [SA_NUMR_CLNT_SABT]       VARCHAR (7) NOT NULL,
    [DAT_OBSR]                DATE        NOT NULL,
    [FLG_PER_PHY]             BIT         NULL,
    [FLG_CAPC_JURD]           BIT         NULL,
    [FLG_DECEDE]              BIT         NULL,
    [FLG_ANCNT_6M]            BIT         NULL,
    [FLG_PRDT_CVX_CVU]        BIT         NULL,
    [FLG_CRDT_CONS_RENV]      BIT         NULL,
    [FLG_CRDT_CONS_AMRT]      BIT         NULL,
    [FLG_CRDT_CONS_AMRT_RCNT] BIT         NULL,
    [FLG_CRDT_CONS_AMRT_DECS] BIT         NULL,
    [INDC_IMPAYE_AN]          BIT         NULL,
    [INDC_COTATION_SCORE]     BIT         NULL,
    [DAT_CRTN_ENRG]           DATETIME    NULL,
    CONSTRAINT [PK_T_DIM_CR] PRIMARY KEY CLUSTERED ([DAT_OBSR] DESC, [SA_NUMR_CLNT_SABT] DESC)
);

