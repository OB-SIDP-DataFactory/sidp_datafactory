﻿CREATE TABLE [dbo].[T_CLI_SEG_Q] (
    [DAT_OBSR]                      DATE          NOT NULL,
    [SA_NUMR_CLNT_SAB]              VARCHAR (7)   NOT NULL,
    [SEG_SCR_COMP]                  CHAR (3)      NOT NULL,
    [CLSS_RISQ_SCR_COMP_N2]         VARCHAR (15)  NULL,
    [AGE]                           DECIMAL (3)   NULL,
    [FLG_PER_PHY]                   BIT           DEFAULT ((0)) NULL,
    [LIB_MARC]                      VARCHAR (50)  NULL,
    [FLG_DECEDE]                    BIT           DEFAULT ((0)) NULL,
    [FLG_CAPC_JURD]                 BIT           DEFAULT ((0)) NULL,
    [FLG_ANCNT_6M]                  BIT           DEFAULT ((0)) NULL,
    [SCORE_ER]                      VARCHAR (255) NULL,
    [SF_COTT_RISQ]                  VARCHAR (2)   NULL,
    [CANAL_DISTRIBUTION]            VARCHAR (255) NULL,
    [SCORE_PREATTRIBUTION_ORANGE]   VARCHAR (255) NULL,
    [SCORE_PREATTRIBUTION_GROUPAMA] VARCHAR (255) NULL,
    [Validity_StartDate]            DATETIME2 (7) NULL,
    [Validity_EndDate]              DATETIME2 (7) NULL,
    [DAT_CRTN_ENRG]                 DATETIME      NULL,
    CONSTRAINT [Primary_T_CLI_SEG_Q] PRIMARY KEY CLUSTERED ([DAT_OBSR] DESC, [SA_NUMR_CLNT_SAB] ASC)
)
GO
CREATE NONCLUSTERED INDEX [IX_NC_T_CLI_SEG_Q_DAT_ENRG]
ON [dbo].[T_CLI_SEG_Q] ([DAT_CRTN_ENRG])
INCLUDE ([DAT_OBSR],[SA_NUMR_CLNT_SAB],[SEG_SCR_COMP],[Validity_EndDate])
GO