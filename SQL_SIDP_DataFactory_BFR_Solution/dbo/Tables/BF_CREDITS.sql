﻿
CREATE TABLE [dbo].[BF_CREDITS] (
    [NUMERO_PRET]                     VARCHAR (20)    NOT NULL,
    [NUMERO_DOSSIER]                  VARCHAR (20)    NULL,
    [NUMERO_COMPTE_MIROIR]            VARCHAR (20)    NULL,
    [RUBRIQUE_COMPTABLE]              VARCHAR (10)    NULL,
    [CODE_PRODUIT_CRM]                VARCHAR (25)    NULL,
    [CODE_PRODUIT_SAB]                VARCHAR (25)    NULL,
    [CODE_OPERATION_SAB]              VARCHAR (10)    NULL,
    [CODE_NATURE_CREDIT_SAB]          VARCHAR (10)    NULL,
    [CODE_PRODUIT_DF]                 VARCHAR (10)    NULL,
    [LIBELLE_PRODUIT_DF]              VARCHAR (100)   NULL,
    [NUMERO_CLIENT]                   VARCHAR (10)    NULL,
    [DATE_OUVERTURE]                  DATE            NULL,
    [DATE_CLOTURE]                    DATE            NULL,
    [DUREE_CREDIT]                    INT             NULL,
    [MONTANT_NOMINAL_CVL]             DECIMAL (18, 3) NULL,
    [CRD_CVL]                         DECIMAL (18, 3) NULL,
    [ENCOURS_MOYEN_CVL]               DECIMAL (18, 3) NULL,
    [ENCOURS_MOYEN]                   DECIMAL (18, 3) NULL,
    [MONTANT_RAT]                     DECIMAL (18, 3) NULL,
    [MONTANT_INTERETS_MOIS_CVL]       DECIMAL (18, 3) NULL,
    [MONTANT_INTERETS_MOIS]           DECIMAL (18, 3) NULL,
    [MONTANT_INTERETS_COURUS_CVL]     DECIMAL (18, 3) NULL,
    [MONTANT_INTERETS_COURUS]         DECIMAL (18, 3) NULL,
    [MONTANT_INTERETS_TRESORERIE_CVL] DECIMAL (18, 3) NULL,
    [MONTANT_MARGE_CLIENT]            DECIMAL (14, 9) NULL,
    [MONTANT_MARGE_COMMERCIALE_CVL]   DECIMAL (18, 3) NULL,
    [TAUX_FIXE]                       DECIMAL (14, 9) NULL,
    [TAUX_ANALYSE]                    DECIMAL (14, 9) NULL,
    [DATE_EXPLOITATION]               DATE            NOT NULL,
    [DATE_ACTION]                     DATE            NOT NULL,
    CONSTRAINT [PK_BF_CREDITS] PRIMARY KEY CLUSTERED ([NUMERO_PRET] ASC, [DATE_ACTION] ASC)
);




