﻿CREATE TABLE [dbo].[BF_CREDIT_RENOUVELABLE_ARCHIVE] (
    [NUMERO_PRET]                                    VARCHAR (11)    NOT NULL,
    [FFQ_NUMERO_PRET]                                VARCHAR (11)    NULL,
    [FFM_NUMERO_PRET]                                VARCHAR (11)    NULL,
    [FFM_DATE_TRAITEMENT]                            DATE            NULL,
    [CTX_NUMERO_PRET]                                VARCHAR (11)    NULL,
    [CTX_TYPE_FLUX]                                  VARCHAR (3)     NULL,
    [CTX_TYPE_PRET]                                  VARCHAR (3)     NULL,
    [FFQ_IDENTIFIANT_CRM_EMPRUNTEUR]                 VARCHAR (8)     NULL,
    [FFQ_IDENTIFIANT_CRM_COEMPRUNTEUR]               VARCHAR (8)     NULL,
    [FFQ_TYPE_COMPLEO]                               VARCHAR (5)     NULL,
    [FFQ_DATE_OUVERTURE]                             DATE            NULL,
    [FFQ_DATE_1ERE_UTILISATION]                      DATE            NULL,
    [FFQ_NUMERO_BAREME_VIGUEUR]                      VARCHAR (6)     NULL,
    [FFQ_VARIANTE_BAREME_VIGUEUR]                    VARCHAR (3)     NULL,
    [FFQ_GRILLE_TAUX_BAREME_VIGUEUR]                 VARCHAR (2)     NULL,
    [FFQ_GRILLE_MENSUALITE_BAREME_VIGUEUR]           VARCHAR (2)     NULL,
    [FFQ_VITESSE_BAREME_VIGUEUR]                     VARCHAR (1)     NULL,
    [FFQ_DATE_FIN_BAREME_VIGUEUR]                    DATE            NULL,
    [FFQ_TYPE_BAREME]                                VARCHAR (1)     NULL,
    [FFQ_NUMERO_BAREME_STANDARD_FUTUR]               VARCHAR (6)     NULL,
    [FFQ_VARIANTE_BAREME_STANDARD_FUTUR]             VARCHAR (3)     NULL,
    [FFQ_GRILLE_TAUX_BAREME_STANDARD_FUTUR]          VARCHAR (2)     NULL,
    [FFQ_GRILLE_MENSUALITE_BAREME_STANDARD_FUTUR]    VARCHAR (2)     NULL,
    [FFQ_VITESSE_BAREME_STANDARD_FUTUR]              VARCHAR (1)     NULL,
    [FFQ_CODE_BANQUE_COMPTE_PRELEVEMENT]             VARCHAR (5)     NULL,
    [FFQ_CODE_GUICHET_COMPTE_PRELEVEMENT]            VARCHAR (5)     NULL,
    [FFQ_NUMERO_COMPTE_PRELEVEMENT]                  VARCHAR (11)    NULL,
    [FFQ_CLE_RIB_COMPTE_PRELEVEMENT]                 VARCHAR (2)     NULL,
    [FFQ_LIBELLE_TITULAIRE_COMPTE]                   VARCHAR (26)    NULL,
    [FFQ_TOP_ASSURANCE]                              VARCHAR (1)     NULL,
    [FFQ_NATURE_ASSURANCE]                           VARCHAR (1)     NULL,
    [FFQ_MONTANT_TOTAL_AUTORISATION]                 DECIMAL (11, 2) NULL,
    [FFQ_MONTANT_DISPONIBLE]                         DECIMAL (11, 2) NULL,
    [FFQ_MONTANT_TOTAL_UTILISATION]                  DECIMAL (11, 2) NULL,
    [FFQ_MONTANT_UC]                                 DECIMAL (11, 2) NULL,
    [FFQ_MONTANT_UP]                                 DECIMAL (11, 2) NULL,
    [FFQ_DATE_DERNIERE_ECHEANCE_TRAITEE]             DATE            NULL,
    [FFQ_MONTANT_TOTAL_DERNIERE_ECHEANCE_TRAITEE]    DECIMAL (11, 2) NULL,
    [FFQ_MONTANT_DERNIERE_ECHEANCE_UC]               DECIMAL (11, 2) NULL,
    [FFQ_MONTANT_CAPITAL_DERNIERE_ECHEANCE_UC]       DECIMAL (11, 2) NULL,
    [FFQ_MONTANT_INTERETS_DERNIERE_ECHEANCE_UC]      DECIMAL (11, 2) NULL,
    [FFQ_MONTANT_ASSURANCE_DERNIERE_ECHEANCE_UC]     DECIMAL (11, 2) NULL,
    [FFQ_MONTANT_DERNIERE_ECHEANCE_UP]               DECIMAL (11, 2) NULL,
    [FFQ_MONTANT_CAPITAL_DERNIERE_ECHEANCE_UP]       DECIMAL (11, 2) NULL,
    [FFQ_MONTANT_INTERETS_DERNIERE_ECHEANCE_UP]      DECIMAL (11, 2) NULL,
    [FFQ_MONTANT_ASSURANCE_DERNIERE_ECHEANCE_UP]     DECIMAL (11, 2) NULL,
    [FFQ_NOMBRE_MENSUALITES_REPORTEES]               INT             NULL,
    [FFQ_TOP_MODIFICATION_RESERVE_AUTORISEE]         VARCHAR (1)     NULL,
    [FFQ_DATE_MODIFICATION_RESERVE_AUTORISEE]        DATE            NULL,
    [FFQ_NOMBRE_ECHEANCES_IMPAYEES]                  INT             NULL,
    [FFQ_CODE_SITUATION_DOSSIER]                     VARCHAR (2)     NULL,
    [FFQ_DATE_DERNIERE_MODIFICATION_POSITION_CASH]   DATE            NULL,
    [FFQ_CODE_POSITION]                              INT             NULL,
    [FFQ_DATE_DERNIERE_MODIFICATION_CODE_POSITION]   DATE            NULL,
    [FFQ_NOMBRE_UP]                                  INT             NULL,
    [FFM_DOMICILIATION_ETABLISSEMENT]                VARCHAR (5)     NULL,
    [FFM_DOMICILIATION_GUICHET]                      VARCHAR (5)     NULL,
    [FFM_DOMICILIATION_COMPTE]                       VARCHAR (16)    NULL,
    [FFM_DATE_OUVERTURE]                             DATE            NULL,
    [FFM_DEVISE]                                     VARCHAR (3)     NULL,
    [FFM_MONTANT_MAXIMUM_AUTORISE_TOUTES_OPERATIONS] DECIMAL (9, 2)  NULL,
    [FFM_MONTANT_MAXIMUM_AUTORISE_VIREMENTS]         DECIMAL (9, 2)  NULL,
    [FFM_MONTANT_DISPONIBLE_TOUTES_OPERATIONS]       DECIMAL (9, 2)  NULL,
    [FFM_MONTANT_UTILISE_TOUTES_OPERATIONS]          DECIMAL (9, 2)  NULL,
    [FFM_MONTANT_DISPONIBLE_VIREMENTS]               DECIMAL (9, 2)  NULL,
    [FFM_ENCOURS_MOYEN]                              DECIMAL (9, 2)  NULL,
    [FFM_MONTANT_ECHEANCE]                           DECIMAL (9, 2)  NULL,
    [FFM_TAUX_MOYEN_HORS_ASSURANCE]                  VARCHAR (5)     NULL,
    [FFM_CODE_STATUT_SUIVI_DOSSIER]                  VARCHAR (2)     NULL,
    [FFM_DATE_CLOTURE]                               DATE            NULL,
    [FFM_MONTANT_FRAIS]                              DECIMAL (9, 2)  NULL,
    [FFM_NOMBRE_DEMANDES_VIREMENT]                   INT             NULL,
    [FFM_MONTANT_DEMANDES_VIREMENT]                  DECIMAL (9, 2)  NULL,
    [FFM_NOMBRE_RETRAITS_GUICHET]                    INT             NULL,
    [FFM_MONTANT_RETRAITS_GUICHET]                   DECIMAL (9, 2)  NULL,
    [FFM_NOMBRE_REMBOURSEMENTS_ANTICIPES]            INT             NULL,
    [FFM_MONTANT_REMBOURSEMENTS_ANTICIPES]           DECIMAL (9, 2)  NULL,
    [FFM_MONTANT_COMMISSIONS]                        DECIMAL (9, 2)  NULL,
    [FFM_IDENTIFIANT_CRM]                            VARCHAR (8)     NULL,
    [FFM_PHASE_RECOUVREMENT]                         VARCHAR (1)     NULL,
    [FFM_MONTANT_IMPAYES]                            DECIMAL (9, 2)  NULL,
    [FFM_NOMBRE_IMPAYES]                             INT             NULL,
    [FFM_DATE_1ER_IMPAYE]                            DATE            NULL,
    [CTX_IDENTIFIANT_CRM]                            VARCHAR (8)     NULL,
    [CTX_CODE_BANQUE]                                VARCHAR (5)     NULL,
    [CTX_CODE_GUICHET]                               VARCHAR (5)     NULL,
    [CTX_NUMERO_COMPTE]                              VARCHAR (11)    NULL,
    [CTX_DEVISE]                                     VARCHAR (3)     NULL,
    [CTX_DATE_REMISE_CONTENTIEUX]                    DATE            NULL,
    [CTX_CAPITAL_DU_SUR_MENSUALITES_A_ECHOIR]        DECIMAL (9, 2)  NULL,
    [CTX_MONTANT_MENSUALITES_IMPAYEES]               DECIMAL (9, 2)  NULL,
    [CTX_MONTANT_INTERETS_MENSUALITES_IMPAYEES]      DECIMAL (9, 2)  NULL,
    [CTX_INDEMNITES_LEGALES]                         DECIMAL (9, 2)  NULL,
    [CTX_SOLDE_AFFAIRE_CONTENTIEUSE]                 DECIMAL (9, 2)  NULL,
    [CTX_DATE_DERNIERE_ECRITURE_COMPTABLE]           DATE            NULL,
    [CTX_CUMUL_PERTES]                               DECIMAL (9, 2)  NULL,
    [CTX_CODE_POSITION]                              VARCHAR (1)     NULL,
    [CPT_MONTANT_TOTAL_AUTORISE]                     DECIMAL (18, 3) NULL,
    [CPT_MONTANT_TOTAL_UTILISE]                      DECIMAL (18, 3) NULL,
    [DATE_EXPLOITATION]                              DATE            NOT NULL,
    [DATE_ACTION]                                    DATE            NOT NULL,
    CONSTRAINT [PK_BF_CREDIT_RENOUVELABLE_ARCHIVE] PRIMARY KEY CLUSTERED ([NUMERO_PRET] ASC, [DATE_ACTION] ASC)
);




