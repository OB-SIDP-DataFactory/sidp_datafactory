﻿CREATE TABLE [dbo].[PARAM_mail_SEG](
	[Id] [int] NOT NULL,
	[Smtp] [nvarchar](50) NULL,
	[From] [nvarchar](50) NOT NULL,
	[To] [nvarchar](255) NOT NULL,
	[Cc] [nvarchar](255) NOT NULL,
	[Subject] [nvarchar](100) NOT NULL,
	[Body] [nvarchar](2000) NOT NULL,
	[MaxRows] [int] NULL,
 CONSTRAINT [PK_PARAM_SSRS_SUBSCRIPTION] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
));
