﻿CREATE PROCEDURE [dbo].[Alim_BF_Clients]
(
   @date_analyse DATE
)
AS
BEGIN
   
  --declare @date_analyse DATE
    declare @date_action DATE
    declare @ST varchar(200)
    declare @SC varchar(200)
    declare @t varchar(1) 

  --set @date_analyse =  (select MAX(DWHCPTDTX) from [$(DataHubDatabaseName)].[dbo].[PV_SA_M_COMPTE])
    set @date_action= DATEADD (MONTH, DATEDIFF (MONTH,0,@date_analyse)+1,0)
	set @ST = 'Principal: Oui'
    set @SC = 'Score:'
    set @t = '*'
	SET NOCOUNT ON;
-----------------------------------------------------------///////////////// Step 1
--------------------------------------filtre sur la CLI0 + extraction donnés brutes + creation du champ FLAG_CLIENT_EQUIPE
select [NUMERO_CLIENT], [AGENCE], [NOM_CLIENT], [PRENOM_CLIENT], [SIGLE_USUEL_CLIENT], [CODE_SIREN], [CODE_SIRET], [DATE_CREATION_CLIENT], [DWHCLICRT], [DATE_FIN_RELATION_CLIENT], [date_naissance], [DWHCLINAT], [DWHCLICRE], [DWHCLIRSD], [cd_responsable], [quali], [DWHCLISAC], [cd_PCS], [DWHCLIREG], [cate], [DWHCLIGEO], [DWHCLIENT], [DWHCLIACT], [DWHCLICRD], [cotation_interne], [DWHCLIDIR], [DWHCLIPRF], [DWHCLINBE], [DWHCLIRGM], [DWHCLIEFC], [DWHCLICHF], [DWHCLIANF], [capacite_juridique], [DWHCLIREV], [DWHCLICHA], [DWHCLIPAT], [DWHCLISER], [DWHCLISEP], [cd_douteux], [clt_douteux], [interdit_chequier], [type_interdit], [indicateur_deces], [DWHCLIPNA], [situation_familiale], [tiers_coll], [DWHCLIGRI], [DWHCLITRI], [dt_litigieux_contentieux], [motif_litigieux_contentieux], [DWHCLIPRO], [DWHCLIDOM], [DWHCLIEST], [cd_appo], [cd_caisse_crm], [DWHCLIBIL], [DWHCLIBIM], [DWHCLIATR], [DWHCLIBLN], [DWHCLISTA], [titu_princp], [DWHCLIMUT], [DWHCLIACC], [DWHCLIDGI], [DWHCLIFOJ], [DWHCLINFI], [DWHCLIAD1], [DWHCLICOP], [DWHCLIVIL], [DWHCLIDEP], [DWHCLICDN], [DWHCLICCN], [DWHCLICPI], [DWHCLIIDT], [DWHCLIIDN], [DWHCLIPYR], [DWHCLIPYN], [DWHCLIMBI], [DWHCLICON], [DWHCLICIB], [DWHCLITTI], [DWHCLIDAS], [DWHCLICPJ], [DWHCLIDPJ], [DWHCLIRIS], [IDENTIFIANT_CLIENT_CRM], [STATUT_CREATION_CLIENT_CRM], [cot_risq], [reseau_distributeur], [canal_dis], [score_preattribution_orange], [score_preattribution_groupama], [canal_org], [score_ER], [FLAG_CLIENT_EQUIPE], [RowID] into #res1 
from
(
select   
 c.DWHCLICLI as NUMERO_CLIENT
,c.DWHCLIAGE as AGENCE 
,c.DWHCLIRA1 as NOM_CLIENT
,c.DWHCLIRA2 as PRENOM_CLIENT
,c.DWHCLISIG as SIGLE_USUEL_CLIENT
,c.DWHCLISRN as CODE_SIREN
,c.DWHCLISRT as CODE_SIRET
,c.DWHCLIDCR as DATE_CREATION_CLIENT
,c.DWHCLICRT
,c.DWHCLICLO as DATE_FIN_RELATION_CLIENT
,c.DWHCLIDNA as date_naissance
,c.DWHCLINAT
,c.DWHCLICRE
,c.DWHCLIRSD
,c.DWHCLIRES as cd_responsable
,c.DWHCLIECO as quali
,c.DWHCLISAC
,c.DWHCLIPCS as cd_PCS
,c.DWHCLIREG
,c.DWHCLICAT as cate
,c.DWHCLIGEO
,c.DWHCLIENT
,c.DWHCLIACT
,c.DWHCLICRD
,c.DWHCLICOT as cotation_interne
,c.DWHCLIDIR
,c.DWHCLIPRF
,c.DWHCLINBE
,c.DWHCLIRGM
,c.DWHCLIEFC
,c.DWHCLICHF
,c.DWHCLIANF
,c.DWHCLIJUR as capacite_juridique
,c.DWHCLIREV
,c.DWHCLICHA
,c.DWHCLIPAT
,c.DWHCLISER
,c.DWHCLISEP
,c.DWHCLIDBA as cd_douteux
,c.DWHCLIDOU as clt_douteux
,c.DWHCLIINT as interdit_chequier
,c.DWHCLITYI as type_interdit
,c.DWHCLIDEC as indicateur_deces
,c.DWHCLIPNA
,c.DWHCLIFAM as situation_familiale
,c.DWHCLICOL as tiers_coll
,c.DWHCLIGRI
,c.DWHCLITRI
,c.DWHCLIDLC as dt_litigieux_contentieux
,c.DWHCLIMOT as motif_litigieux_contentieux
,c.DWHCLIPRO
,c.DWHCLIDOM
,c.DWHCLIEST
-- Enrichissement du champ cd_appo 
,case when c.DWHCLICOL = 1 then
           (case when b.DWHCLIRU2  like '' and b.DWHCLIRES in ('GAG','GAL','GAN','GCA','GCM','GGE','GNE','GOC','GOI','GPA','GPR','GRA','GSU','LBR','PVL')
                 then b.DWHCLIRES
 	             when b.DWHCLIRU2 like '' and b.DWHCLIRES not in ('GAG','GAL','GAN','GCA','GCM','GGE','GNE','GOC','GOI','GPA','GPR','GRA','GSU','LBR','PVL')
                 then 'BAN'    
	             when b.DWHCLIRU2 not like ''
	             then b.DWHCLIRU2
				 else (case when c.DWHCLIRES in ('GAG','GAL','GAN','GCA','GCM','GGE','GNE','GOC','GOI','GPA','GPR','GRA','GSU','LBR','PVL') then c.DWHCLIRES
				       else 'BAN'
				       end)     
			end)
	  when c.DWHCLICOL = 0 then 
	       (case when c.DWHCLIRU2 like '' and c.DWHCLIRES in ('GAG','GAL','GAN','GCA','GCM','GGE','GNE','GOC','GOI','GPA','GPR','GRA','GSU','LBR','PVL')
                then c.DWHCLIRES
	            when c.DWHCLIRU2 like '' and c.DWHCLIRES not in ('GAG','GAL','GAN','GCA','GCM','GGE','GNE','GOC','GOI','GPA','GPR','GRA','GSU','LBR','PVL')
                then 'BAN'
                when c.DWHCLIRU2 not like ''
	            then c.DWHCLIRU2
		    end)
	  end as cd_appo

-- Alimentation du champ cd_caisse_CRM
-- Reprise du code_caisse_CRM de S2S avant migration (avant octobre 2018)
,case when @date_analyse < '2018-10-01' then 
	       (case when r.CODE_CAISSE_CRM not like '' then r.CODE_CAISSE_CRM --modification 11/07/2019
--				 when r.code_caisse_crm like '' and c.DWHCLICOL = 0 then c.DWHCLIRU2
				 when r.CODE_CAISSE_CRM like '' and c.DWHCLICOL = 0 then (case when c.DWHCLIRU2 like '' and c.DWHCLIRES in ('GAG','GAL','GAN','GCA','GCM','GGE','GNE','GOC','GOI','GPA','GPR','GRA','GSU','LBR','PVL') then c.DWHCLIRES
																			   when c.DWHCLIRU2 like '' and c.DWHCLIRES not in ('GAG','GAL','GAN','GCA','GCM','GGE','GNE','GOC','GOI','GPA','GPR','GRA','GSU','LBR','PVL') then 'BAN'
																			   when c.DWHCLIRU2 not like '' then c.DWHCLIRU2
																	      end)
--				 when (r.code_caisse_crm like '') and c.DWHCLICOL = 1 then b.DWHCLIRU2
				 when r.CODE_CAISSE_CRM like '' and c.DWHCLICOL = 1 then (case when b.DWHCLIRU2 like '' and b.DWHCLIRES in ('GAG','GAL','GAN','GCA','GCM','GGE','GNE','GOC','GOI','GPA','GPR','GRA','GSU','LBR','PVL') then b.DWHCLIRES
 																			   when b.DWHCLIRU2 like '' and b.DWHCLIRES not in ('GAG','GAL','GAN','GCA','GCM','GGE','GNE','GOC','GOI','GPA','GPR','GRA','GSU','LBR','PVL') then 'BAN' 
																			   when b.DWHCLIRU2 not like '' then b.DWHCLIRU2
																			   else (case when c.DWHCLIRES in ('GAG','GAL','GAN','GCA','GCM','GGE','GNE','GOC','GOI','GPA','GPR','GRA','GSU','LBR','PVL') then c.DWHCLIRES
																					 else 'BAN'
																					 end)     
																		  end)
			 end)
-- Reprise du code caisse_CRM de SAB après migration (à partir d'octobre 2018)
	  when @date_analyse > '2018-09-30' then 
	       (case when c.DWHCLICOL = 1 then
	                 (case when b.DWHCLIRU2  like '' and b.DWHCLIRES in ('GAG','GAL','GAN','GCA','GCM','GGE','GNE','GOC','GOI','GPA','GPR','GRA','GSU','LBR','PVL')
                           then b.DWHCLIRES
 	                       when b.DWHCLIRU2 like '' and b.DWHCLIRES not in ('GAG','GAL','GAN','GCA','GCM','GGE','GNE','GOC','GOI','GPA','GPR','GRA','GSU','LBR','PVL')
                           then 'BAN' 
				           when b.DWHCLIRU2 not like ''
				           then b.DWHCLIRU2
					       else (case when c.DWHCLIRES in ('GAG','GAL','GAN','GCA','GCM','GGE','GNE','GOC','GOI','GPA','GPR','GRA','GSU','LBR','PVL') then c.DWHCLIRES
					                  else 'BAN'
						         end)     
					  end)
	             when c.DWHCLICOL = 0 then
				     (case when c.DWHCLIRU2 like '' and c.DWHCLIRES in ('GAG','GAL','GAN','GCA','GCM','GGE','GNE','GOC','GOI','GPA','GPR','GRA','GSU','LBR','PVL')
                           then c.DWHCLIRES
	                       when c.DWHCLIRU2 like '' and c.DWHCLIRES not in ('GAG','GAL','GAN','GCA','GCM','GGE','GNE','GOC','GOI','GPA','GPR','GRA','GSU','LBR','PVL')
                           then 'BAN'
			               when c.DWHCLICOL = 0 and c.DWHCLIRU2 not like ''
				           then c.DWHCLIRU2
				      end)
			end)
	  end as cd_caisse_crm  
,c.DWHCLIBIL
,c.DWHCLIBIM
,c.DWHCLIATR
,c.DWHCLIBLN
,c.DWHCLISTA
,c.DWHCLITIE as titu_princp
,c.DWHCLIMUT
,c.DWHCLIACC
,c.DWHCLIDGI
,c.DWHCLIFOJ
,c.DWHCLINFI
,c.DWHCLIAD1
,c.DWHCLICOP
,c.DWHCLIVIL
,c.DWHCLIDEP
,c.DWHCLICDN
,c.DWHCLICCN
,c.DWHCLICPI
,c.DWHCLIIDT
,c.DWHCLIIDN
,c.DWHCLIPYR
,c.DWHCLIPYN
,c.DWHCLIMBI
,c.DWHCLICON
,c.DWHCLICIB
,c.DWHCLITTI
,c.DWHCLIDAS
,c.DWHCLICPJ
,c.DWHCLIDPJ
,c.DWHCLIRIS
,a.IDCustomer__pc as IDENTIFIANT_CLIENT_CRM
,a.CreateIDCustomer__pc as STATUT_CREATION_CLIENT_CRM
,a.QuoteRisk__c as cot_risq
,left(replace(substring(DistributorData__c,patindex('%Principal%',DistributorData__c) - patindex('%Nom%',DistributorData__c)-2,patindex('%Nom%',DistributorData__c)),'|',''),2) as reseau_distributeur --modification 11/07/2019
,o.DistributionChannel__c as canal_dis
,case when left(replace(substring(DistributorData__c,patindex('%Principal%',DistributorData__c) - patindex('%Nom%',DistributorData__c)-2,patindex('%Nom%',DistributorData__c)),'|',''),2) in ('01','02')
      then replace(substring(replace(replace(replace(CAST('<t>' + replace(DistributorData__c,'-','</t><t>') + '</t>' AS XML).value('/t[2]','nvarchar(2000)'),@SC,'*'),char(13),''),char(10),''),charindex(@t,replace(replace(replace(CAST('<t>' + replace(DistributorData__c,'-','</t><t>') + '</t>' AS XML).value('/t[2]','nvarchar(2000)'),@SC,'*'),char(13),''),char(10),'')),12),@t,'')	--modification 11/07/2019
	  else '' end as score_preattribution_orange

,case when left(replace(substring(DistributorData__c,patindex('%Principal%',DistributorData__c) - patindex('%Nom%',DistributorData__c)-2,patindex('%Nom%',DistributorData__c)),'|',''),2) in ('03')
	  then replace(substring(replace(replace(replace(CAST('<t>' + replace(DistributorData__c,'-','</t><t>') + '</t>' AS XML).value('/t[2]','nvarchar(2000)'),@SC,'*'),char(13),''),char(10),''),charindex(@t,replace(replace(replace(CAST('<t>' + replace(DistributorData__c,'-','</t><t>') + '</t>' AS XML).value('/t[2]','nvarchar(2000)'),@SC,'*'),char(13),''),char(10),'')),12),@t,'')	--modification 11/07/2019
	  else '' end as score_preattribution_groupama
,o.StartedChannel__c as canal_org 
,o.RelationEntryScore__c as score_ER 
,NULL as FLAG_CLIENT_EQUIPE
,ROW_NUMBER() OVER(PARTITION BY  c.DWHCLICLI ORDER BY a.Id_SF,a.CreatedDate ASC)  AS RowID
from [$(DataHubDatabaseName)].[dbo].[PV_SA_M_CLIENT_AS_OF_DATE](@date_analyse) c
--caractéristiques du titulaire de référence : modification 26-03-2019
left join [$(DataHubDatabaseName)].[dbo].[PV_SA_M_CLIENT_AS_OF_DATE](@date_analyse) b
        on  c.DWHCLITIE = b.DWHCLICLI
left join [$(DataHubDatabaseName)].[dbo].[PV_SF_ACCOUNT_AS_OF_DATE](@date_analyse) a
        on  c.DWHCLICLI = a.[IDCustomerSAB__pc] 
left join [$(DataHubDatabaseName)].[dbo].[PV_SF_OPPORTUNITY_HISTORY_AS_OF_DATE](@date_analyse) o 
        on   o.AccountId = a.Id_SF
               and o.StageName = '09' 
               and o.CommercialOfferName__c = 'Compte bancaire'  -- à valider avec Meryem
left join [$(DataHubDatabaseName)].[dbo].[REPRISE_CODE_CAISSE] r
        on c.DWHCLICLI = r.NUMERO_CLIENT and r.DATE_ANALYSE = @date_analyse
left join dbo.BF_CLIENTS s
on c.DWHCLICLI = s.NUMERO_CLIENT and s.DATE_EXPLOITATION = EOMONTH (@date_analyse, -1)
where c.DWHCLIAGE not in ('51','52')							
) r

--------------------------------------------------------------------------------///////////////// Step 2
---------------------------calcul des codes reseau + marché + creation du champ ACTIVITE_CLIENT
select NUMERO_CLIENT
      ,AGENCE 
      ,NOM_CLIENT
      ,PRENOM_CLIENT
      ,SIGLE_USUEL_CLIENT
      ,CODE_SIREN
      ,CODE_SIRET
      ,DATE_CREATION_CLIENT
      ,DWHCLICRT
      ,DATE_FIN_RELATION_CLIENT
      ,date_naissance
      ,DWHCLINAT
      ,DWHCLICRE
      ,DWHCLIRSD
      ,cd_responsable
      ,quali
      ,DWHCLISAC
      ,cd_PCS
      ,DWHCLIREG
      ,cate
      ,DWHCLIGEO
      ,DWHCLIENT
      ,DWHCLIACT
      ,DWHCLICRD
      ,cotation_interne
      ,DWHCLIDIR
      ,DWHCLIPRF
      ,DWHCLINBE
      ,DWHCLIRGM
      ,DWHCLIEFC
      ,DWHCLICHF
      ,DWHCLIANF
      ,capacite_juridique
      ,DWHCLIREV
      ,DWHCLICHA
      ,DWHCLIPAT
      ,DWHCLISER
      ,DWHCLISEP
      ,cd_douteux
      ,clt_douteux
      ,interdit_chequier
      ,type_interdit
      ,indicateur_deces
      ,DWHCLIPNA
      ,situation_familiale
      ,tiers_coll
      ,DWHCLIGRI
      ,DWHCLITRI
      ,dt_litigieux_contentieux
      ,motif_litigieux_contentieux
      ,DWHCLIPRO
      ,DWHCLIDOM
      ,DWHCLIEST
      ,cd_appo
	  ,cd_caisse_crm								-- ajout 15/01/2019
      ,DWHCLIBIL
      ,DWHCLIBIM
      ,DWHCLIATR
      ,DWHCLIBLN
      ,DWHCLISTA
      ,titu_princp
      ,DWHCLIMUT
      ,DWHCLIACC
      ,DWHCLIDGI
      ,DWHCLIFOJ
      ,DWHCLINFI
      ,DWHCLIAD1
      ,DWHCLICOP
      ,DWHCLIVIL
      ,DWHCLIDEP
      ,DWHCLICDN
      ,DWHCLICCN
      ,DWHCLICPI
      ,DWHCLIIDT
      ,DWHCLIIDN
      ,DWHCLIPYR
      ,DWHCLIPYN
      ,DWHCLIMBI
      ,DWHCLICON
      ,DWHCLICIB
      ,DWHCLITTI
      ,DWHCLIDAS
      ,DWHCLICPJ
      ,DWHCLIDPJ
      ,DWHCLIRIS
	  ,IDENTIFIANT_CLIENT_CRM
	  ,STATUT_CREATION_CLIENT_CRM
      ,cot_risq
	  ,reseau_distributeur
	  ,canal_dis
	  ,case when score_preattribution_orange like '%Aucun%'
			then '' else score_preattribution_orange end as score_preattribution_orange
	  ,case when score_preattribution_groupama like '%Aucun%'
            then '' else score_preattribution_groupama end as score_preattribution_groupama
	  ,canal_org
	  ,score_ER 
	  ,FLAG_CLIENT_EQUIPE
	  ,r2.[code_reseau] as cd_res					-- modification 26-03-2019
	  ,r2.libelle_reseau as lib_res
	  ,m2.[code_marche] as cd_march					-- modification 26-03-2019
	  ,m2.libelle_marche_n2 as lib_march
	  ,NULL as ACTIVITE_CLIENT
	  ,@date_analyse as DATE_EXPLOITATION
	  ,@date_action as DATE_ACTION
into #res2	  
from  #res1 r 
left join [$(DataHubDatabaseName)].[dbo].[REF_MARCHE] m2 
        on r.cd_responsable = m2.[code_responsable]
left join [$(DataHubDatabaseName)].[dbo].[REF_RESEAU] r2 
        on r.cd_caisse_crm = r2.[code_apporteur]	-- modification 15/01/2019
where RowID=1; 
-----------------------------------------------------------///////////////// Step 3	   
--insertion dans la table cible  
	   
insert into BF_CLIENTS
select 
	NUMERO_CLIENT
   ,AGENCE 
   ,NOM_CLIENT
   ,PRENOM_CLIENT
   ,SIGLE_USUEL_CLIENT
   ,CODE_SIREN
   ,CODE_SIRET
   ,DATE_CREATION_CLIENT
   ,DWHCLICRT
   ,DATE_FIN_RELATION_CLIENT
   ,date_naissance
   ,DWHCLINAT
   ,DWHCLICRE
   ,DWHCLIRSD
   ,cd_responsable
   ,quali
   ,DWHCLISAC
   ,cd_PCS
   ,DWHCLIREG
   ,cate
   ,DWHCLIGEO
   ,DWHCLIENT
   ,DWHCLIACT
   ,DWHCLICRD
   ,cotation_interne
   ,DWHCLIDIR
   ,DWHCLIPRF
   ,DWHCLINBE
   ,DWHCLIRGM
   ,DWHCLIEFC
   ,DWHCLICHF
   ,DWHCLIANF
   ,capacite_juridique
   ,DWHCLIREV
   ,DWHCLICHA
   ,DWHCLIPAT
   ,DWHCLISER
   ,DWHCLISEP
   ,cd_douteux
   ,clt_douteux
   ,interdit_chequier
   ,type_interdit
   ,indicateur_deces
   ,DWHCLIPNA
   ,situation_familiale
   ,tiers_coll
   ,DWHCLIGRI
   ,DWHCLITRI
   ,dt_litigieux_contentieux
   ,motif_litigieux_contentieux
   ,DWHCLIPRO
   ,DWHCLIDOM
   ,DWHCLIEST
   ,cd_appo
   ,cd_caisse_crm  -- ajout 15/01/2019
   ,DWHCLIBIL
   ,DWHCLIBIM
   ,DWHCLIATR
   ,DWHCLIBLN
   ,DWHCLISTA
   ,titu_princp
   ,DWHCLIMUT
   ,DWHCLIACC
   ,DWHCLIDGI
   ,DWHCLIFOJ
   ,DWHCLINFI
   ,DWHCLIAD1
   ,DWHCLICOP
   ,DWHCLIVIL
   ,DWHCLIDEP
   ,DWHCLICDN
   ,DWHCLICCN
   ,DWHCLICPI
   ,DWHCLIIDT
   ,DWHCLIIDN
   ,DWHCLIPYR
   ,DWHCLIPYN
   ,DWHCLIMBI
   ,DWHCLICON
   ,DWHCLICIB
   ,DWHCLITTI
   ,DWHCLIDAS
   ,DWHCLICPJ
   ,DWHCLIDPJ
   ,DWHCLIRIS
   ,IDENTIFIANT_CLIENT_CRM
   ,STATUT_CREATION_CLIENT_CRM
   ,cot_risq
   ,reseau_distributeur
   ,canal_dis
   ,replace(replace(score_preattribution_orange,char(13),''),char(10),'')
   ,replace(replace(score_preattribution_groupama,char(13),''),char(10),'')
   ,canal_org
   ,score_ER 
   ,FLAG_CLIENT_EQUIPE
   ,cd_res
   ,lib_res
   ,cd_march
   ,lib_march
   ,ACTIVITE_CLIENT
   ,DATE_EXPLOITATION
   ,DATE_ACTION
from #res2 r 

END
