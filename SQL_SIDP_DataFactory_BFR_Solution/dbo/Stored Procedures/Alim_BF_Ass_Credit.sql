﻿CREATE PROCEDURE [dbo].[Alim_BF_Ass_Credit] 
(
   @date_analyse DATE
)
AS
BEGIN
    SET NOCOUNT ON;

    declare @date_action DATE
    set @date_action = DATEADD (MONTH, DATEDIFF (MONTH,0,@date_analyse)+1,0);

-- insertion dans la table cible

    insert into dbo.BF_ASS_CREDIT
	SELECT 
	DWHASSETA as COD_ETABLISSEMENT,
	DWHASSAGE as COD_AGENCE,
	DWHASSSER as COD_SERVICE,
	DWHASSSSE as COD_SOUS_SERVICE,
	DWHASSOPE as COD_OPERATION,
	DWHASSNAT as COD_NATURE,
	DWHASSNUM as NUMERO_PRET,
	DWHASSNUA as NUMERO_ASSURANCE,
	DWHASSCOA as COD_ASSURANCE,
	DWHASSMOA as MONT_ASSURANCE,
	DWHASSDEV as COD_DEVISE,
	DWHASSTAU as TAUX_ASSURE,
	DWHASSCLI as NUMERO_CLIENT_ASSURE,
	DWHASSTYP as IND_ROLE_CLIENT,
	@date_analyse as DT_EXPLOITATION,
    @date_action  as DATE_ACTION
from  [$(DataHubDatabaseName)].dbo.PV_SA_M_ASSCRE_AS_OF_DATE(@date_analyse) 
END