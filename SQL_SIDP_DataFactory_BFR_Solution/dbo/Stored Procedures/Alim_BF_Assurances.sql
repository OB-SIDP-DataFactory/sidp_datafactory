﻿CREATE PROCEDURE [dbo].[Alim_BF_Assurances]
(
   @date_analyse DATE
)
AS
BEGIN
    SET NOCOUNT ON;

    declare @date_action DATE
    set @date_action = CAST(DATEADD (MONTH, DATEDIFF (MONTH,0,@date_analyse)+1,0) AS DATE)

    --------------------------------------------///////////////// Step 1
    --extraction des données brut + calcul du code type de la convention

    select [numero_compte], [numero_client], [cd_convention], [dt_db_convention], [dt_fn_convention], [cd_type_convention_assurance1], [cd_type_convention_assurance2], [cd_service], [dt_db_service], [dt_fn_service], [dt_resi_service], [dt_resi_convention], [dt_exploitation], [dt_action] into #res1
    from
    (
    select c.NUMERO_COMPTE as numero_compte
         , c.NUMERO_CLIENT as numero_client
         , t1.DWHABOPRO as cd_convention
         , t1.DWHABOADH as dt_db_convention
         , t1.DWHABOFIN as dt_fn_convention
         , t1.DWHABOPRO as cd_type_convention_assurance1
         , t2.DWHABSCSE as cd_type_convention_assurance2
         , t2.DWHABSCSE as cd_service
         , t2.DWHABSADH as dt_db_service
         , t2.DWHABSFIN as dt_fn_service
         , t2.DWHABSRES as dt_resi_service
         , t1.DWHABORES as dt_resi_convention
		 , @date_analyse as dt_exploitation
         , c.DATE_ACTION as dt_action
      from ( select NUMERO_CLIENT, NUMERO_COMPTE, DATE_ACTION from BF_COMPTES where DATE_ACTION = @date_action ) c
      left join [$(DataHubDatabaseName)].[dbo].[PV_SA_M_ABONNEMENT_AS_OF_DATE](@date_analyse) t1
        on t1.DWHABOCOM = c.NUMERO_COMPTE and t1.DWHABOCET = 2
      left join [$(DataHubDatabaseName)].[dbo].[PV_SA_M_SERVICE_AS_OF_DATE](@date_analyse) t2
        on t2.DWHABSSER = t1.DWHABOSER
       and t2.DWHABSSSE = t1.DWHABOSSE
       and t2.DWHABSNUM = t1.DWHABONUM
    )r

    ------------------------------///////////////// Step 2
    --insertion dans la table cible

    insert into BF_ASSURANCES ([NUMERO_COMPTE],[NUMERO_CLIENT],[CODE_CONVENTION],[DATE_ADHESION_CONVENTION_DEBUT]
                              ,[DATE_ADHESION_CONVENTION_FIN],[CODE_TYPE_ASSURANCE],[CODE_SERVICE],[DATE_ADHESION_SERVICE_DEBUT]
                              ,[DATE_ADHESION_SERVICE_FIN],[DATE_RESILIATION_SERVICE],[DATE_RESILIATION_CONVENTION],[DATE_EXPLOITATION], [DATE_ACTION])
    select r.numero_compte
         , r.numero_client
         , r.cd_convention
         , r.dt_db_convention
         , r.dt_fn_convention
         , a.[code_assurance]
         , r.cd_service
         , r.dt_db_service
         , r.dt_fn_service
         , r.dt_resi_service
         , r.dt_resi_convention
		 , r.dt_exploitation
         , r.dt_action
      from #res1 r
      join [$(DataHubDatabaseName)].[dbo].[REF_TRANSCO_ASSURANCE] a
        on a.[code_service] = r.cd_type_convention_assurance2
       and a.[code_abonnement] = r.cd_type_convention_assurance1
END