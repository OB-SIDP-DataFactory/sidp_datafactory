﻿CREATE PROCEDURE [dbo].[Alim_BF_LK_GARANTIE] 
(
   @date_analyse DATE
)
AS
BEGIN
    SET NOCOUNT ON;

    declare @date_action DATE
    set @date_action = DATEADD (MONTH, DATEDIFF (MONTH,0,@date_analyse)+1,0);

-- insertion dans la table cible

    insert into dbo.BF_LK_GARANTIE

	SELECT 
	 DWHLIEETA     as COD_ETABLISSEMENT
    ,DWHLIEAGE     as COD_AGENCE
    ,DWHLIESER     as COD_SERVICE
    ,DWHLIESSE     as COD_SOUS_SERVICE
    ,DWHLIEOPE     as COD_OPERATION
    ,DWHLIENAT     as COD_NATURE
    ,DWHLIENDO     as NUMERO_OPERATION
    ,DWHLIEAG2     as AGENCE_RATTACHEMENT
    ,DWHLIESE2     as SERVICE_RATTACHEMENT
    ,DWHLIESS2     as SPUS_SERVICE_RATTACHEMENT
    ,DWHLIEOP2     as COD_OPERATION_RATTACHEMENT
    ,DWHLIERAT     as NUMERO_RATTACHEMENT
    ,DWHLIEPRC     as PCT_COUVERTURE_GARANTIE
    ,DWHLIECDG     as NUMERO_COD_GARANTIE
    ,DWHLIEORG     as ORIGINE_LIEN
    ,DWHLIEPER     as PERIMETRE_RISQUE
    ,DWHLIEFRE     as FREQUENCE
    ,@date_analyse as DT_EXPLOITATION
    ,@date_action  as DT_ACTION

from  [$(DataHubDatabaseName)].dbo.PV_SA_M_LKGAR_AS_OF_DATE(@date_analyse) 
where   DWHLIEAGE  not in (51,52)

END