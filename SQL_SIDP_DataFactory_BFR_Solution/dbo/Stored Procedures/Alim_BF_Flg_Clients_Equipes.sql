﻿
CREATE PROCEDURE [dbo].[Alim_BF_Flg_Clients_Equipes] 
(
   @date_analyse DATE
)
AS
BEGIN
   
--declare @date_analyse DATE
  declare @date_action DATE
--set @date_analyse =  (select MAX(DWHCPTDTX) from [$(DataHubDatabaseName)].[dbo].[PV_SA_M_COMPTE])
  set @date_action= DATEADD (MONTH, DATEDIFF (MONTH,0,@date_analyse)+1,0)

SET NOCOUNT ON;

----------------------------------------///////////////// Step 1
--preparation de la table intermediaire (numero client - FCE)

select 
 r.NUMERO_CLIENT
,'O' as FLAG_CLIENT_EQUIPE 
,r.DATE_EXPLOITATION
into #FCE1
from
(
select 
 row_number() over(partition by NUMERO_CLIENT ORDER BY REFERENCE_PRODUIT desc) as rang
,NUMERO_CLIENT 
,DATE_EXPLOITATION
from [dbo].BF_CLIENTS_EQUIPEMENT
where DATE_EXPLOITATION = @date_analyse
) r
where r.rang = 1

select
 NUMERO_CLIENT
,case when FCE = 'O' then 'O'
      when (TIERS_COLLECTIF = 0 and FCE is NULL) then 'N'
	  else NULL
	  end as FLAG_CLIENT_EQUIPE
,DATE_EXPLOITATION
into #FCE2
from
(
select 
  clieq.FLAG_CLIENT_EQUIPE as FCE
 ,cli.NUMERO_CLIENT 
 ,TIERS_COLLECTIF
 ,cli.DATE_EXPLOITATION
from [dbo].BF_CLIENTS cli
left join #FCE1 clieq
on cli.NUMERO_CLIENT = clieq.NUMERO_CLIENT and cli.DATE_EXPLOITATION = clieq.DATE_EXPLOITATION
where cli.DATE_EXPLOITATION = @date_analyse
) r

----------------------------------------///////////////// Step 2
--mise à jour du champ FLAG_CLIENT_EQUIPE de la table BF_CLIENTS

update cl
set cl.FLAG_CLIENT_EQUIPE = r.FLAG_CLIENT_EQUIPE
from dbo.BF_CLIENTS cl
inner join #FCE2 r   
       on r.NUMERO_CLIENT = cl.NUMERO_CLIENT and r.DATE_EXPLOITATION = cl.DATE_EXPLOITATION

END