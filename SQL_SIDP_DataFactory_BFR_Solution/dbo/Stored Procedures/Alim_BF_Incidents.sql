﻿

CREATE PROCEDURE [dbo].[Alim_BF_Incidents] 
(
   @date_analyse DATE
)
AS
BEGIN
    SET NOCOUNT ON;

    declare @date_action DATE
    set @date_action = DATEADD (MONTH, DATEDIFF (MONTH,0,@date_analyse)+1,0);

-- insertion dans la table cible

    insert into dbo.BF_INCIDENTS

	SELECT 
	DWHFIMETA as COD_ETABLISSEMENT,
	DWHFIMTYP as TYP_INCIDENT,
	DWHFIMNUM as NUMERO_INCIDENT,
	DWHFIMCLT as NUMERO_CLIENT,
	DWHFIMPLA as NUMERO_PLAN,
	DWHFIMCPT as NUMERO_COMPTE,
	DWHFIMNUC as NUMERO_CHEQUE,
	DWHFIMSEQ as NUMERO_SEQUENCE,
	DWHFIMEVE as DAT_EVENEMENT,
	DWHFIMDEV as DEVISE,
	DWHFIMMON as MONT_GLOBAL_INCIDENT_DEVISE_CPT,
	DWHFIMMOB as MONT_GLOBAL_INCIDENT_DEVISE_BASE,
	@date_analyse as DT_EXPLOITATION,
    @date_action  as DATE_ACTION

from  [$(DataHubDatabaseName)].dbo.PV_SA_M_INCIDENT_AS_OF_DATE(@date_analyse) 
END
