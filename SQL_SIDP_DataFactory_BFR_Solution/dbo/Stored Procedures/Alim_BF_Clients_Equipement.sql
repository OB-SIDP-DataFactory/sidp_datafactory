﻿CREATE PROCEDURE [dbo].[Alim_BF_Clients_Equipement] 
(
   @date_analyse DATE
)
AS
BEGIN
   
--declare @date_analyse DATE
  declare @date_action DATE
--set @date_analyse =  (select MAX(DWHCPTDTX) from [$(DataHubDatabaseName)].[dbo].[PV_SA_M_COMPTE])
  set @date_action= CAST(DATEADD (MONTH, DATEDIFF (MONTH,0,@date_analyse)+1,0) AS DATE)

SET NOCOUNT ON;

--extraction des donnees 
select 
 NUMERO_CLIENT
,NUMERO_CLIENT_GROUPE
,REFERENCE_PRODUIT
,TYPE_PRODUIT
,DATE_OUVERTURE
,DATE_CLOTURE
,DATE_EXPLOITATION
,DATE_ACTION
into #res2
from 
(

--clients detenteurs de comptes
--cas 1 : comptes + tiers collectif = 0
select
 cli.NUMERO_CLIENT
,NULL as NUMERO_CLIENT_GROUPE
,cpt.NUMERO_COMPTE as REFERENCE_PRODUIT
,case when pdt2.code_regroupement_SIDP in ('EP','EL','CV') then cpt.CODE_PRODUIT_SAB 
 else NULL end as TYPE_PRODUIT
,cpt.DATE_OUVERTURE
,cpt.DATE_CLOTURE
,cli.DATE_EXPLOITATION
,cli.DATE_ACTION
from [dbo].BF_CLIENTS cli
left join [dbo].[BF_COMPTES] cpt
on cpt.NUMERO_CLIENT = cli.NUMERO_CLIENT and cpt.DATE_EXPLOITATION = cli.DATE_EXPLOITATION
left join [$(DataHubDatabaseName)].dbo.REF_PRODUIT pdt2
on cpt.CODE_PRODUIT_DF = pdt2.code_produit_SIDP
where cli.DATE_EXPLOITATION = @date_analyse
and pdt2.code_regroupement_SIDP in ('EP','EL','CV')
and (cpt.DATE_CLOTURE is NULL or cpt.DATE_CLOTURE > EOMONTH (@date_analyse, -1))
and cli.TIERS_COLLECTIF = 0

union

--clients detenteurs de comptes
--cas 2 : comptes + tiers collectif = 1
select
 grp.NUMERO_CLIENT as NUMERO_CLIENT
,grp.NUMERO_CLIENT_GROUPE as NUMERO_CLIENT_GROUPE
,cpt.NUMERO_COMPTE as REFERENCE_PRODUIT
,case when pdt2.code_regroupement_SIDP in ('EP','EL','CV') then cpt.CODE_PRODUIT_SAB 
 else NULL end as TYPE_PRODUIT
,cpt.DATE_OUVERTURE
,cpt.DATE_CLOTURE
,cli.DATE_EXPLOITATION
,cli.DATE_ACTION
from [dbo].BF_CLIENTS cli
left join [dbo].[BF_COMPTES] cpt
on cpt.NUMERO_CLIENT = cli.NUMERO_CLIENT and cpt.DATE_EXPLOITATION = cli.DATE_EXPLOITATION
left join [$(DataHubDatabaseName)].dbo.REF_PRODUIT pdt2
on cpt.CODE_PRODUIT_DF = pdt2.code_produit_SIDP
left join [dbo].BF_LIEN_CLIENTS grp
on cpt.NUMERO_CLIENT = grp.NUMERO_CLIENT_GROUPE and cpt.DATE_EXPLOITATION = grp.DATE_EXPLOITATION
where cli.DATE_EXPLOITATION = @date_analyse
and pdt2.code_regroupement_SIDP in ('EP','EL','CV')
and (cpt.DATE_CLOTURE is NULL or cpt.DATE_CLOTURE > EOMONTH (@date_analyse, -1))
and cli.TIERS_COLLECTIF = 1

union

--cas 3 : credits immobiliers + tiers collectif = 0
select
 cli.NUMERO_CLIENT
,NULL as NUMERO_CLIENT_GROUPE
,cre.NUMERO_PRET as REFERENCE_PRODUIT
,case when pdt2.code_regroupement_SIDP in ('CR') then cre.CODE_NATURE_CREDIT_SAB
 else NULL end as TYPE_PRODUIT
,cre.DATE_OUVERTURE
,cre.DATE_CLOTURE
,cli.DATE_EXPLOITATION
,cli.DATE_ACTION
from [dbo].BF_CLIENTS cli
left join  [dbo].[BF_CREDITS] cre
on cre.NUMERO_CLIENT = cli.NUMERO_CLIENT and cre.DATE_EXPLOITATION = cli.DATE_EXPLOITATION
left join [$(DataHubDatabaseName)].dbo.REF_PRODUIT pdt2
on cre.CODE_PRODUIT_DF = pdt2.code_produit_SIDP
where cli.DATE_EXPLOITATION = @date_analyse
and pdt2.code_regroupement_SIDP in ('CR')
and  (cre.DATE_CLOTURE is NULL or cre.DATE_CLOTURE > EOMONTH (@date_analyse, -1))
and cli.TIERS_COLLECTIF = 0

union

--cas 4 : credits immobiliers + tiers collectif = 1
select
 grp.NUMERO_CLIENT as NUMERO_CLIENT
,grp.NUMERO_CLIENT_GROUPE as NUMERO_CLIENT_GROUPE
,cre.NUMERO_PRET as REFERENCE_PRODUIT
,case when pdt2.code_regroupement_SIDP in ('CR') then cre.CODE_NATURE_CREDIT_SAB
 else NULL end as TYPE_PRODUIT
,cre.DATE_OUVERTURE
,cre.DATE_CLOTURE
,cli.DATE_EXPLOITATION
,cli.DATE_ACTION
from [dbo].BF_CLIENTS cli
left join [dbo].[BF_CREDITS] cre
on cre.NUMERO_CLIENT = cli.NUMERO_CLIENT and cre.DATE_EXPLOITATION = cli.DATE_EXPLOITATION
left join [$(DataHubDatabaseName)].dbo.REF_PRODUIT pdt2
on cre.CODE_PRODUIT_DF = pdt2.code_produit_SIDP
left join [dbo].BF_LIEN_CLIENTS grp
on cre.NUMERO_CLIENT = grp.NUMERO_CLIENT_GROUPE and cre.DATE_EXPLOITATION = grp.DATE_EXPLOITATION
where cli.DATE_EXPLOITATION = @date_analyse
and pdt2.code_regroupement_SIDP in ('CR')
and  (cre.DATE_CLOTURE is NULL or cre.DATE_CLOTURE > EOMONTH (@date_analyse, -1))
and cli.TIERS_COLLECTIF = 1

union

--cas 5 : credits FF AMORTISSABLES + tiers collectif = 0
select
 cli.NUMERO_CLIENT
,NULL as NUMERO_CLIENT_GROUPE
,cre.NUMERO_PRET as REFERENCE_PRODUIT
,case when pdt2.code_regroupement_SIDP in ('DE','CA') then cre.CODE_PRODUIT_CRM
 else NULL end as TYPE_PRODUIT
,cre.DATE_OUVERTURE
,amo.FFM_DATE_FIN_PRET as DATE_CLOTURE
,cli.DATE_EXPLOITATION
,cli.DATE_ACTION
from [dbo].BF_CLIENTS cli
left join [dbo].[BF_CREDITS] cre
on cre.NUMERO_CLIENT = cli.NUMERO_CLIENT and cre.DATE_EXPLOITATION = cli.DATE_EXPLOITATION
left join [dbo].[BF_CREDIT_AMORTISSABLE] amo
on cre.NUMERO_PRET = amo.NUMERO_PRET and cre.DATE_EXPLOITATION = amo.DATE_EXPLOITATION
left join [dbo].[BF_COMPTES] cpt
on cpt.NUMERO_COMPTE = cre.NUMERO_COMPTE_MIROIR and cre.DATE_EXPLOITATION = cpt.DATE_EXPLOITATION
left join [$(DataHubDatabaseName)].dbo.REF_PRODUIT pdt2
on cre.CODE_PRODUIT_DF = pdt2.code_produit_SIDP
where cli.DATE_EXPLOITATION = @date_analyse
and pdt2.code_regroupement_SIDP in ('DE','CA')
and (amo.FFM_DATE_FIN_PRET > EOMONTH (@date_analyse, -1))
and cpt.DATE_OUVERTURE is not NULL
and cli.TIERS_COLLECTIF = 0
--and amo.FFQ_DATE_REALISATION is not NULL

union

--cas 6 : credits FF AMORTISSABLES + tiers collectif = 1
select
 grp.NUMERO_CLIENT as NUMERO_CLIENT
,grp.NUMERO_CLIENT_GROUPE as NUMERO_CLIENT_GROUPE
,cre.NUMERO_PRET as REFERENCE_PRODUIT
,case when pdt2.code_regroupement_SIDP in ('DE','CA') then cre.CODE_PRODUIT_CRM
 else NULL end as TYPE_PRODUIT
,cre.DATE_OUVERTURE
,amo.FFM_DATE_FIN_PRET as DATE_CLOTURE
,cli.DATE_EXPLOITATION
,cli.DATE_ACTION
from [dbo].BF_CLIENTS cli
left join [dbo].[BF_CREDITS] cre
on cre.NUMERO_CLIENT = cli.NUMERO_CLIENT and cre.DATE_EXPLOITATION = cli.DATE_EXPLOITATION
left join [dbo].[BF_CREDIT_AMORTISSABLE] amo
on cre.NUMERO_PRET = amo.NUMERO_PRET and cre.DATE_EXPLOITATION = amo.DATE_EXPLOITATION
left join [dbo].[BF_COMPTES] cpt
on cpt.NUMERO_COMPTE = cre.NUMERO_COMPTE_MIROIR and cre.DATE_EXPLOITATION = cpt.DATE_EXPLOITATION
left join [$(DataHubDatabaseName)].dbo.REF_PRODUIT pdt2
on cre.CODE_PRODUIT_DF = pdt2.code_produit_SIDP
left join [dbo].BF_LIEN_CLIENTS grp
on cre.NUMERO_CLIENT = grp.NUMERO_CLIENT_GROUPE and cre.DATE_EXPLOITATION = grp.DATE_EXPLOITATION
where cli.DATE_EXPLOITATION = @date_analyse
and pdt2.code_regroupement_SIDP in ('DE','CA')
and (amo.FFM_DATE_FIN_PRET > EOMONTH (@date_analyse, -1))
and cre.DATE_OUVERTURE is not NULL
and cli.TIERS_COLLECTIF = 1
--and amo.FFQ_DATE_REALISATION is not NULL

union

--cas 7 : credits FF RENOUVELABLES + tiers collectif = 0
select
 cli.NUMERO_CLIENT
,NULL as NUMERO_CLIENT_GROUPE
,cre.NUMERO_PRET as REFERENCE_PRODUIT
,case when pdt2.code_regroupement_SIDP in ('CO') then cre.CODE_PRODUIT_CRM
 else NULL end as TYPE_PRODUIT
,cre.DATE_OUVERTURE
,cpt.DATE_CLOTURE
,cli.DATE_EXPLOITATION
,cli.DATE_ACTION
from [dbo].BF_CLIENTS cli
left join [dbo].[BF_CREDITS] cre
on cre.NUMERO_CLIENT = cli.NUMERO_CLIENT and cre.DATE_EXPLOITATION = cli.DATE_EXPLOITATION
left join [dbo].[BF_COMPTES] cpt
on cpt.NUMERO_COMPTE = cre.NUMERO_COMPTE_MIROIR and cre.DATE_EXPLOITATION = cpt.DATE_EXPLOITATION
left join [$(DataHubDatabaseName)].dbo.REF_PRODUIT pdt2
on cre.CODE_PRODUIT_DF = pdt2.code_produit_SIDP
where cli.DATE_EXPLOITATION = @date_analyse
and pdt2.code_regroupement_SIDP in ('CO')
and (cpt.DATE_CLOTURE is NULL or cpt.DATE_CLOTURE > EOMONTH (@date_analyse, -1))
and cpt.DATE_OUVERTURE is not NULL
and cli.TIERS_COLLECTIF = 0

union

--cas 8 : credits FF RENOUVELABLES + tiers collectif = 1
select
 grp.NUMERO_CLIENT as NUMERO_CLIENT
,grp.NUMERO_CLIENT_GROUPE as NUMERO_CLIENT_GROUPE
,cre.NUMERO_PRET as REFERENCE_PRODUIT
,case when pdt2.code_regroupement_SIDP in ('CO') then cre.CODE_PRODUIT_CRM
 else NULL end as TYPE_PRODUIT
,cre.DATE_OUVERTURE
,cpt.DATE_CLOTURE
,cli.DATE_EXPLOITATION
,cli.DATE_ACTION
from [dbo].BF_CLIENTS cli
left join [dbo].[BF_CREDITS] cre
on cre.NUMERO_CLIENT = cli.NUMERO_CLIENT and cre.DATE_EXPLOITATION = cli.DATE_EXPLOITATION
left join [dbo].[BF_COMPTES] cpt
on cpt.NUMERO_COMPTE = cre.NUMERO_COMPTE_MIROIR and cre.DATE_EXPLOITATION = cpt.DATE_EXPLOITATION
left join [$(DataHubDatabaseName)].dbo.REF_PRODUIT pdt2
on cre.CODE_PRODUIT_DF = pdt2.code_produit_SIDP
left join [dbo].BF_LIEN_CLIENTS grp
on cre.NUMERO_CLIENT = grp.NUMERO_CLIENT_GROUPE and cre.DATE_EXPLOITATION = grp.DATE_EXPLOITATION
where cli.DATE_EXPLOITATION = @date_analyse
and pdt2.code_regroupement_SIDP in ('CO')
and (cpt.DATE_CLOTURE is NULL or cpt.DATE_CLOTURE > EOMONTH (@date_analyse, -1))
and cre.DATE_OUVERTURE is not NULL
and cli.TIERS_COLLECTIF = 1

) r

--insertion dans la table BF_CLIENTS_EQUIPEMENT
insert into BF_CLIENTS_EQUIPEMENT
select NUMERO_CLIENT
	  ,NUMERO_CLIENT_GROUPE
	  ,REFERENCE_PRODUIT
	  ,TYPE_PRODUIT
	  ,DATE_OUVERTURE
	  ,DATE_CLOTURE
	  ,DATE_EXPLOITATION
	  ,DATE_ACTION
from #res2 r 
END