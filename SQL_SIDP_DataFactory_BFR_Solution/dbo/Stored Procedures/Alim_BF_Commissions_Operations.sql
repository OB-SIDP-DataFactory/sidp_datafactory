﻿CREATE PROCEDURE [dbo].[Alim_BF_Commissions_Operations] 
(
   @date_analyse DATE
)
AS
BEGIN
    SET NOCOUNT ON;

    declare @date_action DATE
    set @date_action = DATEADD (MONTH, DATEDIFF (MONTH,0,@date_analyse)+1,0);

-- insertion dans la table cible

    insert into dbo.BF_COMMISSIONS_OPE

	SELECT 
	DWHMM4ETA as COD_ETABLISSEMENT,
	DWHMM4AGE as COD_AGENCE,
	DWHMM4SER as COD_SERVICE,
	DWHMM4SES as COD_SOUS_SERVICE,
	DWHMM4OPE as COD_OPERATION,
	DWHMM4NAT as COD_NATURE,
	DWHMM4NUM as NUMERO_OPERATION,
	DWHMM4SEN as SENS,
	DWHMM4COM as COD_COMMISSION,
	DWHMM4SEQ as NUMERO_SEQUENCE,
	DWHMM4DTD as DAT_DEBUT_SITUATION,
	DWHMM4DEV as DEVISE,
	DWHMM4MON as MONT_COMMISSION,
	DWHMM4MFL as MONT_COMMISSION_FLAT,
	DWHMM4DSY as DAT_SYSTEME,
	@date_analyse as DT_EXPLOITATION,
    @date_action  as DATE_ACTION

from  [$(DataHubDatabaseName)].dbo.PV_SA_M_COMOPE_AS_OF_DATE(@date_analyse) 
END
