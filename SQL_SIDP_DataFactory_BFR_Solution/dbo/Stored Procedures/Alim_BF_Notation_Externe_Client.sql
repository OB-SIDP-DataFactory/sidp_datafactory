﻿CREATE PROCEDURE [dbo].[Alim_BF_Notation_Externe_Client] 
(
   @date_analyse DATE
)
AS
BEGIN
    SET NOCOUNT ON;

    declare @date_action DATE
    set @date_action = DATEADD (MONTH, DATEDIFF (MONTH,0,@date_analyse)+1,0);

-- insertion dans la table cible

    insert into dbo.BF_NOTATION_EXTERNE_CLIENT
	SELECT 
	DWHNEXETB as COD_ETABLISSEMENT,
	DWHNEXCLI as NUMERO_CLIENT,
	DWHNEXNOT as COD_ENTITE_NOTATION,
	DWHNEXTYP as TYP_NOTATION,
	DWHNEXDAT as DATE_NOTATION,
	DWHNEXLON as COD_NOTATION_LONG_TERME,
	DWHNEXCOU as COD_NOTATION_COURT_TERME,
	DWHNEXFRE as FREQUENCE,
	DWHNEXDSY as DATE_SYSTEME,
	@date_analyse as DT_EXPLOITATION,
    @date_action  as DATE_ACTION
from  [$(DataHubDatabaseName)].dbo.PV_SA_M_NOTEXTCLI_AS_OF_DATE(@date_analyse) 
END
