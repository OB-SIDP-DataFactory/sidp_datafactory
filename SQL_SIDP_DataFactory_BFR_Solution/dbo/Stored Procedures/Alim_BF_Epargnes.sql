﻿



-- =============================================
-- Author:		
-- Create date:			<27/03/2017>
-- Modification date:	<11/09/2019>
-- Description:	<Alimentation de la table BF_Epargne>
-- =============================================
CREATE PROCEDURE [dbo].[Alim_BF_Epargnes] 
(
   @date_analyse DATE
)
AS
BEGIN
    SET NOCOUNT ON;

    declare @date_action DATE
    set @date_action = DATEADD (MONTH, DATEDIFF (MONTH,0,@date_analyse)+1,0)

------------------------------///////////////// Step 1
--préparation des données

select 
  c.NUMERO_COMPTE as NUMERO_COMPTE
, case when p.code_regroupement_SIDP = 'EP' and c.CODE_PRODUIT_SAB not in ('DAT','ERS','PE2','ER2') then e.DWHEREPLA else e2. DWHENCPLA end as NUMERO_PLAN
, e.DWHERESEQ as NUMERO_SEQUENCE
, e.DWHEREDAD as DATE_DEBUT
, e.DWHEREDTD as DATE_DEBUT_SITUATION
, e.DWHEREDAF as DATE_FIN
, e.DWHEREREC as DATE_RECEPTION
, e.DWHEREECH as DATE_ECHEANCE
, e.DWHEREPRO as TYPE_PRODUIT_ERE
, case when p.code_regroupement_SIDP = 'EP' and c.CODE_PRODUIT_SAB not in ('DAT','ERS','PE2','ER2') then e.DWHERESOL else e2. DWHENCSOL end as SOLDE_FIN_PERIODE
, case when p.code_regroupement_SIDP = 'EP' and c.CODE_PRODUIT_SAB not in ('DAT','ERS','PE2','ER2') then e.DWHERESM1 else e2. DWHENCSM1 end as SOLDE_MOYEN_DEBITEUR
, case when p.code_regroupement_SIDP = 'EP' and c.CODE_PRODUIT_SAB not in ('DAT','ERS','PE2','ER2') then e.DWHERESM2 else e2. DWHENCSM2 end as SOLDE_MOYEN_CREDITEUR
, case when p.code_regroupement_SIDP = 'EP' and c.CODE_PRODUIT_SAB not in ('DAT','ERS','PE2','ER2') then e.DWHEREJDB else e2. DWHENCJDB end as NOMBRE_JOURS_DEBITEURS
, case when p.code_regroupement_SIDP = 'EP' and c.CODE_PRODUIT_SAB not in ('DAT','ERS','PE2','ER2') then e.DWHEREJCR else e2. DWHENCJCR end as NOMBRE_JOURS_CREDITEURS
, case when p.code_regroupement_SIDP = 'EP' and c.CODE_PRODUIT_SAB not in ('DAT','ERS','PE2','ER2') then e.DWHEREDEV else e2. DWHENCDEV end as DEVISE
, case when p.code_regroupement_SIDP = 'EP' and c.CODE_PRODUIT_SAB not in ('DAT','ERS','PE2','ER2') then e.DWHEREIDB else e2. DWHENCIDB end as INTERETS_TRESORERIE_DEBITEURS
, case when p.code_regroupement_SIDP = 'EP' and c.CODE_PRODUIT_SAB not in ('DAT','ERS','PE2','ER2') then e.DWHEREICR else e2. DWHENCICR end as INTERETS_TRESORERIE_CREDITEURS
, case when p.code_regroupement_SIDP = 'EP' and c.CODE_PRODUIT_SAB not in ('DAT','ERS','PE2','ER2') then e.DWHEREMDB else e2. DWHENCMDB end as MARGE_MONTANT_DEBITEUR
, case when p.code_regroupement_SIDP = 'EP' and c.CODE_PRODUIT_SAB not in ('DAT','ERS','PE2','ER2') then e.DWHEREMCR else e2. DWHENCMCR end as MARGE_MONTANT_CREDITEUR
, case when p.code_regroupement_SIDP = 'EP' and c.CODE_PRODUIT_SAB not in ('DAT','ERS','PE2','ER2') then e.DWHERETDB else e2. DWHENCTDB end as MARGE_TAUX_DEBITEUR
, case when p.code_regroupement_SIDP = 'EP' and c.CODE_PRODUIT_SAB not in ('DAT','ERS','PE2','ER2') then e.DWHERETCR else e2. DWHENCTCR end as MARGE_TAUX_CREDITEUR
, case when p.code_regroupement_SIDP = 'EP' and c.CODE_PRODUIT_SAB not in ('DAT','ERS','PE2','ER2') then e.DWHEREID1 else e2. DWHENCID1 end as INTERETS_TRESORERIE_DEBITEURS_CVL
, case when p.code_regroupement_SIDP = 'EP' and c.CODE_PRODUIT_SAB not in ('DAT','ERS','PE2','ER2') then e.DWHEREIC1 else e2. DWHENCIC1 end as INTERETS_TRESORERIE_CREDITEURS_CVL
, case when p.code_regroupement_SIDP = 'EP' and c.CODE_PRODUIT_SAB not in ('DAT','ERS','PE2','ER2') then e.DWHERETD1 else e2.DWHENCTD1 end as INTERETS_DEBITEURS
, case when p.code_regroupement_SIDP = 'EP' and c.CODE_PRODUIT_SAB not in ('DAT','ERS','PE2','ER2') then e.DWHERETC1 else e2.DWHENCTC1 end as INTERETS_CREDITEURS
, case when p.code_regroupement_SIDP = 'EP' and c.CODE_PRODUIT_SAB not in ('DAT','ERS','PE2','ER2') then e.DWHERETD2 else e2.DWHENCTD2 end as INTERETS_DEBITEURS_CVL
, case when p.code_regroupement_SIDP = 'EP' and c.CODE_PRODUIT_SAB not in ('DAT','ERS','PE2','ER2') then e.DWHERETC2 else e2.DWHENCTC2 end as INTERETS_CREDITEURS_CVL
, case when p.code_regroupement_SIDP = 'EP' and c.CODE_PRODUIT_SAB not in ('DAT','ERS','PE2','ER2') then e.DWHERENLE else e2. DWHENCNLE end as NUMERO_LIGNE_EMPLOIS
, case when p.code_regroupement_SIDP = 'EP' and c.CODE_PRODUIT_SAB not in ('DAT','ERS','PE2','ER2') then e.DWHERENLR else e2. DWHENCNLR end as NUMERO_LIGNE_RESSOURCES
, case when p.code_regroupement_SIDP = 'EP' and c.CODE_PRODUIT_SAB not in ('DAT','ERS','PE2','ER2') then e.DWHERETXD else e2. DWHENCTXD end as TAUX_ANALYSE_DEBITEUR
, case when p.code_regroupement_SIDP = 'EP' and c.CODE_PRODUIT_SAB not in ('DAT','ERS','PE2','ER2') then e.DWHERETXC else e2. DWHENCTXC end as TAUX_ANALYSE_CREDITEUR
, e2.DWHENCTCD as CODE_TAUX_ICR			--23/05/2019 ajout du code taux ICR à prende en compte en remplacement du code grille de taux de la table CPTBIS
, e2.DWHENCMCD as MARGE_TAUX_ICR		--23/05/2019 ajout du code taux ICR à prende en compte en remplacement du code grille de taux de la table CPTBIS
, case when p.code_regroupement_SIDP = 'EL' then CAST(REPLACE(e3.HISTORIQUE_MENSUEL_TAUX_VALEUR_TAUX,',','.') as DECIMAL(15,9)) else NULL end as TAUX_INTERET_CONTRACTUEL_CTP
, @date_analyse as DATE_EXPLOITATION
, c.DATE_ACTION
into #BF_EPARGNE
from ( select NUMERO_COMPTE, CODE_PRODUIT_DF, CODE_PRODUIT_SAB, CODE_PRODUIT_CRM, DATE_ACTION  from dbo.BF_COMPTES where DATE_EXPLOITATION = @date_analyse ) c
     join [$(DataHubDatabaseName)].[dbo].REF_PRODUIT p
       on p.code_produit_SIDP = c.CODE_PRODUIT_DF
	  and p.code_regroupement_SIDP in ('EP', 'EL')
left join [$(DataHubDatabaseName)].[dbo].[PV_SA_M_ENCRS_EPRGN_AS_OF_DATE](@date_analyse) e 
       on e.DWHERECOM = c.NUMERO_COMPTE 
left join [$(DataHubDatabaseName)].[dbo].[PV_SA_M_ENCRS_ECHL_AS_OF_DATE](@date_analyse) e2
       on e2.DWHENCCOM = c.NUMERO_COMPTE
left join [$(DataHubDatabaseName)].[dbo].[REF_HISTORIQUE_MENSUEL_TAUX] e3
       on e3.HISTORIQUE_MENSUEL_TAUX_CODE_TAUX = e2.DWHENCTCD and e3.HISTORIQUE_MENSUEL_TAUX_DATE_ANALYSE = @date_analyse --23/05/2019 calcul taux à partir du code taux ICR


------------------------------///////////////// Step 2
--insertion dans la table cible

insert into BF_EPARGNE
select 
  NUMERO_COMPTE
, NUMERO_PLAN
, NUMERO_SEQUENCE
, DATE_DEBUT
, DATE_DEBUT_SITUATION
, DATE_FIN
, DATE_RECEPTION
, DATE_ECHEANCE
, TYPE_PRODUIT_ERE
, SOLDE_FIN_PERIODE
, SOLDE_MOYEN_DEBITEUR
, SOLDE_MOYEN_CREDITEUR
, NOMBRE_JOURS_DEBITEURS
, NOMBRE_JOURS_CREDITEURS
, DEVISE
, INTERETS_TRESORERIE_DEBITEURS
, INTERETS_TRESORERIE_CREDITEURS
, MARGE_MONTANT_DEBITEUR
, MARGE_MONTANT_CREDITEUR
, MARGE_TAUX_DEBITEUR
, MARGE_TAUX_CREDITEUR
, INTERETS_TRESORERIE_DEBITEURS_CVL
, INTERETS_TRESORERIE_CREDITEURS_CVL
, INTERETS_DEBITEURS
, INTERETS_CREDITEURS
, INTERETS_DEBITEURS_CVL
, INTERETS_CREDITEURS_CVL
, NUMERO_LIGNE_EMPLOIS
, NUMERO_LIGNE_RESSOURCES
, TAUX_ANALYSE_DEBITEUR
, TAUX_ANALYSE_CREDITEUR
, CODE_TAUX_ICR
, MARGE_TAUX_ICR
, TAUX_INTERET_CONTRACTUEL_CTP
, DATE_EXPLOITATION
, DATE_ACTION
from #BF_EPARGNE


END
