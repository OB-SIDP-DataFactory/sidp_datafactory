﻿CREATE PROCEDURE [dbo].[Alim_BF_Credits] 
(
   @date_analyse DATE
)
AS
BEGIN

--declare @date_analyse DATE
declare @date_action DATE
declare @anneeCourante INT 
declare @anneeCouranteOuPrecedente INT
declare @moisCourant INT
declare @moisAnalyse INT 
declare @moisPrecedent INT 
declare @dernierJourMoisPrecedent  DATE

--set @date_analyse = (select MAX(DWHCPTDTX) from [$(DataHubDatabaseName)].[dbo].[PV_SA_M_COMPTE])
set @date_action = DATEADD (MONTH, DATEDIFF (MONTH,0,@date_analyse)+1,0)
set @moisCourant = MONTH (@date_action)
set @moisAnalyse = MONTH (@date_analyse)
set @moisPrecedent = @moisCourant - 1
set @anneeCourante = YEAR (@date_action)
set @anneeCouranteOuPrecedente = @anneeCourante
set @dernierJourMoisPrecedent = EOMONTH (@date_action, -1)

if (@moisCourant = 1)
  (select @anneeCouranteOuPrecedente = @anneeCourante - 1)

SET NOCOUNT ON;

-------------------------------------///////////////// Step 1
--extraction des informations des credits gérés dans SAB (immobilier + équipement + trésorerie + autres)
--select * from #res1bis
select [NUMERO_PRET], [NUMERO_DOSSIER], [NUMERO_COMPTE_MIROIR], [RUBRIQUE_COMPTABLE], [CODE_PRODUIT_CRM], [CODE_PRODUIT_SAB], [CODE_OPERATION_SAB], [CODE_NATURE_CREDIT_SAB], [CODE_PRODUIT_DF], [LIBELLE_PRODUIT_DF], [NUMERO_CLIENT], [DATE_OUVERTURE], [DATE_CLOTURE], [DUREE_CREDIT], [MONTANT_NOMINAL_CVL], [CRD_CVL], [ENCOURS_MOYEN_CVL], [ENCOURS_MOYEN], [MONTANT_INTERETS_MOIS_CVL], [MONTANT_INTERETS_MOIS], [MONTANT_INTERETS_COURUS_CVL], [MONTANT_INTERETS_COURUS], [MONTANT_INTERETS_TRESORERIE_CVL], [MONTANT_MARGE_CLIENT], [MONTANT_MARGE_COMMERCIALE_CVL], [TAUX_FIXE], [TAUX_ANALYSE] into #res1 
from ( 
select 
 ltrim(rtrim(cast(o.DWHOPENDO as varchar(20)))) as NUMERO_PRET					--modification 2019-08-28
,substring(cast(o.DWHOPENDO as varchar(20)), 1, 7) as NUMERO_DOSSIER			--modification 2019-08-28
,cast(null as varchar(20)) as NUMERO_COMPTE_MIROIR
,cast(null as varchar(10)) as RUBRIQUE_COMPTABLE 
,cast(null as varchar(20)) as CODE_PRODUIT_CRM
,cast(null as varchar(20)) as CODE_PRODUIT_SAB 
,cast(o.DWHOPEOPR as varchar(20)) as CODE_OPERATION_SAB
,cast(o.DWHOPENAT as varchar(10)) as CODE_NATURE_CREDIT_SAB
,p.code_produit_SIDP as CODE_PRODUIT_DF
,p.libelle_code_produit_SIDP as LIBELLE_PRODUIT_DF
,ltrim(rtrim(cast(o.DWHOPECON as varchar(10)))) as NUMERO_CLIENT
,o.DWHOPECRE as DATE_OUVERTURE
,o.DWHOPEFIN as DATE_CLOTURE
,o.DWHOPEDUR6 as DUREE_CREDIT
,o.DWHOPECMOI as MONTANT_NOMINAL_CVL
,o.DWHOPEVAL as CRD_CVL
,o.DWHOPECMM as ENCOURS_MOYEN_CVL
,o.DWHOPEEMM as ENCOURS_MOYEN
,sum(l.DWHMM3INC) as MONTANT_INTERETS_MOIS_CVL
,sum(l.DWHMM3INT) as MONTANT_INTERETS_MOIS										--modification 2019-09-09
,sum(l.DWHMM3COC) as MONTANT_INTERETS_COURUS_CVL
,sum(l.DWHMM3COU) as MONTANT_INTERETS_COURUS									--modification 2019-09-09
,sum(l.DWHMM3MIC) as MONTANT_INTERETS_TRESORERIE_CVL
,sum(l.DWHMM3MAR) as MONTANT_MARGE_CLIENT										--modification 2019-08-28
,sum(l.DWHMM3MCC) as MONTANT_MARGE_COMMERCIALE_CVL  							--modification 2019-08-28
,sum(l.DWHMM3TAU)/max(DWHMM3SEQ) as TAUX_FIXE   								--modification 2019-08-28
,sum(l.DWHMM3TXA)/max(DWHMM3SEQ) as TAUX_ANALYSE  								--modification 2019-08-28

from [$(DataHubDatabaseName)].[dbo].[PV_SA_M_OPCREDIT_AS_OF_DATE](@date_analyse) o
left join [$(DataHubDatabaseName)].[dbo].[REF_PRODUIT] p 
       on o.DWHOPENAT = p.code_nature_credit_SAB  
left join [$(DataHubDatabaseName)].[dbo].[PV_SA_M_INT_MARGE_AS_OF_DATE](@date_analyse) l 
       on o.DWHOPENDO = l.DWHMM3NUM
where p.code_regroupement_SIDP = 'CR' and o.DWHOPENAT not in ('314','312')		-- suppression des codes nature de crédits FRANFINANCE
group by o.DWHOPECON  
        ,o.DWHOPENDO
        ,p.code_produit_SIDP 
 	    ,p.libelle_code_produit_SIDP 
        ,o.DWHOPECRE  
        ,o.DWHOPEFIN  
        ,o.DWHOPEVAL  
        ,o.DWHOPEDUR6  
        ,o.DWHOPECMOI 
        ,o.DWHOPECMM 
		,o.DWHOPEEMM
        ,o.DWHOPENAT  
        ,o.DWHOPEOPR   
) r

--------------------------------------///////////////// Step 1-Bis
--Enrichissement de la #res1: calcul du montant RAT    

select 
 NUMERO_PRET
,NUMERO_DOSSIER
,NUMERO_COMPTE_MIROIR
,RUBRIQUE_COMPTABLE 
,CODE_PRODUIT_CRM
,CODE_PRODUIT_SAB 
,CODE_OPERATION_SAB
,CODE_NATURE_CREDIT_SAB
,CODE_PRODUIT_DF
,LIBELLE_PRODUIT_DF
,NUMERO_CLIENT
,DATE_OUVERTURE
,DATE_CLOTURE
,DUREE_CREDIT
,MONTANT_NOMINAL_CVL
,CRD_CVL
,ENCOURS_MOYEN_CVL
,ENCOURS_MOYEN
,MONTANT_RAT
,MONTANT_INTERETS_MOIS_CVL
,MONTANT_INTERETS_MOIS
,MONTANT_INTERETS_COURUS_CVL
,MONTANT_INTERETS_COURUS
,MONTANT_INTERETS_TRESORERIE_CVL
,MONTANT_MARGE_CLIENT
,MONTANT_MARGE_COMMERCIALE_CVL  						 											
,TAUX_FIXE  									
,TAUX_ANALYSE 
into #res1bis    
from ( select [r].[NUMERO_PRET], [r].[NUMERO_DOSSIER], [r].[NUMERO_COMPTE_MIROIR], [r].[RUBRIQUE_COMPTABLE], [r].[CODE_PRODUIT_CRM], [r].[CODE_PRODUIT_SAB], [r].[CODE_OPERATION_SAB], [r].[CODE_NATURE_CREDIT_SAB], [r].[CODE_PRODUIT_DF], [r].[LIBELLE_PRODUIT_DF], [r].[NUMERO_CLIENT], [r].[DATE_OUVERTURE], [r].[DATE_CLOTURE], [r].[DUREE_CREDIT], [r].[MONTANT_NOMINAL_CVL], [r].[CRD_CVL], [r].[ENCOURS_MOYEN_CVL], [r].[ENCOURS_MOYEN], [r].[MONTANT_INTERETS_MOIS_CVL], [r].[MONTANT_INTERETS_MOIS], [r].[MONTANT_INTERETS_COURUS_CVL], [r].[MONTANT_INTERETS_COURUS], [r].[MONTANT_INTERETS_TRESORERIE_CVL], [r].[MONTANT_MARGE_CLIENT], [r].[MONTANT_MARGE_COMMERCIALE_CVL], [r].[TAUX_FIXE], [r].[TAUX_ANALYSE] 
             ,c.CRD_CVL as MONTANT_RAT															
       from #res1 r
-- jointure avec les crédits SAB avec CRD et durée restante à 0 et périodicité M/T/S/A
       left join (select [Id], [DWHOPEDTX], [DWHOPEETA], [DWHOPEAGE], [DWHOPESER], [DWHOPESSE], [DWHOPEOPR], [DWHOPENAT], [DWHOPENDO], [DWHOPESEQ], [DWHOPECON], [DWHOPEPAS], [DWHOPEBDF], [DWHOPECRE], [DWHOPEENG], [DWHOPEDIS], [DWHOPEFIN], [DWHOPEDEV], [DWHOPEMON], [DWHOPEVAL], [DWHOPECOE], [DWHOPETYA], [DWHOPECDA], [DWHOPENOA], [DWHOPEDEA], [DWHOPEAUT], [DWHOPERUB], [DWHOPEDUR6], [DWHOPERES6], [DWHOPETYP], [DWHOPEFIX], [DWHOPENOUV], [DWHOPEMOIN], [DWHOPECMOI], [DWHOPEMIN], [DWHOPEVIN], [DWHOPEMFI], [DWHOPECFI], [DWHOPEEMM], [DWHOPECMM], [DWHOPETYC], [DWHOPEOFI], [DWHOPEREC], [DWHOPEPOU], [DWHOPEPOL], [DWHOPEPTR], [DWHOPEPRI], [DWHOPEMTT], [DWHOPEMTR], [DWHOPECAR], [DWHOPECFIM], [DWHOPETYT], [DWHOPETAU], [DWHOPEBAS], [DWHOPEREF], [DWHOPEMAR], [DWHOPETEG], [DWHOPETESE], [DWHOPETESL], [DWHOPEPER], [DWHOPETAM], [DWHOPEDIA], [DWHOPEDIT], [DWHOPECPD], [DWHOPEDPE], [DWHOPEDRE], [DWHOPENDF], [DWHOPEMAD], [DWHOPEMTG], [DWHOPEDMD], [DWHOPEMTD], [DWHOPECAT], [DWHOPEMTI], [DWHOPECMI], [DWHOPEPRE], [DWHOPENPRE], [DWHOPETNM], [DWHOPEMNP], [DWHOPETMM], [DWHOPEDRT], [DWHOPEPRT], [DWHOPENJR], [DWHOPENJB], [DWHOPEPOS], [DWHOPENAN], [DWHOPEDEI], [DWHOPEDPEI], [DWHOPENBI], [DWHOPENBE], [DWHOPETOI], [DWHOPETOB], [DWHOPECAI], [DWHOPEINI], [DWHOPETAX], [DWHOPEASI], [DWHOPECOI], [DWHOPEFRA], [DWHOPEIND], [DWHOPEDDT], [DWHOPEMDT], [DWHOPETIR], [DWHOPEMNO], [DWHOPEMSD], [DWHOPEMCO], [DWHOPELSD], [DWHOPEDE2], [DWHOPEMO2], [DWHOPEVA2], [DWHOPESEN], [DWHOPETPA], [DWHOPENCR], [DWHOPENCO], [DWHOPENRE], [DWHOPENEF], [DWHOPEICO], [DWHOPENBJ], [DWHOPESUR], [DWHOPEAJU], [DWHOPECAP], [DWHOPERES], [DWHOPEECH], [DWHOPETXC], [DWHOPEUSP], [DWHOPEMTE], [DWHOPERAC], [DWHOPEPTF], [DWHOPETPL], [DWHOPERCS], [DWHOPECRI], [DWHOPERIS], [DWHOPEFRE], [DWHOPEDOM], [DWHOPEAPP], [DWHOPEDSY], [Validity_StartDate], [Validity_EndDate], [Startdt], [Enddt]
                  from [$(DataHubDatabaseName)].[dbo].[PV_SA_M_OPCREDIT_AS_OF_DATE](@date_analyse)
                  where DWHOPEVAL = 0
                    and DWHOPERES6 = 0
                    and DWHOPEPER in ('M','T','S','A') ) o
              on o.DWHOPENDO = r.NUMERO_PRET
-- jointure avec les CREDIT_SAB avec durée restante pour le mois précédent différente de la périodicité --> détection des RAT
       left join dbo.[BF_CREDIT_IMMO] cei
              on cei.NUMERO_PRET = r.NUMERO_PRET											
             and year(cei.DATE_ACTION) = @anneeCouranteOuPrecedente
             and month(cei.DATE_ACTION) = @moisAnalyse 
             and cei.DUREE_RESTANTE > case when o.DWHOPEPER = 'M' then 30
                                           when o.DWHOPEPER = 'T' then 90
                                           when o.DWHOPEPER = 'S' then 180
                                           when o.DWHOPEPER = 'A' then 360 end
-- jointure avec la table BF_CREDITS pour affectation du CRD du mois précédent dans le RAT
       left join dbo.BF_CREDITS c
              on c.NUMERO_PRET = cei.NUMERO_PRET										
             and c.DATE_ACTION = cei.DATE_ACTION
) r

------------------------------------------///////////////// Step 2
--extraction des informations des credits renouvelables (compleo) (mofification 2019-08-26)

select [NUMERO_PRET], [NUMERO_DOSSIER], [NUMERO_COMPTE_MIROIR], [RUBRIQUE_COMPTABLE], [CODE_PRODUIT_CRM], [CODE_PRODUIT_SAB], [CODE_OPERATION_SAB], [CODE_NATURE_CREDIT_SAB], [CODE_PRODUIT_DF], [LIBELLE_PRODUIT_DF], [NUMERO_CLIENT], [DATE_OUVERTURE], [DATE_CLOTURE], [DUREE_CREDIT], [MONTANT_NOMINAL_CVL], [CRD_CVL], [ENCOURS_MOYEN_CVL], [ENCOURS_MOYEN], [MONTANT_RAT], [MONTANT_INTERETS_MOIS_CVL], [MONTANT_INTERETS_MOIS], [MONTANT_INTERETS_COURUS_CVL], [MONTANT_INTERETS_COURUS], [MONTANT_INTERETS_TRESORERIE_CVL], [MONTANT_MARGE_CLIENT], [MONTANT_MARGE_COMMERCIALE_CVL], [TAUX_FIXE], [TAUX_ANALYSE] into #res2  
from (
select 
 ltrim(rtrim(cast(b.COMREFREF as varchar(20)))) as NUMERO_PRET									--modification 2019-08-29
,cast(null as varchar(20)) as NUMERO_DOSSIER
,ltrim(rtrim(cast(c.DWHCPTCOM as varchar(20)))) as NUMERO_COMPTE_MIROIR
,ltrim(rtrim(cast(c.DWHCPTRUB as varchar(10)))) as RUBRIQUE_COMPTABLE
,cast(coalesce(w.CPTBISGES, opp.CommercialOfferCode__c) as varchar(20)) as CODE_PRODUIT_CRM		--modification 2019-06-19 : code produit CRM nouvelle offre
,cast(y.PLAN_DE_COMPTES_CODE_PRODUIT as varchar(20)) as CODE_PRODUIT_SAB
,cc.DWHOPEOPR as CODE_OPERATION_SAB
,cc.DWHOPENAT as CODE_NATURE_CREDIT_SAB
,x.code_produit_SIDP as CODE_PRODUIT_DF
,x.libelle_code_produit_SIDP as LIBELLE_PRODUIT_DF
,ltrim(rtrim(c.DWHCPTPPAL)) as NUMERO_CLIENT
,c.DWHCPTDAO as DATE_OUVERTURE
,c.DWHCPTDAC as DATE_CLOTURE
,datediff(day, c.DWHCPTDAO, @dernierJourMoisPrecedent) as DUREE_CREDIT
,NULL as MONTANT_NOMINAL_CVL
,c.DWHCPTCPT as CRD_CVL
,sum(ISNULL(c.DWHCPTSMD,0)) + SUM(ISNULL(c.DWHCPTSMC,0)) as ENCOURS_MOYEN_CVL					--modification 2019-08-26 : encours moyen déterminé à partir de la table PV_SA_M_COMPTE et non plus à partir de la table PV_SA_M_OPCREDIT
,sum(ISNULL(c.DWHCPTSMVD,0)) + SUM(ISNULL(c.DWHCPTSMVC,0)) as ENCOURS_MOYEN
,NULL as MONTANT_RAT  
,sum(j.DWHMM3INC) as MONTANT_INTERETS_MOIS_CVL
,sum(j.DWHMM3INT) as MONTANT_INTERETS_MOIS														--modification 2019-09-09
,sum(j.DWHMM3COC) as MONTANT_INTERETS_COURUS_CVL
,sum(j.DWHMM3COU) as MONTANT_INTERETS_COURUS													--modification 2019-09-09
,sum(j.DWHMM3MIC) as MONTANT_INTERETS_TRESORERIE_CVL
,sum(j.DWHMM3MAR) as MONTANT_MARGE_CLIENT														--modification 2019-08-28
,sum(j.DWHMM3MCC) as MONTANT_MARGE_COMMERCIALE_CVL  											--modification 2019-08-28
,sum(j.DWHMM3TAU)/max(DWHMM3SEQ) as TAUX_FIXE   												--modification 2019-08-28
,sum(j.DWHMM3TXA)/max(DWHMM3SEQ) as TAUX_ANALYSE  												--modification 2019-08-28																		--modification 2019-08-28
--données table CPT: caractéristiques du compte miroir
from [$(DataHubDatabaseName)].[dbo].[PV_SA_M_COMPTE_AS_OF_DATE](@date_analyse) c
--jointure avec table CPTBIS :  code produit CRM associé au compte miroir
left join [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_CPTBIS] w	WITH(NOLOCK)
       on c.DWHCPTCOM = w.CPTBISCOM
--jointure avec table PLAN_DE_COMPTE
left join [$(DataHubDatabaseName)].[dbo].[REF_PLAN_DE_COMPTES] y	WITH(NOLOCK)
       on c.DWHCPTRUB = y.PLAN_DE_COMPTES_COMPTE_OBLIGATOIRE
--jointure avec les tables PV_FE_EQUIPMENT + PV_FE_ENROLMENTFOLDER + PV_SF_OPPORTUNITY_HISTORY : code produit CRM nouvelle offre
left join  [$(DataHubDatabaseName)].[dbo].PV_FE_EQUIPMENT equi	WITH(NOLOCK)
       on c.DWHCPTCOM = equi.EQUIPMENT_NUMBER  
left join  [$(DataHubDatabaseName)].[dbo].PV_FE_ENROLMENTFOLDER enr	WITH(NOLOCK)
       on equi.ENROLMENT_FOLDER_ID = enr.ID_FE_ENROLMENT_FOLDER  
left join  [$(DataHubDatabaseName)].[dbo].[PV_SF_OPPORTUNITY_HISTORY_AS_OF_DATE](@date_analyse) opp
       on enr.SALESFORCE_OPPORTUNITY_ID = opp.Id_SF
--jointure referentiel produits
left join [$(DataHubDatabaseName)].[dbo].[REF_PRODUIT] x 	WITH(NOLOCK)
       on x.code_rubrique_comptable_SAB = c.DWHCPTRUB 
      and x.code_produit_CRM = isnull(coalesce(w.CPTBISGES, opp.CommercialOfferCode__c),'')
--jointure avec COMREFCOM : association reference FRANFINANCE et reference compte miroir 
left join [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_COM] b	WITH(NOLOCK)
       on  c.DWHCPTCOM = b.COMREFCOM
--jointure avec table mensuelle
left join  [$(DataHubDatabaseName)].[dbo].[PV_FI_M_COMPL_AS_OF_DATE](@date_analyse) e
       on b.COMREFREF = e.NUM_PRE
-- jointure avec données flux quotidien fin de mois
left join [$(DataHubDatabaseName)].[dbo].[PV_FI_M_COMPL_Q_AS_OF_DATE](@date_analyse) a
       on b.COMREFREF = a.NUM_PRE
--jointure avec les CTX
left join  [$(DataHubDatabaseName)].[dbo].[PV_FI_M_CTX_AS_OF_DATE] (@date_analyse) d
       on b.COMREFREF = d.NUM_PRE --d.TYP_CON = 'REV' => CTX des crédits révisables
--jointure table OPECREDIT
left join [$(DataHubDatabaseName)].[dbo].[PV_SA_M_OPCREDIT_AS_OF_DATE](@date_analyse) cc 
	   on b.COMREFREF = cast(substring(cast(cc.DWHOPENDO as varchar(20)),1,2) + '13' + substring(cast(cc.DWHOPENDO as varchar(20)), 3, len(cc.DWHOPENDO)) as varchar(50))
	  and cc.DWHOPENAT = '314'
--jointure table interets
left join [$(DataHubDatabaseName)].[dbo].[PV_SA_M_LKCPTINT_AS_OF_DATE](@date_analyse) i 
       on c.DWHCPTCOM = i.DWHMM1COM 
left join [$(DataHubDatabaseName)].[dbo].[PV_SA_M_INT_MARGE_AS_OF_DATE](@date_analyse) j 
       on j.DWHMM3DTX = i.DWHMM1DTX
      and j.DWHMM3ETA = i.DWHMM1ETA
      and j.DWHMM3AGE = i.DWHMM1AGE
      and j.DWHMM3SER = i.DWHMM1SER
      and j.DWHMM3SES = i.DWHMM1SES
      and j.DWHMM3OPE = i.DWHMM1OPE
      and j.DWHMM3NAT = i.DWHMM1NAT
      and j.DWHMM3NUM = i.DWHMM1NUM
--filtre code produit des comptes-miroirs
where cast(y.PLAN_DE_COMPTES_CODE_PRODUIT as varchar(20)) in ('CPR','CPS') and b.COMREFREF is not NULL
group by c.DWHCPTPPAL
        ,c.DWHCPTCOM
        ,c.DWHCPTRUB  
        ,x.code_produit_SIDP 
		,x.libelle_code_produit_SIDP
        ,c.DWHCPTDAO
        ,c.DWHCPTDAC
        ,c.DWHCPTCPT
        ,a.NUM_PRE
		,e.NUM_PRE
        ,y.PLAN_DE_COMPTES_CODE_PRODUIT
        ,w.CPTBISGES
		,opp.CommercialOfferCode__c
		,b.COMREFREF
		,cc.DWHOPEOPR
		,cc.DWHOPENAT
) r      

------------------------------///////////////// Step 3 (mofification 2019-05-29 => code produit CRM nouvelle offre + modification 2019-08-26 => modification ordre d'alimentation de la table #res3 : point de départ PV_SA_M_COMPTES et non plus PV_FI_M_DES_Q)
--extraction des informations des credits amortissables + suppression des doublons dans la table PV_FI_M_DES
select [NUMERO_PRET], [NUMERO_DOSSIER], [NUMERO_COMPTE_MIROIR], [RUBRIQUE_COMPTABLE], [CODE_PRODUIT_CRM], [CODE_PRODUIT_SAB], [CODE_OPERATION_SAB], [CODE_NATURE_CREDIT_SAB], [CODE_PRODUIT_DF], [LIBELLE_PRODUIT_DF], [NUMERO_CLIENT], [COD_SIT_DOS], [DATE_OUVERTURE], [DATE_CLOTURE], [DUREE_CREDIT], [MONTANT_NOMINAL_CVL], [CRD_CVL], [ENCOURS_MOYEN_CVL], [ENCOURS_MOYEN], [MONTANT_INTERETS_MOIS_CVL], [MONTANT_INTERETS_MOIS], [MONTANT_INTERETS_COURUS_CVL], [MONTANT_INTERETS_COURUS], [MONTANT_INTERETS_TRESORERIE_CVL], [MONTANT_MARGE_CLIENT], [MONTANT_MARGE_COMMERCIALE_CVL], [TAUX_FIXE], [TAUX_ANALYSE] into #res3
from (
select
 ltrim(rtrim(cast(b.COMREFREF as varchar(20)))) as NUMERO_PRET						--modification 2019-08-29
,cast(null as varchar(20)) as NUMERO_DOSSIER										--modification 2019-08-29
,ltrim(rtrim(cast(c.DWHCPTCOM as varchar(20)))) as NUMERO_COMPTE_MIROIR
,ltrim(rtrim(cast(c.DWHCPTRUB as varchar(10)))) as RUBRIQUE_COMPTABLE
,cast(coalesce(w.CPTBISGES, opp.CommercialOfferCode__c) as varchar(20)) as CODE_PRODUIT_CRM
,cast(y.PLAN_DE_COMPTES_CODE_PRODUIT as varchar(20)) as CODE_PRODUIT_SAB
,cc.DWHOPEOPR as CODE_OPERATION_SAB
,cc.DWHOPENAT as CODE_NATURE_CREDIT_SAB
,x.code_produit_SIDP as CODE_PRODUIT_DF
,x.libelle_code_produit_SIDP as LIBELLE_PRODUIT_DF
,ltrim(rtrim(c.DWHCPTPPAL)) as NUMERO_CLIENT
,e.COD_SIT_DOS																		--ajout 26/08/2019 pour détection des RAT
,c.DWHCPTDAO as DATE_OUVERTURE														--modification 19/06/2019 => a.DTE_FIN_PRE => c.DWHCPTDAO
,c.DWHCPTDAC as DATE_CLOTURE														--modification 19/06/2019 => a.DTE_FIN_PRE => c.DWHCPTDAC
,a.DUR_LEG as DUREE_CREDIT
,a.MTT_NOM as MONTANT_NOMINAL_CVL													--modification 19/09/2019
,c.DWHCPTCPT as CRD_CVL
,sum(isnull(c.DWHCPTSMD,0)) + SUM(isnull(c.DWHCPTSMC,0)) as ENCOURS_MOYEN_CVL		--modification 2019-08-26 : encours moyen déterminé à partir de la table PV_SA_M_COMPTE et non plus à partir de la table PV_SA_M_OPCREDIT
,sum(isnull(c.DWHCPTSMVD,0)) + SUM(isnull(c.DWHCPTSMVC,0)) as ENCOURS_MOYEN
,sum(j.DWHMM3INC) as MONTANT_INTERETS_MOIS_CVL
,sum(j.DWHMM3INT) as MONTANT_INTERETS_MOIS											--modification 2019-09-09
,sum(j.DWHMM3COC) as MONTANT_INTERETS_COURUS_CVL
,sum(j.DWHMM3COU) as MONTANT_INTERETS_COURUS										--modification 2019-09-09
,sum(j.DWHMM3MIC) as MONTANT_INTERETS_TRESORERIE_CVL
,sum(j.DWHMM3MAR) as MONTANT_MARGE_CLIENT											--modification 2019-08-28
,sum(j.DWHMM3MCC) as MONTANT_MARGE_COMMERCIALE_CVL  								--modification 2019-08-28
,sum(j.DWHMM3TAU)/max(DWHMM3SEQ) as TAUX_FIXE   									--modification 2019-08-28
,sum(j.DWHMM3TXA)/max(DWHMM3SEQ) as TAUX_ANALYSE  									--modification 2019-08-28
--données table CPT: caractéristiques du compte miroir
from [$(DataHubDatabaseName)].[dbo].[PV_SA_M_COMPTE_AS_OF_DATE](@date_analyse) c
--jointure avec table CPTBIS : code produit CRM associé au compte miroir
left join [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_CPTBIS] w WITH(NOLOCK)
       on c.DWHCPTCOM = w.CPTBISCOM
--jointure avec table PLAN_DE_COMPTE : code produit SAB associe a la rubrique comptable
left join [$(DataHubDatabaseName)].[dbo].[REF_PLAN_DE_COMPTES] y WITH(NOLOCK)
       on c.DWHCPTRUB = y.PLAN_DE_COMPTES_COMPTE_OBLIGATOIRE
--jointure avec les tables PV_FE_EQUIPMENT + PV_FE_ENROLMENTFOLDER + PV_SF_OPPORTUNITY_HISTORY : code produit CRM nouvelle offre
--warning : après validation de la version V2 des tables SF et FE => à prendre en compte 
left join  [$(DataHubDatabaseName)].[dbo].PV_FE_EQUIPMENT equi WITH(NOLOCK)
       on c.DWHCPTCOM = equi.EQUIPMENT_NUMBER  
left join  [$(DataHubDatabaseName)].[dbo].PV_FE_ENROLMENTFOLDER enr WITH(NOLOCK)
       on equi.ENROLMENT_FOLDER_ID = enr.ID_FE_ENROLMENT_FOLDER  
left join  [$(DataHubDatabaseName)].[dbo].[PV_SF_OPPORTUNITY_HISTORY_AS_OF_DATE](@date_analyse) opp
       on enr.SALESFORCE_OPPORTUNITY_ID = opp.Id_SF
--jointure referentiel produits
left join [$(DataHubDatabaseName)].[dbo].[REF_PRODUIT] x WITH(NOLOCK)
       on x.code_rubrique_comptable_SAB = c.DWHCPTRUB
      and x.code_produit_CRM = isnull(coalesce(w.CPTBISGES, opp.CommercialOfferCode__c),'')
--jointure avec COMREFCOM: association reference FRANFINANCE et reference compte miroir
left join [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_COM] b WITH(NOLOCK)
       on c.DWHCPTCOM = b.COMREFCOM
--jointure avec table mensuelle: donnees flux mensuel Franfinance + suppression des doublons 
left join (select [RowID], [Id], [COD_ENR], [NUMERO_ENREG], [COD_SEQ], [COD_BAN_GES], [COD_GUI_GES], [NUM_PRE], [COD_BAN_RIB], [COD_GUI_RIB], [NUM_CPT_RIB], [DTE_OUV_PRE], [DTE_FER_PRE], [DUR_INI_PRE], [TX_ACT_EFF_GAR], [DEV_PRE], [NB_DEC_DEV_PRE], [MTT_NOM], [ENC_CAP], [ENC_MOY], [DTE_PRO_ECH], [MTT_ECH_TRA], [COD_SIT_DOS], [DTE_INC_PRE_IMP], [MTT_IMP_FIN_MOI], [MTT_FRA_DOS], [MTT_COM], [MTT_REM_SUP_MOI], [ASS], [DATTTP], [IDE_GRC], [NB_IMP], [MTT_GLO_IR_IMP], [MTT_GLO_IL_IMP], [PHA_PRO_REC], [FILLER], [CIV_CLIENT], [NOM_CLIENT], [PRENOM_CLIENT], [ADRESSE_EMPR], [COMPL_ADRESSE], [LIEU_EMP], [COD_POSTAL], [VILLE], [CO_EMPRUNTEUR], [PSA], [SIT_FAMILIALE], [SIT_LOGEMENT], [NB_ENFANTS], [CSP_EMPRUNTEUR], [DATE_NAISSANCE], [REVENU_MENAGE], [CAT_PRE], [COD_OBJ], [TAX_CLI_INITIAL], [TAX_VEN], [TAEG_CLIENT], [MTT_NOM_COURANT], [INT_INI], [ASS_INI], [DTE_PRE_ECH], [DTE_FIN_INI], [DTE_FIN_PRE], [CPT_DER_ECH], [INT_DER_ECH], [ASS_DER_ECH], [ACC_DER_ECH], [CPT_PRO_ECH], [INT_PRO_ECH], [ASS_PRO_ECH], [ACC_PRO_ECH], [COD_SIT], [NAT_ASS_EMP], [NAT_ASS_CO_EMP], [RES_DIS], [SOUS_RES_DIS], [COD_PDT], [Validity_StartDate], [Validity_EndDate], [Startdt], [Enddt] from (select ROW_NUMBER() OVER(PARTITION BY m.NUM_PRE ORDER BY m.DTE_OUV_PRE DESC)  AS RowID, [m].[Id], [m].[COD_ENR], [m].[NUMERO_ENREG], [m].[COD_SEQ], [m].[COD_BAN_GES], [m].[COD_GUI_GES], [m].[NUM_PRE], [m].[COD_BAN_RIB], [m].[COD_GUI_RIB], [m].[NUM_CPT_RIB], [m].[DTE_OUV_PRE], [m].[DTE_FER_PRE], [m].[DUR_INI_PRE], [m].[TX_ACT_EFF_GAR], [m].[DEV_PRE], [m].[NB_DEC_DEV_PRE], [m].[MTT_NOM], [m].[ENC_CAP], [m].[ENC_MOY], [m].[DTE_PRO_ECH], [m].[MTT_ECH_TRA], [m].[COD_SIT_DOS], [m].[DTE_INC_PRE_IMP], [m].[MTT_IMP_FIN_MOI], [m].[MTT_FRA_DOS], [m].[MTT_COM], [m].[MTT_REM_SUP_MOI], [m].[ASS], [m].[DATTTP], [m].[IDE_GRC], [m].[NB_IMP], [m].[MTT_GLO_IR_IMP], [m].[MTT_GLO_IL_IMP], [m].[PHA_PRO_REC], [m].[FILLER], [m].[CIV_CLIENT], [m].[NOM_CLIENT], [m].[PRENOM_CLIENT], [m].[ADRESSE_EMPR], [m].[COMPL_ADRESSE], [m].[LIEU_EMP], [m].[COD_POSTAL], [m].[VILLE], [m].[CO_EMPRUNTEUR], [m].[PSA], [m].[SIT_FAMILIALE], [m].[SIT_LOGEMENT], [m].[NB_ENFANTS], [m].[CSP_EMPRUNTEUR], [m].[DATE_NAISSANCE], [m].[REVENU_MENAGE], [m].[CAT_PRE], [m].[COD_OBJ], [m].[TAX_CLI_INITIAL], [m].[TAX_VEN], [m].[TAEG_CLIENT], [m].[MTT_NOM_COURANT], [m].[INT_INI], [m].[ASS_INI], [m].[DTE_PRE_ECH], [m].[DTE_FIN_INI], [m].[DTE_FIN_PRE], [m].[CPT_DER_ECH], [m].[INT_DER_ECH], [m].[ASS_DER_ECH], [m].[ACC_DER_ECH], [m].[CPT_PRO_ECH], [m].[INT_PRO_ECH], [m].[ASS_PRO_ECH], [m].[ACC_PRO_ECH], [m].[COD_SIT], [m].[NAT_ASS_EMP], [m].[NAT_ASS_CO_EMP], [m].[RES_DIS], [m].[SOUS_RES_DIS], [m].[COD_PDT], [m].[Validity_StartDate], [m].[Validity_EndDate], [m].[Startdt], [m].[Enddt] 
						  from [$(DataHubDatabaseName)].[dbo].[PV_FI_M_DES_AS_OF_DATE](@date_analyse) m
						  ) t where RowId = 1) e
       on b.COMREFREF = e.NUM_PRE 
--left join #PV_FI_M_DES e
--       on e.NUM_PRE = b.COMREFREF --and a.DTE_DER_DEB=e.DTE_OUV_PRE and a.MTT_NOM=e.MTT_NOM
--jointure avec flux quotidien fin de mois : données flux quotidien FRANFINANCE
left join [$(DataHubDatabaseName)].[dbo].[PV_FI_M_DES_Q_AS_OF_DATE] (@date_analyse) a
       on a.REF_CPT = b.COMREFREF --and a.DTE_DER_DEB=e.DTE_OUV_PRE and a.MTT_NOM=e.MTT_NOM
--jointure avec les CTX: donnees dossiers en contentieux
left join [$(DataHubDatabaseName)].[dbo].[PV_FI_M_CTX_AS_OF_DATE] (@date_analyse) d
       on d.NUM_PRE = b.COMREFREF 
--jointure table OPECREDIT
left join [$(DataHubDatabaseName)].[dbo].[PV_SA_M_OPCREDIT_AS_OF_DATE](@date_analyse) cc 
       on b.COMREFREF = cast(substring(cast(cc.DWHOPENDO as varchar(20)),1,2) + '13' + substring(cast(cc.DWHOPENDO as varchar(20)), 3, len(cc.DWHOPENDO)) as varchar(50))
	  and cc.DWHOPENAT = '312'
--jointure table interets
left join [$(DataHubDatabaseName)].[dbo].[PV_SA_M_LKCPTINT_AS_OF_DATE](@date_analyse) i 
       on c.DWHCPTCOM = i.DWHMM1COM 
left join [$(DataHubDatabaseName)].[dbo].[PV_SA_M_INT_MARGE_AS_OF_DATE](@date_analyse) j 
       on j.DWHMM3DTX = i.DWHMM1DTX
      and j.DWHMM3ETA = i.DWHMM1ETA
      and j.DWHMM3AGE = i.DWHMM1AGE
      and j.DWHMM3SER = i.DWHMM1SER
      and j.DWHMM3SES = i.DWHMM1SES
      and j.DWHMM3OPE = i.DWHMM1OPE
      and j.DWHMM3NAT = i.DWHMM1NAT
      and j.DWHMM3NUM = i.DWHMM1NUM
--filtre code produit SAB
where cast(y.PLAN_DE_COMPTES_CODE_PRODUIT as varchar(20)) in ('CRE') and b.COMREFREF is not NULL
--warning : a.COD_SIT_TOKOS = '1' => dossier accordé mais pas réalisé
group by b.COMREFREF
		,cc.DWHOPEOPR
		,cc.DWHOPENAT
		,c.DWHCPTPPAL
        ,c.DWHCPTCOM
        ,c.DWHCPTRUB  
        ,x.code_produit_SIDP 
		,x.libelle_code_produit_SIDP
		,opp.CommercialOfferCode__c
		,c.DWHCPTDAO								--modification 19/06/2019 => a.DTE_REA => c.DWHCPTDAO
		,c.DWHCPTDAC								--modification 19/06/2019 => a.DTE_FIN_PRE => c.DWHCPTDAC
		,a.DUR_LEG
        ,c.DWHCPTCPT
        ,a.REF_CPT
        ,y.PLAN_DE_COMPTES_CODE_PRODUIT
        ,w.CPTBISGES
		,a.INT_RES_DU_DER_ECH
        ,a.MTT_NOM
		,e.COD_SIT_DOS
) r

-----------------------------------///////////////// Step 3-Bis
--Enrichissement de la #res3 (Amortissable) avec le montant RAT  (mofification 2019-05-29 => code produit CRM nouvelle offre + RDG calcul RAT + modification 2019-08-06 => modification RDG détermination des RAT)
select 
	  NUMERO_PRET
     ,NUMERO_DOSSIER
	 ,NUMERO_COMPTE_MIROIR
	 ,RUBRIQUE_COMPTABLE 
     ,CODE_PRODUIT_CRM
     ,CODE_PRODUIT_SAB 
	 ,CODE_OPERATION_SAB
	 ,CODE_NATURE_CREDIT_SAB
     ,CODE_PRODUIT_DF
	 ,LIBELLE_PRODUIT_DF
     ,NUMERO_CLIENT
     ,DATE_OUVERTURE
     ,DATE_CLOTURE
     ,DUREE_CREDIT
     ,MONTANT_NOMINAL_CVL
     ,CRD_CVL
	 ,ENCOURS_MOYEN_CVL
	 ,ENCOURS_MOYEN
	 ,MONTANT_RAT
     ,MONTANT_INTERETS_MOIS_CVL
	 ,MONTANT_INTERETS_MOIS
     ,MONTANT_INTERETS_COURUS_CVL
	 ,MONTANT_INTERETS_COURUS
     ,MONTANT_INTERETS_TRESORERIE_CVL
	 ,MONTANT_MARGE_CLIENT
	 ,MONTANT_MARGE_COMMERCIALE_CVL  													
	 ,TAUX_FIXE  									
	 ,TAUX_ANALYSE 
into #res3bis    
from
(
select * 
from
(
select r.*
      ,case when r.COD_SIT_DOS in (12,64) then cp.DWHCPTCPT
	   else NULL end as MONTANT_RAT 
from #res3 r
left join [$(DataHubDatabaseName)].[dbo].[PV_SA_M_COMPTE_AS_OF_DATE](EOMONTH(@date_analyse , -1 )) cp
       on r.NUMERO_COMPTE_MIROIR = cp.DWHCPTCOM
) r
) t

------------------------------///////////////// Step 4
-------------------------insertion dans la table cible 

insert into BF_CREDITS
select r.*
	 , @date_analyse as dt_exploitation
     , @date_action as date_action
from (select [NUMERO_PRET], [NUMERO_DOSSIER], [NUMERO_COMPTE_MIROIR], [RUBRIQUE_COMPTABLE], [CODE_PRODUIT_CRM], [CODE_PRODUIT_SAB], [CODE_OPERATION_SAB], [CODE_NATURE_CREDIT_SAB], [CODE_PRODUIT_DF], [LIBELLE_PRODUIT_DF], [NUMERO_CLIENT], [DATE_OUVERTURE], [DATE_CLOTURE], [DUREE_CREDIT], [MONTANT_NOMINAL_CVL], [CRD_CVL], [ENCOURS_MOYEN_CVL], [ENCOURS_MOYEN], [MONTANT_RAT], [MONTANT_INTERETS_MOIS_CVL], [MONTANT_INTERETS_MOIS], [MONTANT_INTERETS_COURUS_CVL], [MONTANT_INTERETS_COURUS], [MONTANT_INTERETS_TRESORERIE_CVL], [MONTANT_MARGE_CLIENT], [MONTANT_MARGE_COMMERCIALE_CVL], [TAUX_FIXE], [TAUX_ANALYSE] from #res1bis   
         union all
         select [NUMERO_PRET], [NUMERO_DOSSIER], [NUMERO_COMPTE_MIROIR], [RUBRIQUE_COMPTABLE], [CODE_PRODUIT_CRM], [CODE_PRODUIT_SAB], [CODE_OPERATION_SAB], [CODE_NATURE_CREDIT_SAB], [CODE_PRODUIT_DF], [LIBELLE_PRODUIT_DF], [NUMERO_CLIENT], [DATE_OUVERTURE], [DATE_CLOTURE], [DUREE_CREDIT], [MONTANT_NOMINAL_CVL], [CRD_CVL], [ENCOURS_MOYEN_CVL], [ENCOURS_MOYEN], [MONTANT_RAT], [MONTANT_INTERETS_MOIS_CVL], [MONTANT_INTERETS_MOIS], [MONTANT_INTERETS_COURUS_CVL], [MONTANT_INTERETS_COURUS], [MONTANT_INTERETS_TRESORERIE_CVL], [MONTANT_MARGE_CLIENT], [MONTANT_MARGE_COMMERCIALE_CVL], [TAUX_FIXE], [TAUX_ANALYSE] from #res2
         union all
         select [NUMERO_PRET], [NUMERO_DOSSIER], [NUMERO_COMPTE_MIROIR], [RUBRIQUE_COMPTABLE], [CODE_PRODUIT_CRM], [CODE_PRODUIT_SAB], [CODE_OPERATION_SAB], [CODE_NATURE_CREDIT_SAB], [CODE_PRODUIT_DF], [LIBELLE_PRODUIT_DF], [NUMERO_CLIENT], [DATE_OUVERTURE], [DATE_CLOTURE], [DUREE_CREDIT], [MONTANT_NOMINAL_CVL], [CRD_CVL], [ENCOURS_MOYEN_CVL], [ENCOURS_MOYEN], [MONTANT_RAT], [MONTANT_INTERETS_MOIS_CVL], [MONTANT_INTERETS_MOIS], [MONTANT_INTERETS_COURUS_CVL], [MONTANT_INTERETS_COURUS], [MONTANT_INTERETS_TRESORERIE_CVL], [MONTANT_MARGE_CLIENT], [MONTANT_MARGE_COMMERCIALE_CVL], [TAUX_FIXE], [TAUX_ANALYSE] from #res3bis) r


END
