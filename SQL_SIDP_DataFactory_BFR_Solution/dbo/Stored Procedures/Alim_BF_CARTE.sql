﻿CREATE PROCEDURE [dbo].[Alim_BF_CARTE] 
(
   @date_analyse DATE
)
AS
BEGIN
    SET NOCOUNT ON;

    declare @date_action DATE
    set @date_action = DATEADD (MONTH, DATEDIFF (MONTH,0,@date_analyse)+1,0);

-- insertion dans la table cible

    insert into dbo.BF_CARTE
	 SELECT 
	 DWHCARCOM AS NUMERO_COMPTE
    ,DWHCARCAR AS COD_CARTE
    ,DWHCARETB AS COD_ETABLISSEMENT
    ,DWHCARNUS AS NUMERO_SEQUENCE
    ,DWHCARNPO AS NUMERO_CLIENT
    ,DWHCARTPO AS TYP_PORTEUR
    ,DWHCARLIB AS LIBL_COD_CARTE
    ,DWHCARNAT AS NATURE_CARTE
    ,DWHCARTYP AS TYP_CARTE
    ,DWHCAROPT AS OPTN_DEBIT
    ,DWHCARUTI AS UTILISATION
    ,DWHCARVAL AS DAT_VALIDITE
    ,DWHCARDUR AS DUREE_VALIDITE
    ,DWHCARAGE AS AGENCE_GESTIONNAIRE
    ,DWHCARHSE AS MISE_HORS_SERVICE
    ,DWHCARENR AS DAT_COMMANDE_CARTE
    ,DWHCARREC AS DAT_RECPETION_CARTE
    ,DWHCARREM AS DAT_REMISE_CARTE
    ,DWHCARCRE AS CARTE_CREEE_MOIS
    ,DWHCARREN AS CARTE_RENOUVELLEE_MOIS
    ,DWHCARRES AS CARTE_RESILIEE_MOIS
    ,DWHCARDRN AS DAT_RENOUVELLEMENT
    ,DWHCARDRS AS DAT_RESILIATION
    ,DWHCARENV AS DAT_ENVOI_FACONNIER
    ,DWHCARAUT AS RENOUVELLEMENT_AUTOMATIQUE
    ,DWHCARLIV AS COD_LIVRAISON
    ,DWHCARACH AS MONT_PLFN_ACHAT
    ,DWHCARMRA AS MONT_PLFN_RETRAIT
    ,DWHCARCET AS COD_ETAT_CARTE
    ,DWHCARVIS AS TYP_VISUEL
    ,DWHCARACA AS NUMERO_ANCIENNE_CARTE
    ,DWHCARCRA AS CREATION_AUTOMATIQUE
    ,DWHCARINV AS DAT_INVALIDITE
    ,@date_analyse as DT_EXPLOITATION
    ,@date_action  as DATE_ACTION

from  [$(DataHubDatabaseName)].dbo.PV_SA_M_CARTE_AS_OF_DATE(@date_analyse) 
END