﻿CREATE PROCEDURE [dbo].[Alim_BF_Comptes] 
(
   @date_analyse DATE
)
AS
BEGIN

declare    @Mois_courant INT = 0

--declare @date_analyse DATE
declare @date_action DATE
declare @moisCourant INT
declare @anneeCourante INT
declare @anneeCouranteOuPrecedente INT

--set @date_analyse = (select MAX(DWHCPTDTX) from [$(DataHubDatabaseName)].[dbo].[PV_SA_M_COMPTE])
set @date_action = DATEADD (MONTH, DATEDIFF (MONTH,0,@date_analyse)+1,0)
set @anneeCourante = YEAR (@date_action)
set @moisCourant = MONTH(@date_action)
set @anneeCouranteOuPrecedente = @anneeCourante

if (@moisCourant = 1)
 (select @anneeCouranteOuPrecedente = @anneeCourante - 1)

SET NOCOUNT ON;

---------------------------------------------------------------------------------------///////////////// Step 0
--chargement de la table temporaire : #PV_SA_M_COMPTE_ENRICHIE
select [DWHCPTDTX], [DWHCPTETA], [DWHCPTPLA], [DWHCPTCOM], [DWHCPTRUB], [DWHCPTINT], [DWHCPTNBR], [DWHCPTPPAL], [DWHCPTAGE], [DWHCPTDEV], [DWHCPTOPR], [DWHCPTVAL], [DWHCPTCPT], [DWHCPTCPC], [DWHCPTRES], [DWHCPTDUR], [DWHCPTMG1], [DWHCPTMG2], [DWHCPTGAR], [DWHCPTGA1], [DWHCPTQUA], [DWHCPTGA2], [DWHCPTGA3], [DWHCPTSMOD], [DWHCPTSMOC], [DWHCPTCDS], [DWHCPTDAA], [DWHCPTPER], [DWHCPTNPR], [DWHCPTMTU], [DWHCPTMTD], [DWHCPTMTR], [DWHCPTMTA], [DWHCPTMTNR], [DWHCPTMTNS], [DWHCPTSMD], [DWHCPTSMC], [DWHCPTML1], [DWHCPTML2], [DWHCPTCLO], [DWHCPTDAC], [DWHCPTDAO], [DWHCPTMIP], [DWHCPTCIP], [DWHCPTPBC], [DWHCPTCPB], [DWHCPTDDD], [DWHCPTNJD], [DWHCPTCPM], [DWHCPTFLG], [DWHCPTMVC], [DWHCPTCOC], [DWHCPTCOB], [DWHCPTCLA], [DWHCPTUTI], [DWHCPTTXN], [DWHCPTREV], [DWHCPTSUR], [DWHCPTFRE], [DWHCPTRIS], [CPTBISNAT], [CPTBISGES], [PLAN_DE_COMPTES_CODE_PRODUIT] INTO #PV_SA_M_COMPTE_ENRICHIE
from
(
select distinct
       [DWHCPTDTX]
     , [DWHCPTETA]
     , [DWHCPTPLA]
     , [DWHCPTCOM]
     , [DWHCPTRUB]
     , [DWHCPTINT]
     , [DWHCPTNBR]
     , [DWHCPTPPAL]
     , [DWHCPTAGE]
     , [DWHCPTDEV]
     , [DWHCPTOPR]
     , [DWHCPTVAL]
     , [DWHCPTCPT]
     , [DWHCPTCPC]
     , [DWHCPTRES]
     , [DWHCPTDUR]
     , [DWHCPTMG1]
     , [DWHCPTMG2]
     , [DWHCPTGAR]
     , [DWHCPTGA1]
     , [DWHCPTQUA]
     , [DWHCPTGA2]
     , [DWHCPTGA3]
     , [DWHCPTSMOD]
     , [DWHCPTSMOC]
     , [DWHCPTCDS]
     , [DWHCPTDAA]
     , [DWHCPTPER]
     , [DWHCPTNPR]
     , [DWHCPTMTU]
     , [DWHCPTMTD]
     , [DWHCPTMTR]
     , [DWHCPTMTA]
     , [DWHCPTMTNR]
     , [DWHCPTMTNS]
     , [DWHCPTSMD]
     , [DWHCPTSMC]
     , [DWHCPTML1]
     , [DWHCPTML2]
     , [DWHCPTCLO]
     , [DWHCPTDAC]
     , [DWHCPTDAO]
     , [DWHCPTMIP]
     , [DWHCPTCIP]
     , [DWHCPTPBC]
     , [DWHCPTCPB]
     , [DWHCPTDDD]
     , [DWHCPTNJD]
     , [DWHCPTCPM]
     , [DWHCPTFLG]
     , [DWHCPTMVC]
     , [DWHCPTCOC]
     , [DWHCPTCOB]
     , [DWHCPTCLA]
     , [DWHCPTUTI]
     , [DWHCPTTXN]
     , [DWHCPTREV]
     , [DWHCPTSUR]
     , [DWHCPTFRE]
     , [DWHCPTRIS]
     , c.CPTBISNAT as CPTBISNAT
     , c.CPTBISGES as CPTBISGES
     , b.PLAN_DE_COMPTES_CODE_PRODUIT
  from [$(DataHubDatabaseName)].dbo.PV_SA_M_COMPTE_AS_OF_DATE(@date_analyse) a
  left join [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_CPTBIS] c
         on a.DWHCPTCOM = c.CPTBISCOM
  left join  [$(DataHubDatabaseName)].[dbo].[REF_PLAN_DE_COMPTES] b
         on a.DWHCPTRUB = b.PLAN_DE_COMPTES_COMPTE_OBLIGATOIRE
  where DWHCPTAGE not in ('51','52')                            --exclusion des agences 51 et 52 (CM)
) r

---------------------------------------------------------------------------------------///////////////// Step 1
--extraction des données brutes
select  NUMERO_COMPTE
      , NUMERO_CLIENT
	  , INTITULE_COMPTE
      , NOMBRE_TITULAIRE
	  , ETABLISSEMENT
	  , AGENCE
	  , DEVISE
      , SOLDE_DATE_OPERATION     
	  , SOLDE_DATE_VALEUR 
      , SOLDE_DATE_COMPTABLE_CVL
	  , SOLDE_DATE_COMPTABLE
      , RUBRIQUE_COMPTABLE
      , cd_pdt1													--rubrique comptable
      , cd_pdt2													--code produit SAB
	  , cd_pdt3													--code produit CRM : ancienne offre = code produit SIEBEL | nouvelle offre = code produit SALESFORCE
      , DATE_OUVERTURE
      , DATE_CLOTURE
	  , MOTIF_CLOTURE
      , SOLDE_MOYEN_DEBITEUR_CVL
      , SOLDE_MOYEN_CREDITEUR_CVL
	  , MONTANT_DEBITEUR_CVL
	  , MONTANT_CREDITEUR_CVL
      , FLUX_DEBITEUR_CVL
      , FLUX_CREDITEUR_CVL
	  , PART_BANQUE_TRESORERIE_CVL
	  , PART_BANQUE_TRESORERIE
      , COMPTE_MAITRE
      , FLAG_COMPTE_MAITRE
	  , CLASSE_SECURITE
	  , UTILISATION_MOIS
	  , TAUX_NOMINAL
	  , REVENUS_COTITULAIRE
	  , SURETE 
	  , CODE_APPORTEUR_COMPTE
      , CODE_CANAL_DISTRIBUTION
      , LIBELLE_CANAL_DISTRIBUTION			
      , CODE_CANAL_INTERACTION_ORIGINE
      , LIBELLE_CANAL_INTERACTION_ORIGINE	
      , CODE_RESEAU_DISTRIBUTEUR		
      , LIBELLE_RESEAU_DISTRIBUTEUR	
      , CODE_SOUS_RESEAU					
      , LIBELLE_SOUS_RESEAU
      , FLAG_INDICATION				
      , FLAG_DOSSIER_COMPLET
      , ID_PROCESSUS_SOUSCRIPTION
      , FLAG_PRESENCE_FE
	  , @date_analyse as DATE_EXPLOITATION
      , @date_action as DATE_ACTION
into #res1
from
(select row_number() over(partition by ISNULL(equi.EQUIPMENT_NUMBER,c.DWHCPTCOM) ORDER BY c.DWHCPTCOM desc) as rang
      , ltrim(rtrim(c.DWHCPTCOM)) as NUMERO_COMPTE
      , ltrim(rtrim(c.DWHCPTPPAL)) as NUMERO_CLIENT
	  , c.DWHCPTINT as INTITULE_COMPTE
      , c.DWHCPTNBR as NOMBRE_TITULAIRE
	  , c.DWHCPTETA as ETABLISSEMENT
	  , c.DWHCPTAGE as AGENCE
	  , c.DWHCPTDEV as DEVISE
      , c.DWHCPTOPR as SOLDE_DATE_OPERATION     
	  , c.DWHCPTVAL as SOLDE_DATE_VALEUR 
      , c.DWHCPTCPT as SOLDE_DATE_COMPTABLE_CVL
	  , c.DWHCPTCPC as SOLDE_DATE_COMPTABLE
      , c.DWHCPTRUB as RUBRIQUE_COMPTABLE
      , ltrim(rtrim(c.DWHCPTRUB)) as cd_pdt1												--rubrique comptable
      , ltrim(rtrim(c.PLAN_DE_COMPTES_CODE_PRODUIT)) as cd_pdt2								--code produit SAB
	  , isnull(coalesce(c.CPTBISGES, opp.CommercialOfferCode__c),'') as cd_pdt3				--code produit CRM : ancienne offre = code produit SIEBEL | nouvelle offre = code produit SALESFORCE
      , c.DWHCPTDAO as DATE_OUVERTURE
      , c.DWHCPTDAC as DATE_CLOTURE
	  , c.DWHCPTCLO as MOTIF_CLOTURE
      , c.DWHCPTSMD as SOLDE_MOYEN_DEBITEUR_CVL
      , c.DWHCPTSMC as SOLDE_MOYEN_CREDITEUR_CVL
	  , c.DWHCPTML1 as MONTANT_DEBITEUR_CVL
	  , c.DWHCPTML2 as MONTANT_CREDITEUR_CVL
      , c.DWHCPTSMOD as FLUX_DEBITEUR_CVL
      , c.DWHCPTSMOC as FLUX_CREDITEUR_CVL
	  , c.DWHCPTPBC as PART_BANQUE_TRESORERIE_CVL
	  , c.DWHCPTCPB as PART_BANQUE_TRESORERIE
      , ltrim(rtrim(c.DWHCPTCPM)) as COMPTE_MAITRE
      , c.DWHCPTFLG as FLAG_COMPTE_MAITRE
	  , c.DWHCPTCLA as CLASSE_SECURITE
	  , c.DWHCPTUTI as UTILISATION_MOIS
	  , c.DWHCPTTXN as TAUX_NOMINAL
	  , c.DWHCPTREV as REVENUS_COTITULAIRE
	  , c.DWHCPTSUR as SURETE 
	  , ltrim(rtrim(c.CPTBISNAT)) as CODE_APPORTEUR_COMPTE
      , opp.DistributionChannel__c as CODE_CANAL_DISTRIBUTION
      , opp.DistributionChannel__c_label as LIBELLE_CANAL_DISTRIBUTION			
      , opp.StartedChannel__c	as CODE_CANAL_INTERACTION_ORIGINE
      , opp.StartedChannel__c_label as LIBELLE_CANAL_INTERACTION_ORIGINE	
      , opp.DistributorNetwork__c	as CODE_RESEAU_DISTRIBUTEUR		
      , opp.DistributorNetwork__c_label as LIBELLE_RESEAU_DISTRIBUTEUR	
      , opp.DistributorEntity__c	as CODE_SOUS_RESEAU					
      , opp.DistributorEntity__c_label as LIBELLE_SOUS_RESEAU
      , opp.Indication__c as FLAG_INDICATION				
      , opp.CompleteFileFlag__c as FLAG_DOSSIER_COMPLET
      , opp.IDProcessSous__c as ID_PROCESSUS_SOUSCRIPTION
      , case when equi.EQUIPMENT_NUMBER is NULL then 0 else 1 end as FLAG_PRESENCE_FE
	  , @date_analyse as DATE_EXPLOITATION
      , @date_action as DATE_ACTION
   from #PV_SA_M_COMPTE_ENRICHIE c
--jointure avec les tables PV_FE_EQUIPMENT + PV_FE_ENROLMENTFOLDER + [PV_SF_OPPORTUNITY_HISTORY : code produit CRM nouvelle offre
   left join  [$(DataHubDatabaseName)].[dbo].PV_FE_EQUIPMENT equi
          on c.DWHCPTCOM = equi.EQUIPMENT_NUMBER  
   left join  [$(DataHubDatabaseName)].[dbo].PV_FE_ENROLMENTFOLDER enr
          on equi.ENROLMENT_FOLDER_ID = enr.ID_FE_ENROLMENT_FOLDER  
   left join  [$(DataHubDatabaseName)].[dbo].[PV_SF_OPPORTUNITY_HISTORY_AS_OF_DATE](@date_analyse) opp
          on enr.SALESFORCE_OPPORTUNITY_ID = opp.Id_SF
) r
where rang = 1

---------------------------------------------------------------------------------------///////////////// Step 2
--calcul des code produit SIDP + enrichissement de la table avec les codes produit SAB et les codes produit CRM
select [NUMERO_COMPTE], [NUMERO_CLIENT], [INTITULE_COMPTE], [NOMBRE_TITULAIRE], [ETABLISSEMENT], [AGENCE], [DEVISE], [SOLDE_DATE_OPERATION], [SOLDE_DATE_VALEUR], [SOLDE_DATE_COMPTABLE_CVL], [SOLDE_DATE_COMPTABLE], [RUBRIQUE_COMPTABLE], [CODE_PRODUIT_DF], [CODE_REGROUPEMENT_DF], [DATE_OUVERTURE], [DATE_CLOTURE], [MOTIF_CLOTURE], [SOLDE_MOYEN_DEBITEUR_CVL], [SOLDE_MOYEN_CREDITEUR_CVL], [MONTANT_DEBITEUR_CVL], [MONTANT_CREDITEUR_CVL], [FLUX_DEBITEUR_CVL], [FLUX_CREDITEUR_CVL], [PART_BANQUE_TRESORERIE_CVL], [PART_BANQUE_TRESORERIE], [COMPTE_MAITRE], [FLAG_COMPTE_MAITRE], [CLASSE_SECURITE], [UTILISATION_MOIS], [TAUX_NOMINAL], [REVENUS_COTITULAIRE], [SURETE], [CODE_PRODUIT_SAB], [CODE_PRODUIT_CRM], [CODE_APPORTEUR_COMPTE], [CODE_CANAL_DISTRIBUTION], [LIBELLE_CANAL_DISTRIBUTION], [CODE_CANAL_INTERACTION_ORIGINE], [LIBELLE_CANAL_INTERACTION_ORIGINE], [CODE_RESEAU_DISTRIBUTEUR], [LIBELLE_RESEAU_DISTRIBUTEUR], [CODE_SOUS_RESEAU], [LIBELLE_SOUS_RESEAU], [FLAG_INDICATION], [FLAG_DOSSIER_COMPLET], [ID_PROCESSUS_SOUSCRIPTION], [FLAG_PRESENCE_FE], [DATE_EXPLOITATION], [DATE_ACTION] into #res2
from
( select NUMERO_COMPTE
       , NUMERO_CLIENT
       , INTITULE_COMPTE
       , NOMBRE_TITULAIRE
	   , ETABLISSEMENT
	   , AGENCE
	   , DEVISE
       , SOLDE_DATE_OPERATION
       , SOLDE_DATE_VALEUR
	   , SOLDE_DATE_COMPTABLE_CVL
	   , SOLDE_DATE_COMPTABLE
       , RUBRIQUE_COMPTABLE
       , coalesce(p_el.code_produit_SIDP, p_cv_ep.code_produit_SIDP, case when t1.ID is not null then 'CAV030' when r1.cd_pdt2 in ('CW4') then 'CAV009' end) as CODE_PRODUIT_DF
       , coalesce(p_el.code_regroupement_SIDP, p_cv_ep.code_regroupement_SIDP, case when r1.cd_pdt2 in ('CW4') then 'CV' end) as CODE_REGROUPEMENT_DF
       , DATE_OUVERTURE
       , DATE_CLOTURE
	   , MOTIF_CLOTURE
       , SOLDE_MOYEN_DEBITEUR_CVL
       , SOLDE_MOYEN_CREDITEUR_CVL
       , MONTANT_DEBITEUR_CVL
       , MONTANT_CREDITEUR_CVL
	   , FLUX_DEBITEUR_CVL
	   , FLUX_CREDITEUR_CVL
	   , PART_BANQUE_TRESORERIE_CVL
	   , PART_BANQUE_TRESORERIE
       , COMPTE_MAITRE
       , FLAG_COMPTE_MAITRE
	   , CLASSE_SECURITE
	   , UTILISATION_MOIS
	   , TAUX_NOMINAL
	   , REVENUS_COTITULAIRE
	   , SURETE
       , coalesce(p_el.[code_produit_SAB], p_cv_ep.[code_produit_SAB], case when r1.cd_pdt2 in ('CW4') then r1.cd_pdt2 end) as CODE_PRODUIT_SAB
	   , r1.cd_pdt3 as CODE_PRODUIT_CRM
	   , CODE_APPORTEUR_COMPTE
       , CODE_CANAL_DISTRIBUTION
       , LIBELLE_CANAL_DISTRIBUTION			
       , CODE_CANAL_INTERACTION_ORIGINE
       , LIBELLE_CANAL_INTERACTION_ORIGINE	
       , CODE_RESEAU_DISTRIBUTEUR		
       , LIBELLE_RESEAU_DISTRIBUTEUR	
       , CODE_SOUS_RESEAU					
       , LIBELLE_SOUS_RESEAU
       , FLAG_INDICATION				
       , FLAG_DOSSIER_COMPLET
       , ID_PROCESSUS_SOUSCRIPTION
       , FLAG_PRESENCE_FE
	   , DATE_EXPLOITATION
       , DATE_ACTION
    from #res1 r1
--traitement du regroupement des ELANCIO, des credits à la consommation, des credits affectes, des credits revolving
    left join  [$(DataHubDatabaseName)].[dbo].[REF_PRODUIT] p_el WITH(NOLOCK)
--          on r1.cd_pdt1 = cast(cast(p_el.[code_rubrique_comptable_SAB] as bigint) as varchar(30))
            on r1.cd_pdt1 = p_el.[code_rubrique_comptable_SAB] 
           and r1.cd_pdt2 = p_el.[code_produit_SAB]
           and r1.cd_pdt3 = p_el.[code_produit_CRM]
           and p_el.[code_regroupement_SIDP] in ('EL','DE','CA','CO')

--traitement des regroupements des comptes hors ELANCIO, ASTREA & ASTREA+, credits a la consommation, credits affectes, credits revolving         
     left join  [$(DataHubDatabaseName)].[dbo].[REF_PRODUIT] p_cv_ep WITH(NOLOCK)
--          on r1.cd_pdt1 = cast(cast(p_cv_ep.[code_rubrique_comptable_SAB] as bigint) as varchar(30))
            on r1.cd_pdt1 = p_cv_ep.[code_rubrique_comptable_SAB] 
           and r1.cd_pdt2 = p_cv_ep.[code_produit_SAB]
           and p_cv_ep.[code_regroupement_SIDP] not in ('EL','DE','CA','CO')                      
           and r1.cd_pdt2 not in ('CW4')                          

--traitement des comptes ASTREA & ASTREA+
     left join (select [ID], [DWHABODTX], [DWHABOETA], [DWHABOAGE], [DWHABOSER], [DWHABOSSE], [DWHABONUM], [DWHABOCLI], [DWHABOCOM], [DWHABOPRO], [DWHABOADH], [DWHABOFIN], [DWHABOREN], [DWHABOCET], [DWHABOCOF], [DWHABOUT1], [DWHABOUT2], [DWHABORES], [DWHABOMOR], [DWHABOCRE], [DWHABOVAL], [DWHABOANN], [DWHABOREL], [Validity_StartDate], [Validity_EndDate], [Startdt], [Enddt], [rang] from
               (select [ID], [DWHABODTX], [DWHABOETA], [DWHABOAGE], [DWHABOSER], [DWHABOSSE], [DWHABONUM], [DWHABOCLI], [DWHABOCOM], [DWHABOPRO], [DWHABOADH], [DWHABOFIN], [DWHABOREN], [DWHABOCET], [DWHABOCOF], [DWHABOUT1], [DWHABOUT2], [DWHABORES], [DWHABOMOR], [DWHABOCRE], [DWHABOVAL], [DWHABOANN], [DWHABOREL], [Validity_StartDate], [Validity_EndDate], [Startdt], [Enddt], ROW_NUMBER() OVER(PARTITION BY DWHABOCOM ORDER BY DWHABOADH desc) rang
                from  [$(DataHubDatabaseName)].[dbo].[PV_SA_M_ABONNEMENT_AS_OF_DATE](@date_analyse)
                where DWHABOPRO like 'E40' and ((DWHABOCET = 2 and DWHABOADH <= @date_analyse) or (DWHABOCET = 4 and DWHABORES > @date_analyse))
		   	   ) rq1
			    where rq1.rang = 1
			   ) t1     --selection des Astréa+
           on r1.NUMERO_COMPTE = t1.DWHABOCOM
          and r1.cd_pdt2 in ('CW4')                                           
    where coalesce(p_el.code_regroupement_SIDP, p_cv_ep.code_regroupement_SIDP, case when r1.cd_pdt2 in ('CW4') then 'CV' end) is not null

) r

---------------------------------------------------------------------------------------///////////////// Step 3
--Mise a jour des données Epargne
select [NUMERO_COMPTE], [NUMERO_CLIENT], [INTITULE_COMPTE], [NOMBRE_TITULAIRE], [ETABLISSEMENT], [AGENCE], [DEVISE], [SOLDE_DATE_OPERATION], [SOLDE_DATE_VALEUR], [SOLDE_DATE_COMPTABLE_CVL], [SOLDE_DATE_COMPTABLE], [RUBRIQUE_COMPTABLE], [CODE_PRODUIT_DF], [LIBELLE_PRODUIT_DF], [DATE_OUVERTURE], [DATE_CLOTURE], [MOTIF_CLOTURE], [SOLDE_MOYEN_DEBITEUR_CVL], [SOLDE_MOYEN_CREDITEUR_CVL], [MONTANT_DEBITEUR_CVL], [MONTANT_CREDITEUR_CVL], [FLUX_DEBITEUR_CVL], [FLUX_CREDITEUR_CVL], [PART_BANQUE_TRESORERIE_CVL], [PART_BANQUE_TRESORERIE], [COMPTE_MAITRE], [FLAG_COMPTE_MAITRE], [CLASSE_SECURITE], [UTILISATION_MOIS], [TAUX_NOMINAL], [REVENUS_COTITULAIRE], [SURETE], [ACTIVITE_CLIENT_CAV], [ACTIVITE_CLIENT_EPARGNE], [CODE_PRODUIT_SAB], [CODE_PRODUIT_CRM], [CODE_APPORTEUR_COMPTE], [CODE_CANAL_DISTRIBUTION], [LIBELLE_CANAL_DISTRIBUTION], [CODE_CANAL_INTERACTION_ORIGINE], [LIBELLE_CANAL_INTERACTION_ORIGINE], [CODE_RESEAU_DISTRIBUTEUR], [LIBELLE_RESEAU_DISTRIBUTEUR], [CODE_SOUS_RESEAU], [LIBELLE_SOUS_RESEAU], [FLAG_INDICATION], [FLAG_DOSSIER_COMPLET], [ID_PROCESSUS_SOUSCRIPTION], [FLAG_PRESENCE_FE], [DATE_EXPLOITATION], [DATE_ACTION] into #res3
from
(
select NUMERO_COMPTE
     , NUMERO_CLIENT
     , INTITULE_COMPTE
     , NOMBRE_TITULAIRE
	 , ETABLISSEMENT
	 , AGENCE
	 , DEVISE
     , SOLDE_DATE_OPERATION
     , SOLDE_DATE_VALEUR
	 , SOLDE_DATE_COMPTABLE_CVL
	 , SOLDE_DATE_COMPTABLE
     , RUBRIQUE_COMPTABLE
     , r2.CODE_PRODUIT_DF
	 , p.libelle_code_produit_SIDP as LIBELLE_PRODUIT_DF
     , DATE_OUVERTURE
     , DATE_CLOTURE
	 , MOTIF_CLOTURE
     , case when r2.[CODE_REGROUPEMENT_DF] = 'EP' and r2.[CODE_PRODUIT_SAB] not in ('DAT','ERS','PE2','ER2') then e.DWHEREBA1 else SOLDE_MOYEN_DEBITEUR_CVL end as SOLDE_MOYEN_DEBITEUR_CVL
     , case when r2.[CODE_REGROUPEMENT_DF] = 'EP' and r2.[CODE_PRODUIT_SAB] not in ('DAT','ERS','PE2','ER2') then e.DWHEREBA2 else SOLDE_MOYEN_CREDITEUR_CVL end as SOLDE_MOYEN_CREDITEUR_CVL
	 , MONTANT_DEBITEUR_CVL
	 , MONTANT_CREDITEUR_CVL
     , FLUX_DEBITEUR_CVL
     , FLUX_CREDITEUR_CVL
	 , PART_BANQUE_TRESORERIE_CVL
	 , PART_BANQUE_TRESORERIE
     , COMPTE_MAITRE
     , FLAG_COMPTE_MAITRE
	 , CLASSE_SECURITE
	 , UTILISATION_MOIS
	 , TAUX_NOMINAL
	 , REVENUS_COTITULAIRE
	 , SURETE
	 , NULL as ACTIVITE_CLIENT_CAV
	 , NULL as ACTIVITE_CLIENT_EPARGNE
     , r2.CODE_PRODUIT_SAB
     , r2.CODE_PRODUIT_CRM
	 , CODE_APPORTEUR_COMPTE
     , CODE_CANAL_DISTRIBUTION
     , LIBELLE_CANAL_DISTRIBUTION			
     , CODE_CANAL_INTERACTION_ORIGINE
     , LIBELLE_CANAL_INTERACTION_ORIGINE	
     , CODE_RESEAU_DISTRIBUTEUR		
     , LIBELLE_RESEAU_DISTRIBUTEUR	
     , CODE_SOUS_RESEAU					
     , LIBELLE_SOUS_RESEAU
     , FLAG_INDICATION				
     , FLAG_DOSSIER_COMPLET
     , ID_PROCESSUS_SOUSCRIPTION
     , FLAG_PRESENCE_FE
	 , DATE_EXPLOITATION
     , DATE_ACTION
  from #res2 r2
  left join [$(DataHubDatabaseName)].[dbo].[PV_SA_M_ENCRS_EPRGN_AS_OF_DATE](@date_analyse) e
         on r2.NUMERO_COMPTE = e.DWHERECOM
        and r2.[CODE_REGROUPEMENT_DF] = 'EP'
  left join [$(DataHubDatabaseName)].[dbo].[REF_PRODUIT] p WITH(NOLOCK)
         on r2.CODE_PRODUIT_DF = p.code_produit_SIDP
  where r2.[CODE_REGROUPEMENT_DF] in ('CV','EL','EP','CB','AU','DE','CA','CO')                      --integration autres comptes (modification 2019-05-29:ajout 'FF')
) r

---------------------------------------------------------------------------------------///////////////// Step 4
--insertion dans la table cible + calcul du marché et de la marque
insert into [SIDP_DataFactory_BFR].[dbo].[BF_COMPTES]
select r.NUMERO_COMPTE
     , case when r.NOMBRE_TITULAIRE = 0 then rr.NUMERO_CLIENT
			else r.NUMERO_CLIENT
	   end as NUMERO_CLIENT
     , r.INTITULE_COMPTE
     , r.NOMBRE_TITULAIRE
	 , r.ETABLISSEMENT
	 , r.AGENCE
	 , r.DEVISE
     , r.SOLDE_DATE_OPERATION
     , r.SOLDE_DATE_VALEUR
	 , r.SOLDE_DATE_COMPTABLE_CVL
	 , r.SOLDE_DATE_COMPTABLE
     , r.RUBRIQUE_COMPTABLE
	 , case when r.NOMBRE_TITULAIRE = 0 then rr.CODE_MARCHE_DF
	        else c.CODE_MARCHE_DF 
	   end as CODE_MARCHE_DF												
	 , case when r.NOMBRE_TITULAIRE = 0 then rr.CODE_RESEAU_DF
	        else c.CODE_RESEAU_DF
	   end as CODE_RESEAU_DF												
     , r.CODE_PRODUIT_DF
	 , r.LIBELLE_PRODUIT_DF
     , r.DATE_OUVERTURE
     , r.DATE_CLOTURE
	 , r.MOTIF_CLOTURE
     , r.SOLDE_MOYEN_DEBITEUR_CVL
     , r.SOLDE_MOYEN_CREDITEUR_CVL
	 , r.MONTANT_DEBITEUR_CVL
	 , r.MONTANT_CREDITEUR_CVL
     , r.FLUX_DEBITEUR_CVL
     , r.FLUX_CREDITEUR_CVL
	 , r.PART_BANQUE_TRESORERIE_CVL
	 , r.PART_BANQUE_TRESORERIE
     , r.COMPTE_MAITRE
     , r.FLAG_COMPTE_MAITRE
	 , r.CLASSE_SECURITE
	 , r.UTILISATION_MOIS
	 , r.TAUX_NOMINAL
	 , r.REVENUS_COTITULAIRE
	 , r.SURETE
     , r.ACTIVITE_CLIENT_CAV
	 , r.ACTIVITE_CLIENT_EPARGNE
     , r.CODE_PRODUIT_SAB
     , r.CODE_PRODUIT_CRM
	 , r.CODE_APPORTEUR_COMPTE
     , r.CODE_CANAL_DISTRIBUTION
     , r.LIBELLE_CANAL_DISTRIBUTION			
     , r.CODE_CANAL_INTERACTION_ORIGINE
     , r.LIBELLE_CANAL_INTERACTION_ORIGINE	
     , r.CODE_RESEAU_DISTRIBUTEUR		
     , r.LIBELLE_RESEAU_DISTRIBUTEUR	
     , r.CODE_SOUS_RESEAU					
     , r.LIBELLE_SOUS_RESEAU
     , r.FLAG_INDICATION				
     , r.FLAG_DOSSIER_COMPLET
     , r.ID_PROCESSUS_SOUSCRIPTION
     , r.FLAG_PRESENCE_FE
	 , r.DATE_EXPLOITATION
     , r.DATE_ACTION
  from #res3 r
  left join [SIDP_DataFactory_BFR].dbo.[BF_COMPTES] rr
	on rr.NUMERO_COMPTE = r.NUMERO_COMPTE and dateadd(month, datediff(month, 0, rr.DATE_ACTION), 0) = dateadd(month, datediff(month, 0, r.DATE_ACTION) - 1, 0)
  left join [SIDP_DataFactory_BFR].dbo.[BF_CLIENTS] c
    on c.[NUMERO_CLIENT] = r.NUMERO_CLIENT and c.[DATE_ACTION] = r.DATE_ACTION

END