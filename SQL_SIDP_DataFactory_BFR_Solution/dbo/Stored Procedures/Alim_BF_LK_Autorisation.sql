﻿CREATE PROCEDURE [dbo].[Alim_BF_LK_Autorisation] 
(
   @date_analyse DATE
)
AS
BEGIN
    SET NOCOUNT ON;

    declare @date_action DATE
    set @date_action = DATEADD (MONTH, DATEDIFF (MONTH,0,@date_analyse)+1,0);

-- insertion dans la table cible

    insert into dbo.BF_LK_AUTORISATION
	SELECT 
	DWHLIAETA as COD_ETABLISSEMENT,
	DWHLIACLI  as NUMERO_CLIENT_FILS,
	DWHLIATYP  as TYP_FILS,
	DWHLIAAUT  as COD_AUTORISATION_FILS,
	DWHLIATY1  as TYP_PERE,
	DWHLIAAU1  as COD_AUTORISATION_PERE,
	DWHLIATAU  as TAUX_REPORT,
	DWHLIAUTI as COD_UTILISATEUR,
	DWHLIAFRA  as FREQUENCE,
	DWHLIADOM  as DOMAINE,
	DWHLIAAPP  as APPLICATION,
	DWHLIADSY as DATE_JOUR,
	@date_analyse as DT_EXPLOITATION,
    @date_action  as DATE_ACTION
from  [$(DataHubDatabaseName)].dbo.PV_SA_M_LKAUT_AS_OF_DATE(@date_analyse) 
END
