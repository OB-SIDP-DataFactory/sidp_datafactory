﻿

CREATE PROCEDURE [dbo].[Alim_BF_Lien_Clients] 
(
   @date_analyse DATE
)
AS
BEGIN
	--declare @date_analyse DATE
    declare @date_action DATE
	
	--set @date_analyse =  (select MAX(dwhcptdtx) from [$(DataHubDatabaseName)].[dbo].[PV_SA_M_COMPTE])
	set @date_action = DATEADD (MONTH, DATEDIFF (MONTH,0,@date_analyse)+1,0)
	
	SET NOCOUNT ON;

------------------------------///////////////// Step 1
--insertion dans la table cible

insert  into BF_LIEN_CLIENTS

select [DWHGRPCLI] as NUMERO_CLIENT
      ,[DWHGRPREG] as NUMERO_CLIENT_GROUPE
      ,[DWHGRPREL] as NATURE_RELATION
	  ,@date_analyse as DATE_EXPLOITATION
	  ,@date_action as DATE_ACTION

from [$(DataHubDatabaseName)].[dbo].[PV_SA_M_LKCPTCLI_AS_OF_DATE](@date_analyse)		

END