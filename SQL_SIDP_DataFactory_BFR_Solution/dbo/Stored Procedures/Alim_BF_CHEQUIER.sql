﻿
CREATE PROCEDURE [dbo].[Alim_BF_CHEQUIER] 
(
   @date_analyse DATE
)
AS
BEGIN
    SET NOCOUNT ON;

    declare @date_action DATE
    set @date_action = DATEADD (MONTH, DATEDIFF (MONTH,0,@date_analyse)+1,0);

-- insertion dans la table cible

    insert into dbo.BF_CHEQUIER

	SELECT 
	 DWHCHQCOM     as NUMERO_COMPTE
    ,DWHCHQETB     as COD_ETABLISSEMENT
    ,DWHCHQAG1     as COD_AGENCE_STOCK
    ,DWHCHQAGE     as COD_AGENCE_COMPTE
    ,DWHCHQTYP     as TYP_CHEQUIER
    ,DWHCHQDAT     as DAT_DEMANDE
    ,DWHCHQSEQ     as NUMERO_SEQUENCE
    ,DWHCHQREC     as DAT_RECEPTION_CHEQUIER
    ,DWHCHQREM     as DATE_REMISE_CHEQUIER
    ,DWHCHQCAL     as COD_LIVRAISON
    ,DWHCHQDSP     as DAT_SUPPRESSION
    ,DWHCHQREN     as COD_ETAT_RENOUVELLEMENT
    ,DWHCHQDRE     as DAT_RENOUVELLEMENT
    ,DWHCHQORI     as ORIGINE_CHEQUIER
    ,@date_analyse as DT_EXPLOITATION
    ,@date_action  as DATE_ACTION
from  [$(DataHubDatabaseName)].dbo.VW_PV_SA_M_CHEQUIER
WHERE DWHCHQDTX=@date_analyse

END