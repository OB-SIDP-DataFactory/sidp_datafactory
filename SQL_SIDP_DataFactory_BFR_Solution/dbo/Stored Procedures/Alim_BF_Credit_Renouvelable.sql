﻿CREATE PROCEDURE [dbo].[Alim_BF_Credit_Renouvelable] 
(
   @date_analyse DATE
)
AS
BEGIN
    SET NOCOUNT ON;

    declare @date_action DATE
    set @date_action = CAST(DATEADD (MONTH, DATEDIFF (MONTH,0,@date_analyse)+1,0) AS DATE)

----------------------------------///////////////// Step 1
----------------------------------selection des données CREDIT RENOUVELABLE
select [NUMERO_PRET], [FFQ_NUMERO_PRET], [FFM_NUMERO_PRET], [FFM_DATE_TRAITEMENT], [CTX_NUMERO_PRET], [CTX_TYPE_FLUX], [CTX_TYPE_PRET], [FFQ_IDENTIFIANT_CRM_EMPRUNTEUR], [FFQ_IDENTIFIANT_CRM_COEMPRUNTEUR], [FFQ_TYPE_COMPLEO], [FFQ_DATE_OUVERTURE], [FFQ_DATE_1ERE_UTILISATION], [FFQ_NUMERO_BAREME_VIGUEUR], [FFQ_VARIANTE_BAREME_VIGUEUR], [FFQ_GRILLE_TAUX_BAREME_VIGUEUR], [FFQ_GRILLE_MENSUALITE_BAREME_VIGUEUR], [FFQ_VITESSE_BAREME_VIGUEUR], [FFQ_DATE_FIN_BAREME_VIGUEUR], [FFQ_TYPE_BAREME], [FFQ_NUMERO_BAREME_STANDARD_FUTUR], [FFQ_VARIANTE_BAREME_STANDARD_FUTUR], [FFQ_GRILLE_TAUX_BAREME_STANDARD_FUTUR], [FFQ_GRILLE_MENSUALITE_BAREME_STANDARD_FUTUR], [FFQ_VITESSE_BAREME_STANDARD_FUTUR], [FFQ_CODE_BANQUE_COMPTE_PRELEVEMENT], [FFQ_CODE_GUICHET_COMPTE_PRELEVEMENT], [FFQ_NUMERO_COMPTE_PRELEVEMENT], [FFQ_CLE_RIB_COMPTE_PRELEVEMENT], [FFQ_LIBELLE_TITULAIRE_COMPTE], [FFQ_TOP_ASSURANCE], [FFQ_NATURE_ASSURANCE], [FFQ_MONTANT_TOTAL_AUTORISATION], [FFQ_MONTANT_DISPONIBLE], [FFQ_MONTANT_TOTAL_UTILISATION], [FFQ_MONTANT_UC], [FFQ_MONTANT_UP], [FFQ_DATE_DERNIERE_ECHEANCE_TRAITEE], [FFQ_MONTANT_TOTAL_DERNIERE_ECHEANCE_TRAITEE], [FFQ_MONTANT_DERNIERE_ECHEANCE_UC], [FFQ_MONTANT_CAPITAL_DERNIERE_ECHEANCE_UC], [FFQ_MONTANT_INTERETS_DERNIERE_ECHEANCE_UC], [FFQ_MONTANT_ASSURANCE_DERNIERE_ECHEANCE_UC], [FFQ_MONTANT_DERNIERE_ECHEANCE_UP], [FFQ_MONTANT_CAPITAL_DERNIERE_ECHEANCE_UP], [FFQ_MONTANT_INTERETS_DERNIERE_ECHEANCE_UP], [FFQ_MONTANT_ASSURANCE_DERNIERE_ECHEANCE_UP], [FFQ_NOMBRE_MENSUALITES_REPORTEES], [FFQ_TOP_MODIFICATION_RESERVE_AUTORISEE], [FFQ_DATE_MODIFICATION_RESERVE_AUTORISEE], [FFQ_NOMBRE_ECHEANCES_IMPAYEES], [FFQ_CODE_SITUATION_DOSSIER], [FFQ_DATE_DERNIERE_MODIFICATION_POSITION_CASH], [FFQ_CODE_POSITION], [FFQ_DATE_DERNIERE_MODIFICATION_CODE_POSITION], [FFQ_NOMBRE_UP], [FFM_DOMICILIATION_ETABLISSEMENT], [FFM_DOMICILIATION_GUICHET], [FFM_DOMICILIATION_COMPTE], [FFM_DATE_OUVERTURE], [FFM_DEVISE], [FFM_MONTANT_MAXIMUM_AUTORISE_TOUTES_OPERATIONS], [FFM_MONTANT_MAXIMUM_AUTORISE_VIREMENTS], [FFM_MONTANT_DISPONIBLE_TOUTES_OPERATIONS], [FFM_MONTANT_UTILISE_TOUTES_OPERATIONS], [FFM_MONTANT_DISPONIBLE_VIREMENTS], [FFM_ENCOURS_MOYEN], [FFM_MONTANT_ECHEANCE], [FFM_TAUX_MOYEN_HORS_ASSURANCE], [FFM_CODE_STATUT_SUIVI_DOSSIER], [FFM_DATE_CLOTURE], [FFM_MONTANT_FRAIS], [FFM_NOMBRE_DEMANDES_VIREMENT], [FFM_MONTANT_DEMANDES_VIREMENT], [FFM_NOMBRE_RETRAITS_GUICHET], [FFM_MONTANT_RETRAITS_GUICHET], [FFM_NOMBRE_REMBOURSEMENTS_ANTICIPES], [FFM_MONTANT_REMBOURSEMENTS_ANTICIPES], [FFM_MONTANT_COMMISSIONS], [FFM_IDENTIFIANT_CRM], [FFM_PHASE_RECOUVREMENT], [FFM_MONTANT_IMPAYES], [FFM_NOMBRE_IMPAYES], [FFM_DATE_1ER_IMPAYE], [CTX_IDENTIFIANT_CRM], [CTX_CODE_BANQUE], [CTX_CODE_GUICHET], [CTX_NUMERO_COMPTE], [CTX_DEVISE], [CTX_DATE_REMISE_CONTENTIEUX], [CTX_CAPITAL_DU_SUR_MENSUALITES_A_ECHOIR], [CTX_MONTANT_MENSUALITES_IMPAYEES], [CTX_MONTANT_INTERETS_MENSUALITES_IMPAYEES], [CTX_INDEMNITES_LEGALES], [CTX_SOLDE_AFFAIRE_CONTENTIEUSE], [CTX_DATE_DERNIERE_ECRITURE_COMPTABLE], [CTX_CUMUL_PERTES], [CTX_CODE_POSITION], [CPT_MONTANT_TOTAL_AUTORISE], [CPT_MONTANT_TOTAL_UTILISE] into #TEMPCR
from
(
select 
 ltrim(rtrim(cast(b.COMREFREF as varchar(20)))) as NUMERO_PRET
,a.NUM_PRE as FFQ_NUMERO_PRET
,e.NUM_PRE as FFM_NUMERO_PRET
,e.DATTTP as FFM_DATE_TRAITEMENT
,d.NUM_PRE as CTX_NUMERO_PRET
,d.TYP_CON as CTX_TYPE_FLUX
,d.TYP_PRE as CTX_TYPE_PRET

--donnees flux quotidien FF dernier jour ouvre du mois
,a.IDE_GRC as FFQ_IDENTIFIANT_CRM_EMPRUNTEUR
,a.IDE_GRC_COE as FFQ_IDENTIFIANT_CRM_COEMPRUNTEUR
,a.TYP_COM as FFQ_TYPE_COMPLEO
,a.DTE_OUV as FFQ_DATE_OUVERTURE
,a.DTE_PRE_UTI as FFQ_DATE_1ERE_UTILISATION 
,a.NUM_BAR as FFQ_NUMERO_BAREME_VIGUEUR
,a.VAR_BAR as FFQ_VARIANTE_BAREME_VIGUEUR
,a.GRI_TAX_BAR as FFQ_GRILLE_TAUX_BAREME_VIGUEUR
,a.GRI_TAX_MEN as FFQ_GRILLE_MENSUALITE_BAREME_VIGUEUR
,a.VIT_BAR as FFQ_VITESSE_BAREME_VIGUEUR
,a.DTE_FIN_BAR as FFQ_DATE_FIN_BAREME_VIGUEUR
,a.TYP_BAR as FFQ_TYPE_BAREME
,a.NUM_BAR_FUT as FFQ_NUMERO_BAREME_STANDARD_FUTUR
,a.VAR_BAR_FUT as FFQ_VARIANTE_BAREME_STANDARD_FUTUR
,a.GRI_TAX_BAR_FUT as FFQ_GRILLE_TAUX_BAREME_STANDARD_FUTUR
,a.GRI_TAX_MEN_FUT as FFQ_GRILLE_MENSUALITE_BAREME_STANDARD_FUTUR
,a.VIT_BAR_FUT as FFQ_VITESSE_BAREME_STANDARD_FUTUR
,a.COD_BAN_CAV as FFQ_CODE_BANQUE_COMPTE_PRELEVEMENT
,a.COD_GUI_BAN as FFQ_CODE_GUICHET_COMPTE_PRELEVEMENT
,a.NUM_CAV as FFQ_NUMERO_COMPTE_PRELEVEMENT
,a.CLE_RIB_CAV as FFQ_CLE_RIB_COMPTE_PRELEVEMENT
,a.LIB_CPT as FFQ_LIBELLE_TITULAIRE_COMPTE
,a.TOP_ASS as FFQ_TOP_ASSURANCE
,a.NAT_ASS as FFQ_NATURE_ASSURANCE
,a.MTT_TOT_AUT as FFQ_MONTANT_TOTAL_AUTORISATION
,a.MTT_DIS as FFQ_MONTANT_DISPONIBLE
,a.MTT_TOT_UTI as FFQ_MONTANT_TOTAL_UTILISATION
,a.MTT_UTI_COU as FFQ_MONTANT_UC
,a.MTT_UTI_UP as FFQ_MONTANT_UP
,a.DTE_LST_ECH as FFQ_DATE_DERNIERE_ECHEANCE_TRAITEE
,a.MTT_TOT_ECH as FFQ_MONTANT_TOTAL_DERNIERE_ECHEANCE_TRAITEE
,a.MTT_ECH_UC as FFQ_MONTANT_DERNIERE_ECHEANCE_UC
,a.MTT_CAP_UC as FFQ_MONTANT_CAPITAL_DERNIERE_ECHEANCE_UC
,a.MTT_INT_UC as FFQ_MONTANT_INTERETS_DERNIERE_ECHEANCE_UC
,a.MTT_ASS_UC as FFQ_MONTANT_ASSURANCE_DERNIERE_ECHEANCE_UC
,a.MTT_ECH_UP as FFQ_MONTANT_DERNIERE_ECHEANCE_UP
,a.MTT_CAP_UP as FFQ_MONTANT_CAPITAL_DERNIERE_ECHEANCE_UP
,a.MTT_INT_UP as FFQ_MONTANT_INTERETS_DERNIERE_ECHEANCE_UP
,a.MTT_ASS_UP as FFQ_MONTANT_ASSURANCE_DERNIERE_ECHEANCE_UP
,a.NBR_MEN_REP_RES as FFQ_NOMBRE_MENSUALITES_REPORTEES
,a.TOP_MOD_RES_AUT as FFQ_TOP_MODIFICATION_RESERVE_AUTORISEE
,a.DTE_MOD_RES_AUT as FFQ_DATE_MODIFICATION_RESERVE_AUTORISEE
,a.NBR_IMP as FFQ_NOMBRE_ECHEANCES_IMPAYEES
,a.COD_SIT_DOS as FFQ_CODE_SITUATION_DOSSIER
,a.DTE_DER_MOD_CACH as FFQ_DATE_DERNIERE_MODIFICATION_POSITION_CASH
,a.COD_POS as FFQ_CODE_POSITION
,a.DTE_MOD_COD_POS as FFQ_DATE_DERNIERE_MODIFICATION_CODE_POSITION
,a.NBR_UP as FFQ_NOMBRE_UP

--donnees flux mensuel FF
,e.COD_BAN as FFM_DOMICILIATION_ETABLISSEMENT
,e.COD_GUI_GES as FFM_DOMICILIATION_GUICHET
,e.COD_DOM_CPT as FFM_DOMICILIATION_COMPTE
,e.DTE_OUV_CPT as FFM_DATE_OUVERTURE
,e.DEV_CPT as FFM_DEVISE
,e.MTT_CRE_MAX_AUT as FFM_MONTANT_MAXIMUM_AUTORISE_TOUTES_OPERATIONS
,e.MTT_CRE_MAX_AUT_VIR as FFM_MONTANT_MAXIMUM_AUTORISE_VIREMENTS
,e.MTT_CRE_DIS as FFM_MONTANT_DISPONIBLE_TOUTES_OPERATIONS
,e.MTT_CRE_UTI as FFM_MONTANT_UTILISE_TOUTES_OPERATIONS
,e.MTT_CRE_DIS_VIR as FFM_MONTANT_DISPONIBLE_VIREMENTS
,e.ENC_MOY_MOI as FFM_ENCOURS_MOYEN
,e.MTT_ECH_MOI as FFM_MONTANT_ECHEANCE
,e.TX_MOY as FFM_TAUX_MOYEN_HORS_ASSURANCE
,e.COD_STA_SUI_DOS as FFM_CODE_STATUT_SUIVI_DOSSIER
,e.DTE_FER_CPT as FFM_DATE_CLOTURE
,e.MTT_FRA as FFM_MONTANT_FRAIS
,e.NB_DEM_VIR as FFM_NOMBRE_DEMANDES_VIREMENT
,e.MTT_DEM_VIR as FFM_MONTANT_DEMANDES_VIREMENT
,e.NB_RET_GUI as FFM_NOMBRE_RETRAITS_GUICHET
,e.MTT_RET_GUI as FFM_MONTANT_RETRAITS_GUICHET
,e.NB_REM_ANT as FFM_NOMBRE_REMBOURSEMENTS_ANTICIPES
,e.MTT_REM_ANT as FFM_MONTANT_REMBOURSEMENTS_ANTICIPES
,e.MTT_COM as FFM_MONTANT_COMMISSIONS
,e.IDE_GRC as FFM_IDENTIFIANT_CRM
,e.PHA_PRO_REC as FFM_PHASE_RECOUVREMENT
,e.MTT_IMP as FFM_MONTANT_IMPAYES
,e.NB_IMP_NOU_GES as FFM_NOMBRE_IMPAYES
,e.DTE_PRE_IMP as FFM_DATE_1ER_IMPAYE

--donnees flux mensuel FF CTX
,d.IDE_GRC as CTX_IDENTIFIANT_CRM
,d.COD_BAN as CTX_CODE_BANQUE
,d.COD_GUI as CTX_CODE_GUICHET
,d.NUM_CPT as CTX_NUMERO_COMPTE
,d.DEV_CPT as CTX_DEVISE
,d.DTE_REM_CTX as CTX_DATE_REMISE_CONTENTIEUX
,d.CAP_DU_MEN_ECH as CTX_CAPITAL_DU_SUR_MENSUALITES_A_ECHOIR
,d.MTT_MEN_IMP as CTX_MONTANT_MENSUALITES_IMPAYEES
,d.INT_MEN_IMP as CTX_MONTANT_INTERETS_MENSUALITES_IMPAYEES
,d.IND_LEG as CTX_INDEMNITES_LEGALES
,d.SLD_AFF_CTX as CTX_SOLDE_AFFAIRE_CONTENTIEUSE
,d.DTE_DER_ECR_COM as CTX_DATE_DERNIERE_ECRITURE_COMPTABLE
,d.CUM_PER as CTX_CUMUL_PERTES
,d.COD_POS as CTX_CODE_POSITION

--donnees flux mensuel SAB ZDWHCPT0
,c.DWHCPTMTA as CPT_MONTANT_TOTAL_AUTORISE
,c.DWHCPTMTU as CPT_MONTANT_TOTAL_UTILISE

--donnees table CPT: caractéristiques du compte miroir
from [$(DataHubDatabaseName)].[dbo].[PV_SA_M_COMPTE_AS_OF_DATE](@date_analyse) c
--jointure avec table CPTBIS :  code produit CRM associé au compte miroir
left join [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_CPTBIS] w
       on c.DWHCPTCOM = w.CPTBISCOM
--jointure avec table PLAN_DE_COMPTE
left join [$(DataHubDatabaseName)].[dbo].[REF_PLAN_DE_COMPTES] y
       on c.DWHCPTRUB = y.PLAN_DE_COMPTES_COMPTE_OBLIGATOIRE
--jointure avec COMREFCOM : association reference FRANFINANCE et reference compte miroir 
left join [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_COM] b
       on  c.DWHCPTCOM = b.COMREFCOM
--jointure avec table quotidienne dernier jour du mois
left join [$(DataHubDatabaseName)].[dbo].[PV_FI_M_COMPL_Q_AS_OF_DATE](@date_analyse) a
	   on b.COMREFREF = a.NUM_PRE
--jointure avec les CTX: donnees dossiers en contentieux
left join [$(DataHubDatabaseName)].[dbo].[PV_FI_M_CTX_AS_OF_DATE] (@date_analyse) d
       on b.COMREFREF = d.NUM_PRE --and d.TYP_CON = 'REV'
--jointure avec table mensuelle: donnees complementaires flux mensuel
left join [$(DataHubDatabaseName)].[dbo].[PV_FI_M_COMPL_AS_OF_DATE](@date_analyse) e
       on b.COMREFREF = e.NUM_PRE 
where cast(y.PLAN_DE_COMPTES_CODE_PRODUIT as varchar(20)) in ('CPR','CPS') and b.COMREFREF is not NULL
) r

----------------------------------///////////////// Step 2
----------------------------------insertion dans la table cible 
insert  into dbo.BF_CREDIT_RENOUVELABLE
select
  NUMERO_PRET
, FFQ_NUMERO_PRET
, FFM_NUMERO_PRET
, FFM_DATE_TRAITEMENT
, CTX_NUMERO_PRET
, CTX_TYPE_FLUX
, CTX_TYPE_PRET
, FFQ_IDENTIFIANT_CRM_EMPRUNTEUR
, FFQ_IDENTIFIANT_CRM_COEMPRUNTEUR
, FFQ_TYPE_COMPLEO
, FFQ_DATE_OUVERTURE
, FFQ_DATE_1ERE_UTILISATION 
, FFQ_NUMERO_BAREME_VIGUEUR
, FFQ_VARIANTE_BAREME_VIGUEUR
, FFQ_GRILLE_TAUX_BAREME_VIGUEUR
, FFQ_GRILLE_MENSUALITE_BAREME_VIGUEUR
, FFQ_VITESSE_BAREME_VIGUEUR
, FFQ_DATE_FIN_BAREME_VIGUEUR
, FFQ_TYPE_BAREME
, FFQ_NUMERO_BAREME_STANDARD_FUTUR
, FFQ_VARIANTE_BAREME_STANDARD_FUTUR
, FFQ_GRILLE_TAUX_BAREME_STANDARD_FUTUR
, FFQ_GRILLE_MENSUALITE_BAREME_STANDARD_FUTUR
, FFQ_VITESSE_BAREME_STANDARD_FUTUR
, FFQ_CODE_BANQUE_COMPTE_PRELEVEMENT
, FFQ_CODE_GUICHET_COMPTE_PRELEVEMENT
, FFQ_NUMERO_COMPTE_PRELEVEMENT
, FFQ_CLE_RIB_COMPTE_PRELEVEMENT
, FFQ_LIBELLE_TITULAIRE_COMPTE
, FFQ_TOP_ASSURANCE
, FFQ_NATURE_ASSURANCE
, FFQ_MONTANT_TOTAL_AUTORISATION
, FFQ_MONTANT_DISPONIBLE
, FFQ_MONTANT_TOTAL_UTILISATION
, FFQ_MONTANT_UC
, FFQ_MONTANT_UP
, FFQ_DATE_DERNIERE_ECHEANCE_TRAITEE
, FFQ_MONTANT_TOTAL_DERNIERE_ECHEANCE_TRAITEE
, FFQ_MONTANT_DERNIERE_ECHEANCE_UC
, FFQ_MONTANT_CAPITAL_DERNIERE_ECHEANCE_UC
, FFQ_MONTANT_INTERETS_DERNIERE_ECHEANCE_UC
, FFQ_MONTANT_ASSURANCE_DERNIERE_ECHEANCE_UC
, FFQ_MONTANT_DERNIERE_ECHEANCE_UP
, FFQ_MONTANT_CAPITAL_DERNIERE_ECHEANCE_UP
, FFQ_MONTANT_INTERETS_DERNIERE_ECHEANCE_UP
, FFQ_MONTANT_ASSURANCE_DERNIERE_ECHEANCE_UP
, FFQ_NOMBRE_MENSUALITES_REPORTEES
, FFQ_TOP_MODIFICATION_RESERVE_AUTORISEE
, FFQ_DATE_MODIFICATION_RESERVE_AUTORISEE
, FFQ_NOMBRE_ECHEANCES_IMPAYEES
, FFQ_CODE_SITUATION_DOSSIER
, FFQ_DATE_DERNIERE_MODIFICATION_POSITION_CASH
, FFQ_CODE_POSITION
, FFQ_DATE_DERNIERE_MODIFICATION_CODE_POSITION
, FFQ_NOMBRE_UP
, FFM_DOMICILIATION_ETABLISSEMENT
, FFM_DOMICILIATION_GUICHET
, FFM_DOMICILIATION_COMPTE
, FFM_DATE_OUVERTURE
, FFM_DEVISE
, FFM_MONTANT_MAXIMUM_AUTORISE_TOUTES_OPERATIONS
, FFM_MONTANT_MAXIMUM_AUTORISE_VIREMENTS
, FFM_MONTANT_DISPONIBLE_TOUTES_OPERATIONS
, FFM_MONTANT_UTILISE_TOUTES_OPERATIONS
, FFM_MONTANT_DISPONIBLE_VIREMENTS
, FFM_ENCOURS_MOYEN
, FFM_MONTANT_ECHEANCE
, FFM_TAUX_MOYEN_HORS_ASSURANCE
, FFM_CODE_STATUT_SUIVI_DOSSIER
, FFM_DATE_CLOTURE
, FFM_MONTANT_FRAIS
, FFM_NOMBRE_DEMANDES_VIREMENT
, FFM_MONTANT_DEMANDES_VIREMENT
, FFM_NOMBRE_RETRAITS_GUICHET
, FFM_MONTANT_RETRAITS_GUICHET
, FFM_NOMBRE_REMBOURSEMENTS_ANTICIPES
, FFM_MONTANT_REMBOURSEMENTS_ANTICIPES
, FFM_MONTANT_COMMISSIONS
, FFM_IDENTIFIANT_CRM
, FFM_PHASE_RECOUVREMENT
, FFM_MONTANT_IMPAYES
, FFM_NOMBRE_IMPAYES
, FFM_DATE_1ER_IMPAYE
, CTX_IDENTIFIANT_CRM
, CTX_CODE_BANQUE
, CTX_CODE_GUICHET
, CTX_NUMERO_COMPTE
, CTX_DEVISE
, CTX_DATE_REMISE_CONTENTIEUX
, CTX_CAPITAL_DU_SUR_MENSUALITES_A_ECHOIR
, CTX_MONTANT_MENSUALITES_IMPAYEES
, CTX_MONTANT_INTERETS_MENSUALITES_IMPAYEES
, CTX_INDEMNITES_LEGALES
, CTX_SOLDE_AFFAIRE_CONTENTIEUSE
, CTX_DATE_DERNIERE_ECRITURE_COMPTABLE
, CTX_CUMUL_PERTES
, CTX_CODE_POSITION
, CPT_MONTANT_TOTAL_AUTORISE
, CPT_MONTANT_TOTAL_UTILISE
, @date_analyse as DATE_EXPLOITATION
, @date_action as DATE_ACTION

from #TEMPCR
       
END
