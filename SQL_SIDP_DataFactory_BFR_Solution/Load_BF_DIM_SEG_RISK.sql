﻿USE [$(DataFactoryBFRDatabaseName)]
GO

TRUNCATE TABLE [dbo].[BF_DIM_SEG_RISK] 
GO

INSERT INTO [dbo].[BF_DIM_SEG_RISK] ([LIB_SEG_RISK]) VALUES 
('RFO'),
('RMO'),
('RFA'),
('Autre')
GO