﻿USE [$(DataFactoryBFRDatabaseName)]
GO

TRUNCATE TABLE [dbo].[BF_DIM_COT_RISK_SF] 
GO

INSERT INTO [dbo].[BF_DIM_COT_RISK_SF] ([COTATION_RISQUE],[LIB_COT_RISK_SF]) VALUES 
('01','Oui'),
('02','Non')
GO