﻿USE [$(DataFactoryBFRDatabaseName)]
GO

TRUNCATE TABLE [dbo].[BF_DIM_CLASSE_RISK] 
GO

INSERT INTO [dbo].[BF_DIM_CLASSE_RISK] ( [CR],[CR_1],[CR_2])
 VALUES 
('Risque','Risque','Risque'),
('Autre','Autre','Autre'),
('.','Neutre','Neutre'),
('.','A','Bon'),
('.','B','Bon'),
('.','C','Bon'),
('.','D','Insuffisant'),
('.','E','Insuffisant'),
('.','F','Insuffisant')

GO