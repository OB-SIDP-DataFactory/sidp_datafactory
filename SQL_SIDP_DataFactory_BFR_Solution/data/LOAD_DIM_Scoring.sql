﻿USE [$(DatabaseName)]
GO 
TRUNCATE TABLE  [dbo].BF_DIM_CLASSE_RISK 
GO 

INSERT INTO [BF_DIM_CLASSE_RISK] ( [CR],[CR_1],[CR_2])
 VALUES 
('Risque','Risque','Risque'),
('Autre','Autre','Autre'),
('.','Neutre','Neutre'),
('.','A','Bon'),
('.','B','Bon'),
('.','C','Bon'),
('.','D','Insuffisant'),
('.','E','Insuffisant'),
('.','F','Insuffisant')
 GO
 TRUNCATE TABLE  [dbo].[BF_DIM_VERSION_SCORE]
 go
 INSERT INTO [BF_DIM_VERSION_SCORE] ([VERSION_SCORE]) VALUES 
(1)
GO 

TRUNCATE TABLE [dbo].[BF_DIM_COT_RISK_SF] 
GO
INSERT INTO [BF_DIM_COT_RISK_SF] ([COTATION_RISQUE],[LIB_COT_RISK_SF]) VALUES 
('01','Oui'),
('02','Non')
GO
TRUNCATE TABLE [dbo].[BF_DIM_SCORE_COMP] 
GO
INSERT INTO [dbo].[BF_DIM_SCORE_COMP] ([LIB_SCORE_COMP],COD_SCORE_COMP) VALUES 
('Insuffisant','INS'),
('Bon','BON'),
('Neutre','NEU'),
('Risque','RIS'),
('Autre','AUT')
GO
TRUNCATE TABLE [dbo].[BF_DIM_SCORE_COMP] 
GO
INSERT INTO [dbo].[BF_DIM_SCORE_COMP] ([LIB_SCORE_COMP],COD_SCORE_COMP) VALUES 
('Insuffisant','INS'),
('Bon','BON'),
('Neutre','NEU'),
('Risque','RIS'),
('Autre','AUT')
GO