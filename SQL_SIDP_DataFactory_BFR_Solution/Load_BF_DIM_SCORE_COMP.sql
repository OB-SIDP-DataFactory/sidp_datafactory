﻿USE [$(DataFactoryBFRDatabaseName)]
GO

TRUNCATE TABLE [dbo].[BF_DIM_SCORE_COMP] 
GO

INSERT INTO [dbo].[BF_DIM_SCORE_COMP] ([LIB_SCORE_COMP],COD_SCORE_COMP) VALUES 
('Insuffisant','INS'),
('Bon','BON'),
('Neutre','NEU'),
('Risque','RIS'),
('Autre','AUT')
GO
