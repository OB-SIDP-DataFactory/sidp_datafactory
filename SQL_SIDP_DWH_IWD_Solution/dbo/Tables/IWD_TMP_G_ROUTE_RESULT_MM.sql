﻿CREATE TABLE [dbo].[IWD_TMP_G_ROUTE_RESULT_MM] (
    [ID]                    NUMERIC (16)  NOT NULL,
    [CALLID]                VARCHAR (50)  NOT NULL,
    [CONNID]                VARCHAR (50)  NOT NULL,
    [PARTYID]               VARCHAR (50)  NOT NULL,
    [RTARGETOBJECTSELECTED] VARCHAR (255) NULL,
    [RTARGETTYPESELECTED]   INT           NULL,
    [RREQUESTEDSKILLCOMB]   VARCHAR (255) NULL,
    [RSTRATEGYNAME]         VARCHAR (255) NULL,
    [RTENANT]               VARCHAR (255) NULL,
    [CREATED]               DATETIME      NOT NULL,
    [CREATED_TS]            INT           NULL,
    [TERMINATED_TS]         INT           NULL,
    [GSYS_EXT_VCH1]         VARCHAR (255) NULL,
    [GSYS_EXT_VCH2]         VARCHAR (255) NULL
);

