﻿CREATE TABLE [dbo].[IWD_CTL_AUDIT_LOG] (
    [AUDIT_KEY]               NUMERIC (19) NOT NULL,
    [JOB_ID]                  VARCHAR (64) NOT NULL,
    [CREATED]                 DATETIME     NOT NULL,
    [INSERTED]                DATETIME     NULL,
    [PROCESSING_STATUS_KEY]   INT          NOT NULL,
    [MIN_START_DATE_TIME_KEY] INT          NULL,
    [MAX_START_DATE_TIME_KEY] INT          NULL,
    [MAX_CHUNK_TS]            INT          NULL,
    [DATA_SOURCE_KEY]         INT          NULL,
    [ROW_COUNT]               INT          NULL,
    [CREATED_TS]              INT          NOT NULL,
    CONSTRAINT [PK_CTL_AUDIT_LOG] PRIMARY KEY CLUSTERED ([AUDIT_KEY] ASC)
)
GO