﻿CREATE TABLE [dbo].[IWD_CTL_DS] (
    [DATA_SOURCE_KEY] INT         IDENTITY (1, 1) NOT NULL,
    [DS_DBID]         INT         NOT NULL,
    [DS_DBID_PRIM]    INT         NOT NULL,
    [DS2_DBID]        INT         NOT NULL,
    [DS_TYPE]         INT         NOT NULL,
    [ACTIVE_FLAG]     NUMERIC (1) DEFAULT ((1)) NOT NULL,
    [CREATED_TS]      INT         NOT NULL,
    [UPDATED_TS]      INT         NULL,
    CONSTRAINT [PK_CTL_DS] PRIMARY KEY CLUSTERED ([DATA_SOURCE_KEY] ASC)
);

