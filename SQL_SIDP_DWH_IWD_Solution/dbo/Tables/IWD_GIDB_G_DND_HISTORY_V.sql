﻿CREATE TABLE [dbo].[IWD_GIDB_G_DND_HISTORY_V] (
    [ID]               NUMERIC (16)  NOT NULL,
    [LOGINSESSIONID]   VARCHAR (50)  NOT NULL,
    [AGENTSTATE]       INT           NULL,
    [ENDPOINTID]       INT           NOT NULL,
    [STATE]            INT           NOT NULL,
    [PREVSTATE]        INT           NULL,
    [PREVSENTER]       DATETIME      NULL,
    [PREVSENTER_TS]    INT           NULL,
    [ADDED]            DATETIME      NOT NULL,
    [ADDED_TS]         INT           NULL,
    [GSYS_DOMAIN]      INT           NULL,
    [GSYS_SYS_ID]      INT           NULL,
    [GSYS_SEQ]         BIGINT        NULL,
    [GSYS_EXT_VCH1]    VARCHAR (255) NULL,
    [GSYS_EXT_VCH2]    VARCHAR (255) NULL,
    [GSYS_EXT_INT1]    INT           NULL,
    [GSYS_EXT_INT2]    INT           NULL,
    [CREATE_AUDIT_KEY] NUMERIC (19)  NOT NULL
);

