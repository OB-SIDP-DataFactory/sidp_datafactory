﻿CREATE TABLE [dbo].[IWD_SM_RES_SESSION_FACT] (
    [SM_RES_SESSION_FACT_KEY]        NUMERIC (19) NOT NULL,
    [START_DATE_TIME_KEY]            INT          NOT NULL,
    [END_DATE_TIME_KEY]              INT          NOT NULL,
    [TENANT_KEY]                     INT          NOT NULL,
    [MEDIA_TYPE_KEY]                 INT          NOT NULL,
    [RESOURCE_KEY]                   INT          NOT NULL,
    [RESOURCE_GROUP_COMBINATION_KEY] INT          NOT NULL,
    [CREATE_AUDIT_KEY]               NUMERIC (19) NOT NULL,
    [UPDATE_AUDIT_KEY]               NUMERIC (19) NOT NULL,
    [START_TS]                       INT          NULL,
    [END_TS]                         INT          NULL,
    [TOTAL_DURATION]                 INT          NULL,
    [LEAD_CLIP_DURATION]             INT          NULL,
    [TRAIL_CLIP_DURATION]            INT          NULL,
    [ACTIVE_FLAG]                    NUMERIC (1)  NULL,
    [PURGE_FLAG]                     NUMERIC (1)  NULL,
    CONSTRAINT [PK_SM_RS_SSSN_FT] PRIMARY KEY CLUSTERED ([SM_RES_SESSION_FACT_KEY] ASC)
);


GO