﻿CREATE TABLE [dbo].[IWD_GIDB_GO_CHAIN] (
    [ID]               NUMERIC (16)  NOT NULL,
    [CHAINGUID]        VARCHAR (64)  NOT NULL,
    [PARENTGUID]       VARCHAR (64)  NULL,
    [SESSID]           VARCHAR (64)  NOT NULL,
    [CAMPAIGNID]       INT           NOT NULL,
    [GROUPID]          INT           NOT NULL,
    [CALLINGLISTID]    INT           NOT NULL,
    [OCSID]            INT           NOT NULL,
    [CPDID]            INT           NULL,
    [RECORDHANDLE]     INT           NULL,
    [STATE]            INT           NOT NULL,
    [LASTCALLATTID]    VARCHAR (64)  NULL,
    [SWITCHID]         INT           NULL,
    [CPDRESULT]        INT           NULL,
    [CALLRESULT]       INT           NULL,
    [CREATED]          DATETIME      NOT NULL,
    [CREATED_TS]       INT           NOT NULL,
    [TERMINATED]       DATETIME      NULL,
    [TERMINATED_TS]    INT           NULL,
    [LASTCHANGED]      DATETIME      NOT NULL,
    [LASTCHANGED_TS]   INT           NULL,
    [INTERNALREASON]   INT           NULL,
    [GSYS_DOMAIN]      INT           NULL,
    [GSYS_SYS_ID]      INT           NULL,
    [GSYS_EXT_VCH1]    VARCHAR (255) NULL,
    [GSYS_EXT_VCH2]    VARCHAR (255) NULL,
    [GSYS_EXT_INT1]    INT           NULL,
    [GSYS_EXT_INT2]    INT           NULL,
    [CREATE_AUDIT_KEY] NUMERIC (19)  NOT NULL,
    CONSTRAINT [PK_GO_CHAIN] PRIMARY KEY CLUSTERED ([CHAINGUID] ASC)
);

