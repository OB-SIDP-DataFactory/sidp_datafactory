﻿CREATE TABLE [dbo].[IWD_CTL_PURGE_HISTORY] (
    [JOB_ID]           VARCHAR (64)  NOT NULL,
    [JOB_VERSION]      VARCHAR (64)  NULL,
    [TABLE_NAME]       VARCHAR (255) NOT NULL,
    [PURGE_MAX_TIME]   DATETIME      NULL,
    [PURGE_MAX_TS]     INT           NOT NULL,
    [PURGE_START_TIME] DATETIME      NULL,
    [PURGE_END_TIME]   DATETIME      NULL,
    [ROW_COUNT]        INT           NULL,
    [CREATED_TS]       INT           NOT NULL
);

