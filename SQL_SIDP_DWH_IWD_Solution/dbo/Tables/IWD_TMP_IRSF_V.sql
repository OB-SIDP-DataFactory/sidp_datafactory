﻿CREATE TABLE [dbo].[IWD_TMP_IRSF_V] (
    [START_DATE_TIME_KEY]            INT           NOT NULL,
    [END_DATE_TIME_KEY]              INT           NOT NULL,
    [TENANT_KEY]                     INT           NOT NULL,
    [MEDIA_TYPE_KEY]                 INT           NOT NULL,
    [RESOURCE_KEY]                   INT           NOT NULL,
    [MEDIA_RESOURCE_KEY]             INT           NOT NULL,
    [PLACE_KEY]                      INT           NOT NULL,
    [INTERACTION_RESOURCE_STATE_KEY] INT           NOT NULL,
    [INTERACTION_TYPE_KEY]           INT           NOT NULL,
    [INTERACTION_RESOURCE_SDT_KEY]   INT           DEFAULT ((-2)) NULL,
    [INTERACTION_RESOURCE_ID]        NUMERIC (19)  NULL,
    [START_TS]                       INT           NULL,
    [END_TS]                         INT           NULL,
    [TOTAL_DURATION]                 INT           NULL,
    [LEAD_CLIP_DURATION]             INT           NULL,
    [TRAIL_CLIP_DURATION]            INT           NULL,
    [TARGET_ADDRESS]                 VARCHAR (255) NULL
);

