﻿CREATE TABLE [dbo].[IWD_INTERACTION_RESOURCE_FACT] (
    [INTERACTION_RESOURCE_ID]        NUMERIC (19) NOT NULL,
    [TENANT_KEY]                     INT          NOT NULL,
    [INTERACTION_TYPE_KEY]           INT          NOT NULL,
    [MEDIA_TYPE_KEY]                 INT          NOT NULL,
    [TECHNICAL_DESCRIPTOR_KEY]       INT          NOT NULL,
    [MEDIA_RESOURCE_KEY]             INT          NOT NULL,
    [RESOURCE_GROUP_COMBINATION_KEY] INT          NOT NULL,
    [PLACE_KEY]                      INT          NOT NULL,
    [STRATEGY_KEY]                   INT          NOT NULL,
    [ROUTING_TARGET_KEY]             INT          NOT NULL,
    [REQUESTED_SKILL_KEY]            INT          NOT NULL,
    [INTERACTION_SDT_KEY]            INT          NULL,
    [INTERACTION_ID]                 NUMERIC (19) NOT NULL,
    [RES_PREVIOUS_SM_STATE_KEY]      INT          NOT NULL,
    [RES_PREV_SM_STATE_FACT_SDT_KEY] INT          NULL,
    [RES_PREVIOUS_SM_STATE_FACT_KEY] NUMERIC (19) NULL,
    [RESOURCE_KEY]                   INT          NOT NULL,
    [LAST_RP_RESOURCE_KEY]           INT          NOT NULL,
    [LAST_QUEUE_RESOURCE_KEY]        INT          NOT NULL,
    [LAST_VQUEUE_RESOURCE_KEY]       INT          NOT NULL,
    [LAST_IVR_RESOURCE_KEY]          INT          NOT NULL,
    [PREV_IRF_SDT_KEY]               INT          NULL,
    [PREV_IRF_ID]                    NUMERIC (19) NULL,
    [MEDIATION_SEGMENT_SDT_KEY]      INT          NULL,
    [MEDIATION_SEGMENT_ID]           NUMERIC (19) NULL,
    [MEDIATION_RESOURCE_KEY]         INT          NOT NULL,
    [MEDIATION_START_DATE_TIME_KEY]  INT          NULL,
    [INTERACTION_RESOURCE_ORDINAL]   SMALLINT     NULL,
    [IRF_ANCHOR]                     NUMERIC (1)  NULL,
    [IRF_ANCHOR_TS]                  INT          NULL,
    [ANCHOR_FLAGS_KEY]               INT          NULL,
    [LAST_INTERACTION_RESOURCE]      NUMERIC (1)  NULL,
    [LAST_MEDIATION_SEGMENT_SDT_KEY] INT          NULL,
    [LAST_MEDIATION_SEGMENT_ID]      NUMERIC (19) NULL,
    [RECEIVED_FROM_IXN_RES_SDT_KEY]  INT          NULL,
    [RECEIVED_FROM_IXN_RESOURCE_ID]  NUMERIC (19) NULL,
    [PARTYGUID]                      VARCHAR (50) NULL,
    [LEAD_CLIP_DURATION]             INT          NULL,
    [TRAIL_CLIP_DURATION]            INT          NULL,
    [ROUTING_POINT_DURATION]         INT          NULL,
    [QUEUE_DURATION]                 INT          NULL,
    [IVR_PORT_DURATION]              INT          NULL,
    [HANDLE_COUNT]                   SMALLINT     NULL,
    [CUSTOMER_HANDLE_COUNT]          SMALLINT     NULL,
    [PREVIOUS_MEDIATION_DURATION]    INT          NULL,
    [MEDIATION_DURATION]             INT          NULL,
    [MEDIATION_COUNT]                SMALLINT     NULL,
    [MET_SERVICE_OBJECTIVE_FLAG]     NUMERIC (1)  NULL,
    [SHORT_ABANDONED_FLAG]           NUMERIC (1)  NULL,
    [STOP_ACTION]                    NUMERIC (1)  NULL,
    [DIAL_COUNT]                     SMALLINT     NULL,
    [DIAL_DURATION]                  INT          NULL,
    [RING_COUNT]                     SMALLINT     NULL,
    [RING_DURATION]                  INT          NULL,
    [TALK_COUNT]                     SMALLINT     NULL,
    [TALK_DURATION]                  INT          NULL,
    [HOLD_COUNT]                     SMALLINT     NULL,
    [HOLD_DURATION]                  INT          NULL,
    [AFTER_CALL_WORK_COUNT]          SMALLINT     NULL,
    [AFTER_CALL_WORK_DURATION]       INT          NULL,
    [CUSTOMER_DIAL_COUNT]            SMALLINT     NULL,
    [CUSTOMER_DIAL_DURATION]         INT          NULL,
    [CUSTOMER_RING_COUNT]            SMALLINT     NULL,
    [CUSTOMER_RING_DURATION]         INT          NULL,
    [CUSTOMER_TALK_COUNT]            SMALLINT     NULL,
    [CUSTOMER_TALK_DURATION]         INT          NULL,
    [CUSTOMER_HOLD_COUNT]            SMALLINT     NULL,
    [CUSTOMER_HOLD_DURATION]         INT          NULL,
    [CUSTOMER_ACW_COUNT]             SMALLINT     NULL,
    [CUSTOMER_ACW_DURATION]          INT          NULL,
    [POST_CONS_XFER_TALK_COUNT]      SMALLINT     NULL,
    [POST_CONS_XFER_TALK_DURATION]   INT          NULL,
    [POST_CONS_XFER_HOLD_COUNT]      SMALLINT     NULL,
    [POST_CONS_XFER_HOLD_DURATION]   INT          NULL,
    [POST_CONS_XFER_RING_COUNT]      SMALLINT     NULL,
    [POST_CONS_XFER_RING_DURATION]   INT          NULL,
    [CONF_INIT_TALK_COUNT]           SMALLINT     NULL,
    [CONF_INIT_TALK_DURATION]        INT          NULL,
    [CONF_INIT_HOLD_COUNT]           SMALLINT     NULL,
    [CONF_INIT_HOLD_DURATION]        INT          NULL,
    [CONF_JOIN_RING_COUNT]           SMALLINT     NULL,
    [CONF_JOIN_RING_DURATION]        INT          NULL,
    [CONF_JOIN_TALK_COUNT]           SMALLINT     NULL,
    [CONF_JOIN_TALK_DURATION]        INT          NULL,
    [CONF_JOIN_HOLD_COUNT]           SMALLINT     NULL,
    [CONF_JOIN_HOLD_DURATION]        INT          NULL,
    [CONFERENCE_INITIATED_COUNT]     SMALLINT     NULL,
    [CONS_INIT_DIAL_COUNT]           SMALLINT     NULL,
    [CONS_INIT_DIAL_DURATION]        INT          NULL,
    [CONS_INIT_TALK_COUNT]           SMALLINT     NULL,
    [CONS_INIT_TALK_DURATION]        INT          NULL,
    [CONS_INIT_HOLD_COUNT]           SMALLINT     NULL,
    [CONS_INIT_HOLD_DURATION]        INT          NULL,
    [CONS_RCV_RING_COUNT]            SMALLINT     NULL,
    [CONS_RCV_RING_DURATION]         INT          NULL,
    [CONS_RCV_TALK_COUNT]            SMALLINT     NULL,
    [CONS_RCV_TALK_DURATION]         INT          NULL,
    [CONS_RCV_HOLD_COUNT]            SMALLINT     NULL,
    [CONS_RCV_HOLD_DURATION]         INT          NULL,
    [CONS_RCV_ACW_COUNT]             SMALLINT     NULL,
    [CONS_RCV_ACW_DURATION]          INT          NULL,
    [AGENT_TO_AGENT_CONS_COUNT]      SMALLINT     NULL,
    [AGENT_TO_AGENT_CONS_DURATION]   INT          NULL,
    [FOCUS_TIME_COUNT]               SMALLINT     NULL,
    [FOCUS_TIME_DURATION]            INT          NULL,
    [ASM_COUNT]                      SMALLINT     NULL,
    [ASM_ENGAGE_DURATION]            INT          NULL,
    [CREATE_AUDIT_KEY]               NUMERIC (19) NOT NULL,
    [UPDATE_AUDIT_KEY]               NUMERIC (19) NOT NULL,
    [START_DATE_TIME_KEY]            INT          NOT NULL,
    [END_DATE_TIME_KEY]              INT          NULL,
    [START_TS]                       INT          NULL,
    [END_TS]                         INT          NULL,
    [ACTIVE_FLAG]                    NUMERIC (1)  NULL,
    [PURGE_FLAG]                     NUMERIC (1)  NULL,
    CONSTRAINT [PK_INTRCTN_RC_FT] PRIMARY KEY CLUSTERED ([INTERACTION_RESOURCE_ID] ASC)
);


GO

CREATE NONCLUSTERED INDEX [IX_NC_INTERACTION_RESOURCE]
ON [dbo].[IWD_INTERACTION_RESOURCE_FACT] ([RESOURCE_KEY],[START_DATE_TIME_KEY])
INCLUDE ([INTERACTION_TYPE_KEY],[MEDIA_TYPE_KEY],[TALK_DURATION])