﻿CREATE TABLE [dbo].[IWD_TMP_IRF_ACW] (
    [INTERACTION_RESOURCE_ID] NUMERIC (19) NOT NULL,
    [CONS_RCV_ACW_DURATION]   INT          NULL,
    [IRF_START_DT_KEY]        INT          NOT NULL,
    [START_TS]                INT          NULL,
    [END_TS]                  INT          NULL
);

