﻿CREATE TABLE [dbo].[IWD_GIDB_G_CALL_HISTORY_MM] (
    [CHID]             NUMERIC (16)  NOT NULL,
    [CALLID]           VARCHAR (50)  NOT NULL,
    [CSEQ]             INT           NOT NULL,
    [CHANGETYPE]       INT           NULL,
    [REFID]            VARCHAR (50)  NULL,
    [ADDED]            DATETIME      NOT NULL,
    [ADDED_TS]         INT           NULL,
    [GSYS_DOMAIN]      INT           NULL,
    [GSYS_SYS_ID]      INT           NULL,
    [GSYS_EXT_VCH1]    VARCHAR (255) NULL,
    [GSYS_EXT_VCH2]    VARCHAR (255) NULL,
    [GSYS_EXT_INT1]    INT           NULL,
    [GSYS_EXT_INT2]    INT           NULL,
    [CREATE_AUDIT_KEY] NUMERIC (19)  NOT NULL
);

