﻿CREATE TABLE [dbo].[IWD_CTL_EXTRACT_METRICS] (
    [LAST_CFG_EXTRACT_TS] INT NOT NULL,
    CONSTRAINT [PK_CTL_EXTR_MTRCS] PRIMARY KEY CLUSTERED ([LAST_CFG_EXTRACT_TS] ASC)
);

