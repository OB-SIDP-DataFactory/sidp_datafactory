﻿CREATE TABLE [dbo].[IWD_SM_RES_STATE_REASON_FACT_V2] (
    [SM_RES_STATE_REASON_FACT_KEY]   NUMERIC (19) NOT NULL,
    [TENANT_KEY]                     INT          NOT NULL,
    [CREATE_AUDIT_KEY]               NUMERIC (19) NOT NULL,
    [UPDATE_AUDIT_KEY]               NUMERIC (19) NOT NULL,
    [START_DATE_TIME_KEY]            INT          NOT NULL,
    [END_DATE_TIME_KEY]              INT          NOT NULL,
    [RESOURCE_STATE_KEY]             INT          NOT NULL,
    [RESOURCE_STATE_REASON_KEY]      INT          NOT NULL,
    [MEDIA_TYPE_KEY]                 INT          NOT NULL,
    [RESOURCE_KEY]                   INT          NOT NULL,
    [RESOURCE_GROUP_COMBINATION_KEY] INT          NOT NULL,
    [SM_RES_SESSION_FACT_SDT_KEY]    INT          NULL,
    [SM_RES_SESSION_FACT_KEY]        NUMERIC (19) NULL,
    [SM_RES_STATE_FACT_SDT_KEY]      INT          NULL,
    [SM_RES_STATE_FACT_KEY]          NUMERIC (19) NOT NULL,
    [START_TS]                       INT          NULL,
    [END_TS]                         INT          NULL,
    [TOTAL_DURATION]                 INT          NULL,
    [LEAD_CLIP_DURATION]             INT          NULL,
    [TRAIL_CLIP_DURATION]            INT          NULL,
    [ACTIVE_FLAG]                    NUMERIC (1)  NULL,
    [PURGE_FLAG]                     NUMERIC (1)  NULL,
    [REASON_VALUE]                   VARCHAR (50) NULL,
    CONSTRAINT [PK_SM_RS_ST_RSN_FT_BIS] PRIMARY KEY CLUSTERED ([SM_RES_STATE_REASON_FACT_KEY] ASC)
);


GO

CREATE INDEX [IX_IWD_SM_RES_STATE_REASON_FACT_V2_Column] ON [dbo].[IWD_SM_RES_STATE_REASON_FACT_V2]
(
START_DATE_TIME_KEY ASC,
RESOURCE_KEY ASC,
RESOURCE_STATE_REASON_KEY ASC,
RESOURCE_STATE_KEY ASC
)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
