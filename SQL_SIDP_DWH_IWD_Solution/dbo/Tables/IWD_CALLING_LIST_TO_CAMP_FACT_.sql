﻿CREATE TABLE [dbo].[IWD_CALLING_LIST_TO_CAMP_FACT_] (
    [CALLING_LIST_TO_CAMP_FACT_KEY] NUMERIC (19) NOT NULL,
    [CALLING_LIST_KEY]              INT          NOT NULL,
    [CAMPAIGN_KEY]                  INT          NOT NULL,
    [TENANT_KEY]                    INT          NOT NULL,
    [START_DATE_TIME_KEY]           INT          NOT NULL,
    [END_DATE_TIME_KEY]             INT          NOT NULL,
    [CREATE_AUDIT_KEY]              NUMERIC (19) NOT NULL,
    [UPDATE_AUDIT_KEY]              NUMERIC (19) NOT NULL,
    [START_TS]                      INT          NULL,
    [END_TS]                        INT          NULL,
    [IDB_ID]                        NUMERIC (19) NOT NULL,
    [ACTIVE_FLAG]                   NUMERIC (1)  NULL,
    [PURGE_FLAG]                    NUMERIC (1)  NULL,
    CONSTRAINT [PK_CLNGLTTCMPFT] PRIMARY KEY CLUSTERED ([CALLING_LIST_TO_CAMP_FACT_KEY] ASC)
);

