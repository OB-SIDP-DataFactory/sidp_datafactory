﻿CREATE TABLE [dbo].[IWD_TMP_GCX_OBJTABLE_RECORD] (
    [ID]               NUMERIC (16)  NOT NULL,
    [OBJTABLEID]       INT           NOT NULL,
    [MEDIATYPEID]      INT           NOT NULL,
    [SERVICETYPEID]    INT           NOT NULL,
    [CUSTSEGMENTID]    INT           NOT NULL,
    [THRESHOLD]        INT           NULL,
    [DELTA]            INT           NULL,
    [STATUS]           INT           NOT NULL,
    [CREATED]          DATETIME      NULL,
    [DELETED]          DATETIME      NULL,
    [CREATED_TS]       INT           NULL,
    [DELETED_TS]       INT           NULL,
    [GSYS_DOMAIN]      INT           NULL,
    [GSYS_SYS_ID]      INT           NULL,
    [GSYS_EXT_VCH2]    VARCHAR (255) NULL,
    [GSYS_EXT_INT1]    INT           NULL,
    [GSYS_EXT_INT2]    INT           NULL,
    [UPDATE_AUDIT_KEY] NUMERIC (19)  NOT NULL,
    [CREATE_AUDIT_KEY] NUMERIC (19)  NOT NULL
);

