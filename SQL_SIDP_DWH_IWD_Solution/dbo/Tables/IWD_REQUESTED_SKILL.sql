﻿CREATE TABLE [dbo].[IWD_REQUESTED_SKILL] (
    [ID]                    NUMERIC (19) NOT NULL,
    [SKILL_KEY]             INT          NOT NULL,
    [TENANT_KEY]            INT          NOT NULL,
    [SKILL_COMBINATION_KEY] INT          NOT NULL,
    [CREATE_AUDIT_KEY]      NUMERIC (19) NOT NULL,
    [UPDATE_AUDIT_KEY]      NUMERIC (19) NOT NULL,
    [SKILL_LEVEL]           INT          NULL,
    [PURGE_FLAG]            NUMERIC (1)  NULL,
    CONSTRAINT [PK_REQUESTED_SKILL] PRIMARY KEY CLUSTERED ([ID] ASC)
);

