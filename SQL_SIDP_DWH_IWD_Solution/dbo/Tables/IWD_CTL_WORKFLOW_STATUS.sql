﻿CREATE TABLE [dbo].[IWD_CTL_WORKFLOW_STATUS] (
    [WORKFLOW_TYPE]           VARCHAR (32)  NOT NULL,
    [JOB_NAME]                VARCHAR (32)  NOT NULL,
    [JOB_VERSION]             VARCHAR (32)  NULL,
    [START_TIME]              DATETIME      NULL,
    [END_TIME]                DATETIME      NULL,
    [START_UTC]               INT           NULL,
    [END_UTC]                 INT           NULL,
    [STATUS]                  VARCHAR (32)  NOT NULL,
    [LAST_STATUS_UPDATE_UTC]  INT           NULL,
    [LAST_STATUS_UPDATE_TIME] DATETIME      NULL,
    [JOB_ID]                  VARCHAR (512) NULL,
    CONSTRAINT [PK_CTL_WFLOW_ST] PRIMARY KEY CLUSTERED ([WORKFLOW_TYPE] ASC, [JOB_NAME] ASC)
);

