﻿CREATE TABLE [dbo].[IWD_STG_TRANSFORM_DISCARDS] (
    [TABLE_NAME]        VARCHAR (30)  NOT NULL,
    [INTERACTION_ID]    NUMERIC (19)  DEFAULT ((-2)) NOT NULL,
    [GUID]              VARCHAR (50)  NULL,
    [CREATE_AUDIT_KEY]  NUMERIC (19)  NOT NULL,
    [CODE]              INT           NOT NULL,
    [REASON]            VARCHAR (255) NOT NULL,
    [ETL_TS]            INT           NOT NULL,
    [ETL_DATE_TIME_KEY] INT           NOT NULL
);

