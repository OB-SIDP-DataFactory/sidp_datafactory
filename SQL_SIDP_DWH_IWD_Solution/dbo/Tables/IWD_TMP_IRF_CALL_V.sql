﻿CREATE TABLE [dbo].[IWD_TMP_IRF_CALL_V] (
    [INTERACTION_RESOURCE_ID] NUMERIC (19) NOT NULL,
    [CALLID]                  VARCHAR (50) NOT NULL,
    [START_TS]                INT          NOT NULL,
    [END_TS]                  INT          NOT NULL,
    [CSEQ]                    INT          NOT NULL,
    [ORDINAL]                 INT          NOT NULL
);

