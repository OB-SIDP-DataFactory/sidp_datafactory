﻿CREATE TABLE [dbo].[IWD_GIDB_G_IS_LINK_V] (
    [ID]               NUMERIC (16)  NOT NULL,
    [LINKID]           VARCHAR (50)  NOT NULL,
    [CALLID]           VARCHAR (50)  NOT NULL,
    [ISLINKTYPE]       INT           NOT NULL,
    [REMOTELOCATION]   VARCHAR (255) NOT NULL,
    [STATE]            INT           NOT NULL,
    [MERGESTATE]       INT           NULL,
    [INITIATED]        DATETIME      NOT NULL,
    [INITIATED_TS]     INT           NOT NULL,
    [TERMINATED]       DATETIME      NULL,
    [TERMINATED_TS]    INT           NULL,
    [LASTCHANGE]       DATETIME      NOT NULL,
    [LASTCHANGE_TS]    INT           NULL,
    [GSYS_DOMAIN]      INT           NULL,
    [GSYS_SYS_ID]      INT           NULL,
    [GSYS_EXT_VCH1]    VARCHAR (255) NULL,
    [GSYS_EXT_VCH2]    VARCHAR (255) NULL,
    [GSYS_EXT_INT1]    INT           NULL,
    [GSYS_EXT_INT2]    INT           NULL,
    [CREATE_AUDIT_KEY] NUMERIC (19)  NOT NULL,
    [UPDATE_AUDIT_KEY] NUMERIC (19)  NOT NULL,
    CONSTRAINT [PK_GIDB_G_IS_LINK_V] PRIMARY KEY CLUSTERED ([LINKID] ASC, [ISLINKTYPE] ASC)
);

