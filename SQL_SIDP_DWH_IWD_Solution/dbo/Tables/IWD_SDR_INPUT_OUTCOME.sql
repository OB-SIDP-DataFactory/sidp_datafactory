﻿CREATE TABLE [dbo].[IWD_SDR_INPUT_OUTCOME] (
    [ID]               INT           IDENTITY (1, 1) NOT NULL,
    [CREATE_AUDIT_KEY] NUMERIC (19)  NOT NULL,
    [SELECTED_OPTION]  VARCHAR (255) DEFAULT ('NO_VALUE') NOT NULL,
    [NO_INPUT_COUNT]   INT           DEFAULT ((0)) NOT NULL,
    [NO_MATCH_COUNT]   INT           DEFAULT ((0)) NOT NULL,
    [STRIKEOUT]        VARCHAR (10)  DEFAULT ('False') NOT NULL,
    [SUCCESS]          VARCHAR (10)  DEFAULT ('True') NOT NULL,
    CONSTRAINT [PK_SDR_INPUT_OUTCOME] PRIMARY KEY CLUSTERED ([ID] ASC)
);

