﻿CREATE TABLE [dbo].[IWD_CTL_EXTRACT_HISTORY] (
    [TABLE_NAME]         VARCHAR (255) NOT NULL,
    [DATA_SOURCE_KEY]    INT           NOT NULL,
    [DATA_SOURCE_TYPE]   INT           NULL,
    [ROW_COUNT]          INT           NULL,
    [MAX_TIME]           DATETIME      NULL,
    [MAX_TS]             INT           NULL,
    [ICON_DBID]          INT           DEFAULT ((0)) NOT NULL,
    [DSS_ID]             INT           NULL,
    [PROVIDERTAG]        INT           NULL,
    [EXTRACT_START_TIME] DATETIME      NULL,
    [EXTRACT_END_TIME]   DATETIME      NULL,
    [JOB_ID]             VARCHAR (64)  NOT NULL,
    [JOB_NAME]           VARCHAR (32)  NULL,
    [JOB_VERSION]        VARCHAR (64)  NULL,
    [DAP_NAME]           VARCHAR (255) NULL,
    [CREATE_AUDIT_KEY]   NUMERIC (19)  NOT NULL,
    [CREATED_TS]         INT           NOT NULL
);

