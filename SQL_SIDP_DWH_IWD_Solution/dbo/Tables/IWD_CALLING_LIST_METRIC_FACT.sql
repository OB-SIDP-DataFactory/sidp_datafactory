﻿CREATE TABLE [dbo].[IWD_CALLING_LIST_METRIC_FACT] (
    [CALLING_LIST_METRIC_FACT_KEY] NUMERIC (19) NOT NULL,
    [TENANT_KEY]                   INT          NOT NULL,
    [CREATE_AUDIT_KEY]             NUMERIC (19) NOT NULL,
    [UPDATE_AUDIT_KEY]             NUMERIC (19) NOT NULL,
    [CAMPAIGN_KEY]                 INT          NOT NULL,
    [CALLING_LIST_KEY]             INT          NOT NULL,
    [START_DATE_TIME_KEY]          INT          NOT NULL,
    [CAMP_GROUP_SESS_FACT_SDT_KEY] INT          NULL,
    [CAMP_GROUP_SESSION_FACT_KEY]  NUMERIC (19) NULL,
    [GMT_TS]                       INT          NULL,
    [TOTAL_RECORDS]                INT          NULL,
    [NOT_PROCESSED_RECORDS]        INT          NULL,
    [TOTAL_CONTACTS]               INT          NULL,
    [NOT_PROCESSED_CONTACTS]       INT          NULL,
    [ACTIVE_FLAG]                  NUMERIC (1)  NULL,
    [PURGE_FLAG]                   NUMERIC (1)  NULL,
    CONSTRAINT [PK_CALL_LST_MT_FT] PRIMARY KEY CLUSTERED ([CALLING_LIST_METRIC_FACT_KEY] ASC)
);

