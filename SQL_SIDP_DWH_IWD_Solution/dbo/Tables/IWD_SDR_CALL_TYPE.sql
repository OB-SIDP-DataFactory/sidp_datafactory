﻿CREATE TABLE [dbo].[IWD_SDR_CALL_TYPE] (
    [ID]               INT           IDENTITY (1, 1) NOT NULL,
    [CREATE_AUDIT_KEY] NUMERIC (19)  NOT NULL,
    [CALL_TYPE]        VARCHAR (255) DEFAULT ('NO_VALUE') NOT NULL,
    CONSTRAINT [PK_SDR_CALL_TYPE] PRIMARY KEY CLUSTERED ([ID] ASC)
);

