﻿CREATE TABLE [dbo].[IWD_SDR_SURVEY_I2] (
    [ID]               INT          IDENTITY (1, 1) NOT NULL,
    [CREATE_AUDIT_KEY] NUMERIC (19) NOT NULL,
    [IQ6]              INT          DEFAULT ((-1)) NOT NULL,
    [IQ7]              INT          DEFAULT ((-1)) NOT NULL,
    [IQ8]              INT          DEFAULT ((-1)) NOT NULL,
    [IQ9]              INT          DEFAULT ((-1)) NOT NULL,
    [IQ10]             INT          DEFAULT ((-1)) NOT NULL,
    CONSTRAINT [PK_SDR_SURVEY_I2] PRIMARY KEY CLUSTERED ([ID] ASC)
);

