﻿CREATE TABLE [dbo].[IWD_SDR_SURVEY_I1] (
    [ID]               INT          IDENTITY (1, 1) NOT NULL,
    [CREATE_AUDIT_KEY] NUMERIC (19) NOT NULL,
    [IQ1]              INT          DEFAULT ((-1)) NOT NULL,
    [IQ2]              INT          DEFAULT ((-1)) NOT NULL,
    [IQ3]              INT          DEFAULT ((-1)) NOT NULL,
    [IQ4]              INT          DEFAULT ((-1)) NOT NULL,
    [IQ5]              INT          DEFAULT ((-1)) NOT NULL,
    CONSTRAINT [PK_SDR_SURVEY_I1] PRIMARY KEY CLUSTERED ([ID] ASC)
);

