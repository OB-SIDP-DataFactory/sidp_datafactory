﻿CREATE TABLE [dbo].[IWD_GIDB_GCX_FORMAT_FIELD] (
    [ID]               NUMERIC (16)  NOT NULL,
    [FORMATID]         INT           NOT NULL,
    [FIELDID]          INT           NOT NULL,
    [STATUS]           INT           NOT NULL,
    [CREATED]          DATETIME      NULL,
    [DELETED]          DATETIME      NULL,
    [CREATED_TS]       INT           NOT NULL,
    [DELETED_TS]       INT           NULL,
    [GSYS_DOMAIN]      INT           NULL,
    [GSYS_SYS_ID]      INT           NULL,
    [GSYS_EXT_VCH1]    VARCHAR (255) NULL,
    [GSYS_EXT_VCH2]    VARCHAR (255) NULL,
    [GSYS_EXT_INT1]    INT           NULL,
    [GSYS_EXT_INT2]    INT           NULL,
    [CREATE_AUDIT_KEY] NUMERIC (19)  NOT NULL,
    [UPDATE_AUDIT_KEY] NUMERIC (19)  NOT NULL,
    CONSTRAINT [PK_GCX_FRMT_FLD] PRIMARY KEY CLUSTERED ([FORMATID] ASC, [FIELDID] ASC, [CREATED_TS] ASC)
);

