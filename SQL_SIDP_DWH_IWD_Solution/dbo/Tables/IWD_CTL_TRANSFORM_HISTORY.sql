﻿CREATE TABLE [dbo].[IWD_CTL_TRANSFORM_HISTORY] (
    [JOB_ID]               VARCHAR (64)  NOT NULL,
    [JOB_VERSION]          VARCHAR (64)  NULL,
    [HWM_NAME]             VARCHAR (255) NULL,
    [HWM_VALUE]            NUMERIC (19)  NOT NULL,
    [TRANSFORM_START_TIME] DATETIME      NULL,
    [TRANSFORM_END_TIME]   DATETIME      NULL,
    [ROW_COUNT]            INT           NULL,
    [CREATED_TS]           INT           NOT NULL
);

