﻿CREATE TABLE [dbo].[IWD_PLACE_GROUP_FACT_] (
    [PLACE_GROUP_FACT_KEY] NUMERIC (19) NOT NULL,
    [START_DATE_TIME_KEY]  INT          NOT NULL,
    [END_DATE_TIME_KEY]    INT          NOT NULL,
    [TENANT_KEY]           INT          NOT NULL,
    [PLACE_KEY]            INT          NOT NULL,
    [GROUP_KEY]            INT          NOT NULL,
    [CREATE_AUDIT_KEY]     NUMERIC (19) NOT NULL,
    [UPDATE_AUDIT_KEY]     NUMERIC (19) NOT NULL,
    [START_TS]             INT          NULL,
    [END_TS]               INT          NULL,
    [IDB_ID]               NUMERIC (19) NOT NULL,
    [ACTIVE_FLAG]          NUMERIC (1)  NULL,
    [PURGE_FLAG]           NUMERIC (1)  NULL,
    CONSTRAINT [PK_PLC_GR_FT] PRIMARY KEY CLUSTERED ([PLACE_GROUP_FACT_KEY] ASC)
);

