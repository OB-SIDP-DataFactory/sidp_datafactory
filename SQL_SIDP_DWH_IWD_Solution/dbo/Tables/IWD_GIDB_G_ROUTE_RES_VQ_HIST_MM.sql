﻿CREATE TABLE [dbo].[IWD_GIDB_G_ROUTE_RES_VQ_HIST_MM] (
    [ID]               NUMERIC (16) NOT NULL,
    [PARTYID]          VARCHAR (50) NOT NULL,
    [CALLID]           VARCHAR (50) NOT NULL,
    [VQID]             VARCHAR (50) NOT NULL,
    [VQSEQ]            INT          NOT NULL,
    [ADDED]            DATETIME     NOT NULL,
    [ADDED_TS]         INT          NOT NULL,
    [GSYS_DOMAIN]      INT          NULL,
    [GSYS_SYS_ID]      INT          NULL,
    [CREATE_AUDIT_KEY] NUMERIC (19) NOT NULL
);

