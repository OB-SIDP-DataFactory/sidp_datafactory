﻿CREATE TABLE [dbo].[IWD_STG_UDH_V2] (
    [CALLID]     VARCHAR (50)  NOT NULL,
    [PARTYID]    VARCHAR (50)  NULL,
    [ENDPOINTID] INT           NULL,
    [KEYNAME]    VARCHAR (64)  NOT NULL,
    [KEYVALUE]   VARCHAR (255) NULL,
    [ADDED_TS]   INT           NULL,
    [USER_DATA]  INT           NOT NULL,
    [SEQ]        NUMERIC (19)  NOT NULL,
    [CHANGETYPE] INT           NULL,
    [CSEQ]       INT           NULL
);

