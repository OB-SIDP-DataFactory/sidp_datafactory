﻿create view [dbo].[SKILL] as
select
   ID               AS SKILL_KEY,
   TENANTID         AS TENANT_KEY,
   NAME             AS SKILL_NAME,
   CREATE_AUDIT_KEY AS CREATE_AUDIT_KEY,
   UPDATE_AUDIT_KEY AS UPDATE_AUDIT_KEY,
   ID               AS SKILL_CFG_DBID,
   CREATED_TS       AS START_TS,
   DELETED_TS       AS END_TS
FROM IWD_GIDB_GC_SKILL
UNION ALL
SELECT
   -1               AS SKILL_KEY,
   -1               AS TENANT_KEY,
   'UNKNOWN'        AS SKILL_NAME,
   -1               AS CREATE_AUDIT_KEY,
   -1               AS UPDATE_AUDIT_KEY,
   -1               AS SKILL_CFG_DBID,
   -1               AS START_TS,
   -1               AS END_TS
FROM IWD_dual
UNION ALL
SELECT
   -2               AS SKILL_KEY,
   -1               AS TENANT_KEY,
   'NO_VALUE'       AS SKILL_NAME,
   -1               AS CREATE_AUDIT_KEY,
   -1               AS UPDATE_AUDIT_KEY,
   -1               AS SKILL_CFG_DBID,
   -1               AS START_TS,
   -1               AS END_TS
FROM IWD_dual