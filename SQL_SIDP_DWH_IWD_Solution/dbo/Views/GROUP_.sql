﻿create view [dbo].[GROUP_] as
SELECT
   ID       AS GROUP_KEY,
   TENANTID AS TENANT_KEY,
   NAME     AS GROUP_NAME,
   CREATE_AUDIT_KEY AS CREATE_AUDIT_KEY,
   UPDATE_AUDIT_KEY AS UPDATE_AUDIT_KEY,
      CASE TYPE
         WHEN 0 THEN 'Unknown'
         WHEN 1 THEN 'Agent'
         WHEN 2 THEN 'Place'
         WHEN 3 THEN
                  CASE DNGROUPTYPE
                    WHEN 0 THEN 'Unknown'
                    WHEN 1 THEN 'Single Port'
                    WHEN 2 THEN 'Queue'
                    WHEN 3 THEN 'RoutingPoint'
                    WHEN 4 THEN 'Network Port'
                    WHEN 5 THEN 'Service Number'
                    ELSE 'Unknown'
                  END
         ELSE 'Unknown'
      END AS GROUP_TYPE,
      CASE TYPE
         WHEN 0 THEN 'UNKNOWN'
         WHEN 1 THEN 'AGENT'
         WHEN 2 THEN 'PLACE'
         WHEN 3 THEN
                  CASE DNGROUPTYPE
                    WHEN 0 THEN 'UNKNOWN'
                    WHEN 1 THEN 'SINGLEPORT'
                    WHEN 2 THEN 'QUEUE'
                    WHEN 3 THEN 'ROUTINGPOINT'
                    WHEN 4 THEN 'NETWORKPORT'
                    WHEN 5 THEN 'SERVICENUMBER'
                    ELSE 'UNKNOWN'
                  END
         ELSE 'UNKNOWN'
      END AS GROUP_TYPE_CODE,
   ID       AS GROUP_CFG_DBID,
   TYPE     AS GROUP_CFG_TYPE_ID,
   CREATED_TS  AS START_TS,
   DELETED_TS  AS END_TS
FROM IWD_GIDB_GC_GROUP
UNION ALL
SELECT
       -1           AS GROUP_KEY,
       -1           AS TENANT_KEY,
       'UNKNOWN'    AS GROUP_NAME,
        -1          AS CREATE_AUDIT_KEY,
        -1          AS UPDATE_AUDIT_KEY,
        'UNKNOWN'   AS GROUP_TYPE,
        'UNKNOWN'   AS GROUP_TYPE_CODE,
        -1          AS GROUP_CFG_DBID,
        -1          AS GROUP_CFG_TYPE_ID,
        -1          AS START_TS,
        -1          AS END_TS
FROM IWD_dual
UNION ALL
SELECT
       -2           AS GROUP_KEY,
       -1           AS TENANT_KEY,
       'No Group'   AS GROUP_NAME,
        -1          AS CREATE_AUDIT_KEY,
        -1          AS UPDATE_AUDIT_KEY,
        'NO_VALUE'  AS GROUP_TYPE,
        'NO_VALUE'  AS GROUP_TYPE_CODE,
        -1          AS GROUP_CFG_DBID,
        -1          AS GROUP_CFG_TYPE_ID,
        -1          AS START_TS,
        -1          AS END_TS
FROM IWD_dual