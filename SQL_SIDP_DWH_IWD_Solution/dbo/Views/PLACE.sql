﻿create view [dbo].[PLACE] as
select
   ID as PLACE_KEY,
   TENANTID as TENANT_KEY,
   NAME as PLACE_NAME,
   ID as PLACE_CFG_DBID,
   CREATED_TS as START_TS,
   DELETED_TS as END_TS,
   CREATE_AUDIT_KEY as CREATE_AUDIT_KEY,
   UPDATE_AUDIT_KEY as UPDATE_AUDIT_KEY
from
   IWD_GIDB_GC_PLACE
UNION ALL
select
   -1 as PLACE_KEY,
   -1 as TENANT_KEY,
   'UNKNOWN' as PLACE_NAME,
   -1    as PLACE_CFG_DBID,
   -1    as START_TS,
   -1    as END_TS,
   -1    as CREATE_AUDIT_KEY,
   -1    as UPDATE_AUDIT_KEY
from
   IWD_dual
UNION ALL
select
   -2 as PLACE_KEY,
   -1 as TENANT_KEY,
   'NO_VALUE' as PLACE_NAME,
   -1    as PLACE_CFG_DBID,
   -1    as START_TS,
   -1    as END_TS,
   -1    as CREATE_AUDIT_KEY,
   -1    as UPDATE_AUDIT_KEY
from
   IWD_dual