﻿CREATE procedure [dbo].[GIM_EXEC](@v_sql varchar(max)) as
begin
    declare @v_sql_trimmed varchar(max);
    set	@v_sql_trimmed = ltrim(rtrim(@v_sql));
    print 'Executing: ' + @v_sql_trimmed;
    exec(@v_sql_trimmed);
end