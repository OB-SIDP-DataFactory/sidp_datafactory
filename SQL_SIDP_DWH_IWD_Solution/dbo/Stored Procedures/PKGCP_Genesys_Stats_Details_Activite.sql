﻿CREATE PROCEDURE  [dbo].[PKGCP_Genesys_Stats_Details_Activite] 
		@date_obs DATE,
		@NOM_MANAGER AS VARCHAR(100),
		@Conseiller AS VARCHAR(100)
AS 
BEGIN
SET DATEFIRST 1;
WITH Conseillers AS
	(
		SELECT DISTINCT
			Manager,
			login_conseiller,
			Concat(LTRIM(RTRIM(Nom_conseiller)),'  ',LTRIM(RTRIM(Prenom_conseiller))) AS Nom_Complet_Conseiller,Service 
		FROM
			dbo.IWD_ORGANISATION_CONSEILLER
		WHERE
			(Manager = @NOM_MANAGER OR '999'=@NOM_MANAGER)
	),
	cte_date_obs AS
	(
		SELECT
			@date_obs AS StandardDate,
			DATEADD(day, (-1 * DATEPART(dw,  @date_obs)) + 1,  @date_obs) AS FirstDOW,
			DATEADD(day, (7 - DATEPART(dw, @date_obs)), @date_obs) AS LastDOW
	),
	cte_jour_alim_list AS
	(
		SELECT
			DATE_TIME_KEY,
			T1.[LABEL_YYYY_MM_DD] AS jour_alim
		FROM
			[dbo].[IWD_DATE_TIME] T1
			INNER JOIN cte_date_obs
			ON	T1.LABEL_YYYY_MM_DD >= FirstDOW AND T1.LABEL_YYYY_MM_DD <=LastDOW
	),
---------  Temps d'occupation'------------
Temps_Occupation as 
(
select 
  temp.jour_alim,
  DATEADD(ss,FACT.START_DATE_TIME_KEY,(cast('19700101' as datetime))) AS DateTimeInteraction,
  DATEADD(ss,END_TS,'19700101') AS END_Time_Interaction,
  FACT.RESOURCE_KEY, 
  DATEADD(ss,START_TS,'19700101') AS Start_Time,
  DATEADD(ss,END_TS,'19700101') AS END_Time,
  c.Nom_Complet_Conseiller, 
  c.Manager, 
  c.Service, 
  TOTAL_DURATION, 
  REASON.REASON_TYPE_CODE, 
  CASE 
	WHEN HARDWARE_REASON='0' OR SOFTWARE_REASON_VALUE='0' THEN 'Action Code_0'
	WHEN HARDWARE_REASON='1' OR SOFTWARE_REASON_VALUE='1' THEN 'PAUSE'
	WHEN HARDWARE_REASON='2' OR SOFTWARE_REASON_VALUE='2' THEN 'CHAT'
	WHEN HARDWARE_REASON='3' OR SOFTWARE_REASON_VALUE='3' THEN 'Reunion - Formation'
	WHEN HARDWARE_REASON='4' OR SOFTWARE_REASON_VALUE='4' THEN 'Suivi Individuel'
	WHEN HARDWARE_REASON='5' OR SOFTWARE_REASON_VALUE='5' THEN 'Gestion Post Appel'
	WHEN HARDWARE_REASON='6' OR SOFTWARE_REASON_VALUE='6' THEN 'Saisie'
	WHEN HARDWARE_REASON='7' OR SOFTWARE_REASON_VALUE='7' THEN 'Technique'
	WHEN HARDWARE_REASON='8' OR SOFTWARE_REASON_VALUE='8' THEN 'Authentification'
	WHEN HARDWARE_REASON='90' OR SOFTWARE_REASON_VALUE='90' THEN 'Autre1'
	WHEN HARDWARE_REASON='91' OR SOFTWARE_REASON_VALUE='91' THEN 'Autre2'
	WHEN HARDWARE_REASON='92' OR SOFTWARE_REASON_VALUE='92' THEN 'Autre3'
	WHEN HARDWARE_REASON='93' OR SOFTWARE_REASON_VALUE='93' THEN 'Autre4'
  END AS SOFTWARE_REASON_KEY 
From 
	IWD_SM_RES_STATE_REASON_FACT FACT 
	INNER JOIN IWD_RESOURCE_STATE_REASON REASON 
	ON FACT.RESOURCE_STATE_REASON_KEY=REASON.RESOURCE_STATE_REASON_KEY
	INNER JOIN IWD_RESOURCE_ 
	ON IWD_RESOURCE_.RESOURCE_KEY=FACT.RESOURCE_KEY
	INNER JOIN Conseillers c
	ON C.login_conseiller=IWD_RESOURCE_.RESOURCE_NAME
	--ON C.Nom_Complet_Conseiller = CONCAT(LTRIM(RTRIM(IWD_RESOURCE_.AGENT_LAST_NAME)),'  ',LTRIM(RTRIM(IWD_RESOURCE_.AGENT_FIRST_NAME)))
	INNER JOIN	cte_jour_alim_list temp
	ON	FACT.START_DATE_TIME_KEY = temp.DATE_TIME_KEY
WHERE   IWD_RESOURCE_.GMT_END_TIME IS NULL 
AND (FACT.RESOURCE_KEY=@Conseiller OR 999=@Conseiller)
),
--ORDER BY jour_alim,service,Manager,Nom_Complet_Conseiller,Start_Time,SOFTWARE_REASON_KEY
----------------- Temps dinteraction ----------------------
Temps_Interaction as 
(
SELECT 
temp.jour_alim,
DATEADD(ss,START_TS,'19700101') AS Start_Time,
DATEADD(ss,END_TS,'19700101') AS END_Time,
c.Service,
c.Manager,
c.Nom_Complet_Conseiller,
IWD_RESOURCE_.RESOURCE_TYPE,
FACT.TALK_DURATION,
MEDIA.MEDIA_NAME_CODE,
INTER.INTERACTION_TYPE_CODE
FROM IWD_INTERACTION_RESOURCE_FACT FACT
INNER JOIN IWD_MEDIA_TYPE MEDIA 
	ON FACT.MEDIA_TYPE_KEY=MEDIA.MEDIA_TYPE_KEY
INNER JOIN IWD_INTERACTION_TYPE INTER
	ON FACT.INTERACTION_TYPE_KEY=INTER.INTERACTION_TYPE_KEY
INNER JOIN IWD_RESOURCE_ 
	ON IWD_RESOURCE_.RESOURCE_KEY=FACT.RESOURCE_KEY
INNER JOIN Conseillers c
	ON C.login_conseiller=IWD_RESOURCE_.RESOURCE_NAME
	--ON C.Nom_Complet_Conseiller = CONCAT(LTRIM(RTRIM(IWD_RESOURCE_.AGENT_LAST_NAME)),'  ',LTRIM(RTRIM(IWD_RESOURCE_.AGENT_FIRST_NAME)))
INNER JOIN	cte_jour_alim_list temp
	ON	FACT.START_DATE_TIME_KEY = temp.DATE_TIME_KEY
WHERE	(FACT.RESOURCE_KEY=@Conseiller OR 999=@Conseiller)
AND  IWD_RESOURCE_.GMT_END_TIME IS NULL 
--ORDER BY jour_alim,service,Manager,Nom_Complet_Conseiller,Start_Time,RESOURCE_TYPE,MEDIA_NAME_CODE,INTERACTION_TYPE_CODE
)

SELECT
		jour_alim,
		Start_Time,
		END_Time,
		Service,
		Manager,
		Nom_Complet_Conseiller,
		MEDIA,
		Type_Interaction_occupation,
		Duree_Interaction_Occupation
FROM 
(
	SELECT 
		jour_alim,
		Start_Time,
		END_Time,
		Service,
		Manager,
		Nom_Complet_Conseiller,
		MEDIA_NAME_CODE AS MEDIA,
		CASE 
			WHEN INTERACTION_TYPE_CODE='INBOUND' THEN 'Entrant'
			WHEN INTERACTION_TYPE_CODE='OUTBOUND' THEN 'Sortant'
			WHEN INTERACTION_TYPE_CODE='INTERNAL' THEN 'Interne '
			END  
		AS Type_Interaction_occupation,
		TALK_DURATION as Duree_Interaction_Occupation
	FROM Temps_Interaction
	Union all 
	SELECT 
		jour_alim,
		Start_Time,
		END_Time,
		Service,
		Manager,
		Nom_Complet_Conseiller,
		'----' AS MEDIA,
		SOFTWARE_REASON_KEY AS Type_Interaction_occupation,
		TOTAL_DURATION as Duree_Interaction_Occupation
	FROM Temps_Occupation
) Dump 
ORDER BY jour_alim,service,Manager,Nom_Complet_Conseiller,Start_Time,END_Time

END
GO