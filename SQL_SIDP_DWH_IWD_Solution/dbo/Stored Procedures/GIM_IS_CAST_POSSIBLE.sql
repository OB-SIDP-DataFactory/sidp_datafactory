﻿CREATE procedure [dbo].[GIM_IS_CAST_POSSIBLE](@V varchar(max), @CAST_TYPE varchar(max), @debug bit = 0) as 
begin
    declare @V_SQL varchar(max)
    set @V_SQL = 'declare @var ' + @CAST_TYPE + '; set @var = ' + @V +';';
    if @debug=1 print @V_SQL;
    begin try
        exec(@V_SQL);
        return 1;
    end try
    begin catch
        return 0;
    end catch
end