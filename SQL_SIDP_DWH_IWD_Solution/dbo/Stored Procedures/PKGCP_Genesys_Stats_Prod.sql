﻿CREATE PROCEDURE [dbo].[PKGCP_Genesys_Stats_Prod]
	@date_obs DATE,
	@NOM_MANAGER AS VARCHAR(100),
	@Conseiller AS VARCHAR(100),--Nouveau champ ajouté
	@Duree as varchar(50)  -- week or month
AS
BEGIN
	SET DATEFIRST 1;

	WITH Conseillers AS
	(
		SELECT DISTINCT
			login_conseiller,
			Manager,
			Concat(LTRIM(RTRIM(Nom_conseiller)),'  ',LTRIM(RTRIM(Prenom_conseiller))) AS Nom_Complet_Conseiller
		FROM
			IWD_ORGANISATION_CONSEILLER
		WHERE
			Manager = @NOM_MANAGER
	),
	cte_date_obs AS
	(
		SELECT
			@date_obs AS StandardDate,
			CASE WHEN @Duree='Week' 
				THEN DATEADD(day, (-1 * DATEPART(dw,  @date_obs)) + 1,  @date_obs)
				 WHEN @Duree='Month' THEN Cast(DATEADD(month, DATEDIFF(month, 0, @date_obs), 0) as date ) 
			END as FirstDayOfDate,
			CASE WHEN @Duree='Week' 
				THEN DATEADD(day, (7 - DATEPART(dw, @date_obs)), @date_obs)
				WHEN @Duree='Month' THEN EOMONTH(@date_obs)
			END AS LastDayOfDate
	),
	cte_jour_alim_list AS
	(
		SELECT
			DATE_TIME_KEY,
			T1.[LABEL_YYYY_MM_DD] AS jour_alim
		FROM
			[dbo].[IWD_DATE_TIME] T1
			INNER JOIN cte_date_obs
				ON	T1.LABEL_YYYY_MM_DD >= FirstDayOfDate
					AND T1.LABEL_YYYY_MM_DD <=LastDayOfDate
	),
	--Temps de connexion
	SM_RES_SESSION_FACT_CUMUL AS
	(
		SELECT
			temp.jour_alim,
			IWD_RESOURCE_.RESOURCE_KEY,
			C.Manager,
			C.Nom_Complet_Conseiller,
			AGENT_FIRST_NAME,
			AGENT_LAST_NAME,
			SUM(TOTAL_DURATION) AS TEMPS_CONNEXION
		FROM
			IWD_RESOURCE_
			INNER JOIN Conseillers	C
				ON C.login_conseiller=IWD_RESOURCE_.RESOURCE_NAME
				--ON	C.Nom_Complet_Conseiller = CONCAT(LTRIM(RTRIM(IWD_RESOURCE_.AGENT_LAST_NAME)),'  ',LTRIM(RTRIM(IWD_RESOURCE_.AGENT_FIRST_NAME)))
			LEFT JOIN	IWD_SM_RES_SESSION_FACT
				ON	IWD_RESOURCE_.RESOURCE_KEY = IWD_SM_RES_SESSION_FACT.RESOURCE_KEY
			RIGHT JOIN	cte_jour_alim_list temp
				ON	IWD_SM_RES_SESSION_FACT.START_DATE_TIME_KEY = temp.DATE_TIME_KEY
			WHERE IWD_RESOURCE_.GMT_END_TIME IS NULL 
		GROUP BY
			temp.jour_alim,
			C.Manager,
			C.Nom_Complet_Conseiller,
			AGENT_FIRST_NAME,
			AGENT_LAST_NAME,
			IWD_RESOURCE_.RESOURCE_KEY,
			MEDIA_TYPE_KEY
	),
	SM_RES_SESSION_FACT AS
	(
		SELECT
			jour_alim,
			Manager,
			Nom_Complet_Conseiller,
			AGENT_FIRST_NAME,
			AGENT_LAST_NAME,
			RESOURCE_KEY,
			MAX(TEMPS_CONNEXION) AS TEMPS_CONNEXION
		FROM
			SM_RES_SESSION_FACT_CUMUL
		GROUP BY
			jour_alim,
			Manager,
			Nom_Complet_Conseiller,
			AGENT_FIRST_NAME,
			AGENT_LAST_NAME,
			RESOURCE_KEY
	),
	--Temps de pause
	SM_RES_STATE_REASON_FACT AS
	(
		SELECT
			temp.jour_alim,
			IWD_RESOURCE_.RESOURCE_KEY,
			C.Manager,
			C.Nom_Complet_Conseiller,
			AGENT_FIRST_NAME,
			AGENT_LAST_NAME,
			--Temps passé en prod
			SUM
			(
				CASE
					WHEN		SOFTWARE_REASON_VALUE IN ('1','2','3', '4','6','90','91','92','93') 
					OR HARDWARE_REASON IN ('1','2','3', '4','6','90','91','92','93')
						THEN	TOTAL_DURATION
					ELSE		0
				END
			)	AS Temps_Tot,
			SUM
			(
				CASE
					WHEN		HARDWARE_REASON='1' 
								OR SOFTWARE_REASON_VALUE = '1'
						THEN	TOTAL_DURATION
					ELSE		0
				END
			)	AS  TEMPS_PAUSE,
			SUM
			(
				CASE
					WHEN		HARDWARE_REASON='2' 
								OR SOFTWARE_REASON_VALUE = '2'
						THEN	TOTAL_DURATION
					ELSE		0
				END
			)	AS  Temps_Chat,
			SUM
			(
				CASE
					WHEN		HARDWARE_REASON='3' 
								OR SOFTWARE_REASON_VALUE = '3'
						THEN	TOTAL_DURATION
					ELSE		0
				END
			)	AS  TEMPS_REUNION_FORMATION,
			SUM
			(
				CASE
					WHEN		HARDWARE_REASON='7' 
								OR SOFTWARE_REASON_VALUE = '7'
						THEN	TOTAL_DURATION
					ELSE		0
				END
			)	AS  TEMPS_TECHNIQUE,
			SUM
			(
				CASE
					WHEN		HARDWARE_REASON='4' 
								OR SOFTWARE_REASON_VALUE = '4'
						THEN	TOTAL_DURATION
					ELSE		0
				END
			)	AS  TEMPS_SUIVI_INDIVI,
			SUM   --Somme de Temps de post appel 
			(
				CASE
					WHEN		HARDWARE_REASON='5' 
								OR SOFTWARE_REASON_VALUE = '5'
						THEN	TOTAL_DURATION
					ELSE		0
				END
			) AS Temps_Post_Appel,
			SUM   -- Nombre de post appel 
			(
				CASE
					WHEN		HARDWARE_REASON='5' 
								OR SOFTWARE_REASON_VALUE = '5'
						THEN	1
					ELSE		0
				END
			) AS Nbr_Post_appel,
			--------- Calcul Post Appel Moyen 
			IIF(SUM (CASE WHEN HARDWARE_REASON='5' OR SOFTWARE_REASON_VALUE = '5' THEN	1 ELSE 0 END)=0,0, 
			SUM   --Somme de Temps de post appel 
			(
				CASE
					WHEN		HARDWARE_REASON='5' 
								OR SOFTWARE_REASON_VALUE = '5'
						THEN	TOTAL_DURATION
					ELSE		0
				END
			) / 
			SUM   -- Nombre de post appel 
			(
				CASE
					WHEN		HARDWARE_REASON='5' 
								OR SOFTWARE_REASON_VALUE = '5'
						THEN	1
					ELSE		0
				END
			) 
			)   AS  POST_APPEL_MOY,
			SUM
			(
				CASE
					WHEN		HARDWARE_REASON='6' 
								OR SOFTWARE_REASON_VALUE = '6'
						THEN	TOTAL_DURATION
					ELSE		0
				END
			) AS  TEMPS_SAISIE,
			SUM
			(
				CASE
					WHEN		HARDWARE_REASON='8' 
								OR SOFTWARE_REASON_VALUE = '8'
						THEN	TOTAL_DURATION
					ELSE		0
				END
			) AS  TEMPS_Authentification
		FROM
			IWD_RESOURCE_
			INNER JOIN Conseillers	C
				ON C.login_conseiller=IWD_RESOURCE_.RESOURCE_NAME
				--ON	C.Nom_Complet_Conseiller = CONCAT(LTRIM(RTRIM(IWD_RESOURCE_.AGENT_LAST_NAME)),'  ',LTRIM(RTRIM(IWD_RESOURCE_.AGENT_FIRST_NAME)))
			LEFT JOIN IWD_SM_RES_STATE_REASON_FACT_V2 rs_fact
				ON	IWD_RESOURCE_.RESOURCE_KEY=rs_fact.RESOURCE_KEY
			LEFT JOIN IWD_RESOURCE_STATE_REASON RR
				ON rs_fact.RESOURCE_STATE_REASON_KEY= RR.RESOURCE_STATE_REASON_KEY
			LEFT JOIN IWD_RESOURCE_STATE RS
				ON rs_fact.RESOURCE_STATE_KEY=RS.RESOURCE_STATE_KEY
			RIGHT JOIN cte_jour_alim_list temp
				ON rs_fact.START_DATE_TIME_KEY=temp.DATE_TIME_KEY
			WHERE IWD_RESOURCE_.GMT_END_TIME IS NULL 
		GROUP BY
			temp.jour_alim,
			C.Manager,
			C.Nom_Complet_Conseiller,
			AGENT_FIRST_NAME,
			AGENT_LAST_NAME,
			IWD_RESOURCE_.RESOURCE_KEY
	),
	INTERACTION_RESOURCE_FACT AS
	(
		SELECT
			temp.jour_alim,
			IWD_RESOURCE_.RESOURCE_KEY,
			C.Manager,
			C.Nom_Complet_Conseiller,
			AGENT_FIRST_NAME,
			AGENT_LAST_NAME,
			SUM
			(
			Case WHEN MEDIA_NAME='voice' AND RESOURCE_TYPE='agent' AND TALK_DURATION>10 AND INTERACTION_TYPE ='inbound' THEN TALK_DURATION ELSE 0 END 
			)	AS TEMPS_PASSE_EN_APPEL,
			SUM
			(
				CASE WHEN MEDIA_NAME='voice' AND INTERACTION_TYPE ='inbound' AND RESOURCE_TYPE='agent' AND TALK_DURATION >10  THEN 1 ELSE 0 END)             AS NB_APPELS_TRAITES_ENTRANTS,
			sum
			(
				CASE WHEN MEDIA_NAME='voice' AND INTERACTION_TYPE='inbound' AND RESOURCE_TYPE='agent' AND TALK_DURATION >10 THEN TALK_DURATION ELSE 0 END)   AS duration_APPELS_TRAITES_ENTRANTS,
			SUM
			(
				CASE WHEN MEDIA_NAME='voice' AND INTERACTION_TYPE='outbound' AND RESOURCE_TYPE='agent' AND TALK_DURATION >10  THEN 1 ELSE 0 END)             AS NB_APPELS_ABOUTIS_SORTANTS,
			sum
			(
				CASE WHEN MEDIA_NAME='voice' AND INTERACTION_TYPE='outbound' AND RESOURCE_TYPE='agent' AND TALK_DURATION >10 THEN TALK_DURATION ELSE 0 END)  AS duration_APPELS_ABOUTIS_SORTANTS,
			SUM
			(
				CASE WHEN MEDIA_NAME='chat_crm' AND RESOURCE_TYPE='agent' AND TALK_DURATION >=10 THEN 1 ELSE 0 END)	AS NB_CHAT_TRAITES,
			sum
			(
				CASE WHEN MEDIA_NAME='chat_crm' AND RESOURCE_TYPE='agent' AND TALK_DURATION >=10  THEN TALK_DURATION ELSE 0 END)	AS duration_CHAT_TRAITES,
			SUM
			(
				CASE
					WHEN MEDIA_NAME='case_crm' AND RESOURCE_TYPE='agent' AND TALK_DURATION >=10 THEN 1 ELSE 0
				END
			)	AS NB_REQ_TRAITES,
			sum
			(
				CASE WHEN MEDIA_NAME='case_crm' AND RESOURCE_TYPE='agent' AND TALK_DURATION >=10 THEN TALK_DURATION ELSE 0 END
			)	AS duration_REQ_TRAITES,
			SUM
			(
				CASE
					WHEN MEDIA_NAME='email_CRM' THEN 1 ELSE 0
				END)	AS NB_MAILS_TRAITES,
			sum
			(
				CASE WHEN MEDIA_NAME='email_CRM' THEN TALK_DURATION ELSE 0
				END
			)	AS duration_MAILS_TRAITES
		FROM
			IWD_RESOURCE_
			CROSS JOIN cte_jour_alim_list temp
			INNER JOIN Conseillers	C
				ON C.login_conseiller=IWD_RESOURCE_.RESOURCE_NAME
				--ON	C.Nom_Complet_Conseiller = CONCAT(LTRIM(RTRIM(IWD_RESOURCE_.AGENT_LAST_NAME)),'  ',LTRIM(RTRIM(IWD_RESOURCE_.AGENT_FIRST_NAME)))
			LEFT JOIN IWD_INTERACTION_RESOURCE_FACT rs_fact
				ON IWD_RESOURCE_.RESOURCE_KEY=rs_fact.RESOURCE_KEY
				and rs_fact.START_DATE_TIME_KEY=temp.DATE_TIME_KEY
			LEFT JOIN IWD_MEDIA_TYPE
				ON  IWD_MEDIA_TYPE.MEDIA_TYPE_KEY=rs_fact.MEDIA_TYPE_KEY
			LEFT JOIN IWD_INTERACTION_TYPE
				ON IWD_INTERACTION_TYPE.INTERACTION_TYPE_KEY = rs_fact.INTERACTION_TYPE_KEY
			--RIGHT JOIN cte_jour_alim_list temp
			--	ON rs_fact.START_DATE_TIME_KEY=temp.DATE_TIME_KEY
			WHERE IWD_RESOURCE_.GMT_END_TIME IS NULL 
		GROUP BY
			temp.jour_alim,
			C.Manager,
			C.Nom_Complet_Conseiller,
			AGENT_FIRST_NAME,
			AGENT_LAST_NAME,IWD_RESOURCE_.RESOURCE_KEY
	),
	INTERACTION_RESOURCE_FACT_Cumul AS
	(
		SELECT
			F1.jour_alim,
			F1.RESOURCE_KEY,
			F1.Manager,
			F1.Nom_Complet_Conseiller,
			F1.AGENT_FIRST_NAME,
			F1.AGENT_LAST_NAME,
			SUM(TEMPS_PASSE_EN_APPEL)	AS TEMPS_PASSE_EN_APPEL,
			SUM(NB_APPELS_TRAITES_ENTRANTS)	AS NB_APPELS_TRAITES_ENTRANTS,
			SUM(duration_APPELS_TRAITES_ENTRANTS)	AS duration_APPELS_TRAITES_ENTRANTS,
			SUM(Temps_Post_Appel) as Temps_Post_Appel,
			Sum(Nbr_post_appel) as Nbr_Post_Appel,
			/*IIF(
				ISNULL(SUM(TEMPS_PASSE_EN_APPEL),0)=0,
				0,
				cast(3600*(SUM(NB_APPELS_TRAITES_ENTRANTS)+ SUM(NB_APPELS_ABOUTIS_SORTANTS)) AS float)/CAST(SUM(TEMPS_PASSE_EN_APPEL) as decimal(10,2))
				)	AS NB_APPELS_HEURE,*/
			IIF(ISNULL(SUM(a.Temps_CONNEXION-ISNULL(F2.Temps_Tot,0)),0)=0,
				0,
				Cast((Cast(3600*(SUM(NB_APPELS_TRAITES_ENTRANTS)) AS FLOAT)/SUM(a.Temps_CONNEXION-ISNULL(F2.Temps_Tot,0))) AS DECIMAL(5,1))
				) AS NB_APPELS_HEURE,
			SUM(NB_APPELS_ABOUTIS_SORTANTS)	AS NB_APPELS_ABOUTIS_SORTANTS,
			SUM(duration_APPELS_ABOUTIS_SORTANTS)	AS duration_APPELS_ABOUTIS_SORTANTS,
			IIF(
				ISNULL(SUM(NB_APPELS_TRAITES_ENTRANTS),0)=0,0,(SUM(TEMPS_PASSE_EN_APPEL)/Cast((SUM(NB_APPELS_TRAITES_ENTRANTS)) as decimal(10,2)))
				)	AS DMC_MOY,
			SUM(NB_CHAT_TRAITES)	AS NB_CHAT_TRAITES,
			IIF(ISNULL(SUM(NB_CHAT_TRAITES),0)=0,0,(SUM(duration_CHAT_TRAITES)/Cast(SUM(NB_CHAT_TRAITES) as decimal(10,2))))	AS DMT_CHAT,
			--IIF(ISNULL(SUM(duration_CHAT_TRAITES),0)=0,0,3600*SUM(NB_CHAT_TRAITES)/CAST(SUM(duration_CHAT_TRAITES) as decimal(10,2)))	AS NB_CHAT_HEURE,
			IIF(ISNULL(SUM(a.Temps_CONNEXION-ISNULL(F2.Temps_Tot,0)),0)=0,0,Cast(Cast(3600*SUM(NB_CHAT_TRAITES) AS float)/SUM(a.Temps_CONNEXION-ISNULL(F2.Temps_Tot,0)) AS decimal(5,1))) AS NB_CHAT_HEURE,
			SUM(NB_REQ_TRAITES)	AS NB_REQ_TRAITES,
			SUM(duration_REQ_TRAITES)	AS duration_REQ_TRAITES,
			SUM(duration_CHAT_TRAITES) AS duration_CHAT_TRAITES,
			IIF(ISNULL(SUM(NB_REQ_TRAITES),0)=0,0,(SUM(duration_REQ_TRAITES)/Cast(SUM(NB_REQ_TRAITES) as decimal(10,2))))	AS DMT_Requete,
			/*IIF(
				ISNULL(SUM(duration_REQ_TRAITES),0)=0,0,3600*SUM(NB_REQ_TRAITES)/CAST(SUM(duration_REQ_TRAITES) as decimal(10,2))
				)	AS NB_REQ_TRAITES_HEURE,*/
			IIF(ISNULL(SUM(a.Temps_CONNEXION-ISNULL(F2.Temps_Tot,0)),0)=0,0,Cast(Cast(3600*SUM(NB_REQ_TRAITES) AS float)/SUM(a.Temps_CONNEXION-ISNULL(F2.Temps_Tot,0)) AS decimal(5,1))) AS NB_REQ_TRAITES_HEURE,
			SUM(NB_MAILS_TRAITES)	AS NB_MAILS_TRAITES,
			SUM(duration_MAILS_TRAITES)	AS duration_MAILS_TRAITES,
			IIF(ISNULL(SUM(NB_MAILS_TRAITES),0)=0,0,(SUM(duration_MAILS_TRAITES)/Cast(SUM(NB_MAILS_TRAITES) as decimal(10,2))))	AS DMT_Mail,
			--IIF(ISNULL(SUM(duration_MAILS_TRAITES),0)=0,0,3600*SUM(NB_MAILS_TRAITES)/CAST(SUM(duration_MAILS_TRAITES)as decimal(10,2)))	AS NB_MAILS_TRAITES_HEURE
			IIF(ISNULL(SUM(a.Temps_CONNEXION-ISNULL(F2.Temps_Tot,0)),0)=0,0,Cast(Cast(3600*SUM(NB_MAILS_TRAITES) AS float)/SUM(a.Temps_CONNEXION-ISNULL(F2.Temps_Tot,0)) AS decimal(5,1))) AS NB_MAILS_TRAITES_HEURE
		FROM
			INTERACTION_RESOURCE_FACT F1
			LEFT OUTER JOIN SM_RES_STATE_REASON_FACT F2 
			ON F1.RESOURCE_KEY=F2.RESOURCE_KEY AND F1.jour_alim=F2.jour_alim
			LEFT JOIN SM_RES_SESSION_FACT a
			ON (F1.RESOURCE_KEY=a.RESOURCE_KEY AND a.jour_alim=F1.jour_alim )
		GROUP BY
			F1.jour_alim,
			F1.Manager,
			F1.Nom_Complet_Conseiller,
			F1.AGENT_FIRST_NAME,
			F1.AGENT_LAST_NAME,
			F1.RESOURCE_KEY,
			F2.Temps_Tot
	)
	/*********************_*_Requête_*_*********************/
	SELECT DISTINCT
		COALESCE(temp.jour_alim,a.jour_alim,b.jour_alim,c.jour_alim) jour_alim,
		COALESCE(a.RESOURCE_KEY,b.RESOURCE_KEY,c.RESOURCE_KEY)	AS  RESOURCE_KEY,
		Cn.Manager,
		Cn.Nom_Complet_Conseiller AS NOM_COMPLET,
		ISNULL(a.TEMPS_CONNEXION,0)	AS TEMPS_CONNEXION,
		CASE WHEN ISNULL(a.TEMPS_CONNEXION,0)>3600 THEN 1 ELSE 0 END AS PRESENCE_JOUR,
		ISNULL((a.Temps_CONNEXION-ISNULL(b.Temps_Tot,0)),0)	AS TEMPS_PASSE_PROD,
		CONVERT(DECIMAL(12,2),cast(ISNULL((a.Temps_CONNEXION-ISNULL(b.Temps_Tot,0)),0) AS FLOAT)/IIF(ISNULL(a.TEMPS_CONNEXION,0)=0,1, ISNULL(a.TEMPS_CONNEXION,0)) )	AS TAUX_OCCUPATION,
		ISNULL(b.TEMPS_PAUSE,0)	AS TEMPS_PAUSE,
		ISNULL(b.Temps_Chat,0)	AS TEMPS_CHAT,
		ISNULL(b.TEMPS_REUNION_FORMATION,0) AS TEMPS_REUNION_FORMATION,
		ISNULL(b.TEMPS_SUIVI_INDIVI,0) AS TEMPS_SUIVI_INDIVI,
		ISNULL(b.TEMPS_TECHNIQUE,0) AS TEMPS_TECHNIQUE,
		ISNULL(b.POST_APPEL_MOY,0)	AS POST_APPEL_MOY,
		ISNULL(b.TEMPS_SAISIE,0) AS TEMPS_SAISIE,
		ISNULL(b.Temps_Post_Appel,0) as Temps_Gestion,
		ISNULL(b.TEMPS_Authentification,0) as Temps_Authentification,
		ISNULL(c.NB_APPELS_TRAITES_ENTRANTS,0)	AS NB_APPELS_TRAITES_ENTRANTS,
		--SERVICE_NB_APPELS_TRAITES_ENTRANTS,
		ISNULL(c.NB_APPELS_HEURE,0)	AS NB_APPELS_HEURE,
		TEMPS_PASSE_EN_APPEL,
		duration_CHAT_TRAITES,
		duration_REQ_TRAITES,
		duration_MAILS_TRAITES,
		c.Temps_Post_Appel,
		c.Nbr_post_appel,
		ISNULL(c.NB_APPELS_ABOUTIS_SORTANTS,0)	AS NB_APPELS_ABOUTIS_SORTANTS,
		ISNULL(c.DMC_MOY,0)	AS DMC_MOY,
		ISNULL(c.NB_CHAT_TRAITES,0)	AS NB_CHAT_TRAITES,
		ISNULL(c.NB_CHAT_HEURE,0)	AS NB_CHAT_HEURE,
		ISNULL(c.DMT_CHAT,0) as DMT_CHAT,
		--ISNULL(CAST(IIF(c.NB_CHAT_HEURE=0,0,3600/c.NB_CHAT_HEURE) AS int),0) AS DMT_CHAT,
		ISNULL(c.NB_REQ_TRAITES,0)	AS NB_REQ_TRAITES,
		ISNULL(c.NB_REQ_TRAITES_HEURE,0)	AS NB_REQ_TRAITES_HEURE ,
		--ISNULL(CAST(IIF(c.NB_REQ_TRAITES_HEURE=0,0,3600/c.NB_REQ_TRAITES_HEURE) AS int),0) AS DMT_REQ,
		ISNULL(c.DMT_Requete,0) as DMT_REQ,
		ISNULL(c.NB_MAILS_TRAITES,0)	AS NB_MAILS_TRAITES,
		ISNULL(c.NB_MAILS_TRAITES_HEURE,0)	AS NB_MAILS_TRAITES_HEURE,
		ISNULL(c.DMT_Mail,0) as DMT_MAIL,
		(c.NB_APPELS_ABOUTIS_SORTANTS+c.NB_APPELS_TRAITES_ENTRANTS+c.NB_CHAT_TRAITES+c.NB_MAILS_TRAITES+c.NB_REQ_TRAITES) as NB_TOTAL_INTERACTION
		--ISNULL(CAST(IIF(c.NB_MAILS_TRAITES_HEURE=0,0,3600/c.NB_MAILS_TRAITES_HEURE) AS int),0) AS DMT_MAIL
	--select distinct temp.jour_alim
	FROM
		IWD_RESOURCE_
		INNER JOIN Conseillers	Cn
			ON Cn.login_conseiller=IWD_RESOURCE_.RESOURCE_NAME
			--ON	Cn.Nom_Complet_Conseiller = CONCAT(LTRIM(RTRIM(IWD_RESOURCE_.AGENT_LAST_NAME)),'  ',LTRIM(RTRIM(IWD_RESOURCE_.AGENT_FIRST_NAME)))
		CROSS JOIN cte_jour_alim_list temp
		LEFT JOIN SM_RES_SESSION_FACT a
			ON (IWD_RESOURCE_.RESOURCE_KEY=a.RESOURCE_KEY AND a.jour_alim=temp.jour_alim )
		LEFT JOIN SM_RES_STATE_REASON_FACT b
			ON (IWD_RESOURCE_.RESOURCE_KEY=b.RESOURCE_KEY AND b.jour_alim=temp.jour_alim)
		LEFT JOIN INTERACTION_RESOURCE_FACT_Cumul c
			ON (IWD_RESOURCE_.RESOURCE_KEY=c.RESOURCE_KEY AND c.jour_alim=temp.jour_alim)
	WHERE
		IWD_RESOURCE_.GMT_END_TIME IS NULL 
		and COALESCE(a.RESOURCE_KEY,b.RESOURCE_KEY,c.RESOURCE_KEY) IS NOT NULL
		and (COALESCE(a.RESOURCE_KEY,b.RESOURCE_KEY,c.RESOURCE_KEY)=@Conseiller OR 999=@Conseiller)
    ORDER by 2,1

END;
GO
