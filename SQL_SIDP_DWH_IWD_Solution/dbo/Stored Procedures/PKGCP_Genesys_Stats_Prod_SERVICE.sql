﻿CREATE PROCEDURE [dbo].[PKGCP_Genesys_Stats_Prod_SERVICE]
	@date_obs DATE,
	@NOM_MANAGER AS VARCHAR(100),
	@conseiller as VARCHAR(100) 
AS
BEGIN
	SET DATEFIRST 1;

	WITH	ResourceByService AS -- table qui permet de récuperer toute les resources de service du manager selectionné
	(
		SELECT DISTINCT
			Service,Manager,login_conseiller,
			Concat(LTRIM(RTRIM(Nom_conseiller)),'  ',LTRIM(RTRIM(Prenom_conseiller))) AS Nom_Complet_Conseiller,RESOURCE_KEY
		FROM
			dbo.IWD_ORGANISATION_CONSEILLER C	WITH(NOLOCK)
			INNER JOIN	IWD_RESOURCE_	WITH(NOLOCK)
				ON	C.login_conseiller = IWD_RESOURCE_.RESOURCE_NAME
				--ON Concat(LTRIM(RTRIM(C.Nom_conseiller)),'  ',LTRIM(RTRIM(C.Prenom_conseiller))) = CONCAT(LTRIM(RTRIM(IWD_RESOURCE_.AGENT_LAST_NAME)),'  ',LTRIM(RTRIM(IWD_RESOURCE_.AGENT_FIRST_NAME))) 
		WHERE
			1 = 1
			AND	IWD_RESOURCE_.GMT_END_TIME IS NULL
			AND	Service in 
			(
				select distinct
					Service
				from
					dbo.IWD_ORGANISATION_CONSEILLER	WITH(NOLOCK)
				WHERE
					1 = 1
					AND	Manager = @NOM_MANAGER
			)
	),
	-------------------------définir la date d'observation ------------------
	cte_date_obs AS
	(
	SELECT
	@date_obs AS StandardDate,
	DATEADD(day, (-1 * DATEPART(dw,  @date_obs)) + 1,  @date_obs) AS FirstDOW,
	DATEADD(day, (7 - DATEPART(dw, @date_obs)), @date_obs) AS LastDOW
	) ,
	cte_jour_alim_list AS
	(
	SELECT
	DATE_TIME_KEY,
	T1.[LABEL_YYYY_MM_DD] AS jour_alim
	FROM
	[dbo].[IWD_DATE_TIME] T1
	INNER JOIN cte_date_obs
	ON	T1.LABEL_YYYY_MM_DD >= FirstDOW
	AND T1.LABEL_YYYY_MM_DD <=LastDOW
	)
	,
	--------------------------------------------------------------------------------------------
	----------------- récupération des resources presentes par journée TempsDeConnexion>0 --------------------------

	ResourcesPresentesParJour AS 
	(
	select  SUM(TOTAL_DURATION) as TempConex,[LABEL_YYYY_MM_DD] as jour_alim,RESOURCE_KEY
	from 
	IWD_SM_RES_SESSION_FACT FACT
	INNER JOIN [IWD_DATE_TIME] temp 
	ON FACT.START_DATE_TIME_KEY = temp.DATE_TIME_KEY
	group by [LABEL_YYYY_MM_DD],RESOURCE_KEY
	HAVING SUM(TOTAL_DURATION)>3600
	)
	---------- Temps de connexion ------------------------------------------
	,ConnexionTime AS (
	select SUM(TOTAL_DURATION) as TempsDeConnexion,
	temp.jour_alim AS jour_alim,
	SF.RESOURCE_KEY,
	MEDIA_TYPE_KEY
	--,Manager--à ajouter  
	from IWD_SM_RES_SESSION_FACT SF
	INNER JOIN	cte_jour_alim_list temp
	ON	SF.START_DATE_TIME_KEY = temp.DATE_TIME_KEY
	WHERE SF.RESOURCE_KEY IN 
	(
	Select RESOURCE_KEY from ResourceByService
	)
	group by MEDIA_TYPE_KEY,jour_alim,RESOURCE_KEY)

	,ConnexionTimeMaxByMedia AS (
	select MAX(TempsDeConnexion) AS TempsDeConnexion,jour_alim,RESOURCE_KEY
	-- ,Manager
	from ConnexionTime
	group by jour_alim,RESOURCE_KEY
	)
	,
	ConnexionTimeMaxByMediaAVG
	AS
	(
	select Cast(AVG(Cast(TempsDeConnexion as decimal (10,2))) as decimal(10,2)) AS TempsDeConnexion
	-- ,Manager
	from ConnexionTimeMaxByMedia conx
	INNER JOIN ResourcesPresentesParJour rp
	on conx.RESOURCE_KEY=rp.RESOURCE_KEY
	and conx.jour_alim=rp.jour_alim

	)

	------------------Temps de pause par resource et par jour ------------------------------------------

	,TempsPause AS
	(	select 
	temp.jour_alim AS jour_alim,
	rs_fact.RESOURCE_KEY,
	SUM
	(
	CASE
	WHEN		SOFTWARE_REASON_VALUE IN ('1','2','3', '4','6','90','91','92','93') 
	OR HARDWARE_REASON IN ('1','2','3', '4','6','90','91','92','93')
	THEN	TOTAL_DURATION
	ELSE		0
	END
	)	AS Temps_Tot,
	SUM
	(
	CASE
	WHEN		HARDWARE_REASON='1' 
	OR SOFTWARE_REASON_VALUE = '1'
	THEN	TOTAL_DURATION
	ELSE		0
	END
	)	AS  TEMPS_PAUSE,
	SUM
	(
	CASE
	WHEN		HARDWARE_REASON='2' 
	OR SOFTWARE_REASON_VALUE = '2'
	THEN	TOTAL_DURATION
	ELSE		0
	END
	)	AS  TEMPS_Chat,
	SUM
	(
	CASE
	WHEN		HARDWARE_REASON='3' 
	OR SOFTWARE_REASON_VALUE = '3'
	THEN	TOTAL_DURATION
	ELSE		0
	END
	)	AS  TEMPS_REUNION_FORMATION,
	SUM
	(
	CASE
	WHEN		HARDWARE_REASON='7' 
	OR SOFTWARE_REASON_VALUE = '7'
	THEN	TOTAL_DURATION
	ELSE		0
	END
	)	AS  TEMPS_TECHNIQUE,
	SUM
	(
	CASE
	WHEN		HARDWARE_REASON='4' 
	OR SOFTWARE_REASON_VALUE = '4'
	THEN	TOTAL_DURATION
	ELSE		0
	END
	)	AS  TEMPS_SUIVI_INDIVI,
	SUM
	(
	CASE
	WHEN		HARDWARE_REASON='5' 
	OR SOFTWARE_REASON_VALUE = '5'
	THEN	TOTAL_DURATION
	ELSE		0
	END
	) AS  POST_APPEL_TIME,
	SUM
	(
	CASE
	WHEN		HARDWARE_REASON='5' 
	OR SOFTWARE_REASON_VALUE = '5'
	THEN	1
	ELSE		0
	END
	) AS  NBR_POST_APPEL,
	SUM
	(
	CASE
	WHEN		HARDWARE_REASON='6' 
	OR SOFTWARE_REASON_VALUE = '6'
	THEN	TOTAL_DURATION
	ELSE		0
	END
	) AS  TEMPS_SAISIE,
	SUM
	(
	CASE
	WHEN		HARDWARE_REASON='8' 
	OR SOFTWARE_REASON_VALUE = '8'
	THEN	TOTAL_DURATION
	ELSE		0
	END
	) AS  TEMPS_Authentification
	FROM
	IWD_SM_RES_STATE_REASON_FACT_V2 rs_fact
	INNER JOIN   IWD_RESOURCE_STATE_REASON RR
	ON rs_fact.RESOURCE_STATE_REASON_KEY= RR.RESOURCE_STATE_REASON_KEY
	INNER JOIN	cte_jour_alim_list temp
	ON	rs_fact.START_DATE_TIME_KEY = temp.DATE_TIME_KEY
	WHERE rs_fact.RESOURCE_KEY IN 
	(
	Select RESOURCE_KEY from ResourceByService
	)
	Group by jour_alim 	,RESOURCE_KEY
	) 
	,
	--------------------------------------AVG temps de pause ------------------------------------
	TempsPause_AVG as 
	(
	SELECT 
	Cast(AVG(Cast(Temps_Tot as decimal (10,2))) as decimal(10,2))  as Temps_Tot_AVG,
	CAST(AVG(Cast(TEMPS_PAUSE  as decimal (10,2))) as decimal(10,2))  as TEMPS_PAUSE_AVG,
	CAST(AVG(Cast(TEMPS_Chat  as decimal (10,2))) as decimal(10,2))  as TEMPS_chat_AVG,
	CAST(AVG(CAST(TEMPS_REUNION_FORMATION  as decimal (10,2))) as decimal(10,2))  as TEMPS_REUNION_FORMATION_AVG,
	CAST(AVG(CAST(TEMPS_TECHNIQUE  as decimal (10,2))) as decimal(10,2))  as TEMPS_TECHNIQUE_AVG,
	CAST(AVG(CAST(TEMPS_SUIVI_INDIVI  as decimal (10,2))) as decimal(10,2))  as TEMPS_SUIVI_INDIVI_AVG,
	IIF(AVG(NBR_POST_APPEL) =0,0,CAST(AVG(CAST(POST_APPEL_TIME  as decimal (10,2))) as FLOAT)/AVG(NBR_POST_APPEL))  as POST_APPEL_MOY_AVG,
	CAST(AVG(CAST(TEMPS_SAISIE  as decimal (10,2))) as decimal(10,2))  as TEMPS_SAISIE_AVG,
	CAST(AVG(CAST(POST_APPEL_TIME  as decimal (10,2))) as decimal(10,2))  as Temps_Post_Appel_AVG,
	CAST(AVG(CAST(Temps_Authentification  as decimal (10,2))) as decimal(10,2))  as Temps_Authentification_AVG
	From TempsPause
	INNER JOIN ResourcesPresentesParJour rp
	on TempsPause.RESOURCE_KEY=rp.RESOURCE_KEY
	and TempsPause.jour_alim=rp.jour_alim
		
	),

	------------------------------------------- Nombre d'appels -------------------------------------
	Cal_Nb as
	(
		SELECT
			temp.jour_alim,
			rs_fact.RESOURCE_KEY,
	SUM
	(TALK_DURATION)	AS TEMPS_PASSE_EN_APPEL,
	SUM
	(
	CASE WHEN MEDIA_NAME='voice' AND INTERACTION_TYPE ='inbound' AND RESOURCE_TYPE='agent' AND TALK_DURATION !=0  THEN 1 ELSE 0 END)             AS NB_APPELS_TRAITES_ENTRANTS,
	sum
	(
	CASE WHEN MEDIA_NAME='voice' AND INTERACTION_TYPE='inbound' AND RESOURCE_TYPE='agent' AND TALK_DURATION !=0 THEN TALK_DURATION ELSE 0 END)   AS duration_APPELS_TRAITES_ENTRANTS,
	SUM
	(
	CASE WHEN MEDIA_NAME='voice' AND INTERACTION_TYPE='outbound' AND RESOURCE_TYPE='agent' AND TALK_DURATION !=0  THEN 1 ELSE 0 END)             AS NB_APPELS_ABOUTIS_SORTANTS,
	sum
	(
	CASE WHEN MEDIA_NAME='voice' AND INTERACTION_TYPE='outbound' AND RESOURCE_TYPE='agent' AND TALK_DURATION !=0 THEN TALK_DURATION ELSE 0 END)  AS duration_APPELS_ABOUTIS_SORTANTS,
	SUM
	(
	CASE WHEN MEDIA_NAME='chat_crm' AND RESOURCE_TYPE='agent' AND TALK_DURATION >=10 THEN 1 ELSE 0 END)	AS NB_CHAT_TRAITES,
	sum
	(
	CASE WHEN MEDIA_NAME='chat_crm' AND RESOURCE_TYPE='agent' AND TALK_DURATION >=10  THEN TALK_DURATION ELSE 0 END)	AS duration_CHAT_TRAITES,
	SUM
	(
	CASE
	WHEN MEDIA_NAME='case_crm' AND RESOURCE_TYPE='agent' AND TALK_DURATION >=10 THEN 1 ELSE 0
	END
	)	AS NB_REQ_TRAITES,
	sum
	(
	CASE WHEN MEDIA_NAME='case_crm' AND RESOURCE_TYPE='agent' AND TALK_DURATION >=10 THEN TALK_DURATION ELSE 0 END
	)	AS duration_REQ_TRAITES,
	SUM
	(
	CASE
	WHEN MEDIA_NAME='email_CRM' THEN 1 ELSE 0
	END)	AS NB_MAILS_TRAITES,
	sum
	(
	CASE WHEN MEDIA_NAME='email_CRM' THEN TALK_DURATION ELSE 0
	END
	)	AS duration_MAILS_TRAITES		
	FROM
	IWD_INTERACTION_RESOURCE_FACT rs_fact
	INNER JOIN IWD_RESOURCE_ 
	ON rs_fact.RESOURCE_KEY=IWD_RESOURCE_.RESOURCE_KEY
	INNER JOIN	cte_jour_alim_list temp
	ON	rs_fact.START_DATE_TIME_KEY = temp.DATE_TIME_KEY
	LEFT JOIN IWD_MEDIA_TYPE ON IWD_MEDIA_TYPE.MEDIA_TYPE_KEY=rs_fact.MEDIA_TYPE_KEY
	LEFT JOIN IWD_INTERACTION_TYPE ON IWD_INTERACTION_TYPE.INTERACTION_TYPE_KEY = rs_fact.INTERACTION_TYPE_KEY
	WHERE rs_fact.RESOURCE_KEY IN 
	(
	Select RESOURCE_KEY from ResourceByService
	)
	Group by jour_alim,rs_fact.RESOURCE_KEY		
	),
	---------------------------------------- CAL_NB_AVG ---------------------------------
	CAL_NB_AVG AS
	(
		SELECT
			CAST(AVG(CAST(TEMPS_PASSE_EN_APPEL  as decimal (10,2))) as decimal(10,2))  as TEMPS_PASSE_EN_APPEL_AVG,
			CAST(AVG(CAST(NB_APPELS_TRAITES_ENTRANTS  as decimal (10,2))) as decimal(10,2))  as NB_APPELS_TRAITES_ENTRANTS_AVG,
			CAST(AVG(CAST(duration_APPELS_TRAITES_ENTRANTS  as decimal (10,2))) as decimal(10,2))  as duration_APPELS_TRAITES_ENTRANTS_AVG,
			CAST(AVG(CAST(NB_APPELS_ABOUTIS_SORTANTS  as decimal (10,2))) as decimal(10,2))  as NB_APPELS_ABOUTIS_SORTANTS_AVG,
			CAST(AVG(CAST(duration_APPELS_ABOUTIS_SORTANTS  as decimal (10,2))) as decimal(10,2))  as duration_APPELS_ABOUTIS_SORTANTS_AVG,
			CAST(AVG(CAST(NB_CHAT_TRAITES  as decimal (10,2))) as decimal(10,2))  as NB_CHAT_TRAITES_AVG,
			CAST(AVG(CAST(duration_CHAT_TRAITES  as decimal (10,2))) as decimal(10,2))  as duration_CHAT_TRAITES_AVG,
			CAST(AVG(CAST(NB_REQ_TRAITES  as decimal (10,2))) as decimal(10,2))  as NB_REQ_TRAITES_AVG,
			CAST(AVG(CAST(duration_REQ_TRAITES  as decimal (10,2))) as decimal(10,2))  as duration_REQ_TRAITES_AVG,
			CAST(AVG(CAST(NB_MAILS_TRAITES  as decimal (10,2))) as decimal(10,2))  as NB_MAILS_TRAITES_AVG,
			CAST(AVG(CAST(duration_MAILS_TRAITES  as decimal (10,2))) as decimal(10,2))  as duration_MAILS_TRAITES_AVG
		FROM
			Cal_Nb CAL_NB
			INNER JOIN ResourcesPresentesParJour RP
				ON	CAL_NB.RESOURCE_KEY=RP.RESOURCE_KEY
					AND CAL_NB.jour_alim=RP.jour_alim
	),
	Conseillers AS
	(
		SELECT DISTINCT
			login_conseiller,
			Manager,
			Concat(LTRIM(RTRIM(Nom_conseiller)),'  ',LTRIM(RTRIM(Prenom_conseiller))) AS Nom_Complet_Conseiller
		FROM
			IWD_ORGANISATION_CONSEILLER
		WHERE
			Manager =@NOM_MANAGER --@NOM_MANAGER
	),
	Resources as 
	(
		SELECT
			MANAGER,
			RESOURCE_KEY
		FROM
			dbo.IWD_RESOURCE_	WITH(NOLOCK)
			inner JOIN Conseillers	C
				ON	C.login_conseiller = IWD_RESOURCE_.RESOURCE_NAME
		WHERE
			1 = 1
			AND
			(
				RESOURCE_KEY = @conseiller
				OR 999 = @conseiller
			)
			AND IWD_RESOURCE_.GMT_END_TIME IS NULL
	),
	/*********************_*_Requête_*_*********************/
	ResultOfService	AS
	(
		SELECT DISTINCT
			ISNULL(a.TempsDeConnexion,0)	AS TEMPS_CONNEXION,
			ISNULL((a.TempsDeConnexion-ISNULL(b.Temps_Tot_AVG,0)),0)	AS TEMPS_PASSE_PROD,
			CONVERT(DECIMAL(4,2),cast(ISNULL((a.TempsDeConnexion-ISNULL(b.Temps_Tot_AVG,0)),0) AS FLOAT)/IIF(ISNULL(a.TempsDeConnexion,0)=0,1, ISNULL(a.TempsDeConnexion,0)) )	AS TAUX_OCCUPATION,
			ISNULL(b.TEMPS_PAUSE_AVG,0)	AS TEMPS_PAUSE,
			ISNULL(b.TEMPS_chat_AVG,0)	AS TEMPS_Chat,
			ISNULL(b.TEMPS_REUNION_FORMATION_AVG,0)	AS TEMPS_REUNION_FORMATION,
			ISNULL(b.TEMPS_TECHNIQUE_AVG,0)	AS TEMPS_TECHNIQUE,
			ISNULL(b.TEMPS_SUIVI_INDIVI_AVG,0)	AS TEMPS_SUIVI_INDIVI,
			ISNULL(b.POST_APPEL_MOY_AVG,0)	AS POST_APPEL_MOY,
			ISNULL(b.TEMPS_SAISIE_AVG,0)	AS TEMPS_SAISIE,
			ISNULL(b.Temps_Post_Appel_AVG,0) as Temps_Gestion,
			ISNULL(b.Temps_Authentification_AVG,0) as Temps_Authentification,
			CAST(ISNULL(c.NB_APPELS_TRAITES_ENTRANTS_AVG,0) as decimal(10,2))	AS NB_APPELS_TRAITES_ENTRANTS,
			IIF(ISNULL((a.TempsDeConnexion-ISNULL(b.Temps_Tot_AVG,0)),0)=0, 0, Cast((Cast(3600*((c.NB_APPELS_TRAITES_ENTRANTS_AVG)) AS FLOAT)/(a.TempsDeConnexion-ISNULL(b.Temps_Tot_AVG,0))) AS DECIMAL(10,2))) AS NB_APPELS_HEURE,
			ISNULL(c.NB_APPELS_ABOUTIS_SORTANTS_AVG,0)	AS NB_APPELS_ABOUTIS_SORTANTS,
			IIF(ISNULL((c.NB_APPELS_TRAITES_ENTRANTS_AVG),0)=0, 0,Cast((Cast ( (c.duration_APPELS_TRAITES_ENTRANTS_AVG ) as FLOAT)/((c.NB_APPELS_TRAITES_ENTRANTS_AVG))) AS DECIMAL(10,2))) AS DMC_MOY,
			ISNULL(c.NB_CHAT_TRAITES_AVG,0) 	AS NB_CHAT_TRAITES,
			IIF(ISNULL((a.TempsDeConnexion-ISNULL(b.Temps_Tot_AVG,0)),0)=0,0,Cast(Cast(3600*(c.NB_CHAT_TRAITES_AVG) AS float)/(a.TempsDeConnexion-ISNULL(b.Temps_Tot_AVG,0)) AS decimal(10,2))) AS NB_CHAT_HEURE,
			IIF(ISNULL(c.NB_CHAT_TRAITES_AVG,0)=0,0,Cast((Cast ( (c.duration_CHAT_TRAITES_AVG) as FLOAT)/(c.NB_CHAT_TRAITES_AVG)) AS DECIMAL(10,2))) AS DMT_CHAT,
			ISNULL(c.NB_REQ_TRAITES_AVG,0) AS NB_REQ_TRAITES,
			IIF(ISNULL((a.TempsDeConnexion-ISNULL(b.Temps_Tot_AVG,0)),0)=0,0,Cast(Cast(3600*(c.NB_REQ_TRAITES_AVG) AS float)/(a.TempsDeConnexion-ISNULL(b.Temps_Tot_AVG,0)) AS decimal(10,2))) AS NB_REQ_TRAITES_HEURE,
			IIF(ISNULL(c.NB_REQ_TRAITES_AVG,0)=0,0,Cast((Cast ( (c.duration_REQ_TRAITES_AVG) as FLOAT)/(c.NB_REQ_TRAITES_AVG)) AS DECIMAL(10,2))) AS DMT_REQ,
			ISNULL(c.NB_MAILS_TRAITES_AVG,0) 	AS NB_MAILS_TRAITES,
			IIF(ISNULL((a.TempsDeConnexion-ISNULL(b.Temps_Tot_AVG,0)),0)=0,0,Cast(Cast(3600*(c.NB_MAILS_TRAITES_AVG) AS float)/(a.TempsDeConnexion-ISNULL(b.Temps_Tot_AVG,0)) AS decimal(10,2))) AS NB_MAILS_TRAITES_HEURE,
			IIF(ISNULL(c.NB_MAILS_TRAITES_AVG,0)=0,0,Cast((Cast ( (c.duration_MAILS_TRAITES_AVG) as FLOAT)/(c.NB_MAILS_TRAITES_AVG)) AS DECIMAL(10,2))) AS DMT_MAIL
		FROM
			ConnexionTimeMaxByMediaAVG a
			CROSS JOIN TempsPause_AVG b
			CROSS JOIN CAL_NB_AVG c 
	),
	FinaleRequest AS
	(
		SELECT
			DMC_MOY,
			DMT_CHAT,
			DMT_MAIL,
			DMT_REQ,
			NB_APPELS_ABOUTIS_SORTANTS,
			NB_APPELS_HEURE,
			NB_APPELS_TRAITES_ENTRANTS,
			NB_CHAT_HEURE,
			NB_CHAT_TRAITES,
			NB_MAILS_TRAITES,
			NB_MAILS_TRAITES_HEURE,
			NB_REQ_TRAITES,
			NB_REQ_TRAITES_HEURE,
			POST_APPEL_MOY,
			TAUX_OCCUPATION,
			Temps_Authentification,
			TEMPS_Chat,
			TEMPS_CONNEXION,
			Temps_Gestion,
			TEMPS_PASSE_PROD,
			TEMPS_PAUSE,
			TEMPS_REUNION_FORMATION,
			TEMPS_SAISIE,
			TEMPS_SUIVI_INDIVI,
			TEMPS_TECHNIQUE
		FROM
			ResultOfService
	)
	SELECT
		DMC_MOY,
		DMT_CHAT,
		DMT_MAIL,
		DMT_REQ,
		MANAGER,
		NB_APPELS_ABOUTIS_SORTANTS,
		NB_APPELS_HEURE,
		NB_APPELS_TRAITES_ENTRANTS,
		NB_CHAT_HEURE,
		NB_CHAT_TRAITES,
		NB_MAILS_TRAITES,
		NB_MAILS_TRAITES_HEURE,
		NB_REQ_TRAITES,
		NB_REQ_TRAITES_HEURE,
		POST_APPEL_MOY,
		RESOURCE_KEY,
		TAUX_OCCUPATION,
		Temps_Authentification,
		TEMPS_Chat,
		TEMPS_CONNEXION,
		Temps_Gestion,
		TEMPS_PASSE_PROD,
		TEMPS_PAUSE,
		TEMPS_REUNION_FORMATION,
		TEMPS_SAISIE,
		TEMPS_SUIVI_INDIVI,
		TEMPS_TECHNIQUE
	FROM
		FinaleRequest 
		CROSS JOIN Resources 
END
GO