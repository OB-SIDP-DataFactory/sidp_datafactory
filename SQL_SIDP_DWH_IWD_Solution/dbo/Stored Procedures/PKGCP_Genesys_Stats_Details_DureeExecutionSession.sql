﻿CREATE PROCEDURE  [dbo].[PKGCP_Genesys_Stats_Details_DureeExecutionSession]
 	@date_obs DATE,
	@NOM_MANAGER AS VARCHAR(100),
	@Conseiller AS VARCHAR(100)--Nouveau champ ajouté
AS

BEGIN

SET DATEFIRST 1;

WITH Conseillers AS
	(
		SELECT DISTINCT
			Manager,
			login_conseiller,
			Concat(LTRIM(RTRIM(Nom_conseiller)),'  ',LTRIM(RTRIM(Prenom_conseiller))) AS Nom_Complet_Conseiller,Service 
		FROM
			IWD_ORGANISATION_CONSEILLER
		WHERE
			(Manager = @NOM_MANAGER OR '999'=@NOM_MANAGER)
	),
	cte_date_obs AS
	(
		SELECT
			@date_obs AS StandardDate,
			DATEADD(day, (-1 * DATEPART(dw,  @date_obs)) + 1,  @date_obs) AS FirstDOW,
			DATEADD(day, (7 - DATEPART(dw, @date_obs)), @Date_obs) AS LastDOW
	),
	cte_jour_alim_list AS
	(
		SELECT
			DATE_TIME_KEY,
			T1.[LABEL_YYYY_MM_DD] AS jour_alim
		FROM
			[dbo].[IWD_DATE_TIME] T1
			INNER JOIN cte_date_obs
			ON	T1.LABEL_YYYY_MM_DD >= FirstDOW AND T1.LABEL_YYYY_MM_DD <=LastDOW
	)

select 
  temp.jour_alim, 
  FACT.RESOURCE_KEY, 
  c.Nom_Complet_Conseiller, 
  c.Manager, 
  c.Service, 
  MEDIA.MEDIA_NAME_CODE AS MEDIA,
  DATEADD(SECOND,3600+START_TS,CONVERT(datetime, convert(varchar(10), 19700101)) ) AS StartSessionTime,
  DATEADD(SECOND,3600+END_TS,CONVERT(datetime, convert(varchar(10), 19700101)) ) AS EndSessionTime,
  FACT.TOTAL_DURATION AS DureeDeConnexion
From 
	IWD_SM_RES_SESSION_FACT FACT
	INNER JOIN IWD_MEDIA_TYPE MEDIA 
	ON FACT.MEDIA_TYPE_KEY=MEDIA.MEDIA_TYPE_KEY 
	INNER JOIN IWD_RESOURCE_ 
	ON IWD_RESOURCE_.RESOURCE_KEY=FACT.RESOURCE_KEY
	INNER JOIN Conseillers c
	ON C.login_conseiller=IWD_RESOURCE_.RESOURCE_NAME
	--ON C.Nom_Complet_Conseiller = CONCAT(LTRIM(RTRIM(IWD_RESOURCE_.AGENT_LAST_NAME)),'  ',LTRIM(RTRIM(IWD_RESOURCE_.AGENT_FIRST_NAME)))
	INNER JOIN	cte_jour_alim_list temp
	ON	FACT.START_DATE_TIME_KEY = temp.DATE_TIME_KEY
AND (FACT.RESOURCE_KEY=@Conseiller OR 999=@Conseiller)
AND  IWD_RESOURCE_.GMT_END_TIME IS NULL 
ORDER BY jour_alim,service,Manager,Nom_Complet_Conseiller,StartSessionTime
END
