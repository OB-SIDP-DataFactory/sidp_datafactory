﻿CREATE PROCEDURE [dbo].[PKGCP_Genesys_Stats_Prod_TEST]

--@date_obs date,
@Date_debut date,
@Date_fin date

AS BEGIN

--,@NOM_COMPLET varchar(max)
--set @date_obs='2018-07-13'
--set @Date_debut='2018-07-01'
--set @Date_fin='2018-09-20'

SET DATEFIRST 1; -->1    Lundi

/*With 
cte_date_obs AS
( SELECT @Date_obs AS StandardDate
       --, DATEADD(day, (-1 * DATEPART(dw,  @date_obs)) + 1,  @date_obs) AS FirstDOW
       --, DATEADD(day, (7 - DATEPART(dw, @Date_obs)), @Date_obs) AS LastDOW
	  
)
--select * from cte_date_obs*/

WITH
cte_jour_alim_list AS
(
SELECT DATE_TIME_KEY,T1.[LABEL_YYYY_MM_DD] AS jour_alim
    FROM [dbo].[IWD_DATE_TIME] T1 --inner join cte_date_obs
    where T1.LABEL_YYYY_MM_DD >= @Date_debut and T1.LABEL_YYYY_MM_DD <=@Date_fin
	--CAST(DATEADD(day, -(1+DATEDIFF(dd,DATEADD(day, (-1 * DATEPART(dw,@date_obs)) + 1, @date_obs),@date_obs)), @date_obs) AS DATE) AND T1.LABEL_YYYY_MM_DD <= @date_obs --7 jours de la semaine  
)
--select * from cte_jour_alim_list

--Temps de connexion

,SM_RES_SESSION_FACT as (

select IWD_RESOURCE_.RESOURCE_KEY,AGENT_FIRST_NAME,AGENT_LAST_NAME ,SUM(TOTAL_DURATION) as TEMPS_CONNEXION
from IWD_RESOURCE_ left join IWD_SM_RES_SESSION_FACT  on IWD_RESOURCE_.RESOURCE_KEY=IWD_SM_RES_SESSION_FACT.RESOURCE_KEY
                   inner join cte_jour_alim_list temp on IWD_SM_RES_SESSION_FACT.START_DATE_TIME_KEY=temp.DATE_TIME_KEY
				                                     --and IWD_SM_RES_SESSION_FACT.END_DATE_TIME_KEY=temp.DATE_TIME_KEY
--where IWD_RESOURCE_.RESOURCE_KEY is not null
group by AGENT_FIRST_NAME,AGENT_LAST_NAME,IWD_RESOURCE_.RESOURCE_KEY
)
--SELECT * from SM_RES_SESSION_FACT order by jour_alim asc 

--Temps de pause

,SM_RES_STATE_REASON_FACT as (
select IWD_RESOURCE_.RESOURCE_KEY
	  ,AGENT_FIRST_NAME,AGENT_LAST_NAME
	  --Temps passé en prod
	  ,SUM(case when SOFTWARE_REASON_VALUE in ('1','4','7') then TOTAL_DURATION else 0 end)                                                    as Temps_Tot
	  ,SUM(case when REASON_TYPE_CODE='SOFTWARE' and SOFTWARE_REASON_KEY='Pause' and SOFTWARE_REASON_VALUE='1' then TOTAL_DURATION else 0 end )as  TEMPS_PAUSE
	  ,SUM(case when RS.RESOURCE_STATE_KEY='6' then TOTAL_DURATION else 0 end)                                                                 as  POST_APPEL_MOY
from IWD_RESOURCE_ left join IWD_SM_RES_STATE_REASON_FACT rs_fact on IWD_RESOURCE_.RESOURCE_KEY=rs_fact.RESOURCE_KEY
                   left join IWD_RESOURCE_STATE_REASON RR on rs_fact.RESOURCE_STATE_REASON_KEY= RR.RESOURCE_STATE_REASON_KEY 
                   left join IWD_RESOURCE_STATE RS on rs_fact.RESOURCE_STATE_KEY=RS.RESOURCE_STATE_KEY
                   inner join cte_jour_alim_list temp on rs_fact.START_DATE_TIME_KEY=temp.DATE_TIME_KEY
				                                     --and rs_fact.END_DATE_TIME_KEY=temp.DATE_TIME_KEY
/*where REASON_TYPE_CODE='SOFTWARE'
  and SOFTWARE_REASON_KEY='Pause'
  and SOFTWARE_REASON_VALUE='1'*/
  --and IWD_RESOURCE_.RESOURCE_KEY is not null
GROUP BY AGENT_FIRST_NAME,AGENT_LAST_NAME,IWD_RESOURCE_.RESOURCE_KEY
)
--select * from SM_RES_STATE_REASON_FACT
 
,INTERACTION_RESOURCE_FACT as (

select IWD_RESOURCE_.RESOURCE_KEY
      ,AGENT_FIRST_NAME
	  ,AGENT_LAST_NAME 
	  ,SUM(TALK_DURATION)                                                                                                                               as TEMPS_PASSE_EN_APPEL 
	  ,SUM(case when MEDIA_NAME='voice' and INTERACTION_TYPE ='inbound' and RESOURCE_TYPE='agent' and TALK_DURATION !=0  then 1 else 0 end)             as NB_APPELS_TRAITES_ENTRANTS
	  ,sum(case when MEDIA_NAME='voice' and INTERACTION_TYPE='inbound' and RESOURCE_TYPE='agent' and TALK_DURATION !=0 then TALK_DURATION else 0 end)   as duration_APPELS_TRAITES_ENTRANTS
	  ,SUM(case when MEDIA_NAME='voice' and INTERACTION_TYPE='outbound' and RESOURCE_TYPE='agent' and TALK_DURATION !=0  then 1 else 0 end)             as NB_APPELS_ABOUTIS_SORTANTS
	  ,sum(case when MEDIA_NAME='voice' and INTERACTION_TYPE='outbound' and RESOURCE_TYPE='agent' and TALK_DURATION !=0 then TALK_DURATION else 0 end)  as duration_APPELS_ABOUTIS_SORTANTS
	  ,SUM(case when MEDIA_NAME='chat_crm' and RESOURCE_TYPE='agent' and TALK_DURATION !=0  then 1 else 0 end)                                          as NB_CHAT_TRAITES
	  ,sum(case when MEDIA_NAME='chat_crm' and RESOURCE_TYPE='agent' and TALK_DURATION !=0  then TALK_DURATION else 0 end)                              as duration_CHAT_TRAITES
	  ,SUM(case when MEDIA_NAME='case_crm' and RESOURCE_TYPE='agent' and TALK_DURATION !=0  then 1 else 0 end)                                          as NB_REQ_TRAITES
	  ,sum(case when MEDIA_NAME='case_crm' and RESOURCE_TYPE='agent' and TALK_DURATION !=0  then TALK_DURATION else 0 end)                              as duration_REQ_TRAITES
	  ,SUM(case when MEDIA_NAME='email_CRM' then 1 else 0 end)                                                                                        as NB_MAILS_TRAITES
	  ,sum(case when MEDIA_NAME='email_CRM' then TALK_DURATION else 0 end)                                                                              as duration_MAILS_TRAITES
  
from IWD_RESOURCE_ left join IWD_INTERACTION_RESOURCE_FACT rs_fact on IWD_RESOURCE_.RESOURCE_KEY=rs_fact.RESOURCE_KEY
                   inner join IWD_MEDIA_TYPE on  IWD_MEDIA_TYPE.MEDIA_TYPE_KEY=rs_fact.MEDIA_TYPE_KEY 
				   inner join IWD_INTERACTION_TYPE on IWD_INTERACTION_TYPE.INTERACTION_TYPE_KEY = rs_fact.INTERACTION_TYPE_KEY
				   inner join cte_jour_alim_list temp on rs_fact.START_DATE_TIME_KEY=temp.DATE_TIME_KEY
				                                    -- and rs_fact.END_DATE_TIME_KEY=temp.DATE_TIME_KEY
/*where MEDIA_NAME in ('voice','chat_crm','email_crm')
 and  INTERACTION_TYPE in ('inbound','outbound')*/ --and IWD_RESOURCE_.RESOURCE_KEY is not null
 group by AGENT_FIRST_NAME,AGENT_LAST_NAME,IWD_RESOURCE_.RESOURCE_KEY

)
--select * from INTERACTION_RESOURCE_FACT

,INTERACTION_RESOURCE_FACT_Cumul as (
 select RESOURCE_KEY
       ,AGENT_FIRST_NAME
	   ,AGENT_LAST_NAME 
	   ,SUM(TEMPS_PASSE_EN_APPEL)                                                                                                                                                          as TEMPS_PASSE_EN_APPEL
	   ,SUM(NB_APPELS_TRAITES_ENTRANTS)                                                                                                                                                    as NB_APPELS_TRAITES_ENTRANTS
	   ,SUM(duration_APPELS_TRAITES_ENTRANTS)	                                                                                                                                           as duration_APPELS_TRAITES_ENTRANTS
	   ,IIF(SUM(TEMPS_PASSE_EN_APPEL)=0,0,																                               
	   cast(3600*(SUM(NB_APPELS_TRAITES_ENTRANTS)+ SUM(NB_APPELS_ABOUTIS_SORTANTS)) as float)/SUM(TEMPS_PASSE_EN_APPEL))                                                                   as NB_APPELS_HEURE
	   ,SUM(NB_APPELS_ABOUTIS_SORTANTS)			                                                                                                                                           as NB_APPELS_ABOUTIS_SORTANTS
	   ,SUM(duration_APPELS_ABOUTIS_SORTANTS)	                                                                                                                                           as duration_APPELS_ABOUTIS_SORTANTS
	   ,IIF((SUM(NB_APPELS_TRAITES_ENTRANTS)+SUM(NB_APPELS_ABOUTIS_SORTANTS))=0,0,(SUM(TEMPS_PASSE_EN_APPEL)/(SUM(NB_APPELS_TRAITES_ENTRANTS)+SUM(NB_APPELS_ABOUTIS_SORTANTS))))           as DMC_MOY
	   ,SUM(NB_CHAT_TRAITES)					                                                                                                                                           as NB_CHAT_TRAITES
	   ,SUM(duration_CHAT_TRAITES)                                                                                                                                                         as duration_CHAT_TRAITES
	   ,IIF(SUM(duration_CHAT_TRAITES)=0,0,3600*SUM(NB_CHAT_TRAITES)/SUM(duration_CHAT_TRAITES))                                                                                           as NB_CHAT_HEURE
	   ,SUM(NB_REQ_TRAITES)                                                                                                                                                                as NB_REQ_TRAITES
	   ,SUM(duration_REQ_TRAITES)                                                                                                                                                          as duration_REQ_TRAITES
	   ,IIF(SUM(duration_REQ_TRAITES)=0,0,3600*SUM(NB_REQ_TRAITES)/SUM(duration_REQ_TRAITES))                                                                                              as NB_REQ_TRAITES_HEURE
	   ,SUM(NB_MAILS_TRAITES)					                                                                                                                                           as NB_MAILS_TRAITES
	   ,SUM(duration_MAILS_TRAITES)                                                                                                                                                        as duration_MAILS_TRAITES
	   ,IIF(SUM(duration_MAILS_TRAITES)=0,0,3600*SUM(NB_MAILS_TRAITES)/SUM(duration_MAILS_TRAITES))                                                                                        as NB_MAILS_TRAITES_HEURE
 from INTERACTION_RESOURCE_FACT
 group by AGENT_FIRST_NAME,AGENT_LAST_NAME,RESOURCE_KEY
)

--select * from INTERACTION_RESOURCE_FACT_Cumul where jour_alim='2018-07-13' --and RESOURCE_KEY='13'
		 

/*********************_*_Requête_*_*********************/

select distinct  --coalesce(temp.jour_alim,a.jour_alim,b.jour_alim,c.jour_alim) jour_alim
                 coalesce(a.RESOURCE_KEY,b.RESOURCE_KEY,c.RESOURCE_KEY)                                           AS  RESOURCE_KEY
                 --,coalesce(a.AGENT_FIRST_NAME,b.AGENT_FIRST_NAME,c.AGENT_FIRST_NAME)                               AS AGENT_FIRST_NAME
				 --,coalesce(a.AGENT_LAST_NAME,b.AGENT_LAST_NAME,c.AGENT_LAST_NAME)                                  AS AGENT_LAST_NAME
				 ,coalesce(CONCAT(a.AGENT_LAST_NAME,'  ',a.AGENT_FIRST_NAME),CONCAT(b.AGENT_LAST_NAME,'  ',b.AGENT_FIRST_NAME),CONCAT(c.AGENT_LAST_NAME,'  ',c.AGENT_FIRST_NAME)) AS NOM_COMPLET
                 ,ISNULL(a.TEMPS_CONNEXION,0)                                                                      AS TEMPS_CONNEXION
				 ,ISNULL(DIFFERENCE(a.Temps_CONNEXION,b.Temps_Tot),0)                                              AS TEMPS_PASSE_PROD
				 ,ISNULL(IIF(a.TEMPS_CONNEXION=0,0,DIFFERENCE(a.Temps_CONNEXION,b.Temps_Tot)/a.TEMPS_CONNEXION),0) AS TAUX_OCCUPATION
                 ,ISNULL(b.TEMPS_PAUSE,0)					                                                       AS TEMPS_PAUSE
				 ,ISNULL(b.POST_APPEL_MOY,0)                                                                       AS POST_APPEL_MOY
				 ,ISNULL(c.TEMPS_PASSE_EN_APPEL,0)                                                                 AS TEMPS_PASSE_EN_APPEL 
			     ,ISNULL(c.NB_APPELS_TRAITES_ENTRANTS,0)		                                                   AS NB_APPELS_TRAITES_ENTRANTS
			     ,ISNULL(c.NB_APPELS_HEURE,0)					                                                   AS NB_APPELS_HEURE
			     ,ISNULL(c.NB_APPELS_ABOUTIS_SORTANTS,0)		                                                   AS NB_APPELS_ABOUTIS_SORTANTS
				 ,ISNULL(c.DMC_MOY,0)                                                                              AS DMC_MOY
			     ,ISNULL(c.NB_CHAT_TRAITES,0)					                                                   AS NB_CHAT_TRAITES
			     ,ISNULL(c.NB_CHAT_HEURE,0)					                                                       AS NB_CHAT_HEURE
			     ,ISNULL(c.NB_REQ_TRAITES,0)					                                                   AS NB_REQ_TRAITES
			     ,ISNULL(c.NB_REQ_TRAITES_HEURE,0) 			                                                       AS NB_REQ_TRAITES_HEURE 
			     ,ISNULL(c.NB_MAILS_TRAITES,0)				                                                       AS NB_MAILS_TRAITES
			     ,ISNULL(c.NB_MAILS_TRAITES_HEURE,0)			                                                   AS NB_MAILS_TRAITES_HEURE
--select distinct temp.jour_alim
from IWD_RESOURCE_ 
--cross join cte_jour_alim_list temp
left join SM_RES_SESSION_FACT a on (IWD_RESOURCE_.RESOURCE_KEY=a.RESOURCE_KEY)
left join SM_RES_STATE_REASON_FACT b on (IWD_RESOURCE_.RESOURCE_KEY=b.RESOURCE_KEY )
left join INTERACTION_RESOURCE_FACT_Cumul c on (IWD_RESOURCE_.RESOURCE_KEY=c.RESOURCE_KEY )
--where coalesce(CONCAT(a.AGENT_LAST_NAME,'  ',a.AGENT_FIRST_NAME),CONCAT(b.AGENT_LAST_NAME,'  ',b.AGENT_FIRST_NAME),CONCAT(c.AGENT_LAST_NAME,'  ',c.AGENT_FIRST_NAME)) in (select [Value] from [SIDP_DataFactory].dbo.FnSplit(@NOM_COMPLET, ','))
End;