﻿CREATE PROCEDURE [dbo].[GIM_IS_INT_CAST_POSSIBLE]
(
	@V VARCHAR(MAX)
)
AS
BEGIN
    DECLARE @R INT;
    IF COALESCE(LTRIM(RTRIM(@V)), '') = ''
        RETURN 1;
    IF ISNUMERIC(@V) = 0
        RETURN 0;
    BEGIN TRY
        SET @R = CAST(@V AS INT);
        RETURN 1;
    END TRY
    BEGIN CATCH
        RETURN 0;
    END CATCH
END
GO