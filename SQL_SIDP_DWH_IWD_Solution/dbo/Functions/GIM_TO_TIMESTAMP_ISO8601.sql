﻿create function [dbo].[GIM_TO_TIMESTAMP_ISO8601](@V varchar(max)) returns datetime as 
begin
    declare @R datetime;
    if coalesce(ltrim(rtrim(@V)),'')=''
        return null;
    if isdate(@V)=0
        exec GIM_THROW;
  return convert(datetime, @V, 126);
end