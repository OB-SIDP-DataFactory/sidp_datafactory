﻿create function [dbo].[GIM_QUOTE](@V varchar(max)) returns varchar(max) as 
begin
  return case when @V is null then 'NULL' else '''' + replace(@V,'''','''''') + '''' end;
end