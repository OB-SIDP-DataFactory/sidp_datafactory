﻿CREATE TABLE [dbo].[DIM_ACTIVITE_EPARGNE] (
    [ID_DIM_ACTIVITE_EPARGNE] INT          IDENTITY (1, 1) NOT NULL,
    [ACTIVITE]                VARCHAR (50) NULL,
    [DAT_DEB]                 DATE         NOT NULL,
    [DAT_FIN]                 DATE         NOT NULL
);

