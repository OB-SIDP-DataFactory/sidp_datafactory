﻿CREATE TABLE [dbo].[DIM_CANAL_DISTRIBUTION] (
    [ID_DIM_CANAL_DISTRIBUTION] BIGINT         NOT NULL,
    [COD_CANL_DIST]             NVARCHAR (255) NULL,
    [LIBL_CANL_DIST]            NVARCHAR (255) NULL,
    [DAT_DEBT_VALD]             DATE           NOT NULL,
    [DAT_FIN_VALD]              DATE           NOT NULL
);

