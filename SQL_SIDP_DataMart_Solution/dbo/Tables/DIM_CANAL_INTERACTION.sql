﻿CREATE TABLE [dbo].[DIM_CANAL_INTERACTION] (
    [ID_DIM_CANAL_INTERACTION] BIGINT         NOT NULL,
    [COD_CANL_INTR]            NVARCHAR (255) NULL,
    [LIBL_CANL_INTR]           NVARCHAR (255) NULL,
    [DAT_DEBT_VALD]            DATE           NOT NULL,
    [DAT_FIN_VALD]             DATE           NOT NULL
);

