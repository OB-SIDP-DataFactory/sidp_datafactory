﻿CREATE TABLE [dbo].[PARAM_ALIM_DMT] (
    [NUMR_TRTM]     INT          IDENTITY (1, 1) NOT NULL,
    [DOMN]          VARCHAR (50) NOT NULL,
    [NOM_BATCH]     VARCHAR (50) NULL,
    [DAT_DEBT_TRTM] DATETIME     NULL,
    [DAT_VALD]      DATE         NOT NULL,
    [STATUT]        CHAR(2)  NOT NULL
);

