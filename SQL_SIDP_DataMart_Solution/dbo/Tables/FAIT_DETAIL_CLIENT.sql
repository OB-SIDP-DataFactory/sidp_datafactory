﻿CREATE TABLE [dbo].[FAIT_DETAIL_CLIENT] (
    [ID_DIM_TEMPS_DAT_OBSR]            INT          NOT NULL,
    [FLG_NOUV_CLNT]                    INT          NOT NULL,
    [FLG_ANCN_CLNT]                    INT          NOT NULL,
    [FLG_CLNT_STCK]                    INT          NOT NULL,
    [ANCNT_CLNT_MOIS]                  INT          NULL,
    [SCORE_PRE_ATTRIBUTION]            VARCHAR (10) NULL,
    [ID_DIM_TEMPS_DAT_ENTREE_RELATION] INT          NOT NULL,
    [ID_DIM_TEMPS_DAT_FIN_RELATION]    INT          NOT NULL,
    [ID_DIM_CLIENT]                    INT          NOT NULL,
    [ID_DIM_TYPE_CLIENT]               INT          NOT NULL,
    [ID_DIM_SEGMENT_LIFE]              INT          NOT NULL,
    [ID_DIM_ACTIVITE_CLIENT]           INT          NOT NULL
)
GO