﻿CREATE TABLE [dbo].[DIM_SEGMENT_GEOLIFE] (
    [ID_DIM_SEGMENT_GEOLIFE] INT            IDENTITY (1, 1) NOT NULL,
    [SF_SEGM_GEOL]           NVARCHAR (100) NULL,
    [DAT_DEB]                DATE           NOT NULL,
    [DAT_FIN]                DATE           NOT NULL
);

