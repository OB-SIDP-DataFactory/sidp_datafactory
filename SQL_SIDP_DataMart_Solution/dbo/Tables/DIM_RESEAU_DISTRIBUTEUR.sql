﻿CREATE TABLE [dbo].[DIM_RESEAU_DISTRIBUTEUR] (
    [ID_DIM_RESEAU_DISTRIBUTEUR] BIGINT         NOT NULL,
    [COD_RES_DIST]               NVARCHAR (255) NULL,
    [LIBL_RES_DIST]              NVARCHAR (255) NULL,
    [DAT_DEBT_VALD]              DATE           NOT NULL,
    [DAT_FIN_VALD]               DATE           NOT NULL
);

