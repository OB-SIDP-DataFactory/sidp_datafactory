﻿CREATE TABLE [dbo].[DIM_ACTIVITE_COMPTE] (
    [ID_DIM_ACTIVITE_COMPTE] INT          IDENTITY (1, 1) NOT NULL,
    [ACTIVITE]               VARCHAR (50) NULL,
    [DAT_DEB]                DATE         NOT NULL,
    [DAT_FIN]                DATE         NOT NULL
);

