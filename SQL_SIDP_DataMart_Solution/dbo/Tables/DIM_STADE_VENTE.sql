﻿CREATE TABLE [dbo].[DIM_STADE_VENTE] (
    [ID_DIM_STADE_VENTE] BIGINT         NOT NULL,
    [COD_STD_VENT]       NVARCHAR (255) NULL,
    [LIBL_STD_VENT]      NVARCHAR (255) NULL,
    [DAT_DEBT_VALD]      DATE           NOT NULL,
    [DAT_FIN_VALD]       DATE           NOT NULL
);

