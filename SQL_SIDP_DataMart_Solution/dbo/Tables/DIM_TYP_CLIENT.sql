﻿CREATE TABLE [dbo].[DIM_TYP_CLIENT] (
    [ID_DIM_TYP_CLIENT] INT            IDENTITY (1, 1) NOT NULL,
    [SF_COD_CIVL]       NVARCHAR (40)  NULL,
    [SEXE]              NVARCHAR (40)  NULL,
    [SF_PRFS_PCS_PRNC]  NVARCHAR (255) NULL,
    [LIB_CSP_NIV1]      NVARCHAR (255) NULL,
    [AGE]               INT            NULL,
    [DAT_DEB]           DATE           NOT NULL,
    [DAT_FIN]           DATE           NOT NULL
);

