﻿CREATE TABLE [dbo].[DIM_ENTITE_DISTRIBUTRICE] (
    [ID_DIM_ENTITE_DISTRIBUTRICE] BIGINT         NOT NULL,
    [COD_ENTT_DIST]               NVARCHAR (255) NULL,
    [LIBL_ENTT_DIST]              NVARCHAR (255) NULL,
    [DAT_DEBT_VALD]               DATE           NOT NULL,
    [DAT_FIN_VALD]                DATE           NOT NULL
);

