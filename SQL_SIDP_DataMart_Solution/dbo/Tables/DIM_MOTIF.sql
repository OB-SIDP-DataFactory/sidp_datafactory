﻿CREATE TABLE [dbo].[DIM_MOTIF] (
    [ID_DIM_MOTIF]  BIGINT         NOT NULL,
    [COD_MOTF]      NVARCHAR (255) NULL,
    [LIBL_MOTF]     NVARCHAR (255) NULL,
    [DAT_DEBT_VALD] DATE           NOT NULL,
    [DAT_FIN_VALD]  DATE           NOT NULL
);

