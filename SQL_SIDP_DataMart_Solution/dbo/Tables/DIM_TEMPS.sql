﻿CREATE TABLE [dbo].[DIM_TEMPS] (
    [ID_DIM_TEMPS]              INT          NOT NULL,
    [DATE]                      DATETIME     NOT NULL,
    [JOUR]                      CHAR (2)     NOT NULL,
    [JOUR_SUFFIXE]              VARCHAR (4)  NOT NULL,
    [JOUR_DE_SEMAINE]           VARCHAR (9)  NOT NULL,
    [JOUR_DE_SEMAINE_DANS_MOIS] TINYINT      NOT NULL,
    [JOUR_DE_ANNEE]             INT          NOT NULL,
    [SEMAINE_DE_ANNEE]          TINYINT      NOT NULL,
    [SEMAINE_DE_MOIS]           TINYINT      NOT NULL,
    [ISO_SEMAINE]               TINYINT      NOT NULL,
    [ISO_SEMAINE_DE_ANNEE]      CHAR (4)     NOT NULL,
    [MOIS]                      CHAR (2)     NOT NULL,
    [NOM_MOIS]                  VARCHAR (9)  NOT NULL,
    [MOIS_ANNEE]                VARCHAR (50) NULL,
    [TRIMESTRE]                 TINYINT      NOT NULL,
    [NOM_TRIMESTRE]             VARCHAR (6)  NOT NULL,
    [ANNEE]                     CHAR (4)     NOT NULL,
    [TOP_JOUR_OUVRE]            TINYINT      NOT NULL,
    [STANDARD_DATE]             DATE         NULL
);
GO
CREATE NONCLUSTERED INDEX IX_NC_DIM_TMP_STD_DTE ON dbo.DIM_TEMPS (STANDARD_DATE)
GO