﻿CREATE FUNCTION [dbo].[RETRIEVE_PARAM_EXTRACT]
(
	@DOMAIN nvarchar(50), 
	@BATCH_NAME nvarchar(50)
)
RETURNS datetime
WITh EXECUTE AS CALLER
AS
BEGIN
	-- Declare the return variable here
	DECLARE @LAST_EXTRACTION_DATE datetime

	-- Add the T-SQL statements to compute the return value here
	SELECT TOP 1
		@LAST_EXTRACTION_DATE = LAST_EXTRACTION_DATE
	FROM
		dbo.PARAM_EXTRACT
	WHERE
		1 = 1
		AND	DOMAIN = @DOMAIN
		AND	BATCH_NAME = @BATCH_NAME
		AND	FLAG = 'OK'
	ORDER BY
		LAST_EXTRACTION_DATE	DESC;
	
	-- Return the result of the function
	RETURN @LAST_EXTRACTION_DATE
END
GO