﻿CREATE TABLE [dbo].[EXT_OF_FLUX9](
	[Date_Traitement] [datetime] NOT NULL,
	[Account_Id] [nvarchar](18) NULL,
	[ID_FE_EQUIPMENT] [nvarchar](20) NULL,
	[Statut_Compte] [nvarchar](20) NULL,
	[DWHCPTCLO] [nvarchar](20) NULL,
	[DWHCPTDAC] [date] NULL
) ON [PRIMARY]

GO