﻿CREATE TABLE [dbo].[Param_Pilotage_extract] (
    [Id]             INT            IDENTITY (1, 1) NOT NULL,
    [ProcessingType] NVARCHAR (50)  NOT NULL,
    [domain]         NVARCHAR (50)  NOT NULL,
    [batch_name]     NVARCHAR (50)  NOT NULL,
    [variable_name]  NVARCHAR (255) NOT NULL,
    [variable_value] NVARCHAR (255) NOT NULL,
    [create_date]    DATE           NOT NULL,
    [update_date]    DATE           NULL,
    CONSTRAINT [PK_PARAM_Pilotage_extract] PRIMARY KEY CLUSTERED ([Id] ASC)
)
GO