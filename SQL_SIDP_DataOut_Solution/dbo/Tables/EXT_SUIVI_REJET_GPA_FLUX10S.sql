﻿CREATE TABLE [dbo].[EXT_SUIVI_REJET_GPA_FLUX10S] (
    [id]                        BIGINT         IDENTITY (1, 1) NOT NULL,
    [REJ_dateTraitement]        DATETIME2(7)   NULL,
    [REJ_codeProduit]           NVARCHAR (256) NULL,
    [REJ_LibelleProduit]        NVARCHAR (255) NULL,
    [REJ_ideGrc]                NVARCHAR (256) NULL,
    [REJ_IdeOpportuniteSF]      NVARCHAR (255) NULL,
    [REJ_IdeOpportuniteGRC]     NVARCHAR (255) NULL,
    [REJ_StadeDeVente]          VARCHAR (256)  NULL,
    [REJ_LastModifiedDate]      NVARCHAR (256) NULL,
    [REJ_ContactEmpSalutation]  NVARCHAR (256) NULL,
    [REJ_ContactEmpFirstName]   NVARCHAR (256) NULL,
    [REJ_ContactEmpLastName]    NVARCHAR (256) NULL,
    [REJ_ContactEmpBirthdate]   NVARCHAR (255) NULL,
    [REJ_ContactEmpEmail]       NVARCHAR (255) NULL,
    [REJ_ContactEmpMobilePhone] NVARCHAR (255) NULL,
    [REJ_Traite]                INT            NULL,
    [REJ_DateCorrection]        DATETIME       NULL
);

