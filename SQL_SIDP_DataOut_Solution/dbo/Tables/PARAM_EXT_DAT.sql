﻿CREATE TABLE [dbo].[PARAM_EXT_DAT] (
    [Id]         INT           IDENTITY (1, 1) NOT NULL,
    [batch_name] NVARCHAR (50) NOT NULL,
    [DAT_DEB]    DATE          NULL,
    [DAT_FIN]    DATE          NULL,
    [flag]       NCHAR (2)  NULL,
    CONSTRAINT [PK_PARAM_EXT_DAT] PRIMARY KEY CLUSTERED ([Id] ASC)
)
GO