﻿CREATE TABLE [dbo].[PARAM_EXTRACT] (
    [ID]                   INT           IDENTITY (1, 1) NOT NULL,
    [DOMAIN]               NVARCHAR (50) NOT NULL,
    [BATCH_NAME]           NVARCHAR (50) NOT NULL,
    [LAST_EXTRACTION_DATE] DATETIME      NOT NULL,
    [FLAG]                 NCHAR (2)  NOT NULL,
    CONSTRAINT [PK_PARAM_Extract] PRIMARY KEY CLUSTERED ([ID] ASC)
)
GO