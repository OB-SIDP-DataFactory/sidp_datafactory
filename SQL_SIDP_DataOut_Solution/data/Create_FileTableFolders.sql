﻿PRINT 'Executing Create_FileTableFolder DataExtracted scripts ...'
:setvar FileTableName DataExtracted
PRINT 'Executing Create_FileTableFolder GPA script ...'
:setvar FolderName GPA
:r .\Create_FileTableFolder.sql
PRINT 'Executing Create_FileTableFolder LAB script ...'
:setvar FolderName LAB
:r .\Create_LABFileTableFolders.sql
PRINT 'Executing Create_FileTableFolder OF script ...'
:setvar FolderName OF
:r .\Create_FileTableFolder.sql
PRINT 'Executing Create_FileTableFolder QS script ...'
:setvar FolderName QS
:r .\Create_FileTableFolder.sql
PRINT 'Executing Create_FileTableFolder SAB script ...'
:setvar FolderName SAB
:r .\Create_FileTableFolder.sql
PRINT 'Executing Create_FileTableFolder SF script ...'
:setvar FolderName SF
:r .\Create_FileTableFolder.sql