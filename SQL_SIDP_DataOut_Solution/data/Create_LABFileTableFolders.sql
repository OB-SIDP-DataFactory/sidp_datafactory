﻿USE [$(DataOutDatabaseName)] 
GO

IF NOT EXISTS (SELECT 1 FROM dbo.[$(FileTableName)] WHERE [name]='$(FolderName)')
BEGIN
	DECLARE @parentdir table(path hierarchyid not null);
	DECLARE @subdir_locator hierarchyid
	
	PRINT 'Creating LAB Folder ...'
	INSERT INTO [dbo].[$(FileTableName)] (name, is_directory, is_archive) 
	OUTPUT INSERTED.path_locator into @parentdir
	SELECT '$(FolderName)', 1, 0
	
	PRINT 'Creating LAB001 SubFolder ...'
	SELECT @subdir_locator = dbo.GetNewPathLocator(path) from @parentdir
	INSERT INTO [dbo].[$(FileTableName)] (name,path_locator,is_directory,is_archive) 
	VALUES ('$(FolderName)001', @subdir_locator, 1, 0);

	PRINT 'Creating LAB002 SubFolder ...'
	SELECT @subdir_locator = dbo.GetNewPathLocator(path) from @parentdir
	INSERT INTO [dbo].[$(FileTableName)] (name,path_locator,is_directory,is_archive) 
	VALUES ('$(FolderName)002', @subdir_locator, 1, 0);

	PRINT 'Creating LAB003 SubFolder ...'
	SELECT @subdir_locator = dbo.GetNewPathLocator(path) from @parentdir
	INSERT INTO [dbo].[$(FileTableName)] (name,path_locator,is_directory,is_archive) 
	VALUES ('$(FolderName)003', @subdir_locator, 1, 0);

	PRINT 'Creating LAB004 SubFolder ...'
	SELECT @subdir_locator = dbo.GetNewPathLocator(path) from @parentdir
	INSERT INTO [dbo].[$(FileTableName)] (name,path_locator,is_directory,is_archive) 
	VALUES ('$(FolderName)004', @subdir_locator, 1, 0);

	PRINT 'Creating LAB005 SubFolder ...'
	SELECT @subdir_locator = dbo.GetNewPathLocator(path) from @parentdir
	INSERT INTO [dbo].[$(FileTableName)] (name,path_locator,is_directory,is_archive) 
	VALUES ('$(FolderName)005', @subdir_locator, 1, 0);

	PRINT 'Creating LAB006 SubFolder ...'
	SELECT @subdir_locator = dbo.GetNewPathLocator(path) from @parentdir
	INSERT INTO [dbo].[$(FileTableName)] (name,path_locator,is_directory,is_archive) 
	VALUES ('$(FolderName)006', @subdir_locator, 1, 0);
END
GO