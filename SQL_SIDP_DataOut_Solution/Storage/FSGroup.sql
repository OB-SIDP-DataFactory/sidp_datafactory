﻿ALTER DATABASE [$(DatabaseName)]
    ADD FILEGROUP [FSGroup] CONTAINS FILESTREAM;
GO
ALTER DATABASE [$(DatabaseName)]
    ADD FILE ( NAME = 'SIDP_DataOut_FS', FILENAME = '$(DefaultDataPath)$(DefaultFilePrefix)_FS') TO FILEGROUP [FSGroup];
GO