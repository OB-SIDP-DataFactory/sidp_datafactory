﻿USE [$(DataOutDatabaseName)]
GO
DROP TABLE IF EXISTS [dbo].[DataExtracted]
GO
CREATE TABLE [dbo].[DataExtracted] AS FILETABLE FILESTREAM_ON [FSGroup]
WITH (FILETABLE_COLLATE_FILENAME = French_CI_AS, FILETABLE_DIRECTORY = N'DataExtracted');