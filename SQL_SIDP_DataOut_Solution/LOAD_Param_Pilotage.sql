﻿USE [$(DataOutDatabaseName)]  
GO
TRUNCATE TABLE [dbo].param_pilotage_extract 
GO
insert into dbo.param_pilotage_extract(ProcessingType,domain,batch_name,variable_name,variable_value,create_date,update_date)
values 
 ('extract','OF','PKGEX_SSIS_OF_SIDORA001','var_StageName','09',getdate(),getdate())
,('extract','OF','PKGEX_SSIS_OF_SIDORA001','var_StageName','10',getdate(),getdate())
,('extract','OF','PKGEX_SSIS_OF_SIDORA001','var_StageName','11',getdate(),getdate())
,('extract','OF','PKGEX_SSIS_OF_SIDORA001','var_CommercialOfferCode__c','OC80',getdate(),getdate())
,('extract','OF','PKGEX_SSIS_OF_SIDORA001','var_DistributorNetwork__c','01',getdate(),getdate())
,('extract','OF','PKGEX_SSIS_OF_SIDORA001','var_DistributorEntity__c','01',getdate(),getdate())
,('extract','OF','PKGEX_SSIS_OF_SIDORA001','var_DistributorEntity__c','02',getdate(),getdate())
,('extract','OF','PKGEX_SSIS_OF_SIDORA001','var_DistributorEntity__c','04',getdate(),getdate())
GO
