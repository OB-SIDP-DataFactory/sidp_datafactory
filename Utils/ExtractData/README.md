# Extract data from DB

## Extract-Data.ps1

1. Purpose
    - Generating data dynamically from SQLServer based on a configurtion table and saving the data in a CSV format files.
2. Configuration (in Config/config.json)
    - The variables to update in order to be able change the processing behavior :
        - SQLServer.ParameterServer : Name of Parameter Server (hostname or IP address).
        - SQLServer.ParameterDatabase : Name of Parameter Database.
        - SQLServer.ParameterTable : Name of Parameter Table.
        - SQLServer.LogsDatabase : Name of Log Database.
        - SQLServer.LogsTable : Name of Log Table.
3. How to use
    - In a powershell console, type :

    ```powershell
    .\Extract-Data.ps1;
    ```
    Or with a ProcessingFrequency Paramater (Default : "D", it filters data based on the column "ProcessingFrequency" value in the table SQLServer.ParameterTable) :
    ```powershell
    .\Extract-Data.ps1 "M";
    ```