﻿function Export-Data {
	Param
	(
		[Parameter(Mandatory)]
		[Alias("Configuration")] 
		$ConfigurationRecord
	)

	try {
		# Build Connectoin String from Configuration File Content
		$ConnectionString = "Data Source=$($ConfigFileContent.sqlserver.parameterServer); Database=$($ConfigFileContent.sqlserver.parameterDatabase); Trusted_Connection=True;";

		# Defining output folder path
		if([string]::IsNullOrWhiteSpace($ConfigurationRecord.DestinationPath)){
			$ConfigurationRecord.DestinationPath = $DataFolder;
		}
		Set-Logger -Origin $ConfigurationRecord.ExtractName.Substring(0, $ConfigurationRecord.ExtractName.IndexOf(".")) -Content "Extract folder : $($ConfigurationRecord.DestinationPath)";

		try {
			#Create folder if it does not exists
			if(!(Test-Path "$($ConfigurationRecord.DestinationPath)")){New-Item "$($ConfigurationRecord.DestinationPath)" -ItemType Directory};
		}
		catch {
			Set-Logger -Origin $ConfigurationRecord.ExtractName.Substring(0, $ConfigurationRecord.ExtractName.IndexOf(".")) -Content "$('Creating destination folder Exception: {0}' -f $_.Exception.ToString())";
		}
		finally {
		}

		# Defining output folder path
		if($ConfigurationRecord.Timestamped -eq 1)
		{
			$ExternalOperationOutputFile = "$($ConfigurationRecord.DestinationPath)\$ProcessingDate-$($ConfigurationRecord.ExtractName)";
		}
		else
		{
			$ExternalOperationOutputFile = "$($ConfigurationRecord.DestinationPath)\$($ConfigurationRecord.ExtractName)";
		}
		
		$OperationOutputFile = "$DataFolder\$($ConfigurationRecord.ExtractName)";

		$InsertedLogRecordID = $(Add-LogEntry $ConfigurationRecord.ExtractName)[1];
		
		[System.Int32]$bufferSize = 1048576;
		[System.Text.Encoding]$encoding = [System.Text.Encoding]::UTF8;
		$streamWriter = new-object System.IO.StreamWriter -ArgumentList $OperationOutputFile,$false,$encoding,$bufferSize;
		
		$Connection = New-Object System.Data.SqlClient.SqlConnection $ConnectionString
		$SqlCommand = New-Object System.Data.SqlClient.SqlCommand
		$SqlCommand.Connection = $Connection
		if($($ConfigurationRecord.ObjectNameOrQuery) -like "*SELECT*FROM*"){
			$Query = "$($ConfigurationRecord.ObjectNameOrQuery)"
		}
		else{
			$Query = "SELECT * FROM $($ConfigurationRecord.ObjectNameOrQuery)"
		}

		Set-Logger -Origin $ConfigurationRecord.ExtractName.Substring(0, $ConfigurationRecord.ExtractName.IndexOf(".")) -Content "$($Query)";

		$SqlCommand.CommandText = $Query;
		$SqlCommand.CommandTimeout = 1800;
		$Connection.Open();
		$reader = $SqlCommand.ExecuteReader();
		
		# Initialze the array that hold the values
		$array = @()
		for ( $i = 0 ; $i -lt $reader.FieldCount; $i++ )
		{
			$array += @($i)
		}
		
		$QuotedExtract = $ConfigurationRecord.Quoted
		$Separator = $ConfigurationRecord.Separator
		
		# Write Header
		if($QuotedExtract -eq 1){
			$streamWriter.Write("$($ConfigurationRecord.QuoteCharacter)" + $reader.GetName(0))
		}
		else{
			$streamWriter.Write($reader.GetName(0))
		}
		for ( $i = 1; $i -lt $reader.FieldCount; $i ++)
		{		
			if($QuotedExtract -eq 1){
				$streamWriter.Write("$($ConfigurationRecord.QuoteCharacter)"+"$($Separator)"+"$($ConfigurationRecord.QuoteCharacter)"+"$($reader.GetName($i))")
			}
			else{
				$streamWriter.Write("$($Separator)$($reader.GetName($i))")
			}
		}
		if($QuotedExtract -eq 1){
			$streamWriter.Write("$($ConfigurationRecord.QuoteCharacter)")
		}
		$streamWriter.WriteLine("") # Close the header line
		
		$RowCount = 0;
		
		while ($reader.Read())
		{
			# get the values;
			$fieldCount = $reader.GetValues($array);
			# add quotes if the values have a comma
			for ($i = 0; $i -lt $array.Length; $i++)
			{
				$array[$i] = $array[$i] -replace "`n", " ";
				$array[$i] = $array[$i] -replace "`r", " ";
				if ($array[$i].ToString().Contains("$($ConfigurationRecord.Separator)"))
				{
					$array[$i] = '"' + $array[$i].ToString() + '"';
				}
			}
			
			if($QuotedExtract -eq 1){
				$newRow = "$($ConfigurationRecord.QuoteCharacter)" + [string]::Join("$($ConfigurationRecord.QuoteCharacter)"+"$($Separator)"+"$($ConfigurationRecord.QuoteCharacter)", $array) + "$($ConfigurationRecord.QuoteCharacter)";
			}
			else{

				$newRow = [string]::Join("$($Separator)", $array);
			}
			
			$streamWriter.WriteLine($newRow);
			if(($RowCount % 1000000) -eq 0){
				Set-Logger -Origin $ConfigurationRecord.ExtractName.Substring(0, $ConfigurationRecord.ExtractName.IndexOf(".")) -Content "Loading $($RowCount) records from database into $OperationOutputFile finished.";
			}
			$RowCount += 1;
		}
	
		Set-Logger -Origin $ConfigurationRecord.ExtractName.Substring(0, $ConfigurationRecord.ExtractName.IndexOf(".")) -Content "Loading records from database into $OperationOutputFile finished.";
		Set-Logger -Origin $ConfigurationRecord.ExtractName.Substring(0, $ConfigurationRecord.ExtractName.IndexOf(".")) -Content "$RowCount records loaded.";
		Update-LogEntry -RowCount $RowCount -LogId $InsertedLogRecordID;
		$streamWriter.Close();
		$reader.Close();
		$Connection.Close();

		Move-Item -Path $OperationOutputFile -Destination $ExternalOperationOutputFile -Force -ErrorAction SilentlyContinue;
	}
	catch {
		Set-Logger -Origin $ConfigurationRecord.ExtractName.Substring(0, $ConfigurationRecord.ExtractName.IndexOf(".")) -Content "$('Database Exception: {0}`n{1}' -f $ConnectionString, $_.Exception.ToString())";
		$streamWriter.Close();
		$reader.Close();
		$Connection.Close();
	}
	finally {
	}

	Set-Logger -Origin $ConfigurationRecord.ExtractName.Substring(0, $ConfigurationRecord.ExtractName.IndexOf(".")) -Content "Collecting data for $TableName Finished.";
}