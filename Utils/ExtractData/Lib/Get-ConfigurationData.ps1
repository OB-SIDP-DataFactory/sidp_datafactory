function Get-ConfigurationData {
	Param
	(
        [Parameter(Mandatory)]
        [Alias("ProcessingFrequency")] 
        [string]$Frequency = "D"
	)

    try {

		# Build Connectoin String from Configuration File Content
        $ConnectionString = "Data Source=$($ConfigFileContent.sqlserver.parameterServer); Database=$($ConfigFileContent.sqlserver.parameterDatabase); Trusted_Connection=True;";
        

		$Connection = New-Object System.Data.SqlClient.SqlConnection $ConnectionString
        $SqlCommand = New-Object System.Data.SqlClient.SqlCommand
        $SqlCommand.Connection = $Connection
        
        $SqlCommand.CommandText = "SELECT * FROM $($ConfigFileContent.sqlserver.parameterDatabase)..$($ConfigFileContent.sqlserver.parameterTable) WHERE ProcessingFrequency = '$Frequency'"
		
		# Initializing Parameters Array
		$ParametersArray = @{}
        
		# Opening connection to configuration database
		$Connection.Open();
        $reader = $SqlCommand.ExecuteReader();

        $RowCount = 0;
        
        # Initialze the array that hold the values
        $array = @()
        for ( $i = 0 ; $i -lt $reader.FieldCount; $i++ )
        {
            $array += @($i)
        }
		Set-Logger -Origin "Extract-Data" -Content "$($array.Length)";
        
        while ($reader.Read())
        {
			$ParametersArray[$RowCount] = @{}
                    
            # get the values
            for ($i = 0; $i -lt $array.Length; $i++)
            {
				$ParametersArray[$RowCount]["$($reader.GetName($i))"] = $reader.GetValue($i)
            }

            $RowCount += 1;
        }
		
		Set-Logger -Origin "Extract-Data" -Content "Loading configuration from database finished.";
        Set-Logger -Origin "Extract-Data" -Content "$RowCount records loaded.";

		Set-Logger -Origin "Extract-Data" -Content "Collecting configuration data Finished.";
    }
    catch {
        Set-Logger -Origin "Extract-Data" -Content "$('Exception while trying to import configuration parameters: `n{1}' -f $_.Exception.ToString())";
    }
    finally {
        $reader.Close();
        $Connection.Close();
    }
	return $ParametersArray;
}