﻿function Update-LogEntry {
    Param
    (
        [Alias("RowCount")] 
        $ExtractedRows,
        [Alias("LogId")] 
        $ExtractLogId
    )

	# Build Connectoin String from Configuration File Content
	$LogConnectionString = "Data Source=$($ConfigFileContent.sqlserver.parameterServer); Database=$($ConfigFileContent.sqlserver.logsDatabase); Trusted_Connection=True;";
	
    try {
        Set-Logger -Origin $ConfigurationRecord.ExtractName.Substring(0, $ConfigurationRecord.ExtractName.IndexOf(".")) -Content "Updating Log entry for $ExtractFileName...";
		$LogConnection = New-Object System.Data.SqlClient.SqlConnection $LogConnectionString
		$LogSqlCommand = New-Object System.Data.SqlClient.SqlCommand
		$LogSqlCommand.Connection = $LogConnection
		
		$OperationDateTime = Get-Date -UFormat "%Y-%m-%d %H:%M:%S";

		$LogQuery = "UPDATE $($ConfigFileContent.sqlserver.logsDatabase).dbo.$($ConfigFileContent.sqlserver.logsTable) 
		SET ProcessingEndDate = CAST('$OperationDateTime' AS DATETIME2), NbRecordsExtracted = $ExtractedRows, ProcessingStatus = 'OK' 
		WHERE Id = $ExtractLogId"
		Set-Logger -Origin $ConfigurationRecord.ExtractName.Substring(0, $ConfigurationRecord.ExtractName.IndexOf(".")) -Content "$($LogQuery)";
		$LogSqlCommand.CommandText = $LogQuery
        
        $LogConnection.Open();
		$LogSqlCommand.ExecuteScalar();

        Set-Logger -Origin $ConfigurationRecord.ExtractName.Substring(0, $ConfigurationRecord.ExtractName.IndexOf(".")) -Content "Log entry updated for $ExtractFileName";
    }
    catch {
        Set-Logger -Origin $ConfigurationRecord.ExtractName.Substring(0, $ConfigurationRecord.ExtractName.IndexOf(".")) -Content "$('Exception while trying to update log entry: `n{1}' -f $_.Exception.ToString())";
    }
    finally {
        $LogConnection.Close();
    }
}