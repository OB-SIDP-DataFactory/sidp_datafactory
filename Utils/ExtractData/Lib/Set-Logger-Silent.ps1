﻿function Set-Logger {
    Param
    (
        [Parameter(Mandatory)]
        [Alias("Content")] 
        [string]$LogContent,
        [Alias("Origin")] 
        [string]$LogOrigin = 'Log',
        [Alias("WriteToFile")] 
        [boolean]$LogToFile = $true,
        [Alias("WriteHost")] 
        [boolean]$LogToScreen = $true
    )

    $LogFilePath = "$LogFolder\$(Get-Date -UFormat "%Y%m%d%H")_$LogOrigin.log";

    try {
        $OperationDateTime = $(Get-Date -Format o).Substring(0,24);
        $OperationLog = "$OperationDateTime : $LogContent";
        
        if ($LogToFile) {
            $OperationLog | Add-Content $LogFilePath;
        }
    }
    catch {
        # exit 1
    }
}