Param
(
	[string]$ProcessingFrequency="D",
	$MaxThreads = 5
)

# Get Processing Date and time
$ProcessingDate = Get-Date -UFormat "%Y%m%d%H%M%S";

# Define Work Folder Path
$RootFolder = (Get-Item -Path ".\").FullName;

# Define Data Folder Path
$DataFolder = "$RootFolder\Data";
#Create data folder if it does not exists.
if(!(Test-Path $DataFolder)){
	Set-Logger -Origin "Extract-Data" -Content "Creating Data Folder $DataFolder";
	New-Item "$DataFolder" -ItemType Directory;
}

# Define Log Folder Path
$LogFolder = "$RootFolder\Log";
#Create log folder if it does not exists.
if(!(Test-Path $LogFolder)){
	Set-Logger -Origin "Extract-Data" -Content "Creating Log Folder $LogFolder";
	New-Item "$LogFolder" -ItemType Directory;
}

# Define Library Folder Path.
$LibraryFolder = "$RootFolder\Lib";

# Define Configuration File Path.
$ConfigFilePath = "$RootFolder\Config\config.json";
$ConfigFileContent = $(Get-Content -Path $ConfigFilePath -Raw | ConvertFrom-Json);

. $LibraryFolder\Set-Logger.ps1;
. $LibraryFolder\Get-ConfigurationData.ps1;
. $LibraryFolder\Export-Data.ps1;
. $LibraryFolder\Add-LogEntry.ps1;
. $LibraryFolder\Update-LogEntry.ps1;

Set-Logger -Origin "Extract-Data" -Content "Start Processing (with frequency $ProcessingFrequency, $MaxThreads Parallel threads) ... ";

try
{
    Set-Logger -Origin "Extract-Data" -Content "Gathering configuration data started...";
    $ParametersArray = $(Get-ConfigurationData -ProcessingFrequency $ProcessingFrequency).Values.Where{$_.Enabled -eq $true};
    Set-Logger -Origin "Extract-Data" -Content "Gathering configuration data finished.";

    Set-Logger -Origin "Extract-Data" -Content "$($ParametersArray.Count) configuration records found.";
    $ProcessingBlock = {
	    Param(
            $RootFolder,
            $ConfigFileContent,
		    $ConfigurationRecord
	    )
        $LibraryFolder = "$RootFolder\Lib";
        $LogFolder = "$RootFolder\Log";
        $DataFolder = "$RootFolder\Data";
        . $LibraryFolder\Set-Logger.ps1;
        . $LibraryFolder\Export-Data.ps1;
        . $LibraryFolder\Add-LogEntry.ps1;
        . $LibraryFolder\Update-LogEntry.ps1;

	    Set-Logger -Origin $ConfigurationRecord.ExtractName.Substring(0, $ConfigurationRecord.ExtractName.IndexOf(".")) -Content "Configuration Record n� $($ConfigurationRecord.ExtractId)";
	    Set-Logger -Origin $ConfigurationRecord.ExtractName.Substring(0, $ConfigurationRecord.ExtractName.IndexOf(".")) -Content "Data Collection started...";
	    try{
		    Export-Data -Configuration $ConfigurationRecord;
	    }
	    catch{
		    Set-Logger -Origin $ConfigurationRecord.ExtractName.Substring(0, $ConfigurationRecord.ExtractName.IndexOf(".")) -Content "Error : $($Error[0].exception.GetBaseException().Message)";
	    }
	    Set-Logger -Origin $ConfigurationRecord.ExtractName.Substring(0, $ConfigurationRecord.ExtractName.IndexOf(".")) -Content "Data Collection finished.";
	    Start-Sleep -Seconds 1;
    }
    #Remove all jobs
    Get-Job | Remove-Job

    #Start the jobs. Max 5jobs running simultaneously.
    foreach($ParameterArray in $ParametersArray){
	    While ($(Get-Job -state running).count -ge $MaxThreads){
		    Start-Sleep -Seconds 1;
	    }
	    Start-Job -Scriptblock $ProcessingBlock -ArgumentList $RootFolder, $ConfigFileContent, $ParameterArray | Out-Null;
    }
    #Wait for all jobs to finish.
    While ($(Get-Job -State Running).count -gt 0){
	    Start-Sleep -Seconds 1;
    }
    #Get information from each job.
    foreach($job in Get-Job){
	    $info = Receive-Job -Id ($job.Id);
    }
    #Remove all jobs created.
    Get-Job | Remove-Job
}
catch
{
    Set-Logger -Origin "Extract-Data" -Content "Error : $($Error[0].exception.GetBaseException().Message)";
    exit 1
}

Set-Logger -Origin "Extract-Data" -Content "Finished Processing.";