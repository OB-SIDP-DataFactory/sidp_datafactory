﻿SELECT
	I.object_id,
	OBJECT_NAME(I.object_id),
	I.name,
	'DROP INDEX '+OBJECT_NAME(IUS.object_id)+'.' + I.name + ';' AS Drop_Index_Query,
	user_seeks, user_scans, user_lookups, user_updates,
	last_user_seek, last_user_scan, last_user_lookup,last_user_update,
	system_seeks, system_scans, system_lookups, system_updates,
	last_system_seek, last_system_scan, last_system_lookup,last_system_update,
	I.*,
	O.*
FROM
	sys.indexes	AS	I	WITH(NOLOCK)
	JOIN	sys.dm_db_index_usage_stats	AS	IUS	WITH(NOLOCK)
		ON	I.index_id = IUS.index_id
			AND	IUS.OBJECT_ID = I.OBJECT_ID
    JOIN sys.objects	AS	O	WITH(NOLOCK)
		ON IUS.OBJECT_ID = O.OBJECT_ID
WHERE
	1 = 1
	AND	I.name IS NOT NULL
	AND	I.is_primary_key = 0 --This line excludes primary key constarint
    AND	I.is_unique = 0 --This line excludes unique key constarint
    AND	user_updates + system_updates <> 0 -- This line excludes indexes SQL Server hasn’t done any work with
    AND	user_seeks + user_scans + user_lookups + system_seeks + system_scans + system_lookups = 0
ORDER BY
    user_updates DESC