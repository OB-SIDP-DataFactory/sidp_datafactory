#-------------------------------------------------------------------------------------------------#
# Call the PowerShell script and pass the arguments
# .\SF_DescribeObjects.ps1 "account;case;opportunity;opportunitycontactrole;opportunityfieldhistory;user;task;emailmessage;livechattranscript;profile;contact;recordtype" "DEV"
#-------------------------------------------------------------------------------------------------#

#-------------------------------------------------------------------------------------------------#
# Script parameters
#-------------------------------------------------------------------------------------------------#
[CmdletBinding()]
param (
    [string]$SFDescribeObjects,
    [string]$SFEnv
)

#-------------------------------------------------------------------------------------------------#
# Function Write-Log
#-------------------------------------------------------------------------------------------------#
Function Write-Log
{
    Param (
        [string]$logOutput
    )
    Write-Output $logOutput
    Add-content $logFile -value $logOutput
}

#-------------------------------------------------------------------------------------------------#
# Variables
#-------------------------------------------------------------------------------------------------#
$scriptHome = Split-Path $MyInvocation.MyCommand.Path
$scriptName = $MyInvocation.MyCommand.Name
$scriptName = $scriptName.Substring(0, ($scriptName.Length - 4))

$currentLogDate = Get-Date -format yyyyMMddHHmmss
$logFile = "$($scriptHome)\$($scriptName)_$($currentLogDate).log"
$resultFile = "$($scriptHome)\$($scriptName)_$($SFEnv)_$($currentLogDate).csv"

#-------------------------------------------------------------------------------------------------#
# Start script
#-------------------------------------------------------------------------------------------------#
Write-Log "Starting $($scriptName) ..."
Write-Log "Log available in $($logFile)"

try
{
	$SFApiVersion="v46.0";

    if ( "$($SFEnv)" -eq "DEV" )
    {
        $SFDomain="https://test.salesforce.com";
	    $SFConsumerKey = "3MVG9w8uXui2aB_pj2OLKLUa2ETIUyO746rNpL2W3K0ytYz6iQUBDm8AsBTqx42oP9_FZEkl3kYksFDZE1I2g";
        $SFConsumerSecret = "5494683081769735358";
        $SFRefreshTokenKey = "5Aep861tlMDNuVXI5pA_YN9R66vfp2Xwk_hRW4seZNblrto6zVTB0hv6ZLA0ykzLBvQcWqReIdnfQW48gZsk.dI";
    }
	
    if ( "$($SFEnv)" -eq "PPD" )
    {
        $SFDomain="https://test.salesforce.com";
	    $SFConsumerKey = "3MVG96mGXeuuwTZgfgv2HHK2T5lPnAMXoBxaKjJZo_rbSVQZIeXWS8aY.pCtoJn9whzEjrBLRSWvGpfuTHuLu";
        $SFConsumerSecret = "628301745801010361";
        $SFRefreshTokenKey = "5Aep8611MDY00mymq6OzdT2TCLPAiBveUzlTfYX9YGN2Qf_6ljqPblK7JwuUR6QiCcHTL1Q0iSGwXqNXyKX8ua.";
    }
 
    if ( "$($SFEnv)" -eq "SITR3" )
    {
        $SFDomain="https://test.salesforce.com";
	    $SFConsumerKey = "3MVG9X0_oZyBSzHrnzENhROatHFteBau2Z9TxzQNpSr908ePOZeA6hEILT0sJoOXU5PLFdlYNQFiijWM6RdrJ";
        $SFConsumerSecret = "6928304021844234818";
        $SFRefreshTokenKey = "5Aep861Q11ycGgw9UllZq1G7MYM6E4z1LmeEa82RD8Jfow8OmmXukuwutlc_gpElDfIztlgz8L4jHCXE0nbbVze";
    }

    if ( "$($SFEnv)" -eq "PRD" )
    {
        $SFDomain="https://login.salesforce.com";
	    $SFConsumerKey = "3MVG98_Psg5cppyYWacwP.LonHY5GTxB.O8n.Q7L4QVS8u4JYZFisrLUj6f1JFJRjQlh56drGL.FTip_dLrpW";
        $SFConsumerSecret = "3856924705062705799";
        $SFRefreshTokenKey = "5Aep861D4x38iD8mDLDZBv5XcS5bC8i16EbpuVPG6YSanKSQhyqUhm.J6Qoy_zIiC1Uy2MOWcv8SE9kv_ZEG5JA";
    }

	
	#NEW

	if ( "$($SFEnv)" -eq "Homol" )
    {
        $SFDomain="https://orangebank--homolr3.cs80.my.salesforce.com";
	    $SFConsumerKey = "3MVG9rKhT8ocoxGkPdSEUBFzU_fig9i5YqzoN2e76clRbYGSsRl_73oAtAImvUlxzU3i6JoRGgl5a.CC4OWdN";
        $SFConsumerSecret = "4580209181804502828";
        $SFRefreshTokenKey = "5Aep861LWFV7t8HdC8G9E.h0Mf1g020tvBruozIM8YCoyGuUTZ1POa7QdooMBbVTWpcbhil4JYtcKBFAlAZz7Yv";
    }

	if ( "$($SFEnv)" -eq "DEVR5" )
    { 
        $SFDomain="https://orangebank--devr5.cs105.my.salesforce.com";
	    $SFConsumerKey = "3MVG9LzKxa43zqdKmeTTNjkqgD27paW2fsTiZM7SMtD9MDfdioGqV.Lh5vJQ.t6xoISqAAYaBVQSc0CwZ7TfE";
        $SFConsumerSecret = "4390452397645179595";
        $SFRefreshTokenKey = "5Aep861TOZx2uFTje4cqu2K49A3XnyKNFfFKAf3dBqJrwEGODz28Z6tbk1FW_cZEEHPssGPQN2NvGQTS6WTX9tB";
    }
	
	if ( "$($SFEnv)" -eq "prepro" )
    { 
        $SFDomain="https://orangebank--preprod.cs80.my.salesforce.com";
	    $SFConsumerKey = "3MVG9rKhT8ocoxGkPdSEUBFzU_SDEtcQCO5TtOLoJrxTK2sX96FtQ7IZPAW2ULlQRu90Hmgt1hHqqXzFIMrBj";
        $SFConsumerSecret = "7578695768871641641";
        $SFRefreshTokenKey = "5Aep861LWFV7t8HdC_QRic75ba_7iu65tO60iKslJCP8GO77Sg85UDdJGXtDAkcFPxgqtYaNJsup5sjUHZSjK22";
    }

  
    # Init WebClient with default proxy credentials
    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12;
    $browser = New-Object System.Net.WebClient;
    $browser.Proxy.Credentials =[System.Net.CredentialCache]::DefaultNetworkCredentials;

    # Refresh token
    $refreshtokenurl = "$($SFDomain)/services/oauth2/token";
    $refreshtokenparams = @{
        grant_type="refresh_token";
        client_id="$($SFConsumerKey)";
        client_secret="$($SFConsumerSecret)";
        refresh_token="$($SFRefreshTokenKey)";
    };

    Write-Log "Refresh new oAuth2 token";
    $refreshtokenresponse = (Invoke-RestMethod -Uri $refreshtokenurl -Method POST -Body $refreshtokenparams);
    Write-Log "Access token retrieved";

    # Init resultFile
    Add-content $resultFile -value "ObjectName|FieldIndex|FieldName|FieldLabel|Type|Length|Custom|CompoundFieldName|ControllerName|ReferenceTo|RelationshipName"

    # Loop on objects to describe
    $describeobjecturl = "$($refreshtokenresponse.instance_url)/services/data/$($SFApiVersion)/sobjects/#object#/describe";
    foreach ( $SFObject in $SFDescribeObjects.Split(";") )
    {
        # Invoke-RestMethod to describe object
        $url = $describeobjecturl -replace "#object#", "$($SFObject)";
        $params = @{
            Accept="application/xml";
            Authorization="Bearer $($refreshtokenresponse.access_token)"
        };

        Write-Log "Invoke-RestMethod : '$($url)'"
        $response = (Invoke-RestMethod -Uri $url -Method GET -Headers $params);

        # Parse xml response and fill resultFile
        Write-Log "Parsing response"
        $i = 0;
        foreach ( $field in $response.SelectNodes("//fields") )
        {
            $i = $i + 1;
            Add-content $resultFile -value "$($SFObject)|$($i)|$($field.name)|$($field.label)|$($field.type)|$($field.length)|$($field.custom)|$($field.compoundFieldName)|$($field.controllerName)|$($field.referenceTo)|$($field.relationshipName)"
        }
    }
}
catch
{
    Write-Log "Error : $($Error[0].exception.GetBaseException().Message)"
    exit 1
}
