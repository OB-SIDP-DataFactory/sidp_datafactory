# Get Processing Date and time
$ProcessingDate = Get-Date -UFormat "%Y-%m-%d %H:%M:%S";

# Define Work Folder Path
$RootFolder = (Get-Item -Path ".\").FullName;

# Define Log Folder Path
$LogFolder = "$RootFolder\Log";
#Create log folder if it does not exists.
if(!(Test-Path $LogFolder)){New-Item "$LogFolder" -ItemType Directory};

# Define Library Folder Path.
$LibraryFolder = "$RootFolder\Lib";

# Define Configuration File Path.
$ConfigFilePath = "$RootFolder\Config\config.json";
$ConfigFileContent = $(Get-Content -Path $ConfigFilePath -Raw | ConvertFrom-Json);

. $LibraryFolder\Set-Logger.ps1;
. $LibraryFolder\Get-Environments.ps1;
. $LibraryFolder\Get-Tables.ps1;
. $LibraryFolder\Get-MetaDataFromSalesForce.ps1;
. $LibraryFolder\Get-MetaDataFromSIDP.ps1;
. $LibraryFolder\Add-LogEntry.ps1;
. $LibraryFolder\Update-LogEntry.ps1;

Set-Logger -Content "Start Processing..." -Origin "GetMetaData" -Folder $RootFolder;

try
{
    Set-Logger -Content "Gathering Environments data started..." -Origin "GetMetaData" -Folder $RootFolder;
    $EnvironmentsArray = Get-Environments;
    Set-Logger -Content "Gathering Environments data finished." -Origin "GetMetaData" -Folder $RootFolder;

    Set-Logger -Content "$($EnvironmentsArray.Count) Environments Records found." -Origin "GetMetaData" -Folder $RootFolder;

	for ($EnvironmentIndex = 0; $EnvironmentIndex -lt $EnvironmentsArray.Count; $EnvironmentIndex++){
		Set-Logger -Content "Environment Record n� $($EnvironmentIndex + 1)" -Origin "GetMetaData" -Folder $RootFolder;

		ForEach($Key in $EnvironmentsArray[$EnvironmentIndex].Keys){
			Set-Logger -Content "$Key : $($EnvironmentsArray[$EnvironmentIndex][$Key])" -Origin "GetMetaData" -Folder $RootFolder -WriteHost $false;
		}

		Set-Logger -Content "Data Collection started..." -Origin "GetMetaData" -Folder $RootFolder;

		if($EnvironmentsArray[$EnvironmentIndex]['IsEnabled'] -eq $true){
			try{
				Set-Logger -Content "Gathering Tables data started..." -Origin "GetMetaData" -Folder $RootFolder;
				$TablesArray = Get-Tables -EnvironmentIdentifier $EnvironmentsArray[$EnvironmentIndex]['Id'];
				Set-Logger -Content "Gathering Tables data finished." -Origin "GetMetaData" -Folder $RootFolder;
		    
				Set-Logger -Content "$($TablesArray.Count) Tables Records found." -Origin "GetMetaData" -Folder $RootFolder;

				if($EnvironmentsArray[$EnvironmentIndex]['SourceSystem'] -eq "SalesForce"){
					Get-MetaDataFromSalesForce -Environment $EnvironmentsArray[$EnvironmentIndex] -Tables $TablesArray -Date $ProcessingDate -Folder $RootFolder;
				}
				else{
					if($EnvironmentsArray[$EnvironmentIndex]['SourceSystem'] -eq "SIDP"){
						Get-MetaDataFromSIDP -Environment $EnvironmentsArray[$EnvironmentIndex] -Tables $TablesArray -Date $ProcessingDate -Folder $RootFolder;
					}
				}
			}
			catch
			{
				Set-Logger -Content "Error : $($Error[0].exception.GetBaseException().Message)" -Origin "GetMetaData" -Folder $RootFolder;
			}
		}
	}
}
catch
{
    Set-Logger -Content "Error : $($Error[0].exception.GetBaseException().Message)" -Origin "GetMetaData" -Folder $RootFolder;
    exit 1
}

Set-Logger -Content "Finished Processing." -Origin "GetMetaData" -Folder $RootFolder;