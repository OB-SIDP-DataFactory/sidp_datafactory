function Get-Environments {
    try {

		# Build Connectoin String from Configuration File Content
        $ConnectionString = "Data Source=$($ConfigFileContent.SQLServer.ReferenceServer); Database=$($ConfigFileContent.SQLServer.ReferenceDatabase); Trusted_Connection=True;";
        

		$Connection = New-Object System.Data.SqlClient.SqlConnection $ConnectionString
        $SqlCommand = New-Object System.Data.SqlClient.SqlCommand
        $SqlCommand.Connection = $Connection
        
        $SqlCommand.CommandText = "SELECT * FROM $($ConfigFileContent.SQLServer.ReferenceDatabase)..$($ConfigFileContent.SQLServer.ReferenceEnvironmentsTable)"
		
		# Initializing Environments Array
		$EnvironmentsArray = @{}
        
		# Opening connection to configuration database
		$Connection.Open();
        $reader = $SqlCommand.ExecuteReader();

        $RowCount = 0;
        
        # Initialze the array that hold the values
        $array = @()
        for ( $i = 0 ; $i -lt $reader.FieldCount; $i++ )
        {
            $array += @($i)
        }
		Write-Host $array.Length
        
        while ($reader.Read())
        {
			$EnvironmentsArray[$RowCount] = @{}
                    
            # get the values
            for ($i = 0; $i -lt $array.Length; $i++)
            {
				$EnvironmentsArray[$RowCount]["$($reader.GetName($i))"] = $reader.GetValue($i)
            }

            $RowCount += 1;
        }
		
		Set-Logger -Content "Loading configuration from environments database finished." -Origin "GetMetaData" -Folder $RootFolder;
        Set-Logger -Content "$RowCount records loaded." -Origin "GetMetaData" -Folder $RootFolder;

		Set-Logger -Content "Collecting environments Finished." -Origin "GetMetaData" -Folder $RootFolder;
    }
    catch {
        Set-Logger -Content $("Exception while trying to import environments : `n{1}" -f $_.Exception.ToString()) -Origin "GetMetaData" -Folder $RootFolder;
    }
    finally {
        $reader.Close();
        $Connection.Close();
    }
	return $EnvironmentsArray;
}