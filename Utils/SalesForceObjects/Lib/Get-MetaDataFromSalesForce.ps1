function Get-MetaDataFromSalesForce {
	Param
	(
		[Alias("Folder")] 
		[string]$RootFolder,
		[Alias("Date")] 
		[string]$ProcessingDate,
		[Alias("Environment")]
		$SalesForceEnvironment,
		[Alias("Tables")]
		$SalesForceEnvironmentTables
	)
	# Collect SalesForce Environment Connection String
	$SalesForceEnvironmentConnectionString = $SalesForceEnvironment['ConnectionString']

	$SalesForceDomain = $SalesForceEnvironmentConnectionString.Split(" ")[0];
	$SalesForceConnectionString = $SalesForceEnvironmentConnectionString.Split(" ")[1].Replace(",",";");
	$SalesForceConsumerKey = $SalesForceConnectionString.Split(";")[0].Split("=")[1].Replace('"',"");
	$SalesForceConsumerSecret = $SalesForceConnectionString.Split(";")[1].Split("=")[1].Replace('"',"");
	$SalesForceRefreshTokenKey = $SalesForceConnectionString.Split(";")[2].Split("=")[1].Replace('"',"");
	$SalesForceEnvironmentId = $SalesForceEnvironment['Id'];
	$SalesForceEnvironmentName = $SalesForceEnvironment['EnvironmentName'];

	try {
		$ConnectionStringGlobal = "Data Source=$($ConfigFileContent.SQLServer.ReferenceServer); Database=$($ConfigFileContent.SQLServer.ReferenceDatabase); Trusted_Connection=True;";
		$ConnectionGlobal = New-Object System.Data.SqlClient.SqlConnection $ConnectionStringGlobal
		$ConnectionGlobal.Open();
		$SqlCommandGlobal = New-Object System.Data.SqlClient.SqlCommand
		$SqlCommandGlobal.Connection = $ConnectionGlobal

		# Init WebClient with default proxy credentials
		[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12;
		$Browser = New-Object System.Net.WebClient;
		$Browser.Proxy.Credentials = [System.Net.CredentialCache]::DefaultNetworkCredentials;

		# Refresh token
		$RefreshTokenUrl = "$($SalesForceDomain)/services/oauth2/token";
		$RefreshTokenParameters = @{
			'grant_type'    = "refresh_token";
			'client_id'     = "$($SalesForceConsumerKey)";
			'client_secret' = "$($SalesForceConsumerSecret)";
			'refresh_token' = "$($SalesForceRefreshTokenKey)";
		};

		Set-Logger -Content "Refresh new oAuth2 token" -Folder $RootFolder -Origin "GetSalesForceMetaData";
		$RefreshTokenResponse = (Invoke-RestMethod -Uri $RefreshTokenUrl -Method POST -Body $RefreshTokenParameters -Proxy 'http://proxy-server-b.intra.groupama.fr:8080' -ProxyUseDefaultCredentials);
		Set-Logger -Content "Access token retrieved" -Folder $RootFolder -Origin "GetSalesForceMetaData";
		$instanceUrl = $($RefreshTokenResponse.instance_url)

		$RequestUrl = "$instanceUrl/services/data/";

		$RequestUrlParameters = @{
			Authorization = "Bearer $($RefreshTokenResponse.access_token)";
			'Content-type' = "application/json";
			'X-PrettyPrint' = 1;
		};
                    
		Set-Logger -Content "Invoke-RestMethod : '$($RequestUrl)'" -Folder $RootFolder -Origin "GetSalesForceMetaData" -WriteHost $false;
		$Response = (Invoke-RestMethod -Uri $RequestUrl -Method GET -Headers $RequestUrlParameters -Proxy 'http://proxy-server-b.intra.groupama.fr:8080' -ProxyUseDefaultCredentials);

		$version = 0
		$versionUrl = ""
		$Response | %{if ($_.version -gt $version) {$version = $_.version; $versionUrl = $_.url}}
		$RootRequestUrl = "$($instanceUrl)$($versionUrl)"

		$RequestUrl = "$RootRequestUrl/sobjects/";
		Set-Logger -Content "Invoke-RestMethod : '$($RequestUrl)'" -Folder $RootFolder -Origin "GetSalesForceMetaData" -WriteHost $false;
		$Response = (Invoke-RestMethod -Uri $RequestUrl -Method GET -Headers $RequestUrlParameters -Proxy 'http://proxy-server-b.intra.groupama.fr:8080' -ProxyUseDefaultCredentials);
		$Response.sobjects | %{
			try {
				$TableName = $_.name.Replace("__c","");
				$TableExists = $false;
				for ($TableIndex = 0; $TableIndex -lt $SalesForceEnvironmentTables.Count; $TableIndex++){
					if($SalesForceEnvironmentTables[$TableIndex]['TableName'] -eq $TableName){
						$TableExists = $true
						$TableIdentifier = $SalesForceEnvironmentTables[$TableIndex]['Id']
						$Table = $SalesForceEnvironmentTables[$TableIndex]
					}
				}		
				
				if($TableExists){
					if($Table.IsEnabled -eq $true){
						$QueryGlobal = "";
						$QueryGlobal += "UPDATE $($ConfigFileContent.SQLServer.ReferenceDatabase).dbo.$($ConfigFileContent.SQLServer.ReferenceColumnsTable) 
						SET IsEnabled = 0 
						WHERE TableId = $TableIdentifier AND YEAR(ValidityEndDate) = '9999'
						"
						
						$SqlCommandGlobal.commandtext = $QueryGlobal;
						$SqlCommandGlobal.CommandTimeout = 0;
						$ReaderGlobal = $SqlCommandGlobal.executescalar();

						$RequestUrl = "$($instanceUrl)$($_.urls.describe)"
						Set-Logger -Content "Invoke-RestMethod : '$($RequestUrl)'" -Folder $RootFolder -Origin "GetSalesForceMetaData";
						#Set-Logger -Content "Invoke-RestMethod : '$($RequestUrl)'" -Folder $RootFolder -Origin "GetSalesForceMetaData" -WriteHost $false;
						$Response = (Invoke-RestMethod -Uri $RequestUrl -Method GET -Headers $RequestUrlParameters -Proxy 'http://proxy-server-b.intra.groupama.fr:8080' -ProxyUseDefaultCredentials);
				        $QueryGlobal = "";
						$Response.fields | %{
							$Fields = $_;
							$FieldName = $Fields.name -replace "'","''";
							$FieldLabel = $Fields.label -replace "'","''";
							$FieldType = $Fields.type -replace "'","''";
							if($FieldType -eq "boolean"){
								$FieldType = "bit";
							}
							elseif($FieldType -eq "date"){
								$FieldType = "datetime2(7)";
							}
							elseif($FieldType -eq "datetime"){
								$FieldType = "datetime2(7)";
							}
							elseif($FieldType -eq "id"){
								$FieldType = "varchar($($Fields.length))";
							}
							elseif($FieldType -eq "reference"){
								$FieldType = "varchar($($Fields.length))";
							}
							elseif($FieldType -eq "string"){
								$FieldType = "varchar($($Fields.length))";
							}
							elseif($FieldType -eq "textarea"){
								$FieldType = "varchar($($Fields.length))";
							}
							elseif($FieldType -eq "url"){
								$FieldType = "varchar($($Fields.length))";
							}
							elseif($FieldType -eq "double"){
								$FieldType += "($($Fields.precision),$($Fields.scale))";
							}
							else{
								$FieldType += "($($Fields.length))";
							}
							$QueryGlobal += "IF EXISTS(SELECT 1 FROM $($ConfigFileContent.SQLServer.ReferenceDatabase).dbo.$($ConfigFileContent.SQLServer.ReferenceColumnsTable) WHERE TableId = $TableIdentifier AND ColumnName = '$FieldName' AND YEAR(ValidityEndDate) = '9999') 
							BEGIN 
							UPDATE $($ConfigFileContent.SQLServer.ReferenceDatabase).dbo.$($ConfigFileContent.SQLServer.ReferenceColumnsTable) SET ColumnDataType = '$FieldType', IsEnabled = 1 WHERE TableId = $TableIdentifier AND ColumnName = '$FieldName' 
							END
							ELSE 
							BEGIN 
							INSERT INTO $($ConfigFileContent.SQLServer.ReferenceDatabase).dbo.$($ConfigFileContent.SQLServer.ReferenceColumnsTable) (TableId,ColumnName,ColumnDescription,ColumnDataType,ValidityStartDate) VALUES ($TableIdentifier,'$FieldName','$FieldLabel','$FieldType','$ProcessingDate') 
							END
							"
						}
						$SqlCommandGlobal.commandtext = $QueryGlobal;
						$SqlCommandGlobal.CommandTimeout = 0;
						$ReaderGlobal = $SqlCommandGlobal.executescalar();

						$QueryGlobal = "";
						$QueryGlobal += "UPDATE $($ConfigFileContent.SQLServer.ReferenceDatabase).dbo.$($ConfigFileContent.SQLServer.ReferenceColumnsTable) 
						SET ValidityEndDate = '$ProcessingDate' 
						WHERE TableId = $TableIdentifier AND IsEnabled = 0 
						"
						
						$SqlCommandGlobal.commandtext = $QueryGlobal;
						$SqlCommandGlobal.CommandTimeout = 0;
						$ReaderGlobal = $SqlCommandGlobal.executescalar();
					}
					else{
					}
				}
				else{
					$QueryGlobal = "";
					$QueryGlobal += "INSERT INTO $($ConfigFileContent.SQLServer.ReferenceDatabase).dbo.$($ConfigFileContent.SQLServer.ReferenceTablesTable) 
					([EnvironmentId], [TableName], [IsEnabled], [ValidityStartDate]) 
					VALUES ($SalesForceEnvironmentId,'$TableName', 0, CAST(N'2019-01-01T00:00:00.0000000' AS DateTime2)) 
					"
					
					$SqlCommandGlobal.commandtext = $QueryGlobal;
					$SqlCommandGlobal.CommandTimeout = 0;
					$ReaderGlobal = $SqlCommandGlobal.executescalar();
				}
			}
			catch {
				Set-Logger -Content "Query : $($QueryGlobal) `r`n Error : $($Error[0].exception.GetBaseException().Message)" -Folder $RootFolder -Origin "GetSalesForceMetaData";
			}
			finally {
			}
		}
		

		Set-Logger -Content "Updating SalesForce Metadata for $SalesForceEnvironmentName Finished." -Origin "GetMetaData" -Folder $RootFolder;
	}
	catch {
		$ReaderGlobal.Close();
		$ConnectionGlobal.Close();
		Set-Logger -Content "Query : $($Query) `r`n Error : $($Error[0].exception.GetBaseException().Message)" -Folder $RootFolder -Origin "GetSalesForceMetaData";
	}
	finally {
	}
}