# Get MetaData From SalesForce Environements (Sales and SIDP)

## Get-MetaData.ps1

1. Purpose
    - Generating data dynamically from SIDP SQLServer and SalesForce API based on a configurtion table and saving the data into SIDP Database.
2. Configuration (in Config/config.json)
    - The variables to update in order to be able change the processing behavior :
        - SQLServer.ReferenceServer : Name of Reference Server (hostname or IP address).
        - SQLServer.ReferenceDatabase : Name of Reference Database.
        - SQLServer.ReferenceEnvironmentsTable : Name of Environments Reference Table.
        - SQLServer.ReferenceTablesTable : Name of Environments Tables Table.
        - SQLServer.ReferenceColumnsTable : Name of Columns Reference Table.
        - SQLServer.LogsDatabase : Name of Log Database.
        - SQLServer.LogsTable : Name of Log Table.
3. How to use
    - In a powershell console, type :

    ```powershell
    .\Get-MetaData.ps1;
    ```