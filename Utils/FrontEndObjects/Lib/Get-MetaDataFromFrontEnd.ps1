function Get-MetaDataFromFrontEnd {
	Param
	(
		[Alias("Folder")] 
		[string]$RootFolder,
		[Alias("Date")] 
		[string]$ProcessingDate,
		[Alias("Environment")]
		$FEEnvironment,
		[Alias("Tables")]
		$FEEnvironmentTables
	)
#	# Collect SIDP Environment Connection String
	$FEEnvironmentConnectionString = "User Id=FRONT_RO;Password=readonly;Data Source=SL_R3"; #"Data Source=$($ConfigFileContent.SQLServer.ReferenceServer); Database=$($ConfigFileContent.SQLServer.ReferenceDatabase); Trusted_Connection=True;";;
	$FEEnvironmentId = $FEEnvironment['Id'];
	#$FEEnvironmentId = 4;
	$FEEnvironmentName = $FEEnvironment['EnvironmentName'];

     $dataSource = ("(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST={0})(PORT={1}))(CONNECT_DATA=(SERVICE_NAME={2})))" -f "obksvlrh09.res.intra.orangebank.com", 36080, "OBKH1");
     $FEEnvironmentConnectionString = "Data Source={0};User Id={1};Password={2}" -f $dataSource, "FRONT_RO", "readonly";	
	$ConnectionStringGlobal = "Data Source=$($ConfigFileContent.SQLServer.ReferenceServer); Database=$($ConfigFileContent.SQLServer.ReferenceDatabase); Trusted_Connection=True;";
	

	try {
		try {
        
		
            add-type -AssemblyName System.Data.OracleClient;
			$ConnectionFETables = New-Object System.Data.OracleClient.OracleConnection($FEEnvironmentConnectionString);
			$ConnectionFETables.Open();
			#$SqlCommandGlobalTables.Connection = $ConnectionFETables
			$QueryFETables = "SELECT DISTINCT TABLE_NAME  AS	TableName
									FROM ALL_TAB_COLUMNS
									WHERE OWNER='FRONT'
									AND TABLE_NAME NOT LIKE 'SYS%'
									ORDER BY TABLE_NAME
			";
			$SqlCommandFETables = $ConnectionFETables.CreateCommand();
			$SqlCommandFETables.CommandText = $QueryFETables;
			$SqlCommandFETables.CommandTimeout = 0;
			$ReaderGlobalTables = $SqlCommandFETables.ExecuteReader();
			# Initialze the array that hold the values
			$TablesArray = @()
			$Tables = @()
			for ($TableIndex = 0 ; $TableIndex -lt $ReaderGlobalTables.FieldCount; $TableIndex++)
			{
				$TablesArray += @($TableIndex)
			}

			while ($ReaderGlobalTables.Read()){
				# get the values;
				$fieldCount = $ReaderGlobalTables.GetValues($TablesArray);

				for ($TableIndex = 0; $TableIndex -lt $TablesArray.Length; $TableIndex++){
					$TableName = $TablesArray[$TableIndex];
					$Tables += $TableName;
					
				}
			}
 		}
		catch {
			Set-Logger -Content "Query : $($QueryGlobalTables) `r`n Error : $($Error[0].exception.GetBaseException().Message)" -Folder $RootFolder -Origin "GetSIDPMetaData";
		}
		finally {
			$ReaderGlobalTables.Close();
			$ConnectionFETables.Close();
		}
       
		$ConnectionGlobal = New-Object System.Data.SQLClient.SqlConnection $ConnectionStringGlobal
		$ConnectionGlobal.Open();
		$SqlCommandGlobal = New-Object System.Data.SQLClient.SqlCommand
		$SqlCommandGlobal.Connection = $ConnectionGlobal


		  
				

		for ($TableIndex = 0; $TableIndex -lt $Tables.Count; $TableIndex++)
		{
		
		
			try {
				$TableName = $Tables[$TableIndex];
				
				$TableExists = $false;
				
				for ($TablesIndex = 0; $TablesIndex -lt $FEEnvironmentTables.Count; $TablesIndex++){
				
					if($FEEnvironmentTables[$TablesIndex]['TableName'] -eq $TableName){
						$TableExists = $true
						$TableIdentifier = $FEEnvironmentTables[$TablesIndex]['Id']
						$Table = $FEEnvironmentTables[$TablesIndex]
						
					}
				}
				
				if($TableExists){
					if($Table.IsEnabled -eq $true){
						$QueryGlobal = "";
						
						$QueryGlobal += "UPDATE $($ConfigFileContent.SQLServer.ReferenceDatabase).dbo.$($ConfigFileContent.SQLServer.ReferenceColumnsTable) 
						SET IsEnabled = 0 
						WHERE TableId = $TableIdentifier AND YEAR(ValidityEndDate) = '9999'
						"
						
						$SqlCommandGlobal.commandtext = $QueryGlobal;
						$SqlCommandGlobal.CommandTimeout = 0;
						$ReaderGlobal = $SqlCommandGlobal.executescalar();
							
						##$ConnectionGlobalColumns = New-Object System.Data.SQLClient.SqlConnection $ConnectionStringGlobal
						##$ConnectionGlobalColumns.Open();
						##$SqlCommandGlobalColumns = New-Object System.Data.SQLClient.SqlCommand
						##$SqlCommandGlobalColumns.Connection = $ConnectionGlobalColumns

						add-type -AssemblyName System.Data.OracleClient;
						$ConnectionFETables = New-Object System.Data.OracleClient.OracleConnection($FEEnvironmentConnectionString);
						
						
						$ConnectionFETables.Open();
						$QueryFEColumns = "";
						$QueryFEColumns += "
												SELECT  OWNER AS NAME_SCHEMA, 
												TABLE_NAME AS TableName, 
												COLUMN_ID AS ID_COLUMN, 
												COLUMN_NAME AS ColumnName,
												DATA_TYPE, 
												DATA_PRECISION, 
												DATA_SCALE,
												CHAR_LENGTH,
											    NULLABLE, 
											    CASE  WHEN DATA_TYPE IN ('CHAR', 'VARCHAR2', 'NCHAR', 'NVARCHAR') 
														THEN DATA_TYPE || '(' || CHAR_LENGTH || ')'
														WHEN DATA_TYPE IN ('NUMBER') AND DATA_PRECISION IS NULL
														THEN DATA_TYPE || '(38,' || DATA_SCALE || ')'
														WHEN DATA_TYPE IN ('NUMBER') 
														THEN DATA_TYPE || '(' || DATA_PRECISION || ',' || DATA_SCALE || ')'
														ELSE DATA_TYPE
												END AS ColumnDataType
												FROM ALL_TAB_COLUMNS
												WHERE OWNER='FRONT'
												AND TABLE_NAME NOT LIKE 'SYS%'
												and TABLE_NAME='$TableName'
												ORDER BY TABLE_NAME, COLUMN_ID
						";
						

			 			$SqlCommandFEColumns = $ConnectionFETables.CreateCommand();
			            $SqlCommandFEColumns.CommandText = $QueryFEColumns;
		             	$SqlCommandFEColumns.CommandTimeout = 0;
		            	
						
						$ReaderGlobalColumns = $SqlCommandFEColumns.ExecuteReader();
                           
						# Initialze the array that hold the values
						$ColumnsArray = @()
						for ($ColumnIndex = 0 ; $ColumnIndex -lt $ReaderGlobalColumns.FieldCount; $ColumnIndex++ )
						{
							$ColumnsArray += @($ColumnIndex)

						}

						$QueryGlobal = "";
						while ($ReaderGlobalColumns.Read()){
							# get the values;
							$fieldCount = $ReaderGlobalColumns.GetValues($ColumnsArray);

							$FieldName = $($ColumnsArray[3]) -replace "'","''";
							$FieldLabel = $($ColumnsArray[2]) -replace "'","''";
							$FieldType = $($ColumnsArray[9]) -replace "'","''";
							$FieldType
							$FieldName

							


							$QueryGlobal += "IF EXISTS(SELECT 1 FROM $($ConfigFileContent.SQLServer.ReferenceDatabase).dbo.$($ConfigFileContent.SQLServer.ReferenceColumnsTable) WHERE TableId = $TableIdentifier AND ColumnName = '$FieldName' AND YEAR(ValidityEndDate) = '9999') 
							BEGIN 
							UPDATE $($ConfigFileContent.SQLServer.ReferenceDatabase).dbo.$($ConfigFileContent.SQLServer.ReferenceColumnsTable) SET IsEnabled = 1 WHERE TableId = $TableIdentifier AND ColumnName = '$FieldName' 
							END
							ELSE 
							BEGIN 
							INSERT INTO $($ConfigFileContent.SQLServer.ReferenceDatabase).dbo.$($ConfigFileContent.SQLServer.ReferenceColumnsTable) (TableId,ColumnName,ColumnDescription,ColumnDataType,ValidityStartDate) VALUES ($TableIdentifier,'$FieldName','$FieldLabel','$FieldType','$ProcessingDate') 
							END
							"
						}

						$ReaderGlobalColumns.Close();
						$ConnectionFETables.Close();

						$SqlCommandGlobal.commandtext = $QueryGlobal;
						$SqlCommandGlobal.CommandTimeout = 0;
						$ReaderGlobal = $SqlCommandGlobal.executescalar();

						$QueryGlobal = "";
						$QueryGlobal += "UPDATE $($ConfigFileContent.SQLServer.ReferenceDatabase).dbo.$($ConfigFileContent.SQLServer.ReferenceColumnsTable) 
						SET ValidityEndDate = '$ProcessingDate' 
						WHERE TableId = $TableIdentifier AND IsEnabled = 0 
						"
						
						$SqlCommandGlobal.commandtext = $QueryGlobal;
						$SqlCommandGlobal.CommandTimeout = 0;
						$ReaderGlobal = $SqlCommandGlobal.executescalar();
					}
					else{
					}
				}
				else{
					$QueryGlobal = "";
					$QueryGlobal += "INSERT INTO $($ConfigFileContent.SQLServer.ReferenceDatabase).dbo.$($ConfigFileContent.SQLServer.ReferenceTablesTable) 
					([EnvironmentId], [TableName], [IsEnabled], [ValidityStartDate]) 
					VALUES ($FEEnvironmentId,'$TableName', 0, CAST(N'2019-01-01T00:00:00.0000000' AS DateTime2)) 
					"
					
					$SqlCommandGlobal.commandtext = $QueryGlobal;
					$SqlCommandGlobal.CommandTimeout = 0;
					$ReaderGlobal = $SqlCommandGlobal.executescalar();
				}
			}
			catch {
				Set-Logger -Content "Query : $($QueryGlobalColumns) `r`n Error : $($Error[0].exception.GetBaseException().Message)" -Folder $RootFolder -Origin "GetSIDPMetaData";
			}
			finally {
			}
		}

		Set-Logger -Content "Updating FE Metadata for $FEEnvironmentName Finished." -Origin "GetMetaData" -Folder $RootFolder;
	}
	catch {
		Set-Logger -Content "Query : $($QueryGlobal) `r`n Error : $($Error[0].exception.GetBaseException().Message)" -Folder $RootFolder -Origin "GetSIDPMetaData";
	}
	finally {
		$ReaderGlobal.Close();
		$ConnectionGlobal.Close();
	
	}
}

