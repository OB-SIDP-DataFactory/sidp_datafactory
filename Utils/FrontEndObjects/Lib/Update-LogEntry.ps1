﻿function Update-LogEntry {
    Param
    (
        [Alias("RowCount")] 
        $ExtractedRows,
        [Alias("LogId")] 
        $ExtractLogId
    )

	# Build Connectoin String from Configuration File Content
	$LogConnectionString = "Data Source=$($ConfigFileContent.sqlserver.parameterServer); Database=$($ConfigFileContent.sqlserver.logsDatabase); Trusted_Connection=True;";
	
    try {
        Set-Logger -Content "Updating Log entry for $ExtractFileName..." -Origin "Extract-Data";
		$LogConnection = New-Object System.Data.SqlClient.SqlConnection $LogConnectionString
		$LogSqlCommand = New-Object System.Data.SqlClient.SqlCommand
		$LogSqlCommand.Connection = $LogConnection
		
		$OperationDateTime = Get-Date -UFormat "%Y-%m-%d %H:%M:%S";

		$LogQuery = "UPDATE $($ConfigFileContent.sqlserver.logsDatabase).dbo.$($ConfigFileContent.sqlserver.logsTable) 
		SET ProcessingEndDate = CAST('$OperationDateTime' AS DATETIME2), NbRecordsExtracted = $ExtractedRows, ProcessingStatus = 'OK' 
		WHERE Id = $ExtractLogId"
		$LogQuery
		$LogSqlCommand.CommandText = $LogQuery
        
        $LogConnection.Open();
		$LogSqlCommand.ExecuteScalar();

        Set-Logger -Content "Log entry updated for $ExtractFileName" -Origin "Extract-Data";
    }
    catch {
        Set-Logger -Content $("Exception while trying to update log entry: `n{1}" -f $_.Exception.ToString()) -Origin "Extract-Data";
    }
    finally {
        $LogConnection.Close();
    }
}