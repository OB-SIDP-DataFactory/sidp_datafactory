function Get-MetaDataFromSIDP {
	Param
	(
		[Alias("Folder")] 
		[string]$RootFolder,
		[Alias("Date")] 
		[string]$ProcessingDate,
		[Alias("Environment")]
		$SIDPEnvironment,
		[Alias("Tables")]
		$SIDPEnvironmentTables
	)
	# Collect SIDP Environment Connection String
	$SIDPEnvironmentConnectionString = $SIDPEnvironment['ConnectionString'];
	$SIDPEnvironmentConnectionString

	$SIDPEnvironmentId = $SIDPEnvironment['Id'];
	$SIDPEnvironmentName = $SIDPEnvironment['EnvironmentName'];
	$ConnectionStringGlobal = "Data Source=$($ConfigFileContent.SQLServer.ReferenceServer); Database=$($ConfigFileContent.SQLServer.ReferenceDatabase); Trusted_Connection=True;";

	$ConnectionStringGlobal;
	try {
		try {
			$ConnectionGlobalTables = New-Object System.Data.SqlClient.SqlConnection $ConnectionStringGlobal
			$ConnectionGlobalTables.Open();
			$SqlCommandGlobalTables = New-Object System.Data.SqlClient.SqlCommand
			$SqlCommandGlobalTables.Connection = $ConnectionGlobalTables
			$QueryGlobalTables = "";
			$QueryGlobalTables += "
			SELECT DISTINCT
				O.name	AS	TableName
			FROM
				$($ConfigFileContent.SQLServer.ReferenceDatabase).dbo.REF_TRAITEMENT					AS	RT	WITH(NOLOCK)
				INNER JOIN	SIDP_DataHub.sys.all_objects			AS	O	WITH(NOLOCK)
					ON	O.name = RT.NOM_TABL
			WHERE
				1 = 1
				AND	RT.FLG_ACTIF = 1
				AND (O.name LIKE 'PV_FE_%' or O.name  like 'WK_FE%' )
				AND O.type = 'U'
			ORDER BY
				TableName
			"
			$SqlCommandGlobalTables.CommandText = $QueryGlobalTables;
			$SqlCommandGlobalTables.CommandTimeout = 0;
			$ReaderGlobalTables = $SqlCommandGlobalTables.ExecuteReader();
			# Initialze the array that hold the values
			$TablesArray = @()
			$Tables = @()
			for ($TableIndex = 0 ; $TableIndex -lt $ReaderGlobalTables.FieldCount; $TableIndex++)
			{
				$TablesArray += @($TableIndex)
			}

			while ($ReaderGlobalTables.Read()){
				# get the values;
				$fieldCount = $ReaderGlobalTables.GetValues($TablesArray);

				for ($TableIndex = 0; $TableIndex -lt $TablesArray.Length; $TableIndex++)
				{
					$TableName = $TablesArray[$TableIndex];
					$Tables += $TableName;
				}
			}
 		}
		catch {
			Set-Logger -Content "Query : $($QueryGlobalTables) `r`n Error : $($Error[0].exception.GetBaseException().Message)" -Folder $RootFolder -Origin "GetSIDPMetaData";
		}
		finally {
			$ReaderGlobalTables.Close();
			$ConnectionGlobalTables.Close();
		}
       
		$ConnectionGlobal = New-Object System.Data.SqlClient.SqlConnection $ConnectionStringGlobal
		$ConnectionGlobal.Open();
		$SqlCommandGlobal = New-Object System.Data.SqlClient.SqlCommand
		$SqlCommandGlobal.Connection = $ConnectionGlobal

		for ($TableIndex = 0; $TableIndex -lt $Tables.Count; $TableIndex++){
			try {
				$TableName = $Tables[$TableIndex];
				$TableName
				$TableExists = $false;
				for ($TablesIndex = 0; $TablesIndex -lt $SIDPEnvironmentTables.Count; $TablesIndex++){
					if($SIDPEnvironmentTables[$TablesIndex]['TableName'] -eq $TableName){
						$TableExists = $true
						$TableIdentifier = $SIDPEnvironmentTables[$TablesIndex]['Id']
						$Table = $SIDPEnvironmentTables[$TablesIndex]
					}
				}
				
				if($TableExists){
					if($Table.IsEnabled -eq $true){
						$QueryGlobal = "";
						$QueryGlobal += "UPDATE $($ConfigFileContent.SQLServer.ReferenceDatabase).dbo.$($ConfigFileContent.SQLServer.ReferenceColumnsTable) 
						SET IsEnabled = 0 
						WHERE TableId = $TableIdentifier AND YEAR(ValidityEndDate) = '9999'
						"
						
						$SqlCommandGlobal.commandtext = $QueryGlobal;
						$SqlCommandGlobal.CommandTimeout = 0;
						$ReaderGlobal = $SqlCommandGlobal.executescalar();
							
						$ConnectionGlobalColumns = New-Object System.Data.SqlClient.SqlConnection $ConnectionStringGlobal
						$ConnectionGlobalColumns.Open();
						$SqlCommandGlobalColumns = New-Object System.Data.SqlClient.SqlCommand
						$SqlCommandGlobalColumns.Connection = $ConnectionGlobalColumns

						$QueryGlobalColumns = "";
						$QueryGlobalColumns += "
							SELECT DISTINCT
								O.name	AS	TableName,
								C.name							AS	ColumnName,
								ISNULL(CEP.value, '')			AS	ColumnDescription,
								UPPER(
									IIF(
										ST.name	IN	('date'),
										ST.name,
										IIF(
											ST.name	LIKE '%nvarchar%',
											ST.name + '(' + CAST(COALESCE(NULLIF(C.max_length/2,-1),8000) AS VARCHAR(5))+ ')',
											IIF(
												ST.name	LIKE '%varchar%',
												ST.name + '(' + CAST(COALESCE(NULLIF(C.max_length,-1),8000) AS VARCHAR(5))+ ')',
												IIF(
													ST.name	IN ('decimal'),
													ST.name + '(' + CAST(C.precision AS VARCHAR(5)) + ',' + CAST(C.scale AS VARCHAR(5)) + ')',
													ST.name
												)
											)
										)
									)
								)								AS	ColumnDataType
							FROM
								SIDP_DataFactory.dbo.REF_TRAITEMENT					AS	RT	WITH(NOLOCK)
								INNER JOIN	SIDP_DataHub.sys.all_objects			AS	O	WITH(NOLOCK)
									ON	O.name = RT.NOM_TABL
								INNER JOIN	SIDP_DataHub.sys.all_columns			AS	C	WITH(NOLOCK)
									ON	C.object_id = O.object_id
								INNER JOIN SIDP_DataHub.sys.systypes				AS	ST	WITH(NOLOCK)
									ON	ST.xusertype = C.user_type_id
								LEFT JOIN	SIDP_DataHub.sys.extended_properties	AS	CEP	WITH(NOLOCK)
									ON	CEP.name = 'MS_Description'
										AND	CEP.major_id = O.object_id
										AND	CEP.minor_id = C.column_id
							WHERE
								1 = 1
								AND	RT.FLG_ACTIF = 1
								AND (O.name LIKE 'PV_FE_%' or O.name  like 'WK_FE%' )
								AND O.name = '$TableName'
								AND O.type = 'U'
							ORDER BY
								TableName
						"
						
						
						$SqlCommandGlobalColumns.CommandText = $QueryGlobalColumns;
						$SqlCommandGlobalColumns.CommandTimeout = 0;
						$ReaderGlobalColumns = $SqlCommandGlobalColumns.ExecuteReader();
        
						# Initialze the array that hold the values
						$ColumnsArray = @()
						for ($ColumnIndex = 0 ; $ColumnIndex -lt $ReaderGlobalColumns.FieldCount; $ColumnIndex++ )
						{
							$ColumnsArray += @($ColumnIndex)
						}

						$QueryGlobal = "";
						while ($ReaderGlobalColumns.Read()){
							# get the values;
							$fieldCount = $ReaderGlobalColumns.GetValues($ColumnsArray);

							$FieldName = $($ColumnsArray[1]) -replace "'","''";
							$FieldLabel = $($ColumnsArray[2]) -replace "'","''";
							$FieldType = $($ColumnsArray[3]) -replace "'","''";
							$QueryGlobal += "IF EXISTS(SELECT 1 FROM $($ConfigFileContent.SQLServer.ReferenceDatabase).dbo.$($ConfigFileContent.SQLServer.ReferenceColumnsTable) WHERE TableId = $TableIdentifier AND ColumnName = '$FieldName' AND YEAR(ValidityEndDate) = '9999') 		
							BEGIN 
							UPDATE $($ConfigFileContent.SQLServer.ReferenceDatabase).dbo.$($ConfigFileContent.SQLServer.ReferenceColumnsTable) SET IsEnabled = 1 WHERE TableId = $TableIdentifier AND ColumnName = '$FieldName' 
							END
							ELSE 
							BEGIN 
							INSERT INTO $($ConfigFileContent.SQLServer.ReferenceDatabase).dbo.$($ConfigFileContent.SQLServer.ReferenceColumnsTable) (TableId,ColumnName,ColumnDescription,ColumnDataType,ValidityStartDate) VALUES ($TableIdentifier,'$FieldName','$FieldLabel','$FieldType','$ProcessingDate') 
							END
							"

							
						}
						#$QueryGlobal;
						$ReaderGlobalColumns.Close();
						$ConnectionGlobalColumns.Close();

						$SqlCommandGlobal.commandtext = $QueryGlobal;
						$SqlCommandGlobal;
						$SqlCommandGlobal.CommandTimeout = 0;
						$ReaderGlobal = $SqlCommandGlobal.executescalar();

						$QueryGlobal = "";
						$QueryGlobal += "UPDATE $($ConfigFileContent.SQLServer.ReferenceDatabase).dbo.$($ConfigFileContent.SQLServer.ReferenceColumnsTable) 
						SET ValidityEndDate = '$ProcessingDate' 
						WHERE TableId = $TableIdentifier AND IsEnabled = 0 
						"
						
						$SqlCommandGlobal.commandtext = $QueryGlobal;
						$SqlCommandGlobal.CommandTimeout = 0;
						$ReaderGlobal = $SqlCommandGlobal.executescalar();
					}
					else{
					}
				}
				else{
					$QueryGlobal = "";
					$QueryGlobal += "INSERT INTO $($ConfigFileContent.SQLServer.ReferenceDatabase).dbo.$($ConfigFileContent.SQLServer.ReferenceTablesTable) 
					([EnvironmentId], [TableName], [IsEnabled], [ValidityStartDate]) 
					VALUES ($SIDPEnvironmentId,'$TableName', 0, CAST(N'2019-01-01T00:00:00.0000000' AS DateTime2)) 
					"
					
					$SqlCommandGlobal.commandtext = $QueryGlobal;
					$SqlCommandGlobal.CommandTimeout = 0;
					$ReaderGlobal = $SqlCommandGlobal.executescalar();
				}
			}
			catch {
				Set-Logger -Content "Query : $($QueryGlobalColumns) `r`n Error : $($Error[0].exception.GetBaseException().Message)" -Folder $RootFolder -Origin "GetSIDPMetaData";
			}
			finally {
			}
		}

		Set-Logger -Content "Updating SIDP Metadata for $SIDPEnvironmentName Finished." -Origin "GetMetaData" -Folder $RootFolder;
	}
	catch {
		Set-Logger -Content "Query : $($QueryGlobal) `r`n Error : $($Error[0].exception.GetBaseException().Message)" -Folder $RootFolder -Origin "GetSIDPMetaData";
	}
	finally {
		$ReaderGlobal.Close();
		$ConnectionGlobal.Close();
	}
}