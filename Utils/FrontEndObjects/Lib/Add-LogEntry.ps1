﻿function Add-LogEntry {
    Param
    (
        [Alias("ExtractName")] 
        $ExtractFileName
    )

	# Build Connectoin String from Configuration File Content
	$LogConnectionString = "Data Source=$($ConfigFileContent.sqlserver.parameterServer); Database=$($ConfigFileContent.sqlserver.logsDatabase); Trusted_Connection=True;";
	$InsertedLogRecordID = 0;
	
    try {
        Set-Logger -Content "Adding Log entry for $ExtractFileName..." -Origin "Extract-Data";
		$LogConnection = New-Object System.Data.SqlClient.SqlConnection $LogConnectionString
		$LogSqlCommand = New-Object System.Data.SqlClient.SqlCommand
		$LogSqlCommand.Connection = $LogConnection
		
		# Date and Time for the operation
		$OperationDateTime = Get-Date -UFormat "%Y-%m-%d %H:%M:%S";

		# The query based on the extract name
		$LogQuery = "INSERT INTO $($ConfigFileContent.sqlserver.logsDatabase).dbo.$($ConfigFileContent.sqlserver.logsTable)
		(ProcessingType, ETLName, FileOrTableName, ProcessingStartDate) VALUES ('Extract', 'SIDP_Data_Extractor', '$ExtractFileName', CAST('$OperationDateTime' AS DATETIME2));
        SELECT SCOPE_IDENTITY() as [InsertedLogRecordID];"
		$LogQuery
		$LogSqlCommand.CommandText = $LogQuery
        
        $LogConnection.Open();

        # Run the query and get the scope ID back into $LogRecordID
        $InsertedLogRecordID = $LogSqlCommand.ExecuteScalar()

        Set-Logger -Content "Log entry added for $ExtractFileName (Id: $InsertedLogRecordID)" -Origin "Extract-Data";
    }
    catch {
        Set-Logger -Content $("Exception while trying to add log entry: `n{1}" -f $_.Exception.ToString()) -Origin "Extract-Data";
    }
    finally {
        $LogConnection.Close();
    }
	return $InsertedLogRecordID;
}