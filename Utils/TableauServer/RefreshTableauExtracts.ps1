# Get Processing Date and time
$ProcessingDate = Get-Date -UFormat "%Y%m%d%H%M%S";

# Define Work Folder Path
$RootFolder = (Get-Item -Path ".\").FullName;

# Define Configuration File Path.
$ConfigFilePath = "$RootFolder\Config\config.json";
$ConfigFileContent = $(Get-Content -Path $ConfigFilePath -Raw | ConvertFrom-Json);

$TableauCommandLineUtilityPath = $ConfigFileContent.tableau.utilityFolderPath;
Set-Location $TableauCommandLineUtilityPath;

$User = $ConfigFileContent.tableau.user;
$SecurePassword = $ConfigFileContent.tableau.password;
$TableauServerName =  $ConfigFileContent.tableau.serverName;
.\tabcmd login -s $TableauServerName -u $User -p $SecurePassword --no-certcheck 
.\tabcmd refreshextracts --project Default --datasource "Requête SQL personnalisée (Extrait)" --no-certcheck 
.\tabcmd logout