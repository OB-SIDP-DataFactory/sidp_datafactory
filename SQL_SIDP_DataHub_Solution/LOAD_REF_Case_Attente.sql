USE [$(DataHubDatabaseName)] 
GO
TRUNCATE TABLE [dbo].[REF_CASE_ATTENTE] 
GO
INSERT INTO [dbo].[REF_CASE_ATTENTE] ([CODE_SF], [LIBELLE]) VALUES (N'01', N'Client ')
,(N'02', N'Partenaire')
,(N'03', N'Autres')
,(N'04', N'Services internes')
,(N'05', N'Services externes / Admnistration ')
GO