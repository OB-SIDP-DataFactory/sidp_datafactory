USE [$(DataHubDatabaseName)] 
GO
TRUNCATE TABLE [dbo].[REF_CASE_STATUS]
GO 

INSERT INTO [dbo].[REF_CASE_STATUS] ([CODE_SF], [LIBELLE]) VALUES (N'01', N'A faire')
,(N'02', N'En cours')
,(N'03', N'Fermée')
,(N'04', N'Abandonnée')
,(N'05', N'A valider')
,(N'06', N'En attente')
,(N'07', N'Contrôle LCB-FT terminé')
,(N'08', N'Contacts Argumenté positif')
,(N'09', N'Contacts Argumentés négatifs')
,(N'10', N'Contacts Non Argumentés')
,(N'11', N'Stop ')
GO