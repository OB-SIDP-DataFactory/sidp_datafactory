USE [$(DataHubDatabaseName)] 
GO
TRUNCATE TABLE [dbo].[REF_CASE_TYPE_CLI]
GO
INSERT INTO [dbo].[REF_CASE_TYPE_CLI] ([CODE_SF], [LIBELLE]) VALUES (N'01', N'Particulier')
,(N'02', N'Professionnel Personne Physique')
,(N'03', N'Personne Morale')
GO