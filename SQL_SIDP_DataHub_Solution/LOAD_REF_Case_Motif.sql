USE [$(DataHubDatabaseName)] 
GO
TRUNCATE TABLE [dbo].[REF_CASE_MOTIF] 
GO 

INSERT INTO [dbo].[REF_CASE_MOTIF] ([CODE_SF], [LIBELLE]) VALUES (N'01', N'Favorable')
,(N'02', N'Défavorable')
GO