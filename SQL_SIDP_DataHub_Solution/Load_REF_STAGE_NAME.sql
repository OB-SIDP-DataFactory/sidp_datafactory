USE [$(DataHubDatabaseName)]
GO
TRUNCATE TABLE [dbo].[REF_STAGE_NAME]
GO
INSERT INTO [dbo].[REF_STAGE_NAME] ([CODE_SF], [LIBELLE], [Validity_StartDate]) 
VALUES ('01', 'Détection', '2017-01-01')
     , ('02', 'Découverte', '2017-01-01')
     , ('03', 'Elaboration proposition', '2017-01-01')
     , ('04', 'Affaire signée en attente', '2017-01-01')
     , ('05', 'Vérification Banque', '2017-01-01')
     , ('06', 'Dossier incomplet', '2017-01-01')
     , ('07', 'Dossier validé', '2017-01-01')
     , ('08', 'Incident technique', '2017-01-01')
     , ('09', 'Compte ouvert', '2017-01-01')
     , ('10', 'Affaire refusée', '2017-01-01')
     , ('11', 'Sans suite', '2017-01-01')
GO
