﻿ALTER DATABASE [$(DatabaseName)]
    ADD FILEGROUP [MODGroup] CONTAINS MEMORY_OPTIMIZED_DATA;
GO
ALTER DATABASE [$(DatabaseName)]
    ADD FILE ( NAME = 'SIDP_DataHub_MOD', FILENAME = '$(DefaultDataPath)$(DefaultFilePrefix)_MOD') TO FILEGROUP [MODGroup];
GO
