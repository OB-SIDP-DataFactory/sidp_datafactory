USE [$(DataHubDatabaseName)]
GO
TRUNCATE TABLE [dbo].[param_opportunity_tracking]
GO
INSERT [dbo].[param_opportunity_tracking] ([Label])
VALUES (N'Account')
     , (N'CloseDate')
     , (N'LeadSource')
     , (N'Owner')
     , (N'StageName')
     , (N'StartedChannel__c')
     , (N'DistributionChannel__c')
     , (N'AdvisorCode__c')
     , (N'CommercialOfferCode__c')
     , (N'ProductFamilyCode__c')
     , (N'PointOfSaleCode__c')
     , (N'FinalizedBy__c')
     , (N'Indication__c')
     , (N'IDProcessSous__c')
     , (N'NoIndication__c')
     , (N'DeniedOpportunityReason__c')
     , (N'RejectReason__c')
     , (N'CommercialOfferName__c')
     , (N'ProductFamilyName__c')
     , (N'DistributorNetwork__c')
     , (N'FormValidation__c')
GO
