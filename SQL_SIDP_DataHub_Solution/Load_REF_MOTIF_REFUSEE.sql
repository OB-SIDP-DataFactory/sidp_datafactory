USE [$(DataHubDatabaseName)]
GO
TRUNCATE TABLE [dbo].[REF_MOTIF_REFUSEE]
GO
INSERT INTO [dbo].[REF_MOTIF_REFUSEE] ([CODE_SF], [LIBELLE], [Validity_StartDate]) 
VALUES ('01', 'Confidentiel', '2017-01-01')
     , ('02', 'Cotation interne défavorable', '2017-01-01')
     , ('03', 'Client existant en défaut', '2017-01-01')
     , ('04', 'Fraude', '2017-01-01')
     , ('05', 'Suspicion', '2017-01-01')
GO
