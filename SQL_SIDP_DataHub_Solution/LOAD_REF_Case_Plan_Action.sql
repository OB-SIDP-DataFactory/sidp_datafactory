USE [$(DataHubDatabaseName)] 
GO
TRUNCATE TABLE [dbo].[REF_CASE_PLAN_ACTION] 
GO
INSERT INTO [dbo].[REF_CASE_PLAN_ACTION] ([CODE_SF], [LIBELLE]) VALUES (N'01', N'Clôture client côté R')
,(N'02', N'Interdiction bancaire')
,(N'03', N'Clôture compte débiteur non régularisé')
,(N'04', N'Clôture interne banque')
,(N'05', N'Transfert judiciaire')
,(N'06', N'FICP')
,(N'07', N'Retour prestataire traitement judiciaire')
,(N'08', N'Compte créditeur avec ou sans prêt ')
,(N'09', N'Compte clos avec ou sans prêt')
,(N'10', N'Compte débiteur avec ou sans prêt')
,(N'11', N'Compte débiteur <100€ avec ou sans prêt')
,(N'12', N'Contrôle LCB-FT')
GO