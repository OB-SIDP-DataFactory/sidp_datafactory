USE [$(DataHubDatabaseName)]
GO
TRUNCATE TABLE [dbo].[REF_INTERACT_CHANNEL]
GO
INSERT INTO [dbo].[REF_INTERACT_CHANNEL] ([CODE_SF], [LIBELLE], [Validity_StartDate])
VALUES ('01', 'A préciser', '2017-01-01')
     , ('02', 'Boutique IOBSP', '2017-01-01')
     , ('03', 'Boutique non IOBSP', '2017-01-01')
     , ('04', 'E-shop', '2017-01-01')
     , ('05', 'Orange&moi web', '2017-01-01')
     , ('06', 'Orange&moi appli', '2017-01-01')
     , ('07', 'Orange Cash', '2017-01-01')
     , ('08', 'Portail OF', '2017-01-01')
     , ('09', 'TV', '2017-01-01')
     , ('10', 'Appli OB', '2017-01-01')
     , ('11', 'Web OB', '2017-01-01')
     , ('12', 'Appel entrant', '2017-01-01')
     , ('13', 'Appel sortant', '2017-01-01')
     , ('14', 'Chat', '2017-01-01')
     , ('15', 'E-mail', '2017-01-01')
     , ('16', 'Courrier/fax', '2017-01-01')
GO
