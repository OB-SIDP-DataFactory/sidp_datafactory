USE [$(DataHubDatabaseName)]
GO
TRUNCATE TABLE [dbo].[REF_MOTIF_SANS_SUITE]
GO
INSERT INTO [dbo].[REF_MOTIF_SANS_SUITE] ([CODE_SF], [LIBELLE], [Validity_StartDate]) 
VALUES ('06', 'Refus entrée en relation', '2017-01-01')
     , ('01', 'Indication expirée', '2017-01-01')
     , ('02', 'Dossier de souscription expiré', '2017-01-01')
     , ('03', 'Refus client', '2017-01-01')
GO
