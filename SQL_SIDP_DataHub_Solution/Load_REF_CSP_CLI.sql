USE [$(DataHubDatabaseName)]
GO
TRUNCATE TABLE [dbo].[REF_CSP_CLI]
GO
INSERT INTO [dbo].[REF_CSP_CLI] ([CODE_SF_CSP], [LIBELLE], [Validity_StartDate])
VALUES ('01', 'Agriculteurs exploitants', '2017-01-01')
     , ('02', 'Artisans, commerçants et chefs d''entreprise', '2017-01-01')
     , ('03', 'Cadres et professions intellectuelles supérieures', '2017-01-01')
     , ('04', 'Professions intermédiaires', '2017-01-01')
     , ('05', 'Employés', '2017-01-01')
     , ('06', 'Ouvriers', '2017-01-01')
     , ('07', 'Retraités', '2017-01-01')
     , ('08', 'Autres personnes sans activité professionnelle', '2017-01-01')
GO
