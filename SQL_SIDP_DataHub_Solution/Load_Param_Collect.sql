USE [$(DataHubDatabaseName)]
GO
TRUNCATE TABLE [dbo].[Param_Collect]
GO
INSERT [dbo].[Param_Collect] ([domain], [batch_name], [last_extraction_date], [flag])
VALUES (N'SF', N'PGMRUN_API_RAW_OPPORTUNITY_History', CAST(N'2017-04-21T04:00:00.000' AS DateTime), N'OK')
     , (N'SF', N'PGMRUN_API_RAW_ACCOUNT', CAST(N'2017-04-21T04:00:00.000' AS DateTime), N'OK')
     , (N'SF', N'PGMRUN_API_RAW_CASE', CAST(N'2017-04-21T04:00:00.000' AS DateTime), N'OK')
     , (N'SF', N'PGMRUN_API_RAW_USER', CAST(N'2017-04-21T04:00:00.000' AS DateTime), N'OK')
     , (N'SF', N'PGMRUN_API_RAW_TASK', CAST(N'2017-04-21T04:00:00.000' AS DateTime), N'OK')
     , (N'SF', N'PGMRUN_API_RAW_EMAILMESSAGE', CAST(N'2017-04-21T04:00:00.000' AS DateTime), N'OK')
     , (N'SF', N'PGMRUN_API_RAW_LIVECHATTRANSCRIPT', CAST(N'2017-04-21T04:00:00.000' AS DateTime), N'OK')
     , (N'SF', N'PGMRUN_API_RAW_PROFILE', CAST(N'2017-04-21T04:00:00.000' AS DateTime), N'OK')
     , (N'SF', N'PGMRUN_API_RAW_CONTACT', CAST(N'2017-04-21T04:00:00.000' AS DateTime), N'OK')
     , (N'FE', N'PKGCO_SSIS_FE_COMMERCIALOFFER', CAST(N'2017-04-01T03:00:00.000' AS DateTime), N'OK')
     , (N'FE', N'PKGCO_SSIS_FE_COMMERCIALPRODUCT', CAST(N'2017-04-01T03:00:00.000' AS DateTime), N'OK')
     , (N'FE', N'PKGCO_SSIS_FE_ENROLMENTFOLDER', CAST(N'2017-04-21T03:00:00.000' AS DateTime), N'OK')
     , (N'FE', N'PKGCO_SSIS_FE_EQUIPMENT', CAST(N'2017-04-21T03:00:00.000' AS DateTime), N'OK')
     , (N'FE', N'PKGCO_SSIS_FE_SBDSERVICESASSOC', CAST(N'2017-04-01T03:00:00.000' AS DateTime), N'OK')
     , (N'FE', N'PKGCO_SSIS_FE_SERVICEPRODUCT', CAST(N'2017-04-01T03:00:00.000' AS DateTime), N'OK')
     , (N'FE', N'PKGCO_SSIS_FE_SUBSCRIBEDPRODUCT', CAST(N'2017-04-01T03:00:00.000' AS DateTime), N'OK')
     , (N'FE', N'PKGCO_SSIS_FE_SVCPRODUCTASSOC', CAST(N'2017-04-01T03:00:00.000' AS DateTime), N'OK')
GO
