﻿USE [$(DataHubDatabaseName)] 
GO
TRUNCATE TABLE [dbo].[IWD_PILOTAGE_CHARGEMENT]
GO
INSERT [dbo].[IWD_PILOTAGE_CHARGEMENT] ([dte_deb_periode], [dte_fin_periode], [statut]) VALUES (CAST(N'2018-10-01' AS Date), CAST(N'2018-10-09' AS Date), N'en cours')
GO
UPDATE [dbo].[Param_IWD_Collecte]
SET flag_dim = 1
WHERE table_cible in ('IWD_USER_DATA_CUST_DIM_1', 'IWD_USER_DATA_CUST_DIM_2')
GO
USE [SIDP_DataHub]
GO
INSERT [dbo].[Param_IWD_TABLE_STRUCT] ([source_table], [source_column_name], [source_data_type], [cible_table_name], [cible_column_name], [cible_data_type], [flag_Identity], [key_column]) VALUES (N'V_USER_DATA_CUST_DIM_1', N'ID', N'NUMBER', N'IWD_USER_DATA_CUST_DIM_1', N'ID', N'NUMERIC', 0, NULL)
GO
INSERT [dbo].[Param_IWD_TABLE_STRUCT] ([source_table], [source_column_name], [source_data_type], [cible_table_name], [cible_column_name], [cible_data_type], [flag_Identity], [key_column]) VALUES (N'V_USER_DATA_CUST_DIM_1', N'TENANT_KEY', N'NUMBER', N'IWD_USER_DATA_CUST_DIM_1', N'TENANT_KEY', N'NUMERIC', 0, NULL)
GO
INSERT [dbo].[Param_IWD_TABLE_STRUCT] ([source_table], [source_column_name], [source_data_type], [cible_table_name], [cible_column_name], [cible_data_type], [flag_Identity], [key_column]) VALUES (N'V_USER_DATA_CUST_DIM_1', N'CREATE_AUDIT_KEY', N'NUMBER', N'IWD_USER_DATA_CUST_DIM_1', N'CREATE_AUDIT_KEY', N'NUMERIC', 0, NULL)
GO
INSERT [dbo].[Param_IWD_TABLE_STRUCT] ([source_table], [source_column_name], [source_data_type], [cible_table_name], [cible_column_name], [cible_data_type], [flag_Identity], [key_column]) VALUES (N'V_USER_DATA_CUST_DIM_1', N'DIM_ATTRIBUTE_1', N'VARCHAR2', N'IWD_USER_DATA_CUST_DIM_1', N'DIM_ATTRIBUTE_1', N'VARCHAR', 0, NULL)
GO
INSERT [dbo].[Param_IWD_TABLE_STRUCT] ([source_table], [source_column_name], [source_data_type], [cible_table_name], [cible_column_name], [cible_data_type], [flag_Identity], [key_column]) VALUES (N'V_USER_DATA_CUST_DIM_1', N'DIM_ATTRIBUTE_2', N'VARCHAR2', N'IWD_USER_DATA_CUST_DIM_1', N'DIM_ATTRIBUTE_2', N'VARCHAR', 0, NULL)
GO
INSERT [dbo].[Param_IWD_TABLE_STRUCT] ([source_table], [source_column_name], [source_data_type], [cible_table_name], [cible_column_name], [cible_data_type], [flag_Identity], [key_column]) VALUES (N'V_USER_DATA_CUST_DIM_1', N'DIM_ATTRIBUTE_3', N'VARCHAR2', N'IWD_USER_DATA_CUST_DIM_1', N'DIM_ATTRIBUTE_3', N'VARCHAR', 0, NULL)
GO
INSERT [dbo].[Param_IWD_TABLE_STRUCT] ([source_table], [source_column_name], [source_data_type], [cible_table_name], [cible_column_name], [cible_data_type], [flag_Identity], [key_column]) VALUES (N'V_USER_DATA_CUST_DIM_1', N'DIM_ATTRIBUTE_4', N'VARCHAR2', N'IWD_USER_DATA_CUST_DIM_1', N'DIM_ATTRIBUTE_4', N'VARCHAR', 0, NULL)
GO
INSERT [dbo].[Param_IWD_TABLE_STRUCT] ([source_table], [source_column_name], [source_data_type], [cible_table_name], [cible_column_name], [cible_data_type], [flag_Identity], [key_column]) VALUES (N'V_USER_DATA_CUST_DIM_1', N'DIM_ATTRIBUTE_5', N'VARCHAR2', N'IWD_USER_DATA_CUST_DIM_1', N'DIM_ATTRIBUTE_5', N'VARCHAR', 0, NULL)
GO
INSERT [dbo].[Param_IWD_TABLE_STRUCT] ([source_table], [source_column_name], [source_data_type], [cible_table_name], [cible_column_name], [cible_data_type], [flag_Identity], [key_column]) VALUES (N'V_USER_DATA_CUST_DIM_2', N'ID', N'NUMBER', N'IWD_USER_DATA_CUST_DIM_2', N'ID', N'NUMERIC', 0, NULL)
GO
INSERT [dbo].[Param_IWD_TABLE_STRUCT] ([source_table], [source_column_name], [source_data_type], [cible_table_name], [cible_column_name], [cible_data_type], [flag_Identity], [key_column]) VALUES (N'V_USER_DATA_CUST_DIM_2', N'TENANT_KEY', N'NUMBER', N'IWD_USER_DATA_CUST_DIM_2', N'TENANT_KEY', N'NUMERIC', 0, NULL)
GO
INSERT [dbo].[Param_IWD_TABLE_STRUCT] ([source_table], [source_column_name], [source_data_type], [cible_table_name], [cible_column_name], [cible_data_type], [flag_Identity], [key_column]) VALUES (N'V_USER_DATA_CUST_DIM_2', N'CREATE_AUDIT_KEY', N'NUMBER', N'IWD_USER_DATA_CUST_DIM_2', N'CREATE_AUDIT_KEY', N'NUMERIC', 0, NULL)
GO
INSERT [dbo].[Param_IWD_TABLE_STRUCT] ([source_table], [source_column_name], [source_data_type], [cible_table_name], [cible_column_name], [cible_data_type], [flag_Identity], [key_column]) VALUES (N'V_USER_DATA_CUST_DIM_2', N'DIM_ATTRIBUTE_1', N'VARCHAR2', N'IWD_USER_DATA_CUST_DIM_2', N'DIM_ATTRIBUTE_1', N'VARCHAR', 0, NULL)
GO
INSERT [dbo].[Param_IWD_TABLE_STRUCT] ([source_table], [source_column_name], [source_data_type], [cible_table_name], [cible_column_name], [cible_data_type], [flag_Identity], [key_column]) VALUES (N'V_USER_DATA_CUST_DIM_2', N'DIM_ATTRIBUTE_2', N'VARCHAR2', N'IWD_USER_DATA_CUST_DIM_2', N'DIM_ATTRIBUTE_2', N'VARCHAR', 0, NULL)
GO
INSERT [dbo].[Param_IWD_TABLE_STRUCT] ([source_table], [source_column_name], [source_data_type], [cible_table_name], [cible_column_name], [cible_data_type], [flag_Identity], [key_column]) VALUES (N'V_USER_DATA_CUST_DIM_2', N'DIM_ATTRIBUTE_3', N'VARCHAR2', N'IWD_USER_DATA_CUST_DIM_2', N'DIM_ATTRIBUTE_3', N'VARCHAR', 0, NULL)
GO
INSERT [dbo].[Param_IWD_TABLE_STRUCT] ([source_table], [source_column_name], [source_data_type], [cible_table_name], [cible_column_name], [cible_data_type], [flag_Identity], [key_column]) VALUES (N'V_USER_DATA_CUST_DIM_2', N'DIM_ATTRIBUTE_4', N'VARCHAR2', N'IWD_USER_DATA_CUST_DIM_2', N'DIM_ATTRIBUTE_4', N'VARCHAR', 0, NULL)
GO
INSERT [dbo].[Param_IWD_TABLE_STRUCT] ([source_table], [source_column_name], [source_data_type], [cible_table_name], [cible_column_name], [cible_data_type], [flag_Identity], [key_column]) VALUES (N'V_USER_DATA_CUST_DIM_2', N'DIM_ATTRIBUTE_5', N'VARCHAR2', N'IWD_USER_DATA_CUST_DIM_2', N'DIM_ATTRIBUTE_5', N'VARCHAR', 0, NULL)
GO
