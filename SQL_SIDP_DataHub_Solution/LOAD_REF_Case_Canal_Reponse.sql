USE [$(DataHubDatabaseName)] 
GO
TRUNCATE TABLE [dbo].[REF_CASE_CANAL_REPONSE] 
GO
INSERT INTO [dbo].[REF_CASE_CANAL_REPONSE] ([CODE_SF], [LIBELLE]) VALUES (N'01', N'Mobile/SMS')
,(N'02', N'Internet / Espace Client')
,(N'03', N'Courriers')
,(N'04', N'Appel entrant')
,(N'05', N'Appel sortant')
,(N'06', N'Fax')
,(N'07', N'Chat')
,(N'08', N'Réseaux sociaux')
GO
