USE [$(DataHubDatabaseName)]
GO
TRUNCATE TABLE [dbo].[REF_DISTRIB_CHANNEL]
GO
INSERT INTO [dbo].[REF_DISTRIB_CHANNEL] ([CODE_SF], [LIBELLE], [Validity_StartDate])
VALUES ('01', 'Digital', '2017-01-01')
     , ('02', 'Agence', '2017-01-01')
     , ('03', 'CRC', '2017-01-01')
     , ('04', 'Autre', '2017-01-01')
GO
