USE [$(DataHubDatabaseName)] 
GO
TRUNCATE TABLE [dbo].[REF_CASE_ORIGINE] 
GO
INSERT INTO [dbo].[REF_CASE_ORIGINE] ([CODE_SF], [LIBELLE]) VALUES (N'01', N'Client incompréhension')
,(N'02', N'Banque dysfonct informatique')
,(N'03', N'Banque erreur opérationnelle')
,(N'04', N'Banque délai')
,(N'05', N'Réseau erreur')
,(N'06', N'Réseau retard')
,(N'07', N'Presta erreur')
,(N'08', N'Presta retard')
,(N'09', N'Evènement externe')
GO