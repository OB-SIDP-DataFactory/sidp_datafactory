﻿USE [$(DataHubDatabaseName)] 
GO
TRUNCATE TABLE  [dbo].[REF_BOUTIQUE]
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'59000LIL-5', N'IOB', N'LILLE NEUVE', N'Direction Orange Nord de France', N'Distribution Nord de France', N'59000', N'LILLE', N'10 RUE NEUVE', N'Boutique Orange')
GO																																											  
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'13001MC2-8', N'IOB', N'MARSEILLE CANEBIERE', N'Direction Orange Sud Est', N'Distribution Sud Est', N'13001', N'MARSEILLE 01', N'30 RUE DE LA CANEBIERE', N'Boutique Orange')
GO																																											  
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'75009OP9-5', N'IOB', N'PARIS OPERA', N'Direction Orange IDF', N'Distribution Ile de France Centre', N'75009', N'PARIS 09', N'10 RUE HALEVY', N'Boutique Orange')
GO																																											  
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'21000FT4-8', N'IOB', N'DIJON TOISON D''OR', N'Direction Orange Est', N'Distribution Grand Est', N'21000', N'DIJON', N' CENTRE COMMERCIAL TOISON D''OR', N'Boutique Orange')
GO																																											  
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'57037FU8-4', N'IOB', N'METZ SERPENOISE', N'Direction Orange Est', N'Distribution Grand Est', N'57000', N'METZ', N'24 RUE SERPENOISE', N'Boutique Orange')
GO																																											  
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'57180FU8-5', N'IOB', N'THIONVILLE CENTRE LECLERC', N'Direction Orange Est', N'Distribution Grand Est', N'57100', N'THIONVILLE', N'20 ROUTE D ARLON', N'Boutique Orange')
GO																																											  
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'71100EE4-P', N'IOB', N'CHALON', N'Direction Orange Est', N'Distribution Grand Est', N'71100', N'CHALON SUR SAONE', N' RUE THOMAS DUMOREY', N'Boutique Orange')
GO																																											  
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'71019FG3-2', N'IOB', N'MACON CARNOT', N'Direction Orange Est', N'Distribution Grand Est', N'71000', N'MACON', N'3 RUE CARNOT', N'Boutique Orange')
GO																																											  
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'25000FX1-1', N'IOB', N'BESANCON CHATEAUFARINE', N'Direction Orange Est', N'Distribution Grand Est', N'25000', N'BESANCON', N' CENTRE COMMERCIAL GEANT CHATEAUFARINE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'68065FF2-0', N'IOB', N'MULHOUSE VICTOIRE', N'Direction Orange Est', N'Distribution Grand Est', N'68200', N'MULHOUSE', N'27 RUE DU SAUVAGE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'68019FF2-A', N'IOB', N'COLMAR HOUSSEN', N'Direction Orange Est', N'Distribution Grand Est', N'68125', N'HOUSSEN', N' ZONE COMMERCIALE DU BUHLFELD', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'67302FG5-0', N'IOB', N'MUNDOLSHEIM', N'Direction Orange Est', N'Distribution Grand Est', N'67452', N'MUNDOLSHEIM', N' GALERIE MUNDO CORA', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'67500FT5-1', N'IOB', N'HAGUENAU', N'Direction Orange Est', N'Distribution Grand Est', N'67500', N'HAGUENAU', N'2 PLACE D''ARMES', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'67100FT5-5', N'IOB', N'STRASBOURG KLEBER', N'Direction Orange Est', N'Distribution Grand Est', N'67000', N'STRASBOURG', N' CENTRE COMMERCIAL MAISON ROUGE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'54039FW2-0', N'IOB', N'NANCY', N'Direction Orange Est', N'Distribution Grand Est', N'54000', N'NANCY', N'8 RUE SAINT THIEBAUT', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'89000FM9-4', N'IOB', N'AUXERRE FONTAINES LES CLAIRIONS', N'Direction Orange Est', N'Distribution Grand Est', N'89000', N'AUXERRE', N' CENTRE COMMERCIAL GEANT', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'58000DE6-X', N'IOB', N'NEVERS MITTERRAND', N'Direction Orange Est', N'Distribution Grand Est', N'58000', N'NEVERS', N'90 ter RUE FRANCOIS MITTERRAND', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'85000BFB-5', N'IOB', N'LA ROCHE SUR YON LES FLANERIES', N'Direction Orange Ouest', N'Distribution Ouest', N'85000', N'LA ROCHE SUR YON', N' ROUTE DE NANTES', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'85302FC7-2', N'IOB', N'CHALLANS', N'Direction Orange Ouest', N'Distribution Ouest', N'85300', N'CHALLANS', N' PLACE DU CHAMP DE FOIRE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'53015FF5-5', N'IOB', N'LAVAL GRENOUX', N'Direction Orange Ouest', N'Distribution Ouest', N'53000', N'LAVAL', N'46 BOULEVARD DE LATTRE DE TASSIGNY', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'726508U6-H', N'IOB', N'LA CHAPELLE AUCHAN LE MANS', N'Direction Orange Ouest', N'Distribution Ouest', N'72650', N'LA CHAPELLE ST AUBIN', N' AVENUE DU MANS', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'44000EDM-1', N'IOB', N'COEUR DE NANTES', N'Direction Orange Ouest', N'Distribution Ouest', N'44000', N'NANTES', N'10 RUE DE SANTEUIL', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'44407FC1-0', N'IOB', N'REZE OCEANE', N'Direction Orange Ouest', N'Distribution Ouest', N'44400', N'REZE', N'10 ROND-POINT DE LA CORBINERIE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'44800FM8-B', N'IOB', N'SAINT HERBLAIN ATLANTIS', N'Direction Orange Ouest', N'Distribution Ouest', N'44800', N'ST HERBLAIN', N' CENTRE COMMERCIAL ATLANTIS', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'35740FZ9-8', N'IOB', N'RENNES OPERA', N'Direction Orange Ouest', N'Distribution Ouest', N'35740', N'PACE', N' ZONE D''ACTIVITE COMMERCIALE DE LA GIRAUDAIS', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'35000FZ9-A', N'IOB', N'RENNES ALMA 1', N'Direction Orange Ouest', N'Distribution Ouest', N'35000', N'RENNES', N' RUE BOSPHORE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'35406FZ9-5', N'IOB', N'SAINT MALO', N'Direction Orange Ouest', N'Distribution Ouest', N'35400', N'ST MALO', N' AVENUE DE LA FLAUDAIS', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'22360FW5-A', N'IOB', N'LANGUEUX', N'Direction Orange Ouest', N'Distribution Ouest', N'22360', N'LANGUEUX', N'1 RUE JULES VERNES', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'29603FT2-4', N'IOB', N'BREST JAURES', N'Direction Orange Ouest', N'Distribution Ouest', N'29200', N'BREST', N'58 RUE JEAN JAURES', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'56000R9D-5', N'IOB', N'VANNES FOURCHENE', N'Direction Orange Ouest', N'Distribution Ouest', N'56000', N'VANNES', N' ROUTE D AURAY ', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'29000FJ6-5', N'IOB', N'QUIMPER GEANT', N'Direction Orange Ouest', N'Distribution Ouest', N'29000', N'QUIMPER', N'163 ROUTE DE BENODET', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'56600FX7-4', N'IOB', N'LANESTER', N'Direction Orange Ouest', N'Distribution Ouest', N'56600', N'LANESTER', N' ROUTE D HENNEBONT', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'44600ER7-9', N'IOB', N'TRIGNAC', N'Direction Orange Ouest', N'Distribution Ouest', N'44570', N'TRIGNAC', N' ZONE INDUSTRIELLE DE LA SAVINE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'49000FK1-7', N'IOB', N'ANGERS ESPACE ANJOU', N'Direction Orange Ouest', N'Distribution Ouest', N'49000', N'ANGERS', N'75 AVENUE MONTAIGNE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'69002FG4-2', N'IOB', N'LYON REPUBLIQUE', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', N'69002', N'LYON 02', N'50 RUE DE LA REPUBLIQUE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'69003FY9-4', N'IOB', N'LYON PART DIEU 1', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', N'69003', N'LYON 03', N'17 RUE DOCTEUR BOUCHUT', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'38080FD8-4', N'IOB', N'L''ISLE D''ABEAU', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', N'38080', N'L ISLE D ABEAU', N' CENTRE COMMERCIAL CARREFOUR', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'01011FG1-6', N'IOB', N'BOURG EN BRESSE', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', N'01000', N'BOURG EN BRESSE', N'13 PLACE NEUVE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'73200FR2-1', N'IOB', N'ALBERTVILLE CHIRIAC', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', N'73200', N'ALBERTVILLE', N' ZONE DE CHIRIAC', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'74000FL8-2', N'IOB', N'ANNECY CARNOT', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', N'74000', N'ANNECY', N'17 RUE CARNOT', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'74200FV3-2', N'IOB', N'THONON LES BAINS', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', N'74200', N'THONON LES BAINS', N'12 GRANDE RUE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'74107FV3-0', N'IOB', N'ANNEMASSE', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', N'74100', N'ANNEMASSE', N'29 RUE DE GENEVE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'43012FG8-4', N'IOB', N'LE PUY SAINT GILLES', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', N'43000', N'LE PUY EN VELAY', N'5 RUE SAINT GILLES', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'42000FD5-6', N'IOB', N'SAINT ETIENNE GRANDE RUE', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', N'42000', N'ST ETIENNE', N'13 RUE DU GENERAL FOY', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'73010FR2-0', N'IOB', N'CHAMBERY CHAMNORD', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', N'73000', N'CHAMBERY', N' AVENUE DES LANDIERS', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'38028FK0-2', N'IOB', N'GRENOBLE GRENETTE', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', N'38000', N'GRENOBLE', N'17 PLACE GRENETTE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'03200FQ4-2', N'IOB', N'VICHY', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', N'03200', N'VICHY', N'2 RUE JEAN JAURES', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'63000FF7-8', N'IOB', N'CLERMONT JAUDE', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', N'63000', N'CLERMONT FERRAND', N'7 PLACE JAUDE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'42328FE6-3', N'IOB', N'ROANNE', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', N'42300', N'ROANNE', N'43 RUE CHARLES DE GAULLE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'50009FP2-0', N'IOB', N'SAINT LO', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', N'50000', N'ST LO', N'23 ter RUE TORTERON', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'45073FN5-5', N'IOB', N'ORLEANS HALLES CHATELET', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', N'45000', N'ORLEANS', N' CENTRE COMMERCIAL HALLES CHATELET', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'37000FE2-7', N'IOB', N'TOURS NATIONALE', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', N'37000', N'TOURS', N'27 RUE NATIONALE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'36020FG9-1', N'IOB', N'CHATEAUROUX HUGO', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', N'36000', N'CHATEAUROUX', N'2 RUE VICTOR HUGO', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'61014FM7-0', N'IOB', N'ALENCON', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', N'61000', N'ALENCON', N'24 RUE AUX SIEURS', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'280009HR-5', N'IOB', N'CHARTRES BALLAY', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', N'28000', N'CHARTRES', N'4 RUE NOEL BALLAY', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'76173FC9-1', N'IOB', N'ROUEN PALAIS', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', N'76000', N'ROUEN', N'8 ALLEE EUGENE DELACROIX', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'76410FC9-G', N'IOB', N'TOURVILLE LA RIVIERE', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', N'76410', N'TOURVILLE LA RIVIERE', N' CENTRE COMMERCIAL LE CLOS AUX ANTES', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'76290FK2-9', N'IOB', N'MONTIVILLIERS', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', N'76290', N'MONTIVILLIERS', N' CENTRE COMMERCIAL GRAND HAVRE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'14000PJC-I', N'IOB', N'CAEN', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', N'14000', N'CAEN', N'7 RUE DE STRASBOURG', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'50470FP2-A', N'IOB', N'LA GLACERIE COTENTIN', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', N'50470', N'LA GLACERIE', N' CENTRE COMMERCIAL COTENTIN', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'12031FF4-4', N'IOB', N'RODEZ', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', N'12000', N'RODEZ', N'5 CARREFOUR SAINT ETIENNE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'31200FX1-1', N'IOB', N'TOULOUSE PORTE DE GRAMONT', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', N'31200', N'TOULOUSE', N'92 CHEMIN DE GABARDIE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'31700QJ2-5', N'IOB', N'BLAGNAC', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', N'31700', N'BLAGNAC', N'2 ALLEE EMILE ZOLA', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'82017FH7-2', N'IOB', N'MONTAUBAN', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', N'82000', N'MONTAUBAN', N'21 RUE DE LA RESISTANCE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'34500FJ8-4', N'IOB', N'BEZIERS LA GALERIE', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', N'34500', N'BEZIERS', N' AVENUE DE LA VOIE DOMITIENNE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'66965FM1-4', N'IOB', N'PERPIGNAN PORTE ESPAGNE', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', N'66000', N'PERPIGNAN', N'1135 AVENUE D ESPAGNE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'30900F1F-8', N'IOB', N'NIMES CAP COSTIERES', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', N'30900', N'NIMES', N'400 RUE DU DOCTEUR CLAUDE BAILLET', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'30100FX9-0', N'IOB', N'ALES', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', N'30100', N'ALES', N'26 RUE AVEJEAN', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'31670FJ1-3', N'IOB', N'LABEGE', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', N'31670', N'LABEGE', N' CENTRE COMMERCIAL LABEGE 2', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'310007DI-G', N'IOB', N'TOULOUSE ALSACE LORRAINE', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', N'31000', N'TOULOUSE', N'42 RUE D''ALSACE LORRAINE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'09100FW1-4', N'IOB', N'PAMIERS', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', N'09100', N'PAMIERS', N' ROUTE DE MIREPOIX', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'34976FK6-A', N'IOB', N'LATTES', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', N'34970', N'LATTES', N' RUE DES PLATANES', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'3400000X-5', N'IOB', N'MONTPELLIER JEAN MOULIN', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', N'34000', N'MONTPELLIER', N'28 GRANDE RUE JEAN MOULIN', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'34980FX1-1', N'IOB', N'MONTPELLIER TRIFONTAINE', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', N'34980', N'ST CLEMENT DE RIVIERE', N' ROUTE DE GANGES', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'13300FE3-2', N'IOB', N'SALON DE PROVENCE', N'Direction Orange Sud Est', N'Distribution Sud Est', N'13300', N'SALON DE PROVENCE', N'1 BOULEVARD DE LA REPUBLIQUE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'84130JMX-B', N'IOB', N'AVIGNON LE PONTET SOLEIL', N'Direction Orange Sud Est', N'Distribution Sud Est', N'84130', N'LE PONTET', N' CENTRE COMMERCIAL ESPACE SOLEIL', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'26200FK7-7', N'IOB', N'MONTELIMAR', N'Direction Orange Sud Est', N'Distribution Sud Est', N'26200', N'MONTELIMAR', N'179 ROUTE DE MARSEILLE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'26021FE4-0', N'IOB', N'VALENCE VICTOR HUGO', N'Direction Orange Sud Est', N'Distribution Sud Est', N'26000', N'VALENCE', N'13 RUE VICTOR HUGO', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'26100FE4-5', N'IOB', N'ROMANS', N'Direction Orange Sud Est', N'Distribution Sud Est', N'26100', N'ROMANS SUR ISERE', N' PLACE DU 75EME R I', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'06700FQ2-2', N'IOB', N'CAP 3000', N'Direction Orange Sud Est', N'Distribution Sud Est', N'06700', N'ST LAURENT DU VAR', N' PORTE ANTIBES', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'06036FD7-0', N'IOB', N'NICE TNL', N'Direction Orange Sud Est', N'Distribution Sud Est', N'06300', N'NICE', N'15 BOULEVARD DELFINO', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'13011L5D-5', N'IOB', N'MARSEILLE LA VALENTINE 2', N'Direction Orange Sud Est', N'Distribution Sud Est', N'13011', N'MARSEILLE 11', N' CENTRE COMMERCIAL PRINTEMPS LA VALENTINE ', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'83160FC5-9', N'IOB', N'LA VALETTE GRAND VAR', N'Direction Orange Sud Est', N'Distribution Sud Est', N'83160', N'LA VALETTE DU VAR', N' CENTRE COMMERCIAL GRAND VAR', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'13090FE3-7', N'IOB', N'AIX JAS DE BOUFFAN', N'Direction Orange Sud Est', N'Distribution Sud Est', N'13090', N'AIX EN PROVENCE', N' ROUTE DE BERRE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'06402FH3-0', N'IOB', N'CANNES', N'Direction Orange Sud Est', N'Distribution Sud Est', N'06400', N'CANNES', N'28 RUE D''ANTIBES', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'06130FH3-2', N'IOB', N'GRASSE', N'Direction Orange Sud Est', N'Distribution Sud Est', N'06130', N'GRASSE', N'225 ROUTE DE CANNES', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'20294FE9-1', N'IOB', N'BASTIA', N'Direction Orange Sud Est', N'Distribution Corse', N'20200', N'BASTIA', N'5 AVENUE DU MARECHAL SEBASTIANI', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'64230FP9-6', N'IOB', N'PAU LESCAR', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', N'64230', N'LESCAR', N' CENTRE COMMERCIAL QUARTIER LIBRE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'64601FA6-4', N'IOB', N'ANGLET BAB 2', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', N'64600', N'ANGLET', N' CENTRE COMMERCIAL BAB2', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'87033FK5-1', N'IOB', N'LIMOGES CLOCHER', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', N'87000', N'LIMOGES', N'21 RUE DU CLOCHER', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'19100FP5-4', N'IOB', N'BRIVE THEATRE', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', N'19100', N'BRIVE LA GAILLARDE', N'4 AVENUE DE PARIS', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'33260D6L-3', N'IOB', N'LA TESTE', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', N'33260', N'LA TESTE DE BUCH', N'1060 AVENUE DE L EUROPE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'33140FX5-2', N'IOB', N'MERIGNAC', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', N'33700', N'MERIGNAC', N' CENTRE COMMERCIAL MERIGNAC SOLEIL', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'79000FS1-2', N'IOB', N'NIORT VICTOR HUGO', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', N'79000', N'NIORT', N'26 RUE VICTOR HUGO', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'17138FY4-7', N'IOB', N'LA ROCHELLE BEAULIEU', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', N'17138', N'PUILBOREAU', N' CENTRE COMMERCIAL BEAULIEU', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'86000FN4-9', N'IOB', N'POITIERS CASINO', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', N'86000', N'POITIERS', N'2 AVENUE LAFAYETTE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'16000FL9-2', N'IOB', N'ANGOULEME HOTEL DE VILLE', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', N'16000', N'ANGOULEME', N'21 RUE HERGE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'33324FX5-3', N'IOB', N'BEGLES RIVES D'' ARCINS', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', N'33130', N'BEGLES', N' CENTRE COMMERCIAL CARREFOUR', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES (N'33000FT6-A', N'IOB', N'BORDEAUX SAINTE CATHERINE', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', N'33000', N'BORDEAUX', N'93 RUE SAINTE CATHERINE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'24017FK4-4', N'IOB', N'PERIGUEUX TAILLEFER', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', N'24000', N'PERIGUEUX', N'1 RUE TAILLEFER', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'47000FS2-8', N'IOB', N'AGEN', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', N'47000', N'AGEN', N'93 BOULEVARD REPUBLIQUE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'622198GC-5', N'IOB', N'LONGUENESSE', N'Direction Orange Nord de France', N'Distribution Nord de France', N'62219', N'LONGUENESSE', N' RUE DES FRAIS FONDS', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'62000FF9-4', N'IOB', N'ARRAS DELANSORNE', N'Direction Orange Nord de France', N'Distribution Nord de France', N'62000', N'ARRAS', N'32 RUE DESIRE DELANSORNE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'59300FC2-2', N'IOB', N'VALENCIENNES PLACE D''ARMES', N'Direction Orange Nord de France', N'Distribution Nord de France', N'59300', N'VALENCIENNES', N'57 PLACE D''ARMES', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'62407FA8-5', N'IOB', N'NOYELLES GODAULT', N'Direction Orange Nord de France', N'Distribution Nord de France', N'62950', N'NOYELLES GODAULT', N' AVENUE DE LA REPUBLIQUE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'59650FG6-1', N'IOB', N'VILLENEUVE D''ASCQ 2', N'Direction Orange Nord de France', N'Distribution Nord de France', N'59650', N'VILLENEUVE D ASCQ', N' CENTRE COMMERCIAL V2', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'59320I3V-6', N'IOB', N'ENGLOS', N'Direction Orange Nord de France', N'Distribution Nord de France', N'59320', N'ENGLOS', N' ROUTE NATIONALE 352', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'10007FA5-0', N'IOB', N'TROYES', N'Direction Orange Nord de France', N'Distribution Nord de France', N'10000', N'TROYES', N'26 RUE DE LA REPUBLIQUE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'51070FL2-1', N'IOB', N'REIMS OPERA', N'Direction Orange Nord de France', N'Distribution Nord de France', N'51100', N'REIMS', N'18 RUE DE VESLE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'51000J27-4', N'IOB', N'CHALONS CROIX DAMPIERRE', N'Direction Orange Nord de France', N'Distribution Nord de France', N'51000', N'CHALONS EN CHAMPAGNE', N' AVENUE DU PRESIDENT ROOSEVELT', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'60740FN9-7', N'IOB', N'SAINT MAXIMIN', N'Direction Orange Nord de France', N'Distribution Nord de France', N'60740', N'ST MAXIMIN', N' ZONE COMMERCIALE ST MAX AVENUE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'60000FN9-5', N'IOB', N'BEAUVAIS', N'Direction Orange Nord de France', N'Distribution Nord de France', N'60000', N'BEAUVAIS', N'5 RUE JEAN RACINE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'80000ZSB-5', N'IOB', N'AMIENS CARRE ROYAL', N'Direction Orange Nord de France', N'Distribution Nord de France', N'80000', N'AMIENS', N'20 RUE DES TROIS CAILLOUX', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'62321FQ9-8', N'IOB', N'BOULOGNE', N'Direction Orange Nord de France', N'Distribution Nord de France', N'62200', N'BOULOGNE SUR MER', N'1 RUE VICTOR HUGO', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'59385FL1-2', N'IOB', N'DUNKERQUE', N'Direction Orange Nord de France', N'Distribution Nord de France', N'59140', N'DUNKERQUE', N'100 BOULEVARD ALEXANDRE III', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'77102FS6-0', N'IOB', N'MEAUX', N'Direction Orange IDF', N'Distribution Portes de Paris', N'77124', N'CHAUCONIN NEUFMONTIERS', N' ZONE D''ACTIVITE COMMERCIALE DU PAYS DE MEAUX', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'77410FS6-4', N'IOB', N'CLAYE SOUILLY', N'Direction Orange IDF', N'Distribution Portes de Paris', N'77410', N'CLAYE SOUILLY', N' ROUTE NATIONALE 3', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'93330FU3-4', N'IOB', N'NOISY LES ARCADES', N'Direction Orange IDF', N'Distribution Portes de Paris', N'93160', N'NOISY LE GRAND', N' CENTRE COMMERCIAL DES ARCADES RDC PORTE 11', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'78180FU9-0', N'IOB', N'MONTIGNY LE BRETONNEUX', N'Direction Orange IDF', N'Distribution Portes de Paris', N'78180', N'MONTIGNY LE BRETONNEUX', N' RUE COLBERT', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'78150FF6-3', N'IOB', N'LE CHESNAY', N'Direction Orange IDF', N'Distribution Portes de Paris', N'78150', N'LE CHESNAY', N'AVENUE CHARLES DE GAULLE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'91440FW6-5', N'IOB', N'LES ULIS', N'Direction Orange IDF', N'Distribution Portes de Paris', N'91940', N'LES ULIS', N' CENTRE COMMERCIAL REGIONAL LES ULIS 2', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'94205FW8-4', N'IOB', N'THIAIS BELLE EPINE', N'Direction Orange IDF', N'Distribution Portes de Paris', N'94320', N'THIAIS', N' CENTRE COMMERCIAL BELLE EPINE RDC PORTE 1', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'94000FV5-6', N'IOB', N'CRETEIL SOLEIL', N'Direction Orange IDF', N'Distribution Portes de Paris', N'94000', N'CRETEIL', N' CENTRE COMMERCIAL CRETEIL SOLEIL', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'91140AHI-5', N'IOB', N'VILLEBON', N'Direction Orange IDF', N'Distribution Portes de Paris', N'91140', N'VILLEBON SUR YVETTE', N' CHEMIN DE BRIIS', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'93600FU3-6', N'IOB', N'AULNAY SOUS BOIS', N'Direction Orange IDF', N'Distribution Portes de Paris', N'93600', N'AULNAY SOUS BOIS', N' CENTRE COMMERCIAL PARINOR', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'943003I4-I', N'IOB', N'VINCENNES', N'Direction Orange IDF', N'Distribution Portes de Paris', N'94300', N'VINCENNES', N'20 AVENUE DU CHATEAU', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'93110FV7-1', N'IOB', N'ROSNY 2', N'Direction Orange IDF', N'Distribution Portes de Paris', N'93110', N'ROSNY SOUS BOIS', N' CENTRE COMMERCIAL ROSNY 2', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'77711FS6-7', N'IOB', N'VAL EUROPE', N'Direction Orange IDF', N'Distribution Portes de Paris', N'77700', N'SERRIS', N'14 COURS DU DANUBE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'94430FV5-1', N'IOB', N'CHENNEVIERES', N'Direction Orange IDF', N'Distribution Portes de Paris', N'94430', N'CHENNEVIERES SUR MARNE', N' CENTRE COMMERCIAL PINCE VENT', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'95001FP8-1', N'IOB', N'CERGY 3 FONTAINES', N'Direction Orange IDF', N'Distribution Portes de Paris', N'95000', N'CERGY', N' CENTRE COMMERCIAL LES 3 FONTAINES ', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'77566FW7-B', N'IOB', N'CARRE SENART', N'Direction Orange IDF', N'Distribution Portes de Paris', N'77127', N'LIEUSAINT', N'3 ALLEE DU PREAMBULE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'75SDRBEAUM', N'IOB', N'PARIS BASTILLE', N'Direction Orange IDF', N'Distribution Ile de France Centre', N'75012', N'PARIS 12', N'40 RUE DU FAUBOURG SAINT ANTOINE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'75SDRREPUB', N'IOB', N'PARIS REPUBLIQUE', N'Direction Orange IDF', N'Distribution Ile de France Centre', N'75003', N'PARIS 03', N'13 PLACE DE LA REPUBLIQUE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'92100FH6-0', N'IOB', N'BOULOGNE BILLANCOURT', N'Direction Orange IDF', N'Distribution Ile de France Centre', N'92100', N'BOULOGNE BILLANCOURT', N'110 BOULEVARD JEAN JAURES', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'75006FQ8-5', N'IOB', N'PARIS RENNES', N'Direction Orange IDF', N'Distribution Ile de France Centre', N'75006', N'PARIS 06', N'128 RUE DE RENNES', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'92300FR8-3', N'IOB', N'LEVALLOIS', N'Direction Orange IDF', N'Distribution Ile de France Centre', N'92300', N'LEVALLOIS PERRET', N'37 RUE DU PRESIDENT WILSON', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'92603FX4-0', N'IOB', N'ASNIERES', N'Direction Orange IDF', N'Distribution Ile de France Centre', N'92600', N'ASNIERES SUR SEINE', N'2 RUE PIERRE BROSSOLETTE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'92400FS7-7', N'IOB', N'LA DEFENSE', N'Direction Orange IDF', N'Distribution Ile de France Centre', N'92800', N'PUTEAUX', N' CENTRE COMMERCIAL LES 4 TEMPS', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'92130FH6-2', N'IOB', N'ISSY LES MOULINEAUX', N'Direction Orange IDF', N'Distribution Ile de France Centre', N'92130', N'ISSY LES MOULINEAUX', N'32 AVENUE DE LA REPUBLIQUE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'92120FP7-3', N'IOB', N'MONTROUGE', N'Direction Orange IDF', N'Distribution Ile de France Centre', N'92120', N'MONTROUGE', N'49 AVENUE DE LA REPUBLIQUE', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'750086YO-K', N'IOB', N'PARIS CHAMPS ELYSEES', N'Direction Orange IDF', N'Distribution Ile de France Centre', N'75008', N'PARIS 08', N'125 AVENUE DES CHAMPS ELYSEES', N'Boutique Orange')
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'38PHSEHERE', N'Non IOB', N'ORANGE GDT ST MARTIN D''HERES', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'69PHSEHUGO', N'Non IOB', N'ORANGE GDT LYON VICTOR HUGO', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'69PHSELYO2', N'Non IOB', N'ORANGE GDT LYON CONFLUENCE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'74PHSESALL', N'Non IOB', N'ORANGE GDT SALLANCHES', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'42PHSEFIRM', N'Non IOB', N'ORANGE GDT FIRMINY', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'38PHSESALA', N'Non IOB', N'ORANGE GDT SALAISE SUR SANNE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'63PHSETHIE', N'Non IOB', N'ORANGE GDT THIERS', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'74PHSEANNE', N'Non IOB', N'ORANGE GDT ANNECY BROGNY', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'74PHSEEPAG', N'Non IOB', N'ORANGE GDT ANNECY GRAND EPAGNY', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'42PHSEVILL', N'Non IOB', N'ORANGE GDT ST ETIENNE VILLARS', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'38PHSESTEG', N'Non IOB', N'ORANGE GDT GRENOBLE ST EGREVE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'74PHSEETRE', N'Non IOB', N'ORANGE GDT ANNEMASSE ETREMBIERES', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'38PHSEECHI', N'Non IOB', N'ORANGE GDT ECHIROLLES', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'73PHSEBASS', N'Non IOB', N'ORANGE GDT BASSENS', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'63PHSECLER', N'Non IOB', N'ORANGE GDT CLERMONT FERRAND', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'01PHSETHOI', N'Non IOB', N'ORANGE GDT VAL THOIRY', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'69PHSEPRIE', N'Non IOB', N'ORANGE GDT LYON ST PRIEST', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'21PHSECHEN', N'Non IOB', N'ORANGE GDT CHENOVE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'67PHSEILLK', N'Non IOB', N'ORANGE GDT STRASBOURG BAGGERSEE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'67PHSEHAUT', N'Non IOB', N'ORANGE GDT STRASBOURG HAUTEPIERRE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'71PHSECREC', N'Non IOB', N'ORANGE GDT CRECHES SUR SAONE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'58PHSECOSN', N'Non IOB', N'ORANGE GDT COSNE SUR LOIRE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'89PHSEAVAL', N'Non IOB', N'ORANGE GDT AVALLON', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'71PHSEPARA', N'Non IOB', N'ORANGE GDT PARAY LE MONIAL', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'57PHSEMOUL', N'Non IOB', N'ORANGE GDT MOULINS LES METZ', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'54PHSEFROU', N'Non IOB', N'ORANGE GDT FROUARD', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'88PHSEVITT', N'Non IOB', N'ORANGE GDT VITTEL', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'39PHSECHAM', N'Non IOB', N'ORANGE GDT CHAMPAGNOLE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'25PHSEECOL', N'Non IOB', N'ORANGE GDT ECOLE VALENTIN', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'71PHSECHA1', N'Non IOB', N'ORANGE GDT CHALON SUR SAONE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'71PHSELOUH', N'Non IOB', N'ORANGE GDT LOUHANS', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'70PHSEGRAY', N'Non IOB', N'ORANGE GDT GRAY', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'21PHSECHAT', N'Non IOB', N'ORANGE GDT CHATILLON', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'89PHSEJOIG', N'Non IOB', N'ORANGE GDT JOIGNY', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'71PHSEAUTU', N'Non IOB', N'ORANGE GDT AUTUN', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'55PHSEBARD', N'Non IOB', N'ORANGE GDT BAR LE DUC', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'67PHSEMOLS', N'Non IOB', N'ORANGE GDT MOLSHEIM', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'54PHSELUNE', N'Non IOB', N'ORANGE GDT LUNEVILLE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'67PHSESELE', N'Non IOB', N'ORANGE GDT SELESTAT', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'68PHSEMULW', N'Non IOB', N'ORANGE GDT MULHOUSE WITTENHEIM', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'67PHSESCHW', N'Non IOB', N'ORANGE GDT SCHWEIGHOUSE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'58PHSEMARZ', N'Non IOB', N'ORANGE GDT NEVERS', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'92PHSEOUES', N'Non IOB', N'ORANGE GDT LEVALLOIS SO OUEST', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'75PHSEBELL', N'Non IOB', N'ORANGE GDT PARIS BELLEVILLE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'75PHSEOUEN', N'Non IOB', N'ORANGE MONEY PARIS ST OUEN', N'Direction Orange IDF', N'Distribution Ile de France Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'75PHSEBONN', N'Non IOB', N'ORANGE MONEY PARIS BONNE NOUVELLE', N'Direction Orange IDF', N'Distribution Ile de France Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'75PHSEFLAN', N'Non IOB', N'ORANGE GDT PARIS FLANDRE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'75PHSESEVR', N'Non IOB', N'ORANGE GDT PARIS RUE DE SEVRES', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'75PHSEPA15', N'Non IOB', N'ORANGE GDT PARIS VAUGIRARD', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'75PHSEITAL', N'Non IOB', N'ORANGE GDT PARIS ITALIE 2', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'75PHSEPAR1', N'Non IOB', N'ORANGE GDT PARIS FORUM', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'75PHSEPA14', N'Non IOB', N'ORANGE GDT PARIS ALESIA', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'75PHSEPA01', N'Non IOB', N'ORANGE GDT PARIS BEAUGRENELLE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'91PHSEVIRY', N'Non IOB', N'ORANGE GDT VIRY CHATILLON', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'93PHSESEIN', N'Non IOB', N'ORANGE GDT EPINAY SUR SEINE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'93PHSESEVR', N'Non IOB', N'ORANGE GDT SEVRAN', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'77PHSENEMO', N'Non IOB', N'ORANGE GDT NEMOURS', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'78PHSEMANT', N'Non IOB', N'ORANGE GDT MANTES GAMBETTA', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'93PHSEDRA1', N'Non IOB', N'ORANGE GDT DRANCY', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'94PHSENOGE', N'Non IOB', N'ORANGE GDT NOGENT SUR MARNE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'91PHSEVILL', N'Non IOB', N'ORANGE GDT VILLABE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'78PHSECHAM', N'Non IOB', N'ORANGE GDT CHAMBOURCY', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'93PHSEBAGN', N'Non IOB', N'ORANGE GDT BAGNOLET', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'78PHSEMABU', N'Non IOB', N'ORANGE GDT MANTES-BUCHELAY', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'77PHSTMELU', N'Non IOB', N'ORANGE GDT MELUN', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'78PHSEVELI', N'Non IOB', N'ORANGE GDT VELIZY', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'78PHSERAM1', N'Non IOB', N'ORANGE GDT RAMBOUILLET', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'95PHSESTBR', N'Non IOB', N'ORANGE GDT ST BRICE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'94PHSEBERC', N'Non IOB', N'ORANGE GDT BERCY', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'78PHSESART', N'Non IOB', N'ORANGE GDT SARTROUVILLE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'95PHSEERMO', N'Non IOB', N'ORANGE GDT ERMONT', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'78PHSEMAUR', N'Non IOB', N'ORANGE GDT MAUREPAS', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'51PHSETINQ', N'Non IOB', N'ORANGE GDT TINQUEUX', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'59PHSETOUR', N'Non IOB', N'ORANGE GDT TOURCOING', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'62PHSECALA', N'Non IOB', N'ORANGE GDT CALAIS', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'60PHSEBEAU', N'Non IOB', N'ORANGE GDT BEAUVAIS', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'59PHSEROUB', N'Non IOB', N'ORANGE GDT ROUBAIX', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'62PHSELENS', N'Non IOB', N'ORANGE GDT LENS', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'51PHSESTBR', N'Non IOB', N'ORANGE GDT SAINT BRICE COURCELLES', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'10PHSESTPA', N'Non IOB', N'ORANGE GDT SAINT PARRES AUX TERTRES', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'02PHSECHAU', N'Non IOB', N'ORANGE GDT CHAUNY', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'80PHSEPERO', N'Non IOB', N'ORANGE GDT PERONNE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'60PHSECREP', N'Non IOB', N'ORANGE GDT CREPY EN VALOIS', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'60PHSELACH', N'Non IOB', N'ORANGE GDT LA CHAPELLE EN SERVAL', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'62PHSESTPO', N'Non IOB', N'ORANGE GDT ST POL SUR TERNOISE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'80PHSEMERS', N'Non IOB', N'ORANGE GDT MERS LES BAINS', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'08PHSESEDA', N'Non IOB', N'ORANGE GDT SEDAN', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'10PHSEROMI', N'Non IOB', N'ORANGE GDT ROMILLY SUR SEINE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'60PHSENOGE', N'Non IOB', N'ORANGE GDT CREIL NOGENT', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'59PHSEVALE', N'Non IOB', N'ORANGE GDT VALENCIENNES', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'59PHSELEER', N'Non IOB', N'ORANGE GDT LEERS', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'59PHSTSYNT', N'Non IOB', N'ORANGE GDT GRANDE SYNTHE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'59PHSTAULN', N'Non IOB', N'ORANGE GDT AULNOY VALENCIENNES', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'80PHSEFASH', N'Non IOB', N'ORANGE GDT AMIENS GLISY', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'62PHSEBETH', N'Non IOB', N'ORANGE GDT BETHUNE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'60PHSECOMP', N'Non IOB', N'ORANGE GDT COMPIEGNE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'27PHSELOUV', N'Non IOB', N'ORANGE GDT LOUVIERS', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'14PHSEORNE', N'Non IOB', N'ORANGE GDT CAEN', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'27PHSEVILL', N'Non IOB', N'ORANGE GDT EVREUX GUICHAINVILLE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'76PHSEVILL', N'Non IOB', N'ORANGE GDT GONFREVILLE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'37PHSEARCH', N'Non IOB', N'ORANGE GDT TOURS PETITE ARCHE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'45PHSERUEL', N'Non IOB', N'ORANGE GDT ST JEAN DE RUELLE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'28PHSELUCE', N'Non IOB', N'ORANGE GDT CHARTRES LUCE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'76PHSEDIE2', N'Non IOB', N'ORANGE GDT DIEPPE LA BARRE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'61PHSEAIGL', N'Non IOB', N'ORANGE GDT L''AIGLE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'18PHSESTDO', N'Non IOB', N'ORANGE GDT SAINT DOULCHARD', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'18PHSESTAM', N'Non IOB', N'ORANGE GDT SAINT AMAND MONTROND', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'36PHSEARGE', N'Non IOB', N'ORANGE GDT ARGENTON SUR CREUSE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'37PHSELOCH', N'Non IOB', N'ORANGE GDT LOCHES', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'28PHSEMARG', N'Non IOB', N'ORANGE GDT MARGON', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'27PHSEBERN', N'Non IOB', N'ORANGE GDT BERNAY', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'36PHSEPOIN', N'Non IOB', N'ORANGE GDT LE POINCONNET', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'50PHSECHER', N'Non IOB', N'ORANGE GDT CHERBOURG ', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'14PHSEVIRE', N'Non IOB', N'ORANGE GDT VIRE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'50PHSEGRAN', N'Non IOB', N'ORANGE GDT GRANVILLE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'41PHSEVEND', N'Non IOB', N'ORANGE GDT VENDOME', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'61PHSEARGE', N'Non IOB', N'ORANGE GDT ARGENTAN', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'18PHSEVIER', N'Non IOB', N'ORANGE GDT VIERZON', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'45PHSEPITH', N'Non IOB', N'ORANGE GDT PITHIVIERS', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'28PHSECHAT', N'Non IOB', N'ORANGE GDT CHATEAUDUN', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'50PHSECOUT', N'Non IOB', N'ORANGE GDT COUTANCES', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'76PHSENEBR', N'Non IOB', N'ORANGE GDT NEUFCHATEL NOTRE DAME', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'35PHSEVITR', N'Non IOB', N'ORANGE GDT VITRE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'72PHSESABL', N'Non IOB', N'ORANGE GDT SABLE SUR SARTHE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'85PHSELUCO', N'Non IOB', N'ORANGE GDT LUCON', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'44PHSESTGE', N'Non IOB', N'ORANGE GDT SAINT GEREON', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'85PHSEBOUF', N'Non IOB', N'ORANGE GDT BOUFFERE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'29PHSELAND', N'Non IOB', N'ORANGE GDT LANDERNEAU', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'29PHSECONC', N'Non IOB', N'ORANGE GDT CONCARNEAU', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'22PHSEPAIM', N'Non IOB', N'ORANGE GDT PAIMPOL', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'44PHSEGUER', N'Non IOB', N'ORANGE GDT GUERANDE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'44PHSEPORN', N'Non IOB', N'ORANGE GDT PORNIC', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'56PHSEPLOE', N'Non IOB', N'ORANGE GDT PLOERMEL', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'44PHSECHAT', N'Non IOB', N'ORANGE GDT CHATEAUBRIANT', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'85PHSEFONT', N'Non IOB', N'ORANGE GDT FONTENAY LE COMTE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'29PHSECARH', N'Non IOB', N'ORANGE GDT CARHAIX', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'35PHSERENN', N'Non IOB', N'ORANGE GDT RENNES COLOMBIA', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'44PHSENANT', N'Non IOB', N'ORANGE GDT NANTES BEAULIEU', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'20PHSEGHIS', N'Non IOB', N'ORANGE GDT GHISONACCIA', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'20PHSEPROP', N'Non IOB', N'ORANGE GDT PROPRIANO', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'20PHSECORT', N'Non IOB', N'ORANGE GDT CORTE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'20PHSEFURI', N'Non IOB', N'ORANGE GDT FURIANI', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'20PHSESARR', N'Non IOB', N'ORANGE GDT AJACCIO BALEONE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'13PHSEISTR', N'Non IOB', N'ORANGE GDT ISTRES', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'06PHSETRIN', N'Non IOB', N'ORANGE GDT LA TRINITE VICTOR', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'83PHSEHYER', N'Non IOB', N'ORANGE GDT HYERES', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'13PHSECABR', N'Non IOB', N'ORANGE GDT PLAN DE CAMPAGNE AVANT CAP', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'06PHSEDORE', N'Non IOB', N'ORANGE GDT NICE ST ISIDORE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'13PHSELITT', N'Non IOB', N'ORANGE GDT MARSEILLE GRAND LITTORAL', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'83PHSESEYN', N'Non IOB', N'ORANGE GDT LA SEYNE SUR MER', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'84PHSEAVIC', N'Non IOB', N'ORANGE GDT AVIGNON CENTRE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'05PHSEBRIA', N'Non IOB', N'ORANGE GDT BRIANCON', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'04PHSESIST', N'Non IOB', N'ORANGE GDT SISTERON', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'84PHSEPERT', N'Non IOB', N'ORANGE GDT PERTUIS', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'84PHSEAPT1', N'Non IOB', N'ORANGE GDT APT', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'07PHSETOUR', N'Non IOB', N'ORANGE GDT TOURNON SUR RHONE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'26PHSENYON', N'Non IOB', N'ORANGE GDT NYONS', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'07PHSEPRIV', N'Non IOB', N'ORANGE GDT PRIVAS', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'84PHSECAR1', N'Non IOB', N'ORANGE GDT CARPENTRAS', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'06PHSECAGN', N'Non IOB', N'ORANGE GDT CAGNES', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'07PHSEDAVE', N'Non IOB', N'ORANGE GDT DAVEZIEUX', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'26PHSEVALE', N'Non IOB', N'ORANGE GDT VALENCE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'83PHSESTRA', N'Non IOB', N'ORANGE GDT SAINT RAPHAEL', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'06PHSEBEAU', N'Non IOB', N'ORANGE GDT BEAUSOLEIL', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'84PHSECAVA', N'Non IOB', N'ORANGE GDT CAVAILLON', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'06PHSEETOI', N'Non IOB', N'ORANGE GDT NICE ETOILE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'06PHSEANTI', N'Non IOB', N'ORANGE GDT ANTIBES', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'13PHSEMAR9', N'Non IOB', N'ORANGE GDT MARSEILLE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'07PHSEGUIL', N'Non IOB', N'ORANGE GDT GUILHERAND GRANGES', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'64PHSEPAU6', N'Non IOB', N'ORANGE GDT PAU', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'16PHSERONN', N'Non IOB', N'ORANGE GDT LA COURONNE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'33PHSELALI', N'Non IOB', N'ORANGE GDT SAINTE EULALIE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'79PHSEPART', N'Non IOB', N'ORANGE GDT PARTHENAY', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'47PHSEVILL', N'Non IOB', N'ORANGE GDT VILLENEUVE SUR LOT', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'33PHSELESP', N'Non IOB', N'ORANGE GDT LESPARRE MEDOC', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'19PHSEUSSE', N'Non IOB', N'ORANGE GDT USSEL', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'64PHSEOLOR', N'Non IOB', N'ORANGE GDT OLORON SAINTE MARIE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'40PHSETARN', N'Non IOB', N'ORANGE GDT TARNOS', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'40PHSEBISC', N'Non IOB', N'ORANGE GDT BISCARROSSE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'19PHSEBRIV', N'Non IOB', N'ORANGE GDT BRIVE LA GAILLARDE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'64PHSEPAU1', N'Non IOB', N'ORANGE GDT PAU SALLENAVE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'87PHSESTJU', N'Non IOB', N'ORANGE GDT SAINT JUNIEN', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'79PHSETHOU', N'Non IOB', N'ORANGE GDT THOUARS', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'87PHSELIM1', N'Non IOB', N'ORANGE GDT LIMOGES', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'79PHSENIOR', N'Non IOB', N'ORANGE GDT NIORT', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'64PHSESTJL', N'Non IOB', N'ORANGE GDT ST JEAN DE LUZ VICTOR HUGO', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'64PHSEPYRE', N'Non IOB', N'ORANGE GDT PAU PALAIS DES PYRENEES', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'33PHSEPESS', N'Non IOB', N'ORANGE GDT PESSAC', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'24PHSEMARS', N'Non IOB', N'ORANGE GDT PERIGUEUX', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'47PHSEABOE', N'Non IOB', N'ORANGE GDT AGEN BOE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'86PHSEPOIT', N'Non IOB', N'ORANGE GDT POITIERS', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'33PHSEMERI', N'Non IOB', N'ORANGE GDT MERIADECK', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'87PHSELIBO', N'Non IOB', N'ORANGE GDT LIMOGES BOISSEUIL', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'81PHSETRES', N'Non IOB', N'ORANGE GDT CASTRES', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'82PHSEMONT', N'Non IOB', N'ORANGE GDT MONTAUBAN', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'66PHSEFONT', N'Non IOB', N'ORANGE GDT FONT ROMEU ODEILLO VIA', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'81PHSEMAZA', N'Non IOB', N'ORANGE GDT MAZAMET', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'12PHSEVILL', N'Non IOB', N'ORANGE GDT VILLEFRANCHE DE ROUERGUE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'82PHSEMOIS', N'Non IOB', N'ORANGE GDT MOISSAC', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'32PHSENOGA', N'Non IOB', N'ORANGE GDT NOGARO', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'12PHSEONET', N'Non IOB', N'ORANGE GDT ONET LE CHATEAU', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'34PHSESTJE', N'Non IOB', N'ORANGE GDT SAINT JEAN DE VEDAS', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'09PHSESTGI', N'Non IOB', N'ORANGE GDT ST GIRONS', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'65PHSEIBOS', N'Non IOB', N'ORANGE GDT IBOS', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'31PHSELABE', N'Non IOB', N'ORANGE GDT TOULOUSE LABEGE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'66PHSEPERP', N'Non IOB', N'ORANGE GDT PERPIGNAN', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'31PHSETOUL', N'Non IOB', N'ORANGE GDT TOULOUSE PURPAN', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'31PHSEPORT', N'Non IOB', N'ORANGE GDT PORTET SUR GARONNE', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'34PHSEBALA', N'Non IOB', N'ORANGE GDT BALARUC', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'30PHSEANGL', N'Non IOB', N'ORANGE GDT AVIGNON LES ANGLES', N'GDT', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'34500FX1-1', N'Non IOB', N'BEZIERS RIVE GAUCHE', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'97490STC-1', N'Non IOB', N'SAINTE CLOTILDE', N'Direction Orange Réunion-Mayotte', N'Adrc Reunion Mayotte', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'13600BOUT1', N'Non IOB', N'LA CIOTAT PARK', N'Direction Orange Sud Est', N'Distribution Sud Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'93300AUB-1', N'Non IOB', N'AUBERVILLIERS LE MILLENAIRE', N'Direction Orange IDF', N'Distribution Portes de Paris', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'62000BOUT2', N'Non IOB', N'ARRAS CHURCHILL', N'Direction Orange Nord de France', N'Distribution Nord de France', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'97231FM5-1', N'Non IOB', N'LE ROBERT', N'Direction Orange Caraïbes', N'Distribution Caraïbes', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'97232FM3-3', N'Non IOB', N'LE LAMENTIN ESPACE PRO', N'Direction Orange Caraïbes', N'Distribution Caraïbes', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'93290FX5-5', N'Non IOB', N'ROISSY AEROVILLE', N'Direction Orange IDF', N'Distribution Portes de Paris', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'13002MAR-2', N'Non IOB', N'MARSEILLE LES TERRASSES DU PORT', N'Direction Orange Sud Est', N'Distribution Sud Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'92390GA2-1', N'Non IOB', N'VILLENEUVE LA GARENNE', N'Direction Orange IDF', N'Distribution Portes de Paris', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'97416SLP-1', N'Non IOB', N'SAINT LEU PORTAIL', N'Direction Orange Réunion-Mayotte', N'Adrc Reunion Mayotte', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'97430KI5-9', N'Non IOB', N'LE TAMPON', N'Direction Orange Réunion-Mayotte', N'Adrc Reunion Mayotte', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'97410CAN-9', N'Non IOB', N'SAINT PIERRE CANABADY', N'Direction Orange Réunion-Mayotte', N'Adrc Reunion Mayotte', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'97480M3M-5', N'Non IOB', N'SAINT JOSEPH', N'Direction Orange Réunion-Mayotte', N'Adrc Reunion Mayotte', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'97438SAI-4', N'Non IOB', N'SAINTE MARIE', N'Direction Orange Réunion-Mayotte', N'Adrc Reunion Mayotte', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'97440TE6-N', N'Non IOB', N'SAINT ANDRE', N'Direction Orange Réunion-Mayotte', N'Adrc Reunion Mayotte', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'97470BEN-1', N'Non IOB', N'SAINT BENOIT', N'Direction Orange Réunion-Mayotte', N'Adrc Reunion Mayotte', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'97410ENF-4', N'Non IOB', N'BONS ENFANTS', N'Direction Orange Réunion-Mayotte', N'Adrc Reunion Mayotte', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'97441SUZ-1', N'Non IOB', N'SAINTE SUZANNE', N'Direction Orange Réunion-Mayotte', N'Adrc Reunion Mayotte', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'97400DEN-7', N'Non IOB', N'SAINT DENIS CHATEL', N'Direction Orange Réunion-Mayotte', N'Adrc Reunion Mayotte', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'97420SCO-6', N'Non IOB', N'JUMBO SCORE LE PORT', N'Direction Orange Réunion-Mayotte', N'Adrc Reunion Mayotte', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'97460M0R-3', N'Non IOB', N'SAINT PAUL', N'Direction Orange Réunion-Mayotte', N'Adrc Reunion Mayotte', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'21200FT4-6', N'Non IOB', N'BEAUNE', N'Direction Orange Est', N'Direction Orange Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'21000FT4-A', N'Non IOB', N'DIJON LIBERTE', N'Direction Orange Est', N'Direction Orange Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'21800935-G', N'Non IOB', N'DIJON QUETIGNY', N'Direction Orange Est', N'Direction Orange Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'39108FN7-4', N'Non IOB', N'DOLE', N'Direction Orange Est', N'Direction Orange Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'57210FU8-3', N'Non IOB', N'SEMECOURT', N'Direction Orange Est', N'Direction Orange Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'57311FH5-3', N'Non IOB', N'THIONVILLE CITY CENTER', N'Direction Orange Est', N'Direction Orange Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'57501FV2-4', N'Non IOB', N'SAINT AVOLD', N'Direction Orange Est', N'Direction Orange Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'54400FH5-2', N'Non IOB', N'MONT ST MARTIN', N'Direction Orange Est', N'Direction Orange Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'39021FN7-5', N'Non IOB', N'LONS LE SAUNIER LIBERTE', N'Direction Orange Est', N'Direction Orange Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'71300FB3-4', N'Non IOB', N'MONTCEAU LES MINES', N'Direction Orange Est', N'Direction Orange Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'25200FF8-3', N'Non IOB', N'MONTBELIARD', N'Direction Orange Est', N'Direction Orange Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'25300FD6-1', N'IOB', N'PONTARLIER REPUBLIQUE', N'Direction Orange Est', N'Direction Orange Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'68300FF2-1', N'Non IOB', N'SAINT LOUIS', N'Direction Orange Est', N'Direction Orange Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'70014FR5-2', N'Non IOB', N'VESOUL CENTRE', N'Direction Orange Est', N'Direction Orange Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'90025FF8-2', N'Non IOB', N'BELFORT CENTRE VILLE', N'Direction Orange Est', N'Direction Orange Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'25026FD6-1', N'Non IOB', N'BESANCON GRANDE RUE', N'Direction Orange Est', N'Direction Orange Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'88100FE1-1', N'Non IOB', N'SAINT DIE', N'Direction Orange Est', N'Direction Orange Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'68110UU0-4', N'Non IOB', N'ILLZACH', N'Direction Orange Est', N'Direction Orange Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'88207FE1-4', N'Non IOB', N'REMIREMONT', N'Direction Orange Est', N'Direction Orange Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'67000FX1-1', N'Non IOB', N'STRASBOURG RIVETOILE', N'Direction Orange Est', N'Direction Orange Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'67700FF5-4', N'Non IOB', N'SAVERNE', N'Direction Orange Est', N'Direction Orange Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'57200FV2-2', N'Non IOB', N'SARREGUEMINES', N'Direction Orange Est', N'Direction Orange Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'57400FV2-1', N'Non IOB', N'SARREBOURG', N'Direction Orange Est', N'Direction Orange Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'88000LVU-5', N'Non IOB', N'EPINAL CARREFOUR', N'Direction Orange Est', N'Direction Orange Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'55100FE7-2', N'Non IOB', N'VERDUN', N'Direction Orange Est', N'Direction Orange Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'541803EV-5', N'IOB', N'HOUDEMONT', N'Direction Orange Est', N'Direction Orange Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'89100FM9-1', N'Non IOB', N'SENS', N'Direction Orange Est', N'Direction Orange Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'85340KG8-5', N'Non IOB', N'OLONNE SUR MER', N'Direction Orange Ouest', N'Distribution Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'85500FC7-5', N'Non IOB', N'LES HERBIERS', N'Direction Orange Ouest', N'Distribution Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'49321FU7-2', N'IOB', N'CHOLET', N'Direction Orange Ouest', N'Distribution Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'85008FC7-1', N'Non IOB', N'LA ROCHE SUR YON CLEMENCEAU', N'Direction Orange Ouest', N'Distribution Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'53100FF5-3', N'Non IOB', N'MAYENNE', N'Direction Orange Ouest', N'Distribution Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'72000FU5-0', N'Non IOB', N'LE MANS GALERIE BLONDEAU', N'Direction Orange Ouest', N'Distribution Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'72100FU5-C', N'Non IOB', N'LE MANS CENTRE SUD', N'Direction Orange Ouest', N'Distribution Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'53015FF5-2', N'Non IOB', N'LAVAL MEDIAPOLE', N'Direction Orange Ouest', N'Distribution Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'72000FU5-3', N'Non IOB', N'LE MANS MINIMES', N'Direction Orange Ouest', N'Distribution Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'35305FZ9-2', N'Non IOB', N'FOUGERES', N'Direction Orange Ouest', N'Distribution Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'44300FM8-2', N'IOB', N'NANTES PARIDIS', N'Direction Orange Ouest', N'Distribution Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'44230FC1-5', N'Non IOB', N'SAINT SEBASTIEN', N'Direction Orange Ouest', N'Distribution Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'35760FV1-5', N'IOB', N'RENNES GRAND QUARTIER', N'Direction Orange Ouest', N'Distribution Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'22018FW5-4', N'Non IOB', N'DINAN', N'Direction Orange Ouest', N'Distribution Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'35510FG6-3', N'Non IOB', N'CESSON LA RIGOURDIERE', N'Direction Orange Ouest', N'Distribution Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'35000FV1-1', N'IOB', N'RENNES REPUBLIQUE', N'Direction Orange Ouest', N'Distribution Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'29600FT2-1', N'Non IOB', N'SAINT MARTIN DES CHAMPS', N'Direction Orange Ouest', N'Distribution Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'29200FT2-2', N'Non IOB', N'BREST CCIAL GEANT', N'Direction Orange Ouest', N'Distribution Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'22000FW5-B', N'Non IOB', N'SAINT BRIEUC ST GUILLAUME', N'Direction Orange Ouest', N'Distribution Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'29200335-G', N'Non IOB', N'BREST IROISE', N'Direction Orange Ouest', N'Distribution Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'22205FW5-1', N'Non IOB', N'GUINGAMP', N'Direction Orange Ouest', N'Distribution Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'22303FW5-2', N'Non IOB', N'LANNION', N'Direction Orange Ouest', N'Distribution Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'56321FX7-2', N'Non IOB', N'LORIENT ARISTIDE', N'Direction Orange Ouest', N'Distribution Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'29334FJ6-4', N'Non IOB', N'QUIMPER CORENTIN', N'Direction Orange Ouest', N'Distribution Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'56400FM3-2', N'Non IOB', N'AURAY', N'Direction Orange Ouest', N'Distribution Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'56305FX7-1', N'Non IOB', N'PONTIVY', N'Direction Orange Ouest', N'Distribution Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'56019FM3-3', N'Non IOB', N'VANNES REPUBLIQUE', N'Direction Orange Ouest', N'Distribution Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'35032FV1-2', N'Non IOB', N'REDON', N'Direction Orange Ouest', N'Distribution Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'49043FK1-2', N'Non IOB', N'ANGERS COEUR DE VILLE', N'Direction Orange Ouest', N'Distribution Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'49000FK1-A', N'Non IOB', N'ANGERS GRAND MAINE', N'Direction Orange Ouest', N'Distribution Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'49421FU7-3', N'Non IOB', N'SAUMUR', N'Direction Orange Ouest', N'Distribution Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'69317FG4-0', N'Non IOB', N'LYON CROIX ROUSSE', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'69100FY9-3', N'Non IOB', N'LYON VILLEURBANNE', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'38205FD8-3', N'Non IOB', N'VIENNE', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'01500FG1-5', N'Non IOB', N'AMBERIEU', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'01100FG1-7', N'Non IOB', N'OYONNAX', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'69665FT8-2', N'Non IOB', N'VILLEFRANCHE SUR SAONE', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'74013FL8-0', N'Non IOB', N'SEYNOD', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'74303FL8-1', N'Non IOB', N'CLUSES', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'01210FV3-1', N'Non IOB', N'FERNEY VOLTAIRE', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'73103FR2-4', N'Non IOB', N'AIX LES BAINS', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'42100FD5-2', N'Non IOB', N'SAINT ETIENNE CENTRE 2', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'63500FF7-9', N'Non IOB', N'ISSOIRE', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'42100746-1', N'Non IOB', N'SAINT ETIENNE MONTHIEU', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'15000FV9-3', N'Non IOB', N'AURILLAC', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'15100FV9-4', N'Non IOB', N'SAINT FLOUR', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'38000FS4-2', N'Non IOB', N'GRENOBLE GRAND''PLACE', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'38240VEZ-5', N'Non IOB', N'MEYLAN', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'38505FK0-4', N'Non IOB', N'VOIRON', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'38130SPF-3', N'Non IOB', N'ECHIROLLES COMBOIRE', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'73000FR2-7', N'Non IOB', N'CHAMBERY GENEVE', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'63170FF7-5', N'IOB', N'AUBIERE PLEIN SUD', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'03018FQ4-0', N'Non IOB', N'MOULINS', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'63200NVL-5', N'Non IOB', N'RIOM', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'03100FQ4-1', N'IOB', N'MONTLUCON', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'69120N0P-5', N'Non IOB', N'VAULX EN VELIN SEPT CHEMINS', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'42600FE6-4', N'Non IOB', N'MONTBRISON', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'69371FL7-4', N'Non IOB', N'VENISSIEUX', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'69230PE0-5', N'Non IOB', N'SAINT GENIS 2', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'69700MUC-2', N'Non IOB', N'GIVORS DEUX VALLEES', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'69009FG4-1', N'Non IOB', N'LYON ECULLY GRAND OUEST', N'Direction Orange Centre Est', N'Distribution Rhone Alpes Auvergne', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'61100FM7-2', N'Non IOB', N'FLERS', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'50300FP2-2', N'Non IOB', N'AVRANCHES', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'18023FD2-0', N'Non IOB', N'BOURGES CUJAS', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'45503FN5-2', N'Non IOB', N'GIEN', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'45000FN5-A', N'Non IOB', N'ORLEANS REPUBLIQUE', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'45770FX1-1', N'IOB', N'ORLEANS SARAN', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'45207FN5-3', N'Non IOB', N'MONTARGIS', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'97600KAW-3', N'Non IOB', N'MAMOUDZOU KAWENI', N'Direction Orange Réunion-Mayotte', N'Adrc Reunion Mayotte', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'37501FE2-4', N'Non IOB', N'CHINON', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'37170FE2-9', N'Non IOB', N'CHAMBRAY LES TOURS', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'37700FX1-1', N'Non IOB', N'TOURS LES ATLANTES', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'27500FF1-3', N'Non IOB', N'PONT AUDEMER', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'14100FC3-1', N'Non IOB', N'LISIEUX', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'27006FF1-1', N'IOB', N'EVREUX OURSEL', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'41350HFQ-Q', N'Non IOB', N'BLOIS VINEUIL', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'28100FU1-3', N'Non IOB', N'DREUX', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'41000FP6-1', N'Non IOB', N'BLOIS ORFEVRES', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'41200FP6-2', N'Non IOB', N'ROMORANTIN', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'76360FC9-B', N'Non IOB', N'BARENTIN', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'76190FC9-4', N'Non IOB', N'YVETOT', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'76173FC9-0', N'Non IOB', N'MONT SAINT AIGNAN', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'27200FF1-E', N'Non IOB', N'VERNON SAINT JACQUES', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'7620090I-Y', N'Non IOB', N'DIEPPE BELVEDERE', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'76400FK2-3', N'Non IOB', N'FECAMP', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'76600FK2-8', N'Non IOB', N'LE HAVRE COTY', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'14124FZ8-5', N'IOB', N'MONDEVILLE', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'14400FC3-3', N'Non IOB', N'BAYEUX', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'14200FZ8-1', N'Non IOB', N'HEROUVILLE SAINT CLAIR', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'14800FC3-2', N'Non IOB', N'DEAUVILLE', N'Direction Orange Normandie Centre', N'Distribution Normandie Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'97122DE1-4', N'Non IOB', N'DESTRELAND', N'Direction Orange Caraïbes', N'Distribution Caraïbes', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'97110POI-3', N'Non IOB', N'POINTE A PITRE', N'Direction Orange Caraïbes', N'Distribution Caraïbes', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'97122JAR-1', N'Non IOB', N'JARRY', N'Direction Orange Caraïbes', N'Distribution Caraïbes', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'97139MIL-4', N'Non IOB', N'MILENIS LES ABYMES', N'Direction Orange Caraïbes', N'Distribution Caraïbes', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'97150MAR-0', N'Non IOB', N'SAINT MARTIN', N'Direction Orange Caraïbes', N'Distribution Caraïbes', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'97133BAR-9', N'Non IOB', N'SAINT BARTHELEMY', N'Direction Orange Caraïbes', N'Distribution Caraïbes', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'97100BDT-2', N'Non IOB', N'BASSE TERRE', N'Direction Orange Caraïbes', N'Distribution Caraïbes', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'97300VOL-7', N'Non IOB', N'CAYENNE MONTJOLY', N'Direction Orange Caraïbes', N'Distribution Caraïbes', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'97300CAY-6', N'Non IOB', N'CAYENNE MATOURY', N'Direction Orange Caraïbes', N'Distribution Caraïbes', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'97200POR-5', N'Non IOB', N'FORT DE FRANCE', N'Direction Orange Caraïbes', N'Distribution Caraïbes', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'97232PLA-6', N'Non IOB', N'LAMENTIN PLACE D ARMES', N'Direction Orange Caraïbes', N'Distribution Caraïbes', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'97232LAM-8', N'Non IOB', N'LAMENTIN GALLERIA', N'Direction Orange Caraïbes', N'Distribution Caraïbes', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'81013FC4-2', N'Non IOB', N'ALBI', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'81000ALB-F', N'Non IOB', N'ALBI FONLABOUR', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'811006GC-5', N'Non IOB', N'CASTRES GRAND SUD', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'11090LYE-1', N'IOB', N'CARCASSONNE SALVAZA', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'46100FV4-4', N'Non IOB', N'FIGEAC', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'46005FV4-3', N'Non IOB', N'CAHORS', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'31650X2Y-3', N'Non IOB', N'SAINT ORENS', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'31150IZU-5', N'IOB', N'FENOUILLET', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'1110099L-Q', N'IOB', N'NARBONNE GEANT', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'11108FA2-3', N'Non IOB', N'NARBONNE JAURES', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'66965FM1-1', N'Non IOB', N'PERPIGNAN VAUBAN', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'66530FX1-1', N'Non IOB', N'PERPIGNAN CLAIRA', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'30932FB1-4', N'Non IOB', N'NIMES COUPOLE', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'30200FX9-1', N'Non IOB', N'BAGNOLS SUR CEZE', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'48005FW4-1', N'Non IOB', N'MENDE', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'31000FQ1-3', N'IOB', N'TOULOUSE ESQUIROL', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'310004OQ-5', N'Non IOB', N'TOULOUSE WILSON', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'311204IM-2', N'Non IOB', N'ROQUES SUR GARONNE', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'65015FJ7-1', N'Non IOB', N'TARBES', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'32008FF3-2', N'Non IOB', N'AUCH', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'31806FX8-2', N'Non IOB', N'SAINT GAUDENS', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'34035FC6-1', N'Non IOB', N'MONTPELLIER POLYGONE', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'34200FK6-1', N'Non IOB', N'SETE', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'12100FF4-2', N'Non IOB', N'MILLAU', N'Direction Orange Sud', N'Distribution Sud Ouest Méditerranée', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'84100FR6-5', N'Non IOB', N'ORANGE', N'Direction Orange Sud Est', N'Distribution Sud Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'84000FR6-B', N'Non IOB', N'AVIGNON MISTRAL 7', N'Direction Orange Sud Est', N'Distribution Sud Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'13200FE3-1', N'IOB', N'ARLES', N'Direction Orange Sud Est', N'Distribution Sud Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'07200FK7-1', N'Non IOB', N'AUBENAS', N'Direction Orange Sud Est', N'Distribution Sud Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'06500FD7-5', N'Non IOB', N'MENTON', N'Direction Orange Sud Est', N'Distribution Sud Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'06200W4C-1', N'Non IOB', N'NICE LINGOSTIERE', N'Direction Orange Sud Est', N'Distribution Sud Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'06000GX6-5', N'Non IOB', N'NICE MAGENTA', N'Direction Orange Sud Est', N'Distribution Sud Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'06000XDY-B', N'Non IOB', N'NICE JEAN MEDECIN', N'Direction Orange Sud Est', N'Distribution Sud Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'13008FM5-4', N'Non IOB', N'MARSEILLE BONNEVEINE', N'Direction Orange Sud Est', N'Distribution Sud Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'13400FV6-7', N'IOB', N'AUBAGNE ', N'Direction Orange Sud Est', N'Distribution Sud Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'83190FC5-A', N'Non IOB', N'OLLIOULES', N'Direction Orange Sud Est', N'Distribution Sud Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'83170FC5-1', N'Non IOB', N'BRIGNOLES', N'Direction Orange Sud Est', N'Distribution Sud Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'83000FC5-8', N'Non IOB', N'TOULON MAYOL', N'Direction Orange Sud Est', N'Distribution Sud Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'83580FP3-4', N'Non IOB', N'GASSIN LA FOUX', N'Direction Orange Sud Est', N'Distribution Sud Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'04100FQ7-1', N'Non IOB', N'MANOSQUE', N'Direction Orange Sud Est', N'Distribution Sud Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'13098FE3-3', N'Non IOB', N'AIX ALLEES PROVENCALES', N'Direction Orange Sud Est', N'Distribution Sud Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'13500RPN-8', N'Non IOB', N'MARTIGUES AUCHAN', N'Direction Orange Sud Est', N'Distribution Sud Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'04008FQ7-3', N'Non IOB', N'DIGNE', N'Direction Orange Sud Est', N'Distribution Sud Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'13100JAX-5', N'Non IOB', N'AIX LA PIOLINE', N'Direction Orange Sud Est', N'Distribution Sud Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'131273DR-5', N'IOB', N'VITROLLES', N'Direction Orange Sud Est', N'Distribution Sud Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'05008FT3-2', N'Non IOB', N'GAP', N'Direction Orange Sud Est', N'Distribution Sud Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'83480FP3-8', N'IOB', N'PUGET SUR ARGENS', N'Direction Orange Sud Est', N'Distribution Sud Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'06600FH3-1', N'Non IOB', N'ANTIBES', N'Direction Orange Sud Est', N'Distribution Sud Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'83300FP3-1', N'Non IOB', N'DRAGUIGNAN', N'Direction Orange Sud Est', N'Distribution Sud Est', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'20220FE8-8', N'Non IOB', N'ILE ROUSSE', N'Direction Orange Sud Est', N'Distribution Corse', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'20137FE8-9', N'Non IOB', N'PORTO VECCHIO', N'Direction Orange Sud Est', N'Distribution Corse', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'20178FM2-1', N'IOB', N'AJACCIO', N'Direction Orange Sud Est', N'Distribution Corse', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'20290VXJ-3', N'Non IOB', N'BORGO', N'Direction Orange Sud Est', N'Distribution Corse', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'64100FA6-6', N'Non IOB', N'BAYONNE', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'64300FP9-5', N'Non IOB', N'ORTHEZ', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'40150FQ3-2', N'Non IOB', N'HOSSEGOR', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'19012FP5-2', N'Non IOB', N'TULLE', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'23011FX3-3', N'Non IOB', N'GUERET', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'87100YBQ-5', N'Non IOB', N'LIMOGES CORGNAC', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'40990IRC-5', N'IOB', N'SAINT PAUL LES DAX', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'40100FQ3-5', N'Non IOB', N'DAX', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'33380KRO-5', N'Non IOB', N'BIGANOS', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'33160517-3', N'Non IOB', N'SAINT MEDARD EN JALLES LECLERC', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'40019FQ3-4', N'IOB', N'MONT DE MARSAN CORDELIER', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'33210FL6-A', N'Non IOB', N'LANGON', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'17200FP1-1', N'Non IOB', N'ROYAN', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'17000FY4-2', N'Non IOB', N'LA ROCHELLE SAINT YON', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'79300FS1-1', N'Non IOB', N'BRESSUIRE', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'17100FP1-2', N'Non IOB', N'SAINTES', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'17300FY4-1', N'Non IOB', N'ROCHEFORT', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'86000FN4-5', N'Non IOB', N'POITIERS NOTRE DAME', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'16100FL9-1', N'Non IOB', N'COGNAC', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'16017FL9-3', N'Non IOB', N'ANGOULEME CASINO', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'86100FN4-1', N'Non IOB', N'CHATELLERAULT', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'3330024P-5', N'IOB', N'BORDEAUX AUCHAN LAC', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'330000TI-4', N'Non IOB', N'BORDEAUX INTENDANCE', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'33500FL6-8', N'IOB', N'LIBOURNE', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'24100FK4-1', N'Non IOB', N'BERGERAC', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'24750RXP-5', N'Non IOB', N'TRELISSAC LA FEUILLERAIE', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'47200FS2-7', N'Non IOB', N'MARMANDE', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'24100A4J-5', N'Non IOB', N'BERGERAC LA CAVAILLE', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'24200FK4-2', N'Non IOB', N'SARLAT', N'Direction Orange Sud Ouest', N'Distribution Sud Ouest', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'59190FL1-3', N'Non IOB', N'HAZEBROUCK', N'Direction Orange Nord de France', N'Distribution Nord de France', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'62407FA8-7', N'Non IOB', N'BETHUNE', N'Direction Orange Nord de France', N'Distribution Nord de France', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'02321FL3-5', N'Non IOB', N'SAINT QUENTIN', N'Direction Orange Nord de France', N'Distribution Nord de France', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'0210067T-5', N'Non IOB', N'SAINT QUENTIN FAYET', N'Direction Orange Nord de France', N'Distribution Nord de France', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'02500FL3-3', N'Non IOB', N'HIRSON', N'Direction Orange Nord de France', N'Distribution Nord de France', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'08011FM6-3', N'IOB', N'CHARLEVILLE', N'Direction Orange Nord de France', N'Distribution Nord de France', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'02400FT9-2', N'Non IOB', N'CHATEAU THIERRY', N'Direction Orange Nord de France', N'Distribution Nord de France', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'02000FL3-6', N'IOB', N'LAON', N'Direction Orange Nord de France', N'Distribution Nord de France', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'02208FT9-5', N'Non IOB', N'SOISSONS', N'Direction Orange Nord de France', N'Distribution Nord de France', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'62800FA8-9', N'IOB', N'LIEVIN', N'Direction Orange Nord de France', N'Distribution Nord de France', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'59720FX1-1', N'Non IOB', N'LOUVROIL', N'Direction Orange Nord de France', N'Distribution Nord de France', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'59407FV8-0', N'Non IOB', N'CAMBRAI VICTOIRE', N'Direction Orange Nord de France', N'Distribution Nord de France', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'59508FV8-5', N'IOB', N'DOUAI', N'Direction Orange Nord de France', N'Distribution Nord de France', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'592902DK-8', N'Non IOB', N'WASQUEHAL', N'Direction Orange Nord de France', N'Distribution Nord de France', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'592234AI-7', N'Non IOB', N'RONCQ', N'Direction Orange Nord de France', N'Distribution Nord de France', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'59155LMZ-5', N'Non IOB', N'FACHES THUMESNIL', N'Direction Orange Nord de France', N'Distribution Nord de France', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'52901FS3-0', N'Non IOB', N'CHAUMONT', N'Direction Orange Nord de France', N'Distribution Nord de France', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'51200FL4-1', N'Non IOB', N'EPERNAY', N'Direction Orange Nord de France', N'Distribution Nord de France', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'52100FS3-1', N'Non IOB', N'SAINT DIZIER GAMBETTA', N'Direction Orange Nord de France', N'Distribution Nord de France', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'80044FZ3-6', N'Non IOB', N'AMIENS SUD', N'Direction Orange Nord de France', N'Distribution Nord de France', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'60200FT9-6', N'Non IOB', N'COMPIEGNE', N'Direction Orange Nord de France', N'Distribution Nord de France', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'60280FT9-8', N'Non IOB', N'VENETTE', N'Direction Orange Nord de France', N'Distribution Nord de France', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'62230UA8-2', N'Non IOB', N'COQUELLES', N'Direction Orange Nord de France', N'Distribution Nord de France', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'80100FZ3-1', N'Non IOB', N'ABBEVILLE', N'Direction Orange Nord de France', N'Distribution Nord de France', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'62600FQ9-2', N'Non IOB', N'BERCK', N'Direction Orange Nord de France', N'Distribution Nord de France', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'93100FV7-2', N'Non IOB', N'MONTREUIL GRAND ANGLE', N'Direction Orange IDF', N'Distribution Portes de Paris', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'91150FS8-3', N'Non IOB', N'ETAMPES', N'Direction Orange IDF', N'Distribution Portes de Paris', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'78370FU9-4', N'Non IOB', N'PLAISIR', N'Direction Orange IDF', N'Distribution Portes de Paris', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'91620FW6-4', N'Non IOB', N'LA VILLE DU BOIS', N'Direction Orange IDF', N'Distribution Portes de Paris', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'91230FK9-1', N'Non IOB', N'VALDOLY', N'Direction Orange IDF', N'Distribution Portes de Paris', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'94125FT1-1', N'Non IOB', N'VAL DE FONTENAY', N'Direction Orange IDF', N'Distribution Portes de Paris', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'95520XVB-5', N'Non IOB', N'OSNY', N'Direction Orange IDF', N'Distribution Portes de Paris', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'78360LQC-5', N'Non IOB', N'MONTESSON', N'Direction Orange IDF', N'Distribution Portes de Paris', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'78300FD9-4', N'Non IOB', N'POISSY', N'Direction Orange IDF', N'Distribution Portes de Paris', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'78410FD9-5', N'Non IOB', N'FLINS', N'Direction Orange IDF', N'Distribution Portes de Paris', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'78100FY7-2', N'Non IOB', N'SAINT GERMAIN EN LAYE', N'Direction Orange IDF', N'Distribution Portes de Paris', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'77200FB9-2', N'Non IOB', N'BAY 2', N'Direction Orange IDF', N'Distribution Portes de Paris', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'77160FJ9-2', N'Non IOB', N'PROVINS', N'Direction Orange IDF', N'Distribution Portes de Paris', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'77120FS6-1', N'Non IOB', N'COULOMMIERS', N'Direction Orange IDF', N'Distribution Portes de Paris', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'95880FK8-1', N'Non IOB', N'ENGHIEN LES BAINS', N'Direction Orange IDF', N'Distribution Portes de Paris', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'95150FM4-1', N'Non IOB', N'TAVERNY', N'Direction Orange IDF', N'Distribution Portes de Paris', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'95290PF0-5', N'Non IOB', N'L''ISLE ADAM GRAND VAL', N'Direction Orange IDF', N'Distribution Portes de Paris', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'95570FK8-7', N'Non IOB', N'MOISSELLES', N'Direction Orange IDF', N'Distribution Portes de Paris', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'91290FS8-1', N'IOB', N'BRETIGNY SUR ORGE', N'Direction Orange IDF', N'Distribution Portes de Paris', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'77305FJ9-0', N'Non IOB', N'FONTAINEBLEAU', N'Direction Orange IDF', N'Distribution Portes de Paris', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'77130FJ9-1', N'IOB', N'VARENNES SUR SEINE', N'Direction Orange IDF', N'Distribution Portes de Paris', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'77340FW7-1', N'Non IOB', N'PONTAULT COMBAULT', N'Direction Orange IDF', N'Distribution Portes de Paris', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'77190FW7-4', N'Non IOB', N'VILLIERS EN BIERE', N'Direction Orange IDF', N'Distribution Portes de Paris', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'75013FB7-0', N'Non IOB', N'PARIS TOLBIAC', N'Direction Orange IDF', N'Distribution Ile de France Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'75011FW3-2', N'Non IOB', N'PARIS NATION', N'Direction Orange IDF', N'Distribution Ile de France Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'75020FW3-3', N'Non IOB', N'PARIS GAMBETTA', N'Direction Orange IDF', N'Distribution Ile de France Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'75016FX6-4', N'Non IOB', N'PARIS PASSY', N'Direction Orange IDF', N'Distribution Ile de France Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'75005U77-4', N'Non IOB', N'PARIS SAINT MICHEL', N'Direction Orange IDF', N'Distribution Ile de France Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'75SDRCOMME', N'Non IOB', N'PARIS COMMERCE', N'Direction Orange IDF', N'Distribution Ile de France Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'75006C3L-J', N'Non IOB', N'PARIS ODEON', N'Direction Orange IDF', N'Distribution Ile de France Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'75018FR7-3', N'Non IOB', N'PARIS DUHESME', N'Direction Orange IDF', N'Distribution Ile de France Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'92500FS7-4', N'Non IOB', N'RUEIL', N'Direction Orange IDF', N'Distribution Ile de France Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'92200FR8-2', N'Non IOB', N'NEUILLY MICHELIS', N'Direction Orange IDF', N'Distribution Ile de France Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'75017FN6-2', N'Non IOB', N'PARIS TERNES', N'Direction Orange IDF', N'Distribution Ile de France Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'75116FX6-3', N'Non IOB', N'PARIS VICTOR HUGO', N'Direction Orange IDF', N'Distribution Ile de France Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'94110ARH-6', N'Non IOB', N'SHOWROOM SALARIES', N'Direction Orange IDF', N'Distribution Ile de France Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'75001FN3-7', N'Non IOB', N'PARIS MONTPARNASSE', N'Direction Orange IDF', N'Distribution Ile de France Centre', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[REF_BOUTIQUE] ( [Code_ADV_Orange], [Type_Boutique], [Site_Libelle], [DO], [AD], [Site_CodePostal], [Site_Ville], [Site_Adresse1], [TypeDeSite_Libelle]) VALUES ( N'ND', NULL, NULL, N'ND : Boutique inexistante dans REF_BOUTIQUE', N'ND : Boutique inexistante dans REF_BOUTIQUE', NULL, NULL, NULL, NULL)
GO