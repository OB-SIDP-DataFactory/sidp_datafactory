﻿USE [$(DataHubDatabaseName)] 
GO
TRUNCATE TABLE [dbo].[PARAM_DATE_VALIDITE]
GO
INSERT INTO [dbo].[PARAM_DATE_VALIDITE]
SELECT MAX([DWHCPTDTX]) AS DAT_VALD FROM PV_SA_Q_COMPTE
GO
TRUNCATE TABLE [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].[dbo].[DWH_PARAM_DATE_VALIDITE]
GO
INSERT INTO [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].[dbo].[DWH_PARAM_DATE_VALIDITE]
SELECT DAT_VALD FROM  [$(DataHubDatabaseName)].[dbo].[PARAM_DATE_VALIDITE]
GO
