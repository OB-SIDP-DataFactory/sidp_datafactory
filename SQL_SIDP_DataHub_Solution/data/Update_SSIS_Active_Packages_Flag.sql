﻿USE [$(DataHubDatabaseName)] 
GO
IF NOT EXISTS ( SELECT 1 FROM [dbo].[Param_SSISPackages_Actif] WHERE [batch_name]='PKGCO_SSIS_FE_IBAN_ON_BENEFICIARY_V2' )
BEGIN
    INSERT INTO [dbo].[Param_SSISPackages_Actif] (batch_name, flag_actif) VALUES ('PKGCO_SSIS_FE_IBAN_ON_BENEFICIARY_V2', 1)
END
GO
IF NOT EXISTS ( SELECT 1 FROM [dbo].[Param_SSISPackages_Actif] WHERE [batch_name]='PKGCO_SSIS_FE_TRANSFER_BENEFICIARY_V2' )
BEGIN
    INSERT INTO [dbo].[Param_SSISPackages_Actif] (batch_name, flag_actif) VALUES ('PKGCO_SSIS_FE_TRANSFER_BENEFICIARY_V2', 1)
END
GO