﻿USE [$(DataHubDatabaseName)] 
GO
IF NOT EXISTS ( SELECT 1 FROM dbo.[$(FileTableName)] WHERE [name]='$(FolderName)' )
BEGIN
    INSERT INTO dbo.[$(FileTableName)] (name, is_directory, is_archive)
    VALUES ('$(FolderName)', 1, 0)
END
GO