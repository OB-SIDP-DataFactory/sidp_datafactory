﻿USE [$(DataHubDatabaseName)]
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WK_SF_OPPORTUNITY_HISTORY]') AND type in (N'U'))
BEGIN
    TRUNCATE TABLE [dbo].[WK_SF_OPPORTUNITY_HISTORY]
END
GO
