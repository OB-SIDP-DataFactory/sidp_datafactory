USE [$(DataHubDatabaseName)] 
GO
TRUNCATE TABLE [dbo].[REF_CASE_PRIORITE]
GO 

INSERT INTO [dbo].[REF_CASE_PRIORITE] ([CODE_SF], [LIBELLE]) VALUES (N'01', N'Normal')
,(N'02', N'Urgent')
,(N'03', N'Très urgent')
GO