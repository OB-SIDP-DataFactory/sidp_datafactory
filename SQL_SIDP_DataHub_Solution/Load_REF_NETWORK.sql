USE [$(DataHubDatabaseName)]
GO
TRUNCATE TABLE [dbo].[REF_NETWORK]
GO
INSERT INTO [dbo].[REF_NETWORK] ([CODE_SF], [LIBELLE], [Validity_StartDate]) 
VALUES ('01', 'Orange France', '2017-01-01')
     , ('02', 'Orange Bank', '2017-01-01')
     , ('03', 'Groupama', '2017-01-01')
GO
