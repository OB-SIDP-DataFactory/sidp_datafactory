﻿CREATE VIEW exposition.PV_MC_CGLOG AS
SELECT AUDIENCE_NAME AS NOM_CIBLE
     , ID_CAMPAIGN_MESSAGE AS IDE_CAMP_MSG_MC
     , MESSAGE_NAME AS NOM_MSG
     , ID_CAMPAIGN AS IDE_CAMP_MC
     , CAMPAIGN_NAME AS NOM_CAMPAGNE
     , ID_CONTACT AS IDE_CONTACT_SF
     , DATE_TIME_SEND AS DTE_ENVOI
  FROM dbo.VW_PV_MC_CGLOG