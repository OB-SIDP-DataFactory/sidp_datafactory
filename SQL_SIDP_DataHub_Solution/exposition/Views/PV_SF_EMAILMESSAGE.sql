﻿CREATE VIEW exposition.PV_SF_EMAILMESSAGE AS
SELECT Id AS ID
     , Id_SF AS IDE_EMAIL_SF
     , RelatedToId AS IDE_PERS_SF
     , ParentId AS IDE_REQ_SF
     , Subject AS OBJET
     , CreatedById AS IDE_USER_CREA_SF
     , Incoming AS FLG_ENT
     , MessageDate AS DTE_MES
     , ToAddress AS ADR_DES
     , CcAddress AS ADR_CC
     , BccAddress AS ADR_CCI
     , TextBody AS COP_TXT
     , HtmlBody AS COR_TXT_HTML
     , CreatedDate AS DTE_CREA
     , LastModifiedDate AS DTE_DER_MOD
     , FromAddress AS ADR_EXP
     , FromName AS EXPEDITEUR
     , Validity_StartDate AS DTE_DEBUT_VAL
     , Validity_EndDate AS DTE_FIN_VAL
  FROM dbo.PV_SF_EMAILMESSAGE