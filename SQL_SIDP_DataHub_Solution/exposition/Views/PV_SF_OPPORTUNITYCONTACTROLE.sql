﻿
CREATE VIEW [exposition].PV_SF_OPPORTUNITYCONTACTROLE AS
SELECT Id AS ID
	 , Id_SF AS IDE_OPPRT_CONTACT_ROLE_SF
     , OpportunityId AS IDE_OPPRT_SF
     , ContactId AS IDE_CONTACT
     , Role AS ROLE
     , IsPrimary AS FLG_PPAL
     , CreatedDate AS DTE_CREA
     , CreatedById AS IDE_USER_CREA_SF
     , LastModifiedDate AS DTE_DER_MOD
     , LastModifiedById AS IDE_USER_MOD_SF
     , SystemModstamp AS SystemModstamp
     , IsDeleted AS FLG_SUPP
     , Validity_StartDate AS DTE_DEBUT_VAL
     , Validity_EndDate AS DTE_FIN_VAL
     , Startdt AS DTE_DEBUT_TRN
     , Enddt AS DTE_FIN_TRN
  FROM dbo.VW_PV_SF_OPPORTUNITYCONTACTROLE