﻿CREATE VIEW exposition.PV_MC_SENDJOBS AS
SELECT ClientID AS IDE_CLT_MC
     , SendID AS IDE_SEND_MC
     , FromName AS NOM_EXPEDITEUR
     , FromEmail AS EML_EXPEDITEUR
     , SchedTime AS DTE_PROG
     , SentTime AS DTE_ENV
     , Subject AS OBJET
     , EmailName AS LIBELLE
     , TriggeredSendExternalKey AS IDE_TRIG_ENV_MAIL_MC
     , SendDefinitionExternalKey AS IDE_DEF_ENV_MAIL_MC
     , JobStatus AS STATUT_ENV
     , PreviewURL AS URL
     , IsMultipart AS ENV_MIME
     , Additional AS ADDITIONAL
  FROM dbo.VW_PV_MC_SENDJOBS