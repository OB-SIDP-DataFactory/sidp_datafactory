﻿
CREATE VIEW exposition.PV_FE_CUSTOMERSUBPRODUCTASSOC AS
SELECT Id AS ID
	 , ID_FE_CUSTOMERSUBPRODUCTASSOC AS IDE_CLT_PROD_SOUS_ASSOC
     , CUSTOMER_ID AS NUM_CLT_FE
     , SUBSCRIBED_PRODUCT_ID AS IDE_PROD_SOUS
     , SALESFORCE_PERSON_ID AS IDE_PERS_SF
     , CUSTOMER_ROLE_ID AS IDE_ROL_CLT
     , CREATION_DATE AS DTE_CREA
     , CREATION_USER AS USR_CREA
     , LAST_UPDATE_DATE AS DTE_DER_MOD
     , LAST_UPDATE_USER AS USR_DER_MOD
     , USER_TYPE AS TYP_USR
	 , Validity_StartDate AS DTE_DEBUT_VAL 
	 , Validity_EndDate AS DTE_FIN_VAL
  FROM dbo.PV_FE_CUSTOMERSUBPRODUCTASSOC