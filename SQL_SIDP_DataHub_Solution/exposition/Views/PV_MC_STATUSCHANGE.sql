﻿CREATE VIEW exposition.PV_MC_STATUSCHANGE AS
SELECT ClientID AS IDE_CLT_MC
     , SubscriberKey AS IDE_CONTACT_SF
     , EmailAddress AS ADR_EMAIL
     , SubscriberID AS IDE_SUBSC_MC
     , OldStatus AS ANC_STATUT
     , NewStatus AS NOUV_STATUT
     , DateChanged AS DTE_CHG_STAT
  FROM dbo.VW_PV_MC_STATUSCHANGE