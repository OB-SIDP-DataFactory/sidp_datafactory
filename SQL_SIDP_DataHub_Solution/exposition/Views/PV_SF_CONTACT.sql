﻿CREATE VIEW exposition.PV_SF_CONTACT AS
SELECT sf_contact.Id AS ID
     , sf_contact.Id_SF AS IDE_CONTACT_SF
     , sf_contact.Name AS NOM_CONTACT
     , sf_contact. Phone AS TEL_CONTACT
     , sf_contact.Email AS MAIL_CONTACT
     , sf_contact.SellerHashedCuid__c AS COD_VENDEUR
     , sf_contact.CreatedDate AS DTE_CREA
     , sf_contact.LastModifiedDate AS DTE_DER_MOD
	 , sf_contact.Validity_StartDate AS DTE_DEBUT_VAL
     , sf_contact.Validity_EndDate AS DTE_FIN_VAL
     , sf_contact.Startdt AS DTE_DEBUT_TRN
     , sf_contact.Enddt AS DTE_FIN_TRN
  FROM dbo.VW_PV_SF_CONTACT sf_contact