﻿CREATE VIEW exposition.PV_MC_COMPLAINTS AS
SELECT ClientID AS IDE_CLT_MC
     , SendID AS IDE_SEND_MC
     , SubscriberKey AS IDE_CONTACT_SF
     , EmailAddress AS ADR_EMAIL
     , SubscriberID AS IDE_SUBSC_MC
     , ListID AS IDE_LST_MC
     , EventDate AS DTE_EVENT
     , EventType AS TYPE_EVENT
     , BatchID AS IDE_BATCH_MC
     , TriggeredSendExternalKey AS IDE_TRIG_ENV_MAIL_MC
     , Domain AS DOMAINE
  FROM dbo.VW_PV_MC_COMPLAINTS