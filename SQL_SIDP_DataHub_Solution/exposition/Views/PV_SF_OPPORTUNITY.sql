﻿

CREATE VIEW [exposition].[PV_SF_OPPORTUNITY] AS
SELECT Id_SF AS IDE_OPPRT_SF
     ,sf_opportunity. AccountId AS IDE_PERS_SF
     ,sf_opportunity. AdvisorCode__c AS COD_VENDEUR
     ,sf_opportunity. CampaignId__c AS IDE_CAMP_MC
     ,sf_opportunity. CloseDate AS DTE_CLO_OPPRT
     ,sf_opportunity. CommercialOfferCode__c AS COD_OFF_COM
     ,sf_opportunity. CommercialOfferName__c AS LABEL_OFF_COM
     ,sf_opportunity. CompleteFileFlag__c AS FLG_DOS_COMP
	 ,sf_opportunity.CompleteFileFlag__c_label AS LIB_DOS_COMP
     ,sf_opportunity. CreatedById AS IDE_USER_CREA_SF
     ,sf_opportunity. CreatedDate AS DTE_CREA
     ,sf_opportunity. DeniedOpportunityReason__c AS MOT_AFF_REF
	 ,sf_opportunity.DeniedOpportunityReason__c_label AS LIB_MOT_AFF_REF
     ,sf_opportunity. DistributionChannel__c AS COD_CAN_DIS_ORI
	 ,sf_opportunity.DistributionChannel__c_label AS LIB_CAN_DIS_ORI
     ,sf_opportunity. DistributorEntity__c AS ENT_DIS
	 ,sf_opportunity.DistributorEntity__c_label AS LIB_ENT_DIS
     ,sf_opportunity. DistributorNetwork__c AS COD_RES_DIS
	 ,sf_opportunity.DistributorNetwork__c_label AS LIB_RES_DIS
     ,sf_opportunity. FinalizedBy__c AS REAL_OPPRT
     ,sf_opportunity. FormValidation__c AS DTE_VAL_FORM
     ,sf_opportunity. IDOppty__c AS NUM_OPPRT
     ,sf_opportunity. IDProcessSous__c AS NUM_PROC_SOUS
     ,sf_opportunity. IdSource__c AS IDE_SRCE
     ,sf_opportunity. Indication__c AS FLG_IND
     ,sf_opportunity. LastModifiedDate AS DTE_DER_MOD
     ,sf_opportunity. LeadSource AS COD_CAN_INT
	 ,sf_opportunity.LeadSource_label AS LIB_CAN_INT
     ,sf_opportunity. Manager__c AS RESP_ENT
     ,sf_opportunity. NoIndication__c AS NUM_PROC_IND
     ,sf_opportunity. OwnerId AS IDE_USER_SF
     ,sf_opportunity. PointOfSaleCode__c AS COD_BOUT
     ,sf_opportunity. ProductFamilyCode__c AS COD_FAM_PROD
     ,sf_opportunity. ProductFamilyName__c AS NOM_FAM_PROD
     ,sf_opportunity. ProvenanceIndicationIndicatorId__c AS FLG_IND_DIG
     ,sf_opportunity. RecordTypeId AS IDE_TYP_ENR
	 ,sf_opportunity.RecordTypeCode AS COD_TYP_ENR
     ,sf_opportunity.RecordTypeName AS LIB_TYP_ENR
     ,sf_opportunity. RejectReason__c AS MOT_SANS_SUITE
	 ,sf_opportunity.RejectReason__c_label AS LIB_MOT_SANS_SUITE
     ,sf_opportunity. RelationEntryScore__c AS SCO_ENT_REL
	 ,sf_opportunity.RelationEntryScore__c_label AS LIB_SCO_ENT_REL
     ,sf_opportunity. StageName AS STADE_VENTE
	 ,sf_opportunity.StageName_label AS LIB_STADE_VENTE
     ,sf_opportunity. StartedChannel__c AS COD_CAN_INT_ORI
	 ,sf_opportunity.StartedChannel__c_label AS LIB_CAN_INT_ORI
     ,sf_opportunity. SystemModstamp AS SystemModstamp
     ,sf_opportunity. Type AS TYP_OPPRT
     ,sf_opportunity. IsWon AS FLG_GAGN
     ,sf_opportunity. Name AS NOM_OPPRT
     ,sf_opportunity. realizedBy__c AS REAL_PAR
     ,sf_opportunity. SignatureDate__c AS DTE_SIG_OPPRT
     ,sf_opportunity. StartedChannelForIndication__c AS CAN_INT_ORI_IND
     ,sf_opportunity. ToBeDeleted__c AS FLG_A_SUPP
     ,sf_opportunity. Amount AS MTT_OPPRT
     ,sf_opportunity. Fiscal AS TYP_EXC_FIS
     ,sf_opportunity. FiscalQuarter AS TRIM_FISC
     ,sf_opportunity. FiscalYear AS AA_FISC
     ,sf_opportunity. FolderAlias__c AS ALIAS_DOS_SOUS
     ,sf_opportunity. ForecastCategory AS COD_CAT_PREV
     ,sf_opportunity. ForecastCategoryName AS NOM_CAT_PREV
     ,sf_opportunity. HasOpenActivity AS FLG_ACT_ENC
     ,sf_opportunity. HasOpportunityLineItem AS FLG_LIG_CMD_ATT
     ,sf_opportunity. HasOverdueTask AS FLG_TACHE_RET
     ,sf_opportunity. IsClosed AS FLG_FERM
     ,sf_opportunity. IsDeleted AS FLG_SUPP
     ,sf_opportunity. LastActivityDate AS DTE_DER_ACT
     ,sf_opportunity. LastModifiedById AS IDE_USER_MOD_SF
     ,sf_opportunity. LastReferencedDate AS DER_DTE_REF
     ,sf_opportunity. LastViewedDate AS DTE_DER_AFF
     ,sf_opportunity. NextStep AS ETA_SUIV
     ,sf_opportunity. OriginalEvent__c AS EVE_ORI
     ,sf_opportunity. SendingInformations__c AS DTE_ENV_INF
     ,sf_opportunity. TCHExpiredIndication__c AS NBE_IND_EXP
     ,sf_opportunity. TCHExpiredSubscription__c AS NBE_SOUSC_EXP
     ,sf_opportunity. TCHManualAnalysis__c AS NBE_ANA_MAN
     ,sf_opportunity. TCHOppCreation__c AS NBE_CRE_OPPRT
     ,sf_opportunity. IsPrivate AS FLG_PRIVE
     ,sf_opportunity. Probability AS PROBABILITE
     ,sf_opportunity. ExpectedRevenue AS REV_ATT
     ,sf_opportunity. TotalOpportunityQuantity AS TOT_QTE
     ,sf_opportunity. CampaignId AS IDE_CAMP_MC_xx
     ,sf_opportunity. Pricebook2Id AS IDE_CAT_PRIX
     ,sf_opportunity. ContractId AS IDE_CONTRAT
     ,sf_opportunity. TCHCompleteFile__c AS COMPLETE_FILE_TCH
     ,sf_opportunity. TCHFromMarketingCase__c AS FROM_MKT_CASE_TCH
     ,sf_opportunity. TCHNotifDuplicateProspect__c AS NOTIF_DUP_PROSPECT_TCH
     ,sf_opportunity. RecoveryLink__c AS LIEN_REPRISE
     ,sf_opportunity. TCHId__c AS IDE_TCH
     ,sf_opportunity. ContractNumberDistrib__c AS NUM_CONT_DIST
     ,sf_opportunity. OptInTelcoData__c AS FLG_OPT_IN_DON_TEL
     ,sf_opportunity. SubscriberHolder__c AS SOUSC_TIT
     ,sf_opportunity. CreditAmount__c AS MTT_PRET
     ,sf_opportunity. CustomerAdviserId__c AS IDE_CONS
     ,sf_opportunity. DebitDay__c AS JR_DEBIT
     ,sf_opportunity. DepositAmount__c AS MTT_VER
     ,sf_opportunity. Derogation__c AS DEROGATION
     ,sf_opportunity. FFinalDecision__c AS DECISION_FIN
     ,sf_opportunity. FPreScoreColor__c AS COUL_PRE_SCO
     ,sf_opportunity. FirstDueDate__c AS PREM_ECH
     ,sf_opportunity. ForbiddenWords__c AS MOTS_INTERDITS
     ,sf_opportunity. FundingSubject__c AS OBJ_FINAN
     ,sf_opportunity. HasForbiddenWord__c AS CONT_MOTS_INTERDITS
     ,sf_opportunity. IncompleteFileReason__c AS MOT_DOS_INCOMP
     ,sf_opportunity. LoanAcceptationDate__c AS DTE_ACCORD
     ,sf_opportunity. ObjectInApproval__c AS ENT_APPROB
     ,sf_opportunity. OffersRate__c AS TX_OFFRE
     ,sf_opportunity. PrescriberContact__c AS PRESCRIPTEUR
     ,sf_opportunity. ReleaseAmount__c AS MTT_DEBLOQUE
     ,sf_opportunity. ReleaseDate__c AS DTE_DEBLOCAGE
     ,sf_opportunity. StartDate__c AS DTE_DEB
     ,sf_opportunity. SubscribedAmount__c AS MT_SOUSC
     ,sf_opportunity. TCHCategory__c AS CAT_TCH
     ,sf_opportunity. TracingProduct__c AS PROD
     ,sf_opportunity. RetractationDate__c AS DTE_RETRACTATION
	 ,sf_opportunity.Validity_StartDate AS DTE_DEBUT_VAL
     ,sf_opportunity.Validity_EndDate AS DTE_FIN_VAL
  FROM dbo.VW_PV_SF_OPPORTUNITY sf_opportunity