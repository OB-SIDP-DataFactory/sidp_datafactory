﻿CREATE VIEW exposition.PV_MC_OPENS AS
SELECT ClientID AS IDE_CLT_MC
     , SendID AS IDE_SEND_MC
     , SubscriberKey AS IDE_CONTACT_SF
     , EmailAddress AS ADR_EMAIL
     , SubscriberID AS IDE_SUBSC_MC
     , ListID AS IDE_LST_MC
     , EventDate AS DTE_EVENT
     , EventType AS TYPE_EVENT
     , BatchID AS IDE_BATCH_MC
     , TriggeredSendExternalKey AS IDE_TRIG_ENV_MAIL_MC
     , IsUnique AS FLG_UNIQUE
     , Browser AS NAVIGATEUR
     , EmailClient AS ADR_EMAIL_CLT
     , OperatingSystem AS OS
     , Device AS DEVICE
  FROM dbo.VW_PV_MC_OPENS