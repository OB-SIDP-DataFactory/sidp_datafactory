﻿CREATE VIEW exposition.PV_SF_PROFILE AS
SELECT Id AS ID
     , Id_SF AS IDE_PROFIL_SF
     , Name AS NOM_PROFIL
     , CreatedDate AS DTE_CREA
     , LastModifiedDate AS DTE_DER_MOD
     , Validity_StartDate AS DTE_DEBUT_VAL
     , Validity_EndDate AS DTE_FIN_VAL
  FROM dbo.PV_SF_PROFILE