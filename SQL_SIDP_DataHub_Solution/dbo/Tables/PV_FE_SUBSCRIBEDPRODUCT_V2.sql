﻿CREATE TABLE [dbo].[PV_FE_SUBSCRIBEDPRODUCT_V2] (
    [Id]                      BIGINT         IDENTITY (1, 1) NOT NULL,
    [ID_FE_SUBSCRIBEDPRODUCT] DECIMAL (19)   NOT NULL,
    [CREATION_DATE]           DATETIME2 (6)  NOT NULL,
    [LAST_UPDATE_DATE]        DATETIME2 (6)  NOT NULL,
    [ENROLMENT_FOLDER_ID]     DECIMAL (19)   NOT NULL,
    [COMMERCIAL_PRODUCT_ID]   DECIMAL (19)   NOT NULL,
    [DISCRIMINATOR]           NVARCHAR (31)  NOT NULL,
    [CREATION_USER]           NVARCHAR (255) NULL,
    [USER_TYPE]               NVARCHAR (255) NULL,
    [LAST_UPDATE_USER]        NVARCHAR (255) NULL,
    [BANK_SECRECY_OPTIN]      DECIMAL (1)    NULL,
    [Validity_StartDate]      DATETIME2 (7)  NOT NULL,
    [Validity_EndDate]        DATETIME2 (7)  DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_FE_SUBSCRIBEDPRODUCT_V2] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UC_ID_FE_SUBSCRIBEDPRODUCT] UNIQUE NONCLUSTERED ([ID_FE_SUBSCRIBEDPRODUCT] ASC, [Validity_StartDate] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_NC_FE_SUB_PRD_V2_ID]
ON [dbo].[PV_FE_SUBSCRIBEDPRODUCT_V2] ([ID_FE_SUBSCRIBEDPRODUCT])
INCLUDE ([Validity_EndDate])
GO