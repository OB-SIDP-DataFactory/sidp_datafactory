﻿CREATE TABLE [dbo].[IWD_TMP_GCX_CAMPLIST_INFO] (
    [ID]               NUMERIC (16)  NOT NULL,
    [LISTID]           INT           NOT NULL,
    [CAMPAIGNID]       INT           NOT NULL,
    [STATUS]           INT           NOT NULL,
    [CREATED]          DATETIME      NULL,
    [DELETED]          DATETIME      NULL,
    [CREATED_TS]       INT           NULL,
    [DELETED_TS]       INT           NULL,
    [GSYS_DOMAIN]      INT           NULL,
    [GSYS_SYS_ID]      INT           NULL,
    [GSYS_EXT_VCH1]    VARCHAR (255) NULL,
    [GSYS_EXT_VCH2]    VARCHAR (255) NULL,
    [GSYS_EXT_INT1]    INT           NULL,
    [GSYS_EXT_INT2]    INT           NULL,
    [CREATE_AUDIT_KEY] NUMERIC (19)  NOT NULL,
    [UPDATE_AUDIT_KEY] NUMERIC (19)  NOT NULL
);

