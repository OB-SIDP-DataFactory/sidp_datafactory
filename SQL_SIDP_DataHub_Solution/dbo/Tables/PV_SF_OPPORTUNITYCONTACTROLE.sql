﻿CREATE TABLE [dbo].[PV_SF_OPPORTUNITYCONTACTROLE] (
    [Id]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [Id_SF]              NVARCHAR (50)                               NOT NULL,
    [OpportunityId]      NVARCHAR (18)                               NULL,
    [ContactId]          NVARCHAR (18)                               NULL,
    [Role]               NVARCHAR (40)                               NULL,
    [IsPrimary]          NVARCHAR (10)                               NULL,
    [CreatedDate]        DATETIME                                    NULL,
    [CreatedById]        NVARCHAR (18)                               NULL,
    [LastModifiedDate]   DATETIME                                    NULL,
    [LastModifiedById]   NVARCHAR (18)                               NULL,
    [SystemModstamp]     DATETIME                                    NULL,
    [IsDeleted]          NVARCHAR (10)                               NULL,
    [Validity_StartDate] DATETIME2 (7)                               NOT NULL,
    [Validity_EndDate]   DATETIME2 (7)                               DEFAULT ('99991231') NOT NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SF_OPPORTUNITYCONTACTROLE] PRIMARY KEY CLUSTERED ([Id] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SF_OPPORTUNITYCONTACTROLEHistory], DATA_CONSISTENCY_CHECK=ON));
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Supprimé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITYCONTACTROLE', @level2type = N'COLUMN', @level2name = N'IsDeleted';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Horodateur des modifications du système', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITYCONTACTROLE', @level2type = N'COLUMN', @level2name = N'SystemModstamp';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernière modification par ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITYCONTACTROLE', @level2type = N'COLUMN', @level2name = N'LastModifiedById';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITYCONTACTROLE', @level2type = N'COLUMN', @level2name = N'LastModifiedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID créé par', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITYCONTACTROLE', @level2type = N'COLUMN', @level2name = N'CreatedById';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITYCONTACTROLE', @level2type = N'COLUMN', @level2name = N'CreatedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Principal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITYCONTACTROLE', @level2type = N'COLUMN', @level2name = N'IsPrimary';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Rôle', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITYCONTACTROLE', @level2type = N'COLUMN', @level2name = N'Role';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID du contact', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITYCONTACTROLE', @level2type = N'COLUMN', @level2name = N'ContactId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de l''opportunité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITYCONTACTROLE', @level2type = N'COLUMN', @level2name = N'OpportunityId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID du rôle des contacts', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITYCONTACTROLE', @level2type = N'COLUMN', @level2name = N'Id_SF';

