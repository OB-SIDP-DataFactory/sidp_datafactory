﻿CREATE TABLE [dbo].[IWD_TMP_G_ROUTE_RESULT_V] (
    [ID]                    NUMERIC (16)  NOT NULL,
    [CALLID]                VARCHAR (50)  NOT NULL,
    [CONNID]                VARCHAR (50)  NOT NULL,
    [PARTYID]               VARCHAR (50)  NOT NULL,
    [IRID]                  VARCHAR (50)  NULL,
    [RTARGETOBJECTSELECTED] VARCHAR (255) NULL,
    [RTARGETTYPESELECTED]   INT           NULL,
    [RREQUESTEDSKILLCOMB]   VARCHAR (255) NULL,
    [RSTRATEGYNAME]         VARCHAR (255) NULL,
    [RTENANT]               VARCHAR (255) NULL,
    [RESULT]                INT           NOT NULL,
    [CREATED]               DATETIME      NOT NULL,
    [GSYS_EXT_VCH1]         VARCHAR (255) NULL
);

