﻿CREATE TABLE [dbo].[REF_COUNTRY] (
    [ID]                             INT            IDENTITY (1, 1) NOT NULL,
    [ID_COUNTRY]                     DECIMAL (19)   NULL,
    [CORE_BANKING_CODE]              NVARCHAR (255) NULL,
    [ISO_NUMERIC_CODE]               NVARCHAR (255) NULL,
    [ISO_THREE_CHAR_CODE]            NVARCHAR (255) NULL,
    [ISO_TWO_CHARACTERS_CODE]        NVARCHAR (255) NULL,
    [INSEE_CODE]                     NVARCHAR (255) NULL,
    [FRANFINANCE_CODE]               NVARCHAR (255) NULL,
    [FRANFINANCE_NATIONALITY_CODE]   NVARCHAR (255) NULL,
    [NATIONALITY]                    NVARCHAR (255) NULL,
    [TERRITORY_POSTAL_CODE]          NVARCHAR (3)   NULL,
    [IS_NATIONALITY_OR_BIRTHCOUNTRY] DECIMAL (1)    NULL,
    [BBAN_LENGTH]                    DECIMAL (38)   NULL,
    [SEPA]                           DECIMAL (1)    NULL,
	[EUROPE_ECONOMIC_AREA]           DECIMAL (1)    NULL,
	[IS_DROM]                        DECIMAL (1)    NULL,
    CONSTRAINT [PK_REF_COUNTRY] PRIMARY KEY CLUSTERED ([ID] ASC)
);

