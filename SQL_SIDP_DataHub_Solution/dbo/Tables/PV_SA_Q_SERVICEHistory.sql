﻿CREATE TABLE [dbo].[PV_SA_Q_SERVICEHistory] (
    [ID]                 BIGINT        NOT NULL,
    [DWHABSDTX]          DATE          NULL,
    [DWHABSETA]          INT           NULL,
    [DWHABSAGE]          INT           NULL,
    [DWHABSSER]          VARCHAR (2)   NULL,
    [DWHABSSSE]          VARCHAR (2)   NULL,
    [DWHABSNUM]          INT           NULL,
    [DWHABSCSE]          VARCHAR (6)   NULL,
    [DWHABSADH]          DATE          NULL,
    [DWHABSFIN]          DATE          NULL,
    [DWHABSREN]          DATE          NULL,
    [DWHABSCET]          VARCHAR (1)   NULL,
    [DWHABSRES]          DATE          NULL,
    [DWHABSMOR]          VARCHAR (6)   NULL,
    [DWHABSCRE]          DATE          NULL,
    [DWHABSVAL]          DATE          NULL,
    [Validity_StartDate] DATETIME      NULL,
    [Validity_EndDate]   DATETIME      NULL,
    [Startdt]            DATETIME2 (7) NOT NULL,
    [Enddt]              DATETIME2 (7) NOT NULL,
    [NUMR_LIGN_FICH]     INT           NULL
)
GO