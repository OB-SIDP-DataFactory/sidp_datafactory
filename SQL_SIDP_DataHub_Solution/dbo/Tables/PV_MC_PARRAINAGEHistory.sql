﻿CREATE TABLE [dbo].[PV_MC_PARRAINAGEHistory] (
    [Id]                         BIGINT         NOT NULL,
    [ID_CONTACT_PARRAIN]         NVARCHAR (18)  NOT NULL,
    [ID_CONTACT_ACCOUNT_PARRAIN] NVARCHAR (18)  NULL,
    [NOM_FILLEUL]                NVARCHAR (80)  NULL,
    [PRENOM_FILLEUL]             NVARCHAR (40)  NULL,
    [EMAIL_FILLEUL]              NVARCHAR (254) NOT NULL,
    [DATE_PARRAINAGE]            DATETIME       NULL,
    [RECONCILIATION]             NVARCHAR (10)  NULL,
    [DATE_RECONCILIATION]        DATETIME       NULL,
    [OPTIN_FILLEUL]              NVARCHAR (5)   NULL,
    [OPTIN_PARRAINAGE]           NVARCHAR (5)   NULL,
    [NB_RELANCES]                INT            NULL,
    [DATE_DERNIERE_RELANCE]      DATETIME       NULL,
    [Validity_StartDate]         DATETIME       NULL,
    [Validity_EndDate]           DATETIME       NULL,
    [Startdt]                    DATETIME2 (7)  NOT NULL,
    [Enddt]                      DATETIME2 (7)  NOT NULL
);
GO
CREATE CLUSTERED INDEX [ix_PV_MC_PARRAINAGEHistory]
    ON [dbo].[PV_MC_PARRAINAGEHistory]([Enddt] ASC, [Startdt] ASC) WITH (DATA_COMPRESSION = PAGE);

