﻿CREATE TABLE [dbo].[PV_FE_SUBSCRIBEDCONSUMERLOAN_V2] (
    [Id]                             INT             IDENTITY (1, 1) NOT NULL,
    [ID_FE_SUBSCRIBEDCONSUMERLOAN]   DECIMAL (38)    NULL,
    [ACCOUNT_NUMBER]                 NVARCHAR (11)   NULL,
    [FRANFINANCE_APPLICATION_NUMBER] NVARCHAR (7)    NULL,
    [FRANFINANCE_CONTRACT_NUMBER]    NVARCHAR (11)   NULL,
    [SEPA_MANDATE_NUMBER]            NVARCHAR (255)  NULL,
    [RETRACTION_PERIOD_END_DATE]     DATE            NULL,
    [LOAN_DISBURSMENT_END_DATE]      DATE            NULL,
    [LOAN_DISBURSMENT_START_DATE]    DATE            NULL,
    [RETRACTION_RIGHT_RENOUNCED]     DECIMAL (1)     NULL,
    [COMPLIANCE_COMMIT_ACCEPT_DATE]  DATE            NULL,
    [APPROVAL_DATE]                  DATE            NULL,
    [LOAN_SCORE_COLOR_ID]            DECIMAL (38)    NULL,
    [LOAN_SCORE_VALUE]               DECIMAL (38)    NULL,
    [DEBT_RATE]                      DECIMAL (19, 7) NULL,
    [DEBT_RATE_COLOR_ID]             DECIMAL (38)    NULL,
    [DIRECT_DEBIT_ACCOUNT_TYPE_ID]   DECIMAL (38)    NULL,
    [FRANFINANCE_SCORE_COLOR_ID]     DECIMAL (38)    NULL,
    [BORR_INSUR_COVERAGE_REFUSAL]    DECIMAL (1)     NULL,
    [CO_BORR_INSUR_COVERAGE_REFUSAL] DECIMAL (1)     NULL,
    [DIRECT_DEBIT_CONTROL_KEY]       NVARCHAR (2)    NULL,
    [DIRECT_DEBIT_BBAN]              NVARCHAR (30)   NULL,
    [BORRO_OUTSTANDING_AMOUNT]       DECIMAL (19, 2) NULL,
    [DIRECT_DEBIT_COUNTRY_ID]        DECIMAL (38)    NULL,
    [D_N_SEPA_MANDATE_NUMBER]        NVARCHAR (255)  NULL,
    [CREATION_DATE]                  DATETIME2 (6)   NULL,
    [LAST_UPDATE_DATE]               DATETIME2 (6)   NULL,
    [Validity_StartDate]             DATETIME2 (7)   NOT NULL,
    [Validity_EndDate]               DATETIME2 (7)   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_FE_SUBSCRIBEDCONSUMERLOAN_V2] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UC_ID_FE_SUBSCRIBEDCONSUMERLOAN] UNIQUE NONCLUSTERED ([ID_FE_SUBSCRIBEDCONSUMERLOAN] ASC, [Validity_StartDate] ASC)
);



