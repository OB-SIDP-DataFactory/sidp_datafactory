﻿CREATE TABLE [dbo].[RAW_SF_REFERENTIEL_xsn] (
    [ID]                   BIGINT         IDENTITY (1, 1) NOT NULL,
    [OBJECT_CODE_SF]       NVARCHAR (255) NULL,
    [FIELD_CODE_SF]        NVARCHAR (255) NULL,
    [FIELD_NAME_SF]        NVARCHAR (255) NULL,
    [IDX_SF]               INT            NULL,
    [CODE_SF]              NVARCHAR (255) NULL,
    [LIBELLE_SF]           NVARCHAR (255) NULL,
    [IS_DEFAULT]           BIT            NULL,
    [IS_ACTIVE]            BIT            NULL,
    [PARENT_FIELD_CODE_SF] NVARCHAR (255) NULL,
    [PARENT_FIELD_NAME_SF] NVARCHAR (255) NULL,
    [PARENT_IDX_SF]        INT            NULL,
    [PARENT_CODE_SF]       NVARCHAR (255) NULL,
    [PARENT_LIBELLE_SF]    NVARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
)
GO