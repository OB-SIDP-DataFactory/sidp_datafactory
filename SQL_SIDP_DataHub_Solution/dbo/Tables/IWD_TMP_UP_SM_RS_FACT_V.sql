﻿CREATE TABLE [dbo].[IWD_TMP_UP_SM_RS_FACT_V] (
    [SM_RES_SESSION_FACT_KEY]        NUMERIC (19) NOT NULL,
    [START_DATE_TIME_KEY]            INT          NOT NULL,
    [END_DATE_TIME_KEY]              INT          NOT NULL,
    [START_TS]                       INT          NULL,
    [END_TS]                         INT          NOT NULL,
    [START_MSEC]                     NUMERIC (19) NULL,
    [END_MSEC]                       NUMERIC (19) NULL,
    [TOTAL_DURATION]                 INT          NOT NULL,
    [LEAD_CLIP_DURATION]             INT          NOT NULL,
    [TRAIL_CLIP_DURATION]            INT          NOT NULL,
    [ACTIVE_FLAG]                    NUMERIC (1)  NOT NULL,
    [RESOURCE_GROUP_COMBINATION_KEY] INT          NULL,
    [ACTIVATION_AUDIT_KEY]           NUMERIC (19) NULL,
    [UPDATE_AUDIT_KEY]               NUMERIC (19) NOT NULL,
    CONSTRAINT [PK_T_SM_RS_FACT_V] PRIMARY KEY CLUSTERED ([SM_RES_SESSION_FACT_KEY] ASC)
);

