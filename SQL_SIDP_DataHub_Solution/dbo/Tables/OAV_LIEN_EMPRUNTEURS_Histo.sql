﻿CREATE TABLE [dbo].[OAV_LIEN_EMPRUNTEURS_Histo] (
    [ID_LIEN_EMPRUNTEURS] DECIMAL (10)   NOT NULL,
    [LABEL]               NVARCHAR (255) NOT NULL,
    [DAT_OBSR]            DATE           NOT NULL,
    [DAT_CHRG]            DATETIME       NOT NULL
);

