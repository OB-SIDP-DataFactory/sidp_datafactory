﻿CREATE TABLE [dbo].[WK_SF_LEAD] (
    [Id_SF]                          NVARCHAR (18)    NULL,
    [IsDeleted]                      NVARCHAR (10)    NULL,
    [MasterRecordId]                 NVARCHAR (18)    NULL,
    [LastName]                       NVARCHAR (80)    NULL,
    [FirstName]                      NVARCHAR (40)    NULL,
    [Salutation]                     NVARCHAR (40)    NULL,
    [Name]                           NVARCHAR (121)   NULL,
    [RecordTypeId]                   NVARCHAR (18)    NULL,
    [Title]                          NVARCHAR (128)   NULL,
    [Company]                        NVARCHAR (255)   NULL,
    [Street]                         NVARCHAR (255)   NULL,
    [City]                           NVARCHAR (40)    NULL,
    [State]                          NVARCHAR (80)    NULL,
    [PostalCode]                     NVARCHAR (20)    NULL,
    [Country]                        NVARCHAR (80)    NULL,
    [StateCode]                      NVARCHAR (10)    NULL,
    [CountryCode]                    NVARCHAR (10)    NULL,
    [Latitude]                       DECIMAL (18, 15) NULL,
    [Longitude]                      DECIMAL (18, 15) NULL,
    [GeocodeAccuracy]                NVARCHAR (40)    NULL,
    [Phone]                          NVARCHAR (40)    NULL,
    [Email]                          NVARCHAR (80)    NULL,
    [Website]                        NVARCHAR (255)   NULL,
    [PhotoUrl]                       NVARCHAR (255)   NULL,
    [Description]                    NVARCHAR (MAX)   NULL,
    [LeadSource]                     NVARCHAR (40)    NULL,
    [Status]                         NVARCHAR (40)    NULL,
    [Industry]                       NVARCHAR (40)    NULL,
    [Rating]                         NVARCHAR (40)    NULL,
    [AnnualRevenue]                  DECIMAL (18, 2)  NULL,
    [NumberOfEmployees]              INT              NULL,
    [OwnerId]                        NVARCHAR (18)    NULL,
    [IsConverted]                    NVARCHAR (10)    NULL,
    [ConvertedDate]                  DATE             NULL,
    [ConvertedAccountId]             NVARCHAR (18)    NULL,
    [ConvertedContactId]             NVARCHAR (18)    NULL,
    [ConvertedOpportunityId]         NVARCHAR (18)    NULL,
    [IsUnreadByOwner]                NVARCHAR (10)    NULL,
    [CreatedDate]                    DATETIME         NULL,
    [CreatedById]                    NVARCHAR (18)    NULL,
    [LastModifiedDate]               DATETIME         NULL,
    [LastModifiedById]               NVARCHAR (18)    NULL,
    [SystemModstamp]                 DATETIME         NULL,
    [LastActivityDate]               DATE             NULL,
    [LastViewedDate]                 DATETIME         NULL,
    [LastReferencedDate]             DATETIME         NULL,
    [Jigsaw]                         NVARCHAR (20)    NULL,
    [JigsawContactId]                NVARCHAR (20)    NULL,
    [IndividualId]					 VARCHAR (18)	  NULL,
    [EmailBouncedReason]             NVARCHAR (255)   NULL,
    [EmailBouncedDate]               DATETIME         NULL,
    [et4ae5__HasOptedOutOfMobile__c] NVARCHAR (10)    NULL,
    [et4ae5__Mobile_Country_Code__c] NVARCHAR (255)   NULL,
    [CampaignCode__c]                NVARCHAR (MAX)   NULL,
    [ConversionDate__c]              DATE             NULL,
    [EmailOptinDate__c]              DATE             NULL,
    [EventCode__c]                   NVARCHAR (MAX)   NULL,
    [FirstVisitDate__c]              DATE             NULL,
    [IDLead__c]                      NVARCHAR (30)    NULL,
    [LeadProductFamily__c]           NVARCHAR (255)   NULL,
    [Numberofvisit__c]               DECIMAL (18, 15) NULL,
    [Optin_Email__c]                 NVARCHAR (10)    NULL,
    [Optin_SMS__c]                   NVARCHAR (10)    NULL,
    [Optin_contactcommercial__c]     NVARCHAR (10)    NULL,
    [SMSOptinDate__c]                DATE             NULL,
    [LeadSource__c]                  NVARCHAR (255)   NULL
)
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SMSOptinDate__c', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'LeadSource__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Optin_contactcommercial__c', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'SMSOptinDate__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Optin_SMS__c', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'Optin_contactcommercial__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Optin_Email__c', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'Optin_SMS__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numberofvisit__c', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'Optin_Email__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LeadProductFamily__c', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'Numberofvisit__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IDLead__c', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'LeadProductFamily__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FirstVisitDate__c', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'IDLead__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'EventCode__c', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'FirstVisitDate__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'EmailOptinDate__c', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'EventCode__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ConversionDate__c', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'EmailOptinDate__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CampaignCode__c', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'ConversionDate__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'et4ae5__Mobile_Country_Code__c', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'CampaignCode__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'et4ae5__HasOptedOutOfMobile__c', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'et4ae5__Mobile_Country_Code__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'EmailBouncedDate', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'et4ae5__HasOptedOutOfMobile__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'EmailBouncedReason', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'EmailBouncedDate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'JigsawContactId', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'EmailBouncedReason'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Jigsaw', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'JigsawContactId'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LastReferencedDate', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'Jigsaw'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LastViewedDate', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'LastReferencedDate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LastActivityDate', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'LastViewedDate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SystemModstamp', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'LastActivityDate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LastModifiedById', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'SystemModstamp'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LastModifiedDate', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'LastModifiedById'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CreatedById', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'LastModifiedDate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CreatedDate', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'CreatedById'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IsUnreadByOwner', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'CreatedDate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ConvertedOpportunityId', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'IsUnreadByOwner'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ConvertedContactId', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'ConvertedOpportunityId'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ConvertedAccountId', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'ConvertedContactId'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ConvertedDate', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'ConvertedAccountId'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IsConverted', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'ConvertedDate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'OwnerId', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'IsConverted'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NumberOfEmployees', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'OwnerId'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AnnualRevenue', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'NumberOfEmployees'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Rating', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'AnnualRevenue'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Industry', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'Rating'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Status', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'Industry'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LeadSource', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'Status'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Description', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'LeadSource'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PhotoUrl', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'Description'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Website', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'PhotoUrl'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Email', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'Website'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Phone', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'Email'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Address', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'Phone'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'GeocodeAccuracy', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'GeocodeAccuracy'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Longitude', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'Longitude'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Latitude', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'Latitude'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CountryCode', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'CountryCode'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'StateCode', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'StateCode'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Country', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'Country'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PostalCode', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'PostalCode'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'State', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'State'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'City', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'City'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Street', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'Street'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Company', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'Company'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Title', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'Title'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RecordTypeId', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'RecordTypeId'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Name', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'Name'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Salutation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'Salutation'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FirstName', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'FirstName'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LastName', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'LastName'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MasterRecordId', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'MasterRecordId'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IsDeleted', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'IsDeleted'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Id', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LEAD', @level2type = N'COLUMN', @level2name = N'Id_SF'
GO