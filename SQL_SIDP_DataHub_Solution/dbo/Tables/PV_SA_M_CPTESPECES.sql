﻿CREATE TABLE [dbo].[PV_SA_M_CPTESPECES] (
    [ID]        BIGINT       IDENTITY (1, 1) NOT NULL,
    [DWHPTCETB] INT          NULL,
    [DWHPTCAGC] INT          NULL,
    [DWHPTCNPT] INT          NULL,
    [DWHPTCRCL] VARCHAR (7)  NULL,
    [DWHPTCSFC] INT          NULL,
    [DWHPTCECL] VARCHAR (20) NULL,
    [DWHPTCDCL] DATE         NULL,
    CONSTRAINT [PK_PV_SA_M_CPTESPECES] PRIMARY KEY CLUSTERED ([ID] ASC)
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE CLÔTURE COMPTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CPTESPECES', @level2type = N'COLUMN', @level2name = N'DWHPTCDCL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RÉF. COMPTE CLIENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CPTESPECES', @level2type = N'COLUMN', @level2name = N'DWHPTCECL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SUFFIXE DE COMPTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CPTESPECES', @level2type = N'COLUMN', @level2name = N'DWHPTCSFC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RACINE CLIENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CPTESPECES', @level2type = N'COLUMN', @level2name = N'DWHPTCRCL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° PORTEFEUILLE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CPTESPECES', @level2type = N'COLUMN', @level2name = N'DWHPTCNPT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AGENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CPTESPECES', @level2type = N'COLUMN', @level2name = N'DWHPTCAGC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ETABLISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CPTESPECES', @level2type = N'COLUMN', @level2name = N'DWHPTCETB';

