﻿CREATE TABLE [dbo].[OAV_TAUX] (
    [ID_TAUX]                DECIMAL (10) NOT NULL,
    [ID_TAUX_DATES]          DECIMAL (10) NULL,
    [ID_TAUX_DUREES]         DECIMAL (10) NULL,
    [ID_TAUX_MONTANTS]       DECIMAL (10) NULL,
    [ID_OBJETS_FINANCEMENTS] DECIMAL (10) NULL,
    [VALUE]                  FLOAT (53)   NULL,
    [DAT_OBSR]               DATE         CONSTRAINT [DefValStartDatOAV_TAUX] DEFAULT (getdate()) NOT NULL,
    [DAT_CHRG]               DATETIME     CONSTRAINT [DefValEndDatOAV_TAUX] DEFAULT (CONVERT([date],'9999-12-31')) NOT NULL,
    CONSTRAINT [PK_PrimaryKeyTAUX] PRIMARY KEY CLUSTERED ([ID_TAUX] ASC)
);


