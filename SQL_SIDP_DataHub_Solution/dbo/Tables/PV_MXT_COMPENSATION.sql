﻿CREATE TABLE [dbo].[PV_MXT_COMPENSATION] (
    [Id]                     BIGINT         IDENTITY (1, 1) NOT NULL,
    [COD_ENRG]               NVARCHAR (2)   NULL,
    [NUMR_SEQN]              NVARCHAR (6)   NULL,
    [EVNM_DECL]              NVARCHAR (4)   NULL,
    [TYP_OPRT_EXTR]          NVARCHAR (4)   NULL,
    [NATR_OPRT]              NVARCHAR (2)   NULL,
    [COD_OPRT_EXTR]          NVARCHAR (3)   NULL,
    [SOUS_OPRT_EXTR]         NVARCHAR (3)   NULL,
    [TYP_MOUV]               NVARCHAR (1)   NULL,
    [DAT_TRTM]               DATE           NULL,
    [COD_OPRT_SAB]           NVARCHAR (3)   NULL,
    [COD_OPRT_ANNL__SAB]     NVARCHAR (3)   NULL,
    [NOM_PORT]               NVARCHAR (32)  NULL,
    [NUMR_CART]              NVARCHAR (19)  NULL,
    [NUMR_SEQN_CART]         NVARCHAR (2)   NULL,
    [IBN_COD_PAYS]           NVARCHAR (2)   NULL,
    [IBN_CL]                 NVARCHAR (2)   NULL,
    [IBN_BANQ]               NVARCHAR (5)   NULL,
    [IBN_GUIC]               NVARCHAR (5)   NULL,
    [IBN_COMP]               NVARCHAR (20)  NULL,
    [IBN_CL_RIB]             NVARCHAR (2)   NULL,
    [IDNT_PORT]              NVARCHAR (7)   NULL,
    [REFR_CONT]              NVARCHAR (10)  NULL,
    [TOP_CART_OPPS]          NVARCHAR (1)   NULL,
    [TOP_CART_CLTR]          NVARCHAR (1)   NULL,
    [MONT_BRT]               NVARCHAR (12)  NULL,
    [MONT_BRT__DEVS]         NVARCHAR (12)  NULL,
    [MONT_NET]               NVARCHAR (12)  NULL,
    [COD_DEVS_MONT_BRT]      NVARCHAR (3)   NULL,
    [COD_DEVS_MONT_BRT_DEVS] NVARCHAR (3)   NULL,
    [SENS_MONT_BRT]          NVARCHAR (1)   NULL,
    [COMM_SERV]              NVARCHAR (12)  NULL,
    [REFR_ARCH]              NVARCHAR (12)  NULL,
    [NUMR_AUTR]              NVARCHAR (6)   NULL,
    [DAT_LOCL]               NVARCHAR (8)   NULL,
    [HEUR_LOCL]              NVARCHAR (6)   NULL,
    [LIBL_OPRT]              NVARCHAR (32)  NULL,
    [STTT_OPRT]              NVARCHAR (1)   NULL,
    [DAT_REGL]               NVARCHAR (8)   NULL,
    [IDNT_UNQ]               NVARCHAR (17)  NULL,
    [IDNT_UNQ_OPRT_ORGN]     NVARCHAR (17)  NULL,
    [REGN_OPRT]              NVARCHAR (1)   NULL,
    [TYP_DEBT]               NVARCHAR (1)   NULL,
    [NOM_ACCP]               NVARCHAR (25)  NULL,
    [COD_PAYS]               NVARCHAR (3)   NULL,
    [COD_DEPR]               NVARCHAR (3)   NULL,
    [VILL_DAB]               NVARCHAR (16)  NULL,
    [COD_MCC]                NVARCHAR (4)   NULL,
    [IDNT_TERM]              NVARCHAR (8)   NULL,
    [CONT_COMM]              NVARCHAR (7)   NULL,
    [IBN_COD_PAYS2]          NVARCHAR (2)   NULL,
    [IBN_CL2]                NVARCHAR (2)   NULL,
    [IBN_BANQ2]              NVARCHAR (5)   NULL,
    [IBN_GUIC2]              NVARCHAR (5)   NULL,
    [IBN_COMP2]              NVARCHAR (20)  NULL,
    [CL_RIB]                 NVARCHAR (2)   NULL,
    [IDNT_CLNT]              NVARCHAR (7)   NULL,
    [NOMB_OPRT]              NVARCHAR (6)   NULL,
    [MONT]                   NVARCHAR (12)  NULL,
    [SENS_MONT_BRT2]         NVARCHAR (1)   NULL,
    [MONT_BRT_DEVS]          NVARCHAR (12)  NULL,
    [COD_DEVS]               NVARCHAR (3)   NULL,
    [REFR_REMS]              NVARCHAR (6)   NULL,
    [REFR_ARCH2]             NVARCHAR (17)  NULL,
    [DAT_REMS]               NVARCHAR (8)   NULL,
    [COMM_FIX]               NVARCHAR (12)  NULL,
    [CUML_COMM]              NVARCHAR (12)  NULL,
    [COD_DEVS2]              NVARCHAR (3)   NULL,
    [DAT_VALR]               NVARCHAR (8)   NULL,
    [DAT_REGL2]              NVARCHAR (8)   NULL,
    [STTT]                   NVARCHAR (1)   NULL,
    [COD_BANQ_EMTT]          NVARCHAR (5)   NULL,
    [COD_BANQ_CHF_FIL]       NVARCHAR (5)   NULL,
    [COD_BANQ_ACQR]          NVARCHAR (5)   NULL,
    [COD_BANQ_ACCP]          NVARCHAR (5)   NULL,
    [MONT_COMP]              NVARCHAR (12)  NULL,
    [SENS_MONT_COMP]         NVARCHAR (1)   NULL,
    [MONT_COMM_INTR]         NVARCHAR (12)  NULL,
    [SENS_COMM]              NVARCHAR (1)   NULL,
    [COD_DEVS_MONT_COMP]     NVARCHAR (3)   NULL,
    [COD_DEVS_COMM_INTR]     NVARCHAR (3)   NULL,
    [MOD_GEST]               NVARCHAR (3)   NULL,
    [RES_COMP]               NVARCHAR (1)   NULL,
    [MONT_BRT2]              NVARCHAR (12)  NULL,
    [COD_DEVS3]              NVARCHAR (3)   NULL,
    [MOTF_SUSP]              NVARCHAR (4)   NULL,
    [MOTF_IMP]               NVARCHAR (4)   NULL,
    [DAT_IMP]                NVARCHAR (8)   NULL,
    [DAT_REPR]               NVARCHAR (8)   NULL,
    [MOTF_REPR]              NVARCHAR (4)   NULL,
    [MONT_REJT__PORT]        NVARCHAR (12)  NULL,
    [DEVS_MONT_REJT]         NVARCHAR (3)   NULL,
    [REFR_IMP]               NVARCHAR (12)  NULL,
    [REFR_DOSS_LITG]         NVARCHAR (12)  NULL,
    [TOP_CRDT_BRT]           NVARCHAR (1)   NULL,
    [MOTF_CAPT]              NVARCHAR (2)   NULL,
    [TAUX_CHNG]              NVARCHAR (16)  NULL,
    [DAT_ET_HEUR_TRNS_TRNS]  NVARCHAR (10)  NULL,
    [TYP_ACCP]               NVARCHAR (1)   NULL,
    [MONT_PENL_RETR]         NVARCHAR (12)  NULL,
    [SENS_MONT_PENL]         NVARCHAR (1)   NULL,
    [REFR_LETT_CART]         NVARCHAR (16)  NULL,
    [COD_FOUR]               NVARCHAR (3)   NULL,
    [Validity_StartDate]     DATETIME2 (7)  NOT NULL,
    [Validity_EndDate]       DATETIME2 (7)  NOT NULL,
    [NOM_FICHIER]            NVARCHAR (200) NULL,
    [INSRT_TS]               DATETIME       NULL,
    [FILL_TXT] VARCHAR(50) NULL, 
    CONSTRAINT [PK_PV_MXT_COMPENSATION] PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'code fournisseur ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'COD_FOUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'référence lettrage carte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'REFR_LETT_CART';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'sens du montant pénalité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'SENS_MONT_PENL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant pénalité de retard ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'MONT_PENL_RETR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'type daccepteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'TYP_ACCP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'date et heure transmission de la transaction ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'DAT_ET_HEUR_TRNS_TRNS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux de change ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'TAUX_CHNG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motif de capture ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'MOTF_CAPT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Top crédit brut', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'TOP_CRDT_BRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'référence du dossier litige', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'REFR_DOSS_LITG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'référence de limpayé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'REFR_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'devise du montant rejeté', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'DEVS_MONT_REJT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'montant rejeté  par le porteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'MONT_REJT__PORT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'motif de représentation ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'MOTF_REPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'date de représentation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'DAT_REPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'date de limpayé ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'DAT_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'motif impayé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'MOTF_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motif du suspens', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'MOTF_SUSP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'code devise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'COD_DEVS3';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant brut ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'MONT_BRT2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Réseau de compensation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'RES_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mode de gestion', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'MOD_GEST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'code devise de la commission interbancaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'COD_DEVS_COMM_INTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'code devise du montant compensé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'COD_DEVS_MONT_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'sens de la commission ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'SENS_COMM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'montant commission interbancaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'MONT_COMM_INTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'sens du montant compensé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'SENS_MONT_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant compensé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'MONT_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'code banque accepteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'COD_BANQ_ACCP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'code banque acquéreur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'COD_BANQ_ACQR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'code banque chef de file', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'COD_BANQ_CHF_FIL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'code banque émettrice', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'COD_BANQ_EMTT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'statut ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'STTT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'date de règlement ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'DAT_REGL2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'date de valeur ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'DAT_VALR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'code devise ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'COD_DEVS2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'cumul commission', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'CUML_COMM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'commission fixe', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'COMM_FIX';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'dat de la remise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'DAT_REMS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'référence darchivage ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'REFR_ARCH2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'référence de la remise ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'REFR_REMS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'code devise ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'COD_DEVS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'montant brut devise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'MONT_BRT_DEVS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'sens du montant brut ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'SENS_MONT_BRT2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'MONT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'nombre dopération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'NOMB_OPRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'IDNT_CLNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'clé rib', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'CL_RIB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IBAN compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'IBN_COMP2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IBAN guichet', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'IBN_GUIC2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IBAN Banque', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'IBN_BANQ2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IBAN clé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'IBN_CL2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IBAN code Pays ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'IBN_COD_PAYS2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Contrat commerçant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'CONT_COMM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant du terminal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'IDNT_TERM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code MCC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'COD_MCC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ville du DAB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'VILL_DAB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'code département ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'COD_DEPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'code pays ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'COD_PAYS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom de laccepteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'NOM_ACCP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type de débit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'TYP_DEBT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Région de lopération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'REGN_OPRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant unique de lopération dorigine', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'IDNT_UNQ_OPRT_ORGN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant unique ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'IDNT_UNQ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de règlement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'DAT_REGL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Statut de lopération ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'STTT_OPRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé de lopération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'LIBL_OPRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Heure locale ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'HEUR_LOCL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date locale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'DAT_LOCL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro dautorisation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'NUMR_AUTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Référence darchivage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'REFR_ARCH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Commission de service ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'COMM_SERV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'sens du montant brut ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'SENS_MONT_BRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code devise du montant brut en devise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'COD_DEVS_MONT_BRT_DEVS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code devise du montant brut', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'COD_DEVS_MONT_BRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant Net', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'MONT_NET';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant Brut  en devise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'MONT_BRT__DEVS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant Brut ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'MONT_BRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Top carte cloturée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'TOP_CART_CLTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Top carte opposée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'TOP_CART_OPPS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'référence contrat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'REFR_CONT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'identifiant porteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'IDNT_PORT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IBAN clé rib', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'IBN_CL_RIB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IBAN compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'IBN_COMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IBAN Guichet', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'IBN_GUIC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IBAN Banque', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'IBN_BANQ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IBAN clé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'IBN_CL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IBAN code pays', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'IBN_COD_PAYS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro de séquence de la carte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'NUMR_SEQN_CART';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro de carte ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'NUMR_CART';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom du porteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'NOM_PORT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code opération annulée  SAB ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'COD_OPRT_ANNL__SAB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code opération SAB ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'COD_OPRT_SAB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de traitement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'DAT_TRTM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type de mouvement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'TYP_MOUV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Sous-code opération externe', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'SOUS_OPRT_EXTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code opération externe', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'COD_OPRT_EXTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nature de lopération  ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'NATR_OPRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type dopération externe', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'TYP_OPRT_EXTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Evènement déclencheur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'EVNM_DECL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro de séquence ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'NUMR_SEQN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_COMPENSATION', @level2type = N'COLUMN', @level2name = N'COD_ENRG';

