﻿CREATE TABLE [dbo].[IWD_GIDB_GC_ENDPOINT] (
    [ID]               INT           NOT NULL,
    [DN]               VARCHAR (255) NOT NULL,
    [TYPE]             INT           NOT NULL,
    [STATE]            INT           NOT NULL,
    [SWITCHID]         INT           NOT NULL,
    [TENANTID]         INT           NOT NULL,
    [FOLDERID]         INT           NULL,
    [ASSOCIATION]      VARCHAR (255) NULL,
    [DNLOGINID]        VARCHAR (255) NULL,
    [ROUTETYPE]        INT           NOT NULL,
    [STATUS]           INT           NOT NULL,
    [CREATED]          DATETIME      NULL,
    [DELETED]          DATETIME      NULL,
    [LASTCHANGE]       DATETIME      NULL,
    [CREATED_TS]       INT           NULL,
    [DELETED_TS]       INT           NULL,
    [LASTCHANGE_TS]    INT           NULL,
    [GSYS_DOMAIN]      INT           NULL,
    [GSYS_SYS_ID]      INT           NULL,
    [GSYS_EXT_VCH1]    VARCHAR (255) NULL,
    [GSYS_EXT_VCH2]    VARCHAR (255) NULL,
    [GSYS_EXT_INT1]    INT           NULL,
    [GSYS_EXT_INT2]    INT           NULL,
    [DATA_SOURCE_KEY]  INT           NOT NULL,
    [CREATE_AUDIT_KEY] NUMERIC (19)  NOT NULL,
    [UPDATE_AUDIT_KEY] NUMERIC (19)  NOT NULL,
    CONSTRAINT [PK_GIDB_GC_ENDPOINT] PRIMARY KEY NONCLUSTERED ([ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IWD_I_GIDB_GC_ENDPOINT_UAK]
    ON [dbo].[IWD_GIDB_GC_ENDPOINT]([UPDATE_AUDIT_KEY] ASC);


GO
CREATE NONCLUSTERED INDEX [IWD_I_GIDB_GC_ENDPOINT_CAK]
    ON [dbo].[IWD_GIDB_GC_ENDPOINT]([CREATE_AUDIT_KEY] ASC);

