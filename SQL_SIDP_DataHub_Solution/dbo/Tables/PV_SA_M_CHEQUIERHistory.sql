﻿CREATE TABLE [dbo].[PV_SA_M_CHEQUIERHistory] (
    [ID]                 BIGINT        NOT NULL,
    [DWHCHQDTX]          DATE          NULL,
    [DWHCHQETB]          INT           NULL,
    [DWHCHQAG1]          INT           NULL,
    [DWHCHQAGE]          INT           NULL,
    [DWHCHQCOM]          VARCHAR (20)  NULL,
    [DWHCHQTYP]          VARCHAR (6)   NULL,
    [DWHCHQDAT]          DATE          NULL,
    [DWHCHQSEQ]          INT           NULL,
    [DWHCHQREC]          DATE          NULL,
    [DWHCHQREM]          DATE          NULL,
    [DWHCHQCAL]          VARCHAR (3)   NULL,
    [DWHCHQDSP]          DATE          NULL,
    [DWHCHQREN]          INT           NULL,
    [DWHCHQDRE]          DATE          NULL,
    [DWHCHQORI]          VARCHAR (1)   NULL,
    [Validity_StartDate] DATETIME      NULL,
    [Validity_EndDate]   DATETIME      NULL,
    [Startdt]            DATETIME2 (7) NOT NULL,
    [Enddt]              DATETIME2 (7) NOT NULL
);

