﻿CREATE TABLE [dbo].[REF_FINANCING_TYPE] (
    [ID]                       BIGINT         IDENTITY (1, 1) NOT NULL,
    [FINANCING_TYPE_ID]        DECIMAL (38)   NOT NULL,
    [HARVEST_CODE]             NVARCHAR (2)   NULL,
    [FRANFINANCE_CODE]         NVARCHAR (5)   NULL,
    [FINANCING_TYPE_FAMILY_ID] DECIMAL (38)   NULL,
    [SALESFORCE_CODE]          NVARCHAR (2)   NULL,
    [REF_FAMILY_ID]            DECIMAL (19)   NULL,
    [CODE]                     NVARCHAR (255) NULL,
    [PRIORITY]                 DECIMAL (19)   NULL,
    [REF_LANG_ID]              DECIMAL (19)   NULL,
    [MESSAGE]                  NVARCHAR (255) NULL,
    [Validity_StartDate]       DATETIME2 (7)  NOT NULL,
    [Validity_EndDate]         DATETIME2 (7)  NOT NULL,
    CONSTRAINT [PK_REF_FINANCING_TYPE] PRIMARY KEY CLUSTERED ([ID] ASC)
);

