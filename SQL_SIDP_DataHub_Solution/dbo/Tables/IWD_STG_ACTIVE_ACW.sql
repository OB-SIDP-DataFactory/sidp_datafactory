﻿CREATE TABLE [dbo].[IWD_STG_ACTIVE_ACW] (
    [PARTYID]                        VARCHAR (50)  NULL,
    [CALLDNIS]                       VARCHAR (50)  NULL,
    [ENDPOINTDN]                     VARCHAR (255) NULL,
    [ENDPOINTID]                     INT           NULL,
    [SWITCHID]                       INT           NULL,
    [INTERACTION_RESOURCE_STATE_KEY] INT           NOT NULL,
    [START_TS]                       INT           NULL,
    [END_TS]                         INT           NULL,
    [TENANT_KEY]                     INT           NOT NULL,
    [MEDIA_TYPE_KEY]                 INT           NOT NULL,
    [RESOURCE_KEY]                   INT           NOT NULL,
    [RESOURCE_DBID]                  INT           NULL,
    [MEDIA_RESOURCE_KEY]             INT           NOT NULL,
    [PLACE_KEY]                      INT           NOT NULL,
    [INTERACTION_TYPE_KEY]           INT           NOT NULL,
    [INTERACTION_RESOURCE_ID]        NUMERIC (19)  NULL,
    [IRF_START_DT_KEY]               INT           NULL,
    [INTERACTION_ID]                 NUMERIC (19)  NULL,
    [PARTYGUID]                      VARCHAR (50)  NULL,
    [CREATE_AUDIT_KEY]               NUMERIC (19)  NULL
);

