﻿CREATE TABLE [dbo].[TableResScore] (
    [DAT_VALD]           DATE            NOT NULL,
    [Num]                INT             NULL,
    [IDCustomer__pc]     NVARCHAR (11)   NULL,
    [Id_SF]              NVARCHAR (18)   NOT NULL,
    [DistributorData__c] NVARCHAR (MAX)  NULL,
    [FLG_ACTIF]          BIT             NULL,
    [value]              NVARCHAR (1000) NULL,
    [nom]                NVARCHAR (50)   NULL,
    [lib_nom]            NVARCHAR (50)   NULL,
    [code_nom]           NVARCHAR (2)    NULL,
    [principal]          NVARCHAR (50)   NULL,
    [score]              NVARCHAR (6)    NULL
);

