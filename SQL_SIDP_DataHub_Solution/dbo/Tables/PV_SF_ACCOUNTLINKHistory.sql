﻿CREATE TABLE [dbo].[PV_SF_ACCOUNTLINKHistory] (
    [Id]                 BIGINT         NOT NULL,
    [Id_SF]              NVARCHAR (18)  NOT NULL,
    [IsDeleted]          NVARCHAR (10)  NULL,
    [Name]               NVARCHAR (80)  NULL,
    [RecordTypeId]       NVARCHAR (18)  NULL,
    [CreatedDate]        DATETIME       NULL,
    [CreatedById]        NVARCHAR (18)  NULL,
    [LastModifiedDate]   DATETIME       NULL,
    [LastModifiedById]   NVARCHAR (18)  NULL,
    [SystemModstamp]     DATETIME       NULL,
    [LastActivityDate]   DATETIME       NULL,
    [Person1__c]         NVARCHAR (18)  NULL,
    [Description__c]     NVARCHAR (80)  NULL,
    [ExpiryDate__c]      DATETIME       NULL,
    [IsActive__c]        NVARCHAR (10)  NULL,
    [LastCheckDate__c]   DATETIME       NULL,
    [LinkType__c]        NVARCHAR (255) NULL,
    [MustCheckLink__c]   NVARCHAR (10)  NULL,
    [Person2__c]         NVARCHAR (18)  NULL,
    [PersonRole1__c]     NVARCHAR (255) NULL,
    [PersonRole2__c]     NVARCHAR (255) NULL,
    [LastViewedDate]     DATETIME       NULL,
    [LastReferencedDate] DATETIME       NULL,
    [Validity_StartDate] DATETIME2 (7)  NOT NULL,
    [Validity_EndDate]   DATETIME2 (7)  NOT NULL,
    [Startdt]            DATETIME2 (7)  NOT NULL,
    [Enddt]              DATETIME2 (7)  NOT NULL
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Horodateur des modifications du système', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNTLINKHistory', @level2type = N'COLUMN', @level2name = N'SystemModstamp';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type d''enregistrement de la personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNTLINKHistory', @level2type = N'COLUMN', @level2name = N'RecordTypeId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Rôle Personne 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNTLINKHistory', @level2type = N'COLUMN', @level2name = N'PersonRole2__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Rôle Personne 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNTLINKHistory', @level2type = N'COLUMN', @level2name = N'PersonRole1__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom de la personne associée 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNTLINKHistory', @level2type = N'COLUMN', @level2name = N'Person2__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom de la personne associée 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNTLINKHistory', @level2type = N'COLUMN', @level2name = N'Person1__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom de la personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNTLINKHistory', @level2type = N'COLUMN', @level2name = N'Name';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TCH_Doit être vérifié', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNTLINKHistory', @level2type = N'COLUMN', @level2name = N'MustCheckLink__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nature du lien', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNTLINKHistory', @level2type = N'COLUMN', @level2name = N'LinkType__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNTLINKHistory', @level2type = N'COLUMN', @level2name = N'LastModifiedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernière modification par ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNTLINKHistory', @level2type = N'COLUMN', @level2name = N'LastModifiedById';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière vérification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNTLINKHistory', @level2type = N'COLUMN', @level2name = N'LastCheckDate__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernière activité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNTLINKHistory', @level2type = N'COLUMN', @level2name = N'LastActivityDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Supprimé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNTLINKHistory', @level2type = N'COLUMN', @level2name = N'IsDeleted';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNTLINKHistory', @level2type = N'COLUMN', @level2name = N'IsActive__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date d''expiration', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNTLINKHistory', @level2type = N'COLUMN', @level2name = N'ExpiryDate__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Commentaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNTLINKHistory', @level2type = N'COLUMN', @level2name = N'Description__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNTLINKHistory', @level2type = N'COLUMN', @level2name = N'CreatedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID créé par', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNTLINKHistory', @level2type = N'COLUMN', @level2name = N'CreatedById';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernier affichage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNTLINKHistory', @level2type = N'COLUMN', @level2name = N'LastViewedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernière date référencée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNTLINKHistory', @level2type = N'COLUMN', @level2name = N'LastReferencedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de la personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNTLINKHistory', @level2type = N'COLUMN', @level2name = N'Id_SF';

