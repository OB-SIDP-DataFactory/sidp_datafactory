﻿CREATE TABLE [dbo].[WK_SA_Q_MOUVEMENT] (
    [COD_OPE]         VARCHAR (3)     NULL,
    [NUM_OPE]         INT             NULL,
    [COD_EVE]         VARCHAR (3)     NULL,
    [COD_SCH]         INT             NULL,
    [NUM_PIE]         INT             NULL,
    [NUM_ECR]         INT             NULL,
    [COD_ANA]         VARCHAR (6)     NULL,
    [STR_ANA]         VARCHAR (6)     NULL,
    [COD_EXO]         VARCHAR (1)     NULL,
    [COD_BDF]         VARCHAR (3)     NULL,
    [NAT_OPE]         VARCHAR (3)     NULL,
    [NUM_CPT]         VARCHAR (20)    NULL,
    [MTT_DEV]         DECIMAL (16, 2) NULL,
    [SEN_MTT]         VARCHAR (1)     NULL,
    [DTE_OPE]         DATE            NULL,
    [DTE_CPT]         DATE            NULL,
    [DTE_VAL]         DATE            NULL,
    [DTE_TRT]         DATE            NULL,
    [COD_UTI]         VARCHAR (4)     NULL,
    [COD_AGE]         VARCHAR (4)     NULL,
    [COD_SVC]         VARCHAR (2)     NULL,
    [COD_SOU_SVC]     VARCHAR (2)     NULL,
    [COD_ANN]         VARCHAR (1)     NULL,
    [COD_BAN_EIC]     VARCHAR (5)     NULL,
    [LIB_MVT_1]       VARCHAR (30)    NULL,
    [LIB_MVT_2]       VARCHAR (30)    NULL,
    [LIB_MVT_3]       VARCHAR (30)    NULL,
    [COD_EME]         VARCHAR (6)     NULL,
    [DTE_TRT_MAD]     DATE            NULL,
    [DTE_REF_MAD]     DATE            NULL,
    [DTE_LIM_REJ_MAD] DATE            NULL,
    [COD_UTI_INI_MAD] VARCHAR (4)     NULL,
    [DTE_CRE_MAD]     DATE            NULL,
    [DTE_UTI_MAD]     DATE            NULL,
    [COD_UTI_MAD]     VARCHAR (4)     NULL,
    [REJ_ACC_MAD]     VARCHAR (1)     NULL,
    [DON_ANA]         VARCHAR (80)    NULL,
    [LIB_COMP_1]      VARCHAR (70)    NULL,
    [LIB_COMP_2]      VARCHAR (70)    NULL,
    [NUM_REM]         VARCHAR (7)     NULL,
    [NUM_CHQ]         VARCHAR (7)     NULL,
    [MTT_EUR]         DECIMAL (16, 2) NULL,
    [DEV]             VARCHAR (3)     NULL,
    [IDE_DO]          VARCHAR (7)     NULL,
    [IDE_CTP]         VARCHAR (7)     NULL,
    [NOM_CTP]         VARCHAR (70)    NULL,
    [NUM_CPT_CTP]     VARCHAR (35)    NULL,
    [BIC_CTP]         VARCHAR (12)    NULL,
    [COD_BAN_CTP]     VARCHAR (12)    NULL,
    [COD_PAY_CTP]     VARCHAR (5)     NULL,
    [TOP_CLI]         VARCHAR (1)     NULL,
    [FIX_DEV]         DECIMAL (14, 9) NULL,
    [COD_AGE_OPE]     VARCHAR (6)     NULL
);


