﻿CREATE TABLE [dbo].[REF_CODE_CATEGORIES_CLIENTS] (
    [ID]                                      BIGINT       IDENTITY (1, 1) NOT NULL,
    [CATEGORIES_CLIENTS_CODE_ETABLISSEMENT]   INT          NULL,
    [CATEGORIES_CLIENTS_CODE_IDENTIFIANT]     VARCHAR (3)  NULL,
    [CATEGORIES_CLIENTS_CODE_CATEGORIE_CLIEN] VARCHAR (3)  NULL,
    [CATEGORIES_CLIENTS_ABREGE]               VARCHAR (12) NULL,
    [CATEGORIES_CLIENTS_LIBELLE]              VARCHAR (30) NULL,
    [CATEGORIES_CLIENTS_TYPE_CATEGORIE]       VARCHAR (1)  NULL,
    CONSTRAINT [PK_REF_CODE_CATEGORIES_CLIENTS] PRIMARY KEY CLUSTERED ([ID] ASC)
);

