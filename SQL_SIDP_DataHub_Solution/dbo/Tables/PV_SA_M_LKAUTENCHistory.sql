﻿CREATE TABLE [dbo].[PV_SA_M_LKAUTENCHistory] (
    [ID]                 BIGINT          NOT NULL,
    [DWHENADTX]          DATE            NULL,
    [DWHENAETA]          INT             NULL,
    [DWHENACLI]          VARCHAR (7)     NULL,
    [DWHENATYP]          VARCHAR (1)     NULL,
    [DWHENAAUT]          VARCHAR (20)    NULL,
    [DWHENADEV]          VARCHAR (3)     NULL,
    [DWHENAAGE]          INT             NULL,
    [DWHENASER]          VARCHAR (2)     NULL,
    [DWHENASSE]          VARCHAR (2)     NULL,
    [DWHENAOPE]          VARCHAR (6)     NULL,
    [DWHENADOS]          INT             NULL,
    [DWHENANAT]          VARCHAR (6)     NULL,
    [DWHENAMBA]          DECIMAL (18, 3) NULL,
    [DWHENAENC]          DECIMAL (18, 3) NULL,
    [DWHENAREP]          DECIMAL (18, 3) NULL,
    [DWHENADEB]          DATE            NULL,
    [DWHENAFIN]          DATE            NULL,
    [DWHENAUTI]          INT             NULL,
    [DWHENAFRA]          VARCHAR (1)     NULL,
    [DWHENADOM]          VARCHAR (12)    NULL,
    [DWHENAAPP]          VARCHAR (3)     NULL,
    [DWHENADSY]          DATE            NULL,
    [Validity_StartDate] DATETIME        NULL,
    [Validity_EndDate]   DATETIME        NULL,
    [Startdt]            DATETIME2 (7)   NOT NULL,
    [Enddt]              DATETIME2 (7)   NOT NULL
);
GO
CREATE CLUSTERED INDEX [ix_PV_SA_M_LKAUTENCHistory]
    ON [dbo].[PV_SA_M_LKAUTENCHistory]([Enddt] ASC, [Startdt] ASC) WITH (DATA_COMPRESSION = PAGE);

