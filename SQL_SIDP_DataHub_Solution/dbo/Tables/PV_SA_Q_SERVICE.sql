﻿CREATE TABLE [dbo].[PV_SA_Q_SERVICE] (
    [ID]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [DWHABSDTX]          DATE                                        NULL,
    [DWHABSETA]          INT                                         NULL,
    [DWHABSAGE]          INT                                         NULL,
    [DWHABSSER]          VARCHAR (2)                                 NULL,
    [DWHABSSSE]          VARCHAR (2)                                 NULL,
    [DWHABSNUM]          INT                                         NULL,
    [DWHABSCSE]          VARCHAR (6)                                 NULL,
    [DWHABSADH]          DATE                                        NULL,
    [DWHABSFIN]          DATE                                        NULL,
    [DWHABSREN]          DATE                                        NULL,
    [DWHABSCET]          VARCHAR (1)                                 NULL,
    [DWHABSRES]          DATE                                        NULL,
    [DWHABSMOR]          VARCHAR (6)                                 NULL,
    [DWHABSCRE]          DATE                                        NULL,
    [DWHABSVAL]          DATE                                        NULL,
    [Validity_StartDate] DATETIME                                    NULL,
    [Validity_EndDate]   DATETIME                                    DEFAULT ('99991231') NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    [NUMR_LIGN_FICH]     INT                                         NULL,
    CONSTRAINT [PK_PV_SA_Q_SERVICE] PRIMARY KEY CLUSTERED ([ID] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SA_Q_SERVICEHistory], DATA_CONSISTENCY_CHECK=ON));
GO
