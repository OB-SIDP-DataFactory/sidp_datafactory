﻿CREATE TABLE [dbo].[REF_SEUIL] (
    [CODE_INDICATEUR_PARENT] VARCHAR (50) NULL,
    [CODE_INDICATEUR]        VARCHAR (50) NULL,
    [CODE_TYPE_INDICATEUR]   VARCHAR (50) NULL,
    [CODE_AXE1]              VARCHAR (50) NULL,
    [VALEUR_AXE1]            VARCHAR (50) NULL,
    [CODE_AXE2]              VARCHAR (50) NULL,
    [VALEUR_AXE2]            VARCHAR (50) NULL,
    [CODE_AXE3]              VARCHAR (50) NULL,
    [VALEUR_AXE3]            VARCHAR (50) NULL,
    [CODE_AXE4]              VARCHAR (50) NULL,
    [VALEUR_AXE4]            VARCHAR (50) NULL,
    [CODE_DATE]              DATE         NULL,
    [VALEUR_INDICATEUR]      FLOAT (53)   NULL,
    [DATE_ACTION]            DATETIME     NULL
);

