﻿CREATE TABLE [dbo].[PV_MC_OPENS] (
    [Id]                       BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [ClientID]                 INT                                         NULL,
    [SendID]                   INT                                         NOT NULL,
    [SubscriberKey]            NVARCHAR (100)                              NULL,
    [EmailAddress]             NVARCHAR (100)                              NULL,
    [SubscriberID]             INT                                         NOT NULL,
    [ListID]                   INT                                         NULL,
    [EventDate]                DATETIME                                    NOT NULL,
    [EventType]                NVARCHAR (100)                              NULL,
    [BatchID]                  NVARCHAR (100)                              NULL,
    [TriggeredSendExternalKey] NVARCHAR (100)                              NULL,
    [IsUnique]                 NVARCHAR (5)                                NULL,
    [Browser]                  NVARCHAR (100)                              NULL,
    [EmailClient]              NVARCHAR (100)                              NULL,
    [OperatingSystem]          NVARCHAR (100)                              NULL,
    [Device]                   NVARCHAR (100)                              NULL,
    [Startdt]                  DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]                    DATETIME2 (7) GENERATED ALWAYS AS ROW END   NOT NULL,
    CONSTRAINT [PK_PV_MC_OPENS] PRIMARY KEY CLUSTERED ([Id] ASC, [SendID] ASC, [SubscriberID] ASC, [EventDate] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_MC_OPENSHistory], DATA_CONSISTENCY_CHECK=ON));


