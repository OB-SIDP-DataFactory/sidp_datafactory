﻿CREATE TABLE [dbo].[PV_SA_M_EXPRISK] (
    [ID]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [DWHEXPDTA]          DATE                                        NULL,
    [DWHEXPETA]          INT                                         NULL,
    [DWHEXPAGE]          INT                                         NULL,
    [DWHEXPSER]          VARCHAR (2)                                 NULL,
    [DWHEXPSSE]          VARCHAR (2)                                 NULL,
    [DWHEXPPLA]          INT                                         NULL,
    [DWHEXPOPE]          VARCHAR (6)                                 NULL,
    [DWHEXPNAT]          VARCHAR (10)                                NULL,
    [DWHEXPNUM]          VARCHAR (20)                                NULL,
    [DWHEXPNSE]          INT                                         NULL,
    [DWHEXPSEQ]          INT                                         NULL,
    [DWHEXPTYP]          VARCHAR (1)                                 NULL,
    [DWHEXPCOM]          VARCHAR (1)                                 NULL,
    [DWHEXPDEV]          VARCHAR (3)                                 NULL,
    [DWHEXPFIN]          DATE                                        NULL,
    [DWHEXPDUI]          INT                                         NULL,
    [DWHEXPDUR]          INT                                         NULL,
    [DWHEXPTYO]          VARCHAR (1)                                 NULL,
    [DWHEXPTYA]          VARCHAR (1)                                 NULL,
    [DWHEXPCDA]          VARCHAR (3)                                 NULL,
    [DWHEXPNOA]          VARCHAR (6)                                 NULL,
    [DWHEXPDEA]          VARCHAR (3)                                 NULL,
    [DWHEXPAUT]          VARCHAR (20)                                NULL,
    [DWHEXPRUB]          VARCHAR (10)                                NULL,
    [DWHEXPCLI]          VARCHAR (7)                                 NULL,
    [DWHEXPECO]          VARCHAR (3)                                 NULL,
    [DWHEXPCHF]          DECIMAL (18, 3)                             NULL,
    [DWHEXPDBA]          VARCHAR (1)                                 NULL,
    [DWHEXPACT]          VARCHAR (1)                                 NULL,
    [DWHEXPCOE]          VARCHAR (4)                                 NULL,
    [DWHEXPFIX]          DECIMAL (14, 9)                             NULL,
    [DWHEXPOFI]          VARCHAR (6)                                 NULL,
    [DWHEXPTAU]          DECIMAL (14, 9)                             NULL,
    [DWHEXPENC]          DECIMAL (18, 3)                             NULL,
    [DWHEXPINT]          DECIMAL (18, 3)                             NULL,
    [DWHEXPIMP]          DECIMAL (18, 3)                             NULL,
    [DWHEXPEXB]          DECIMAL (18, 3)                             NULL,
    [DWHEXPPRO]          DECIMAL (18, 3)                             NULL,
    [DWHEXPEXN]          DECIMAL (18, 3)                             NULL,
    [DWHEXPCAT]          VARCHAR (6)                                 NULL,
    [DWHEXPREG]          VARCHAR (1)                                 NULL,
    [DWHEXPTXP]          DECIMAL (14, 9)                             NULL,
    [DWHEXPCRD]          INT                                         NULL,
    [DWHEXPCOU]          INT                                         NULL,
    [DWHEXPMIB]          INT                                         NULL,
    [DWHEXPIAS]          VARCHAR (1)                                 NULL,
    [DWHEXPEXA]          DECIMAL (18, 3)                             NULL,
    [DWHEXPEAP]          DECIMAL (18, 3)                             NULL,
    [DWHEXPEXS]          DECIMAL (18, 3)                             NULL,
    [DWHEXPESP]          DECIMAL (18, 3)                             NULL,
    [DWHEXPEXR]          DECIMAL (18, 3)                             NULL,
    [DWHEXPENA]          DECIMAL (18, 3)                             NULL,
    [DWHEXPINA]          DECIMAL (18, 3)                             NULL,
    [DWHEXPIMA]          DECIMAL (18, 3)                             NULL,
    [DWHEXPPRA]          DECIMAL (18, 3)                             NULL,
    [DWHEXPAJU]          VARCHAR (100)                               NULL,
    [DWHEXPUTI]          INT                                         NULL,
    [DWHEXPDAT]          DATE                                        NULL,
    [DWHEXPFIL]          VARCHAR (100)                               NULL,
    [Validity_StartDate] DATETIME                                    NULL,
    [Validity_EndDate]   DATETIME                                    DEFAULT ('99991231') NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SA_M_EXPRISK] PRIMARY KEY CLUSTERED ([ID] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SA_M_EXPRISKHistory], DATA_CONSISTENCY_CHECK=ON));
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FILLER', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPFIL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE AJUSTEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPDAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'UTILISATEUR AJUSTEM', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPUTI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MOTIF AJUSTEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPAJU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PROVISIONS AV AJUSTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPPRA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MNT IMPAY. AV AJUSTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPIMA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'INT PRO/COUR AV AJUS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPINA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ENCOURS BRUT AV AJUS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPENA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'EXPO RISQUE CREDIT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPEXR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'EXP NON ASSUJ PONDÉR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPESP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'EXPO NON ASSUJETTIE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPEXS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'EXPOS ASSUJETTIE PON', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPEAP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'EXPO ASSUJ.A REDUCT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPEXA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ENCOURS IAS O/N', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPIAS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MNT. PROVISIONS IAS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPMIB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MNT.INTER.COURUS IAS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPCOU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ENCOURS IAS DV BASE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPCRD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TAUX PONDERAT°/CATEG', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPTXP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'REGIM DE LA CATEG', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPREG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE CATEG EXPOSIT°', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPCAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'EXPOSITION NETTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPEXN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PROVISIONS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPPRO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'EXPOSITION BRUTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPEXB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT TOTAL IMPAYE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPIMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'INT. PROV/COURUS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPINT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ENCOURS BRUT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPENC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TAUX DE CONVERS° HB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPTAU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'OBJET DE FINANCEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPOFI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FIXING DATE ARRÊTÉ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPFIX';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE ETAT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPCOE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COTE ACTIVITE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPACT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE DOUTEUX REGLEME', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPDBA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CHIFFRE AFFAIRES', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPCHF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE QUALITÉ CLIENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPECO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMERO CLIENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPCLI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RUBRIQUE DU COMPTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPRUB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COMPTE AUTORISATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPAUT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEVISE AUTORISATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPDEA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N°  AUTORISATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPNOA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE AUTORISATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPCDA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE AUTORISATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPTYA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FLAG BILAN / HB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPTYO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DUREE RESIDUELLE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPDUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DUREE INITIALE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPDUI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE FIN OPERATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPFIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEVISE OPERAT°/CPTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPDEV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COMPTE CLIENT/INTERN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPCOM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE EXPOSITION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPTYP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N°DE SÉQUENCE PRERES', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPSEQ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMERO DE SÉQUENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPNSE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMERO OPERAT°/CPT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPNUM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE NATURE/RUBRIQ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPNAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE OPERA/TYPE CPT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPOPE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PLAN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPPLA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SOUS SERVICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPSSE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SERVICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPSER';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AGENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPAGE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE ETABLISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPETA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ANALYSE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_EXPRISK', @level2type = N'COLUMN', @level2name = N'DWHEXPDTA';

