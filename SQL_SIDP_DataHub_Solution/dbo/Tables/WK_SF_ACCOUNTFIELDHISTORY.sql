﻿CREATE TABLE [dbo].[WK_SF_ACCOUNTFIELDHISTORY] (
    [Id]            INT            IDENTITY (1, 1) NOT NULL,
    [Id_SF]         NVARCHAR (18)  NOT NULL,
    [CreatedByID]   NVARCHAR (18)  NULL,
    [CreatedDate]   DATETIME2 (7)  NOT NULL,
    [Field]         NVARCHAR (255) NULL,
    [IsDeleted]     [nvarchar](10) NULL,
    [NewValue]      NVARCHAR (MAX) NULL,
    [OldValue]      NVARCHAR (MAX) NULL,
    [AccountId]		NVARCHAR (18)  NULL,
    CONSTRAINT [PK_WK_SF_ACCOUNTFIELDHISTORY] PRIMARY KEY CLUSTERED ([Id] ASC)
)
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID d''historique de compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_ACCOUNTFIELDHISTORY', @level2type = N'COLUMN', @level2name = N'Id_SF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ancienne valeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_ACCOUNTFIELDHISTORY', @level2type = N'COLUMN', @level2name = N'OldValue';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nouvelle valeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_ACCOUNTFIELDHISTORY', @level2type = N'COLUMN', @level2name = N'NewValue';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Supprimé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_ACCOUNTFIELDHISTORY', @level2type = N'COLUMN', @level2name = N'IsDeleted';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de la personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_ACCOUNTFIELDHISTORY', @level2type = N'COLUMN', @level2name = N'AccountId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Champ modifié', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_ACCOUNTFIELDHISTORY', @level2type = N'COLUMN', @level2name = N'Field';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_ACCOUNTFIELDHISTORY', @level2type = N'COLUMN', @level2name = N'CreatedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID créé par', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_ACCOUNTFIELDHISTORY', @level2type = N'COLUMN', @level2name = N'CreatedByID';
GO