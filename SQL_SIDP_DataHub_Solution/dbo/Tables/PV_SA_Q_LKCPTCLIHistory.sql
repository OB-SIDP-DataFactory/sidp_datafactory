﻿CREATE TABLE [dbo].[PV_SA_Q_LKCPTCLIHistory] (
    [Id]                 BIGINT        NOT NULL,
    [DWHGRPDTX]          DATE          NULL,
    [DWHGRPETB]          INT           NULL,
    [DWHGRPCLI]          VARCHAR (7)   NULL,
    [DWHGRPREG]          VARCHAR (7)   NULL,
    [DWHGRPREL]          VARCHAR (3)   NULL,
    [DWHGRPTGR]          VARCHAR (1)   NULL,
    [DWHGRPLGR]          VARCHAR (1)   NULL,
    [DWHGRPRIS]          VARCHAR (1)   NULL,
    [DWHGRPFRE]          VARCHAR (1)   NULL,
    [DWHGRPDSY]          DATE          NULL,
    [Validity_StartDate] DATETIME      NULL,
    [Validity_EndDate]   DATETIME      NULL,
    [Startdt]            DATETIME2 (7) NOT NULL,
    [Enddt]              DATETIME2 (7) NOT NULL
);
GO
CREATE NONCLUSTERED INDEX IX_NC_LKCLIENT_HIS_VLD_DTE
ON dbo.PV_SA_Q_LKCPTCLIHistory  (DWHGRPREG, DWHGRPCLI)
INCLUDE (Validity_StartDate, Validity_EndDate)
GO