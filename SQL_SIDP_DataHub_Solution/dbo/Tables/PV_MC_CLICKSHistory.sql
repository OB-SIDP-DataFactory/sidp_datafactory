﻿CREATE TABLE [dbo].[PV_MC_CLICKSHistory] (
    [Id]                       BIGINT          NOT NULL,
    [ClientID]                 INT             NULL,
    [SendID]                   INT             NULL,
    [SubscriberKey]            NVARCHAR (100)  NULL,
    [EmailAddress]             NVARCHAR (100)  NULL,
    [SubscriberID]             INT             NULL,
    [ListID]                   INT             NULL,
    [EventDate]                DATETIME        NULL,
    [EventType]                NVARCHAR (100)  NULL,
    [SendURLID]                INT             NULL,
    [URLID]                    INT             NULL,
    [URL]                      NVARCHAR (4000) NULL,
    [Alias]                    NVARCHAR (2000) NULL,
    [BatchID]                  NVARCHAR (100)  NULL,
    [TriggeredSendExternalKey] NVARCHAR (101)  NULL,
    [IsUnique]                 NVARCHAR (20)   NULL,
    [Browser]                  NVARCHAR (20)   NULL,
    [EmailClient]              NVARCHAR (100)  NULL,
    [OperatingSystem]          NVARCHAR (100)  NULL,
    [Device]                   NVARCHAR (100)  NULL,
    [Startdt]                  DATETIME2 (7)   NOT NULL,
    [Enddt]                    DATETIME2 (7)   NOT NULL
);
GO

