﻿CREATE TABLE [dbo].[Param_AlimVues] (
    [Id]             INT           IDENTITY (1, 1) NOT NULL,
    [domain]         NVARCHAR (50) NOT NULL,
    [batch_name]     NVARCHAR (50) NOT NULL,
    [last_alim_date] DATETIME      NOT NULL,
    [flag]           NVARCHAR (2)  NOT NULL,
    CONSTRAINT [PK_PARAM_AlimVues] PRIMARY KEY CLUSTERED ([Id] ASC)
)
GO
CREATE NONCLUSTERED INDEX [IX_NC_PAR_ALM_VUE_DOM_BAT_FLG]
ON [dbo].[Param_AlimVues] ([domain], [batch_name], [flag])
INCLUDE ([last_alim_date])
GO