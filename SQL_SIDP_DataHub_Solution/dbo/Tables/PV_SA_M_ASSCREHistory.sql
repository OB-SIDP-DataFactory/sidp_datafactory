﻿CREATE TABLE [dbo].[PV_SA_M_ASSCREHistory] (
    [ID]                 BIGINT          NOT NULL,
    [DWHASSDTX]          DATE            NULL,
    [DWHASSETA]          INT             NULL,
    [DWHASSAGE]          INT             NULL,
    [DWHASSSER]          VARCHAR (2)     NULL,
    [DWHASSSSE]          VARCHAR (2)     NULL,
    [DWHASSOPE]          VARCHAR (6)     NULL,
    [DWHASSNAT]          VARCHAR (6)     NULL,
    [DWHASSNUM]          INT             NULL,
    [DWHASSNUA]          INT             NULL,
    [DWHASSCOA]          VARCHAR (6)     NULL,
    [DWHASSMOA]          DECIMAL (18, 3) NULL,
    [DWHASSDEV]          VARCHAR (3)     NULL,
    [DWHASSTAU]          DECIMAL (14, 9) NULL,
    [DWHASSCLI]          VARCHAR (7)     NULL,
    [DWHASSTYP]          VARCHAR (1)     NULL,
    [Validity_StartDate] DATETIME        NULL,
    [Validity_EndDate]   DATETIME        NULL,
    [Startdt]            DATETIME2 (7)   NOT NULL,
    [Enddt]              DATETIME2 (7)   NOT NULL
);

