﻿CREATE TABLE [dbo].[REF_EMPLOYMENT_CONTRACT] (
    [ID]                        BIGINT         IDENTITY (1, 1) NOT NULL,
    [EMPLOYMENT_CONTRACT_ID]    NUMERIC (19)   NOT NULL,
    [SALESFORCE_CODE]           NVARCHAR (255) NULL,
    [FRANFINANCE_CODE]          NVARCHAR (255) NULL,
    [MAXIMAL_CONTRACT_DURATION] NUMERIC (38)   NULL,
    [CODE]                      NVARCHAR (255) NULL,
    [END_DATE]                  DATETIME       NULL,
    [PRIORITY]                  NUMERIC (19)   NULL,
    [REF_FAMILY_ID]             NUMERIC (19)   NULL,
    [START_DATE]                DATETIME       NULL
);

