﻿CREATE TABLE [dbo].[PV_SA_M_CARVM] (
    [ID]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [DWHCVMIDV]          INT                                         NULL,
    [DWHCVMDDV]          DATE                                        NULL,
    [DWHCVMDFV]          DATE                                        NULL,
    [DWHCVMRVM]          INT                                         NULL,
    [DWHCVMPCT]          INT                                         NULL,
    [DWHCVMPCA]          VARCHAR (3)                                 NULL,
    [DWHCVMISI]          VARCHAR (12)                                NULL,
    [DWHCVMTCN]          VARCHAR (23)                                NULL,
    [DWHCVMDSC]          VARCHAR (32)                                NULL,
    [DWHCVMCVM]          INT                                         NULL,
    [DWHCVMLCV]          VARCHAR (32)                                NULL,
    [DWHCVMTVM]          INT                                         NULL,
    [DWHCVMLCT]          VARCHAR (32)                                NULL,
    [DWHCVMREM]          VARCHAR (7)                                 NULL,
    [DWHCVMLEM]          VARCHAR (32)                                NULL,
    [DWHCVMPEM]          VARCHAR (3)                                 NULL,
    [DWHCVMSEC]          INT                                         NULL,
    [DWHCVMLSE]          VARCHAR (32)                                NULL,
    [DWHCVMSEF]          INT                                         NULL,
    [DWHCVMLEF]          VARCHAR (32)                                NULL,
    [DWHCVMNVM]          VARCHAR (3)                                 NULL,
    [DWHCVMFOR]          INT                                         NULL,
    [DWHCVMLFO]          VARCHAR (32)                                NULL,
    [DWHCVMDVC]          VARCHAR (3)                                 NULL,
    [DWHCVMRBC]          INT                                         NULL,
    [DWHCVMPBC]          VARCHAR (3)                                 NULL,
    [DWHCVMLBC]          VARCHAR (32)                                NULL,
    [DWHCVMMCO]          INT                                         NULL,
    [DWHCVMLMC]          VARCHAR (32)                                NULL,
    [DWHCVMNDM]          DECIMAL (18, 6)                             NULL,
    [DWHCVMDVN]          VARCHAR (3)                                 NULL,
    [DWHCVMNDN]          DECIMAL (18, 6)                             NULL,
    [DWHCVMDEM]          DATE                                        NULL,
    [DWHCVMDVE]          VARCHAR (3)                                 NULL,
    [DWHCVMDEC]          DATE                                        NULL,
    [DWHCVMPEX]          DECIMAL (18, 9)                             NULL,
    [DWHCVMSEN]          VARCHAR (1)                                 NULL,
    [DWHCVMSMT]          INT                                         NULL,
    [DWHCVMMLT]          INT                                         NULL,
    [DWHCVMSJC]          INT                                         NULL,
    [DWHCVMFCO]          VARCHAR (2)                                 NULL,
    [DWHCVMLFC]          VARCHAR (32)                                NULL,
    [DWHCVMDCO]          VARCHAR (3)                                 NULL,
    [DWHCVMDDC]          DATE                                        NULL,
    [DWHCVMDPC]          DATE                                        NULL,
    [DWHCVMTXC]          DECIMAL (14, 9)                             NULL,
    [DWHCVMTXV]          VARCHAR (1)                                 NULL,
    [DWHCVMDIV]          INT                                         NULL,
    [DWHCVMECJ]          VARCHAR (1)                                 NULL,
    [DWHCVMLOC]          VARCHAR (20)                                NULL,
    [DWHCVMREL]          VARCHAR (1)                                 NULL,
    [DWHCVMUNT]          VARCHAR (3)                                 NULL,
    [DWHCVMPEA]          VARCHAR (1)                                 NULL,
    [DWHCVMMIF]          VARCHAR (1)                                 NULL,
    [DWHCVMCMP]          VARCHAR (1)                                 NULL,
    [DWHCVMCTR]          VARCHAR (2)                                 NULL,
    [DWHCVMMIC]          VARCHAR (4)                                 NULL,
    [DWHCVMSBF]          INT                                         NULL,
    [DWHCVMLSB]          VARCHAR (32)                                NULL,
    [DWHCVMNTB]          INT                                         NULL,
    [DWHCVMTRB]          INT                                         NULL,
    [DWHCVMNIB]          VARCHAR (2)                                 NULL,
    [DWHCVMQEM]          INT                                         NULL,
    [DWHCVMLQM]          VARCHAR (32)                                NULL,
    [DWHCVMCSR]          INT                                         NULL,
    [DWHCVMNPE]          INT                                         NULL,
    [DWHCVMLPE]          VARCHAR (32)                                NULL,
    [DWHCVMERS]          VARCHAR (1)                                 NULL,
    [DWHCVMPEE]          VARCHAR (1)                                 NULL,
    [DWHCVMCOT]          VARCHAR (1)                                 NULL,
    [DWHCVMNOM]          DECIMAL (15, 2)                             NULL,
    [DWHCVMCSI]          VARCHAR (7)                                 NULL,
    [DWHCVMRGA]          VARCHAR (7)                                 NULL,
    [DWHCVMCOC]          VARCHAR (9)                                 NULL,
    [DWHCVMQUA]          INT                                         NULL,
    [DWHCVMLUA]          VARCHAR (32)                                NULL,
    [DWHCVMCRD]          VARCHAR (1)                                 NULL,
    [DWHCVMDUI]          INT                                         NULL,
    [DWHCVMCOL]          VARCHAR (1)                                 NULL,
    [DWHCVMCLO]          DATE                                        NULL,
    [DWHCVMCAT]          VARCHAR (1)                                 NULL,
    [DWHCVMCIR]          VARCHAR (1)                                 NULL,
    [DWHCVMIND]          VARCHAR (6)                                 NULL,
    [DWHCVMLIN]          VARCHAR (32)                                NULL
 );
 
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIBELLE INDICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMLIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RÉFÉRENCE INDICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMIND';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COUPON IRRÉGULIER', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMCIR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COUPON ATTACHÉ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMCAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DE CLÔTURE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMCLO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'VALEUR COLLATÉRALIS.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMCOL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DURÉE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMDUI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COD RÉSIDENCE DÉCLAR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMCRD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIBELLE QUALITÉ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMLUA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'QUALITÉ DE TITRE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMQUA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COMMON CODE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMCOC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE RGA.RCA', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMRGA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE SICOVAM', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMCSI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NOMINAL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMNOM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'VALEUR COTÉE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMCOT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COD ÉMETTEUR ENUM', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMPEE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COD ÉMETTEUR RÉSIDEN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMERS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIB NAT PAYS ÉMETTEU', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMLPE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NAT PAYS ÉMETTEUR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMNPE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COD SECTEUR EMETTEUR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMCSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIB AGENT ÉCONOMIQUE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMLQM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'QUAL ÉMET-AGENT ÉCO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMQEM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NATURE INSTRUMENT CB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMNIB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE REVENU BAFI', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMTRB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NATURE TITRE BAFI', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMNTB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIBEL DU SECT BDF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMLSB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SECT INSTITUT BDF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMSBF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE MIC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMMIC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CAT DE RISQUE (MIF)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMCTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COMPLEXE (MIF)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMCMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ELIGIBILITÉ MIF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMMIF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ELIGIBILITÉ PEA', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMPEA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'UNITÉ QTITÉ. RELIT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMUNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ADMISSION RELIT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMREL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE LOCAL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMLOC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ECART DE JOUR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMECJ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DIVISEUR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMDIV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TX VARIABLE COUPON', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMTXV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TAUX DU COUPON', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMTXC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DT PROCHAIN COUPON', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMDPC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DT DERNIER COUPON', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMDDC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEVISE COUPON ISO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMDCO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIB FRÉQU. COUPON', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMLFC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FRÉQUENCE COUPON', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMFCO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RACINE VM SS-JACENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMSJC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MULTIPLE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMMLT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SOUS-MULTIPLE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMSMT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SENS OPTION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMSEN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PRIX EXERCICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMPEX';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ÉCHÉANCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMDEC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEVISE ÉMISSION ISO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMDVE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ÉMISSION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMDEM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NOMINAL DE NÉGOCIAT°', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMNDN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEV NOM. MARCHÉ ISO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMDVN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NOMINAL DE MARCHÉ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMNDM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIB. MODE COMPTABI.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMLMC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MODE COMPTABILISAT°', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMMCO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIBELLÉ BOURSE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMLBC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PAYS BOURSE COTAT°', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMPBC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RACINE BOURSE COTAT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMRBC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEVISE COTATION ISO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMDVC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIBELLÉ FORME', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMLFO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FORME', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMFOR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NATIONALITÉ VM', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMNVM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIB SECT. ÉCO. FTSE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMLEF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SECTEUR ÉCONO. FTSE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMSEF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIB. SECTEUR ÉCONO.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMLSE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SECTEUR ÉCONOMIQUE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMSEC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PAYS ÉMISSION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMPEM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIBELLÉ EMETTEUR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMLEM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RACINE EMETTEUR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMREM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIBELLÉ TYPE VM', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMLCT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE TYPE VM', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMTVM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIBELLÉ CLASSE VM', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMLCV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE CLASSE VM', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMCVM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DESCRIPTION VM', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMDSC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE MNÉMONIQUE TCN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMTCN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE ISIN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMISI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PLACE COTAT° ALPHA', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMPCA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PLACE DE COTATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMPCT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RACINE VALEUR SAMIC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMRVM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE FIN VALIDITE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMDFV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DÉBUT VALIDITÉ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMDDV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID. VALEUR MOBILIERE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARVM', @level2type = N'COLUMN', @level2name = N'DWHCVMIDV';

