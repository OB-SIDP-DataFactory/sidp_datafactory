﻿CREATE TABLE [dbo].[PV_SA_M_PROOP] (
    [ID]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [DWHPRODTX]          DATE                                        NOT NULL,
    [DWHPROETA]          INT                                         NOT NULL,
    [DWHPROAGE]          INT                                         NOT NULL,
    [DWHPROSER]          VARCHAR (2)                                 NOT NULL,
    [DWHPROSSE]          VARCHAR (2)                                 NOT NULL,
    [DWHPROOPE]          VARCHAR (6)                                 NOT NULL,
    [DWHPRONAT]          VARCHAR (6)                                 NOT NULL,
    [DWHPRONUM]          INT                                         NOT NULL,
    [DWHPRODEV]          VARCHAR (3)                                 NULL,
    [DWHPROMOP]          DECIMAL (17, 2)                             NULL,
    [DWHPROMOB]          DECIMAL (17, 2)                             NULL,
    [DWHPRODOT]          DECIMAL (17, 2)                             NULL,
    [DWHPRODOB]          DECIMAL (17, 2)                             NULL,
    [DWHPROREP]          DECIMAL (17, 2)                             NULL,
    [DWHPROREB]          DECIMAL (17, 2)                             NULL,
    [DWHPROPER]          DECIMAL (17, 2)                             NULL,
    [DWHPROPEB]          DECIMAL (17, 2)                             NULL,
    [DWHPROSTP]          DECIMAL (17, 2)                             NULL,
    [DWHPROSTB]          DECIMAL (17, 2)                             NULL,
    [DWHPROSRE]          DECIMAL (17, 2)                             NULL,
    [DWHPROSRB]          DECIMAL (17, 2)                             NULL,
    [DWHPROMIO]          DECIMAL (17, 2)                             NULL,
    [DWHPROMIB]          DECIMAL (17, 2)                             NULL,
    [DWHPROORG]          VARCHAR (1)                                 NULL,
    [DWHPRORIS]          VARCHAR (1)                                 NULL,
    [DWHPROFRE]          VARCHAR (1)                                 NULL,
    [DWHPRODOM]          VARCHAR (12)                                NULL,
    [Validity_StartDate] DATETIME                                    NULL,
    [Validity_EndDate]   DATETIME                                    DEFAULT ('99991231') NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SA_M_PROOP] PRIMARY KEY CLUSTERED ([ID] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SA_M_PROOPHistory], DATA_CONSISTENCY_CHECK=ON));
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DOMAINE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROOP', @level2type = N'COLUMN', @level2name = N'DWHPRODOM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FREQUENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROOP', @level2type = N'COLUMN', @level2name = N'DWHPROFRE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PÉRIMÈTRE RISQUE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROOP', @level2type = N'COLUMN', @level2name = N'DWHPRORIS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ORIGINE PROVISION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROOP', @level2type = N'COLUMN', @level2name = N'DWHPROORG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CTVL PROVISIONS IAS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROOP', @level2type = N'COLUMN', @level2name = N'DWHPROMIB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT PROVISIONS IAS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROOP', @level2type = N'COLUMN', @level2name = N'DWHPROMIO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CTVL STOCK REPRISE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROOP', @level2type = N'COLUMN', @level2name = N'DWHPROSRB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT STOCK DE REPRISE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROOP', @level2type = N'COLUMN', @level2name = N'DWHPROSRE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CTVL MT STCK PRO/DOT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROOP', @level2type = N'COLUMN', @level2name = N'DWHPROSTB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT STOCK PROV/DOT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROOP', @level2type = N'COLUMN', @level2name = N'DWHPROSTP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT DE PERTES', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROOP', @level2type = N'COLUMN', @level2name = N'DWHPROPEB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT PERTE PROVISION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROOP', @level2type = N'COLUMN', @level2name = N'DWHPROPER';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT REPRISE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROOP', @level2type = N'COLUMN', @level2name = N'DWHPROREB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT REPRISE PROVISION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROOP', @level2type = N'COLUMN', @level2name = N'DWHPROREP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT DOTATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROOP', @level2type = N'COLUMN', @level2name = N'DWHPRODOB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT DOTAT° PROVISION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROOP', @level2type = N'COLUMN', @level2name = N'DWHPRODOT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT PROVISION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROOP', @level2type = N'COLUMN', @level2name = N'DWHPROMOB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT PROVISION SPÉCIF.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROOP', @level2type = N'COLUMN', @level2name = N'DWHPROMOP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEVISE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROOP', @level2type = N'COLUMN', @level2name = N'DWHPRODEV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMERO OPERATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROOP', @level2type = N'COLUMN', @level2name = N'DWHPRONUM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NATURE OPERATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROOP', @level2type = N'COLUMN', @level2name = N'DWHPRONAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE OPERATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROOP', @level2type = N'COLUMN', @level2name = N'DWHPROOPE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SOUS-SERVICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROOP', @level2type = N'COLUMN', @level2name = N'DWHPROSSE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SERVICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROOP', @level2type = N'COLUMN', @level2name = N'DWHPROSER';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AGENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROOP', @level2type = N'COLUMN', @level2name = N'DWHPROAGE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ETABLISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROOP', @level2type = N'COLUMN', @level2name = N'DWHPROETA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE EXTRACTION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROOP', @level2type = N'COLUMN', @level2name = N'DWHPRODTX';

