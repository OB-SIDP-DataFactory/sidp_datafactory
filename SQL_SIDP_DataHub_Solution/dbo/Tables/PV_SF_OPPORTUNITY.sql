﻿CREATE TABLE [dbo].[PV_SF_OPPORTUNITY] (
    [Id]                                 INT             IDENTITY (1, 1) NOT NULL,
    [Id_SF]                              NVARCHAR (18)   NOT NULL,
    [IsDeleted]                          NVARCHAR (10)   NULL,
    [AccountId]                          NVARCHAR (18)   NULL,
    [RecordTypeId]                       NVARCHAR (18)   NULL,
    [IsPrivate]                          NVARCHAR (10)   NULL,
    [Name]                               NVARCHAR (120)  NULL,
    [StageName]                          NVARCHAR (40)   NULL,
    [Amount]                             DECIMAL (18, 2) NULL,
    [Probability]                        DECIMAL (18, 2) NULL,
    [ExpectedRevenue]                    DECIMAL (18, 2) NULL,
    [TotalOpportunityQuantity]           DECIMAL (18, 2) NULL,
    [CloseDate]                          DATE            NULL,
    [Type]                               NVARCHAR (40)   NULL,
    [NextStep]                           NVARCHAR (255)  NULL,
    [LeadSource]                         NVARCHAR (40)   NULL,
    [IsClosed]                           NVARCHAR (10)   NULL,
    [IsWon]                              NVARCHAR (10)   NULL,
    [ForecastCategory]                   NVARCHAR (40)   NULL,
    [ForecastCategoryName]               NVARCHAR (40)   NULL,
    [CampaignId]                         NVARCHAR (18)   NULL,
    [HasOpportunityLineItem]             NVARCHAR (10)   NULL,
    [Pricebook2Id]                       NVARCHAR (18)   NULL,
    [OwnerId]                            NVARCHAR (18)   NULL,
    [CreatedDate]                        DATETIME        NULL,
    [CreatedById]                        NVARCHAR (18)   NULL,
    [LastModifiedDate]                   DATETIME        NULL,
    [LastModifiedById]                   NVARCHAR (18)   NULL,
    [SystemModstamp]                     DATETIME        NULL,
    [LastActivityDate]                   DATE            NULL,
    [FiscalQuarter]                      INT             NULL,
    [FiscalYear]                         INT             NULL,
    [Fiscal]                             NVARCHAR (6)    NULL,
    [LastViewedDate]                     DATETIME        NULL,
    [LastReferencedDate]                 DATETIME        NULL,
    [ContractId]                         NVARCHAR (18)   NULL,
    [HasOpenActivity]                    NVARCHAR (10)   NULL,
    [HasOverdueTask]                     NVARCHAR (10)   NULL,
    [AdvisorCode__c]                     NVARCHAR (80)   NULL,
    [TCHCompleteFile__c]                 NVARCHAR (10)   NULL,
    [CommercialOfferCode__c]             NVARCHAR (80)   NULL,
    [CommercialOfferName__c]             NVARCHAR (80)   NULL,
    [DeniedOpportunityReason__c]         NVARCHAR (255)  NULL,
    [CampaignId__c]                      NVARCHAR (80)   NULL,
    [TCHFromMarketingCase__c]            NVARCHAR (10)   NULL,
    [realizedBy__c]                      NVARCHAR (50)   NULL,
    [FinalizedBy__c]                     NVARCHAR (18)   NULL,
    [FormValidation__c]                  DATETIME        NULL,
    [IDOppty__c]                         NVARCHAR (30)   NULL,
    [Indication__c]                      NVARCHAR (80)   NULL,
    [NoIndication__c]                    NVARCHAR (80)   NULL,
    [OriginalEvent__c]                   NVARCHAR (255)  NULL,
    [PointOfSaleCode__c]                 NVARCHAR (80)   NULL,
    [ProductFamilyCode__c]               NVARCHAR (80)   NULL,
    [ProductFamilyName__c]               NVARCHAR (80)   NULL,
    [RejectReason__c]                    NVARCHAR (255)  NULL,
    [SendingInformations__c]             DATETIME        NULL,
    [SignatureDate__c]                   DATE            NULL,
    [TCHNotifDuplicateProspect__c]       NVARCHAR (10)   NULL,
    [FolderAlias__c]                     NVARCHAR (36)   NULL,
    [DistributionChannel__c]             NVARCHAR (255)  NULL,
    [IDProcessSous__c]                   NVARCHAR (80)   NULL,
    [RecoveryLink__c]                    NVARCHAR (1300) NULL,
    [TCHExpiredIndication__c]            NVARCHAR (10)   NULL,
    [TCHExpiredSubscription__c]          NVARCHAR (10)   NULL,
    [TCHManualAnalysis__c]               NVARCHAR (10)   NULL,
    [TCHOppCreation__c]                  NVARCHAR (10)   NULL,
    [ToBeDeleted__c]                     NVARCHAR (10)   NULL,
    [IdSource__c]                        NVARCHAR (80)   NULL,
    [Manager__c]                         NVARCHAR (18)   NULL,
    [ProvenanceIndicationIndicatorId__c] NVARCHAR (80)   NULL,
    [RelationEntryScore__c]              NVARCHAR (255)  NULL,
    [StartedChannelForIndication__c]     NVARCHAR (255)  NULL,
    [StartedChannel__c]                  NVARCHAR (255)  NULL,
    [DistributorNetwork__c]              NVARCHAR (255)  NULL,
    [DistributorEntity__c]               NVARCHAR (255)  NULL,
    [TCHId__c]                           NVARCHAR (18)   NULL,
    [CompleteFileFlag__c]                NVARCHAR (255)  NULL,
    [ContractNumberDistrib__c]           NVARCHAR (80)   NULL,
    [OptInTelcoData__c]                  NVARCHAR (10)   NULL,
    [SubscriberHolder__c]                NVARCHAR (10)   NULL,
    [CreditAmount__c]                    DECIMAL (18, 2) NULL,
    [CustomerAdviserId__c]               NVARCHAR (80)   NULL,
    [DebitDay__c]                        NVARCHAR (255)  NULL,
    [DepositAmount__c]                   DECIMAL (18, 2) NULL,
    [Derogation__c]                      NVARCHAR (255)  NULL,
    [FFinalDecision__c]                  NVARCHAR (1300) NULL,
    [FPreScoreColor__c]                  NVARCHAR (1300) NULL,
    [FirstDueDate__c]                    NVARCHAR (255)  NULL,
    [ForbiddenWords__c]                  NVARCHAR (255)  NULL,
    [FundingSubject__c]                  NVARCHAR (255)  NULL,
    [HasForbiddenWord__c]                NVARCHAR (10)   NULL,
    [IncompleteFileReason__c]            NVARCHAR (4000) NULL,
    [LoanAcceptationDate__c]             DATETIME        NULL,
    [ObjectInApproval__c]                NVARCHAR (10)   NULL,
    [OffersRate__c]                      DECIMAL (18, 2) NULL,
    [PrescriberContact__c]               NVARCHAR (18)   NULL,
    [ReleaseAmount__c]                   DECIMAL (18, 2) NULL,
    [ReleaseDate__c]                     DATETIME        NULL,
    [StartDate__c]                       DATE            NULL,
    [SubscribedAmount__c]                DECIMAL (18, 2) NULL,
    [TCHCategory__c]                     NVARCHAR (1300) NULL,
    [TracingProduct__c]                  NVARCHAR (255)  NULL,
    [RetractationDate__c]                DATE            NULL,
    [OriginDistributionChannel__c]       NVARCHAR (255)  NULL,
    [isSynchronized__c]                  NVARCHAR (10)   NULL,
    [Description]                        NVARCHAR (4000) NULL,
    [EndingChannel__c]                   NVARCHAR (255)  NULL,
    [CheckGo__c]                         NVARCHAR (10)   NULL,
    [IsClientOF__c]                      NVARCHAR (10)   NULL,
    [OptInOrderTelco__c]                 NVARCHAR (10)   NULL,
    [ApprovalInitiator__c]               NVARCHAR (18)   NULL,
    [CommitmentRefusalReason__c]         NVARCHAR (255)  NULL,
    [DebtRate__c]                        DECIMAL (18, 2) NULL,
    [ManagerApproval__c]                 DATE            NULL,
    [PreScoreCode__c]                    NVARCHAR (80)   NULL,
    [PreScoreReason__c]                  NVARCHAR (4000) NULL,
    [TCHFinalDecision__c]                NVARCHAR (255)  NULL,
    [TCHPreScoreColor__c]                NVARCHAR (255)  NULL,
    [RTDN__c]                            NVARCHAR (1300) NULL,
    [TCH_PTD__c]                         NVARCHAR (10)   NULL,
    [Commentthread__c]                   NVARCHAR (MAX)  NULL,
    [coBorrowerInsurance__c]             NVARCHAR (10)   NULL,
    [mainBorrowerInsurance__c]           NVARCHAR (10)   NULL,
    [TCH_OAV_Granted__c]                 NVARCHAR (10)   NULL,
    [CampaignCode__c]                    NVARCHAR (MAX)  NULL,
    [CreationLeadDate__c]                DATE            NULL,
    [DateOfFirstVisit__c]                DATE            NULL,
    [EventCode__c]                       NVARCHAR (MAX)  NULL,
    [LeadChannel__c]                     NVARCHAR (255)  NULL,
    [LeadID__c]                          NVARCHAR (20)   NULL,
    [LeadProductFamily__c]               NVARCHAR (255)  NULL,
    [NumberOfVisits__c]                  DECIMAL (18, 2) NULL,
    [OldLead__c]                         NVARCHAR (10)   NULL,
    [ContactId]                          NVARCHAR (18)   NULL,
    [TCHInitDistribChannel__c]           NVARCHAR (2)    NULL,
    [RecoveryLinkRIO__c]                 NVARCHAR (255)  NULL,
	[OptinPreattTelco__c]                NVARCHAR (10)	 NULL,
    [Validity_StartDate]                 DATETIME2 (7)   NOT NULL,
    [Validity_EndDate]                   DATETIME2 (7)   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SF_OPPORTUNITY] PRIMARY KEY CLUSTERED ([Id] ASC)
)
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type d''opportunité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'Type'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Horodateur des modifications du système', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'SystemModstamp'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Canal d''interaction d''origine', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'StartedChannel__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Étape', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'StageName'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Score d''entrée en relation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'RelationEntryScore__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motif sans suite', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'RejectReason__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID du type d''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'RecordTypeId'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Indicateur indication digitale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'ProvenanceIndicationIndicatorId__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom famille produit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'ProductFamilyName__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code famille produit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'ProductFamilyCode__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Point de vente', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'PointOfSaleCode__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID du propriétaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'OwnerId'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Id process indication', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'NoIndication__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Responsable d''entité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'Manager__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Canal d''interaction', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'LeadSource'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'LastModifiedDate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gagnée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'IsWon'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Indication', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'Indication__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Source', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'IdSource__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Id process souscription', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'IDProcessSous__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de l''Opportunité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'IDOppty__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de l''opportunité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'Id_SF'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Validation du formulaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'FormValidation__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Finalisé par', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'FinalizedBy__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Réseau Distributeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'DistributorNetwork__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Sous-réseau', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'DistributorEntity__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Canal de distribution', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'DistributionChannel__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motif Affaire refusée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'DeniedOpportunityReason__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'CreatedDate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID créé par', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'CreatedById'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dossier Complet', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'CompleteFileFlag__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom de l’offre commerciale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'CommercialOfferName__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code de l’offre commerciale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'CommercialOfferCode__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de clôture', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'CloseDate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Campagne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'CampaignId__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Conseiller', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'AdvisorCode__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom de la personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'AccountId'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'A effacer', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'ToBeDeleted__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Canal d''intéraction d''origine indication', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'StartedChannelForIndication__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de signature', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'SignatureDate__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Réalisé par', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'realizedBy__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'Name'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Produit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'TracingProduct__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Quantité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'TotalOpportunityQuantity'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nb de création d''opportunités', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'TCHOppCreation__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TCHNotifDuplicateProspect', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'TCHNotifDuplicateProspect__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nb d''analyse manuelle', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'TCHManualAnalysis__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TCHId', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'TCHId__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TCHFromMarketingCase', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'TCHFromMarketingCase__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nb de souscriptions expirées', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'TCHExpiredSubscription__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nb d''indications expirées', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'TCHExpiredIndication__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TCHCompleteFile', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'TCHCompleteFile__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Categorie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'TCHCategory__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Souscripteur/Titulaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'SubscriberHolder__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant souscrit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'SubscribedAmount__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de démarrage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'StartDate__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Envoi des informations', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'SendingInformations__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de la rétractation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'RetractationDate__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date du déblocage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'ReleaseDate__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant débloqué', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'ReleaseAmount__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Lien de reprise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'RecoveryLink__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Probabilité (%)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'Probability'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID du catalogue de prix', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'Pricebook2Id'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prescripteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'PrescriberContact__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Événement d''origine', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'OriginalEvent__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Opt-in données Telco', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'OptInTelcoData__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux de l''offre', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'OffersRate__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Entité en approbation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'ObjectInApproval__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Étape suivante', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'NextStep'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de l''accord du crédit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'LoanAcceptationDate__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernier affichage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'LastViewedDate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernière date référencée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'LastReferencedDate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernière modification par ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'LastModifiedById'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernière activité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'LastActivityDate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Privé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'IsPrivate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Supprimé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'IsDeleted'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fermée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'IsClosed'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motif Dossier Incomplet', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'IncompleteFileReason__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Inclut une tâche en retard', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'HasOverdueTask'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ligne de commande attribuée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'HasOpportunityLineItem'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Inclut une activité en cours', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'HasOpenActivity'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Contient Mot(s) Interdit(s)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'HasForbiddenWord__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Objet de financement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'FundingSubject__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Couleur pré-score', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'FPreScoreColor__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Catégorie de prévision', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'ForecastCategoryName'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Catégorie de prévision', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'ForecastCategory'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mots interdits', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'ForbiddenWords__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Alias du dossier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'FolderAlias__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Année fiscale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'FiscalYear'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Trimestre fiscal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'FiscalQuarter'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type d''exercice', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'Fiscal'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'1ère échéance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'FirstDueDate__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Décision finale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'FFinalDecision__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant prévu', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'ExpectedRevenue'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dérogation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'Derogation__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant versé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'DepositAmount__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Jour de prélèvement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'DebitDay__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant conseiller', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'CustomerAdviserId__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant du prêt', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'CreditAmount__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° contrat distributeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'ContractNumberDistrib__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID du contrat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'ContractId'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de campagne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'CampaignId'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'Amount'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Canal de distribution d''origine', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'OriginDistributionChannel__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Synchroniser', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'isSynchronized__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TCH Couleur pré-score', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'TCHPreScoreColor__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TCH Décision finale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'TCHFinalDecision__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TCH PushTopic Desirio', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'TCH_PTD__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Record Type Developer Name', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'RTDN__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motif Pré-score', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'PreScoreReason__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code pré-score', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'PreScoreCode__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Opt-in facture Telco', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'OptInOrderTelco__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Approbation manager', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'ManagerApproval__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Client OF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'IsClientOF__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Canal d''interaction de finalisation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'EndingChannel__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Commentaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'Description'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux d''endettement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'DebtRate__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motif Refus Engagement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'CommitmentRefusalReason__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Enrôlement Check&Go', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'CheckGo__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Soumetteur de l''approbation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'ApprovalInitiator__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TCHOAVGranted', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'TCH_OAV_Granted__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Assurance emprunteur principal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'mainBorrowerInsurance__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fil des commentaires', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'Commentthread__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Assurance co-emprunteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'coBorrowerInsurance__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TCHInitDistribChannel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'TCHInitDistribChannel__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ancien Lead', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'OldLead__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre de visites', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'NumberOfVisits__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Famille Produit Lead', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'LeadProductFamily__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID Lead', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'LeadID__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Canal d''interaction Lead', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'LeadChannel__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Salon', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'EventCode__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de première visite', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'DateOfFirstVisit__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création du Lead', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'CreationLeadDate__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID du contact', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'ContactId'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Campagne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'CampaignCode__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Lien de reprise RIO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'RecoveryLinkRIO__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Opt-in Préattribution Telco', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_OPPORTUNITY', @level2type = N'COLUMN', @level2name = N'OptinPreattTelco__c';
GO
CREATE NONCLUSTERED INDEX IX_NC_OPP_ACC_COM_OFF_COD_ID
ON [dbo].[PV_SF_OPPORTUNITY] ([CommercialOfferCode__c])
INCLUDE ([Id_SF])
GO
CREATE NONCLUSTERED INDEX [IX_NC_OPP_ACC_COM_OFF]
ON [dbo].[PV_SF_OPPORTUNITY]([AccountId] ASC, [CommercialOfferName__c] ASC)
INCLUDE([StageName], [IsWon], [Indication__c], [OriginalEvent__c], [IDProcessSous__c], [StartedChannel__c], [DistributorNetwork__c], [DistributorEntity__c], [CompleteFileFlag__c])
GO
CREATE NONCLUSTERED INDEX [IX_NC_OPP_CLO_DTE_WON_COM_OFF]
ON [dbo].[PV_SF_OPPORTUNITY]([CloseDate] ASC, [IsWon] ASC, [CommercialOfferCode__c] ASC)
INCLUDE([RecordTypeId], [StageName], [Type], [LeadSource], [DeniedOpportunityReason__c], [Indication__c], [OriginalEvent__c], [RejectReason__c], [DistributionChannel__c], [IDProcessSous__c], [RelationEntryScore__c], [StartedChannel__c], [DistributorNetwork__c], [DistributorEntity__c], [CompleteFileFlag__c], [OriginDistributionChannel__c])
GO
CREATE NONCLUSTERED INDEX [IX_NC_OPP_COM_OFF_COD]
ON [dbo].[PV_SF_OPPORTUNITY]([CommercialOfferCode__c] ASC)
INCLUDE([RecordTypeId], [StageName], [CloseDate], [Type], [LeadSource], [CreatedDate], [AdvisorCode__c], [DeniedOpportunityReason__c], [IDOppty__c], [Indication__c], [OriginalEvent__c], [PointOfSaleCode__c], [RejectReason__c], [DistributionChannel__c], [RelationEntryScore__c], [StartedChannel__c], [DistributorNetwork__c], [DistributorEntity__c], [CompleteFileFlag__c], [OriginDistributionChannel__c])
GO
CREATE NONCLUSTERED INDEX [IX_NC_OPP_COM_OFF_NAM]
ON [dbo].[PV_SF_OPPORTUNITY]([CommercialOfferName__c] ASC)
INCLUDE([AccountId], [StageName], [IsWon], [Indication__c], [OriginalEvent__c], [IDProcessSous__c], [StartedChannel__c], [DistributorNetwork__c], [DistributorEntity__c], [CompleteFileFlag__c])
GO
CREATE NONCLUSTERED INDEX [IX_NC_OPP_ID_SF]
ON [dbo].[PV_SF_OPPORTUNITY]([Id_SF] ASC)
INCLUDE([Indication__c], [DistributionChannel__c], [StartedChannelForIndication__c], [StartedChannel__c], [DistributorNetwork__c])
GO
CREATE NONCLUSTERED INDEX [IX_NC_OPP_STG_COM_OFF]
ON [dbo].[PV_SF_OPPORTUNITY]([StageName] ASC, [CommercialOfferCode__c] ASC)
INCLUDE([RecordTypeId], [CloseDate], [Type], [LeadSource], [CreatedDate], [AdvisorCode__c], [DeniedOpportunityReason__c], [IDOppty__c], [Indication__c], [OriginalEvent__c], [PointOfSaleCode__c], [RejectReason__c], [DistributionChannel__c], [RelationEntryScore__c], [StartedChannel__c], [DistributorNetwork__c], [DistributorEntity__c], [CompleteFileFlag__c], [OriginDistributionChannel__c])
GO
CREATE NONCLUSTERED INDEX [IX_NC_OPP_WON_COM_OFF_CLO_DTE_SF_ACC]
ON [dbo].[PV_SF_OPPORTUNITY]([IsWon] ASC, [CommercialOfferCode__c] ASC, [CloseDate] ASC)
INCLUDE([Id_SF], [AccountId], [RecordTypeId], [StageName], [Type], [LeadSource], [DeniedOpportunityReason__c], [IDOppty__c], [Indication__c], [OriginalEvent__c], [RejectReason__c], [DistributionChannel__c], [IDProcessSous__c], [RelationEntryScore__c], [StartedChannel__c], [DistributorNetwork__c], [DistributorEntity__c], [CompleteFileFlag__c], [OriginDistributionChannel__c])
GO
CREATE NONCLUSTERED INDEX [IX_NC_OPP_WON_COM_OFF_CLO_DTE_SF_REC]
ON [dbo].[PV_SF_OPPORTUNITY]([IsWon] ASC, [CommercialOfferCode__c] ASC, [CloseDate] ASC)
INCLUDE([Id_SF], [RecordTypeId], [StageName], [Type], [LeadSource], [DeniedOpportunityReason__c], [IDOppty__c], [Indication__c], [OriginalEvent__c], [RejectReason__c], [DistributionChannel__c], [IDProcessSous__c], [RelationEntryScore__c], [StartedChannel__c], [DistributorNetwork__c], [DistributorEntity__c], [CompleteFileFlag__c], [OptInTelcoData__c], [OriginDistributionChannel__c])
GO
CREATE NONCLUSTERED INDEX [IX_NC_OPP_WON_COM_OFF_DIS_CLO_DTE_REC]
ON [dbo].[PV_SF_OPPORTUNITY]([IsWon] ASC, [CommercialOfferCode__c] ASC, [DistributorNetwork__c] ASC, [CloseDate] ASC)
INCLUDE([RecordTypeId], [StageName], [Type], [LeadSource], [CreatedDate], [DeniedOpportunityReason__c], [Indication__c], [OriginalEvent__c], [RejectReason__c], [DistributionChannel__c], [IDProcessSous__c], [RelationEntryScore__c], [StartedChannel__c], [DistributorEntity__c], [CompleteFileFlag__c], [OriginDistributionChannel__c])
GO