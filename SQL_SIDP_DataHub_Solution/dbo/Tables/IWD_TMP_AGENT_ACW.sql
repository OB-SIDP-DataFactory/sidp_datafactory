﻿CREATE TABLE [dbo].[IWD_TMP_AGENT_ACW] (
    [PARTYID]      VARCHAR (50) NOT NULL,
    [PARTYGUID]    VARCHAR (50) NULL,
    [RESOURCE_KEY] INT          NOT NULL,
    [ACW_DURATION] INT          NULL,
    [START_TS]     INT          NULL
);

