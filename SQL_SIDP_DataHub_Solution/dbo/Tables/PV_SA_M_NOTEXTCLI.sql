﻿CREATE TABLE [dbo].[PV_SA_M_NOTEXTCLI] (
    [ID]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [DWHNEXDTX]          DATE                                        NULL,
    [DWHNEXETB]          INT                                         NULL,
    [DWHNEXCLI]          VARCHAR (7)                                 NULL,
    [DWHNEXNOT]          VARCHAR (7)                                 NULL,
    [DWHNEXTYP]          VARCHAR (1)                                 NULL,
    [DWHNEXDAT]          DATE                                        NULL,
    [DWHNEXLON]          VARCHAR (6)                                 NULL,
    [DWHNEXCOU]          VARCHAR (6)                                 NULL,
    [DWHNEXFRE]          VARCHAR (1)                                 NULL,
    [DWHNEXDSY]          DATE                                        NULL,
    [Validity_StartDate] DATETIME                                    NULL,
    [Validity_EndDate]   DATETIME                                    DEFAULT ('99991231') NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SA_M_NOTEXTCLI] PRIMARY KEY CLUSTERED ([ID] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SA_M_NOTEXTCLIHistory], DATA_CONSISTENCY_CHECK=ON));
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE SYSTEME', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_NOTEXTCLI', @level2type = N'COLUMN', @level2name = N'DWHNEXDSY';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FRÉQUENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_NOTEXTCLI', @level2type = N'COLUMN', @level2name = N'DWHNEXFRE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NOTAT° COURT TERME', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_NOTEXTCLI', @level2type = N'COLUMN', @level2name = N'DWHNEXCOU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NOTAT° LONG TERME', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_NOTEXTCLI', @level2type = N'COLUMN', @level2name = N'DWHNEXLON';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DE NOTATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_NOTEXTCLI', @level2type = N'COLUMN', @level2name = N'DWHNEXDAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE DE LA NOTATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_NOTEXTCLI', @level2type = N'COLUMN', @level2name = N'DWHNEXTYP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ENTITE DE NOTATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_NOTEXTCLI', @level2type = N'COLUMN', @level2name = N'DWHNEXNOT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMERO CLIENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_NOTEXTCLI', @level2type = N'COLUMN', @level2name = N'DWHNEXCLI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE ETABLISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_NOTEXTCLI', @level2type = N'COLUMN', @level2name = N'DWHNEXETB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ANALYSE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_NOTEXTCLI', @level2type = N'COLUMN', @level2name = N'DWHNEXDTX';

