﻿CREATE TABLE [dbo].[REF_MOTIFS_DE_RESILIATION] (
    [ID]                                       BIGINT       IDENTITY (1, 1) NOT NULL,
    [MOTIFS_DE_RESILIATION_CODE_ETABLISSEMENT] INT          NULL,
    [MOTIFS_DE_RESILIATION_CODE_MOTIFS]        VARCHAR (6)  NULL,
    [MOTIFS_DE_RESILIATION_ABREGE]             VARCHAR (12) NULL,
    [MOTIFS_DE_RESILIATION_LIBELLE]            VARCHAR (30) NULL,
    CONSTRAINT [PK_REF_MOTIFS_DE_RESILIATION] PRIMARY KEY CLUSTERED ([ID] ASC)
);

