﻿CREATE TABLE [dbo].[WK_SA_M_INCIDENT] (
    [DWHFIMDTX] DATE            NULL,
    [DWHFIMETA] INT             NULL,
    [DWHFIMTYP] VARCHAR (2)     NULL,
    [DWHFIMNUM] INT             NULL,
    [DWHFIMCLT] VARCHAR (7)     NULL,
    [DWHFIMPLA] INT             NULL,
    [DWHFIMCPT] VARCHAR (20)    NULL,
    [DWHFIMNUC] INT             NULL,
    [DWHFIMSEQ] INT             NULL,
    [DWHFIMEVE] DATE            NULL,
    [DWHFIMDEV] VARCHAR (3)     NULL,
    [DWHFIMMON] DECIMAL (18, 3) NULL,
    [DWHFIMMOB] DECIMAL (18, 3) NULL
);

