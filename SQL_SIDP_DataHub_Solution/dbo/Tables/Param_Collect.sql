﻿CREATE TABLE [dbo].[Param_Collect] (
    [Id]                   INT           IDENTITY (1, 1) NOT NULL,
    [domain]               NVARCHAR (50) NOT NULL,
    [batch_name]           NVARCHAR (50) NOT NULL,
    [last_extraction_date] DATETIME      NOT NULL,
    [flag]                 NVARCHAR (2)  NOT NULL,
    CONSTRAINT [PK_PARAM_Collect] PRIMARY KEY CLUSTERED ([Id] ASC)
)
GO
CREATE NONCLUSTERED INDEX IX_NC_PRM_COL_BTC_NAM
ON [dbo].[Param_Collect] ([batch_name])
INCLUDE ([last_extraction_date])
GO
CREATE NONCLUSTERED INDEX IX_NC_PRM_COL_BTC_FLG
ON [dbo].[Param_Collect] ([batch_name], [flag])
GO
CREATE NONCLUSTERED INDEX IX_NC_PRM_COL_DOM_BTC_FLG
ON [dbo].[Param_Collect] ([domain], [batch_name], [flag])
INCLUDE ([last_extraction_date])
GO