﻿CREATE TABLE [dbo].[Param_IWD_TABLE_STRUCT] (
    [source_table]       VARCHAR (100) NULL,
    [source_column_name] VARCHAR (100) NULL,
    [source_data_type]   VARCHAR (100) NULL,
    [cible_table_name]   VARCHAR (100) NULL,
    [cible_column_name]  VARCHAR (100) NULL,
    [cible_data_type]    VARCHAR (100) NULL,
    [flag_Identity]      INT           NULL,
    [key_column]         INT           NULL
);

