﻿CREATE TABLE [dbo].[IWD_TMP_TABLE_STRUCT_svg] (
    [source_table]       VARCHAR (100) NULL,
    [source_column_name] VARCHAR (100) NULL,
    [source_data_type]   VARCHAR (100) NULL,
    [cible_table_name]   VARCHAR (100) NULL,
    [cible_column_name]  VARCHAR (100) NULL,
    [cible_data_type]    VARCHAR (100) NULL,
    [flag_Identity]      INT           NULL
);

