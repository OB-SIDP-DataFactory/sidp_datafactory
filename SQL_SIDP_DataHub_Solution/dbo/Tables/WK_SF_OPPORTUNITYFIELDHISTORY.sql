﻿CREATE TABLE [dbo].[WK_SF_OPPORTUNITYFIELDHISTORY] (
    [Id]            INT            IDENTITY (1, 1) NOT NULL,
    [Id_SF]         NVARCHAR (50)  NOT NULL,
    [CreatedByID]   NVARCHAR (50)  NULL,
    [CreatedDate]   DATETIME2 (7)  NOT NULL,
    [Field]         NVARCHAR (255) NULL,
    [IsDeleted]     NVARCHAR (10)  NULL,
    [NewValue]      NVARCHAR (MAX) NULL,
    [OldValue]      NVARCHAR (MAX) NULL,
    [OpportunityId] NVARCHAR (50)  NULL,
    CONSTRAINT [PK_WK_SF_OPPORTUNITYFIELDHISTORY] PRIMARY KEY CLUSTERED ([Id] ASC)
)
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de l''opportunité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYFIELDHISTORY', @level2type = N'COLUMN', @level2name = N'OpportunityId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ancienne valeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYFIELDHISTORY', @level2type = N'COLUMN', @level2name = N'OldValue';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nouvelle valeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYFIELDHISTORY', @level2type = N'COLUMN', @level2name = N'NewValue';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Supprimé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYFIELDHISTORY', @level2type = N'COLUMN', @level2name = N'IsDeleted';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID d''historique d''opportunité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYFIELDHISTORY', @level2type = N'COLUMN', @level2name = N'Id_SF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Champ modifié', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYFIELDHISTORY', @level2type = N'COLUMN', @level2name = N'Field';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYFIELDHISTORY', @level2type = N'COLUMN', @level2name = N'CreatedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID créé par', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYFIELDHISTORY', @level2type = N'COLUMN', @level2name = N'CreatedByID';
GO