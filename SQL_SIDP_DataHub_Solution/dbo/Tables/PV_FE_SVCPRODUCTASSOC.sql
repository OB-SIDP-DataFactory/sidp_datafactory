﻿CREATE TABLE [dbo].[PV_FE_SVCPRODUCTASSOC] (
    [Id]                    BIGINT       IDENTITY (1, 1) NOT NULL,
    [SERVICE_PRODUCT_ID]    DECIMAL (19) NULL,
    [COMMERCIAL_PRODUCT_ID] DECIMAL (19) NULL,
    CONSTRAINT [PK_PV_FE_SVCPRODUCTASSOC] PRIMARY KEY CLUSTERED ([Id] ASC)
);

