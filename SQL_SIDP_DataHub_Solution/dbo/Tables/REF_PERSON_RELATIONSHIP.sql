﻿CREATE TABLE [dbo].[REF_PERSON_RELATIONSHIP] (
    [ID]                     BIGINT         IDENTITY (1, 1) NOT NULL,
    [PERSON_RELATIONSHIP_ID] DECIMAL (38)   NOT NULL,
    [FRANFINANCE_CODE]       NVARCHAR (5)   NULL,
    [SAB_STATE_CODE]         NVARCHAR (5)   NULL,
    [SAB_RELATIONSHIP_CODE]  NVARCHAR (5)   NULL,
    [REF_FAMILY_ID]          DECIMAL (19)   NULL,
    [CODE]                   NVARCHAR (255) NULL,
    [PRIORITY]               DECIMAL (19)   NULL,
    [REF_LANG_ID]            DECIMAL (19)   NULL,
    [MESSAGE]                NVARCHAR (255) NULL,
    [Validity_StartDate]     DATETIME2 (7)  NOT NULL,
    [Validity_EndDate]       DATETIME2 (7)  NOT NULL,
    CONSTRAINT [PK_REF_PERSON_RELATIONSHIP] PRIMARY KEY CLUSTERED ([ID] ASC)
);

