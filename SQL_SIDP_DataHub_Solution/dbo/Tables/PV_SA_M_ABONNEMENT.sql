﻿CREATE TABLE [dbo].[PV_SA_M_ABONNEMENT] (
    [ID]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [DWHABODTX]          DATE                                        NULL,
    [DWHABOETA]          INT                                         NULL,
    [DWHABOAGE]          INT                                         NULL,
    [DWHABOSER]          VARCHAR (2)                                 NULL,
    [DWHABOSSE]          VARCHAR (2)                                 NULL,
    [DWHABONUM]          INT                                         NULL,
    [DWHABOCLI]          VARCHAR (7)                                 NULL,
    [DWHABOCOM]          VARCHAR (20)                                NULL,
    [DWHABOPRO]          VARCHAR (3)                                 NULL,
    [DWHABOADH]          DATE                                        NULL,
    [DWHABOFIN]          DATE                                        NULL,
    [DWHABOREN]          DATE                                        NULL,
    [DWHABOCET]          VARCHAR (1)                                 NULL,
    [DWHABOCOF]          VARCHAR (20)                                NULL,
    [DWHABOUT1]          INT                                         NULL,
    [DWHABOUT2]          INT                                         NULL,
    [DWHABORES]          DATE                                        NULL,
    [DWHABOMOR]          VARCHAR (6)                                 NULL,
    [DWHABOCRE]          DATE                                        NULL,
    [DWHABOVAL]          DATE                                        NULL,
    [DWHABOANN]          DATE                                        NULL,
    [DWHABOREL]          VARCHAR (1)                                 NULL,
    [Validity_StartDate] DATETIME                                    NULL,
    [Validity_EndDate]   DATETIME                                    DEFAULT ('99991231') NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SA_M_ABONNEMENT] PRIMARY KEY CLUSTERED ([ID] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SA_M_ABONNEMENTHistory], DATA_CONSISTENCY_CHECK=ON));
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ANALYSE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ABONNEMENT', @level2type = N'COLUMN', @level2name = N'DWHABODTX';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ETABLISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ABONNEMENT', @level2type = N'COLUMN', @level2name = N'DWHABOETA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AGENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ABONNEMENT', @level2type = N'COLUMN', @level2name = N'DWHABOAGE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SERVICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ABONNEMENT', @level2type = N'COLUMN', @level2name = N'DWHABOSER';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SOUS-SERVICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ABONNEMENT', @level2type = N'COLUMN', @level2name = N'DWHABOSSE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMÉRO CONTRAT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ABONNEMENT', @level2type = N'COLUMN', @level2name = N'DWHABONUM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMÉRO CLIENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ABONNEMENT', @level2type = N'COLUMN', @level2name = N'DWHABOCLI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COMPTE PRINCIPAL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ABONNEMENT', @level2type = N'COLUMN', @level2name = N'DWHABOCOM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE PRODUIT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ABONNEMENT', @level2type = N'COLUMN', @level2name = N'DWHABOPRO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ADHÉSION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ABONNEMENT', @level2type = N'COLUMN', @level2name = N'DWHABOADH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE FIN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ABONNEMENT', @level2type = N'COLUMN', @level2name = N'DWHABOFIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE RENOUVELLEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ABONNEMENT', @level2type = N'COLUMN', @level2name = N'DWHABOREN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE ÉTAT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ABONNEMENT', @level2type = N'COLUMN', @level2name = N'DWHABOCET';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COMPTE FACTURATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ABONNEMENT', @level2type = N'COLUMN', @level2name = N'DWHABOCOF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'UTILISATEUR 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ABONNEMENT', @level2type = N'COLUMN', @level2name = N'DWHABOUT1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'UTILISATEUR 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ABONNEMENT', @level2type = N'COLUMN', @level2name = N'DWHABOUT2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE RÉSILIATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ABONNEMENT', @level2type = N'COLUMN', @level2name = N'DWHABORES';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MOTIF RÉSILIATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ABONNEMENT', @level2type = N'COLUMN', @level2name = N'DWHABOMOR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE CRÉATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ABONNEMENT', @level2type = N'COLUMN', @level2name = N'DWHABOCRE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE VALIDATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ABONNEMENT', @level2type = N'COLUMN', @level2name = N'DWHABOVAL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ANNULATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ABONNEMENT', @level2type = N'COLUMN', @level2name = N'DWHABOANN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MODE ENVOI RELEVÉS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ABONNEMENT', @level2type = N'COLUMN', @level2name = N'DWHABOREL';
GO
CREATE UNIQUE NONCLUSTERED INDEX [IDX_NUMABO]
    ON [dbo].[PV_SA_M_ABONNEMENT]([DWHABONUM] ASC);

