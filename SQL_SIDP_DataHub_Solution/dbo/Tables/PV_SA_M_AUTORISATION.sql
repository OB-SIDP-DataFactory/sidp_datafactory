﻿CREATE TABLE [dbo].[PV_SA_M_AUTORISATION] (
    [ID]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [DWHAUTDTX]          DATE                                        NULL,
    [DWHAUTETA]          INT                                         NULL,
    [DWHAUTCLI]          VARCHAR (7)                                 NULL,
    [DWHAUTTYP]          VARCHAR (1)                                 NULL,
    [DWHAUTAUT]          VARCHAR (20)                                NULL,
    [DWHAUTDEV]          VARCHAR (3)                                 NULL,
    [DWHAUTGRP]          VARCHAR (1)                                 NULL,
    [DWHAUTMAU]          DECIMAL (18, 3)                             NULL,
    [DWHAUTMAB]          DECIMAL (18, 3)                             NULL,
    [DWHAUTDAD]          DATE                                        NULL,
    [DWHAUTDAF]          DATE                                        NULL,
    [DWHAUTELM]          VARCHAR (1)                                 NULL,
    [DWHAUTNIV]          INT                                         NULL,
    [DWHAUTCOM]          VARCHAR (1)                                 NULL,
    [DWHAUTDEP]          VARCHAR (1)                                 NULL,
    [DWHAUTMTD]          DECIMAL (18, 3)                             NULL,
    [DWHAUTMTB]          DECIMAL (18, 3)                             NULL,
    [DWHAUTDPD]          DATE                                        NULL,
    [DWHAUTNDP]          INT                                         NULL,
    [DWHAUTECI]          DATE                                        NULL,
    [DWHAUTRAT]          VARCHAR (1)                                 NULL,
    [DWHAUTATR]          VARCHAR (1)                                 NULL,
    [DWHAUTREL]          VARCHAR (3)                                 NULL,
    [DWHAUTRUB]          VARCHAR (10)                                NULL,
    [DWHAUTUTI]          INT                                         NULL,
    [DWHAUTFRA]          VARCHAR (1)                                 NULL,
    [DWHAUTDOM]          VARCHAR (12)                                NULL,
    [DWHAUTAPP]          VARCHAR (3)                                 NULL,
    [DWHAUTDSY]          DATE                                        NULL,
    [Validity_StartDate] DATETIME                                    NULL,
    [Validity_EndDate]   DATETIME                                    DEFAULT ('99991231') NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SA_M_AUTORISATION] PRIMARY KEY CLUSTERED ([ID] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SA_M_AUTORISATIONHistory], DATA_CONSISTENCY_CHECK=ON));
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DU JOUR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_AUTORISATION', @level2type = N'COLUMN', @level2name = N'DWHAUTDSY';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'APPLICATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_AUTORISATION', @level2type = N'COLUMN', @level2name = N'DWHAUTAPP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DOMAINE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_AUTORISATION', @level2type = N'COLUMN', @level2name = N'DWHAUTDOM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FREQUENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_AUTORISATION', @level2type = N'COLUMN', @level2name = N'DWHAUTFRA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE UTILISATEUR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_AUTORISATION', @level2type = N'COLUMN', @level2name = N'DWHAUTUTI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RUBRIQUE COMPTABLE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_AUTORISATION', @level2type = N'COLUMN', @level2name = N'DWHAUTRUB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RELATION CLI/GROUPE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_AUTORISATION', @level2type = N'COLUMN', @level2name = N'DWHAUTREL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AUTORISATION GROUPE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_AUTORISATION', @level2type = N'COLUMN', @level2name = N'DWHAUTATR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RATTACHEMENT GROUPE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_AUTORISATION', @level2type = N'COLUMN', @level2name = N'DWHAUTRAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DTE ÉCHÉANCE INTERNE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_AUTORISATION', @level2type = N'COLUMN', @level2name = N'DWHAUTECI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NBRE DE JOUR DÈPAS.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_AUTORISATION', @level2type = N'COLUMN', @level2name = N'DWHAUTNDP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DEBUT DÈPAS.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_AUTORISATION', @level2type = N'COLUMN', @level2name = N'DWHAUTDPD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT DEPAS/DEVISE BAS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_AUTORISATION', @level2type = N'COLUMN', @level2name = N'DWHAUTMTB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT DE DEPAS.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_AUTORISATION', @level2type = N'COLUMN', @level2name = N'DWHAUTMTD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE DÉPASSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_AUTORISATION', @level2type = N'COLUMN', @level2name = N'DWHAUTDEP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COMPENSATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_AUTORISATION', @level2type = N'COLUMN', @level2name = N'DWHAUTCOM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NIVEAU', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_AUTORISATION', @level2type = N'COLUMN', @level2name = N'DWHAUTNIV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE ÉLÉMENTAIRE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_AUTORISATION', @level2type = N'COLUMN', @level2name = N'DWHAUTELM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE FIN AUT.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_AUTORISATION', @level2type = N'COLUMN', @level2name = N'DWHAUTDAF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DEBUT AUT.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_AUTORISATION', @level2type = N'COLUMN', @level2name = N'DWHAUTDAD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT.AUT/DEVISE BASE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_AUTORISATION', @level2type = N'COLUMN', @level2name = N'DWHAUTMAB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT AUTORISATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_AUTORISATION', @level2type = N'COLUMN', @level2name = N'DWHAUTMAU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CLIENT GROUPE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_AUTORISATION', @level2type = N'COLUMN', @level2name = N'DWHAUTGRP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEVISE UTILISATEUR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_AUTORISATION', @level2type = N'COLUMN', @level2name = N'DWHAUTDEV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE AUTORISATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_AUTORISATION', @level2type = N'COLUMN', @level2name = N'DWHAUTAUT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE (1,2,3)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_AUTORISATION', @level2type = N'COLUMN', @level2name = N'DWHAUTTYP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMERO CLIENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_AUTORISATION', @level2type = N'COLUMN', @level2name = N'DWHAUTCLI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ETABLISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_AUTORISATION', @level2type = N'COLUMN', @level2name = N'DWHAUTETA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE EXTRACTION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_AUTORISATION', @level2type = N'COLUMN', @level2name = N'DWHAUTDTX';

