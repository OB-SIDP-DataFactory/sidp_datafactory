﻿CREATE TABLE [dbo].[PV_SA_Q_MOUVEMENT] (
	[Id]              BIGINT          IDENTITY(1, 1) NOT NULL,
	[COD_OPE]         VARCHAR(3)     NULL,
	[NUM_OPE]         INT             NULL,
	[COD_EVE]         VARCHAR(3)     NULL,
	[COD_SCH]         INT             NULL,
	[NUM_PIE]         INT             NULL,
	[NUM_ECR]         INT             NULL,
	[COD_ANA]         VARCHAR(6)     NULL,
	[STR_ANA]         VARCHAR(6)     NULL,
	[COD_EXO]         CHAR(1)     NULL,
	[COD_BDF]         VARCHAR(3)     NULL,
	[NAT_OPE]         VARCHAR(3)     NULL,
	[NUM_CPT]         VARCHAR(20)    NULL,
	[MTT_DEV]         DECIMAL(16, 2) NULL,
	[SEN_MTT]         CHAR(1)     NULL,
	[DTE_OPE]         DATE            NULL,
	[DTE_CPT]         DATE            NULL,
	[DTE_VAL]         DATE            NULL,
	[DTE_TRT]         DATE            NULL,
	[COD_UTI]         VARCHAR(4)     NULL,
	[COD_AGE]         VARCHAR(4)     NULL,
	[COD_SVC]         CHAR(2)     NULL,
	[COD_SOU_SVC]     CHAR(2)     NULL,
	[COD_ANN]         CHAR(1)     NULL,
	[COD_BAN_EIC]     VARCHAR(5)     NULL,
	[LIB_MVT_1]       VARCHAR(30)    NULL,
	[LIB_MVT_2]       VARCHAR(30)    NULL,
	[LIB_MVT_3]       VARCHAR(30)    NULL,
	[COD_EME]         VARCHAR(6)     NULL,
	[DTE_TRT_MAD]     DATE            NULL,
	[DTE_REF_MAD]     DATE            NULL,
	[DTE_LIM_REJ_MAD] DATE            NULL,
	[COD_UTI_INI_MAD] VARCHAR(4)     NULL,
	[DTE_CRE_MAD]     DATE            NULL,
	[DTE_UTI_MAD]     DATE            NULL,
	[COD_UTI_MAD]     VARCHAR(4)     NULL,
	[REJ_ACC_MAD]     CHAR(1)     NULL,
	[DON_ANA]         VARCHAR(80)    NULL,
	[LIB_COMP_1]      VARCHAR(70)    NULL,
	[LIB_COMP_2]      VARCHAR(70)    NULL,
	[NUM_REM]         VARCHAR(7)     NULL,
	[NUM_CHQ]         VARCHAR(7)     NULL,
	[MTT_EUR]         DECIMAL(16, 2) NULL,
	[DEV]             VARCHAR(3)     NULL,
	[IDE_DO]          VARCHAR(7)     NULL,
	[IDE_CTP]         VARCHAR(7)     NULL,
	[NOM_CTP]         VARCHAR(70)    NULL,
	[NUM_CPT_CTP]     VARCHAR(35)    NULL,
	[BIC_CTP]         VARCHAR(12)    NULL,
	[COD_BAN_CTP]     VARCHAR(12)    NULL,
	[COD_PAY_CTP]     VARCHAR(5)     NULL,
	[TOP_CLI]			CHAR(1)	NULL,
	[FIX_DEV]         DECIMAL(14, 9) NULL,
	[COD_AGE_OPE]     VARCHAR(6)     NULL,
	CONSTRAINT [PK_PV_SA_Q_MOUVEMENT] PRIMARY KEY NONCLUSTERED ([Id] ASC)
	ON [PRIMARY]
) ON PV_SA_Q_MVT_PartitionScheme(DTE_OPE)
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Opérations Clients', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code opération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'COD_OPE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro opération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'NUM_OPE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Evénement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'COD_EVE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code schéma', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'COD_SCH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro de pièce', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'NUM_PIE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro écriture', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'NUM_ECR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code analytique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'COD_ANA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Structure analytique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'STR_ANA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code exonération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'COD_EXO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code BDF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'COD_BDF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nature opération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'NAT_OPE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'NUM_CPT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant de l''opération en devise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'MTT_DEV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date opération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'DTE_OPE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date comptable', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'DTE_CPT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date valeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'DTE_VAL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date traitement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'DTE_TRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Utilisateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'COD_UTI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Agence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'COD_AGE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Service', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'COD_SVC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Sous service', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'COD_SOU_SVC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code annulation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'COD_ANN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code banque EIC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'COD_BAN_EIC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'LIB_MVT_1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'LIB_MVT_2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé 3', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'LIB_MVT_3';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code émetteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'COD_EME';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MAD Date traitement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'DTE_TRT_MAD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MAD Date de référence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'DTE_REF_MAD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MAD Date limite rejet', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'DTE_LIM_REJ_MAD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MAD Utilis.initiateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'COD_UTI_INI_MAD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MAD Date création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'DTE_CRE_MAD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MAD Date utilisation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'DTE_UTI_MAD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MAD Utilisateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'COD_UTI_MAD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MAD Rejet acceptation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'REJ_ACC_MAD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Données analytiques', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'DON_ANA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé complémentaire 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'LIB_COMP_1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé complémentaire 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'LIB_COMP_2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° remise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'NUM_REM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° chèque', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'NUM_CHQ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant de l''opération en euro', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'MTT_EUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Devis de l''opération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'DEV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant D.O', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'IDE_DO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant contrepartie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'IDE_CTP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom/Raison sociale de la contrepartie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'NOM_CTP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Compte de la contrepartie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'NUM_CPT_CTP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'BIC de la contrepartie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'BIC_CTP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code banque de la contrepartie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'COD_BAN_CTP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code pays établissement de la contrepartie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'COD_PAY_CTP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Top O/N compte client ou non', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'TOP_CLI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fixing devise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_MOUVEMENT', @level2type = N'COLUMN', @level2name = N'FIX_DEV';
GO
CREATE CLUSTERED INDEX [IX_C_MVT_DTE_OPE]
ON [dbo].[PV_SA_Q_MOUVEMENT] ([DTE_OPE])
WITH (ONLINE = ON)
ON [PV_SA_Q_MVT_PartitionScheme]([DTE_OPE])
GO
CREATE NONCLUSTERED INDEX [IX_NC_MVT_DTE_CPT_OPE_EVE]
ON [dbo].[PV_SA_Q_MOUVEMENT] ([DTE_OPE],[DTE_TRT], [NUM_CPT], [COD_OPE], [COD_EVE])
INCLUDE ([NUM_OPE], [NUM_PIE], [COD_ANA], [NAT_OPE], [SEN_MTT], [DTE_CPT], [DTE_VAL], [LIB_MVT_1], [LIB_MVT_2], [LIB_MVT_3], [DON_ANA], [DEV], [BIC_CTP], [MTT_EUR], [MTT_DEV])
WITH (ONLINE = ON)
ON PV_SA_Q_MVT_PartitionScheme(DTE_OPE)
GO
CREATE NONCLUSTERED INDEX [IX_NC_MVT_OPE]
ON [dbo].[PV_SA_Q_MOUVEMENT] ([DTE_OPE],[COD_OPE])
INCLUDE ([NUM_CPT],[DON_ANA],[MTT_EUR])
WITH (ONLINE = ON)
ON PV_SA_Q_MVT_PartitionScheme(DTE_OPE)
GO