﻿CREATE TABLE [dbo].[PV_MC_STATUSCHANGEHistory] (
    [Id]            BIGINT         NOT NULL,
    [ClientID]      INT            NULL,
    [SubscriberKey] NVARCHAR (100) NOT NULL,
    [EmailAddress]  NVARCHAR (100) NULL,
    [SubscriberID]  INT            NULL,
    [OldStatus]     NVARCHAR (10)  NULL,
    [NewStatus]     NVARCHAR (10)  NULL,
    [DateChanged]   DATETIME       NOT NULL,
    [Startdt]       DATETIME2 (7)  NOT NULL,
    [Enddt]         DATETIME2 (7)  NOT NULL
);
GO
CREATE CLUSTERED INDEX [ix_PV_MC_STATUSCHANGEHistory]
    ON [dbo].[PV_MC_STATUSCHANGEHistory]([Enddt] ASC, [Startdt] ASC) WITH (DATA_COMPRESSION = PAGE);

