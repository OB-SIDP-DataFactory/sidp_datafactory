﻿CREATE TABLE [dbo].[WK_SA_M_LKCPTINT] (
    [DWHMM1DTX] DATE            NULL,
    [DWHMM1ETA] INT             NULL,
    [DWHMM1AGE] INT             NULL,
    [DWHMM1SER] VARCHAR (2)     NULL,
    [DWHMM1SES] VARCHAR (2)     NULL,
    [DWHMM1OPE] VARCHAR (6)     NULL,
    [DWHMM1NAT] VARCHAR (6)     NULL,
    [DWHMM1NUM] INT             NULL,
    [DWHMM1DTD] DATE            NULL,
    [DWHMM1MON] DECIMAL (18, 3) NULL,
    [DWHMM1NBR] INT             NULL,
    [DWHMM1DEV] VARCHAR (3)     NULL,
    [DWHMM1TYC] VARCHAR (1)     NULL,
    [DWHMM1CLI] VARCHAR (7)     NULL,
    [DWHMM1COM] VARCHAR (20)    NULL,
    [DWHMM1ENG] DATE            NULL,
    [DWHMM1DEB] DATE            NULL,
    [DWHMM1FIN] DATE            NULL,
    [DWHMM1DUR] INT             NULL,
    [DWHMM1TYP] VARCHAR (1)     NULL,
    [DWHMM1AUT] VARCHAR (3)     NULL,
    [DWHMM1CVL] DECIMAL (18, 3) NULL,
    [DWHMM1NLO] INT             NULL,
    [DWHMM1DSY] DATE            NULL,
    [DWHMM1TYA] VARCHAR (1)     NULL,
    [DWHMM1PER] VARCHAR (1)     NULL,
    [DWHMM1VTI] DECIMAL (14, 9) NULL,
    [DWHMM1PLF] DECIMAL (14, 9) NULL,
    [DWHMM1PLC] DECIMAL (14, 9) NULL
);

