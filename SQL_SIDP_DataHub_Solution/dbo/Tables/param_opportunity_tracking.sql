﻿CREATE TABLE [dbo].[param_opportunity_tracking] (
    [Id]    INT           IDENTITY (1, 1) NOT NULL,
    [Label] NVARCHAR (50) NULL,
    CONSTRAINT [PK_param_opportunity_tracking] PRIMARY KEY CLUSTERED ([Id] ASC)
);

