﻿CREATE TABLE [dbo].[REF_RECORDTYPE] (
    [PK_ID]              INT            IDENTITY (1, 1) NOT NULL,
    [OBJECTTYPE_SF]      NVARCHAR (255) NULL,
    [ID_SF]              NVARCHAR (18)  NULL,
    [CODE_SF]            NVARCHAR (255) NULL,
    [NAME_SF]            NVARCHAR (255) NULL,
    [DESCRIPTION_SF]     NVARCHAR (255) NULL,
    [Validity_StartDate] DATETIME       CONSTRAINT [DF_REF_RECORDTYPE_Validity_StartDate] DEFAULT ('2017-01-01') NULL,
    [Validity_EndDate]   DATETIME       CONSTRAINT [DF_REF_RECORDTYPE_Validity_EndDate] DEFAULT ('9999-12-31') NULL,
    CONSTRAINT [PK_REF_RECORDTYPE] PRIMARY KEY CLUSTERED ([PK_ID] ASC)
);
GO
CREATE NONCLUSTERED INDEX IX_NC_REC_TYP_OBJ_SF
ON [dbo].[REF_RECORDTYPE]([OBJECTTYPE_SF] ASC)
INCLUDE([ID_SF]);
GO
CREATE NONCLUSTERED INDEX IX_NC_REF_RCD_TYP_SF
ON [dbo].[REF_RECORDTYPE] ([CODE_SF], [OBJECTTYPE_SF])
INCLUDE ([ID_SF], [NAME_SF])
GO