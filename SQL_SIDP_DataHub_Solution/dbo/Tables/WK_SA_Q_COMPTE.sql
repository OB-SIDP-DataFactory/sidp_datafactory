﻿CREATE TABLE [dbo].[WK_SA_Q_COMPTE] (
    [DWHCPTDTX]  DATE            NULL,
    [DWHCPTETA]  INT             NULL,
    [DWHCPTPLA]  INT             NULL,
    [DWHCPTCOM]  VARCHAR (20)    NULL,
    [DWHCPTRUB]  VARCHAR (10)    NULL,
    [DWHCPTINT]  VARCHAR (32)    NULL,
    [DWHCPTNBR]  INT             NULL,
    [DWHCPTPPAL] VARCHAR (7)     NULL,
    [DWHCPTAGE]  INT             NULL,
    [DWHCPTDEV]  VARCHAR (3)     NULL,
    [DWHCPTOPR]  DECIMAL (18, 3) NULL,
    [DWHCPTVAL]  DECIMAL (18, 3) NULL,
    [DWHCPTCPT]  DECIMAL (18, 3) NULL,
    [DWHCPTCPC]  DECIMAL (18, 3) NULL,
    [DWHCPTRES]  INT             NULL,
    [DWHCPTDUR]  INT             NULL,
    [DWHCPTMG1]  DECIMAL (18, 3) NULL,
    [DWHCPTMG2]  DECIMAL (18, 3) NULL,
    [DWHCPTGAR]  VARCHAR (6)     NULL,
    [DWHCPTGA1]  VARCHAR (7)     NULL,
    [DWHCPTQUA]  VARCHAR (3)     NULL,
    [DWHCPTGA2]  VARCHAR (3)     NULL,
    [DWHCPTGA3]  VARCHAR (3)     NULL,
    [DWHCPTSMOD] DECIMAL (18, 3) NULL,
    [DWHCPTSMOC] DECIMAL (18, 3) NULL,
    [DWHCPTSMVD] DECIMAL (18, 3) NULL,
    [DWHCPTSMVC] DECIMAL (18, 3) NULL,
    [DWHCPTCDS]  VARCHAR (3)     NULL,
    [DWHCPTDAA]  DATE            NULL,
    [DWHCPTPER]  VARCHAR (1)     NULL,
    [DWHCPTNPR]  INT             NULL,
    [DWHCPTMTA]  DECIMAL (18, 3) NULL,
    [DWHCPTMTU]  DECIMAL (18, 3) NULL,
    [DWHCPTMTD]  DECIMAL (18, 3) NULL,
    [DWHCPTMTR]  DECIMAL (18, 3) NULL,
    [DWHCPTMTNR] DECIMAL (18, 3) NULL,
    [DWHCPTMTNS] DECIMAL (18, 3) NULL,
    [DWHCPTSMD]  DECIMAL (18, 3) NULL,
    [DWHCPTSMC]  DECIMAL (18, 3) NULL,
    [DWHCPTML1]  DECIMAL (18, 3) NULL,
    [DWHCPTML2]  DECIMAL (18, 3) NULL,
    [DWHCPTCLO]  VARCHAR (6)     NULL,
    [DWHCPTDAC]  DATE            NULL,
    [DWHCPTDAO]  DATE            NULL,
    [DWHCPTMIP]  DECIMAL (18, 3) NULL,
    [DWHCPTCIP]  DECIMAL (18, 3) NULL,
    [DWHCPTPBC]  DECIMAL (18, 3) NULL,
    [DWHCPTCPB]  DECIMAL (18, 3) NULL,
    [DWHCPTDDD]  DATE            NULL,
    [DWHCPTNJD]  INT             NULL,
    [DWHCPTCPM]  VARCHAR (20)    NULL,
    [DWHCPTFLG]  VARCHAR (1)     NULL,
    [DWHCPTMVC]  DECIMAL (18, 3) NULL,
    [DWHCPTCOC]  DECIMAL (18, 3) NULL,
    [DWHCPTCOB]  DECIMAL (18, 3) NULL,
    [DWHCPTCLA]  INT             NULL,
    [DWHCPTUTI]  VARCHAR (1)     NULL,
    [DWHCPTTXN]  DECIMAL (14, 9) NULL,
    [DWHCPTREV]  DECIMAL (18, 3) NULL,
    [DWHCPTSUR]  VARCHAR (1)     NULL,
    [DWHCPTFRE]  VARCHAR (1)     NULL,
    [DWHCPTRIS]  VARCHAR (1)     NULL
)
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE EXTRACTION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTDTX';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ETABLISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTETA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMERO PLAN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTPLA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMERO COMPTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTCOM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RUBRIQUE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTRUB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'INTITULE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTINT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NOMBRE DE TITULAIRES', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTNBR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TITULAIRE PRINCIPAL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTPPAL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AGENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTAGE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEVISE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTDEV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SOLDE DATE OPERATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTOPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SOLDE DATE VALEUR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTVAL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SOLDE DATE COMPTABLE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTCPT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SLD DTE COMP DEV.CPT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTCPC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'  EN JOURS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTRES';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'  EN JOURS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTDUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'EN DEVISE DE BASE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTMG1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'EN DEVISE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTMG2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE GARANTIE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTGAR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'GARANT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTGA1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE QUALITE GARANT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTQUA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE RESIDENCE GARAN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTGA2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NON UTILISE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTGA3';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CPT SOLDE MOY DEBIT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTSMOD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CPT SOLDE MOY CREDIT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTSMOC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CV ENC MOY CPT SLD D', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTSMVD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CV ENC MOY CPT SLD C', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTSMVC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ECHL CODE SELECTION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTCDS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ECHL DATE DERN ARRET', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTDAA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ECHL UNITE PERIODE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTPER';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ECHL NBR UNIT PERIOD', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTNPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT AUTORISE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTMTA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT UTILISE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTMTU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MNT EN DEPASSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTMTD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IEDOM MNT REFINANCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTMTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IEDOM MNT NON REFIN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTMTNR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IEDOM MNT SNS RESERV', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTMTNS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SOLDE MOYEN DEBIT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTSMD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SOLDE MOYEN CREDIT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTSMC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT LIBRE 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTML1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT LIBRE 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTML2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MOTIF DE CLOTURE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTCLO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DE CLOTURE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTDAC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE OUVERTURE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTDAO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MNT.INT.PROV.DEB.CUM', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTMIP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MNT.INT.PROV.DEV.BAS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTCIP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PART BQU/TRÉSORERIE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTPBC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CTVL PART BQU/TRÉSOR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTCPB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DÉBUT DÉCOUVERT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTDDD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NBR JOUR DÉPASSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTNJD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COMPTE MAITRE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTCPM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FLAG', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTFLG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CUMUL MVMENT CRÉDIT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTMVC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COM PROVIS DEV COMPT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTCOC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COM PROVIS DEV BASE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTCOB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CLASSE DE SECURITE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTCLA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'UTILISAT° DS LE MOIS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTUTI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TAUX NOMINAL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTTXN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'REVENUS CO-TITULAIRE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTREV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SURETÉ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTSUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FREQUENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTFRE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PÉRIMÈTRE RISQUE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SA_Q_COMPTE', @level2type = N'COLUMN', @level2name = N'DWHCPTRIS';
GO