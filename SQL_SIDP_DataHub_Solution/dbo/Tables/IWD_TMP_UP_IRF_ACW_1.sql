﻿CREATE TABLE [dbo].[IWD_TMP_UP_IRF_ACW] (
    [INTERACTION_RESOURCE_ID]  NUMERIC (19) NOT NULL,
    [AFTER_CALL_WORK_DURATION] INT          NULL,
    [CUSTOMER_ACW_COUNT]       SMALLINT     NULL,
    [CUSTOMER_ACW_DURATION]    INT          NULL,
    [START_DATE_TIME_KEY]      INT          NULL,
    [END_TS]                   INT          NULL
);

