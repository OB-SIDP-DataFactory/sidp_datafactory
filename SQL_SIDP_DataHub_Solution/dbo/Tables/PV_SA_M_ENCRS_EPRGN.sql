﻿CREATE TABLE [dbo].[PV_SA_M_ENCRS_EPRGN] (
    [Id]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [DWHEREDTX]          DATE                                        NULL,
    [DWHEREETA]          INT                                         NULL,
    [DWHERECLI]          VARCHAR (7)                                 NULL,
    [DWHEREPLA]          INT                                         NULL,
    [DWHERECOM]          VARCHAR (20)                                NULL,
    [DWHERESEQ]          INT                                         NULL,
    [DWHEREDAD]          DATE                                        NULL,
    [DWHEREDTD]          DATE                                        NULL,
    [DWHEREDAF]          DATE                                        NULL,
    [DWHEREOUV]          DATE                                        NULL,
    [DWHEREREC]          DATE                                        NULL,
    [DWHEREECH]          DATE                                        NULL,
    [DWHERERUB]          VARCHAR (10)                                NULL,
    [DWHERECOP]          VARCHAR (3)                                 NULL,
    [DWHEREPRO]          VARCHAR (6)                                 NULL,
    [DWHERESOL]          DECIMAL (18, 3)                             NULL,
    [DWHERESM1]          DECIMAL (18, 3)                             NULL,
    [DWHERESM2]          DECIMAL (18, 3)                             NULL,
    [DWHEREJDB]          INT                                         NULL,
    [DWHEREJCR]          INT                                         NULL,
    [DWHEREDEV]          VARCHAR (3)                                 NULL,
    [DWHEREIDB]          DECIMAL (18, 3)                             NULL,
    [DWHEREICR]          DECIMAL (18, 3)                             NULL,
    [DWHEREMDB]          DECIMAL (18, 3)                             NULL,
    [DWHEREMCR]          DECIMAL (18, 3)                             NULL,
    [DWHERETDB]          DECIMAL (14, 9)                             NULL,
    [DWHERETCR]          DECIMAL (14, 9)                             NULL,
    [DWHEREBA1]          DECIMAL (18, 3)                             NULL,
    [DWHEREBA2]          DECIMAL (18, 3)                             NULL,
    [DWHEREID1]          DECIMAL (18, 3)                             NULL,
    [DWHEREIC1]          DECIMAL (18, 3)                             NULL,
    [DWHERETD1]          DECIMAL (18, 3)                             NULL,
    [DWHERETC1]          DECIMAL (18, 3)                             NULL,
    [DWHERETD2]          DECIMAL (18, 3)                             NULL,
    [DWHERETC2]          DECIMAL (18, 3)                             NULL,
    [DWHERENLE]          INT                                         NULL,
    [DWHERENLR]          INT                                         NULL,
    [DWHERETXD]          DECIMAL (14, 9)                             NULL,
    [DWHERETXC]          DECIMAL (14, 9)                             NULL,
    [DWHEREDSY]          DATE                                        NULL,
    [Validity_StartDate] DATETIME                                    NULL,
    [Validity_EndDate]   DATETIME                                    DEFAULT ('99991231') NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SA_M_ENCRS_EPRGN] PRIMARY KEY CLUSTERED ([Id] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SA_M_ENCRS_EPRGNHistory], DATA_CONSISTENCY_CHECK=ON));
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE EXTRACTION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHEREDTX';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ETABLISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHEREETA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° CLIENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHERECLI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMERO DE PLAN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHEREPLA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMERO DE COMPTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHERECOM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMERO DE SEQUEN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHERESEQ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DE DEBUT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHEREDAD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DEB SITU', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHEREDTD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DE FIN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHEREDAF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE OUVERTURE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHEREOUV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DE RECEPT°', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHEREREC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ECHEANCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHEREECH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RUBRIQUE COMPTAB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHERERUB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE PRODUIT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHERECOP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE PRODUIT ERE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHEREPRO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SOLDE FIN PERIO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHERESOL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SOLDE MOYEN DB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHERESM1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SOLDE MOYEN CR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHERESM2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NBJ DE DEBIT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHEREJDB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NBJ DE CREDIT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHEREJCR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEVISE DE COMPTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHEREDEV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'INT. TRESO DB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHEREIDB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'INT. TRESO CR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHEREICR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MARGE MT DB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHEREMDB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MARGE MT CR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHEREMCR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MARGE TAUX DB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHERETDB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MARGE TAUX CR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHERETCR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SLD MOY  DB BASE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHEREBA1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SLD MOY  CR BASE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHEREBA2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'INT TRE. DB BASE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHEREID1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'INT TRE. CR BASE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHEREIC1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'INTERETS DB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHERETD1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'INTERETS CR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHERETC1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'INTERETS DB BASE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHERETD2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'INTERETS CR BASE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHERETC2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° LIGNE EMPLOIS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHERENLE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° LIGNE RESSOUR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHERENLR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TAUX ANALYSE DB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHERETXD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TAUX ANALYSE CR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHERETXC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE SYSTEME', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ENCRS_EPRGN', @level2type = N'COLUMN', @level2name = N'DWHEREDSY';
GO
CREATE UNIQUE NONCLUSTERED INDEX [IDX_XSIMERE0]
    ON [dbo].[PV_SA_M_ENCRS_EPRGN]([DWHERECOM] ASC);

