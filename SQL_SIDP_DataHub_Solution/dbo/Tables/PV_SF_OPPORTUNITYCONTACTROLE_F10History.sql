﻿CREATE TABLE [dbo].[PV_SF_OPPORTUNITYCONTACTROLE_F10History] (
    [Id]                 BIGINT        NOT NULL,
    [Id_SF]              NVARCHAR (50) NOT NULL,
    [OpportunityId]      NVARCHAR (18) NULL,
    [ContactId]          NVARCHAR (18) NULL,
    [Role]               NVARCHAR (40) NULL,
    [IsPrimary]          NVARCHAR (10) NULL,
    [CreatedDate]        DATETIME      NULL,
    [CreatedById]        NVARCHAR (18) NULL,
    [LastModifiedDate]   DATETIME      NULL,
    [LastModifiedById]   NVARCHAR (18) NULL,
    [SystemModstamp]     DATETIME      NULL,
    [IsDeleted]          NVARCHAR (10) NULL,
    [Validity_StartDate] DATETIME2 (7) NOT NULL,
    [Validity_EndDate]   DATETIME2 (7) NOT NULL,
    [Startdt]            DATETIME2 (7) NOT NULL,
    [Enddt]              DATETIME2 (7) NOT NULL
);
GO
CREATE CLUSTERED INDEX [ix_PV_SF_OPPORTUNITYCONTACTROLE_F10History]
    ON [dbo].[PV_SF_OPPORTUNITYCONTACTROLE_F10History]([Enddt] ASC, [Startdt] ASC) WITH (DATA_COMPRESSION = PAGE);

