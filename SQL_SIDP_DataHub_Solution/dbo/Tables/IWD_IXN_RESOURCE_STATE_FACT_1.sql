﻿CREATE TABLE [dbo].[IWD_IXN_RESOURCE_STATE_FACT] (
    [IXN_RESOURCE_STATE_FACT_KEY]    NUMERIC (19)  NOT NULL,
    [START_DATE_TIME_KEY]            INT           NOT NULL,
    [END_DATE_TIME_KEY]              INT           NOT NULL,
    [TENANT_KEY]                     INT           NOT NULL,
    [MEDIA_TYPE_KEY]                 INT           NOT NULL,
    [RESOURCE_KEY]                   INT           NOT NULL,
    [MEDIA_RESOURCE_KEY]             INT           NOT NULL,
    [PLACE_KEY]                      INT           NOT NULL,
    [INTERACTION_RESOURCE_STATE_KEY] INT           NOT NULL,
    [INTERACTION_TYPE_KEY]           INT           NOT NULL,
    [CREATE_AUDIT_KEY]               NUMERIC (19)  NOT NULL,
    [UPDATE_AUDIT_KEY]               NUMERIC (19)  NOT NULL,
    [INTERACTION_RESOURCE_SDT_KEY]   INT           NULL,
    [INTERACTION_RESOURCE_ID]        NUMERIC (19)  NULL,
    [START_TS]                       INT           NULL,
    [END_TS]                         INT           NULL,
    [TOTAL_DURATION]                 INT           NULL,
    [LEAD_CLIP_DURATION]             INT           NULL,
    [TRAIL_CLIP_DURATION]            INT           NULL,
    [TARGET_ADDRESS]                 VARCHAR (255) NULL,
    [ACTIVE_FLAG]                    NUMERIC (1)   NULL,
    [PURGE_FLAG]                     NUMERIC (1)   NULL,
    CONSTRAINT [PK_IXN_RC_ST_FT] PRIMARY KEY CLUSTERED ([IXN_RESOURCE_STATE_FACT_KEY] ASC)
);
GO
CREATE NONCLUSTERED INDEX [IWD_I_IRSF_SDT]
    ON [dbo].[IWD_IXN_RESOURCE_STATE_FACT]([START_DATE_TIME_KEY] ASC);

