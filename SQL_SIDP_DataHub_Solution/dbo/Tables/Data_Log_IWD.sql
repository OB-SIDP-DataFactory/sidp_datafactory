﻿CREATE TABLE [dbo].[Data_Log_IWD] (
    [table_source]           VARCHAR (100)  NULL,
    [table_cible]            VARCHAR (100)  NULL,
    [Dte_debut_alimentation] DATE           NULL,
    [Dte_fin_alimentation]   DATE           NULL,
    [statut]                 VARCHAR (100)  NULL,
    [commentaire]            VARCHAR (1000) NULL
);

