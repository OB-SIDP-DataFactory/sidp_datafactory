﻿CREATE TABLE [dbo].[IWD_GIDB_GO_METRICS] (
    [ID]               NUMERIC (16)  NOT NULL,
    [SESSID]           VARCHAR (64)  NULL,
    [CALLINGLISTID]    INT           NULL,
    [CHAINGUID]        VARCHAR (64)  NULL,
    [CALLATTID]        VARCHAR (64)  NULL,
    [OCSID]            INT           NULL,
    [RECORDHANDLE]     INT           NULL,
    [RESID1]           VARCHAR (64)  NULL,
    [RESID2]           VARCHAR (64)  NULL,
    [TYPE]             INT           NOT NULL,
    [VALUE]            INT           NOT NULL,
    [SEQ]              INT           NULL,
    [USEQ]             INT           NULL,
    [ADDED]            DATETIME      NOT NULL,
    [ADDED_TS]         INT           NULL,
    [GSYS_DOMAIN]      INT           NULL,
    [GSYS_SYS_ID]      INT           NULL,
    [GSYS_EXT_VCH1]    VARCHAR (255) NULL,
    [GSYS_EXT_VCH2]    VARCHAR (255) NULL,
    [GSYS_EXT_INT1]    INT           NULL,
    [GSYS_EXT_INT2]    INT           NULL,
    [CREATE_AUDIT_KEY] NUMERIC (19)  NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [IWD_I_G_GO_MTRCS_CTS]
    ON [dbo].[IWD_GIDB_GO_METRICS]([ADDED_TS] ASC);


GO
CREATE NONCLUSTERED INDEX [IWD_I_G_GO_MTRCS_MID]
    ON [dbo].[IWD_GIDB_GO_METRICS]([CALLINGLISTID] ASC, [TYPE] ASC, [ADDED_TS] ASC, [SESSID] ASC);


GO
CREATE NONCLUSTERED INDEX [IWD_I_G_GO_MTRCS_CATID]
    ON [dbo].[IWD_GIDB_GO_METRICS]([CALLATTID] ASC);

