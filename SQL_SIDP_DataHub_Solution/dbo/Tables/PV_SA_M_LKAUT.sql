﻿CREATE TABLE [dbo].[PV_SA_M_LKAUT] (
    [ID]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [DWHLIADTX]          DATE                                        NULL,
    [DWHLIAETA]          INT                                         NULL,
    [DWHLIACLI]          VARCHAR (7)                                 NULL,
    [DWHLIATYP]          VARCHAR (1)                                 NULL,
    [DWHLIAAUT]          VARCHAR (20)                                NULL,
    [DWHLIATY1]          VARCHAR (1)                                 NULL,
    [DWHLIAAU1]          VARCHAR (20)                                NULL,
    [DWHLIATAU]          DECIMAL (21, 9)                             NULL,
    [DWHLIAUTI]          INT                                         NULL,
    [DWHLIAFRA]          VARCHAR (1)                                 NULL,
    [DWHLIADOM]          VARCHAR (12)                                NULL,
    [DWHLIAAPP]          VARCHAR (3)                                 NULL,
    [DWHLIADSY]          DATE                                        NULL,
    [Validity_StartDate] DATETIME                                    NULL,
    [Validity_EndDate]   DATETIME                                    DEFAULT ('99991231') NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SA_M_LKAUT] PRIMARY KEY CLUSTERED ([ID] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SA_M_LKAUTHistory], DATA_CONSISTENCY_CHECK=ON));
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DU JOUR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUT', @level2type = N'COLUMN', @level2name = N'DWHLIADSY';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'APPLICATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUT', @level2type = N'COLUMN', @level2name = N'DWHLIAAPP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DOMAINE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUT', @level2type = N'COLUMN', @level2name = N'DWHLIADOM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FREQUENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUT', @level2type = N'COLUMN', @level2name = N'DWHLIAFRA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE UTILISATEUR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUT', @level2type = N'COLUMN', @level2name = N'DWHLIAUTI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TAUX DE REPORT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUT', @level2type = N'COLUMN', @level2name = N'DWHLIATAU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE AUTORIS. PÈRE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUT', @level2type = N'COLUMN', @level2name = N'DWHLIAAU1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUT', @level2type = N'COLUMN', @level2name = N'DWHLIATY1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE AUTORIS. FILS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUT', @level2type = N'COLUMN', @level2name = N'DWHLIAAUT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUT', @level2type = N'COLUMN', @level2name = N'DWHLIATYP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CLIENT FILS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUT', @level2type = N'COLUMN', @level2name = N'DWHLIACLI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ETABLISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUT', @level2type = N'COLUMN', @level2name = N'DWHLIAETA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE EXTRACTION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUT', @level2type = N'COLUMN', @level2name = N'DWHLIADTX';

