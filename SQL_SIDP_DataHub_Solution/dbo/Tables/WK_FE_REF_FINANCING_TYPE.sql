﻿CREATE TABLE [dbo].[WK_FE_REF_FINANCING_TYPE] (
    [FINANCING_TYPE_ID]        DECIMAL (38)   NOT NULL,
    [HARVEST_CODE]             NVARCHAR (2)   NULL,
    [FRANFINANCE_CODE]         NVARCHAR (5)   NULL,
    [FINANCING_TYPE_FAMILY_ID] DECIMAL (38)   NULL,
    [SALESFORCE_CODE]          NVARCHAR (2)   NULL,
    [REF_FAMILY_ID]            DECIMAL (19)   NULL,
    [CODE]                     NVARCHAR (255) NULL,
    [PRIORITY]                 DECIMAL (19)   NULL,
    [START_DATE]               DATETIME2 (7)  NULL,
    [END_DATE]                 DATETIME2 (7)  NULL,
    [REF_LANG_ID]              DECIMAL (19)   NULL,
    [MESSAGE]                  NVARCHAR (255) NULL
);

