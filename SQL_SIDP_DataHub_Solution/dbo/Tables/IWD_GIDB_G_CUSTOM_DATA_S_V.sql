﻿CREATE TABLE [dbo].[IWD_GIDB_G_CUSTOM_DATA_S_V] (
    [ID]               NUMERIC (19)  NOT NULL,
    [CALLID]           VARCHAR (50)  NULL,
    [PARTYID]          VARCHAR (50)  NULL,
    [PSEQ]             INT           NOT NULL,
    [ENDPOINTID]       INT           NULL,
    [ENDPOINTDN]       VARCHAR (255) NULL,
    [AGENTID]          INT           NULL,
    [SWITCHID]         INT           NULL,
    [TENANTID]         INT           NULL,
    [KEYNAME]          VARCHAR (64)  NOT NULL,
    [VALUE]            VARCHAR (255) NULL,
    [ADDED]            DATETIME      NULL,
    [ADDED_TS]         INT           NULL,
    [GSYS_DOMAIN]      INT           NULL,
    [GSYS_SYS_ID]      INT           NULL,
    [GSYS_EXT_INT2]    INT           NULL,
    [CREATE_AUDIT_KEY] NUMERIC (19)  NOT NULL
);


GO
CREATE CLUSTERED INDEX [I_G_CDH_V_ADDTS]
    ON [dbo].[IWD_GIDB_G_CUSTOM_DATA_S_V]([ADDED_TS] ASC);

