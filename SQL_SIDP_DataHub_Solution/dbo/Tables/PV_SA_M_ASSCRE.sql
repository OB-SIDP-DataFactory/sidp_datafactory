﻿CREATE TABLE [dbo].[PV_SA_M_ASSCRE] (
    [ID]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [DWHASSDTX]          DATE                                        NULL,
    [DWHASSETA]          INT                                         NULL,
    [DWHASSAGE]          INT                                         NULL,
    [DWHASSSER]          VARCHAR (2)                                 NULL,
    [DWHASSSSE]          VARCHAR (2)                                 NULL,
    [DWHASSOPE]          VARCHAR (6)                                 NULL,
    [DWHASSNAT]          VARCHAR (6)                                 NULL,
    [DWHASSNUM]          INT                                         NULL,
    [DWHASSNUA]          INT                                         NULL,
    [DWHASSCOA]          VARCHAR (6)                                 NULL,
    [DWHASSMOA]          DECIMAL (18, 3)                             NULL,
    [DWHASSDEV]          VARCHAR (3)                                 NULL,
    [DWHASSTAU]          DECIMAL (14, 9)                             NULL,
    [DWHASSCLI]          VARCHAR (7)                                 NULL,
    [DWHASSTYP]          VARCHAR (1)                                 NULL,
    [Validity_StartDate] DATETIME                                    NULL,
    [Validity_EndDate]   DATETIME                                    DEFAULT ('99991231') NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SA_M_ASSCRE] PRIMARY KEY CLUSTERED ([ID] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SA_M_ASSCREHistory], DATA_CONSISTENCY_CHECK=ON));
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'INDICATEUR RÔLE CLT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ASSCRE', @level2type = N'COLUMN', @level2name = N'DWHASSTYP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° CLIENT ASSURÉ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ASSCRE', @level2type = N'COLUMN', @level2name = N'DWHASSCLI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TAUX ASSURE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ASSCRE', @level2type = N'COLUMN', @level2name = N'DWHASSTAU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE DEVISE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ASSCRE', @level2type = N'COLUMN', @level2name = N'DWHASSDEV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT ASSURANCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ASSCRE', @level2type = N'COLUMN', @level2name = N'DWHASSMOA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE ASSURANCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ASSCRE', @level2type = N'COLUMN', @level2name = N'DWHASSCOA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMERO ASSURANCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ASSCRE', @level2type = N'COLUMN', @level2name = N'DWHASSNUA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMERO DOSSIER', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ASSCRE', @level2type = N'COLUMN', @level2name = N'DWHASSNUM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE NATURE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ASSCRE', @level2type = N'COLUMN', @level2name = N'DWHASSNAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE OPERATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ASSCRE', @level2type = N'COLUMN', @level2name = N'DWHASSOPE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SOUS-SERVICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ASSCRE', @level2type = N'COLUMN', @level2name = N'DWHASSSSE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SERVICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ASSCRE', @level2type = N'COLUMN', @level2name = N'DWHASSSER';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AGENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ASSCRE', @level2type = N'COLUMN', @level2name = N'DWHASSAGE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ETABLISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ASSCRE', @level2type = N'COLUMN', @level2name = N'DWHASSETA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ANALYSE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_ASSCRE', @level2type = N'COLUMN', @level2name = N'DWHASSDTX';

