﻿CREATE TABLE [dbo].[PV_FI_M_DES_Q]
(
    [Id]                       INT                                         IDENTITY(1, 1) NOT NULL,
	[COD_SOC_TEC]				NVARCHAR(3)		NULL,
	[REF_CPT]					NVARCHAR(11)	NOT	NULL,
	[DTE_MOD]					DATETIME	NULL,
	[DTE_CRE]					DATE	NULL,
	[TYP_AGE]					NCHAR(1)		NULL,
	[UID_AGE]					NVARCHAR(8)		NULL,
	[COD_MOD_REG]				NCHAR(1)		NULL,
	[COD_BAN]					NVARCHAR(5)		NULL,
	[COD_GUI]					NVARCHAR(5)		NULL,
	[NUM_CPT]					NVARCHAR(11)	NULL,
	[LIB_BAN_GUI]				NVARCHAR(24)	NULL,
	[LIB_TIT_CPT]				NVARCHAR(26)	NULL,
	[DTE_ACC_TOKOS]				DATE	NULL,
	[DTE_ACP_CLI]				DATE	NULL,
	[DTE_REA]					DATE	NULL,
	[CAT_PRE]					NVARCHAR(9)		NULL,
	[COD_OBJ]					NVARCHAR(5)		NULL,
	[COD_DEV]					NVARCHAR(3)		NULL,
	[MTT_NOM]					DECIMAL(11, 2)	NULL,
	[DUR_LEG]					DECIMAL(22)		NULL,
	[MTT_TOT_DEB]				DECIMAL(11, 2)	NULL,
	[DTE_DER_DEB]				DATE	NULL,
	[DTE_PRE_ECH]				DATE	NULL,
	[COD_SIT_TOKOS]				NCHAR(1)		NULL,
	[DTE_FIN_PRE]				DATE	NULL,
	[MTT_TOT_DU]				DECIMAL(11, 2)	NULL,
	[CPT_RES_DU]				DECIMAL(11, 2)	NULL,
	[NBR_ECH_TOT]				DECIMAL(22)		NULL,
	[NBR_ECH_ECH]				DECIMAL(22)		NULL,
	[DTE_DER_ECH]				DATE	NULL,
	[CPT_RES_DU_DER_ECH]		DECIMAL(11, 2)	NULL,
	[INT_RES_DU_DER_ECH]		DECIMAL(11, 2)	NULL,
	[ACC_DER_ECH]				DECIMAL(11, 2)	NULL,
	[ASS_DER_ECH]				DECIMAL(11, 2)	NULL,
	[DTE_PRO_ECH]				DATE	NULL,
	[CPT_PRO_ECH]				DECIMAL(11, 2)	NULL,
	[INT_PRO_ECH]				DECIMAL(11, 2)	NULL,
	[ACC_PRO_ECH]				DECIMAL(11, 2)	NULL,
	[ASS_PRO_ECH]				DECIMAL(11, 2)	NULL,
	[NUM_PAL]					DECIMAL(22)		NULL,
	[TAX_PAL]					DECIMAL(11, 2)	NULL,
	[IND_BAL]					NCHAR(1)		NULL,
	[PLA_REA]					NCHAR(1)		NULL,
	[DTE_PRO_PER_ECH]			DATE	NULL,
	[DTE_PRO_ANT]				DATE	NULL,
	[CON_TIT]					NCHAR(1)		NULL,
	[DTE_CES_CON_TIT]			DATE	NULL,
	[NUM_FON_COM_CRE]			NCHAR(2)		NULL,
	[NUM_CES_FCC]				DECIMAL(22)		NULL,
	[DTE_SOR_CON_TIT]			DATE	NULL,
	[COD_FIN_DOS]				NCHAR(1)		NULL,
	[COD_AGI]					NCHAR(1)		NULL,
	[NBR_JOU_DIF]				DECIMAL(22)		NULL,
	[TYP_CON]					NCHAR(2)		NULL,
	[IND_REP]					NCHAR(1)		NULL,
	[ORI_SIG_CLI]				NCHAR(1)		NULL,
	[PRE_MUT]					NCHAR(1)		NULL,
	[PER_AMO]					DECIMAL(22)		NULL,
	[DTE_PAS_CON]				DATE	NULL,
	[DTE_MOD_RIB]				DATE	NULL,
	[DTE_MOD_REG]				DATE	NULL,
	[FRA_DOS]					DECIMAL(11, 2)	NULL,
	[MTT_AUT_FRA]				DECIMAL(11, 2)	NULL,
	[DTE_DER_MOD]				DATE	NULL,
	[SIT_CON]					NCHAR(1)		NULL,
	[REF_CON_REA_CP]			NVARCHAR(11)	NULL,
	[DUR_TOT_REP_ECH]			DECIMAL(22)		NULL,
	[TAX_VEN]					DECIMAL(11, 2)	NULL,
	[INT_REP_RES_DU]			DECIMAL(11, 2)	NULL,
	[COD_MOD_DEB_FON]			NCHAR(1)		NULL,
	[TOP_ASS]					NCHAR(1)		NULL,
	[DTE_DER_IMP_COM]			DATE	NULL,
	[NAT_ASS_EMP]				NCHAR(2)		NULL,
	[NAT_ASS_CO_EMP]			NCHAR(2)		NULL,
	[NAT_ASS_CAU1]				NCHAR(2)		NULL,
	[NAT_ASS_CAU2]				NCHAR(2)		NULL,
	[DTE_DER_ACT]				DATE	NULL,
	[NBR_ECH_IMP_FF]			DECIMAL(11, 2)	NULL,
	[MTT_GLO_IMP]				DECIMAL(11, 2)	NULL,
	[MTT_GLO_CAP_IMP]			DECIMAL(11, 2)	NULL,
	[MTT_GLO_INT_IMP]			DECIMAL(11, 2)	NULL,
	[MTT_GLO_ASS_IMP]			DECIMAL(11, 2)	NULL,
	[MTT_GLO_ACC_IMP]			DECIMAL(11, 2)	NULL,
	[MTT_GLO_IR_IMP]			DECIMAL(11, 2)	NULL,
	[MTT_GLO_IL_IMP]			DECIMAL(11, 2)	NULL,
	[TAX_INT_RET]				DECIMAL(11, 2)	NULL,
	[DTE_PRE_IMP]				DATE	NULL,
	[DTE_DER_IMP]				DATE	NULL,
	[DTE_PRO_ECH_RAT]			DATE	NULL,
	[MTT_IMP_DEC_FICP]			DECIMAL(11, 2)	NULL,
	[NBR_IMP_AVT_TRA]			DECIMAL(11, 2)	NULL,
	[NUM_PLA_RAT_TOKOS]			NVARCHAR(3)		NULL,
	[MTT_IMP_AVT_TRA]			DECIMAL(11, 2)	NULL,
	[DER_COD_REJ_TOKOS]			NCHAR(2)		NULL,
	[MTT_IMP_COU_REM]			DECIMAL(11, 2)	NULL,
	[MTT_IMP_PLA_RAT]			DECIMAL(11, 2)	NULL,
	[MTT_IMP_HORS_PLA_NON_REM]	DECIMAL(11, 2)	NULL,
	[DTE_DER_REG]				DATE	NULL,
	[EXI_PLA_RAT]				NCHAR(1)		NULL,
	[DTE_DER_REP]				DATE	NULL,
	[DTE_ECH_REP]				DATE	NULL,
	[DTE_PRO_REP]				DATE	NULL,
	[DTE_BAS_CPT_EUR]			DATE	NULL,
	[INT_CAP_RES_DU]			DECIMAL(11, 2)	NULL,
	[INT_DIF_RES_DU]			DECIMAL(11, 2)	NULL,
	[RES_DIS]					NVARCHAR(3)		NULL,
    [Validity_StartDate]       DATETIME2(7)                               NOT NULL,
    [Validity_EndDate]         DATETIME2(7)                               DEFAULT('99991231') NOT NULL,
    [Startdt]                  DATETIME2(7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]                    DATETIME2(7) GENERATED ALWAYS AS ROW END   DEFAULT('99991231') NOT NULL,
    CONSTRAINT [PK_PV_FI_M_DES_Q] PRIMARY KEY CLUSTERED([Id] ASC),
    PERIOD FOR SYSTEM_TIME([Startdt], [Enddt])
)
WITH(SYSTEM_VERSIONING = ON(HISTORY_TABLE=[dbo].[PV_FI_M_DES_QHistory], DATA_CONSISTENCY_CHECK=ON));
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Interets différés restant du', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'INT_DIF_RES_DU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Interets capitalisés restant du', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'INT_CAP_RES_DU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date bascule compte en euro', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'DTE_BAS_CPT_EUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date prochaine représentation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'DTE_PRO_REP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date échéance représenté', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'DTE_ECH_REP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere représentation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'DTE_DER_REP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Existence plan de rattrapage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'EXI_PLA_RAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date dernier règlement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'DTE_DER_REG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant impayé hors plan non ré émis', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'MTT_IMP_HORS_PLA_NON_REM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant impayé du plan rattrapage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'MTT_IMP_PLA_RAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant impayé en cours ré émis', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'MTT_IMP_COU_REM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernier code rejet TOKOS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'DER_COD_REJ_TOKOS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant impayés avant traitement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'MTT_IMP_AVT_TRA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro plan rattrapage TOKOS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'NUM_PLA_RAT_TOKOS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre d impayés avant traitement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'NBR_IMP_AVT_TRA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant impayés pour déclaration FICP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'MTT_IMP_DEC_FICP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date prochaine échéance de rattrapage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'DTE_PRO_ECH_RAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date du dernier impayé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'DTE_DER_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date du premier impayé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'DTE_PRE_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux interets de retard', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'TAX_INT_RET';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant global IL impayé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'MTT_GLO_IL_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant global IR impayé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'MTT_GLO_IR_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant global accessoires impayé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'MTT_GLO_ACC_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant global assurance impayé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'MTT_GLO_ASS_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant global interets impayé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'MTT_GLO_INT_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant global capital impayé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'MTT_GLO_CAP_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant global impayé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'MTT_GLO_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre échéances impayées (sens FF)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'NBR_ECH_IMP_FF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere actualisation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'DTE_DER_ACT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nature assurance caution 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'NAT_ASS_CAU2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nature assurance caution 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'NAT_ASS_CAU1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nature assurance co-emprunteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'NAT_ASS_CO_EMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nature assurance emprunteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'NAT_ASS_EMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere imputation comptable', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'DTE_DER_IMP_COM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Indicateur assurance souscrite', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'TOP_ASS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code mode de déblocage des fonds', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'COD_MOD_DEB_FON';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Interets reportés restant dus', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'INT_REP_RES_DU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux vendeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'TAX_VEN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Durée totale des reports d échéance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'DUR_TOT_REP_ECH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Référence contrat réaménagé CP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'REF_CON_REA_CP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Situation du contrat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'SIT_CON';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'DTE_DER_MOD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant autres frais', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'MTT_AUT_FRA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Frais de dossier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'FRA_DOS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de modification du règlement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'DTE_MOD_REG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de modification du RIB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'DTE_MOD_RIB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date passage en contentieux', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'DTE_PAS_CON';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Périodicité d amortissement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'PER_AMO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prêt mutualisé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'PRE_MUT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Origine signature client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'ORI_SIG_CLI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Indicateur de reprise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'IND_REP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type de contrat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'TYP_CON';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre de jours différés', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'NBR_JOU_DIF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'code Agios', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'COD_AGI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code fin de dossier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'COD_FIN_DOS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de sortie pour contrat titrisé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'DTE_SOR_CON_TIT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro de cession au FCC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'NUM_CES_FCC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro fond commun de créance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'NUM_FON_COM_CRE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de cession contrat titrisé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'DTE_CES_CON_TIT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Contrat titrisé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'CON_TIT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date prochaine anticipation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'DTE_PRO_ANT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date prochaine perception échéance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'DTE_PRO_PER_ECH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Existence plan de réaménagement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'PLA_REA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Indicateur de balayage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'IND_BAL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux interet du palier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'TAX_PAL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro de palier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'NUM_PAL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Assurance prochaine échéance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'ASS_PRO_ECH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Accessoires prochaine échéance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'ACC_PRO_ECH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Interets prochaine échéance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'INT_PRO_ECH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Capital prochaine échéance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'CPT_PRO_ECH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date prochaine échéance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'DTE_PRO_ECH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Assurances derniere échéance traitée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'ASS_DER_ECH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Accessoire dernier échéance traitée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'ACC_DER_ECH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Interets derniere échéance traitée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'INT_RES_DU_DER_ECH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Capital restant du derniere échéance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'CPT_RES_DU_DER_ECH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere échéance traitée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'DTE_DER_ECH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre échéances échues', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'NBR_ECH_ECH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre échéances totales', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'NBR_ECH_TOT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Capital restant du', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'CPT_RES_DU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant total somme due', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'MTT_TOT_DU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date fin prévisionnelle dossier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'DTE_FIN_PRE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code situation tokos', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'COD_SIT_TOKOS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date premiere échéance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'DTE_PRE_ECH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date dernier déblocage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'DTE_DER_DEB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant total débloqué', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'MTT_TOT_DEB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Durée légale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'DUR_LEG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant du nominal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'MTT_NOM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code devise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'COD_DEV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code de l objet financé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'COD_OBJ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Catégorie de prêt', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'CAT_PRE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date réalisation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'DTE_REA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date acceptation client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'DTE_ACP_CLI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date accord tokos', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'DTE_ACC_TOKOS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé titulaire du compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'LIB_TIT_CPT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé banque + guichet', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'LIB_BAN_GUI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro de compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'NUM_CPT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code guichet', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'COD_GUI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code banque', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'COD_BAN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code mode de règlement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'COD_MOD_REG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MATR USERID Gestionnaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'UID_AGE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type agent', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'TYP_AGE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'DTE_CRE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'DTE_MOD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Référence contrat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'REF_CPT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code société technique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES_Q', @level2type = N'COLUMN', @level2name = N'COD_SOC_TEC';
GO