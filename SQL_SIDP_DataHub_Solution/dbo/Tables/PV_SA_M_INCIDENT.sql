﻿CREATE TABLE [dbo].[PV_SA_M_INCIDENT] (
    [ID]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [DWHFIMDTX]          DATE                                        NULL,
    [DWHFIMETA]          INT                                         NULL,
    [DWHFIMTYP]          VARCHAR (2)                                 NULL,
    [DWHFIMNUM]          INT                                         NULL,
    [DWHFIMCLT]          VARCHAR (7)                                 NULL,
    [DWHFIMPLA]          INT                                         NULL,
    [DWHFIMCPT]          VARCHAR (20)                                NULL,
    [DWHFIMNUC]          INT                                         NULL,
    [DWHFIMSEQ]          INT                                         NULL,
    [DWHFIMEVE]          DATE                                        NULL,
    [DWHFIMDEV]          VARCHAR (3)                                 NULL,
    [DWHFIMMON]          DECIMAL (18, 3)                             NULL,
    [DWHFIMMOB]          DECIMAL (18, 3)                             NULL,
    [Validity_StartDate] DATETIME                                    NULL,
    [Validity_EndDate]   DATETIME                                    DEFAULT ('99991231') NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SA_M_INCIDENT] PRIMARY KEY CLUSTERED ([ID] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SA_M_INCIDENTHistory], DATA_CONSISTENCY_CHECK=ON));
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONT. GLOB. INC DEVB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INCIDENT', @level2type = N'COLUMN', @level2name = N'DWHFIMMOB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONT. GLOB. INC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INCIDENT', @level2type = N'COLUMN', @level2name = N'DWHFIMMON';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEVISE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INCIDENT', @level2type = N'COLUMN', @level2name = N'DWHFIMDEV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE EVENEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INCIDENT', @level2type = N'COLUMN', @level2name = N'DWHFIMEVE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° SEQUENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INCIDENT', @level2type = N'COLUMN', @level2name = N'DWHFIMSEQ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° CHEQUE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INCIDENT', @level2type = N'COLUMN', @level2name = N'DWHFIMNUC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° COMPTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INCIDENT', @level2type = N'COLUMN', @level2name = N'DWHFIMCPT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° PLAN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INCIDENT', @level2type = N'COLUMN', @level2name = N'DWHFIMPLA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° CLIENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INCIDENT', @level2type = N'COLUMN', @level2name = N'DWHFIMCLT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° INCIDENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INCIDENT', @level2type = N'COLUMN', @level2name = N'DWHFIMNUM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE INCIDENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INCIDENT', @level2type = N'COLUMN', @level2name = N'DWHFIMTYP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ETABLISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INCIDENT', @level2type = N'COLUMN', @level2name = N'DWHFIMETA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ANALYSE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INCIDENT', @level2type = N'COLUMN', @level2name = N'DWHFIMDTX';

