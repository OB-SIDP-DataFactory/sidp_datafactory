﻿CREATE TABLE [dbo].[PV_MC_CAMPAIGNCANCELLEDHistory] (
    [Id]                BIGINT         NOT NULL,
    [CAMPAIGN_NAME]     NVARCHAR (255) NULL,
    [CANCELLATION_DATE] DATETIME       NULL,
    [ID_CAMPAIGN]       NVARCHAR (50)  NOT NULL,
    [JOURNEY_NAME]      NVARCHAR (100) NULL,
    [AUTOMATION_NAME]   NVARCHAR (100) NULL,
    [Startdt]           DATETIME2 (7)  NOT NULL,
    [Enddt]             DATETIME2 (7)  NOT NULL
);


