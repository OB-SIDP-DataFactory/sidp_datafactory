﻿CREATE TABLE [dbo].[SIDP_QOD_RESULT_AGG] (
    [RUN_ID]           INT            NULL,
    [DATE_DEB]         DATETIME       NULL,
    [DATE_FIN]         DATETIME       NULL,
    [DUREE_EXEC]       INT            NULL,
    [ID_CONTROL]       INT            NULL,
    [NB_LIGNES_RESULT] INT            NULL,
    [message]          NVARCHAR (MAX) NULL
);

