﻿CREATE TABLE [dbo].[WK_SF_CASE] (
    [MasterRecordId]                  NVARCHAR (18)   NULL,
    [AccountId]                       NVARCHAR (18)   NULL,
    [ActionPlan__c]                   NVARCHAR (255)  NULL,
    [ARDate__c]                       DATETIME        NULL,
    [CaseNumber]                      NVARCHAR (30)   NULL,
    [ClosedDate]                      DATETIME        NULL,
    [CommercialGestAmount__c]         MONEY           NULL,
    [CreatedById]                     NVARCHAR (18)   NULL,
    [CreatedDate]                     DATETIME        NULL,
    [CustomerType__c]                 NVARCHAR (255)  NULL,
    [Description]                     NVARCHAR (MAX)  NULL,
    [DUT__c]                          DECIMAL (18)    NULL,
    [Equipment__c]                    NVARCHAR (200)  NULL,
    [ExpiredMilestone__c]             NVARCHAR (10)   NULL,
    [FirstClaim__c]                   NVARCHAR (255)  NULL,
    [Habilitation__c]                 NVARCHAR (10)   NULL,
    [Id_SF]                           NVARCHAR (50)   NOT NULL,
    [Improvement__c]                  NVARCHAR (255)  NULL,
    [Indemnity__c]                    NVARCHAR (255)  NULL,
    [InitialCase__c]                  NVARCHAR (18)   NULL,
    [isDoneWithExpMilestone__c]       DECIMAL (18)    NULL,
    [IsDoneWithRespMilestone__c]      DECIMAL (18)    NULL,
    [IsDone__c]                       DECIMAL (18)    NULL,
    [isNotDoneExpMilestone__c]        DECIMAL (18)    NULL,
    [IsNotDoneWithRespMilestone__c]   DECIMAL (18)    NULL,
    [IsNotDone__c]                    DECIMAL (18)    NULL,
    [LastModifiedById]                NVARCHAR (18)   NULL,
    [LastModifiedDate]                DATETIME        NULL,
    [LetterWaitingDate__c]            DATETIME        NULL,
    [LevelClaim__c]                   NVARCHAR (255)  NULL,
    [LostAmount__c]                   MONEY           NULL,
    [Metier__c]                       NVARCHAR (255)  NULL,
    [MilestoneStatus]                 NVARCHAR (30)   NULL,
    [Network__c]                      NVARCHAR (255)  NULL,
    [Origin]                          NVARCHAR (40)   NULL,
    [Origin__c]                       NVARCHAR (255)  NULL,
    [OwnerId]                         NVARCHAR (18)   NULL,
    [ParentId]                        NVARCHAR (18)   NULL,
    [ParentOppy__c]                   NVARCHAR (18)   NULL,
    [PrimaryQualification__c]         NVARCHAR (255)  NULL,
    [PrimarySubject__c]               NVARCHAR (255)  NULL,
    [Priority]                        NVARCHAR (40)   NULL,
    [Process__c]                      NVARCHAR (255)  NULL,
    [queueName__c]                    NVARCHAR (255)  NULL,
    [Reason]                          NVARCHAR (40)   NULL,
    [RecordTypeId]                    NVARCHAR (18)   NULL,
    [ResponseChannel__c]              NVARCHAR (255)  NULL,
    [ResponseDate__c]                 DATETIME        NULL,
    [RetrocessionAmount__c]           MONEY           NULL,
    [SecondarySubject__c]             NVARCHAR (255)  NULL,
    [SecondQualification__c]          NVARCHAR (255)  NULL,
    [ShopSeller__c]                   NVARCHAR (18)   NULL,
    [StandBy__c]                      NVARCHAR (255)  NULL,
    [Status]                          NVARCHAR (40)   NULL,
    [SubType__c]                      NVARCHAR (255)  NULL,
    [TechCloseCase__c]                NVARCHAR (10)   NULL,
    [Tech_PreviousOwner__c]           NVARCHAR (255)  NULL,
    [Tech_ReviewDateExpired__c]       NVARCHAR (10)   NULL,
    [TreatementReason__c]             NVARCHAR (255)  NULL,
    [Type]                            NVARCHAR (40)   NULL,
    [IDCampagne__c]                   NVARCHAR (100)  NULL,
    [CallStatus__c]                   NVARCHAR (255)  NULL,
    [CallDetail__c]                   NVARCHAR (255)  NULL,
    [OBShop__c]                       NVARCHAR (18)   NULL,
    [Typology__c]                     NVARCHAR (255)  NULL,
    [IsDeleted]                       NVARCHAR (10)   NULL,
    [ContactId]                       NVARCHAR (18)   NULL,
    [AssetId]                         NVARCHAR (18)   NULL,
    [ProductId]                       NVARCHAR (18)   NULL,
    [EntitlementId]                   NVARCHAR (18)   NULL,
    [QuestionId]                      NVARCHAR (18)   NULL,
    [SourceId]                        NVARCHAR (18)   NULL,
    [CommunityId]                     NVARCHAR (18)   NULL,
    [BusinessHoursId]                 NVARCHAR (18)   NULL,
    [SuppliedName]                    NVARCHAR (80)   NULL,
    [SuppliedEmail]                   NVARCHAR (255)  NULL,
    [SuppliedPhone]                   NVARCHAR (40)   NULL,
    [SuppliedCompany]                 NVARCHAR (80)   NULL,
    [IsVisibleInSelfService]          NVARCHAR (10)   NULL,
    [Subject]                         NVARCHAR (255)  NULL,
    [IsClosed]                        NVARCHAR (10)   NULL,
    [IsEscalated]                     NVARCHAR (10)   NULL,
    [HasCommentsUnreadByOwner]        NVARCHAR (10)   NULL,
    [HasSelfServiceComments]          NVARCHAR (10)   NULL,
    [IsClosedOnCreate]                NVARCHAR (10)   NULL,
    [IsSelfServiceClosed]             NVARCHAR (10)   NULL,
    [SlaStartDate]                    DATETIME2 (7)   NULL,
    [SlaExitDate]                     DATETIME2 (7)   NULL,
    [IsStopped]                       NVARCHAR (10)   NULL,
    [StopStartDate]                   DATETIME2 (7)   NULL,
    [SystemModstamp]                  DATETIME2 (7)   NULL,
    [ContactPhone]                    NVARCHAR (40)   NULL,
    [ContactMobile]                   NVARCHAR (40)   NULL,
    [ContactEmail]                    NVARCHAR (255)  NULL,
    [ContactFax]                      NVARCHAR (40)   NULL,
    [Comments]                        VARCHAR (MAX)   NULL,
    [LastViewedDate]                  DATETIME2 (7)   NULL,
    [LastReferencedDate]              DATETIME2 (7)   NULL,
    [CreatorFullPhotoUrl]             NVARCHAR (255)  NULL,
    [CreatorSmallPhotoUrl]            NVARCHAR (255)  NULL,
    [CreatorName]                     NVARCHAR (121)  NULL,
    [ManualAuthent__c]                NVARCHAR (10)   NULL,
    [Comments255__c]                  NVARCHAR (MAX)  NULL,
    [NextSLAStartDate__c]             DATETIME2 (7)   NULL,
    [PreventAssign__c]                NVARCHAR (10)   NULL,
    [ReviewDate__c]                   DATETIME2 (7)   NULL,
    [ReviewSubject__c]                NVARCHAR (255)  NULL,
    [EighteenCharId__c]               NVARCHAR (1300) NULL,
    [TCHNbMilestonesClosed__c]        DECIMAL (18)    NULL,
    [isOwnerQueue__c]                 NVARCHAR (10)   NULL,
    [Amount__c]                       MONEY           NULL,
    [AuthenticationResult__c]         NVARCHAR (255)  NULL,
    [Answer__c]                       NVARCHAR (255)  NULL,
    [Detail__c]                       NVARCHAR (MAX)  NULL,
    [DocumentType__c]                 NVARCHAR (MAX)  NULL,
    [Duration__c]                     NVARCHAR (80)   NULL,
    [Dysfunction__c]                  NVARCHAR (255)  NULL,
    [ManagerialAct__c]                NVARCHAR (255)  NULL,
    [Confidencelevel__c]              NVARCHAR (10)   NULL,
    [PaymentCeiling__c]               MONEY           NULL,
    [WithdrawalLimit__c]              MONEY           NULL,
    [ForbiddenWords__c]               NVARCHAR (255)  NULL,
    [ClientVisibility__c]             NVARCHAR (255)  NULL,
    [TCHTreatmentTime__c]             DECIMAL (18, 2) NULL,
    [CancelConfiramtion__c]           NVARCHAR (10)   NULL,
    [ClaimTreatementTime__c]          DECIMAL (18)    NULL,
    [PJPresence__c]                   NVARCHAR (80)   NULL,
    [HasForbiddenWord__c]             NVARCHAR (10)   NULL,
    [TCHAuthent__c]                   NVARCHAR (255)  NULL,
    [realizedBy__c]                   NVARCHAR (18)   NULL,
    [MessageCounter__c]               DECIMAL (18)    NULL,
    [NewMessage__c]                   NVARCHAR (1300) NULL,
    [PlanifiedDate__c]                DATETIME        NULL,
    [TCHCaseAttributionOwnerDate__c]  DATETIME2 (7)   NULL,
    [TCHCaseTimeProcessing__c]        DECIMAL (18)    NULL,
    [TCHIdCase__c]                    NVARCHAR (20)   NULL,
    [TCHPurge__c]                     DECIMAL (18)    NULL,
    [TCHTriggerPass__c]               NVARCHAR (10)   NULL,
    [TreatmentDate__c]                DATETIME        NULL,
    [Details__c]                      NVARCHAR (MAX)  NULL,
    [EFrontNumber__c]                 NVARCHAR (255)  NULL,
    [Specification__c]                NVARCHAR (255)  NULL,
    [SubUniverse__c]                  NVARCHAR (255)  NULL,
    [Universe__c]                     NVARCHAR (255)  NULL,
    [IncidentGravity__c]              NVARCHAR (255)  NULL,
    [ConseillerTechnique__c]          NVARCHAR (18)   NULL,
    [EmailSource__c]                  NVARCHAR (255)  NULL,
    [Manager__c]                      NVARCHAR (1300) NULL,
    [Decision__c]                     NVARCHAR (255)  NULL,
    [ExpectedCorrectionDeadline__c]   DATETIME        NULL,
    [ReasonAnomalyTA3C__c]            NVARCHAR (MAX)  NULL,
    [Validation_PJ__c]                NVARCHAR (255)  NULL,
    [Perimeter__c]                    NVARCHAR (255)  NULL,
    [Suivi_Evolution_du_Statut__c]    NVARCHAR (10)   NULL,
    [Relance_client__c]               NVARCHAR (255)  NULL,
    [TCHOppIdProcessSous__c]          NVARCHAR (1300) NULL,
    [TCHOptDistributorNetwork__c]     NVARCHAR (1300) NULL,
    [Incident_en_Cours__c]            NVARCHAR (1300) NULL,
    [IsClaimHavingApprovalProcess__c] BIT             NULL,
	[ScoringCardPaymentCeiling__c] [decimal](18, 2) NULL,
    CONSTRAINT [PK_WK_SF_CASE] PRIMARY KEY CLUSTERED ([Id_SF] ASC)
);








GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type de requête', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Type';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motif de traitement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'TreatementReason__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TechCloseCase', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'TechCloseCase__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tech_ReviewDateExpired', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Tech_ReviewDateExpired__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tech_PreviousOwner', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Tech_PreviousOwner__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Sous-Type', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'SubType__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Statut', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Status';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'En Attente de', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'StandBy__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom vendeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'ShopSeller__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Qualification réponse (objet secondaire)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'SecondQualification__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Objet secondaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'SecondarySubject__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant rétrocession', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'RetrocessionAmount__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de la réponse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'ResponseDate__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Canal réponse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'ResponseChannel__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID du type d''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'RecordTypeId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motif de la requête', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Reason';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'File d''attente', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'queueName__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Processus', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Process__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Priorité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Priority';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Objet principal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'PrimarySubject__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Qualification réponse (objet principal)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'PrimaryQualification__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom de l''opportunité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'ParentOppy__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de la requête principale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'ParentId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID du propriétaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'OwnerId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Origine', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Origin__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Canal d''origine', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Origin';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Réseau', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Network__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Statut du jalon', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'MilestoneStatus';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Métier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Metier__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant perte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'LostAmount__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Réclamation niveau', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'LevelClaim__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date lettre d''attente', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'LetterWaitingDate__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'LastModifiedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernière modification par ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'LastModifiedById';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Non traité avec jalon respecté', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'IsNotDoneWithRespMilestone__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Non traité avec jalon dépassé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'isNotDoneExpMilestone__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Requete Non Traitee', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'IsNotDone__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Requetes traitees avec jalon respecte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'IsDoneWithRespMilestone__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Requête traitée avec jalon dépassé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'isDoneWithExpMilestone__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Requete Traitee', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'IsDone__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Requête d''origine', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'InitialCase__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Indemnisation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Indemnity__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Piste d''amélioration', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Improvement__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de la Campagne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'IDCampagne__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de la requête', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Id_SF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Habilitation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Habilitation__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'1ère réclamation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'FirstClaim__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Jalon dépassé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'ExpiredMilestone__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Equipement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Equipment__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DUT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'DUT__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Description', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Description';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type de client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'CustomerType__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'CreatedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID créé par', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'CreatedById';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant geste co', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'CommercialGestAmount__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de fermeture', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'ClosedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro de la requête', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'CaseNumber';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Statut de l''appel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'CallStatus__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Détail Statut Appel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'CallDetail__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date AR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'ARDate__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Plan d''action', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'ActionPlan__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom de la personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'AccountId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom de la boutique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'OBShop__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Plafond Retrait', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'WithdrawalLimit__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Validation PJ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Validation_PJ__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Univers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Universe__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de traitement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'TreatmentDate__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TCHTriggerPass', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'TCHTriggerPass__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Durée traitement (Heures)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'TCHTreatmentTime__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TCHPurge', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'TCHPurge__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tech Nombre jalons fermés', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'TCHNbMilestonesClosed__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TCHIdCase', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'TCHIdCase__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Durée de traitement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'TCHCaseTimeProcessing__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date d''attribution de la requête', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'TCHCaseAttributionOwnerDate__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TCHAuthent', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'TCHAuthent__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Horodateur des modifications du système', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'SystemModstamp';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Téléphone', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'SuppliedPhone';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'SuppliedName';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Adresse e-mail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'SuppliedEmail';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Société', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'SuppliedCompany';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Sous-Univers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'SubUniverse__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Objet', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Subject';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Arrêté depuis', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'StopStartDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Spécification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Specification__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID source', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'SourceId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Heure de début du processus d''autorisation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'SlaStartDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Heure de fin du processus d''autorisation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'SlaExitDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motif à revoir', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'ReviewSubject__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date à revoir', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'ReviewDate__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Réalisé par', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'realizedBy__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de question', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'QuestionId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID produit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'ProductId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PreventAssign', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'PreventAssign__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de planification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'PlanifiedDate__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Présence PJ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'PJPresence__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Plafond Paiement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'PaymentCeiling__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de prochaine action', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'NextSLAStartDate__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nouveau Message', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'NewMessage__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Compteur de message', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'MessageCounter__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Authentification Manuelle', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'ManualAuthent__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Acte de Gestion', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'ManagerialAct__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Responsable', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Manager__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernier affichage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'LastViewedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernière date référencée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'LastReferencedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Visible dans le portail libre-service', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'IsVisibleInSelfService';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Arrêté', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'IsStopped';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fermeture par l''utilisateur du libre-service', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'IsSelfServiceClosed';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Est File d''attente', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'isOwnerQueue__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Escaladée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'IsEscalated';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Supprimé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'IsDeleted';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fermeture lors de la création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'IsClosedOnCreate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fermée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'IsClosed';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gravité de l''incident', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'IncidentGravity__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libre-service commenté', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'HasSelfServiceComments';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Contient Mot(s) Interdit(s)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'HasForbiddenWord__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nouveau commentaire Libre-service', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'HasCommentsUnreadByOwner';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mots interdits', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'ForbiddenWords__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Echéance de correction prévue', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'ExpectedCorrectionDeadline__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID d''autorisation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'EntitlementId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Email source', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'EmailSource__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dixhuit Chars Id', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'EighteenCharId__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Référence Ticket Externe', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'EFrontNumber__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dysfonctionnement Banque', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Dysfunction__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Durée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Duration__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type Document', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'DocumentType__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Détails', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Details__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Précisez', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Detail__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'URL de la photo miniature du créateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'CreatorSmallPhotoUrl';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom du créateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'CreatorName';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'URL de la photo de profil du créateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'CreatorFullPhotoUrl';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° de téléphone du contact', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'ContactPhone';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Téléphone mobile du contact', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'ContactMobile';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID du contact', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'ContactId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Contact Télécopie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'ContactFax';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Adresse e-mail du contact', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'ContactEmail';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Conseiller technique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'ConseillerTechnique__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux de confiance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Confidencelevel__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de zone', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'CommunityId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Commentaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Comments255__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Visibilité Client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'ClientVisibility__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Délai de traitement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'ClaimTreatementTime__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Confirmer l''abandon', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'CancelConfiramtion__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID d''heures d''ouverture', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'BusinessHoursId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Résultat de l''authentification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'AuthenticationResult__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de l''actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'AssetId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Reponse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Answer__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Amount__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Suivi Evolution du Statut', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Suivi_Evolution_du_Statut__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Relance client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Relance_client__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Périmètre', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Perimeter__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Relance client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'TCHOptDistributorNetwork__c';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Relance client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'TCHOppIdProcessSous__c';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IsClaimHavingApprovalProcess', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'IsClaimHavingApprovalProcess__c';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Incident en Cours', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASE', @level2type = N'COLUMN', @level2name = N'Incident_en_Cours__c';
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Score Expérian' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WK_SF_CASE', @level2type=N'COLUMN',@level2name=N'ScoringCardPaymentCeiling__c'
GO