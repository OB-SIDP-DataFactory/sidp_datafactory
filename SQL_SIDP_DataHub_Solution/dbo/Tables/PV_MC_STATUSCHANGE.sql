﻿CREATE TABLE [dbo].[PV_MC_STATUSCHANGE] (
    [Id]            BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [ClientID]      INT                                         NULL,
    [SubscriberKey] NVARCHAR (100)                              NOT NULL,
    [EmailAddress]  NVARCHAR (100)                              NULL,
    [SubscriberID]  INT                                         NULL,
    [OldStatus]     NVARCHAR (10)                               NULL,
    [NewStatus]     NVARCHAR (10)                               NULL,
    [DateChanged]   DATETIME                                    NOT NULL,
    [Startdt]       DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]         DATETIME2 (7) GENERATED ALWAYS AS ROW END   NOT NULL,
    CONSTRAINT [PK_PV_MC_STATUSCHANGE] PRIMARY KEY CLUSTERED ([Id] ASC, [SubscriberKey] ASC, [DateChanged] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_MC_STATUSCHANGEHistory], DATA_CONSISTENCY_CHECK=ON));

