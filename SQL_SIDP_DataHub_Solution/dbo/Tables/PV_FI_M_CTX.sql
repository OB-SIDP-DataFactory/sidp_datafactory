﻿CREATE TABLE [dbo].[PV_FI_M_CTX] (
    [Id]                 INT                                         IDENTITY (1, 1) NOT NULL,
    [COD_ENR]            VARCHAR (2)                                 NULL,
    [TYP_CON]            VARCHAR (3)                                 NULL,
    [TYP_PRE]            VARCHAR (3)                                 NULL,
    [NUM_PRE]            VARCHAR (11)                                NULL,
    [IDE_GRC]            VARCHAR (8)                                 NULL,
    [COD_BAN]            VARCHAR (5)                                 NULL,
    [COD_GUI]            VARCHAR (5)                                 NULL,
    [NUM_CPT]            VARCHAR (11)                                NULL,
    [DEV_CPT]            VARCHAR (3)                                 NULL,
    [DTE_REM_CTX]        DATE                                        NULL,
    [CAP_DU_MEN_ECH]     DECIMAL (9, 2)                              NULL,
    [MTT_MEN_IMP]        DECIMAL (9, 2)                              NULL,
    [INT_MEN_IMP]        DECIMAL (9, 2)                              NULL,
    [IND_LEG]            DECIMAL (9, 2)                              NULL,
    [SLD_AFF_CTX]        DECIMAL (9, 2)                              NULL,
    [DTE_DER_ECR_COM]    DATE                                        NULL,
    [CUM_PER]            DECIMAL (9, 2)                              NULL,
    [COD_POS]            VARCHAR (1)                                 NULL,
    [Validity_StartDate] DATETIME                                    NOT NULL,
    [Validity_EndDate]   DATETIME                                    DEFAULT ('99991231') NOT NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   NOT NULL,
    CONSTRAINT [PK_PV_FI_M_CTX] PRIMARY KEY CLUSTERED ([Id] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_FI_M_CTXHistory], DATA_CONSISTENCY_CHECK=ON))
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code position', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_CTX', @level2type = N'COLUMN', @level2name = N'COD_POS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cumul des pertes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_CTX', @level2type = N'COLUMN', @level2name = N'CUM_PER';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière écriture comptable', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_CTX', @level2type = N'COLUMN', @level2name = N'DTE_DER_ECR_COM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Solde affaire contentieuse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_CTX', @level2type = N'COLUMN', @level2name = N'SLD_AFF_CTX';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Indemnités légales', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_CTX', @level2type = N'COLUMN', @level2name = N'IND_LEG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Intérêts sur les mensualités impayées', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_CTX', @level2type = N'COLUMN', @level2name = N'INT_MEN_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant des mensualités impayées', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_CTX', @level2type = N'COLUMN', @level2name = N'MTT_MEN_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Capital dû sur mensualité à échoir', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_CTX', @level2type = N'COLUMN', @level2name = N'CAP_DU_MEN_ECH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de remise au contentieux', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_CTX', @level2type = N'COLUMN', @level2name = N'DTE_REM_CTX';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Devise du compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_CTX', @level2type = N'COLUMN', @level2name = N'DEV_CPT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro de compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_CTX', @level2type = N'COLUMN', @level2name = N'NUM_CPT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code guichet', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_CTX', @level2type = N'COLUMN', @level2name = N'COD_GUI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code banque', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_CTX', @level2type = N'COLUMN', @level2name = N'COD_BAN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant GRC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_CTX', @level2type = N'COLUMN', @level2name = N'IDE_GRC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro de prestation du contrat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_CTX', @level2type = N'COLUMN', @level2name = N'NUM_PRE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type de prestation du contrat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_CTX', @level2type = N'COLUMN', @level2name = N'TYP_PRE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type de contrat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_CTX', @level2type = N'COLUMN', @level2name = N'TYP_CON';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_CTX', @level2type = N'COLUMN', @level2name = N'COD_ENR';
GO