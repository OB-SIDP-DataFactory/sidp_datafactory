﻿CREATE TABLE [dbo].[REF_MOTIF_SS] (
    [ID_MOT_SS]  INT          IDENTITY (1, 1) NOT NULL,
    [COD_MOT_SS] VARCHAR (3)  NULL,
    [LIB_MOT_SS] VARCHAR (80) NULL,
    CONSTRAINT [PK_REF_MOTIF_SS] PRIMARY KEY CLUSTERED ([ID_MOT_SS] ASC)
);

