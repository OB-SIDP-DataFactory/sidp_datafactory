CREATE TABLE [dbo].[PV_FI_M_COMPLHistory] (
    [Id]                  INT            NOT NULL,
    [COD_ENR]             VARCHAR (2)    NULL,
    [COD_SEQ]             VARCHAR (6)    NULL,
    [NUMERO_ENREG]        VARCHAR (2)    NULL,
    [COD_BAN]             VARCHAR (5)    NULL,
    [COD_GUI_GES]         VARCHAR (5)    NULL,
    [COD_DOM_CPT]         VARCHAR (16)   NULL,
    [NUM_PRE]             VARCHAR (11)   NULL,
    [DTE_OUV_CPT]         DATE           NULL,
    [DEV_CPT]             VARCHAR (3)    NULL,
    [NB_DEC_DEV_CPT]      INT            NULL,
    [MTT_CRE_MAX_AUT]     DECIMAL (9, 2) NULL,
    [MTT_CRE_MAX_AUT_VIR] DECIMAL (9, 2) NULL,
    [MTT_CRE_DIS]         DECIMAL (9, 2) NULL,
    [MTT_CRE_UTI]         DECIMAL (9, 2) NULL,
    [MTT_CRE_DIS_VIR]     DECIMAL (9, 2) NULL,
    [ENC_MOY_MOI]         DECIMAL (9, 2) NULL,
    [MTT_ECH_MOI]         DECIMAL (9, 2) NULL,
    [TX_MOY]              VARCHAR (5)    NULL,
    [COD_STA_SUI_DOS]     VARCHAR (2)    NULL,
    [NB_IMP]              INT            NULL,
    [DTE_FER_CPT]         DATE           NULL,
    [MTT_FRA]             DECIMAL (9, 2) NULL,
    [NB_DEM_VIR]          INT            NULL,
    [MTT_DEM_VIR]         DECIMAL (9, 2) NULL,
    [NB_RET_GUI]          INT            NULL,
    [MTT_RET_GUI]         DECIMAL (9, 2) NULL,
    [NB_REM_ANT]          INT            NULL,
    [MTT_REM_ANT]         DECIMAL (9, 2) NULL,
    [MTT_COM]             DECIMAL (9, 2) NULL,
    [IDE_GRC]             VARCHAR (8)    NULL,
    [DATTTP]              DATE           NULL,
    [PHA_PRO_REC]         VARCHAR (1)    NULL,
    [MTT_IMP]             DECIMAL (9, 2) NULL,
    [NB_IMP_NOU_GES]      INT            NULL,
    [DTE_PRE_IMP]         DATE           NULL,
    [Validity_StartDate]  DATETIME       NOT NULL,
    [Validity_EndDate]    DATETIME       NOT NULL,
    [Startdt]             DATETIME2 (7)  NOT NULL,
    [Enddt]               DATETIME2 (7)  NOT NULL
);
GO
CREATE CLUSTERED INDEX [ix_PV_FI_M_COMPLHistory]
ON [dbo].[PV_FI_M_COMPLHistory]([Enddt] ASC, [Startdt] ASC) WITH (DATA_COMPRESSION = PAGE)
GO
CREATE NONCLUSTERED INDEX IX_NC_CMP_HIS_DAT
ON [dbo].[PV_FI_M_COMPLHistory] ([DATTTP])
INCLUDE ([Id], [COD_ENR], [COD_SEQ], [NUMERO_ENREG], [COD_BAN], [COD_GUI_GES], [COD_DOM_CPT], [NUM_PRE], [DTE_OUV_CPT], [DEV_CPT], [NB_DEC_DEV_CPT], [MTT_CRE_MAX_AUT], [MTT_CRE_MAX_AUT_VIR], [MTT_CRE_DIS], [MTT_CRE_UTI], [MTT_CRE_DIS_VIR], [ENC_MOY_MOI], [MTT_ECH_MOI], [TX_MOY], [COD_STA_SUI_DOS], [NB_IMP], [DTE_FER_CPT], [MTT_FRA], [NB_DEM_VIR], [MTT_DEM_VIR], [NB_RET_GUI], [MTT_RET_GUI], [NB_REM_ANT], [MTT_REM_ANT], [MTT_COM], [IDE_GRC], [PHA_PRO_REC], [MTT_IMP], [NB_IMP_NOU_GES], [DTE_PRE_IMP], [Validity_StartDate], [Validity_EndDate], [Startdt], [Enddt])
GO
CREATE NONCLUSTERED INDEX IX_NC_CMP_HIS_DTE_PHA
ON [dbo].[PV_FI_M_COMPLHistory] ([DTE_FER_CPT], [PHA_PRO_REC], [Validity_StartDate], [Validity_EndDate])
INCLUDE ([NUM_PRE], [DTE_OUV_CPT], [MTT_CRE_MAX_AUT], [DATTTP])
GO
CREATE NONCLUSTERED INDEX IX_NC_CMP_HIS_DTE
ON [dbo].[PV_FI_M_COMPLHistory] ([DTE_FER_CPT], [Validity_StartDate], [Validity_EndDate])
INCLUDE ([NUM_PRE], [DTE_OUV_CPT], [MTT_CRE_MAX_AUT], [DATTTP], [PHA_PRO_REC])
GO
CREATE NONCLUSTERED INDEX IX_NC_CMP_HIS_VLD_DTE
ON [dbo].[PV_FI_M_COMPLHistory] ([Validity_StartDate], [Validity_EndDate])
INCLUDE ([NUM_PRE], [DTE_OUV_CPT], [MTT_CRE_MAX_AUT], [DATTTP])
GO