﻿CREATE TABLE [dbo].[REF_CONS_LOAN_REJECT_REASON] (
    [ID]                         BIGINT         IDENTITY (1, 1) NOT NULL,
    [REJECT_REASON_ID]           DECIMAL (38)   NOT NULL,
    [FRANFINANCE_CODE]           NVARCHAR (255) NULL,
    [REF_FAMILY_ID]              DECIMAL (19)   NULL,
    [CODE]                       NVARCHAR (255) NULL,
    [PRIORITY]                   DECIMAL (19)   NULL,
    [REF_LANG_ID]                DECIMAL (19)   NULL,
    [MESSAGE]                    NVARCHAR (255) NULL,
    [SIMULATION_ALLOWED]         DECIMAL (1)    NULL,
    [ALLOCATED_LOAN_FRANFI_CODE] NVARCHAR (255) NULL,
    [FRANFINANCE_CATEGORY]       NVARCHAR (1)   NULL,
    [Validity_StartDate]         DATETIME2 (7)  NOT NULL,
    [Validity_EndDate]           DATETIME2 (7)  NOT NULL,
    CONSTRAINT [PK_REF_CONS_LOAN_REJECT_REASON] PRIMARY KEY CLUSTERED ([ID] ASC)
);

