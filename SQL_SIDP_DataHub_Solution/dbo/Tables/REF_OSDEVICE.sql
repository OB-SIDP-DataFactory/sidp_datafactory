﻿CREATE TABLE [dbo].[REF_OSDEVICE] (
    [ID]                    BIGINT         IDENTITY (1, 1) NOT NULL,
    [REF_OSDEVICE_ID]       NUMERIC (38)   NULL,
    [WIRECARD_CARD_PROGRAM] NVARCHAR (255) NULL,
    [Validity_StartDate]    DATETIME2 (7)  NOT NULL,
    [Validity_EndDate]      DATETIME2 (7)  NOT NULL,
    CONSTRAINT [PK_REF_OSDEVICE] PRIMARY KEY CLUSTERED ([ID] ASC)
);

