﻿CREATE TABLE [dbo].[IWD_SM_RES_STATE_FACT] (
    [SM_RES_STATE_FACT_KEY]          NUMERIC (19) NOT NULL,
    [START_DATE_TIME_KEY]            INT          NOT NULL,
    [END_DATE_TIME_KEY]              INT          NOT NULL,
    [TENANT_KEY]                     INT          NOT NULL,
    [MEDIA_TYPE_KEY]                 INT          NOT NULL,
    [RESOURCE_KEY]                   INT          NOT NULL,
    [RESOURCE_GROUP_COMBINATION_KEY] INT          NOT NULL,
    [PRIMARY_MEDIA_RESOURCE_KEY]     INT          NOT NULL,
    [RESOURCE_STATE_KEY]             INT          NOT NULL,
    [CREATE_AUDIT_KEY]               NUMERIC (19) NOT NULL,
    [UPDATE_AUDIT_KEY]               NUMERIC (19) NOT NULL,
    [SM_RES_SESSION_FACT_SDT_KEY]    INT          NULL,
    [SM_RES_SESSION_FACT_KEY]        NUMERIC (19) NULL,
    [START_TS]                       INT          NULL,
    [END_TS]                         INT          NULL,
    [START_MSEC]                     NUMERIC (19) NULL,
    [END_MSEC]                       NUMERIC (19) NULL,
    [TOTAL_DURATION]                 INT          NULL,
    [LEAD_CLIP_DURATION]             INT          NULL,
    [TRAIL_CLIP_DURATION]            INT          NULL,
    [ACTIVE_FLAG]                    NUMERIC (1)  NULL,
    [PURGE_FLAG]                     NUMERIC (1)  NULL,
    CONSTRAINT [PK_SM_RSF] PRIMARY KEY CLUSTERED ([SM_RES_STATE_FACT_KEY] ASC)
);
GO
CREATE NONCLUSTERED INDEX [IWD_I_RSSF_SDT]
    ON [dbo].[IWD_SM_RES_STATE_FACT]([START_DATE_TIME_KEY] ASC);

