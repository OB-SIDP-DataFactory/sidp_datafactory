﻿CREATE TABLE [dbo].[OAV_EMPRUNTEURS] (
    [ID_EMPRUNTEURS]               DECIMAL (10)    NOT NULL,
    [ID_FORMES_JURIDIQUES]         DECIMAL (10)    NULL,
    [ID_SITUATIONS_MATRIMONIALES]  DECIMAL (10)    NULL,
    [ID_REGIME_MATRIMONIAL]        DECIMAL (10)    NULL,
    [CIVILITY_ID]                  DECIMAL (10)    NULL,
    [BIRTH_DEPARTMENT_ID]          DECIMAL (10)    NULL,
    [COUNTRY_ID]                   DECIMAL (10)    NULL,
    [NATIONALITY_ID]               DECIMAL (10)    NULL,
    [COUNTRY_SOCIETE_ID]           DECIMAL (10)    NULL,
    [CIVILITE_MANDATAIRE_ID]       DECIMAL (10)    NULL,
    [NATIONALITE_MANDATAIRE_ID]    DECIMAL (10)    NULL,
    [PAYS_MANDATAIRE_ID]           DECIMAL (10)    NULL,
    [TYPE]                         NVARCHAR (1020) NULL,
    [FIRSTNAME]                    NVARCHAR (1020) NULL,
    [LASTNAME]                     NVARCHAR (1020) NULL,
    [FORME_JURIDIQUE_AUTRE]        NVARCHAR (1020) NULL,
    [BIRTH_NAME]                   NVARCHAR (400)  NULL,
    [BIRTH_DATE]                   DATETIME2 (7)   NULL,
    [BIRTH_CITY]                   NVARCHAR (400)  NULL,
    [ADDRESS1]                     NVARCHAR (1020) NULL,
    [ADDRESS2]                     NVARCHAR (1020) NULL,
    [ZIP_CODE]                     NVARCHAR (40)   NULL,
    [CITY]                         NVARCHAR (200)  NULL,
    [HOME_PHONE_NUMBER]            NVARCHAR (80)   NULL,
    [MOBILE_PHONE_NUMBER]          NVARCHAR (80)   NULL,
    [FAX]                          NVARCHAR (80)   NULL,
    [EMAIL]                        NVARCHAR (400)  NULL,
    [PROFESSION]                   NVARCHAR (400)  NULL,
    [INCOMES_OR_PENSIONS]          FLOAT (53)      NULL,
    [FINANCIAL_INCOMES]            FLOAT (53)      NULL,
    [PROPERTY_INCOMES]             FLOAT (53)      NULL,
    [OTHER_INCOMES]                FLOAT (53)      NULL,
    [CHILDREN]                     FLOAT (53)      NULL,
    [ALIMONY]                      FLOAT (53)      NULL,
    [OTHER_CREDITS]                FLOAT (53)      NULL,
    [TAXES]                        FLOAT (53)      NULL,
    [CREATION_DATE]                DATETIME2 (7)   NULL,
    [CLOSING_DATE]                 DATETIME2 (7)   NULL,
    [INSTALLATION_DATE]            DATETIME2 (7)   NULL,
    [CONTACT_DATE]                 DATETIME2 (7)   NULL,
    [TURNOVERS]                    FLOAT (53)      NULL,
    [NET_INCOME]                   FLOAT (53)      NULL,
    [CATEGORY]                     NVARCHAR (120)  NULL,
    [NUM_SIREN]                    NVARCHAR (120)  NULL,
    [ASSURANCE]                    NVARCHAR (400)  NULL,
    [RAISON_SOCIALE]               NVARCHAR (120)  NULL,
    [SMS_EMAIL]                    DECIMAL (1)     NULL,
    [DENOMINATION_SOCIALE]         NVARCHAR (320)  NULL,
    [APE]                          NVARCHAR (320)  NULL,
    [MAIL_PHONE]                   DECIMAL (1)     NULL,
    [CAPITAL_SOCIAL]               NVARCHAR (320)  NULL,
    [PHONE_SOCIETE]                NVARCHAR (80)   NULL,
    [ADDRESS_SOCIETE1]             NVARCHAR (1020) NULL,
    [ADDRESS_SOCIETE2]             NVARCHAR (1020) NULL,
    [FAX_SOCIETE]                  NVARCHAR (80)   NULL,
    [ZIP_CODE_SOCIETE]             NVARCHAR (40)   NULL,
    [EMAIL_SOCIETE]                NVARCHAR (400)  NULL,
    [CITY_SOCIETE]                 NVARCHAR (600)  NULL,
    [GRC_NUMBER]                   NVARCHAR (1020) NULL,
    [NOM_MANDATAIRE]               NVARCHAR (1020) NULL,
    [PRENOM_MANDATAIRE]            NVARCHAR (1020) NULL,
    [NOM_DE_NAISSANCE_MANDATAIRE]  NVARCHAR (1020) NULL,
    [DATE_DE_NAISSANCE_MANDATAIRE] DATETIME2 (7)   NULL,
    [COMMUNE_NAISSANCE_MANDATAIRE] NVARCHAR (1020) NULL,
    [PROFESSION_MANDATAIRE]        NVARCHAR (1020) NULL,
    [RESIDENCE_MANDATAIRE]         DECIMAL (1)     NULL,
    [OPTION_COURRIER_MANDATAIRE]   DECIMAL (1)     NULL,
    [OPTION_SMS_MANDATAIRE]        DECIMAL (1)     NULL,
    [ADRESSE_MANDATAIRE1]          NVARCHAR (1020) NULL,
    [ADRESSE_MANDATAIRE2]          NVARCHAR (1020) NULL,
    [CODE_POSTAL_MANDATAIRE]       NVARCHAR (1020) NULL,
    [VILLE_MANDATAIRE]             NVARCHAR (1020) NULL,
    [TELEPHONE_FIX_MANDATAIRE]     NVARCHAR (1020) NULL,
    [TELEPHONE_MOBILE_MANDATAIRE]  NVARCHAR (1020) NULL,
    [FAX_MANDATAIRE]               NVARCHAR (1020) NULL,
    [EMAIL_MANDATAIRE]             NVARCHAR (1020) NULL,
    [FONCTION]                     NVARCHAR (1020) NULL,
    [IS_PERSMOR]                   DECIMAL (10)    NULL,
    [TYPE_OCCUPATION_ID]           DECIMAL (10)    NULL,
    [TYPE_CONTRAT_ID]              DECIMAL (10)    NULL,
    [ID_LIEN_EMPRUNTEURS]          DECIMAL (10)    NULL,
    [ASSURANCE_SUBSCRIBE]          DECIMAL (1)     NULL,
    [JUSTIFICATIF_SANTE]           NVARCHAR (200)  NULL,
    [CLIENT_OB]                    NVARCHAR (1020) NULL,
    [DATE_CLIENT_OB]               DATETIME2 (7)   NULL,
    [CONSEIL_ASSURANCE]            NVARCHAR (80)   NULL,
    [SANTE_QUESTION_UN]            NVARCHAR (4)    NULL,
    [SANTE_QUESTION_DEUX]          NVARCHAR (4)    NULL,
    [SANTE_QUESTION_TROIS]         NVARCHAR (4)    NULL,
    [SANTE_QUESTION_QUATRE]        NVARCHAR (4)    NULL,
    [CODE_POSTAL_NAISSANCE]        NVARCHAR (1020) NULL,
    [DATE_ANCIENNETE_ASSURANCE]    DATETIME2 (7)   NULL,
    [EMPLOYEUR]                    NVARCHAR (1020) NULL,
    [DATE_ENTREE_EMPLOYEUR]        DATETIME2 (7)   NULL,
    [CODE_POSTAL_EMPLOYEUR]        NVARCHAR (1020) NULL,
    [VILLE_EMPLOYEUR]              NVARCHAR (1020) NULL,
    [ADRESSE1EMPLOYEUR]            NVARCHAR (1020) NULL,
    [ADRESSE2EMPLOYEUR]            NVARCHAR (1020) NULL,
    [TELEPHONE_EMPLOYEUR]          NVARCHAR (1020) NULL,
    [RENT]                         FLOAT (53)      NULL,
    [CREDIT_OB]                    FLOAT (53)      NULL,
    [DATE_DEBUT_CONTRAT]           DATETIME2 (7)   NULL,
    [DATE_FIN_CONTRAT]             DATETIME2 (7)   NULL,
    [ID_CSPS]                      DECIMAL (10)    NULL,
    [ANCIENNETE_ASSURANCE]         NVARCHAR (1020) NULL,
    [PREATTRIBUTION]               NVARCHAR (1020) NULL,
    [AGE]                          NVARCHAR (1020) NULL,
    [BIRTH_COUNTRY]                NVARCHAR (1020) NULL,
    [DAT_OBSR]                     DATE            CONSTRAINT [DefValStartDatOAV_EMPRUNTEURS] DEFAULT (getdate()) NOT NULL,
    [DAT_CHRG]                     DATETIME2 (7)   CONSTRAINT [DefValEndDatOAV_EMPRUNTEURS] DEFAULT (CONVERT([date],'9999-12-31')) NOT NULL,
    CONSTRAINT [PK_PrimaryKeyEMPRUNTEURS] PRIMARY KEY CLUSTERED ([ID_EMPRUNTEURS] ASC)
);


