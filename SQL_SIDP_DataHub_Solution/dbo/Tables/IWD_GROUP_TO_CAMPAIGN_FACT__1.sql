﻿CREATE TABLE [dbo].[IWD_GROUP_TO_CAMPAIGN_FACT_] (
    [GROUP_TO_CAMPAIGN_FACT_KEY] NUMERIC (19) NOT NULL,
    [GROUP_KEY]                  INT          NOT NULL,
    [CAMPAIGN_KEY]               INT          NOT NULL,
    [TENANT_KEY]                 INT          NOT NULL,
    [START_DATE_TIME_KEY]        INT          NOT NULL,
    [END_DATE_TIME_KEY]          INT          NOT NULL,
    [CREATE_AUDIT_KEY]           NUMERIC (19) NOT NULL,
    [UPDATE_AUDIT_KEY]           NUMERIC (19) NOT NULL,
    [START_TS]                   INT          NULL,
    [END_TS]                     INT          NULL,
    [IDB_ID]                     NUMERIC (19) NOT NULL,
    [ACTIVE_FLAG]                NUMERIC (1)  NULL,
    [PURGE_FLAG]                 NUMERIC (1)  NULL,
    CONSTRAINT [PK_GR_TO_CMPGN_FT] PRIMARY KEY CLUSTERED ([GROUP_TO_CAMPAIGN_FACT_KEY] ASC)
);
GO
CREATE UNIQUE NONCLUSTERED INDEX [IWD_I_GPCM_STS_CM_GR]
    ON [dbo].[IWD_GROUP_TO_CAMPAIGN_FACT_]([START_TS] ASC, [CAMPAIGN_KEY] ASC, [GROUP_KEY] ASC);
GO
