﻿CREATE TABLE [dbo].[WK_SF_PROFILE] (
    [Id_SF]                                     NVARCHAR (50)  NOT NULL,
    [Name]                                      NVARCHAR (255) NULL,
    [CreatedDate]                               DATETIME       NULL,
    [LastModifiedDate]                          DATETIME       NULL,
    [PermissionsEmailSingle]                    NVARCHAR (10)  NULL,
    [PermissionsEmailMass]                      NVARCHAR (10)  NULL,
    [PermissionsEditTask]                       NVARCHAR (10)  NULL,
    [PermissionsEditEvent]                      NVARCHAR (10)  NULL,
    [PermissionsExportReport]                   NVARCHAR (10)  NULL,
    [PermissionsImportPersonal]                 NVARCHAR (10)  NULL,
    [PermissionsDataExport]                     NVARCHAR (10)  NULL,
    [PermissionsManageUsers]                    NVARCHAR (10)  NULL,
    [PermissionsEditPublicFilters]              NVARCHAR (10)  NULL,
    [PermissionsEditPublicTemplates]            NVARCHAR (10)  NULL,
    [PermissionsModifyAllData]                  NVARCHAR (10)  NULL,
    [PermissionsManageCases]                    NVARCHAR (10)  NULL,
    [PermissionsMassInlineEdit]                 NVARCHAR (10)  NULL,
    [PermissionsEditKnowledge]                  NVARCHAR (10)  NULL,
    [PermissionsManageKnowledge]                NVARCHAR (10)  NULL,
    [PermissionsManageSolutions]                NVARCHAR (10)  NULL,
    [PermissionsCustomizeApplication]           NVARCHAR (10)  NULL,
    [PermissionsEditReadonlyFields]             NVARCHAR (10)  NULL,
    [PermissionsRunReports]                     NVARCHAR (10)  NULL,
    [PermissionsViewSetup]                      NVARCHAR (10)  NULL,
    [PermissionsTransferAnyEntity]              NVARCHAR (10)  NULL,
    [PermissionsNewReportBuilder]               NVARCHAR (10)  NULL,
    [PermissionsManageCssUsers]                 NVARCHAR (10)  NULL,
    [PermissionsActivateContract]               NVARCHAR (10)  NULL,
    [PermissionsActivateOrder]                  NVARCHAR (10)  NULL,
    [PermissionsImportLeads]                    NVARCHAR (10)  NULL,
    [PermissionsManageLeads]                    NVARCHAR (10)  NULL,
    [PermissionsTransferAnyLead]                NVARCHAR (10)  NULL,
    [PermissionsViewAllData]                    NVARCHAR (10)  NULL,
    [PermissionsEditPublicDocuments]            NVARCHAR (10)  NULL,
    [PermissionsViewEncryptedData]              NVARCHAR (10)  NULL,
    [PermissionsEditBrandTemplates]             NVARCHAR (10)  NULL,
    [PermissionsEditHtmlTemplates]              NVARCHAR (10)  NULL,
    [PermissionsChatterInternalUser]            NVARCHAR (10)  NULL,
    [PermissionsManageTranslation]              NVARCHAR (10)  NULL,
    [PermissionsDeleteActivatedContract]        NVARCHAR (10)  NULL,
    [PermissionsChatterInviteExternalUsers]     NVARCHAR (10)  NULL,
    [PermissionsSendSitRequests]                NVARCHAR (10)  NULL,
    [PermissionsApiUserOnly]                    NVARCHAR (10)  NULL,
    [PermissionsManageRemoteAccess]             NVARCHAR (10)  NULL,
    [PermissionsCanUseNewDashboardBuilder]      NVARCHAR (10)  NULL,
    [PermissionsManageCategories]               NVARCHAR (10)  NULL,
    [PermissionsConvertLeads]                   NVARCHAR (10)  NULL,
    [PermissionsPasswordNeverExpires]           NVARCHAR (10)  NULL,
    [PermissionsUseTeamReassignWizards]         NVARCHAR (10)  NULL,
    [PermissionsEditActivatedOrders]            NVARCHAR (10)  NULL,
    [PermissionsInstallMultiforce]              NVARCHAR (10)  NULL,
    [PermissionsPublishMultiforce]              NVARCHAR (10)  NULL,
    [PermissionsChatterOwnGroups]               NVARCHAR (10)  NULL,
    [PermissionsEditOppLineItemUnitPrice]       NVARCHAR (10)  NULL,
    [PermissionsCreateMultiforce]               NVARCHAR (10)  NULL,
    [PermissionsBulkApiHardDelete]              NVARCHAR (10)  NULL,
    [PermissionsInboundMigrationToolsUser]      NVARCHAR (10)  NULL,
    [PermissionsSolutionImport]                 NVARCHAR (10)  NULL,
    [PermissionsManageCallCenters]              NVARCHAR (10)  NULL,
    [PermissionsPortalSuperUser]                NVARCHAR (10)  NULL,
    [PermissionsManageSynonyms]                 NVARCHAR (10)  NULL,
    [PermissionsOutboundMigrationToolsUser]     NVARCHAR (10)  NULL,
    [PermissionsDelegatedPortalUserAdmin]       NVARCHAR (10)  NULL,
    [PermissionsViewContent]                    NVARCHAR (10)  NULL,
    [PermissionsManageEmailClientConfig]        NVARCHAR (10)  NULL,
    [PermissionsEnableNotifications]            NVARCHAR (10)  NULL,
    [PermissionsManageDataIntegrations]         NVARCHAR (10)  NULL,
    [PermissionsDistributeFromPersWksp]         NVARCHAR (10)  NULL,
    [PermissionsViewDataCategories]             NVARCHAR (10)  NULL,
    [PermissionsManageDataCategories]           NVARCHAR (10)  NULL,
    [PermissionsAuthorApex]                     NVARCHAR (10)  NULL,
    [PermissionsManageMobile]                   NVARCHAR (10)  NULL,
    [PermissionsApiEnabled]                     NVARCHAR (10)  NULL,
    [PermissionsManageCustomReportTypes]        NVARCHAR (10)  NULL,
    [PermissionsEditCaseComments]               NVARCHAR (10)  NULL,
    [PermissionsTransferAnyCase]                NVARCHAR (10)  NULL,
    [PermissionsContentAdministrator]           NVARCHAR (10)  NULL,
    [PermissionsCreateWorkspaces]               NVARCHAR (10)  NULL,
    [PermissionsManageContentPermissions]       NVARCHAR (10)  NULL,
    [PermissionsManageContentProperties]        NVARCHAR (10)  NULL,
    [PermissionsManageContentTypes]             NVARCHAR (10)  NULL,
    [PermissionsScheduleJob]                    NVARCHAR (10)  NULL,
    [PermissionsManageExchangeConfig]           NVARCHAR (10)  NULL,
    [PermissionsManageAnalyticSnapshots]        NVARCHAR (10)  NULL,
    [PermissionsScheduleReports]                NVARCHAR (10)  NULL,
    [PermissionsManageBusinessHourHolidays]     NVARCHAR (10)  NULL,
    [PermissionsManageEntitlements]             NVARCHAR (10)  NULL,
    [PermissionsManageInteraction]              NVARCHAR (10)  NULL,
    [PermissionsViewMyTeamsDashboards]          NVARCHAR (10)  NULL,
    [PermissionsModerateChatter]                NVARCHAR (10)  NULL,
    [PermissionsResetPasswords]                 NVARCHAR (10)  NULL,
    [PermissionsFlowUFLRequired]                NVARCHAR (10)  NULL,
    [PermissionsCanInsertFeedSystemFields]      NVARCHAR (10)  NULL,
    [PermissionsManageKnowledgeImportExport]    NVARCHAR (10)  NULL,
    [PermissionsDeferSharingCalculations]       NVARCHAR (10)  NULL,
    [PermissionsEmailTemplateManagement]        NVARCHAR (10)  NULL,
    [PermissionsEmailAdministration]            NVARCHAR (10)  NULL,
    [PermissionsManageChatterMessages]          NVARCHAR (10)  NULL,
    [PermissionsChatterFileLink]                NVARCHAR (10)  NULL,
    [PermissionsForceTwoFactor]                 NVARCHAR (10)  NULL,
    [PermissionsViewEventLogFiles]              NVARCHAR (10)  NULL,
    [PermissionsManageNetworks]                 NVARCHAR (10)  NULL,
    [PermissionsManageAuthProviders]            NVARCHAR (10)  NULL,
    [PermissionsRunFlow]                        NVARCHAR (10)  NULL,
    [PermissionsCreateCustomizeDashboards]      NVARCHAR (10)  NULL,
    [PermissionsCreateDashboardFolders]         NVARCHAR (10)  NULL,
    [PermissionsViewPublicDashboards]           NVARCHAR (10)  NULL,
    [PermissionsManageDashbdsInPubFolders]      NVARCHAR (10)  NULL,
    [PermissionsCreateCustomizeReports]         NVARCHAR (10)  NULL,
    [PermissionsCreateReportFolders]            NVARCHAR (10)  NULL,
    [PermissionsViewPublicReports]              NVARCHAR (10)  NULL,
    [PermissionsManageReportsInPubFolders]      NVARCHAR (10)  NULL,
    [PermissionsEditMyDashboards]               NVARCHAR (10)  NULL,
    [PermissionsEditMyReports]                  NVARCHAR (10)  NULL,
    [PermissionsViewAllUsers]                   NVARCHAR (10)  NULL,
    [PermissionsAllowUniversalSearch]           NVARCHAR (10)  NULL,
    [PermissionsConnectOrgToEnvironmentHub]     NVARCHAR (10)  NULL,
    [PermissionsCreateCustomizeFilters]         NVARCHAR (10)  NULL,
    [PermissionsGovernNetworks]                 NVARCHAR (10)  NULL,
    [PermissionsSalesConsole]                   NVARCHAR (10)  NULL,
    [PermissionsTwoFactorApi]                   NVARCHAR (10)  NULL,
    [PermissionsDeleteTopics]                   NVARCHAR (10)  NULL,
    [PermissionsEditTopics]                     NVARCHAR (10)  NULL,
    [PermissionsCreateTopics]                   NVARCHAR (10)  NULL,
    [PermissionsAssignTopics]                   NVARCHAR (10)  NULL,
    [PermissionsIdentityEnabled]                NVARCHAR (10)  NULL,
    [PermissionsIdentityConnect]                NVARCHAR (10)  NULL,
    [PermissionsAllowViewKnowledge]             NVARCHAR (10)  NULL,
    [PermissionsContentWorkspaces]              NVARCHAR (10)  NULL,
    [PermissionsManageSearchPromotionRules]     NVARCHAR (10)  NULL,
    [PermissionsCustomMobileAppsAccess]         NVARCHAR (10)  NULL,
    [PermissionsViewHelpLink]                   NVARCHAR (10)  NULL,
    [PermissionsManageProfilesPermissionsets]   NVARCHAR (10)  NULL,
    [PermissionsAssignPermissionSets]           NVARCHAR (10)  NULL,
    [PermissionsManageRoles]                    NVARCHAR (10)  NULL,
    [PermissionsManageIpAddresses]              NVARCHAR (10)  NULL,
    [PermissionsManageSharing]                  NVARCHAR (10)  NULL,
    [PermissionsManageInternalUsers]            NVARCHAR (10)  NULL,
    [PermissionsManagePasswordPolicies]         NVARCHAR (10)  NULL,
    [PermissionsManageLoginAccessPolicies]      NVARCHAR (10)  NULL,
    [PermissionsManageCustomPermissions]        NVARCHAR (10)  NULL,
    [PermissionsCanVerifyComment]               NVARCHAR (10)  NULL,
    [PermissionsManageUnlistedGroups]           NVARCHAR (10)  NULL,
    [PermissionsManageTwoFactor]                NVARCHAR (10)  NULL,
    [PermissionsLightningExperienceUser]        NVARCHAR (10)  NULL,
    [PermissionsConfigCustomRecs]               NVARCHAR (10)  NULL,
    [PermissionsSubmitMacrosAllowed]            NVARCHAR (10)  NULL,
    [PermissionsBulkMacrosAllowed]              NVARCHAR (10)  NULL,
    [PermissionsShareInternalArticles]          NVARCHAR (10)  NULL,
    [PermissionsManageSessionPermissionSets]    NVARCHAR (10)  NULL,
    [PermissionsSendAnnouncementEmails]         NVARCHAR (10)  NULL,
    [PermissionsChatterEditOwnPost]             NVARCHAR (10)  NULL,
    [PermissionsChatterEditOwnRecordPost]       NVARCHAR (10)  NULL,
    [PermissionsCreateAuditFields]              NVARCHAR (10)  NULL,
    [PermissionsUpdateWithInactiveOwner]        NVARCHAR (10)  NULL,
    [PermissionsAssignUserToSkill]              NVARCHAR (10)  NULL,
    [PermissionsImportCustomObjects]            NVARCHAR (10)  NULL,
    [PermissionsDelegatedTwoFactor]             NVARCHAR (10)  NULL,
    [PermissionsChatterComposeUiCodesnippet]    NVARCHAR (10)  NULL,
    [PermissionsSelectFilesFromSalesforce]      NVARCHAR (10)  NULL,
    [PermissionsModerateNetworkUsers]           NVARCHAR (10)  NULL,
    [PermissionsMergeTopics]                    NVARCHAR (10)  NULL,
    [PermissionsSubscribeToLightningReports]    NVARCHAR (10)  NULL,
    [PermissionsManagePvtRptsAndDashbds]        NVARCHAR (10)  NULL,
    [PermissionsCampaignInfluence2]             NVARCHAR (10)  NULL,
    [PermissionsViewDataAssessment]             NVARCHAR (10)  NULL,
    [PermissionsRemoveDirectMessageMembers]     NVARCHAR (10)  NULL,
    [PermissionsCanApproveFeedPost]             NVARCHAR (10)  NULL,
    [PermissionsAddDirectMessageMembers]        NVARCHAR (10)  NULL,
    [PermissionsAllowViewEditConvertedLeads]    NVARCHAR (10)  NULL,
    [PermissionsShowCompanyNameAsUserBadge]     NVARCHAR (10)  NULL,
    [PermissionsAccessCMC]                      NVARCHAR (10)  NULL,
    [PermissionsViewHealthCheck]                NVARCHAR (10)  NULL,
    [PermissionsManageHealthCheck]              NVARCHAR (10)  NULL,
    [PermissionsPackaging2]                     NVARCHAR (10)  NULL,
    [PermissionsManageCertificates]             NVARCHAR (10)  NULL,
    [PermissionsCreateReportInLightning]        NVARCHAR (10)  NULL,
    [PermissionsPreventClassicExperience]       NVARCHAR (10)  NULL,
    [PermissionsHideReadByList]                 NVARCHAR (10)  NULL,
    [PermissionsListEmailSend]                  NVARCHAR (10)  NULL,
    [PermissionsFeedPinning]                    NVARCHAR (10)  NULL,
    [PermissionsUseWebLink]                     NVARCHAR (10)  NULL,
    [PermissionsViewAllActivities]              NVARCHAR (10)  NULL,
    [PermissionsSubscribeReportToOtherUsers]    NVARCHAR (10)  NULL,
    [PermissionsLightningConsoleAllowedForUser] NVARCHAR (10)  NULL,
    [PermissionsSubscribeReportsRunAsUser]      NVARCHAR (10)  NULL,
    [PermissionsSubscribeToLightningDashboards] NVARCHAR (10)  NULL,
    [PermissionsApexRestServices]               NVARCHAR (10)  NULL,
    [PermissionsEnableCommunityAppLauncher]     NVARCHAR (10)  NULL,
    [PermissionsRecordVisibilityAPI]            NVARCHAR (10)  NULL,
    [UserLicenseId]                             NVARCHAR (18)  NULL,
    [UserType]                                  NVARCHAR (40)  NULL,
    [CreatedById]                               NVARCHAR (18)  NULL,
    [LastModifiedById]                          NVARCHAR (18)  NULL,
    [SystemModstamp]                            DATETIME       NULL,
    [Description]                               NVARCHAR (255) NULL,
    [LastViewedDate]                            DATETIME       NULL,
    [LastReferencedDate]                        DATETIME       NULL,
    CONSTRAINT [PK_WK_SF_PROFILE] PRIMARY KEY CLUSTERED ([Id_SF] ASC)
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'Name';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'LastModifiedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de profil', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'Id_SF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'CreatedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type d''utilisateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'UserType';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de licence utilisateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'UserLicenseId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Horodateur des modifications du système', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'SystemModstamp';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Afficher la configuration', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsViewSetup';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Afficher des rapports dans des dossiers publics', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsViewPublicReports';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Afficher des tableaux de bord dans des dossiers publics', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsViewPublicDashboards';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Afficher les tableaux de bord de mon équipe', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsViewMyTeamsDashboards';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Afficher le lien d''aide', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsViewHelpLink';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Afficher le contrôle d''intégrité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsViewHealthCheck';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Afficher les fichiers journaux des événements', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsViewEventLogFiles';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Afficher les données cryptées', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsViewEncryptedData';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Afficher les catégories de données dans la Configuration', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsViewDataCategories';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Accéder à l''affichage Évaluation des données', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsViewDataAssessment';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Afficher le contenu dans les portails', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsViewContent';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Afficher tous les utilisateurs', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsViewAllUsers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Afficher toutes les données', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsViewAllData';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Afficher toutes les activités', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsViewAllActivities';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autoriser l''accès aux actions personnalisées', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsUseWebLink';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Utiliser les assistants de réattribution de l''équipe', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsUseTeamReassignWizards';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mettre à jour les enregistrements avec des propriétaires inactifs', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsUpdateWithInactiveOwner';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Authentification à deux facteurs pour les connexions API', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsTwoFactorApi';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Transférer les pistes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsTransferAnyLead';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Transférer l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsTransferAnyEntity';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Transférer des requêtes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsTransferAnyCase';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'S’abonner à des rapports', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsSubscribeToLightningReports';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'S''abonner aux tableaux de bord', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsSubscribeToLightningDashboards';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Abonnement aux rapports : ajouter des destinataires', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsSubscribeReportToOtherUsers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Abonnement aux rapports : définir l''utilisateur actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsSubscribeReportsRunAsUser';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les macros que les utilisateurs ne peuvent pas annuler', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsSubmitMacrosAllowed';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Importer des solutions', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsSolutionImport';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Afficher le nom de la société en tant que rôle de communauté', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsShowCompanyNameAsUserBadge';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Partager les articles Knowledge internes avec l''extérieur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsShareInternalArticles';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Envoyer des demandes Rester en contact', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsSendSitRequests';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Envoyer les e-mails d''annonce', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsSendAnnouncementEmails';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Sélectionner des fichiers depuis Salesforce', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsSelectFilesFromSalesforce';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Planifier les rapports', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsScheduleReports';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Planifier les tableaux de bord', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsScheduleJob';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Sales Console', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsSalesConsole';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Exécuter les rapports', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsRunReports';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Exécuter des flux', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsRunFlow';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Réinitialiser les mots de passe utilisateur et déverrouiller les utilisateurs', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsResetPasswords';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Retirer les personnes des messages privés', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsRemoveDirectMessageMembers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Activer l''API RecordVisibility', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsRecordVisibilityAPI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Transférer les packages AppExchange', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsPublishMultiforce';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Masquer l''option de basculement vers Salesforce Classic', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsPreventClassicExperience';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Super utilisateur du portail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsPortalSuperUser';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Le mot de passe n''expire jamais', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsPasswordNeverExpires';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Créer et mettre à jour des packages de deuxième génération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsPackaging2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Créer et télécharger des ensembles de modifications', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsOutboundMigrationToolsUser';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Générateur de rapport', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsNewReportBuilder';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Modifier toutes les données', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsModifyAllData';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Modérer les utilisateurs de communautés', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsModerateNetworkUsers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Modérer Chatter', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsModerateChatter';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fusionner les rubriques', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsMergeTopics';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Modifier en masse à partir de listes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsMassInlineEdit';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les utilisateurs', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageUsers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les groupes non répertoriés', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageUnlistedGroups';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer l''authentification à deux facteurs dans l''API', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageTwoFactor';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer la traduction', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageTranslation';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les synonymes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageSynonyms';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les solutions publiées', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageSolutions';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer le partage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageSharing';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les activations d''ensembles d''autorisations de session', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageSessionPermissionSets';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les termes de recherche promus', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageSearchPromotionRules';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les rôles', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageRoles';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer des rapports dans des dossiers publics', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageReportsInPubFolders';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les applications connectées', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageRemoteAccess';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer tous les rapports et les tableaux de bord privés', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManagePvtRptsAndDashbds';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les profils et les ensembles d''autorisations', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageProfilesPermissionsets';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les stratégies de mot de passe', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManagePasswordPolicies';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Créer et configurer des communautés', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageNetworks';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les configurations mobiles', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageMobile';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les stratégies d''accès à la connexion', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageLoginAccessPolicies';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les pistes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageLeads';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer l''importation/exportation d''articles Knowledge', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageKnowledgeImportExport';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer Salesforce Knowledge', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageKnowledge';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les adresses IP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageIpAddresses';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les utilisateurs internes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageInternalUsers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les flux', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageInteraction';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer le contrôle d''intégrité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageHealthCheck';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer Lightning Sync', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageExchangeConfig';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les autorisations', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageEntitlements';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les configurations de clients de messagerie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageEmailClientConfig';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les intégrations de données', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageDataIntegrations';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les catégories de données', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageDataCategories';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer des tableaux de bord dans des dossiers publics', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageDashbdsInPubFolders';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les types de rapport personnalisé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageCustomReportTypes';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les autorisations personnalisées', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageCustomPermissions';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les utilisateurs clients', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageCssUsers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les types d''enregistrement et les présentations des fichiers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageContentTypes';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les propriétés du contenu', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageContentProperties';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les autorisations de contenu', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageContentPermissions';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les messages Chatter et les messages privés', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageChatterMessages';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les certificats', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageCertificates';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les catégories', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageCategories';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les requêtes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageCases';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les centres d''appels', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageCallCenters';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les vacances pendant les heures ouvrables', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageBusinessHourHolidays';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les fournisseurs d''authentification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageAuthProviders';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les instantanés de rapports', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsManageAnalyticSnapshots';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autoriser l''envoi d''e-mails de liste', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsListEmailSend';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Utilisateur de Lightning Experience', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsLightningExperienceUser';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Utilisateur de Lightning Console', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsLightningConsoleAllowedForUser';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Télécharger les packages AppExchange', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsInstallMultiforce';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Déployer les ensembles de modifications', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsInboundMigrationToolsUser';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Importer des contacts personnels', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsImportPersonal';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Importer des pistes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsImportLeads';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Importer des objets personnalisés', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsImportCustomObjects';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Utiliser les fonctionnalités d''identité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsIdentityEnabled';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Utiliser Identity Connect', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsIdentityConnect';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Masquer la liste Vu par', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsHideReadByList';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les communautés', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsGovernNetworks';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Authentification à deux facteurs pour les connexions à l''interface utilisateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsForceTwoFactor';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nécessite une licence de fonctionnalité Flow User', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsFlowUFLRequired';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Épingler les publications dans les fils', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsFeedPinning';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Exporter les rapports', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsExportReport';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Envoyer les messages sortants', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsEnableNotifications';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Afficher le Lanceur d''application dans les communautés', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsEnableCommunityAppLauncher';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les modèles d''e-mail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsEmailTemplateManagement';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Envoyer un e-mail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsEmailSingle';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Envoyer des messages en masse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsEmailMass';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Administration des e-mails', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsEmailAdministration';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Modifier les rubriques', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsEditTopics';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Modifier les tâches', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsEditTask';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Modifier les champs en lecture seule', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsEditReadonlyFields';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les modèles d''e-mail Classic publics', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsEditPublicTemplates';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les vues de liste publique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsEditPublicFilters';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les documents publics', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsEditPublicDocuments';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Modifier le prix de vente du produit de l''opportunité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsEditOppLineItemUnitPrice';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Modifier mes rapports', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsEditMyReports';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Modifier mes tableaux de bord', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsEditMyDashboards';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les articles', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsEditKnowledge';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Modifier les modèles HTML', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsEditHtmlTemplates';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Modifier les événements', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsEditEvent';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Modifier les commentaires de requête', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsEditCaseComments';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer les en-têtes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsEditBrandTemplates';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Modifier les commandes activées', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsEditActivatedOrders';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Créer des livraisons de contenu', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsDistributeFromPersWksp';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Supprimer les rubriques', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsDeleteTopics';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Supprimer les contrats activés', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsDeleteActivatedContract';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer l''authentification à deux facteurs dans l''interface utilisateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsDelegatedTwoFactor';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Administrateur utilisateur externe délégué', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsDelegatedPortalUserAdmin';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer le report du calcul de partage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsDeferSharingCalculations';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Exportation hebdomadaire de données', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsDataExport';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Accéder à des applications mobiles personnalisées', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsCustomMobileAppsAccess';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Personnaliser l''application', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsCustomizeApplication';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Créer des bibliothèques', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsCreateWorkspaces';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Créer des rubriques', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsCreateTopics';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Générateur de rapport (Lightning Experience)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsCreateReportInLightning';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Créer des dossiers de rapport', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsCreateReportFolders';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Créer des packages AppExchange', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsCreateMultiforce';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Créer des dossiers de tableau de bord', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsCreateDashboardFolders';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Créer et personnaliser des rapports', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsCreateCustomizeReports';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Créer et personnaliser des vues de liste', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsCreateCustomizeFilters';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Créer et personnaliser des tableaux de bord', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsCreateCustomizeDashboards';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Définir des champs d''audit lors de la création d''enregistrements', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsCreateAuditFields';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Convertir les pistes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsConvertLeads';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Accéder aux bibliothèques', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsContentWorkspaces';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérer Salesforce CRM Content', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsContentAdministrator';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Connecter l''organisation à la plate-forme d''environnement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsConnectOrgToEnvironmentHub';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Configurer des recommandations personnalisées', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsConfigCustomRecs';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Créer et être propriétaire de nouveaux groupes Chatter', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsChatterOwnGroups';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Inviter des clients dans Chatter', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsChatterInviteExternalUsers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Utilisateur interne de Chatter', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsChatterInternalUser';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Créer des liens publics', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsChatterFileLink';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Modifier les publications dans mes enregistrements', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsChatterEditOwnRecordPost';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Modifier mes propres publications', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsChatterEditOwnPost';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autoriser l''insertion d''extraits de code depuis l''interface utilisateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsChatterComposeUiCodesnippet';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Vérifier les réponses aux questions Chatter', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsCanVerifyComment';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Générateur de tableau de bord glisser-déposer', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsCanUseNewDashboardBuilder';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Insérer des valeurs de champ système pour les fils Chatter', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsCanInsertFeedSystemFields';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Peut approuver les publications et les commentaires de fil', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsCanApproveFeedPost';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Influence de la campagne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsCampaignInfluence2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Exécuter les macros sur plusieurs enregistrements', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsBulkMacrosAllowed';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Suppression définitive de l''API de transfert en masse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsBulkApiHardDelete';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Apex - Auteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsAuthorApex';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Attribuer des compétences Live Agent aux utilisateurs', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsAssignUserToSkill';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Attribuer des rubriques', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsAssignTopics';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Attribuer des ensembles d''autorisations', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsAssignPermissionSets';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Utilisateur API uniquement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsApiUserOnly';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'API activée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsApiEnabled';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Services REST Apex', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsApexRestServices';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autoriser l''affichage de Knowledge', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsAllowViewKnowledge';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Afficher et modifier les pistes converties', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsAllowViewEditConvertedLeads';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Knowledge One', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsAllowUniversalSearch';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ajouter les personnes aux messages privés', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsAddDirectMessageMembers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Activer les commandes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsActivateOrder';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Activer les contrats', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsActivateContract';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Accéder à la Gestion de la communauté', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'PermissionsAccessCMC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernier affichage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'LastViewedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernière date référencée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'LastReferencedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernière modification par ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'LastModifiedById';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Description', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'Description';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID créé par', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_PROFILE', @level2type = N'COLUMN', @level2name = N'CreatedById';

