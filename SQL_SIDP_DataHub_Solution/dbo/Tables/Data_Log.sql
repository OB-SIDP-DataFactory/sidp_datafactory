﻿CREATE TABLE [dbo].[Data_Log] (
    [Id]                  BIGINT         IDENTITY (1, 1) NOT NULL,
    [ProcessingType]      NVARCHAR (50)  NOT NULL,
    [ETLName]             NVARCHAR (100) NOT NULL,
    [FileOrTableName]     NVARCHAR (255) NOT NULL,
    [ProcessingStartDate] DATETIME       NOT NULL,
    [ProcessingEndDate]   DATETIME       NULL,
    [ProcessingStatus]    NVARCHAR (50)  NULL,
    [NbRecordsProcessed]  INT            NULL,
    [NbRecordsCollected]  INT            NULL,
    [NbRecordsExtracted]  INT            NULL,
    CONSTRAINT [PK_Data_Log] PRIMARY KEY CLUSTERED ([Id] ASC)
)
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The processing type : Collect, Integration, Business View Generation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Data_Log', @level2type = N'COLUMN', @level2name = N'ProcessingType';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Name of the ETL package', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Data_Log', @level2type = N'COLUMN', @level2name = N'ETLName';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Staus : OK, KO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Data_Log', @level2type = N'COLUMN', @level2name = N'ProcessingStatus';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Only for Collect', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Data_Log', @level2type = N'COLUMN', @level2name = N'NbRecordsCollected';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Only for Collect', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Data_Log', @level2type = N'COLUMN', @level2name = N'NbRecordsExtracted';
GO
CREATE NONCLUSTERED INDEX [IX_NC_DTA_LOG_ETL_TBL_NAM]
ON [dbo].[Data_Log] ([ETLName],[FileOrTableName])
INCLUDE ([Id],[ProcessingType],[ProcessingStartDate])
GO
CREATE NONCLUSTERED INDEX [IX_NC_DTA_LOG_ETL_PRO]
ON [dbo].[Data_Log] ([ETLName],[FileOrTableName],[ProcessingStatus],[ProcessingStartDate],[ProcessingEndDate])
GO
CREATE TRIGGER [dbo].[TR_UPDATE_DATA_LOG] 
ON [dbo].[Data_Log]
AFTER UPDATE 
AS
BEGIN
    IF @@rowcount = 0
        RETURN;

    SET NOCOUNT ON;
	DECLARE
		@ID_CONTROLE AS int = NULL,
		@COD_CONTROL AS varchar(80) = NULL, 
		@FAMILLE_CONTROL AS nvarchar(50) = NULL, 
		@FileOrTableName AS nvarchar(255) = NULL, 
		@FLG_ACTIF AS int,
		@nb as int;
	IF EXISTS (
		SELECT
			*
		FROM
			inserted I
			INNER JOIN [$(DataFactoryDatabaseName)].dbo.SIDP_QOD_LISTE_CONTROLE_FILEORTABLENAME	AS	L	WITH(NOLOCK)
				ON	i.FileOrTableName = L.FileOrTableName
					AND	UPPER(I.ProcessingType) like 'COLLECT%' AND  I.ETLName  NOT like 'PKGCO_SSIS_FI_%' AND  I.ETLName  NOT like 'PKGCO_SSIS_SA_%'
					AND	L.FLG_ACTIF = 1
					AND	I.ProcessingStatus = 'OK'
	)
	BEGIN
		DECLARE CURSOR_CONTROL CURSOR DYNAMIC
		FOR
		SELECT
			L.ID_CONTROL,
			L.COD_CONTROL,
			L.FAMILLE_CONTROL,
			L.FileOrTableName,
			L.FLG_ACTIF
		FROM
			inserted I
			INNER JOIN	[$(DataFactoryDatabaseName)].dbo.SIDP_QOD_LISTE_CONTROLE_FILEORTABLENAME	AS	L	WITH(NOLOCK)
				ON	i.FileOrTableName = L.FileOrTableName
					AND	UPPER(I.ProcessingType) like 'COLLECT%' AND  I.ETLName  NOT like 'PKGCO_SSIS_FI_%' AND  I.ETLName  NOT like 'PKGCO_SSIS_SA_%'
					AND	L.FLG_ACTIF = 1
					AND	I.ProcessingStatus = 'OK'
		OPEN CURSOR_CONTROL
		-- Perform the first fetch.
		FETCH NEXT FROM CURSOR_CONTROL INTO @ID_CONTROLE,@COD_CONTROL, @FAMILLE_CONTROL, @FileOrTableName,@FLG_ACTIF
		-- Check @@FETCH_STATUS to see if there are any more rows to fetch.  
		WHILE @@FETCH_STATUS = 0  
		BEGIN
			BEGIN TRY
				exec [$(DataFactoryDatabaseName)].dbo.PKG_QOD_EXEC_CONTROLE @ID_CONTROL=@ID_CONTROLE;
			END TRY
			BEGIN CATCH
				print 'ErrorNumber :'+isnull(cast(ERROR_NUMBER() as char), '')+' 
				ErrorSeverity :'+isnull(cast(ERROR_SEVERITY() AS char), '')+'  
				ErrorState :'+isnull(cast(ERROR_STATE() AS char), '')+'  
				ErrorProcedure :'+isnull(cast(ERROR_PROCEDURE() AS char), '')+'  
				ErrorLine :'+isnull(cast(ERROR_LINE() AS char),'')+'  
				ErrorMessage :'+isnull(ERROR_MESSAGE(), '')
			END CATCH
		
			FETCH NEXT FROM CURSOR_CONTROL INTO @ID_CONTROLE,@COD_CONTROL, @FAMILLE_CONTROL, @FileOrTableName,@FLG_ACTIF;
		END
		CLOSE CURSOR_CONTROL;  
		DEALLOCATE CURSOR_CONTROL;
	END
END