﻿CREATE TABLE [dbo].[PV_MC_BOUNCES] (
    [Id]                       BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [ClientID]                 INT                                         NULL,
    [SendID]                   INT                                         NOT NULL,
    [SubscriberKey]            NVARCHAR (100)                              NOT NULL,
    [EmailAddress]             NVARCHAR (100)                              NULL,
    [SubscriberID]             INT                                         NULL,
    [ListID]                   INT                                         NULL,
    [EventDate]                DATETIME                                    NOT NULL,
    [EventType]                NVARCHAR (100)                              NULL,
    [BounceCategory]           NVARCHAR (50)                               NULL,
    [SMTPCode]                 SMALLINT                                    NULL,
    [BounceReason]             NVARCHAR (MAX)                              NULL,
    [BatchID]                  NVARCHAR (100)                              NULL,
    [TriggeredSendExternalKey] NVARCHAR (101)                              NULL,
    [Startdt]                  DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]                    DATETIME2 (7) GENERATED ALWAYS AS ROW END   NOT NULL,
    CONSTRAINT [PK_PV_MC_BOUNCES] PRIMARY KEY CLUSTERED ([Id] ASC, [SendID] ASC, [SubscriberKey] ASC, [EventDate] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_MC_BOUNCESHistory], DATA_CONSISTENCY_CHECK=ON));

