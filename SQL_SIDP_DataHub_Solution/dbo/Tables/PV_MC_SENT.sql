﻿CREATE TABLE [dbo].[PV_MC_SENT] (
    [Id]                       BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [ClientID]                 INT                                         NULL,
    [SendID]                   INT                                         NOT NULL,
    [SubscriberKey]            NVARCHAR (100)                              NOT NULL,
    [EmailAddress]             NVARCHAR (100)                              NULL,
    [SubscriberID]             INT                                         NULL,
    [ListID]                   INT                                         NULL,
    [EventDate]                DATETIME                                    NOT NULL,
    [EventType]                NVARCHAR (100)                              NULL,
    [BatchID]                  NVARCHAR (100)                              NULL,
    [TriggeredSendExternalKey] NVARCHAR (100)                              NULL,
    [Startdt]                  DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]                    DATETIME2 (7) GENERATED ALWAYS AS ROW END   NOT NULL,
    CONSTRAINT [PK_PV_MC_SENT] PRIMARY KEY CLUSTERED ([Id] ASC, [SendID] ASC, [SubscriberKey] ASC, [EventDate] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_MC_SENTHistory], DATA_CONSISTENCY_CHECK=ON));

