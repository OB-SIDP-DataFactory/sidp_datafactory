﻿CREATE TABLE [dbo].[IWD_RECORD_STATUS] (
    [RECORD_STATUS_KEY]  INT          NOT NULL,
    [RECORD_STATUS]      VARCHAR (32) NULL,
    [RECORD_STATUS_CODE] VARCHAR (32) NULL,
    [CREATE_AUDIT_KEY]   NUMERIC (19) NOT NULL,
    [UPDATE_AUDIT_KEY]   NUMERIC (19) NOT NULL,
    CONSTRAINT [PK_RECORD_STATUS] PRIMARY KEY CLUSTERED ([RECORD_STATUS_KEY] ASC)
);

