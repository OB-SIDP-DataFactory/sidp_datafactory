﻿CREATE TABLE [dbo].[IWD_TMP_UD_KV_TEMPL] (
    [INTERACTION_RESOURCE_ID] NUMERIC (19)  NOT NULL,
    [UD_TO_UDE_MAPPING_ID]    INT           NOT NULL,
    [END_TS]                  INT           NOT NULL,
    [PROPAGATION_RULE]        VARCHAR (16)  NOT NULL,
    [KEYVALUE]                VARCHAR (255) NULL
);

