﻿CREATE TABLE [dbo].[PV_FE_CUSTOMERSUBPRODUCTASSOC] (
    [Id]                            BIGINT         IDENTITY (1, 1) NOT NULL,
    [ID_FE_CUSTOMERSUBPRODUCTASSOC] DECIMAL (38)   NOT NULL,
    [CUSTOMER_ID]                   DECIMAL (38)   NULL,
    [SUBSCRIBED_PRODUCT_ID]         DECIMAL (38)   NOT NULL,
    [SALESFORCE_PERSON_ID]          NVARCHAR (255) NOT NULL,
    [CUSTOMER_ROLE_ID]              DECIMAL (38)   NOT NULL,
    [CREATION_DATE]                 DATETIME2 (6)  NULL,
    [CREATION_USER]                 NVARCHAR (255) NULL,
    [LAST_UPDATE_DATE]              DATETIME2 (6)  NULL,
    [LAST_UPDATE_USER]              NVARCHAR (255) NULL,
    [USER_TYPE]                     NVARCHAR (255) NULL,
    [Validity_StartDate]            DATETIME2 (7)  NOT NULL,
    [Validity_EndDate]              DATETIME2 (7)  DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_FE_CUSTOMERSUBPRODUCTASSOC] PRIMARY KEY CLUSTERED ([Id] ASC)
);

