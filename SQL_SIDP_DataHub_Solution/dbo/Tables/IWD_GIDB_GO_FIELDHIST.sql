﻿CREATE TABLE [dbo].[IWD_GIDB_GO_FIELDHIST] (
    [ID]               NUMERIC (16)  NOT NULL,
    [CHAINGUID]        VARCHAR (64)  NOT NULL,
    [RECORDHANDLE]     INT           NOT NULL,
    [FIELDID]          INT           NOT NULL,
    [OLDVALUE]         VARCHAR (255) NULL,
    [NEWVALUE]         VARCHAR (255) NULL,
    [PREVENTER]        DATETIME      NULL,
    [PREVENTER_TS]     INT           NULL,
    [SEQ]              INT           NOT NULL,
    [CHAINHISTSEQ]     INT           NOT NULL,
    [CALLATTID]        VARCHAR (50)  NULL,
    [ADDED]            DATETIME      NOT NULL,
    [ADDED_TS]         INT           NULL,
    [GSYS_DOMAIN]      INT           NULL,
    [GSYS_SYS_ID]      INT           NULL,
    [GSYS_EXT_VCH1]    VARCHAR (255) NULL,
    [GSYS_EXT_VCH2]    VARCHAR (255) NULL,
    [GSYS_EXT_INT1]    INT           NULL,
    [GSYS_EXT_INT2]    INT           NULL,
    [CREATE_AUDIT_KEY] NUMERIC (19)  NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [IWD_I_G_GO_FIELD_H_CTS]
    ON [dbo].[IWD_GIDB_GO_FIELDHIST]([ADDED_TS] ASC);


GO
CREATE NONCLUSTERED INDEX [IWD_I_G_GO_FIELD_H_GUID]
    ON [dbo].[IWD_GIDB_GO_FIELDHIST]([CHAINGUID] ASC);

