﻿CREATE TABLE [dbo].[PV_FI_M_COMPL_Q] (
    [Id]                 INT                                         IDENTITY (1, 1) NOT NULL,
    [COD_ENR]            VARCHAR (2)                                 NULL,
    [IDE_GRC]            VARCHAR (8)                                 NULL,
    [IDE_GRC_COE]        VARCHAR (8)                                 NULL,
    [TYP_COM]            VARCHAR (5)                                 NULL,
    [NUM_PRE]            VARCHAR (11)                                NULL,
    [TYP_ENR]            VARCHAR (2)                                 NULL,
    [DTE_OUV]            DATE                                        NULL,
    [DTE_PRE_UTI]        DATE                                        NULL,
    [NUM_BAR]            VARCHAR (6)                                 NULL,
    [VAR_BAR]            VARCHAR (3)                                 NULL,
    [GRI_TAX_BAR]        VARCHAR (2)                                 NULL,
    [GRI_TAX_MEN]        VARCHAR (2)                                 NULL,
    [VIT_BAR]            VARCHAR (1)                                 NULL,
    [DTE_FIN_BAR]        DATE                                        NULL,
    [TYP_BAR]            VARCHAR (1)                                 NULL,
    [NUM_BAR_FUT]        VARCHAR (6)                                 NULL,
    [VAR_BAR_FUT]        VARCHAR (3)                                 NULL,
    [GRI_TAX_BAR_FUT]    VARCHAR (2)                                 NULL,
    [GRI_TAX_MEN_FUT]    VARCHAR (2)                                 NULL,
    [VIT_BAR_FUT]        VARCHAR (1)                                 NULL,
    [COD_BAN_CAV]        VARCHAR (5)                                 NULL,
    [COD_GUI_BAN]        VARCHAR (5)                                 NULL,
    [NUM_CAV]            VARCHAR (11)                                NULL,
    [CLE_RIB_CAV]        VARCHAR (2)                                 NULL,
    [LIB_CPT]            VARCHAR (26)                                NULL,
    [TOP_ASS]            VARCHAR (1)                                 NULL,
    [NAT_ASS]            VARCHAR (1)                                 NULL,
    [MTT_TOT_AUT]        DECIMAL (11, 2)                             NULL,
    [MTT_DIS]            DECIMAL (11, 2)                             NULL,
    [MTT_TOT_UTI]        DECIMAL (11, 2)                             NULL,
    [MTT_UTI_COU]        DECIMAL (11, 2)                             NULL,
    [MTT_UTI_UP]         DECIMAL (11, 2)                             NULL,
    [DTE_LST_ECH]        DATE                                        NULL,
    [MTT_TOT_ECH]        DECIMAL (11, 2)                             NULL,
    [MTT_ECH_UC]         DECIMAL (11, 2)                             NULL,
    [MTT_CAP_UC]         DECIMAL (11, 2)                             NULL,
    [MTT_INT_UC]         DECIMAL (11, 2)                             NULL,
    [MTT_ASS_UC]         DECIMAL (11, 2)                             NULL,
    [MTT_ECH_UP]         DECIMAL (11, 2)                             NULL,
    [MTT_CAP_UP]         DECIMAL (11, 2)                             NULL,
    [MTT_INT_UP]         DECIMAL (11, 2)                             NULL,
    [MTT_ASS_UP]         DECIMAL (11, 2)                             NULL,
    [NBR_MEN_REP_RES]    INT                                         NULL,
    [TOP_MOD_RES_AUT]    VARCHAR (1)                                 NULL,
    [DTE_MOD_RES_AUT]    DATE                                        NULL,
    [NBR_IMP]            INT                                         NULL,
    [COD_SIT_DOS]        VARCHAR (2)                                 NULL,
    [DTE_DER_MOD_CACH]   DATE                                        NULL,
    [COD_POS]            INT                                         NULL,
    [DTE_MOD_COD_POS]    DATE                                        NULL,
    [NBR_UP]             INT                                         NULL,
    [Validity_StartDate] DATETIME                                    NOT NULL,
    [Validity_EndDate]   DATETIME                                    DEFAULT ('99991231') NOT NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   NOT NULL,
    CONSTRAINT [PK_PV_FI_M_COMPL_Q] PRIMARY KEY CLUSTERED ([Id] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_FI_M_COMPL_QHistory], DATA_CONSISTENCY_CHECK=ON));
GO
CREATE NONCLUSTERED INDEX IX_NC_CMP_POS_VLD_DTE_DOS
ON [dbo].[PV_FI_M_COMPL_Q] ([COD_POS], [Validity_StartDate])
INCLUDE ([COD_SIT_DOS])
GO
CREATE NONCLUSTERED INDEX IX_NC_CMP_POS_VLD_DTE_ID
ON [dbo].[PV_FI_M_COMPL_Q] ([COD_POS], [Validity_StartDate])
INCLUDE ([Id], [COD_ENR], [IDE_GRC], [IDE_GRC_COE], [TYP_COM], [NUM_PRE], [TYP_ENR], [DTE_OUV], [DTE_PRE_UTI], [NUM_BAR], [VAR_BAR], [GRI_TAX_BAR], [GRI_TAX_MEN], [VIT_BAR], [DTE_FIN_BAR], [TYP_BAR], [NUM_BAR_FUT], [VAR_BAR_FUT], [GRI_TAX_BAR_FUT], [GRI_TAX_MEN_FUT], [VIT_BAR_FUT], [COD_BAN_CAV], [COD_GUI_BAN], [NUM_CAV], [CLE_RIB_CAV], [LIB_CPT], [TOP_ASS], [NAT_ASS], [MTT_TOT_AUT], [MTT_DIS], [MTT_TOT_UTI], [MTT_UTI_COU], [MTT_UTI_UP], [DTE_LST_ECH], [MTT_TOT_ECH], [MTT_ECH_UC], [MTT_CAP_UC], [MTT_INT_UC], [MTT_ASS_UC], [MTT_ECH_UP], [MTT_CAP_UP], [MTT_INT_UP], [MTT_ASS_UP], [NBR_MEN_REP_RES], [TOP_MOD_RES_AUT], [DTE_MOD_RES_AUT], [NBR_IMP], [COD_SIT_DOS], [DTE_DER_MOD_CACH], [DTE_MOD_COD_POS], [NBR_UP], [Validity_EndDate], [Startdt], [Enddt])
GO
CREATE NONCLUSTERED INDEX IX_NC_CMP_POS_VLD_DTE_NUM
ON [dbo].[PV_FI_M_COMPL_Q] ([COD_POS], [Validity_StartDate])
INCLUDE ([NUM_PRE], [DTE_OUV], [DTE_PRE_UTI], [MTT_TOT_AUT])
GO
CREATE NONCLUSTERED INDEX IX_NC_CMP_POS_VLD_DTE_NUM_DOS
ON [dbo].[PV_FI_M_COMPL_Q] ([COD_POS], [Validity_StartDate])
INCLUDE ([NUM_PRE], [DTE_OUV], [DTE_PRE_UTI], [MTT_TOT_AUT], [COD_SIT_DOS])
GO
CREATE NONCLUSTERED INDEX IX_NC_CMP_POS_VLD_DTE_LST
ON [dbo].[PV_FI_M_COMPL_Q] ([COD_POS], [Validity_StartDate], [DTE_LST_ECH])
INCLUDE ([Id], [COD_ENR], [IDE_GRC], [IDE_GRC_COE], [TYP_COM], [NUM_PRE], [TYP_ENR], [DTE_OUV], [DTE_PRE_UTI], [NUM_BAR], [VAR_BAR], [GRI_TAX_BAR], [GRI_TAX_MEN], [VIT_BAR], [DTE_FIN_BAR], [TYP_BAR], [NUM_BAR_FUT], [VAR_BAR_FUT], [GRI_TAX_BAR_FUT], [GRI_TAX_MEN_FUT], [VIT_BAR_FUT], [COD_BAN_CAV], [COD_GUI_BAN], [NUM_CAV], [CLE_RIB_CAV], [LIB_CPT], [TOP_ASS], [NAT_ASS], [MTT_TOT_AUT], [MTT_DIS], [MTT_TOT_UTI], [MTT_UTI_COU], [MTT_UTI_UP], [MTT_TOT_ECH], [MTT_ECH_UC], [MTT_CAP_UC], [MTT_INT_UC], [MTT_ASS_UC], [MTT_ECH_UP], [MTT_CAP_UP], [MTT_INT_UP], [MTT_ASS_UP], [NBR_MEN_REP_RES], [TOP_MOD_RES_AUT], [DTE_MOD_RES_AUT], [NBR_IMP], [COD_SIT_DOS], [DTE_DER_MOD_CACH], [DTE_MOD_COD_POS], [NBR_UP], [Validity_EndDate], [Startdt], [Enddt])
GO
CREATE NONCLUSTERED INDEX IX_NC_CMP_POS_VLD_DTE_PRE
ON [dbo].[PV_FI_M_COMPL_Q] ([COD_POS], [Validity_StartDate], [DTE_PRE_UTI])
INCLUDE ([Id], [COD_ENR], [IDE_GRC], [IDE_GRC_COE], [TYP_COM], [NUM_PRE], [TYP_ENR], [DTE_OUV], [NUM_BAR], [VAR_BAR], [GRI_TAX_BAR], [GRI_TAX_MEN], [VIT_BAR], [DTE_FIN_BAR], [TYP_BAR], [NUM_BAR_FUT], [VAR_BAR_FUT], [GRI_TAX_BAR_FUT], [GRI_TAX_MEN_FUT], [VIT_BAR_FUT], [COD_BAN_CAV], [COD_GUI_BAN], [NUM_CAV], [CLE_RIB_CAV], [LIB_CPT], [TOP_ASS], [NAT_ASS], [MTT_TOT_AUT], [MTT_DIS], [MTT_TOT_UTI], [MTT_UTI_COU], [MTT_UTI_UP], [DTE_LST_ECH], [MTT_TOT_ECH], [MTT_ECH_UC], [MTT_CAP_UC], [MTT_INT_UC], [MTT_ASS_UC], [MTT_ECH_UP], [MTT_CAP_UP], [MTT_INT_UP], [MTT_ASS_UP], [NBR_MEN_REP_RES], [TOP_MOD_RES_AUT], [DTE_MOD_RES_AUT], [NBR_IMP], [COD_SIT_DOS], [DTE_DER_MOD_CACH], [DTE_MOD_COD_POS], [NBR_UP], [Validity_EndDate], [Startdt], [Enddt])
GO
CREATE NONCLUSTERED INDEX IX_NC_CMP_POS_VLD_DTE_MTT
ON [dbo].[PV_FI_M_COMPL_Q] ([COD_POS], [Validity_StartDate], [MTT_TOT_UTI])
INCLUDE ([Id], [COD_ENR], [IDE_GRC], [IDE_GRC_COE], [TYP_COM], [NUM_PRE], [TYP_ENR], [DTE_OUV], [DTE_PRE_UTI], [NUM_BAR], [VAR_BAR], [GRI_TAX_BAR], [GRI_TAX_MEN], [VIT_BAR], [DTE_FIN_BAR], [TYP_BAR], [NUM_BAR_FUT], [VAR_BAR_FUT], [GRI_TAX_BAR_FUT], [GRI_TAX_MEN_FUT], [VIT_BAR_FUT], [COD_BAN_CAV], [COD_GUI_BAN], [NUM_CAV], [CLE_RIB_CAV], [LIB_CPT], [TOP_ASS], [NAT_ASS], [MTT_TOT_AUT], [MTT_DIS], [MTT_UTI_COU], [MTT_UTI_UP], [DTE_LST_ECH], [MTT_TOT_ECH], [MTT_ECH_UC], [MTT_CAP_UC], [MTT_INT_UC], [MTT_ASS_UC], [MTT_ECH_UP], [MTT_CAP_UP], [MTT_INT_UP], [MTT_ASS_UP], [NBR_MEN_REP_RES], [TOP_MOD_RES_AUT], [DTE_MOD_RES_AUT], [NBR_IMP], [COD_SIT_DOS], [DTE_DER_MOD_CACH], [DTE_MOD_COD_POS], [NBR_UP], [Validity_EndDate], [Startdt], [Enddt])
GO
CREATE NONCLUSTERED INDEX IX_NC_CMP_VLD_DTE_COD
ON [dbo].[PV_FI_M_COMPL_Q] ([Validity_StartDate])
INCLUDE ([COD_ENR], [IDE_GRC], [IDE_GRC_COE], [TYP_COM], [NUM_PRE], [TYP_ENR], [DTE_OUV], [DTE_PRE_UTI], [NUM_BAR], [VAR_BAR], [GRI_TAX_BAR], [GRI_TAX_MEN], [VIT_BAR], [DTE_FIN_BAR], [TYP_BAR], [NUM_BAR_FUT], [VAR_BAR_FUT], [GRI_TAX_BAR_FUT], [GRI_TAX_MEN_FUT], [VIT_BAR_FUT], [COD_BAN_CAV], [COD_GUI_BAN], [NUM_CAV], [CLE_RIB_CAV], [LIB_CPT], [TOP_ASS], [NAT_ASS], [MTT_TOT_AUT], [MTT_DIS], [MTT_TOT_UTI], [MTT_UTI_COU], [MTT_UTI_UP], [DTE_LST_ECH], [MTT_TOT_ECH], [MTT_ECH_UC], [MTT_CAP_UC], [MTT_INT_UC], [MTT_ASS_UC], [MTT_ECH_UP], [MTT_CAP_UP], [MTT_INT_UP], [MTT_ASS_UP], [NBR_MEN_REP_RES], [TOP_MOD_RES_AUT], [DTE_MOD_RES_AUT], [NBR_IMP], [COD_SIT_DOS], [DTE_DER_MOD_CACH], [COD_POS], [DTE_MOD_COD_POS], [NBR_UP], [Validity_EndDate], [Startdt], [Enddt])
GO
CREATE NONCLUSTERED INDEX IX_NC_CMP_VLD_DTE
ON [dbo].[PV_FI_M_COMPL_Q] ([Validity_StartDate], [Validity_EndDate])
INCLUDE ([COD_ENR], [IDE_GRC], [IDE_GRC_COE], [TYP_COM], [NUM_PRE], [TYP_ENR], [DTE_OUV], [DTE_PRE_UTI], [NUM_BAR], [VAR_BAR], [GRI_TAX_BAR], [GRI_TAX_MEN], [VIT_BAR], [DTE_FIN_BAR], [TYP_BAR], [NUM_BAR_FUT], [VAR_BAR_FUT], [GRI_TAX_BAR_FUT], [GRI_TAX_MEN_FUT], [VIT_BAR_FUT], [COD_BAN_CAV], [COD_GUI_BAN], [NUM_CAV], [CLE_RIB_CAV], [LIB_CPT], [TOP_ASS], [NAT_ASS], [MTT_TOT_AUT], [MTT_DIS], [MTT_TOT_UTI], [MTT_UTI_COU], [MTT_UTI_UP], [DTE_LST_ECH], [MTT_TOT_ECH], [MTT_ECH_UC], [MTT_CAP_UC], [MTT_INT_UC], [MTT_ASS_UC], [MTT_ECH_UP], [MTT_CAP_UP], [MTT_INT_UP], [MTT_ASS_UP], [NBR_MEN_REP_RES], [TOP_MOD_RES_AUT], [DTE_MOD_RES_AUT], [NBR_IMP], [COD_SIT_DOS], [DTE_DER_MOD_CACH], [COD_POS], [DTE_MOD_COD_POS], [NBR_UP], [Startdt], [Enddt])
GO