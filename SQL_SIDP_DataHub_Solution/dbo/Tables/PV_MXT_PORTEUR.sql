﻿CREATE TABLE [dbo].[PV_MXT_PORTEUR] (
    [Id]                           BIGINT         IDENTITY (1, 1) NOT NULL,
    [RECR_COD]                     NVARCHAR (2)   NULL,
    [RECR_NUMB]                    NVARCHAR (8)   NULL,
    [ISSR_BANK_BIC_COD]            NVARCHAR (11)  NULL,
    [COMP_BANK_BRNC_COD]           NVARCHAR (5)   NULL,
    [BANK_BRNC_EMPL_NUMB]          NVARCHAR (7)   NULL,
    [BANK_CORP_IDNT_TYP]           NVARCHAR (3)   NULL,
    [BANK_CORP_IDNT]               NVARCHAR (17)  NULL,
    [CORP_NAM]                     NVARCHAR (60)  NULL,
    [SUBS_IDNT_TYP]                NVARCHAR (3)   NULL,
    [SUBS_BANK_IDNT]               NVARCHAR (17)  NULL,
    [COMP_TOPS_IDNT_LEV1]          NVARCHAR (15)  NULL,
    [CORP_LONG_NAM]                NVARCHAR (40)  NULL,
    [SUBS_TOPS_IDNT_LEV2]          NVARCHAR (15)  NULL,
    [SUBS_BANK_BRNC_COD]           NVARCHAR (5)   NULL,
    [SUBS_LONG_NAM]                NVARCHAR (40)  NULL,
    [DEPR_IDNT]                    NVARCHAR (15)  NULL,
    [DEPR_BANK_BRNC_COD]           NVARCHAR (5)   NULL,
    [DEPR_LONG_NAM]                NVARCHAR (40)  NULL,
    [SERV_IDNT]                    NVARCHAR (15)  NULL,
    [SERV_BANK_BRNC_COD]           NVARCHAR (5)   NULL,
    [SERV_WORD]                    NVARCHAR (40)  NULL,
    [INTR_TOPS_CARD_IDNT]          NVARCHAR (15)  NULL,
    [CARD_PRDC]                    NVARCHAR (3)   NULL,
    [CARD_ID]                      NVARCHAR (19)  NULL,
    [EVNT_STRT_DAT]                NVARCHAR (10)  NULL,
    [CARD_LANG]                    NVARCHAR (1)   NULL,
    [CARD_EMBS_NAM]                NVARCHAR (26)  NULL,
    [CARD_ADRS_LIN_1]              NVARCHAR (38)  NULL,
    [CARD_ADRS_LIN_2]              NVARCHAR (38)  NULL,
    [CARD_ADRS_LIN_3]              NVARCHAR (38)  NULL,
    [CARD_ADRS_LIN_4]              NVARCHAR (38)  NULL,
    [CARD_ADRS_LIN_5]              NVARCHAR (38)  NULL,
    [CARD_ADRS_POST_COD]           NVARCHAR (9)   NULL,
    [CARD_ADRS_CIT]                NVARCHAR (32)  NULL,
    [CARD_ADRS_COUN_IS_COD]        NVARCHAR (3)   NULL,
    [IDNT_PORT_SI_BANQ]            NVARCHAR (18)  NULL,
    [CORP_CARD_IBN_STTS]           NVARCHAR (1)   NULL,
    [CORP_CARD_IBN_REAS_COD]       NVARCHAR (2)   NULL,
    [CONT_FEES_BANK_ACCN_BIC_COD]  NVARCHAR (11)  NULL,
    [CONT_FEES_BANK_ACCN_IBN]      NVARCHAR (34)  NULL,
    [TE_PURC_BANK_ACCN_BIC_COD]    NVARCHAR (11)  NULL,
    [TE_PURC_BANK_ACCN_IBN]        NVARCHAR (34)  NULL,
    [OTHR_PURC_BANK_ACCN_BIC_COD]  NVARCHAR (11)  NULL,
    [OTHR_PURC_BANK_ACCN_IBN]      NVARCHAR (34)  NULL,
    [WITH_BANK_ACCN_BIC_COD]       NVARCHAR (11)  NULL,
    [WITH_BANK_ACCN_IBN]           NVARCHAR (34)  NULL,
    [TE_PURC_BANK_ACCN_OWNR_TYP]   NVARCHAR (1)   NULL,
    [OTHR_PURC_BANK_ACCN_OWNR_TYP] NVARCHAR (1)   NULL,
    [WITH_BANK_ACCN_OWNR_TYP]      NVARCHAR (1)   NULL,
    [LIMT_COD]                     NVARCHAR (15)  NULL,
    [LIMT_WORD]                    NVARCHAR (80)  NULL,
    [COUN_CARD_CURR_IS_COD]        NVARCHAR (3)   NULL,
    [CARD_ACCN_BIC]                NVARCHAR (11)  NULL,
    [CARD_ACCN_IBN]                NVARCHAR (34)  NULL,
    [DIFF_PERD_TYP]                NVARCHAR (1)   NULL,
    [NUMB_OF_DAYS_FOR_PAYM_DIFF]   NVARCHAR (3)   NULL,
    [NUMB_OF_DAYS_FOR_WITH_DIFF]   NVARCHAR (3)   NULL,
    [WITH_CAPB]                    NVARCHAR (1)   NULL,
    [CASH_BACK_CAPB]               NVARCHAR (1)   NULL,
    [ADDR_TYP_FOR_SUBS_CARD_SEND]  NVARCHAR (1)   NULL,
    [ADDR_TYP_FOR_RENW_CARD_SEND]  NVARCHAR (1)   NULL,
    [ADDR_TYP_FOR_PIN_MAIL_SEND]   NVARCHAR (1)   NULL,
    [FORB_MCC_WORD]                NVARCHAR (80)  NULL,
    [CARD_LIMT_AMNT_FOR_PURC]      NVARCHAR (10)  NULL,
    [CARD_LIMT_AMNT_FOR_WITH]      NVARCHAR (10)  NULL,
    [SOLD_OF_CORP_WITH_CARD_FLG]   NVARCHAR (1)   NULL,
    [CARD_EXPR_DAT]                NVARCHAR (8)   NULL,
    [HOT_CARD_LIST_REAS_COD]       NVARCHAR (2)   NULL,
    [CARD_CLSR_REAS_COD]           NVARCHAR (2)   NULL,
    [CARD_TITL]                    NVARCHAR (16)  NULL,
    [CARD_EMPL_NUMB]               NVARCHAR (20)  NULL,
    [NP_FLG_ADD_VALD_FLG]          NVARCHAR (1)   NULL,
    [CARD_BANK_BRNC_COD]           NVARCHAR (5)   NULL,
    [COMP_NAM_PRNT_ON_CARD]        NVARCHAR (32)  NULL,
    [FLG_BANK_IDNT_LEVL_1_UPDT]    NVARCHAR (1)   NULL,
    [CARD_LAST_NAM]                NVARCHAR (40)  NULL,
    [CARD_FIRS_NAM]                NVARCHAR (40)  NULL,
    [TOPS_ID_SHDW_ACCN]            NVARCHAR (18)  NULL,
    [TRCK_NUMB]                    NVARCHAR (15)  NULL,
    [FILL]                         NVARCHAR (163) NULL,
    [COMP_VAT_NUMB]                NVARCHAR (20)  NULL,
    [CARD_COD_FISC]                NVARCHAR (30)  NULL,
    [CORP_LEVL1_BANK_IDNT]         NVARCHAR (18)  NULL,
    [CORP_LEVL2_BANK_IDNT]         NVARCHAR (18)  NULL,
    [CARD_BANK_IDNT]               NVARCHAR (18)  NULL,
    [BRNC_COD]                     NVARCHAR (8)   NULL,
    [COMP_LIMT]                    NVARCHAR (18)  NULL,
    [COMM_LIMT_FLG]                NVARCHAR (1)   NULL,
    [COMP_MANG_FISC_COD]           NVARCHAR (16)  NULL,
    [IBN_IMPC_BY_STTS_CHNG]        NVARCHAR (34)  NULL,
    [IBN_FOR_ANNL_SUBS]            NVARCHAR (34)  NULL,
    [IBN_FOR_SERV]                 NVARCHAR (34)  NULL,
    [IBN_FOR_DIFF_COST]            NVARCHAR (34)  NULL,
    [IBN_FOR_CARD_FEES]            NVARCHAR (34)  NULL,
    [TYP_OWN_OF_IBN_FOR_CARD]      NVARCHAR (1)   NULL,
    [PRFL_STD]                     NVARCHAR (3)   NULL,
    [PAN_MASK]                     NVARCHAR (19)  NULL,
    [FILL2]                        NVARCHAR (2)   NULL,
    [Validity_StartDate]           DATETIME2 (7)  NOT NULL,
    [Validity_EndDate]             DATETIME2 (7)  NOT NULL,
    [NOM_FICHIER]                  NVARCHAR (200) NULL,
    [INSRT_TS]                     DATETIME       NULL,
    CONSTRAINT [PK_PV_MXT_PORTEUR] PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PAN mask', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'PAN_MASK';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Profil std', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'PRFL_STD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type (owner) of IBAN for Cardholder', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'TYP_OWN_OF_IBN_FOR_CARD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IBAN for Cardholder Fees', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'IBN_FOR_CARD_FEES';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IBAN for Differed costs', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'IBN_FOR_DIFF_COST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IBAN for Services', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'IBN_FOR_SERV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IBAN for annual subscription', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'IBN_FOR_ANNL_SUBS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IBAN impacted by a status change', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'IBN_IMPC_BY_STTS_CHNG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Company manager fiscal code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'COMP_MANG_FISC_COD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Common limit flag', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'COMM_LIMT_FLG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Company Limit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'COMP_LIMT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'BRANCH CODE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'BRNC_COD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cardholder bank identifier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CARD_BANK_IDNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Corporate level2 bank identifier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CORP_LEVL2_BANK_IDNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Corporate level1 bank identifier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CORP_LEVL1_BANK_IDNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cardholder code fiscal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CARD_COD_FISC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Company VAT Number ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'COMP_VAT_NUMB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FILLER', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'FILL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tracking Number', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'TRCK_NUMB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Topase ID / Shadow Account', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'TOPS_ID_SHDW_ACCN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cardholder first name', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CARD_FIRS_NAM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cardholder last name', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CARD_LAST_NAM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Bank identifier level 1 updated', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'FLG_BANK_IDNT_LEVL_1_UPDT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Company name printed on card', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'COMP_NAM_PRNT_ON_CARD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cardholder Bank Branch Code ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CARD_BANK_BRNC_COD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NPAI Flag (Address validity flag)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'NP_FLG_ADD_VALD_FLG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cardholder Employee Number', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CARD_EMPL_NUMB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cardholder title', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CARD_TITL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Card closure reason code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CARD_CLSR_REAS_COD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Hot Card List Reason Code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'HOT_CARD_LIST_REAS_COD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Card Expiry date', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CARD_EXPR_DAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Solidarity (of corporation with cardholder) flag', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'SOLD_OF_CORP_WITH_CARD_FLG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CARD limit amount for Withdrawal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CARD_LIMT_AMNT_FOR_WITH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CARD limit amount for purchase', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CARD_LIMT_AMNT_FOR_PURC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Forbidden MCC wording', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'FORB_MCC_WORD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Address type for PIN mailer sending', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'ADDR_TYP_FOR_PIN_MAIL_SEND';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Address type for Renewal Card sending', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'ADDR_TYP_FOR_RENW_CARD_SEND';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Address type for Subscription Card sending', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'ADDR_TYP_FOR_SUBS_CARD_SEND';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cash Back capability', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CASH_BACK_CAPB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Withdrawal capability', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'WITH_CAPB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Number of days for Withdrawal differed', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'NUMB_OF_DAYS_FOR_WITH_DIFF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Number of days for Payment differed', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'NUMB_OF_DAYS_FOR_PAYM_DIFF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Differed period type', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'DIFF_PERD_TYP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cardholder Account IBAN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CARD_ACCN_IBN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cardholder Account BIC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CARD_ACCN_BIC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Country Card currency ISO Code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'COUN_CARD_CURR_IS_COD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Limit Wording', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'LIMT_WORD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Limit Code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'LIMT_COD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Withdrawal bank account owner type', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'WITH_BANK_ACCN_OWNR_TYP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Other purchases bank account owner type', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'OTHR_PURC_BANK_ACCN_OWNR_TYP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'T&E purchase bank account owner type', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'TE_PURC_BANK_ACCN_OWNR_TYP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Withdrawal bank account IBAN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'WITH_BANK_ACCN_IBN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Withdrawal bank account BIC code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'WITH_BANK_ACCN_BIC_COD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Other purchases bank account IBAN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'OTHR_PURC_BANK_ACCN_IBN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Other purchases bank account BIC code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'OTHR_PURC_BANK_ACCN_BIC_COD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'T&E purchase bank account IBAN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'TE_PURC_BANK_ACCN_IBN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'T&E purchase bank account BIC code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'TE_PURC_BANK_ACCN_BIC_COD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Contract fees bank account IBAN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CONT_FEES_BANK_ACCN_IBN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Contract fees bank account BIC code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CONT_FEES_BANK_ACCN_BIC_COD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Corporate / Card / IBAN reason code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CORP_CARD_IBN_REAS_COD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Corporate / Card / IBAN status', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CORP_CARD_IBN_STTS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant porteur SI BANQUE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'IDNT_PORT_SI_BANQ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cardholder Adress: Country ISO code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CARD_ADRS_COUN_IS_COD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cardholder Adress: City', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CARD_ADRS_CIT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cardholder Adress: Postal code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CARD_ADRS_POST_COD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cardholder Adress: line 5', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CARD_ADRS_LIN_5';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cardholder Adress: line 4', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CARD_ADRS_LIN_4';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cardholder Adress: line 3', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CARD_ADRS_LIN_3';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cardholder Adress: line 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CARD_ADRS_LIN_2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cardholder Adress: line 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CARD_ADRS_LIN_1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Card embossed Name', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CARD_EMBS_NAM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cardholder language', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CARD_LANG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Event start date', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'EVNT_STRT_DAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Card ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CARD_ID';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Card Product', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CARD_PRDC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Internal TOPASE Cardholder Identifier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'INTR_TOPS_CARD_IDNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Service Wording', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'SERV_WORD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Service Bank Branch Code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'SERV_BANK_BRNC_COD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Service identifier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'SERV_IDNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Departement long name', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'DEPR_LONG_NAM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Department Bank Branch Code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'DEPR_BANK_BRNC_COD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Departement identifier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'DEPR_IDNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Subsidiary Long Name', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'SUBS_LONG_NAM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Subsidiary Bank Branch Code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'SUBS_BANK_BRNC_COD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Subsidiary TOPASE Identifier Lev2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'SUBS_TOPS_IDNT_LEV2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Corporate Long Name', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CORP_LONG_NAM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Company TOPASE identifier Lev1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'COMP_TOPS_IDNT_LEV1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Subsidiary Bank Identifier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'SUBS_BANK_IDNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Subsidiary Identifier type', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'SUBS_IDNT_TYP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Corporate name', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'CORP_NAM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Bank Corporate Identifier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'BANK_CORP_IDNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Bank Corporate Identifier type', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'BANK_CORP_IDNT_TYP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Bank Branch Employee Number ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'BANK_BRNC_EMPL_NUMB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Company Bank Branch Code ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'COMP_BANK_BRNC_COD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Issuer Bank BIC Code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'ISSR_BANK_BIC_COD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Record code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_MXT_PORTEUR', @level2type = N'COLUMN', @level2name = N'RECR_COD';

