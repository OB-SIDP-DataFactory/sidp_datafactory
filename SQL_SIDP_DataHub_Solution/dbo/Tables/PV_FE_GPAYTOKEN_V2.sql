﻿CREATE TABLE [dbo].[PV_FE_GPAYTOKEN_V2] (
    [Id]                       BIGINT         IDENTITY (1, 1) NOT NULL,
    [ID_FE_GPAYTOKEN]          DECIMAL (38)   NOT NULL,
    [WALLET_ID]                NVARCHAR (255) NOT NULL,
    [TOKEN_IDENTIFIER]         NVARCHAR (255) NOT NULL,
    [LAST_INSTALLATION_DATE]   DATETIME2 (6)  NOT NULL,
    [DEVICE_NAME]              NVARCHAR (255) NULL,
    [DEVICE_TYPE_ID]           DECIMAL (38)   NOT NULL,
    [MOBILE_CARD_EQUIPMENT_ID] DECIMAL (38)   NOT NULL,
    [CREATION_DATE]            DATETIME2 (6)  NOT NULL,
    [CREATION_USER]            NVARCHAR (255) NOT NULL,
    [LAST_UPDATE_USER]         NVARCHAR (255) NULL,
    [LAST_UPDATE_DATE]         DATETIME2 (6)  NULL,
    [USER_TYPE]                NVARCHAR (255) NULL,
    [Validity_StartDate]       DATETIME2 (7)  NOT NULL,
    [Validity_EndDate]         DATETIME2 (7)  DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_FE_GPAYTOKEN_V2] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UC_PV_FE_GPAYTOKEN_V2] UNIQUE NONCLUSTERED ([ID_FE_GPAYTOKEN] ASC, [Validity_StartDate] ASC)
);

