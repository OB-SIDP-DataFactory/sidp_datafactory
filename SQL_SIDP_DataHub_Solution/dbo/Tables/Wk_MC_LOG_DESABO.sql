﻿CREATE TABLE [dbo].[Wk_MC_LOG_DESABO] (
    [SubscriberKey]           NVARCHAR (18)  NOT NULL,
    [ID_CONTACT]              NVARCHAR (18)  NULL,
    [ID_CONTACT_SOUSCRIPTEUR] NVARCHAR (18)  NULL,
    [EMAIL]                   NVARCHAR (254) NULL,
    [DATE_DESABO]             DATETIME       NULL,
    [MESSAGE_NAME]            NVARCHAR (200) NULL,
    [JobID]                   INT            NULL,
    [INITIATIVE_DESABO]       BIT            NULL,
    [Reason]                  INT            NULL,
    [Reason_Label]            NVARCHAR (200) NULL
);

