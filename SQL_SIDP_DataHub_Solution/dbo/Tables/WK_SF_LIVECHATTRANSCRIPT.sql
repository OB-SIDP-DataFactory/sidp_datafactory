﻿CREATE TABLE [dbo].[WK_SF_LIVECHATTRANSCRIPT] (
    [Id_SF]                       NVARCHAR (18)  NOT NULL,
    [CreatedDate]                 DATETIME       NULL,
    [LastModifiedDate]            DATETIME       NULL,
    [StartTime]                   DATETIME2 (6)  NULL,
    [OwnerId]                     NVARCHAR (18)  NULL,
    [CaseId]                      NVARCHAR (18)  NULL,
    [AccountId]                   NVARCHAR (18)  NULL,
    [Subject__c]                  NVARCHAR (255) NULL,
    [ChatDuration]                INT            NULL,
    [WaitTime]                    INT            NULL,
    [ConversationID__c]           DECIMAL (18)   NULL,
    [IsDeleted]                   NVARCHAR (10)  NULL,
    [Name]                        NVARCHAR (255) NULL,
    [RecordTypeId]                NVARCHAR (18)  NULL,
    [CreatedById]                 NVARCHAR (18)  NULL,
    [LastModifiedById]            NVARCHAR (18)  NULL,
    [SystemModstamp]              DATETIME2 (7)  NULL,
    [LastViewedDate]              DATETIME2 (7)  NULL,
    [LastReferencedDate]          DATETIME2 (7)  NULL,
    [LiveChatVisitorId]           NVARCHAR (18)  NULL,
    [Body]                        NVARCHAR (MAX) NULL,
    [ContactId]                   NVARCHAR (18)  NULL,
    [LeadId]                      NVARCHAR (18)  NULL,
    [LiveChatDeploymentId]        NVARCHAR (18)  NULL,
    [LiveChatButtonId]            NVARCHAR (18)  NULL,
    [SkillId]                     NVARCHAR (18)  NULL,
    [IpAddress]                   NVARCHAR (39)  NULL,
    [Location]                    NVARCHAR (200) NULL,
    [UserAgent]                   NVARCHAR (200) NULL,
    [Browser]                     NVARCHAR (200) NULL,
    [Platform]                    NVARCHAR (200) NULL,
    [BrowserLanguage]             NVARCHAR (200) NULL,
    [ScreenResolution]            NVARCHAR (200) NULL,
    [ReferrerUri]                 NVARCHAR (200) NULL,
    [Status]                      NVARCHAR (255) NULL,
    [RequestTime]                 DATETIME2 (7)  NULL,
    [EndTime]                     DATETIME2 (7)  NULL,
    [EndedBy]                     NVARCHAR (255) NULL,
    [AverageResponseTimeVisitor]  INT            NULL,
    [AverageResponseTimeOperator] INT            NULL,
    [OperatorMessageCount]        INT            NULL,
    [VisitorMessageCount]         INT            NULL,
    [MaxResponseTimeOperator]     INT            NULL,
    [ChatKey]                     NVARCHAR (200) NULL,
    [SupervisorTranscriptBody]    NVARCHAR (MAX) NULL,
    [MaxResponseTimeVisitor]      INT            NULL,
    [VisitorNetwork]              NVARCHAR (200) NULL,
    [Abandoned]                   INT            NULL,
    [IsChatbotSession]            BIT            NULL,
    [authenticated__c]            NVARCHAR (10)  NULL,
    [TCHPrechatSignature__c]      NVARCHAR (MAX) NULL,
    [ForbiddenWords__c]           NVARCHAR (255) NULL,
    [HasForbiddenWord__c]         NVARCHAR (10)  NULL,
    [TCHAccountTCHId__c]          NVARCHAR (18)  NULL,
    CONSTRAINT [PK_WK_SF_LIVECHATTRANSCRIPT] PRIMARY KEY CLUSTERED ([Id_SF] ASC)
)
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Délai d''attente', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'WaitTime';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Subject', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'Subject__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Heure de début', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'StartTime';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID du propriétaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'OwnerId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'LastModifiedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de Transcription Live Chat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'Id_SF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'CreatedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'conversationID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'ConversationID__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Durée du chat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'ChatDuration';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de la requête', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'CaseId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom de la personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'AccountId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Réseau', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'VisitorNetwork';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre de messages de visiteurs', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'VisitorMessageCount';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Agent utilisateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'UserAgent';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Signature du préchat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'TCHPrechatSignature__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TCH Id externe de la personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'TCHAccountTCHId__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Horodateur des modifications du système', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'SystemModstamp';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Corps de transcription du superviseur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'SupervisorTranscriptBody';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Statut', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'Status';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de compétence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'SkillId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Résolution d''écran', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'ScreenResolution';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Heure de la requête', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'RequestTime';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Site référent', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'ReferrerUri';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID du type d''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'RecordTypeId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Plate-forme', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'Platform';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre de messages des agents', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'OperatorMessageCount';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom de la Transcription Live Chat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'Name';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Délai de réponse maximal du visiteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'MaxResponseTimeVisitor';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Délai de réponse maximal de l''agent', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'MaxResponseTimeOperator';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Emplacement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'Location';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de Visiteur Live Chat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'LiveChatVisitorId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de Déploiement Live Chat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'LiveChatDeploymentId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de bouton de chat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'LiveChatButtonId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de piste', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'LeadId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernier affichage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'LastViewedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernière date référencée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'LastReferencedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernière modification par ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'LastModifiedById';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Supprimé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'IsDeleted';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Adresse IP du visiteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'IpAddress';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Contient Mot(s) Interdit(s)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'HasForbiddenWord__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mots interdits', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'ForbiddenWords__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Heure de fin', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'EndTime';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Terminée par', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'EndedBy';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID créé par', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'CreatedById';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID du contact', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'ContactId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Clé du chat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'ChatKey';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Langue du navigateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'BrowserLanguage';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Navigateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'Browser';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Corps', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'Body';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Délai moyen de réponse des visiteurs', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'AverageResponseTimeVisitor';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Délai moyen de réponse des agents', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'AverageResponseTimeOperator';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Client authentifié lors du chat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'authenticated__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Abandonné après', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'Abandoned';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Est une session d''agent conversationnel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_LIVECHATTRANSCRIPT', @level2type = N'COLUMN', @level2name = N'IsChatbotSession';
GO