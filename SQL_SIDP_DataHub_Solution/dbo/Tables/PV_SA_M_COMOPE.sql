﻿CREATE TABLE [dbo].[PV_SA_M_COMOPE] (
    [ID]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [DWHMM4DTX]          DATE                                        NULL,
    [DWHMM4ETA]          INT                                         NULL,
    [DWHMM4AGE]          INT                                         NULL,
    [DWHMM4SER]          VARCHAR (2)                                 NULL,
    [DWHMM4SES]          VARCHAR (2)                                 NULL,
    [DWHMM4OPE]          VARCHAR (6)                                 NULL,
    [DWHMM4NAT]          VARCHAR (6)                                 NULL,
    [DWHMM4NUM]          INT                                         NULL,
    [DWHMM4SEN]          VARCHAR (1)                                 NULL,
    [DWHMM4COM]          VARCHAR (6)                                 NULL,
    [DWHMM4SEQ]          INT                                         NULL,
    [DWHMM4DTD]          DATE                                        NULL,
    [DWHMM4DEV]          VARCHAR (3)                                 NULL,
    [DWHMM4MON]          DECIMAL (18, 3)                             NULL,
    [DWHMM4MFL]          DECIMAL (18, 3)                             NULL,
    [DWHMM4DSY]          DATE                                        NULL,
    [Validity_StartDate] DATETIME                                    NULL,
    [Validity_EndDate]   DATETIME                                    DEFAULT ('99991231') NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SA_M_COMOPE] PRIMARY KEY CLUSTERED ([ID] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SA_M_COMOPEHistory], DATA_CONSISTENCY_CHECK=ON));
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE SYSTEME', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COMOPE', @level2type = N'COLUMN', @level2name = N'DWHMM4DSY';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT COMMISS° FLAT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COMOPE', @level2type = N'COLUMN', @level2name = N'DWHMM4MFL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT COMMISS°', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COMOPE', @level2type = N'COLUMN', @level2name = N'DWHMM4MON';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEVISE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COMOPE', @level2type = N'COLUMN', @level2name = N'DWHMM4DEV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DEB SITU', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COMOPE', @level2type = N'COLUMN', @level2name = N'DWHMM4DTD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° SEQUENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COMOPE', @level2type = N'COLUMN', @level2name = N'DWHMM4SEQ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE COMMISS°', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COMOPE', @level2type = N'COLUMN', @level2name = N'DWHMM4COM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SENS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COMOPE', @level2type = N'COLUMN', @level2name = N'DWHMM4SEN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMERO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COMOPE', @level2type = N'COLUMN', @level2name = N'DWHMM4NUM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NATURE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COMOPE', @level2type = N'COLUMN', @level2name = N'DWHMM4NAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'OPERATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COMOPE', @level2type = N'COLUMN', @level2name = N'DWHMM4OPE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SOUS SERVICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COMOPE', @level2type = N'COLUMN', @level2name = N'DWHMM4SES';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SERVICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COMOPE', @level2type = N'COLUMN', @level2name = N'DWHMM4SER';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AGENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COMOPE', @level2type = N'COLUMN', @level2name = N'DWHMM4AGE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ETABLISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COMOPE', @level2type = N'COLUMN', @level2name = N'DWHMM4ETA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE EXTRACTION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COMOPE', @level2type = N'COLUMN', @level2name = N'DWHMM4DTX';

