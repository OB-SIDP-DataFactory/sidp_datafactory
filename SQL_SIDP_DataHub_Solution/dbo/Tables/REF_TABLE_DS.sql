﻿CREATE TABLE [dbo].[REF_TABLE_DS] (
    [TABLE_CATALOG]            NVARCHAR (255) NULL,
    [TABLE_SCHEMA]             NVARCHAR (255) NULL,
    [TABLE_NAME]               NVARCHAR (255) NULL,
    [COLUMN_NAME]              NVARCHAR (255) NULL,
    [ORDINAL_POSITION]         FLOAT (53)     NULL,
    [Sensitive]                NVARCHAR (255) NULL,
    [COLUMN_DEFAULT]           NVARCHAR (255) NULL,
    [IS_NULLABLE]              NVARCHAR (255) NULL,
    [DATA_TYPE]                NVARCHAR (255) NULL,
    [CHARACTER_MAXIMUM_LENGTH] FLOAT (53)     NULL,
    [CHARACTER_OCTET_LENGTH]   FLOAT (53)     NULL,
    [NUMERIC_PRECISION]        NVARCHAR (255) NULL,
    [NUMERIC_PRECISION_RADIX]  NVARCHAR (255) NULL,
    [NUMERIC_SCALE]            NVARCHAR (255) NULL,
    [DATETIME_PRECISION]       NVARCHAR (255) NULL,
    [CHARACTER_SET_CATALOG]    NVARCHAR (255) NULL,
    [CHARACTER_SET_SCHEMA]     NVARCHAR (255) NULL,
    [CHARACTER_SET_NAME]       NVARCHAR (255) NULL,
    [COLLATION_CATALOG]        NVARCHAR (255) NULL,
    [COLLATION_SCHEMA]         NVARCHAR (255) NULL,
    [COLLATION_NAME]           NVARCHAR (255) NULL,
    [DOMAIN_CATALOG]           NVARCHAR (255) NULL,
    [DOMAIN_SCHEMA]            NVARCHAR (255) NULL,
    [DOMAIN_NAME]              NVARCHAR (255) NULL,
    [Traitement]               NVARCHAR (255) NULL
);

