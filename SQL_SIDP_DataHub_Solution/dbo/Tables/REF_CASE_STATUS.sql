﻿CREATE TABLE [dbo].[REF_CASE_STATUS] (
    [PK_ID]              INT            IDENTITY (1, 1) NOT NULL,
    [CODE_SF]            NVARCHAR (255) NULL,
    [LIBELLE]            NVARCHAR (255) NULL,
    [Validity_StartDate] DATETIME       CONSTRAINT [DF_REF_CASE_STATUS_Validity_StartDate] DEFAULT ('2017-01-01') NULL,
    [Validity_EndDate]   DATETIME       CONSTRAINT [DF_REF_CASE_STATUS_Validity_End_Date] DEFAULT ('9999-12-31') NULL,
    CONSTRAINT [PK_REF_CASE_STATUS] PRIMARY KEY CLUSTERED ([PK_ID] ASC)
);

