﻿CREATE TABLE [dbo].[REF_TRANSCO_RESEAU] (
    [code_reseau_n1]    VARCHAR (50) NULL,
    [libelle_reseau_n1] VARCHAR (50) NULL,
    [code_reseau_n2]    VARCHAR (50) NULL,
    [libelle_reseau_n2] VARCHAR (50) NULL,
    [code_reseau]       VARCHAR (50) NULL,
    [libelle_reseau]    VARCHAR (50) NULL,
    [date_debut]        DATE         NULL,
    [date_fin]          DATE         NULL,
    [code_apporteur]    VARCHAR (50) NULL
);

