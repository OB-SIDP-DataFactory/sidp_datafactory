﻿CREATE TABLE [dbo].[REF_CONSUMER_LOAN_SCORE_COLOR] (
    [ID]                 BIGINT         IDENTITY (1, 1) NOT NULL,
    [SCORE_ID]           DECIMAL (38)   NOT NULL,
    [REF_FAMILY_ID]      DECIMAL (19)   NULL,
    [CODE]               NVARCHAR (255) NULL,
    [PRIORITY]           DECIMAL (19)   NULL,
    [REF_LANG_ID]        DECIMAL (19)   NULL,
    [MESSAGE]            NVARCHAR (255) NULL,
    [Validity_StartDate] DATETIME2 (7)  NOT NULL,
    [Validity_EndDate]   DATETIME2 (7)  NOT NULL,
    CONSTRAINT [PK_REF_CONSUMER_LOAN_SCORE_COLOR] PRIMARY KEY CLUSTERED ([ID] ASC)
);

