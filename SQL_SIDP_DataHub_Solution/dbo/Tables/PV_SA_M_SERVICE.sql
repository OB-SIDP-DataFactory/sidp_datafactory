﻿CREATE TABLE [dbo].[PV_SA_M_SERVICE] (
    [ID]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [DWHABSDTX]          DATE                                        NULL,
    [DWHABSETA]          INT                                         NULL,
    [DWHABSAGE]          INT                                         NULL,
    [DWHABSSER]          VARCHAR (2)                                 NULL,
    [DWHABSSSE]          VARCHAR (2)                                 NULL,
    [DWHABSNUM]          INT                                         NULL,
    [DWHABSCSE]          VARCHAR (6)                                 NULL,
    [DWHABSADH]          DATE                                        NULL,
    [DWHABSFIN]          DATE                                        NULL,
    [DWHABSREN]          DATE                                        NULL,
    [DWHABSCET]          VARCHAR (1)                                 NULL,
    [DWHABSRES]          DATE                                        NULL,
    [DWHABSMOR]          VARCHAR (6)                                 NULL,
    [DWHABSCRE]          DATE                                        NULL,
    [DWHABSVAL]          DATE                                        NULL,
    [Validity_StartDate] DATETIME                                    NULL,
    [Validity_EndDate]   DATETIME                                    DEFAULT ('99991231') NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SA_M_SERVICE] PRIMARY KEY CLUSTERED ([ID] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SA_M_SERVICEHistory], DATA_CONSISTENCY_CHECK=ON));
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ANALYSE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_SERVICE', @level2type = N'COLUMN', @level2name = N'DWHABSDTX';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ETABLISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_SERVICE', @level2type = N'COLUMN', @level2name = N'DWHABSETA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AGENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_SERVICE', @level2type = N'COLUMN', @level2name = N'DWHABSAGE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SERVICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_SERVICE', @level2type = N'COLUMN', @level2name = N'DWHABSSER';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SOUS-SERVICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_SERVICE', @level2type = N'COLUMN', @level2name = N'DWHABSSSE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMÉRO CONTRAT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_SERVICE', @level2type = N'COLUMN', @level2name = N'DWHABSNUM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE SERVICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_SERVICE', @level2type = N'COLUMN', @level2name = N'DWHABSCSE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ADHÉSION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_SERVICE', @level2type = N'COLUMN', @level2name = N'DWHABSADH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE FIN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_SERVICE', @level2type = N'COLUMN', @level2name = N'DWHABSFIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE RENOUVELLEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_SERVICE', @level2type = N'COLUMN', @level2name = N'DWHABSREN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE ÉTAT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_SERVICE', @level2type = N'COLUMN', @level2name = N'DWHABSCET';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE RÉSILIATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_SERVICE', @level2type = N'COLUMN', @level2name = N'DWHABSRES';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MOTIF RÉSILIATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_SERVICE', @level2type = N'COLUMN', @level2name = N'DWHABSMOR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE CRÉATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_SERVICE', @level2type = N'COLUMN', @level2name = N'DWHABSCRE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE VALIDATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_SERVICE', @level2type = N'COLUMN', @level2name = N'DWHABSVAL';
GO
CREATE UNIQUE NONCLUSTERED INDEX [IDX_SERVICE]
    ON [dbo].[PV_SA_M_SERVICE]([DWHABSETA] ASC, [DWHABSAGE] ASC, [DWHABSSER] ASC, [DWHABSSSE] ASC, [DWHABSNUM] ASC, [DWHABSCSE] ASC);

