﻿CREATE TABLE [dbo].[IWD_RESOURCE_SKILL_FACT_] (
    [RESOURCE_SKILL_FACT_KEY] NUMERIC (19) NOT NULL,
    [START_DATE_TIME_KEY]     INT          NOT NULL,
    [END_DATE_TIME_KEY]       INT          NOT NULL,
    [TENANT_KEY]              INT          NOT NULL,
    [RESOURCE_KEY]            INT          NOT NULL,
    [SKILL_KEY]               INT          NOT NULL,
    [CREATE_AUDIT_KEY]        NUMERIC (19) NOT NULL,
    [UPDATE_AUDIT_KEY]        NUMERIC (19) NOT NULL,
    [START_TS]                INT          NULL,
    [END_TS]                  INT          NULL,
    [SKILL_LEVEL]             INT          NULL,
    [IDB_ID]                  NUMERIC (19) NOT NULL,
    [ACTIVE_FLAG]             NUMERIC (1)  NULL,
    [PURGE_FLAG]              NUMERIC (1)  NULL,
    CONSTRAINT [PK_RSRC_SKLL_FT] PRIMARY KEY CLUSTERED ([RESOURCE_SKILL_FACT_KEY] ASC)
)
GO
CREATE UNIQUE NONCLUSTERED INDEX [IWD_I_RSKL_STS_RC_SKL]
ON [dbo].[IWD_RESOURCE_SKILL_FACT_]([START_TS] ASC, [RESOURCE_KEY] ASC, [SKILL_KEY] ASC);
GO