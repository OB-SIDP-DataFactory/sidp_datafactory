﻿CREATE TABLE [dbo].[Param_OAV_TABLE_STRUCT_HISTORISATION] (
    [source_table]       VARCHAR (100) NULL,
    [source_column_name] VARCHAR (100) NULL,
    [cible_table_name]   VARCHAR (100) NULL,
    [cible_column_name]  VARCHAR (100) NULL,
    [Flg_Histo]          BIT           NULL
);

