﻿CREATE TABLE [dbo].[IWD_GIDB_G_ROUTE_RESULT_V] (
    [ID]                    NUMERIC (16)  NOT NULL,
    [CALLID]                VARCHAR (50)  NOT NULL,
    [CONNID]                VARCHAR (50)  NOT NULL,
    [PARTYID]               VARCHAR (50)  NOT NULL,
    [IRID]                  VARCHAR (50)  NULL,
    [ENDPOINTID]            INT           NULL,
    [RTARGETRULESELECTED]   VARCHAR (255) NULL,
    [RTARGETOBJECTSELECTED] VARCHAR (255) NULL,
    [RTARGETTYPESELECTED]   INT           NULL,
    [RTARGETAGENTSELECTED]  VARCHAR (255) NULL,
    [RTARGETPLACESELECTED]  VARCHAR (255) NULL,
    [RREQUESTEDSKILLCOMB]   VARCHAR (255) NULL,
    [RSTRATEGYNAME]         VARCHAR (255) NULL,
    [RTENANT]               VARCHAR (255) NULL,
    [DESTENDPOINTDN]        VARCHAR (255) NULL,
    [DESTENDPOINTID]        INT           NULL,
    [DESTENDPOINTTYPE]      INT           NULL,
    [RESULT]                INT           NOT NULL,
    [DURATION]              INT           NULL,
    [CREATED]               DATETIME      NOT NULL,
    [CREATED_TS]            INT           NULL,
    [TERMINATED]            DATETIME      NULL,
    [TERMINATED_TS]         INT           NULL,
    [GSYS_DOMAIN]           INT           NULL,
    [GSYS_SYS_ID]           INT           NULL,
    [GSYS_EXT_VCH1]         VARCHAR (255) NULL,
    [GSYS_EXT_VCH2]         VARCHAR (255) NULL,
    [GSYS_EXT_INT1]         INT           NULL,
    [GSYS_EXT_INT2]         INT           NULL,
    [CREATE_AUDIT_KEY]      NUMERIC (19)  NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [IWD_I_G_RRES_V_CTS]
    ON [dbo].[IWD_GIDB_G_ROUTE_RESULT_V]([CREATED_TS] ASC);


GO
CREATE NONCLUSTERED INDEX [IWD_I_G_RRES_V_CID]
    ON [dbo].[IWD_GIDB_G_ROUTE_RESULT_V]([CALLID] ASC);

