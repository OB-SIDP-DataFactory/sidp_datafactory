﻿CREATE TABLE [dbo].[IWD_TMP_UD_KEYS_KV_V] (
    [INTERACTION_RESOURCE_ID]    NUMERIC (19) NOT NULL,
    [UDE_KEYS_TO_DIM_MAPPING_ID] INT          NOT NULL,
    [START_DATE_TIME_KEY]        INT          NOT NULL,
    [KEYVALUE]                   INT          NOT NULL
);

