﻿CREATE TABLE [dbo].[WK_FE_REF_PERSON_RELATIONSHIP] (
    [PERSON_RELATIONSHIP_ID] DECIMAL (38)   NOT NULL,
    [FRANFINANCE_CODE]       NVARCHAR (5)   NULL,
    [SAB_STATE_CODE]         NVARCHAR (5)   NULL,
    [SAB_RELATIONSHIP_CODE]  NVARCHAR (5)   NULL,
    [REF_FAMILY_ID]          DECIMAL (19)   NULL,
    [CODE]                   NVARCHAR (255) NULL,
    [PRIORITY]               DECIMAL (19)   NULL,
    [START_DATE]             DATETIME2 (7)  NULL,
    [END_DATE]               DATETIME2 (7)  NULL,
    [REF_LANG_ID]            DECIMAL (19)   NULL,
    [MESSAGE]                NVARCHAR (255) NULL
);

