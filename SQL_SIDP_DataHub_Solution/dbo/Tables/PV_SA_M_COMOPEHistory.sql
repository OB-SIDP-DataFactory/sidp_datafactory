﻿CREATE TABLE [dbo].[PV_SA_M_COMOPEHistory] (
    [ID]                 BIGINT          NOT NULL,
    [DWHMM4DTX]          DATE            NULL,
    [DWHMM4ETA]          INT             NULL,
    [DWHMM4AGE]          INT             NULL,
    [DWHMM4SER]          VARCHAR (2)     NULL,
    [DWHMM4SES]          VARCHAR (2)     NULL,
    [DWHMM4OPE]          VARCHAR (6)     NULL,
    [DWHMM4NAT]          VARCHAR (6)     NULL,
    [DWHMM4NUM]          INT             NULL,
    [DWHMM4SEN]          VARCHAR (1)     NULL,
    [DWHMM4COM]          VARCHAR (6)     NULL,
    [DWHMM4SEQ]          INT             NULL,
    [DWHMM4DTD]          DATE            NULL,
    [DWHMM4DEV]          VARCHAR (3)     NULL,
    [DWHMM4MON]          DECIMAL (18, 3) NULL,
    [DWHMM4MFL]          DECIMAL (18, 3) NULL,
    [DWHMM4DSY]          DATE            NULL,
    [Validity_StartDate] DATETIME        NULL,
    [Validity_EndDate]   DATETIME        NULL,
    [Startdt]            DATETIME2 (7)   NOT NULL,
    [Enddt]              DATETIME2 (7)   NOT NULL
);

