﻿CREATE TABLE [dbo].[PV_SF_ACCOUNT_ALL] (
    [Id]                                   BIGINT           NOT NULL,
    [Id_SF]                                NVARCHAR (18)    NULL,
    [IsDeleted]                            NVARCHAR (10)    NULL,
    [MasterRecordId]                       NVARCHAR (18)    NULL,
    [Name]                                 NVARCHAR (255)   NULL,
    [LastName]                             NVARCHAR (80)    NULL,
    [FirstName]                            NVARCHAR (40)    NULL,
    [Salutation]                           NVARCHAR (40)    NULL,
    [Type]                                 NVARCHAR (40)    NULL,
    [RecordTypeId]                         NVARCHAR (18)    NULL,
    [ParentId]                             NVARCHAR (18)    NULL,
    [BillingStreet]                        NVARCHAR (255)   NULL,
    [BillingCity]                          NVARCHAR (40)    NULL,
    [BillingState]                         NVARCHAR (80)    NULL,
    [BillingPostalCode]                    NVARCHAR (20)    NULL,
    [BillingCountry]                       NVARCHAR (80)    NULL,
    [BillingStateCode]                     NVARCHAR (10)    NULL,
    [BillingCountryCode]                   NVARCHAR (10)    NULL,
    [BillingLatitude]                      DECIMAL (18, 15) NULL,
    [BillingLongitude]                     DECIMAL (18, 15) NULL,
    [BillingGeocodeAccuracy]               NVARCHAR (40)    NULL,
    [ShippingStreet]                       NVARCHAR (255)   NULL,
    [ShippingCity]                         NVARCHAR (40)    NULL,
    [ShippingState]                        NVARCHAR (80)    NULL,
    [ShippingPostalCode]                   NVARCHAR (20)    NULL,
    [ShippingCountry]                      NVARCHAR (80)    NULL,
    [ShippingStateCode]                    NVARCHAR (10)    NULL,
    [ShippingCountryCode]                  NVARCHAR (10)    NULL,
    [ShippingLatitude]                     DECIMAL (18, 15) NULL,
    [ShippingLongitude]                    DECIMAL (18, 15) NULL,
    [ShippingGeocodeAccuracy]              NVARCHAR (40)    NULL,
    [Phone]                                NVARCHAR (40)    NULL,
    [Fax]                                  NVARCHAR (40)    NULL,
    [AccountNumber]                        NVARCHAR (40)    NULL,
    [Website]                              NVARCHAR (255)   NULL,
    [PhotoUrl]                             NVARCHAR (255)   NULL,
    [Sic]                                  NVARCHAR (20)    NULL,
    [Industry]                             NVARCHAR (40)    NULL,
    [AnnualRevenue]                        DECIMAL (18)     NULL,
    [NumberOfEmployees]                    INT              NULL,
    [Ownership]                            NVARCHAR (40)    NULL,
    [TickerSymbol]                         NVARCHAR (20)    NULL,
    [Rating]                               NVARCHAR (40)    NULL,
    [Site]                                 NVARCHAR (80)    NULL,
    [OwnerId]                              NVARCHAR (18)    NULL,
    [CreatedDate]                          DATETIME         NULL,
    [CreatedById]                          NVARCHAR (18)    NULL,
    [LastModifiedDate]                     DATETIME         NULL,
    [LastModifiedById]                     NVARCHAR (18)    NULL,
    [SystemModstamp]                       DATETIME         NULL,
    [LastActivityDate]                     DATE             NULL,
    [LastViewedDate]                       DATETIME         NULL,
    [LastReferencedDate]                   DATETIME         NULL,
    [IsCustomerPortal]                     NVARCHAR (10)    NULL,
    [PersonContactId]                      NVARCHAR (18)    NULL,
    [IsPersonAccount]                      NVARCHAR (10)    NULL,
    [PersonMailingStreet]                  NVARCHAR (255)   NULL,
    [PersonMailingCity]                    NVARCHAR (40)    NULL,
    [PersonMailingState]                   NVARCHAR (80)    NULL,
    [PersonMailingPostalCode]              NVARCHAR (20)    NULL,
    [PersonMailingCountry]                 NVARCHAR (80)    NULL,
    [PersonMailingStateCode]               NVARCHAR (10)    NULL,
    [PersonMailingCountryCode]             NVARCHAR (10)    NULL,
    [PersonMailingLatitude]                DECIMAL (18, 15) NULL,
    [PersonMailingLongitude]               DECIMAL (18, 15) NULL,
    [PersonMailingGeocodeAccuracy]         NVARCHAR (40)    NULL,
    [PersonOtherStreet]                    NVARCHAR (255)   NULL,
    [PersonOtherCity]                      NVARCHAR (40)    NULL,
    [PersonOtherState]                     NVARCHAR (80)    NULL,
    [PersonOtherPostalCode]                NVARCHAR (20)    NULL,
    [PersonOtherCountry]                   NVARCHAR (80)    NULL,
    [PersonOtherStateCode]                 NVARCHAR (10)    NULL,
    [PersonOtherCountryCode]               NVARCHAR (10)    NULL,
    [PersonOtherLatitude]                  DECIMAL (18, 15) NULL,
    [PersonOtherLongitude]                 DECIMAL (18, 15) NULL,
    [PersonOtherGeocodeAccuracy]           NVARCHAR (40)    NULL,
    [PersonMobilePhone]                    NVARCHAR (40)    NULL,
    [PersonHomePhone]                      NVARCHAR (40)    NULL,
    [PersonOtherPhone]                     NVARCHAR (40)    NULL,
    [PersonAssistantPhone]                 NVARCHAR (40)    NULL,
    [PersonEmail]                          NVARCHAR (80)    NULL,
    [PersonTitle]                          NVARCHAR (80)    NULL,
    [PersonDepartment]                     NVARCHAR (80)    NULL,
    [PersonAssistantName]                  NVARCHAR (40)    NULL,
    [PersonLeadSource]                     NVARCHAR (40)    NULL,
    [PersonBirthdate]                      DATE             NULL,
    [PersonHasOptedOutOfEmail]             NVARCHAR (10)    NULL,
    [PersonHasOptedOutOfFax]               NVARCHAR (10)    NULL,
    [PersonDoNotCall]                      NVARCHAR (10)    NULL,
    [PersonLastCURequestDate]              DATETIME         NULL,
    [PersonLastCUUpdateDate]               DATETIME         NULL,
    [PersonEmailBouncedReason]             NVARCHAR (255)   NULL,
    [PersonEmailBouncedDate]               DATETIME         NULL,
    [Jigsaw]                               NVARCHAR (20)    NULL,
    [JigsawCompanyId]                      NVARCHAR (20)    NULL,
    [AccountSource]                        NVARCHAR (40)    NULL,
    [SicDesc]                              NVARCHAR (80)    NULL,
    [BankingActivityRate__c]               NVARCHAR (255)   NULL,
    [BehaviouralScoring__c]                NVARCHAR (255)   NULL,
    [AddresseState__c]                     NVARCHAR (1300)  NULL,
    [AttestedClient__c]                    NVARCHAR (10)    NULL,
    [Expenses__c]                          NVARCHAR (2)     NULL,
    [FinancialPrivacy__c]                  NVARCHAR (255)   NULL,
    [FiscalCountry__c]                     NVARCHAR (255)   NULL,
    [Income__c]                            NVARCHAR (1)     NULL,
    [DistributorNetwork__c]                NVARCHAR (255)   NULL,
    [TCHId__c]                             NVARCHAR (18)    NULL,
    [AccountType__c]                       NVARCHAR (255)   NULL,
    [RealizedBy__c]                        NVARCHAR (18)    NULL,
    [TCHPartnerVisibility__c]              NVARCHAR (10)    NULL,
    [WrongAddress__c]                      NVARCHAR (10)    NULL,
    [SFMCTest__c]                          NVARCHAR (10)    NULL,
    [OBFirstContactDate__c]                DATE             NULL,
    [OBSeniority__c]                       NVARCHAR (1300)  NULL,
    [OBTerminationDate__c]                 DATE             NULL,
    [OBTerminationReason__c]               NVARCHAR (255)   NULL,
    [OFFirstContactDate__c]                DATETIME         NULL,
    [OFSeniority__c]                       DECIMAL (3)      NULL,
    [OFTerminationDate__c]                 DATE             NULL,
    [OFTerminationReason__c]               NVARCHAR (255)   NULL,
    [OccupationArea__c]                    NVARCHAR (255)   NULL,
    [Preattribution__c]                    NVARCHAR (255)   NULL,
    [Agency__c]                            NVARCHAR (18)    NULL,
    [BusinessPhone__c]                     NVARCHAR (40)    NULL,
    [CodeNAFRev2__c]                       NVARCHAR (5)     NULL,
    [DistributorData__c]                   NVARCHAR (MAX)   NULL,
    [BanLimitDate__c]                      DATE             NULL,
    [ShopNetworkMesh__c]                   NVARCHAR (255)   NULL,
    [BloquedAccount__c]                    NVARCHAR (10)    NULL,
    [CheckbookBanOrigin__c]                NVARCHAR (255)   NULL,
    [CheckbookBan__c]                      NVARCHAR (255)   NULL,
    [DistributorNetworks__c]               NVARCHAR (4000)  NULL,
    [FiveYearsRefusalDate__c]              DATE             NULL,
    [GeolifeSegment__c]                    NVARCHAR (100)   NULL,
    [IDCustomerBA__c]                      NVARCHAR (11)    NULL,
    [IDCustomerCID__c]                     NVARCHAR (15)    NULL,
    [IDCustomerPID__c]                     NVARCHAR (15)    NULL,
    [IDCustomerSAB__c]                     NVARCHAR (15)    NULL,
    [OverdraftExcess__c]                   NVARCHAR (10)    NULL,
    [TchMigError__c]                       NVARCHAR (10)    NULL,
    [Email__c]                             NVARCHAR (80)    NULL,
    [Recovery__c]                          NVARCHAR (10)    NULL,
    [RefusalFlag__c]                       NVARCHAR (1300)  NULL,
    [SixMonthsRefusalDate__c]              DATE             NULL,
    [UncertainCustomer__c]                 NVARCHAR (10)    NULL,
    [AdditionToAddress__c]                 NVARCHAR (255)   NULL,
    [Alert__c]                             NVARCHAR (255)   NULL,
    [BankRating__c]                        NVARCHAR (1)     NULL,
    [CreateIDCustomerBA__c]                NVARCHAR (10)    NULL,
    [HomePhone__c]                         NVARCHAR (40)    NULL,
    [ErrorSavingRIB__c]                    DATETIME         NULL,
    [InseeCode__c]                         NVARCHAR (5)     NULL,
    [IsFiscalResidencyInFrance__c]         NVARCHAR (10)    NULL,
    [IsPhoneCompatible__c]                 NVARCHAR (10)    NULL,
    [IsSIMCardCompatible__c]               NVARCHAR (10)    NULL,
    [MobileOS__c]                          NVARCHAR (255)   NULL,
    [IncomeGatheredOn__c]                  DATE             NULL,
    [LabelNAFRev2__c]                      NVARCHAR (80)    NULL,
    [OptInDataTelco__c]                    NVARCHAR (10)    NULL,
    [OptInOrderTelco__c]                   NVARCHAR (10)    NULL,
    [LegalForm__c]                         NVARCHAR (255)   NULL,
    [MigratedClient__c]                    NVARCHAR (10)    NULL,
    [QuoteRisk__c]                         NVARCHAR (255)   NULL,
    [ShopBrand__c]                         NVARCHAR (64)    NULL,
    [ShopCode__c]                          NVARCHAR (64)    NULL,
    [ShopManagerFirstName__c]              NVARCHAR (64)    NULL,
    [ShopManagerLastName__c]               NVARCHAR (64)    NULL,
    [MobilePhone__c]                       NVARCHAR (40)    NULL,
    [SubscriberOwnerIndicator__c]          NVARCHAR (10)    NULL,
    [USIndicator__c]                       NVARCHAR (10)    NULL,
    [NumSiret__c]                          NVARCHAR (14)    NULL,
    [PreferredPhoneNumber__c]              NVARCHAR (255)   NULL,
    [PrimaryNetwork__c]                    NVARCHAR (255)   NULL,
    [RightOpposition__c]                   NVARCHAR (255)   NULL,
    [SalesAgent__c]                        NVARCHAR (80)    NULL,
    [SegmentationDistributive__c]          NVARCHAR (255)   NULL,
    [SegmentationSaving__c]                NVARCHAR (255)   NULL,
    [SocialReason__c]                      NVARCHAR (80)    NULL,
    [TCHFirstEnrolement__c]                NVARCHAR (10)    NULL,
    [Turnover__c]                          DECIMAL (18, 2)  NULL,
    [UndeliverabilityReason__c]            NVARCHAR (255)   NULL,
    [VIPParnasseCustomer__c]               NVARCHAR (255)   NULL,
    [AntenuptialSettlement__pc]            NVARCHAR (255)   NULL,
    [BirthCity__pc]                        NVARCHAR (255)   NULL,
    [BirthCountry__pc]                     NVARCHAR (255)   NULL,
    [BirthDepartment__pc]                  NVARCHAR (80)    NULL,
    [CreateIDCustomer__pc]                 NVARCHAR (10)    NULL,
    [ClientProfileLastModif__pc]           DATE             NULL,
    [DeathDate__pc]                        DATE             NULL,
    [DependentChildrenNb__pc]              DECIMAL (2)      NULL,
    [TCHSalutation__pc]                    NVARCHAR (1300)  NULL,
    [EmploymentContractEndDate__pc]        DATE             NULL,
    [EmploymentContractStartDate__pc]      DATE             NULL,
    [EmploymentContractType__pc]           NVARCHAR (255)   NULL,
    [AcceptationMembershipDate__pc]        DATE             NULL,
    [JustifiedIncomes__pc]                 DECIMAL (18, 2)  NULL,
    [IDCustomerSAB__pc]                    NVARCHAR (15)    NULL,
    [IDCustomer__pc]                       NVARCHAR (11)    NULL,
    [IncomeGatheredOn__pc]                 DATE             NULL,
    [InvestorProfile__pc]                  NVARCHAR (255)   NULL,
    [MaidenName__pc]                       NVARCHAR (80)    NULL,
    [MaritalStatus__pc]                    NVARCHAR (255)   NULL,
    [MonthExpenses__pc]                    DECIMAL (18, 2)  NULL,
    [MonthFoncIncome__pc]                  DECIMAL (18, 2)  NULL,
    [MonthMobIncome__pc]                   DECIMAL (18, 2)  NULL,
    [MonthNetIncome__pc]                   DECIMAL (18, 2)  NULL,
    [MonthOtherExpenses__pc]               DECIMAL (18, 2)  NULL,
    [MonthOtherIncomes__pc]                DECIMAL (18, 2)  NULL,
    [MonthOtherLoans__pc]                  DECIMAL (18, 2)  NULL,
    [MonthPaidPension__pc]                 DECIMAL (18, 2)  NULL,
    [MonthReceivPension__pc]               DECIMAL (18, 2)  NULL,
    [MonthSocBenefits__pc]                 DECIMAL (18, 2)  NULL,
    [MonthTaxes__pc]                       DECIMAL (18, 2)  NULL,
    [Nationality__pc]                      NVARCHAR (255)   NULL,
    [OccupationNiv1__pc]                   NVARCHAR (255)   NULL,
    [OccupationNiv3__pc]                   NVARCHAR (255)   NULL,
    [Occupation__pc]                       NVARCHAR (255)   NULL,
    [PersonalProperty__pc]                 DECIMAL (18, 2)  NULL,
    [PhoneticFirstname__pc]                NVARCHAR (255)   NULL,
    [PhoneticLastName__pc]                 NVARCHAR (255)   NULL,
    [PhoneticMaidenName__pc]               NVARCHAR (255)   NULL,
    [Person__pc]                           NVARCHAR (18)    NULL,
    [PreferredPhoneNumber__pc]             NVARCHAR (255)   NULL,
    [PrimaryResidOccupationType__pc]       NVARCHAR (255)   NULL,
    [PrimaryResidenceEntryDate__pc]        DATE             NULL,
    [RealEstate__pc]                       DECIMAL (18, 2)  NULL,
    [RightOpposition__pc]                  NVARCHAR (255)   NULL,
    [TotalDisposableIncome__pc]            DECIMAL (18, 2)  NULL,
    [TotalExpenses__pc]                    DECIMAL (18, 2)  NULL,
    [TotalIncome__pc]                      DECIMAL (18, 2)  NULL,
    [USNationality__pc]                    NVARCHAR (255)   NULL,
    [UndeliverabilityReason__pc]           NVARCHAR (255)   NULL,
    [WealthTax__pc]                        NVARCHAR (255)   NULL,
    [ActivityBranch__pc]                   NVARCHAR (255)   NULL,
    [BirthPostalCode__pc]                  NVARCHAR (5)     NULL,
    [EmployeeType__pc]                     NVARCHAR (255)   NULL,
    [InvalideAdr__pc]                      NVARCHAR (10)    NULL,
    [LegalCapacity__pc]                    NVARCHAR (255)   NULL,
    [NoCall__pc]                           NVARCHAR (10)    NULL,
    [NoLetter__pc]                         NVARCHAR (10)    NULL,
    [NoOrangeEmail__pc]                    NVARCHAR (10)    NULL,
    [NoOrangeSMS__pc]                      NVARCHAR (10)    NULL,
    [NoPartnerEmail__pc]                   NVARCHAR (10)    NULL,
    [NoPartnerSMS__pc]                     NVARCHAR (10)    NULL,
    [PhoneEligibility__pc]                 NVARCHAR (255)   NULL,
    [RunUnicityCheck__pc]                  NVARCHAR (10)    NULL,
    [AdditionToAddress__pc]                NVARCHAR (255)   NULL,
    [VIPParnasseCustomer__pc]              NVARCHAR (255)   NULL,
    [FacebookAlias__pc]                    NVARCHAR (80)    NULL,
    [SecondEmail__pc]                      NVARCHAR (80)    NULL,
    [SellerHashedCuid__pc]                 NVARCHAR (64)    NULL,
    [TwitterAlias__pc]                     NVARCHAR (80)    NULL,
    [USIndicator__pc]                      NVARCHAR (10)    NULL,
    [ApplicationMembershipDate__pc]        DATE             NULL,
    [BIC__pc]                              NVARCHAR (11)    NULL,
    [CreditInProgressExceptOB__pc]         DECIMAL (18, 2)  NULL,
    [DistributorNetwork__pc]               NVARCHAR (4000)  NULL,
    [FamilyAllocation__pc]                 DECIMAL (18)     NULL,
    [HousingAllocation__pc]                DECIMAL (18)     NULL,
    [IBAN__pc]                             NVARCHAR (34)    NULL,
    [IDCustomerCID__pc]                    NVARCHAR (15)    NULL,
    [IsAMF__pc]                            NVARCHAR (10)    NULL,
    [IsContactAccountSync__pc]             NVARCHAR (10)    NULL,
    [IsIOBSP__pc]                          NVARCHAR (255)   NULL,
    [IsTA3C__pc]                           NVARCHAR (10)    NULL,
    [JustifiedIncomesData__pc]             NVARCHAR (90)    NULL,
    [Manager__pc]                          NVARCHAR (255)   NULL,
    [Preattribution__pc]                   DECIMAL (4)      NULL,
    [Prescripteur__pc]                     NVARCHAR (255)   NULL,
    [RCSNumber__pc]                        NVARCHAR (255)   NULL,
    [TaxResidenceInFrance__pc]             NVARCHAR (10)    NULL,
    [ClientDataUpdate__c]                  NVARCHAR (10)    NULL,
    [SCPIClient__c]                        NVARCHAR (10)    NULL,
    [LastClientDataUpdate__c]              DATETIME         NULL,
    [PropagationDate__c]                   DATETIME         NULL,
    [TCHCivility__c]                       NVARCHAR (1300)  NULL,
    [TCH_TodayDate__c]                     NVARCHAR (1300)  NULL,
    [TCHTracingOpportunity__c]             NVARCHAR (10)    NULL,
    [Description]                          NVARCHAR (MAX)   NULL,
    [ChoicesForAdress__c]                  NVARCHAR (MAX)   NULL,
    [IDCustomer__c]                        NVARCHAR (11)    NULL,
    [TCHIDCustomer__c]                     NVARCHAR (11)    NULL,
    [BetaProducts__c]                      NVARCHAR (255)   NULL,
    [OptInP2PTransfert__c]                 NVARCHAR (10)    NULL,
    [AgenceCommerciale__c]                 NVARCHAR (255)   NULL,
    [Commercialprincipal__c]               NVARCHAR (80)    NULL,
    [Exercicereference__c]                 DATE             NULL,
    [Finterlocuteur__c]                    NVARCHAR (80)    NULL,
    [Interlocuteur__c]                     NVARCHAR (80)    NULL,
    [NoconformitePJ__c]                    NVARCHAR (10)    NULL,
    [CustomerClub__c]                      NVARCHAR (10)    NULL,
    [AnomalieNonConformite__c]             NVARCHAR (4000)  NULL,
    [NoPersoAlerts__pc]                    NVARCHAR (10)    NULL,
    [et4ae5__HasOptedOutOfMobile__pc]      NVARCHAR (10)    NULL,
    [et4ae5__Mobile_Country_Code__pc]      NVARCHAR (255)   NULL,
    [DoubleNationality__pc]                NVARCHAR (255)   NULL,
    [ProductsNoEmail__pc]                  NVARCHAR (4000)  NULL,
    [SocialReason__pc]                     NVARCHAR (80)    NULL,
    [FiscalCountry__pc]                    NVARCHAR (255)   NULL,
    [GeolifeSegment__pc]                   NVARCHAR (100)   NULL,
    [TCHId__pc]                            NVARCHAR (15)    NULL,
    [Customer_premium__pc]                 NVARCHAR (10)    NULL,
    [LastModificationJustifiedIncomes__pc] DATE             NULL,
    [BusinessPhone__pc]                    NVARCHAR (40)    NULL,
    [CodeNAFRev2__pc]                      NVARCHAR (4)     NULL,
    [Interlocuteur__pc]                    NVARCHAR (80)    NULL,
    [LabelNAFRev2__pc]                     NVARCHAR (80)    NULL,
    [MainCommercial__pc]                   NVARCHAR (80)    NULL,
    [NumSiret__pc]                         NVARCHAR (14)    NULL,
    [ShopNetworkMesh__pc]                  NVARCHAR (255)   NULL,
    [Turnover__pc]                         DECIMAL (18, 2)  NULL,
    [agenceCommerciale__pc]                NVARCHAR (255)   NULL,
    [exerciceReference__pc]                DATE             NULL,
    [fonctionInterlocuteur__pc]            NVARCHAR (80)    NULL,
    [nomPersonne__pc]                      NVARCHAR (80)    NULL,
    [KYCStatus__pc]                        NVARCHAR (255)   NULL,
    [KYCValidationDate__pc]                DATE             NULL,
    [Tech_Temp_Habilitation_IOBSP__pc]     NVARCHAR (10)    NULL,
    [KiosqueRegistrationDate__pc]          DATE             NULL,
    [MobilityDate__pc]                     DATE             NULL,
    [NoAccountBalanceAlerts__pc]           NVARCHAR (10)    NULL,
    [NoPaymentAlerts__pc]                  NVARCHAR (10)    NULL,
    [FlowAction__pc]                       NVARCHAR (255)   NULL,
    [IDInwebo__c]                          NVARCHAR (1300)  NULL,
    [TCH_LinkSponsor__c]                   DECIMAL (18, 2)  NULL,
    [PersonIndividualId]                   NVARCHAR (18)    NULL,
    [PersonRGPD__c]                        BIT              NULL,
    [DigitalScore__pc]                     DECIMAL (4)      NULL,
    [TCH_IsDistributor__c]                 BIT              NULL,
    [IsOpenIncidentExisted__c]             BIT              NULL,
    [OFFirstContactDate__pc]               DATETIME         NULL,
    [IDGRC__c]                             NVARCHAR (1300)  NULL,
    [Validity_StartDate]                   DATETIME2 (7)    NOT NULL,
    [Validity_EndDate]                     DATETIME2 (7)    NOT NULL,
    [Startdt]                              DATETIME2 (7)    NOT NULL,
    [Enddt]                                DATETIME2 (7)    NOT NULL
)
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'*Invalide adresse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'InvalideAdr__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Action Flux', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'FlowAction__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Adresse e-mail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonEmail'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Adresse email 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'SecondEmail__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Adresse incorrecte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'WrongAddress__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Agence commerciale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'AgenceCommerciale__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Agence commerciale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'agenceCommerciale__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Agence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Agency__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Alerte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Alert__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Alias Facebook', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'FacebookAlias__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Alias Twitter', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'TwitterAlias__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Allocation logement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'HousingAllocation__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Allocations familiales', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'FamilyAllocation__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ancienneté OB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'OBSeniority__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ancienneté OF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'OFSeniority__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Anomalie de non coformité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'AnomalieNonConformite__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Assistant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonAssistantName'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Asst. Téléphone', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonAssistantPhone'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autre code de pays', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonOtherCountryCode'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autre code de région/province', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonOtherStateCode'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autre code postal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonOtherPostalCode'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autre latitude', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonOtherLatitude'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autre longitude', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonOtherLongitude'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autre pays', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonOtherCountry'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autre rue', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonOtherStreet'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autre région/province', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonOtherState'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autre téléphone', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonOtherPhone'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autre ville', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonOtherCity'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autres charges mensuelles', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'MonthOtherExpenses__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autres revenus mensuels', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'MonthOtherIncomes__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'BIC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'BIC__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Billing Geocode Accuracy', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'BillingGeocodeAccuracy'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Canal d''interaction', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonLeadSource'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Capacité juridique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'LegalCapacity__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Catégorie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Type'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Certification AMF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'IsAMF__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Charges mensuelles', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'MonthExpenses__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Charges', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Expenses__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Chiffre d''affaires annuel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'AnnualRevenue'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Chiffre d''affaires', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Turnover__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Chiffre d''affaires', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Turnover__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Civilité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Salutation'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Client Douteux', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'UncertainCustomer__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Client Premium', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Customer_premium__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Client VIP / First / Parnasse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'VIPParnasseCustomer__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Client VIP/First/Parnasse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'VIPParnasseCustomer__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Client authentifié', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'AttestedClient__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Client migré', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'MigratedClient__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Club client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'CustomerClub__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Clé Data.com', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Jigsaw'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Insee de la ville de naissance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'InseeCode__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code NAF Rev2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'CodeNAFRev2__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code NAF Rev2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'CodeNAFRev2__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code SIC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Sic'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code boutique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'ShopCode__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code de pays d''expédition', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'ShippingCountryCode'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code de pays de publipostage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonMailingCountryCode'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code de région/province d''expédition', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'ShippingStateCode'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code de région/province de facturation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'BillingStateCode'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code de région/province de publipostage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonMailingStateCode'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code national mobile', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'et4ae5__Mobile_Country_Code__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code postal d''expédition', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'ShippingPostalCode'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code postal de naissance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'BirthPostalCode__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code postal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'BillingPostalCode'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code postal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonMailingPostalCode'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'BillingCountryCode'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Commercial en charge', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'SalesAgent__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Commercial principal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Commercialprincipal__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Commercial principal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'MainCommercial__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Commune de naissance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'BirthCity__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Compatibilité SIM', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'IsSIMCardCompatible__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Compatibilité Téléphone', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'IsPhoneCompatible__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Complément d''adresse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'AdditionToAddress__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Complément d''adresse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'AdditionToAddress__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Compte bloqué', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'BloquedAccount__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Compte du portail Client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'IsCustomerPortal'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cotation Banque', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'BankRating__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cotation/Risque', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'QuoteRisk__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cote de la personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Rating'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CreateIDCustomer', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'CreateIDCustomer__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CreateIDCustomerBA', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'CreateIDCustomerBA__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Crédit en cours hors Orange Bank', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'CreditInProgressExceptOB__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date confirmation adhésion', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'AcceptationMembershipDate__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date d''entrée dans résidence principale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PrimaryResidenceEntryDate__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date d''entrée en relation avec OB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'OBFirstContactDate__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date d''entrée en relation avec OF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'OFFirstContactDate__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date d''inscription Kiosque', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'KiosqueRegistrationDate__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'CreatedDate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernier affichage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'LastViewedDate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'LastModifiedDate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière requête Rester en contact', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonLastCURequestDate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de début du contrat de travail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'EmploymentContractStartDate__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de décès', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'DeathDate__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de fin de relation avec OB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'OBTerminationDate__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de fin de relation avec OF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'OFTerminationDate__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de fin du contrat de travail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'EmploymentContractEndDate__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de mobilité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'MobilityDate__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de renvoi d''e-mail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonEmailBouncedDate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de validation du KYC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'KYCValidationDate__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date demande adhésion', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'ApplicationMembershipDate__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date du dernier enregistrement Rester en contact', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonLastCUUpdateDate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date limite d''interdit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'BanLimitDate__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernière activité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'LastActivityDate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernière date référencée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'LastReferencedDate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernière modification du revenu justifié', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'LastModificationJustifiedIncomes__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernière modification par ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'LastModifiedById'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Description SIC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'SicDesc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Description de la personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Description'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Domicile', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'HomePhone__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Domicile', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonHomePhone'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Données Revenus Justifiés', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'JustifiedIncomesData__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Double nationalité américaine', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'DoubleNationality__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Droit d''opposition', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'RightOpposition__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Droit d''opposition', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'RightOpposition__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Département de naissance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'BirthDepartment__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dépassement de découvert', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'OverdraftExcess__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Désinscription d''adresse e-mail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonHasOptedOutOfEmail'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Désinscription de télécopie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonHasOptedOutOfFax'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Désinscription mobile', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'et4ae5__HasOptedOutOfMobile__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'E-mail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Email__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Employés', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'NumberOfEmployees'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Enregistrement du RIB impossible', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'ErrorSavingRIB__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Enseigne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'ShopBrand__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Est une personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'IsPersonAccount'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Etat de l''adresse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'AddresseState__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Exercice de référence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Exercicereference__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Exercice de référence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'exerciceReference__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fonction de l''interlocuteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Finterlocuteur__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fonction de l''interlocuteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'fonctionInterlocuteur__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fonction', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonTitle'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Forme Juridique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'LegalForm__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Forme juridique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Ownership'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Manager__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Habilitation IOBSP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'IsIOBSP__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Habilitation TA3C', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'IsTA3C__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Horodateur des modifications du système', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'SystemModstamp'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IBAN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'IBAN__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID Client SAB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'IDCustomerSAB__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID Client SAB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'IDCustomerSAB__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID Inwebo', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'IDInwebo__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID créé par', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'CreatedById'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID d''enregistrement principal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'MasterRecordId'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID d''individu', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonIndividualId'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de la personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Id_SF'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ISF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'WealthTax__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant client BA', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'IDCustomerBA__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'IDCustomer__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant du conseiller', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'SellerHashedCuid__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Impôts mensuels', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'MonthTaxes__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Indice d''américanité (FATCA)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'USIndicator__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Indice d''américanité (FATCA)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'USIndicator__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Infos Distrib', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'DistributorData__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Interdiction chéquier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'CheckbookBan__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Interlocuteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Interlocuteur__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Interlocuteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Interlocuteur__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IsContactAccountSync', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'IsContactAccountSync__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Jigsaw Company ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'JigsawCompanyId'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Latitude de l''adresse d''expédition', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'ShippingLatitude'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Latitude de l''adresse de facturation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'BillingLatitude'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Latitude de publipostage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonMailingLatitude'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé NAF Rev2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'LabelNAFRev2__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé NAF Rev2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'LabelNAFRev2__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Longitude de l''adresse d''expédition', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'ShippingLongitude'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Longitude de l''adresse de facturation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'BillingLongitude'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Longitude de publipostage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonMailingLongitude'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mailing Geocode Accuracy', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonMailingGeocodeAccuracy'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Marché', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'AccountType__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mensualités des autres crédits en cours', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'MonthOtherLoans__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mobile', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'MobilePhone__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mobile', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonMobilePhone'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motif de fin de relation OB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'OBTerminationReason__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motif de fin de relation OF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'OFTerminationReason__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motif de renvoi d''e-mail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonEmailBouncedReason'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motifs retours courriers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'UndeliverabilityReason__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motifs retours courriers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'UndeliverabilityReason__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nationalité américaine', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'USNationality__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nationalité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Nationality__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ne pas appeler', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonDoNotCall'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Niveau d''activité CAV', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'BankingActivityRate__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom d''usage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'LastName'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom de la personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Name'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom de la personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'nomPersonne__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom de naissance phonétique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PhoneticMaidenName__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom de naissance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'MaidenName__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom du responsable', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'ShopManagerLastName__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom phonétique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PhoneticLastName__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre d''enfants à charge', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'DependentChildrenNb__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Non conformité PJ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'NoconformitePJ__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Client GRC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'TCHIDCustomer__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro Client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'IDCustomer__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro PID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'IDCustomerPID__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro RCS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'RCSNumber__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro de la personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'AccountNumber'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° Client Distributeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'IDCustomerCID__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° SIRET', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'NumSiret__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° SIRET', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'NumSiret__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° de télécopie de la personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Fax'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° de téléphone de la personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Phone'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° et Rue', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'BillingStreet'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Né(e) le', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonBirthdate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Opt-in données Telco', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'OptInDataTelco__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Opt-in facture Telco', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'OptInOrderTelco__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Origine de l''interdit chéquier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'CheckbookBanOrigin__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Other Geocode Accuracy', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonOtherGeocodeAccuracy'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'P2P Transfert', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'OptInP2PTransfert__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Patrimoine immobilier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'RealEstate__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Patrimoine mobilier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonalProperty__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Pays d''expédition', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'ShippingCountry'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Pays de naissance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'BirthCountry__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Pays de résidence fiscale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'FiscalCountry__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Pays de résidence fiscale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'FiscalCountry__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Pays', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'BillingCountry'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Pays', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonMailingCountry'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Pensions alimentaires mensuelles reçues', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'MonthReceivPension__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Pensions alimentaires mensuelles versées', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'MonthPaidPension__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Personne principal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'ParentId'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonContactId'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Person__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prescripteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Prescripteur__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prestations sociales mensuelles', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'MonthSocBenefits__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Produit beta', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'BetaProducts__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Profession PCS principale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'OccupationNiv1__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Profession PCS secondaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'OccupationNiv3__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Profession', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Occupation__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Profil investisseur mis à jour le', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'ClientProfileLastModif__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Profil investisseur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'InvestorProfile__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Propositions d''adresses', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'ChoicesForAdress__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Propriétaire de la personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'OwnerId'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Pré-attribution', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Preattribution__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prénom du responsable', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'ShopManagerFirstName__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prénom phonétique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PhoneticFirstname__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prénom', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'FirstName'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Qualification employé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'EmployeeType__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Raison Sociale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'SocialReason__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Raison Sociale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'SocialReason__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Recouvrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Recovery__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus 5 ans', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'FiveYearsRefusalDate__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus 6 mois', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'SixMonthsRefusalDate__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus E-mail partenaires', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'NoPartnerEmail__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus E-mail produits', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'ProductsNoEmail__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus E-mail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'NoOrangeEmail__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus SMS Partenaires', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'NoPartnerSMS__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus SMS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'NoOrangeSMS__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus Souscription', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'RefusalFlag__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus appels téléphoniques', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'NoCall__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus contact courrier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'NoLetter__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus notification paiement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'NoPaymentAlerts__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus notifications solde', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'NoAccountBalanceAlerts__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus recommandation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'NoPersoAlerts__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Revenus collectés le', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'IncomeGatheredOn__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Revenus collectés le', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'IncomeGatheredOn__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Revenus fonciers mensuels', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'MonthFoncIncome__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Revenus justifiés', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'JustifiedIncomes__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Revenus mensuels nets', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'MonthNetIncome__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Revenus mobiliers mensuels', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'MonthMobIncome__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Revenus', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Income__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Rue d''expédition', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'ShippingStreet'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Rue', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonMailingStreet'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RunUnicityCheck', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'RunUnicityCheck__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Réalisé par', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'RealizedBy__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Régime matrimonial', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'AntenuptialSettlement__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Région/Province d''expédition', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'ShippingState'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Région/Province', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'BillingState'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Région/Province', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonMailingState'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Réseau distributeur principal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PrimaryNetwork__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Réseaux distributeurs', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'DistributorNetwork__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Réseaux distributeurs', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'DistributorNetworks__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Résidence Fiscal en France', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'IsFiscalResidencyInFrance__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Résidence fiscale en France', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'TaxResidenceInFrance__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SFMCTest', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'SFMCTest__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Salutation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'TCHSalutation__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Score de comportement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'BehaviouralScoring__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Secret bancaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'FinancialPrivacy__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Secteur d''activité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'ActivityBranch__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Secteur d''activité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Industry'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Secteur d''activité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'OccupationArea__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Segment Géolife', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'GeolifeSegment__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Segment Géolife', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'GeolifeSegment__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Segmentation distributive', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'SegmentationDistributive__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Segmentation épargne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'SegmentationSaving__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Service', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonDepartment'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Shipping Geocode Accuracy', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'ShippingGeocodeAccuracy'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Site Web', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Website'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Site de la personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Site'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Situation familiale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'MaritalStatus__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Source de la personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'AccountSource'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Sous-réseau', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'ShopNetworkMesh__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Sous-réseau', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'ShopNetworkMesh__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Souscripteur/Titulaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'SubscriberOwnerIndicator__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Statut KYC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'KYCStatus__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Supprimé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'IsDeleted'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Symbole', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'TickerSymbol'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Système', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'MobileOS__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TCHFirstEnrolement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'TCHFirstEnrolement__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TCHId', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'TCHId__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TCHId', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'TCHId__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TCHPartnerVisibility', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'TCHPartnerVisibility__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TCH_LinkSponsor', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'TCH_LinkSponsor__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TCH_N° Client Distributeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'IDCustomerCID__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TCH_Pré-attribution', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Preattribution__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TCH_Réseau distributeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'DistributorNetwork__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TchMigError', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'TchMigError__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tech Temp Habilitation IOBSP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'Tech_Temp_Habilitation_IOBSP__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Total charges', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'TotalExpenses__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Total revenus disponibles', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'TotalDisposableIncome__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Total revenus', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'TotalIncome__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type d''enregistrement de la personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'RecordTypeId'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type d''occupation du logement principal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PrimaryResidOccupationType__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type de contrat de travail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'EmploymentContractType__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tél. Favoris', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PreferredPhoneNumber__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tél. favoris', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PreferredPhoneNumber__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Téléphone professionnel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'BusinessPhone__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Téléphone professionnel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'BusinessPhone__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'URL de la photo', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PhotoUrl'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ville d''expédition', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'ShippingCity'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ville', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'BillingCity'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ville', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonMailingCity'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Éligibilité du mobile', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PhoneEligibility__pc'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de modification des données client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'LastClientDataUpdate__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TCH_IsDistributor', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'TCH_IsDistributor__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Score de confiance sur Identité digitale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'DigitalScore__pc';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Demande RGPD de suppression de données', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'PersonRGPD__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date d''entrée en relation avec OF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'OFFirstContactDate__pc';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Incident en Cours', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'IsOpenIncidentExisted__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant GRC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_ACCOUNT_ALL', @level2type = N'COLUMN', @level2name = N'IDGRC__c';
GO