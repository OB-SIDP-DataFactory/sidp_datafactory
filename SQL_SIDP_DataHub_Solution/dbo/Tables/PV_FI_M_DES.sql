﻿CREATE TABLE dbo.PV_FI_M_DES
(
	Id                 INT            IDENTITY(1,1) NOT NULL,
	COD_ENR            NCHAR(2)     NULL,
	NUMERO_ENREG       NVARCHAR(6)     NULL,
	COD_SEQ            NCHAR(2)     NULL,
	COD_BAN_GES        NVARCHAR(5)     NULL,
	COD_GUI_GES        NVARCHAR(5)     NULL,
	NUM_PRE            NVARCHAR(11)    NULL,
	COD_BAN_RIB        NVARCHAR(5)     NULL,
	COD_GUI_RIB        NVARCHAR(5)     NULL,
	NUM_CPT_RIB        NVARCHAR(11)    NULL,
	DTE_OUV_PRE        DATE           NULL,
	DTE_FER_PRE        DATE           NULL,
	DUR_INI_PRE        INT            NULL,
	TX_ACT_EFF_GAR     DECIMAL(7, 5)  NULL,
	DEV_PRE            NVARCHAR(3)     NULL,
	NB_DEC_DEV_PRE     INT            NULL,
	MTT_NOM            DECIMAL(18, 2) NULL,
	ENC_CAP            DECIMAL(18, 2) NULL,
	ENC_MOY            DECIMAL(18, 2) NULL,
	DTE_PRO_ECH        DATE           NULL,
	MTT_ECH_TRA        DECIMAL(18, 2) NULL,
	COD_SIT_DOS        NCHAR(2)     NULL,
	DTE_INC_PRE_IMP    DATE           NULL,
	MTT_IMP_FIN_MOI    DECIMAL(18, 2) NULL,
	MTT_FRA_DOS        DECIMAL(18, 3) NULL,
	MTT_COM            DECIMAL(18, 3) NULL,
	MTT_REM_SUP_MOI    DECIMAL(18, 2) NULL,
	ASS                NCHAR(1)     NULL,
	DATTTP             DATE           NULL,
	IDE_GRC            NVARCHAR(8)     NULL,
	NB_IMP             DECIMAL(7, 2)  NULL,
	MTT_GLO_IR_IMP     DECIMAL(18, 2) NULL,
	MTT_GLO_IL_IMP     DECIMAL(18, 2) NULL,
	PHA_PRO_REC        NCHAR(1)     NULL,
	FILLER             NVARCHAR(99)    NULL,
	CIV_CLIENT         NCHAR(2)     NULL,
	NOM_CLIENT         NVARCHAR(24)    NULL,
	PRENOM_CLIENT      NVARCHAR(16)    NULL,
	ADRESSE_EMPR       NVARCHAR(38)    NULL,
	COMPL_ADRESSE      NVARCHAR(38)    NULL,
	LIEU_EMP           NVARCHAR(38)    NULL,
	COD_POSTAL         NVARCHAR(5)     NULL,
	VILLE              NVARCHAR(32)    NULL,
	CO_EMPRUNTEUR      NCHAR(1)     NULL,
	PSA                NCHAR(1)     NULL,
	SIT_FAMILIALE      NCHAR(1)     NULL,
	SIT_LOGEMENT       NCHAR(1)     NULL,
	NB_ENFANTS         INT            NULL,
	CSP_EMPRUNTEUR     NVARCHAR(4)     NULL,
	DATE_NAISSANCE     DATE           NULL,
	REVENU_MENAGE      DECIMAL(18, 2) NULL,
	CAT_PRE            NVARCHAR(9)     NULL,
	COD_OBJ            NVARCHAR(5)     NULL,
	TAX_CLI_INITIAL    DECIMAL(4, 2)  NULL,
	TAX_VEN            DECIMAL(4, 2)  NULL,
	TAEG_CLIENT        DECIMAL(7, 5)  NULL,
	MTT_NOM_COURANT    DECIMAL(11, 2)  NULL,
	INT_INI            DECIMAL(11, 2)  NULL,
	ASS_INI            DECIMAL(11, 2)  NULL,
	DTE_PRE_ECH        DATE           NULL,
	DTE_FIN_INI        DATE           NULL,
	DTE_FIN_PRE        DATE           NULL,
	CPT_DER_ECH        DECIMAL(11, 2)  NULL,
	INT_DER_ECH        DECIMAL(11, 2)  NULL,
	ASS_DER_ECH        DECIMAL(11, 2)  NULL,
	ACC_DER_ECH        DECIMAL(11, 2)  NULL,
	CPT_PRO_ECH        DECIMAL(11, 2)  NULL,
	INT_PRO_ECH        DECIMAL(11, 2)  NULL,
	ASS_PRO_ECH        DECIMAL(11, 2)  NULL,
	ACC_PRO_ECH        DECIMAL(11, 2)	NULL,
	COD_SIT            NCHAR(1)			NULL,
	NAT_ASS_EMP        NCHAR(2)			NULL,
	NAT_ASS_CO_EMP     NCHAR(2)			NULL,
	RES_DIS            NVARCHAR(3)		NULL,
	SOUS_RES_DIS       NVARCHAR(8)		NULL,
	COD_PDT            NCHAR(2)			NULL,
	Validity_StartDate DATETIME2(7)		NOT NULL,
	Validity_EndDate   DATETIME2(7)		DEFAULT('99991231') NOT NULL,
	Startdt            DATETIME2(7)		GENERATED  ALWAYS  AS  ROW  START  NOT NULL,
	Enddt              DATETIME2(7)		GENERATED  ALWAYS  AS  ROW  END  NOT NULL,
	CONSTRAINT PK_PV_FI_M_DES PRIMARY KEY CLUSTERED(Id ASC),
	PERIOD FOR SYSTEM_TIME(Startdt, Enddt)
)
WITH(SYSTEM_VERSIONING = ON(HISTORY_TABLE=dbo.PV_FI_M_DESHistory, DATA_CONSISTENCY_CHECK=ON))
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code banque de gestion', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'COD_BAN_GES'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code banque du RIB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'COD_BAN_RIB'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'COD_ENR'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code guichet de gestion', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'COD_GUI_GES'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code guichet du RIB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'COD_GUI_RIB'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code sequence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'COD_SEQ'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code situation du dossier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'COD_SIT_DOS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Stade du Contrat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'PHA_PRO_REC'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de fermeture de la prestation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'DTE_FER_PRE'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de la prochaine échéance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'DTE_PRO_ECH'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de traitement de periode Annee Mois', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'DATTTP'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date d''ouverture du prêt', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'DTE_OUV_PRE'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date incident premier impayé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'DTE_INC_PRE_IMP'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Devise du prêt', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'DEV_PRE'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Durée initiale du prêt', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'DUR_INI_PRE'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Encours Capital', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'ENC_CAP'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Encours moyen', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'ENC_MOY'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Indicateur Assurance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'ASS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant de commission', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'MTT_COM'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant de l''échéance traitée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'MTT_ECH_TRA'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant des frais de dossier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'MTT_FRA_DOS'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant des impayés à fin de mois', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'MTT_IMP_FIN_MOI'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant des remboursements supplémentaires du mois', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'MTT_REM_SUP_MOI'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant du nominal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'MTT_NOM'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant global IL impayé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'MTT_GLO_IL_IMP'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant global IR impayé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'MTT_GLO_IR_IMP'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre de décimales de la devise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'NB_DEC_DEV_PRE'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre d''impayés', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'NB_IMP'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro de compte du RIB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'NUM_CPT_RIB'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro de prestation du contrat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'NUM_PRE'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numero enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'NUMERO_ENREG'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Référence SIEBEL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'IDE_GRC'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux actuariel Effective Garantie (TAEG)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_DES', @level2type = N'COLUMN', @level2name = N'TX_ACT_EFF_GAR'
GO