﻿CREATE TABLE [dbo].[PV_FE_ENROLMENTDOCUMENT_V2] (
    [Id]                          INT             IDENTITY (1, 1) NOT NULL,
    [ID_FE_ENROLMENTDOCUMENT]     DECIMAL (38)    NULL,
    [CREATION_DATE]               DATETIME2 (6)   NULL,
    [CREATION_USER]               NVARCHAR (255)  NULL,
    [LAST_UPDATE_DATE]            DATETIME2 (6)   NULL,
    [LAST_UPDATE_USER]            NVARCHAR (255)  NULL,
    [USER_TYPE]                   NVARCHAR (255)  NULL,
    [DOCUMENT]                    VARBINARY (MAX) NULL,
    [ENTITLEMENT_PROOF_FOLDER_ID] DECIMAL (38)    NULL,
    [ENROLMENT_DOCUMENT_TYPE_ID]  DECIMAL (38)    NULL,
    [MIME_TYPE]                   NVARCHAR (255)  NULL,
    [Validity_StartDate]          DATETIME2 (7)   NOT NULL,
    [Validity_EndDate]            DATETIME2 (7)   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_FE_ENROLMENTDOCUMENT_V2] PRIMARY KEY CLUSTERED ([Id] ASC)
);

