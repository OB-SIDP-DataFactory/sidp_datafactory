﻿CREATE TABLE [dbo].[WK_SF_OPPORTUNITYHistory] (
    [Id_SF_FldHistOpp]                   NVARCHAR (50)  NOT NULL,
    [Id_SF]                              NVARCHAR (50)  NOT NULL,
    [AccountId]                          NVARCHAR (18)  NULL,
    [AdvisorCode__c]                     NVARCHAR (80)  NULL,
    [CampaignId__c]                      NVARCHAR (80)  NULL,
    [CloseDate]                          DATETIME       NULL,
    [CommercialOfferCode__c]             NVARCHAR (80)  NULL,
    [CommercialOfferName__c]             NVARCHAR (80)  NULL,
    [CompleteFileFlag__c]                NVARCHAR (10)  NULL,
    [CreatedById]                        NVARCHAR (18)  NULL,
    [CreatedDate]                        DATETIME       NULL,
    [DeniedOpportunityReason__c]         NVARCHAR (255) NULL,
    [DistributionChannel__c]             NVARCHAR (255) NULL,
    [DistributorEntity__c]               NVARCHAR (80)  NULL,
    [DistributorNetwork__c]              NVARCHAR (80)  NULL,
    [FinalizedBy__c]                     NVARCHAR (18)  NULL,
    [FormValidation__c]                  DATETIME       NULL,
    [IDOppty__c]                         NVARCHAR (30)  NULL,
    [IDProcessSous__c]                   NVARCHAR (80)  NULL,
    [IdSource__c]                        NVARCHAR (80)  NULL,
    [Indication__c]                      NVARCHAR (80)  NULL,
    [LastModifiedDate]                   DATETIME       NULL,
    [LeadSource]                         NVARCHAR (80)  NULL,
    [Manager__c]                         NVARCHAR (18)  NULL,
    [NoIndication__c]                    NVARCHAR (80)  NULL,
    [OwnerId]                            NVARCHAR (18)  NULL,
    [PointOfSaleCode__c]                 NVARCHAR (80)  NULL,
    [ProductFamilyCode__c]               NVARCHAR (80)  NULL,
    [ProductFamilyName__c]               NVARCHAR (80)  NULL,
    [ProvenanceIndicationIndicatorId__c] NVARCHAR (80)  NULL,
    [RecordTypeId]                       NVARCHAR (18)  NULL,
    [RejectReason__c]                    NVARCHAR (255) NULL,
    [RelationEntryScore__c]              NVARCHAR (255) NULL,
    [StageName]                          NVARCHAR (40)  NULL,
    [StartedChannel__c]                  NVARCHAR (255) NULL,
    [SystemModstamp]                     DATETIME       NULL,
    [Type]                               NVARCHAR (40)  NULL,
    [IsWon]                              NVARCHAR (10)  NULL,
    [Validity_StartDate]                 DATETIME2 (7)  NOT NULL,
    [Validity_EndDate]                   DATETIME2 (7)  NOT NULL,
    CONSTRAINT [PK_WK_SF_OPPORTUNITYHistory] PRIMARY KEY CLUSTERED ([Id_SF_FldHistOpp] ASC)
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de début de validité fonctionnelle', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'Validity_StartDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de fin de validité fonctionnelle', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'Validity_EndDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type d''opportunité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'Type';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Horodateur des modifications du système', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'SystemModstamp';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Canal d''interaction d''origine', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'StartedChannel__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Étape', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'StageName';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Score d''entrée en relation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'RelationEntryScore__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motif sans suite', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'RejectReason__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID du type d''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'RecordTypeId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Indicateur indication digitale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'ProvenanceIndicationIndicatorId__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom famille produit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'ProductFamilyName__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code famille produit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'ProductFamilyCode__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Point de vente', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'PointOfSaleCode__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID du propriétaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'OwnerId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Id process indication', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'NoIndication__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Responsable d''entité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'Manager__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Canal d''interaction', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'LeadSource';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'LastModifiedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gagnée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'IsWon';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Indication', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'Indication__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Source', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'IdSource__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Id process souscription', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'IDProcessSous__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant de l''Opportunité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'IDOppty__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID d''historique d''opportunité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'Id_SF_FldHistOpp';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de l''opportunité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'Id_SF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Validation du formulaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'FormValidation__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Finalisé par', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'FinalizedBy__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Réseau Distributeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'DistributorNetwork__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Entité distributrice', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'DistributorEntity__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Canal de distribution', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'DistributionChannel__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motif Affaire refusée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'DeniedOpportunityReason__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'CreatedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID créé par', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'CreatedById';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dossier Complet', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'CompleteFileFlag__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom de l’offre commerciale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'CommercialOfferName__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code de l’offre commerciale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'CommercialOfferCode__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de clôture', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'CloseDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Campagne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'CampaignId__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Conseiller', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'AdvisorCode__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom de la personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_OPPORTUNITYHistory', @level2type = N'COLUMN', @level2name = N'AccountId';

