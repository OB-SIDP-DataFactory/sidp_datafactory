﻿CREATE TABLE [dbo].[IWD_GIDB_G_AGENT_STATE_HISTORY_V] (
    [TYPE]                INT           NOT NULL,
    [ID]                  NUMERIC (16)  NOT NULL,
    [STATE]               INT           NOT NULL,
    [PENDINGSTATE]        INT           NULL,
    [SEQ]                 INT           NOT NULL,
    [LSEQ]                INT           NULL,
    [PSEQ]                INT           NULL,
    [PREVSTATE]           INT           NULL,
    [PREVSENTER]          DATETIME      NULL,
    [PREVSENTER_TS]       INT           NULL,
    [CAUSE]               INT           NULL,
    [AGENTSTATECONDITION] INT           NULL,
    [WORKMODE]            INT           NULL,
    [REASONCODE]          VARCHAR (255) NULL,
    [SYSREASON]           INT           NULL,
    [AGENTID]             INT           NULL,
    [LOGINID]             INT           NULL,
    [ENDPOINTID]          INT           NULL,
    [QUEUEID]             INT           NULL,
    [PLACEID]             INT           NULL,
    [LOGINSESSIONID]      VARCHAR (50)  NULL,
    [PARTYID]             VARCHAR (50)  NULL,
    [ADDED]               DATETIME      NOT NULL,
    [ADDED_TS]            INT           NULL,
    [GSYS_DOMAIN]         INT           NULL,
    [GSYS_SYS_ID]         INT           NULL,
    [GSYS_EXT_VCH1]       VARCHAR (255) NULL,
    [GSYS_EXT_VCH2]       VARCHAR (255) NULL,
    [GSYS_EXT_INT1]       INT           NULL,
    [GSYS_EXT_INT2]       INT           NULL,
    [CREATE_AUDIT_KEY]    NUMERIC (19)  NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [IWD_I_G_ASH_V_ADDTS]
    ON [dbo].[IWD_GIDB_G_AGENT_STATE_HISTORY_V]([ADDED_TS] ASC);

