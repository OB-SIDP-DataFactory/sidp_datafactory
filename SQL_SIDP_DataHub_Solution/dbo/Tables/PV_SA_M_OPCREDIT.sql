CREATE TABLE [dbo].[PV_SA_M_OPCREDIT] (
    [Id]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [DWHOPEDTX]          DATE                                        NULL,
    [DWHOPEETA]          INT                                         NULL,
    [DWHOPEAGE]          INT                                         NULL,
    [DWHOPESER]          VARCHAR (2)                                 NULL,
    [DWHOPESSE]          VARCHAR (2)                                 NULL,
    [DWHOPEOPR]          VARCHAR (6)                                 NULL,
    [DWHOPENAT]          VARCHAR (6)                                 NULL,
    [DWHOPENDO]          INT                                         NULL,
    [DWHOPESEQ]          INT                                         NULL,
    [DWHOPECON]          VARCHAR (7)                                 NULL,
    [DWHOPEPAS]          VARCHAR (1)                                 NULL,
    [DWHOPEBDF]          VARCHAR (3)                                 NULL,
    [DWHOPECRE]          DATE                                        NULL,
    [DWHOPEENG]          DATE                                        NULL,
    [DWHOPEDIS]          DATE                                        NULL,
    [DWHOPEFIN]          DATE                                        NULL,
    [DWHOPEDEV]          VARCHAR (3)                                 NULL,
    [DWHOPEMON]          DECIMAL (18, 3)                             NULL,
    [DWHOPEVAL]          DECIMAL (18, 3)                             NULL,
    [DWHOPECOE]          VARCHAR (3)                                 NULL,
    [DWHOPETYA]          VARCHAR (1)                                 NULL,
    [DWHOPECDA]          VARCHAR (4)                                 NULL,
    [DWHOPENOA]          VARCHAR (6)                                 NULL,
    [DWHOPEDEA]          VARCHAR (3)                                 NULL,
    [DWHOPEAUT]          VARCHAR (20)                                NULL,
    [DWHOPERUB]          VARCHAR (10)                                NULL,
    [DWHOPEDUR6]         INT                                         NULL,
    [DWHOPERES6]         INT                                         NULL,
    [DWHOPETYP]          VARCHAR (2)                                 NULL,
    [DWHOPEFIX]          DECIMAL (14, 9)                             NULL,
    [DWHOPENOUV]         VARCHAR (1)                                 NULL,
    [DWHOPEMOIN]         DECIMAL (18, 3)                             NULL,
    [DWHOPECMOI]         DECIMAL (18, 3)                             NULL,
    [DWHOPEMIN]          DECIMAL (18, 3)                             NULL,
    [DWHOPEVIN]          DECIMAL (18, 3)                             NULL,
    [DWHOPEMFI]          DECIMAL (18, 3)                             NULL,
    [DWHOPECFI]          DECIMAL (18, 3)                             NULL,
    [DWHOPEEMM]          DECIMAL (18, 3)                             NULL,
    [DWHOPECMM]          DECIMAL (18, 3)                             NULL,
    [DWHOPETYC]          VARCHAR (1)                                 NULL,
    [DWHOPEOFI]          VARCHAR (6)                                 NULL,
    [DWHOPEREC]          VARCHAR (1)                                 NULL,
    [DWHOPEPOU]          VARCHAR (1)                                 NULL,
    [DWHOPEPOL]          VARCHAR (1)                                 NULL,
    [DWHOPEPTR]          DECIMAL (14, 9)                             NULL,
    [DWHOPEPRI]          DECIMAL (14, 9)                             NULL,
    [DWHOPEMTT]          DECIMAL (18, 3)                             NULL,
    [DWHOPEMTR]          DECIMAL (18, 3)                             NULL,
    [DWHOPECAR]          DECIMAL (18, 3)                             NULL,
    [DWHOPECFIM]         DECIMAL (18, 3)                             NULL,
    [DWHOPETYT]          VARCHAR (1)                                 NULL,
    [DWHOPETAU]          DECIMAL (15, 9)                             NULL,
    [DWHOPEBAS]          VARCHAR (2)                                 NULL,
    [DWHOPEREF]          VARCHAR (6)                                 NULL,
    [DWHOPEMAR]          DECIMAL (15, 9)                             NULL,
    [DWHOPETEG]          DECIMAL (15, 9)                             NULL,
    [DWHOPETESE]         DECIMAL (15, 9)                             NULL,
    [DWHOPETESL]         DECIMAL (15, 9)                             NULL,
    [DWHOPEPER]          VARCHAR (1)                                 NULL,
    [DWHOPETAM]          VARCHAR (1)                                 NULL,
    [DWHOPEDIA]          VARCHAR (1)                                 NULL,
    [DWHOPEDIT]          VARCHAR (1)                                 NULL,
    [DWHOPECPD]          VARCHAR (1)                                 NULL,
    [DWHOPEDPE]          DATE                                        NULL,
    [DWHOPEDRE]          DATE                                        NULL,
    [DWHOPENDF]          INT                                         NULL,
    [DWHOPEMAD]          DATE                                        NULL,
    [DWHOPEMTG]          DECIMAL (18, 3)                             NULL,
    [DWHOPEDMD]          DATE                                        NULL,
    [DWHOPEMTD]          DECIMAL (18, 3)                             NULL,
    [DWHOPECAT]          DECIMAL (18, 3)                             NULL,
    [DWHOPEMTI]          DECIMAL (18, 3)                             NULL,
    [DWHOPECMI]          DECIMAL (18, 3)                             NULL,
    [DWHOPEPRE]          VARCHAR (1)                                 NULL,
    [DWHOPENPRE]         INT                                         NULL,
    [DWHOPETNM]          DECIMAL (15, 9)                             NULL,
    [DWHOPEMNP]          DECIMAL (15, 9)                             NULL,
    [DWHOPETMM]          DECIMAL (15, 9)                             NULL,
    [DWHOPEDRT]          DATE                                        NULL,
    [DWHOPEPRT]          DATE                                        NULL,
    [DWHOPENJR]          INT                                         NULL,
    [DWHOPENJB]          INT                                         NULL,
    [DWHOPEPOS]          VARCHAR (1)                                 NULL,
    [DWHOPENAN]          VARCHAR (1)                                 NULL,
    [DWHOPEDEI]          DATE                                        NULL,
    [DWHOPEDPEI]         DATE                                        NULL,
    [DWHOPENBI]          INT                                         NULL,
    [DWHOPENBE]          INT                                         NULL,
    [DWHOPETOI]          DECIMAL (18, 3)                             NULL,
    [DWHOPETOB]          DECIMAL (18, 3)                             NULL,
    [DWHOPECAI]          DECIMAL (18, 3)                             NULL,
    [DWHOPEINI]          DECIMAL (18, 3)                             NULL,
    [DWHOPETAX]          DECIMAL (18, 3)                             NULL,
    [DWHOPEASI]          DECIMAL (18, 3)                             NULL,
    [DWHOPECOI]          DECIMAL (18, 3)                             NULL,
    [DWHOPEFRA]          DECIMAL (18, 3)                             NULL,
    [DWHOPEIND]          DECIMAL (18, 3)                             NULL,
    [DWHOPEDDT]          DATE                                        NULL,
    [DWHOPEMDT]          DECIMAL (18, 3)                             NULL,
    [DWHOPETIR]          VARCHAR (8)                                 NULL,
    [DWHOPEMNO]          DECIMAL (18, 3)                             NULL,
    [DWHOPEMSD]          DECIMAL (18, 3)                             NULL,
    [DWHOPEMCO]          DECIMAL (18, 3)                             NULL,
    [DWHOPELSD]          DECIMAL (18, 3)                             NULL,
    [DWHOPEDE2]          VARCHAR (3)                                 NULL,
    [DWHOPEMO2]          DECIMAL (18, 3)                             NULL,
    [DWHOPEVA2]          DECIMAL (18, 3)                             NULL,
    [DWHOPESEN]          VARCHAR (1)                                 NULL,
    [DWHOPETPA]          VARCHAR (3)                                 NULL,
    [DWHOPENCR]          VARCHAR (5)                                 NULL,
    [DWHOPENCO]          VARCHAR (5)                                 NULL,
    [DWHOPENRE]          VARCHAR (7)                                 NULL,
    [DWHOPENEF]          VARCHAR (3)                                 NULL,
    [DWHOPEICO]          VARCHAR (1)                                 NULL,
    [DWHOPENBJ]          INT                                         NULL,
    [DWHOPESUR]          VARCHAR (1)                                 NULL,
    [DWHOPEAJU]          VARCHAR (1)                                 NULL,
    [DWHOPECAP]          DECIMAL (14, 9)                             NULL,
    [DWHOPERES]          VARCHAR (1)                                 NULL,
    [DWHOPEECH]          VARCHAR (1)                                 NULL,
    [DWHOPETXC]          DECIMAL (14, 9)                             NULL,
    [DWHOPEUSP]          VARCHAR (1)                                 NULL,
    [DWHOPEMTE]          DECIMAL (18, 3)                             NULL,
    [DWHOPERAC]          DECIMAL (18, 3)                             NULL,
    [DWHOPEPTF]          VARCHAR (6)                                 NULL,
    [DWHOPETPL]          VARCHAR (15)                                NULL,
    [DWHOPERCS]          VARCHAR (7)                                 NULL,
    [DWHOPECRI]          DECIMAL (18, 3)                             NULL,
    [DWHOPERIS]          VARCHAR (1)                                 NULL,
    [DWHOPEFRE]          VARCHAR (1)                                 NULL,
    [DWHOPEDOM]          VARCHAR (12)                                NULL,
    [DWHOPEAPP]          VARCHAR (3)                                 NULL,
    [DWHOPEDSY]          DATE                                        NULL,
    [Validity_StartDate] DATETIME                                    NULL,
    [Validity_EndDate]   DATETIME                                    DEFAULT ('99991231') NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SA_M_OPCREDIT] PRIMARY KEY CLUSTERED ([Id] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SA_M_OPCREDITHistory], DATA_CONSISTENCY_CHECK=ON));
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ETABLISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEETA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AGENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEAGE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SERVICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPESER';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SOUS-SERVICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPESSE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE OPERATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEOPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE NATURE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPENAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMERO DOSSIER', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPENDO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SEQUENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPESEQ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CONTREPARTIE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPECON';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CLIEN PASSAGE/TIERS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEPAS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE B.D.F', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEBDF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE CREATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPECRE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ENGAGEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEENG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE MISE A DISPOS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEDIS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DE FIN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEFIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEVISE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEDEV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEMON';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CONTREVALEUR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEVAL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE ETAT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPECOE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE AUTORISATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPETYA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE AUTORISATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPECDA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N°  AUTORISATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPENOA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEVISE AUTORISATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEDEA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COMPTE AUTORISATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEAUT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RUBRIQUE DU COMPTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPERUB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'  EN JOURS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEDUR6';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'  EN JOURS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPERES6';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE D''OPÉRATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPETYP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FINXING DATE ARRÊTÉ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEFIX';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NOUVELLE OPERATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPENOUV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT INITIAL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEMOIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CV MONTANT INITIAL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPECMOI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT INTÉRÊTS COURUS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEMIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CTVL INT.COURUS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEVIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COURUS DU MOIS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEMFI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CTVL COURUS MOIS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPECFI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ENCOURS MOYEN.MENS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEEMM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CTVL ENCOURS MOYEN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPECMM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE DU DOSSIER', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPETYC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'OBJET DE FINANCEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEOFI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE RECONDUCTION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEREC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CRÉDIT EN POOL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEPOU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE POOL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEPOL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'% TRESORERIE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEPTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'% RISQUE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEPRI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONT N/PART DEVISE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEMTT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONT N/PART RISQUE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEMTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CAP RESTANT EN RISQU', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPECAR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONT FLUX INTERÊTS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPECFIM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE DE TAUX', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPETYT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TAUX APPLIQUÉ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPETAU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'BASE DE CALCUL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEBAS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'REFERENCE TAUX', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEREF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MARGE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEMAR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TEG', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPETEG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TESE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPETESE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TESL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPETESL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PERIODICITE CAPITAL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEPER';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MODE REMBOURSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPETAM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DIFFERE AMORTISS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEDIA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DIFFERE TOTAL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEDIT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE PERIODE DIFFERE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPECPD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE 1ÈRE ÉCHÉANCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEDPE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE PROCHA ÉCHÉANCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEDRE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NOMBRE DE DIFFÉRÉ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPENDF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE 1ÈRE MAD CREDIT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEMAD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT GLOBAL MAD', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEMTG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE MAD DS LE MOIS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEDMD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT MAD DS MOIS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEMTD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CAP RESTANT EN TRESO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPECAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONT INT EN DEVISE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEMTI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CTVL DU MNT INTÉRÊT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPECMI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PÉRIODE DE PRÉAVIS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEPRE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NBR PÉRIODE PRÉAVIS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPENPRE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TAUX NORMAL MOYEN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPETNM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MARGE MOYENNE PONDER', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEMNP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TAUX OU MARGE MINI', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPETMM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DERN REVISION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEDRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE PROCHA REVISION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEPRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NBR JOUR/DATE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPENJR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NBR JOURB/DATE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPENJB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYP PERCEPT° INTÉRÊT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEPOS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE NANTISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPENAN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ENTRÉE IMPAYÉES', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEDEI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE 1ÈRE ÉCH IMPAY', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEDPEI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NBR JOURS IMPAYÉS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPENBI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NB ECHEANCES IMPAYÉS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPENBE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MON IMPA DEVISE PRÊT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPETOI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MON IMPA DEVISE BASE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPETOB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MON CAP IMP DEV PRÊT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPECAI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MON INT IMP DEV PRÊT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEINI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT TAXE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPETAX';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT ASSURANCES', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEASI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT COMMISSIONS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPECOI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT FRAIS IMPAY', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEFRA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT INDEMNITÉS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEIND';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ÉCHÉANCE TERME', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEDDT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT ÉCHÉAN TERME', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEMDT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° DU TITRE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPETIR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT NOMINAL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEMNO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT SURCOT/DECOT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEMSD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONT COUPONS COURUS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEMCO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LISSAGE SURCOT/DECOT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPELSD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEV JAMBE 2 OPERAT°', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEDE2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT JAMBE 2 OPERAT°', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEMO2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CONTRE VAL DWHOPEMO2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEVA2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SENS DE L OPERATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPESEN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE DE PAPIER', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPETPA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMÉRO CRÉDIT SAMIC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPENCR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMÉRO CONTRAT SAMIC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPENCO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMÉRO REMISE SAMIC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPENRE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMÉRO EFFET  SAMIC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPENEF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IND.PRÊT CONSORTIAU', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEICO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NB.JOURS AV RENV TX', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPENBJ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SÛRETÉ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPESUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MODE AJUSTEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEAJU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TX INTÉRÊT MAX', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPECAP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'OP°NOU/REN CRÉ RESTR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPERES';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ÉCHÉANCE CONSTANTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEECH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TX COMMISSION TYP TX', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPETXC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'USAGE DU PRÊT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEUSP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT GLOBAL ÉCHÉANCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEMTE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'REVENU ANNUEL CO-EMP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPERAC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RACINE PORTEFEUILLE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEPTF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PÉRIMÈTRE RISQUE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPERIS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FRÉQUENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEFRE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DOMAINE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEDOM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'APPLICATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEAPP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE SYSTÈME', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPEDSY';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TAUX PLANCHER', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPETPL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Référ contra synd', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPERCS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mtt cuml remb  impy', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPCREDIT', @level2type = N'COLUMN', @level2name = N'DWHOPECRI';

