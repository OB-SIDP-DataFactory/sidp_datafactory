﻿CREATE TABLE [dbo].[OAV_OBJETS_FINANCEMENTS] (
    [ID_OBJETS_FINANCEMENTS] DECIMAL (10)   NOT NULL,
    [PRODUIT_ID]             DECIMAL (10)   NULL,
    [LABEL]                  NVARCHAR (255) NOT NULL,
    [DEACTIVATED]            DECIMAL (1)    NOT NULL,
    [FEES]                   FLOAT (53)     NULL,
    [REMISE]                 FLOAT (53)     NULL,
    [MODE_EMPLOI]            FLOAT (53)     NULL,
    [GRILLE_NAF]             FLOAT (53)     NULL,
    [CREATED_AT]             DATETIME2 (7)  NULL,
    [NEXT_ACCOUNT_NUMBER]    DECIMAL (10)   NULL,
    [DAT_OBSR]               DATE           NOT NULL,
    [DAT_CHRG]               DATETIME       NOT NULL,
    CONSTRAINT [PK_PrimaryKeyOBJETSFINANCEMENTS] PRIMARY KEY CLUSTERED ([ID_OBJETS_FINANCEMENTS] ASC)
);

