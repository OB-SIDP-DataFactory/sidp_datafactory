﻿CREATE TABLE [dbo].[WK_SA_M_IDVM] (
    [DWHIDVIDV] INT             NULL,
    [DWHIDVRVM] INT             NULL,
    [DWHIDVPCT] INT             NULL,
    [DWHIDVISI] VARCHAR (12)    NULL,
    [DWHIDVTCN] VARCHAR (23)    NULL,
    [DWHIDVDEC] DATE            NULL,
    [DWHIDVPEX] DECIMAL (18, 9) NULL,
    [DWHIDVDCR] DATE            NULL
);

