﻿CREATE TABLE [dbo].[IWD_GIDB_G_VIRTUAL_QUEUE_MM] (
    [VQ_KEY]           NUMERIC (19)  IDENTITY (4, 4) NOT NULL,
    [ID]               NUMERIC (16)  NOT NULL,
    [VQID]             VARCHAR (50)  NOT NULL,
    [VQDN]             VARCHAR (255) NOT NULL,
    [VQDNID]           INT           NOT NULL,
    [VQSWITCHID]       INT           NOT NULL,
    [ORIGCALLID]       VARCHAR (50)  NOT NULL,
    [ORIGSWITCHID]     INT           NULL,
    [ORIGDNTYPE]       INT           NULL,
    [ORIGDN]           VARCHAR (255) NULL,
    [ORIGDNID]         INT           NULL,
    [ORIGOBJTYPE]      INT           NULL,
    [TARGETCALLID]     VARCHAR (50)  NULL,
    [TARGETSWITCHID]   INT           NULL,
    [TARGETDNTYPE]     INT           NULL,
    [TARGETDN]         VARCHAR (255) NULL,
    [TARGETDNID]       INT           NULL,
    [TARGETOBJTYPE]    INT           NULL,
    [DISTCALLID]       VARCHAR (50)  NULL,
    [DISTSWITCHID]     INT           NULL,
    [DISTDNTYPE]       INT           NULL,
    [DISTDN]           VARCHAR (255) NULL,
    [DISTDNID]         INT           NULL,
    [DISTOBJTYPE]      INT           NULL,
    [VQEXTVCH1]        VARCHAR (50)  NULL,
    [VQEXTVCH2]        VARCHAR (50)  NULL,
    [STATUS]           INT           NOT NULL,
    [CAUSE]            INT           NOT NULL,
    [CREATED]          DATETIME      NOT NULL,
    [CREATED_TS]       INT           NOT NULL,
    [TERMINATED]       DATETIME      NULL,
    [TERMINATED_TS]    INT           NULL,
    [ADDED_TS]         INT           NOT NULL,
    [GSYS_DOMAIN]      INT           NULL,
    [GSYS_SYS_ID]      INT           NULL,
    [GSYS_EXT_VCH1]    VARCHAR (255) NULL,
    [GSYS_EXT_VCH2]    VARCHAR (255) NULL,
    [GSYS_EXT_INT1]    INT           NULL,
    [GSYS_EXT_INT2]    INT           NULL,
    [CREATE_AUDIT_KEY] NUMERIC (19)  NOT NULL
);
GO
CREATE NONCLUSTERED INDEX [IWD_I_G_G_VQ_MM_OCID]
    ON [dbo].[IWD_GIDB_G_VIRTUAL_QUEUE_MM]([ORIGCALLID] ASC);
GO
CREATE NONCLUSTERED INDEX [IWD_I_G_G_VQ_MM_ATS]
    ON [dbo].[IWD_GIDB_G_VIRTUAL_QUEUE_MM]([ADDED_TS] ASC);

