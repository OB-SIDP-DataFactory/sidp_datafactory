﻿CREATE TABLE [dbo].[OAV_TAUX_MONTANTS] (
    [ID_TAUX_MONTANTS] DECIMAL (10) NOT NULL,
    [MIN_VALUE]        DECIMAL (10) NOT NULL,
    [MAX_VALUE]        DECIMAL (10) NULL,
    [DAT_OBSR]         DATE         CONSTRAINT [DefValStartDatOAV_TAUX_MONTANTS] DEFAULT (getdate()) NOT NULL,
    [DAT_CHRG]         DATETIME     CONSTRAINT [DefValEndDatOAV_TAUX_MONTANTS] DEFAULT (CONVERT([date],'9999-12-31')) NOT NULL,
    CONSTRAINT [PK_PrimaryKeyTAUX_MONTANTS] PRIMARY KEY CLUSTERED ([ID_TAUX_MONTANTS] ASC)
);


