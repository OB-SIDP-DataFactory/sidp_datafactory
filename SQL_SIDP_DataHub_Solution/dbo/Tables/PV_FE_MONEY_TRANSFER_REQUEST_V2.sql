﻿CREATE TABLE [dbo].[PV_FE_MONEY_TRANSFER_REQUEST_V2] (
    [ID]                            INT             IDENTITY (1, 1) NOT NULL,
    [ID_FE_MONEY_TRANSFERT_REQUEST] NUMERIC (38)    NOT NULL,
    [ALIAS]                         NVARCHAR (255)  NULL,
    [AMOUNT]                        NUMERIC (19, 2) NULL,
    [AUTHORIZATION_NUMBER]          NVARCHAR (255)  NULL,
    [CREATION_DATE]                 DATETIME2 (7)   NULL,
    [CREATION_USER]                 NVARCHAR (255)  NULL,
    [LAST_UPDATE_DATE]              DATETIME2 (7)   NULL,
    [LAST_UPDATE_USER]              NVARCHAR (255)  NULL,
    [MOTIVE]                        NVARCHAR (255)  NULL,
    [PAYER_CUSTOMER_ID]             NUMERIC (38)    NULL,
    [PAYER_MOBILE_PHONE]            NVARCHAR (255)  NULL,
    [PAYMENT_DATE]                  DATETIME2 (7)   NULL,
    [PAYMENT_NUMBER]                NVARCHAR (255)  NULL,
    [RECEIVER_ACCOUNT_EQUIPMENT_ID] NUMERIC (38)    NULL,
    [RELAUNCH_DATE]                 DATETIME2 (7)   NULL,
    [STATUS_ID]                     NUMERIC (38)    NULL,
    [STATUS_LAST_UPDATE_DATE]       DATETIME2 (7)   NULL,
    [TRANSFER_ID]                   NUMERIC (38)    NULL,
    [USER_TYPE]                     NVARCHAR (255)  NULL,
    [Validity_StartDate]            DATETIME2 (7)   NOT NULL,
    [Validity_EndDate]              DATETIME2 (7)   NOT NULL,
    CONSTRAINT [PK_MONEY_TRANSFER_REQUEST] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [UC_ID_FE_MONEY_TRANSFER_REQUEST] UNIQUE NONCLUSTERED ([ID_FE_MONEY_TRANSFERT_REQUEST] ASC, [Validity_StartDate] ASC)
);



