﻿CREATE TABLE [dbo].[IWD_GIDB_G_IR_MM] (
    [IR_KEY]           NUMERIC (19)  IDENTITY (2, 2) NOT NULL,
    [ID]               NUMERIC (16)  NOT NULL,
    [IRID]             VARCHAR (50)  NOT NULL,
    [STATE]            INT           NOT NULL,
    [PARENTIRID]       VARCHAR (50)  NULL,
    [PARENTLINKTYPE]   INT           NULL,
    [ROOTCALLID]       VARCHAR (50)  NOT NULL,
    [ROOTIRID]         VARCHAR (50)  NULL,
    [MERGESTATE]       INT           NULL,
    [CREATED]          DATETIME      NOT NULL,
    [CREATED_TS]       INT           NULL,
    [TERMINATED_TS]    INT           NULL,
    [TERMINATED]       DATETIME      NULL,
    [ADDED_TS]         INT           NOT NULL,
    [GSYS_DOMAIN]      INT           NULL,
    [GSYS_SYS_ID]      INT           NULL,
    [GSYS_EXT_VCH1]    VARCHAR (255) NULL,
    [GSYS_EXT_VCH2]    VARCHAR (255) NULL,
    [GSYS_EXT_INT1]    INT           NULL,
    [GSYS_EXT_INT2]    INT           NULL,
    [CREATE_AUDIT_KEY] NUMERIC (19)  NOT NULL
);
GO
CREATE NONCLUSTERED INDEX [IWD_I_G_IR_MM_PIRID]
    ON [dbo].[IWD_GIDB_G_IR_MM]([PARENTIRID] ASC);
GO
CREATE NONCLUSTERED INDEX [IWD_I_G_IR_MM_IRID]
    ON [dbo].[IWD_GIDB_G_IR_MM]([IRID] ASC);
GO
CREATE NONCLUSTERED INDEX [IWD_I_G_IR_MM_ATS]
    ON [dbo].[IWD_GIDB_G_IR_MM]([ADDED_TS] ASC);

