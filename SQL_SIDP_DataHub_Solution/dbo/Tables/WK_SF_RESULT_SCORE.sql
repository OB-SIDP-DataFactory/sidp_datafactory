﻿CREATE TABLE [dbo].[WK_SF_RESULT_SCORE] (
    [DAT_VALD]           DATE            NOT NULL,
    [Num]                INT             NULL,
    [IDCustomer__pc]     NVARCHAR (11)   NULL,
    [Id_SF]              NVARCHAR (18)   NOT NULL,
    [DistributorData__c] NVARCHAR (MAX)  NULL,
    [FLG_ENRG_COUR]      BIT             NULL,
    [value]              NVARCHAR (1000) NULL,
    [nom]                NVARCHAR (50)   NULL,
    [lib_nom]            NVARCHAR (50)   NULL,
    [code_nom]           NVARCHAR (2)    NULL,
    [principal]          NVARCHAR (50)   NULL,
    [score]              NVARCHAR (6)    NULL
)
GO
CREATE NONCLUSTERED INDEX [IX_NC_WK_SF_RST_SCR_PRN]
ON [dbo].[WK_SF_RESULT_SCORE] ([principal])
INCLUDE ([DAT_VALD],[IDCustomer__pc],[Id_SF],[FLG_ENRG_COUR],[lib_nom],[code_nom],[score])
GO