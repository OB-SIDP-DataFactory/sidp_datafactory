﻿CREATE TABLE [dbo].[PV_SA_M_FUSIONCPTOPE] (
    [ID]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [DWHC01DTX]          DATE                                        NULL,
    [DWHC01ETA]          INT                                         NULL,
    [DWHC01AGE]          INT                                         NULL,
    [DWHC01SER]          VARCHAR (2)                                 NULL,
    [DWHC01SSE]          VARCHAR (2)                                 NULL,
    [DWHC01HI1]          VARCHAR (6)                                 NULL,
    [DWHC01HI2]          VARCHAR (10)                                NULL,
    [DWHC01HI3]          VARCHAR (20)                                NULL,
    [DWHC01CLI]          VARCHAR (7)                                 NULL,
    [DWHC01DEV]          VARCHAR (3)                                 NULL,
    [DWHC01CPT]          DECIMAL (18, 3)                             NULL,
    [DWHC01VAL]          DECIMAL (18, 3)                             NULL,
    [DWHC01OPR]          DECIMAL (18, 3)                             NULL,
    [DWHC01RES]          INT                                         NULL,
    [DWHC01DUR]          INT                                         NULL,
    [DWHC01MOD]          DECIMAL (18, 3)                             NULL,
    [DWHC01MOC]          DECIMAL (18, 3)                             NULL,
    [DWHC01MVD]          DECIMAL (18, 3)                             NULL,
    [DWHC01MVC]          DECIMAL (18, 3)                             NULL,
    [DWHC01SMD]          DECIMAL (18, 3)                             NULL,
    [DWHC01SMC]          DECIMAL (18, 3)                             NULL,
    [DWHC01CLO]          DATE                                        NULL,
    [DWHC01OUV]          DATE                                        NULL,
    [DWHC01ENG]          DATE                                        NULL,
    [DWHC01AUT]          DECIMAL (18, 3)                             NULL,
    [DWHC01SOU]          VARCHAR (3)                                 NULL,
    [DWHC01MIN]          DECIMAL (18, 3)                             NULL,
    [DWHC01VIN]          DECIMAL (18, 3)                             NULL,
    [DWHC01MFI]          DECIMAL (18, 3)                             NULL,
    [DWHC01CFI]          DECIMAL (18, 3)                             NULL,
    [DWHC01DSY]          DATE                                        NULL,
    [Validity_StartDate] DATETIME                                    NULL,
    [Validity_EndDate]   DATETIME                                    DEFAULT ('99991231') NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SA_M_FUSIONCPTOPE] PRIMARY KEY CLUSTERED ([ID] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SA_M_FUSIONCPTOPEHistory], DATA_CONSISTENCY_CHECK=ON));
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE SYSTEME', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_FUSIONCPTOPE', @level2type = N'COLUMN', @level2name = N'DWHC01DSY';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CTVL COURUS DU MOIS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_FUSIONCPTOPE', @level2type = N'COLUMN', @level2name = N'DWHC01CFI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COURUS DU MOIS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_FUSIONCPTOPE', @level2type = N'COLUMN', @level2name = N'DWHC01MFI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CTVL. INT. COURUS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_FUSIONCPTOPE', @level2type = N'COLUMN', @level2name = N'DWHC01VIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MNT.INTERETS COURUS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_FUSIONCPTOPE', @level2type = N'COLUMN', @level2name = N'DWHC01MIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SOURCE DE LA DONNEE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_FUSIONCPTOPE', @level2type = N'COLUMN', @level2name = N'DWHC01SOU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT AUTORISE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_FUSIONCPTOPE', @level2type = N'COLUMN', @level2name = N'DWHC01AUT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ENGAGEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_FUSIONCPTOPE', @level2type = N'COLUMN', @level2name = N'DWHC01ENG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE OUVERTURE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_FUSIONCPTOPE', @level2type = N'COLUMN', @level2name = N'DWHC01OUV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE CLOTURE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_FUSIONCPTOPE', @level2type = N'COLUMN', @level2name = N'DWHC01CLO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SLD CRD.VAL/DEV BASE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_FUSIONCPTOPE', @level2type = N'COLUMN', @level2name = N'DWHC01SMC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SLD DEB.VAL/DEV BASE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_FUSIONCPTOPE', @level2type = N'COLUMN', @level2name = N'DWHC01SMD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SLD MOYEN CREDI.VAL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_FUSIONCPTOPE', @level2type = N'COLUMN', @level2name = N'DWHC01MVC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SLD MOYEN DEBIT.VAL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_FUSIONCPTOPE', @level2type = N'COLUMN', @level2name = N'DWHC01MVD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SLD MOYEN CREDI.CPT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_FUSIONCPTOPE', @level2type = N'COLUMN', @level2name = N'DWHC01MOC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SLD MOYEN DEBIT.CPT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_FUSIONCPTOPE', @level2type = N'COLUMN', @level2name = N'DWHC01MOD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DUREE INITIALE/JOURS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_FUSIONCPTOPE', @level2type = N'COLUMN', @level2name = N'DWHC01DUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DUREE RESTANT/JOURS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_FUSIONCPTOPE', @level2type = N'COLUMN', @level2name = N'DWHC01RES';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SLD CPTBL/DEV CPT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_FUSIONCPTOPE', @level2type = N'COLUMN', @level2name = N'DWHC01OPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SLD VALEUR/DEV BASE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_FUSIONCPTOPE', @level2type = N'COLUMN', @level2name = N'DWHC01VAL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SLD CPTBL/DEV BASE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_FUSIONCPTOPE', @level2type = N'COLUMN', @level2name = N'DWHC01CPT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEVISE OPERAT°', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_FUSIONCPTOPE', @level2type = N'COLUMN', @level2name = N'DWHC01DEV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMÉRO CLIENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_FUSIONCPTOPE', @level2type = N'COLUMN', @level2name = N'DWHC01CLI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMÉRO OPÉRATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_FUSIONCPTOPE', @level2type = N'COLUMN', @level2name = N'DWHC01HI3';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE NATURE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_FUSIONCPTOPE', @level2type = N'COLUMN', @level2name = N'DWHC01HI2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE OPERATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_FUSIONCPTOPE', @level2type = N'COLUMN', @level2name = N'DWHC01HI1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SOUS-SERVICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_FUSIONCPTOPE', @level2type = N'COLUMN', @level2name = N'DWHC01SSE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SERVICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_FUSIONCPTOPE', @level2type = N'COLUMN', @level2name = N'DWHC01SER';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AGENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_FUSIONCPTOPE', @level2type = N'COLUMN', @level2name = N'DWHC01AGE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ETABLISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_FUSIONCPTOPE', @level2type = N'COLUMN', @level2name = N'DWHC01ETA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE EXTRACTION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_FUSIONCPTOPE', @level2type = N'COLUMN', @level2name = N'DWHC01DTX';

