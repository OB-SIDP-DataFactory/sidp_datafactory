﻿CREATE TABLE [dbo].[secfi_ref_ope] (
    [famille_operation] VARCHAR (50) NULL,
    [instrument]        VARCHAR (50) NULL,
    [code_transaction]  VARCHAR (50) NULL,
    [perimetre]         VARCHAR (50) NULL,
    [cod_ope]           VARCHAR (50) NULL,
    [cod_eve]           VARCHAR (50) NULL,
    [cod_nat]           VARCHAR (50) NULL,
    [cod_analytique]    VARCHAR (50) NULL,
    [zone_operation]    VARCHAR (50) NULL,
    [region]            VARCHAR (50) NULL,
    [sens_ope]          VARCHAR (50) NULL,
    [scenario_associe]  VARCHAR (50) NULL,
    [date_maj]          VARCHAR (50) NULL,
    [instrument_prod]   VARCHAR (50) NULL
);

