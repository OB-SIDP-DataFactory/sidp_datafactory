﻿CREATE TABLE [dbo].[WK_MC_OPENS] (
    [ClientID]                 INT            NULL,
    [SendID]                   INT            NULL,
    [SubscriberKey]            NVARCHAR (100) NULL,
    [EmailAddress]             NVARCHAR (100) NULL,
    [SubscriberID]             INT            NULL,
    [ListID]                   INT            NULL,
    [EventDate]                DATETIME       NULL,
    [EventType]                NVARCHAR (100) NULL,
    [BatchID]                  NVARCHAR (100) NULL,
    [TriggeredSendExternalKey] NVARCHAR (100) NULL,
    [IsUnique]                 NVARCHAR (5)   NULL,
    [Browser]                  NVARCHAR (100) NULL,
    [EmailClient]              NVARCHAR (100) NULL,
    [OperatingSystem]          NVARCHAR (100) NULL,
    [Device]                   NVARCHAR (100) NULL
);

