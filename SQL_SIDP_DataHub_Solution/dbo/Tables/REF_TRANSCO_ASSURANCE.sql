﻿CREATE TABLE [dbo].[REF_TRANSCO_ASSURANCE] (
    [code_assurance_n1]    VARCHAR (50) NULL,
    [libelle_assurance_n1] VARCHAR (50) NULL,
    [code_assurance_n2]    VARCHAR (50) NULL,
    [libelle_assurance_n2] VARCHAR (50) NULL,
    [code_assurance_n3]    VARCHAR (50) NULL,
    [libelle_assurance_n3] VARCHAR (50) NULL,
    [code_assurance_n4]    VARCHAR (50) NULL,
    [libelle_assurance_n4] VARCHAR (50) NULL,
    [code_assurance]       VARCHAR (50) NULL,
    [libelle_assurance]    VARCHAR (50) NULL,
    [date_debut]           DATE         NULL,
    [date_fin]             DATE         NULL,
    [code_abonnement]      VARCHAR (50) NULL,
    [code_service]         VARCHAR (50) NULL
);


