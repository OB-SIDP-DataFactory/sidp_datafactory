﻿CREATE TABLE [dbo].[WK_MC_SENDJOBS] (
    [ClientID]                  INT            NULL,
    [SendID]                    INT            NULL,
    [FromName]                  NVARCHAR (130) NULL,
    [FromEmail]                 NVARCHAR (100) NULL,
    [SchedTime]                 DATETIME       NULL,
    [SentTime]                  DATETIME       NULL,
    [Subject]                   NVARCHAR (200) NULL,
    [EmailName]                 NVARCHAR (100) NULL,
    [TriggeredSendExternalKey]  NVARCHAR (100) NULL,
    [SendDefinitionExternalKey] NVARCHAR (100) NULL,
    [JobStatus]                 NVARCHAR (30)  NULL,
    [PreviewURL]                NVARCHAR (MAX) NULL,
    [IsMultipart]               NVARCHAR (5)   NULL,
    [Additional]                NVARCHAR (MAX) NULL
);

