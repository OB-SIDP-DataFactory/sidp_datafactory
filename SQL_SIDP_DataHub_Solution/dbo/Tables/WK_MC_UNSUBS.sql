﻿CREATE TABLE [dbo].[WK_MC_UNSUBS] (
    [ClientID]                 INT            NULL,
    [SendID]                   INT            NULL,
    [SubscriberKey]            NVARCHAR (100) NULL,
    [EmailAddress]             NVARCHAR (100) NULL,
    [SubscriberID]             INT            NULL,
    [ListID]                   INT            NULL,
    [EventDate]                DATETIME       NULL,
    [EventType]                NVARCHAR (100) NULL,
    [BatchID]                  NVARCHAR (100) NULL,
    [TriggeredSendExternalKey] NVARCHAR (100) NULL,
    [UnsubReason]              NVARCHAR (MAX) NULL
);

