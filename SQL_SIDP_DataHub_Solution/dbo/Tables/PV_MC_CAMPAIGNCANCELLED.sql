﻿CREATE TABLE [dbo].[PV_MC_CAMPAIGNCANCELLED] (
    [Id]                BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [CAMPAIGN_NAME]     NVARCHAR (255)                              NULL,
    [CANCELLATION_DATE] DATETIME                                    NULL,
    [ID_CAMPAIGN]       NVARCHAR (50)                               NOT NULL,
    [JOURNEY_NAME]      NVARCHAR (100)                              NULL,
    [AUTOMATION_NAME]   NVARCHAR (100)                              NULL,
    [Startdt]           DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]             DATETIME2 (7) GENERATED ALWAYS AS ROW END   NOT NULL,
    CONSTRAINT [PK_PV_MC_CAMPAIGNCANCELLED] PRIMARY KEY CLUSTERED ([Id] ASC, [ID_CAMPAIGN] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_MC_CAMPAIGNCANCELLEDHistory], DATA_CONSISTENCY_CHECK=ON));


