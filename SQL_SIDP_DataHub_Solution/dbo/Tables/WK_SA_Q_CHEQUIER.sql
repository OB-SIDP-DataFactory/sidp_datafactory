﻿CREATE TABLE [dbo].[WK_SA_Q_CHEQUIER] (
    [ID]             BIGINT       IDENTITY (1, 1) NOT NULL,
    [DWHCHQDTX]      DATE         NULL,
    [DWHCHQETB]      INT          NULL,
    [DWHCHQAG1]      INT          NULL,
    [DWHCHQAGE]      INT          NULL,
    [DWHCHQCOM]      VARCHAR (20) NULL,
    [DWHCHQTYP]      VARCHAR (6)  NULL,
    [DWHCHQDAT]      DATE         NULL,
    [DWHCHQSEQ]      INT          NULL,
    [DWHCHQREC]      DATE         NULL,
    [DWHCHQREM]      DATE         NULL,
    [DWHCHQCAL]      VARCHAR (3)  NULL,
    [DWHCHQDSP]      DATE         NULL,
    [DWHCHQREN]      INT          NULL,
    [DWHCHQDRE]      DATE         NULL,
    [DWHCHQORI]      VARCHAR (1)  NULL,
    [NUMR_LIGN_FICH] INT          NULL
);

