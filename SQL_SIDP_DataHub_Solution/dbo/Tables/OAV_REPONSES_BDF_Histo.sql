﻿CREATE TABLE [dbo].[OAV_REPONSES_BDF_Histo] (
    [ID_REPONSES_BDF] DECIMAL (10)   NOT NULL,
    [ERROR]           DECIMAL (1)    NULL,
    [TYPE_ERROR]      NVARCHAR (255) NULL,
    [COLOR]           NVARCHAR (255) NULL,
    [DAT_OBSR]        DATE           NOT NULL,
    [DAT_CHRG]        DATETIME       NOT NULL
);

