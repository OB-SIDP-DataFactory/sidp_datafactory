﻿CREATE TABLE [dbo].[PV_SA_M_PORTEFEUILLE] (
    [ID]        BIGINT          IDENTITY (1, 1) NOT NULL,
    [DWHPTFETB] INT             NULL,
    [DWHPTFAGC] INT             NULL,
    [DWHPTFNPT] INT             NULL,
    [DWHPTFREX] VARCHAR (32)    NULL,
    [DWHPTFRCL] VARCHAR (7)     NULL,
    [DWHPTFSCL] INT             NULL,
    [DWHPTFECL] VARCHAR (20)    NULL,
    [DWHPTFIN1] VARCHAR (32)    NULL,
    [DWHPTFIN2] VARCHAR (32)    NULL,
    [DWHPTFGST] INT             NULL,
    [DWHPTFNGS] VARCHAR (32)    NULL,
    [DWHPTFGSA] INT             NULL,
    [DWHPTFNGA] VARCHAR (32)    NULL,
    [DWHPTFMDG] VARCHAR (3)     NULL,
    [DWHPTFLMG] VARCHAR (32)    NULL,
    [DWHPTFDOV] DATE            NULL,
    [DWHPTFDCL] DATE            NULL,
    [DWHPTFPCL] VARCHAR (1)     NULL,
    [DWHPTFPEA] VARCHAR (1)     NULL,
    [DWHPTFDOP] DATE            NULL,
    [DWHPTFDPR] DATE            NULL,
    [DWHPTFDDR] DATE            NULL,
    [DWHPTFDCP] DATE            NULL,
    [DWHPTFMCP] VARCHAR (1)     NULL,
    [DWHPTFLMC] VARCHAR (32)    NULL,
    [DWHPTFNAN] VARCHAR (1)     NULL,
    [DWHPTFTMD] VARCHAR (2)     NULL,
    [DWHPTFLMD] VARCHAR (32)    NULL,
    [DWHPTFCMI] VARCHAR (3)     NULL,
    [DWHPTFDMJ] DATE            NULL,
    [DWHPTFRMJ] INT             NULL,
    [DWHPTFNMJ] VARCHAR (32)    NULL,
    [DWHPTFVGL] DECIMAL (18, 3) NULL,
    [DWHPTFSLQ] DECIMAL (18, 3) NULL,
    [DWHPTFINT] DECIMAL (18, 3) NULL,
    [DWHPTFPVL] DECIMAL (18, 3) NULL,
    [DWHPTFPVR] DECIMAL (18, 3) NULL,
    [DWHPTFVER] DECIMAL (18, 3) NULL,
    [DWHPTFRET] DECIMAL (18, 3) NULL,
    [DWHPTFCES] DECIMAL (18, 3) NULL,
    [DWHPTFDDE] DATE            NULL,
    [DWHPTFGER] VARCHAR (1)     NULL,
    [DWHPTFPRO] VARCHAR (1)     NULL,
    [DWHPTFTPB] INT             NULL,
    CONSTRAINT [PK_PV_SA_M_PORTEFEUILLE] PRIMARY KEY CLUSTERED ([ID] ASC)
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE DE PTF PROPRE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFTPB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PTF PROPRE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFPRO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PTF GÉRÉ / PAS GÉRÉ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFGER';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE CUMUL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFDDE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CUMUL DES CESSIONS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFCES';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CUMUL DES RETRAITS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFRET';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CUMUL DES VERSEMENTS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFVER';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CUM +/- VAL RÉALIS.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFPVR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CUM +/- VAL LATENTES', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFPVL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CUMUL INTÉRÊTS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFINT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SOLDE EN LIQUIDAT°', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFSLQ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'VAL GLOBALE JOURNAL.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFVGL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MAJ PTF - NOM UTIL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFNMJ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MAJ PTF - RACINE UTI', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFRMJ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MAJ PTF - DATE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFDMJ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CLASSIFICATION MIF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFCMI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIB MAN GES/CL GÉRÉ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFLMD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYP MAN GES/CL GÉRÉ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFTMD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NANTI', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFNAN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIB MOT DE CLÔTURE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFLMC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MOTIF DE CLÔTURE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFMCP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DT CLÔTURE PEA', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFDCP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DT DERNIER RETRAIT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFDDR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DT PREMIER RETRAIT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFDPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE OUVERTURE PEA', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFDOP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PEA', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFPEA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PTF CLOTURÉ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFPCL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE CLÔTURE PTF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFDCL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE OUVERTURE PTF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFDOV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIBELLÉ MODE GEST°', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFLMG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MODE GESTION PTF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFMDG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NOM GESTIO. ADJ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFNGA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'GESTIONNAIRE ADJ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFGSA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NOM GESTIONNAIRE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFNGS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'GESTIONNAIRE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFGST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'INTITULÉ PTF 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFIN2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'INTITULÉ PTF 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFIN1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'REF CPT CL ORIGINE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFECL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SUFFIXE CPT ORIGINE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFSCL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RACINE CL ORIGINE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFRCL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'REF EXTERNE PTF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFREX';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° PORTEFEUILLE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFNPT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AGENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFAGC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ETABLISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PORTEFEUILLE', @level2type = N'COLUMN', @level2name = N'DWHPTFETB';

