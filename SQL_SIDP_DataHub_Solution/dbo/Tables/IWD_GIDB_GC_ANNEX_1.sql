﻿CREATE TABLE [dbo].[IWD_GIDB_GC_ANNEX] (
    [ID]               INT           NOT NULL,
    [CFGOBJECTID]      INT           NOT NULL,
    [CFGOBJECTTYPE]    TINYINT       NOT NULL,
    [SECTIONNAME]      VARCHAR (255) NOT NULL,
    [KEYNAME]          VARCHAR (255) NOT NULL,
    [TENANTID]         INT           NOT NULL,
    [VALUE]            VARCHAR (255) NULL,
    [STATUS]           TINYINT       NOT NULL,
    [CREATED]          DATETIME      NULL,
    [DELETED]          DATETIME      NULL,
    [LASTCHANGE]       DATETIME      NULL,
    [CREATED_TS]       INT           NULL,
    [DELETED_TS]       INT           NULL,
    [LASTCHANGE_TS]    INT           NULL,
    [GSYS_DOMAIN]      INT           NULL,
    [GSYS_SYS_ID]      INT           NULL,
    [GSYS_EXT_VCH1]    VARCHAR (255) NULL,
    [GSYS_EXT_VCH2]    VARCHAR (255) NULL,
    [GSYS_EXT_INT1]    INT           NULL,
    [GSYS_EXT_INT2]    INT           NULL,
    [DATA_SOURCE_KEY]  INT           NOT NULL,
    [CREATE_AUDIT_KEY] NUMERIC (19)  NOT NULL,
    [UPDATE_AUDIT_KEY] NUMERIC (19)  NOT NULL,
    CONSTRAINT [PK_GIDB_GC_ANNEX] PRIMARY KEY CLUSTERED ([CFGOBJECTID] ASC, [CFGOBJECTTYPE] ASC, [KEYNAME] ASC, [SECTIONNAME] ASC)
);
GO
