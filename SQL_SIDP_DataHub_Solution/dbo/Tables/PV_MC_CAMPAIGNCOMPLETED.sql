﻿CREATE TABLE [dbo].[PV_MC_CAMPAIGNCOMPLETED] (
    [Id]                    BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [CAMPAIGN_NAME]         NVARCHAR (255)                              NULL,
    [TYPOLOGY_CAMPAIGN]     NVARCHAR (50)                               NULL,
    [START_DATE]            DATETIME                                    NULL,
    [END_DATE]              DATETIME                                    NULL,
    [FLAG_ROI]              INT                                         NULL,
    [RETARGETTING_FACEBOOK] NVARCHAR (5)                                NULL,
    [RETARGETTING_TWITTER]  NVARCHAR (5)                                NULL,
    [RETARGETTING_COMMENTS] NVARCHAR (200)                              NULL,
    [STATUS]                NVARCHAR (50)                               NULL,
    [ID_CAMPAIGN]           NVARCHAR (50)                               NOT NULL,
    [UPDATE_DATE]           DATETIME                                    NULL,
    [JOURNEY_NAME]          NVARCHAR (100)                              NULL,
    [AUTOMATION_NAME]       NVARCHAR (100)                              NULL,
    [Startdt]               DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]                 DATETIME2 (7) GENERATED ALWAYS AS ROW END   NOT NULL,
    CONSTRAINT [PK_PV_MC_CAMPAIGNCOMPLETED] PRIMARY KEY CLUSTERED ([Id] ASC, [ID_CAMPAIGN] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_MC_CAMPAIGNCOMPLETEDHistory], DATA_CONSISTENCY_CHECK=ON));


