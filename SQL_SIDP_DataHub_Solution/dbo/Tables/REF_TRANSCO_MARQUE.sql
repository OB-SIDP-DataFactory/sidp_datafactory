﻿CREATE TABLE [dbo].[REF_TRANSCO_MARQUE] (
    [code_marque_n1]            VARCHAR (50) NULL,
    [libelle_marque_n1]         VARCHAR (50) NULL,
    [code_marque_n2]            VARCHAR (50) NULL,
    [libelle_marque_n2]         VARCHAR (50) NULL,
    [code_marque]               VARCHAR (50) NULL,
    [libelle_marque]            VARCHAR (50) NULL,
    [date_debut]                DATE         NULL,
    [date_fin]                  DATE         NULL,
    [code_ref_interact_channel] VARCHAR (50) NULL,
    [code_apporteur]            VARCHAR (50) NULL
);

