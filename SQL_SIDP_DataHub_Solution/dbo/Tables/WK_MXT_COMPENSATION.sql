﻿CREATE TABLE [dbo].[WK_MXT_COMPENSATION] (
    [COD_ENRG]               NVARCHAR (2)  NULL,
    [NUMR_SEQN]              NVARCHAR (6)  NULL,
    [EVNM_DECL]              NVARCHAR (4)  NULL,
    [TYP_OPRT_EXTR]          NVARCHAR (4)  NULL,
    [NATR_OPRT]              NVARCHAR (2)  NULL,
    [COD_OPRT_EXTR]          NVARCHAR (3)  NULL,
    [SOUS_OPRT_EXTR]         NVARCHAR (3)  NULL,
    [TYP_MOUV]               NVARCHAR (1)  NULL,
    [DAT_TRTM]               NVARCHAR (8)  NULL,
    [COD_OPRT_SAB]           NVARCHAR (3)  NULL,
    [COD_OPRT_ANNL__SAB]     NVARCHAR (3)  NULL,
    [NOM_PORT]               NVARCHAR (32) NULL,
    [NUMR_CART]              NVARCHAR (19) NULL,
    [NUMR_SEQN_CART]         NVARCHAR (2)  NULL,
    [IBN_COD_PAYS]           NVARCHAR (2)  NULL,
    [IBN_CL]                 NVARCHAR (2)  NULL,
    [IBN_BANQ]               NVARCHAR (5)  NULL,
    [IBN_GUIC]               NVARCHAR (5)  NULL,
    [IBN_COMP]               NVARCHAR (20) NULL,
    [IBN_CL_RIB]             NVARCHAR (2)  NULL,
    [IDNT_PORT]              NVARCHAR (7)  NULL,
    [REFR_CONT]              NVARCHAR (10) NULL,
    [TOP_CART_OPPS]          NVARCHAR (1)  NULL,
    [TOP_CART_CLTR]          NVARCHAR (1)  NULL,
    [MONT_BRT]               NVARCHAR (12) NULL,
    [MONT_BRT__DEVS]         NVARCHAR (12) NULL,
    [MONT_NET]               NVARCHAR (12) NULL,
    [COD_DEVS_MONT_BRT]      NVARCHAR (3)  NULL,
    [COD_DEVS_MONT_BRT_DEVS] NVARCHAR (3)  NULL,
    [SENS_MONT_BRT]          NVARCHAR (1)  NULL,
    [COMM_SERV]              NVARCHAR (12) NULL,
    [REFR_ARCH]              NVARCHAR (12) NULL,
    [NUMR_AUTR]              NVARCHAR (6)  NULL,
    [DAT_LOCL]               NVARCHAR (8)  NULL,
    [HEUR_LOCL]              NVARCHAR (6)  NULL,
    [LIBL_OPRT]              NVARCHAR (32) NULL,
    [STTT_OPRT]              NVARCHAR (1)  NULL,
    [DAT_REGL]               NVARCHAR (8)  NULL,
    [IDNT_UNQ]               NVARCHAR (17) NULL,
    [IDNT_UNQ_OPRT_ORGN]     NVARCHAR (17) NULL,
    [REGN_OPRT]              NVARCHAR (1)  NULL,
    [TYP_DEBT]               NVARCHAR (1)  NULL,
    [NOM_ACCP]               NVARCHAR (25) NULL,
    [COD_PAYS]               NVARCHAR (3)  NULL,
    [COD_DEPR]               NVARCHAR (3)  NULL,
    [VILL_DAB]               NVARCHAR (16) NULL,
    [COD_MCC]                NVARCHAR (4)  NULL,
    [IDNT_TERM]              NVARCHAR (8)  NULL,
    [CONT_COMM]              NVARCHAR (7)  NULL,
    [IBN_COD_PAYS2]          NVARCHAR (2)  NULL,
    [IBN_CL2]                NVARCHAR (2)  NULL,
    [IBN_BANQ2]              NVARCHAR (5)  NULL,
    [IBN_GUIC2]              NVARCHAR (5)  NULL,
    [IBN_COMP2]              NVARCHAR (20) NULL,
    [CL_RIB]                 NVARCHAR (2)  NULL,
    [IDNT_CLNT]              NVARCHAR (7)  NULL,
    [NOMB_OPRT]              NVARCHAR (6)  NULL,
    [MONT]                   NVARCHAR (12) NULL,
    [SENS_MONT_BRT2]         NVARCHAR (1)  NULL,
    [MONT_BRT_DEVS]          NVARCHAR (12) NULL,
    [COD_DEVS]               NVARCHAR (3)  NULL,
    [REFR_REMS]              NVARCHAR (6)  NULL,
    [REFR_ARCH2]             NVARCHAR (17) NULL,
    [DAT_REMS]               NVARCHAR (8)  NULL,
    [COMM_FIX]               NVARCHAR (12) NULL,
    [CUML_COMM]              NVARCHAR (12) NULL,
    [COD_DEVS2]              NVARCHAR (3)  NULL,
    [DAT_VALR]               NVARCHAR (8)  NULL,
    [DAT_REGL2]              NVARCHAR (8)  NULL,
    [STTT]                   NVARCHAR (1)  NULL,
    [COD_BANQ_EMTT]          NVARCHAR (5)  NULL,
    [COD_BANQ_CHF_FIL]       NVARCHAR (5)  NULL,
    [COD_BANQ_ACQR]          NVARCHAR (5)  NULL,
    [COD_BANQ_ACCP]          NVARCHAR (5)  NULL,
    [MONT_COMP]              NVARCHAR (12) NULL,
    [SENS_MONT_COMP]         NVARCHAR (1)  NULL,
    [MONT_COMM_INTR]         NVARCHAR (12) NULL,
    [SENS_COMM]              NVARCHAR (1)  NULL,
    [COD_DEVS_MONT_COMP]     NVARCHAR (3)  NULL,
    [COD_DEVS_COMM_INTR]     NVARCHAR (3)  NULL,
    [MOD_GEST]               NVARCHAR (3)  NULL,
    [RES_COMP]               NVARCHAR (1)  NULL,
    [MONT_BRT2]              NVARCHAR (12) NULL,
    [COD_DEVS3]              NVARCHAR (3)  NULL,
    [MOTF_SUSP]              NVARCHAR (4)  NULL,
    [MOTF_IMP]               NVARCHAR (4)  NULL,
    [DAT_IMP]                NVARCHAR (8)  NULL,
    [DAT_REPR]               NVARCHAR (8)  NULL,
    [MOTF_REPR]              NVARCHAR (4)  NULL,
    [MONT_REJT__PORT]        NVARCHAR (12) NULL,
    [DEVS_MONT_REJT]         NVARCHAR (3)  NULL,
    [REFR_IMP]               NVARCHAR (12) NULL,
    [REFR_DOSS_LITG]         NVARCHAR (12) NULL,
    [TOP_CRDT_BRT]           NVARCHAR (1)  NULL,
    [MOTF_CAPT]              NVARCHAR (2)  NULL,
    [TAUX_CHNG]              NVARCHAR (16) NULL,
    [DAT_ET_HEUR_TRNS_TRNS]  NVARCHAR (10) NULL,
    [TYP_ACCP]               NVARCHAR (1)  NULL,
    [MONT_PENL_RETR]         NVARCHAR (12) NULL,
    [SENS_MONT_PENL]         NVARCHAR (1)  NULL,
    [REFR_LETT_CART]         NVARCHAR (16) NULL,
    [COD_FOUR]               NVARCHAR (3)  NULL, 
    [FILL_TXT] VARCHAR(50) NULL
);

