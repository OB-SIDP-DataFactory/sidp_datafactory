﻿CREATE TABLE [dbo].[PV_SA_Q_LKCPTCLI] (
    [Id]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [DWHGRPDTX]          DATE                                        NULL,
    [DWHGRPETB]          INT                                         NULL,
    [DWHGRPCLI]          VARCHAR (7)                                 NULL,
    [DWHGRPREG]          VARCHAR (7)                                 NULL,
    [DWHGRPREL]          VARCHAR (3)                                 NULL,
    [DWHGRPTGR]          VARCHAR (1)                                 NULL,
    [DWHGRPLGR]          VARCHAR (1)                                 NULL,
    [DWHGRPRIS]          VARCHAR (1)                                 NULL,
    [DWHGRPFRE]          VARCHAR (1)                                 NULL,
    [DWHGRPDSY]          DATE                                        NULL,
    [Validity_StartDate] DATETIME                                    NULL,
    [Validity_EndDate]   DATETIME                                    DEFAULT ('99991231') NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SA_Q_LKCPTCLI] PRIMARY KEY CLUSTERED ([Id] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SA_Q_LKCPTCLIHistory], DATA_CONSISTENCY_CHECK=ON));
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE GROUPE (C/A)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_LKCPTCLI', @level2type = N'COLUMN', @level2name = N'DWHGRPTGR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PÉRIMÈTRE RISQUE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_LKCPTCLI', @level2type = N'COLUMN', @level2name = N'DWHGRPRIS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NATURE RELATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_LKCPTCLI', @level2type = N'COLUMN', @level2name = N'DWHGRPREL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUM CLIENT GROUPE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_LKCPTCLI', @level2type = N'COLUMN', @level2name = N'DWHGRPREG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIEN GRP RISQ (0/1)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_LKCPTCLI', @level2type = N'COLUMN', @level2name = N'DWHGRPLGR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FRÉQUENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_LKCPTCLI', @level2type = N'COLUMN', @level2name = N'DWHGRPFRE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE ETABLISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_LKCPTCLI', @level2type = N'COLUMN', @level2name = N'DWHGRPETB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE SYSTEME', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_LKCPTCLI', @level2type = N'COLUMN', @level2name = N'DWHGRPDSY';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMERO CLIENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_LKCPTCLI', @level2type = N'COLUMN', @level2name = N'DWHGRPCLI';
GO
CREATE NONCLUSTERED INDEX IX_NC_LKCLIENT_VLD_DTE
ON dbo.PV_SA_Q_LKCPTCLI (DWHGRPREG, DWHGRPCLI)
INCLUDE (Validity_StartDate, Validity_EndDate)
GO