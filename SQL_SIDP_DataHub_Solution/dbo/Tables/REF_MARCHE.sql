﻿CREATE TABLE [dbo].[REF_MARCHE] (
    [code_marche_n1]           VARCHAR (50) NULL,
    [libelle_marche_n1]        VARCHAR (50) NULL,
    [code_marche_n2]           VARCHAR (50) NULL,
    [libelle_marche_n2]        VARCHAR (50) NULL,
    [code_marche_n3]           VARCHAR (50) NULL,
    [libelle_marche_n3]        VARCHAR (50) NULL,
    [code_marche]              VARCHAR (50) NULL,
    [libelle_marche]           VARCHAR (50) NULL,
    [date_debut]               DATE         NULL,
    [date_fin]                 DATE         NULL,
    [code_responsable]         VARCHAR (50) NULL,
    [Libelle_code_responsable] VARCHAR (50) NULL
);

