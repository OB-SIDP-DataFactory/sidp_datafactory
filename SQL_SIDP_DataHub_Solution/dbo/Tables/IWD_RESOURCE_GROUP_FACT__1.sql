﻿CREATE TABLE [dbo].[IWD_RESOURCE_GROUP_FACT_] (
    [RESOURCE_GROUP_FACT_KEY] NUMERIC (19) NOT NULL,
    [START_DATE_TIME_KEY]     INT          NOT NULL,
    [END_DATE_TIME_KEY]       INT          NOT NULL,
    [TENANT_KEY]              INT          NOT NULL,
    [RESOURCE_KEY]            INT          NOT NULL,
    [GROUP_KEY]               INT          NOT NULL,
    [RESOURCE_CFG_TYPE_ID]    INT          NOT NULL,
    [CREATE_AUDIT_KEY]        NUMERIC (19) NOT NULL,
    [UPDATE_AUDIT_KEY]        NUMERIC (19) NOT NULL,
    [START_TS]                INT          NULL,
    [END_TS]                  INT          NULL,
    [IDB_ID]                  NUMERIC (19) NOT NULL,
    [ACTIVE_FLAG]             NUMERIC (1)  NULL,
    [PURGE_FLAG]              NUMERIC (1)  NULL,
    CONSTRAINT [PK_RSRC_GR_FT] PRIMARY KEY CLUSTERED ([RESOURCE_GROUP_FACT_KEY] ASC)
);
GO
CREATE UNIQUE NONCLUSTERED INDEX [IWD_I_RGF_STS_RC_GR]
    ON [dbo].[IWD_RESOURCE_GROUP_FACT_]([START_TS] ASC, [RESOURCE_KEY] ASC, [GROUP_KEY] ASC);
GO
