﻿CREATE TABLE [dbo].[OAV_DOSSIER_COMMENTS_Histo] (
    [ID_DOSSIER_COMMENTS] DECIMAL (10)    NOT NULL,
    [ID_UTILISATEURS]     DECIMAL (10)    NULL,
    [ID_DOSSIERS]         DECIMAL (10)    NULL,
    [DATA]                NVARCHAR (4000) NULL,
    [CREATED_AT]          DATETIME2 (7)   NOT NULL,
    [UPDATED_AT]          DATETIME2 (7)   NOT NULL,
    [DAT_OBSR]            DATE            NOT NULL,
    [DAT_CHRG]            DATETIME        NOT NULL
);

