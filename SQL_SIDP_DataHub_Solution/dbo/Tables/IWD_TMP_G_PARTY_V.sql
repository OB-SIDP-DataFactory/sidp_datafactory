﻿CREATE TABLE [dbo].[IWD_TMP_G_PARTY_V] (
    [PARTY_KEY]          NUMERIC (19)  NOT NULL,
    [PARTYID]            VARCHAR (50)  NOT NULL,
    [PARTYGUID]          VARCHAR (50)  NOT NULL,
    [PARENTPARTYID]      VARCHAR (50)  NULL,
    [TS_PARENTPARTYGUID] VARCHAR (50)  NULL,
    [PARENTLINKTYPE]     INT           NULL,
    [TYPE]               INT           NULL,
    [CALLID]             VARCHAR (50)  NULL,
    [ENDPOINTDN]         VARCHAR (255) NULL,
    [ENDPOINTID]         INT           NULL,
    [ENDPOINTTYPE]       INT           NULL,
    [TENANTID]           INT           NULL,
    [AGENTID]            INT           NULL,
    [CCEVENTCAUSE]       INT           NULL,
    [CREATED_TS]         INT           NULL,
    [TERMINATED_TS]      INT           NULL,
    [GSYS_EXT_VCH1]      VARCHAR (255) NULL,
    [INTERACTION_ID]     NUMERIC (19)  NULL,
    [PROLE]              INT           NULL,
    [GSYS_EXT_VCH2]      VARCHAR (255) NULL
);

