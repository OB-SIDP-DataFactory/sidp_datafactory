﻿CREATE TABLE [dbo].[PV_SA_M_PROCPT] (
    [ID]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [DWHPRCDTX]          DATE                                        NULL,
    [DWHPRCETA]          INT                                         NULL,
    [DWHPRCNUM]          VARCHAR (20)                                NULL,
    [DWHPRCOPE]          VARCHAR (3)                                 NULL,
    [DWHPRCNAT]          VARCHAR (10)                                NULL,
    [DWHPRCDEV]          VARCHAR (3)                                 NULL,
    [DWHPRCMOP]          DECIMAL (17, 2)                             NULL,
    [DWHPRCMOB]          DECIMAL (17, 2)                             NULL,
    [DWHPRCDOT]          DECIMAL (17, 2)                             NULL,
    [DWHPRCDOB]          DECIMAL (17, 2)                             NULL,
    [DWHPRCREP]          DECIMAL (17, 2)                             NULL,
    [DWHPRCREB]          DECIMAL (17, 2)                             NULL,
    [DWHPRCPER]          DECIMAL (17, 2)                             NULL,
    [DWHPRCPEB]          DECIMAL (17, 2)                             NULL,
    [DWHPRCSTP]          DECIMAL (17, 2)                             NULL,
    [DWHPRCSTB]          DECIMAL (17, 2)                             NULL,
    [DWHPRCSRE]          DECIMAL (17, 2)                             NULL,
    [DWHPRCSRB]          DECIMAL (17, 2)                             NULL,
    [DWHPRCORG]          VARCHAR (1)                                 NULL,
    [DWHPRCRIS]          VARCHAR (1)                                 NULL,
    [DWHPRCFRE]          VARCHAR (1)                                 NULL,
    [DWHPRCDOM]          VARCHAR (12)                                NULL,
    [Validity_StartDate] DATETIME                                    NULL,
    [Validity_EndDate]   DATETIME                                    DEFAULT ('99991231') NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SA_M_PROCPT] PRIMARY KEY CLUSTERED ([ID] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SA_M_PROCPTHistory], DATA_CONSISTENCY_CHECK=ON));
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DOMAINE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROCPT', @level2type = N'COLUMN', @level2name = N'DWHPRCDOM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FREQUENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROCPT', @level2type = N'COLUMN', @level2name = N'DWHPRCFRE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PÉRIMÈTRE RISQUE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROCPT', @level2type = N'COLUMN', @level2name = N'DWHPRCRIS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ORIGINE PROVISION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROCPT', @level2type = N'COLUMN', @level2name = N'DWHPRCORG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CTVL STOCK REPRISE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROCPT', @level2type = N'COLUMN', @level2name = N'DWHPRCSRB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT STOCK DE REPRISE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROCPT', @level2type = N'COLUMN', @level2name = N'DWHPRCSRE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CTVL MT STCK PRO/DOT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROCPT', @level2type = N'COLUMN', @level2name = N'DWHPRCSTB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT STOCK PROV/DOT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROCPT', @level2type = N'COLUMN', @level2name = N'DWHPRCSTP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT DE PERTES', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROCPT', @level2type = N'COLUMN', @level2name = N'DWHPRCPEB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT PERTE PROVISION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROCPT', @level2type = N'COLUMN', @level2name = N'DWHPRCPER';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT REPRISE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROCPT', @level2type = N'COLUMN', @level2name = N'DWHPRCREB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT REPRISE PROVISION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROCPT', @level2type = N'COLUMN', @level2name = N'DWHPRCREP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT DOTATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROCPT', @level2type = N'COLUMN', @level2name = N'DWHPRCDOB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT DOTAT° PROVISION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROCPT', @level2type = N'COLUMN', @level2name = N'DWHPRCDOT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT PROVISION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROCPT', @level2type = N'COLUMN', @level2name = N'DWHPRCMOB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT PROVISION SPÉCIF.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROCPT', @level2type = N'COLUMN', @level2name = N'DWHPRCMOP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEVISE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROCPT', @level2type = N'COLUMN', @level2name = N'DWHPRCDEV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RUBRIQUE COMPTABLE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROCPT', @level2type = N'COLUMN', @level2name = N'DWHPRCNAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE PRODUIT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROCPT', @level2type = N'COLUMN', @level2name = N'DWHPRCOPE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMÉRO DU COMPTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROCPT', @level2type = N'COLUMN', @level2name = N'DWHPRCNUM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ETABLISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROCPT', @level2type = N'COLUMN', @level2name = N'DWHPRCETA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE EXTRACTION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PROCPT', @level2type = N'COLUMN', @level2name = N'DWHPRCDTX';

