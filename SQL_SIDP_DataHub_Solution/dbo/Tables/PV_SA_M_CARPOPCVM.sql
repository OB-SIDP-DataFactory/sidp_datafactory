﻿CREATE TABLE [dbo].[PV_SA_M_CARPOPCVM] (
    [ID]        BIGINT       IDENTITY (1, 1) NOT NULL,
    [DWHCVOIDV] INT          NULL,
    [DWHCVODDV] DATE         NULL,
    [DWHCVOPDM] VARCHAR (1)  NULL,
    [DWHCVORSC] VARCHAR (7)  NULL,
    [DWHCVOCSC] INT          NULL,
    [DWHCVOESC] VARCHAR (20) NULL,
    [DWHCVORSG] VARCHAR (7)  NULL,
    [DWHCVOCSG] INT          NULL,
    [DWHCVOESG] VARCHAR (20) NULL,
    [DWHCVODEP] VARCHAR (12) NULL,
    [DWHCVORDP] INT          NULL,
    [DWHCVOLDP] VARCHAR (32) NULL,
    [DWHCVOPVL] VARCHAR (2)  NULL,
    [DWHCVONBP] INT          NULL,
    [DWHCVOPBI] INT          NULL,
    [DWHCVOBIA] VARCHAR (3)  NULL,
    [DWHCVOTYG] VARCHAR (2)  NULL,
    [DWHCVOCEN] VARCHAR (7)  NULL,
    CONSTRAINT [PK_PV_SA_M_CARPOPCVM] PRIMARY KEY CLUSTERED ([ID] ASC)
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CHAMP CENTRALISATEUR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARPOPCVM', @level2type = N'COLUMN', @level2name = N'DWHCVOCEN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE DE GESTION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARPOPCVM', @level2type = N'COLUMN', @level2name = N'DWHCVOTYG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PL BOURSE INV ALPHA', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARPOPCVM', @level2type = N'COLUMN', @level2name = N'DWHCVOBIA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PLACE BOURSE INVES', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARPOPCVM', @level2type = N'COLUMN', @level2name = N'DWHCVOPBI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NOMBRE DE PÉRIODES', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARPOPCVM', @level2type = N'COLUMN', @level2name = N'DWHCVONBP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PÉRIO VAL LIQUIDAT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARPOPCVM', @level2type = N'COLUMN', @level2name = N'DWHCVOPVL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIBELLÉ DÉPOSITAIRE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARPOPCVM', @level2type = N'COLUMN', @level2name = N'DWHCVOLDP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RACINE DÉPOSITAIRE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARPOPCVM', @level2type = N'COLUMN', @level2name = N'DWHCVORDP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DÉPOSITAIRE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARPOPCVM', @level2type = N'COLUMN', @level2name = N'DWHCVODEP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CPT STE GES(REF EXT)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARPOPCVM', @level2type = N'COLUMN', @level2name = N'DWHCVOESG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COMPTE SOCIÉTÉ GEST', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARPOPCVM', @level2type = N'COLUMN', @level2name = N'DWHCVOCSG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RACINE SOCIÉTÉ GEST', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARPOPCVM', @level2type = N'COLUMN', @level2name = N'DWHCVORSG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CPT SC/RA.(REF EXT)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARPOPCVM', @level2type = N'COLUMN', @level2name = N'DWHCVOESC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COMPTE SC / RACHAT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARPOPCVM', @level2type = N'COLUMN', @level2name = N'DWHCVOCSC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RACINE SC / RACHAT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARPOPCVM', @level2type = N'COLUMN', @level2name = N'DWHCVORSC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PROD MAISON (0/1)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARPOPCVM', @level2type = N'COLUMN', @level2name = N'DWHCVOPDM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DÉBUT VALIDITÉ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARPOPCVM', @level2type = N'COLUMN', @level2name = N'DWHCVODDV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID. VALEUR MOBILIERE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARPOPCVM', @level2type = N'COLUMN', @level2name = N'DWHCVOIDV';

