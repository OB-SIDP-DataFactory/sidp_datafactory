﻿CREATE TABLE [dbo].[PV_SA_M_PRESTATION] (
    [ID]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [DWHABPDTX]          DATE                                        NULL,
    [DWHABPETA]          INT                                         NULL,
    [DWHABPAGE]          INT                                         NULL,
    [DWHABPSER]          VARCHAR (2)                                 NULL,
    [DWHABPSSE]          VARCHAR (2)                                 NULL,
    [DWHABPNUM]          INT                                         NULL,
    [DWHABPCSE]          VARCHAR (6)                                 NULL,
    [DWHABPPRE]          VARCHAR (6)                                 NULL,
    [DWHABPADH]          DATE                                        NULL,
    [DWHABPFIN]          DATE                                        NULL,
    [DWHABPREN]          DATE                                        NULL,
    [DWHABPCET]          VARCHAR (1)                                 NULL,
    [DWHABPRES]          DATE                                        NULL,
    [DWHABPMOR]          VARCHAR (6)                                 NULL,
    [DWHABPCRE]          DATE                                        NULL,
    [DWHABPVAL]          DATE                                        NULL,
    [Validity_StartDate] DATETIME                                    NULL,
    [Validity_EndDate]   DATETIME                                    DEFAULT ('99991231') NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SA_M_PRESTATION] PRIMARY KEY CLUSTERED ([ID] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SA_M_PRESTATIONHistory], DATA_CONSISTENCY_CHECK=ON));
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE VALIDATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRESTATION', @level2type = N'COLUMN', @level2name = N'DWHABPVAL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE CRÉATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRESTATION', @level2type = N'COLUMN', @level2name = N'DWHABPCRE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MOTIF RÉSILIATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRESTATION', @level2type = N'COLUMN', @level2name = N'DWHABPMOR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE RÉSILIATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRESTATION', @level2type = N'COLUMN', @level2name = N'DWHABPRES';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE ÉTAT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRESTATION', @level2type = N'COLUMN', @level2name = N'DWHABPCET';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE RENOUVELLEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRESTATION', @level2type = N'COLUMN', @level2name = N'DWHABPREN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE FIN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRESTATION', @level2type = N'COLUMN', @level2name = N'DWHABPFIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ADHÉSION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRESTATION', @level2type = N'COLUMN', @level2name = N'DWHABPADH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE PRESTATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRESTATION', @level2type = N'COLUMN', @level2name = N'DWHABPPRE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE SERVICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRESTATION', @level2type = N'COLUMN', @level2name = N'DWHABPCSE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMÉRO CONTRAT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRESTATION', @level2type = N'COLUMN', @level2name = N'DWHABPNUM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SOUS-SERVICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRESTATION', @level2type = N'COLUMN', @level2name = N'DWHABPSSE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SERVICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRESTATION', @level2type = N'COLUMN', @level2name = N'DWHABPSER';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AGENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRESTATION', @level2type = N'COLUMN', @level2name = N'DWHABPAGE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ETABLISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRESTATION', @level2type = N'COLUMN', @level2name = N'DWHABPETA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ANALYSE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRESTATION', @level2type = N'COLUMN', @level2name = N'DWHABPDTX';

