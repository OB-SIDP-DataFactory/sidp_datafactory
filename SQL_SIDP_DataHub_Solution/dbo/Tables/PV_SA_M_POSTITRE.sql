﻿CREATE TABLE [dbo].[PV_SA_M_POSTITRE] (
    [ID]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [DWHPOSDTE]          DATE                                        NULL,
    [DWHPOSTPO]          VARCHAR (1)                                 NULL,
    [DWHPOSETB]          INT                                         NULL,
    [DWHPOSAGC]          INT                                         NULL,
    [DWHPOSNPT]          INT                                         NULL,
    [DWHPOSIDV]          INT                                         NULL,
    [DWHPOSDDV]          DATE                                        NULL,
    [DWHPOSNTP]          VARCHAR (2)                                 NULL,
    [DWHPOSCMA]          VARCHAR (2)                                 NULL,
    [DWHPOSDEP]          VARCHAR (12)                                NULL,
    [DWHPOSREX]          VARCHAR (32)                                NULL,
    [DWHPOSRVM]          INT                                         NULL,
    [DWHPOSPCT]          INT                                         NULL,
    [DWHPOSPCA]          VARCHAR (3)                                 NULL,
    [DWHPOSISI]          VARCHAR (12)                                NULL,
    [DWHPOSTCN]          VARCHAR (23)                                NULL,
    [DWHPOSDSC]          VARCHAR (32)                                NULL,
    [DWHPOSDEC]          DATE                                        NULL,
    [DWHPOSPEX]          DECIMAL (18, 9)                             NULL,
    [DWHPOSLNT]          VARCHAR (32)                                NULL,
    [DWHPOSLMA]          VARCHAR (32)                                NULL,
    [DWHPOSRDP]          VARCHAR (7)                                 NULL,
    [DWHPOSLDP]          VARCHAR (32)                                NULL,
    [DWHPOSRCL]          VARCHAR (7)                                 NULL,
    [DWHPOSECL]          VARCHAR (20)                                NULL,
    [DWHPOSGST]          INT                                         NULL,
    [DWHPOSNGS]          VARCHAR (32)                                NULL,
    [DWHPOSGSA]          INT                                         NULL,
    [DWHPOSNGA]          VARCHAR (32)                                NULL,
    [DWHPOSRBC]          INT                                         NULL,
    [DWHPOSNBC]          VARCHAR (2)                                 NULL,
    [DWHPOSQTE]          DECIMAL (18, 6)                             NULL,
    [DWHPOSTYC]          VARCHAR (2)                                 NULL,
    [DWHPOSLTC]          VARCHAR (32)                                NULL,
    [DWHPOSDVC]          VARCHAR (3)                                 NULL,
    [DWHPOSCVA]          DECIMAL (18, 9)                             NULL,
    [DWHPOSCVR]          DECIMAL (18, 9)                             NULL,
    [DWHPOSCRF]          DECIMAL (14, 9)                             NULL,
    [DWHPOSMTC]          DECIMAL (18, 6)                             NULL,
    [DWHPOSMTR]          DECIMAL (18, 6)                             NULL,
    [DWHPOSSLC]          DECIMAL (18, 6)                             NULL,
    [DWHPOSSLR]          DECIMAL (18, 6)                             NULL,
    [DWHPOSINC]          DECIMAL (18, 6)                             NULL,
    [DWHPOSINR]          DECIMAL (18, 6)                             NULL,
    [DWHPOSPVC]          DECIMAL (18, 6)                             NULL,
    [DWHPOSPVR]          DECIMAL (18, 6)                             NULL,
    [DWHPOSPMC]          DECIMAL (18, 6)                             NULL,
    [DWHPOSPMR]          DECIMAL (18, 6)                             NULL,
    [DWHPOSOPE]          VARCHAR (6)                                 NULL,
    [DWHPOSNAT]          VARCHAR (6)                                 NULL,
    [DWHPOSNUM]          INT                                         NULL,
    [DWHPOSDDC]          DATE                                        NULL,
    [DWHPOSQTP]          DECIMAL (18, 6)                             NULL,
    [DWHPOSTYP]          VARCHAR (3)                                 NULL,
    [DWHPOSTTR]          INT                                         NULL,
    [DWHPOSTSU]          VARCHAR (2)                                 NULL,
    [DWHPOSNMT]          VARCHAR (1)                                 NULL,
    [DWHPOSCPR]          VARCHAR (2)                                 NULL,
    [DWHPOSLIV]          VARCHAR (1)                                 NULL,
    [DWHPOSSND]          VARCHAR (6)                                 NULL,
    [DWHPOSSEN]          INT                                         NULL,
    [DWHPOSMCC]          DECIMAL (18, 6)                             NULL,
    [DWHPOSMCR]          DECIMAL (18, 6)                             NULL,
    [DWHPOSVBC]          DECIMAL (18, 6)                             NULL,
    [DWHPOSVBR]          DECIMAL (18, 6)                             NULL,
    [DWHPOSVRD]          DECIMAL (18, 6)                             NULL,
    [DWHPOSDUR]          INT                                         NULL,
    [DWHPOSREF]          VARCHAR (20)                                NULL,
    [DWHPOSVNC]          DECIMAL (18, 6)                             NULL,
    [DWHPOSVNR]          DECIMAL (18, 6)                             NULL,
    [Validity_StartDate] DATETIME                                    NULL,
    [Validity_EndDate]   DATETIME                                    DEFAULT ('99991231') NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SA_M_POSTITRE] PRIMARY KEY CLUSTERED ([ID] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SA_M_POSTITREHistory], DATA_CONSISTENCY_CHECK=ON));
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'VAL NET EN DEV REG', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSVNR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'VAL NET EN DEV COT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSVNC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RÉFÉRENCE CONTRAT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSREF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DUREE REST. À COURIR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSDUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'VAL REMBR.EN DEV REG', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSVRD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'VAL BRUT EN DEV REG', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSVBR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'VAL BRUT EN DEV COT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSVBC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT COUPON EN DEV REG', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSMCR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT COUPON EN DEV COT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSMCC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SENS DE LA TRANSACT°', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSSEN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CONTREPARTIE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSSND';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIVRÉ/NON LIVRÉ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSLIV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CLASSE PRODUIT TCN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSCPR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NATURE MARCHÉ TCN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSNMT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE SUPPORT TCN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSTSU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE TRANSACT° TCN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSTTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE OPERATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSTYP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'QTÉ EN POSITION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSQTP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DERNIÈRE COTATI', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSDDC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IDENTIFIANT POSITION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSNUM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE NATURE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSNAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE OPÉRATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSOPE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PRIX MOY ACH DEV REF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSPMR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PRIX MOYEN ACHAT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSPMC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PLUS VAL LAT DEV REF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSPVR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PLUS VAL LAT DEV COT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSPVC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'INTERÊTS EN DEV REF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSINR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'INTERÊTS EN DEV COT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSINC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SOLDE LIQ EN DEV REF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSSLR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SOLDE LIQ EN DEV COT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSSLC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT EVALUÉ EN DEV REF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSMTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT EVALUÉ EN DEV COT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSMTC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COURS VAL / REF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSCRF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COURS VAL EN DEV REF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSCVR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COURS VAL EN DEV COT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSCVA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEVISE COTATION ISO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSDVC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIB TYPE DE COURS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSLTC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE COURS UTILISÉ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSTYC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'QTÉ EN NOMINAL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSQTE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NATIO BOURSE ISO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSNBC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RACINE BOURSE COTAT°', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSRBC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NOM GESTIO ADJOINT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSNGA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'GESTIONNAIRE ADJOINT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSGSA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NOM GESTIONNAIRE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSNGS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'GESTIONNAIRE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSGST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RÉF COMPTE CLIENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSECL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RACINE CLIENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSRCL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIBELLE DÉPOSITAIRE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSLDP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RACINE DÉPOSITAIRE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSRDP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIBELLE DU MARCHÉ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSLMA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIB NATURE POSIT°', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSLNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PRIX EXERCICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSPEX';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ÉCHÉANCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSDEC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DESCRIPTION VM', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSDSC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE MNÉMONIQUE TCN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSTCN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE ISIN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSISI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PLACE COTAT° ALPHA', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSPCA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PLACE DE COTATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSPCT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RACINE VALEUR SAMIC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSRVM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'REF EXT DU PTF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSREX';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DÉPOSITAIRE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSDEP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE MARCHÉ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSCMA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NATURE DE POSITION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSNTP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DT DÉBUT VALIDITÉ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSDDV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID. VALEUR MOBILIERE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSIDV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° PORTEFEUILLE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSNPT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AGENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSAGC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ETABLISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSETB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE DE POSITION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSTPO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ÉVALUATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_POSTITRE', @level2type = N'COLUMN', @level2name = N'DWHPOSDTE';

