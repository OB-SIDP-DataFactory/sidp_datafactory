﻿CREATE TABLE [dbo].[RAW_SF_REFERENTIEL] (
    [ID]                   BIGINT         IDENTITY (1, 1) NOT NULL,
    [OBJECT_CODE_SF]       NVARCHAR (255) NULL,
    [FIELD_CODE_SF]        NVARCHAR (255) NULL,
    [FIELD_NAME_SF]        NVARCHAR (255) NULL,
    [IDX_SF]               INT            NULL,
    [CODE_SF]              NVARCHAR (255) NULL,
    [LIBELLE_SF]           NVARCHAR (255) NULL,
    [IS_DEFAULT]           BIT            NULL,
    [IS_ACTIVE]            BIT            NULL,
    [PARENT_FIELD_CODE_SF] NVARCHAR (255) NULL,
    [PARENT_FIELD_NAME_SF] NVARCHAR (255) NULL,
    [PARENT_IDX_SF]        INT            NULL,
    [PARENT_CODE_SF]       NVARCHAR (255) NULL,
    [PARENT_LIBELLE_SF]    NVARCHAR (255) NULL
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant technique SIDP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RAW_SF_REFERENTIEL', @level2type = N'COLUMN', @level2name = N'ID';
GO
CREATE NONCLUSTERED INDEX IX_NC_RAW_SF_REF_COD_LIB
ON [dbo].[RAW_SF_REFERENTIEL]([OBJECT_CODE_SF], [FIELD_CODE_SF])
INCLUDE([CODE_SF], [LIBELLE_SF]);
GO