﻿CREATE TABLE [dbo].[WK_FE_COMMERCIALPRODUCT_V2] (
    [ID_FE_COMMERCIALPRODUCT] DECIMAL (19)   NOT NULL,
    [BILLING_SERVICE_CODE]    NVARCHAR (255) NULL,
    [CODE]                    NVARCHAR (255) NULL,
    [CONVENTION_CODE]         NVARCHAR (255) NULL,
    [CREATION_DATE]           DATETIME2 (6)  NULL,
    [CREATION_USER]           NVARCHAR (255) NULL,
    [DISCRIMINATOR]           NVARCHAR (31)  NULL,
    [END_DATE]                DATE           NULL,
    [EQUIPMENT_LABEL]         NVARCHAR (255) NULL,
    [LABEL]                   NVARCHAR (255) NULL,
    [LAST_UPDATE_DATE]        DATETIME2 (6)  NULL,
    [LAST_UPDATE_USER]        NVARCHAR (255) NULL,
    [OFFER_DURATION_IN_DAYS]  DECIMAL (10)   NULL,
    [PRICE_CODE_ID]           DECIMAL (19)   NULL,
    [PRODUCT_TYPE_ID]         DECIMAL (19)   NULL,
    [START_DATE]              DATE           NULL,
    [USER_TYPE]               NVARCHAR (255) NULL,
    [SAB_CODE]                NVARCHAR (255) NULL
);

