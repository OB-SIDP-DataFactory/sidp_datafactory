﻿CREATE TABLE [dbo].[WK_FE_COMMERCIALOFFER_V2] (
    [ID_FE_COMMERCIALOFFER]   DECIMAL (19)   NOT NULL,
    [CODE]                    NVARCHAR (255) NULL,
    [CREATION_DATE]           DATETIME2 (6)  NULL,
    [CREATION_USER]           NVARCHAR (255) NULL,
    [DISTRIBUTION_NETWORK_ID] DECIMAL (19)   NULL,
    [END_DATE]                DATE           NULL,
    [LABEL]                   NVARCHAR (255) NULL,
    [LAST_UPDATE_DATE]        DATETIME2 (6)  NULL,
    [LAST_UPDATE_USER]        NVARCHAR (255) NULL,
    [PRODUCT_FAMILY_ID]       DECIMAL (19)   NULL,
    [START_DATE]              DATE           NULL,
    [USER_TYPE]               NVARCHAR (255) NULL
);

