﻿CREATE TABLE [dbo].[PV_SA_M_CHEQUIER] (
    [ID]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [DWHCHQDTX]          DATE                                        NULL,
    [DWHCHQETB]          INT                                         NULL,
    [DWHCHQAG1]          INT                                         NULL,
    [DWHCHQAGE]          INT                                         NULL,
    [DWHCHQCOM]          VARCHAR (20)                                NULL,
    [DWHCHQTYP]          VARCHAR (6)                                 NULL,
    [DWHCHQDAT]          DATE                                        NULL,
    [DWHCHQSEQ]          INT                                         NULL,
    [DWHCHQREC]          DATE                                        NULL,
    [DWHCHQREM]          DATE                                        NULL,
    [DWHCHQCAL]          VARCHAR (3)                                 NULL,
    [DWHCHQDSP]          DATE                                        NULL,
    [DWHCHQREN]          INT                                         NULL,
    [DWHCHQDRE]          DATE                                        NULL,
    [DWHCHQORI]          VARCHAR (1)                                 NULL,
    [Validity_StartDate] DATETIME                                    NULL,
    [Validity_EndDate]   DATETIME                                    DEFAULT ('99991231') NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SA_M_CHEQUIER] PRIMARY KEY CLUSTERED ([ID] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SA_M_CHEQUIERHistory], DATA_CONSISTENCY_CHECK=ON));
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ORIGINE DU CHEQUIER', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CHEQUIER', @level2type = N'COLUMN', @level2name = N'DWHCHQORI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DE RENOUV.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CHEQUIER', @level2type = N'COLUMN', @level2name = N'DWHCHQDRE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE ETAT RENOUV.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CHEQUIER', @level2type = N'COLUMN', @level2name = N'DWHCHQREN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE SUPPRESSION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CHEQUIER', @level2type = N'COLUMN', @level2name = N'DWHCHQDSP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE LIVRAISON', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CHEQUIER', @level2type = N'COLUMN', @level2name = N'DWHCHQCAL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE REMISE CHEQUIER', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CHEQUIER', @level2type = N'COLUMN', @level2name = N'DWHCHQREM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE RECEPT°CHEQUIER', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CHEQUIER', @level2type = N'COLUMN', @level2name = N'DWHCHQREC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMERO DE SEQUENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CHEQUIER', @level2type = N'COLUMN', @level2name = N'DWHCHQSEQ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DEMANDE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CHEQUIER', @level2type = N'COLUMN', @level2name = N'DWHCHQDAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE CHEQUIER', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CHEQUIER', @level2type = N'COLUMN', @level2name = N'DWHCHQTYP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUNMERO DE COMPTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CHEQUIER', @level2type = N'COLUMN', @level2name = N'DWHCHQCOM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AGENCE DU COMPTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CHEQUIER', @level2type = N'COLUMN', @level2name = N'DWHCHQAGE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AGENCE DU STOCK', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CHEQUIER', @level2type = N'COLUMN', @level2name = N'DWHCHQAG1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE ETABLISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CHEQUIER', @level2type = N'COLUMN', @level2name = N'DWHCHQETB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE D''ANALYSE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CHEQUIER', @level2type = N'COLUMN', @level2name = N'DWHCHQDTX';

