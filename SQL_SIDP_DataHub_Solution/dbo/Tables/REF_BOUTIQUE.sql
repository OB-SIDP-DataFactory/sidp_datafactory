﻿CREATE TABLE [dbo].[REF_BOUTIQUE]
(
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Code_ADV_Orange] [nvarchar](255) NULL,
	[Type_Boutique] [nvarchar](255) NULL,
	[Site_Libelle] [nvarchar](255) NULL,
	[DO] [nvarchar](255) NULL,
	[AD] [nvarchar](255) NULL,
	[Site_CodePostal] [nvarchar](255) NULL,
	[Site_Ville] [nvarchar](255) NULL,
	[Site_Adresse1] [nvarchar](255) NULL,
	[TypeDeSite_Libelle] [nvarchar](255) NULL, 
    CONSTRAINT [PK_REF_BOUTIQUE] PRIMARY KEY ([ID]),
);
