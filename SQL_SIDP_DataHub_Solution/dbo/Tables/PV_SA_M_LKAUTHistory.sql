﻿CREATE TABLE [dbo].[PV_SA_M_LKAUTHistory] (
    [ID]                 BIGINT          NOT NULL,
    [DWHLIADTX]          DATE            NULL,
    [DWHLIAETA]          INT             NULL,
    [DWHLIACLI]          VARCHAR (7)     NULL,
    [DWHLIATYP]          VARCHAR (1)     NULL,
    [DWHLIAAUT]          VARCHAR (20)    NULL,
    [DWHLIATY1]          VARCHAR (1)     NULL,
    [DWHLIAAU1]          VARCHAR (20)    NULL,
    [DWHLIATAU]          DECIMAL (21, 9) NULL,
    [DWHLIAUTI]          INT             NULL,
    [DWHLIAFRA]          VARCHAR (1)     NULL,
    [DWHLIADOM]          VARCHAR (12)    NULL,
    [DWHLIAAPP]          VARCHAR (3)     NULL,
    [DWHLIADSY]          DATE            NULL,
    [Validity_StartDate] DATETIME        NULL,
    [Validity_EndDate]   DATETIME        NULL,
    [Startdt]            DATETIME2 (7)   NOT NULL,
    [Enddt]              DATETIME2 (7)   NOT NULL
);

