﻿CREATE TABLE [dbo].[PV_SA_M_PRESTATIONHistory] (
    [ID]                 BIGINT        NOT NULL,
    [DWHABPDTX]          DATE          NULL,
    [DWHABPETA]          INT           NULL,
    [DWHABPAGE]          INT           NULL,
    [DWHABPSER]          VARCHAR (2)   NULL,
    [DWHABPSSE]          VARCHAR (2)   NULL,
    [DWHABPNUM]          INT           NULL,
    [DWHABPCSE]          VARCHAR (6)   NULL,
    [DWHABPPRE]          VARCHAR (6)   NULL,
    [DWHABPADH]          DATE          NULL,
    [DWHABPFIN]          DATE          NULL,
    [DWHABPREN]          DATE          NULL,
    [DWHABPCET]          VARCHAR (1)   NULL,
    [DWHABPRES]          DATE          NULL,
    [DWHABPMOR]          VARCHAR (6)   NULL,
    [DWHABPCRE]          DATE          NULL,
    [DWHABPVAL]          DATE          NULL,
    [Validity_StartDate] DATETIME      NULL,
    [Validity_EndDate]   DATETIME      NULL,
    [Startdt]            DATETIME2 (7) NOT NULL,
    [Enddt]              DATETIME2 (7) NOT NULL
);

