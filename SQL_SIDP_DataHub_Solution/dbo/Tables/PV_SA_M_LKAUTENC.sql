﻿CREATE TABLE [dbo].[PV_SA_M_LKAUTENC] (
    [ID]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [DWHENADTX]          DATE                                        NULL,
    [DWHENAETA]          INT                                         NULL,
    [DWHENACLI]          VARCHAR (7)                                 NULL,
    [DWHENATYP]          VARCHAR (1)                                 NULL,
    [DWHENAAUT]          VARCHAR (20)                                NULL,
    [DWHENADEV]          VARCHAR (3)                                 NULL,
    [DWHENAAGE]          INT                                         NULL,
    [DWHENASER]          VARCHAR (2)                                 NULL,
    [DWHENASSE]          VARCHAR (2)                                 NULL,
    [DWHENAOPE]          VARCHAR (6)                                 NULL,
    [DWHENADOS]          INT                                         NULL,
    [DWHENANAT]          VARCHAR (6)                                 NULL,
    [DWHENAMBA]          DECIMAL (18, 3)                             NULL,
    [DWHENAENC]          DECIMAL (18, 3)                             NULL,
    [DWHENAREP]          DECIMAL (18, 3)                             NULL,
    [DWHENADEB]          DATE                                        NULL,
    [DWHENAFIN]          DATE                                        NULL,
    [DWHENAUTI]          INT                                         NULL,
    [DWHENAFRA]          VARCHAR (1)                                 NULL,
    [DWHENADOM]          VARCHAR (12)                                NULL,
    [DWHENAAPP]          VARCHAR (3)                                 NULL,
    [DWHENADSY]          DATE                                        NULL,
    [Validity_StartDate] DATETIME                                    NULL,
    [Validity_EndDate]   DATETIME                                    DEFAULT ('99991231') NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SA_M_LKAUTENC] PRIMARY KEY CLUSTERED ([ID] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SA_M_LKAUTENCHistory], DATA_CONSISTENCY_CHECK=ON));
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DU JOUR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUTENC', @level2type = N'COLUMN', @level2name = N'DWHENADSY';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'APPLICATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUTENC', @level2type = N'COLUMN', @level2name = N'DWHENAAPP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DOMAINE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUTENC', @level2type = N'COLUMN', @level2name = N'DWHENADOM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FRÉQUENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUTENC', @level2type = N'COLUMN', @level2name = N'DWHENAFRA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE UTILISATEUR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUTENC', @level2type = N'COLUMN', @level2name = N'DWHENAUTI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE FIN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUTENC', @level2type = N'COLUMN', @level2name = N'DWHENAFIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DEBUT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUTENC', @level2type = N'COLUMN', @level2name = N'DWHENADEB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT REPORTS/DEV BASE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUTENC', @level2type = N'COLUMN', @level2name = N'DWHENAREP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT ENCOURS/DEV BASE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUTENC', @level2type = N'COLUMN', @level2name = N'DWHENAENC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT ENCOURS/DEV CLI', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUTENC', @level2type = N'COLUMN', @level2name = N'DWHENAMBA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NATURE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUTENC', @level2type = N'COLUMN', @level2name = N'DWHENANAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMERO DU DOSSIER', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUTENC', @level2type = N'COLUMN', @level2name = N'DWHENADOS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE OPP.DU DOSSIER', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUTENC', @level2type = N'COLUMN', @level2name = N'DWHENAOPE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SOUS-SERVICE DOSSIER', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUTENC', @level2type = N'COLUMN', @level2name = N'DWHENASSE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SERVICE DU DOSSIER', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUTENC', @level2type = N'COLUMN', @level2name = N'DWHENASER';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AGENCE DU DOSSIER', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUTENC', @level2type = N'COLUMN', @level2name = N'DWHENAAGE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEVISE DU DOSSIER', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUTENC', @level2type = N'COLUMN', @level2name = N'DWHENADEV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE AUTORISATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUTENC', @level2type = N'COLUMN', @level2name = N'DWHENAAUT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE AUTORISATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUTENC', @level2type = N'COLUMN', @level2name = N'DWHENATYP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CLIENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUTENC', @level2type = N'COLUMN', @level2name = N'DWHENACLI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ETABLISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUTENC', @level2type = N'COLUMN', @level2name = N'DWHENAETA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE EXTRACTION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKAUTENC', @level2type = N'COLUMN', @level2name = N'DWHENADTX';

