﻿CREATE TABLE [dbo].[WK_SA_Q_SERVICE] (
    [DWHABSDTX]      DATE        NULL,
    [DWHABSETA]      INT         NULL,
    [DWHABSAGE]      INT         NULL,
    [DWHABSSER]      VARCHAR (2) NULL,
    [DWHABSSSE]      VARCHAR (2) NULL,
    [DWHABSNUM]      INT         NULL,
    [DWHABSCSE]      VARCHAR (6) NULL,
    [DWHABSADH]      DATE        NULL,
    [DWHABSFIN]      DATE        NULL,
    [DWHABSREN]      DATE        NULL,
    [DWHABSCET]      VARCHAR (1) NULL,
    [DWHABSRES]      DATE        NULL,
    [DWHABSMOR]      VARCHAR (6) NULL,
    [DWHABSCRE]      DATE        NULL,
    [DWHABSVAL]      DATE        NULL,
    [NUMR_LIGN_FICH] INT         NULL
);

