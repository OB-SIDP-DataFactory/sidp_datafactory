﻿CREATE TABLE [dbo].[PV_SA_M_NOTEXTCLIHistory] (
    [ID]                 BIGINT        NOT NULL,
    [DWHNEXDTX]          DATE          NULL,
    [DWHNEXETB]          INT           NULL,
    [DWHNEXCLI]          VARCHAR (7)   NULL,
    [DWHNEXNOT]          VARCHAR (7)   NULL,
    [DWHNEXTYP]          VARCHAR (1)   NULL,
    [DWHNEXDAT]          DATE          NULL,
    [DWHNEXLON]          VARCHAR (6)   NULL,
    [DWHNEXCOU]          VARCHAR (6)   NULL,
    [DWHNEXFRE]          VARCHAR (1)   NULL,
    [DWHNEXDSY]          DATE          NULL,
    [Validity_StartDate] DATETIME      NULL,
    [Validity_EndDate]   DATETIME      NULL,
    [Startdt]            DATETIME2 (7) NOT NULL,
    [Enddt]              DATETIME2 (7) NOT NULL
);

