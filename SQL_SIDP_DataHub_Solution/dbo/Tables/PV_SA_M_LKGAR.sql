﻿CREATE TABLE [dbo].[PV_SA_M_LKGAR] (
    [ID]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [DWHLIEDTX]          DATE                                        NULL,
    [DWHLIEETA]          INT                                         NULL,
    [DWHLIEAGE]          INT                                         NULL,
    [DWHLIESER]          VARCHAR (2)                                 NULL,
    [DWHLIESSE]          VARCHAR (2)                                 NULL,
    [DWHLIEOPE]          VARCHAR (6)                                 NULL,
    [DWHLIENAT]          VARCHAR (6)                                 NULL,
    [DWHLIENDO]          INT                                         NULL,
    [DWHLIEAG2]          INT                                         NULL,
    [DWHLIESE2]          VARCHAR (2)                                 NULL,
    [DWHLIESS2]          VARCHAR (2)                                 NULL,
    [DWHLIEOP2]          VARCHAR (6)                                 NULL,
    [DWHLIERAT]          VARCHAR (20)                                NULL,
    [DWHLIEPRC]          DECIMAL (6, 3)                              NULL,
    [DWHLIECDG]          VARCHAR (7)                                 NULL,
    [DWHLIEORG]          VARCHAR (1)                                 NULL,
    [DWHLIEPER]          VARCHAR (1)                                 NULL,
    [DWHLIEFRE]          VARCHAR (1)                                 NULL,
    [DWHLIEDSY]          DATE                                        NULL,
    [Validity_StartDate] DATETIME                                    NOT NULL,
    [Validity_EndDate]   DATETIME                                    CONSTRAINT [DF__PV_SA_M_LKGAR__ValidEndDate] DEFAULT ('99991231') NOT NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SA_M_LKGAR] PRIMARY KEY CLUSTERED ([ID] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SA_M_LKGARHistory], DATA_CONSISTENCY_CHECK=ON));
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE SYSTEME', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKGAR', @level2type = N'COLUMN', @level2name = N'DWHLIEDSY';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FRÉQUENCE EXTRACTION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKGAR', @level2type = N'COLUMN', @level2name = N'DWHLIEFRE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PÉRIMÈTRE DE RISQUE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKGAR', @level2type = N'COLUMN', @level2name = N'DWHLIEPER';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ORIGINE:OPE,CPT,AUT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKGAR', @level2type = N'COLUMN', @level2name = N'DWHLIEORG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° CODE GARANT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKGAR', @level2type = N'COLUMN', @level2name = N'DWHLIECDG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'% COUVERTURE GAR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKGAR', @level2type = N'COLUMN', @level2name = N'DWHLIEPRC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° OPERAT° RATTACHEE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKGAR', @level2type = N'COLUMN', @level2name = N'DWHLIERAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE OPERA RATTACHEE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKGAR', @level2type = N'COLUMN', @level2name = N'DWHLIEOP2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SSERVI OPERA RATTACH', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKGAR', @level2type = N'COLUMN', @level2name = N'DWHLIESS2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SERVI OPERA RATTACHE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKGAR', @level2type = N'COLUMN', @level2name = N'DWHLIESE2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AGENCE OPERA RATTACH', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKGAR', @level2type = N'COLUMN', @level2name = N'DWHLIEAG2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° OPERATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKGAR', @level2type = N'COLUMN', @level2name = N'DWHLIENDO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE NATURE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKGAR', @level2type = N'COLUMN', @level2name = N'DWHLIENAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE OPERATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKGAR', @level2type = N'COLUMN', @level2name = N'DWHLIEOPE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SOUS-SERVICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKGAR', @level2type = N'COLUMN', @level2name = N'DWHLIESSE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SERVICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKGAR', @level2type = N'COLUMN', @level2name = N'DWHLIESER';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AGENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKGAR', @level2type = N'COLUMN', @level2name = N'DWHLIEAGE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ETABLISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKGAR', @level2type = N'COLUMN', @level2name = N'DWHLIEETA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ANALYSE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKGAR', @level2type = N'COLUMN', @level2name = N'DWHLIEDTX';

