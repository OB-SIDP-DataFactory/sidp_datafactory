﻿CREATE TABLE [dbo].[PV_SF_OPPORTUNITYCONTACTROLE_F10] (
    [Id]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [Id_SF]              NVARCHAR (50)                               NOT NULL,
    [OpportunityId]      NVARCHAR (18)                               NULL,
    [ContactId]          NVARCHAR (18)                               NULL,
    [Role]               NVARCHAR (40)                               NULL,
    [IsPrimary]          NVARCHAR (10)                               NULL,
    [CreatedDate]        DATETIME                                    NULL,
    [CreatedById]        NVARCHAR (18)                               NULL,
    [LastModifiedDate]   DATETIME                                    NULL,
    [LastModifiedById]   NVARCHAR (18)                               NULL,
    [SystemModstamp]     DATETIME                                    NULL,
    [IsDeleted]          NVARCHAR (10)                               NULL,
    [Validity_StartDate] DATETIME2 (7)                               NOT NULL,
    [Validity_EndDate]   DATETIME2 (7)                               DEFAULT ('99991231') NOT NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SF_OPPORTUNITYCONTACTROLE_F10] PRIMARY KEY CLUSTERED ([Id] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SF_OPPORTUNITYCONTACTROLE_F10History], DATA_CONSISTENCY_CHECK=ON));

