﻿CREATE TABLE [dbo].[IWD_GIDB_GO_CAMPAIGNHISTORY] (
    [ID]               NUMERIC (16)  NOT NULL,
    [SESSID]           VARCHAR (64)  NOT NULL,
    [STATE]            INT           NOT NULL,
    [PREVSTATE]        INT           NULL,
    [CAUSE]            INT           NOT NULL,
    [PREVSENTER]       DATETIME      NULL,
    [PREVSENTER_TS]    INT           NULL,
    [SEQ]              INT           NOT NULL,
    [APPID]            INT           NULL,
    [TRANS]            INT           NULL,
    [ADDED]            DATETIME      NOT NULL,
    [ADDED_TS]         INT           NULL,
    [GSYS_DOMAIN]      INT           NULL,
    [GSYS_SYS_ID]      INT           NULL,
    [GSYS_EXT_VCH1]    VARCHAR (255) NULL,
    [GSYS_EXT_VCH2]    VARCHAR (255) NULL,
    [GSYS_EXT_INT1]    INT           NULL,
    [GSYS_EXT_INT2]    INT           NULL,
    [CREATE_AUDIT_KEY] NUMERIC (19)  NOT NULL
);
GO
CREATE NONCLUSTERED INDEX [IWD_I_G_GO_CMP_H_CTS]
    ON [dbo].[IWD_GIDB_GO_CAMPAIGNHISTORY]([ADDED_TS] ASC);

