﻿CREATE TABLE [dbo].[PV_MC_UNSUBSHistory] (
    [Id]                       BIGINT         NOT NULL,
    [ClientID]                 INT            NULL,
    [SendID]                   INT            NULL,
    [SubscriberKey]            NVARCHAR (100) NOT NULL,
    [EmailAddress]             NVARCHAR (100) NULL,
    [SubscriberID]             INT            NULL,
    [ListID]                   INT            NULL,
    [EventDate]                DATETIME       NOT NULL,
    [EventType]                NVARCHAR (100) NULL,
    [BatchID]                  NVARCHAR (100) NULL,
    [TriggeredSendExternalKey] NVARCHAR (100) NULL,
    [UnsubReason]              NVARCHAR (MAX) NULL,
    [Startdt]                  DATETIME2 (7)  NOT NULL,
    [Enddt]                    DATETIME2 (7)  NOT NULL
);
GO
CREATE CLUSTERED INDEX [ix_PV_MC_UNSUBSHistory]
    ON [dbo].[PV_MC_UNSUBSHistory]([Enddt] ASC, [Startdt] ASC) WITH (DATA_COMPRESSION = PAGE);

