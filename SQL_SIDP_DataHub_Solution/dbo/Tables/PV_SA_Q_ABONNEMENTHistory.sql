﻿CREATE TABLE [dbo].[PV_SA_Q_ABONNEMENTHistory] (
    [ID]                 BIGINT        NOT NULL,
    [DWHABODTX]          DATE          NULL,
    [DWHABOETA]          INT           NULL,
    [DWHABOAGE]          INT           NULL,
    [DWHABOSER]          VARCHAR (2)   NULL,
    [DWHABOSSE]          VARCHAR (2)   NULL,
    [DWHABONUM]          INT           NULL,
    [DWHABOCLI]          VARCHAR (7)   NULL,
    [DWHABOCOM]          VARCHAR (20)  NULL,
    [DWHABOPRO]          VARCHAR (3)   NULL,
    [DWHABOADH]          DATE          NULL,
    [DWHABOFIN]          DATE          NULL,
    [DWHABOREN]          DATE          NULL,
    [DWHABOCET]          VARCHAR (1)   NULL,
    [DWHABOCOF]          VARCHAR (20)  NULL,
    [DWHABOUT1]          INT           NULL,
    [DWHABOUT2]          INT           NULL,
    [DWHABORES]          DATE          NULL,
    [DWHABOMOR]          VARCHAR (6)   NULL,
    [DWHABOCRE]          DATE          NULL,
    [DWHABOVAL]          DATE          NULL,
    [DWHABOANN]          DATE          NULL,
    [DWHABOREL]          VARCHAR (1)   NULL,
    [Validity_StartDate] DATETIME      NULL,
    [Validity_EndDate]   DATETIME      NULL,
    [Startdt]            DATETIME2 (7) NOT NULL,
    [Enddt]              DATETIME2 (7) NOT NULL,
    [NUMR_LIGN_FICH]     INT           NULL
);
GO
CREATE CLUSTERED INDEX [ix_PV_SA_Q_ABONNEMENTHistory]
    ON [dbo].[PV_SA_Q_ABONNEMENTHistory]([Enddt] ASC, [Startdt] ASC) WITH (DATA_COMPRESSION = PAGE);

