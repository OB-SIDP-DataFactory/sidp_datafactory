﻿CREATE TABLE [dbo].[PV_FE_CARD_PRODUCT] (
    [Id]                          INT             IDENTITY (1, 1) NOT NULL,
    [ID_CARD_PRODUCT]             NUMERIC (19)    NOT NULL,
    [BILLING_SERVICE_CODE]        NVARCHAR (255)  NULL,
    [CONVENTION_CODE]             NVARCHAR (255)  NULL,
    [MINIMUM_INCOMES]             NUMERIC (19, 2) NULL,
    [NUMBER_OF_USE_BEFORE_FREE]   NUMERIC (10)    NULL,
    [PRIORITY_CODE]               NUMERIC (10)    NULL,
    [RENEWAL_CEILING_PAYMENT]     NUMERIC (10)    NULL,
    [RENEWAL_CEILING_WITHDRAWAL]  NUMERIC (10)    NULL,
    [VALIDITY_PERIOD_IN_MONTHS]   NUMERIC (10)    NOT NULL,
    [MINIMUM_INCOMES_CURRENCY_ID] NUMERIC (19)    NOT NULL,
    [PHYSICAL_TYPE_ID]            NUMERIC (19)    NOT NULL,
    [TYPE_ID]                     NUMERIC (19)    NOT NULL,
    [DAT_CRTN_ENRG]               DATETIME        NOT NULL,
    [DAT_DERN_MODF_ENRG]          DATETIME        NOT NULL,
    [DAT_DEBT_VALD]               DATE            NOT NULL,
    [DAT_FIN_VALD]                DATE            NOT NULL,
    [FLG_ENRG_ACTIF]              BIT             NOT NULL,
    CONSTRAINT [PK_PV_FE_CARD_PRODUCT] PRIMARY KEY CLUSTERED ([Id] ASC)
);

