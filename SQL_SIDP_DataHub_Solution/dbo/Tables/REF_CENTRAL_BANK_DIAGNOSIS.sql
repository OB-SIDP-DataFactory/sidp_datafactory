﻿CREATE TABLE [dbo].[REF_CENTRAL_BANK_DIAGNOSIS] (
    [ID]                        INT            IDENTITY (1, 1) NOT NULL,
    [ID_CENTRAL_BANK_DIAGNOSIS] DECIMAL (38)   NULL,
    [FRANFINANCE_CODE]          NVARCHAR (255) NULL,
    CONSTRAINT [PK_REF_CENTRAL_BANK_DIAGNOSIS] PRIMARY KEY CLUSTERED ([ID] ASC)
);

