﻿CREATE TABLE [dbo].[WK_MC_BOUNCES] (
    [ClientID]                 INT            NULL,
    [SendID]                   INT            NULL,
    [SubscriberKey]            NVARCHAR (100) NULL,
    [EmailAddress]             NVARCHAR (100) NULL,
    [SubscriberID]             INT            NULL,
    [ListID]                   INT            NULL,
    [EventDate]                DATETIME       NULL,
    [EventType]                NVARCHAR (100) NULL,
    [BounceCategory]           NVARCHAR (50)  NULL,
    [SMTPCode]                 SMALLINT       NULL,
    [BounceReason]             NVARCHAR (MAX) NULL,
    [BatchID]                  NVARCHAR (100) NULL,
    [TriggeredSendExternalKey] NVARCHAR (101) NULL
);

