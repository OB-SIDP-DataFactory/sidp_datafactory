﻿CREATE TABLE [dbo].[WK_FE_REF_DISTRIBUTION_NETWORK] (
    [DISTRIBUTION_NETWORK_ID] NUMERIC (19)   NOT NULL,
    [ALIAS_CODE]              NVARCHAR (255) NULL,
    [SALESFORCE_CODE]         NVARCHAR (255) NULL,
    [FRANFINANCE_CODE]        NVARCHAR (255) NULL,
    [CORE_BANKING_CODE]       NVARCHAR (255) NULL,
    [CODE]                    NVARCHAR (255) NULL,
    [END_DATE]                DATETIME       NULL,
    [PRIORITY]                NUMERIC (19)   NULL,
    [REF_FAMILY_ID]           NUMERIC (19)   NULL,
    [START_DATE]              DATETIME       NULL
);

