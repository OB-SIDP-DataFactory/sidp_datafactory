﻿CREATE TABLE [dbo].[PV_SF_USER] (
    [Id]                                                 INT                                         IDENTITY (1, 1) NOT NULL,
    [Id_SF]                                              NVARCHAR (18)                               NULL,
    [CreatedDate]                                        DATETIME                                    NULL,
    [Firstname]                                          NVARCHAR (40)                               NULL,
    [Lastname]                                           NVARCHAR (80)                               NULL,
    [Title]                                              NVARCHAR (80)                               NULL,
    [Phone]                                              NVARCHAR (40)                               NULL,
    [Email]                                              NVARCHAR (128)                              NULL,
    [City]                                               NVARCHAR (40)                               NULL,
    [Country]                                            NVARCHAR (80)                               NULL,
    [CountryCode]                                        NVARCHAR (10)                               NULL,
    [Latitude]                                           DECIMAL (18, 15)                            NULL,
    [LastModifiedDate]                                   DATETIME                                    NULL,
    [Longitude]                                          DECIMAL (18, 15)                            NULL,
    [PostalCode]                                         NVARCHAR (20)                               NULL,
    [State]                                              NVARCHAR (80)                               NULL,
    [StateCode]                                          NVARCHAR (10)                               NULL,
    [Street]                                             NVARCHAR (255)                              NULL,
    [UserRoleId]                                         NVARCHAR (18)                               NULL,
    [ProfileId]                                          NVARCHAR (18)                               NULL,
    [QSProfile__c]                                       NVARCHAR (255)                              NULL,
    [GenesysUserId__c]                                   NVARCHAR (10)                               NULL,
    [Username]                                           NVARCHAR (80)                               NULL,
    [Name]                                               NVARCHAR (121)                              NULL,
    [CompanyName]                                        NVARCHAR (80)                               NULL,
    [Division]                                           NVARCHAR (80)                               NULL,
    [Department]                                         NVARCHAR (80)                               NULL,
    [GeocodeAccuracy]                                    NVARCHAR (40)                               NULL,
    [EmailPreferencesAutoBcc]                            NVARCHAR (10)                               NULL,
    [EmailPreferencesAutoBccStayInTouch]                 NVARCHAR (10)                               NULL,
    [EmailPreferencesStayInTouchReminder]                NVARCHAR (10)                               NULL,
    [SenderEmail]                                        NVARCHAR (80)                               NULL,
    [SenderName]                                         NVARCHAR (80)                               NULL,
    [Signature]                                          NVARCHAR (1333)                             NULL,
    [StayInTouchSubject]                                 NVARCHAR (80)                               NULL,
    [StayInTouchSignature]                               NVARCHAR (512)                              NULL,
    [StayInTouchNote]                                    NVARCHAR (512)                              NULL,
    [Fax]                                                NVARCHAR (40)                               NULL,
    [MobilePhone]                                        NVARCHAR (40)                               NULL,
    [Alias]                                              NVARCHAR (8)                                NULL,
    [CommunityNickname]                                  NVARCHAR (40)                               NULL,
    [BadgeText]                                          NVARCHAR (80)                               NULL,
    [IsActive]                                           NVARCHAR (10)                               NULL,
    [TimeZoneSidKey]                                     NVARCHAR (40)                               NULL,
    [LocaleSidKey]                                       NVARCHAR (40)                               NULL,
    [ReceivesInfoEmails]                                 NVARCHAR (10)                               NULL,
    [ReceivesAdminInfoEmails]                            NVARCHAR (10)                               NULL,
    [EmailEncodingKey]                                   NVARCHAR (40)                               NULL,
    [UserType]                                           NVARCHAR (40)                               NULL,
    [LanguageLocaleKey]                                  NVARCHAR (40)                               NULL,
    [EmployeeNumber]                                     NVARCHAR (20)                               NULL,
    [DelegatedApproverId]                                NVARCHAR (18)                               NULL,
    [ManagerId]                                          NVARCHAR (18)                               NULL,
    [LastLoginDate]                                      DATETIME2 (7)                               NULL,
    [CreatedById]                                        NVARCHAR (18)                               NULL,
    [LastModifiedById]                                   NVARCHAR (18)                               NULL,
    [SystemModstamp]                                     DATETIME2 (7)                               NULL,
    [OfflineTrialExpirationDate]                         DATETIME2 (7)                               NULL,
    [OfflinePdaTrialExpirationDate]                      DATETIME2 (7)                               NULL,
    [UserPermissionsMarketingUser]                       NVARCHAR (10)                               NULL,
    [UserPermissionsOfflineUser]                         NVARCHAR (10)                               NULL,
    [UserPermissionsAvantgoUser]                         NVARCHAR (10)                               NULL,
    [UserPermissionsCallCenterAutoLogin]                 NVARCHAR (10)                               NULL,
    [UserPermissionsMobileUser]                          NVARCHAR (10)                               NULL,
    [UserPermissionsSFContentUser]                       NVARCHAR (10)                               NULL,
    [UserPermissionsKnowledgeUser]                       NVARCHAR (10)                               NULL,
    [UserPermissionsInteractionUser]                     NVARCHAR (10)                               NULL,
    [UserPermissionsSupportUser]                         NVARCHAR (10)                               NULL,
    [UserPermissionsLiveAgentUser]                       NVARCHAR (10)                               NULL,
    [UserPermissionsChatterAnswersUser]                  NVARCHAR (10)                               NULL,
    [ForecastEnabled]                                    NVARCHAR (10)                               NULL,
    [UserPreferencesActivityRemindersPopup]              NVARCHAR (10)                               NULL,
    [UserPreferencesEventRemindersCheckboxDefault]       NVARCHAR (10)                               NULL,
    [UserPreferencesTaskRemindersCheckboxDefault]        NVARCHAR (10)                               NULL,
    [UserPreferencesReminderSoundOff]                    NVARCHAR (10)                               NULL,
    [UserPreferencesDisableAllFeedsEmail]                NVARCHAR (10)                               NULL,
    [UserPreferencesDisableFollowersEmail]               NVARCHAR (10)                               NULL,
    [UserPreferencesDisableProfilePostEmail]             NVARCHAR (10)                               NULL,
    [UserPreferencesDisableChangeCommentEmail]           NVARCHAR (10)                               NULL,
    [UserPreferencesDisableLaterCommentEmail]            NVARCHAR (10)                               NULL,
    [UserPreferencesDisProfPostCommentEmail]             NVARCHAR (10)                               NULL,
    [UserPreferencesApexPagesDeveloperMode]              NVARCHAR (10)                               NULL,
    [UserPreferencesHideCSNGetChatterMobileTask]         NVARCHAR (10)                               NULL,
    [UserPreferencesDisableMentionsPostEmail]            NVARCHAR (10)                               NULL,
    [UserPreferencesDisMentionsCommentEmail]             NVARCHAR (10)                               NULL,
    [UserPreferencesHideCSNDesktopTask]                  NVARCHAR (10)                               NULL,
    [UserPreferencesHideChatterOnboardingSplash]         NVARCHAR (10)                               NULL,
    [UserPreferencesHideSecondChatterOnboardingSplash]   NVARCHAR (10)                               NULL,
    [UserPreferencesDisCommentAfterLikeEmail]            NVARCHAR (10)                               NULL,
    [UserPreferencesDisableLikeEmail]                    NVARCHAR (10)                               NULL,
    [UserPreferencesSortFeedByComment]                   NVARCHAR (10)                               NULL,
    [UserPreferencesDisableMessageEmail]                 NVARCHAR (10)                               NULL,
    [UserPreferencesDisableBookmarkEmail]                NVARCHAR (10)                               NULL,
    [UserPreferencesDisableSharePostEmail]               NVARCHAR (10)                               NULL,
    [UserPreferencesEnableAutoSubForFeeds]               NVARCHAR (10)                               NULL,
    [UserPreferencesDisableFileShareNotificationsForApi] NVARCHAR (10)                               NULL,
    [UserPreferencesShowTitleToExternalUsers]            NVARCHAR (10)                               NULL,
    [UserPreferencesShowManagerToExternalUsers]          NVARCHAR (10)                               NULL,
    [UserPreferencesShowEmailToExternalUsers]            NVARCHAR (10)                               NULL,
    [UserPreferencesShowWorkPhoneToExternalUsers]        NVARCHAR (10)                               NULL,
    [UserPreferencesShowMobilePhoneToExternalUsers]      NVARCHAR (10)                               NULL,
    [UserPreferencesShowFaxToExternalUsers]              NVARCHAR (10)                               NULL,
    [UserPreferencesShowStreetAddressToExternalUsers]    NVARCHAR (10)                               NULL,
    [UserPreferencesShowCityToExternalUsers]             NVARCHAR (10)                               NULL,
    [UserPreferencesShowStateToExternalUsers]            NVARCHAR (10)                               NULL,
    [UserPreferencesShowPostalCodeToExternalUsers]       NVARCHAR (10)                               NULL,
    [UserPreferencesShowCountryToExternalUsers]          NVARCHAR (10)                               NULL,
    [UserPreferencesShowProfilePicToGuestUsers]          NVARCHAR (10)                               NULL,
    [UserPreferencesShowTitleToGuestUsers]               NVARCHAR (10)                               NULL,
    [UserPreferencesShowCityToGuestUsers]                NVARCHAR (10)                               NULL,
    [UserPreferencesShowStateToGuestUsers]               NVARCHAR (10)                               NULL,
    [UserPreferencesShowPostalCodeToGuestUsers]          NVARCHAR (10)                               NULL,
    [UserPreferencesShowCountryToGuestUsers]             NVARCHAR (10)                               NULL,
    [UserPreferencesHideS1BrowserUI]                     NVARCHAR (10)                               NULL,
    [UserPreferencesDisableEndorsementEmail]             NVARCHAR (10)                               NULL,
    [UserPreferencesPathAssistantCollapsed]              NVARCHAR (10)                               NULL,
    [UserPreferencesCacheDiagnostics]                    NVARCHAR (10)                               NULL,
    [UserPreferencesShowEmailToGuestUsers]               NVARCHAR (10)                               NULL,
    [UserPreferencesShowManagerToGuestUsers]             NVARCHAR (10)                               NULL,
    [UserPreferencesShowWorkPhoneToGuestUsers]           NVARCHAR (10)                               NULL,
    [UserPreferencesShowMobilePhoneToGuestUsers]         NVARCHAR (10)                               NULL,
    [UserPreferencesShowFaxToGuestUsers]                 NVARCHAR (10)                               NULL,
    [UserPreferencesShowStreetAddressToGuestUsers]       NVARCHAR (10)                               NULL,
    [UserPreferencesLightningExperiencePreferred]        NVARCHAR (10)                               NULL,
    [UserPreferencesPreviewLightning]                    NVARCHAR (10)                               NULL,
    [UserPreferencesHideEndUserOnboardingAssistantModal] NVARCHAR (10)                               NULL,
    [UserPreferencesHideLightningMigrationModal]         NVARCHAR (10)                               NULL,
    [UserPreferencesHideSfxWelcomeMat]                   NVARCHAR (10)                               NULL,
    [UserPreferencesHideBiggerPhotoCallout]              NVARCHAR (10)                               NULL,
    [UserPreferencesGlobalNavBarWTShown]                 NVARCHAR (10)                               NULL,
    [UserPreferencesGlobalNavGridMenuWTShown]            NVARCHAR (10)                               NULL,
    [UserPreferencesCreateLEXAppsWTShown]                NVARCHAR (10)                               NULL,
    [UserPreferencesFavoritesWTShown]                    NVARCHAR (10)                               NULL,
    [UserPreferencesRecordHomeSectionCollapseWTShown]    NVARCHAR (10)                               NULL,
    [UserPreferencesRecordHomeReservedWTShown]           NVARCHAR (10)                               NULL,
    [UserPreferencesFavoritesShowTopFavorites]           NVARCHAR (10)                               NULL,
    [UserPreferencesExcludeMailAppAttachments]           NVARCHAR (10)                               NULL,
    [ContactId]                                          NVARCHAR (18)                               NULL,
    [AccountId]                                          NVARCHAR (18)                               NULL,
    [CallCenterId]                                       NVARCHAR (18)                               NULL,
    [Extension]                                          NVARCHAR (40)                               NULL,
    [PortalRole]                                         NVARCHAR (40)                               NULL,
    [IsPortalEnabled]                                    NVARCHAR (10)                               NULL,
    [IsPortalSelfRegistered]                             NVARCHAR (10)                               NULL,
    [FederationIdentifier]                               NVARCHAR (512)                              NULL,
    [AboutMe]                                            NVARCHAR (1000)                             NULL,
    [FullPhotoUrl]                                       NVARCHAR (1024)                             NULL,
    [SmallPhotoUrl]                                      NVARCHAR (1024)                             NULL,
    [IsExtIndicatorVisible]                              NVARCHAR (10)                               NULL,
    [OutOfOfficeMessage]                                 NVARCHAR (40)                               NULL,
    [MediumPhotoUrl]                                     NVARCHAR (1024)                             NULL,
    [DigestFrequency]                                    NVARCHAR (40)                               NULL,
    [DefaultGroupNotificationFrequency]                  NVARCHAR (40)                               NULL,
    [LastViewedDate]                                     DATETIME2 (7)                               NULL,
    [LastReferencedDate]                                 DATETIME2 (7)                               NULL,
    [BannerPhotoUrl]                                     NVARCHAR (1024)                             NULL,
    [SmallBannerPhotoUrl]                                NVARCHAR (1024)                             NULL,
    [MediumBannerPhotoUrl]                               NVARCHAR (1024)                             NULL,
    [IsProfilePhotoActive]                               NVARCHAR (10)                               NULL,
    [IndividualId]									     NVARCHAR (20)                               NULL,
    [BypassPB__c]                                        NVARCHAR (10)                               NULL,
    [BypassVR__c]                                        NVARCHAR (10)                               NULL,
    [BypassWF__c]                                        NVARCHAR (10)                               NULL,
    [PAD_BypassTrigger__c]                               NVARCHAR (MAX)                              NULL,
    [et4ae5__Default_ET_Page__c]                         NVARCHAR (255)                              NULL,
    [et4ae5__Default_MID__c]                             NVARCHAR (20)                               NULL,
    [et4ae5__ExactTargetForAppExchangeAdmin__c]          NVARCHAR (10)                               NULL,
    [et4ae5__ExactTargetForAppExchangeUser__c]           NVARCHAR (10)                               NULL,
    [et4ae5__ExactTargetUsername__c]                     NVARCHAR (255)                              NULL,
    [et4ae5__ExactTarget_OAuth_Token__c]                 NVARCHAR (175)                              NULL,
    [et4ae5__ValidExactTargetAdmin__c]                   NVARCHAR (10)                               NULL,
    [et4ae5__ValidExactTargetUser__c]                    NVARCHAR (10)                               NULL,
    [TCHQSProducts__c]                                   NVARCHAR (1300)                             NULL,
    [Referent__c]                                        NVARCHAR (18)                               NULL,
    [Conseiller_technique__c]                            NVARCHAR (10)                               NULL,
	[CUID__c]											 NVARCHAR (8)                                NULL,	
    [Validity_StartDate]                                 DATETIME2 (7)                               NOT NULL,
    [Validity_EndDate]                                   DATETIME2 (7)                               DEFAULT ('99991231') NOT NULL,
    [Startdt]                                            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]                                              DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SF_USER] PRIMARY KEY CLUSTERED ([Id] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SF_USERHistory], DATA_CONSISTENCY_CHECK=ON))
GO
CREATE NONCLUSTERED INDEX IX_NC_USR_ID_SF
ON dbo.PV_SF_USER ([Id_SF])
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de début de validité fonctionnelle', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'Validity_StartDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de fin de validité fonctionnelle', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'Validity_EndDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de rôle', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserRoleId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fonction', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'Title';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Rue', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'Street';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code de région/province', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'StateCode';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Région/Province', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'State';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de début de validité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'Startdt';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Profil QuickSign', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'QSProfile__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de profil', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'ProfileId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code postal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'PostalCode';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Téléphone', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'Phone';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Longitude', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'Longitude';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Latitude', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'Latitude';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'Lastname';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'LastModifiedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID d''utilisateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'Id_SF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant technique SIDP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'Id';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Id téléphonique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'GenesysUserId__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prénom', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'Firstname';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de fin de validité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'Enddt';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'E-mail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'Email';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'CreatedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code de pays', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'CountryCode';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Pays', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'Country';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ville', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'City';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type d''utilisateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserType';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TaskRemindersCheckboxDefault', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesTaskRemindersCheckboxDefault';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SortFeedByComment', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesSortFeedByComment';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ShowWorkPhoneToGuestUsers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesShowWorkPhoneToGuestUsers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ShowWorkPhoneToExternalUsers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesShowWorkPhoneToExternalUsers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ShowTitleToGuestUsers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesShowTitleToGuestUsers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ShowTitleToExternalUsers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesShowTitleToExternalUsers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ShowStreetAddressToGuestUsers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesShowStreetAddressToGuestUsers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ShowStreetAddressToExternalUsers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesShowStreetAddressToExternalUsers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ShowStateToGuestUsers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesShowStateToGuestUsers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ShowStateToExternalUsers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesShowStateToExternalUsers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ShowProfilePicToGuestUsers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesShowProfilePicToGuestUsers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ShowPostalCodeToGuestUsers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesShowPostalCodeToGuestUsers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ShowPostalCodeToExternalUsers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesShowPostalCodeToExternalUsers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ShowMobilePhoneToGuestUsers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesShowMobilePhoneToGuestUsers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ShowMobilePhoneToExternalUsers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesShowMobilePhoneToExternalUsers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ShowManagerToGuestUsers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesShowManagerToGuestUsers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ShowManagerToExternalUsers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesShowManagerToExternalUsers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ShowFaxToGuestUsers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesShowFaxToGuestUsers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ShowFaxToExternalUsers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesShowFaxToExternalUsers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ShowEmailToGuestUsers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesShowEmailToGuestUsers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ShowEmailToExternalUsers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesShowEmailToExternalUsers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ShowCountryToGuestUsers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesShowCountryToGuestUsers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ShowCountryToExternalUsers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesShowCountryToExternalUsers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ShowCityToGuestUsers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesShowCityToGuestUsers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ShowCityToExternalUsers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesShowCityToExternalUsers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ReminderSoundOff', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesReminderSoundOff';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RecordHomeSectionCollapseWTShown', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesRecordHomeSectionCollapseWTShown';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RecordHomeReservedWTShown', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesRecordHomeReservedWTShown';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PreviewLightning', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesPreviewLightning';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PathAssistantCollapsed', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesPathAssistantCollapsed';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LightningExperiencePreferred', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesLightningExperiencePreferred';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'HideSfxWelcomeMat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesHideSfxWelcomeMat';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'HideSecondChatterOnboardingSplash', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesHideSecondChatterOnboardingSplash';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'HideS1BrowserUI', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesHideS1BrowserUI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'HideLightningMigrationModal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesHideLightningMigrationModal';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'HideEndUserOnboardingAssistantModal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesHideEndUserOnboardingAssistantModal';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'HideCSNGetChatterMobileTask', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesHideCSNGetChatterMobileTask';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'HideCSNDesktopTask', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesHideCSNDesktopTask';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'HideChatterOnboardingSplash', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesHideChatterOnboardingSplash';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'HideBiggerPhotoCallout', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesHideBiggerPhotoCallout';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'GlobalNavGridMenuWTShown', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesGlobalNavGridMenuWTShown';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'GlobalNavBarWTShown', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesGlobalNavBarWTShown';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FavoritesWTShown', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesFavoritesWTShown';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FavoritesShowTopFavorites', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesFavoritesShowTopFavorites';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ExcludeMailAppAttachments', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesExcludeMailAppAttachments';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'EventRemindersCheckboxDefault', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesEventRemindersCheckboxDefault';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'EnableAutoSubForFeeds', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesEnableAutoSubForFeeds';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DisProfPostCommentEmail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesDisProfPostCommentEmail';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DisMentionsCommentEmail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesDisMentionsCommentEmail';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DisCommentAfterLikeEmail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesDisCommentAfterLikeEmail';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DisableSharePostEmail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesDisableSharePostEmail';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DisableProfilePostEmail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesDisableProfilePostEmail';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DisableMessageEmail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesDisableMessageEmail';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DisableMentionsPostEmail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesDisableMentionsPostEmail';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DisableLikeEmail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesDisableLikeEmail';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DisableLaterCommentEmail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesDisableLaterCommentEmail';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DisableFollowersEmail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesDisableFollowersEmail';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DisableFileShareNotificationsForApi', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesDisableFileShareNotificationsForApi';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DisableEndorsementEmail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesDisableEndorsementEmail';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DisableChangeCommentEmail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesDisableChangeCommentEmail';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DisableBookmarkEmail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesDisableBookmarkEmail';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DisableAllFeedsEmail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesDisableAllFeedsEmail';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CreateLEXAppsWTShown', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesCreateLEXAppsWTShown';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CacheDiagnostics', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesCacheDiagnostics';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ApexPagesDeveloperMode', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesApexPagesDeveloperMode';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ActivityRemindersPopup', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPreferencesActivityRemindersPopup';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Utilisateur de Service Cloud', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPermissionsSupportUser';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Utilisateur de Salesforce CRM Content', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPermissionsSFContentUser';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Utilisateur de Offline', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPermissionsOfflineUser';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Utilisateur d''Apex Mobile', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPermissionsMobileUser';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Utilisateur de Marketing', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPermissionsMarketingUser';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Utilisateur de Live Agent', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPermissionsLiveAgentUser';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Utilisateur de Knowledge', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPermissionsKnowledgeUser';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Utilisateur de flux', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPermissionsInteractionUser';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Utilisateur de Réponses Chatter', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPermissionsChatterAnswersUser';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Connexion automatique au centre d''appels', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPermissionsCallCenterAutoLogin';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Utilisateur d''AvantGo', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'UserPermissionsAvantgoUser';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom d''utilisateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'Username';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fuseau horaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'TimeZoneSidKey';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TCHQSProducts', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'TCHQSProducts__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Horodateur des modifications du système', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'SystemModstamp';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Objet de l''e-mail Rester en contact', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'StayInTouchSubject';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Signature de l''e-mail Rester en contact', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'StayInTouchSignature';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Note de l''e-mail Rester en contact', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'StayInTouchNote';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Photo', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'SmallPhotoUrl';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'URL de la photo de bannière iOS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'SmallBannerPhotoUrl';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Signature du message', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'Signature';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom de l''expéditeur de l''e-mail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'SenderName';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Adresse de l''expéditeur de l''e-mail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'SenderEmail';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Référent', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'Referent__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Messages d''informations', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'ReceivesInfoEmails';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Messages d''informations d''administration', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'ReceivesAdminInfoEmails';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Niveau du rôle du portail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'PortalRole';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PAD Bypass', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'PAD_BypassTrigger__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Message d''absence du bureau', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'OutOfOfficeMessage';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date d''expiration de la version d''essai de Offline Edition', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'OfflineTrialExpirationDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date d''expiration de la période d''essai de Sales Anywhere', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'OfflinePdaTrialExpirationDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom complet', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'Name';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mobile', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'MobilePhone';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'URL de la photo de profil moyenne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'MediumPhotoUrl';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'URL de la photo de bannière Android', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'MediumBannerPhotoUrl';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID du responsable', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'ManagerId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Paramètres régionaux', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'LocaleSidKey';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernier affichage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'LastViewedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernière date référencée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'LastReferencedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernière modification par ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'LastModifiedById';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernière connexion', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'LastLoginDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Langue', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'LanguageLocaleKey';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Avec une photo de profil', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'IsProfilePhotoActive';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Inscrit automatiquement via le portail Client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'IsPortalSelfRegistered';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Le portail activé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'IsPortalEnabled';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Afficher l''indicateur externe', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'IsExtIndicatorVisible';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'IsActive';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Geocode Accuracy', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'GeocodeAccuracy';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'URL de la photo en taille réelle', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'FullPhotoUrl';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autoriser les prévisions', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'ForecastEnabled';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de fédération SAML', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'FederationIdentifier';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Télécopie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'Fax';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Extension', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'Extension';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Utilisateur Marketing Cloud valide', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'et4ae5__ValidExactTargetUser__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Administrateur Marketing Cloud valide', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'et4ae5__ValidExactTargetAdmin__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom d''utilisateur Marketing Cloud', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'et4ae5__ExactTargetUsername__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Utilisateur Marketing Cloud AppExchange', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'et4ae5__ExactTargetForAppExchangeUser__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Admin Marketing Cloud pour AppExchange', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'et4ae5__ExactTargetForAppExchangeAdmin__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'@déprécié', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'et4ae5__ExactTarget_OAuth_Token__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MID par défaut', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'et4ae5__Default_MID__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Page Marketing Cloud par défaut', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'et4ae5__Default_ET_Page__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro d''employé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'EmployeeNumber';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'StayInTouchReminder', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'EmailPreferencesStayInTouchReminder';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AutoBccStayInTouch', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'EmailPreferencesAutoBccStayInTouch';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AutoBcc', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'EmailPreferencesAutoBcc';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Codage des e-mails', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'EmailEncodingKey';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Division', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'Division';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fréquence de résumé des e-mails Chatter', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'DigestFrequency';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Service', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'Department';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de l''approbateur délégué', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'DelegatedApproverId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fréquence de notification par défaut en joignant des groupes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'DefaultGroupNotificationFrequency';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID créé par', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'CreatedById';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID du contact', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'ContactId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Conseiller technique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'Conseiller_technique__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom de la société', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'CompanyName';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Surnom', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'CommunityNickname';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de centre d''appels', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'CallCenterId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'UserByPassWorkflowRules', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'BypassWF__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'UserByPassValidationRules', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'BypassVR__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'UserByPassProcessBuilderCriteria', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'BypassPB__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'URL de la photo de bannière', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'BannerPhotoUrl';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Superposition du texte du badge de la photo de l''utilisateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'BadgeText';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Alias', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'Alias';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de la personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'AccountId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'À propos de moi', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'AboutMe';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID d''individu', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USER', @level2type = N'COLUMN', @level2name = N'IndividualId';
GO