﻿CREATE TABLE [dbo].[PV_SA_M_HIS_DEB] (
    [ANNEE]        INT           NOT NULL,
    [MOIS]         INT           NOT NULL,
    [DWHCPTCOM]    NVARCHAR (50) NOT NULL,
    [NB_JOUR]      INT           NOT NULL,
    [DAT_FIN_VALD] DATE          NULL,
    [DTE_ANALYSE]  DATE          NOT NULL,
    [DTE_UPDATE]   DATE          DEFAULT (getdate()) NULL
);

