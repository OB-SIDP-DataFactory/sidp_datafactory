﻿CREATE TABLE [dbo].[IWD_CTL_AUDIT_LOG_KEY] (
    [AUDIT_KEY] NUMERIC (19) IDENTITY (101, 100) NOT NULL,
    CONSTRAINT [PK_CTL_AUDIT_LOG_KEY] PRIMARY KEY NONCLUSTERED ([AUDIT_KEY] ASC)
);

