﻿CREATE TABLE [dbo].[REF_POST_DONN_ANLT_OPRT] (
    [COD_OPRT]                   VARCHAR (20) NULL,
    [TYP_TRNS_POST_DEBT]         VARCHAR (20) NULL,
    [TYP_TRNS_POST_FIN]          VARCHAR (20) NULL,
    [EVNM_DECL_POST_DEBT]        VARCHAR (20) NULL,
    [EVNM_DECL_POST_FIN]         VARCHAR (20) NULL,
    [COD_OPRT_CART_BL_POST_DEBT] VARCHAR (20) NULL,
    [COD_OPRT_CART_BL_POST_FIN]  VARCHAR (20) NULL,
    [TYP_CART_POST_DEBT]         VARCHAR (20) NULL,
    [TYP_CART_POST_FIN]          VARCHAR (20) NULL,
    [NATR_OPRT_POST_DEBT]        VARCHAR (20) NULL,
    [NATR_OPRT_POST_FIN]         VARCHAR (20) NULL,
    [RES_COMP_POST_DEBT]         VARCHAR (20) NULL,
    [RES_COMP_POST_FIN]          VARCHAR (20) NULL,
    [TYP_RES_POST_DEBT]          VARCHAR (20) NULL,
    [TYP_RES_POST_FIN]           VARCHAR (20) NULL,
    [COD_FOUR_OPRT_POST_DEBT]    VARCHAR (20) NULL,
    [COD_FOUR_OPRT_POST_FIN]     VARCHAR (20) NULL,
    [COD_COMM_POST_DEBT]         VARCHAR (20) NULL,
    [COD_COMM_POST_FIN]          VARCHAR (20) NULL,
    [DAT_DEBT_VALD]              VARCHAR (20) NULL
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de début de validité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'DAT_DEBT_VALD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Commission (Position Fin)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'COD_COMM_POST_FIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Commission (Position Début)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'COD_COMM_POST_DEBT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Fournisseur Opération (Position Fin)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'COD_FOUR_OPRT_POST_FIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Fournisseur Opération (Position Début)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'COD_FOUR_OPRT_POST_DEBT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type Réseau (Position Fin)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'TYP_RES_POST_FIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type Réseau (Position Début)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'TYP_RES_POST_DEBT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Réseau de compensation (Position Fin)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'RES_COMP_POST_FIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Réseau de compensation (Position Début)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'RES_COMP_POST_DEBT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nature Opération (Position Fin)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'NATR_OPRT_POST_FIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nature Opération (Position Début)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'NATR_OPRT_POST_DEBT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type Carte (Position Fin)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'TYP_CART_POST_FIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type Carte (Position Début)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'TYP_CART_POST_DEBT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Opération Carte Bleue (Position Fin)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'COD_OPRT_CART_BL_POST_FIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Opération Carte Bleue (Position Début)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'COD_OPRT_CART_BL_POST_DEBT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Evènement Déclencheur (Position Fin)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'EVNM_DECL_POST_FIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Evènement Déclencheur (Position Début)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'EVNM_DECL_POST_DEBT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type Transaction (Position Fin)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'TYP_TRNS_POST_FIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type Transaction (Position Début)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'TYP_TRNS_POST_DEBT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Opération', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_POST_DONN_ANLT_OPRT', @level2type = N'COLUMN', @level2name = N'COD_OPRT';

