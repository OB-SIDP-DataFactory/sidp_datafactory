﻿CREATE TABLE [dbo].[PV_SA_M_COTVM] (
    [ID]        BIGINT          IDENTITY (1, 1) NOT NULL,
    [DWHCOTIDV] INT             NULL,
    [DWHCOTDDV] DATE            NULL,
    [DWHCOTDAT] DATE            NULL,
    [DWHCOTDVC] VARCHAR (3)     NULL,
    [DWHCOTOUV] DECIMAL (18, 9) NULL,
    [DWHCOTHAU] DECIMAL (18, 9) NULL,
    [DWHCOTBAS] DECIMAL (18, 9) NULL,
    [DWHCOTCLO] DECIMAL (18, 9) NULL,
    [DWHCOTCCJ] DECIMAL (14, 9) NULL,
    [DWHCOTCC3] DECIMAL (14, 9) NULL,
    [DWHCOTDMJ] DATE            NULL,
    [DWHCOTMDF] VARCHAR (1)     NULL,
    [DWHCOTDDC] DATE            NULL,
    [DWHCOTPCT] INT             NULL,
    CONSTRAINT [PK_PV_SA_M_COTVM] PRIMARY KEY CLUSTERED ([ID] ASC)
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PLACE DE COTATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COTVM', @level2type = N'COLUMN', @level2name = N'DWHCOTPCT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DERNIERE COTAT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COTVM', @level2type = N'COLUMN', @level2name = N'DWHCOTDDC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TAG MODIFICATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COTVM', @level2type = N'COLUMN', @level2name = N'DWHCOTMDF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DT MAJ DU COURS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COTVM', @level2type = N'COLUMN', @level2name = N'DWHCOTDMJ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COUPON COURU J+3', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COTVM', @level2type = N'COLUMN', @level2name = N'DWHCOTCC3';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COUPON COURU J', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COTVM', @level2type = N'COLUMN', @level2name = N'DWHCOTCCJ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DER COURS/VL (CLÔT)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COTVM', @level2type = N'COLUMN', @level2name = N'DWHCOTCLO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DER COURS/VL (+BAS)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COTVM', @level2type = N'COLUMN', @level2name = N'DWHCOTBAS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DER COURS/VL (+HAU)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COTVM', @level2type = N'COLUMN', @level2name = N'DWHCOTHAU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DER COURS/VL (OUV)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COTVM', @level2type = N'COLUMN', @level2name = N'DWHCOTOUV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEVISE COTATION ISO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COTVM', @level2type = N'COLUMN', @level2name = N'DWHCOTDVC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DE COTATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COTVM', @level2type = N'COLUMN', @level2name = N'DWHCOTDAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DÉBUT VALIDITÉ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COTVM', @level2type = N'COLUMN', @level2name = N'DWHCOTDDV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID. VALEUR MOBILIERE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COTVM', @level2type = N'COLUMN', @level2name = N'DWHCOTIDV';

