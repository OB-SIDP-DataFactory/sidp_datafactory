﻿CREATE TABLE [dbo].[Data_Error] (
    [Id]                    BIGINT         IDENTITY (1, 1) NOT NULL,
    [ProcessingType]        NVARCHAR (50)  NOT NULL,
    [ErrorDate]             DATETIME       NULL,
    [ETLName]               NVARCHAR (200) NOT NULL,
    [FileOrTableName]       NVARCHAR (100) NOT NULL,
    [ErrorCode]             NVARCHAR (50)  NULL,
    [TechnicalErrorMessage] NVARCHAR (MAX) NULL,
    [AppErrorMessage]       NVARCHAR (MAX) NULL,
    [ColumnNameError]       NVARCHAR (400) NULL,
    [ColumnValueError]      NVARCHAR (50)  NULL,
    [IdColumnValueError]    NVARCHAR (50)  NULL,
    [IdLog]                 BIGINT         NULL,
    [ErrorLine]             NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Data_Error] PRIMARY KEY CLUSTERED ([Id] ASC)
)
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type de traitement : Collecte, Integration, Génération cubes, SAS, ...', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Data_Error', @level2type = N'COLUMN', @level2name = N'ProcessingType';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Name of the ETL processing', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Data_Error', @level2type = N'COLUMN', @level2name = N'ETLName';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Name of the file or table whose data are being processed', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Data_Error', @level2type = N'COLUMN', @level2name = N'FileOrTableName';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Application error message', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Data_Error', @level2type = N'COLUMN', @level2name = N'AppErrorMessage';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Column on which the error occurs', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Data_Error', @level2type = N'COLUMN', @level2name = N'ColumnNameError';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The value of the column which generates the error', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Data_Error', @level2type = N'COLUMN', @level2name = N'ColumnValueError';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The value of the Id column associated to the error column', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Data_Error', @level2type = N'COLUMN', @level2name = N'IdColumnValueError';
GO