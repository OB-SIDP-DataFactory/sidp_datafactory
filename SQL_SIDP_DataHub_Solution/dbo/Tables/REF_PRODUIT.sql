﻿CREATE TABLE [dbo].[REF_PRODUIT] (
	[code_produit_n1] [varchar](50) NULL,
	[libelle_produit_n1] [varchar](50) NULL,
	[code_produit_n2] [varchar](50) NULL,
	[libelle_produit_n2] [varchar](50) NULL,
	[code_produit_n3] [varchar](50) NULL,
	[libelle_produit_n3] [varchar](50) NULL,
	[code_produit_n4] [varchar](50) NULL,
	[libelle_produit_n4] [varchar](50) NULL,
	[code_produit_SIDP] [varchar](50) NULL,
	[libelle_code_produit_SIDP] [varchar](100) NULL,
	[code_regroupement_SIDP] [varchar](50) NULL,
	[date_debut] [date] NULL,
	[date_fin] [date] NULL,
	[code_produit_SAB] [varchar](50) NULL,
	[code_operation_SAB] [varchar](50) NULL,
	[code_nature_credit_SAB] [varchar](50) NULL,
	[code_rubrique_comptable_SAB] [varchar](50) NULL,
	[code_produit_CRM] [varchar](50) NULL,
	[code_grille_SAB] [varchar](50) NULL
);

