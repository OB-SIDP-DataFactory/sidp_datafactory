﻿CREATE TABLE [dbo].[REF_TRANSCO_ELANCIO] (
    [code_produit_n1]             VARCHAR (50) NULL,
    [libelle_produit_n1]          VARCHAR (50) NULL,
    [code_produit_SIDP]           VARCHAR (50) NULL,
    [libelle_produit_SIDP]        VARCHAR (50) NULL,
    [code_rubrique_comptable_SAB] VARCHAR (50) NULL,
    [code_grille_SAB]             VARCHAR (50) NULL,
    [annee_debut]                 INT          NULL,
    [duree]                       INT          NULL,
    [annee_echeance]              INT          NULL,
    [statut]                      VARCHAR (50) NULL
);

