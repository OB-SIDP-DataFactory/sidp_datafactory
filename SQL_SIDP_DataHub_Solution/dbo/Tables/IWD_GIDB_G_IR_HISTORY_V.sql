﻿CREATE TABLE [dbo].[IWD_GIDB_G_IR_HISTORY_V] (
    [IRHID]            NUMERIC (16)  NOT NULL,
    [IRID]             VARCHAR (50)  NOT NULL,
    [XSEQ]             NUMERIC (19)  NULL,
    [CHANGETYPE]       INT           NULL,
    [REFID]            VARCHAR (50)  NULL,
    [ADDED]            DATETIME      NOT NULL,
    [ADDED_TS]         INT           NULL,
    [GSYS_DOMAIN]      INT           NULL,
    [GSYS_SYS_ID]      INT           NULL,
    [GSYS_EXT_VCH1]    VARCHAR (255) NULL,
    [GSYS_EXT_VCH2]    VARCHAR (255) NULL,
    [GSYS_EXT_INT1]    INT           NULL,
    [GSYS_EXT_INT2]    INT           NULL,
    [CREATE_AUDIT_KEY] NUMERIC (19)  NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [IWD_I_G_GIR_H_V_CTS]
    ON [dbo].[IWD_GIDB_G_IR_HISTORY_V]([ADDED_TS] ASC);


GO
CREATE NONCLUSTERED INDEX [IWD_I_G_GIR_H_V_IRID]
    ON [dbo].[IWD_GIDB_G_IR_HISTORY_V]([IRID] ASC);

