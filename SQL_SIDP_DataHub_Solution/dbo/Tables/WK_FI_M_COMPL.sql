﻿CREATE TABLE [dbo].[WK_FI_M_COMPL] (
    [COD_ENR]             VARCHAR (2)    NULL,
    [COD_SEQ]             VARCHAR (6)    NULL,
    [NUMERO_ENREG]        VARCHAR (2)    NULL,
    [COD_BAN]             VARCHAR (5)    NULL,
    [COD_GUI_GES]         VARCHAR (5)    NULL,
    [COD_DOM_CPT]         VARCHAR (16)   NULL,
    [NUM_PRE]             VARCHAR (11)   NULL,
    [DTE_OUV_CPT]         DATE           NULL,
    [DEV_CPT]             VARCHAR (3)    NULL,
    [NB_DEC_DEV_CPT]      INT            NULL,
    [MTT_CRE_MAX_AUT]     DECIMAL (9, 2) NULL,
    [MTT_CRE_MAX_AUT_VIR] DECIMAL (9, 2) NULL,
    [MTT_CRE_DIS]         DECIMAL (9, 2) NULL,
    [MTT_CRE_UTI]         DECIMAL (9, 2) NULL,
    [MTT_CRE_DIS_VIR]     DECIMAL (9, 2) NULL,
    [ENC_MOY_MOI]         DECIMAL (9, 2) NULL,
    [MTT_ECH_MOI]         DECIMAL (9, 2) NULL,
    [TX_MOY]              VARCHAR (5)    NULL,
    [COD_STA_SUI_DOS]     VARCHAR (2)    NULL,
    [NB_IMP]              INT            NULL,
    [DTE_FER_CPT]         DATE           NULL,
    [MTT_FRA]             DECIMAL (9, 2) NULL,
    [NB_DEM_VIR]          INT            NULL,
    [MTT_DEM_VIR]         DECIMAL (9, 2) NULL,
    [NB_RET_GUI]          INT            NULL,
    [MTT_RET_GUI]         DECIMAL (9, 2) NULL,
    [NB_REM_ANT]          INT            NULL,
    [MTT_REM_ANT]         DECIMAL (9, 2) NULL,
    [MTT_COM]             DECIMAL (9, 2) NULL,
    [IDE_GRC]             VARCHAR (8)    NULL,
    [DATTTP]              DATE           NULL,
    [PHA_PRO_REC]         VARCHAR (1)    NULL,
    [MTT_IMP]             DECIMAL (9, 2) NULL,
    [NB_IMP_NOU_GES]      INT            NULL,
    [DTE_PRE_IMP]         DATE           NULL
);

