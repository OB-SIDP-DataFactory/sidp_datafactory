﻿CREATE TABLE [dbo].[WK_SF_RECORDTYPE] (
    [Id_SF]             NVARCHAR (18)   NOT NULL,
    [SobjectType]       NVARCHAR (255)  NULL,
    [DeveloperName]     NVARCHAR (255)  NULL,
    [Name]              NVARCHAR (255)  NULL,
    [Description]       NVARCHAR (1000) NULL,
    [NamespacePrefix]   NVARCHAR (255)  NULL,
    [IsActive]          BIT             NULL,
    [IsPersonType]      BIT             NULL,
    [BusinessProcessId] NVARCHAR (18)   NULL,
    [CreatedById]       NVARCHAR (18)   NULL,
    [CreatedDate]       DATETIME        NULL,
    [LastModifiedById]  NVARCHAR (18)   NULL,
    [LastModifiedDate]  DATETIME        NULL,
    [SystemModstamp]    DATETIME        NULL,
    CONSTRAINT [PK_WK_SF_RECORDTYPE] PRIMARY KEY CLUSTERED ([Id_SF] ASC)
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Horodateur des modifications du système', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_RECORDTYPE', @level2type = N'COLUMN', @level2name = N'SystemModstamp';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom du type d''objet', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_RECORDTYPE', @level2type = N'COLUMN', @level2name = N'SobjectType';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Préfixe d''espace de noms', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_RECORDTYPE', @level2type = N'COLUMN', @level2name = N'NamespacePrefix';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_RECORDTYPE', @level2type = N'COLUMN', @level2name = N'Name';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_RECORDTYPE', @level2type = N'COLUMN', @level2name = N'LastModifiedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernière modification par ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_RECORDTYPE', @level2type = N'COLUMN', @level2name = N'LastModifiedById';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Est un type de personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_RECORDTYPE', @level2type = N'COLUMN', @level2name = N'IsPersonType';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_RECORDTYPE', @level2type = N'COLUMN', @level2name = N'IsActive';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID du type d''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_RECORDTYPE', @level2type = N'COLUMN', @level2name = N'Id_SF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom du type d''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_RECORDTYPE', @level2type = N'COLUMN', @level2name = N'DeveloperName';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Description', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_RECORDTYPE', @level2type = N'COLUMN', @level2name = N'Description';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_RECORDTYPE', @level2type = N'COLUMN', @level2name = N'CreatedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID créé par', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_RECORDTYPE', @level2type = N'COLUMN', @level2name = N'CreatedById';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID du processus commercial', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_RECORDTYPE', @level2type = N'COLUMN', @level2name = N'BusinessProcessId';

