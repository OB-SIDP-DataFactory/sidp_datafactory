﻿CREATE TABLE [dbo].[Param_TABLE_STRUCT] (
    [source_table]       VARCHAR (100) NULL,
    [source_column_name] VARCHAR (100) NULL,
    [source_data_type]   VARCHAR (100) NULL,
    [source_max_len]     INT           NULL,
    [cible_table_name]   VARCHAR (100) NULL,
    [cible_column_name]  VARCHAR (100) NULL,
    [cible_data_type]    VARCHAR (100) NULL,
    [cible_max_len]      INT           NULL,
    [flag_Identity]      INT           NULL,
    [key_column]         INT           NULL,
    [cible_scale]        INT           NULL
);

