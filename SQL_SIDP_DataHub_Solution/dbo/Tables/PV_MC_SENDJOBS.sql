﻿CREATE TABLE [dbo].[PV_MC_SENDJOBS] (
    [Id]                        BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [ClientID]                  INT                                         NULL,
    [SendID]                    INT                                         NOT NULL,
    [FromName]                  NVARCHAR (130)                              NULL,
    [FromEmail]                 NVARCHAR (100)                              NULL,
    [SchedTime]                 DATETIME                                    NULL,
    [SentTime]                  DATETIME                                    NULL,
    [Subject]                   NVARCHAR (200)                              NULL,
    [EmailName]                 NVARCHAR (100)                              NULL,
    [TriggeredSendExternalKey]  NVARCHAR (100)                              NULL,
    [SendDefinitionExternalKey] NVARCHAR (100)                              NULL,
    [JobStatus]                 NVARCHAR (30)                               NULL,
    [PreviewURL]                NVARCHAR (MAX)                              NULL,
    [IsMultipart]               NVARCHAR (5)                                NULL,
    [Additional]                NVARCHAR (MAX)                              NULL,
    [Startdt]                   DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]                     DATETIME2 (7) GENERATED ALWAYS AS ROW END   NOT NULL,
    CONSTRAINT [PK_PV_MC_SENDJOBS] PRIMARY KEY CLUSTERED ([Id] ASC, [SendID] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_MC_SENDJOBSHistory], DATA_CONSISTENCY_CHECK=ON));

