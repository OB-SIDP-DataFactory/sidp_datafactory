﻿CREATE TABLE [dbo].[REF_DISTRIBUTION_NETWORK] (
    [ID]                      INT            IDENTITY (1, 1) NOT NULL,
    [ALIAS_CODE]              NVARCHAR (255) NULL,
    [SALESFORCE_CODE]         NVARCHAR (255) NULL,
    [ID_DISTRIBUTION_NETWORK] DECIMAL (19)   NULL,
    [FRANFINANCE_CODE]        NVARCHAR (255) NULL,
    [CORE_BANKING_CODE]       NVARCHAR (255) NULL,
    CONSTRAINT [PK_REF_DISTRIBUTION_NETWORK] PRIMARY KEY CLUSTERED ([ID] ASC)
)
GO