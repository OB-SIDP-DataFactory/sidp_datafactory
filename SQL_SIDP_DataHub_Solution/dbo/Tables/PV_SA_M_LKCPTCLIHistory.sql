﻿CREATE TABLE [dbo].[PV_SA_M_LKCPTCLIHistory] (
    [Id]                 BIGINT        NOT NULL,
    [DWHGRPDTX]          DATE          NULL,
    [DWHGRPETB]          INT           NULL,
    [DWHGRPCLI]          VARCHAR (7)   NULL,
    [DWHGRPREG]          VARCHAR (7)   NULL,
    [DWHGRPREL]          VARCHAR (3)   NULL,
    [DWHGRPTGR]          VARCHAR (1)   NULL,
    [DWHGRPLGR]          VARCHAR (1)   NULL,
    [DWHGRPRIS]          VARCHAR (1)   NULL,
    [DWHGRPFRE]          VARCHAR (1)   NULL,
    [DWHGRPDSY]          DATE          NULL,
    [Validity_StartDate] DATETIME      NULL,
    [Validity_EndDate]   DATETIME      NULL,
    [Startdt]            DATETIME2 (7) NOT NULL,
    [Enddt]              DATETIME2 (7) NOT NULL
);
GO
CREATE CLUSTERED INDEX [ix_PV_SA_M_LKCPTCLIHistory]
    ON [dbo].[PV_SA_M_LKCPTCLIHistory]([Validity_EndDate] ASC, [Validity_StartDate] ASC);

