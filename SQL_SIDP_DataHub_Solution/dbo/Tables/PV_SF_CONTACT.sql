﻿CREATE TABLE [dbo].[PV_SF_CONTACT] (
    [Id]                                  INT                                         IDENTITY (1, 1) NOT NULL,
    [Id_SF]                               NVARCHAR (18)                               NOT NULL,
    [IsDeleted]                           NVARCHAR (10)                               NULL,
    [MasterRecordId]                      NVARCHAR (18)                               NULL,
    [AccountId]                           NVARCHAR (18)                               NULL,
    [IsPersonAccount]                     NVARCHAR (10)                               NULL,
    [LastName]                            NVARCHAR (80)                               NULL,
    [FirstName]                           NVARCHAR (40)                               NULL,
    [Salutation]                          NVARCHAR (40)                               NULL,
    [Name]                                NVARCHAR (121)                              NULL,
    [RecordTypeId]                        NVARCHAR (18)                               NULL,
    [OtherStreet]                         NVARCHAR (255)                              NULL,
    [OtherCity]                           NVARCHAR (40)                               NULL,
    [OtherState]                          NVARCHAR (80)                               NULL,
    [OtherPostalCode]                     NVARCHAR (20)                               NULL,
    [OtherCountry]                        NVARCHAR (80)                               NULL,
    [OtherStateCode]                      NVARCHAR (10)                               NULL,
    [OtherCountryCode]                    NVARCHAR (10)                               NULL,
    [OtherLatitude]                       DECIMAL (18, 15)                            NULL,
    [OtherLongitude]                      DECIMAL (18, 15)                            NULL,
    [OtherGeocodeAccuracy]                NVARCHAR (40)                               NULL,
    [MailingStreet]                       NVARCHAR (255)                              NULL,
    [MailingCity]                         NVARCHAR (40)                               NULL,
    [MailingState]                        NVARCHAR (80)                               NULL,
    [MailingPostalCode]                   NVARCHAR (20)                               NULL,
    [MailingCountry]                      NVARCHAR (80)                               NULL,
    [MailingStateCode]                    NVARCHAR (10)                               NULL,
    [MailingCountryCode]                  NVARCHAR (10)                               NULL,
    [MailingLatitude]                     DECIMAL (18, 15)                            NULL,
    [MailingLongitude]                    DECIMAL (18, 15)                            NULL,
    [MailingGeocodeAccuracy]              NVARCHAR (40)                               NULL,
    [Phone]                               NVARCHAR (40)                               NULL,
    [Fax]                                 NVARCHAR (40)                               NULL,
    [MobilePhone]                         NVARCHAR (40)                               NULL,
    [HomePhone]                           NVARCHAR (40)                               NULL,
    [OtherPhone]                          NVARCHAR (40)                               NULL,
    [AssistantPhone]                      NVARCHAR (40)                               NULL,
    [ReportsToId]                         NVARCHAR (18)                               NULL,
    [Email]                               NVARCHAR (80)                               NULL,
    [Title]                               NVARCHAR (128)                              NULL,
    [Department]                          NVARCHAR (80)                               NULL,
    [AssistantName]                       NVARCHAR (40)                               NULL,
    [LeadSource]                          NVARCHAR (40)                               NULL,
    [Birthdate]                           DATETIME                                    NULL,
    [OwnerId]                             NVARCHAR (18)                               NULL,
    [HasOptedOutOfEmail]                  NVARCHAR (10)                               NULL,
    [HasOptedOutOfFax]                    NVARCHAR (10)                               NULL,
    [DoNotCall]                           NVARCHAR (10)                               NULL,
    [CanAllowPortalSelfReg]               NVARCHAR (10)                               NULL,
    [CreatedDate]                         DATETIME                                    NULL,
    [CreatedById]                         NVARCHAR (18)                               NULL,
    [LastModifiedDate]                    DATETIME                                    NULL,
    [LastModifiedById]                    NVARCHAR (18)                               NULL,
    [SystemModstamp]                      DATETIME                                    NULL,
    [LastActivityDate]                    DATETIME                                    NULL,
    [LastCURequestDate]                   DATETIME                                    NULL,
    [LastCUUpdateDate]                    DATETIME                                    NULL,
    [LastViewedDate]                      DATETIME                                    NULL,
    [LastReferencedDate]                  DATETIME                                    NULL,
    [EmailBouncedReason]                  NVARCHAR (255)                              NULL,
    [EmailBouncedDate]                    DATETIME                                    NULL,
    [IsEmailBounced]                      NVARCHAR (10)                               NULL,
    [PhotoUrl]                            NVARCHAR (255)                              NULL,
    [Jigsaw]                              NVARCHAR (20)                               NULL,
    [JigsawContactId]                     NVARCHAR (20)                               NULL,
    [IndividualId]                        NVARCHAR (20)                               NULL,
    [AntenuptialSettlement__c]            NVARCHAR (255)                              NULL,
    [BirthCity__c]                        NVARCHAR (255)                              NULL,
    [BirthCountry__c]                     NVARCHAR (255)                              NULL,
    [BirthDepartment__c]                  NVARCHAR (80)                               NULL,
    [CreateIDCustomer__c]                 NVARCHAR (10)                               NULL,
    [ClientProfileLastModif__c]           DATETIME                                    NULL,
    [DeathDate__c]                        DATETIME                                    NULL,
    [DependentChildrenNb__c]              DECIMAL (2)                                 NULL,
    [TCHSalutation__c]                    NVARCHAR (1300)                             NULL,
    [EmploymentContractEndDate__c]        DATETIME                                    NULL,
    [EmploymentContractStartDate__c]      DATETIME                                    NULL,
    [EmploymentContractType__c]           NVARCHAR (255)                              NULL,
    [AcceptationMembershipDate__c]        DATETIME                                    NULL,
    [JustifiedIncomes__c]                 DECIMAL (18, 2)                             NULL,
    [IDCustomerSAB__c]                    NVARCHAR (15)                               NULL,
    [IDCustomer__c]                       NVARCHAR (11)                               NULL,
    [IncomeGatheredOn__c]                 DATETIME                                    NULL,
    [InvestorProfile__c]                  NVARCHAR (255)                              NULL,
    [MaidenName__c]                       NVARCHAR (80)                               NULL,
    [MaritalStatus__c]                    NVARCHAR (255)                              NULL,
    [MonthExpenses__c]                    DECIMAL (18, 2)                             NULL,
    [MonthFoncIncome__c]                  DECIMAL (18, 2)                             NULL,
    [MonthMobIncome__c]                   DECIMAL (18, 2)                             NULL,
    [MonthNetIncome__c]                   DECIMAL (18, 2)                             NULL,
    [MonthOtherExpenses__c]               DECIMAL (18, 2)                             NULL,
    [MonthOtherIncomes__c]                DECIMAL (18, 2)                             NULL,
    [MonthOtherLoans__c]                  DECIMAL (18, 2)                             NULL,
    [MonthPaidPension__c]                 DECIMAL (18, 2)                             NULL,
    [MonthReceivPension__c]               DECIMAL (18, 2)                             NULL,
    [MonthSocBenefits__c]                 DECIMAL (18, 2)                             NULL,
    [MonthTaxes__c]                       DECIMAL (18, 2)                             NULL,
    [Nationality__c]                      NVARCHAR (255)                              NULL,
    [OccupationNiv1__c]                   NVARCHAR (255)                              NULL,
    [OccupationNiv3__c]                   NVARCHAR (255)                              NULL,
    [Occupation__c]                       NVARCHAR (255)                              NULL,
    [PersonalProperty__c]                 DECIMAL (18, 2)                             NULL,
    [PhoneticFirstname__c]                NVARCHAR (255)                              NULL,
    [PhoneticLastName__c]                 NVARCHAR (255)                              NULL,
    [PhoneticMaidenName__c]               NVARCHAR (255)                              NULL,
    [Person__c]                           NVARCHAR (18)                               NULL,
    [PreferredPhoneNumber__c]             NVARCHAR (255)                              NULL,
    [PrimaryResidOccupationType__c]       NVARCHAR (255)                              NULL,
    [PrimaryResidenceEntryDate__c]        DATETIME                                    NULL,
    [RealEstate__c]                       DECIMAL (18, 2)                             NULL,
    [RightOpposition__c]                  NVARCHAR (255)                              NULL,
    [TotalDisposableIncome__c]            DECIMAL (18, 2)                             NULL,
    [TotalExpenses__c]                    DECIMAL (18, 2)                             NULL,
    [TotalIncome__c]                      DECIMAL (18, 2)                             NULL,
    [USNationality__c]                    NVARCHAR (255)                              NULL,
    [UndeliverabilityReason__c]           NVARCHAR (255)                              NULL,
    [WealthTax__c]                        NVARCHAR (255)                              NULL,
    [ActivityBranch__c]                   NVARCHAR (255)                              NULL,
    [BirthPostalCode__c]                  NVARCHAR (5)                                NULL,
    [EmployeeType__c]                     NVARCHAR (255)                              NULL,
    [InvalideAdr__c]                      NVARCHAR (10)                               NULL,
    [LegalCapacity__c]                    NVARCHAR (255)                              NULL,
    [NoCall__c]                           NVARCHAR (10)                               NULL,
    [NoLetter__c]                         NVARCHAR (10)                               NULL,
    [NoOrangeEmail__c]                    NVARCHAR (10)                               NULL,
    [NoOrangeSMS__c]                      NVARCHAR (10)                               NULL,
    [NoPartnerEmail__c]                   NVARCHAR (10)                               NULL,
    [NoPartnerSMS__c]                     NVARCHAR (10)                               NULL,
    [PhoneEligibility__c]                 NVARCHAR (255)                              NULL,
    [RunUnicityCheck__c]                  NVARCHAR (10)                               NULL,
    [AdditionToAddress__c]                NVARCHAR (255)                              NULL,
    [VIPParnasseCustomer__c]              NVARCHAR (255)                              NULL,
    [FacebookAlias__c]                    NVARCHAR (80)                               NULL,
    [SecondEmail__c]                      NVARCHAR (80)                               NULL,
    [SellerHashedCuid__c]                 NVARCHAR (64)                               NULL,
    [TwitterAlias__c]                     NVARCHAR (80)                               NULL,
    [USIndicator__c]                      NVARCHAR (10)                               NULL,
    [ApplicationMembershipDate__c]        DATETIME                                    NULL,
    [BIC__c]                              NVARCHAR (11)                               NULL,
    [CreditInProgressExceptOB__c]         DECIMAL (18, 2)                             NULL,
    [DistributorNetwork__c]               NVARCHAR (4000)                             NULL,
    [FamilyAllocation__c]                 DECIMAL (18)                                NULL,
    [HousingAllocation__c]                DECIMAL (18)                                NULL,
    [IBAN__c]                             NVARCHAR (34)                               NULL,
    [IDCustomerCID__c]                    NVARCHAR (15)                               NULL,
    [IsAMF__c]                            NVARCHAR (10)                               NULL,
    [IsContactAccountSync__c]             NVARCHAR (10)                               NULL,
    [IsIOBSP__c]                          NVARCHAR (255)                              NULL,
    [IsTA3C__c]                           NVARCHAR (10)                               NULL,
    [JustifiedIncomesData__c]             NVARCHAR (90)                               NULL,
    [Manager__c]                          NVARCHAR (255)                              NULL,
    [Preattribution__c]                   DECIMAL (4)                                 NULL,
    [Prescripteur__c]                     NVARCHAR (255)                              NULL,
    [RCSNumber__c]                        NVARCHAR (255)                              NULL,
    [TaxResidenceInFrance__c]             NVARCHAR (10)                               NULL,
    [FiscalCountry__c]                    NVARCHAR (10)                               NULL,
    [GeolifeSegment__c]                   NVARCHAR (100)                              NULL,
    [TCHId__c]                            NVARCHAR (15)                               NULL,
    [Description]                         NVARCHAR (MAX)                              NULL,
    [NoPersoAlerts__c]                    NVARCHAR (10)                               NULL,
    [et4ae5__HasOptedOutOfMobile__c]      NVARCHAR (10)                               NULL,
    [et4ae5__Mobile_Country_Code__c]      NVARCHAR (255)                              NULL,
    [DoubleNationality__c]                NVARCHAR (255)                              NULL,
    [ProductsNoEmail__c]                  NVARCHAR (4000)                             NULL,
    [SocialReason__c]                     NVARCHAR (80)                               NULL,
    [Customer_premium__c]                 NVARCHAR (10)                               NULL,
    [LastModificationJustifiedIncomes__c] DATETIME                                    NULL,
    [BusinessPhone__c]                    NVARCHAR (40)                               NULL,
    [CodeNAFRev2__c]                      NVARCHAR (4)                                NULL,
    [Interlocuteur__c]                    NVARCHAR (80)                               NULL,
    [LabelNAFRev2__c]                     NVARCHAR (80)                               NULL,
    [MainCommercial__c]                   NVARCHAR (80)                               NULL,
    [NumSiret__c]                         NVARCHAR (14)                               NULL,
    [ShopNetworkMesh__c]                  NVARCHAR (255)                              NULL,
    [Turnover__c]                         MONEY                                       NULL,
    [agenceCommerciale__c]                NVARCHAR (255)                              NULL,
    [exerciceReference__c]                DATETIME                                    NULL,
    [fonctionInterlocuteur__c]            NVARCHAR (80)                               NULL,
    [nomPersonne__c]                      NVARCHAR (80)                               NULL,
    [KYCStatus__c]                        NVARCHAR (255)                              NULL,
    [KYCValidationDate__c]                DATETIME                                    NULL,
    [Tech_Temp_Habilitation_IOBSP__c]     NVARCHAR (10)                               NULL,
    [FlowAction__c]                       NVARCHAR (255)                              NULL,
    [KiosqueRegistrationDate__c]          DATETIME                                    NULL,
    [MobilityDate__c]                     DATETIME                                    NULL,
    [NoAccountBalanceAlerts__c]           NVARCHAR (10)                               NULL,
    [NoPaymentAlerts__c]                  NVARCHAR (10)                               NULL,
    [DigitalScore__c]                     DECIMAL (4)                                 NULL,
    [OFFirstContactDate__c]               DATETIME                                    NULL,
    [Validity_StartDate]                  DATETIME2 (7)                               NOT NULL,
    [Validity_EndDate]                    DATETIME2 (7)                               DEFAULT ('99991231') NOT NULL,
    [Startdt]                             DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]                               DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SF_CONTACT] PRIMARY KEY CLUSTERED ([Id] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SF_CONTACTHistory], DATA_CONSISTENCY_CHECK=ON))
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant du conseiller', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'SellerHashedCuid__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Téléphone (bureau)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'Phone'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom complet', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'Name'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'LastModifiedDate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID du contact', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'Id_SF'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Adresse e-mail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'Email'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'CreatedDate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ISF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'WealthTax__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Client VIP/First/Parnasse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'VIPParnasseCustomer__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nationalité américaine', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'USNationality__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Indice d''américanité (FATCA)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'USIndicator__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motifs retours courriers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'UndeliverabilityReason__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Alias Twitter', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'TwitterAlias__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Total revenus', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'TotalIncome__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Total charges', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'TotalExpenses__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Total revenus disponibles', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'TotalDisposableIncome__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fonction', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'Title'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Salutation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'TCHSalutation__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Résidence fiscale en France', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'TaxResidenceInFrance__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Horodateur des modifications du système', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'SystemModstamp'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Adresse email 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'SecondEmail__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Civilité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'Salutation'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RunUnicityCheck', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'RunUnicityCheck__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Droit d''opposition', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'RightOpposition__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID du responsable hiérarchique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'ReportsToId'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID du type d''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'RecordTypeId'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Patrimoine immobilier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'RealEstate__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro RCS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'RCSNumber__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type d''occupation du logement principal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'PrimaryResidOccupationType__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date d''entrée dans résidence principale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'PrimaryResidenceEntryDate__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prescripteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'Prescripteur__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tél. favoris', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'PreferredPhoneNumber__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Pré-attribution', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'Preattribution__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'URL de la photo', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'PhotoUrl'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom de naissance phonétique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'PhoneticMaidenName__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom phonétique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'PhoneticLastName__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prénom phonétique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'PhoneticFirstname__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Éligibilité du mobile', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'PhoneEligibility__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Patrimoine mobilier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'PersonalProperty__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'Person__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID du propriétaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'OwnerId'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autre rue', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'OtherStreet'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autre code de région/province', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'OtherStateCode'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autre région/province', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'OtherState'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autre code postal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'OtherPostalCode'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autre téléphone', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'OtherPhone'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autre longitude', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'OtherLongitude'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autre latitude', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'OtherLatitude'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Other Geocode Accuracy', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'OtherGeocodeAccuracy'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autre code de pays', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'OtherCountryCode'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autre pays', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'OtherCountry'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autre ville', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'OtherCity'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Profession PCS secondaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'OccupationNiv3__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Profession PCS principale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'OccupationNiv1__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Profession', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'Occupation__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus SMS Partenaires', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'NoPartnerSMS__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus E-mail partenaires', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'NoPartnerEmail__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus SMS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'NoOrangeSMS__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus E-mail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'NoOrangeEmail__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus contact courrier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'NoLetter__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus appels téléphoniques', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'NoCall__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nationalité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'Nationality__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Impôts mensuels', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'MonthTaxes__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prestations sociales mensuelles', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'MonthSocBenefits__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Pensions alimentaires mensuelles reçues', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'MonthReceivPension__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Pensions alimentaires mensuelles versées', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'MonthPaidPension__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mensualités des autres crédits en cours', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'MonthOtherLoans__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autres revenus mensuels', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'MonthOtherIncomes__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autres charges mensuelles', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'MonthOtherExpenses__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Revenus mensuels nets', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'MonthNetIncome__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Revenus mobiliers mensuels', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'MonthMobIncome__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Revenus fonciers mensuels', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'MonthFoncIncome__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Charges mensuelles', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'MonthExpenses__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mobile', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'MobilePhone'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID d''enregistrement principal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'MasterRecordId'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Situation familiale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'MaritalStatus__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Gérant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'Manager__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Rue', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'MailingStreet'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code de région/province de publipostage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'MailingStateCode'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Région/Province', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'MailingState'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code postal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'MailingPostalCode'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Longitude de publipostage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'MailingLongitude'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Latitude de publipostage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'MailingLatitude'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mailing Geocode Accuracy', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'MailingGeocodeAccuracy'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code de pays de publipostage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'MailingCountryCode'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Pays', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'MailingCountry'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ville', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'MailingCity'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom de naissance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'MaidenName__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Capacité juridique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'LegalCapacity__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Canal d''interaction', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'LeadSource'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernier affichage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'LastViewedDate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernière date référencée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'LastReferencedDate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom d''usage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'LastName'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernière modification par ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'LastModifiedById'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date du dernier enregistrement Rester en contact', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'LastCUUpdateDate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière requête Rester en contact', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'LastCURequestDate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernière activité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'LastActivityDate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Données Revenus Justifiés', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'JustifiedIncomesData__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Revenus justifiés', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'JustifiedIncomes__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Jigsaw Contact ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'JigsawContactId'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID d''individu', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'IndividualId'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Clé Data.com', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'Jigsaw'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Habilitation TA3C', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'IsTA3C__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Est une personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'IsPersonAccount'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Habilitation IOBSP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'IsIOBSP__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Est un e-mail renvoyé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'IsEmailBounced'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Supprimé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'IsDeleted'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IsContactAccountSync', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'IsContactAccountSync__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Certification AMF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'IsAMF__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Profil investisseur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'InvestorProfile__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'*Invalide adresse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'InvalideAdr__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Revenus collectés le', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'IncomeGatheredOn__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID Client SAB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'IDCustomerSAB__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° Client Distributeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'IDCustomerCID__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'IDCustomer__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IBAN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'IBAN__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Allocation logement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'HousingAllocation__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Domicile', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'HomePhone'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Désinscription de télécopie', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'HasOptedOutOfFax'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Désinscription d''adresse e-mail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'HasOptedOutOfEmail'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prénom', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'FirstName'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Télécopie (bureau)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'Fax'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Allocations familiales', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'FamilyAllocation__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Alias Facebook', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'FacebookAlias__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type de contrat de travail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'EmploymentContractType__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de début du contrat de travail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'EmploymentContractStartDate__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de fin du contrat de travail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'EmploymentContractEndDate__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Qualification employé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'EmployeeType__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Motif de renvoi d''e-mail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'EmailBouncedReason'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de renvoi d''e-mail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'EmailBouncedDate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ne pas appeler', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'DoNotCall'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Réseaux distributeurs', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'DistributorNetwork__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre d''enfants à charge', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'DependentChildrenNb__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Service', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'Department'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de décès', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'DeathDate__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Crédit en cours hors Orange Bank', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'CreditInProgressExceptOB__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CreateIDCustomer', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'CreateIDCustomer__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID créé par', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'CreatedById'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Profil investisseur mis à jour le', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'ClientProfileLastModif__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Autoriser l''auto-inscription au niveau du portail Client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'CanAllowPortalSelfReg'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code postal de naissance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'BirthPostalCode__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Département de naissance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'BirthDepartment__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Né(e) le', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'Birthdate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Pays de naissance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'BirthCountry__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Commune de naissance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'BirthCity__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'BIC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'BIC__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Asst. Téléphone', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'AssistantPhone'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom de l''assistant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'AssistantName'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date demande adhésion', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'ApplicationMembershipDate__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Régime matrimonial', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'AntenuptialSettlement__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Complément d''adresse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'AdditionToAddress__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Secteur d''activité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'ActivityBranch__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom de la personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'AccountId'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date confirmation adhésion', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'AcceptationMembershipDate__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Chiffre d''affaires', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'Turnover__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Raison Sociale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'SocialReason__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Sous-réseau', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'ShopNetworkMesh__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus E-mail produits', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'ProductsNoEmail__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° SIRET', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'NumSiret__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus recommandation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'NoPersoAlerts__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom de la personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'nomPersonne__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Commercial principal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'MainCommercial__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernière modification du revenu justifié', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'LastModificationJustifiedIncomes__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé NAF Rev2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'LabelNAFRev2__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Interlocuteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'Interlocuteur__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fonction de l''interlocuteur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'fonctionInterlocuteur__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Exercice de référence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'exerciceReference__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code national mobile', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'et4ae5__Mobile_Country_Code__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Désinscription mobile', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'et4ae5__HasOptedOutOfMobile__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Double nationalité américaine', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'DoubleNationality__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Description du contact', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'Description'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Client Premium', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'Customer_premium__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code NAF Rev2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'CodeNAFRev2__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Téléphone professionnel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'BusinessPhone__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Agence commerciale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'agenceCommerciale__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de validation du KYC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'KYCValidationDate__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Statut KYC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'KYCStatus__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tech Temp Habilitation IOBSP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'Tech_Temp_Habilitation_IOBSP__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus notification paiement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'NoPaymentAlerts__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Refus notifications solde', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'NoAccountBalanceAlerts__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de mobilité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'MobilityDate__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date d''inscription Kiosque', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'KiosqueRegistrationDate__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Action Flux', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'FlowAction__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Score de confiance sur Identité digitale', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'DigitalScore__c'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date d''entrée en relation avec OF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_CONTACT', @level2type = N'COLUMN', @level2name = N'OFFirstContactDate__c'
GO
CREATE NONCLUSTERED INDEX IX_NC_SF_CON_REC_TYP
ON [dbo].[PV_SF_CONTACT] ([RecordTypeId])
INCLUDE ([Name],[SellerHashedCuid__c])
GO
CREATE NONCLUSTERED INDEX [IX_NC_PV_SF_CON_SEL_HSH]
ON [dbo].[PV_SF_CONTACT] ([SellerHashedCuid__c])
INCLUDE ([Id_SF],[Name],[Phone],[Email],[CreatedDate],[LastModifiedDate])
GO