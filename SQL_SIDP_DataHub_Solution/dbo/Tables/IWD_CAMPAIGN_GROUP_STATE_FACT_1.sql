﻿CREATE TABLE [dbo].[IWD_CAMPAIGN_GROUP_STATE_FACT] (
    [CAMP_GROUP_STATE_FACT_KEY]    NUMERIC (19) NOT NULL,
    [TENANT_KEY]                   INT          NOT NULL,
    [CAMPAIGN_KEY]                 INT          NOT NULL,
    [GROUP_KEY]                    INT          NOT NULL,
    [CAMPAIGN_GROUP_STATE_KEY]     INT          NOT NULL,
    [CAMP_GROUP_SESS_FACT_SDT_KEY] INT          NULL,
    [CAMP_GROUP_SESSION_FACT_KEY]  NUMERIC (19) NULL,
    [START_DATE_TIME_KEY]          INT          NOT NULL,
    [END_DATE_TIME_KEY]            INT          NOT NULL,
    [CREATE_AUDIT_KEY]             NUMERIC (19) NOT NULL,
    [UPDATE_AUDIT_KEY]             NUMERIC (19) NOT NULL,
    [START_TS]                     INT          NULL,
    [END_TS]                       INT          NULL,
    [TOTAL_DURATION]               INT          NULL,
    [CAMPAIGN_GROUP_SESSION_ID]    VARCHAR (64) NULL,
    [ACTIVE_FLAG]                  NUMERIC (1)  NULL,
    [PURGE_FLAG]                   NUMERIC (1)  NULL,
    CONSTRAINT [PK_CMPGN_GR_ST_FT] PRIMARY KEY CLUSTERED ([CAMP_GROUP_STATE_FACT_KEY] ASC)
);
GO
