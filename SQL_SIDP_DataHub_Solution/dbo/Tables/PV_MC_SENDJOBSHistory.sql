﻿CREATE TABLE [dbo].[PV_MC_SENDJOBSHistory] (
    [Id]                        BIGINT         NOT NULL,
    [ClientID]                  INT            NULL,
    [SendID]                    INT            NOT NULL,
    [FromName]                  NVARCHAR (130) NULL,
    [FromEmail]                 NVARCHAR (100) NULL,
    [SchedTime]                 DATETIME       NULL,
    [SentTime]                  DATETIME       NULL,
    [Subject]                   NVARCHAR (200) NULL,
    [EmailName]                 NVARCHAR (100) NULL,
    [TriggeredSendExternalKey]  NVARCHAR (100) NULL,
    [SendDefinitionExternalKey] NVARCHAR (100) NULL,
    [JobStatus]                 NVARCHAR (30)  NULL,
    [PreviewURL]                NVARCHAR (MAX) NULL,
    [IsMultipart]               NVARCHAR (5)   NULL,
    [Additional]                NVARCHAR (MAX) NULL,
    [Startdt]                   DATETIME2 (7)  NOT NULL,
    [Enddt]                     DATETIME2 (7)  NOT NULL
);
GO
CREATE CLUSTERED INDEX [ix_PV_MC_SENDJOBSHistory]
    ON [dbo].[PV_MC_SENDJOBSHistory]([Enddt] ASC, [Startdt] ASC) WITH (DATA_COMPRESSION = PAGE);

