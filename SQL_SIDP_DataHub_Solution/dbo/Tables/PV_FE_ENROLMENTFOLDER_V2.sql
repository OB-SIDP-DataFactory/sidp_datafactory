﻿CREATE TABLE [dbo].[PV_FE_ENROLMENTFOLDER_V2] (
    [Id]                             BIGINT          IDENTITY (1, 1) NOT NULL,
    [ID_FE_ENROLMENT_FOLDER]         DECIMAL (19)    NOT NULL,
    [CREATION_DATE]                  DATETIME2 (6)   NOT NULL,
    [CREATION_USER]                  NVARCHAR (255)  NULL,
    [LAST_UPDATE_DATE]               DATETIME2 (6)   NOT NULL,
    [LAST_UPDATE_USER]               NVARCHAR (255)  NULL,
    [USER_TYPE]                      NVARCHAR (255)  NULL,
    [ALIAS]                          NVARCHAR (255)  NOT NULL,
    [DIST_NETWORK_BILL_ALIAS]        NVARCHAR (255)  NULL,
    [END_DATE]                       DATE            NULL,
    [FIRST_DEPOSIT_EMAIL_SENT]       DECIMAL (1)     NULL,
    [FOLDER_COMPLETE]                DECIMAL (1)     NULL,
    [GC_ACCEPTANCE_DATE]             DATE            NULL,
    [VENDOR_RECOGNITION_CHECK]       DECIMAL (1)     NULL,
    [ENTITLE_PROOF_ID_CHECK_DATE]    DATETIME2 (6)   NULL,
    [SALESFORCE_OPPORTUNITY_ID]      NVARCHAR (255)  NULL,
    [SCORE_CALL_COUNTER]             DECIMAL (10)    NULL,
    [COMMERCIAL_OFFER_ID]            DECIMAL (19)    NULL,
    [SUB_STATUS_ID]                  DECIMAL (19)    NULL,
    [SUSPENSION_CTRL_STATUS_ID]      DECIMAL (19)    NULL,
    [CORE_BANKING_ERROR_CODE]        NVARCHAR (255)  NULL,
    [CORE_BANKING_ERROR_MESSAGE]     NVARCHAR (4000) NULL,
    [D_N_PROSPECT_ID]                DECIMAL (38)    NULL,
    [ENTITLEMENT_PROOF_FOLDER_ID]    DECIMAL (38)    NULL,
    [CONTRACT_SENT]                  DECIMAL (1)     NULL,
    [PRE_CONTRACTUAL_DOC_SENT]       DECIMAL (1)     NULL,
    [PERSON_RELATIONSHIP_ID]         DECIMAL (38)    NULL,
    [FIRST_DEPOSIT_BY_TRANSFER_IBAN] NVARCHAR (255)  NULL,
    [RIO_CONTEXT]                    DECIMAL (1)     NULL,
    [LIVENESS_CONSENT]               DECIMAL (1)     NULL,
    [Validity_StartDate]             DATETIME2 (7)   NOT NULL,
    [Validity_EndDate]               DATETIME2 (7)   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_FE_ENROLMENTFOLDER_V2] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UC_ID_FE_ENROLMENTFOLDER] UNIQUE NONCLUSTERED ([ID_FE_ENROLMENT_FOLDER] ASC, [Validity_StartDate] ASC)
);




GO