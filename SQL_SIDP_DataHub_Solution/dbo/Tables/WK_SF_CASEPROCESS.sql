﻿CREATE TABLE [dbo].[WK_SF_CASEPROCESS] (
    [Id_SF]                   NVARCHAR (18)   NOT NULL,
    [OwnerId]                 NVARCHAR (18)   NULL,
    [IsDeleted]               NVARCHAR (10)   NULL,
    [Name]                    NVARCHAR (80)   NULL,
    [RecordTypeId]            NVARCHAR (18)   NULL,
    [CreatedDate]             DATETIME        NULL,
    [CreatedById]             NVARCHAR (18)   NULL,
    [LastModifiedDate]        DATETIME        NULL,
    [LastModifiedById]        NVARCHAR (18)   NULL,
    [SystemModstamp]          DATETIME        NULL,
    [LastViewedDate]          DATETIME        NULL,
    [LastReferencedDate]      DATETIME        NULL,
    [AttachmentType__c]       NVARCHAR (100)  NULL,
    [CaseProcessName__c]      NVARCHAR (255)  NULL,
    [CaseProcessOrder__c]     DECIMAL (18)    NULL,
    [Country__c]              NVARCHAR (255)  NULL,
    [FormVisibility__c]       NVARCHAR (10)   NULL,
    [HelpTextAttachment__c]   NVARCHAR (2000) NULL,
    [HelpText__c]             NVARCHAR (2000) NULL,
    [IsAttachmentRequired__c] NVARCHAR (10)   NULL,
    [SubjectOrder__c]         DECIMAL (18)    NULL,
    [Subject__c]              NVARCHAR (255)  NULL,
    [CaseRecordType__c]       NVARCHAR (255)  NULL,
    [SubType__c]              NVARCHAR (255)  NULL,
    [Type__c]                 NVARCHAR (255)  NULL,
    CONSTRAINT [PK_WK_SF_CaseProcess] PRIMARY KEY CLUSTERED ([Id_SF] ASC)
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASEPROCESS', @level2type = N'COLUMN', @level2name = N'Type__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Sous-Type', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASEPROCESS', @level2type = N'COLUMN', @level2name = N'SubType__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type d''enregistrement de la Requête', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASEPROCESS', @level2type = N'COLUMN', @level2name = N'CaseRecordType__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Sujet', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASEPROCESS', @level2type = N'COLUMN', @level2name = N'Subject__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ordre du sujet', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASEPROCESS', @level2type = N'COLUMN', @level2name = N'SubjectOrder__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Piece Jointe Obligatoire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASEPROCESS', @level2type = N'COLUMN', @level2name = N'IsAttachmentRequired__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Texte d''aide', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASEPROCESS', @level2type = N'COLUMN', @level2name = N'HelpText__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Texte d''aide de la pièce jointe', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASEPROCESS', @level2type = N'COLUMN', @level2name = N'HelpTextAttachment__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Visibilité du formulaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASEPROCESS', @level2type = N'COLUMN', @level2name = N'FormVisibility__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Pays', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASEPROCESS', @level2type = N'COLUMN', @level2name = N'Country__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ordre de l''acte de gestion', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASEPROCESS', @level2type = N'COLUMN', @level2name = N'CaseProcessOrder__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Acte de gestion', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASEPROCESS', @level2type = N'COLUMN', @level2name = N'CaseProcessName__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type de pièce jointe', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASEPROCESS', @level2type = N'COLUMN', @level2name = N'AttachmentType__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernière date référencée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASEPROCESS', @level2type = N'COLUMN', @level2name = N'LastReferencedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernier affichage', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASEPROCESS', @level2type = N'COLUMN', @level2name = N'LastViewedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Horodateur des modifications du système', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASEPROCESS', @level2type = N'COLUMN', @level2name = N'SystemModstamp';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernière modification par ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASEPROCESS', @level2type = N'COLUMN', @level2name = N'LastModifiedById';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASEPROCESS', @level2type = N'COLUMN', @level2name = N'LastModifiedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID créé par', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASEPROCESS', @level2type = N'COLUMN', @level2name = N'CreatedById';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASEPROCESS', @level2type = N'COLUMN', @level2name = N'CreatedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID du type d''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASEPROCESS', @level2type = N'COLUMN', @level2name = N'RecordTypeId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom de l''acte de gestion', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASEPROCESS', @level2type = N'COLUMN', @level2name = N'Name';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Supprimé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASEPROCESS', @level2type = N'COLUMN', @level2name = N'IsDeleted';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID du propriétaire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASEPROCESS', @level2type = N'COLUMN', @level2name = N'OwnerId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID d''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_CASEPROCESS', @level2type = N'COLUMN', @level2name = N'Id_SF';

