﻿CREATE TABLE [dbo].[OAV_LIEN_EMPRUNTEURS] (
    [ID_LIEN_EMPRUNTEURS] DECIMAL (10)   NOT NULL,
    [LABEL]               NVARCHAR (255) NOT NULL,
    [DAT_OBSR]            DATE           CONSTRAINT [DefValStartDatOAV_LIEN_EMPRUNTEURS] DEFAULT (getdate()) NOT NULL,
    [DAT_CHRG]            DATETIME       CONSTRAINT [DefValEndDatOAV_LIEN_EMPRUNTEURS] DEFAULT (CONVERT([date],'9999-12-31')) NOT NULL,
    CONSTRAINT [PK_PrimaryKeyLIEN_EMPRUNTEURS] PRIMARY KEY CLUSTERED ([ID_LIEN_EMPRUNTEURS] ASC)
);


