﻿CREATE TABLE [dbo].[WK_SF_EMAILMESSAGE] (
    [Id_SF]                 NVARCHAR (18)   NOT NULL,
    [ParentId]              NVARCHAR (18)   NULL,
    [ActivityId]            NVARCHAR (18)   NULL,
    [CreatedById]           NVARCHAR (18)   NULL,
    [CreatedDate]           DATETIME        NULL,
    [LastModifiedDate]      DATETIME        NULL,
    [LastModifiedById]      NVARCHAR (18)   NULL,
    [SystemModstamp]        DATETIME2 (7)   NULL,
    [TextBody]              NVARCHAR (MAX)  NULL,
    [HtmlBody]              NVARCHAR (MAX)  NULL,
    [Headers]               NVARCHAR (MAX)  NULL,
    [Subject]               NVARCHAR (3000) NULL,
    [FromName]              NVARCHAR (1000) NULL,
    [FromAddress]           NVARCHAR (1000) NULL,
    [ValidatedFromAddress]  NVARCHAR (255)  NULL,
    [ToAddress]             NVARCHAR (4000) NULL,
    [CcAddress]             NVARCHAR (4000) NULL,
    [BccAddress]            NVARCHAR (4000) NULL,
    [Incoming]              BIT             NULL,
    [HasAttachment]         NVARCHAR (10)   NULL,
    [Status]                NVARCHAR (40)   NULL,
    [MessageDate]           DATETIME2(7)	NULL,
	[IsPrivateDraft]		BIT             NULL,
    [IsDeleted]             NVARCHAR (10)   NULL,
    [ReplyToEmailMessageId] NVARCHAR (18)   NULL,
    [IsExternallyVisible]   NVARCHAR (10)   NULL,
    [MessageIdentifier]     NVARCHAR (255)  NULL,
    [ThreadIdentifier]      NVARCHAR (255)  NULL,
    [IsClientManaged]       NVARCHAR (10)   NULL,
    [RelatedToId]           NVARCHAR (18)   NULL,
	[IsTracked]				BIT				NULL,
	[IsOpened]				BIT				NULL,
	[FirstOpenedDate]		DATETIME2(7)	NULL,
	[LastOpenedDate]		DATETIME2(7)	NULL,
	[IsBounced]				BIT				NULL,
	[EmailTemplateId]		NVARCHAR (255)  NULL,
    [AlertId__c]            NVARCHAR (255)  NULL,
    [Family__c]             NVARCHAR (255)  NULL,
    [From__c]               NVARCHAR (255)  NULL,
    [SubFamily__c]          NVARCHAR (255)  NULL,
    [ForbiddenWords__c]     NVARCHAR (255)  NULL,
    [HasForbiddenWord__c]   BIT   NULL,
    CONSTRAINT [PK_WK_SF_EMAILMESSAGE] PRIMARY KEY CLUSTERED ([Id_SF] ASC)
)
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Adresse du destinataire', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'ToAddress';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Corps de texte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'TextBody';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Objet', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'Subject';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID d''associé à', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'RelatedToId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de la requête', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'ParentId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date du message', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'MessageDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'LastModifiedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Entrant', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'Incoming';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID d''e-mail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'Id_SF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Corps de texte HTML', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'HtmlBody';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Expéditeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'FromName';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Adresse de l''expéditeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'FromAddress';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'CreatedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID créé par', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'CreatedById';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Adresse Cc', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'CcAddress';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Adresse Cci', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'BccAddress';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'De', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'ValidatedFromAddress';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de fil', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'ThreadIdentifier';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Horodateur des modifications du système', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'SystemModstamp';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Sous-famille', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'SubFamily__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Statut', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'Status';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID d''e-mail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'ReplyToEmailMessageId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de message', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'MessageIdentifier';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernière modification par ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'LastModifiedById';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Est visible en externe', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'IsExternallyVisible';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Supprimé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'IsDeleted';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Est client géré', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'IsClientManaged';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'En-têtes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'Headers';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Contient Mot(s) Interdit(s)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'HasForbiddenWord__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Avec pièce jointe', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'HasAttachment';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Provenance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'From__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mots interdits', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'ForbiddenWords__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Famille', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'Family__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Id de l''alerte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'AlertId__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de l''activité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'ActivityId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Est suivi', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'IsTracked'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ouvert ?', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'IsOpened'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Première ouverture', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'FirstOpenedDate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernière ouverture', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'LastOpenedDate'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Rebondi ?', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'IsBounced'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID du modèle de message', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_EMAILMESSAGE', @level2type = N'COLUMN', @level2name = N'EmailTemplateId'
GO