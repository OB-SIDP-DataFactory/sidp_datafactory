﻿CREATE TABLE [dbo].[PV_FE_COMMERCIALOFFER] (
    [Id]                      INT            IDENTITY (1, 1) NOT NULL,
    [ID_FE_COMMERCIALOFFER]   DECIMAL (19)   NULL,
    [CODE]                    NVARCHAR (255) NULL,
    [CREATION_DATE]           DATETIME2 (6)  NULL,
    [CREATION_USER]           NVARCHAR (255) NULL,
    [DISTRIBUTION_NETWORK_ID] DECIMAL (19)   NULL,
    [END_DATE]                DATE           NULL,
    [LABEL]                   NVARCHAR (255) NULL,
    [LAST_UPDATE_DATE]        DATETIME2 (6)  NULL,
    [LAST_UPDATE_USER]        NVARCHAR (255) NULL,
    [PRODUCT_FAMILY_ID]       DECIMAL (19)   NULL,
    [START_DATE]              DATE           NULL,
    [USER_TYPE]               NVARCHAR (255) NULL,
    [Validity_StartDate]      DATETIME2 (7)  NOT NULL,
    [Validity_EndDate]        DATETIME2 (7)  DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_FE_COMMERCIALOFFER] PRIMARY KEY CLUSTERED ([Id] ASC)
);

