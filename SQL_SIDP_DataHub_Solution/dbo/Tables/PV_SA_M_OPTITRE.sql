﻿CREATE TABLE [dbo].[PV_SA_M_OPTITRE] (
    [ID]        BIGINT          IDENTITY (1, 1) NOT NULL,
    [DWHOPRREF] VARCHAR (16)    NULL,
    [DWHOPRDTO] DATE            NULL,
    [DWHOPRETB] INT             NULL,
    [DWHOPRAGC] INT             NULL,
    [DWHOPRNPT] INT             NULL,
    [DWHOPRREX] VARCHAR (32)    NULL,
    [DWHOPRIDV] INT             NULL,
    [DWHOPRDDV] DATE            NULL,
    [DWHOPRRVM] INT             NULL,
    [DWHOPRPCT] INT             NULL,
    [DWHOPRPCA] VARCHAR (3)     NULL,
    [DWHOPRISI] VARCHAR (12)    NULL,
    [DWHOPRTCN] VARCHAR (23)    NULL,
    [DWHOPRDEC] DATE            NULL,
    [DWHOPRPEX] DECIMAL (18, 9) NULL,
    [DWHOPRRBR] INT             NULL,
    [DWHOPRPBR] VARCHAR (3)     NULL,
    [DWHOPRLBR] VARCHAR (32)    NULL,
    [DWHOPRDVC] VARCHAR (3)     NULL,
    [DWHOPRRCL] VARCHAR (7)     NULL,
    [DWHOPRDOR] INT             NULL,
    [DWHOPRNDO] VARCHAR (32)    NULL,
    [DWHOPRANN] INT             NULL,
    [DWHOPRRAN] VARCHAR (16)    NULL,
    [DWHOPRTOP] VARCHAR (8)     NULL,
    [DWHOPRLBO] VARCHAR (50)    NULL,
    [DWHOPRSOP] INT             NULL,
    [DWHOPRLSE] VARCHAR (32)    NULL,
    [DWHOPRDTR] DATE            NULL,
    [DWHOPRDTN] DATE            NULL,
    [DWHOPRDTV] DATE            NULL,
    [DWHOPRNTP] INT             NULL,
    [DWHOPRLNT] VARCHAR (32)    NULL,
    [DWHOPRCMA] INT             NULL,
    [DWHOPRLMA] VARCHAR (32)    NULL,
    [DWHOPRQTE] DECIMAL (18, 6) NULL,
    [DWHOPRCRN] DECIMAL (18, 6) NULL,
    [DWHOPRRNG] INT             NULL,
    [DWHOPRCNG] VARCHAR (12)    NULL,
    [DWHOPRLNG] VARCHAR (32)    NULL,
    [DWHOPRRCM] INT             NULL,
    [DWHOPRCMP] VARCHAR (12)    NULL,
    [DWHOPRLCM] VARCHAR (32)    NULL,
    [DWHOPRRDP] INT             NULL,
    [DWHOPRDEP] VARCHAR (12)    NULL,
    [DWHOPRLDE] VARCHAR (32)    NULL,
    [DWHOPRADH] VARCHAR (5)     NULL,
    [DWHOPRMBC] DECIMAL (18, 6) NULL,
    [DWHOPRMIC] DECIMAL (18, 6) NULL,
    [DWHOPRCBC] DECIMAL (18, 6) NULL,
    [DWHOPRITC] DECIMAL (18, 6) NULL,
    [DWHOPRTVC] DECIMAL (18, 6) NULL,
    [DWHOPRCOC] DECIMAL (18, 6) NULL,
    [DWHOPRTCC] DECIMAL (18, 6) NULL,
    [DWHOPRIRC] DECIMAL (18, 6) NULL,
    [DWHOPRRSC] DECIMAL (18, 6) NULL,
    [DWHOPRTFC] DECIMAL (18, 6) NULL,
    [DWHOPRMNC] DECIMAL (18, 6) NULL,
    [DWHOPRPVC] DECIMAL (18, 6) NULL,
    [DWHOPRCSC] DECIMAL (18, 6) NULL,
    [DWHOPRMBR] DECIMAL (18, 6) NULL,
    [DWHOPRMIR] DECIMAL (18, 6) NULL,
    [DWHOPRCBR] DECIMAL (18, 6) NULL,
    [DWHOPRITR] DECIMAL (18, 6) NULL,
    [DWHOPRTVR] DECIMAL (18, 6) NULL,
    [DWHOPRCOR] DECIMAL (18, 6) NULL,
    [DWHOPRTCR] DECIMAL (18, 6) NULL,
    [DWHOPRIRR] DECIMAL (18, 6) NULL,
    [DWHOPRRSR] DECIMAL (18, 6) NULL,
    [DWHOPRTFR] DECIMAL (18, 6) NULL,
    [DWHOPRMNR] DECIMAL (18, 6) NULL,
    [DWHOPRPVR] DECIMAL (18, 6) NULL,
    [DWHOPRCSR] DECIMAL (18, 6) NULL,
    [DWHOPRPRV] DECIMAL (18, 6) NULL,
    [DWHOPRCRG] VARCHAR (20)    NULL,
    [DWHOPRCFR] VARCHAR (20)    NULL,
    [DWHOPRCIO] DECIMAL (15, 9) NULL,
    [DWHOPRCAD] VARCHAR (8)     NULL,
    [DWHOPRCEX] VARCHAR (32)    NULL,
    [DWHOPRUSA] INT             NULL,
    [DWHOPRNSA] VARCHAR (32)    NULL,
    [DWHOPRDSA] DATE            NULL,
    [DWHOPRHSA] INT             NULL,
    [DWHOPRUVL] INT             NULL,
    [DWHOPRNVL] VARCHAR (32)    NULL,
    [DWHOPRDVL] DATE            NULL,
    [DWHOPRHVL] INT             NULL,
    [DWHOPRDLQ] DATE            NULL,
    [DWHOPRNEX] VARCHAR (16)    NULL,
    [DWHOPRMBG] DECIMAL (18, 6) NULL,
    [DWHOPRMNG] DECIMAL (18, 6) NULL,
    [DWHOPRRSF] DECIMAL (18, 6) NULL,
    [DWHOPRDSF] VARCHAR (3)     NULL,
    [DWHOPRCBF] DECIMAL (18, 6) NULL,
    [DWHOPRDBF] VARCHAR (3)     NULL,
    [DWHOPRTVF] DECIMAL (18, 6) NULL,
    [DWHOPRDVF] VARCHAR (3)     NULL,
    [DWHOPRITF] DECIMAL (18, 6) NULL,
    [DWHOPRDTF] VARCHAR (3)     NULL,
    [DWHOPRCOF] DECIMAL (18, 6) NULL,
    [DWHOPRDOF] VARCHAR (3)     NULL,
    [DWHOPRTCF] DECIMAL (18, 6) NULL,
    [DWHOPRDCF] VARCHAR (3)     NULL,
    [DWHOPRSGC] DECIMAL (18, 6) NULL,
    [DWHOPRSGF] DECIMAL (18, 6) NULL,
    [DWHOPRDGF] VARCHAR (3)     NULL,
    [DWHOPRRCS] DECIMAL (18, 6) NULL,
    [DWHOPRSGR] DECIMAL (18, 6) NULL,
    [DWHOPRTMF] DECIMAL (18, 6) NULL,
    [DWHOPRDMF] VARCHAR (3)     NULL,
    [DWHOPRTQF] DECIMAL (18, 6) NULL,
    [DWHOPRDQF] VARCHAR (3)     NULL,
    [DWHOPRDVR] VARCHAR (3)     NULL,
    [DWHOPRLOC] VARCHAR (3)     NULL,
    [DWHOPRRSG] VARCHAR (7)     NULL,
    [DWHOPRCSG] INT             NULL,
    [DWHOPRESG] VARCHAR (20)    NULL,
    [DWHOPRDSG] VARCHAR (3)     NULL,
    [DWHOPRBIL] VARCHAR (30)    NULL,
    [DWHOPRBIC] VARCHAR (11)    NULL,
    CONSTRAINT [PK_PV_SA_M_OPTITRE] PRIMARY KEY CLUSTERED ([ID] ASC)
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'BIC ONE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRBIC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'REFERENCE BILATÉRALE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRBIL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEV SOC GEST', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRDSG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CPT SOC GEST(SAB)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRESG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CPT SOC GEST(SAMIC)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRCSG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RAC SOC GEST(SAMIC)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRRSG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEVISE LOCALE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRLOC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEVISE RÈGLEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRDVR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEV PAY FRAIS BANQUE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRDQF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PAY - FRAIS BANQUE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRTQF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEV PAY TOT FRAIS MA', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRDMF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PAY - TOT FRAIS MAR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRTMF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CV - COM SOC GESTION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRSGR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CV - COM SOC GESTION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRRCS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEV COM SOC GESTION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRDGF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PAY - COM SOC GESTIO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRSGF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COT - COM SOC DE GES', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRSGC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEV PAY TVA COURTAGE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRDCF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PAY - TVA COURTAGE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRTCF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEV PAY COURTAGE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRDOF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PAY - COURTAGE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRCOF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEV PAY IMPOT / TAXE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRDTF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PAY - IMPOT / TAXE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRITF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEV PAY TVA COM BANK', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRDVF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PAY - TVA COM BANQUE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRTVF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEV PAY COM BANQUE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRDBF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PAY - COM BANQUE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRCBF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEV PAY RET SRC EURO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRDSF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PAY - RET SRC EURO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRRSF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'REG - MONTANT NET', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRMNG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'REG - MONTANT BRUT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRMBG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° D EXÉCUTION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRNEX';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DE VALEUR LIQU', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRDLQ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'VALIDAT° - HEURE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRHVL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'VALIDAT° - DATE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRDVL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'VALIDAT°- NOM UTILI', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRNVL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'VALIDAT°- UTILI', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRUVL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SAISIE - HEURE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRHSA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SAISIE - DATE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRDSA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SAISIE - NOM UTILI', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRNSA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SAISIE - RACINE UTIL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRUSA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE CLIENT EXTERNE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRCEX';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE ADHÉRENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRCAD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'OATI - COEF INDEX', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRCIO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COMPTE DE FRAIS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRCFR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CPT DE RÈGLEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRCRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PRIX REVIENT NET UNI', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRPRV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CV - CESSION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRCSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CV - VALUE RÉALISÉE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRPVR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CV - MONTANT NET', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRMNR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CV - TOTAL DES FRAIS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRTFR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CV - RETENUE SRC EUR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRRSR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CV - IMPÔT RÉPERT.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRIRR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CV - TVA COURTAGE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRTCR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CV - COURTAGE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRCOR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CV - TVA COMMISS° BQ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRTVR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CV - IMPÔTS ET TAXES', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRITR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CV - COMMISSION BANQ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRCBR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CV - MT INTÉRÊTS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRMIR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CV - MONTANT BRUT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRMBR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COT - CESSION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRCSC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COT - VALUE RÉALISÉE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRPVC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COT - MONTANT NET', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRMNC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COT - TOTAL FRAIS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRTFC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COT - RETENU SRC EUR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRRSC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COT - IMPÔT RÉPERT.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRIRC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COT - TVA COURTAGE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRTCC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COT - COURTAGE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRCOC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COT - TVA COMM BANQ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRTVC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COT - IMPÔTS & TAXES', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRITC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COT - COMMISS° BANQ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRCBC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COT - MT INTÉRÊTS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRMIC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COT - MT BRUT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRMBC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° ADHÉRENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRADH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIB DÉPOSITAIRE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRLDE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DÉPOSITAIRE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRDEP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RACINE DÉPOSITAIRE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRRDP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIB COMPENSATEUR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRLCM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COMPENSATEUR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRCMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RACINE COMPENSATEUR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRRCM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIB CONTREPART/NÉGO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRLNG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CONTREPARTIE/NÉGOCIA', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRCNG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RACINE CONT/NÉGOCIAT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRRNG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COURS NÉG/PRIX REV', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRCRN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'QUANTITÉ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRQTE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIBELLÉ DU MARCHÉ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRLMA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE MARCHÉ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRCMA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIBELLÉ NATURE PTF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRLNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NATURE PTF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRNTP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DT DE VAL/R/L TITRES', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRDTV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE NÉGOCIAT°/EFFET', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRDTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ORDRE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRDTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIBELLÉ SENS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRLSE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SENS OPÉRATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRSOP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIBELLE OPÉRATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRLBO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE OPÉRAT°', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRTOP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'REF OPE ANNULÉE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRRAN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE ANNULATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRANN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NOM DONNEUR ORDRE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRNDO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DONNEUR ORDRE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRDOR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RACINE CLIENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRRCL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEV DE COTAT° ISO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRDVC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIB BOURSE OPE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRLBR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PAYS BOURSE OPE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRPBR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RACINE BOURSE OPÉ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRRBR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PRIX EXERCICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRPEX';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ÉCHÉANCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRDEC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE MNÉMONIQUE TCN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRTCN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE ISIN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRISI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PLACE COTAT° ALPHA', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRPCA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PLACE DE COTATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRPCT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RACINE VAL SAMIC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRRVM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DEB VALIDITÉ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRDDV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID. VALEUR MOBILIERE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRIDV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'REF EXTERNE PTF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRREX';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° PORTEFEUILLE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRNPT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AGENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRAGC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ETABLISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRETB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE OPÉRATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRDTO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RÉFÉRENCE OPÉRAT°', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_OPTITRE', @level2type = N'COLUMN', @level2name = N'DWHOPRREF';

