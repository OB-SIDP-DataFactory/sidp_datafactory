﻿CREATE TABLE [dbo].[IWD_DIALING_MODE] (
    [DIALING_MODE_KEY]  INT          NOT NULL,
    [DIALING_MODE]      VARCHAR (32) NULL,
    [DIALING_MODE_CODE] VARCHAR (32) NULL,
    [CREATE_AUDIT_KEY]  NUMERIC (19) NOT NULL,
    [UPDATE_AUDIT_KEY]  NUMERIC (19) NOT NULL,
    CONSTRAINT [PK_DIALING_MODE] PRIMARY KEY NONCLUSTERED ([DIALING_MODE_KEY] ASC)
);

