﻿CREATE TABLE [dbo].[PV_SF_USERROLE] (
    [Id]                               INT           IDENTITY (1, 1) NOT NULL,
    [Id_sf]                            NVARCHAR (18) NULL,
    [Name]                             NVARCHAR (80) NULL,
    [ParentRoleId]                     NVARCHAR (18) NULL,
    [RollupDescription]                NVARCHAR (80) NULL,
    [OpportunityAccessForAccountOwner] NVARCHAR (40) NULL,
    [CaseAccessForAccountOwner]        NVARCHAR (40) NULL,
    [ContactAccessForAccountOwner]     NVARCHAR (40) NULL,
    [ForecastUserId]                   NVARCHAR (18) NULL,
    [MayForecastManagerShare]          NVARCHAR (10) NULL,
    [LastModifiedDate]                 DATETIME      NULL,
    [LastModifiedById]                 NVARCHAR (18) NULL,
    [SystemModstamp]                   DATETIME      NULL,
    [DeveloperName]                    NVARCHAR (80) NULL,
    [PortalAccountId]                  NVARCHAR (18) NULL,
    [PortalType]                       NVARCHAR (40) NULL,
    [PortalRole]                       NVARCHAR (40) NULL,
    [PortalAccountOwnerId]             NVARCHAR (18) NULL,
    [Validity_StartDate]               DATETIME2 (7) NOT NULL,
    [Validity_EndDate]                 DATETIME2 (7) DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SF_UserRole] PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Rôle du portail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USERROLE', @level2type = N'COLUMN', @level2name = N'PortalRole';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type de portail', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USERROLE', @level2type = N'COLUMN', @level2name = N'PortalType';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de la personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USERROLE', @level2type = N'COLUMN', @level2name = N'PortalAccountId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom du développeur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USERROLE', @level2type = N'COLUMN', @level2name = N'DeveloperName';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Horodateur des modifications du système', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USERROLE', @level2type = N'COLUMN', @level2name = N'SystemModstamp';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernière modification par ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USERROLE', @level2type = N'COLUMN', @level2name = N'LastModifiedById';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USERROLE', @level2type = N'COLUMN', @level2name = N'LastModifiedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Le propriétaire des prévisions peut-il partager', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USERROLE', @level2type = N'COLUMN', @level2name = N'MayForecastManagerShare';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID d''utilisateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USERROLE', @level2type = N'COLUMN', @level2name = N'ForecastUserId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Niveau d''accès aux contacts pour le propriétaire du compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USERROLE', @level2type = N'COLUMN', @level2name = N'ContactAccessForAccountOwner';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Niveau d''accès aux requêtes pour le propriétaire du compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USERROLE', @level2type = N'COLUMN', @level2name = N'CaseAccessForAccountOwner';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Niveau d''accès aux opportunités pour le propriétaire du compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USERROLE', @level2type = N'COLUMN', @level2name = N'OpportunityAccessForAccountOwner';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Description', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USERROLE', @level2type = N'COLUMN', @level2name = N'RollupDescription';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID du rôle principal', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USERROLE', @level2type = N'COLUMN', @level2name = N'ParentRoleId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USERROLE', @level2type = N'COLUMN', @level2name = N'Name';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de rôle', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_USERROLE', @level2type = N'COLUMN', @level2name = N'Id_sf';

