﻿CREATE TABLE [dbo].[RAW_PROCESSEDDATA] (
    [file_name]        VARCHAR (100)    NULL,
    [stream_id]        UNIQUEIDENTIFIER NULL,
    [file_full_path]   VARCHAR (1000)   NULL,
    [file_stream_data] VARCHAR (MAX)    NULL
);
