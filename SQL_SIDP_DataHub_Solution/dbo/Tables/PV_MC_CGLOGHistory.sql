﻿CREATE TABLE [dbo].[PV_MC_CGLOGHistory] (
    [Id]                  BIGINT         NOT NULL,
    [AUDIENCE_NAME]       NVARCHAR (200) NOT NULL,
    [ID_CAMPAIGN_MESSAGE] NVARCHAR (50)  NOT NULL,
    [MESSAGE_NAME]        NVARCHAR (200) NULL,
    [ID_CAMPAIGN]         NVARCHAR (50)  NULL,
    [CAMPAIGN_NAME]       NVARCHAR (255) NULL,
    [ID_CONTACT]          NVARCHAR (50)  NOT NULL,
    [DATE_TIME_SEND]      DATETIME       NULL,
    [Startdt]             DATETIME2 (7)  NOT NULL,
    [Enddt]               DATETIME2 (7)  NOT NULL
);


