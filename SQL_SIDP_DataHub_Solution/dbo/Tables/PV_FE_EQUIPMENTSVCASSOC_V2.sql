﻿CREATE TABLE [dbo].[PV_FE_EQUIPMENTSVCASSOC_V2] (
    [Id]                 INT             IDENTITY (1, 1) NOT NULL,
    [EQUIPMENT_ID]       DECIMAL (38)    NOT NULL,
    [SERVICE_PRODUCT_ID] INT             NOT NULL,
    [STATUS_ID]          INT             NOT NULL,
    [AGREEMENT_NUMBER]   INT             NULL,
    [CREATION_DATE]      DATETIME2 (6)   NOT NULL,
    [CREATION_USER]      NVARCHAR (255)  NULL,
    [LAST_UPDATE_DATE]   DATETIME2 (6)   NOT NULL,
    [LAST_UPDATE_USER]   NVARCHAR (255)  NULL,
    [USER_TYPE]          NVARCHAR (255)  NULL,
    [RISK_RATE_1]        DECIMAL (19, 7) NULL,
    [RISK_RATE_2]        DECIMAL (19, 7) NULL,
    [RISK_TYPE_1]        NVARCHAR (255)  NULL,
    [RISK_TYPE_2]        NVARCHAR (255)  NULL,
    [GUARANTEE_LIST]     NVARCHAR (4000) NULL,
    [CUSTOMER_ID]        NUMERIC (38)    NULL,
    [Validity_StartDate] DATETIME2 (7)   NOT NULL,
    [Validity_EndDate]   DATETIME2 (7)   CONSTRAINT [DF__PV_FE_EQU__Valid__67A95F61] DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_FE_EQUIPMENTSVCASSOC_V2] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UC_ID_FE_EQUIPMENTSVCASSOC] UNIQUE NONCLUSTERED ([EQUIPMENT_ID] ASC, [SERVICE_PRODUCT_ID] ASC, [Validity_StartDate] ASC)
);




