﻿CREATE TABLE [dbo].[REF_DISTRIBUTION_SUB_NET] (
    [ID]                          INT            IDENTITY (1, 1) NOT NULL,
    [ID_DISTRIBUTION_SUB_NET]     DECIMAL (19)   NULL,
    [SALESFORCE_CODE]             NVARCHAR (255) NULL,
    [PARENT_ID]                   DECIMAL (19)   NULL,
    [BRAND_ID]                    DECIMAL (38)   NULL,
    [FRANFINANCE_SUBSCRIBER_CODE] NVARCHAR (255) NULL,
    [FRANFINANCE_SUBSCRIBER_PWD]  NVARCHAR (255) NULL,
    [SAB_CODE]                    NVARCHAR (255) NULL,
    [QUICKSIGN_CERTIFICAT]        NVARCHAR (255) NULL,
    CONSTRAINT [PK_REF_DISTRIBUTION_SUB_NET] PRIMARY KEY CLUSTERED ([ID] ASC)
);

