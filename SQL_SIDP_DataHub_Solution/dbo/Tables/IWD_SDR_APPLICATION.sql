﻿CREATE TABLE [dbo].[IWD_SDR_APPLICATION] (
    [ID]                  INT           IDENTITY (1, 1) NOT NULL,
    [CREATE_AUDIT_KEY]    NUMERIC (19)  NOT NULL,
    [APPLICATION_VERSION] VARCHAR (50)  DEFAULT ('NO_VALUE') NOT NULL,
    [APPLICATION_TITLE]   VARCHAR (255) DEFAULT ('NO_VALUE') NOT NULL,
    [APPLICATION_ID]      VARCHAR (50)  DEFAULT ('NO_VALUE') NOT NULL,
    CONSTRAINT [PK_SDR_APPLICATION] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IWD_I_SDR_APPLICATION]
    ON [dbo].[IWD_SDR_APPLICATION]([APPLICATION_VERSION] ASC, [APPLICATION_TITLE] ASC, [APPLICATION_ID] ASC);

