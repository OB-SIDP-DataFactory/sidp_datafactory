﻿CREATE TABLE [dbo].[IWD_SDR_SURVEY_I1] (
    [ID]               INT          IDENTITY (1, 1) NOT NULL,
    [CREATE_AUDIT_KEY] NUMERIC (19) NOT NULL,
    [IQ1]              INT          DEFAULT ((-1)) NOT NULL,
    [IQ2]              INT          DEFAULT ((-1)) NOT NULL,
    [IQ3]              INT          DEFAULT ((-1)) NOT NULL,
    [IQ4]              INT          DEFAULT ((-1)) NOT NULL,
    [IQ5]              INT          DEFAULT ((-1)) NOT NULL,
    CONSTRAINT [PK_SDR_SURVEY_I1] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IWD_I_SDR_SURVEY_I1]
    ON [dbo].[IWD_SDR_SURVEY_I1]([IQ1] ASC, [IQ2] ASC, [IQ3] ASC, [IQ4] ASC, [IQ5] ASC);

