﻿CREATE TABLE [dbo].[PV_MC_BOUNCESHistory] (
    [Id]                       BIGINT         NOT NULL,
    [ClientID]                 INT            NULL,
    [SendID]                   INT            NOT NULL,
    [SubscriberKey]            NVARCHAR (100) NOT NULL,
    [EmailAddress]             NVARCHAR (100) NULL,
    [SubscriberID]             INT            NULL,
    [ListID]                   INT            NULL,
    [EventDate]                DATETIME       NOT NULL,
    [EventType]                NVARCHAR (100) NULL,
    [BounceCategory]           NVARCHAR (50)  NULL,
    [SMTPCode]                 SMALLINT       NULL,
    [BounceReason]             NVARCHAR (MAX) NULL,
    [BatchID]                  NVARCHAR (100) NULL,
    [TriggeredSendExternalKey] NVARCHAR (101) NULL,
    [Startdt]                  DATETIME2 (7)  NOT NULL,
    [Enddt]                    DATETIME2 (7)  NOT NULL
);
GO
CREATE CLUSTERED INDEX [ix_PV_MC_BOUNCESHistory]
    ON [dbo].[PV_MC_BOUNCESHistory]([Enddt] ASC, [Startdt] ASC) WITH (DATA_COMPRESSION = PAGE);

