﻿CREATE TABLE [dbo].[WK_FI_M_CTX] (
    [COD_ENR]         VARCHAR (2)    NULL,
    [TYP_CON]         VARCHAR (3)    NULL,
    [TYP_PRE]         VARCHAR (3)    NULL,
    [NUM_PRE]         VARCHAR (11)   NULL,
    [IDE_GRC]         VARCHAR (8)    NULL,
    [COD_BAN]         VARCHAR (5)    NULL,
    [COD_GUI]         VARCHAR (5)    NULL,
    [NUM_CPT]         VARCHAR (11)   NULL,
    [DEV_CPT]         VARCHAR (3)    NULL,
    [DTE_REM_CTX]     DATE           NULL,
    [CAP_DU_MEN_ECH]  DECIMAL (9, 2) NULL,
    [MTT_MEN_IMP]     DECIMAL (9, 2) NULL,
    [INT_MEN_IMP]     DECIMAL (9, 2) NULL,
    [IND_LEG]         DECIMAL (9, 2) NULL,
    [SLD_AFF_CTX]     DECIMAL (9, 2) NULL,
    [DTE_DER_ECR_COM] DATE           NULL,
    [CUM_PER]         DECIMAL (9, 2) NULL,
    [COD_POS]         VARCHAR (1)    NULL
);

