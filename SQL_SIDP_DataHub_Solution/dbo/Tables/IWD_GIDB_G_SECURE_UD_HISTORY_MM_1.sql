﻿CREATE TABLE [dbo].[IWD_GIDB_G_SECURE_UD_HISTORY_MM] (
    [ID]               NUMERIC (19)  NOT NULL,
    [CALLID]           VARCHAR (50)  NOT NULL,
    [PARTYID]          VARCHAR (50)  NULL,
    [ENDPOINTID]       INT           NULL,
    [ENDPOINTDN]       VARCHAR (255) NULL,
    [AGENTID]          INT           NULL,
    [SWITCHID]         INT           NULL,
    [KEYNAME]          VARCHAR (64)  NOT NULL,
    [CHANGETYPE]       INT           NOT NULL,
    [TYPE]             INT           NOT NULL,
    [KSEQ]             INT           NOT NULL,
    [CSEQ]             INT           NULL,
    [VALUE]            VARCHAR (255) NULL,
    [ADDED]            DATETIME      NULL,
    [ADDED_TS]         INT           NOT NULL,
    [GSYS_DOMAIN]      INT           NULL,
    [GSYS_SYS_ID]      INT           NULL,
    [CREATE_AUDIT_KEY] NUMERIC (19)  NOT NULL,
    CONSTRAINT [PK_GIDB_G_SECURE_UD_HISTORY_MM] PRIMARY KEY CLUSTERED ([CALLID] ASC, [ID] ASC, [CREATE_AUDIT_KEY] ASC, [ADDED_TS] ASC)
);
GO
CREATE NONCLUSTERED INDEX [IWD_I_G_SUDH_MM_ADDTS]
    ON [dbo].[IWD_GIDB_G_SECURE_UD_HISTORY_MM]([ADDED_TS] ASC);

