﻿CREATE TABLE [dbo].[IWD_GIDB_G_CALL_MM] (
    [ID]               NUMERIC (16)  NOT NULL,
    [CALLID]           VARCHAR (50)  NOT NULL,
    [PARENTCALLID]     VARCHAR (50)  NULL,
    [MERGECALLID]      VARCHAR (50)  NULL,
    [MERGETYPE]        INT           NULL,
    [CONNID]           VARCHAR (50)  NULL,
    [CONNIDNUM]        NUMERIC (20)  NULL,
    [SWITCHCALLID]     INT           NULL,
    [IRID]             VARCHAR (50)  NULL,
    [ROOTIRID]         VARCHAR (50)  NULL,
    [STATE]            INT           NULL,
    [CALLTYPE]         INT           NULL,
    [MEDIATYPE]        INT           NULL,
    [SWITCHID]         INT           NULL,
    [TENANTID]         INT           NULL,
    [CALLANI]          VARCHAR (50)  NULL,
    [CALLDNIS]         VARCHAR (50)  NULL,
    [CREATED]          DATETIME      NOT NULL,
    [CREATED_TS]       INT           NOT NULL,
    [TERMINATED]       DATETIME      NULL,
    [TERMINATED_TS]    INT           NULL,
    [ADDED_TS]         INT           NOT NULL,
    [GSYS_DOMAIN]      INT           NULL,
    [GSYS_SYS_ID]      INT           NULL,
    [GSYS_EXT_VCH1]    VARCHAR (255) NULL,
    [GSYS_EXT_VCH2]    VARCHAR (255) NULL,
    [GSYS_EXT_INT1]    INT           NULL,
    [GSYS_EXT_INT2]    INT           NULL,
    [CREATE_AUDIT_KEY] NUMERIC (19)  NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [IWD_I_G_CALL_MM_RIRID]
    ON [dbo].[IWD_GIDB_G_CALL_MM]([ROOTIRID] ASC);


GO
CREATE NONCLUSTERED INDEX [IWD_I_G_CALL_MM_CID]
    ON [dbo].[IWD_GIDB_G_CALL_MM]([CALLID] ASC);


GO
CREATE NONCLUSTERED INDEX [IWD_I_G_CALL_MM_ATS]
    ON [dbo].[IWD_GIDB_G_CALL_MM]([ADDED_TS] ASC);

