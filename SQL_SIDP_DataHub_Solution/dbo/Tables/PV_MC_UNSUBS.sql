﻿CREATE TABLE [dbo].[PV_MC_UNSUBS] (
    [Id]                       BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [ClientID]                 INT                                         NULL,
    [SendID]                   INT                                         NULL,
    [SubscriberKey]            NVARCHAR (100)                              NOT NULL,
    [EmailAddress]             NVARCHAR (100)                              NULL,
    [SubscriberID]             INT                                         NULL,
    [ListID]                   INT                                         NULL,
    [EventDate]                DATETIME                                    NOT NULL,
    [EventType]                NVARCHAR (100)                              NULL,
    [BatchID]                  NVARCHAR (100)                              NULL,
    [TriggeredSendExternalKey] NVARCHAR (100)                              NULL,
    [UnsubReason]              NVARCHAR (MAX)                              NULL,
    [Startdt]                  DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]                    DATETIME2 (7) GENERATED ALWAYS AS ROW END   NOT NULL,
    CONSTRAINT [PK_PV_MC_UNSUBS] PRIMARY KEY CLUSTERED ([Id] ASC, [SubscriberKey] ASC, [EventDate] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_MC_UNSUBSHistory], DATA_CONSISTENCY_CHECK=ON));

