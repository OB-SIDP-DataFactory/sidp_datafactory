﻿CREATE TABLE [dbo].[IWD_CAMPAIGN_GROUP_SESSION_FACT] (
    [CAMP_GROUP_SESSION_FACT_KEY] NUMERIC (19) NOT NULL,
    [GROUP_KEY]                   INT          NOT NULL,
    [CAMPAIGN_KEY]                INT          NOT NULL,
    [TENANT_KEY]                  INT          NOT NULL,
    [START_DATE_TIME_KEY]         INT          NOT NULL,
    [END_DATE_TIME_KEY]           INT          NOT NULL,
    [CREATE_AUDIT_KEY]            NUMERIC (19) NOT NULL,
    [UPDATE_AUDIT_KEY]            NUMERIC (19) NOT NULL,
    [START_TS]                    INT          NULL,
    [END_TS]                      INT          NULL,
    [TOTAL_DURATION]              INT          NULL,
    [CAMPAIGN_GROUP_SESSION_ID]   VARCHAR (64) NULL,
    [ACTIVE_FLAG]                 NUMERIC (1)  NULL,
    [PURGE_FLAG]                  NUMERIC (1)  NULL,
    CONSTRAINT [PK_CMP_GR_SN_FT] PRIMARY KEY CLUSTERED ([CAMP_GROUP_SESSION_FACT_KEY] ASC)
);
GO
CREATE UNIQUE NONCLUSTERED INDEX [IWD_I_CGSEF_SID]
    ON [dbo].[IWD_CAMPAIGN_GROUP_SESSION_FACT]([CAMPAIGN_GROUP_SESSION_ID] ASC);

