﻿CREATE TABLE [dbo].[WK_SA_M_CPTESPECES] (
    [DWHPTCETB] INT          NULL,
    [DWHPTCAGC] INT          NULL,
    [DWHPTCNPT] INT          NULL,
    [DWHPTCRCL] VARCHAR (7)  NULL,
    [DWHPTCSFC] INT          NULL,
    [DWHPTCECL] VARCHAR (20) NULL,
    [DWHPTCDCL] DATE         NULL
);

