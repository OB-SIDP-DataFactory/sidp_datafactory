﻿CREATE TABLE [dbo].[PV_FE_SBDCHKACCOUNT] (
    [Id]                        BIGINT          IDENTITY (1, 1) NOT NULL,
    [ACCEPTANCE]                NVARCHAR (255)  NULL,
    [ACCOUNT_NUMBER]            BIGINT          NULL,
    [AGREEMENT_ACCOUNT_NUMBER]  BIGINT          NULL,
    [AUTHORIZED_OVERDRAFT]      DECIMAL (19, 2) NULL,
    [BANK_DOMICILIATION]        BIGINT          NULL,
    [FIRST_DEPOSIT_AMOUNT]      DECIMAL (19, 2) NULL,
    [FIRST_DEPOSIT_AMOUNT_DATE] DATE            NULL,
    [PAYMENT_ID]                NVARCHAR (255)  NULL,
    [ID_FE_CHKACCOUNT]          BIGINT          NOT NULL,
    [AUTH_OVERDRAFT_CURR_ID]    BIGINT          NULL,
    [Validity_StartDate]        DATETIME2 (7)   NOT NULL,
    [Validity_EndDate]          DATETIME2 (7)   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_FE__SBDCHKACCOUNT] PRIMARY KEY CLUSTERED ([Id] ASC)
);

