﻿CREATE TABLE [dbo].[PV_SA_M_INT_MARGE] (
    [Id]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [DWHMM3DTX]          DATE                                        NULL,
    [DWHMM3ETA]          INT                                         NULL,
    [DWHMM3AGE]          INT                                         NULL,
    [DWHMM3SER]          VARCHAR (2)                                 NULL,
    [DWHMM3SES]          VARCHAR (2)                                 NULL,
    [DWHMM3OPE]          VARCHAR (6)                                 NULL,
    [DWHMM3NAT]          VARCHAR (6)                                 NULL,
    [DWHMM3NUM]          INT                                         NULL,
    [DWHMM3SEN]          VARCHAR (1)                                 NULL,
    [DWHMM3SEQ]          INT                                         NULL,
    [DWHMM3DTD]          DATE                                        NULL,
    [DWHMM3DEV]          VARCHAR (3)                                 NULL,
    [DWHMM3REF]          VARCHAR (6)                                 NULL,
    [DWHMM3APP]          VARCHAR (1)                                 NULL,
    [DWHMM3TAU]          DECIMAL (14, 9)                             NULL,
    [DWHMM3MAR]          DECIMAL (14, 9)                             NULL,
    [DWHMM3MRC]          DECIMAL (14, 9)                             NULL,
    [DWHMM3DVA]          DATE                                        NULL,
    [DWHMM3DTR]          DATE                                        NULL,
    [DWHMM3DRG]          DATE                                        NULL,
    [DWHMM3INT]          DECIMAL (18, 3)                             NULL,
    [DWHMM3COU]          DECIMAL (18, 3)                             NULL,
    [DWHMM3DEB]          DATE                                        NULL,
    [DWHMM3FIN]          DATE                                        NULL,
    [DWHMM3ASS]          DECIMAL (18, 3)                             NULL,
    [DWHMM3NBJ]          INT                                         NULL,
    [DWHMM3NBP]          INT                                         NULL,
    [DWHMM3BAS]          INT                                         NULL,
    [DWHMM3MAC]          DECIMAL (18, 3)                             NULL,
    [DWHMM3MIN]          DECIMAL (18, 3)                             NULL,
    [DWHMM3TXA]          DECIMAL (14, 9)                             NULL,
    [DWHMM3INC]          DECIMAL (18, 3)                             NULL,
    [DWHMM3COC]          DECIMAL (18, 3)                             NULL,
    [DWHMM3ASC]          DECIMAL (18, 3)                             NULL,
    [DWHMM3MCC]          DECIMAL (18, 3)                             NULL,
    [DWHMM3MIC]          DECIMAL (18, 3)                             NULL,
    [DWHMM3DSY]          DATE                                        NULL,
    [Validity_StartDate] DATETIME                                    NULL,
    [Validity_EndDate]   DATETIME                                    DEFAULT ('99991231') NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SA_M_INT_MARGE] PRIMARY KEY CLUSTERED ([Id] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SA_M_INT_MARGEHistory], DATA_CONSISTENCY_CHECK=ON));
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE EXTRACTION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3DTX';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ETABLISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3ETA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AGENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3AGE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SERVICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3SER';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SOUS SERVICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3SES';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'OPERATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3OPE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NATURE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3NAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMERO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3NUM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SENS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3SEN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° SEQUENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3SEQ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DEB SITU', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3DTD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEVISE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3DEV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE TAUX', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3REF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE APPLICAT°', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3APP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TAUX FIXE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3TAU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MARGE CLIENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3MAR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MARGE COMMERC.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3MRC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE VAL CLIENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3DVA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE VAL TRESO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3DTR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE REGLEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3DRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'INTERETS DS MOIS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3INT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'INTERETS COURUS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3COU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DEBUT PERIO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3DEB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE FIN PERIODE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3FIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT ASSIETTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3ASS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NB JOUR OPE MOIS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3NBJ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NB JOUR PERIODE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3NBP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'BASE DEVISE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3BAS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONT. MARGE COM.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3MAC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONT. INTS.TRESO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3MIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TAUX D ANALYSE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3TXA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CTVL INTER. MOIS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3INC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CTVL INT.COURUS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3COC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CTVL MNT ASSIETT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3ASC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CTVL MARGE COM.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3MCC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CTVL. INTS.TRESO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3MIC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE SYSTEME', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_INT_MARGE', @level2type = N'COLUMN', @level2name = N'DWHMM3DSY';
GO
CREATE UNIQUE NONCLUSTERED INDEX [IDX_XSIMMM30]
    ON [dbo].[PV_SA_M_INT_MARGE]([DWHMM3ETA] ASC, [DWHMM3AGE] ASC, [DWHMM3SES] ASC, [DWHMM3NAT] ASC, [DWHMM3NUM] ASC, [DWHMM3SEN] ASC, [DWHMM3SEQ] ASC);

