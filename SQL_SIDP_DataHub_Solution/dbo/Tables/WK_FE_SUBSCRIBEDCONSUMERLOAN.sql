﻿CREATE TABLE [dbo].[WK_FE_SUBSCRIBEDCONSUMERLOAN] (
    [ID_FE_SUBSCRIBEDCONSUMERLOAN]   DECIMAL (38)    NOT NULL,
    [ACCOUNT_NUMBER]                 NVARCHAR (11)   NULL,
    [FRANFINANCE_APPLICATION_NUMBER] NVARCHAR (7)    NULL,
    [FRANFINANCE_CONTRACT_NUMBER]    NVARCHAR (11)   NULL,
    [SEPA_MANDATE_NUMBER]            NVARCHAR (255)  NULL,
    [RETRACTION_PERIOD_END_DATE]     DATE            NULL,
    [LOAN_DISBURSMENT_END_DATE]      DATE            NULL,
    [LOAN_DISBURSMENT_START_DATE]    DATE            NULL,
    [RETRACTION_RIGHT_RENOUNCED]     DECIMAL (1)     NULL,
    [COMPLIANCE_COMMIT_ACCEPT_DATE]  DATE            NULL,
    [APPROVAL_DATE]                  DATE            NULL,
    [LOAN_SCORE_COLOR_ID]            DECIMAL (38)    NULL,
    [LOAN_SCORE_VALUE]               DECIMAL (38)    NULL,
    [DEBT_RATE]                      DECIMAL (19, 7) NULL,
    [DEBT_RATE_COLOR_ID]             DECIMAL (38)    NULL,
    [DIRECT_DEBIT_ACCOUNT_TYPE_ID]   DECIMAL (38)    NULL,
    [FRANFINANCE_SCORE_COLOR_ID]     DECIMAL (38)    NULL,
    [BORR_INSUR_COVERAGE_REFUSAL]    DECIMAL (1)     NOT NULL,
    [CO_BORR_INSUR_COVERAGE_REFUSAL] DECIMAL (1)     NOT NULL,
    [DIRECT_DEBIT_COUNTRY_ID]        DECIMAL (38)    NULL,
    [DIRECT_DEBIT_CONTROL_KEY]       NVARCHAR (2)    NULL,
    [DIRECT_DEBIT_BBAN]              NVARCHAR (30)   NULL,
    [CREATION_DATE]                  DATETIME2 (6)   NULL,
    [LAST_UPDATE_DATE]               DATETIME2 (6)   NULL,
    CONSTRAINT [PK_WK_FE_SUBSCRIBEDCONSUMERLOAN] PRIMARY KEY CLUSTERED ([ID_FE_SUBSCRIBEDCONSUMERLOAN] ASC)
);


