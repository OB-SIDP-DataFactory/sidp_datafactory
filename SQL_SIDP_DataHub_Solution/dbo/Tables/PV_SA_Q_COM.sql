﻿CREATE TABLE [dbo].[PV_SA_Q_COM] (
    [COMREFETA] INT          NULL,
    [COMREFPLA] INT          NULL,
    [COMREFCOM] VARCHAR (20) NULL,
    [COMREFCOR] VARCHAR (2)  NULL,
    [COMREFREF] VARCHAR (50) NULL
);
GO
CREATE NONCLUSTERED INDEX IX_NC_PV_SA_COM_REF ON dbo.PV_SA_Q_COM (COMREFCOM) INCLUDE (COMREFREF)
GO