﻿CREATE TABLE [dbo].[PV_MC_CGLOG] (
    [Id]                  BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [AUDIENCE_NAME]       NVARCHAR (200)                              NOT NULL,
    [ID_CAMPAIGN_MESSAGE] NVARCHAR (50)                               NOT NULL,
    [MESSAGE_NAME]        NVARCHAR (200)                              NULL,
    [ID_CAMPAIGN]         NVARCHAR (50)                               NULL,
    [CAMPAIGN_NAME]       NVARCHAR (255)                              NULL,
    [ID_CONTACT]          NVARCHAR (50)                               NOT NULL,
    [DATE_TIME_SEND]      DATETIME                                    NULL,
    [Startdt]             DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]               DATETIME2 (7) GENERATED ALWAYS AS ROW END   NOT NULL,
    CONSTRAINT [PK_PV_MC_CGLOG] PRIMARY KEY CLUSTERED ([Id] ASC, [AUDIENCE_NAME] ASC, [ID_CAMPAIGN_MESSAGE] ASC, [ID_CONTACT] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_MC_CGLOGHistory], DATA_CONSISTENCY_CHECK=ON));


