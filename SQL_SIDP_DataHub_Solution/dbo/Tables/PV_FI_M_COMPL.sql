﻿CREATE TABLE [dbo].[PV_FI_M_COMPL] (
    [Id]                  INT                                         IDENTITY (1, 1) NOT NULL,
    [COD_ENR]             VARCHAR (2)                                 NULL,
    [COD_SEQ]             VARCHAR (6)                                 NULL,
    [NUMERO_ENREG]        VARCHAR (2)                                 NULL,
    [COD_BAN]             VARCHAR (5)                                 NULL,
    [COD_GUI_GES]         VARCHAR (5)                                 NULL,
    [COD_DOM_CPT]         VARCHAR (16)                                NULL,
    [NUM_PRE]             VARCHAR (11)                                NULL,
    [DTE_OUV_CPT]         DATE                                        NULL,
    [DEV_CPT]             VARCHAR (3)                                 NULL,
    [NB_DEC_DEV_CPT]      INT                                         NULL,
    [MTT_CRE_MAX_AUT]     DECIMAL (9, 2)                              NULL,
    [MTT_CRE_MAX_AUT_VIR] DECIMAL (9, 2)                              NULL,
    [MTT_CRE_DIS]         DECIMAL (9, 2)                              NULL,
    [MTT_CRE_UTI]         DECIMAL (9, 2)                              NULL,
    [MTT_CRE_DIS_VIR]     DECIMAL (9, 2)                              NULL,
    [ENC_MOY_MOI]         DECIMAL (9, 2)                              NULL,
    [MTT_ECH_MOI]         DECIMAL (9, 2)                              NULL,
    [TX_MOY]              VARCHAR (5)                                 NULL,
    [COD_STA_SUI_DOS]     VARCHAR (2)                                 NULL,
    [NB_IMP]              INT                                         NULL,
    [DTE_FER_CPT]         DATE                                        NULL,
    [MTT_FRA]             DECIMAL (9, 2)                              NULL,
    [NB_DEM_VIR]          INT                                         NULL,
    [MTT_DEM_VIR]         DECIMAL (9, 2)                              NULL,
    [NB_RET_GUI]          INT                                         NULL,
    [MTT_RET_GUI]         DECIMAL (9, 2)                              NULL,
    [NB_REM_ANT]          INT                                         NULL,
    [MTT_REM_ANT]         DECIMAL (9, 2)                              NULL,
    [MTT_COM]             DECIMAL (9, 2)                              NULL,
    [IDE_GRC]             VARCHAR (8)                                 NULL,
    [DATTTP]              DATE                                        NULL,
    [PHA_PRO_REC]         VARCHAR (1)                                 NULL,
    [MTT_IMP]             DECIMAL (9, 2)                              NULL,
    [NB_IMP_NOU_GES]      INT                                         NULL,
    [DTE_PRE_IMP]         DATE                                        NULL,
    [Validity_StartDate]  DATETIME                                    NOT NULL,
    [Validity_EndDate]    DATETIME                                    DEFAULT ('99991231') NOT NULL,
    [Startdt]             DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]               DATETIME2 (7) GENERATED ALWAYS AS ROW END   NOT NULL,
    CONSTRAINT [PK_PV_FI_M_COMPL] PRIMARY KEY CLUSTERED ([Id] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_FI_M_COMPLHistory], DATA_CONSISTENCY_CHECK=ON));
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de première impayé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'DTE_PRE_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre d''impayé (nouvelle gestion)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'NB_IMP_NOU_GES';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant des impayés', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'MTT_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Phase processus de recouvrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'PHA_PRO_REC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création du fichier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'DATTTP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Référence tires SIEBEL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'IDE_GRC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant des commissions', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'MTT_COM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant des remboursements anticipés', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'MTT_REM_ANT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre de remboursements anticipés', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'NB_REM_ANT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant des retraits via le guichet', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'MTT_RET_GUI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre de retraits via le guichet', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'NB_RET_GUI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant des demandes de virement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'MTT_DEM_VIR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre de demandes de virement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'NB_DEM_VIR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant des frais', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'MTT_FRA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de fermeture du compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'DTE_FER_CPT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre d''impayés', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'NB_IMP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code statut suivi dossier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'COD_STA_SUI_DOS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Taux moyen hors assurance', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'TX_MOY';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant de l''échéance du mois', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'MTT_ECH_MOI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Encours moyen du mois', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'ENC_MOY_MOI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant de crédit disponible virements', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'MTT_CRE_DIS_VIR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant de crédit utilisé toutes opérations', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'MTT_CRE_UTI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant de crédit disponible toutes opérations', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'MTT_CRE_DIS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant de crédit maxi autorisé virements', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'MTT_CRE_MAX_AUT_VIR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Montant de crédit maxi autorisé toutes opérations', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'MTT_CRE_MAX_AUT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre de décimales de la devise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'NB_DEC_DEV_CPT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code devise', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'DEV_CPT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date d''ouverture du compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'DTE_OUV_CPT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Référence du compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'NUM_PRE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Domiciliation compte', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'COD_DOM_CPT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Domiciliation Guichet', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'COD_GUI_GES';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Domiciliation Etablissement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'COD_BAN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numero enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'NUMERO_ENREG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code sequence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'COD_SEQ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_FI_M_COMPL', @level2type = N'COLUMN', @level2name = N'COD_ENR';

