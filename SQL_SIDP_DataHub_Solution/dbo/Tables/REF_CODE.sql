﻿CREATE TABLE [dbo].[REF_CODE] (
    [CODE]          NVARCHAR (255) NULL,
    [END_DATE]      DATETIME       NULL,
    [PRIORITY]      NUMERIC (19)   NULL,
    [REF_FAMILY_ID] NUMERIC (19)   NULL,
    [START_DATE]    DATETIME       NULL,
    [CODE_ID]       NUMERIC (19)   NULL
);

