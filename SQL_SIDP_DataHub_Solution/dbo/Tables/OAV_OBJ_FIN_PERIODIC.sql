﻿CREATE TABLE [dbo].[OAV_OBJ_FIN_PERIODIC] (
    [ID_OBJETS_FINANCEMENTS] DECIMAL (10) NOT NULL,
    [ID_PERIODICITES]        DECIMAL (10) NOT NULL,
    [DAT_OBSR]               DATE         CONSTRAINT [DefValStartDatOAV_OBJ_FIN_PERIODIC] DEFAULT (getdate()) NOT NULL,
    [DAT_CHRG]               DATETIME     CONSTRAINT [DefValEndDatOAV_OBJ_FIN_PERIODIC] DEFAULT (CONVERT([date],'9999-12-31')) NOT NULL,
    CONSTRAINT [PK_PrimaryKeyOBJ_FIN_PERIODIC] PRIMARY KEY CLUSTERED ([ID_OBJETS_FINANCEMENTS] ASC, [ID_PERIODICITES] ASC)
);


