﻿CREATE TABLE [dbo].[IWD_GIDB_G_PARTY_V] (
    [PARTY_KEY]          NUMERIC (19)  IDENTITY (1, 4) NOT NULL,
    [ID]                 NUMERIC (16)  NOT NULL,
    [PARTYID]            VARCHAR (50)  NOT NULL,
    [PARTYGUID]          VARCHAR (50)  NOT NULL,
    [TYPE]               INT           NOT NULL,
    [PROLE]              INT           NOT NULL,
    [PARENTPARTYID]      VARCHAR (50)  NULL,
    [TS_PARENTPARTYGUID] VARCHAR (50)  NULL,
    [PARENTLINKTYPE]     INT           NULL,
    [CALLID]             VARCHAR (50)  NOT NULL,
    [STATE]              INT           NOT NULL,
    [ENDPOINTDN]         VARCHAR (255) NOT NULL,
    [ENDPOINTID]         INT           NOT NULL,
    [ENDPOINTTYPE]       INT           NOT NULL,
    [TENANTID]           INT           NULL,
    [AGENTID]            INT           NULL,
    [CCEVENT]            INT           NULL,
    [CCEVENTCAUSE]       INT           NULL,
    [CREATED]            DATETIME      NOT NULL,
    [CREATED_TS]         INT           NOT NULL,
    [TERMINATED]         DATETIME      NULL,
    [TERMINATED_TS]      INT           NULL,
    [LASTCHANGE]         DATETIME      NOT NULL,
    [LASTCHANGE_TS]      INT           NULL,
    [GSYS_DOMAIN]        INT           NULL,
    [GSYS_SYS_ID]        INT           NULL,
    [GSYS_EXT_VCH1]      VARCHAR (255) NULL,
    [GSYS_EXT_VCH2]      VARCHAR (255) NULL,
    [GSYS_EXT_INT1]      INT           NULL,
    [GSYS_EXT_INT2]      INT           NULL,
    [CREATE_AUDIT_KEY]   NUMERIC (19)  NOT NULL,
    CONSTRAINT [PK_GIDB_G_PARTY_V] PRIMARY KEY NONCLUSTERED ([PARTY_KEY] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IWD_I_G_PT_V_GUID]
    ON [dbo].[IWD_GIDB_G_PARTY_V]([PARTYGUID] ASC);


GO
CREATE NONCLUSTERED INDEX [IWD_I_G_PT_V_CID]
    ON [dbo].[IWD_GIDB_G_PARTY_V]([CALLID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IWD_I_G_PT_V_PID]
    ON [dbo].[IWD_GIDB_G_PARTY_V]([PARTYID] ASC);


GO
CREATE NONCLUSTERED INDEX [IWD_I_G_PT_V_CTS]
    ON [dbo].[IWD_GIDB_G_PARTY_V]([CREATED_TS] ASC);

