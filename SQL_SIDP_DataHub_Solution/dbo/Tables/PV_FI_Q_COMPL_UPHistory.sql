﻿CREATE TABLE [dbo].[PV_FI_Q_COMPL_UPHistory] (
    [Id]                 INT             NOT NULL,
    [COD_ENR]            VARCHAR (2)     NULL,
    [IDE_GRC]            VARCHAR (8)     NULL,
    [IDE_GRC_COE]        VARCHAR (8)     NULL,
    [TYP_COM]            VARCHAR (5)     NULL,
    [NUM_PRE]            VARCHAR (11)    NULL,
    [TYP_ENR]            VARCHAR (2)     NULL,
    [NUM_UP]             VARCHAR (10)    NULL,
    [LIB_UP]             VARCHAR (30)    NULL,
    [DTE_UP]             DATE            NULL,
    [DUR_INI_UP]         VARCHAR (3)     NULL,
    [DUR_RES_UP]         VARCHAR (3)     NULL,
    [TAU_AGIO_UP]        DECIMAL (11, 2) NULL,
    [TAU_AGIO_TAEG]      DECIMAL (11, 2) NULL,
    [DTE_SAI_UP]         DATE            NULL,
    [MTT_INI_UP]         DECIMAL (11, 2) NULL,
    [DTE_ECH]            DATE            NULL,
    [MTT_CRD_UP]         DECIMAL (11, 2) NULL,
    [DTE_PRO_ECH]        DATE            NULL,
    [MTT_PRO_ECH_UP]     DECIMAL (11, 2) NULL,
    [Validity_StartDate] DATETIME        NOT NULL,
    [Validity_EndDate]   DATETIME        NOT NULL,
    [Startdt]            DATETIME2 (7)   NOT NULL,
    [Enddt]              DATETIME2 (7)   NOT NULL
);
GO

