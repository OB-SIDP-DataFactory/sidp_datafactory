﻿CREATE TABLE [dbo].[PV_SA_M_CARTE] (
    [ID]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [DWHCARDTX]          DATE                                        NULL,
    [DWHCARETB]          INT                                         NULL,
    [DWHCARCAR]          VARCHAR (6)                                 NULL,
    [DWHCARCOM]          VARCHAR (20)                                NULL,
    [DWHCARNUS]          INT                                         NULL,
    [DWHCARNUC]          VARCHAR (19)                                NULL,
    [DWHCARNPO]          VARCHAR (7)                                 NULL,
    [DWHCARTPO]          VARCHAR (1)                                 NULL,
    [DWHCARLIB]          VARCHAR (42)                                NULL,
    [DWHCARNAT]          VARCHAR (1)                                 NULL,
    [DWHCARTYP]          VARCHAR (1)                                 NULL,
    [DWHCAROPT]          VARCHAR (1)                                 NULL,
    [DWHCARUTI]          VARCHAR (1)                                 NULL,
    [DWHCARVAL]          DATE                                        NULL,
    [DWHCARDUR]          INT                                         NULL,
    [DWHCARAGE]          INT                                         NULL,
    [DWHCARHSE]          VARCHAR (1)                                 NULL,
    [DWHCARENR]          DATE                                        NULL,
    [DWHCARREC]          DATE                                        NULL,
    [DWHCARREM]          DATE                                        NULL,
    [DWHCARCRE]          VARCHAR (1)                                 NULL,
    [DWHCARREN]          VARCHAR (1)                                 NULL,
    [DWHCARRES]          VARCHAR (1)                                 NULL,
    [DWHCARDRN]          DATE                                        NULL,
    [DWHCARDRS]          DATE                                        NULL,
    [DWHCARENV]          DATE                                        NULL,
    [DWHCARAUT]          VARCHAR (1)                                 NULL,
    [DWHCARLIV]          VARCHAR (5)                                 NULL,
    [DWHCARACH]          DECIMAL (18, 3)                             NULL,
    [DWHCARMRA]          DECIMAL (18, 3)                             NULL,
    [DWHCARCET]          VARCHAR (3)                                 NULL,
    [DWHCARVIS]          VARCHAR (3)                                 NULL,
    [DWHCARACA]          VARCHAR (19)                                NULL,
    [DWHCARCRA]          VARCHAR (1)                                 NULL,
    [DWHCARINV]          DATE                                        NULL,
    [Validity_StartDate] DATETIME                                    NULL,
    [Validity_EndDate]   DATETIME                                    DEFAULT ('99991231') NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SA_M_CARTE] PRIMARY KEY CLUSTERED ([ID] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SA_M_CARTEHistory], DATA_CONSISTENCY_CHECK=ON));
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE INVALIDITE CART', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARINV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CREATION AUTOMATIQUE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARCRA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMÉRO ANCIENNE CART', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARACA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE VISUEL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARVIS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE ETAT DE LA CART', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARCET';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT PLAFOND RETRAIT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARMRA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT PLAFOND ACHAT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARACH';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE LIVRAISON', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARLIV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RENOUVELLEMENT AUTO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARAUT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DT ENV AU  FACONNIER', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARENV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE RESILIATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARDRS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE RENOUVELLEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARDRN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RESILIE DS MOIS(1/0)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARRES';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RENOUVELÉE MOIS(1/0)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARREN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CARTE CRÉE MOIS(1/0)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARCRE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE  REMISE  CARTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARREM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE RECEPTION CARTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARREC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE COMMANDE CARTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARENR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MISE HORS SERVICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARHSE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AGENCE GESTIONNAIRE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARAGE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DURÉE DE VALIDITÉ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARDUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DE VALIDITÉ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARVAL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'UTILISATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARUTI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'OPTION DE DÉBIT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCAROPT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE CARTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARTYP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NATURE CARTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARNAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIBELLÉ CODE CARTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARLIB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE DE PORTEUR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARTPO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMÉRO CLIENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARNPO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMÉRO CARTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARNUC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMÉRO SÉQUENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARNUS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMÉRO COMPTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARCOM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE CARTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARCAR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ETABLISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARETB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ANALYSE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_CARTE', @level2type = N'COLUMN', @level2name = N'DWHCARDTX';

