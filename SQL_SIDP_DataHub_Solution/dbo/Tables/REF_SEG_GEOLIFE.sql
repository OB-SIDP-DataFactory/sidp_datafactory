﻿CREATE TABLE [dbo].[REF_SEG_GEOLIFE] (
    [ID_SEG_GEO]  BIGINT         IDENTITY (1, 1) NOT NULL,
    [LIB_SEG_GEO] NVARCHAR (255) NULL,
    CONSTRAINT [PK_REF_SEG_GEOLIFE] PRIMARY KEY CLUSTERED ([ID_SEG_GEO] ASC)
);

