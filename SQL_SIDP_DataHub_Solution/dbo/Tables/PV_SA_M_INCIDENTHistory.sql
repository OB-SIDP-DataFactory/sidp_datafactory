﻿CREATE TABLE [dbo].[PV_SA_M_INCIDENTHistory] (
    [ID]                 BIGINT          NOT NULL,
    [DWHFIMDTX]          DATE            NULL,
    [DWHFIMETA]          INT             NULL,
    [DWHFIMTYP]          VARCHAR (2)     NULL,
    [DWHFIMNUM]          INT             NULL,
    [DWHFIMCLT]          VARCHAR (7)     NULL,
    [DWHFIMPLA]          INT             NULL,
    [DWHFIMCPT]          VARCHAR (20)    NULL,
    [DWHFIMNUC]          INT             NULL,
    [DWHFIMSEQ]          INT             NULL,
    [DWHFIMEVE]          DATE            NULL,
    [DWHFIMDEV]          VARCHAR (3)     NULL,
    [DWHFIMMON]          DECIMAL (18, 3) NULL,
    [DWHFIMMOB]          DECIMAL (18, 3) NULL,
    [Validity_StartDate] DATETIME        NULL,
    [Validity_EndDate]   DATETIME        NULL,
    [Startdt]            DATETIME2 (7)   NOT NULL,
    [Enddt]              DATETIME2 (7)   NOT NULL
);

