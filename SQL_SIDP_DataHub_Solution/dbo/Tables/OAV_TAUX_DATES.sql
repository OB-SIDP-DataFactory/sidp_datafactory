﻿CREATE TABLE [dbo].[OAV_TAUX_DATES] (
    [ID_TAUX_DATES] DECIMAL (10)  NOT NULL,
    [START_DATE]    DATETIME2 (7) NOT NULL,
    [END_DATE]      DATETIME2 (7) NOT NULL,
    [DAT_OBSR]      DATE          CONSTRAINT [DefValStartDatOAV_TAUX_DATES] DEFAULT (getdate()) NOT NULL,
    [DAT_CHRG]      DATETIME      CONSTRAINT [DefValEndDatOAV_TAUX_DATES] DEFAULT (CONVERT([date],'9999-12-31')) NOT NULL,
    CONSTRAINT [PK_PrimaryKeyTAUX_DATES] PRIMARY KEY CLUSTERED ([ID_TAUX_DATES] ASC)
);


