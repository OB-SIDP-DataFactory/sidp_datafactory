﻿CREATE TABLE [dbo].[WK_SF_REFERENTIEL] (
    [ID]                   BIGINT          IDENTITY (1, 1) NOT NULL,
    [OBJECT_CODE_SF]       NVARCHAR (255)  NULL,
    [FIELD_CODE_SF]        NVARCHAR (255)  NULL,
    [FIELD_NAME_SF]        NVARCHAR (255)  NULL,
    [PARENT_FIELD_CODE_SF] NVARCHAR (255)  NULL,
    [IDX_SF]               INT             NULL,
    [CODE_SF]              NVARCHAR (255)  NULL,
    [LIBELLE_SF]           NVARCHAR (255)  NULL,
    [IS_DEFAULT]           BIT             NULL,
    [IS_ACTIVE]            BIT             NULL,
    [VALID_FOR]            NVARCHAR (1000) NULL,
    [VALID_FOR_MAP]        NVARCHAR (1000) NULL, 
    CONSTRAINT [PK_WK_SF_REFERENTIEL] PRIMARY KEY ([ID])
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant technique SIDP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'WK_SF_REFERENTIEL', @level2type = N'COLUMN', @level2name = N'ID';

