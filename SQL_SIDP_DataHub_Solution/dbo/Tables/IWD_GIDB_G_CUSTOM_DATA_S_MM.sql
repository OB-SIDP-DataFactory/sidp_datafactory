﻿CREATE TABLE [dbo].[IWD_GIDB_G_CUSTOM_DATA_S_MM] (
    [ID]               NUMERIC (19)  NOT NULL,
    [CALLID]           VARCHAR (50)  DEFAULT ('none') NOT NULL,
    [PARTYID]          VARCHAR (50)  NULL,
    [PSEQ]             INT           NOT NULL,
    [ENDPOINTID]       INT           NULL,
    [ENDPOINTDN]       VARCHAR (255) NULL,
    [AGENTID]          INT           NULL,
    [SWITCHID]         INT           NULL,
    [TENANTID]         INT           NULL,
    [KEYNAME]          VARCHAR (64)  NOT NULL,
    [VALUE]            VARCHAR (255) NULL,
    [ADDED]            DATETIME      NULL,
    [ADDED_TS]         INT           NOT NULL,
    [GSYS_DOMAIN]      INT           NULL,
    [GSYS_SYS_ID]      INT           NULL,
    [GSYS_EXT_INT2]    INT           NULL,
    [CREATE_AUDIT_KEY] NUMERIC (19)  NOT NULL,
    CONSTRAINT [PK_GIDB_G_CUSTOM_DATA_S_MM] PRIMARY KEY CLUSTERED ([CALLID] ASC, [ID] ASC, [CREATE_AUDIT_KEY] ASC, [ADDED_TS] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IWD_I_G_CDH_MM_ADDTS]
    ON [dbo].[IWD_GIDB_G_CUSTOM_DATA_S_MM]([ADDED_TS] ASC);

