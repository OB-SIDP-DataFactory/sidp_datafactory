﻿CREATE TABLE [dbo].[MGM_ETL_PILOTE] (
    [CD_TABLE_MGM]                   VARCHAR (255)  NOT NULL,
    [CHAMP_PK]                       VARCHAR (255)  NULL,
    [TOP_TABLE_FAIT]                 BIT            NOT NULL,
    [TOP_TABLE_DIM]                  BIT            NOT NULL,
    [CD_TABLE_IMPORT]                VARCHAR (255)  NULL,
    [CD_TABLE_HISTO]                 VARCHAR (255)  NULL,
    [LOT]                            TINYINT        NULL,
    [SCRIPT_TRUNCATE]                NVARCHAR (255) NULL,
    [SCRIPT_ALIM_IMPORT]             NVARCHAR (500) NULL,
    [SCRIPT_ALIM_HISTO]              NVARCHAR (500) NULL,
    [CHAMP_START_DATE_TIME_KEY]      VARCHAR (255)  NULL,
    [SCRIPT_SELECT_IMPORT]           NVARCHAR (512) NULL,
    [SCRIPT_DELETE_HISTO]            NVARCHAR (255) NULL,
    [NB_ENREG_VUE]                   INT            NULL,
    [SCRIPT_START_DATE_TIME_KEY_MAX] NVARCHAR (512) NULL,
    [CHAMP_END_DATE_TIME_KEY]        VARCHAR (255)  NULL,
    [SCRIPT_END_DATE_TIME_KEY_MAX]   NVARCHAR (512) NULL,
    [CHAMP_CREATE_AUDIT_KEY]         VARCHAR (255)  NULL,
    [SCRIPT_CREATE_AUDIT_KEY_MAX]    NVARCHAR (512) NULL,
    [CHAMP_UPDATE_AUDIT_KEY]         VARCHAR (255)  NULL,
    [SCRIPT_UPDATE_AUDIT_KEY_MAX]    NVARCHAR (512) NULL,
    [TOP_TABLE_GIDB]                 BIT            NULL,
    [CHAMP_LASTCHANGE]               VARCHAR (255)  NULL,
    [SCRIPT_LASTCHANGE_MAX]          NVARCHAR (512) NULL,
    [CD_VUE_INFOMART]                VARCHAR (128)  NULL,
    [TOP_GENERATION_SCRIPT_BLOQUE]   BIT            NULL,
    [NB_ENREG_HISTO]                 INT            NULL,
    [PS_ALIM_HISTO]                  VARCHAR (255)  NULL,
    [NO_ORDRE_LOT]                   SMALLINT       NULL,
    [TOP_DECALAGE_NJOURS]            BIT            NULL,
    CONSTRAINT [PK_MTG_PILOTE_ETL_NEW] PRIMARY KEY CLUSTERED ([CD_TABLE_MGM] ASC)
);

