﻿CREATE TABLE [dbo].[PV_SA_M_GARANTIE] (
    [ID]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [DWHGARDTX]          DATE                                        NULL,
    [DWHGARETA]          INT                                         NULL,
    [DWHGARAGE]          INT                                         NULL,
    [DWHGARSER]          VARCHAR (2)                                 NULL,
    [DWHGARSSE]          VARCHAR (2)                                 NULL,
    [DWHGAROPE]          VARCHAR (6)                                 NULL,
    [DWHGARNAT]          VARCHAR (6)                                 NULL,
    [DWHGARNDO]          INT                                         NULL,
    [DWHGARSEQ]          INT                                         NULL,
    [DWHGARDEV]          VARCHAR (3)                                 NULL,
    [DWHGARDEB]          DATE                                        NULL,
    [DWHGARFIN]          DATE                                        NULL,
    [DWHGARFLG]          VARCHAR (1)                                 NULL,
    [DWHGARTGA]          VARCHAR (1)                                 NULL,
    [DWHGARRAN]          INT                                         NULL,
    [DWHGARUSA]          VARCHAR (6)                                 NULL,
    [DWHGARDUR]          INT                                         NULL,
    [DWHGARRES]          INT                                         NULL,
    [DWHGARMOP]          DECIMAL (18, 3)                             NULL,
    [DWHGARMBS]          DECIMAL (18, 3)                             NULL,
    [DWHGAREOP]          DECIMAL (18, 3)                             NULL,
    [DWHGAREBA]          DECIMAL (18, 3)                             NULL,
    [DWHGARMNT]          DECIMAL (18, 3)                             NULL,
    [DWHGARDAT]          DATE                                        NULL,
    [DWHGARTDO]          VARCHAR (1)                                 NULL,
    [DWHGARIDO]          VARCHAR (7)                                 NULL,
    [DWHGARIDB]          VARCHAR (7)                                 NULL,
    [DWHGARCOQ]          VARCHAR (3)                                 NULL,
    [DWHGARPAY]          VARCHAR (3)                                 NULL,
    [DWHGARDOU]          VARCHAR (1)                                 NULL,
    [DWHGARFLJ]          VARCHAR (1)                                 NULL,
    [DWHGARNUJ]          VARCHAR (9)                                 NULL,
    [DWHGARTYP]          VARCHAR (1)                                 NULL,
    [DWHGARTOP]          VARCHAR (1)                                 NULL,
    [DWHGARPGA]          DECIMAL (9, 3)                              NULL,
    [DWHGARPMI]          DECIMAL (9, 3)                              NULL,
    [DWHGARPRC]          DECIMAL (6, 3)                              NULL,
    [DWHGARRIS]          VARCHAR (1)                                 NULL,
    [DWHGARFRE]          VARCHAR (1)                                 NULL,
    [Validity_StartDate] DATETIME                                    NOT NULL,
    [Validity_EndDate]   DATETIME                                    CONSTRAINT [DF__PV_SA_M_Garantie__Valid] DEFAULT ('99991231') NOT NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SA_M_GARANTIE] PRIMARY KEY CLUSTERED ([ID] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SA_M_GARANTIEHistory], DATA_CONSISTENCY_CHECK=ON));
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FRÉQUENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARFRE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PÉRIMÈTRE RISQUE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARRIS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'%COUVERTURE GARANTIE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARPRC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PONDÉRAT° MINIMUM', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARPMI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PONDÉRAT° DU GARANT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARPGA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TOP ELIG. BÂLE II', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARTOP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYP RÉDUCTEUR RISQUE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARTYP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMÉRO SOUS-JACENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARNUJ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FLAG SOUS-JACENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARFLJ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DOUTEUX', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARDOU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PAYS DE RÉSIDENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARPAY';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE QUALITÉ GARANT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARCOQ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IDENT. BÉNÉFICIAIRE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARIDB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IDENT. DONNEUR ORDRE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARIDO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE DONNEUR ORDRE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARTDO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE EST. DE GARANTI', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARDAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONT EST. OPÉRATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARMNT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ENCOURS BASE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGAREBA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ENCOURS OPÉRATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGAREOP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT INI. BASE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARMBS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONT INI. OPÉRATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARMOP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DURÉE REST. À COURIR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARRES';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DURÉE INITIALE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARDUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'USAGE DE BIEN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARUSA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RANG DE LA GARANTIE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARRAN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE DE GARANTIE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARTGA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FLAG GARANTIE ENG.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARFLG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE FIN GARANTIE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARFIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DEB. GARANTIE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARDEB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEVISE DE GARANTIE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARDEV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMERO SEQUENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARSEQ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMERO DOSSIER', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARNDO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE NATURE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARNAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE OPERATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGAROPE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SOUS-SERVICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARSSE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SERVICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARSER';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AGENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARAGE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ETABLISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARETA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ANALYSE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_GARANTIE', @level2type = N'COLUMN', @level2name = N'DWHGARDTX';

