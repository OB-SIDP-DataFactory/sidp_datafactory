﻿CREATE TABLE [dbo].[IWD_TMP_GCX_SKILL_LEVEL] (
    [ID]               NUMERIC (16)  NOT NULL,
    [SKILLID]          INT           NOT NULL,
    [AGENTID]          INT           NOT NULL,
    [SLEVEL]           INT           NULL,
    [STATUS]           INT           NOT NULL,
    [CREATED]          DATETIME      NULL,
    [DELETED]          DATETIME      NULL,
    [CREATED_TS]       INT           NULL,
    [DELETED_TS]       INT           NULL,
    [GSYS_DOMAIN]      INT           NULL,
    [GSYS_SYS_ID]      INT           NULL,
    [GSYS_EXT_VCH1]    VARCHAR (255) NULL,
    [GSYS_EXT_VCH2]    VARCHAR (255) NULL,
    [GSYS_EXT_INT1]    INT           NULL,
    [GSYS_EXT_INT2]    INT           NULL,
    [CREATE_AUDIT_KEY] NUMERIC (19)  NOT NULL,
    [UPDATE_AUDIT_KEY] NUMERIC (19)  NOT NULL
);

