﻿CREATE TABLE [dbo].[WK_FE_REF_CONSUMER_LOAN_MGT_RT_ST] (
    [MGT_RT_ST_ID]           DECIMAL (38)   NOT NULL,
    [FRANFINANCE_CODE]       NVARCHAR (5)   NULL,
    [FRANFINANCE_COLOR_CODE] NVARCHAR (1)   NULL,
    [SALESFORCE_CODE]        NVARCHAR (2)   NULL,
    [REF_FAMILY_ID]          DECIMAL (19)   NULL,
    [CODE]                   NVARCHAR (255) NULL,
    [PRIORITY]               DECIMAL (19)   NULL,
    [START_DATE]             DATETIME2 (7)  NULL,
    [END_DATE]               DATETIME2 (7)  NULL,
    [REF_LANG_ID]            DECIMAL (19)   NULL,
    [MESSAGE]                NVARCHAR (255) NULL
);

