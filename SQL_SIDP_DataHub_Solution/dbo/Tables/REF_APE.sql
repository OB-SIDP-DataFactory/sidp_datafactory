﻿CREATE TABLE [dbo].[REF_APE] (
    [ID]                    BIGINT       IDENTITY (1, 1) NOT NULL,
    [REF_APE_ETABLISSEMENT] INT          NULL,
    [REF_APE_IDENTIFIANT]   VARCHAR (3)  NULL,
    [REF_APE_APE]           VARCHAR (6)  NULL,
    [REF_APE_ABREGE]        VARCHAR (12) NULL,
    [REF_APE_LIBELLE]       VARCHAR (30) NULL,
    CONSTRAINT [PK_REF_APE] PRIMARY KEY CLUSTERED ([ID] ASC)
);

