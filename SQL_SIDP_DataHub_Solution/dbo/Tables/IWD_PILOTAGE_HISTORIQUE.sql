﻿CREATE TABLE [dbo].[IWD_PILOTAGE_HISTORIQUE] (
    [Table_source]    VARCHAR (100) NULL,
    [Table_cible]     VARCHAR (100) NULL,
    [Dte_Deb_periode] DATE          NULL,
    [dte_Fin_periode] DATE          NULL,
    [dte_deb_exec]    DATETIME      NULL,
    [dte_fin_exec]    DATETIME      NULL,
    [statut]          VARCHAR (20)  NULL,
    [commentaire]     VARCHAR (100) NULL,
    [requete]         VARCHAR (MAX) NULL
)
GO
CREATE NONCLUSTERED INDEX IX_NC_IWD_PIL_HIS_TAB_DTE
ON [dbo].[IWD_PILOTAGE_HISTORIQUE] ([Table_cible], [dte_deb_exec])
INCLUDE ([commentaire])
GO