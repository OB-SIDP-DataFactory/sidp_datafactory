﻿CREATE TABLE [dbo].[PV_SA_Q_CLIENT] (
    [ID]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [DWHCLIDTX]          DATE                                        NULL,
    [DWHCLIETB]          INT                                         NULL,
    [DWHCLICLI]          VARCHAR (7)                                 NULL,
    [DWHCLIAGE]          INT                                         NULL,
    [DWHCLIETA]          VARCHAR (4)                                 NULL,
    [DWHCLIRA1]          VARCHAR (32)                                NULL,
    [DWHCLIRA2]          VARCHAR (32)                                NULL,
    [DWHCLISIG]          VARCHAR (12)                                NULL,
    [DWHCLISRN]          VARCHAR (9)                                 NULL,
    [DWHCLISRT]          INT                                         NULL,
    [DWHCLIDCR]          DATE                                        NULL,
    [DWHCLICRT]          DATE                                        NULL,
    [DWHCLIDNA]          DATE                                        NULL,
    [DWHCLINAT]          VARCHAR (3)                                 NULL,
    [DWHCLICRE]          VARCHAR (1)                                 NULL,
    [DWHCLIRSD]          VARCHAR (3)                                 NULL,
    [DWHCLIRES]          VARCHAR (3)                                 NULL,
    [DWHCLIECO]          VARCHAR (3)                                 NULL,
    [DWHCLISAC]          VARCHAR (6)                                 NULL,
    [DWHCLIPCS]          VARCHAR (4)                                 NULL,
    [DWHCLIREG]          VARCHAR (6)                                 NULL,
    [DWHCLICAT]          VARCHAR (3)                                 NULL,
    [DWHCLIGEO]          VARCHAR (3)                                 NULL,
    [DWHCLIENT]          VARCHAR (3)                                 NULL,
    [DWHCLIACT]          VARCHAR (1)                                 NULL,
    [DWHCLICRD]          VARCHAR (3)                                 NULL,
    [DWHCLICOT]          VARCHAR (3)                                 NULL,
    [DWHCLIDIR]          VARCHAR (1)                                 NULL,
    [DWHCLIPRF]          VARCHAR (3)                                 NULL,
    [DWHCLINBE]          INT                                         NULL,
    [DWHCLIRGM]          VARCHAR (2)                                 NULL,
    [DWHCLIEFC]          INT                                         NULL,
    [DWHCLICHF]          DECIMAL (18, 3)                             NULL,
    [DWHCLIANF]          DATE                                        NULL,
    [DWHCLIJUR]          VARCHAR (2)                                 NULL,
    [DWHCLIREV]          DECIMAL (18, 3)                             NULL,
    [DWHCLICHA]          DECIMAL (18, 3)                             NULL,
    [DWHCLIPAT]          DECIMAL (18, 3)                             NULL,
    [DWHCLISER]          VARCHAR (3)                                 NULL,
    [DWHCLISEP]          VARCHAR (3)                                 NULL,
    [DWHCLIDBA]          VARCHAR (1)                                 NULL,
    [DWHCLIDOU]          VARCHAR (2)                                 NULL,
    [DWHCLIINT]          VARCHAR (1)                                 NULL,
    [DWHCLITYI]          VARCHAR (2)                                 NULL,
    [DWHCLICLO]          DATE                                        NULL,
    [DWHCLIDEC]          VARCHAR (1)                                 NULL,
    [DWHCLIPNA]          VARCHAR (3)                                 NULL,
    [DWHCLIFAM]          VARCHAR (2)                                 NULL,
    [DWHCLICOL]          VARCHAR (1)                                 NULL,
    [DWHCLIGRI]          VARCHAR (7)                                 NULL,
    [DWHCLITRI]          VARCHAR (7)                                 NULL,
    [DWHCLIDLC]          DATE                                        NULL,
    [DWHCLIMOT]          VARCHAR (6)                                 NULL,
    [DWHCLIPRO]          VARCHAR (6)                                 NULL,
    [DWHCLIDOM]          VARCHAR (2)                                 NULL,
    [DWHCLIEST]          VARCHAR (1)                                 NULL,
    [DWHCLIVA1]          VARCHAR (6)                                 NULL,
    [DWHCLIVA2]          VARCHAR (6)                                 NULL,
    [DWHCLIVA3]          VARCHAR (6)                                 NULL,
    [DWHCLIVA4]          VARCHAR (6)                                 NULL,
    [DWHCLIVA5]          VARCHAR (6)                                 NULL,
    [DWHCLIVA6]          VARCHAR (6)                                 NULL,
    [DWHCLIVA7]          VARCHAR (6)                                 NULL,
    [DWHCLIVA8]          VARCHAR (6)                                 NULL,
    [DWHCLIVA9]          VARCHAR (6)                                 NULL,
    [DWHCLIV10]          VARCHAR (6)                                 NULL,
    [DWHCLIV11]          VARCHAR (6)                                 NULL,
    [DWHCLIV12]          VARCHAR (6)                                 NULL,
    [DWHCLILA1]          VARCHAR (1)                                 NULL,
    [DWHCLILA2]          VARCHAR (1)                                 NULL,
    [DWHCLIRU1]          VARCHAR (20)                                NULL,
    [DWHCLIRU2]          VARCHAR (15)                                NULL,
    [DWHCLIDA1]          DATE                                        NULL,
    [DWHCLIDA2]          DATE                                        NULL,
    [DWHCLIMO1]          DECIMAL (18, 3)                             NULL,
    [DWHCLIBIL]          INT                                         NULL,
    [DWHCLIBIM]          INT                                         NULL,
    [DWHCLIATR]          DATE                                        NULL,
    [DWHCLIBLN]          VARCHAR (6)                                 NULL,
    [DWHCLISTA]          VARCHAR (1)                                 NULL,
    [DWHCLITIE]          VARCHAR (7)                                 NULL,
    [DWHCLIMUT]          DATE                                        NULL,
    [DWHCLIACC]          DATE                                        NULL,
    [DWHCLIDGI]          VARCHAR (1)                                 NULL,
    [DWHCLIFOJ]          VARCHAR (4)                                 NULL,
    [DWHCLINFI]          VARCHAR (32)                                NULL,
    [DWHCLIAD1]          VARCHAR (38)                                NULL,
    [DWHCLICOP]          VARCHAR (12)                                NULL,
    [DWHCLIVIL]          VARCHAR (32)                                NULL,
    [DWHCLIDEP]          VARCHAR (2)                                 NULL,
    [DWHCLICDN]          VARCHAR (2)                                 NULL,
    [DWHCLICCN]          VARCHAR (32)                                NULL,
    [DWHCLICPI]          VARCHAR (2)                                 NULL,
    [DWHCLIIDT]          VARCHAR (3)                                 NULL,
    [DWHCLIIDN]          VARCHAR (17)                                NULL,
    [DWHCLIPYR]          VARCHAR (2)                                 NULL,
    [DWHCLIPYN]          VARCHAR (2)                                 NULL,
    [DWHCLIMBI]          DECIMAL (18, 3)                             NULL,
    [DWHCLICON]          VARCHAR (5)                                 NULL,
    [DWHCLICIB]          VARCHAR (5)                                 NULL,
    [DWHCLITTI]          VARCHAR (1)                                 NULL,
    [DWHCLIDAS]          VARCHAR (8)                                 NULL,
    [DWHCLICPJ]          VARCHAR (6)                                 NULL,
    [DWHCLIDPJ]          VARCHAR (8)                                 NULL,
    [DWHCLIRIS]          VARCHAR (1)                                 NULL,
    [DWHCLIFRE]          VARCHAR (1)                                 NULL,
    [DWHCLIDSY]          DATE                                        NULL,
    [Validity_StartDate] DATETIME                                    NULL,
    [Validity_EndDate]   DATETIME                                    DEFAULT ('99991231') NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   NOT NULL,
    CONSTRAINT [PK_PV_SA_Q_CLIENT] PRIMARY KEY CLUSTERED ([ID] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SA_Q_CLIENTHistory], DATA_CONSISTENCY_CHECK=ON));
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ANALYSE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIDTX';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE ETABLISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIETB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMERO CLIENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLICLI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE AGENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIAGE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE ETAT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIETA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NOM OU DESIGNATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIRA1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PRENOM/DESIGNATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIRA2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SIGLE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLISIG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE SIREN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLISRN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE SIRET', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLISRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE CREATION CLIENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIDCR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE CREAT° ENTREPRI', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLICRT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DE NAISSANCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIDNA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CDE PAYS NATIONALITE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLINAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE CLIENT RESIDENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLICRE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE PAYS RESIDENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIRSD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RESPONS/EXPLOITATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIRES';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE QUALITÉ CLIENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIECO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SECTEUR ACTIVITE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLISAC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE PCS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIPCS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SECT ACTIVITE REGLEM', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIREG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CATEGORIE CLIENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLICAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SECTEUR GEOGRAPHIQUE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIGEO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ENTREPRISE LIEE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIENT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COTE ACTIVITÉ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIACT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COTE CRÉDIT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLICRD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COTATION INTERNE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLICOT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COTE DIRIGEANT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIDIR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE PROFESSION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIPRF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NOMBRE D"ENFANTS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLINBE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE RÉGIME MATRIMON', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIRGM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'EFFECTIF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIEFC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CHIFFRE AFFAIRES', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLICHF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ANNÉE CHIFF. AFFAIRE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIANF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CAPACITÉ JURIDIQUE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIJUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT REVENUS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIREV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT CHARGES', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLICHA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT PATRIMOINE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIPAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE SEGMENT RESULTA', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLISER';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE SEGMENT POTENTI', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLISEP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE DOUTEUX REGLEME', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIDBA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CLIENT DOUTEUX O/N', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIDOU';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'INTERDICT° CHÉQUIER', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIINT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE INTERDIT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLITYI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DE CLOTURE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLICLO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'INDICATEUR DECES O/N', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIDEC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PAYS DE NAISSANCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIPNA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SITUATION FAMILIALE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIFAM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IND.TIERS COLLEC.1/0', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLICOL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CLIENT GROUPE RISQUE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIGRI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TÊTE DE GROUPE RISQU', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLITRI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ENTRÉE LIT/CONT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIDLC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MOTIF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIMOT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PROCEDURE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIPRO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COTATION IEDOM', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIDOM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'AUTORISÉ AU PRODUIT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIEST';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE VALEUR 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIVA1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE VALEUR 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIVA2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE VALEUR 3', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIVA3';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE VALEUR 4', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIVA4';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE VALEUR 5', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIVA5';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE VALEUR 6', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIVA6';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE VALEUR 7', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIVA7';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE VALEUR 8', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIVA8';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE VALEUR 9', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIVA9';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE VALEUR 10', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIV10';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE VALEUR 11', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIV11';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE VALEUR 12', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIV12';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ZONE LANGAGE 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLILA1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ZONE LANGAGE 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLILA2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ZONE RUBRIQUE 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIRU1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ZONE RUBRIQUE 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIRU2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIDA1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIDA2';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIMO1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ANNÉE DERNIER BILAN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIBIL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MOIS DERNIER BILAN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIBIM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ATTRIBUTION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIATR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE BUSINESS LINE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIBLN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE STATUT CLIENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLISTA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CLIENT DE REFERENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLITIE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE INITIAT° CLIENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIMUT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ACCEPTAT°CLIENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIACC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE EQUIV.DGI', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIDGI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FORME JURIDIQUE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIFOJ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NOM JEUNE FILLE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLINFI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ADR. NUM VOIE CRI', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIAD1';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE POSTAL   CRI', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLICOP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'VILLE         CRI', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIVIL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CD DEP RISQUE BDF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIDEP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE DEPART(NAISS)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLICDN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'LIBEL COMM (NAISS)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLICCN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE PAYS NAISS ISO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLICPI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IDE.NAT TP IDENT.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIIDT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IDE.NAT NU IDENT.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIIDN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CD PAYS RESIDENC ISO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIPYR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CD PAYS NATIONAL ISO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIPYN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PÉRIMÈTRE RISQUE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIRIS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FRÉQUENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIFRE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE DE TIERS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLITTI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT DU BILAN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIMBI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ATTRI STAT DÉFT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIDPJ';
GO
EXECUTE sp_addextendedproperty @name = N'DATE PROCE JUDICIAIR', @value = N'DATE ATTRI STAT DÉFT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIDPJ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ATTRI STAT DÉFT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLIDAS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE ATTRI STAT DÉFT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLICPJ';
GO
EXECUTE sp_addextendedproperty @name = N'CODE PROCE JUDICIAIR', @value = N'DATE ATTRI STAT DÉFT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLICPJ';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE COMMU NAISSANCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLICON';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CIB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_Q_CLIENT', @level2type = N'COLUMN', @level2name = N'DWHCLICIB';
GO
/*
CREATE NONCLUSTERED INDEX IX_NC_CLIENT_CLI
ON dbo.PV_SA_Q_CLIENT (DWHCLICLI)
GO
CREATE NONCLUSTERED INDEX IX_NC_CLIENT_CLO
ON dbo.PV_SA_Q_CLIENT (DWHCLICLO)
INCLUDE (DWHCLICLI)
GO
CREATE NONCLUSTERED INDEX IX_NC_CLIENT_VLD_DTE
ON dbo.PV_SA_Q_CLIENT (DWHCLICLI, DWHCLICOL, DWHCLITIE)
INCLUDE (Validity_StartDate, Validity_EndDate)
GO
CREATE NONCLUSTERED INDEX IX_NC_CLI_DTE
ON dbo.PV_SA_Q_CLIENT (Validity_StartDate, Validity_EndDate)
INCLUDE (DWHCLIDTX, DWHCLICLI)
GO
CREATE NONCLUSTERED INDEX IX_NC_CLI_VLD_STR_DTE
ON dbo.PV_SA_Q_CLIENT (Validity_StartDate)
INCLUDE (DWHCLIDTX, DWHCLIETB, DWHCLICLI, DWHCLIETA, DWHCLIRA1, DWHCLIRA2, DWHCLISIG, DWHCLISRN, DWHCLIDNA, DWHCLINAT, DWHCLICRE, DWHCLIRSD, DWHCLIRES, DWHCLIECO, DWHCLIPCS, DWHCLIREG, DWHCLIGEO, DWHCLIENT, DWHCLIACT, DWHCLICRD, DWHCLICOT, DWHCLIDIR, DWHCLIPRF, DWHCLINBE, DWHCLIANF, DWHCLIJUR, DWHCLIREV, DWHCLICHA, DWHCLISER, DWHCLISEP, DWHCLIDBA, DWHCLIDOU, DWHCLIINT, DWHCLITYI, DWHCLICLO, DWHCLIPNA, DWHCLIFAM, DWHCLICOL, DWHCLIGRI, DWHCLITRI, DWHCLIDLC, DWHCLIMOT, DWHCLIPRO, DWHCLIDOM, DWHCLIEST, DWHCLIRU2, DWHCLIBIL, DWHCLIBIM, DWHCLIATR, DWHCLIBLN, DWHCLISTA, DWHCLITIE, DWHCLIMUT, DWHCLIACC, DWHCLIDGI, DWHCLINFI, DWHCLIAD1, DWHCLICOP, DWHCLIVIL, DWHCLIDEP, DWHCLICCN, DWHCLIIDT, DWHCLIIDN, DWHCLIRIS, DWHCLIFRE, Validity_EndDate)
GO
*/
CREATE NONCLUSTERED INDEX IX_NC_CLI_VLD_DTE
ON [dbo].[PV_SA_Q_CLIENT] ([Validity_StartDate],[Validity_EndDate])
INCLUDE ([DWHCLICLI],[DWHCLIRES],[DWHCLIRU2])
WITH(MAXDOP=1)
GO