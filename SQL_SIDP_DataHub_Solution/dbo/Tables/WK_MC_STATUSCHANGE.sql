﻿CREATE TABLE [dbo].[WK_MC_STATUSCHANGE] (
    [ClientID]      INT            NULL,
    [SubscriberKey] NVARCHAR (100) NULL,
    [EmailAddress]  NVARCHAR (100) NULL,
    [SubscriberID]  INT            NULL,
    [OldStatus]     NVARCHAR (10)  NULL,
    [NewStatus]     NVARCHAR (10)  NULL,
    [DateChanged]   DATETIME       NULL
);

