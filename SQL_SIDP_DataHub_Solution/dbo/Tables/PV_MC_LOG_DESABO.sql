﻿CREATE TABLE [dbo].[PV_MC_LOG_DESABO] (
    [Id]                      BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [SubscriberKey]           NVARCHAR (18)                               NOT NULL,
    [ID_CONTACT]              NVARCHAR (18)                               NULL,
    [ID_CONTACT_SOUSCRIPTEUR] NVARCHAR (18)                               NULL,
    [EMAIL]                   NVARCHAR (254)                              NULL,
    [DATE_DESABO]             DATETIME                                    NULL,
    [MESSAGE_NAME]            NVARCHAR (200)                              NULL,
    [JobID]                   INT                                         NULL,
    [INITIATIVE_DESABO]       BIT                                         NULL,
    [Reason]                  INT                                         NULL,
    [Reason_Label]            NVARCHAR (200)                              NULL,
    [Startdt]                 DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]                   DATETIME2 (7) GENERATED ALWAYS AS ROW END   NOT NULL,
    [NOM_FICHIER]             NVARCHAR (200)                              NULL,
    [INSRT_TS]                DATETIME                                    NULL,
    CONSTRAINT [PK_PV_MC_LOGDESABO] PRIMARY KEY CLUSTERED ([Id] ASC, [SubscriberKey] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_MC_LOG_DESABOHistory], DATA_CONSISTENCY_CHECK=ON));



