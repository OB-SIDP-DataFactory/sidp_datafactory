﻿CREATE TABLE [dbo].[REF_TRANSCO_ENCOURS] (
    [code_produit_n1]             VARCHAR (50)  NULL,
    [libelle_produit_n1]          VARCHAR (50)  NULL,
    [code_produit_n2]             VARCHAR (50)  NULL,
    [libelle_produit_n2]          VARCHAR (50)  NULL,
    [code_produit_SIDP]           VARCHAR (50)  NULL,
    [libelle_code_produit_SIDP]   VARCHAR (100) NULL,
    [code_regroupement_SIDP]      VARCHAR (50)  NULL,
    [date_debut]                  DATE          NULL,
    [date_fin]                    DATE          NULL,
    [code_operation_SAB]          VARCHAR (50)  NULL,
    [code_nature_credit_SAB]      VARCHAR (50)  NULL,
    [code_rubrique_comptable_SAB] VARCHAR (50)  NULL,
    [code_grille_SAB]             VARCHAR (50)  NULL
);

