﻿CREATE TABLE [dbo].[REF_FLUX] (
    [Id]              INT            IDENTITY (1, 1) NOT NULL,
    [ProcessingType]  NVARCHAR (50)  NULL,
    [Frequency]       NVARCHAR (10)  NULL,
    [Domain]          NVARCHAR (50)  NULL,
    [FileSource]      NVARCHAR (MAX) NULL,
    [Batch_name]      NVARCHAR (MAX) NULL,
    [FileOrTableName] NVARCHAR (MAX) NULL,
    [Flag]            BIT            DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_REF_FLUX] PRIMARY KEY CLUSTERED ([Id] ASC)
);

