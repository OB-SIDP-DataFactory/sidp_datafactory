﻿CREATE TABLE [dbo].[PV_SA_M_IDVM] (
    [ID]        BIGINT          IDENTITY (1, 1) NOT NULL,
    [DWHIDVIDV] INT             NULL,
    [DWHIDVRVM] INT             NULL,
    [DWHIDVPCT] INT             NULL,
    [DWHIDVISI] VARCHAR (12)    NULL,
    [DWHIDVTCN] VARCHAR (23)    NULL,
    [DWHIDVDEC] DATE            NULL,
    [DWHIDVPEX] DECIMAL (18, 9) NULL,
    [DWHIDVDCR] DATE            NULL,
    CONSTRAINT [PK_PV_SA_M_IDVM] PRIMARY KEY CLUSTERED ([ID] ASC)
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE CRÉATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_IDVM', @level2type = N'COLUMN', @level2name = N'DWHIDVDCR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PRIX EXERCICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_IDVM', @level2type = N'COLUMN', @level2name = N'DWHIDVPEX';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE ÉCHÉANCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_IDVM', @level2type = N'COLUMN', @level2name = N'DWHIDVDEC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE MNÉMONIQUE TCN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_IDVM', @level2type = N'COLUMN', @level2name = N'DWHIDVTCN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE ISIN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_IDVM', @level2type = N'COLUMN', @level2name = N'DWHIDVISI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PLACE DE COTATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_IDVM', @level2type = N'COLUMN', @level2name = N'DWHIDVPCT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RACINE VALEUR SAMIC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_IDVM', @level2type = N'COLUMN', @level2name = N'DWHIDVRVM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID. VALEUR MOBILIERE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_IDVM', @level2type = N'COLUMN', @level2name = N'DWHIDVIDV';

