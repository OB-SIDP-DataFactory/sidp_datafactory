﻿CREATE TABLE [dbo].[PV_MC_OPENSHistory] (
    [Id]                       BIGINT         NOT NULL,
    [ClientID]                 INT            NULL,
    [SendID]                   INT            NOT NULL,
    [SubscriberKey]            NVARCHAR (100) NULL,
    [EmailAddress]             NVARCHAR (100) NULL,
    [SubscriberID]             INT            NOT NULL,
    [ListID]                   INT            NULL,
    [EventDate]                DATETIME       NOT NULL,
    [EventType]                NVARCHAR (100) NULL,
    [BatchID]                  NVARCHAR (100) NULL,
    [TriggeredSendExternalKey] NVARCHAR (100) NULL,
    [IsUnique]                 NVARCHAR (5)   NULL,
    [Browser]                  NVARCHAR (100) NULL,
    [EmailClient]              NVARCHAR (100) NULL,
    [OperatingSystem]          NVARCHAR (100) NULL,
    [Device]                   NVARCHAR (100) NULL,
    [Startdt]                  DATETIME2 (7)  NOT NULL,
    [Enddt]                    DATETIME2 (7)  NOT NULL
);
GO
CREATE CLUSTERED INDEX [ix_PV_MC_OPENSHistory]
    ON [dbo].[PV_MC_OPENSHistory]([Enddt] ASC, [Startdt] ASC) WITH (DATA_COMPRESSION = PAGE);

