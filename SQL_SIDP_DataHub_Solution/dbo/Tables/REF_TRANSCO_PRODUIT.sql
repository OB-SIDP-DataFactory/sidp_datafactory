﻿CREATE TABLE [dbo].[REF_TRANSCO_PRODUIT] (
    [code_produit_n1]             VARCHAR (50)  NULL,
    [libelle_produit_n1]          VARCHAR (50)  NULL,
    [code_produit_n2]             VARCHAR (50)  NULL,
    [libelle_produit_n2]          VARCHAR (50)  NULL,
    [code_produit_n3]             VARCHAR (50)  NULL,
    [libelle_produit_n3]          VARCHAR (50)  NULL,
    [code_produit_n4]             VARCHAR (50)  NULL,
    [libelle_produit_n4]          VARCHAR (50)  NULL,
    [code_produit_SIDP]           VARCHAR (50)  NULL,
    [libelle_code_produit_SIDP]   VARCHAR (100) NULL,
    [code_regroupement_SIDP]      VARCHAR (50)  NULL,
    [date_debut]                  DATE          NULL,
    [date_fin]                    DATE          NULL,
    [code_produit_SAB]            VARCHAR (50)  NULL,
    [code_operation_SAB]          VARCHAR (50)  NULL,
    [code_nature_credit_SAB]      VARCHAR (50)  NULL,
    [code_rubrique_comptable_SAB] VARCHAR (50)  NULL,
    [code_produit_CRM]            VARCHAR (50)  NULL,
    [code_grille_SAB]             VARCHAR (50)  NULL
);

