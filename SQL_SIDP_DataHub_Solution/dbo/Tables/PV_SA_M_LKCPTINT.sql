﻿CREATE TABLE [dbo].[PV_SA_M_LKCPTINT] (
    [Id]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [DWHMM1DTX]          DATE                                        NULL,
    [DWHMM1ETA]          INT                                         NULL,
    [DWHMM1AGE]          INT                                         NULL,
    [DWHMM1SER]          VARCHAR (2)                                 NULL,
    [DWHMM1SES]          VARCHAR (2)                                 NULL,
    [DWHMM1OPE]          VARCHAR (6)                                 NULL,
    [DWHMM1NAT]          VARCHAR (6)                                 NULL,
    [DWHMM1NUM]          INT                                         NULL,
    [DWHMM1DTD]          DATE                                        NULL,
    [DWHMM1MON]          DECIMAL (18, 3)                             NULL,
    [DWHMM1NBR]          INT                                         NULL,
    [DWHMM1DEV]          VARCHAR (3)                                 NULL,
    [DWHMM1TYC]          VARCHAR (1)                                 NULL,
    [DWHMM1CLI]          VARCHAR (7)                                 NULL,
    [DWHMM1COM]          VARCHAR (20)                                NULL,
    [DWHMM1ENG]          DATE                                        NULL,
    [DWHMM1DEB]          DATE                                        NULL,
    [DWHMM1FIN]          DATE                                        NULL,
    [DWHMM1DUR]          INT                                         NULL,
    [DWHMM1TYP]          VARCHAR (1)                                 NULL,
    [DWHMM1AUT]          VARCHAR (3)                                 NULL,
    [DWHMM1CVL]          DECIMAL (18, 3)                             NULL,
    [DWHMM1NLO]          INT                                         NULL,
    [DWHMM1DSY]          DATE                                        NULL,
    [DWHMM1TYA]          VARCHAR (1)                                 NULL,
    [DWHMM1PER]          VARCHAR (1)                                 NULL,
    [DWHMM1VTI]          DECIMAL (14, 9)                             NULL,
    [DWHMM1PLF]          DECIMAL (14, 9)                             NULL,
    [DWHMM1PLC]          DECIMAL (14, 9)                             NULL,
    [Validity_StartDate] DATETIME                                    NULL,
    [Validity_EndDate]   DATETIME                                    DEFAULT ('99991231') NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SA_M_LKCPTINT] PRIMARY KEY CLUSTERED ([Id] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SA_M_LKCPTINTHistory], DATA_CONSISTENCY_CHECK=ON));
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'VAL TAUX PLACHER', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKCPTINT', @level2type = N'COLUMN', @level2name = N'DWHMM1PLC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'VAL TAUX PLAFOND', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKCPTINT', @level2type = N'COLUMN', @level2name = N'DWHMM1PLF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'VAL TAUX INITIALE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKCPTINT', @level2type = N'COLUMN', @level2name = N'DWHMM1VTI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'PERIODE PAIE.INT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKCPTINT', @level2type = N'COLUMN', @level2name = N'DWHMM1PER';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'TYPE AMORTI.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKCPTINT', @level2type = N'COLUMN', @level2name = N'DWHMM1TYA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'DATE SYSTEME', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKCPTINT', @level2type = N'COLUMN', @level2name = N'DWHMM1DSY';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'NOMBRE DE LOT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKCPTINT', @level2type = N'COLUMN', @level2name = N'DWHMM1NLO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'NOMINAL CONTREV.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKCPTINT', @level2type = N'COLUMN', @level2name = N'DWHMM1CVL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'CODE AUTORISAT.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKCPTINT', @level2type = N'COLUMN', @level2name = N'DWHMM1AUT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'TYPE DE PREAVIS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKCPTINT', @level2type = N'COLUMN', @level2name = N'DWHMM1TYP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'DUREE PREAVIS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKCPTINT', @level2type = N'COLUMN', @level2name = N'DWHMM1DUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'DATE FIN OPE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKCPTINT', @level2type = N'COLUMN', @level2name = N'DWHMM1FIN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'DATE DEBUT OPE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKCPTINT', @level2type = N'COLUMN', @level2name = N'DWHMM1DEB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'DATE ENGAGEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKCPTINT', @level2type = N'COLUMN', @level2name = N'DWHMM1ENG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'COMPTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKCPTINT', @level2type = N'COLUMN', @level2name = N'DWHMM1COM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'CLIENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKCPTINT', @level2type = N'COLUMN', @level2name = N'DWHMM1CLI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'TYPE CLIENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKCPTINT', @level2type = N'COLUMN', @level2name = N'DWHMM1TYC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'DEVISE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKCPTINT', @level2type = N'COLUMN', @level2name = N'DWHMM1DEV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'NOMBRE OPE.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKCPTINT', @level2type = N'COLUMN', @level2name = N'DWHMM1NBR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'NOMINAL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKCPTINT', @level2type = N'COLUMN', @level2name = N'DWHMM1MON';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'DATE DEB SITU', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKCPTINT', @level2type = N'COLUMN', @level2name = N'DWHMM1DTD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'NUMERO OPE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKCPTINT', @level2type = N'COLUMN', @level2name = N'DWHMM1NUM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'NATURE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKCPTINT', @level2type = N'COLUMN', @level2name = N'DWHMM1NAT';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'OPERATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKCPTINT', @level2type = N'COLUMN', @level2name = N'DWHMM1OPE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'SOUS SERVICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKCPTINT', @level2type = N'COLUMN', @level2name = N'DWHMM1SES';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'SERVICE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKCPTINT', @level2type = N'COLUMN', @level2name = N'DWHMM1SER';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'AGENCE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKCPTINT', @level2type = N'COLUMN', @level2name = N'DWHMM1AGE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'ETABLISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKCPTINT', @level2type = N'COLUMN', @level2name = N'DWHMM1ETA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'DATE EXTRACTION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_LKCPTINT', @level2type = N'COLUMN', @level2name = N'DWHMM1DTX';
GO
CREATE UNIQUE NONCLUSTERED INDEX [IDX_XSIMM10]
    ON [dbo].[PV_SA_M_LKCPTINT]([DWHMM1ETA] ASC, [DWHMM1AGE] ASC, [DWHMM1SER] ASC, [DWHMM1SES] ASC, [DWHMM1OPE] ASC, [DWHMM1NAT] ASC, [DWHMM1NUM] ASC);

