﻿CREATE TABLE [dbo].[PV_SA_Q_ABONNEMENT] (
    [ID]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [DWHABODTX]          DATE                                        NULL,
    [DWHABOETA]          INT                                         NULL,
    [DWHABOAGE]          INT                                         NULL,
    [DWHABOSER]          VARCHAR (2)                                 NULL,
    [DWHABOSSE]          VARCHAR (2)                                 NULL,
    [DWHABONUM]          INT                                         NULL,
    [DWHABOCLI]          VARCHAR (7)                                 NULL,
    [DWHABOCOM]          VARCHAR (20)                                NULL,
    [DWHABOPRO]          VARCHAR (3)                                 NULL,
    [DWHABOADH]          DATE                                        NULL,
    [DWHABOFIN]          DATE                                        NULL,
    [DWHABOREN]          DATE                                        NULL,
    [DWHABOCET]          VARCHAR (1)                                 NULL,
    [DWHABOCOF]          VARCHAR (20)                                NULL,
    [DWHABOUT1]          INT                                         NULL,
    [DWHABOUT2]          INT                                         NULL,
    [DWHABORES]          DATE                                        NULL,
    [DWHABOMOR]          VARCHAR (6)                                 NULL,
    [DWHABOCRE]          DATE                                        NULL,
    [DWHABOVAL]          DATE                                        NULL,
    [DWHABOANN]          DATE                                        NULL,
    [DWHABOREL]          VARCHAR (1)                                 NULL,
    [Validity_StartDate] DATETIME                                    NULL,
    [Validity_EndDate]   DATETIME                                    DEFAULT ('99991231') NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    [NUMR_LIGN_FICH]     INT                                         NULL,
    CONSTRAINT [PK_PV_SA_Q_ABONNEMENT] PRIMARY KEY CLUSTERED ([ID] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SA_Q_ABONNEMENTHistory], DATA_CONSISTENCY_CHECK=ON));
GO
CREATE UNIQUE NONCLUSTERED INDEX [IDX_NUMABO]
    ON [dbo].[PV_SA_Q_ABONNEMENT]([DWHABONUM] ASC);

