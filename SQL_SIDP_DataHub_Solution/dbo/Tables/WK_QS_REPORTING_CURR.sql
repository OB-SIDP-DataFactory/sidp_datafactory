﻿CREATE TABLE [dbo].[WK_QS_REPORTING_CURR] (
    [Id]                                   INT            NULL,
    [nbRow]                                INT            NULL,
    [nbRow2]                               INT            NULL,
    [ENTERPRISE_NAME]                      VARCHAR (50)   NULL,
    [ENTITY_NAME]                          VARCHAR (50)   NULL,
    [PRODUCT_NAME]                         VARCHAR (50)   NULL,
    [TRANSACTION_ID]                       INT            NOT NULL,
    [DOCUMENT_REFERENCE]                   VARCHAR (14)   NOT NULL,
    [STATUS]                               VARCHAR (32)   NULL,
    [SUB_STATUS]                           VARCHAR (32)   NULL,
    [MODIFICATION_DATE]                    DATETIME       NULL,
    [CREATION_DATE]                        DATETIME       NULL,
    [MAIN_SIGNATORY_SIGNATURE_DATE]        DATETIME       NULL,
    [CO_SIGNATORY_SIGNATURE_DATE]          DATETIME       NULL,
    [HAS_COSIGNATORY]                      VARCHAR (1)    NULL,
    [PROCESSED_BY_USER_IDENTIFIER]         VARCHAR (255)  NULL,
    [LEVEL_ONE_DOCUMENTS_NOT_YET_UPLOADED] VARCHAR (MAX)  NULL,
    [DOCUMENTS_UPLOADED_LABEL]             VARCHAR (MAX)  NULL,
    [DOCUMENTS_UPLOADED_LABEL_AND_DATE]    VARCHAR (MAX)  NULL,
    [DOCUMENTS_KO]                         VARCHAR (MAX)  NULL,
    [FTP_SEND_STATUS]                      VARCHAR (50)   NULL,
    [BUSINESS_DATA_01]                     VARCHAR (255)  NULL,
    [BUSINESS_DATA_02]                     VARCHAR (255)  NULL,
    [BUSINESS_DATA_03]                     VARCHAR (255)  NULL,
    [BUSINESS_DATA_04]                     VARCHAR (255)  NULL,
    [BUSINESS_DATA_05]                     VARCHAR (255)  NULL,
    [BUSINESS_DATA_06]                     VARCHAR (255)  NULL,
    [BUSINESS_DATA_07]                     VARCHAR (255)  NULL,
    [BUSINESS_DATA_08]                     VARCHAR (255)  NULL,
    [BUSINESS_DATA_09]                     VARCHAR (255)  NULL,
    [BUSINESS_DATA_10]                     VARCHAR (255)  NULL,
    [BUSINESS_DATA_11]                     VARCHAR (255)  NULL,
    [BUSINESS_DATA_12]                     NVARCHAR (MAX) NULL,
    [Validity_StartDate]                   DATETIME       NULL,
    [Validity_EndDate]                     DATETIME       NULL,
    [MIN_MODIFICATION_DATE]                DATETIME       NULL,
    [MAX_MODIFICATION_DATE]                DATETIME       NULL
);





