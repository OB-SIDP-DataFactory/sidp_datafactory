﻿CREATE TABLE [dbo].[IWD_STG_ACTIVE_MSF] (
    [MEDIATION_SEGMENT_ID] NUMERIC (19) NOT NULL,
    [EFFECTIVE_ROOT_ID]    NUMERIC (19) NOT NULL,
    [MEDIATION_GUID]       VARCHAR (50) NOT NULL,
    [CALLID]               VARCHAR (50) NULL,
    [PARTYID]              VARCHAR (50) NULL,
    [START_DATE_TIME_KEY]  INT          NOT NULL,
    [START_TS]             INT          NULL,
    [END_TS]               INT          NULL,
    [EXPINTRL_END_TS]      INT          DEFAULT ((0)) NULL,
    [RESOURCE_KEY]         INT          NULL,
    [WORKBIN_KEY]          INT          NULL,
    [VQSEQ]                INT          NULL,
    [RESULT]               INT          NULL,
    CONSTRAINT [PK_STG_ACTIVE_MSF] PRIMARY KEY NONCLUSTERED ([MEDIATION_SEGMENT_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IWD_I_S_A_MSF_EFRTID]
    ON [dbo].[IWD_STG_ACTIVE_MSF]([EFFECTIVE_ROOT_ID] ASC);

