﻿CREATE TABLE [dbo].[IWD_GIDB_GO_CAMPAIGN] (
    [ID]               NUMERIC (16)  NOT NULL,
    [SESSID]           VARCHAR (64)  NOT NULL,
    [CAMPAIGNID]       INT           NOT NULL,
    [GROUPID]          INT           NOT NULL,
    [STATE]            INT           NOT NULL,
    [OCSID]            INT           NOT NULL,
    [DIALMODE]         INT           NULL,
    [OPTIMMODE]        INT           NULL,
    [OPTIMVALUE]       INT           NULL,
    [GENERALN]         INT           NULL,
    [GENERALW]         INT           NULL,
    [SCHEDN]           INT           NULL,
    [SCHEDW]           INT           NULL,
    [CALLBACKN]        INT           NULL,
    [CALLBACKW]        INT           NULL,
    [CREATED]          DATETIME      NOT NULL,
    [CREATED_TS]       INT           NOT NULL,
    [TERMINATED]       DATETIME      NULL,
    [TERMINATED_TS]    INT           NULL,
    [LASTCHANGED]      DATETIME      NOT NULL,
    [LASTCHANGED_TS]   INT           NULL,
    [INTERNALREASON]   INT           NULL,
    [GSYS_DOMAIN]      INT           NULL,
    [GSYS_SYS_ID]      INT           NULL,
    [GSYS_EXT_VCH1]    VARCHAR (255) NULL,
    [GSYS_EXT_VCH2]    VARCHAR (255) NULL,
    [GSYS_EXT_INT1]    INT           NULL,
    [GSYS_EXT_INT2]    INT           NULL,
    [CREATE_AUDIT_KEY] NUMERIC (19)  NOT NULL,
    [UPDATE_AUDIT_KEY] NUMERIC (19)  NOT NULL,
    CONSTRAINT [PK_GO_CAMPAIGN] PRIMARY KEY NONCLUSTERED ([SESSID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IWD_I_G_GO_CAMP_CTS]
    ON [dbo].[IWD_GIDB_GO_CAMPAIGN]([CREATED_TS] ASC);


GO
CREATE NONCLUSTERED INDEX [IWD_I_G_GO_CAMP_TTS]
    ON [dbo].[IWD_GIDB_GO_CAMPAIGN]([TERMINATED_TS] ASC, [CREATED_TS] ASC);

