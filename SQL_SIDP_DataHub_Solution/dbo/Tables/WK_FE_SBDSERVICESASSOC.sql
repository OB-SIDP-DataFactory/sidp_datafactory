﻿CREATE TABLE [dbo].[WK_FE_SBDSERVICESASSOC] (
    [SERVICEPRODUCT_ID]      DECIMAL (19)   NULL,
    [SUBSCRIBEDPRODUCT_ID]   DECIMAL (19)   NULL,
    [ID_FE_SBDSERVICESASSOC] NUMERIC (38)   NOT NULL,
    [CUSTOMER_ID]            NUMERIC (38)   NULL,
    [SALESFORCE_PERSON_ID]   NVARCHAR (255) NOT NULL,
    [CREATION_DATE]          DATETIME2 (6)  NULL,
    [CREATION_USER]          NVARCHAR (255) NULL,
    [LAST_UPDATE_DATE]       DATETIME2 (6)  NULL,
    [LAST_UPDATE_USER]       NVARCHAR (255) NULL,
    [USER_TYPE]              NVARCHAR (255) NULL
);


