﻿CREATE TABLE [dbo].[REF_ENT_PROOF_CONTEXT] (
    [ID]                           INT            IDENTITY (1, 1) NOT NULL,
    [ID_ENT_PROOF_CONTEXT]         DECIMAL (38)   NULL,
    [ENTITLEMENT_PROOF_PRODUCT_ID] DECIMAL (38)   NULL,
    [CREATION_DATE]                DATETIME2 (6)  NULL,
    [CREATION_USER]                NVARCHAR (255) NULL,
    [LAST_UPDATE_DATE]             DATETIME2 (6)  NULL,
    [LAST_UPDATE_USER]             NVARCHAR (255) NULL,
    [USER_TYPE]                    NVARCHAR (255) NULL,
    [REF_FAMILY_ID]                DECIMAL (19)   NULL,
    [CODE]                         NVARCHAR (255) NULL,
    [PRIORITY]                     DECIMAL (19)   NULL,
    CONSTRAINT [PK_REF_ENT_PROOF_CONTEXT] PRIMARY KEY CLUSTERED ([ID] ASC)
);

