﻿CREATE TABLE [dbo].[PV_SA_M_COMECHL] (
    [ID]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [DWHCOMDTX]          DATE                                        NULL,
    [DWHCOMETA]          INT                                         NULL,
    [DWHCOMCLI]          VARCHAR (7)                                 NULL,
    [DWHCOMPLA]          INT                                         NULL,
    [DWHCOMCOM]          VARCHAR (20)                                NULL,
    [DWHCOMOPE]          VARCHAR (6)                                 NULL,
    [DWHCOMANA]          VARCHAR (6)                                 NULL,
    [DWHCOMDTD]          DATE                                        NULL,
    [DWHCOMMON]          DECIMAL (18, 3)                             NULL,
    [DWHCOMNOP]          INT                                         NULL,
    [DWHCOMDEV]          VARCHAR (3)                                 NULL,
    [DWHCOMBAS]          DECIMAL (18, 3)                             NULL,
    [DWHCOMNCD]          INT                                         NULL,
    [DWHCOMNCC]          INT                                         NULL,
    [DWHCOMDSY]          DATE                                        NULL,
    [Validity_StartDate] DATETIME                                    NULL,
    [Validity_EndDate]   DATETIME                                    DEFAULT ('99991231') NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SA_M_COMECHL] PRIMARY KEY CLUSTERED ([ID] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SA_M_COMECHLHistory], DATA_CONSISTENCY_CHECK=ON));
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE SYSTEME', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COMECHL', @level2type = N'COLUMN', @level2name = N'DWHCOMDSY';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° LIGNE COMM. CR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COMECHL', @level2type = N'COLUMN', @level2name = N'DWHCOMNCC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° LIGNE COMM. DB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COMECHL', @level2type = N'COLUMN', @level2name = N'DWHCOMNCD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT TOTAL EN BASE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COMECHL', @level2type = N'COLUMN', @level2name = N'DWHCOMBAS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEVISE DU COMPTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COMECHL', @level2type = N'COLUMN', @level2name = N'DWHCOMDEV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NOMBRE COMMISS.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COMECHL', @level2type = N'COLUMN', @level2name = N'DWHCOMNOP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT TOTAL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COMECHL', @level2type = N'COLUMN', @level2name = N'DWHCOMMON';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DEB SITU', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COMECHL', @level2type = N'COLUMN', @level2name = N'DWHCOMDTD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE ANALYTIQUE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COMECHL', @level2type = N'COLUMN', @level2name = N'DWHCOMANA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE OPERATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COMECHL', @level2type = N'COLUMN', @level2name = N'DWHCOMOPE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMERO DE COMPTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COMECHL', @level2type = N'COLUMN', @level2name = N'DWHCOMCOM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMERO DE PLAN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COMECHL', @level2type = N'COLUMN', @level2name = N'DWHCOMPLA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMERO CLIENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COMECHL', @level2type = N'COLUMN', @level2name = N'DWHCOMCLI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ETABLISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COMECHL', @level2type = N'COLUMN', @level2name = N'DWHCOMETA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE EXTRACTION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_COMECHL', @level2type = N'COLUMN', @level2name = N'DWHCOMDTX';

