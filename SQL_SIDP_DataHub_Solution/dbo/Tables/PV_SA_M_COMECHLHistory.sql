﻿CREATE TABLE [dbo].[PV_SA_M_COMECHLHistory] (
    [ID]                 BIGINT          NOT NULL,
    [DWHCOMDTX]          DATE            NULL,
    [DWHCOMETA]          INT             NULL,
    [DWHCOMCLI]          VARCHAR (7)     NULL,
    [DWHCOMPLA]          INT             NULL,
    [DWHCOMCOM]          VARCHAR (20)    NULL,
    [DWHCOMOPE]          VARCHAR (6)     NULL,
    [DWHCOMANA]          VARCHAR (6)     NULL,
    [DWHCOMDTD]          DATE            NULL,
    [DWHCOMMON]          DECIMAL (18, 3) NULL,
    [DWHCOMNOP]          INT             NULL,
    [DWHCOMDEV]          VARCHAR (3)     NULL,
    [DWHCOMBAS]          DECIMAL (18, 3) NULL,
    [DWHCOMNCD]          INT             NULL,
    [DWHCOMNCC]          INT             NULL,
    [DWHCOMDSY]          DATE            NULL,
    [Validity_StartDate] DATETIME        NULL,
    [Validity_EndDate]   DATETIME        NULL,
    [Startdt]            DATETIME2 (7)   NOT NULL,
    [Enddt]              DATETIME2 (7)   NOT NULL
);

