﻿CREATE TABLE [dbo].[PV_SA_M_PRDCOMPTA] (
    [ID]                 BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [DWHPRDDTX]          DATE                                        NULL,
    [DWHPRDETA]          INT                                         NULL,
    [DWHPRDTYP]          VARCHAR (1)                                 NULL,
    [DWHPRDCLI]          VARCHAR (7)                                 NULL,
    [DWHPRDPLA]          INT                                         NULL,
    [DWHPRDCOM]          VARCHAR (20)                                NULL,
    [DWHPRDOPE]          VARCHAR (6)                                 NULL,
    [DWHPRDANA]          VARCHAR (6)                                 NULL,
    [DWHPRDDTD]          DATE                                        NULL,
    [DWHPRDMON]          DECIMAL (18, 3)                             NULL,
    [DWHPRDNOP]          INT                                         NULL,
    [DWHPRDDEV]          VARCHAR (3)                                 NULL,
    [DWHPRDFLO]          DECIMAL (18, 3)                             NULL,
    [DWHPRDCOP]          VARCHAR (3)                                 NULL,
    [DWHPRDBAS]          DECIMAL (18, 3)                             NULL,
    [DWHPRDFLB]          DECIMAL (18, 3)                             NULL,
    [DWHPRDCDB]          DECIMAL (18, 3)                             NULL,
    [DWHPRDCIB]          DECIMAL (18, 3)                             NULL,
    [DWHPRDNLO]          INT                                         NULL,
    [DWHPRDNLM]          INT                                         NULL,
    [DWHPRDNCD]          INT                                         NULL,
    [DWHPRDNCC]          INT                                         NULL,
    [DWHPRDDSY]          DATE                                        NULL,
    [Validity_StartDate] DATETIME                                    NULL,
    [Validity_EndDate]   DATETIME                                    DEFAULT ('99991231') NULL,
    [Startdt]            DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]              DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SA_M_PRDCOMPTA] PRIMARY KEY CLUSTERED ([ID] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SA_M_PRDCOMPTAHistory], DATA_CONSISTENCY_CHECK=ON));
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE SYSTEME', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRDCOMPTA', @level2type = N'COLUMN', @level2name = N'DWHPRDDSY';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° LIGNE COMM. CR', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRDCOMPTA', @level2type = N'COLUMN', @level2name = N'DWHPRDNCC';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° LIGNE COMM. DB', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRDCOMPTA', @level2type = N'COLUMN', @level2name = N'DWHPRDNCD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° LIGNE MT MOYEN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRDCOMPTA', @level2type = N'COLUMN', @level2name = N'DWHPRDNLM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'N° LIGNE OPERAT°', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRDCOMPTA', @level2type = N'COLUMN', @level2name = N'DWHPRDNLO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COUT INDIR. BASE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRDCOMPTA', @level2type = N'COLUMN', @level2name = N'DWHPRDCIB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'COUT DIRECT BASE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRDCOMPTA', @level2type = N'COLUMN', @level2name = N'DWHPRDCDB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'FLOAT EN BASE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRDCOMPTA', @level2type = N'COLUMN', @level2name = N'DWHPRDFLB';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MT TOTAL EN BASE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRDCOMPTA', @level2type = N'COLUMN', @level2name = N'DWHPRDBAS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE PRODUIT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRDCOMPTA', @level2type = N'COLUMN', @level2name = N'DWHPRDCOP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT FLOAT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRDCOMPTA', @level2type = N'COLUMN', @level2name = N'DWHPRDFLO';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DEVISE DU COMPTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRDCOMPTA', @level2type = N'COLUMN', @level2name = N'DWHPRDDEV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NOMBRE OPERATIONS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRDCOMPTA', @level2type = N'COLUMN', @level2name = N'DWHPRDNOP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MONTANT TOTAL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRDCOMPTA', @level2type = N'COLUMN', @level2name = N'DWHPRDMON';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE DEBUT SITU', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRDCOMPTA', @level2type = N'COLUMN', @level2name = N'DWHPRDDTD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE ANALYTIQUE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRDCOMPTA', @level2type = N'COLUMN', @level2name = N'DWHPRDANA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CODE OPERATION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRDCOMPTA', @level2type = N'COLUMN', @level2name = N'DWHPRDOPE';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMERO DE COMPTE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRDCOMPTA', @level2type = N'COLUMN', @level2name = N'DWHPRDCOM';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMERO DE PLAN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRDCOMPTA', @level2type = N'COLUMN', @level2name = N'DWHPRDPLA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NUMERO CLIENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRDCOMPTA', @level2type = N'COLUMN', @level2name = N'DWHPRDCLI';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TYPE ENCOURS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRDCOMPTA', @level2type = N'COLUMN', @level2name = N'DWHPRDTYP';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ETABLISSEMENT', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRDCOMPTA', @level2type = N'COLUMN', @level2name = N'DWHPRDETA';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'DATE EXTRACTION', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SA_M_PRDCOMPTA', @level2type = N'COLUMN', @level2name = N'DWHPRDDTX';

