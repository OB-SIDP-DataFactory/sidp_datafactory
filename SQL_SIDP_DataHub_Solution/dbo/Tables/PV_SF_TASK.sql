﻿CREATE TABLE [dbo].[PV_SF_TASK] (
    [Id]                        BIGINT                                      IDENTITY (1, 1) NOT NULL,
    [Id_SF]                     NVARCHAR (18)                               NOT NULL,
    [RecordTypeId]              NVARCHAR (18)                               NULL,
    [WhatId]                    NVARCHAR (18)                               NULL,
    [OwnerId]                   NVARCHAR (18)                               NULL,
    [Description]               NVARCHAR (MAX)                              NULL,
    [WhoId]                     NVARCHAR (18)                               NULL,
    [Subject]                   NVARCHAR (255)                              NULL,
    [InteractionId__c]          NVARCHAR (36)                               NULL,
    [CreatedById]               NVARCHAR (18)                               NULL,
    [CreatedDate]               DATETIME                                    NULL,
    [LastModifiedDate]          DATETIME                                    NULL,
    [TaskSubType]               NVARCHAR (40)                               NULL,
    [CompletedDateTime]			DATETIME                                    NULL,
    [Priority]                  NVARCHAR (40)                               NULL,
    [Status]                    NVARCHAR (40)                               NULL,
    [Family__c]                 NVARCHAR (255)                              NULL,
    [SubFamily__c]              NVARCHAR (255)                              NULL,
    [InteractionDate__c]        DATETIME                                    NULL,
    [ScoreOfInteraction__c]     DECIMAL (2)                                 NULL,
    [NotationLabel__c]          NVARCHAR (1300)                             NULL,
    [ResponseLink__c]           NVARCHAR (255)                              NULL,
    [ActivityDate]              DATE                                        NULL,
    [ConversationID__c]         NVARCHAR (255)                              NULL,
    [CallType__c]               NVARCHAR (1300)                             NULL,
    [IsHighPriority]            BIT                                         NULL,
    [Type]                      NVARCHAR (40)                               NULL,
    [IsDeleted]                 BIT                                         NULL,
    [AccountId]                 NVARCHAR (18)                               NULL,
    [IsClosed]                  BIT                                         NULL,
    [LastModifiedById]          NVARCHAR (18)                               NULL,
    [SystemModstamp]            DATETIME                                    NULL,
    [IsArchived]                BIT                                         NULL,
    [IsVisibleInSelfService]    BIT                                         NULL,
    [CallDurationInSeconds]     INT                                         NULL,
    [CallType]                  NVARCHAR (40)                               NULL,
    [CallDisposition]           NVARCHAR (255)                              NULL,
    [CallObject]                NVARCHAR (255)                              NULL,
    [ReminderDateTime]          DATETIME                                    NULL,
    [IsReminderSet]             BIT                                         NULL,
    [RecurrenceActivityId]      NVARCHAR (18)                               NULL,
    [IsRecurrence]              BIT                                         NULL,
    [RecurrenceStartDateOnly]   DATE                                        NULL,
    [RecurrenceEndDateOnly]     DATE                                        NULL,
    [RecurrenceTimeZoneSidKey]  NVARCHAR (40)                               NULL,
    [RecurrenceType]            NVARCHAR (40)                               NULL,
    [RecurrenceInterval]        INT                                         NULL,
    [RecurrenceDayOfWeekMask]   INT                                         NULL,
    [RecurrenceDayOfMonth]      INT                                         NULL,
    [RecurrenceInstance]        NVARCHAR (40)                               NULL,
    [RecurrenceMonthOfYear]     NVARCHAR (40)                               NULL,
    [RecurrenceRegeneratedType] NVARCHAR (40)                               NULL,
    [Description__c]            NVARCHAR (255)                              NULL,
    [InfosCompl__c]             NVARCHAR (255)                              NULL,
    [NbMonthsSinceCreation__c]  DECIMAL (18)                                NULL,
    [TCHResponseId__c]          NVARCHAR (10)                               NULL,
    [TaskDate__c]               DATETIME                                    NULL,
    [TaskId__c]                 NVARCHAR (30)                               NULL,
    [Gen_example_c__c]          NVARCHAR (255)                              NULL,
    [DocumentURL__c]            NVARCHAR (255)                              NULL,
    [ForbiddenWords__c]         NVARCHAR (255)                              NULL,
    [HasForbiddenWord__c]       BIT                                         NULL,
    [DeliveryAddress__c]        NVARCHAR (255)                              NULL,
    [Validity_StartDate]        DATETIME2 (7)                               NOT NULL,
    [Validity_EndDate]          DATETIME2 (7)                               DEFAULT ('99991231') NOT NULL,
    [Startdt]                   DATETIME2 (7) GENERATED ALWAYS AS ROW START NOT NULL,
    [Enddt]                     DATETIME2 (7) GENERATED ALWAYS AS ROW END   DEFAULT ('99991231') NOT NULL,
    CONSTRAINT [PK_PV_SF_TASK] PRIMARY KEY CLUSTERED ([Id] ASC),
    PERIOD FOR SYSTEM_TIME ([Startdt], [Enddt])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE=[dbo].[PV_SF_TASKHistory], DATA_CONSISTENCY_CHECK=ON))
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de nom', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'WhoId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID d''associé à', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'WhatId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de début de validité fonctionnelle', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'Validity_StartDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de fin de validité fonctionnelle', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'Validity_EndDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Sous-type de tâche', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'TaskSubType';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date d''achèvement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'CompletedDateTime';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Objet', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'Subject';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Sous-famille', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'SubFamily__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Statut', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'Status';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Note de l''interaction', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'ScoreOfInteraction__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Lien de réponse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'ResponseLink__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID du type d''enregistrement de tâche', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'RecordTypeId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Priorité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'Priority';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Attribué à l''ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'OwnerId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Niveau de satisfaction', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'NotationLabel__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'LastModifiedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Id Interaction', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'InteractionId__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date d''interaction', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'InteractionDate__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID de l''activité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'Id_SF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant technique SIDP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'Id';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Famille', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'Family__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Description', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'Description';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'CreatedDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ID créé par', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'CreatedById';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Numéro de conversation IBM', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'ConversationID__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type d''appel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'CallType__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Échéance uniquement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'ActivityDate';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'Type';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Id de la réponse', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'TCHResponseId__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Tâche', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'TaskId__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date Tâche', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'TaskDate__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Horodateur des modifications du système', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'SystemModstamp';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date/heure de rappel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'ReminderDateTime';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type de récurrence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'RecurrenceType';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fuseau horaire récurrent', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'RecurrenceTimeZoneSidKey';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Début de la récurrence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'RecurrenceStartDateOnly';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Répéter cette tâche', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'RecurrenceRegeneratedType';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Récurrence - Mois de l''année', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'RecurrenceMonthOfYear';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Intervalle de récurrence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'RecurrenceInterval';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Instance de récurrence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'RecurrenceInstance';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fin de la récurrence', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'RecurrenceEndDateOnly';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Récurrence - Masque du jour de la semaine', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'RecurrenceDayOfWeekMask';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Récurrence - Jour du mois', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'RecurrenceDayOfMonth';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Récurrence - ID d''activité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'RecurrenceActivityId';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nombre mois depuis création', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'NbMonthsSinceCreation__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Dernière modification par ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'LastModifiedById';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Public', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'IsVisibleInSelfService';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Définition du rappel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'IsReminderSet';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Créer une série récurrente de tâches', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'IsRecurrence';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Priorité élevée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'IsHighPriority';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Supprimé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'IsDeleted';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fermée', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'IsClosed';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Archivé', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'IsArchived';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Informations Complémentaires', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'InfosCompl__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Contient Mot(s) Interdit(s)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'HasForbiddenWord__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Test Field WDE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'Gen_example_c__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Mots interdits', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'ForbiddenWords__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'URL du document', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'DocumentURL__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Description', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'Description__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Adresse de livraison', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'DeliveryAddress__c';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type d''appel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'CallType';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identificateur de l''objet de l''appel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'CallObject';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Durée de l''appel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'CallDurationInSeconds';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Résultat de l''appel', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'CallDisposition';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Personne', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PV_SF_TASK', @level2type = N'COLUMN', @level2name = N'AccountId'
GO