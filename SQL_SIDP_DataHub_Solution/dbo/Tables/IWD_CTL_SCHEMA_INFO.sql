﻿CREATE TABLE [dbo].[IWD_CTL_SCHEMA_INFO] (
    [SCHEMA_NAME]        VARCHAR (255) NOT NULL,
    [SCHEMA_DESCRIPTION] VARCHAR (255) NULL,
    [SCHEMA_VERSION]     VARCHAR (255) NOT NULL,
    [INSTALL_TIME]       DATETIME      NULL,
    [MIGRATE_TIME]       DATETIME      NULL,
    [MIGRATE_FLAG]       NUMERIC (1)   NULL,
    CONSTRAINT [PK_CTL_SCHEMA_INFO] PRIMARY KEY NONCLUSTERED ([SCHEMA_NAME] ASC, [SCHEMA_VERSION] ASC)
);

