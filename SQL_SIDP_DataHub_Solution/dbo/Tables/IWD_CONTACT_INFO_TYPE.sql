﻿CREATE TABLE [dbo].[IWD_CONTACT_INFO_TYPE] (
    [CONTACT_INFO_TYPE_KEY]  INT          NOT NULL,
    [CONTACT_INFO_TYPE]      VARCHAR (32) NULL,
    [CONTACT_INFO_TYPE_CODE] VARCHAR (32) NULL,
    [CREATE_AUDIT_KEY]       NUMERIC (19) NOT NULL,
    [UPDATE_AUDIT_KEY]       NUMERIC (19) NOT NULL,
    CONSTRAINT [PK_CNTCT_INF_TP] PRIMARY KEY NONCLUSTERED ([CONTACT_INFO_TYPE_KEY] ASC)
);

