﻿CREATE TABLE [dbo].[Data_Audit] (
    [Id]             INT            IDENTITY (1, 1) NOT NULL,
    [ProcessingType] NVARCHAR (50)  NOT NULL,
    [ETLName]        NVARCHAR (100) NOT NULL,
    [ProcessingDate] DATETIME       NOT NULL,
    [Source]         NVARCHAR (50)  NULL,
    [Target]         NVARCHAR (50)  NULL,
    [ColumnName]     NVARCHAR (50)  NOT NULL,
    [OldValue]       NVARCHAR (100) NULL,
    [NewValue]       NVARCHAR (100) NOT NULL,
    CONSTRAINT [PK_Data_Audit] PRIMARY KEY CLUSTERED ([Id] ASC)
);

