﻿CREATE PROCEDURE [dbo].[IWD_DIM_TABLE_LOAD]
@Table_source as VARCHAR(1000),
@Table_cible as VARCHAR(1000)=NULL,
@sql_exec as varchar(max)=NULL,
@sql_header as varchar(max)=NULL,
@sql_insert as varchar(max)=NULL,
@sql_select as varchar(max)=NULL,
@dte_deb_period as date=NULL,
@dte_fin_period as date=NULL,
@Dte_deb_exec as datetime=NULL,
@Dte_fin_exec as datetime=NULL,
@resultat_exec as  varchar(max)=NULL,
@Schema_Oracle as varchar(50)=NULL,
@rowcount as int=NULL
as
-- I - init variable schema Oracle

select @Schema_Oracle = nom_schema from Param_IWD_SCHEMA_ORACLE;

-- II - L'instruction en SQL dynamique sera découpée en 3
-- 2.1 -  une partie écriture du header
-- 2.2 -  une partie sql_insert pour créer en dynamique les cast côté Sql server
-- 2.3 -  une partie sql_select pour créer la requête select comprenant le to_char côté Oracle

-- 2.1 - Ecriture du header

select @Table_cible = table_cible from Param_IWD_Collecte
where table_source = @Table_source;

select @sql_header = 'insert into '+ @Table_cible+ '('+
(SELECT 
  cible_column_name +',' 
 AS 'data()' 
 FROM Param_IWD_TABLE_STRUCT
 where cible_table_name = @Table_cible
 and flag_Identity = 0
 FOR XML PATH('') 
 ) 

 --retrait de la dernière virgule
 select @sql_header = SUBSTRING(@sql_header, 1, LEN(@sql_header) - 1)+')'

-- 2.2 - création de sql_insert

select @sql_insert = @sql_header + ' SELECT ' + 
(SELECT 
 case when source_data_type like 'TIMESTAMP%' then 'convert(datetime, ' + cible_column_name +', 120) ' 
 else 'cast('+cible_column_name+' as '+ cible_data_type +')' end + ' as ' +cible_column_name  + ','
 AS 'data()' 
 FROM Param_IWD_TABLE_STRUCT
 where cible_table_name = @Table_cible
 and flag_Identity = 0
 FOR XML PATH('') 
 ) ;
 --retrait de la dernière virgule
 select @sql_insert = SUBSTRING(@sql_insert, 1, LEN(@sql_insert) - 1) 

 -- 2.2 création de sql_select
select @sql_select = ' 
FROM OPENQUERY(INFOMART, ''SELECT ' +
(SELECT 
 case when source_data_type like 'TIMESTAMP%' then 'to_char(' + source_column_name +', ''''yyyy-mm-dd hh24:mi:ss'''') ' else source_column_name end + ' as ' + source_column_name  + ','
 AS 'data()' 
 FROM Param_IWD_TABLE_STRUCT
 where cible_table_name = @Table_cible
 and flag_Identity = 0
 FOR XML PATH('') 
 ) ;

 
 --Retrait de la dernière virgule
 select @sql_select = SUBSTRING(@sql_select, 1, LEN(@sql_select) - 1) +' from '+@Schema_Oracle+'.'+@Table_source+''')';

 -- Construction de la requete à exécuter
  select @sql_exec = @sql_insert + @sql_select

  print (@sql_exec)
  
  select @Dte_deb_exec = getdate();
  select @resultat_exec = 'OK';

  begin try
  exec (@sql_exec)
  SET @rowcount=@@ROWCOUNT
  end try
  begin catch
	select @resultat_exec = ERROR_MESSAGE()
  end catch

  select @Dte_fin_exec = getdate();

  -- tracing du traitement 
  select @dte_deb_period = dte_deb_periode, @dte_fin_period = dte_fin_periode from IWD_PILOTAGE_CHARGEMENT	WITH(NOLOCK)

  insert into [dbo].[IWD_PILOTAGE_HISTORIQUE]
  values (
  @Table_source, 
  @Table_cible,
  @dte_deb_period,
  @dte_fin_period,
  @Dte_deb_exec,
  @Dte_fin_exec,
  @resultat_exec,
  @rowcount,
  @sql_exec)