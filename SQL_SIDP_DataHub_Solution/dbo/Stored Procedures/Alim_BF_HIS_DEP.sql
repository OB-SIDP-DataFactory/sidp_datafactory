﻿CREATE PROCEDURE [dbo].[Alim_BF_HIS_DEP] 
(
     @P_Date date, --date_alim date
	 @P_Date_prev date, -- date dernière alim
	 @nbRows int OUTPUT -- nb lignes processées

)
AS
BEGIN
	IF OBJECT_ID('tempdb.dbo.#HIS_DEP', 'U') IS NOT NULL

	DROP TABLE #HIS_DEP; 

	SELECT c.[DWHCPTDTX]

	,c.[DWHCPTCOM]

	,c.[DWHCPTPPAL]

	,c.[DWHCPTCPT]

	,c.[DWHCPTMTA]

	, IIF(d.DWHCPTCOM IS NULL, 'INSERT', 'UPDATE') AS Operation

	,c.[Validity_StartDate]

	,c.[Validity_EndDate]

	INTO #HIS_DEP

	FROM VW_PV_SA_Q_COMPTE c left join PV_SA_HIS_DEP d ON LTRIM(RTRIM(c.DWHCPTCOM)) = LTRIM(RTRIM(d.DWHCPTCOM)) and d.DAT_FIN_VALD = @P_Date_prev 

	WHERE @P_Date >= cast([Validity_StartDate] as date) and @P_Date < cast([Validity_EndDate] as date)

	and c.[DWHCPTCPT] > c.[DWHCPTMTA] 

	ORDER by c.[DWHCPTCOM], c.[DWHCPTDTX] ;
	update PV_SA_HIS_DEP set DAT_FIN_VALD = @P_Date

	from PV_SA_HIS_DEP d inner join #HIS_DEP h ON RTRIM(LTRIM(h.DWHCPTCOM)) = LTRIM(RTRIM(d.DWHCPTCOM)) and h.Operation = 'UPDATE' and d.DAT_FIN_VALD = @P_Date_prev;

	insert into PV_SA_HIS_DEP

	select LTRIM(RTRIM(res.DWHCPTCOM)), res.DWHCPTPPAL, @P_Date, @P_Date, NULL, NULL

	from #HIS_DEP res where res.Operation = 'INSERT'

	IF OBJECT_ID('tempdb.dbo.#TMP_DEP', 'U') IS NOT NULL

	DROP TABLE #TMP_DEP; 

	WITH CNT_JRS AS

	(select

	DAT_DEBT_VALD,

	DATEDIFF(DAY,DAT_DEBT_VALD,DAT_FIN_VALD)+1 AS NB_JOU_TOT

	FROM

	dbo.PV_SA_HIS_DEP D WITH(NOLOCK)

	WHERE

	D.DAT_FIN_VALD = @P_Date

	GROUP BY DAT_DEBT_VALD,DAT_FIN_VALD

	),

	NB_JR_OUV AS

	(

	SELECT

	CJ.DAT_DEBT_VALD,

	SUM(IIF(T.BusinessDay = 1,1,0)) AS NB_JO
	FROM
		[$(DataFactoryDatabaseName)].dbo.REF_TEMPS T WITH(NOLOCK),
		CNT_JRS CJ
	WHERE
		1 = 1
		AND	T.StandardDate <= @P_Date
		AND T.StandardDate >= CJ.DAT_DEBT_VALD 
	GROUP BY
		CJ.DAT_DEBT_VALD
	),

	NB_JR_FER AS

	(

	SELECT

	CJ.DAT_DEBT_VALD,

	SUM(IIF(T.BusinessDay = 0,1,0)) AS NB_JF

	FROM

	[$(DataFactoryDatabaseName)].dbo.REF_TEMPS T WITH(NOLOCK),

	CNT_JRS CJ

	WHERE

	T.StandardDate <= @P_Date

	AND T.StandardDate >= CJ.DAT_DEBT_VALD 

	GROUP BY

	CJ.DAT_DEBT_VALD

	)

	SELECT distinct

	D.DWHCPTCOM AS T_DWHCPTCOM,

	D.DAT_DEBT_VALD AS T_DAT_DEBT_VALD,

	D.DAT_FIN_VALD AS T_DAT_FIN_VALD,

	CJ.NB_JOU_TOT AS T_NB_JOU_TOT,

	JO.NB_JO AS T_NB_JO,

	JF.NB_JF AS T_NB_JF

	INTO

	#TMP_DEP 

	FROM

	dbo.PV_SA_HIS_DEP D WITH(NOLOCK),

	CNT_JRS CJ,

	NB_JR_OUV JO,

	NB_JR_FER JF

	WHERE

	D.DAT_FIN_VALD = @P_Date

	AND CJ.DAT_DEBT_VALD = D.DAT_DEBT_VALD

	AND JF.DAT_DEBT_VALD = D.DAT_DEBT_VALD

	AND JO.DAT_DEBT_VALD = D.DAT_DEBT_VALD

	OPTION(MAXDOP 1)

	;

	UPDATE

	PV_SA_HIS_DEP

	SET

	NB_JOU_OUV = T.T_NB_JO,

	NB_JOU_FER = T.T_NB_JF

	FROM

	#TMP_DEP T

	WHERE

	T.T_DWHCPTCOM = DWHCPTCOM

	AND DAT_DEBT_VALD = T.T_DAT_DEBT_VALD

	AND DAT_FIN_VALD = @P_Date

	OPTION(MAXDOP 1);

　
	SELECT @nbRows = @@ROWCOUNT　
END