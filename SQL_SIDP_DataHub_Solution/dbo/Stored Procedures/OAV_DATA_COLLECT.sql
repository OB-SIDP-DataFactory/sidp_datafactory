﻿CREATE PROCEDURE [dbo].[OAV_DATA_COLLECT]
	@sql NVARCHAR(max)=NULL,
	@Table_src NVARCHAR(max)=NULL,
	@Table_cible NVARCHAR(max)=NULL
AS
--Curseur sur les tables  à charger il faut que Flag_Use = 1
DECLARE Oav_cursor CURSOR FOR
 SELECT
	table_source,
	table_cible
FROM
	Param_OAV_Collecte
WHERE Flag_Use = 1;

--Boucle sur les tables  à charger
OPEN Oav_cursor
FETCH NEXT FROM  Oav_cursor INTO @Table_src, @Table_cible
WHILE @@FETCH_STATUS = 0
BEGIN

	--Truncate de la table cible
	SELECT
		@sql = 'TRUNCATE TABLE '+ @Table_cible + ';'

	exec(@sql);

	--Exécution de l'alimentation
	EXEC OAV_LOAD_DATA_SCRIPT @Table_source = @Table_src;

	--FIN_BOUCLE:

	FETCH NEXT FROM Oav_cursor INTO @Table_src, @Table_cible
END
--end loop

--clean up
CLOSE Oav_cursor
DEALLOCATE Oav_cursor