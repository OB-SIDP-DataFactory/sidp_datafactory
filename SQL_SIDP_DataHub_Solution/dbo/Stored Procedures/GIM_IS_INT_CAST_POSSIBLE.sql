﻿create procedure GIM_IS_INT_CAST_POSSIBLE(@V varchar(max)) as 
begin
    declare @R int;
    if coalesce(ltrim(rtrim(@V)),'')=''
        return 1;
    if isnumeric(@V)=0
        return 0;
    begin try
        set @R = @V;
        return 1;
    end try
    begin catch
        return 0;
    end catch
end