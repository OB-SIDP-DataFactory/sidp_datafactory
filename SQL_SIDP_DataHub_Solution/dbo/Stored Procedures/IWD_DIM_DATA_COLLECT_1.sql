﻿
 CREATE Procedure [dbo].[IWD_DIM_DATA_COLLECT]
 @sql nvarchar(max)=NULL,
 @Table_src nvarchar(max)=NULL,
 @Table_cible nvarchar(max)=NULL
 as
 --Curseur sur les tables de dimensions à charger
 DECLARE Dim_cursor CURSOR for
 select table_source, table_cible from Param_IWD_Collecte
 where flag_dim = 1;
 
 --Boucle sur les tables de dimensions à charger
OPEN Dim_cursor
FETCH NEXT FROM Dim_cursor INTO @Table_src, @Table_cible
WHILE @@FETCH_STATUS = 0
BEGIN 
 
	--Truncate de la table cible
	SELECT @sql = 'TRUNCATE TABLE '+ @Table_cible + ';'

	exec(@sql);
	
	--Exécution de l'alimentation
	exec IWD_DIM_TABLE_LOAD @Table_source = @Table_src;
 
FIN_BOUCLE:

FETCH NEXT FROM Dim_cursor INTO @Table_src, @Table_cible
END
--end loop
 
--clean up
CLOSE Dim_cursor
DEALLOCATE Dim_cursor