﻿


CREATE PROCEDURE [dbo].[PKG_QOD_EXEC_CONTROLE]
@ID_CONTROL as integer,
@date_deb as datetime=NULL,
@date_fin as datetime=NULL,
@RUN_ID as integer=NULL,
@sql as nvarchar(max)=NULL,
@duree_exec as integer=NULL,
@resultat_exec as nvarchar(max)=NULL,
@nb_lignes as integer=NULL
as

select @date_deb = getdate()

-- Création d'un jeton exécution dans SIDP_QOD_EXECUTION pour la gestion de la concurrence
insert into SIDP_QOD_EXECUTION(DATE_EXEC, ID_CONTROL)
values (@date_deb, @ID_CONTROL)

SELECT @RUN_ID= @@IDENTITY;

-- exécution dans du contrôle dans la table de détail
select @sql = 
'insert into SIDP_QOD_result_detaille 
(run_id, id_control, message, output_col1, output_col2, output_col3, output_col4, output_col5, output_col6, output_col7)
select '+cast(@RUN_ID as char)+' as RUN_ID,
'+cast(@ID_CONTROL as char)+' as ID_CONTROL,
'+OUTPUT_message+' as message,
cast(output_col1 as char), 
cast(output_col2 as char), 
cast(output_col3 as char),  
cast(output_col4 as char), 
cast(output_col5 as char), 
cast(output_col6 as char), 
cast(output_col7  as char) from ('
+ sql_control+') a' from
SIDP_QOD_LISTE_CONTROLE where id_control = @id_control

print (@sql)

begin try
  exec (@sql)
end try
begin catch
  select @resultat_exec = 'ErrorNumber :'+isnull(cast(ERROR_NUMBER() as char), '')+' 
ErrorSeverity :'+isnull(cast(ERROR_SEVERITY() AS char), '')+'  
ErrorState :'+isnull(cast(ERROR_STATE() AS char), '')+'  
ErrorProcedure :'+isnull(cast(ERROR_PROCEDURE() AS char), '')+'  
ErrorLine :'+isnull(cast(ERROR_LINE() AS char),'')+'  
ErrorMessage :'+isnull(ERROR_MESSAGE(), '')

end catch

select @date_fin = getdate()

-- insertion des données dans la table de résultat agrégée

select @duree_exec = datediff(ms, @date_deb, @date_fin)

select @nb_lignes = case when @resultat_exec is null 
then (select count(*) from SIDP_QOD_result_detaille where run_id = @RUN_ID and ID_CONTROL = @ID_CONTROL)
else -1 end

insert into SIDP_QOD_result_agg
values(
@RUN_ID,
@date_deb,
@date_fin,
@duree_exec,
@ID_CONTROL,
@nb_lignes,
@resultat_exec
)

--fin de la procedure