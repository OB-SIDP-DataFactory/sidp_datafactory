﻿CREATE PROC [dbo].[retrieve_param_alim_BFR]
AS
BEGIN
    IF EXISTS ( SELECT 1 FROM [dbo].[Param_AlimBFR] )
    BEGIN
        SELECT CONVERT(varchar(10), date_analyse, 21) as date_analyse
             , CONVERT(varchar(10), DATEADD (MONTH, DATEDIFF (MONTH,0,date_analyse)+1,0), 21) AS date_action
          FROM [dbo].[Param_AlimBFR] order by 1
    END
    ELSE
    BEGIN
        SELECT CONVERT(varchar(10), MAX(DWHCPTDTX), 21) as date_analyse
             , CONVERT(varchar(10), DATEADD (MONTH, DATEDIFF (MONTH,0,MAX(DWHCPTDTX))+1,0), 21) AS date_action
          FROM [dbo].[PV_SA_M_COMPTE] order by 1
    END
END