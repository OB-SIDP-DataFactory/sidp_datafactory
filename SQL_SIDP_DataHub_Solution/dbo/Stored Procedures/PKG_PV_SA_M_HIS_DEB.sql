﻿
Create procedure [dbo].[PKG_PV_SA_M_HIS_DEB]
@date_analyses date 
as
begin
insert into [PV_SA_M_HIS_DEB](ANNEE,MOIS,DWHCPTCOM,DAT_FIN_VALD,NB_JOUR,DTE_ANALYSE)
select ANNEE, MOIS, DWHCPTCOM,max(DAT_FIN_VALD) as DAT_FIN_VALID,sum(nbr_jour) as nbr_jour,@date_analyses as DTE_ANALYSE  from (
select 
 Year(@date_analyses) as annee ,Month(@date_analyses) as mois ,
DWHCPTCOM,
DWHCPTPPAL,
DAT_DEBT_VALD,
DAT_FIN_VALD as DAT_FIN_VALD_init,
case when ( DAT_FIN_VALD > @date_analyses and DAT_DEBT_VALD <= @date_analyses ) 
then @date_analyses else DAT_FIN_VALD 
end as DAT_FIN_VALD,
NB_JOU_OUV,
NB_JOU_FER,
case when  (DAT_FIN_VALD > @date_analyses and DAT_DEBT_VALD <= @date_analyses) then DATEDIFF(D,DAT_DEBT_VALD,@date_analyses) +1 else (NB_JOU_OUV+NB_JOU_FER) end as nbr_jour

from PV_SA_HIS_DEB
where  --DWHCPTCOM='71001394948' and
 ( DAT_DEBT_VALD < @date_analyses)   --delete les enregistements qui n'existait pas pour cette @date_analyses
--order by DAT_FIN_VALD desc
) as a
where
 YEAR(@date_analyses)=YEAR(a.DAT_FIN_VALD) and
MONTH(@date_analyses)=MONTH(a.DAT_FIN_VALD) 
group by DWHCPTCOM,ANNEE, MOIS
end