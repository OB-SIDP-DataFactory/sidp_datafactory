﻿create procedure GIM_IS_DATETIME_CAST_POSSIBLE(@V varchar(max), @datestyle smallint) as 
begin
    declare @R datetime;
    if coalesce(ltrim(rtrim(@V)),'')=''
        return 1;
    if isdate(@V)=0
        return 0;
    begin try
        set @R = convert(datetime, @V, @datestyle);
        return 1;
    end try
    begin catch
        return 0;
    end catch
end