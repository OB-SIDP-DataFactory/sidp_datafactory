﻿CREATE PROC [dbo].[PKG_PROC_ALIM_PV_SA_Q_MOUVMENT_JOURS]
AS  
BEGIN 

return
---Soufiane Mir 19/08/2019 : Il est formellement interdit de modifier une table dans un traitement==> opter plutot pour une table temporaire pour faire vos traitements.

----Colonne calculée
--ALTER TABLE [dbo].[WK_SA_Q_MOUVEMENT] ADD [OPT_DEBIT]  AS (case when [COD_OPE]='CTB' AND [COD_EVE]='IMM' AND [NUM_CPT] like '203130EUR%' then 'D' when [COD_OPE]='CTB' AND [COD_EVE]='IMM' AND NOT [NUM_CPT] like '203130EUR%' then 'I' else '' end); 
----

--/* Préparation du CASE WHEN....END AS */
--DECLARE @LIGNE VARCHAR(max) 
--declare @REQUETE varchar(max) ='CASE '
--DECLARE DB_CURSOR CURSOR FOR 

--select case when EXPR_TECH !='ELSE' THEN CONCAT(EXPR_TECH,' THEN ', IDNT_TYP_OPRT ) 
--       ELSE CONCAT(EXPR_TECH,' ', IDNT_TYP_OPRT ) END  as CASE_STATEMENT
--from [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].[dbo].[DWH_REF_TYPE_OPERATION]
--where EXPR_TECH != 'NULL'
--and IDNT_TYP_OPRT NOT IN (-9999,-9998)
--order by IDNT_TYP_OPRT

--OPEN DB_CURSOR  
--FETCH NEXT FROM DB_CURSOR INTO @LIGNE  

--WHILE @@FETCH_STATUS = 0  
--BEGIN  
--      SET @REQUETE = @REQUETE + @LIGNE + ' ' 
--      FETCH NEXT FROM db_cursor INTO @LIGNE 
--END 
--CLOSE DB_CURSOR  
--DEALLOCATE DB_CURSOR 

 
--set @REQUETE = @REQUETE + ' ELSE CASE WHEN REQ_MVT.MTT_EUR >= 0 THEN -9999 ELSE -9998 END END AS COD_REF_OPE'

--/*REQUETE COMPLETE ET INSERTION DANS MOUVEMENT_JOUR*/

--declare @SQL_DYN nvarchar(max) = 'INSERT INTO [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_MOUVEMENT_JOUR] SELECT [COD_OPE],[NUM_OPE],[COD_EVE],[COD_SCH],[NUM_PIE],[NUM_ECR],[COD_ANA],[STR_ANA],[COD_EXO],[COD_BDF],[NAT_OPE],
--CASE WHEN COD_OPE = ''CTB'' AND NUM_CPT LIKE ''203130EUR%'' THEN substring(REQ_MVT.DON_ANA, 8, 11) ELSE NUM_CPT END AS NUM_CPT,
--[OPT_DEBIT],
--[MTT_DEV],[DTE_OPE]
--,[DTE_CPT],[DTE_VAL],[DTE_TRT],[COD_UTI],[COD_AGE],[COD_SVC],[COD_SOU_SVC],[COD_ANN],[COD_BAN_EIC],[LIB_MVT_1],[LIB_MVT_2],[LIB_MVT_3],[COD_EME],[DTE_TRT_MAD],[DTE_REF_MAD],[DTE_LIM_REJ_MAD],[COD_UTI_INI_MAD]
--,[DTE_CRE_MAD],[DTE_UTI_MAD],[COD_UTI_MAD],[REJ_ACC_MAD],[DON_ANA],[LIB_COMP_1],[LIB_COMP_2],[NUM_REM],[NUM_CHQ],[MTT_EUR],[DEV],[IDE_DO],[IDE_CTP],[NOM_CTP],[NUM_CPT_CTP],[BIC_CTP],[COD_BAN_CTP],[COD_PAY_CTP]
--,[TOP_CLI],[FIX_DEV],[SEN_MTT],[COD_AGE_OPE]
--,CASE WHEN COD_OPE = ''CTB'' AND DON_ANA IS NOT NULL THEN CAST(SUBSTRING(REQ_MVT.DON_ANA, TYP_CART_POST_DEBT, TYP_CART_POST_FIN - TYP_CART_POST_DEBT + 1) AS CHAR(3)) ELSE NULL END AS TYP_CART
--,CASE WHEN COD_OPE = ''CTB'' AND DON_ANA IS NOT NULL THEN CAST(SUBSTRING(REQ_MVT.DON_ANA, NATR_OPRT_POST_DEBT, NATR_OPRT_POST_FIN - NATR_OPRT_POST_DEBT + 1) AS CHAR(2)) ELSE NULL END AS NATR_OPRT
--,CASE WHEN COD_OPE = ''CTB'' AND DON_ANA IS NOT NULL THEN CAST(SUBSTRING(REQ_MVT.DON_ANA, 74,2) AS CHAR(2)) ELSE NULL END AS COD_PAYS_TRNS
--,CASE WHEN COD_OPE = ''FAC'' AND DON_ANA IS NOT NULL THEN CAST(SUBSTRING(DON_ANA, COD_COMM_POST_DEBT,COD_COMM_POST_FIN - COD_COMM_POST_DEBT + 1) AS CHAR(6)) ELSE NULL END AS COD_COMM,
--			  '+@REQUETE+'
--FROM          [$(DataHubDatabaseName)].dbo.WK_SA_Q_MOUVEMENT AS REQ_MVT LEFT OUTER JOIN
--                         [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].dbo.DWH_REF_POST_DONN_ANLT_OPRT ON REQ_MVT.COD_OPE = [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].dbo.DWH_REF_POST_DONN_ANLT_OPRT.COD_OPRT AND 
--                         REQ_MVT.DTE_TRT >= [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].dbo.DWH_REF_POST_DONN_ANLT_OPRT.DAT_DEBT_VALD AND REQ_MVT.DTE_TRT < [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].dbo.DWH_REF_POST_DONN_ANLT_OPRT.DAT_FIN_VALD
--WHERE        (REQ_MVT.COD_OPE LIKE ''*OF%'') OR  (REQ_MVT.COD_OPE NOT LIKE ''*%'') AND (REQ_MVT.COD_OPE NOT LIKE ''+%'')' 

----print @sql_stmt

--EXECUTE sp_executesql @SQL_DYN, N'@REQUETE varchar(max)', @REQUETE;

----Traitement des Virement    
--/*
----Rejet virement créditeur / Rejet Virement débiteur  (IN ('A1V','A2V','R1V','R2V'))
--UPDATE  PV_SA_Q_MOUVEMENT_JOUR SET 
--[COD_REF_OPE]  =   
--       CASE 
--       WHEN COD_OPE IN ('A1V','A2V','R1V','R2V') AND MTT_EUR > 0  THEN CASE WHEN (BIC_CTP = '' OR BIC_CTP LIKE 'GPBA%') THEN 1189
--	                                                                        WHEN (BIC_CTP != '' AND BIC_CTP NOT LIKE 'GPBA%') THEN 1190
--                                                                            ELSE 1203 END 
--	   WHEN COD_OPE IN ('A1V','A2V','R1V','R2V') AND MTT_EUR < 0  THEN 1200
--	   END 
--WHERE ISNULL([COD_REF_OPE],-1) < 0 
---- COD_OPE IN ('ECH','ERE')
--UPDATE PV_SA_Q_MOUVEMENT_JOUR SET  [COD_REF_OPE] = CASE WHEN COD_OPE IN ('ECH','ERE') AND COD_EVE IN ('ECH') AND COD_ANA IN ('ECHICR','INTERE') AND MTT_EUR < 0  THEN 1187 END 
--WHERE ISNULL([COD_REF_OPE],-1) < 0 

---- IN ('ERE')
--UPDATE PV_SA_Q_MOUVEMENT_JOUR SET  [COD_REF_OPE] =
--     CASE WHEN COD_OPE IN ('ERE') THEN 
--            CASE WHEN MTT_EUR > 0 THEN 
--			     CASE WHEN COD_EVE IN ('OUV') THEN 1194
--                      WHEN COD_EVE IN ('VER') THEN 1195
--					  WHEN COD_EVE IN ('CLO') THEN 1199
--				 END 
--				 WHEN MTT_EUR < 0 THEN
--				 CASE WHEN COD_EVE IN ('OUV') AND COD_ANA IN ('VEREPA') THEN 1182
--				 	  WHEN COD_EVE IN ('VER') AND COD_ANA IN ('VEREPA') THEN 1183
--				 	  WHEN COD_EVE IN ('CLO') THEN
--					       CASE WHEN COD_ANA IN ('INTERE') THEN 1188
--					            ELSE 1181
--					        END 
--				 END 
--		    END
--		END		  
--WHERE ISNULL([COD_REF_OPE],-1) < 0 

---- IN ('R0V','A0V')
--UPDATE PV_SA_Q_MOUVEMENT_JOUR SET  [COD_REF_OPE] =
--     CASE WHEN COD_OPE IN ('R0V','A0V') THEN
--            CASE WHEN MTT_EUR > 0 THEN
--			     CASE WHEN NAT_OPE IN ('PUI') THEN 1193
--				      WHEN NAT_OPE IN ('PUE') THEN 1198
--				      WHEN SUBSTRING(NAT_OPE, 2, 1) IN ('P') THEN 
--				           CASE WHEN (BIC_CTP = '' OR BIC_CTP LIKE 'GPBA%')       THEN 1192
--					            WHEN (BIC_CTP != '' AND BIC_CTP NOT LIKE 'GPBA%')  THEN 1197
--                           END 
--					  ELSE CASE WHEN (BIC_CTP = '' OR BIC_CTP LIKE 'GPBA%')       THEN 1191
--				                WHEN (BIC_CTP != '' AND BIC_CTP NOT LIKE 'GPBA%')  THEN 1196
--				            END
--				   END 
--				  WHEN  MTT_EUR < 0 THEN    
--				  CASE WHEN NAT_OPE in ('OUI') THEN 1186
--				       WHEN NAT_OPE IN ('PUI') THEN 1179
--				       WHEN NAT_OPE IN ('PUE') THEN 1184    
--				  ELSE CASE WHEN (BIC_CTP = '' OR BIC_CTP LIKE 'GPBA%')        THEN 1180
--				            WHEN (BIC_CTP != '' AND BIC_CTP NOT LIKE 'GPBA%')  THEN 1185
--						END 
--				 END
--				 END 
--        END 
--WHERE ISNULL([COD_REF_OPE],-1) < 0 

---- AUTRES
--UPDATE PV_SA_Q_MOUVEMENT_JOUR SET  [COD_REF_OPE] = ISNULL([COD_REF_OPE],CASE WHEN MTT_EUR >= 0 THEN -9999 ELSE -9998 END) 
--*/
---- Fin de Traitement

----Suppression de la Colonne calculée
--ALTER TABLE [dbo].[WK_SA_Q_MOUVEMENT] DROP COLUMN [OPT_DEBIT] --delete column
----

END