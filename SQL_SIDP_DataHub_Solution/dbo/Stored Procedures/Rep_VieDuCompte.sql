﻿CREATE proc [dbo].[Rep_VieDuCompte]
as

IF OBJECT_ID('dbo.R_VieDucompte') IS NOT NULL
BEGIN
	drop table R_VieDucompte
END;


with  Pv_Sa_Q_CompteTps as
(

select Cpt.[ID]
      ,Cpt.[DWHCPTDTX]
      ,Cpt.[DWHCPTETA]
      ,Cpt.[DWHCPTPLA]
      ,Cpt.[DWHCPTCOM]
      ,Cpt.[DWHCPTRUB]
      ,Cpt.[DWHCPTINT]
      ,Cpt.[DWHCPTNBR]
      ,Cpt.[DWHCPTPPAL]
      ,Cpt.[DWHCPTAGE]
      ,Cpt.[DWHCPTDEV]
      ,Cpt.[DWHCPTOPR]
      ,Cpt.[DWHCPTVAL]
      ,Cpt.[DWHCPTCPT]
      ,Cpt.[DWHCPTCPC]
      ,Cpt.[DWHCPTRES]
      ,Cpt.[DWHCPTDUR]
      ,Cpt.[DWHCPTMG1]
      ,Cpt.[DWHCPTMG2]
      ,Cpt.[DWHCPTGAR]
      ,Cpt.[DWHCPTGA1]
      ,Cpt.[DWHCPTQUA]
      ,Cpt.[DWHCPTGA2]
      ,Cpt.[DWHCPTGA3]
      ,Cpt.[DWHCPTSMOD]
      ,Cpt.[DWHCPTSMOC]
      ,Cpt.[DWHCPTSMVD]
      ,Cpt.[DWHCPTSMVC]
      ,Cpt.[DWHCPTCDS]
      ,Cpt.[DWHCPTDAA]
      ,Cpt.[DWHCPTPER]
      ,Cpt.[DWHCPTNPR]
      ,Cpt.[DWHCPTMTA]
      ,Cpt.[DWHCPTMTU]
      ,Cpt.[DWHCPTMTD]
      ,Cpt.[DWHCPTMTR]
      ,Cpt.[DWHCPTMTNR]
      ,Cpt.[DWHCPTMTNS]
      ,Cpt.[DWHCPTSMD]
      ,Cpt.[DWHCPTSMC]
      ,Cpt.[DWHCPTML1]
      ,Cpt.[DWHCPTML2]
      ,Cpt.[DWHCPTCLO]
      ,Cpt.[DWHCPTDAC]
      ,Cpt.[DWHCPTDAO]
      ,Cpt.[DWHCPTMIP]
      ,Cpt.[DWHCPTCIP]
      ,Cpt.[DWHCPTPBC]
      ,Cpt.[DWHCPTCPB]
      ,Cpt.[DWHCPTDDD]
      ,Cpt.[DWHCPTNJD]
      ,Cpt.[DWHCPTCPM]
      ,Cpt.[DWHCPTFLG]
      ,Cpt.[DWHCPTMVC]
      ,Cpt.[DWHCPTCOC]
      ,Cpt.[DWHCPTCOB]
      ,Cpt.[DWHCPTCLA]
      ,Cpt.[DWHCPTUTI]
      ,Cpt.[DWHCPTTXN]
      ,Cpt.[DWHCPTREV]
      ,Cpt.[DWHCPTSUR]
      ,Cpt.[DWHCPTFRE]
      ,Cpt.[DWHCPTRIS]
      ,Cpt.[Validity_StartDate]
      ,Cpt.[Validity_EndDate]
      ,Cpt.[Startdt]
      ,Cpt.[Enddt] , t.*,  o.CloseDate, o.IDOppty__c,  o.CommercialOfferCode__c , o.StageName, rsv.LIB_STA_VTE, f.ID_FE_ENROLMENT_FOLDER
from
dbo.[PV_SA_Q_COMPTE] Cpt WITH(NOLOCK)
INNER JOIN dbo.[PV_FE_EQUIPMENT_V2] equ  WITH(NOLOCK) on equ.EQUIPMENT_NUMBER = cpt.DWHCPTCOM 	and cast(equ.Validity_EndDate as date) = '9999-12-31'
inner join dbo.PV_FE_ENROLMENTFOLDER_V2 f  WITH(NOLOCK) on f.ID_FE_ENROLMENT_FOLDER = equ.ENROLMENT_FOLDER_ID and cast(f.Validity_EndDate as date) = '9999-12-31'
inner join dbo.PV_SF_OPPORTUNITY o  WITH(NOLOCK) on o.Id_SF = f.SALESFORCE_OPPORTUNITY_ID and o.StageName in ('09','15','14') 
inner join [$(DataFactoryDatabaseName)].dbo.DIM_TEMPS t  WITH(NOLOCK) on  t.StandardDate in (cpt.DWHCPTDAO , cpt.DWHCPTDAC)
left outer join dbo.REF_STADE_VTE RSV  WITH(NOLOCK) on o.StageName = RSV.COD_STA_VTE

),

 Activite_CAV  as
(
select    
	DWHCPTCOM as num_cpt,
	IDClientSAB as id_client,
	DWHCPTRUB as cod_rub, 
	(case
		when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl >= 30 AND Flg_R = 'A') then 'Actif++'
		when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  >= 12  and nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  <= 29  AND  Flg_R = 'A') then 'Actif+'
		when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  >= 1  and nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  <= 11  AND  Flg_R = 'A') then 'Quasi_Actif'
		when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl   < 1  AND  Flg_R = 'A') then 'Inactif'
		when  Flg_R = 'R' then 'Recent'	
		when  Flg_R = 'C' then 'Clos du mois'	
		else 'AUTRE' end
	) as Nv_Activite_cav
	,
	(case
		when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl >= 30 AND Flg_R = 'A') then 5
		when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  >= 6  and nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  <= 29  AND  Flg_R = 'A') then 4
		when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  >= 1  and nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  <= 5  AND  Flg_R = 'A') then 3
		when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl   < 1  AND  Flg_R = 'A') then 2
		when  Flg_R = 'R' then 1	
		when  Flg_R = 'C' then 0
		else 'AUTRE' end
	) as Poids_Activité
	,
	(case
	 when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl >= 90 ) then 'Tranche 90 et +'
	 when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  >= 75  and nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  <= 89) then 'Tranche 75-89'
	 when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  >= 60  and nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  <= 74) then 'Tranche 60-74'
	 when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  >= 45  and nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  <= 59) then 'Tranche 45-59'
	 when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  >= 30  and nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  <=44) then 'Tranche 30-44'
	 when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  >= 6  and nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  <= 29) then 'Tranche 6-29'
	 when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  >= 1  and nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  <= 5) then 'Tranche 1-5'
	 when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  < 1) then 'Tranche 0'
	 else 'AUTRE TRANCHE'
	end) as Tranche_Activite_cav


from 
	(
	select
		cpt.DWHCPTCOM
		,cpt.DWHCPTPPAL as IDClientSAB
		,cpt.DWHCPTDAO as date_ouv
		,cpt.DWHCPTDAC as date_clo
		, cpt.DWHCPTRUB 
	    ,SUM(case when [COD_OPE] in ('A0P', 'R0P') and MTT_EUR >0 then 1 else 0 end) nb_prel
		,SUM(case when [COD_OPE] in ('A0P', 'R0P','A1P', 'A2P', 'R1P', 'R2P') and MTT_EUR <0 then 1 else 0 end) nb_prel_rej
		,SUM(case when [COD_OPE] in ('A0V', 'R0V') and MTT_EUR >0 then 1 else 0 end) nb_vir_debit
		,sum(case when [COD_OPE] in ('A1V', 'A2V','R1V','R2V') and MTT_EUR <0 then 1 else 0 end) nb_vir_deb_rej
		,SUM(case when [COD_OPE] ='CTB' and MTT_EUR > 0 then 1 else 0 end) as Nb_paiement_carte
		,SUM(case when [COD_OPE] ='CTB' and substring(COD_ANA,5,2) = '17' and MTT_EUR < 0 then 1 else 0 end) as Nb_paiement_carte_rej
		,sum(case when COD_OPE = 'RI0' and MTT_EUR > 0 then 1 else 0 end) nb_chq_emis
	    ,sum(case when COD_OPE IN ('AI1','RI0') and MTT_EUR < 0 then 1 else 0 end) nb_chq_emis_rej
		,sum(case when COD_OPE = 'ERE' and COD_EVE = 'OUV' then 1 else 0 end) nb_1er_ver_csl
		, case 
			when(DWHCPTDAO >= cast(DATEADD(MONTH, DATEDIFF(MONTH, 0,dateadd(day,1,eomonth(dateadd(month, -1,cast(getdate() as date))))) - 3, 0) as date) and (DWHCPTDAC is null OR DWHCPTDAC > cast(DATEADD(DAY, -1, DATEADD(MONTH, DATEDIFF(MONTH, 0, dateadd(day,1,eomonth(dateadd(month, -1,cast(getdate() as date))))), 0)) as date))) then 'R'		-- Client récent en stock
			when(DWHCPTDAC >= cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, dateadd(day,1,eomonth(dateadd(month, -1,cast(getdate() as date))))) - 1, 0) as date) and DWHCPTDAC <= cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, dateadd(day,1,eomonth(dateadd(month, -1,cast(getdate() as date))))), -1) as date)) then 'C'												-- Client clos du mois
			else 'A' 
			end Flg_R

	from 
	

				dbo.PV_SA_Q_COMPTE cpt  WITH(NOLOCK)
				INNER JOIN dbo.PV_FE_EQUIPMENT equ  WITH(NOLOCK) on equ.EQUIPMENT_NUMBER = cpt.DWHCPTCOM 
				inner join dbo.PV_FE_ENROLMENTFOLDER f  WITH(NOLOCK) on f.ID_FE_ENROLMENT_FOLDER = equ.ENROLMENT_FOLDER_ID
				left outer join dbo.[PV_SA_Q_MOUVEMENT] mouv  WITH(NOLOCK) on mouv.NUM_CPT = cpt.DWHCPTCOM 
													and COD_OPE in ('A0P', 'R0P', 'A0V', 'R0V','CTB', 'RI0', 'AI1', 'A1V', 'A2V','ERE','A1P', 'A2P', 'R1P', 'R2P','R1V','R2V') 
													and DTE_OPE >=  cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, dateadd(day,1,eomonth(dateadd(month, -1,cast(getdate() as date))))) - 3, 0) as date) -- 1er jour M-3
													and DTE_OPE <= cast(DATEADD(DAY, -1, DATEADD(MONTH, DATEDIFF(MONTH, 0, dateadd(day,1,eomonth(dateadd(month, -1,cast(getdate() as date))))), 0)) as date) -- dernier jour M-1 -->selection des operations des 3 derniers mois
			
	where 
		cpt.DWHCPTRUB = '251180'			-- sélection CAV
	group by    
		cpt.DWHCPTCOM
		,cpt.DWHCPTPPAL
		,cpt.DWHCPTDAO 
		,cpt.DWHCPTDAC
		,cpt.DWHCPTRUB
		
	) a

),

Detail_Opportunite as (select distinct 
acc.Id_SF
,opp.Id_SF IDE_OPPRT_SF
,opp.AccountId [IDE_PERS_SF]
,opp.IDOppty__c [NUM_OPPRT]
, cast(opp.CreatedDate as date) Date_création
, opp.CloseDate DTE_CLO_OPPRT
,   (case when acc.EmployeeType__pc in ('OSA',	'GDT',	'ORC',	'BUY',	'EQF',	'GBF',	'GBR',	'NEC',	'NRS',	'NDT',	'OCE',	'OCS',	'OCW',	'OAB',	'OCB',	'OCO',	'OCY',	'ORL',	'ORM',	'OPT',	'OPP',	'ORS',	'SOF',	'SAH',	'TEL',	'VIA',	'WHA',	'OHC',	'OGI' ) then 'Salarié Orange' 
		when acc.EmployeeType__pc in ('OBA') then 'Salarié Orange Bank'
		when acc.EmployeeType__pc in ('35') then 'Retraité Groupama'
		when acc.EmployeeType__pc in ('36') then 'Retraité Orange'
		when acc.EmployeeType__pc in ('37') then 'Retraité Orange Bank'
		when acc.EmployeeType__pc in ('05',	'06',	'07',	'08',	'09',	'10',	'11',	'12',	'13',	'14',	'15',	'16',	'17',	'18',	'19',	'20',	'21',	'22',	'23',	'24',	'25',	'26',	'27',	'28',	'29',	'30',	'31',	'32',	'33',	'38',	'39',	'40',	'41',	'42',	'43',	'44',	'45',	'46',	'47',	'48',	'49',	'50' ) then 'Groupama'
		else 'Autre' end) as Salarié
,(case when ref_dis.LIB_RES_DIS = 'Orange Bank'  then 'OB_selfcare'
	  when ref_dis.LIB_RES_DIS = 'Orange France' and opp.CompleteFileFlag__c = '1' then 'Souscription boutique'
      when ref_dis.LIB_RES_DIS = 'Orange France' and (opp.CompleteFileFlag__c <> '1' or opp.CompleteFileFlag__c IS NULL) and opp.IDProcessSous__c is not null then 'Repli Selfcare'
	  when ref_dis.LIB_RES_DIS = 'Orange France' and opp.Indication__c = 'OUI' and opp.IDProcessSous__c is null and ref_ent.LIBELLE in ('AD', 'GDT')  then 'Indication boutique'
	  when ref_dis.LIB_RES_DIS = 'Orange France' and opp.Indication__c = 'OUI' and opp.IDProcessSous__c is null and ref_ent.LIBELLE in ('OF-WEB')  then 'Indication Digitale'
	  when ref_dis.LIB_RES_DIS = 'Orange France' and ref_can.LIB_CAN_INT = 'Web OB' and ref_ent.LIBELLE in ('OF-WEB')  then 'Renvoi trafic' 
	  when ref_dis.LIB_RES_DIS = 'Groupama / GAN' then 'Groupama / GAN'
	end) as Réseau_apporteur,
opp.LoanAcceptationDate__c DAT_ACCORD_CREDIT_SF,
opp.ReleaseDate__c DAT_DEBLOCAGE_CREDIT_SF,
opp.RetractationDate__c DAT_RETRACTATION_CREDIF_SF,
opp.CreditAmount__c MTT_PRET_SF,
opp.ReleaseAmount__c MTT_DEBL_SF

from 
dbo.PV_SF_OPPORTUNITY opp

inner join dbo.PV_SF_ACCOUNT acc on opp.AccountId = acc.Id_SF
	left outer join SIDP_DataHub.dbo.REF_CANAL_INT ref_can  on ref_can.COD_CAN_INT = opp.StartedChannel__c
	left outer join SIDP_DataHub.dbo.REF_RES_DIS ref_dis on ref_dis.COD_RES_DIS = opp.DistributorNetwork__c
	left outer join SIDP_DataHub.dbo.REF_SUB_NETWORK ref_ent on ref_ent.CODE_SF = opp.DistributorEntity__c
where --opp.IsWon = 'true'
opp.StageName in ('09','14','15')
)

select aa.*,bb.*,cc.*,d.[DWHCLIDTX]
      ,d.[DWHCLIETB]
      ,d.[DWHCLICLI]
      ,d.[DWHCLIAGE]
      ,d.[DWHCLIETA]
      ,d.[DWHCLIRA1]
      ,d.[DWHCLIRA2]
      ,d.[DWHCLISIG]
      ,d.[DWHCLISRN]
      ,d.[DWHCLISRT]
      ,d.[DWHCLIDCR]
      ,d.[DWHCLICRT]
      ,d.[DWHCLIDNA]
      ,d.[DWHCLINAT]
      ,d.[DWHCLICRE]
      ,d.[DWHCLIRSD]
      ,d.[DWHCLIRES]
      ,d.[DWHCLIECO]
      ,d.[DWHCLISAC]
      ,d.[DWHCLIPCS]
      ,d.[DWHCLIREG]
      ,d.[DWHCLICAT]
      ,d.[DWHCLIGEO]
      ,d.[DWHCLIENT]
      ,d.[DWHCLIACT]
      ,d.[DWHCLICRD]
      ,d.[DWHCLICOT]
      ,d.[DWHCLIDIR]
      ,d.[DWHCLIPRF]
      ,d.[DWHCLINBE]
      ,d.[DWHCLIRGM]
      ,d.[DWHCLIEFC]
      ,d.[DWHCLICHF]
      ,d.[DWHCLIANF]
      ,d.[DWHCLIJUR]
      ,d.[DWHCLIREV]
      ,d.[DWHCLICHA]
      ,d.[DWHCLIPAT]
      ,d.[DWHCLISER]
      ,d.[DWHCLISEP]
      ,d.[DWHCLIDBA]
      ,d.[DWHCLIDOU]
      ,d.[DWHCLIINT]
      ,d.[DWHCLITYI]
      ,d.[DWHCLICLO]
      ,d.[DWHCLIDEC]
      ,d.[DWHCLIPNA]
      ,d.[DWHCLIFAM]
      ,d.[DWHCLICOL]
      ,d.[DWHCLIGRI]
      ,d.[DWHCLITRI]
      ,d.[DWHCLIDLC]
      ,d.[DWHCLIMOT]
      ,d.[DWHCLIPRO]
      ,d.[DWHCLIDOM]
      ,d.[DWHCLIEST]
      ,d.[DWHCLIVA1]
      ,d.[DWHCLIVA2]
      ,d.[DWHCLIVA3]
      ,d.[DWHCLIVA4]
      ,d.[DWHCLIVA5]
      ,d.[DWHCLIVA6]
      ,d.[DWHCLIVA7]
      ,d.[DWHCLIVA8]
      ,d.[DWHCLIVA9]
      ,d.[DWHCLIV10]
      ,d.[DWHCLIV11]
      ,d.[DWHCLIV12]
      ,d.[DWHCLILA1]
      ,d.[DWHCLILA2]
      ,d.[DWHCLIRU1]
      ,d.[DWHCLIRU2]
      ,d.[DWHCLIDA1]
      ,d.[DWHCLIDA2]
      ,d.[DWHCLIMO1]
      ,d.[DWHCLIBIL]
      ,d.[DWHCLIBIM]
      ,d.[DWHCLIATR]
      ,d.[DWHCLIBLN]
      ,d.[DWHCLISTA]
      ,d.[DWHCLITIE]
      ,d.[DWHCLIMUT]
      ,d.[DWHCLIACC]
      ,d.[DWHCLIDGI]
      ,d.[DWHCLIFOJ]
      ,d.[DWHCLINFI]
      ,d.[DWHCLIAD1]
      ,d.[DWHCLICOP]
      ,d.[DWHCLIVIL]
      ,d.[DWHCLIDEP]
      ,d.[DWHCLICDN]
      ,d.[DWHCLICCN]
      ,d.[DWHCLICPI]
      ,d.[DWHCLIIDT]
      ,d.[DWHCLIIDN]
      ,d.[DWHCLIPYR]
      ,d.[DWHCLIPYN]
      ,d.[DWHCLIMBI]
      ,d.[DWHCLICON]
      ,d.[DWHCLICIB]
      ,d.[DWHCLITTI]
      ,d.[DWHCLIDAS]
      ,d.[DWHCLICPJ]
      ,d.[DWHCLIDPJ]
      ,d.[DWHCLIRIS]
      ,d.[DWHCLIFRE]
      ,d.[DWHCLIDSY]
	into R_VieDucompte from Pv_Sa_Q_CompteTps aa
left outer join  Activite_CAV bb on aa.DWHCPTCOM = bb.num_cpt
left outer join Detail_Opportunite cc on  aa.IDOppty__c=cc.NUM_OPPRT
Left outer join PV_SA_Q_CLIENT d on aa.DWHCPTPPAL=d.DWHCLICLI
GO