﻿CREATE PROCEDURE [dbo].[OAV_LOAD_DATA_SCRIPT_HISTORISATION]
@Table_source as VARCHAR(1000),
@Table_cible as VARCHAR(1000)=NULL,
@sql_exec as varchar(max)=NULL,
@sql_header as varchar(max)=NULL,
@sql_insert as varchar(max)=NULL,
@sql_select as varchar(max)=NULL,
@sql_update as varchar(max)=NULL,
@sql_delete as varchar(max)=NULL,
@Dte_deb_exec as datetime=NULL,
@Dte_fin_exec as datetime=NULL,
@resultat_exec as  varchar(max)=NULL,
@dte_obsrv as varchar(50)=NULL,
@rowcount as INT=NULL
as

-- I - init variable table cible
select @Table_cible = table_cible from Param_OAV_Collecte_Historisation
where table_source = @Table_source;
 -- Date Vald pour ne pas inserer les memes données le meme jour 
  SET @dte_obsrv =  dbo.retrieve_param_collect('OAV' , 'PKGCO_SSIS_OAV_ALIMENTATION') ;

 -- 2.3 Création du Sql  delete
 Select @sql_delete = 'IF   Cast(''' + @dte_obsrv + ''' as date) IN (Select CAST((DAT_CHRG) As date) From '  + @Table_cible +')'+ 
						 'DELETE FROM ' + @Table_cible +' WHERE CAST((DAT_CHRG) As date) =  Cast(''' + @dte_obsrv + ''' as date); ';
print(@sql_delete);
exec(@sql_delete);
-- II - L'instruction en SQL dynamique sera découpée en 3
-- 2.1 -  une partie écriture du header
-- 2.2 -  une partie sql_insert pour créer en dynamique les cast côté Sql server
-- 2.3 -  Une partie pour supprimer et inserer les données du meme jour

-- 2.1 - Ecriture du header

select @sql_header = 'insert into '+ @Table_cible+ '('+
(SELECT 
  cible_column_name +',' 
 AS 'data()' 
 FROM Param_OAV_TABLE_STRUCT_HISTORISATION
 where cible_table_name = @Table_cible
 FOR XML PATH('') 
 ) 

 --retrait de la dernière virgule
 select @sql_header = SUBSTRING(@sql_header, 1, LEN(@sql_header) - 1)+')'
  --print (@sql_header)
-- 2.2 - création de sql_insert

select @sql_insert = @sql_header + ' SELECT ' + 
(SELECT 
 --case when source_data_type like 'TIMESTAMP%' then 'convert(datetime, ' + CIBLE_COLUMN_NAME +', 120) ' 
 --else 'cast('+source_column_name+' as '+ cible_data_type +')' end + ' as ' +CIBLE_COLUMN_NAME  + ','
 cible_column_name  + ','
 AS 'data()' 
 FROM Param_OAV_TABLE_STRUCT_HISTORISATION
 where cible_table_name = @Table_cible
 FOR XML PATH('') 
 ) ; 
 --retrait de la dernière virgule
 select @sql_insert = SUBSTRING(@sql_insert, 1, LEN(@sql_insert) - 1)  

 -- Construction de la requete à exécuter
  select @sql_exec = @sql_insert  + ' From ' + @Table_source 
  print (@sql_exec)
  
  
  select @Dte_deb_exec = getdate();
  select @resultat_exec = 'OK';

  -- Execution de la requete
  begin try
  exec (@sql_exec)
  end try
  begin catch
	select @resultat_exec = ERROR_MESSAGE()
  end catch

  set @rowcount = @@ROWCOUNT;
  select @Dte_fin_exec = getdate();