﻿CREATE PROCEDURE [dbo].[PKGCO_PROC_WK_TO_PV]
	@Table_source as VARCHAR(100),
	@Table_cible as VARCHAR(100)=NULL
AS
BEGIN
	DECLARE
	@sql_exec as NVARCHAR(MAX) =NULL,
	@sql_header as nvarchar(max) =NULL,
	@sql_insert as nvarchar(max) =NULL,
	@sql_select as nvarchar(max) =NULL,
	@sql_select_XML as XML =NULL,
	@dte_deb_period as date=NULL,
	@dte_fin_period as date=NULL,
	@Dte_deb_exec as datetime=NULL,
	@Dte_fin_exec as datetime=NULL,
	@resultat_exec as  nvarchar(max)=NULL,
	@rowcount as INT=NULL

	select @sql_header = 'insert into '+ @Table_cible+ '('+
	(SELECT 
	  cible_column_name +',' 
	 AS 'data()' 
	 FROM Param_TABLE_STRUCT
	 where cible_table_name = @Table_cible
	 FOR XML PATH('') 
	 ) 

	 select @sql_header = SUBSTRING(@sql_header, 1, LEN(@sql_header) - 1)+')'
	 
	select @sql_insert = @sql_header 

	select @sql_select = 'SELECT ' +
	(SELECT 
	  case when cible_data_type like '%varchar%' then 'CAST(' + source_column_name + ' AS '+ cible_data_type + '('+ CAST(cible_max_len AS CHAR(3)) + '))' + ',' + CHAR(10)
	  when cible_data_type like 'decimal' then 'CAST(' + source_column_name + ' AS '+ cible_data_type + '('+ CAST(cible_max_len AS CHAR(3)) + ',' + CAST(cible_scale AS char(1)) +'))' + ',' + CHAR(10)
	  ELSE 'CAST(' + source_column_name + ' AS '+ cible_data_type + ')' +',' END
	 AS 'data()' 
	 FROM Param_TABLE_STRUCT
	 where cible_table_name = @Table_cible
	 FOR XML PATH('') 
	 ) ;
	
	 select @sql_select = SUBSTRING(@sql_select, 1, LEN(@sql_select) - 2) +' from ' +@Table_source

	 -- Construction de la requete à exécuter
	  select @sql_exec = @sql_insert + @sql_select

	 PRINT ('Partie INSERT:')
	  print (@sql_insert)
	  PRINT ('Partie SELECT:')
	  PRINT CAST(@sql_select as NTEXT)
  
	  select @Dte_deb_exec = getdate();
	  select @resultat_exec = 'OK';

	  -- Execution de la requete
	  begin try
	  exec (@sql_exec)
	  end try
	  begin catch
		select @resultat_exec = ERROR_MESSAGE()
	  end catch

	  set @rowcount = @@ROWCOUNT;
	  select @Dte_fin_exec = getdate();
	  PRINT('message: '+ @resultat_exec);

	  -- tracing du traitement 

	  --insert into wk_pv_pilotage_historique
	  --values (
	  --@Table_source, 
	  --@Table_cible,
	  --null,       --@dte_deb_period,
	  --null,       --@dte_fin_period,
	  --@Dte_deb_exec,
	  --@Dte_fin_exec,
	  --@resultat_exec,
	  --null,
	  --@sql_exec)
END