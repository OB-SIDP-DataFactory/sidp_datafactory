﻿CREATE procedure [dbo].[IWD_DATAFACTORY_ALIMENTATION]
@sql nvarchar(max)=NULL,
@Table_dimension nvarchar(max)=NULL,
@Table_fait nvarchar(max)=NULL,
@key_column nvarchar(max)=NULL,
@dte_deb_period as date=NULL,
@dte_fin_period as date=NULL,
@Dte_deb_exec as datetime=NULL,
@Dte_fin_exec as datetime=NULL,
@resultat_exec as  varchar(max)=NULL,
@rowcount as int=NULL
as 

print (
'====================================================================================================
                               2 - CHARGEMENT DU DATAFACTORY
====================================================================================================')
-- 0 - Alimentation de la table CTL_AUDIT_LOG

print (
'****************************************************************************************************
                               2.1 - INITIALISATION TABLE DE PILOTAGE
*****************************************************************************************************')

delete from [$(DwhIwdDatabaseName)].dbo.IWD_CTL_AUDIT_LOG
where AUDIT_KEY in (select distinct AUDIT_KEY from IWD_CTL_AUDIT_LOG)

insert into [$(DwhIwdDatabaseName)].dbo.IWD_CTL_AUDIT_LOG
select * from IWD_CTL_AUDIT_LOG

print (
'****************************************************************************************************
                               2.1 - CHARGEMENT DES TABLES DE DIMENSIONS
*****************************************************************************************************')

-- 1 - Alimentation des tables de dimensions
 DECLARE Dim_cursor CURSOR for
 select table_cible from Param_IWD_Collecte
 where flag_dim = 1;

  --Boucle sur les tables de dimensions à charger
	OPEN Dim_cursor
	FETCH NEXT FROM Dim_cursor INTO @Table_dimension
	WHILE @@FETCH_STATUS = 0
	BEGIN 
 
		--Truncate de la table cible
		SELECT @sql = 'TRUNCATE TABLE [SIDP_DWH_IWD].[dbo].'+ @Table_dimension + ';';

		exec(@sql);
	
		--Exécution de l'alimentation
		SELECT @sql = 'INSERT INTO [SIDP_DWH_IWD].[dbo].'+ @Table_dimension + 
				' select * from ' + @Table_dimension

		print (@sql)

		select @Dte_deb_exec = getdate();
		select @resultat_exec = 'OK';

		--Bloc try catch
		begin try
		exec (@sql)
		SET @rowcount=@@ROWCOUNT
		end try
		begin catch
			select @resultat_exec = ERROR_MESSAGE()
		end catch

		select @Dte_fin_exec = getdate();

		 -- tracing du traitement 
		select @dte_deb_period = dte_deb_periode, @dte_fin_period = dte_fin_periode from dbo.IWD_PILOTAGE_CHARGEMENT	WITH(NOLOCK)

		insert into [dbo].[IWD_PILOTAGE_HISTORIQUE]
		  values (
		  'SIDP_DataHUB.' + @Table_dimension,
		  'SIDP_DWH_IWD.' + @Table_dimension,
		  @dte_deb_period,
		  @dte_fin_period,
		  @Dte_deb_exec,
		  @Dte_fin_exec,
		  @resultat_exec,
		  @rowcount,
		  @sql)
	

	FIN_BOUCLE:
	FETCH NEXT FROM Dim_cursor INTO @Table_dimension
END
--end loop
 
--clean up
CLOSE Dim_cursor
DEALLOCATE Dim_cursor

print (
'****************************************************************************************************
                               2.1 - CHARGEMENT DES TABLES DE FAITS
*****************************************************************************************************')

-- 2 - Alimentation des tables de faits

 DECLARE Fact_cursor CURSOR for
 select table_cible from Param_IWD_Collecte
 where flag_fact = 1;

  --Boucle sur les tables de faits à charger
	OPEN Fact_cursor
	FETCH NEXT FROM Fact_cursor INTO @Table_fait
	WHILE @@FETCH_STATUS = 0
	BEGIN 
	
		print ('table : '+@Table_fait)

		select @key_column = cible_column_name from Param_IWD_TABLE_STRUCT where key_column = 1
		and cible_table_name = @Table_fait

		--Exécution de l'alimentation DELETE/INSERT 
		SELECT @sql = 'Delete from [SIDP_DWH_IWD].[dbo].'+ @Table_fait + 
		' where ' + @key_column + ' in (select distinct ' + @key_column + ' from ' + @Table_fait + ');		
		insert into [SIDP_DWH_IWD].[dbo].'+ @Table_fait +' 
		select * from '+@Table_fait+';'

		select @Dte_deb_exec = getdate();
		select @resultat_exec = 'OK';
		
		print (@sql)

		--Bloc Try Catch
		begin try
		exec (@sql)
		SET @rowcount=@@ROWCOUNT
		end try
		begin catch
			select @resultat_exec = ERROR_MESSAGE()
		end catch

		select @Dte_fin_exec = getdate();

		 -- tracing du traitement 
		select @dte_deb_period = dte_deb_periode, @dte_fin_period = dte_fin_periode from dbo.IWD_PILOTAGE_CHARGEMENT	WITH(NOLOCK)

		insert into [dbo].[IWD_PILOTAGE_HISTORIQUE]
		  values (
		  'SIDP_DataHUB.' + @Table_fait, 
		  'SIDP_DWH_IWD.' + @Table_fait,
		  @dte_deb_period,
		  @dte_fin_period,
		  @Dte_deb_exec,
		  @Dte_fin_exec,
		  @resultat_exec,
		  @rowcount,
		  @sql)

		FETCH NEXT FROM Fact_cursor INTO @Table_fait
END
--end loop
 
--clean up
CLOSE Fact_cursor
DEALLOCATE Fact_cursor

print (
'****************************************************************************************************
                               3 - EXECUTION QOD
*****************************************************************************************************')

exec [$(DataFactoryDatabaseName)].dbo.PKG_QOD_EXEC_FAMILLE_CONTROLE 'IWD - Contrôle Dataware'

	
print (
'****************************************************************************************************
                               4 - TRUNCATE IWD_PILOTAGE_CHARGEMENT
*****************************************************************************************************')

truncate table IWD_PILOTAGE_CHARGEMENT
GO