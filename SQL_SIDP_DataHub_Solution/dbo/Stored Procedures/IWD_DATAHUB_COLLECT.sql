﻿

CREATE PROCEDURE [dbo].[IWD_DATAHUB_COLLECT]

as
print (
'====================================================================================================
                               1 - CHARGEMENT DU DATAHUB
====================================================================================================')

-- 1 - Init de la table de pilotage
print (
'****************************************************************************************************
                               1.1 - INITIALISATION TABLE DE PILOTAGE
*****************************************************************************************************')

exec IWD_INIT_PILOTAGE_CHARGEMENT

update IWD_PILOTAGE_CHARGEMENT set statut = 'en cours'

-- 1 bis - Chargement de la table de pilotage bis
truncate table iwd_ctl_audit_log

insert into iwd_ctl_audit_log
select F.* from openquery(INFOMART, 'select * from V_CTL_AUDIT_LOG') F
join IWD_PILOTAGE_CHARGEMENT on created between dte_deb_periode and dte_fin_periode

-- 2 - Chargement des dimensions
print (
'****************************************************************************************************
                               1.2 - CHARGEMENT DES DIMENSIONS
*****************************************************************************************************')
exec [dbo].[IWD_DIM_DATA_COLLECT]

-- 3 - Chargement des tables de faits
print (
'****************************************************************************************************
                               1.3 - CHARGEMENT DES FAITS
*****************************************************************************************************')
exec [dbo].[IWD_FACT_DATA_COLLECT]

-- Evolutions à prendre en compte
-- gestion du nombre de lignes insérées dans les tables 
--    solution de récupération du résultat exec
--    solution crado : sql synamique de count sur les lignes insérées
-- gestion du try catch
-- -> les erreurs de l'exec ne sont pas catchée par le bloc try/catch

print (
'****************************************************************************************************
                               1.4 - EXECUTION QOD
*****************************************************************************************************')

exec [SIDP_DataFactory].[dbo].[PKG_QOD_EXEC_FAMILLE_CONTROLE] 'IWD - CONTROLE DATAHUB'

exec [SIDP_DataFactory].[dbo].[PKG_QOD_EXEC_FAMILLE_CONTROLE] 'VOL_COLLECT_IWD'