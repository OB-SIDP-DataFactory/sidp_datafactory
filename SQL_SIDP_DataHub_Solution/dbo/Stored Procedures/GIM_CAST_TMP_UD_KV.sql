﻿create procedure GIM_CAST_TMP_UD_KV(@V_IS_MM bit, @V_NEW_IRF_FLAG bit, @V_IS_LATE bit, @UD_TO_UDE_MAPPING_ID int, 
    @CAST_TYPE varchar(max), @CAST_FUNC  varchar(max), @LOG_TABLE_NAME varchar(max), 
    @TMP_UD_KV_BAD varchar(max), @debug bit = 0) as 
begin
    declare @TMP_IRF varchar(max);
    declare @TMP_UD_KV varchar(max);
    declare @V_ETL_TS int;
    declare @V_ETL_DATE_TIME_KEY int;
    declare @V_CREATE_AUDIT_KEY numeric(19);
    declare @V_CODE int;
    set @V_ETL_TS = -2;
    set @V_ETL_DATE_TIME_KEY = -2;
    set @V_CREATE_AUDIT_KEY = -2;
    set @V_CODE = -2;
    
    declare @V_INTERACTION_ID numeric(19);
    declare @V_INTERACTION_RESOURCE_ID numeric(19);
    declare @V_PARTYGUID varchar(max);
    declare @V_REASON varchar(max);
    declare @V_KEYNAME varchar(max);
    declare @V_KEYVALUE varchar(max);
    declare @V_QUOTED_KEYVALUE varchar(max);
    declare @V_IS_CAST_POSSIBLE int;
    declare @curs cursor;
    declare @V varchar(max);
    declare @V_SQL nvarchar(max);
    declare @V_SQL2 nvarchar(max);
    declare @V_STATIC_CAST_NUM int;
    -- determine cast functions without dynamic SQL based on @CAST_FUNC and @CAST_TYPE
    if upper(@CAST_FUNC)=upper('dbo.GIM_TO_TIMESTAMP_ISO8601(${})')
        set @V_STATIC_CAST_NUM = 1;
    else if upper(@CAST_FUNC)=upper('convert(datetime, case when coalesce(ltrim(rtrim(${})),'''')<>'''' and isdate(${})=0 then dbo.GIM_THROW() else ${} end, 120)')
        set @V_STATIC_CAST_NUM = 2;
    else if upper(@CAST_FUNC)=upper('case when coalesce(ltrim(rtrim(${})),'''')='''' then null when isnumeric(${})=0 then dbo.GIM_THROW() else ${} end')
    begin
        if upper(@CAST_TYPE)=upper('NUMERIC(10)') set @V_STATIC_CAST_NUM = 3;
        if upper(@CAST_TYPE)=upper('NUMERIC(19)') set @V_STATIC_CAST_NUM = 4;
        if upper(@CAST_TYPE)=upper('INT') set @V_STATIC_CAST_NUM = 5;
    end;
    set @TMP_IRF = case when @V_IS_LATE = 0 
        then case when @V_IS_MM = 0 then 'TMP_IRF_UD_V'              else 'TMP_IRF_UD_MM'              end
        else case when @V_IS_MM = 0 then 'TMP_UD_IRF_FOR_LATE_UD_V'  else 'TMP_UD_IRF_FOR_LATE_UD_MM'  end
    end;
    set @TMP_UD_KV      = case when @V_IS_MM = 0 then 'TMP_UD_KV_V'     else 'TMP_UD_KV_MM' end;
    set @V_SQL =
        'set @curs = cursor static for select'
        + ' TMP_IRF.INTERACTION_ID, TMP_IRF.INTERACTION_RESOURCE_ID, TMP_IRF.PARTYGUID, UD_MAPPING.UD_KEY_NAME, TMP_UD_KV.KEYVALUE'
        + ' from ' + @TMP_UD_KV + ' TMP_UD_KV'
        + ' join ' + @TMP_IRF + ' TMP_IRF' +  '   on TMP_UD_KV.INTERACTION_RESOURCE_ID = TMP_IRF.INTERACTION_RESOURCE_ID'
        + ' join CTL_UD_TO_UDE_MAPPING UD_MAPPING on TMP_UD_KV.UD_TO_UDE_MAPPING_ID    = UD_MAPPING.ID' 
        + ' where TMP_UD_KV.UD_TO_UDE_MAPPING_ID=' + cast(@UD_TO_UDE_MAPPING_ID as varchar(max))
        + case when @V_IS_MM = 0 then '' else ' and TMP_IRF.NEW_IRF_FLAG=' + str(@V_NEW_IRF_FLAG) end
        + '; open @curs';
    exec sp_executesql @V_SQL, N'@curs cursor output', @curs output;
    FETCH NEXT FROM @curs INTO @V_INTERACTION_ID, @V_INTERACTION_RESOURCE_ID, @V_PARTYGUID, @V_KEYNAME, @V_KEYVALUE;
    WHILE @@FETCH_STATUS = 0 begin
        set @V = null;
        -- specialized and fast cast functions without dynamic SQL
             if @V_STATIC_CAST_NUM = 1 exec @V_IS_CAST_POSSIBLE = GIM_IS_DATETIME_CAST_POSSIBLE @V_KEYVALUE, 126;
        else if @V_STATIC_CAST_NUM = 2 exec @V_IS_CAST_POSSIBLE = GIM_IS_DATETIME_CAST_POSSIBLE @V_KEYVALUE, 120;
        else if @V_STATIC_CAST_NUM = 3 exec @V_IS_CAST_POSSIBLE = GIM_IS_NUMERIC_10_CAST_POSSIBLE @V_KEYVALUE;
        else if @V_STATIC_CAST_NUM = 4 exec @V_IS_CAST_POSSIBLE = GIM_IS_NUMERIC_19_CAST_POSSIBLE @V_KEYVALUE;
        else if @V_STATIC_CAST_NUM = 5 exec @V_IS_CAST_POSSIBLE = GIM_IS_INT_CAST_POSSIBLE @V_KEYVALUE;
        -- generic and slow cast function with dynamic SQL
        else begin
            exec @V_QUOTED_KEYVALUE = GIM_QUOTE @V_KEYVALUE;
            set @V = case when @CAST_FUNC is null then @V_QUOTED_KEYVALUE else replace(@CAST_FUNC, '${}', @V_QUOTED_KEYVALUE) end;
            exec @V_IS_CAST_POSSIBLE = GIM_IS_CAST_POSSIBLE @V, @CAST_TYPE, @debug
        end;
        if @V_IS_CAST_POSSIBLE = 0 begin
            set @V_SQL2 =
                'insert into ' + @TMP_UD_KV_BAD
                + '(INTERACTION_RESOURCE_ID, UD_TO_UDE_MAPPING_ID) values ('
                + cast(@V_INTERACTION_RESOURCE_ID as varchar(max)) + ',' 
                + cast(@UD_TO_UDE_MAPPING_ID as varchar(max)) + ')';
            exec(@V_SQL2);
        end
        
        if @debug=1 begin
            print case when @V_IS_CAST_POSSIBLE = 0 then 'FAIL: ' else 'PASS: ' end
                + @V_KEYNAME + ' ' + @CAST_TYPE + '=' + @V;
        end
        FETCH NEXT FROM @curs INTO @V_INTERACTION_ID, @V_INTERACTION_RESOURCE_ID, @V_PARTYGUID, @V_KEYNAME, @V_KEYVALUE;
    end
    close @curs;
    deallocate @curs
end