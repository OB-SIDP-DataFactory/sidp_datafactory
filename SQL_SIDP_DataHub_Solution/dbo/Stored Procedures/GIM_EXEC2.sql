﻿create procedure GIM_EXEC2(@v_select varchar(max)) as
begin
    declare @v_sql_cmd varchar(max);
    declare @v_cnt int;
    if exists (select 1 from sysobjects where id=object_id('TMP_GIM_SQL') and type='U') drop table IWD_TMP_GIM_SQL;
    create table IWD_TMP_GIM_SQL (SQL_CMD varchar(4000));
    set @v_sql_cmd = 'insert into TMP_GIM_SQL ' + @v_select;
    exec GIM_EXEC @v_sql_cmd;
    set @v_cnt = (select count(1) from TMP_GIM_SQL);
    while @v_cnt > 0
    begin
        set @v_sql_cmd = (select top 1 SQL_CMD from TMP_GIM_SQL order by SQL_CMD);
        exec GIM_EXEC @v_sql_cmd;
        delete from TMP_GIM_SQL where @v_sql_cmd = SQL_CMD;
        set @v_cnt = (select count(1) from TMP_GIM_SQL);
    end
end