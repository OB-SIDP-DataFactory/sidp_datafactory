﻿ CREATE Procedure IWD_INIT_PILOTAGE_CHARGEMENT
 as

 -- 1 - Initialisation de la table de paramétrage
 --     Postulat : la table est vide par défaut sauf en cas de reprise

 select dte_deb_periode from iwd_pilotage_chargement;

 -- 1.1 - Si la table est vide

 if @@ROWCOUNT  = 0
	insert into iwd_pilotage_chargement (dte_deb_periode, dte_fin_periode)
	select max(dte_Fin_periode) as dte_deb_periode,
	getdate() as dte_fin_periode
	from iwd_pilotage_historique;
		if @@ROWCOUNT  = 0
			print 'Warning : aucune ligne dans la table de pilotage'

  -- 1.2 - Sinon cas de reprise on ne fait rien