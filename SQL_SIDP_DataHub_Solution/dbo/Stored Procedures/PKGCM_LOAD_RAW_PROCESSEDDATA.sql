﻿CREATE PROC [dbo].[PKGCM_LOAD_RAW_PROCESSEDDATA]
    @file_name  nvarchar(100)
  , @file_table nvarchar(100)
AS
BEGIN 
    DECLARE @file_root_path varchar(100);  
    DECLARE @file_full_path varchar(1000);  
    DECLARE @file_stream_id uniqueidentifier;  
    SELECT @file_root_path = FileTableRootPath();  

    DECLARE @file_content varchar(max)
    DECLARE @delimiter char(1)
    SET @delimiter = char(10)

    DECLARE @processed_files CURSOR;

    SET @processed_files = CURSOR FOR
    SELECT [stream_id], @file_root_path + file_stream.GetFileNamespacePath(), convert(varchar(max), file_stream)
      FROM [dbo].[DataCollected]
     WHERE @file_table = 'DataCollected' and file_stream.GetFileNamespacePath() LIKE @file_name
    UNION
    SELECT [stream_id], @file_root_path + file_stream.GetFileNamespacePath(), convert(varchar(max), file_stream)
      FROM [dbo].[DataProcessed]
     WHERE @file_table = 'DataProcessed' and file_stream.GetFileNamespacePath() LIKE @file_name
    ORDER BY 2

    OPEN @processed_files 
    FETCH NEXT FROM @processed_files 
    INTO @file_stream_id, @file_full_path, @file_content

    WHILE @@FETCH_STATUS = 0
    BEGIN
		INSERT INTO [dbo].[RAW_PROCESSEDDATA] ([file_name], [stream_id], [file_full_path], [file_stream_data]) 
		SELECT 
			@file_name,
			@file_stream_id,
			@file_full_path,
			[value]
		FROM
			..STRING_SPLIT(@file_content, @delimiter);
        
        FETCH NEXT FROM @processed_files 
        INTO @file_stream_id, @file_full_path, @file_content
    END; 

    CLOSE @processed_files ;
    DEALLOCATE @processed_files;
END