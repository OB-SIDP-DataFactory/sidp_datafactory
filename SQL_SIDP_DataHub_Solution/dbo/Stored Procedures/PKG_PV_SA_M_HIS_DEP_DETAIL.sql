﻿Create procedure [dbo].[PKG_PV_SA_M_HIS_DEP_DETAIL]
	@date_analyses date 
as
begin
	insert into [$(DataFactoryDatabaseName)].dbo.[PV_SA_M_HIS_DEP_DETAIL]
	(
		[annee],
		[mois],
		[DWHCPTCOM],
		[DWHCPTPPAL],
		[DAT_DEBT_VALD],
		[DAT_FIN_VALD],
		[nbr_jour]
	)
	select 
		Year(@date_analyses) as annee ,Month(@date_analyses) as mois ,
		DWHCPTCOM,
		DWHCPTPPAL,
		DAT_DEBT_VALD,
		--DAT_FIN_VALD as DAT_FIN_VALD_init,
		case when ( DAT_FIN_VALD > @date_analyses and DAT_DEBT_VALD <= @date_analyses ) 
		then @date_analyses else DAT_FIN_VALD 
		end as DAT_FIN_VALD,
		--NB_JOU_OUV,
		--NB_JOU_FER,
		case when  (DAT_FIN_VALD > @date_analyses and DAT_DEBT_VALD <= @date_analyses) then DATEDIFF(D,DAT_DEBT_VALD,@date_analyses) +1 else (NB_JOU_OUV+NB_JOU_FER) end as nbr_jour
	from
		PV_SA_HIS_DEP
	where
		--DWHCPTCOM='10010367156' and
		DAT_DEBT_VALD < @date_analyses 
		--delete les enregistements qui n'existait pas pour cette @date_analyses
end