﻿CREATE PROCEDURE [dbo].[Anonymize_Data]
  @sql NVARCHAR(max)=NULL,
  @S_Table NVARCHAR(max),
  @sqlheader  NVARCHAR(max)=NULL,
  @statement NVARCHAR(max) = NULL,
  @trait NVARCHAR(max),
  @cell NVARCHAR(10)
AS
BEGIN
  SET NOCOUNT ON;
  SELECT
    @sql = 'Select ' +
(SELECT
      CASE WHEN ORDINAL_POSITION=1 AND Sensitive='NO' THEN  +COLUMN_NAME
 WHEN ORDINAL_POSITION=1 AND Sensitive='YES' THEN  'HASHBYTES(''SHA2_256'','''++LTRIM(RTRIM(@cell))+'''+CAST('+COLUMN_NAME+' as Nvarchar(' + CAST(COALESCE(CHARACTER_MAXIMUM_LENGTH, NUMERIC_PRECISION + NUMERIC_SCALE + 2, 27) AS VARCHAR) +'))) as '+ COLUMN_NAME
 WHEN ORDINAL_POSITION!=1 AND Sensitive='NO' THEN ',' +COLUMN_NAME
 WHEN ORDINAL_POSITION!=1 AND Sensitive='YES' THEN  ', HASHBYTES(''SHA2_256'','''++LTRIM(RTRIM(@cell))+'''+CAST('+COLUMN_NAME+' as Nvarchar(' + CAST(COALESCE(CHARACTER_MAXIMUM_LENGTH, NUMERIC_PRECISION + NUMERIC_SCALE + 2, 27) AS VARCHAR) +'))) as '+ COLUMN_NAME
END
 AS 'data()'
    FROM
      REF_TABLE_DS
    WHERE TABLE_NAME=@S_Table AND
      TABLE_SCHEMA ='dbo'
      AND Traitement=@trait
    ORDER BY ORDINAL_POSITION
    FOR XML PATH('') 
 ) + ' FROM '+@S_Table +' WITH(NOLOCK)'
  --print cast(@sql as ntext)
  exec (@sql)

END