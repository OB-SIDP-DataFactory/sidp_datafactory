﻿create proc [dbo].[Rep_EnCoursCredit]
as

IF OBJECT_ID('dbo.R_EnCoursCredit') IS NOT NULL
BEGIN
	drop table R_EnCoursCredit
END;


with req as
(
select d.REF_CPT,
d.CPT_RES_DU,
d.Validity_StartDate,
d.Validity_EndDate,
d.CAT_PRE
from dbo.VW_PV_FI_Q_DES d
inner join dbo.PV_SA_Q_COM cm on cm.COMREFREF = d.REF_CPT
inner join dbo.PV_SA_Q_COMPTE c on c.DWHCPTCOM = cm.COMREFCOM and c.DWHCPTRUB in ('203120','203110')

where d.CAT_PRE in ('NF', 'NFDESEPAR','NFPERDES','NFDESETUD','NFOB') or d.CAT_PRE like 'AF%'

UNION

select 
cast(cre.DWHOPENDO as varchar) as 'ref_cpt',
cre.DWHOPEVAL as 'CPT_RES_DU',
cre.Validity_StartDate,
cre.Validity_EndDate,
'Immo' as 'CAT_PRE'
from [dbo].[VW_PV_SA_M_OPCREDIT] cre
where cre.DWHOPENAT in ('CCT','CE2','CEL', 'CTF','HSU','LTM','NPZ','PE2','PTZ')
)

select * into R_EnCoursCredit  from req

GO
