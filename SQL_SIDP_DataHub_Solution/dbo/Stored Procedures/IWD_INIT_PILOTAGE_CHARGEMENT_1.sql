﻿ CREATE Procedure [dbo].[IWD_INIT_PILOTAGE_CHARGEMENT]
 @dte_deb_period as date=NULL,
 @dte_fin_period as date=NULL
 as
 begin
 -- 1 - Initialisation de la table de paramétrage
 --     Postulat : la table est vide par défaut sauf en cas de reprise

 select @dte_deb_period =dte_deb_periode,
 @dte_fin_period = dte_fin_periode from IWD_PILOTAGE_CHARGEMENT;

 -- 1.1 - Si la table est vide

 if @@ROWCOUNT  = 0 
 begin
	insert into IWD_PILOTAGE_CHARGEMENT (dte_deb_periode, dte_fin_periode)
	select max(dte_Fin_periode) as dte_deb_periode,
	getdate() as dte_fin_periode
	from [dbo].[IWD_PILOTAGE_HISTORIQUE]	WITH(NOLOCK);

		if @@ROWCOUNT  = 0 
			print 'Warning : aucune ligne dans la table de pilotage';
		else 
		print 'Periode de chargement : '+cast(@dte_deb_period as varchar)+';'+cast(@dte_fin_period as varchar)
 end
			
 else 
	print 'Reprise sur la période : '+cast(@dte_deb_period as varchar)+';'+cast(@dte_fin_period as varchar)
end