﻿CREATE PROCEDURE [dbo].[IWD_ALIM_SM_RES_STATE_REASON_FACT_V2]
AS
BEGIN
	DELETE FROM
		[$(DwhIwdDatabaseName)].dbo.IWD_SM_RES_STATE_REASON_FACT_V2
	WHERE
		SM_RES_STATE_REASON_FACT_KEY IN
		(
			SELECT DISTINCT
				SM_RES_STATE_REASON_FACT_KEY
			FROM
				dbo.IWD_SM_RES_STATE_REASON_FACT
		);

	INSERT INTO [$(DwhIwdDatabaseName)].dbo.IWD_SM_RES_STATE_REASON_FACT_V2
	(
		[SM_RES_STATE_REASON_FACT_KEY],
		[TENANT_KEY],
		[CREATE_AUDIT_KEY],
		[UPDATE_AUDIT_KEY],
		[START_DATE_TIME_KEY],
		[END_DATE_TIME_KEY],
		[RESOURCE_STATE_KEY],
		[RESOURCE_STATE_REASON_KEY],
		[MEDIA_TYPE_KEY],
		[RESOURCE_KEY],
		[RESOURCE_GROUP_COMBINATION_KEY],
		[SM_RES_SESSION_FACT_SDT_KEY],
		[SM_RES_SESSION_FACT_KEY],
		[SM_RES_STATE_FACT_SDT_KEY],
		[SM_RES_STATE_FACT_KEY],
		[START_TS],
		[END_TS],
		[TOTAL_DURATION],
		[LEAD_CLIP_DURATION],
		[TRAIL_CLIP_DURATION],
		[ACTIVE_FLAG],
		[PURGE_FLAG],
		[REASON_VALUE]
	)
	SELECT
		FACT.[SM_RES_STATE_REASON_FACT_KEY],
		FACT.[TENANT_KEY],
		FACT.[CREATE_AUDIT_KEY],
		FACT.[UPDATE_AUDIT_KEY],
		FACT.[START_DATE_TIME_KEY],
		FACT.[END_DATE_TIME_KEY],
		FACT.[RESOURCE_STATE_KEY],
		FACT.[RESOURCE_STATE_REASON_KEY],
		FACT.[MEDIA_TYPE_KEY],
		FACT.[RESOURCE_KEY],
		FACT.[RESOURCE_GROUP_COMBINATION_KEY],
		FACT.[SM_RES_SESSION_FACT_SDT_KEY],
		FACT.[SM_RES_SESSION_FACT_KEY],
		FACT.[SM_RES_STATE_FACT_SDT_KEY],
		FACT.[SM_RES_STATE_FACT_KEY],
		FACT.[START_TS],
		FACT.[END_TS],
		FACT.[TOTAL_DURATION],
		FACT.[LEAD_CLIP_DURATION],
		FACT.[TRAIL_CLIP_DURATION],
		FACT.[ACTIVE_FLAG],
		FACT.[PURGE_FLAG],
		ISNULL(r.SOFTWARE_REASON_VALUE, r.HARDWARE_REASON)
	FROM
		dbo.IWD_SM_RES_STATE_REASON_FACT									AS	FACT	WITH(NOLOCK)
		LEFT JOIN  [$(DwhIwdDatabaseName)].dbo.IWD_RESOURCE_STATE_REASON	AS	r		WITH(NOLOCK)
			ON	FACT.RESOURCE_STATE_REASON_KEY = r.RESOURCE_STATE_REASON_KEY

	--------- supression des retrait dupliqués sur plusiquers canal ( Media : Voice , Case , Chat  ) 
	DELETE w 
	FROM
		[$(DwhIwdDatabaseName)].dbo.IWD_SM_RES_STATE_REASON_FACT_V2				AS	w	WITH(NOLOCK)
		INNER JOIN	[$(DwhIwdDatabaseName)].dbo.IWD_SM_RES_STATE_REASON_FACT_V2	AS	e	WITH(NOLOCK)
			ON	w.RESOURCE_KEY = e.RESOURCE_KEY
				AND	w.[REASON_VALUE]=e.[REASON_VALUE]
				AND	w.MEDIA_TYPE_KEY<>e.MEDIA_TYPE_KEY
				AND	w.START_TS >= e.START_TS
				AND	w.END_TS <= e.END_TS
	WHERE
		e.MEDIA_TYPE_KEY = 1;

	------------------------------------------------------------
	DELETE w 
	FROM
		[$(DwhIwdDatabaseName)].dbo.IWD_SM_RES_STATE_REASON_FACT_V2				AS	w	WITH(NOLOCK)
		INNER JOIN [$(DwhIwdDatabaseName)].dbo.IWD_SM_RES_STATE_REASON_FACT_V2	AS	e	WITH(NOLOCK)
			ON	w.RESOURCE_KEY = e.RESOURCE_KEY 
				AND	w.[REASON_VALUE] = e.[REASON_VALUE]
				AND	w.MEDIA_TYPE_KEY <> e.MEDIA_TYPE_KEY
				AND	w.START_TS >= e.START_TS
				AND	w.END_TS <= e.END_TS
	WHERE
		e.MEDIA_TYPE_KEY = 1021;

	------------------------------------------------------------- 
	DELETE w 
	FROM
		[$(DwhIwdDatabaseName)].dbo.IWD_SM_RES_STATE_REASON_FACT_V2				AS	w	WITH(NOLOCK)
		INNER JOIN	[$(DwhIwdDatabaseName)].dbo.IWD_SM_RES_STATE_REASON_FACT_V2	AS	e	WITH(NOLOCK)
			ON	w.RESOURCE_KEY = e.RESOURCE_KEY
				AND	w.[REASON_VALUE] = e.[REASON_VALUE]
				AND	w.MEDIA_TYPE_KEY <> e.MEDIA_TYPE_KEY
				AND	w.START_TS >= e.START_TS
				AND	w.END_TS <= e.END_TS
	WHERE
		e.MEDIA_TYPE_KEY = 1022;

END