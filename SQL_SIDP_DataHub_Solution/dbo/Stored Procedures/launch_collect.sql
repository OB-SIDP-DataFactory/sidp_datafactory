﻿CREATE PROC [dbo].[launch_collect]
AS
BEGIN
if object_id('tempdb..#MaTable') is not null
begin drop table #MaTable end
DECLARE @cols AS NVARCHAR(MAX),
@query  AS NVARCHAR(MAX)

SELECT distinct [batch_name],case when flag_actif=1 then 0 else 1 end AS Flag
INTO #MaTable
FROM [dbo].[Param_SSISPackages_Actif] 
select @cols = STUFF((SELECT distinct ',' + QUOTENAME(batch_name) 
    from  #MaTable 
    FOR XML PATH(''), TYPE
        ).value('.', 'NVARCHAR(MAX)') 
    ,1,1,'')

set @query = 'SELECT  ' + @cols + ' from #MaTable 

        pivot 
        (
            MIN([Flag])
            for [batch_name] in (' + @cols + ')
        ) p '
--print(@query)
execute(@query)
END