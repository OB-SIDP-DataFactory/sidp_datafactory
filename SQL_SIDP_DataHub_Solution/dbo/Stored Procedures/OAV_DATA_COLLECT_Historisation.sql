﻿CREATE PROCEDURE [dbo].[OAV_DATA_COLLECT_Historisation]
	@sql NVARCHAR(max)=NULL,
	@Table_src NVARCHAR(max)=NULL,
	@Table_cible NVARCHAR(max)=NULL

AS
--Curseur sur les tables  à charger il faut que flag_Use = 1
DECLARE Oav_cursor CURSOR FOR
 SELECT
	table_source,
	table_cible
FROM
	Param_OAV_Collecte_Historisation
WHERE Flag_Histo = 1;

--Boucle sur les tables  à charger
OPEN Oav_cursor
FETCH NEXT FROM  Oav_cursor INTO @Table_src, @Table_cible
WHILE @@FETCH_STATUS = 0
BEGIN

	--Truncate de la table cible
	--SELECT @sql = 'TRUNCATE TABLE '+ @Table_cible + ';'

	--exec(@sql);

	--Exécution de l'alimentation
	EXEC OAV_LOAD_DATA_SCRIPT_HISTORISATION @Table_source = @Table_src;

	FETCH NEXT FROM Oav_cursor INTO @Table_src, @Table_cible
END
--end loop

--clean up
CLOSE Oav_cursor
DEALLOCATE Oav_cursor