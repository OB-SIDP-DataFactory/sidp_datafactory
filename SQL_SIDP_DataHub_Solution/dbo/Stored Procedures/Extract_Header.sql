﻿CREATE PROCEDURE [dbo].[Extract_Header]
  @S_Table NVARCHAR(max) ,
  @trait NVARCHAR(max),
  @sqlheader  NVARCHAR(max)=NULL
AS
SELECT
  @sqlheader = 'Select ' +
(SELECT
    CASE WHEN ORDINAL_POSITION=1 AND Sensitive='NO' THEN ''''  +COLUMN_NAME +''' as ' +COLUMN_NAME 
 WHEN ORDINAL_POSITION=1 AND Sensitive='YES' THEN  ''''  +COLUMN_NAME +''' as ' +COLUMN_NAME 
 WHEN ORDINAL_POSITION!=1 AND Sensitive='NO' THEN ',' + ''''  +COLUMN_NAME +''' as ' +COLUMN_NAME 
 WHEN ORDINAL_POSITION!=1 AND Sensitive='YES' THEN ','''  +COLUMN_NAME +''' as ' +COLUMN_NAME 
END
 AS 'data()'
  FROM
    REF_TABLE_DS
  WHERE TABLE_NAME=@S_Table AND
    TABLE_SCHEMA ='dbo'
    AND Traitement=@trait
  ORDER BY ORDINAL_POSITION
  FOR XML PATH('') 
 )
exec (@sqlheader)