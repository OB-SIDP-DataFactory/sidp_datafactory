﻿create proc [dbo].[Rep_EnCoursHistorique]
as

IF OBJECT_ID('dbo.R_EnCoursHistorique') IS NOT NULL
BEGIN
	drop table R_EnCoursHistorique
END;


with req as
(
select
    c.DWHCPTCOM,
    -c.DWHCPTCPT as 'Encours',
	c.DWHCPTRUB,
	c.Validity_StartDate,
	c.Validity_EndDate
from
    dbo.VW_PV_SA_Q_COMPTE c
	INNER JOIN dbo.[PV_FE_EQUIPMENT_V2] equ on equ.EQUIPMENT_NUMBER = c.DWHCPTCOM 	and cast(equ.Validity_EndDate as date) = '9999-12-31'
inner join dbo.PV_FE_ENROLMENTFOLDER_V2 f on f.ID_FE_ENROLMENT_FOLDER = equ.ENROLMENT_FOLDER_ID and cast(f.Validity_EndDate as date) = '9999-12-31'
inner join dbo.PV_SF_OPPORTUNITY o on o.Id_SF = f.SALESFORCE_OPPORTUNITY_ID and o.StageName = '09'
where
    c.DWHCPTRUB in ('251180', '254111')
)

select * into R_EnCoursHistorique  from req

GO

