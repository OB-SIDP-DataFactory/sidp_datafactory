﻿-- Batch submitted through debugger: SQLQuery6.sql|7|0|C:\Users\A_GB3376\AppData\Local\Temp\55\~vs154B.sql
CREATE PROCEDURE [dbo].[OAV_LOAD_DATA_SCRIPT]
@Table_source as VARCHAR(1000),
@Table_cible as VARCHAR(1000)=NULL,
@sql_exec as varchar(max)=NULL,
@sql_header as varchar(max)=NULL,
@sql_insert as varchar(max)=NULL,
@sql_select as varchar(max)=NULL,
@sql_update as varchar(max)=NULL,
@dte_deb_period as date=NULL,
@dte_fin_period as date=NULL,
@dte_obsrv as varchar(50)=NULL,
@dte_chrg as  varchar(50)=NULL,
@Dte_deb_exec as datetime=NULL,
@Dte_fin_exec as datetime=NULL,
@resultat_exec as  varchar(max)=NULL,
@Schema_Oracle as varchar(50)=NULL,
@rowcount as INT=NULL

as
-- I - init variable schema Oracle

select @Schema_Oracle = nom_schema from Param_OAV_SCHEMA_ORACLE;

select @Table_cible = table_cible from Param_OAV_Collecte
where table_source = @Table_source;
SET @dte_obsrv = dbo.retrieve_param_collect('OAV' , 'PKGCO_SSIS_OAV_ALIMENTATION') ;
select @dte_chrg= getdate();

-- II - L'instruction en SQL dynamique sera découpée en 3
-- 2.1 -  une partie écriture du header
-- 2.2 -  une partie sql_insert pour créer en dynamique les cast côté Sql server
-- 2.3 -  une partie sql_select pour créer la requête select comprenant le to_char côté Oracle
-- 2.4 -  une partie sql_update  pour gerer les date obdrv et chrg 
-- 2.1 - Ecriture du header

select @sql_header = 'insert into '+ @Table_cible+ '('+
(SELECT 
  cible_column_name +',' 
 AS 'data()' 
 FROM Param_OAV_TABLE_STRUCT
 where cible_table_name = @Table_cible
 FOR XML PATH('') 
 ) 

 --retrait de la dernière virgule
 select @sql_header = SUBSTRING(@sql_header, 1, LEN(@sql_header) - 1)+')'

-- 2.2 - création de sql_insert

select @sql_insert = @sql_header + ' SELECT ' + 
(SELECT 
 case when source_data_type like 'TIMESTAMP%' then 'convert(datetime, ' + cible_column_name +', 120) ' 
 else 'cast('+source_column_name+' as '+ cible_data_type +')' end + ' as ' +cible_column_name  + ','
 AS 'data()' 
 FROM Param_OAV_TABLE_STRUCT
 where cible_table_name = @Table_cible
 FOR XML PATH('') 
 ) ;
 --retrait de la dernière virgule
 select @sql_insert = SUBSTRING(@sql_insert, 1, LEN(@sql_insert) - 1) 

 -- 2.2 création de sql_select
select @sql_select = ' 
FROM OPENQUERY(OAV, ''SELECT ' +
(SELECT 
 case when source_data_type like 'TIMESTAMP%' then 'to_char(' + source_column_name +', ''''yyyy-mm-dd hh24:mi:ss'''') ' else source_column_name end + ' as ' +source_column_name  + ','
 AS 'data()' 
 FROM Param_OAV_TABLE_STRUCT
 where cible_table_name = @Table_cible
 FOR XML PATH('') 
 ) ;
 -- tracing du traitement 
  select @dte_deb_period = dte_deb_periode from dbo.OAV_pilotage_chargement;
  select @dte_fin_period = dte_fin_periode from dbo.OAV_pilotage_chargement;
 
 --Retrait de la dernière virgule
 select @sql_select = SUBSTRING(@sql_select, 1, LEN(@sql_select) - 1) +' from '+@Schema_Oracle+'.'+@Table_source+''') f;'
--join IWD_CTL_AUDIT_LOG a on (F.UPDATE_AUDIT_KEY = A.AUDIT_KEY OR (F.CREATE_AUDIT_KEY = A.AUDIT_KEY AND F.UPDATE_AUDIT_KEY =0) )
--a.created between cast('''+cast(@dte_deb_period as varchar)+''' as date) and cast('''+cast(@dte_fin_period as varchar)+''' as date)';

 -- 2.4 Creation du Sql Update
 select @sql_update =  'update '+ @Table_cible + ' Set DAT_OBSR =  ' + 'Cast(''' + @dte_obsrv + ''' as date) , DAT_CHRG = Cast('''+  @dte_chrg + ''' As datetime)' ;
 
 -- Construction de la requete à exécuter
  select @sql_exec = @sql_insert + @sql_select +  @sql_update

  print (@sql_exec)
  --print (@sql_update)
  
  select @Dte_deb_exec = getdate();
  select @resultat_exec = 'OK';

  -- Execution de la requete
  begin try
  exec (@sql_exec)
  end try
  begin catch
	select @resultat_exec = ERROR_MESSAGE()
  end catch

  set @rowcount = @@ROWCOUNT;
  select @Dte_fin_exec = getdate();

  -- tracing du traitement 

  insert into OAV_pilotage_historique
  values (
  @Table_source, 
  @Table_cible,
  @dte_deb_period,
  @dte_fin_period,
  @Dte_deb_exec,
  @Dte_fin_exec,
  @resultat_exec,
  Null,
  @sql_exec)