﻿create view TENANT as
select
   ID as TENANT_KEY,
   NAME as TENANT_NAME,
   ID as TENANT_CFG_DBID,
   CREATED_TS as START_TS,
   DELETED_TS as END_TS,
   CREATE_AUDIT_KEY as CREATE_AUDIT_KEY,
   UPDATE_AUDIT_KEY as UPDATE_AUDIT_KEY
from
   IWD_GIDB_GC_TENANT
UNION ALL
SELECT     - 1 AS TENANT_KEY,
           'UNKNOWN' AS TENANT_NAME,
           - 1  AS TENANT_CFG_DBID,
           -1   AS START_TS,
           -1   AS END_TS,
           -1   AS CREATE_AUDIT_KEY,
           -1   AS UPDATE_AUDIT_KEY
FROM IWD_dual
UNION ALL
SELECT     - 2 AS TENANT_KEY,
           'NO_VALUE' AS TENANT_NAME,
           - 1  AS TENANT_CFG_DBID,
           -1   AS START_TS,
           -1   AS END_TS,
           -1   AS CREATE_AUDIT_KEY,
           -1   AS UPDATE_AUDIT_KEY
FROM IWD_dual