﻿CREATE VIEW [dbo].[VW_LAB005_IMM]
AS
SELECT
	CAST(FORMAT(GETDATE(), 'yyyyMMdd') AS VARCHAR(8))	AS	[DT_EXT_ENR],
	CAST('' AS VARCHAR(8))								AS	[DT_OPE],
	CAST('' AS VARCHAR(32))								AS	[ID_OPE],
	CAST('' AS VARCHAR(32))								AS	[ID_CPT],
	CAST('' AS VARCHAR(15))								AS	[CD_TXN],
	CAST('' AS VARCHAR(20))								AS	[CD_OPE],
	CAST('' AS VARCHAR(20))								AS	[CD_EVE],
	CAST('' AS VARCHAR(20))								AS	[CD_NAT_OPE],
	CAST('' AS VARCHAR(8))								AS	[DT_VAL],
	CAST('' AS VARCHAR(255))							AS	[LB_OPE],
	CAST('' AS VARCHAR(3))								AS	[CD_DVS_OPE],
	CAST('' AS VARCHAR(3))								AS	[CD_DVS_BAS],
	CAST('' AS CHAR(1))								AS	[CD_DBI_CRD],
	CAST(FORMAT(ABS(0), 'F2') AS VARCHAR(32))			AS	[MT_OPE_DVS],
	CAST(FORMAT(ABS(0), 'F2') AS VARCHAR(32))			AS	[MT_OPE_EUR],
	CAST('' AS VARCHAR(6))								AS	[CD_ANL],
	CAST('' AS VARCHAR(32))								AS	[ID_ETB_CTP],
	CAST('' AS VARCHAR(32))								AS	[ID_CTP],
	CAST('' AS VARCHAR(35))								AS	[ID_IBAN_CTP],
	CAST('' AS VARCHAR(70))								AS	[NM_CTP],
	CAST('' AS VARCHAR(8))								AS	[DT_TXN],
	CAST('' AS CHAR(1))								AS	[CD_SEN_SOL],
	CAST(FORMAT(ABS(0), 'F2') AS VARCHAR(32))			AS	[MT_SOL_DVS],
	CAST(FORMAT(ABS(0), 'F2') AS VARCHAR(32))			AS	[MT_SOL_EUR],
	CAST('' AS VARCHAR(8))								AS	[DT_DER_MVT],
	CAST('001_GB_' + FORMAT(GETDATE(), 'yyyyMMdd') + '_TRANSACTION_BANK_DAILY.txt' AS VARCHAR(256)) AS [FileName],
	CAST(FORMAT(GETDATE(), 'yyyyMMdd') AS VARCHAR(8))	AS	[ValueDate],
	CAST(FORMAT(GETDATE(), 'yyyyMMdd') AS VARCHAR(8))	AS	[LastTransactionDate],
	CAST(FORMAT(GETDATE(), 'yyyyMMdd') AS VARCHAR(8))	AS	[OperationDate]
GO

--SELECT DISTINCT
--    -- Date extraction
--    FORMAT(GETDATE(), 'yyyyMMdd')					AS	[DT_EXT_ENR],
--    -- Date opération
--    --FORMAT(EVE.DTE_EMI, 'yyyyMMdd') +
--    NULL	AS [DATE_OPERATION],
--    -- Numéro opération
--    FORMAT(GETDATE(), 'yyyyMMdd') + RIGHT(REPLICATE('0', 8) + ROW_NUMBER() OVER (PARTITION BY '1' ORDER BY GETDATE()), 8)	AS	[Numéro opération],
--    -- Numéro de compte/prêt/portefeuille
--    --IMM.NUM_PRE +
--    '|' +
--    -- Code transaction
--    'CRE' +
--    --EVE.TYP_EVE +
--    '----------'	AS	[Code transaction],
--    -- Code opération
--    'CRE'	AS	[Code opération],
--    -- Code évènement
--    --EVE.TYP_EVE +
--    NULL	AS	[Code événement],
--    -- Code nature opération
--    NULL	AS	[Code nature opération],
--    -- Date valeur
--    --FORMAT(EVE.DTE_CPT, 'yyyyMMdd') +
--    '|' +
--    -- Libellé de l'opération
--    --CASE
--    --  WHEN        EVE.TYP_EVE = '00'
--    --      THEN    LEFT('Mise à disposition crédit' + REPLICATE('*', 39), 39)
--    --  WHEN        EVE.TYP_EVE = '02'
--    --      THEN    LEFT('Échéance crédit (capital + intérêts)' + REPLICATE('*', 39), 39)
--    --  WHEN        EVE.TYP_EVE = '03'
--    --      THEN    LEFT('Paiement intérêts sur crédit' + REPLICATE('*', 39), 39)
--    --  WHEN        EVE.TYP_EVE = '04'
--    --      THEN    LEFT('Paiement échéance crédit' + REPLICATE('*', 39), 39)
--    --  WHEN        EVE.TYP_EVE = 'RP'
--    --      THEN    LEFT('RAP crédit' + REPLICATE('*', 39), 39)
--    --  WHEN        EVE.TYP_EVE = 'RT'
--    --      THEN    LEFT('RAT crédit' + REPLICATE('*', 39), 39)
--    --END +
--    --IMM.NUM_PRE + EVE.TYP_EVE + FORMAT(EVE.DTE_EMI, 'yyyyMMdd') + EVE.SEQ_COP +
--    '|' +         
--    -- Devise opération
--    --EVE.DEV_REG +
--    '|' +
--    -- Devise de base
--    'EUR'	AS	[Devise de base],
--    -- Code sens montant opération
--    --CASE
--    --  WHEN        EVE.TYP_EVE = '00'
--    --      THEN    'D'
--    --  WHEN        EVE.TYP_EVE IN ('02', '03', '04', 'RP', 'RT')
--    --      THEN    'C'
--    --END +
--    '|' +
--    -- Montant en devise
--    --FORMAT(ABS(EVE.MTT_REG), 'N') +
--    NULL	AS	[Montant en devise],
--    -- Montant en contrevaleur euro
--    --CASE
--    --  WHEN        EVE.DEV_REG = 'EUR'
--    --      THEN    FORMAT(ABS(EVE.MTT_REG), 'N')
--    --END +
--    '|' +
--    -- Code analytique
--    '|' +
--    -- BIC établissement contrepartie
--    '|' +
--    -- Identifiant contrepartie
--    NULL	AS	[Identifiant contrepartie],
--    -- IBAN contrepartie
--    '|' +
--    -- Nom/Raison sociale contrepartie
--    '|' +
--    -- Date traitement opération
--    --FORMAT(EVE.DTE_EMI, 'yyyyMMdd') +
--    '|' +
--    -- Sens solde compte (Montant capital restant dû)
--    --CASE
--    --  WHEN        IMM.CAP_RES_DU >= 0
--    --      THEN    'C'
--    --  ELSE        'D'
--    --END +
--    '|' +
--    -- Solde compte en devise (Montant capital restant dû)
--    --FORMAT(ABS(IMM.CAP_RES_DU), 'N') +
--    NULL	AS	[Solde compte en devise],
--    -- Solde compte en contrevaleur euro (Montant capital restant dû)
--    --CASE
--    --  WHEN        IMM.DEV = 'EUR'
--    --      THEN    FORMAT(ABS(IMM.CAP_RES_DU), 'N')
--    --END +
--    '|' +
--    -- Date dernier mouvement compte
--    --FORMAT(IMM.DTE_CAP_RES_DU, 'yyyyMMdd') +
--    NULL	AS	[Date dernier mouvement compte]
----FROM
----  TMP_IGP_IND_EVE_IMM                         AS  EVE
----  INNER JOIN  TMP_STK_PRE_IMM                 IMM
----      ON  IMM.NUM_PRE = LPAD(EVE.NUM_PRE, 11, '0')
----  INNER JOIN  SIDP_DataHub.dbo.PV_SA_Q_CLIENT CLI WITH(NOLOCK)
----      ON  CLI.DWHCLICLI = EVE.COD_CLI
----WHERE
----  1 = 1
----  AND CLI.DWHCLIRES   NOT IN
----  (
----      'C01','C02','C03',
----      'D01','D05',
----      'T01','T02','T03',
----      '650','651','652','653','654','655','656','657','658','659',
----      '660','661','662','663','664','665','666','667','668','669',
----      '670','671','672','673','674','675','676'
----  )
----  AND EVE.TYP_EVE IN ('00', '02', '03', '04', 'RP', 'RT')