﻿CREATE VIEW [dbo].[VW_LAB005_PTF]
AS
SELECT
	CAST(FORMAT(GETDATE(), 'yyyyMMdd') AS VARCHAR(8))					AS	[DT_EXT_ENR],
	CAST(ISNULL(FORMAT(OPE.DWHOPRDTO, 'yyyyMMdd'), '') AS VARCHAR(8))	AS	[DT_OPE],
	CAST(RIGHT(REPLICATE('0', 16) + LTRIM(RTRIM(OPE.DWHOPRREF)), 16) AS VARCHAR(32))	AS	[ID_OPE],
	CAST(ISNULL(LTRIM(RTRIM(OPE.DWHOPRNPT)), '') AS VARCHAR(32))		AS	[ID_CPT],
	CAST(
		ISNULL(LTRIM(RTRIM(OPE.DWHOPRTOP)), '')
		+ RIGHT(REPLICATE('0', 3) + LTRIM(RTRIM(OPE.DWHOPRSOP)), 3)
		+ '----' AS VARCHAR(15))										AS	[CD_TXN],
	CAST(ISNULL(SUBSTRING(OPE.DWHOPRTOP, 1, 3), '') AS VARCHAR(20))		AS	[CD_OPE],
	CAST(ISNULL(SUBSTRING(OPE.DWHOPRTOP, 4, 3), '') AS VARCHAR(20))		AS	[CD_EVE],
	CAST('' AS VARCHAR(20))												AS	[CD_NAT_OPE],
	CAST(ISNULL(FORMAT(OPE.DWHOPRDTV, 'yyyyMMdd'), '')AS VARCHAR(8))	AS	[DT_VAL],
	CAST(ISNULL(LTRIM(RTRIM(REPLACE(OPE.DWHOPRLBO, '|', ' '))), '') AS VARCHAR(255))	AS	[LB_OPE],
	CAST(ISNULL(LTRIM(RTRIM(OPE.DWHOPRDVC)), '') AS VARCHAR(3))			AS	[CD_DVS_OPE],
	CAST('EUR' AS VARCHAR(3))											AS	[CD_DVS_BAS],
	CAST(IIF(OPE.DWHOPRSOP = '3', 'C', 'D') AS CHAR(1))				AS	[CD_DBI_CRD],
	CAST(FORMAT(ABS(ISNULL(OPE.DWHOPRMNC, 0)), 'F2') AS VARCHAR(32))	AS	[MT_OPE_DVS],
	CAST(FORMAT(ABS(ISNULL(OPE.DWHOPRMNR, 0)), 'F2') AS VARCHAR(32))	AS	[MT_OPE_EUR],
	CAST('' AS VARCHAR(6))												AS	[CD_ANL],
	CAST(ISNULL(LTRIM(RTRIM(OPE.DWHOPRCNG)), '') AS VARCHAR(32))		AS	[ID_ETB_CTP],
	CAST(ISNULL(LTRIM(RTRIM(OPE.DWHOPRRNG)), '') AS VARCHAR(32))		AS	[ID_CTP],
	CAST('' AS VARCHAR(35))												AS	[ID_IBAN_CTP],
	CAST(ISNULL(LTRIM(RTRIM(OPE.DWHOPRLNG)), '') AS VARCHAR(70))		AS	[NM_CTP],
	CAST(ISNULL(FORMAT(OPE.DWHOPRDTR, 'yyyyMMdd'), '') AS VARCHAR(8))	AS	[DT_TXN],
	CAST(IIF(PTF.DWHPTFVGL >= 0, 'C', 'D') AS CHAR(1))				AS	[CD_SEN_SOL],
	CAST(FORMAT(ABS(ISNULL(PTF.DWHPTFVGL, 0)), 'F2') AS VARCHAR(32))	AS	[MT_SOL_DVS],
	CAST(FORMAT(ABS(ISNULL(PTF.DWHPTFVGL, 0)), 'F2') AS VARCHAR(32))	AS	[MT_SOL_EUR],
	CAST(ISNULL(FORMAT(OPE.DWHOPRDTR, 'yyyyMMdd'), '') AS VARCHAR(8))	AS	[DT_DER_MVT],
	CAST('001_GB_' + FORMAT(GETDATE(), 'yyyyMMdd') + '_TRANSACTION_BANK_DAILY.txt' AS VARCHAR(256)) AS [FileName],
	CAST(ISNULL(FORMAT(OPE.DWHOPRDTR, 'yyyyMMdd'), '') AS VARCHAR(8))	AS	[ValueDate],
	CAST(ISNULL(FORMAT(OPE.DWHOPRDTR, 'yyyyMMdd'), '') AS VARCHAR(8))	AS	[LastTransactionDate],
	CAST(ISNULL(FORMAT(OPE.DWHOPRDTO, 'yyyyMMdd'), '') AS VARCHAR(8))	AS	[OperationDate]
FROM
	dbo.PV_SA_M_OPTITRE						AS	OPE	WITH(NOLOCK)
	INNER JOIN	dbo.PV_SA_M_PORTEFEUILLE	AS	PTF	WITH(NOLOCK)
		ON	PTF.DWHPTFNPT = OPE.DWHOPRNPT
	INNER JOIN  dbo.PV_SA_Q_REF				AS  REF WITH(NOLOCK)
		ON  REF.CLIREFCOR = 'SA'
			AND REF.CLIREFCLI = PTF.DWHPTFRCL
	INNER JOIN  dbo.PV_SA_Q_CLIENT			AS  CLI WITH(NOLOCK)
		ON  CLI.DWHCLICLI = REF.CLIREFCLI
WHERE
	1 = 1
	AND CLI.DWHCLIAGE NOT IN ('51', '52')
	AND CLI.DWHCLIRES   NOT IN
	(
		'C01','C02','C03',
		'D01','D05',
		'T01','T02','T03',
		'650','651','652','653','654','655','656','657','658','659',
		'660','661','662','663','664','665','666','667','668','669',
		'670','671','672','673','674','675','676'
	)
	AND ISNULL(OPE.DWHOPRTOP, '') LIKE 'LG01__01%'
	AND ISNULL(OPE.DWHOPRSOP, '') IN ('3', '4')
GO

--SELECT DISTINCT
--    -- Date extraction
--    FORMAT(GETDATE(), 'yyyyMMdd')					AS	[DT_EXT_ENR],
--    -- Date opération
--    ISNULL(FORMAT(OPE.DWHOPRDTO, 'yyyyMMdd'), '')	AS [DATE_OPERATION],
--    -- Numéro opération
--    RIGHT(REPLICATE('0', 16) + LTRIM(RTRIM(OPE.DWHOPRREF)), 16)	AS	[Numéro opération],
--    -- Numéro de compte/prêt/portefeuille
--    ISNULL(LTRIM(RTRIM(OPE.DWHOPRNPT)), '') + '|' +
--    -- Code transaction
--    ISNULL(LTRIM(RTRIM(OPE.DWHOPRTOP)), '')
--    + RIGHT(REPLICATE('0', 3) + LTRIM(RTRIM(OPE.DWHOPRSOP)), 3)
--    + '----'	AS	[Code transaction],
--    -- Code opération
--    ISNULL(SUBSTRING(OPE.DWHOPRTOP, 1, 3), '')	AS	[Code opération],
--    -- Code évènement
--    ISNULL(SUBSTRING(OPE.DWHOPRTOP, 4, 3), '')	AS	[Code événement],
--    -- Code nature opération
--    NULL	AS	[Code nature opération],
--    -- Date valeur
--    ISNULL(FORMAT(OPE.DWHOPRDTV, 'yyyyMMdd'), '') + '|' +
--    --Libellé de l'opération
--    ISNULL(LTRIM(RTRIM(REPLACE(OPE.DWHOPRLBO, '|', ' '))), '') + '|' +
--    -- Devise opération
--    ISNULL(LTRIM(RTRIM(OPE.DWHOPRDVC)), '') + '|' +
--    -- Devise de base
--    'EUR'	AS	[Devise de base],
--    -- Code sens montant opération
--    CASE
--        WHEN        OPE.DWHOPRSOP = '3'
--            THEN    'C'
--        ELSE        'D'
--    END + '|' +
--    -- Montant en devise
--    FORMAT(ABS(ISNULL(OPE.DWHOPRMNC, 0)), 'N')	AS	[Montant en devise],
--    -- Montant en contrevaleur euro
--    FORMAT(ABS(ISNULL(OPE.DWHOPRMNR, 0)), 'N') + '|' +
--    -- Code analytique
--    '|' +
--    -- BIC établissement contrepartie
--    ISNULL(LTRIM(RTRIM(OPE.DWHOPRCNG)), '') + '|' +
--    -- Identifiant contrepartie
--    ISNULL(LTRIM(RTRIM(OPE.DWHOPRRNG)), '')	AS	[Identifiant contrepartie],
--    -- IBAN contrepartie
--    '|' +
--    -- Nom/Raison sociale contrepartie
--    ISNULL(LTRIM(RTRIM(OPE.DWHOPRLNG)), '') + '|' +
--    -- Date traitement opération
--    FORMAT(OPE.DWHOPRDTR, 'yyyyMMdd') + '|' +
--    -- Sens solde compte
--    CASE
--        WHEN        PTF.DWHPTFVGL >= 0
--            THEN    'C'
--        ELSE        'D'
--    END + '|' +
--    -- Solde compte en devise
--    FORMAT(ABS(ISNULL(PTF.DWHPTFVGL, 0)), 'N')	AS	[Solde compte en devise],
--    -- Solde compte en contrevaleur euro
--    FORMAT(ABS(ISNULL(PTF.DWHPTFVGL, 0)), 'N') + '|' +
--    -- Date dernier mouvement compte
--    ISNULL(FORMAT(OPE.DWHOPRDTR, 'yyyyMMdd'), '')	AS	[Date dernier mouvement compte]
--FROM
--    dbo.PV_SA_M_OPTITRE                    AS  OPE WITH(NOLOCK)
--    JOIN    MAX_DATE
--        ON  MAX_DATE.MAX_DTE_TRT = OPE.DWHOPRDTO
--    INNER JOIN  dbo.PV_SA_M_PORTEFEUILLE   AS  PTF WITH(NOLOCK)
--        ON  PTF.DWHPTFNPT = OPE.DWHOPRNPT
--    INNER JOIN  dbo.PV_SA_Q_REF            AS  REF WITH(NOLOCK)
--        ON  REF.CLIREFCOR = 'SA'
--            AND REF.CLIREFCLI = PTF.DWHPTFRCL
--    INNER JOIN  dbo.PV_SA_Q_CLIENT AS  CLI WITH(NOLOCK)
--        ON  CLI.DWHCLICLI = REF.CLIREFCLI
--WHERE
--    1 = 1
--    AND CLI.DWHCLIAGE NOT IN ('51', '52')
--    AND CLI.DWHCLIRES   NOT IN
--    (
--        'C01','C02','C03',
--        'D01','D05',
--        'T01','T02','T03',
--        '650','651','652','653','654','655','656','657','658','659',
--        '660','661','662','663','664','665','666','667','668','669',
--        '670','671','672','673','674','675','676'
--    )
--    AND OPE.DWHOPRTOP LIKE 'LG01__01%'
--    AND OPE.DWHOPRSOP IN ('3', '4')