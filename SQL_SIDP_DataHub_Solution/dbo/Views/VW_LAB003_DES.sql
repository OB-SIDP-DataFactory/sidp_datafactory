﻿CREATE VIEW [dbo].[VW_LAB003_DES]
AS
SELECT DISTINCT
	CAST(FORMAT(GETDATE(), 'yyyyMMdd') AS VARCHAR(8))						AS	[DT_EXT_ENR],
	CAST(ISNULL(LTRIM(RTRIM(REF.COMREFCOM)), '') AS VARCHAR(32))			AS	[ID_CPT],
	CAST('BANQUE' AS VARCHAR(6))											AS	[IC_UNI_ORG],
	CAST(ISNULL(LTRIM(RTRIM(CPT.DWHCPTINT)), '') AS VARCHAR(50))			AS	[LB_CPT],
	CAST(ISNULL(LTRIM(RTRIM(CPT.DWHCPTPPAL)), '') AS VARCHAR(7))			AS	[ID_SAB_CLI],
	CAST('' AS VARCHAR(100))												AS	[ID_GRC_CLI_JOI_1],
	CAST('' AS VARCHAR(100))												AS	[ID_GRC_CLI_JOI_2],
	CAST(
		LTRIM(
			RTRIM(
				ISNULL(
					IIF(
						(ISNULL(CLI.DWHCLITIE, '') = '' AND R.CLIREFCOR IN ('SI', 'SF', 'AC') AND R.CLIREFREF NOT LIKE 'I%' AND R.CLIREFREF NOT LIKE 'T%'),
						IIF(R.CLIREFCOR = 'SF', 'SF', '') + R.CLIREFREF,
						''
					),
					''
				)
			)
		) AS VARCHAR(100))													AS	[ID_GRC_CLI],
	CAST(ISNULL(LTRIM(RTRIM(PRD.code_produit_SAB)), '') AS VARCHAR(4))		AS	[CD_TYP_CPT],
	CAST(ISNULL(LTRIM(RTRIM(D.COD_DEV)), '') AS VARCHAR(3))					AS	[CD_DVS_CPT],
	CAST('' AS VARCHAR(3))													AS	[CD_DVS_PIV],
	CAST('' AS VARCHAR(10))													AS	[CD_OTO],
	CAST(FORMAT(ABS(0), 'F2') AS VARCHAR(32))								AS	[MT_OTO],
	CAST(FORMAT(ABS(0), 'F2') AS VARCHAR(32))								AS	[MT_CAP_RTT_DVS],
	CAST(FORMAT(ABS(0), 'F2') AS VARCHAR(32))								AS	[MT_CAP_RTT_EUR],
	CAST(IIF(ISNULL(CPT.DWHCPTDAC, '') = '', 'V', 'S') AS VARCHAR(3))		AS	[CD_STT],
	CAST(ISNULL(FORMAT(D.DTE_REA, 'yyyyMMdd'), '99990101') AS VARCHAR(8))	AS	[DT_OUV],
	CAST(
		ISNULL(
			IIF(
				ISNULL(CPT.DWHCPTDAC, '') = '',
				FORMAT(D.DTE_FIN_PRE, 'yyyyMMdd'),
				FORMAT(CPT.DWHCPTDAC, 'yyyyMMdd')
			),
			'') AS VARCHAR(8))												AS	[DT_FER],
	CAST(ISNULL(FORMAT(D.DTE_DER_DEB ,'yyyyMMdd'), '') AS VARCHAR(8))		AS	[DT_DER_DBL_CRD],
	CAST('' AS VARCHAR(30))													AS	[CD_DTT_BQE],
	CAST(ISNULL(FORMAT(D.DTE_DER_ECH, 'yyyyMMdd'), '') AS VARCHAR(8))		AS	[DT_DER_ECH_PY],
	CAST(ISNULL(FORMAT(ISNULL(D.MTT_NOM, 0), 'F2'), '') AS VARCHAR(32))		AS	[MT_PE],
	CAST(ISNULL(LTRIM(RTRIM(D.DUR_LEG)), '') AS VARCHAR(3))					AS	[DR_CON],
	CAST('' AS VARCHAR(35))													AS	[RF_IBAN],
	CAST('' AS CHAR(1))													AS	[CD_BLC_BQE],
	CAST('' AS VARCHAR(32))													AS	[CD_RAI_BLC],
	CAST(ISNULL(FORMAT(D.DTE_MOD ,'yyyyMMdd'), '') AS VARCHAR(8))			AS	[DT_MAJ_CPT],
	CAST('001_GB_' + FORMAT(GETDATE(), 'yyyyMMdd') + '_ACCOUNT_BANK_DAILY.txt' AS VARCHAR(256))	AS	[FileName],
	CAST(FORMAT(GETDATE(), 'yyyyMMdd') AS VARCHAR(8))	AS	[CreatedDate],
	CAST(FORMAT(GETDATE(), 'yyyyMMdd') AS VARCHAR(8))	AS	[LastModifiedDate],
	CAST(FORMAT(GETDATE(), 'yyyyMMdd') AS VARCHAR(8))	AS	[TerminatedDate]
FROM
	dbo.PV_FI_Q_DES					AS	D	WITH(NOLOCK)
	INNER JOIN	dbo.PV_SA_Q_COM		AS  REF	WITH(NOLOCK)
		ON	REF.COMREFREF = D.REF_CPT
	INNER JOIN	dbo.PV_SA_Q_COMPTE	AS	CPT	WITH(NOLOCK)
		ON	CPT.DWHCPTCOM = REF.COMREFCOM
	INNER JOIN	dbo.REF_PRODUIT		AS	PRD	WITH(NOLOCK)
		ON	PRD.code_rubrique_comptable_SAB = CPT.DWHCPTRUB
	INNER JOIN	dbo.PV_SA_Q_CLIENT	AS	CLI	WITH(NOLOCK)
		ON	CLI.DWHCLICLI = CPT.DWHCPTPPAL
	LEFT JOIN	dbo.PV_SA_Q_REF		AS	R	WITH(NOLOCK)
		ON	CLI.DWHCLICLI = R.CLIREFCLI
	LEFT JOIN
	(
		SELECT	MAX(Validity_StartDate) AS	MAX_DATE	FROM	dbo.PV_FI_Q_DES	WITH(NOLOCK)
		--FROM
		--	(
		--		SELECT MAX(CLI.DWHCLIDTX)	AS	MAX_DATE	FROM	dbo.PV_SA_Q_CLIENT AS  CLI WITH(NOLOCK)
		--		UNION ALL
		--		SELECT MAX(CPT.DWHCPTDTX)	AS	MAX_DATE	FROM	dbo.PV_SA_Q_COMPTE AS  CPT WITH(NOLOCK)
		--	)	AS	MAX_DT
	)	AS	MAX_DT
		ON	D.DTE_REA = MAX_DT.MAX_DATE
WHERE
	1 = 1
GO

--WITH    LSTCLI  AS
--(
--    SELECT
--        MAX(CLI.DWHCLIDTX) AS  LSTCLIDTX
--    FROM
--        dbo.PV_SA_Q_CLIENT AS  CLI WITH(NOLOCK)
--),
--LSTCPT  AS
--(
--    SELECT
--        MAX(CPT.DWHCPTDTX) AS  LSTCPTDTX
--    FROM
--        dbo.PV_SA_Q_COMPTE AS  CPT WITH(NOLOCK)
--)
--SELECT DISTINCT
--    -- Date extraction
--    FORMAT(GETDATE(), 'yyyyMMdd')					AS	[DT_EXT_ENR],
--    -- Numéro de compte/prêt/portefeuille
--    ISNULL(LTRIM(RTRIM(CPT.DWHCPTCOM)), '') + '|' +
--    -- Unité organisationnelle
--    'BANQUE'	AS	[Unité organisationnelle],
--    -- Intitulé du compte/prêt/portefeuille
--    ISNULL(LTRIM(RTRIM(CPT.DWHCPTINT)), '') + '|' +
--    -- Code client (SAB collectif ou unique)
--    ISNULL(LTRIM(RTRIM(CLI.DWHCLICLI)), '') + '|' +
--    -- Code client 1 (GRC si client collectif)
--    ISNULL(LTRIM(RTRIM(
--        CASE
--            WHEN        ISNULL(CLI.DWHCLITIE, '') <> ''
--                        AND REFB.CLIREFCOR IN ('SI', 'SF', 'AC')
--                        AND CLI.DWHCLIETA <> 'INDI'
--                THEN    REFB.CLIREFREF
--        END
--    )), '') + '|' +
--    -- Code client 2 (GRC si client collectif)
--    --CASE
--    --WHEN      ISNULL(CLI.DWHCLITIE, '') <> ''
--    --AND  CLI.DWHCLIETA <> 'INDI'
--    --THEN (
--    --SELECT
--    --min(CLIBIS.CLIREFREF)
--    --FROM
--    --TMP_QRYPIL008 Q8  WITH(NOLOCK)
--    --INNER JOIN TMP_QRYPIL008 Q8BIS    WITH(NOLOCK)
--    --ON    Q8BIS.NUM_CPT = Q8.NUM_CPT
--    --INNER JOIN SIDP_DataHub.dbo.PV_SA_Q_CLIENT CLIBIS WITH(NOLOCK)
--    --ON    CLIBIS.COD_CLI = Q8BIS.NUM_CLI
--    --WHERE
--    --1 = 1
--    --AND  CLIBIS.COD_REF IN ('SI','SF','AC')
--    --AND  CLIBIS.TIE_REF IS NULL
--    --AND  Q8BIS.NUM_CLI <> CLIB.COD_CLI
--    --AND  CLI.COD_CLI=Q8.NUM_CLI
--    --)
--    --END +
--    '|' +
--    -- Code client (GRC si client unique)
--    ISNULL(LTRIM(RTRIM(
--        CASE
--            WHEN        ISNULL(CLI.DWHCLITIE, '') = ''
--                        AND REF.CLIREFCOR IN ('SI', 'SF', 'AC')
--                        AND REF.CLIREFREF NOT LIKE 'I%'
--                        AND REF.CLIREFREF NOT LIKE 'T%'
--                THEN    IIF(REF.CLIREFCOR = 'SF', 'SF', '') + REF.CLIREFREF
--        END
--    )), '') + '|' +
--    -- Code type de compte/prêt/portefeuille (code produit)
--    ISNULL(LTRIM(RTRIM(PRD.code_produit_SAB)), '') + '|' +
--    -- Devise
--    ISNULL(LTRIM(RTRIM(DES.COD_DEV)), '') + '|' +
--    -- Devise pivot
--    '|' +
--    -- Code autorisation
--    '|' +
--    -- Montant autorisation
--    '|' +
--    -- Montant capital restant dû (devise) = solde compte mirroir en devise
--    FORMAT(ABS(ISNULL(CPT.DWHCPTCPC, 0)), 'F2') + '|' +
--    -- Montant capital restant dû (euro) = solde compte mirroir en contrevaleur euro
--    FORMAT(ABS(ISNULL(CPT.DWHCPTCPT, 0)), 'F2') + '|' +
--    -- Statut
--    CASE
--        WHEN        ISNULL(CPT.DWHCPTDAC, '') = ''
--            THEN    'V'
--        ELSE        'S'
--    END +
--    '|'+
--    -- Date ouverture
--    ISNULL(FORMAT(DES.DTE_REA, 'yyyyMMdd'), '99990101') + '|' +
--    -- Date clôture
--    ISNULL(
--        CASE
--            WHEN        ISNULL(CPT.DWHCPTDAC, '') = ''
--                THEN    FORMAT(DES.DTE_FIN_PRE, 'yyyyMMdd')
--            ELSE        FORMAT(CPT.DWHCPTDAC, 'yyyyMMdd')
--        END,
--        '') +
--    '|' +
--    -- Date de dernier déblocage crédit
--    ISNULL(FORMAT(DES.DTE_DER_DEB ,'yyyyMMdd'), '') +
--    '|' +
--    -- Canal de détection Banque
--    --OPP.LIB_CAN_ORI +
--    '|' +
--    -- Date de dernière échéance payée
--    ISNULL(FORMAT(DES.DTE_DER_ECH, 'yyyyMMdd'), '') +
--    '|' +
--    -- Montant du prêt
--    FORMAT(ISNULL(DES.MTT_NOM, 0), 'F2') + '|' +
--    -- Durée du contrat
--    ISNULL(LTRIM(RTRIM(DES.DUR_LEG)), '') + '|' +
--    -- IBAN
--    '|' +
--    -- Code blocage compte
--    '|' +
--    -- Raison blocage
--    '|' +
--    -- Date de mise à jour du compte/prêt/portefeuille
--    ISNULL(FORMAT(DES.DTE_MOD ,'yyyyMMdd'), '') + '|'    AS EXTRACT
--FROM
--    dbo.PV_FI_Q_DES                        AS  DES     WITH(NOLOCK)
--    INNER JOIN  dbo.PV_SA_Q_COM            AS  R_CPT   WITH(NOLOCK)
--        ON  R_CPT.COMREFREF = DES.REF_CPT
--    INNER JOIN  dbo.PV_SA_Q_COMPTE         AS  CPT     WITH(NOLOCK)
--        ON  CPT.DWHCPTCOM = R_CPT.COMREFCOM
--    INNER JOIN  dbo.REF_PRODUIT    AS  PRD     WITH(NOLOCK)
--        ON  PRD.code_rubrique_comptable_SAB = CPT.DWHCPTRUB
--    INNER JOIN  dbo.PV_SA_Q_CLIENT         AS  CLI     WITH(NOLOCK)
--        ON  CLI.DWHCLICLI = CPT.DWHCPTPPAL
--    INNER JOIN  dbo.PV_SA_Q_CLIENT         AS  CLIB    WITH(NOLOCK)
--        ON  CLIB.DWHCLICLI = IIF(ISNULL(CLI.DWHCLITIE, '') <> '', CLI.DWHCLITIE, CLI.DWHCLICLI)
--    LEFT JOIN   dbo.PV_SA_Q_REF    AS  REF     WITH(NOLOCK)
--        ON  CLI.DWHCLICLI = REF.CLIREFCLI
--    LEFT JOIN   dbo.PV_SA_Q_REF    AS  REFB    WITH(NOLOCK)
--        ON  CLIB.DWHCLICLI = REFB.CLIREFCLI
--    --LEFT JOIN      IGP_IND_OPP              OPP   WITH(NOLOCK)
--    --ON   OPP.NUM_DOS_FF = DES.REF_CPT
--    LEFT JOIN   dbo.PV_SF_ACCOUNT  AS  PER WITH(NOLOCK)
--        ON  PER.IDCustomer__pc = REF.CLIREFREF
--    LEFT JOIN   LSTCLI
--        ON  DATEDIFF(DAY, LSTCLI.LSTCLIDTX, PER.OBTerminationDate__c) = 0
--    LEFT JOIN   LSTCPT
--        ON  DATEDIFF(DAY, LSTCPT.LSTCPTDTX, CPT.DWHCPTDAO) = 0
--            OR  DATEDIFF(DAY, LSTCPT.LSTCPTDTX, CPT.DWHCPTDTX) = 0
--            OR  DATEDIFF(DAY, LSTCPT.LSTCPTDTX, CPT.DWHCPTDAC) = 0
--WHERE
--    1 = 1
--    AND ISNULL(LSTCPT.LSTCPTDTX, LSTCPT.LSTCPTDTX) IS NOT NULL
--    AND CLI.DWHCLIAGE NOT IN ('51', '52')
--    AND CLI.DWHCLIRES   NOT IN
--    (
--        'C01','C02','C03',
--        'D01','D05',
--        'T01','T02','T03',
--        '650','651','652','653','654','655','656','657','658','659',
--        '660','661','662','663','664','665','666','667','668','669',
--        '670','671','672','673','674','675','676'
--    )