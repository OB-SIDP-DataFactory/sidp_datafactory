﻿CREATE VIEW [dbo].[VW_LAB003_CPT]
AS
SELECT
	CAST(FORMAT(GETDATE(), 'yyyyMMdd') AS VARCHAR(8)) AS [DT_EXT_ENR],
	CAST(ISNULL(LTRIM(RTRIM(CPT.DWHCPTCOM)), '') AS VARCHAR(32))	AS	[ID_CPT],
	CAST('BANQUE' AS VARCHAR(6))	AS	[IC_UNI_ORG],
	CAST(ISNULL(LTRIM(RTRIM(CPT.DWHCPTINT)), '') AS VARCHAR(50))	AS	[LB_CPT],
	CAST(ISNULL(LTRIM(RTRIM(CPT.DWHCPTPPAL)), '') AS VARCHAR(7))	AS	[ID_SAB_CLI],
	CAST('' AS VARCHAR(100))	AS	[ID_GRC_CLI_JOI_1],
	CAST('' AS VARCHAR(100))	AS	[ID_GRC_CLI_JOI_2],
	CAST('' AS VARCHAR(100))	AS	[ID_GRC_CLI],
	CAST('' AS VARCHAR(4))	AS	[CD_TYP_CPT],
	CAST(ISNULL(LTRIM(RTRIM(CPT.DWHCPTDEV)), '') AS VARCHAR(3))	AS	[CD_DVS_CPT],
	CAST('EUR' AS VARCHAR(3))	AS	[CD_DVS_PIV],
	CAST('' AS VARCHAR(10))	AS	[CD_OTO],
	CAST(FORMAT(ABS(ISNULL(CPT.DWHCPTMTA, 0)), 'F2') AS VARCHAR(32))						AS	[MT_OTO],
	CAST(FORMAT(ABS(0), 'F2') AS VARCHAR(32))						AS	[MT_CAP_RTT_DVS],
	CAST(FORMAT(ABS(0), 'F2') AS VARCHAR(32))						AS	[MT_CAP_RTT_EUR],
	CAST('' AS VARCHAR(3))	AS	[CD_STT],
	CAST(ISNULL(FORMAT(CPT.DWHCPTDAO,'yyyyMMdd'), '') AS VARCHAR(8))	AS	[DT_OUV],
	CAST(ISNULL(FORMAT(CPT.DWHCPTDAC,'yyyyMMdd'), '') AS VARCHAR(8))	AS	[DT_FER],
	CAST('' AS VARCHAR(8))	AS	[DT_DER_DBL_CRD],
	CAST('' AS VARCHAR(30))	AS	[CD_DTT_BQE],
	CAST('' AS VARCHAR(8))	AS	[DT_DER_ECH_PY],
	CAST(FORMAT(ABS(0), 'F2') AS VARCHAR(32))						AS	[MT_PE],
	CAST('' AS VARCHAR(3))	AS	[DR_CON],
	CAST('' AS VARCHAR(35))	AS	[RF_IBAN],
	CAST('' AS CHAR(1))	AS	[CD_BLC_BQE],
	CAST('' AS VARCHAR(32))	AS	[CD_RAI_BLC],
	CAST(ISNULL(LTRIM(RTRIM(FORMAT(CPT.DWHCPTDTX ,'yyyyMMdd'))), '') AS VARCHAR(8))	AS	[DT_MAJ_CPT],
	CAST('001_GB_' + FORMAT(GETDATE(), 'yyyyMMdd') + '_ACCOUNT_BANK_DAILY.txt' AS VARCHAR(256))	AS	[FileName],
	CAST(FORMAT(GETDATE(), 'yyyyMMdd') AS VARCHAR(8))	AS	[CreatedDate],
	CAST(FORMAT(GETDATE(), 'yyyyMMdd') AS VARCHAR(8))	AS	[LastModifiedDate],
	CAST(FORMAT(GETDATE(), 'yyyyMMdd') AS VARCHAR(8))	AS	[TerminatedDate]
FROM
	dbo.PV_SA_Q_COMPTE	AS	CPT	WITH(NOLOCK)
GO



--WITH    LSTCLI  AS
--(
--    SELECT
--        MAX(CLI.DWHCLIDTX) AS  LSTCLIDTX
--    FROM
--        dbo.PV_SA_Q_CLIENT AS  CLI WITH(NOLOCK)
--),
--LSTCPT  AS
--(
--    SELECT
--        MAX(CPT.DWHCPTDTX) AS  LSTCPTDTX
--    FROM
--        dbo.PV_SA_Q_COMPTE AS  CPT WITH(NOLOCK)
--)
--SELECT DISTINCT
--    -- Date extraction
--    FORMAT(GETDATE(), 'yyyyMMdd') AS	[DT_EXT_ENR],
--    -- Numéro de compte
--    ISNULL(LTRIM(RTRIM(CPT.DWHCPTCOM)), '') + '|' +
--    -- Unité organisationnelle
--    'BANQUE'	AS	[Unité organisationnelle],
--    -- Intitulé du compte/prêt/portefeuille
--    ISNULL(LTRIM(RTRIM(CPT.DWHCPTINT)), '') + '|' +
--    -- Code client (SAB collectif ou unique)
--    ISNULL(LTRIM(RTRIM(CPT.DWHCPTPPAL)), '') + '|' +
--    -- Code client 1 (GRC si client collectif)
--    ISNULL(LTRIM(RTRIM(
--        CASE
--            WHEN        ISNULL(CLI.DWHCLITIE, '') <> ''
--                        AND REFB.CLIREFCOR IN ('SI', 'SF', 'AC')
--                        AND CLI.DWHCLIETA <> 'INDI'
--                THEN    REFB.CLIREFREF
--        END
--    )), '') + '|' +
--    -- Code client 2 (GRC si client collectif)
--    --CASE
--    --  WHEN        ISNULL(CLI.DWHCLITIE, '') <> ''
--    --              AND  CLI.DWHCLIETA <> 'INDI'
--    --      THEN    (
--    --                  SELECT
--    --                      MIN(REFBIS.CLIREFREF)
--    --                  FROM
--    --                      TMP_QRYPIL008            Q8 WITH(NOLOCK)
--    --                      INNER JOIN TMP_QRYPIL008 Q8BIS  WITH(NOLOCK)
--    --                          ON  Q8BIS.NUM_CPT = Q8.NUM_CPT
--    --                      INNER JOIN SIDP_DataHub.dbo.PV_SA_Q_CLIENT  AS  CLIBIS  WITH(NOLOCK)
--    --                          ON  CLIBIS.COD_CLI = Q8BIS.NUM_CLI
--    --                  WHERE
--    --                      1 = 1
--    --                      AND REFBIS.CLIREFCOR IN ('SI', 'SF', 'AC')
--    --                      AND ISNULL(CLIBIS.DWHCLITIE, '') <> ''
--    --                      AND Q8BIS.NUM_CLI <> CLIB.COD_CLI
--    --                      AND  CLI.COD_CLI = Q8.NUM_CLI
--    --              )
--    --END +
--    '|' +
--    -- Code client (GRC si client unique)
--    ISNULL(LTRIM(RTRIM(
--        CASE
--            WHEN        ISNULL(CLI.DWHCLITIE, '') = ''
--                        AND REF.CLIREFCOR IN ('SI', 'SF', 'AC')
--                        AND REF.CLIREFREF NOT LIKE 'I%'
--                        AND REF.CLIREFREF NOT LIKE 'T%'
--                THEN    IIF(REF.CLIREFCOR = 'SF', 'SF', '') + REF.CLIREFREF
--        END
--    )), '') + '|' +
--    -- Code type de compte (code produit)
--    ISNULL(LTRIM(RTRIM(IIF(PRD.code_produit_SAB IN ('CV', 'CV+'), 'CW4', PRD.code_produit_SAB))), '') + '|' +
--    -- Devise
--    ISNULL(LTRIM(RTRIM(CPT.DWHCPTDEV)), '') + '|' +
--    -- Devise pivot
--    'EUR' + '|' +
--    -- Code autorisation
--    '|' +
--    -- Montant autorisation
--    LTRIM(RTRIM(FORMAT(ISNULL(CPT.DWHCPTMTA, 0), 'F2'))) + '|' +
--    -- Montant capital restant dû (devise)
--    '|' +
--    -- Montant capital restant dû (euro)
--    '|' +
--    -- Statut
--    --CASE
--    --  WHEN        CPT.COD_MOT_BLO = 4
--    --      THEN    'S'
--    --  ELSE        'V'
--    --END +
--    '|' +
--    -- Date ouverture
--    ISNULL(LTRIM(RTRIM(FORMAT(CPT.DWHCPTDAO,'yyyyMMdd'))), '') + '|' +
--    -- Date clôture
--    ISNULL(LTRIM(RTRIM(FORMAT(CPT.DWHCPTDAC,'yyyyMMdd'))), '') + '|' +
--    -- Date de dernier déblocage crédit
--    '|' +
--    -- Canal de détection Banque
--    '|' +
--    -- Date de dernière échéance payée
--    '|' +
--    -- Montant du prêt
--    '|' +
--    -- Durée de contrat
--    '|' +
--    -- IBAN
--    '|' +
--    -- Code blocage compte
--    --CPT.COD_MOT_BLO +
--    '|' +
--    -- Raison blocage
--    --CPT.MOT_BLO +
--    '|' +
--    -- Date de mise à jour du compte
--    ISNULL(LTRIM(RTRIM(FORMAT(CPT.DWHCPTDTX ,'yyyyMMdd'))), '') + '|'    AS EXTRACT
--FROM
--    dbo.PV_SA_Q_COMPTE                     AS  CPT     WITH(NOLOCK)
--    LEFT JOIN   dbo.PV_SA_Q_COM            AS  R_CPT   WITH(NOLOCK)
--        ON  R_CPT.COMREFCOM = CPT.DWHCPTCOM
--    INNER JOIN  dbo.REF_PRODUIT    AS  PRD     WITH(NOLOCK)
--        ON  PRD.code_rubrique_comptable_SAB = CPT.DWHCPTRUB
--            AND IIF(PRD.code_produit_SAB IN ('CV', 'CV+'), 'CW4', PRD.code_produit_SAB) IN
--            ('CCO','CEL','CEP','CLJ','CL0','CL2','CL3','CL4','COD','COP','CPR','CPS','CPX','CP1','CP2','CP8','CP9','CRE','CTP',
--            'CTT','CT1','CT2','CVA','CVB','CVJ','CV0','CV1','CV2','CV3','CV4','CV6','CV8','CV9','CW4','CW8','C0_','C1_','C_2',
--            'C2_','C_3','C3_','C_4','C4_','C_5','C_6','C_7','D00','D09','D10','D11','D12','D_2','D_3','D3_','D4_','E_A','EA0',
--            'EA1','EA2','EA3','EA4','EA5','EA6','EA7','EA8','EA9','E_B','EB0','EB1','EB2','EB3','EB4','EB5','EB6','EB7','EB8',
--            'EB9','E_C','EC0','EC1','EC2','EC3','EC4','EC5','EC6','EC7','EC8','EC9','E_D','ED0','ED1','ED2','ED3','ED4','ED5',
--            'ED6','ED7','ED8','ED9','E_E','EE0','EE1','EE2','EE3','EE4','EE5','EE6','EE7','EE8','EE9','E_F','EF0','EF1','E_G',
--            'E_H','E_I','E_J','E_K','E_L','E_M','E_N','E_O','E_P','E_Q','E_R','ERS','ER2','E_S','E_T','ET3','ET4','E_U','E_V',
--            'E_W','E_X','E_Y','E_Z','E_0','E12','E_3','E_4','E_5','E_6','E_7','E_8','E_9','LAA','LEP','LGA','LVA','LVJ','LVP',
--            'LW4','LW8','PEA','PEL','PE2','TP1')
--    INNER JOIN  dbo.PV_SA_Q_CLIENT AS  CLI     WITH(NOLOCK)
--        ON  CLI.DWHCLICLI = CPT.DWHCPTPPAL
--    INNER JOIN  dbo.PV_SA_Q_CLIENT AS  CLIB    WITH(NOLOCK)
--        ON  CLIB.DWHCLICLI = IIF(ISNULL(CLI.DWHCLITIE,'') <> '', CLI.DWHCLITIE, CLI.DWHCLICLI)
--    LEFT JOIN   dbo.PV_SA_Q_REF    AS  REF     WITH(NOLOCK)
--        ON  CLI.DWHCLICLI = REF.CLIREFCLI
--    LEFT JOIN   dbo.PV_SA_Q_REF    AS  REFB    WITH(NOLOCK)
--        ON  CLIB.DWHCLICLI = REFB.CLIREFCLI
--    LEFT JOIN   dbo.PV_SF_ACCOUNT  AS  PER WITH(NOLOCK)
--        ON  PER.IDCustomer__pc = REF.CLIREFREF
--    LEFT JOIN   LSTCLI
--        ON  DATEDIFF(DAY, LSTCLI.LSTCLIDTX, PER.OBTerminationDate__c) = 0
--    LEFT JOIN   LSTCPT
--        ON  DATEDIFF(DAY, LSTCPT.LSTCPTDTX, CPT.DWHCPTDAO) = 0
--            OR  DATEDIFF(DAY, LSTCPT.LSTCPTDTX, CPT.DWHCPTDTX) = 0
--            OR  DATEDIFF(DAY, LSTCPT.LSTCPTDTX, CPT.DWHCPTDAC) = 0
--WHERE
--    1 = 1
--    AND ISNULL(LSTCPT.LSTCPTDTX, LSTCPT.LSTCPTDTX) IS NOT NULL
--    AND CLI.DWHCLIAGE NOT IN ('51', '52')
--    AND CLI.DWHCLIRES NOT IN
--    (
--        'C01','C02','C03',
--        'D01','D05',
--        'T01','T02','T03',
--        '650','651','652','653','654','655','656','657','658','659',
--        '660','661','662','663','664','665','666','667','668','669',
--        '670','671','672','673','674','675','676'
--    )
--    AND R_CPT.COMREFCOM IS NULL