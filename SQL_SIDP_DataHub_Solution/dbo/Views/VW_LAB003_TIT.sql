﻿CREATE VIEW [dbo].[VW_LAB003_TIT]
AS
SELECT
	CAST(FORMAT(GETDATE(), 'yyyyMMdd') AS VARCHAR(8)) AS [DT_EXT_ENR],
	CAST('' AS VARCHAR(32))	AS	[ID_CPT],
	CAST('BANQUE' AS VARCHAR(6))	AS	[IC_UNI_ORG],
	CAST('' AS VARCHAR(50))	AS	[LB_CPT],
	CAST('' AS VARCHAR(7))	AS	[ID_SAB_CLI],
	CAST('' AS VARCHAR(100))	AS	[ID_GRC_CLI_JOI_1],
	CAST('' AS VARCHAR(100))	AS	[ID_GRC_CLI_JOI_2],
	CAST('' AS VARCHAR(100))	AS	[ID_GRC_CLI],
	CAST('' AS VARCHAR(4))	AS	[CD_TYP_CPT],
	CAST('' AS VARCHAR(3))	AS	[CD_DVS_CPT],
	CAST('' AS VARCHAR(3))	AS	[CD_DVS_PIV],
	CAST('' AS VARCHAR(10))	AS	[CD_OTO],
	CAST(FORMAT(ABS(0), 'F2') AS VARCHAR(32))						AS	[MT_OTO],
	CAST(FORMAT(ABS(0), 'F2') AS VARCHAR(32))						AS	[MT_CAP_RTT_DVS],
	CAST(FORMAT(ABS(0), 'F2') AS VARCHAR(32))						AS	[MT_CAP_RTT_EUR],
	CAST('' AS VARCHAR(3))	AS	[CD_STT],
	CAST('' AS VARCHAR(8))	AS	[DT_OUV],
	CAST('' AS VARCHAR(8))	AS	[DT_FER],
	CAST('' AS VARCHAR(8))	AS	[DT_DER_DBL_CRD],
	CAST('' AS VARCHAR(30))	AS	[CD_DTT_BQE],
	CAST('' AS VARCHAR(8))	AS	[DT_DER_ECH_PY],
	CAST(FORMAT(ABS(0), 'F2') AS VARCHAR(32))						AS	[MT_PE],
	CAST('' AS VARCHAR(3))	AS	[DR_CON],
	CAST('' AS VARCHAR(35))	AS	[RF_IBAN],
	CAST('' AS CHAR(1))	AS	[CD_BLC_BQE],
	CAST('' AS VARCHAR(32))	AS	[CD_RAI_BLC],
	CAST('' AS VARCHAR(8))	AS	[DT_MAJ_CPT],
	CAST('001_GB_' + FORMAT(GETDATE(), 'yyyyMMdd') + '_ACCOUNT_BANK_DAILY.txt' AS VARCHAR(256))	AS	[FileName],
	CAST(FORMAT(GETDATE(), 'yyyyMMdd') AS VARCHAR(8))	AS	[CreatedDate],
	CAST(FORMAT(GETDATE(), 'yyyyMMdd') AS VARCHAR(8))	AS	[LastModifiedDate],
	CAST(FORMAT(GETDATE(), 'yyyyMMdd') AS VARCHAR(8))	AS	[TerminatedDate]
GO


--SELECT DISTINCT
--    -- Date extraction
--    FORMAT(GETDATE(), 'yyyyMMdd')					AS	[DT_EXT_ENR],
--    -- Numéro de compte/prêt/portefeuille (cpt miroir)
--    --SG.NUM_CPT_TIT +
--    '|' +
--    -- Unité organisationnelle
--    'BANQUE'	AS	[Unité organisationnelle],
--    -- Intitulé du compte/prêt/portefeuille
--    --SG.NOM_CLI +
--    '|' +
--    -- Code client (SAB collectif ou unique)
--    --CLI.COD_CLI +
--    '|' +
--    -- Code client 1 (GRC si client collectif)
--    '|' +
--    -- Code client 2 (GRC si client collectif)
--    '|' +
--    -- Code client (GRC si client unique)
--    --CASE
--    --WHEN      ISNULL(CLI.DWHCLITIE, '') = ''
--    --AND  CLI.COD_REF IN ('SI','SF','AC')
--    --AND  CLI.CLIREFREF NOT LIKE 'I%'
--    --AND  CLI.CLIREFREF NOT LIKE 'T%'
--    --THEN decode(CLI.COD_REF, 'SF', 'SF') + CLI.CLIREFREF
--    --END +
--    '|' +
--    -- Code type de compte/prêt/portefeuille (code produit)
--    'TITG' + '|' +
--    -- Devise
--    --CPT.DEV_CPT +
--    '|' +
--    -- Devise pivot
--    'EUR' + '|' +
--    -- Code autorisation
--    '|' +
--    -- Montant autorisation
--    '|' +
--    -- Montant capital restant dû (devise)
--    '|' +
--    -- Montant capital restant dû (euro)
--    --CASE
--    --WHEN      CPT.DEV_CPT = 'EUR'
--    --THEN FORMAT(SG.VAL_GLO_TIT, 'F2')
--    --END +
--    '|' +
--    -- Statut
--    --CASE
--    --WHEN      SG. ETA_CPT = 'F'
--    --THEN 'S'
--    --ELSE      'V'
--    --END +
--    '|' +
--    -- Date ouverture
--    --FORMAT(SG.DAT_OUV_CPT_TIT, 'yyyyMMdd') +
--    '|' +
--    -- Date clôture
--    --FORMAT(SG.DAT_FER_CPT_TIT, 'yyyyMMdd') +
--    '|' +
--    -- Date de dernier déblocage crédit
--    '|' +
--    -- Canal de détection Banque
--    '|' +
--    -- Date de dernière échéance payée
--    '|' +
--    -- Montant du prêt
--    '|' +
--    -- Durée du contrat
--    '|' +
--    -- IBAN
--    '|' +
--    -- Code blocage
--    '|' +
--    -- Raison de blocage
--    '|' +
--    -- Date de mise à jour du compte/prêt/portefeuille
--    --FORMAT(SG.DAT_MAJ_CPT_TIT, 'yyyyMMdd') +
--    '|'    AS EXTRACT
----FROM
--    --IGP_TIT_SRE01                           SG    WITH(NOLOCK)
--    --INNER JOIN     SIDP_DataHub.dbo.PV_SA_Q_COMPTE            CPT WITH(NOLOCK)
--    --ON   CPT.DWHCPTCOM = SG.NUM_CPT_ESP
--    --INNER JOIN     SIDP_DataHub.dbo.PV_SA_Q_CLIENT            CLI WITH(NOLOCK)
--    --ON   CLI.COD_CLI = CPT.DWHCPTPPAL
----WHERE
--    --1 = 1
--    --AND CLI.DWHCLIAGE NOT IN ('51', '52')
--    --AND CLI.DWHCLIRES   NOT IN
--    --(
--    --'C01','C02','C03',
--    --'D01','D05',
--    --'T01','T02','T03',
--    --'650','651','652','653','654','655','656','657','658','659',
--    --'660','661','662','663','664','665','666','667','668','669',
--    --'670','671','672','673','674','675','676'
--    --)
--    --AND  CLI.DTE_FIN_REL IS NULL;