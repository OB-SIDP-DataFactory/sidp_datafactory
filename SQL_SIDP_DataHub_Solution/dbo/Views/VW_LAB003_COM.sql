﻿CREATE VIEW [dbo].[VW_LAB003_COM]
AS
SELECT
	CAST(FORMAT(GETDATE(), 'yyyyMMdd') AS VARCHAR(8))				AS [DT_EXT_ENR],
	CAST(ISNULL(LTRIM(RTRIM(REF.COMREFCOM)), '') AS VARCHAR(32))	AS	[ID_CPT],
	CAST('BANQUE' AS VARCHAR(6))									AS	[IC_UNI_ORG],
	CAST(ISNULL(LTRIM(RTRIM(C.LIB_CPT)), '') AS VARCHAR(50))		AS	[LB_CPT],
	CAST('' AS VARCHAR(7))											AS	[ID_SAB_CLI],
	CAST('' AS VARCHAR(100))										AS	[ID_GRC_CLI_JOI_1],
	CAST('' AS VARCHAR(100))										AS	[ID_GRC_CLI_JOI_2],
	CAST('' AS VARCHAR(100))										AS	[ID_GRC_CLI],
	CAST('' AS VARCHAR(4))											AS	[CD_TYP_CPT],
	CAST('' AS VARCHAR(3))											AS	[CD_DVS_CPT],
	CAST('' AS VARCHAR(3))											AS	[CD_DVS_PIV],
	CAST('' AS VARCHAR(10))											AS	[CD_OTO],
	CAST(FORMAT(ABS(0), 'F2') AS VARCHAR(32))						AS	[MT_OTO],
	CAST(FORMAT(ABS(0), 'F2') AS VARCHAR(32))						AS	[MT_CAP_RTT_DVS],
	CAST(FORMAT(ABS(0), 'F2') AS VARCHAR(32))						AS	[MT_CAP_RTT_EUR],
	CAST(IIF(C.COD_SIT_DOS = '00', 'V', 'S') AS VARCHAR(3))			AS	[CD_STT],
	CAST(ISNULL(FORMAT(C.DTE_OUV,'yyyyMMdd'), '') AS VARCHAR(8))	AS	[DT_OUV],
	CAST(ISNULL(CASE WHEN C.COD_SIT_DOS = '57' THEN FORMAT(C.DTE_DER_MOD_CACH, 'yyyyMMdd') END, '') AS VARCHAR(8))	AS	[DT_FER],
	CAST(ISNULL((SELECT FORMAT(MAX(UP.DTE_SAI_UP), 'yyyyMMdd') FROM dbo.PV_FI_Q_COMPL_UP UP WITH(NOLOCK) WHERE 1 = 1 AND UP.NUM_PRE = C.NUM_PRE), '') AS VARCHAR(8))	AS	[DT_DER_DBL_CRD],
	CAST('' AS VARCHAR(30))	AS	[CD_DTT_BQE],
	CAST(ISNULL(FORMAT(C.DTE_LST_ECH,'yyyyMMdd'), '') AS VARCHAR(8))	AS	[DT_DER_ECH_PY],
	CAST(FORMAT(ISNULL(C.MTT_TOT_AUT, 0),'F2') AS VARCHAR(32))		AS	[MT_PE],
	CAST('' AS VARCHAR(3))	AS	[DR_CON],
	CAST('' AS VARCHAR(35))	AS	[RF_IBAN],
	CAST('' AS CHAR(1))	AS	[CD_BLC_BQE],
	CAST('' AS VARCHAR(32))	AS	[CD_RAI_BLC],
	CAST(ISNULL(FORMAT(C.DTE_DER_MOD_CACH,'yyyyMMdd'), '') AS VARCHAR(8))	AS	[DT_MAJ_CPT],
	CAST('001_GB_' + FORMAT(GETDATE(), 'yyyyMMdd') + '_ACCOUNT_BANK_DAILY.txt' AS VARCHAR(256))	AS	[FileName],
	CAST(FORMAT(GETDATE(), 'yyyyMMdd') AS VARCHAR(8))	AS	[CreatedDate],
	CAST(FORMAT(GETDATE(), 'yyyyMMdd') AS VARCHAR(8))	AS	[LastModifiedDate],
	CAST(FORMAT(GETDATE(), 'yyyyMMdd') AS VARCHAR(8))	AS	[TerminatedDate]
FROM
	dbo.PV_FI_Q_COMPL				AS	C	WITH(NOLOCK)
	INNER JOIN	dbo.PV_SA_Q_COM		AS  REF	WITH(NOLOCK)
		ON	REF.COMREFREF = C.NUM_PRE
	INNER JOIN	dbo.PV_SA_Q_COMPTE	AS	CPT	WITH(NOLOCK)
		ON  CPT.DWHCPTCOM = REF.COMREFCOM
	INNER JOIN	dbo.REF_PRODUIT		AS	PRD	WITH(NOLOCK)
		ON  PRD.code_rubrique_comptable_SAB = CPT.DWHCPTRUB
	INNER JOIN	dbo.PV_SA_Q_CLIENT	AS	CLI	WITH(NOLOCK)
		ON  CLI.DWHCLICLI = CPT.DWHCPTPPAL
	LEFT JOIN	dbo.PV_SA_Q_REF		AS	R	WITH(NOLOCK)
		ON  CLI.DWHCLICLI = R.CLIREFCLI
WHERE
	1 = 1
GO


--SELECT DISTINCT
--    --Date extraction
--    FORMAT(GETDATE(), 'yyyyMMdd')					AS	[DT_EXT_ENR],
--    -- Numéro de compte/prêt/portefeuille
--    ISNULL(LTRIM(RTRIM(CPT.DWHCPTCOM)), '') + '|' +
--    -- Unité organisationnelle
--    'BANQUE'	AS	[Unité organisationnelle],
--    -- Intitulé du compte/prêt/portefeuille
--    ISNULL(LTRIM(RTRIM(COM.LIB_CPT)), '') + '|' +
--    -- Code client (SAB collectif ou unique)
--    ISNULL(LTRIM(RTRIM(CPT.DWHCPTPPAL)), '') + '|' +
--    -- Code client (SAB si client unique)
--    ISNULL(LTRIM(RTRIM(
--        CASE
--            WHEN        COM.IDE_GRC_COE IS NOT NULL
--                THEN    COM.IDE_GRC + '|' + -- Code client 1 (GRC si client collectif)
--                        COM.IDE_GRC_COE + '|' -- Code client 2 (GRC si client collectif)
--            ElSE        '||' + COM.IDE_GRC
--        END
--    )), '') + '|' +
--    -- Code type de compte/prêt/portefeuille (code produit)
--    ISNULL(LTRIM(RTRIM(PRD.code_produit_SAB)), '') + '|' +
--    -- Devise
--    'EUR' + '|' +
--    -- Devise pivot
--    '|' +
--    -- Code autorisation
--    '|' +
--    -- Montant autorisation
--    '|' +
--    -- Montant capital restant dû (devise) = solde compte mirroir en devise
--    FORMAT(ABS(ISNULL(CPT.DWHCPTCPC, 0)), 'F2') + '|' +
--    -- Montant capital restant dû (euro) = solde compte mirroir en contrevaleur euro
--    FORMAT(ABS(ISNULL(CPT.DWHCPTCPT, 0)), 'F2') + '|' +
--    -- Statut
--    CASE
--        WHEN        COM.COD_SIT_DOS = '00'
--            THEN    'V'
--        ELSE        'S'
--    END + '|' +
--    -- Date ouverture
--    ISNULL(FORMAT(COM.DTE_OUV,'yyyyMMdd'), '') + '|' +
--    -- Date clôture
--    ISNULL(
--        CASE
--            WHEN        COM.COD_SIT_DOS = '57'
--                THEN    FORMAT(COM.DTE_DER_MOD_CACH, 'yyyyMMdd')
--        END,
--        '') + '|' +
--    -- Date de dernier déblocage crédit
--    ISNULL(
--        (
--            SELECT
--                FORMAT(MAX(UP.DTE_SAI_UP), 'yyyyMMdd')
--            FROM
--                dbo.PV_FI_Q_COMPL_UP UP
--            WHERE
--                1 = 1
--                AND UP.NUM_PRE = COM.NUM_PRE
--        ),
--        '') + '|' +
--    -- Canal de détection Banque
--    --OPP.LIB_CAN_ORI +
--    '|' +
--    -- Date de dernière échéance payée
--    ISNULL(FORMAT(COM.DTE_LST_ECH,'yyyyMMdd'), '') + '|' +
--    -- Montant du prêt
--    FORMAT(ISNULL(COM.MTT_TOT_AUT, 0),'F2') + '|' +
--    -- Durée du contrat
--    '|' +
--    -- IBAN
--    '|' +
--    -- Code blocage compte
--    '|' +
--    -- Raison blocage
--    '|' +
--    -- Date de mise à jour du compte/prêt/portefeuille
--    ISNULL(FORMAT(COM.DTE_DER_MOD_CACH,'yyyyMMdd'), '') + '|'    AS EXTRACT
--FROM
--    dbo.PV_FI_Q_COMPL                      AS  COM     WITH(NOLOCK)
--    INNER JOIN  dbo.PV_SA_Q_COM            AS  R_CPT   WITH(NOLOCK)
--        ON  R_CPT.COMREFREF = COM.NUM_PRE
--    INNER JOIN  dbo.PV_SA_Q_COMPTE         AS  CPT     WITH(NOLOCK)
--        ON  CPT.DWHCPTCOM = R_CPT.COMREFCOM
--    INNER JOIN  dbo.REF_PRODUIT    AS  PRD     WITH(NOLOCK)
--        ON  PRD.code_rubrique_comptable_SAB = CPT.DWHCPTRUB
--    INNER JOIN dbo.PV_SA_Q_CLIENT          AS  CLI     WITH(NOLOCK)
--        ON  CLI.DWHCLICLI = CPT.DWHCPTPPAL
--    --LEFT JOIN IGP_IND_OPP OPP WITH(NOLOCK)
--    --ON   OPP.NUM_DOS_FF = COM.NUM_PRE
--    LEFT JOIN   dbo.PV_SA_Q_REF    AS  REF WITH(NOLOCK)
--        ON  CLI.DWHCLICLI = REF.CLIREFCLI
--    LEFT JOIN   dbo.PV_SF_ACCOUNT  AS  PER WITH(NOLOCK)
--        ON  PER.IDCustomer__pc = REF.CLIREFREF
--            AND PER.IDCustomerSAB__pc = CLI.DWHCLICLI
--    LEFT JOIN   LSTCLI
--        ON  DATEDIFF(DAY, LSTCLI.LSTCLIDTX, PER.OBTerminationDate__c) = 0
--    LEFT JOIN   LSTCPT
--        ON  DATEDIFF(DAY, LSTCPT.LSTCPTDTX, CPT.DWHCPTDAO) = 0
--            OR  DATEDIFF(DAY, LSTCPT.LSTCPTDTX, CPT.DWHCPTDTX) = 0
--            OR  DATEDIFF(DAY, LSTCPT.LSTCPTDTX, CPT.DWHCPTDAC) = 0
--WHERE
--    1 = 1
--    AND ISNULL(LSTCPT.LSTCPTDTX, LSTCPT.LSTCPTDTX) IS NOT NULL
--    AND CLI.DWHCLIAGE NOT IN ('51', '52')
--    AND CLI.DWHCLIRES   NOT IN
--    (
--        'C01','C02','C03',
--        'D01','D05',
--        'T01','T02','T03',
--        '650','651','652','653','654','655','656','657','658','659',
--        '660','661','662','663','664','665','666','667','668','669',
--        '670','671','672','673','674','675','676'
--    )
