﻿


CREATE VIEW [dbo].[VW_PV_SF_OPPORTUNITY_HISTORY] AS
select sf_opportunity_history.Id
		,sf_opportunity_history.Id_SF
		,sf_opportunity_history.AccountId
		,sf_opportunity_history.AdvisorCode__c
		,sf_opportunity_history.CampaignId__c
		,sf_opportunity_history.CloseDate
		,sf_opportunity_history.CommercialOfferCode__c
		,sf_opportunity_history.CommercialOfferName__c
		,sf_opportunity_history.CompleteFileFlag__c
		, ref_CompleteFileFlag__c.LIBELLE_SF as CompleteFileFlag__c_label
		,sf_opportunity_history.CreatedById 
		,sf_opportunity_history.CreatedDate
		,sf_opportunity_history.DeniedOpportunityReason__c
		,ref_DeniedOpportunityReason__c.LIBELLE_SF as DeniedOpportunityReason__c_label
		,sf_opportunity_history.DistributionChannel__c
		,ref_DistributionChannel__c.LIBELLE_SF as DistributionChannel__c_label
		,sf_opportunity_history.DistributorEntity__c
		,ref_DistributorEntity__c.LIBELLE_SF as DistributorEntity__c_label
		,sf_opportunity_history.DistributorNetwork__c
		,ref_DistributorNetwork__c.LIBELLE_SF as DistributorNetwork__c_label
		,sf_opportunity_history.FinalizedBy__c
		,sf_opportunity_history.FormValidation__c
		,sf_opportunity_history.IDOppty__c
		,sf_opportunity_history.IDProcessSous__c
		,sf_opportunity_history.IdSource__c
		,sf_opportunity_history.Indication__c
		,sf_opportunity_history.LastModifiedDate
		,sf_opportunity_history.LeadSource
		,ref_LeadSource.LIBELLE_SF as LeadSource_label
		,sf_opportunity_history.Manager__c
		,sf_opportunity_history.NoIndication__c
		,sf_opportunity_history.OwnerId
		,sf_opportunity_history.PointOfSaleCode__c
		,sf_opportunity_history.ProductFamilyCode__c
		,sf_opportunity_history.ProductFamilyName__c
		,sf_opportunity_history.ProvenanceIndicationIndicatorId__c
		,sf_opportunity_history.RecordTypeId
		,ref_RecordTypeId.CODE_SF as RecordTypeCode
        ,ref_RecordTypeId.NAME_SF as RecordTypeName
		,sf_opportunity_history.RejectReason__c
		,ref_RejectReason__c.LIBELLE_SF as RejectReason__c_label
		,sf_opportunity_history.RelationEntryScore__c
		,ref_RelationEntryScore__c.LIBELLE_SF as RelationEntryScore__c_label
		,sf_opportunity_history.StageName
		,ref_StageName.LIBELLE_SF as StageName_label
		,sf_opportunity_history.StartedChannel__c
		,ref_StartedChannel__c.LIBELLE_SF as StartedChannel__c_label
		,sf_opportunity_history.SystemModstamp
		,sf_opportunity_history.Type
		,ref_Type.LIBELLE_SF as Type_label
		,sf_opportunity_history.IsWon
		,sf_opportunity_history.Name
		,sf_opportunity_history.realizedBy__c
		,sf_opportunity_history.SignatureDate__c
		,sf_opportunity_history.StartedChannelForIndication__c
		,sf_opportunity_history.ToBeDeleted__c
		,sf_opportunity_history.Amount
		,sf_opportunity_history.Fiscal
		,sf_opportunity_history.FiscalQuarter
		,sf_opportunity_history.FiscalYear
		,sf_opportunity_history.FolderAlias__c
		,sf_opportunity_history.ForecastCategory
		,sf_opportunity_history.ForecastCategoryName
		,sf_opportunity_history.HasOpenActivity
		,sf_opportunity_history.HasOpportunityLineItem
		,sf_opportunity_history.HasOverdueTask
		,sf_opportunity_history.IsClosed
		,sf_opportunity_history.IsDeleted
		,sf_opportunity_history.LastActivityDate
		,sf_opportunity_history.LastModifiedById
		,sf_opportunity_history.LastReferencedDate
		,sf_opportunity_history.LastViewedDate
		,sf_opportunity_history.NextStep
		,sf_opportunity_history.OriginalEvent__c
        ,ref_OriginalEvent__c.LIBELLE_SF as OriginalEvent__c_label 
		,sf_opportunity_history.SendingInformations__c
		,sf_opportunity_history.TCHExpiredIndication__c
		,sf_opportunity_history.TCHExpiredSubscription__c
		,sf_opportunity_history.TCHManualAnalysis__c
		,sf_opportunity_history.TCHOppCreation__c
		,sf_opportunity_history.IsPrivate
		,sf_opportunity_history.Probability
		,sf_opportunity_history.ExpectedRevenue
		,sf_opportunity_history.TotalOpportunityQuantity
		,sf_opportunity_history.CampaignId
		,sf_opportunity_history.Pricebook2Id
		,sf_opportunity_history.ContractId
		,sf_opportunity_history.TCHCompleteFile__c
		,sf_opportunity_history.TCHFromMarketingCase__c
		,sf_opportunity_history.TCHNotifDuplicateProspect__c
		,sf_opportunity_history.RecoveryLink__c
		,sf_opportunity_history.TCHId__c
		,sf_opportunity_history.ContractNumberDistrib__c
		,sf_opportunity_history.OptInTelcoData__c
		,sf_opportunity_history.SubscriberHolder__c
		,sf_opportunity_history.CreditAmount__c
		,sf_opportunity_history.CustomerAdviserId__c
		,sf_opportunity_history.DebitDay__c
		,sf_opportunity_history.DepositAmount__c
		,sf_opportunity_history.Derogation__c
		,sf_opportunity_history.FFinalDecision__c
		,sf_opportunity_history.FPreScoreColor__c
		,sf_opportunity_history.FirstDueDate__c
		,sf_opportunity_history.ForbiddenWords__c
		,sf_opportunity_history.FundingSubject__c
		,sf_opportunity_history.HasForbiddenWord__c
		,sf_opportunity_history.IncompleteFileReason__c
		,sf_opportunity_history.LoanAcceptationDate__c
		,sf_opportunity_history.ObjectInApproval__c
		,sf_opportunity_history.OffersRate__c
		,sf_opportunity_history.PrescriberContact__c
		,sf_opportunity_history.ReleaseAmount__c
		,sf_opportunity_history.ReleaseDate__c
		,sf_opportunity_history.StartDate__c
		,sf_opportunity_history.SubscribedAmount__c
		,sf_opportunity_history.TCHCategory__c
		,sf_opportunity_history.TracingProduct__c
		,sf_opportunity_history.RetractationDate__c		
		,sf_opportunity_history.OriginDistributionChannel__c 
		,ref_OriginDistributionChannel__c.LIBELLE_SF as OriginDistributionChannel__c_label	
        ,sf_opportunity_history.isSynchronized__c	
		,sf_opportunity_history.ApprovalInitiator__c
		,sf_opportunity_history.CheckGo__c
		,sf_opportunity_history.CommitmentRefusalReason__c
		,sf_opportunity_history.DebtRate__c
		,sf_opportunity_history.Description
		--SF-R5 04-02-2019
		,sf_opportunity_history.EndingChannel__c
		,sf_opportunity_history.IsClientOF__c
		,sf_opportunity_history.ManagerApproval__c
		,sf_opportunity_history.OptInOrderTelco__c
		,sf_opportunity_history.PreScoreCode__c
		,sf_opportunity_history.PreScoreReason__c
		,sf_opportunity_history.RTDN__c
		,sf_opportunity_history.TCH_PTD__c
		,sf_opportunity_history.TCHFinalDecision__c
		,sf_opportunity_history.TCHPreScoreColor__c	
		 -- JIRA 315 SF-R6
		,sf_opportunity_history.Commentthread__c
		,sf_opportunity_history.coBorrowerInsurance__c
		,sf_opportunity_history.mainBorrowerInsurance__c
        ,sf_opportunity_history.TCH_OAV_Granted__c
		--R8 22/06/2019	
		,sf_opportunity_history.CampaignCode__c
		,sf_opportunity_history.CreationLeadDate__c
		,sf_opportunity_history.DateOfFirstVisit__c
		,sf_opportunity_history.EventCode__c
		,sf_opportunity_history.LeadChannel__c
		,sf_opportunity_history.LeadID__c
		,sf_opportunity_history.LeadProductFamily__c
		,sf_opportunity_history.NumberOfVisits__c
		,sf_opportunity_history.OldLead__c
		,sf_opportunity_history.ContactId
		,sf_opportunity_history.TCHInitDistribChannel__c	
		,sf_opportunity_history.RecoveryLinkRIO__c
		,sf_opportunity_history.OptinPreattTelco__c
		,sf_opportunity_history.Validity_StartDate
		,sf_opportunity_history.Validity_EndDate
  from dbo.PV_SF_OPPORTUNITY_HISTORY sf_opportunity_history
  left join dbo.RAW_SF_REFERENTIEL as ref_CompleteFileFlag__c on ref_CompleteFileFlag__c.OBJECT_CODE_SF = 'opportunity' and ref_CompleteFileFlag__c.FIELD_CODE_SF = 'CompleteFileFlag__c' and case when sf_opportunity_history.CompleteFileFlag__c ='true' then '1' else '0' end = ref_CompleteFileFlag__c.CODE_SF
  left join ( select distinct CODE_SF, LIBELLE_SF from dbo.RAW_SF_REFERENTIEL where OBJECT_CODE_SF = 'opportunity' and FIELD_CODE_SF = 'DeniedOpportunityReason__c' ) as ref_DeniedOpportunityReason__c on sf_opportunity_history.DeniedOpportunityReason__c = ref_DeniedOpportunityReason__c.CODE_SF
  left join ( select distinct CODE_SF, LIBELLE_SF from dbo.RAW_SF_REFERENTIEL where OBJECT_CODE_SF = 'opportunity' and FIELD_CODE_SF = 'DistributionChannel__c' ) as ref_DistributionChannel__c on sf_opportunity_history.DistributionChannel__c = ref_DistributionChannel__c.CODE_SF
  left join ( select distinct CODE_SF, LIBELLE_SF from dbo.RAW_SF_REFERENTIEL where OBJECT_CODE_SF = 'opportunity' and FIELD_CODE_SF = 'DistributorEntity__c' ) as ref_DistributorEntity__c on sf_opportunity_history.DistributorEntity__c = ref_DistributorEntity__c.CODE_SF
  left join ( select distinct CODE_SF, LIBELLE_SF from dbo.RAW_SF_REFERENTIEL where OBJECT_CODE_SF = 'opportunity' and FIELD_CODE_SF = 'DistributorNetwork__c' ) as ref_DistributorNetwork__c on sf_opportunity_history.DistributorNetwork__c = ref_DistributorNetwork__c.CODE_SF
  left join ( select distinct CODE_SF, LIBELLE_SF from dbo.RAW_SF_REFERENTIEL where OBJECT_CODE_SF = 'opportunity' and FIELD_CODE_SF = 'LeadSource' ) as ref_LeadSource on sf_opportunity_history.LeadSource = ref_LeadSource.CODE_SF
  left join ( select distinct CODE_SF, LIBELLE_SF from dbo.RAW_SF_REFERENTIEL where OBJECT_CODE_SF = 'opportunity' and FIELD_CODE_SF = 'OriginalEvent__c' ) as ref_OriginalEvent__c on sf_opportunity_history.OriginalEvent__c = ref_OriginalEvent__c.CODE_SF
  left join ( select distinct CODE_SF, LIBELLE_SF from dbo.RAW_SF_REFERENTIEL where OBJECT_CODE_SF = 'opportunity' and FIELD_CODE_SF = 'RejectReason__c' ) as ref_RejectReason__c on sf_opportunity_history.RejectReason__c = ref_RejectReason__c.CODE_SF
  left join ( select distinct CODE_SF, LIBELLE_SF from dbo.RAW_SF_REFERENTIEL where OBJECT_CODE_SF = 'opportunity' and FIELD_CODE_SF = 'RelationEntryScore__c' ) as ref_RelationEntryScore__c on sf_opportunity_history.RelationEntryScore__c = ref_RelationEntryScore__c.CODE_SF
  left join ( select distinct CODE_SF, LIBELLE_SF from dbo.RAW_SF_REFERENTIEL where OBJECT_CODE_SF = 'opportunity' and FIELD_CODE_SF = 'StageName' ) as ref_StageName on sf_opportunity_history.StageName = ref_StageName.CODE_SF
  left join ( select distinct CODE_SF, LIBELLE_SF from dbo.RAW_SF_REFERENTIEL where OBJECT_CODE_SF = 'opportunity' and FIELD_CODE_SF = 'StartedChannel__c' ) as ref_StartedChannel__c on sf_opportunity_history.StartedChannel__c = ref_StartedChannel__c.CODE_SF
  left join ( select distinct CODE_SF, LIBELLE_SF from dbo.RAW_SF_REFERENTIEL where OBJECT_CODE_SF = 'opportunity' and FIELD_CODE_SF = 'Type' ) as ref_Type on sf_opportunity_history.Type = ref_Type.CODE_SF
  left join ( select distinct CODE_SF, LIBELLE_SF from dbo.RAW_SF_REFERENTIEL where OBJECT_CODE_SF = 'opportunity' and FIELD_CODE_SF = 'DistributionChannel__c' ) as ref_OriginDistributionChannel__c on sf_opportunity_history.OriginDistributionChannel__c = ref_OriginDistributionChannel__c.CODE_SF
  left join dbo.REF_RECORDTYPE as ref_RecordTypeId on ref_RecordTypeId.OBJECTTYPE_SF = 'Opportunity' and ref_RecordTypeId.ID_SF = sf_opportunity_history.RecordTypeId
