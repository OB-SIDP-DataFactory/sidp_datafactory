﻿CREATE VIEW [dbo].[VW_PV_SF_OPPORTUNITYCONTACTROLE] AS
select sf_opportunitycontactrole.Id
     , sf_opportunitycontactrole.Id_SF
	 , sf_opportunitycontactrole.OpportunityId
	 , sf_opportunitycontactrole.ContactId
	 , sf_opportunitycontactrole.Role
	 , sf_opportunitycontactrole.IsPrimary
   	 , sf_opportunitycontactrole.CreatedDate
	 , sf_opportunitycontactrole.CreatedById
	 , sf_opportunitycontactrole.LastModifiedDate
	 , sf_opportunitycontactrole.LastModifiedById
	 , sf_opportunitycontactrole.SystemModstamp
	 , sf_opportunitycontactrole.IsDeleted
     , sf_opportunitycontactrole.Validity_StartDate
     , sf_opportunitycontactrole.Validity_EndDate
     , sf_opportunitycontactrole.Startdt
     , sf_opportunitycontactrole.Enddt
   from ( select [Id], [Id_SF], [OpportunityId], [ContactId], [Role], [IsPrimary], [CreatedDate], [CreatedById], [LastModifiedDate], [LastModifiedById], [SystemModstamp], [IsDeleted], [Validity_StartDate], [Validity_EndDate], [Startdt], [Enddt] from dbo.PV_SF_OPPORTUNITYCONTACTROLE
         union all
         select [Id], [Id_SF], [OpportunityId], [ContactId], [Role], [IsPrimary], [CreatedDate], [CreatedById], [LastModifiedDate], [LastModifiedById], [SystemModstamp], [IsDeleted], [Validity_StartDate], [Validity_EndDate], [Startdt], [Enddt] from dbo.PV_SF_OPPORTUNITYCONTACTROLEHistory ) sf_opportunitycontactrole