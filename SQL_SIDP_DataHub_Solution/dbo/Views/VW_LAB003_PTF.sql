﻿CREATE VIEW [dbo].[VW_LAB003_PTF]
AS
SELECT
	CAST(FORMAT(GETDATE(), 'yyyyMMdd') AS VARCHAR(8)) AS [DT_EXT_ENR],
	CAST(ISNULL(LTRIM(RTRIM(PTF.DWHPTFNPT)), '') AS VARCHAR(32))	AS	[ID_CPT],
	CAST('BANQUE' AS VARCHAR(6))	AS	[IC_UNI_ORG],
	CAST(ISNULL(LTRIM(RTRIM(PTF.DWHPTFIN1)), '') AS VARCHAR(50))	AS	[LB_CPT],
	CAST('' AS VARCHAR(7))	AS	[ID_SAB_CLI],
	CAST('' AS VARCHAR(100))	AS	[ID_GRC_CLI_JOI_1],
	CAST('' AS VARCHAR(100))	AS	[ID_GRC_CLI_JOI_2],
	CAST('' AS VARCHAR(100))	AS	[ID_GRC_CLI],
	CAST(IIF(LTRIM(RTRIM(PTF.DWHPTFPEA)) = 'O', 'PEAS', 'TITS') AS VARCHAR(4))	AS	[CD_TYP_CPT],
	CAST('EUR' AS VARCHAR(3))	AS	[CD_DVS_CPT],
	CAST('' AS VARCHAR(3))	AS	[CD_DVS_PIV],
	CAST('' AS VARCHAR(10))	AS	[CD_OTO],
	CAST(FORMAT(ABS(0), 'F2') AS VARCHAR(32))						AS	[MT_OTO],
	CAST(FORMAT(ABS(0), 'F2') AS VARCHAR(32))						AS	[MT_CAP_RTT_DVS],
	CAST(FORMAT(ABS(ISNULL(PTF.DWHPTFVGL, 0)),'F2') AS VARCHAR(32))		AS	[MT_CAP_RTT_EUR],
	CAST('' AS VARCHAR(3))	AS	[CD_STT],
	CAST(ISNULL(FORMAT(PTF.DWHPTFDOV, 'yyyyMMdd'), '') AS VARCHAR(8))	AS	[DT_OUV],
	CAST(ISNULL(FORMAT(PTF.DWHPTFDCL, 'yyyyMMdd'), '') AS VARCHAR(8))	AS	[DT_FER],
	CAST('' AS VARCHAR(8))	AS	[DT_DER_DBL_CRD],
	CAST('' AS VARCHAR(30))	AS	[CD_DTT_BQE],
	CAST('' AS VARCHAR(8))	AS	[DT_DER_ECH_PY],
	CAST(FORMAT(ABS(0), 'F2') AS VARCHAR(32))						AS	[MT_PE],
	CAST('' AS VARCHAR(3))	AS	[DR_CON],
	CAST('' AS VARCHAR(35))	AS	[RF_IBAN],
	CAST('' AS CHAR(1))	AS	[CD_BLC_BQE],
	CAST('' AS VARCHAR(32))	AS	[CD_RAI_BLC],
	CAST(ISNULL(FORMAT(PTF.DWHPTFDMJ ,'yyyyMMdd'), '') AS VARCHAR(8))	AS	[DT_MAJ_CPT],
	CAST('001_GB_' + FORMAT(GETDATE(), 'yyyyMMdd') + '_ACCOUNT_BANK_DAILY.txt' AS VARCHAR(256))	AS	[FileName],
	CAST(FORMAT(GETDATE(), 'yyyyMMdd') AS VARCHAR(8))	AS	[CreatedDate],
	CAST(FORMAT(GETDATE(), 'yyyyMMdd') AS VARCHAR(8))	AS	[LastModifiedDate],
	CAST(FORMAT(GETDATE(), 'yyyyMMdd') AS VARCHAR(8))	AS	[TerminatedDate]
FROM
	dbo.PV_SA_M_PORTEFEUILLE	AS	PTF	WITH(NOLOCK)
GO


--SELECT DISTINCT
--    -- Date extraction
--    FORMAT(GETDATE(), 'yyyyMMdd')					AS	[DT_EXT_ENR],
--    -- Numéro de compte/prêt/portefeuille (cpt miroir)
--    ISNULL(LTRIM(RTRIM(PTF.DWHPTFNPT)), '') + '|' +
--    -- Unité organisationnelle
--    'BANQUE'	AS	[Unité organisationnelle],
--    -- Intitulé du compte/prêt/portefeuille
--    ISNULL(LTRIM(RTRIM(PTF.DWHPTFIN1)), '') + '|' +
--    -- Code client (SAB collectif ou unique)
--    ISNULL(LTRIM(RTRIM(CLI.DWHCLICLI)), '') + '|' +
--    -- Code client 1 (GRC si client collectif)
--    '|' +
--    -- Code client 2 (GRC si client collectif)
--    '|' +
--    -- Code client (GRC si client unique)
--    ISNULL(LTRIM(RTRIM(
--        CASE
--            WHEN        ISNULL(CLI.DWHCLITIE, '') = ''
--                        AND REF.CLIREFCOR IN ('SI', 'SF', 'AC')
--                        AND REF.CLIREFREF NOT LIKE 'I%'
--                        AND REF.CLIREFREF NOT LIKE 'T%'
--                THEN    IIF(REF.CLIREFCOR = 'SF', 'SF', '') + REF.CLIREFREF
--        END
--    )), '') + '|' +
--    -- Code type de compte/prêt/portefeuille (code produit)
--    IIF(LTRIM(RTRIM(PTF.DWHPTFPEA)) = 'O', 'PEAS', 'TITS') + '|' +
--    -- Devise
--    'EUR' + '|' +
--    -- Devise pivot
--    '|' +
--    -- Code autorisation
--    '|' +
--    -- Montant autorisation
--    '|' +
--    -- Montant capital restant dû (devise)
--    '|' +
--    -- Montant capital restant dû (euro)
--    FORMAT(ABS(ISNULL(PTF.DWHPTFVGL, 0)),'F2') + '|' +
--    -- Statut
--    IIF(DWHPTFPCL = 'N', 'V', 'S') + '|' +
--    -- Date ouverture
--    ISNULL(FORMAT(PTF.DWHPTFDOV, 'yyyyMMdd'), '') + '|' +
--    -- Date clôture
--    ISNULL(FORMAT(PTF.DWHPTFDCL, 'yyyyMMdd'), '') + '|' +
--    -- Date de dernier déblocage crédit
--    '|' +
--    -- Canal de détection Banque
--    '|' +
--    -- Date de dernière échéance payée
--    '|' +
--    -- Montant du prêt
--    '|' +
--    -- Durée du contrat
--    '|' +
--    -- IBAN
--    '|' +
--    -- Code blocage
--    '|' +
--    -- Raison de blocage
--    '|' +
--    -- Date de mise à jour du compte/prêt/portefeuille
--    ISNULL(FORMAT(PTF.DWHPTFDMJ ,'yyyyMMdd'), '') + '|'    AS EXTRACT
--FROM
--    dbo.PV_SA_M_PORTEFEUILLE       AS  PTF WITH(NOLOCK)
--    INNER JOIN  dbo.PV_SA_Q_REF    AS  REF WITH(NOLOCK)
--        ON  REF.CLIREFREF = SUBSTRING(PTF.DWHPTFRCL, 2, 6)
--            AND REF.CLIREFCOR = 'SA'
--    INNER JOIN  dbo.PV_SA_Q_CLIENT AS  CLI WITH(NOLOCK)
--        ON  CLI.DWHCLICLI = REF.CLIREFCLI
--WHERE
--    1 = 1
--    AND CLI.DWHCLIAGE NOT IN ('51', '52')
--    AND CLI.DWHCLIRES   NOT IN
--    (
--        'C01','C02','C03',
--        'D01','D05',
--        'T01','T02','T03',
--        '650','651','652','653','654','655','656','657','658','659',
--        '660','661','662','663','664','665','666','667','668','669',
--        '670','671','672','673','674','675','676'
--    )
--    AND CLI.DWHCLICLO IS NULL