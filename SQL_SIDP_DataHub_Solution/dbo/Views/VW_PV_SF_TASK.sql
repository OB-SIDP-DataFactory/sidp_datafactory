﻿
CREATE VIEW [dbo].[VW_PV_SF_TASK]
AS
	SELECT
		sf_task.Id,
		sf_task.Id_SF,
		sf_task.RecordTypeId,
		ref_RecordTypeId.CODE_SF AS RecordTypeCode,
		ref_RecordTypeId.NAME_SF AS RecordTypeName,
		sf_task.WhatId,
		sf_task.OwnerId,
		sf_task.Description,
		sf_task.WhoId,
		sf_task.Subject,
		ref_Subject.LIBELLE_SF AS Subject_label,
		sf_task.InteractionId__c,
		sf_task.CreatedById,
		sf_task.CreatedDate,
		sf_task.LastModifiedDate,
		sf_task.TaskSubType,
		sf_task.CompletedDateTime,
		ref_TaskSubType.LIBELLE_SF AS TaskSubType_label,
		sf_task.Priority,
		ref_Priority.LIBELLE_SF AS Priority_label,
		sf_task.Status,
		ref_Status.LIBELLE_SF AS Status_label,
		sf_task.Family__c,
		ref_Family__c.LIBELLE_SF AS Family__c_label,
		sf_task.SubFamily__c,
		ref_SubFamily__c.LIBELLE_SF AS SubFamily__c_label,
		sf_task.InteractionDate__c,
		sf_task.ScoreOfInteraction__c,
		sf_task.NotationLabel__c,
		sf_task.ResponseLink__c,
		sf_task.ActivityDate,
		sf_task.ConversationID__c,
		sf_task.CallType__c,
		sf_task.IsHighPriority,
		sf_task.Type,
		sf_task.IsDeleted,
		sf_task.AccountId,
		sf_task.IsClosed,
		sf_task.LastModifiedById,
		sf_task.SystemModstamp,
		sf_task.IsArchived,
		sf_task.IsVisibleInSelfService,
		sf_task.CallDurationInSeconds,
		sf_task.CallType,
		sf_task.CallDisposition,
		sf_task.CallObject,
		sf_task.ReminderDateTime,
		sf_task.IsReminderSet,
		sf_task.RecurrenceActivityId,
		sf_task.IsRecurrence,
		sf_task.RecurrenceStartDateOnly,
		sf_task.RecurrenceEndDateOnly,
		sf_task.RecurrenceTimeZoneSidKey,
		sf_task.RecurrenceType,
		sf_task.RecurrenceInterval,
		sf_task.RecurrenceDayOfWeekMask,
		sf_task.RecurrenceDayOfMonth,
		sf_task.RecurrenceInstance,
		sf_task.RecurrenceMonthOfYear,
		sf_task.RecurrenceRegeneratedType,
		sf_task.Description__c,
		sf_task.InfosCompl__c,
		sf_task.NbMonthsSinceCreation__c,
		sf_task.TCHResponseId__c,
		sf_task.TaskDate__c,
		sf_task.TaskId__c,
		sf_task.Gen_example_c__c,
		sf_task.DocumentURL__c,
		sf_task.ForbiddenWords__c,
		sf_task.HasForbiddenWord__c,
		sf_task.DeliveryAddress__c,
		sf_task.Validity_StartDate,
		sf_task.Validity_EndDate
	FROM 	(
		select [Id], [Id_SF], [RecordTypeId], [WhatId], [OwnerId], [Description], [WhoId], [Subject], [InteractionId__c], [CreatedById], [CreatedDate], [LastModifiedDate], [TaskSubType], [CompletedDateTime], [Priority], [Status], [Family__c], [SubFamily__c], [InteractionDate__c], [ScoreOfInteraction__c], [NotationLabel__c], [ResponseLink__c], [ActivityDate], [ConversationID__c], [CallType__c], [IsHighPriority], [Type], [IsDeleted], [AccountId], [IsClosed], [LastModifiedById], [SystemModstamp], [IsArchived], [IsVisibleInSelfService], [CallDurationInSeconds], [CallType], [CallDisposition], [CallObject], [ReminderDateTime], [IsReminderSet], [RecurrenceActivityId], [IsRecurrence], [RecurrenceStartDateOnly], [RecurrenceEndDateOnly], [RecurrenceTimeZoneSidKey], [RecurrenceType], [RecurrenceInterval], [RecurrenceDayOfWeekMask], [RecurrenceDayOfMonth], [RecurrenceInstance], [RecurrenceMonthOfYear], [RecurrenceRegeneratedType], [Description__c], [InfosCompl__c], [NbMonthsSinceCreation__c], [TCHResponseId__c], [TaskDate__c], [TaskId__c], [Gen_example_c__c], [DocumentURL__c], [ForbiddenWords__c], [HasForbiddenWord__c], [DeliveryAddress__c], [Validity_StartDate], [Validity_EndDate], [Startdt], [Enddt] from dbo.PV_SF_TASK
		union all
		select [Id], [Id_SF], [RecordTypeId], [WhatId], [OwnerId], [Description], [WhoId], [Subject], [InteractionId__c], [CreatedById], [CreatedDate], [LastModifiedDate], [TaskSubType], [CompletedDateTime], [Priority], [Status], [Family__c], [SubFamily__c], [InteractionDate__c], [ScoreOfInteraction__c], [NotationLabel__c], [ResponseLink__c], [ActivityDate], [ConversationID__c], [CallType__c], [IsHighPriority], [Type], [IsDeleted], [AccountId], [IsClosed], [LastModifiedById], [SystemModstamp], [IsArchived], [IsVisibleInSelfService], [CallDurationInSeconds], [CallType], [CallDisposition], [CallObject], [ReminderDateTime], [IsReminderSet], [RecurrenceActivityId], [IsRecurrence], [RecurrenceStartDateOnly], [RecurrenceEndDateOnly], [RecurrenceTimeZoneSidKey], [RecurrenceType], [RecurrenceInterval], [RecurrenceDayOfWeekMask], [RecurrenceDayOfMonth], [RecurrenceInstance], [RecurrenceMonthOfYear], [RecurrenceRegeneratedType], [Description__c], [InfosCompl__c], [NbMonthsSinceCreation__c], [TCHResponseId__c], [TaskDate__c], [TaskId__c], [Gen_example_c__c], [DocumentURL__c], [ForbiddenWords__c], [HasForbiddenWord__c], [DeliveryAddress__c], [Validity_StartDate], [Validity_EndDate], [Startdt], [Enddt] from dbo.PV_SF_TASKHistory
	) sf_task

		LEFT JOIN ( SELECT
			DISTINCT
			CODE_SF,
			LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL
		WHERE OBJECT_CODE_SF = 'task' AND FIELD_CODE_SF = 'Subject' ) AS ref_Subject ON sf_task.Subject = ref_Subject.CODE_SF
		LEFT JOIN ( SELECT
			DISTINCT
			CODE_SF,
			LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL
		WHERE OBJECT_CODE_SF = 'task' AND FIELD_CODE_SF = 'TaskSubtype' ) AS ref_TaskSubType ON sf_task.TaskSubType = ref_TaskSubType.CODE_SF
		LEFT JOIN ( SELECT
			DISTINCT
			CODE_SF,
			LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL
		WHERE OBJECT_CODE_SF = 'task' AND FIELD_CODE_SF = 'Priority' ) AS ref_Priority ON sf_task.Priority = ref_Priority.CODE_SF
		LEFT JOIN ( SELECT
			DISTINCT
			CODE_SF,
			LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL
		WHERE OBJECT_CODE_SF = 'task' AND FIELD_CODE_SF = 'Status' ) AS ref_Status ON sf_task.Status = ref_Status.CODE_SF
		LEFT JOIN ( SELECT
			DISTINCT
			CODE_SF,
			LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL
		WHERE OBJECT_CODE_SF = 'task' AND FIELD_CODE_SF = 'Family__c' ) AS ref_Family__c ON sf_task.Family__c = ref_Family__c.CODE_SF
		LEFT JOIN ( SELECT
			DISTINCT
			CODE_SF,
			LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL
		WHERE OBJECT_CODE_SF = 'task' AND FIELD_CODE_SF = 'SubFamily__c' ) AS ref_SubFamily__c ON sf_task.SubFamily__c = ref_SubFamily__c.CODE_SF
		LEFT JOIN ( SELECT
			DISTINCT
			CODE_SF,
			LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL
		WHERE OBJECT_CODE_SF = 'task' AND FIELD_CODE_SF = 'IsHighPriority' ) AS ref_IsHighPriority ON sf_task.IsHighPriority = ref_IsHighPriority.CODE_SF
		LEFT JOIN ( SELECT
			DISTINCT
			CODE_SF,
			LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL
		WHERE OBJECT_CODE_SF = 'task' AND FIELD_CODE_SF = 'Type' ) AS ref_Type ON sf_task.Type = ref_Type.CODE_SF
		LEFT JOIN ( SELECT
			DISTINCT
			CODE_SF,
			LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL
		WHERE OBJECT_CODE_SF = 'task' AND FIELD_CODE_SF = 'CallType' ) AS ref_CallType ON sf_task.CallType = ref_CallType.CODE_SF
		LEFT JOIN ( SELECT
			DISTINCT
			CODE_SF,
			LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL
		WHERE OBJECT_CODE_SF = 'task' AND FIELD_CODE_SF = 'RecurrenceTimeZoneSidKey' ) AS ref_RecurrenceTimeZoneSidKey ON sf_task.RecurrenceTimeZoneSidKey = ref_RecurrenceTimeZoneSidKey.CODE_SF
		LEFT JOIN ( SELECT
			DISTINCT
			CODE_SF,
			LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL
		WHERE OBJECT_CODE_SF = 'task' AND FIELD_CODE_SF = 'RecurrenceType' ) AS ref_RecurrenceType ON sf_task.RecurrenceType = ref_RecurrenceType.CODE_SF
		LEFT JOIN ( SELECT
			DISTINCT
			CODE_SF,
			LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL
		WHERE OBJECT_CODE_SF = 'task' AND FIELD_CODE_SF = 'RecurrenceInstance' ) AS ref_RecurrenceInstance ON sf_task.RecurrenceInstance = ref_RecurrenceInstance.CODE_SF
		LEFT JOIN ( SELECT
			DISTINCT
			CODE_SF,
			LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL
		WHERE OBJECT_CODE_SF = 'task' AND FIELD_CODE_SF = 'RecurrenceMonthOfYear' ) AS ref_RecurrenceMonthOfYear ON sf_task.RecurrenceMonthOfYear = ref_RecurrenceMonthOfYear.CODE_SF
		LEFT JOIN ( SELECT
			DISTINCT
			CODE_SF,
			LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL
		WHERE OBJECT_CODE_SF = 'task' AND FIELD_CODE_SF = 'RecurrenceRegeneratedType' ) AS ref_RecurrenceRegeneratedType ON sf_task.RecurrenceRegeneratedType = ref_RecurrenceRegeneratedType.CODE_SF
		LEFT JOIN dbo.REF_RECORDTYPE AS ref_RecordTypeId ON ref_RecordTypeId.OBJECTTYPE_SF = 'Task' AND ref_RecordTypeId.ID_SF = sf_task.RecordTypeId