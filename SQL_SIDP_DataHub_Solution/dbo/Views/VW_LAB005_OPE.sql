﻿CREATE VIEW [dbo].[VW_LAB005_OPE]
AS
SELECT
	CAST(FORMAT(GETDATE(), 'yyyyMMdd') AS VARCHAR(8))					AS	[DT_EXT_ENR],
	CAST(ISNULL(FORMAT(OPE.DTE_OPE, 'yyyyMMdd'), '') AS VARCHAR(8))		AS	[DT_OPE],
	CAST(
		RIGHT(REPLICATE('0', 9)
		+ LTRIM(RTRIM(OPE.NUM_PIE)), 9)
		+ RIGHT(REPLICATE('0', 7)
		+ LTRIM(RTRIM(OPE.NUM_ECR)), 7) AS VARCHAR(32))					AS	[ID_OPE],
	CAST(ISNULL(LTRIM(RTRIM(OPE.NUM_CPT)), '') AS VARCHAR(32))			AS	[ID_CPT],
	CAST(
		LEFT(
			LTRIM(RTRIM(ISNULL(OPE.COD_OPE, ''))) + REPLICATE('-', 3), 3
		) +
		LEFT(
			LTRIM(RTRIM(ISNULL(OPE.COD_EVE, ''))) + REPLICATE('-', 3), 3
		) +
		LEFT(
			LTRIM(RTRIM(ISNULL(OPE.NAT_OPE, ''))) + REPLICATE('-', 3), 3
		) +
		LEFT(
			LTRIM(RTRIM(ISNULL(OPE.COD_ANA, ''))) +	REPLICATE('-', 6), 6
		) AS VARCHAR(15))												AS	[CD_TXN],
	CAST(ISNULL(LTRIM(RTRIM(OPE.COD_OPE)), '') AS VARCHAR(20))			AS	[CD_OPE],
	CAST(ISNULL(LTRIM(RTRIM(OPE.COD_EVE)), '') AS VARCHAR(20))			AS	[CD_EVE],
	CAST(ISNULL(LTRIM(RTRIM(OPE.NAT_OPE)), '') AS VARCHAR(20))			AS	[CD_NAT_OPE],
	CAST(ISNULL(FORMAT(OPE.DTE_VAL, 'yyyyMMdd'), '') AS VARCHAR(8))		AS	[DT_VAL],
	CAST('' AS VARCHAR(255))											AS	[LB_OPE],
	CAST(ISNULL(OPE.DEV, '') AS VARCHAR(3))								AS	[CD_DVS_OPE],
	CAST('EUR' AS VARCHAR(3))											AS	[CD_DVS_BAS],
	CAST(IIF(OPE.MTT_DEV <= 0, 'C', 'D') AS CHAR(1))					AS	[CD_DBI_CRD],
	CAST(FORMAT(ABS(ISNULL(OPE.MTT_DEV, 0)), 'F2') AS VARCHAR(32))		AS	[MT_OPE_DVS],
	CAST(FORMAT(ABS(ISNULL(OPE.MTT_EUR, 0)), 'F2') AS VARCHAR(32))		AS	[MT_OPE_EUR],
	CAST(ISNULL(LTRIM(RTRIM(OPE.COD_ANA)), '') AS VARCHAR(6))			AS	[CD_ANL],
	CAST(ISNULL(LTRIM(RTRIM(OPE.BIC_CTP)), '') AS VARCHAR(32))			AS	[ID_ETB_CTP],
	CAST(ISNULL(LTRIM(RTRIM(OPE.IDE_CTP)), '') AS VARCHAR(32))			AS	[ID_CTP],
	CAST(
		ISNULL(
			IIF(
				SUBSTRING(LTRIM(OPE.NUM_CPT_CTP), 1, 2) = 'FR',
				RTRIM(LTRIM(OPE.NUM_CPT_CTP)),
				IIF(
					OPE.COD_PAY_CTP IS NOT NULL,
					OPE.COD_PAY_CTP + RTRIM(LTRIM(OPE.NUM_CPT_CTP)),
					'FR' + RTRIM(LTRIM(OPE.NUM_CPT_CTP))
				)
			),
		'') AS VARCHAR(35))												AS	[ID_IBAN_CTP],
	CAST(ISNULL(LTRIM(RTRIM(OPE.NOM_CTP)), '') AS VARCHAR(70))			AS	[NM_CTP],
	CAST(ISNULL(FORMAT(OPE.DTE_TRT, 'yyyyMMdd'), '') AS VARCHAR(8))		AS	[DT_TXN],
	CAST(IIF(CPT.DWHCPTCPC <= 0, 'C', 'D') AS CHAR(1))				AS	[CD_SEN_SOL],
	CAST(FORMAT(ABS(0), 'F2') AS VARCHAR(32))							AS	[MT_SOL_DVS],
	CAST(FORMAT(ABS(ISNULL(CPT.DWHCPTCPT, 0)), 'F2') AS VARCHAR(32))	AS	[MT_SOL_EUR],
	CAST(ISNULL(FORMAT(CPT.DWHCPTDTX, 'yyyyMMdd'), '') AS VARCHAR(8))	AS	[DT_DER_MVT],
	CAST('001_GB_' + FORMAT(GETDATE(), 'yyyyMMdd') + '_TRANSACTION_BANK_DAILY.txt' AS VARCHAR(256)) AS [FileName],
	CAST(ISNULL(FORMAT(OPE.DTE_VAL, 'yyyyMMdd'), '') AS VARCHAR(8))		AS	[ValueDate],
	CAST(ISNULL(FORMAT(CPT.DWHCPTDTX, 'yyyyMMdd'), '') AS VARCHAR(8))	AS	[LastTransactionDate],
	CAST(ISNULL(FORMAT(OPE.DTE_OPE, 'yyyyMMdd'), '') AS VARCHAR(8))		AS	[OperationDate]
FROM
	dbo.PV_SA_Q_MOUVEMENT		AS	OPE	WITH(NOLOCK)
	JOIN	 dbo.PV_SA_Q_COMPTE	AS	CPT	WITH(NOLOCK)
		ON	CPT.DWHCPTCOM = OPE.NUM_CPT
    INNER JOIN  dbo.PV_SA_Q_CLIENT AS  CLI WITH(NOLOCK)
        ON  CLI.DWHCLICLI = CPT.DWHCPTPPAL
WHERE
	1 = 1
	AND CLI.DWHCLIAGE NOT IN ('51', '52')
	AND CLI.DWHCLIRES   NOT IN
	(
		'C01','C02','C03',
		'D01','D05',
		'T01','T02','T03',
		'650','651','652','653','654','655','656','657','658','659',
		'660','661','662','663','664','665','666','667','668','669',
		'670','671','672','673','674','675','676'
	)
	AND OPE.COD_OPE NOT IN ('ECH', 'FAC')
	AND LEFT(DWHCPTRUB, 1) NOT IN ('6','7')
GO

--SELECT DISTINCT
--    -- Date extraction
--    FORMAT(GETDATE(), 'yyyyMMdd') AS	[DT_EXT_ENR],
--    -- Date opération
--    ISNULL(FORMAT(OPE.DTE_OPE, 'yyyyMMdd'), '')	AS [DATE_OPERATION],
--    -- Numéro opération
--    RIGHT(REPLICATE('0', 9) + LTRIM(RTRIM(OPE.NUM_PIE)), 9) + RIGHT(REPLICATE('0', 7) + LTRIM(RTRIM(OPE.NUM_ECR)), 7)	AS	[Numéro opération],
--    -- Numéro de compte/prêt/portefeuille
--    ISNULL(LTRIM(RTRIM(OPE.NUM_CPT)), '') + '|' +
--    -- Code transaction
--    LEFT(LTRIM(RTRIM(ISNULL(OPE.COD_OPE, ''))) + REPLICATE('-', 3), 3) +
--    LEFT(LTRIM(RTRIM(ISNULL(OPE.COD_EVE, ''))) + REPLICATE('-', 3), 3) +
--    LEFT(LTRIM(RTRIM(ISNULL(OPE.NAT_OPE, ''))) + REPLICATE('-', 3), 3) +
--    LEFT(LTRIM(RTRIM(ISNULL(OPE.COD_ANA, ''))) + REPLICATE('-', 6), 6)	AS	[Code transaction],
--    -- Code opération
--    ISNULL(LTRIM(RTRIM(OPE.COD_OPE)), '')	AS	[Code opération],
--    -- Code événement
--    ISNULL(LTRIM(RTRIM(OPE.COD_EVE)), '')	AS	[Code événement],
--    -- Code nature opération
--    ISNULL(LTRIM(RTRIM(OPE.NAT_OPE)), '')	AS	[Code nature opération],
--    -- Date valeur
--    ISNULL(FORMAT(OPE.DTE_VAL, 'yyyyMMdd'), '') + '|' +
--    --Libellé opération
--    ISNULL(LTRIM(RTRIM(REPLACE(OPE.LIB_MVT_1, '|', ' '))), '') +
--    CASE
--        WHEN        LTRIM(RTRIM(ISNULL(OPE.LIB_MVT_2, ''))) <> ''
--            THEN    ' ' + LTRIM(RTRIM(REPLACE(OPE.LIB_MVT_2, '|', ' ')))
--    END +
--    CASE
--        WHEN        LTRIM(RTRIM(ISNULL(OPE.LIB_MVT_3, ''))) <> ''
--            THEN    ' ' + LTRIM(RTRIM(REPLACE(OPE.LIB_MVT_3, '|', ' ')))
--    END + '|' +
--    -- Devise opération
--    ISNULL(OPE.DEV, '') + '|' +
--    -- Devise de base
--    'EUR'	AS	[Devise de base],
--    -- Code sens montant opération
--    CASE
--        WHEN        OPE.MTT_DEV <= 0
--            THEN    'C'
--        ELSE        'D'
--    END + '|' +
--    -- Montant en devise
--    FORMAT(ABS(ISNULL(OPE.MTT_DEV, 0)), 'N')	AS	[Montant en devise],
--    -- Montant en contrevaleur euro
--    FORMAT(ABS(ISNULL(OPE.MTT_EUR, 0)), 'N') + '|' +
--    -- Code analytique
--    ISNULL(LTRIM(RTRIM(OPE.COD_ANA)), '') + '|' +
--    -- BIC établissement contrepartie
--    ISNULL(LTRIM(RTRIM(OPE.BIC_CTP)), '') + '|' +
--    -- Identifiant contrepartie
--    ISNULL(LTRIM(RTRIM(OPE.IDE_CTP)), '')	AS	[Identifiant contrepartie],
--    -- IBAN contrepartie
--    CASE
--        WHEN        SUBSTRING(LTRIM(OPE.NUM_CPT_CTP), 1, 2) = 'FR'
--            THEN    LTRIM(OPE.NUM_CPT_CTP)
--        ELSE
--            CASE
--                WHEN        OPE.COD_PAY_CTP IS NOT NULL
--                    THEN    OPE.COD_PAY_CTP + LTRIM(OPE.NUM_CPT_CTP)
--                ELSE        'FR' + LTRIM(OPE.NUM_CPT_CTP)
--            END
--    END + '|' +
--    -- Nom/Raison sociale contrepartie
--    ISNULL(LTRIM(RTRIM(OPE.NOM_CTP)), '') + '|' +
--    -- Date traitement opération
--    ISNULL(FORMAT(OPE.DTE_TRT, 'yyyyMMdd'), '') + '|' +
--    -- Sens solde compte
--    IIF(CPT.DWHCPTCPC <= 0, 'C', 'D') + '|' +
--    -- Solde compte en devise
--    FORMAT(ABS(ISNULL(CPT.DWHCPTCPC, 0)), 'N')	AS	[Solde compte en devise],
--    -- Solde compte en contrevaleur euro
--    FORMAT(ABS(ISNULL(CPT.DWHCPTCPT, 0)), 'N') + '|' +
--    -- Date dernier mouvement compte
--    ISNULL(FORMAT(CPT.DWHCPTDTX, 'yyyyMMdd'), '')	AS	[Date dernier mouvement compte]
--FROM
--    dbo.PV_SA_Q_MOUVEMENT          AS  OPE WITH(NOLOCK)
--    JOIN    MAX_DATE
--        ON  MAX_DATE.MAX_DTE_TRT = OPE.DTE_TRT
--    INNER JOIN  dbo.PV_SA_Q_COMPTE AS  CPT WITH(NOLOCK)
--        ON  CPT.DWHCPTCOM = OPE.NUM_CPT
--    INNER JOIN  dbo.PV_SA_Q_CLIENT AS  CLI WITH(NOLOCK)
--        ON  CLI.DWHCLICLI = CPT.DWHCPTPPAL
--WHERE
--    1 = 1
--    AND CLI.DWHCLIAGE NOT IN ('51', '52')
--    AND CLI.DWHCLIRES   NOT IN
--    (
--        'C01','C02','C03',
--        'D01','D05',
--        'T01','T02','T03',
--        '650','651','652','653','654','655','656','657','658','659',
--        '660','661','662','663','664','665','666','667','668','669',
--        '670','671','672','673','674','675','676'
--    )
--    AND OPE.COD_OPE NOT IN ('ECH', 'FAC')