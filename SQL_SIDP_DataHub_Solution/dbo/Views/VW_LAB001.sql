﻿CREATE VIEW [dbo].[VW_LAB001]
AS
SELECT
	CAST(FORMAT(GETDATE(), 'yyyyMMdd') AS VARCHAR(8)) AS [DT_EXT_ENR],
	CAST(ISNULL(SCLI.DWHCLICLI, '') AS VARCHAR(7)) AS [ID_SI_SCE],
	CAST('CLIENT' AS VARCHAR(30)) AS [CD_TYP_PRS],
	CAST(ISNULL(FORMAT(CLI.LastActivityDate, 'yyyyMMdd'), '') AS VARCHAR(8)) AS [DT_MAJ_INF_CLI],
	CAST('Y' AS CHAR(1)) AS [IC_CLI_BQE],
	CAST(UPPER(CLI.LastName) AS VARCHAR(32)) AS [NM_CLI],
	CAST(UPPER(IIF(CLI.MaidenName__pc = CLI.LastName, '', CLI.MaidenName__pc)) AS VARCHAR(32)) AS [NM_CLI_NAI],
	CAST(UPPER(CLI.FirstName) AS VARCHAR(32)) AS [PR_CLI],
	CAST(ISNULL(FORMAT(CLI.PersonBirthdate, 'yyyyMMdd'), '') AS VARCHAR(8)) AS [DT_NAI],
	CAST(ISNULL(CLI.BirthCountry__pc, '') AS VARCHAR(3)) AS [CD_PAY_NAI],
	CAST(ISNULL(CLI.Nationality__pc, '') AS VARCHAR(30)) AS [CD_PAY_NTE],
	CAST(ISNULL(CLI.FiscalCountry__c, '') AS VARCHAR(3)) AS [CD_PAY_FIS],
	CAST(ISNULL(FORMAT(CLI.DeathDate__pc, 'yyyyMMdd'), '') AS VARCHAR(8)) AS [DT_DCE],
	CAST(ISNULL(CLI.BillingStreet, '') AS VARCHAR(32)) AS [LB_TYP_VOI],
	CAST(ISNULL(CLI.AdditionToAddress__c, '') AS VARCHAR(32)) AS [LB_LDI],
	CAST(ISNULL(CLI.BillingPostalCode, '') AS VARCHAR(6)) AS [CD_PTL],
	CAST(ISNULL(CLI.BillingCity, '') AS VARCHAR(25)) AS [LB_VIL],
	CAST(ISNULL(CLI.BillingCountry, '') AS VARCHAR(25)) AS [RF_PAY],
	CAST(IIF(CLI.BankRating__c = 'I', 'Y', 'N') AS CHAR(1)) AS [CD_NPAI],
	CAST(ISNULL(FORMAT(CLI.CreatedDate, 'yyyyMMdd'), '') AS VARCHAR(8)) AS [DT_REL_BQE],
	CAST(ISNULL(SCLI.DWHCLISRN, '') AS VARCHAR(15)) AS [RF_SRN],
	CAST(ISNULL(CLI.NumSiret__c, '') AS VARCHAR(15)) AS [RF_SRT],
	CAST(ISNULL(CLI.Ownership, '') AS VARCHAR(4)) AS [CD_FRM_JUR_SAB],
	CAST(ISNULL(CLI.OccupationNiv1__pc, '') AS VARCHAR(4)) AS [CD_PCS],
	CAST(ISNULL(CLI.OccupationArea__c, '') AS VARCHAR(6)) AS [CD_APE],
	CAST(ISNULL(CLI.LegalCapacity__pc, '01') AS CHAR(2)) AS [CD_CAP_JUR],
	CAST(ISNULL(CLI.MaritalStatus__pc, '') AS CHAR(2)) AS [CD_SIT_FAM],
	CAST(FORMAT(ISNULL(CLI.TotalDisposableIncome__pc, 0), 'F2') AS VARCHAR(32)) AS [MT_RVN_MM_NET],
	CAST(ISNULL(FORMAT(CLI.LastModificationJustifiedIncomes__pc, 'yyyyMMdd'), '') AS VARCHAR(8)) AS [DT_ATU_RVN],
	CAST(FORMAT(ISNULL(SCLI.DWHCLICHF, 0), 'F2') AS VARCHAR(32))					AS	[MT_CA_BRU],
	CAST(ISNULL(FORMAT(SCLI.DWHCLIANF, 'yyyy'), '') AS VARCHAR(4))					AS	[RF_EXE_REF],
	CAST(ISNULL(CLI.BankRating__c, '') AS VARCHAR(3))								AS	[CD_COT_BQR],
	CAST(ISNULL(SCLI.DWHCLIETA, '') AS VARCHAR(4))									AS	[CD_ETA_SAB],
	CAST('001_GB_' + FORMAT(GETDATE(), 'yyyyMMdd') + '_S2S_CUSTOMER_BANK_DAILY.txt' AS VARCHAR(256)) AS [FileName],
	CAST(ISNULL(FORMAT(CLI.CreatedDate, 'yyyyMMdd'), '')			AS VARCHAR(8))	AS	[CreatedDate],
	CAST(ISNULL(FORMAT(CLI.LastModifiedDate, 'yyyyMMdd'), '')		AS VARCHAR(8))	AS	[LastModifiedDate],
	CAST(ISNULL(FORMAT(CLI.OBTerminationDate__c, 'yyyyMMdd'), '')	AS VARCHAR(8))	AS	[TerminatedDate]
FROM
    dbo.PV_SA_Q_CLIENT				AS	SCLI	WITH(NOLOCK)
	LEFT JOIN	dbo.PV_SF_ACCOUNT	AS	CLI		WITH(NOLOCK)
		ON	SCLI.DWHCLICLI = CLI.IDCustomerSAB__pc
WHERE
    1 = 1
	AND CLI.PrimaryNetwork__c != '01'
GO