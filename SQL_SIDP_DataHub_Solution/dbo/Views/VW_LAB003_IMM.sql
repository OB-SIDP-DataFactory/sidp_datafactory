﻿CREATE VIEW [dbo].[VW_LAB003_IMM]
AS
SELECT
	CAST(FORMAT(GETDATE(), 'yyyyMMdd') AS VARCHAR(8)) AS [DT_EXT_ENR],
	CAST('' AS VARCHAR(32))	AS	[ID_CPT],
	CAST('BANQUE' AS VARCHAR(6))	AS	[IC_UNI_ORG],
	CAST('' AS VARCHAR(50))	AS	[LB_CPT],
	CAST('' AS VARCHAR(7))	AS	[ID_SAB_CLI],
	CAST('' AS VARCHAR(100))	AS	[ID_GRC_CLI_JOI_1],
	CAST('' AS VARCHAR(100))	AS	[ID_GRC_CLI_JOI_2],
	CAST('' AS VARCHAR(100))	AS	[ID_GRC_CLI],
	CAST('' AS VARCHAR(4))	AS	[CD_TYP_CPT],
	CAST('' AS VARCHAR(3))	AS	[CD_DVS_CPT],
	CAST('' AS VARCHAR(3))	AS	[CD_DVS_PIV],
	CAST('' AS VARCHAR(10))	AS	[CD_OTO],
	CAST(FORMAT(ABS(0), 'F2') AS VARCHAR(32))						AS	[MT_OTO],
	CAST(FORMAT(ABS(0), 'F2') AS VARCHAR(32))						AS	[MT_CAP_RTT_DVS],
	CAST(FORMAT(ABS(0), 'F2') AS VARCHAR(32))						AS	[MT_CAP_RTT_EUR],
	CAST('' AS VARCHAR(3))	AS	[CD_STT],
	CAST('' AS VARCHAR(8))	AS	[DT_OUV],
	CAST('' AS VARCHAR(8))	AS	[DT_FER],
	CAST('' AS VARCHAR(8))	AS	[DT_DER_DBL_CRD],
	CAST('' AS VARCHAR(30))	AS	[CD_DTT_BQE],
	CAST('' AS VARCHAR(8))	AS	[DT_DER_ECH_PY],
	CAST(FORMAT(ABS(0), 'F2') AS VARCHAR(32))						AS	[MT_PE],
	CAST('' AS VARCHAR(3))	AS	[DR_CON],
	CAST('' AS VARCHAR(35))	AS	[RF_IBAN],
	CAST('' AS CHAR(1))	AS	[CD_BLC_BQE],
	CAST('' AS VARCHAR(32))	AS	[CD_RAI_BLC],
	CAST('' AS VARCHAR(8))	AS	[DT_MAJ_CPT],
	CAST('001_GB_' + FORMAT(GETDATE(), 'yyyyMMdd') + '_ACCOUNT_BANK_DAILY.txt' AS VARCHAR(256))	AS	[FileName],
	CAST(FORMAT(GETDATE(), 'yyyyMMdd') AS VARCHAR(8))	AS	[CreatedDate],
	CAST(FORMAT(GETDATE(), 'yyyyMMdd') AS VARCHAR(8))	AS	[LastModifiedDate],
	CAST(FORMAT(GETDATE(), 'yyyyMMdd') AS VARCHAR(8))	AS	[TerminatedDate]
GO

--SELECT DISTINCT
--    -- Date extraction
--    FORMAT(GETDATE(), 'yyyyMMdd')					AS	[DT_EXT_ENR],
--    -- Numéro de compte/prêt/portefeuille
--    --IMM.NUM_PRE +
--    '|' +
--    -- Unité organisationnelle
--    'BANQUE'	AS	[Unité organisationnelle],
--    -- Intitulé du compte/prêt/portefeuille
--    --'CREDIT' + ' ' + IMM.COD_PDT + 'I' +
--    '|' +
--    -- Code client (SAB collectif ou unique)
--    --CLI.COD_CLI +
--    '|' +
--    -- Code client 1 (GRC si client collectif)
--    --CASE
--    --WHEN      ISNULL(CLI.DWHCLITIE, '') <> ''
--    --and  CLIb.COD_REF IN ('SI','SF','AC')
--    --AND  CLI.DWHCLIETA <> 'INDI'
--    --THEN CLIb.CLIREFREF
--    --END +
--    '|' +
--    -- Code client 2 (GRC si client collectif)
--    --CASE
--    --WHEN      ISNULL(CLI.DWHCLITIE, '') <> ''
--    --AND  CLI.DWHCLIETA <> 'INDI'
--    --THEN (
--    --SELECT
--    --MIN(CLIbis.CLIREFREF)
--    --from
--    --TMP_STK_PRE_IMM_CLI cli2  WITH(NOLOCK)
--    --INNER JOIN SIDP_DataHub.dbo.PV_SA_Q_CLIENT CLIbis WITH(NOLOCK)
--    --ON    CLIbis.COD_CLI=cli2.COD_CLI
--    --WHERE
--    --1 = 1
--    --AND  CLIbis.COD_REF IN ('SI','SF','AC')
--    --AND  CLIbis.tie_ref is null
--    --and  cli2.COD_CLI <> CLIb.cod_cli
--    --and  cli2.num_pre = CLI.NUM_PRE
--    --)
--    --END +
--    '|' +
--    -- Code client (GRC si client unique)
--    --CASE
--    --WHEN      ISNULL(CLI.DWHCLITIE, '') = ''
--    --AND  CLI.COD_REF IN ('SI','SF','AC')
--    --AND  CLI.CLIREFREF NOT LIKE 'I%'
--    --AND  CLI.CLIREFREF NOT LIKE 'T%'
--    --THEN decode(CLI.COD_REF, 'SF', 'SF') + CLI.CLIREFREF
--    --END +
--    '|' +
--    -- Code type de compte/prêt/portefeuille (code produit)
--    --IMM.COD_PDT + 'I' +
--    '|' +
--    -- Devise
--    --IMM.DEV +
--    '|' +
--    -- Devise pivot
--    '|'+
--    -- Code autorisation
--    '|'+
--    -- Montant autorisation
--    '|'+
--    -- Montant capital restant dû (devise)
--    --FORMAT(IMM.CAP_RES_DU, 'F2') +
--    '|' +
--    -- Montant capital restant dû (euro)
--    --CASE
--    --WHEN      IMM.DEV = 'EUR'
--    --THEN FORMAT(IMM.CAP_RES_DU, 'F2')
--    --END +
--    '|' +
--    -- Statut
--    --CASE
--    --WHEN      IMM.COD_ETA_PRE IN ('E', 'S')
--    --THEN 'S'
--    --ELSE      'V'
--    --END +
--    '|' +
--    -- Code Eata du prêt (E: Eteint / S: Eteint Soldé / V: Vivant dans une chaîne de gestion)
--    --ISNULL(FORMAT(IMM.DTE_DEP_PRE,'yyyyMMdd'),'99990101') +'|'+-- Date ouverture
--    -- Date clôture
--    --CASE
--    --WHEN      IMM.COD_ETA_PRE = 'S'
--    --THEN FORMAT(IMM.DTE_DER_ECH, 'yyyyMMdd')
--    --END +
--    '|' +
--    -- Date de dernier déblocage crédit
--    '|' +
--    -- Canal de détection Banque
--    '|' +
--    -- Date de dernière échéance payée
--    --FORMAT(IMM.DTE_CAP_RES_DU, 'yyyyMMdd') +
--    '|' +
--    -- Montant du prêt
--    --FORMAT(IMM.CAP_EMP, 'F2') +
--    '|' +
--    -- Durée du contrat
--    --IMM.DUR +
--    '|' +
--    -- IBAN
--    '|'+
--    -- Code blocage compte
--    '|'+
--    -- Raison blocage
--    '|'+
--    -- Date de mise à jour du compte/prêt/portefeuille
--    --FORMAT(IMM.DTE_CAP_RES_DU ,'yyyyMMdd') +
--    '|'    AS EXTRACT
----FROM
--    --TMP_STK_PRE_IMM                         IMM   WITH(NOLOCK)
--    --INNER JOIN     TMP_STK_PRE_IMM_CLI      CLI   WITH(NOLOCK)
--    --ON   CLI.NUM_PRE = IMM.NUM_PRE
--    --INNER JOIN     SIDP_DataHub.dbo.PV_SA_Q_CLIENT            CLI WITH(NOLOCK)
--    --ON   CLI.COD_CLI = CLI.COD_CLI
--    --INNER JOIN     SIDP_DataHub.dbo.PV_SA_Q_CLIENT            CLIB    WITH(NOLOCK)
--    --ON   CLIB.COD_CLI = ISNULL(CLI.DWHCLITIE, CLI.cod_cli)
----WHERE
--    --1 = 1
--    --AND CLI.DWHCLIAGE NOT IN ('51', '52')
--    --AND CLI.DWHCLIRES   NOT IN
--    --(
--    --'C01','C02','C03',
--    --'D01','D05',
--    --'T01','T02','T03',
--    --'650','651','652','653','654','655','656','657','658','659',
--    --'660','661','662','663','664','665','666','667','668','669',
--    --'670','671','672','673','674','675','676'
--    --)
--    --AND  CLI.TOP_EMP = 'O'
--    --AND CLI.DTE_FIN_REL IS NULL