﻿
CREATE VIEW [dbo].[VW_PV_SF_OPPORTUNITY] AS
select   sf_opportunity.Id
		,sf_opportunity.Id_SF
		,sf_opportunity.AccountId
		,sf_opportunity.AdvisorCode__c
		,sf_opportunity.CampaignId__c
		,sf_opportunity.CloseDate
		,sf_opportunity.CommercialOfferCode__c
		,sf_opportunity.CommercialOfferName__c
		,sf_opportunity.CompleteFileFlag__c
		, ref_CompleteFileFlag__c.LIBELLE_SF as CompleteFileFlag__c_label
		,sf_opportunity.CreatedById 
		,sf_opportunity.CreatedDate
		,sf_opportunity.DeniedOpportunityReason__c
		,ref_DeniedOpportunityReason__c.LIBELLE_SF as DeniedOpportunityReason__c_label
		,sf_opportunity.DistributionChannel__c
		,ref_DistributionChannel__c.LIBELLE_SF as DistributionChannel__c_label
		,sf_opportunity.DistributorEntity__c
		,ref_DistributorEntity__c.LIBELLE_SF as DistributorEntity__c_label
		,sf_opportunity.DistributorNetwork__c
		,ref_DistributorNetwork__c.LIBELLE_SF as DistributorNetwork__c_label
		,sf_opportunity.FinalizedBy__c
		,sf_opportunity.FormValidation__c
		,sf_opportunity.IDOppty__c
		,sf_opportunity.IDProcessSous__c
		,sf_opportunity.IdSource__c
		,sf_opportunity.Indication__c
		,sf_opportunity.LastModifiedDate
		,sf_opportunity.LeadSource
		,ref_LeadSource.LIBELLE_SF as LeadSource_label
		,sf_opportunity.Manager__c
		,sf_opportunity.NoIndication__c
		,sf_opportunity.OwnerId
		,sf_opportunity.PointOfSaleCode__c
		,sf_opportunity.ProductFamilyCode__c
		,sf_opportunity.ProductFamilyName__c
		,sf_opportunity.ProvenanceIndicationIndicatorId__c
		,sf_opportunity.RecordTypeId
		,ref_RecordTypeId.CODE_SF as RecordTypeCode
        ,ref_RecordTypeId.NAME_SF as RecordTypeName
		,sf_opportunity.RejectReason__c
		,ref_RejectReason__c.LIBELLE_SF as RejectReason__c_label
		,sf_opportunity.RelationEntryScore__c
		,ref_RelationEntryScore__c.LIBELLE_SF as RelationEntryScore__c_label
		,sf_opportunity.StageName
		,ref_StageName.LIBELLE_SF as StageName_label
		,sf_opportunity.StartedChannel__c
		,ref_StartedChannel__c.LIBELLE_SF as StartedChannel__c_label
		,sf_opportunity.SystemModstamp
		,sf_opportunity.Type
		,ref_Type.LIBELLE_SF as Type_label
		,sf_opportunity.IsWon
		,sf_opportunity.Name
		,sf_opportunity.realizedBy__c
		,sf_opportunity.SignatureDate__c
		,sf_opportunity.StartedChannelForIndication__c
		,sf_opportunity.ToBeDeleted__c
		,sf_opportunity.Amount
		,sf_opportunity.Fiscal
		,sf_opportunity.FiscalQuarter
		,sf_opportunity.FiscalYear
		,sf_opportunity.FolderAlias__c
		,sf_opportunity.ForecastCategory
		,sf_opportunity.ForecastCategoryName
		,sf_opportunity.HasOpenActivity
		,sf_opportunity.HasOpportunityLineItem
		,sf_opportunity.HasOverdueTask
		,sf_opportunity.IsClosed
		,sf_opportunity.IsDeleted
		,sf_opportunity.LastActivityDate
		,sf_opportunity.LastModifiedById
		,sf_opportunity.LastReferencedDate
		,sf_opportunity.LastViewedDate
		,sf_opportunity.NextStep
		,sf_opportunity.OriginalEvent__c
        ,ref_OriginalEvent__c.LIBELLE_SF as OriginalEvent__c_label -----
		,sf_opportunity.SendingInformations__c
		,sf_opportunity.TCHExpiredIndication__c
		,sf_opportunity.TCHExpiredSubscription__c
		,sf_opportunity.TCHManualAnalysis__c
		,sf_opportunity.TCHOppCreation__c
		,sf_opportunity.IsPrivate
		,sf_opportunity.Probability
		,sf_opportunity.ExpectedRevenue
		,sf_opportunity.TotalOpportunityQuantity
		,sf_opportunity.CampaignId
		,sf_opportunity.Pricebook2Id
		,sf_opportunity.ContractId
		,sf_opportunity.TCHCompleteFile__c
		,sf_opportunity.TCHFromMarketingCase__c
		,sf_opportunity.TCHNotifDuplicateProspect__c
		,sf_opportunity.RecoveryLink__c
		,sf_opportunity.TCHId__c
		,sf_opportunity.ContractNumberDistrib__c
		,sf_opportunity.OptInTelcoData__c
		,sf_opportunity.SubscriberHolder__c
		,sf_opportunity.CreditAmount__c
		,sf_opportunity.CustomerAdviserId__c
		,sf_opportunity.DebitDay__c
		,sf_opportunity.DepositAmount__c
		,sf_opportunity.Derogation__c
		,sf_opportunity.FFinalDecision__c
		,sf_opportunity.FPreScoreColor__c
		,sf_opportunity.FirstDueDate__c
		,sf_opportunity.ForbiddenWords__c
		,sf_opportunity.FundingSubject__c
		,sf_opportunity.HasForbiddenWord__c
		,sf_opportunity.IncompleteFileReason__c
		,sf_opportunity.LoanAcceptationDate__c
		,sf_opportunity.ObjectInApproval__c
		,sf_opportunity.OffersRate__c
		,sf_opportunity.PrescriberContact__c
		,sf_opportunity.ReleaseAmount__c
		,sf_opportunity.ReleaseDate__c
		,sf_opportunity.StartDate__c
		,sf_opportunity.SubscribedAmount__c
		,sf_opportunity.TCHCategory__c
		--,sf_opportunity.TracingProductFamily__c
		,sf_opportunity.TracingProduct__c
		,sf_opportunity.RetractationDate__c		
		,sf_opportunity.OriginDistributionChannel__c -----
		,ref_OriginDistributionChannel__c.LIBELLE_SF as OriginDistributionChannel__c_label	-----
        ,sf_opportunity.isSynchronized__c
		  ,sf_opportunity.ApprovalInitiator__c
		  ,sf_opportunity.CheckGo__c
		  ,sf_opportunity.CommitmentRefusalReason__c
		  ,sf_opportunity.DebtRate__c
		  ,sf_opportunity.Description		
		  ,sf_opportunity.EndingChannel__c
		  ,sf_opportunity.IsClientOF__c
		  ,sf_opportunity.ManagerApproval__c
		  ,sf_opportunity.OptInOrderTelco__c
		  ,sf_opportunity.PreScoreCode__c
		  ,sf_opportunity.PreScoreReason__c
		  ,sf_opportunity.RTDN__c
		  ,sf_opportunity.TCH_PTD__c
		  ,sf_opportunity.TCHFinalDecision__c		
		  ,sf_opportunity.TCHPreScoreColor__c
		  -- JIRA 315 SF-R6
		  ,sf_opportunity.Commentthread__c
		  ,sf_opportunity.coBorrowerInsurance__c
		  ,sf_opportunity.mainBorrowerInsurance__c
          ,sf_opportunity.TCH_OAV_Granted__c	
		  ,sf_opportunity.CampaignCode__c
		  , sf_opportunity.CreationLeadDate__c
		  , sf_opportunity.DateOfFirstVisit__c
		  , sf_opportunity.EventCode__c
		  , sf_opportunity.LeadChannel__c
		  , sf_opportunity.LeadID__c
		 , sf_opportunity.LeadProductFamily__c
		 , sf_opportunity.NumberOfVisits__c
		 , sf_opportunity.OldLead__c
		 , sf_opportunity.TCHInitDistribChannel__c	
		 , sf_opportunity.RecoveryLinkRIO__c
		 , sf_opportunity.OptinPreattTelco__c
		,sf_opportunity.Validity_StartDate
		,sf_opportunity.Validity_EndDate
  from dbo.PV_SF_OPPORTUNITY sf_opportunity
  left join dbo.RAW_SF_REFERENTIEL as ref_CompleteFileFlag__c on ref_CompleteFileFlag__c.OBJECT_CODE_SF = 'opportunity' and ref_CompleteFileFlag__c.FIELD_CODE_SF = 'CompleteFileFlag__c' and case when sf_opportunity.CompleteFileFlag__c ='true' then '1' else '0' end = ref_CompleteFileFlag__c.CODE_SF
  left join ( select distinct CODE_SF, LIBELLE_SF from dbo.RAW_SF_REFERENTIEL where OBJECT_CODE_SF = 'opportunity' and FIELD_CODE_SF = 'DeniedOpportunityReason__c' ) as ref_DeniedOpportunityReason__c on sf_opportunity.DeniedOpportunityReason__c = ref_DeniedOpportunityReason__c.CODE_SF
  left join ( select distinct CODE_SF, LIBELLE_SF from dbo.RAW_SF_REFERENTIEL where OBJECT_CODE_SF = 'opportunity' and FIELD_CODE_SF = 'DistributionChannel__c' ) as ref_DistributionChannel__c on sf_opportunity.DistributionChannel__c = ref_DistributionChannel__c.CODE_SF
  left join ( select distinct CODE_SF, LIBELLE_SF from dbo.RAW_SF_REFERENTIEL where OBJECT_CODE_SF = 'opportunity' and FIELD_CODE_SF = 'DistributorEntity__c' ) as ref_DistributorEntity__c on sf_opportunity.DistributorEntity__c = ref_DistributorEntity__c.CODE_SF
  left join ( select distinct CODE_SF, LIBELLE_SF from dbo.RAW_SF_REFERENTIEL where OBJECT_CODE_SF = 'opportunity' and FIELD_CODE_SF = 'DistributorNetwork__c' ) as ref_DistributorNetwork__c on sf_opportunity.DistributorNetwork__c = ref_DistributorNetwork__c.CODE_SF
  left join ( select distinct CODE_SF, LIBELLE_SF from dbo.RAW_SF_REFERENTIEL where OBJECT_CODE_SF = 'opportunity' and FIELD_CODE_SF = 'LeadSource' ) as ref_LeadSource on sf_opportunity.LeadSource = ref_LeadSource.CODE_SF
  left join ( select distinct CODE_SF, LIBELLE_SF from dbo.RAW_SF_REFERENTIEL where OBJECT_CODE_SF = 'opportunity' and FIELD_CODE_SF = 'RejectReason__c' ) as ref_RejectReason__c on sf_opportunity.RejectReason__c = ref_RejectReason__c.CODE_SF
  left join ( select distinct CODE_SF, LIBELLE_SF from dbo.RAW_SF_REFERENTIEL where OBJECT_CODE_SF = 'opportunity' and FIELD_CODE_SF = 'RelationEntryScore__c' ) as ref_RelationEntryScore__c on sf_opportunity.RelationEntryScore__c = ref_RelationEntryScore__c.CODE_SF
  left join ( select distinct CODE_SF, LIBELLE_SF from dbo.RAW_SF_REFERENTIEL where OBJECT_CODE_SF = 'opportunity' and FIELD_CODE_SF = 'StageName' ) as ref_StageName on sf_opportunity.StageName = ref_StageName.CODE_SF
  left join ( select distinct CODE_SF, LIBELLE_SF from dbo.RAW_SF_REFERENTIEL where OBJECT_CODE_SF = 'opportunity' and FIELD_CODE_SF = 'StartedChannel__c' ) as ref_StartedChannel__c on sf_opportunity.StartedChannel__c = ref_StartedChannel__c.CODE_SF
  left join ( select distinct CODE_SF, LIBELLE_SF from dbo.RAW_SF_REFERENTIEL where OBJECT_CODE_SF = 'opportunity' and FIELD_CODE_SF = 'Type' ) as ref_Type on sf_opportunity.Type = ref_Type.CODE_SF
  left join ( select distinct CODE_SF, LIBELLE_SF from dbo.RAW_SF_REFERENTIEL where OBJECT_CODE_SF = 'opportunity' and FIELD_CODE_SF = 'OriginalEvent__c' ) as ref_OriginalEvent__c on sf_opportunity.OriginalEvent__c = ref_OriginalEvent__c.CODE_SF
  left join ( select distinct CODE_SF, LIBELLE_SF from dbo.RAW_SF_REFERENTIEL where OBJECT_CODE_SF = 'opportunity' and FIELD_CODE_SF = 'DistributionChannel__c' ) as ref_OriginDistributionChannel__c on sf_opportunity.OriginDistributionChannel__c = ref_OriginDistributionChannel__c.CODE_SF
  left join dbo.REF_RECORDTYPE as ref_RecordTypeId on ref_RecordTypeId.OBJECTTYPE_SF = 'Opportunity' and ref_RecordTypeId.ID_SF = sf_opportunity.RecordTypeId
