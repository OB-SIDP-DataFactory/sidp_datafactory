﻿CREATE VIEW [dbo].[VW_DATE_MAJ_VUES] AS
SELECT DISTINCT FileOrTableName AS Vue, ProcessingEndDate AS DateTraitement, ProcessingStatus AS StatutTraitement
FROM            Data_Log
WHERE        (FileOrTableName IN ('PV_SF_PROFILE', 'PV_SF_LIVECHATTRANSCRIPT', 'PV_SF_USER', 'PV_SF_ACCOUNT', 'PV_SF_TASK', 'PV_SF_OPPORTUNITY', 'PV_SF_EMAILMESSAGE', 'PV_SF_OPPORTUNITY_History', 
                         'PV_SA_Q_CLIENT', 'PV_SA_Q_CARTE', 'PV_SA_Q_COMPTE', 'PV_SA_Q_LKCPTCLI', 'PV_SA_Q_MOUVEMENT', 'PV_FE_COMMERCIALOFFER', 'PV_FE_COMMERCIALPRODUCT', 'PV_FE_ENROLMENTFOLDER', 
                         'PV_FE_EQUIPMENT', 'PV_FE_EQUIPMENTSVCASSOC', 'PV_FE_SBDSERVICESASSOC', 'PV_FE_SERVICEPRODUCT', 'PV_FE_SUBSCRIBEDPRODUCT', 'PV_FE_SVCPRODUCTASSOC', 'sas_vue_cli', 
                         'sas_vue_ferm_cpt', 'sas_vue_flux_opport_fin', 'sas_vue_stock_opport', 'sas_vue_synth_prod', 'sas_vue_usage_cpt', 'SAS_Vue_RC_Cases', 'SAS_Vue_RC_Stock_Cases', 'SAS_Vue_RC_Interactions', 
                         'SAS_Vue_RC_Ventes')) AND (ProcessingEndDate > CONVERT(DATE, GETDATE()));

