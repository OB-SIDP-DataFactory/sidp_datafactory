﻿CREATE VIEW [dbo].[VW_PV_SF_CASE]
AS
SELECT
	C.Id,
	C.Id_SF,
	C.MasterRecordId,
	C.AccountId,
	C.ActionPlan__c,
	ref_ActionPlan__c.LIBELLE_SF	AS	ActionPlan__c_label,
	C.ARDate__c,
	C.CaseNumber,
	C.ClosedDate,
	C.CommercialGestAmount__c,
	C.CreatedById,
	C.CreatedDate,
	C.CustomerType__c,
	ref_CustomerType__c.LIBELLE_SF	AS	CustomerType__c_label,
	C.Description,
	C.DUT__c,
	C.Equipment__c,
	C.ExpiredMilestone__c,
	C.FirstClaim__c,
	ref_FirstClaim__c.LIBELLE_SF	AS	FirstClaim__c_label,
	C.Habilitation__c,
	C.Improvement__c,
	C.Indemnity__c,
	ref_Indemnity__c.LIBELLE_SF	AS	Indemnity__c_label,
	C.InitialCase__c,
	C.isDoneWithExpMilestone__c,
	C.IsDoneWithRespMilestone__c,
	C.IsDone__c,
	C.isNotDoneExpMilestone__c,
	C.IsNotDoneWithRespMilestone__c,
	C.IsNotDone__c,
	C.LastModifiedById,
	C.LastModifiedDate,
	C.LetterWaitingDate__c,
	C.LevelClaim__c,
	ref_LevelClaim__c.LIBELLE_SF	AS	LevelClaim__c_label,
	C.LostAmount__c,
	C.Metier__c,
	ref_Metier__c.LIBELLE_SF	AS	Metier__c_label,
	C.MilestoneStatus,
	C.Network__c,
	ref_Network__c.LIBELLE_SF	AS	Network__c_label,
	C.Origin,
	ref_Origin.LIBELLE_SF	AS	Origin_label,
	C.Origin__c,
	ref_Origin__c.LIBELLE_SF	AS	Origin__c_label,
	C.OwnerId,
	C.ParentId,
	C.ParentOppy__c,
	C.PrimaryQualification__c,
	ref_PrimaryQualification__c.LIBELLE_SF	AS	PrimaryQualification__c_label,
	C.PrimarySubject__c,
	ref_PrimarySubject__c.LIBELLE_SF	AS	PrimarySubject__c_label,
	C.Priority,
	ref_Priority.LIBELLE_SF	AS	Priority_label,
	C.Process__c,
	ref_Process__c.LIBELLE_SF	AS	Process__c_label,
	C.queueName__c,
	C.Reason,
	ref_Reason.LIBELLE_SF	AS	Reason_label,
	C.RecordTypeId,
	ref_RecordTypeId.CODE_SF	AS	RecordTypeCode,
	ref_RecordTypeId.NAME_SF	AS	RecordTypeName,
	C.ResponseChannel__c,
	ref_ResponseChannel__c.LIBELLE_SF	AS	ResponseChannel__c_label,
	C.ResponseDate__c,
	C.RetrocessionAmount__c,
	C.SecondarySubject__c,
	ref_SecondarySubject__c.LIBELLE_SF	AS	SecondarySubject__c_label,
	C.SecondQualification__c,
	ref_SecondQualification__c.LIBELLE_SF	AS	SecondQualification__c_label,
	C.ShopSeller__c,
	C.StandBy__c,
	ref_StandBy__c.LIBELLE_SF	AS	StandBy__c_label,
	C.Status,
	ref_Status.LIBELLE_SF	AS	Status_label,
	C.SubType__c,
	ref_SubType__c.LIBELLE_SF	AS	SubType__c_label,
	C.TechCloseCase__c,
	C.Tech_PreviousOwner__c,
	C.Tech_ReviewDateExpired__c,
	C.TreatementReason__c,
	ref_TreatementReason__c.LIBELLE_SF	AS	TreatementReason__c_label,
	C.Type,
	ref_Type.LIBELLE_SF	AS	Type_label,
	C.IDCampagne__c,
	C.CallStatus__c,
	ref_CallStatus__c.LIBELLE_SF	AS	CallStatus__c_label,
	C.CallDetail__c,
	ref_CallDetail__c.LIBELLE_SF	AS	CallDetail__c_label,
	C.OBShop__c,
	C.Typology__c,
	C.IsDeleted,
	C.ContactId,
	C.AssetId,
	C.ProductId,
	C.EntitlementId,
	C.QuestionId,
	C.SourceId,
	C.CommunityId,
	C.BusinessHoursId,
	C.SuppliedName,
	C.SuppliedEmail,
	C.SuppliedPhone,
	C.SuppliedCompany,
	C.IsVisibleInSelfService,
	C.Subject,
	C.IsClosed,
	C.IsEscalated,
	C.HasCommentsUnreadByOwner,
	C.HasSelfServiceComments,
	C.IsClosedOnCreate,
	C.IsSelfServiceClosed,
	C.SlaStartDate,
	C.SlaExitDate,
	C.IsStopped,
	C.StopStartDate,
	C.SystemModstamp,
	C.ContactPhone,
	C.ContactMobile,
	C.ContactEmail,
	C.ContactFax,
	C.Comments,
	C.LastViewedDate,
	C.LastReferencedDate,
	C.CreatorFullPhotoUrl,
	C.CreatorSmallPhotoUrl,
	C.CreatorName,
	C.ManualAuthent__c,
	C.Comments255__c,
	C.NextSLAStartDate__c,
	C.PreventAssign__c,
	C.ReviewDate__c,
	C.ReviewSubject__c,
	C.EighteenCharId__c,
	C.TCHNbMilestonesClosed__c,
	C.isOwnerQueue__c,
	C.Amount__c,
	C.AuthenticationResult__c,
	C.Answer__c,
	C.Detail__c,
	C.DocumentType__c,
	C.Duration__c,
	C.Dysfunction__c,
	C.ManagerialAct__c,
	C.Confidencelevel__c,
	C.PaymentCeiling__c,
	C.WithdrawalLimit__c,
	C.ForbiddenWords__c,
	C.ClientVisibility__c,
	C.TCHTreatmentTime__c,
	C.CancelConfiramtion__c,
	C.ClaimTreatementTime__c,
	C.PJPresence__c,
	C.HasForbiddenWord__c,
	C.TCHAuthent__c,
	C.realizedBy__c,
	C.MessageCounter__c,
	C.NewMessage__c,
	C.PlanifiedDate__c,
	C.TCHCaseAttributionOwnerDate__c,
	C.TCHCaseTimeProcessing__c,
	C.TCHIdCase__c,
	C.TCHPurge__c,
	C.TCHTriggerPass__c,
	C.TreatmentDate__c,
	C.Details__c,
	C.EFrontNumber__c,
	C.Specification__c,
	C.SubUniverse__c,
	C.Universe__c,
	C.IncidentGravity__c,
	C.ConseillerTechnique__c,
	C.EmailSource__c,
	C.Manager__c		,
	C.ExpectedCorrectionDeadline__c,
	C.Suivi_Evolution_du_Statut__c,
	C.Relance_client__c,
	C.Decision__c,
	C.ReasonAnomalyTA3C__c,
	C.Validation_PJ__c,
	C.Perimeter__c,
	C.TCHOppIdProcessSous__c,
	C.TCHOptDistributorNetwork__c,
	C.Incident_en_Cours__c,
	C.IsClaimHavingApprovalProcess__c,
	C.ScoringCardPaymentCeiling__c,
	C.Validity_StartDate,
	C.Validity_EndDate,
	C.Startdt,
	C.Enddt
FROM
	(
		SELECT
			SC.[Id],
			SC.[Id_SF],
			SC.[MasterRecordId],
			SC.[AccountId],
			SC.[ActionPlan__c],
			SC.[ARDate__c],
			SC.[CaseNumber],
			SC.[ClosedDate],
			SC.[CommercialGestAmount__c],
			SC.[CreatedById],
			SC.[CreatedDate],
			SC.[CustomerType__c],
			SC.[Description],
			SC.[DUT__c],
			SC.[Equipment__c],
			SC.[ExpiredMilestone__c],
			SC.[FirstClaim__c],
			SC.[Habilitation__c],
			SC.[Improvement__c],
			SC.[Indemnity__c],
			SC.[InitialCase__c],
			SC.[isDoneWithExpMilestone__c],
			SC.[IsDoneWithRespMilestone__c],
			SC.[IsDone__c],
			SC.[isNotDoneExpMilestone__c],
			SC.[IsNotDoneWithRespMilestone__c],
			SC.[IsNotDone__c],
			SC.[LastModifiedById],
			SC.[LastModifiedDate],
			SC.[LetterWaitingDate__c],
			SC.[LevelClaim__c],
			SC.[LostAmount__c],
			SC.[Metier__c],
			SC.[MilestoneStatus],
			SC.[Network__c],
			SC.[Origin],
			SC.[Origin__c],
			SC.[OwnerId],
			SC.[ParentId],
			SC.[ParentOppy__c],
			SC.[PrimaryQualification__c],
			SC.[PrimarySubject__c],
			SC.[Priority],
			SC.[Process__c],
			SC.[queueName__c],
			SC.[Reason],
			SC.[RecordTypeId],
			SC.[ResponseChannel__c],
			SC.[ResponseDate__c],
			SC.[RetrocessionAmount__c],
			SC.[SecondarySubject__c],
			SC.[SecondQualification__c],
			SC.[ShopSeller__c],
			SC.[StandBy__c],
			SC.[Status],
			SC.[SubType__c],
			SC.[TechCloseCase__c],
			SC.[Tech_PreviousOwner__c],
			SC.[Tech_ReviewDateExpired__c],
			SC.[TreatementReason__c],
			SC.[Type],
			SC.[IDCampagne__c],
			SC.[CallStatus__c],
			SC.[CallDetail__c],
			SC.[OBShop__c],
			SC.[Typology__c],
			SC.[IsDeleted],
			SC.[ContactId],
			SC.[AssetId],
			SC.[ProductId],
			SC.[EntitlementId],
			SC.[QuestionId],
			SC.[SourceId],
			SC.[CommunityId],
			SC.[BusinessHoursId],
			SC.[SuppliedName],
			SC.[SuppliedEmail],
			SC.[SuppliedPhone],
			SC.[SuppliedCompany],
			SC.[IsVisibleInSelfService],
			SC.[Subject],
			SC.[IsClosed],
			SC.[IsEscalated],
			SC.[HasCommentsUnreadByOwner],
			SC.[HasSelfServiceComments],
			SC.[IsClosedOnCreate],
			SC.[IsSelfServiceClosed],
			SC.[SlaStartDate],
			SC.[SlaExitDate],
			SC.[IsStopped],
			SC.[StopStartDate],
			SC.[SystemModstamp],
			SC.[ContactPhone],
			SC.[ContactMobile],
			SC.[ContactEmail],
			SC.[ContactFax],
			SC.[Comments],
			SC.[LastViewedDate],
			SC.[LastReferencedDate],
			SC.[CreatorFullPhotoUrl],
			SC.[CreatorSmallPhotoUrl],
			SC.[CreatorName],
			SC.[ManualAuthent__c],
			SC.[Comments255__c],
			SC.[NextSLAStartDate__c],
			SC.[PreventAssign__c],
			SC.[ReviewDate__c],
			SC.[ReviewSubject__c],
			SC.[EighteenCharId__c],
			SC.[TCHNbMilestonesClosed__c],
			SC.[isOwnerQueue__c],
			SC.[Amount__c],
			SC.[AuthenticationResult__c],
			SC.[Answer__c],
			SC.[Detail__c],
			SC.[DocumentType__c],
			SC.[Duration__c],
			SC.[Dysfunction__c],
			SC.[ManagerialAct__c],
			SC.[Confidencelevel__c],
			SC.[PaymentCeiling__c],
			SC.[WithdrawalLimit__c],
			SC.[ForbiddenWords__c],
			SC.[ClientVisibility__c],
			SC.[TCHTreatmentTime__c],
			SC.[CancelConfiramtion__c],
			SC.[ClaimTreatementTime__c],
			SC.[PJPresence__c],
			SC.[HasForbiddenWord__c],
			SC.[TCHAuthent__c],
			SC.[realizedBy__c],
			SC.[MessageCounter__c],
			SC.[NewMessage__c],
			SC.[PlanifiedDate__c],
			SC.[TCHCaseAttributionOwnerDate__c],
			SC.[TCHCaseTimeProcessing__c],
			SC.[TCHIdCase__c],
			SC.[TCHPurge__c],
			SC.[TCHTriggerPass__c],
			SC.[TreatmentDate__c],
			SC.[Details__c],
			SC.[EFrontNumber__c],
			SC.[Specification__c],
			SC.[SubUniverse__c],
			SC.[Universe__c],
			SC.[IncidentGravity__c],
			SC.[ConseillerTechnique__c],
			SC.[EmailSource__c],
			SC.[Manager__c],
			SC.[Decision__c],
			SC.[ExpectedCorrectionDeadline__c],
			SC.[ReasonAnomalyTA3C__c],
			SC.[Suivi_Evolution_du_Statut__c],
			SC.[Relance_client__c],
			SC.[Validation_PJ__c],
			SC.[Perimeter__c],
			SC.[TCHOppIdProcessSous__c],
			SC.[TCHOptDistributorNetwork__c],
			SC.[Incident_en_Cours__c],
			SC.[IsClaimHavingApprovalProcess__c],
			SC.[ScoringCardPaymentCeiling__c],
			SC.[Validity_StartDate],
			SC.[Validity_EndDate],
			SC.[Startdt],
			SC.[Enddt]
		FROM
			dbo.PV_SF_CASE	AS	SC	WITH(NOLOCK)
		UNION ALL
		SELECT
			SCH.[Id],
			SCH.[Id_SF],
			SCH.[MasterRecordId],
			SCH.[AccountId],
			SCH.[ActionPlan__c],
			SCH.[ARDate__c],
			SCH.[CaseNumber],
			SCH.[ClosedDate],
			SCH.[CommercialGestAmount__c],
			SCH.[CreatedById],
			SCH.[CreatedDate],
			SCH.[CustomerType__c],
			SCH.[Description],
			SCH.[DUT__c],
			SCH.[Equipment__c],
			SCH.[ExpiredMilestone__c],
			SCH.[FirstClaim__c],
			SCH.[Habilitation__c],
			SCH.[Improvement__c],
			SCH.[Indemnity__c],
			SCH.[InitialCase__c],
			SCH.[isDoneWithExpMilestone__c],
			SCH.[IsDoneWithRespMilestone__c],
			SCH.[IsDone__c],
			SCH.[isNotDoneExpMilestone__c],
			SCH.[IsNotDoneWithRespMilestone__c],
			SCH.[IsNotDone__c],
			SCH.[LastModifiedById],
			SCH.[LastModifiedDate],
			SCH.[LetterWaitingDate__c],
			SCH.[LevelClaim__c],
			SCH.[LostAmount__c],
			SCH.[Metier__c],
			SCH.[MilestoneStatus],
			SCH.[Network__c],
			SCH.[Origin],
			SCH.[Origin__c],
			SCH.[OwnerId],
			SCH.[ParentId],
			SCH.[ParentOppy__c],
			SCH.[PrimaryQualification__c],
			SCH.[PrimarySubject__c],
			SCH.[Priority],
			SCH.[Process__c],
			SCH.[queueName__c],
			SCH.[Reason],
			SCH.[RecordTypeId],
			SCH.[ResponseChannel__c],
			SCH.[ResponseDate__c],
			SCH.[RetrocessionAmount__c],
			SCH.[SecondarySubject__c],
			SCH.[SecondQualification__c],
			SCH.[ShopSeller__c],
			SCH.[StandBy__c],
			SCH.[Status],
			SCH.[SubType__c],
			SCH.[TechCloseCase__c],
			SCH.[Tech_PreviousOwner__c],
			SCH.[Tech_ReviewDateExpired__c],
			SCH.[TreatementReason__c],
			SCH.[Type],
			SCH.[IDCampagne__c],
			SCH.[CallStatus__c],
			SCH.[CallDetail__c],
			SCH.[OBShop__c],
			SCH.[Typology__c],
			SCH.[IsDeleted],
			SCH.[ContactId],
			SCH.[AssetId],
			SCH.[ProductId],
			SCH.[EntitlementId],
			SCH.[QuestionId],
			SCH.[SourceId],
			SCH.[CommunityId],
			SCH.[BusinessHoursId],
			SCH.[SuppliedName],
			SCH.[SuppliedEmail],
			SCH.[SuppliedPhone],
			SCH.[SuppliedCompany],
			SCH.[IsVisibleInSelfService],
			SCH.[Subject],
			SCH.[IsClosed],
			SCH.[IsEscalated],
			SCH.[HasCommentsUnreadByOwner],
			SCH.[HasSelfServiceComments],
			SCH.[IsClosedOnCreate],
			SCH.[IsSelfServiceClosed],
			SCH.[SlaStartDate],
			SCH.[SlaExitDate],
			SCH.[IsStopped],
			SCH.[StopStartDate],
			SCH.[SystemModstamp],
			SCH.[ContactPhone],
			SCH.[ContactMobile],
			SCH.[ContactEmail],
			SCH.[ContactFax],
			SCH.[Comments],
			SCH.[LastViewedDate],
			SCH.[LastReferencedDate],
			SCH.[CreatorFullPhotoUrl],
			SCH.[CreatorSmallPhotoUrl],
			SCH.[CreatorName],
			SCH.[ManualAuthent__c],
			SCH.[Comments255__c],
			SCH.[NextSLAStartDate__c],
			SCH.[PreventAssign__c],
			SCH.[ReviewDate__c],
			SCH.[ReviewSubject__c],
			SCH.[EighteenCharId__c],
			SCH.[TCHNbMilestonesClosed__c],
			SCH.[isOwnerQueue__c],
			SCH.[Amount__c],
			SCH.[AuthenticationResult__c],
			SCH.[Answer__c],
			SCH.[Detail__c],
			SCH.[DocumentType__c],
			SCH.[Duration__c],
			SCH.[Dysfunction__c],
			SCH.[ManagerialAct__c],
			SCH.[Confidencelevel__c],
			SCH.[PaymentCeiling__c],
			SCH.[WithdrawalLimit__c],
			SCH.[ForbiddenWords__c],
			SCH.[ClientVisibility__c],
			SCH.[TCHTreatmentTime__c],
			SCH.[CancelConfiramtion__c],
			SCH.[ClaimTreatementTime__c],
			SCH.[PJPresence__c],
			SCH.[HasForbiddenWord__c],
			SCH.[TCHAuthent__c],
			SCH.[realizedBy__c],
			SCH.[MessageCounter__c],
			SCH.[NewMessage__c],
			SCH.[PlanifiedDate__c],
			SCH.[TCHCaseAttributionOwnerDate__c],
			SCH.[TCHCaseTimeProcessing__c],
			SCH.[TCHIdCase__c],
			SCH.[TCHPurge__c],
			SCH.[TCHTriggerPass__c],
			SCH.[TreatmentDate__c],
			SCH.[Details__c],
			SCH.[EFrontNumber__c],
			SCH.[Specification__c],
			SCH.[SubUniverse__c],
			SCH.[Universe__c],
			SCH.[IncidentGravity__c],
			SCH.[ConseillerTechnique__c],
			SCH.[EmailSource__c],
			SCH.[Manager__c],
			SCH.[Decision__c],
			SCH.[ExpectedCorrectionDeadline__c],
			SCH.[ReasonAnomalyTA3C__c],
			SCH.[Suivi_Evolution_du_Statut__c],
			SCH.[Relance_client__c],
			SCH.[Validation_PJ__c],
			SCH.[Perimeter__c],
			SCH.[TCHOppIdProcessSous__c],
			SCH.[TCHOptDistributorNetwork__c],
			SCH.[Incident_en_Cours__c],
			SCH.[IsClaimHavingApprovalProcess__c],
			SCH.[ScoringCardPaymentCeiling__c],
			SCH.[Validity_StartDate],
			SCH.[Validity_EndDate],
			SCH.[Startdt],
			SCH.[Enddt]
		FROM
			dbo.PV_SF_CASEHistory	AS	SCH	WITH(NOLOCK)
	)	AS	C
	LEFT JOIN
	(
		SELECT DISTINCT
			CODE_SF, LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL	WITH(NOLOCK)
		WHERE
			ISNULL(OBJECT_CODE_SF, '') = 'case'
			AND	ISNULL(FIELD_CODE_SF, '') = 'ActionPlan__c'
	)	AS	ref_ActionPlan__c
		ON	C.ActionPlan__c = ref_ActionPlan__c.CODE_SF
	LEFT JOIN
	(
		SELECT DISTINCT
			CODE_SF, LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL	WITH(NOLOCK)
		WHERE
			ISNULL(OBJECT_CODE_SF, '') = 'case'
			AND	ISNULL(FIELD_CODE_SF, '') = 'CustomerType__c'
	)	AS	ref_CustomerType__c
		ON	C.CustomerType__c = ref_CustomerType__c.CODE_SF
	LEFT JOIN
	(
		SELECT DISTINCT
			CODE_SF, LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL	WITH(NOLOCK)
		WHERE
			ISNULL(OBJECT_CODE_SF, '') = 'case'
			AND	ISNULL(FIELD_CODE_SF, '') = 'FirstClaim__c'
	)	AS	ref_FirstClaim__c
		ON	C.FirstClaim__c = ref_FirstClaim__c.CODE_SF
	LEFT JOIN
	(
		SELECT DISTINCT
			CODE_SF, LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL	WITH(NOLOCK)
		WHERE
			ISNULL(OBJECT_CODE_SF, '') = 'case'
			AND	ISNULL(FIELD_CODE_SF, '') = 'Indemnity__c'
	)	AS	ref_Indemnity__c
		ON	C.Indemnity__c = ref_Indemnity__c.CODE_SF
	LEFT JOIN
	(
		SELECT DISTINCT
			CODE_SF, LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL	WITH(NOLOCK)
		WHERE
			ISNULL(OBJECT_CODE_SF, '') = 'case'
			AND	ISNULL(FIELD_CODE_SF, '') = 'LevelClaim__c'
	)	AS	ref_LevelClaim__c
		ON	C.LevelClaim__c = ref_LevelClaim__c.CODE_SF
	LEFT JOIN
	(
		SELECT DISTINCT
			CODE_SF, LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL	WITH(NOLOCK)
		WHERE
			ISNULL(OBJECT_CODE_SF, '') = 'case'
			AND	ISNULL(FIELD_CODE_SF, '') = 'Metier__c'
	)	AS	ref_Metier__c
		ON	C.Metier__c = ref_Metier__c.CODE_SF
	LEFT JOIN
	(
		SELECT DISTINCT
			CODE_SF, LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL	WITH(NOLOCK)
		WHERE
			ISNULL(OBJECT_CODE_SF, '') = 'case'
			AND	ISNULL(FIELD_CODE_SF, '') = 'Network__c'
	)	AS	ref_Network__c
		ON	C.Network__c = ref_Network__c.CODE_SF
	LEFT JOIN
	(
		SELECT DISTINCT
			CODE_SF, LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL	WITH(NOLOCK)
		WHERE
			ISNULL(OBJECT_CODE_SF, '') = 'case'
			AND	ISNULL(FIELD_CODE_SF, '') = 'Origin'
	)	AS	ref_Origin
		ON	C.Origin = ref_Origin.CODE_SF
	LEFT JOIN
	(
		SELECT DISTINCT
			CODE_SF, LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL	WITH(NOLOCK)
		WHERE
			ISNULL(OBJECT_CODE_SF, '') = 'case'
			AND	ISNULL(FIELD_CODE_SF, '') = 'Origin__c'
	)	AS	ref_Origin__c
		ON	C.Origin__c = ref_Origin__c.CODE_SF
	LEFT JOIN
	(
		SELECT DISTINCT
			CODE_SF, LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL	WITH(NOLOCK)
		WHERE
			ISNULL(OBJECT_CODE_SF, '') = 'case'
			AND	ISNULL(FIELD_CODE_SF, '') = 'PrimaryQualification__c'
	)	AS	ref_PrimaryQualification__c
		ON	C.PrimaryQualification__c = ref_PrimaryQualification__c.CODE_SF
	LEFT JOIN
	(
		SELECT DISTINCT
			CODE_SF, LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL	WITH(NOLOCK)
		WHERE
			ISNULL(OBJECT_CODE_SF, '') = 'case'
			AND	ISNULL(FIELD_CODE_SF, '') = 'PrimarySubject__c'
	)	AS	ref_PrimarySubject__c
		ON	C.PrimarySubject__c = ref_PrimarySubject__c.CODE_SF
	LEFT JOIN
	(
		SELECT DISTINCT
			CODE_SF, LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL	WITH(NOLOCK)
		WHERE
			ISNULL(OBJECT_CODE_SF, '') = 'case'
			AND	ISNULL(FIELD_CODE_SF, '') = 'Priority'
	)	AS	ref_Priority
		ON	C.Priority = ref_Priority.CODE_SF
	LEFT JOIN
	(
		SELECT DISTINCT
			CODE_SF, LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL	WITH(NOLOCK)
		WHERE
			ISNULL(OBJECT_CODE_SF, '') = 'case'
			AND	ISNULL(FIELD_CODE_SF, '') = 'Process__c'
	)	AS	ref_Process__c
		ON	C.Process__c = ref_Process__c.CODE_SF
	LEFT JOIN
	(
		SELECT DISTINCT
			CODE_SF, LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL	WITH(NOLOCK)
		WHERE
			ISNULL(OBJECT_CODE_SF, '') = 'case'
			AND	ISNULL(FIELD_CODE_SF, '') = 'Reason'
	)	AS	ref_Reason
		ON	C.Reason = ref_Reason.CODE_SF
	LEFT JOIN
	(
		SELECT DISTINCT
			CODE_SF, LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL	WITH(NOLOCK)
		WHERE
			ISNULL(OBJECT_CODE_SF, '') = 'case'
			AND	ISNULL(FIELD_CODE_SF, '') = 'ResponseChannel__c'
	)	AS	ref_ResponseChannel__c
		ON	C.ResponseChannel__c = ref_ResponseChannel__c.CODE_SF
	LEFT JOIN
	(
		SELECT DISTINCT
			CODE_SF, LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL	WITH(NOLOCK)
		WHERE
			ISNULL(OBJECT_CODE_SF, '') = 'case'
			AND	ISNULL(FIELD_CODE_SF, '') = 'SecondarySubject__c'
	)	AS	ref_SecondarySubject__c
		ON	C.SecondarySubject__c = ref_SecondarySubject__c.CODE_SF
	LEFT JOIN
	(
		SELECT DISTINCT
			CODE_SF, LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL	WITH(NOLOCK)
		WHERE
			ISNULL(OBJECT_CODE_SF, '') = 'case'
			AND	ISNULL(FIELD_CODE_SF, '') = 'SecondQualification__c'
	)	AS	ref_SecondQualification__c
		ON	C.SecondQualification__c = ref_SecondQualification__c.CODE_SF
	LEFT JOIN
	(
		SELECT DISTINCT
			CODE_SF, LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL	WITH(NOLOCK)
		WHERE
			ISNULL(OBJECT_CODE_SF, '') = 'case'
			AND	ISNULL(FIELD_CODE_SF, '') = 'StandBy__c'
	)	AS	ref_StandBy__c
		ON	C.StandBy__c = ref_StandBy__c.CODE_SF
	LEFT JOIN
	(
		SELECT DISTINCT
			CODE_SF, LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL	WITH(NOLOCK)
		WHERE
			ISNULL(OBJECT_CODE_SF, '') = 'case'
			AND	ISNULL(FIELD_CODE_SF, '') = 'Status'
	)	AS	ref_Status
		ON	C.Status = ref_Status.CODE_SF
	LEFT JOIN
	(
		SELECT DISTINCT
			CODE_SF, LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL	WITH(NOLOCK)
		WHERE
			ISNULL(OBJECT_CODE_SF, '') = 'case'
			AND	ISNULL(FIELD_CODE_SF, '') = 'SubType__c'
	)	AS	ref_SubType__c
		ON	C.SubType__c = ref_SubType__c.CODE_SF
	LEFT JOIN
	(
		SELECT DISTINCT
			CODE_SF, LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL	WITH(NOLOCK)
		WHERE
			ISNULL(OBJECT_CODE_SF, '') = 'case'
			AND	ISNULL(FIELD_CODE_SF, '') = 'TreatementReason__c'
	)	AS	ref_TreatementReason__c
		ON	C.TreatementReason__c = ref_TreatementReason__c.CODE_SF
	LEFT JOIN
	(
		SELECT DISTINCT
			CODE_SF, LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL	WITH(NOLOCK)
		WHERE
			ISNULL(OBJECT_CODE_SF, '') = 'case'
			AND	ISNULL(FIELD_CODE_SF, '') = 'Type'
	)	AS	ref_Type
		ON	C.Type = ref_Type.CODE_SF
	LEFT JOIN
	(
		SELECT DISTINCT
			CODE_SF, LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL	WITH(NOLOCK)
		WHERE
			ISNULL(OBJECT_CODE_SF, '') = 'case'
			AND	ISNULL(FIELD_CODE_SF, '') = 'CallStatus__c'
	)	AS	ref_CallStatus__c
		ON	C.CallStatus__c = ref_CallStatus__c.CODE_SF
	LEFT JOIN
	(
		SELECT DISTINCT
			CODE_SF, LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL	WITH(NOLOCK)
		WHERE
			ISNULL(OBJECT_CODE_SF, '') = 'case'
			AND	ISNULL(FIELD_CODE_SF, '') = 'CallDetail__c'
	)	AS	ref_CallDetail__c
		ON	C.CallDetail__c = ref_CallDetail__c.CODE_SF
	LEFT JOIN
	(
		SELECT DISTINCT
			CODE_SF, LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL	WITH(NOLOCK)
		WHERE
			ISNULL(OBJECT_CODE_SF, '') = 'case'
			AND	ISNULL(FIELD_CODE_SF, '') = 'DocumentType__c'
	)	AS	ref_DocumentType__c
		ON	C.DocumentType__c = ref_DocumentType__c.CODE_SF
	LEFT JOIN
	(
		SELECT DISTINCT
			CODE_SF, LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL	WITH(NOLOCK)
		WHERE
			ISNULL(OBJECT_CODE_SF, '') = 'case'
			AND	ISNULL(FIELD_CODE_SF, '') = 'Dysfunction__c'
	)	AS	ref_Dysfunction__c
		ON	C.Dysfunction__c = ref_Dysfunction__c.CODE_SF
	LEFT JOIN
	(
		SELECT DISTINCT
			CODE_SF, LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL	WITH(NOLOCK)
		WHERE
			ISNULL(OBJECT_CODE_SF, '') = 'case'
			AND	ISNULL(FIELD_CODE_SF, '') = 'ManagerialAct__c'
	)	AS	ref_ManagerialAct__c
		ON	C.ManagerialAct__c = ref_ManagerialAct__c.CODE_SF
	LEFT JOIN
	(
		SELECT DISTINCT
			CODE_SF, LIBELLE_SF
		FROM
			dbo.RAW_SF_REFERENTIEL	WITH(NOLOCK)
		WHERE
			ISNULL(OBJECT_CODE_SF, '') = 'case'
			AND	ISNULL(FIELD_CODE_SF, '') = 'ClientVisibility__c'
	)	AS	ref_ClientVisibility__c
		ON	C.ClientVisibility__c = ref_ClientVisibility__c.CODE_SF
	LEFT JOIN dbo.REF_RECORDTYPE	AS	ref_RecordTypeId	WITH(NOLOCK)
		ON	ref_RecordTypeId.OBJECTTYPE_SF = 'Case'
			AND	ref_RecordTypeId.ID_SF = C.RecordTypeId
GO