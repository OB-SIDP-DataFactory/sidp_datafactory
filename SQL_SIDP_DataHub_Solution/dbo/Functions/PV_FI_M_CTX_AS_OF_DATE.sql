﻿
CREATE FUNCTION [dbo].[PV_FI_M_CTX_AS_OF_DATE]
( @P_Date DATE )
RETURNS TABLE AS
RETURN
( SELECT [Id], [COD_ENR], [TYP_CON], [TYP_PRE], [NUM_PRE], [IDE_GRC], [COD_BAN], [COD_GUI], [NUM_CPT], [DEV_CPT], [DTE_REM_CTX], [CAP_DU_MEN_ECH], [MTT_MEN_IMP], [INT_MEN_IMP], [IND_LEG], [SLD_AFF_CTX], [DTE_DER_ECR_COM], [CUM_PER], [COD_POS], [Validity_StartDate], [Validity_EndDate], [Startdt], [Enddt] FROM [dbo].[VW_PV_FI_M_CTX]
   WHERE @P_Date >= cast([Validity_StartDate] as date) and @P_Date < cast([Validity_EndDate] as date)
)