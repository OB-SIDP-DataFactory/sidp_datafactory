﻿CREATE FUNCTION [dbo].[PV_SA_M_COMECHL_AS_OF_DATE]
( @P_Date DATE )
RETURNS TABLE AS
RETURN
( SELECT [ID], [DWHCOMDTX], [DWHCOMETA], [DWHCOMCLI], [DWHCOMPLA], [DWHCOMCOM], [DWHCOMOPE], [DWHCOMANA], [DWHCOMDTD], [DWHCOMMON], [DWHCOMNOP], [DWHCOMDEV], [DWHCOMBAS], [DWHCOMNCD], [DWHCOMNCC], [DWHCOMDSY], [Validity_StartDate], [Validity_EndDate], [Startdt], [Enddt] FROM [dbo].[VW_PV_SA_M_COMECHL]
   WHERE @P_Date >= cast([Validity_StartDate] as date) and @P_Date < cast([Validity_EndDate] as date)
)