﻿CREATE FUNCTION [dbo].[PV_SF_OPPORTUNITY_HISTORY_AS_OF_DATE]
( @P_Date DATE )
RETURNS TABLE AS
RETURN
( SELECT * FROM [dbo].[VW_PV_SF_OPPORTUNITY_HISTORY]
   WHERE @P_Date >= cast([Validity_StartDate] as date) and @P_Date < cast([Validity_EndDate] as date)
)