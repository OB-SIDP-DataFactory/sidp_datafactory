﻿CREATE FUNCTION [dbo].[PV_SA_M_GARANTIE_AS_OF_DATE]
( @P_Date DATE )
RETURNS TABLE AS
RETURN
( SELECT [ID], [DWHGARDTX], [DWHGARETA], [DWHGARAGE], [DWHGARSER], [DWHGARSSE], [DWHGAROPE], [DWHGARNAT], [DWHGARNDO], [DWHGARSEQ], [DWHGARDEV], [DWHGARDEB], [DWHGARFIN], [DWHGARFLG], [DWHGARTGA], [DWHGARRAN], [DWHGARUSA], [DWHGARDUR], [DWHGARRES], [DWHGARMOP], [DWHGARMBS], [DWHGAREOP], [DWHGAREBA], [DWHGARMNT], [DWHGARDAT], [DWHGARTDO], [DWHGARIDO], [DWHGARIDB], [DWHGARCOQ], [DWHGARPAY], [DWHGARDOU], [DWHGARFLJ], [DWHGARNUJ], [DWHGARTYP], [DWHGARTOP], [DWHGARPGA], [DWHGARPMI], [DWHGARPRC], [DWHGARRIS], [DWHGARFRE], [Validity_StartDate], [Validity_EndDate], [Startdt], [Enddt] FROM [dbo].[VW_PV_SA_M_GARANTIE]
   WHERE @P_Date >= cast([Validity_StartDate] as date) and @P_Date < cast([Validity_EndDate] as date)
)