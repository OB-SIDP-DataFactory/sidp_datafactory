﻿CREATE FUNCTION [dbo].[GET_SALESFORCE_LIBELLE]
(
      @object_code_sf nvarchar(255),
      @field_code_sf nvarchar(255),
      @code_sf nvarchar(255)
) 
RETURNS NVARCHAR(255)
AS
BEGIN
      declare @libelle nvarchar(255);
      select
            @libelle = LIBELLE_SF
      from
            [dbo].[RAW_SF_REFERENTIEL_xsn]
      where
            1 = 1
            AND   OBJECT_CODE_SF = @object_code_sf
            and FIELD_CODE_SF = @field_code_sf
            and CODE_SF = @code_sf;

      IF (@libelle IS NULL)   
            SET @libelle = 'NA'; 

      RETURN @libelle
END