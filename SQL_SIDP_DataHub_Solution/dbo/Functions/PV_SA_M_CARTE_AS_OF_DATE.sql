﻿CREATE FUNCTION [dbo].[PV_SA_M_CARTE_AS_OF_DATE]
( @P_Date DATE )
RETURNS TABLE AS
RETURN
( SELECT [ID], [DWHCARDTX], [DWHCARETB], [DWHCARCAR], [DWHCARCOM], [DWHCARNUS], [DWHCARNUC], [DWHCARNPO], [DWHCARTPO], [DWHCARLIB], [DWHCARNAT], [DWHCARTYP], [DWHCAROPT], [DWHCARUTI], [DWHCARVAL], [DWHCARDUR], [DWHCARAGE], [DWHCARHSE], [DWHCARENR], [DWHCARREC], [DWHCARREM], [DWHCARCRE], [DWHCARREN], [DWHCARRES], [DWHCARDRN], [DWHCARDRS], [DWHCARENV], [DWHCARAUT], [DWHCARLIV], [DWHCARACH], [DWHCARMRA], [DWHCARCET], [DWHCARVIS], [DWHCARACA], [DWHCARCRA], [DWHCARINV], [Validity_StartDate], [Validity_EndDate], [Startdt], [Enddt] FROM [dbo].[VW_PV_SA_M_CARTE]
   WHERE @P_Date >= cast([Validity_StartDate] as date) and @P_Date < cast([Validity_EndDate] as date)
)