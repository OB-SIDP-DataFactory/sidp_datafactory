﻿CREATE FUNCTION [dbo].[PV_FI_M_COMPL_UP_Q_AS_OF_DATE]
( @P_Date DATE )
RETURNS TABLE AS
RETURN
( SELECT [Id], [COD_ENR], [IDE_GRC], [IDE_GRC_COE], [TYP_COM], [NUM_PRE], [TYP_ENR], [NUM_UP], [LIB_UP], [DTE_UP], [DUR_INI_UP], [DUR_RES_UP], [TAU_AGIO_UP], [TAU_AGIO_TAEG], [DTE_SAI_UP], [MTT_INI_UP], [DTE_ECH], [MTT_CRD_UP], [DTE_PRO_ECH], [MTT_PRO_ECH_UP], [Validity_StartDate], [Validity_EndDate], [Startdt], [Enddt] FROM [dbo].[VW_PV_FI_M_COMPL_UP_Q]
   WHERE @P_Date >= cast([Validity_StartDate] as date) and @P_Date < cast([Validity_EndDate] as date)
)