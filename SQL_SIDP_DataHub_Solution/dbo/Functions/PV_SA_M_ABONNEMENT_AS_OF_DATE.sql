﻿CREATE FUNCTION [dbo].[PV_SA_M_ABONNEMENT_AS_OF_DATE]
( @P_Date DATE )
RETURNS TABLE AS
RETURN
( SELECT [ID], [DWHABODTX], [DWHABOETA], [DWHABOAGE], [DWHABOSER], [DWHABOSSE], [DWHABONUM], [DWHABOCLI], [DWHABOCOM], [DWHABOPRO], [DWHABOADH], [DWHABOFIN], [DWHABOREN], [DWHABOCET], [DWHABOCOF], [DWHABOUT1], [DWHABOUT2], [DWHABORES], [DWHABOMOR], [DWHABOCRE], [DWHABOVAL], [DWHABOANN], [DWHABOREL], [Validity_StartDate], [Validity_EndDate], [Startdt], [Enddt] FROM [dbo].[VW_PV_SA_M_ABONNEMENT]
   WHERE @P_Date >= cast([Validity_StartDate] as date) and @P_Date < cast([Validity_EndDate] as date)
)