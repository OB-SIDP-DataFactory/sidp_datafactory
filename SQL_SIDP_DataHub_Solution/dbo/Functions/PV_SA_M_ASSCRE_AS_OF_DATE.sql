﻿CREATE FUNCTION [dbo].[PV_SA_M_ASSCRE_AS_OF_DATE]
( @P_Date DATE )
RETURNS TABLE AS
RETURN
( SELECT [ID], [DWHASSDTX], [DWHASSETA], [DWHASSAGE], [DWHASSSER], [DWHASSSSE], [DWHASSOPE], [DWHASSNAT], [DWHASSNUM], [DWHASSNUA], [DWHASSCOA], [DWHASSMOA], [DWHASSDEV], [DWHASSTAU], [DWHASSCLI], [DWHASSTYP], [Validity_StartDate], [Validity_EndDate], [Startdt], [Enddt] FROM [dbo].[VW_PV_SA_M_ASSCRE]
   WHERE @P_Date >= cast([Validity_StartDate] as date) and @P_Date < cast([Validity_EndDate] as date)
)