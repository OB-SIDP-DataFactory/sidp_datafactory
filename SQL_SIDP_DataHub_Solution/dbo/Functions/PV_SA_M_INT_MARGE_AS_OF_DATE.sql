﻿CREATE FUNCTION [dbo].[PV_SA_M_INT_MARGE_AS_OF_DATE]
( @P_Date DATE )
RETURNS TABLE AS
RETURN
( SELECT [Id], [DWHMM3DTX], [DWHMM3ETA], [DWHMM3AGE], [DWHMM3SER], [DWHMM3SES], [DWHMM3OPE], [DWHMM3NAT], [DWHMM3NUM], [DWHMM3SEN], [DWHMM3SEQ], [DWHMM3DTD], [DWHMM3DEV], [DWHMM3REF], [DWHMM3APP], [DWHMM3TAU], [DWHMM3MAR], [DWHMM3MRC], [DWHMM3DVA], [DWHMM3DTR], [DWHMM3DRG], [DWHMM3INT], [DWHMM3COU], [DWHMM3DEB], [DWHMM3FIN], [DWHMM3ASS], [DWHMM3NBJ], [DWHMM3NBP], [DWHMM3BAS], [DWHMM3MAC], [DWHMM3MIN], [DWHMM3TXA], [DWHMM3INC], [DWHMM3COC], [DWHMM3ASC], [DWHMM3MCC], [DWHMM3MIC], [DWHMM3DSY], [Validity_StartDate], [Validity_EndDate], [Startdt], [Enddt] FROM [dbo].[VW_PV_SA_M_INT_MARGE]
   WHERE @P_Date >= cast([Validity_StartDate] as date) and @P_Date < cast([Validity_EndDate] as date)
)