﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
CREATE FUNCTION [dbo].[format_mydate] 
(
	@dte varchar(8)
)
RETURNS date
AS
BEGIN
	-- Declare the return variable here
	DECLARE @result date = null

	if @dte != '00000000' and @dte != '00001231'
		select @result = CAST(@dte as date)

	-- Return the result of the function
	RETURN @result

END

--select dbo.format_date('20170203');

