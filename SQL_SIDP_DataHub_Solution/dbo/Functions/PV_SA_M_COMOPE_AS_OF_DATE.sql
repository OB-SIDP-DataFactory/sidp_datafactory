﻿CREATE FUNCTION [dbo].[PV_SA_M_COMOPE_AS_OF_DATE]
( @P_Date DATE )
RETURNS TABLE AS
RETURN
( SELECT [ID], [DWHMM4DTX], [DWHMM4ETA], [DWHMM4AGE], [DWHMM4SER], [DWHMM4SES], [DWHMM4OPE], [DWHMM4NAT], [DWHMM4NUM], [DWHMM4SEN], [DWHMM4COM], [DWHMM4SEQ], [DWHMM4DTD], [DWHMM4DEV], [DWHMM4MON], [DWHMM4MFL], [DWHMM4DSY], [Validity_StartDate], [Validity_EndDate], [Startdt], [Enddt] FROM [dbo].[VW_PV_SA_M_COMOPE]
   WHERE @P_Date >= cast([Validity_StartDate] as date) and @P_Date < cast([Validity_EndDate] as date)
)