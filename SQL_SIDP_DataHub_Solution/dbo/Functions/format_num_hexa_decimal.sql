﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[format_num_hexa_decimal] 
(
	-- Add the parameters for the function here
	@num varchar(50)
)
RETURNS decimal(18,3)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @result decimal(18,3);
	DECLARE @c varchar;
	DECLARE @t numeric;

	-- substring(chaine, début, longueur)
	set @c = SUBSTRING(@num, LEN(@num),1);
	set @t = cast(SUBSTRING(@num, 1, LEN(@num)-1) as bigint); 

	--print 'c = ' + @c + 't = ' + @t;
	if ASCII(@c) between 48 and 57 --chiffres de 0 à 9
		set @result =cast( @num as bigint) / 100.0;
	else
		begin
			if @c = 'é' or @c = '{' --caractère 'é' --> 0
				set @result = @t / 10.0;
			else
				begin
					if @c = 'è' or @c = '}' --caractère 'è' --> -0
						set @result = -@t / 10.0;
					else
						begin
							if ASCII(@c) between 65 and 73 --lettres de A à I --> +
								set @result = @t / 10.0 + ((ASCII(@c) - 64) / 100.0);
							else
								begin
									if ASCII(@c) between 74 and 82 --lettres de J à R --> -
										set @result = -@t / 10.0 - ((ASCII(@c) - 73) / 100.0);
									else
										set @result = @t / 10.0;
								end;
						end;
				end;
		end;
		 

	-- Return the result of the function
	RETURN @result/10.0

END
