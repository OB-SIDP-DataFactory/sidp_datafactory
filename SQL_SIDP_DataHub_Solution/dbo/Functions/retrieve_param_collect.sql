﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[retrieve_param_collect] 
(
	-- Add the parameters for the function here
	@domain nvarchar(50), 
	@batch_name nvarchar(50)
)
RETURNS datetime
WITh EXECUTE AS CALLER
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ResultVar datetime

	-- Add the T-SQL statements to compute the return value here
	SELECT @ResultVar = last_extraction_date
	FROM Param_Collect
	where domain = @domain and batch_name = @batch_name 
		and flag = 'OK'
		and last_extraction_date = ( select max(last_extraction_date) from Param_Collect where domain = @domain and batch_name = @batch_name and flag='OK');		
	-- Return the result of the function
	RETURN @ResultVar

END
