﻿CREATE FUNCTION [dbo].[PV_SA_M_LKAUT_AS_OF_DATE]
( @P_Date DATE )
RETURNS TABLE AS
RETURN
( SELECT [ID], [DWHLIADTX], [DWHLIAETA], [DWHLIACLI], [DWHLIATYP], [DWHLIAAUT], [DWHLIATY1], [DWHLIAAU1], [DWHLIATAU], [DWHLIAUTI], [DWHLIAFRA], [DWHLIADOM], [DWHLIAAPP], [DWHLIADSY], [Validity_StartDate], [Validity_EndDate], [Startdt], [Enddt] FROM [dbo].[VW_PV_SA_M_LKAUT]
   WHERE @P_Date >= cast([Validity_StartDate] as date) and @P_Date < cast([Validity_EndDate] as date)
)