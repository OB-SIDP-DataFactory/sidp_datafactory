﻿CREATE FUNCTION [dbo].[PV_SA_M_INCIDENT_AS_OF_DATE]
( @P_Date DATE )
RETURNS TABLE AS
RETURN
( SELECT [ID], [DWHFIMDTX], [DWHFIMETA], [DWHFIMTYP], [DWHFIMNUM], [DWHFIMCLT], [DWHFIMPLA], [DWHFIMCPT], [DWHFIMNUC], [DWHFIMSEQ], [DWHFIMEVE], [DWHFIMDEV], [DWHFIMMON], [DWHFIMMOB], [Validity_StartDate], [Validity_EndDate], [Startdt], [Enddt] FROM [dbo].[VW_PV_SA_M_INCIDENT]
   WHERE @P_Date >= cast([Validity_StartDate] as date) and @P_Date < cast([Validity_EndDate] as date)
)