﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[retrieve_param_alim] 
(
	-- Add the parameters for the function here
	@domain nvarchar(50), 
	@batch_name nvarchar(50)
)
RETURNS date
WITh EXECUTE AS CALLER
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ResultVar datetime

	-- Add the T-SQL statements to compute the return value here
	SELECT @ResultVar = last_alim_date
	FROM Param_AlimVues
	where domain = @domain and batch_name = @batch_name 
		and flag = 'OK'
		and last_alim_date = ( select max(last_alim_date) from Param_AlimVues where domain = @domain and batch_name = @batch_name and flag='OK');		
	-- Return the result of the function
	RETURN @ResultVar

END