﻿CREATE FUNCTION [dbo].[PV_SA_M_SERVICE_AS_OF_DATE]
( @P_Date DATE )
RETURNS TABLE AS
RETURN
( SELECT * FROM [dbo].[VW_PV_SA_M_SERVICE]
   WHERE @P_Date >= cast([Validity_StartDate] as date) and @P_Date < cast([Validity_EndDate] as date)
)