USE [$(DataHubDatabaseName)]
GO
TRUNCATE TABLE [dbo].[REF_MOTIF_FERMETURE]
GO
INSERT INTO [dbo].[REF_MOTIF_FERMETURE] ([MOT_CLO], [MOTIF_FERMETURE], [Validity_StartDate]) 
VALUES ('CREDIT', 'FIN DE CREDIT', '2017-01-01')
     , ('ALERTE', 'COMPTES DOUTEUX', '2017-01-01')
     , ('DC', 'DECES DU TITULAIRE ', '2017-01-01')
     , ('DECES', 'DECES', '2017-01-01')
     , ('SAICLO', 'CLOTURE SAISIE ADMINISTRATIVE', '2017-01-01')
     , ('TARIFI', 'TARIFIC + ABONNEMENT', '2017-01-01')
     , ('TRANSF', 'TRANSFERT INTERAGENCE', '2017-01-01')
     , ('8', 'CLOTURE APRES 8 ANS             ', '2017-01-01')
     , ('-2', 'CLOTURE AVANT 2 ANS             ', '2017-01-01')
     , ('CE', 'CESSATION ACTIVITE NON SALARIE  ', '2017-01-01')
     , ('CH', 'CHOMAGE                         ', '2017-01-01')
     , ('DS', 'DECES DU CONJOINT               ', '2017-01-01')
     , ('EX', 'EXPIRATION ASSURANCE CHOMAGE    ', '2017-01-01')
     , ('IN', 'INVALIDITE DU TITULAIRE         ', '2017-01-01')
     , ('MR', 'MOTIF REGLEMENTAIRE             ', '2017-01-01')
     , ('NI', 'TITULAIRE NON IMPOSABLE         ', '2017-01-01')
     , ('RE', 'RETRAIT ESPECES AVANT 8 ANS     ', '2017-01-01')
     , ('RT', 'RETRAIT TITRES AVANT 8 ANS      ', '2017-01-01')
     , ('25', 'CLOTURE ENTRE 2 ET 5 ANS        ', '2017-01-01')
     , ('58', 'CLOTURE ENTRE 5 ET 8 ANS        ', '2017-01-01')
     , (' CC   ', 'CHOIX DU CLIENT            ', '2017-01-01')
     , (' DC   ', 'DECES DU TITULAIRE         ', '2017-01-01')
     , ('BANQUE', 'A LA DEMANDE DE LA BANQUE', '2017-01-01')
     , ('RECOUV', 'RECOUVREMENT', '2017-01-01')
     , ('CC', 'CHOIX DU CLIENT', '2017-01-01')
     , ('CHANGE', 'CHANGEMENT GAMME', '2017-01-01')
     , ('CLIENT', 'A LA DEMANDE DU CLIENT', '2017-01-01')
     , ('COMPRO', 'COMPTE PROFESSIONNEL', '2017-01-01')
     , ('DESOLI', 'DESOLIDARISATION', '2017-01-01')
     , ('DORMAN', 'COMPTE SANS MOUV', '2017-01-01')
     , ('DYSFON', 'DYSF - ERREUR BANQUE', '2017-01-01')
     , ('INSATI', 'INSATISFAIT DE L''OFFRE', '2017-01-01')
     , ('RECLAM', 'SUITE A RECLAMATION', '2017-01-01')
     , ('RESEAU', 'ERREUR RESEAU', '2017-01-01')
     , ('RETRAC', 'RETRACTATION CLIENT', '2017-01-01')
     , ('SANMOT', 'SANS MOTIF-EXPLICATI', '2017-01-01')
     , ('SUCCES', 'SUCCESSION', '2017-01-01')
     , ('TITRES', 'TITRES', '2017-01-01')
     , ('TUTELL', 'TUTELLE', '2017-01-01')
     , ('DE', 'DEMANDE DU CLIENT               ', '2017-01-01')
     , (' TC   ', 'TRANSFERT A LA CONCURRENCE ', '2017-01-01')
GO
