USE [$(DataHubDatabaseName)]
GO
TRUNCATE TABLE [dbo].[REF_SUB_NETWORK]
GO
INSERT INTO [dbo].[REF_SUB_NETWORK] ([CODE_SF], [LIBELLE], [Validity_StartDate])
VALUES ('01', 'AD', '2017-01-01')
     , ('02', 'GDT', '2017-01-01')
     , ('03', 'Orange Bank', '2017-01-01')
     , ('04', 'OF-WEB', '2017-01-01')
     , ('05', 'Caisses régionales', '2017-01-01')
     , ('06', 'GAN Patrimoine', '2017-01-01')
     , ('07', 'GAN Prévoyance', '2017-01-01')
     , ('08', 'GAN Assurance', '2017-01-01')
     , ('09', 'DO', '2017-01-01')
GO
