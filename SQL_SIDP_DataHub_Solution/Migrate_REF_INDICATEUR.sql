﻿USE [$(DataHubDatabaseName)] 
GO
IF ( SELECT OBJECT_ID('dbo.Tmp_REF_INDICATEUR') ) IS NOT NULL
BEGIN
    DROP TABLE dbo.Tmp_REF_INDICATEUR
END
GO  

CREATE TABLE dbo.Tmp_REF_INDICATEUR
	(
	CODE_INDICATEUR varchar(10) NULL,
	LIBELLE_INDICATEUR varchar(60) NULL,
	CODE_FAMILLE_INDICATEUR varchar(10) NULL,
	LIBELLE_FAMILLE_INDICATEUR varchar(60) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_REF_INDICATEUR SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM dbo.REF_INDICATEUR)
	 EXEC('INSERT INTO dbo.Tmp_REF_INDICATEUR (CODE_INDICATEUR, LIBELLE_INDICATEUR, CODE_FAMILLE_INDICATEUR, LIBELLE_FAMILLE_INDICATEUR)
		SELECT CONVERT(varchar(10), CODE_INDICATEUR), CONVERT(varchar(60), LIBELLE_INDICATEUR), CONVERT(varchar(10), CODE_FAMILLE_INDICATEUR), CONVERT(varchar(60), LIBELLE_FAMILLE_INDICATEUR) FROM dbo.REF_INDICATEUR WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.REF_INDICATEUR
GO
EXECUTE sp_rename N'dbo.Tmp_REF_INDICATEUR', N'REF_INDICATEUR', 'OBJECT' 
GO
