﻿using System;
using System.ComponentModel.DataAnnotations;
using Salesforce.Common.Attributes;

namespace Salesforce_Solution.Models.Salesforce
{
    public class Case
    {
        [Key]
        [Display(Name = "Case ID")]
        [Createable(false), Updateable(false)]
        public string Id { get; set; }

        public string AccountId { get; set; }
        public string ActionPlan__c { get; set; }
        public Nullable<System.DateTime> ARDate__c { get; set; }
        public string CaseNumber { get; set; }
        public Nullable<System.DateTime> ClosedDate { get; set; }
        public Nullable<decimal> CommercialGestAmount__c { get; set; }
        public string CreatedById { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string CustomerType__c { get; set; }
        public string Description { get; set; }
        public Nullable<decimal> DUT__c { get; set; }
        public string Equipment__c { get; set; }
        public string ExpiredMilestone__c { get; set; }
        public string FirstClaim__c { get; set; }
        public string Habilitation__c { get; set; }
        public string Improvement__c { get; set; }
        public string Indemnity__c { get; set; }
        public string InitialCase__c { get; set; }
        public Nullable<decimal> isDoneWithExpMilestone__c { get; set; }
        public Nullable<decimal> IsDoneWithRespMilestone__c { get; set; }
        public Nullable<decimal> IsDone__c { get; set; }
        public Nullable<decimal> isNotDoneExpMilestone__c { get; set; }
        public Nullable<decimal> IsNotDoneWithRespMilestone__c { get; set; }
        public Nullable<decimal> IsNotDone__c { get; set; }
        public string LastModifiedById { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }
        public Nullable<System.DateTime> LetterWaitingDate__c { get; set; }
        public string LevelClaim__c { get; set; }
        public Nullable<decimal> LostAmount__c { get; set; }
        public string Metier__c { get; set; }
        public string MilestoneStatus { get; set; }
        public string Network__c { get; set; }
        public string Origin { get; set; }
        public string Origin__c { get; set; }
        public string OwnerId { get; set; }
        public string ParentId { get; set; }
        public string ParentOppy__c { get; set; }
        public string PrimaryQualification__c { get; set; }
        public string PrimarySubject__c { get; set; }
        public string Priority { get; set; }
        public string Process__c { get; set; }
        public string queueName__c { get; set; }
        public string Reason { get; set; }
        public string RecordTypeId { get; set; }
        public string ResponseChannel__c { get; set; }
        public Nullable<System.DateTime> ResponseDate__c { get; set; }
        public Nullable<decimal> RetrocessionAmount__c { get; set; }
        public string SecondarySubject__c { get; set; }
        public string SecondQualification__c { get; set; }
        public string ShopSeller__c { get; set; }
        public string StandBy__c { get; set; }
        public string Status { get; set; }
        public string SubType__c { get; set; }
        public string TechCloseCase__c { get; set; }
        public string Tech_PreviousOwner__c { get; set; }
        public string Tech_ReviewDateExpired__c { get; set; }
        public string TreatementReason__c { get; set; }
        public string Type { get; set; }
        public string IDCampagne__c { get; set; }
        public string CallStatus__c { get; set; }
        public string CallDetail__c { get; set; }
        public string OBShop__c { get; set; }
    }
}
