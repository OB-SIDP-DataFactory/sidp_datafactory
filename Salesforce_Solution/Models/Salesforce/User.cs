﻿using System;

namespace Salesforce_Solution.Models.Salesforce
{
    public class User
    {
        public string Id { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Title { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public Nullable<decimal> Latitude { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }
        public Nullable<decimal> Longitude { get; set; }
        public string PostalCode { get; set; }
        public string State { get; set; }
        public string StateCode { get; set; }
        public string Street { get; set; }
        public string UserRoleId { get; set; }
        public string ProfileId { get; set; }
        public string QSProfile__c { get; set; }
        public string GenesysUserId__c { get; set; }
    }
}
