﻿using System;
using System.ComponentModel.DataAnnotations;
using Salesforce.Common.Attributes;

namespace Salesforce_Solution.Models.Salesforce
{
    public class Opportunity
    {
        [Key]
        [Display(Name = "Opportunity ID")]
        [Createable(false), Updateable(false)]
        public string Id { get; set; }

        public string AccountId { get; set; }
        public string AdvisorCode__c { get; set; }
        public string CampaignId__c { get; set; }
        public Nullable<System.DateTime> CloseDate { get; set; }
        public string CommercialOfferCode__c { get; set; }
        public string CommercialOfferName__c { get; set; }
        public string CompleteFileFlag__c { get; set; }
        public string CreatedById { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string DeniedOpportunityReason__c { get; set; }
        public string DistributionChannel__c { get; set; }
        public string DistributorEntity__c { get; set; }
        public string DistributorNetwork__c { get; set; }
        public string FinalizedBy__c { get; set; }
        public Nullable<System.DateTime> FormValidation__c { get; set; }
        public string IDOppty__c { get; set; }
        public string IDProcessSous__c { get; set; }
        public string IdSource__c { get; set; }
        public string Indication__c { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }
        public string LeadSource { get; set; }
        public string Manager__c { get; set; }
        public string NoIndication__c { get; set; }
        public string OwnerId { get; set; }
        public string PointOfSaleCode__c { get; set; }
        public string ProductFamilyCode__c { get; set; }
        public string ProductFamilyName__c { get; set; }
        public string ProvenanceIndicationIndicatorId__c { get; set; }
        public string RecordTypeId { get; set; }
        public string RejectReason__c { get; set; }
        public string RelationEntryScore__c { get; set; }
        public string StageName { get; set; }
        public string StartedChannel__c { get; set; }
        public Nullable<System.DateTime> SystemModstamp { get; set; }
        public string Type { get; set; }
        public string IsWon { get; set; }
        public string Name { get; set; }
        public string realizedBy__c { get; set; }
        public Nullable<System.DateTime> SignatureDate__c { get; set; }
        public string StartedChannelForIndication__c { get; set; }
        public string ToBeDeleted__c { get; set; }
    }
}
