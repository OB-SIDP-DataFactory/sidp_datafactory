﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Salesforce_Solution.Models.Salesforce
{
    public class LiveChatTranscript
    {
        public string Id { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }
        public Nullable<System.DateTime> StartTime { get; set; }
        public string OwnerId { get; set; }
        public string CaseId { get; set; }
        public string AccountId { get; set; }
        public string Subject__c { get; set; }
        public Nullable<int> ChatDuration { get; set; }
        public Nullable<int> WaitTime { get; set; }
        public string ConversationID__c { get; set; }
    }
}
