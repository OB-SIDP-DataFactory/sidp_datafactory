﻿using Salesforce.Common.Attributes;
using System;
using System.ComponentModel.DataAnnotations;

namespace Salesforce_Solution.Models.Salesforce
{
    public class Account
    {
        [Key]
        [Display(Name = "Account ID")]
        [Createable(false), Updateable(false)]
        public string Id { get; set; }

        public string BehaviouralScoring__c { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> DeathDate__pc { get; set; }
        public string DistributorNetwork__c { get; set; }
        public string EmployeeType__pc { get; set; }
        public string Firstname { get; set; }
        public string GeolifeSegment__c { get; set; }
        public string IDCustomerCID__c { get; set; }
        public string IDCustomerPID__c { get; set; }
        public string IDCustomer__pc { get; set; }
        public string IDCustomerSAB__pc { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }
        public string Lastname { get; set; }
        public Nullable<System.DateTime> OBFirstContactDate__c { get; set; }
        public string OBSeniority__c { get; set; }
        public Nullable<System.DateTime> OBTerminationDate__c { get; set; }
        public string OBTerminationReason__c { get; set; }
        public string Occupation__pc { get; set; }
        public string OptInDataTelco__c { get; set; }
        public string OptInOrderTelco__c { get; set; }
        public string OwnerId { get; set; }
        public string ParentId { get; set; }
        public Nullable<System.DateTime> PersonBirthdate { get; set; }
        public string PersonContactId { get; set; }
        public string PersonLeadSource { get; set; }
        public string RecordTypeId { get; set; }
        public string Salutation { get; set; }
        public Nullable<System.DateTime> SystemModstamp { get; set; }
        public string ShopNetworkMesh__c { get; set; }
        public string ShopManagerLastName__c { get; set; }
        public string ShopManagerFirstName__c { get; set; }
        public string ShopCode__c { get; set; }
        public string ShopBrand__c { get; set; }
        public string Phone { get; set; }
        public string Name { get; set; }
        public string BillingStreet { get; set; }
        public string BillingCity { get; set; }
        public string BillingPostalCode { get; set; }
        public string Preattribution__c { get; set; }
        public string BillingCountryCode { get; set; }
        public string BillingCountry { get; set; }
        public string BillingStateCode { get; set; }
        public string BillingState { get; set; }
        public string MaritalStatus__pc { get; set; }
        public string OccupationNiv1__pc { get; set; }
        public string OccupationNiv3__pc { get; set; }
        public Nullable<decimal> TotalDisposableIncome__pc { get; set; }
        public Nullable<decimal> TotalIncome__pc { get; set; }
        public Nullable<decimal> JustifiedIncomes__pc { get; set; }
        public Nullable<System.DateTime> EmploymentContractStartDate__pc { get; set; }
        public Nullable<decimal> OFSeniority__c { get; set; }
        public string PrimaryResidOccupationType__pc { get; set; }
        public string PersonEmail  { get; set; }
        public string PersonMobilePhone { get; set; }
    }
}
