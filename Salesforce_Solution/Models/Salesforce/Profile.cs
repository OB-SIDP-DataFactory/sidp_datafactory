﻿using System;

namespace Salesforce_Solution.Models.Salesforce
{
    public class Profile
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }
    }
}
