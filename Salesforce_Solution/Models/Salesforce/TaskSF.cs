﻿using System;

namespace Salesforce_Solution.Models.Salesforce
{
    public class TaskSF
    {
        public string Id { get; set; }
        public string RecordTypeId { get; set; }
        public string WhatId { get; set; }
        public string OwnerId { get; set; }
        public string Description { get; set; }
        public string WhoId { get; set; }
        public string Subject { get; set; }
        public string InteractionId__c { get; set; }
        public string CreatedById { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }
        public string TaskSubType { get; set; }
        public string Priority { get; set; }
        public string Status { get; set; }
        public string Family__c { get; set; }
        public string SubFamily__c { get; set; }
        public Nullable<System.DateTime> InteractionDate__c { get; set; }
        public Nullable<decimal> ScoreOfInteraction__c { get; set; }
        public string NotationLabel__c { get; set; }
        public string ResponseLink__c { get; set; }
        public Nullable<System.DateTime> ActivityDate { get; set; }
        public string ConversationID__c { get; set; }
        public string CallType__c { get; set; }
    }
}
