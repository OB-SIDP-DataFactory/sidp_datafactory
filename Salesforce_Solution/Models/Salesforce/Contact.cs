﻿using System;

namespace Salesforce_Solution.Models.Salesforce
{
    public class Contact
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public Nullable<System.DateTime> IOBSPStart__c { get; set; }
        public Nullable<System.DateTime> IOBSPEnd__c { get; set; }
        public string SellerHashedCuid__c { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }
    }
}
