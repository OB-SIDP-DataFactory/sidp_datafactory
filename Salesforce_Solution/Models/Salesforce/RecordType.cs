﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Salesforce_Solution.Models.Salesforce
{
    class RecordType
    {
        public string BusinessProcessId { get; set; }
        public string CreatedById { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string Description { get; set; }
        public string DeveloperName { get; set; }
        public string Id { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsPersonType { get; set; }
        public string LastModifiedById { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }
        public string Name { get; set; }
        public string NamespacePrefix { get; set; }
        public string SobjectType { get; set; }
        public Nullable<System.DateTime> SystemModstamp { get; set; }
    }
}
