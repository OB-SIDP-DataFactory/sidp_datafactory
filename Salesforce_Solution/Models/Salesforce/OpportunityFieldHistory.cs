﻿using System;
using System.ComponentModel.DataAnnotations;
using Salesforce.Common.Attributes;

namespace Salesforce_Solution.Models.Salesforce
{
    public class OpportunityFieldHistory
    {
        [Key]
        [Display(Name = "OpportunityFieldHistory ID")]
        [Createable(false), Updateable(false)]
        public string Id { get; set; }

        public string CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Field { get; set; }
        public string IsDeleted { get; set; }
        public string NewValue { get; set; }
        public string OldValue { get; set; }
        public string OpportunityId { get; set; }
    }
}
