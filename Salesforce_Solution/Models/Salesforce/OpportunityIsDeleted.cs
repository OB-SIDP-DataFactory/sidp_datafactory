﻿using System;
using System.ComponentModel.DataAnnotations;
using Salesforce.Common.Attributes;

namespace Salesforce_Solution.Models.Salesforce
{
    class OpportunityIsDeleted
    {
        [Key]
        [Display(Name = "Opportunity ID")]
        [Createable(false), Updateable(false)]
        public string Id { get; set; }
        public string IsDeleted { get; set; }
    }
}