﻿using System;

namespace Salesforce_Solution.Models.Salesforce
{
    public class EmailMessage
    {
        public string Id { get; set; }
        public string CreatedById { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<bool> Incoming { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }
        public Nullable<System.DateTime> MessageDate { get; set; }
        public string FromAddress { get; set; }
        public string FromName { get; set; }
        public string ParentId { get; set; }
        public string Subject { get; set; }
        public string ToAddress { get; set; }
        public string CcAddress { get; set; }
        public string BccAddress { get; set; }
        public string TextBody { get; set; }
        public string HtmlBody { get; set; }
        public string RelatedToId { get; set; }
    }
}
