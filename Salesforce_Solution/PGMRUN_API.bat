@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

REM %~dp0Salesforce_Solution %~1
REM EXIT /B %ERRORLEVEL%

REM ---------------------------------------------------------------------------
REM Call new SSIS packages instead of C# pgm
REM ---------------------------------------------------------------------------
SET CURRENTDIR=%~dp0

CD %CURRENTDIR%
CD ..\SSIS

IF "%~1" == "RAW_ACCOUNT" (
    CALL .\PGMRUN_SSIS.bat "PKGCO_SSIS_SF_ACCOUNT_00"
    SET RC=!ERRORLEVEL!
)

IF "%~1" == "RAW_CASE" (
    CALL .\PGMRUN_SSIS.bat "PKGCO_SSIS_SF_CASE_00"
    SET RC=!ERRORLEVEL!
)
IF "%~1" == "RAW_OPPORTUNITY_History" (
    CALL .\PGMRUN_SSIS.bat "PKGCO_SSIS_SF_OPPORTUNITY_00"
    SET RC=!ERRORLEVEL!
	
    IF "!RC!" == "0" (
        CALL .\PGMRUN_SSIS.bat "PKGCO_SSIS_SF_OPPORTUNITYFIELDHISTORY_00"
        SET RC=!ERRORLEVEL!
	)
)

IF "%~1" == "RAW_USER" (
    CALL .\PGMRUN_SSIS.bat "PKGCO_SSIS_SF_USER_00"
    SET RC=!ERRORLEVEL!
)
IF "%~1" == "RAW_TASK" (
    CALL .\PGMRUN_SSIS.bat "PKGCO_SSIS_SF_TASK_00"
    SET RC=!ERRORLEVEL!
)
IF "%~1" == "RAW_EMAILMESSAGE" (
    CALL .\PGMRUN_SSIS.bat "PKGCO_SSIS_SF_EMAILMESSAGE_00"
    SET RC=!ERRORLEVEL!
)
IF "%~1" == "RAW_LIVECHATTRANSCRIPT" (
    CALL .\PGMRUN_SSIS.bat "PKGCO_SSIS_SF_LIVECHATTRANSCRIPT_00"
    SET RC=!ERRORLEVEL!
)
IF "%~1" == "RAW_PROFILE" (
    CALL .\PGMRUN_SSIS.bat "PKGCO_SSIS_SF_PROFILE_00"
    SET RC=!ERRORLEVEL!
)
IF "%~1" == "RAW_CONTACT" (
    CALL .\PGMRUN_SSIS.bat "PKGCO_SSIS_SF_CONTACT_00"
    SET RC=!ERRORLEVEL!
)
IF "%~1" == "RAW_REFERENTIEL" (
    CALL .\PGMRUN_SSIS.bat "PKGCO_SSIS_SF_RECORDTYPE_00"
    SET RC=!ERRORLEVEL!

    IF "!RC!" == "0" (
        CALL .\PGMRUN_SSIS.bat "PKGCO_SSIS_SF_PICKLISTVALUES_00"
        SET RC=!ERRORLEVEL!
    )
)


IF "%~1" == "PV_ACCOUNT" (
    SET RC="0"
)

IF "%~1" == "PV_CASE" (
    SET RC="0"
)
IF "%~1" == "PV_OPPORTUNITY" (
    SET RC="0"
)
IF "%~1" == "PV_OPPORTUNITYHistory" (
    CALL .\PGMRUN_SSIS.bat "PKGCO_SSIS_SF_OPPORTUNITY_HISTORY_01"
    SET RC=!ERRORLEVEL!
)
IF "%~1" == "PV_USER" (
    SET RC="0"
)
IF "%~1" == "PV_TASK" (
    SET RC="0"
)
IF "%~1" == "PV_EMAILMESSAGE" (
    SET RC="0"
)
IF "%~1" == "PV_LIVECHATTRANSCRIPT" (
    SET RC="0"
)
IF "%~1" == "PV_PROFILE" (
    SET RC=!ERRORLEVEL!
)
IF "%~1" == "PV_CONTACT" (
    SET RC="0"
)
IF "%~1" == "REF_REFERENTIEL" (
    CALL .\PGMRUN_SSIS.bat "PKGCO_SSIS_SF_RECORDTYPE_01"
    SET RC=!ERRORLEVEL!
)

CD %CURRENTDIR%
EXIT /B %RC%
