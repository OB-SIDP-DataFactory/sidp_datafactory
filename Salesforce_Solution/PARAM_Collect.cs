//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Salesforce_Solution
{
    using System;
    using System.Collections.Generic;
    
    public partial class Param_Collect
    {
        public int Id { get; set; }
        public string domain { get; set; }
        public string batch_name { get; set; }
        public System.DateTime last_extraction_date { get; set; }
        public string flag { get; set; }
    }
}
