﻿using Salesforce.Common;
using Salesforce.Common.Models;
using Salesforce.Force;
using Salesforce_Solution.Models.Salesforce;
using Salesforce_Solution.Salesforce;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using System.Net.Http;
using System.Net.Http.Headers;
using System.Xml;
using System.Text;

namespace Salesforce_Solution
{
    public static class sfConnect
    {
        //------------------------------------------------------------------------------------------------------------
        #region Main
        public static int Main(string[] args)
        {
            DateTime startExec = DateTime.Now;
            try
            {
                #region If                                  
                if (args[0].Equals("RAW_ACCOUNT"))
                {
                    var task = RAW_ACCOUNT(args);
                    task.Wait();
                    //PV_ACCOUNT();
                }
                else if (args[0].Equals("RAW_CASE"))
                {
                    var task = RAW_CASE(args);
                    task.Wait();
                    //PV_CASE();
                }
                else if (args[0].Equals("RAW_OPPORTUNITY_History"))  //OPPORTUNITY & History
                {
                    var task = WK_OPPORTUNITY(args, startExec);
                    task.Wait();
                    long idlogopp = task.Result;
                    var task1 = WK_OPPORTUNITYISDELETED(args, idlogopp, startExec);
                    task1.Wait();
                    var task2 = WK_OPPORTUNITYFIELDHISTORY(args, idlogopp, startExec);
                    task2.Wait();
                    //PV_OPPORTUNITY();
                    //PV_OPPORTUNITYHistory();
                }
                else if (args[0].Equals("RAW_USER"))
                {
                    var task = RAW_USER(args);
                    task.Wait();
                    //PV_USER();
                }
                else if (args[0].Equals("RAW_TASK"))
                {
                    var task = RAW_TASK(args);
                    task.Wait();
                    //PV_TASK();
                }
                else if (args[0].Equals("RAW_EMAILMESSAGE"))
                {
                    var task = RAW_EMAILMESSAGE(args);
                    task.Wait();
                    //PV_EMAILMESSAGE();
                }
                else if (args[0].Equals("RAW_LIVECHATTRANSCRIPT"))
                {
                    var task = RAW_LIVECHATTRANSCRIPT(args);
                    task.Wait();
                    //PV_LIVECHATTRANSCRIPT();
                }
                else if (args[0].Equals("RAW_PROFILE"))
                {
                    var task = RAW_PROFILE(args);
                    task.Wait();
                    //PV_PROFILE();
                }
                else if (args[0].Equals("RAW_CONTACT"))
                {
                    var task = RAW_CONTACT(args);
                    task.Wait();
                    //PV_CONTACT();
                }
                else if (args[0].Equals("RAW_REFERENTIEL"))
                {
                    string[] argsnew = new string[1];

                    argsnew[0] = "RAW_RECORDTYPE";
                    var task1 = RAW_RECORDTYPE(argsnew);
                    task1.Wait();

                    argsnew[0] = "RAW_REFERENTIEL";
                    var task2 = RAW_REFERENTIEL(argsnew);
                    task2.Wait();
                }
                else if (args[0].Equals("PV_ACCOUNT"))
                    PV_ACCOUNT();
                else if (args[0].Equals("PV_CASE"))
                    PV_CASE();
                else if (args[0].Equals("PV_OPPORTUNITY"))
                {
                    PV_OPPORTUNITY();
                    PV_OPPORTUNITYISDELETED();
                }
                else if (args[0].Equals("PV_OPPORTUNITYHistory"))
                    PV_OPPORTUNITYHistory();
                else if (args[0].Equals("PV_USER"))
                    PV_USER();
                else if (args[0].Equals("PV_TASK"))
                    PV_TASK();
                else if (args[0].Equals("PV_EMAILMESSAGE"))
                    PV_EMAILMESSAGE();
                else if (args[0].Equals("PV_LIVECHATTRANSCRIPT"))
                    PV_LIVECHATTRANSCRIPT();
                else if (args[0].Equals("PV_PROFILE"))
                    PV_PROFILE();
                else if (args[0].Equals("PV_CONTACT"))
                    PV_CONTACT();
                else if (args[0].Equals("REF_REFERENTIEL"))
                    REF_RECORDTYPE();
                else { }
                #endregion
                
                if (args[0].Contains("RAW_") && ! args[0].Equals("RAW_REFERENTIEL"))
                    UpdateParamCollect(startExec, GetBatchName(args), "OK");
                return 0;
            }
            catch (Exception ex)
            {
                using (var db = new sfDataHub())
                {
                    //------error
                    db.Data_Error.Add(new Data_Error
                    {
                        ProcessingType = GetProcessingType(args)
                        , ETLName = "sfExtraction", FileOrTableName = "so_objects"
                        , ErrorCode = ex.Source, TechnicalErrorMessage = ex.InnerException.Message, AppErrorMessage = "Salesforce exceptions"
                        , IdLog = IdOverallProcess
                        , ErrorDate = DateTime.Now
                    });
                    //------param_collect
                    if (args[0].Contains("RAW_") && !args[0].Equals("RAW_REFERENTIEL")) { 
                        db.Param_Collect.Add(new Param_Collect
                        {
                            domain = "SF",
                            batch_name = GetBatchName(args),
                            last_extraction_date = startExec,
                            flag = "KO"
                        });
                    }
                    //-----log
                    Data_Log log = db.Data_Log.Where(x => x.Id.Equals(IdOverallProcess)).FirstOrDefault();
                    db.Data_Log.Attach(log);
                    log.ProcessingStatus = "KO";
                    db.SaveChanges();
                    #region truncate WK tables
                    //Truncate WK_SF tables when in the collect process (not integration)
                    if (args[0].Equals("RAW_OPPORTUNITY_History"))
                    {
                        db.Database.ExecuteSqlCommand("TRUNCATE Table WK_SF_OPPORTUNITY");
                        //db.Database.ExecuteSqlCommand("TRUNCATE Table WK_SF_OPPORTUNITYFIELDHISTORY");
                        db.Database.ExecuteSqlCommand("TRUNCATE Table WK_SF_OPPORTUNITYHistory");
                    }
                    else if (args[0].Contains("RAW_"))
                        db.Database.ExecuteSqlCommand(GetTruncateWKString(args));
                    else { }
                    #endregion
                }
                return 1;
            }
        }
        #endregion

        //Accounts-------------------------------------------------------------------------------------------------
        #region RAW_ACCOUNT
        private static async Task RAW_ACCOUNT(string[] args)
        {
            int nb = 0;
            long idaccount = CreateLog(new Data_Log { ProcessingType = "COLLECT", ETLName = "PGMRUN_API_RAW_ACCOUNT", FileOrTableName = "RAW_SF_ACCOUNT", ProcessingStartDate = DateTime.Now, ProcessingEndDate = null, ProcessingStatus = null, NbRecordsProcessed = null, NbRecordsCollected = null });
            IdOverallProcess = idaccount;

            //Get the latest SUCCESSFUL SF objects extraction date
            #region Dates
            DateTime lastExtract = GetLastSuccessfulSFExtractDate(args);
            DateTime lastExtractUtc = lastExtract.ToUniversalTime();
            string startDate = lastExtractUtc.ToString("yyyy-MM-ddTHH:mm:ssZ", CultureInfo.InvariantCulture);
            #endregion
            //Delete all WK records
            DeleteWKRecords(args);

            #region API REST
            //Get the access token from the refresh token
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            sfRefreshToken sfAccessToken = await AcquireAccessTokenFromRefreshTokenAsync();
            ForceClient client = sfAccessToken.GetTheForceClient();

            var accounts = new List<Account>();
            var so = await client.QueryAsync<Account>(String.Format(SalesforceService.GetAppSetting("Req:account"), startDate));
            accounts.AddRange(so.Records);
            var nextRecordsUrl = so.NextRecordsUrl;
            if (!string.IsNullOrEmpty(nextRecordsUrl))
            {
                while (true)
                {
                    so = await client.QueryContinuationAsync<Account>(nextRecordsUrl);
                    accounts.AddRange(so.Records);
                    if (string.IsNullOrEmpty(so.NextRecordsUrl))
                        break;
                    nextRecordsUrl = so.NextRecordsUrl;
                }
            }
            #endregion

            using (var db = new sfDataHub())
            {
                #region WK_SF_ACCOUNT
                accounts.ForEach(x =>
                    db.WK_SF_ACCOUNT.Add(new WK_SF_ACCOUNT
                    {
                        Id_SF = x.Id,
                        BehaviouralScoring__c = x.BehaviouralScoring__c,
                        CreatedDate = x.CreatedDate.Value.ToUniversalTime(),
                        DeathDate__pc = x.DeathDate__pc,
                        DistributorNetwork__c = x.DistributorNetwork__c,
                        EmployeeType__pc = x.EmployeeType__pc,
                        Firstname = x.Firstname,
                        GeolifeSegment__c = x.GeolifeSegment__c,
                        IDCustomerCID__c = x.IDCustomerCID__c,
                        IDCustomerPID__c = x.IDCustomerPID__c,
                        IDCustomer__pc = x.IDCustomer__pc,
                        IDCustomerSAB__pc = x.IDCustomerSAB__pc,
                        LastModifiedDate = x.LastModifiedDate.Value.ToUniversalTime(),
                        Lastname = x.Lastname,
                        OBFirstContactDate__c = x.OBFirstContactDate__c,
                        OBSeniority__c = x.OBSeniority__c,
                        OBTerminationDate__c = x.OBTerminationDate__c,
                        OBTerminationReason__c = x.OBTerminationReason__c,
                        Occupation__pc = x.Occupation__pc,
                        OptInDataTelco__c = x.OptInDataTelco__c,
                        OptInOrderTelco__c = x.OptInOrderTelco__c,
                        OwnerId = x.OwnerId,
                        ParentId = x.ParentId,
                        PersonBirthdate = x.PersonBirthdate,
                        PersonContactId = x.PersonContactId,
                        PersonLeadSource = x.PersonLeadSource,
                        RecordTypeId = x.RecordTypeId,
                        Salutation = x.Salutation,
                        SystemModstamp = x.SystemModstamp.Value.ToUniversalTime(),
                        ShopNetworkMesh__c = x.ShopNetworkMesh__c,
                        ShopManagerLastName__c = x.ShopManagerLastName__c,
                        ShopManagerFirstName__c = x.ShopManagerFirstName__c,
                        ShopCode__c = x.ShopCode__c,
                        ShopBrand__c = x.ShopBrand__c,
                        Phone = x.Phone,
                        Name = x.Name,
                        BillingStreet = x.BillingStreet,
                        BillingCity = x.BillingCity,
                        BillingPostalCode = x.BillingPostalCode,
                        Preattribution__c = x.Preattribution__c,
                        BillingCountryCode = x.BillingCountryCode,
                        BillingCountry = x.BillingCountry,
                        BillingStateCode = x.BillingStateCode,
                        BillingState = x.BillingState,
                        MaritalStatus__pc = x.MaritalStatus__pc,
                        OccupationNiv1__pc = x.OccupationNiv1__pc,
                        OccupationNiv3__pc = x.OccupationNiv3__pc,
                        TotalDisposableIncome__pc = x.TotalDisposableIncome__pc,
                        TotalIncome__pc = x.TotalIncome__pc,
                        JustifiedIncomes__pc = x.JustifiedIncomes__pc,
                        EmploymentContractStartDate__pc = x.EmploymentContractStartDate__pc,
                        OFSeniority__c = x.OFSeniority__c,
                        PrimaryResidOccupationType__pc = x.PrimaryResidOccupationType__pc,
                        PersonEmail = x.PersonEmail,
                        PersonMobilePhone = x.PersonMobilePhone
                    })
                );
                #endregion
                //save WK_SF_ACCOUNT
                await db.SaveChangesAsync();
                //save RAW
                db.Database.CommandTimeout = Convert.ToInt32(SalesforceService.GetAppSetting("Req:timeout"));
                nb = db.Database.ExecuteSqlCommand(GetSQL_Account("RAW_SF_ACCOUNT"));
            }
            
            UpdateLog(idaccount, nb, accounts.Count());            
        }
        #endregion

        //Cases----------------------------------------------------------------------------------------------------
        #region RAW_CASE
        private static async Task RAW_CASE(string[] args)
        {
            int nb = 0;
            long idcase = CreateLog(new Data_Log { ProcessingType = "COLLECT", ETLName = "PGMRUN_API_RAW_CASE", FileOrTableName = "RAW_SF_CASE", ProcessingStartDate = DateTime.Now, ProcessingEndDate = null, ProcessingStatus = null, NbRecordsProcessed = null, NbRecordsCollected = null });
            IdOverallProcess = idcase;

            //Get the latest SUCCESSFUL SF objects extraction date
            #region Dates
            DateTime lastExtract = GetLastSuccessfulSFExtractDate(args);
            DateTime lastExtractUtc = lastExtract.ToUniversalTime();
            string startDate = lastExtractUtc.ToString("yyyy-MM-ddTHH:mm:ssZ", CultureInfo.InvariantCulture);
            #endregion
            //Delete all records before
            DeleteWKRecords(args);

            #region API REST
            //Get the access token from the refresh token
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            sfRefreshToken sfAccessToken = await AcquireAccessTokenFromRefreshTokenAsync();
            ForceClient client = sfAccessToken.GetTheForceClient();

            var cases = new List<Case>();
            var so = await client.QueryAsync<Case>(String.Format(SalesforceService.GetAppSetting("Req:case"), startDate));
            cases.AddRange(so.Records);
            var nextRecordsUrl = so.NextRecordsUrl;
            if (!string.IsNullOrEmpty(nextRecordsUrl))
            {
                while (true)
                {
                    so = await client.QueryContinuationAsync<Case>(nextRecordsUrl);
                    cases.AddRange(so.Records);
                    if (string.IsNullOrEmpty(so.NextRecordsUrl))
                        break;
                    nextRecordsUrl = so.NextRecordsUrl;
                }
            }
            #endregion

            using (var db = new sfDataHub())
            {
                #region WK_SF_CASE
                cases.ForEach(x =>
                   db.WK_SF_CASE.Add(new WK_SF_CASE
                   {
                       Id_SF = x.Id,
                       AccountId = x.AccountId,
                       ActionPlan__c = x.ActionPlan__c,
                       ARDate__c = x.ARDate__c.HasValue ? x.ARDate__c.Value.ToUniversalTime() : x.ARDate__c,
                       CaseNumber = x.CaseNumber,
                       ClosedDate = x.ClosedDate.HasValue ? x.ClosedDate.Value.ToUniversalTime() : x.ClosedDate,
                       CommercialGestAmount__c = x.CommercialGestAmount__c,
                       CreatedById = x.CreatedById,
                       CreatedDate = x.CreatedDate.Value.ToUniversalTime(),
                       CustomerType__c = x.CustomerType__c,
                       Description = x.Description,
                       DUT__c = x.DUT__c,
                       Equipment__c = x.Equipment__c,
                       ExpiredMilestone__c = x.ExpiredMilestone__c,
                       FirstClaim__c = x.FirstClaim__c,
                       Habilitation__c = x.Habilitation__c,
                       Improvement__c = x.Improvement__c,
                       Indemnity__c = x.Indemnity__c,
                       InitialCase__c = x.InitialCase__c,
                       isDoneWithExpMilestone__c = x.isDoneWithExpMilestone__c,
                       IsDoneWithRespMilestone__c = x.IsDoneWithRespMilestone__c,
                       IsDone__c = x.IsDone__c,
                       isNotDoneExpMilestone__c = x.isNotDoneExpMilestone__c,
                       IsNotDoneWithRespMilestone__c = x.IsNotDoneWithRespMilestone__c,
                       IsNotDone__c = x.IsNotDone__c,
                       LastModifiedById = x.LastModifiedById,
                       LastModifiedDate = x.LastModifiedDate.Value.ToUniversalTime(),
                       LetterWaitingDate__c = x.LetterWaitingDate__c,
                       LevelClaim__c = x.LevelClaim__c,
                       LostAmount__c = x.LostAmount__c,
                       Metier__c = x.Metier__c,
                       MilestoneStatus = x.MilestoneStatus,
                       Network__c = x.Network__c,
                       Origin = x.Origin,
                       Origin__c = x.Origin__c,
                       OwnerId = x.OwnerId,
                       ParentId = x.ParentId,
                       ParentOppy__c = x.ParentOppy__c,
                       PrimaryQualification__c = x.PrimaryQualification__c,
                       PrimarySubject__c = x.PrimarySubject__c,
                       Priority = x.Priority,
                       Process__c = x.Process__c,
                       queueName__c = x.queueName__c,
                       Reason = x.Reason,
                       RecordTypeId = x.RecordTypeId,
                       ResponseChannel__c = x.ResponseChannel__c,
                       ResponseDate__c = x.ResponseDate__c,
                       RetrocessionAmount__c = x.RetrocessionAmount__c,
                       SecondarySubject__c = x.SecondarySubject__c,
                       SecondQualification__c = x.SecondQualification__c,
                       ShopSeller__c = x.ShopSeller__c,
                       StandBy__c = x.StandBy__c,
                       Status = x.Status,
                       SubType__c = x.SubType__c,
                       TechCloseCase__c = x.TechCloseCase__c,
                       Tech_PreviousOwner__c = x.Tech_PreviousOwner__c,
                       Tech_ReviewDateExpired__c = x.Tech_ReviewDateExpired__c,
                       TreatementReason__c = x.TreatementReason__c,
                       Type = x.Type,
                       IDCampagne__c = x.IDCampagne__c,
                       CallDetail__c = x.CallDetail__c,
                       CallStatus__c = x.CallStatus__c,
                       OBShop__c = x.OBShop__c
                   })
                );
                #endregion

                //save WK_SF_CASE
                await db.SaveChangesAsync();
                //save RAW
                db.Database.CommandTimeout = Convert.ToInt32(SalesforceService.GetAppSetting("Req:timeout"));
                nb = db.Database.ExecuteSqlCommand(GetSQL_Case("RAW_SF_CASE"));
            }

            UpdateLog(idcase, nb, cases.Count());
        }
        #endregion

        //WK Opportunities-----------------------------------------------------------------------------------------
        #region WK_OPPORTUNITY
        private static async Task<long> WK_OPPORTUNITY(string[] args, DateTime execdateopp)
        {
            int nb = 0;
            long idlogopp = CreateLog(new Data_Log { ProcessingType = "COLLECT", ETLName = "PGMRUN_API_RAW_OPPORTUNITY", FileOrTableName = "RAW_SF_OPPORTUNITY", ProcessingStartDate = DateTime.Now, ProcessingEndDate = null, ProcessingStatus = null, NbRecordsProcessed = null, NbRecordsCollected = null });
            IdOverallProcess = idlogopp;

            //Get the latest SUCCESSFUL SF objects extraction 
            #region Dates
            DateTime lastExtract = GetLastSuccessfulSFExtractDate(args);
            DateTime lastExtractUtc = lastExtract.ToUniversalTime();
            DateTime execDateUtc = execdateopp.ToUniversalTime();
            string startDate = lastExtractUtc.ToString("yyyy-MM-ddTHH:mm:ssZ", CultureInfo.InvariantCulture);
            string execDate = execDateUtc.ToString("yyyy-MM-ddTHH:mm:ssZ", CultureInfo.InvariantCulture);
            #endregion
            //Delete all records before 
            DeleteOpps();

            #region API REST
            //Get the access token from the refresh token
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            sfRefreshToken sfAccessToken = await AcquireAccessTokenFromRefreshTokenAsync();
            ForceClient client = sfAccessToken.GetTheForceClient();

            var opportunities = new List<Opportunity>();
            var so = await client.QueryAsync<Opportunity>(string.Format(SalesforceService.GetAppSetting("Req:opportunity"), startDate, execDate));
            opportunities.AddRange(so.Records);
            var nextRecordsUrl = so.NextRecordsUrl;
            if (!string.IsNullOrEmpty(nextRecordsUrl))
            {
                while (true)
                {
                    so = await client.QueryContinuationAsync<Opportunity>(nextRecordsUrl);
                    opportunities.AddRange(so.Records);
                    if (string.IsNullOrEmpty(so.NextRecordsUrl))
                        break;
                    nextRecordsUrl = so.NextRecordsUrl;
                }
            }
            #endregion

            using (var db = new sfDataHub())
            {
                #region WK_SF_OPPORTUNITY
                opportunities.ForEach(x =>
                   db.WK_SF_OPPORTUNITY.Add(new WK_SF_OPPORTUNITY
                   {
                       Id_SF = x.Id,
                       AccountId = x.AccountId,
                       AdvisorCode__c = x.AdvisorCode__c,
                       CampaignId__c = x.CampaignId__c,
                       CloseDate = x.CloseDate,
                       CommercialOfferCode__c = x.CommercialOfferCode__c,
                       CommercialOfferName__c = x.CommercialOfferName__c,
                       CompleteFileFlag__c = x.CompleteFileFlag__c,
                       CreatedById = x.CreatedById,
                       CreatedDate = x.CreatedDate.Value.ToUniversalTime(),
                       DeniedOpportunityReason__c = x.DeniedOpportunityReason__c,
                       DistributionChannel__c = x.DistributionChannel__c,
                       DistributorEntity__c = x.DistributorEntity__c,
                       DistributorNetwork__c = x.DistributorNetwork__c,
                       FinalizedBy__c = x.FinalizedBy__c,
                       FormValidation__c = x.FormValidation__c,
                       IDOppty__c = x.IDOppty__c,
                       IDProcessSous__c = x.IDProcessSous__c,
                       IdSource__c = x.IdSource__c,
                       Indication__c = x.Indication__c,
                       LastModifiedDate = x.LastModifiedDate.Value.ToUniversalTime(),
                       LeadSource = x.LeadSource,
                       Manager__c = x.Manager__c,
                       NoIndication__c = x.NoIndication__c,
                       OwnerId = x.OwnerId,
                       PointOfSaleCode__c = x.PointOfSaleCode__c,
                       ProductFamilyCode__c = x.ProductFamilyCode__c,
                       ProductFamilyName__c = x.ProductFamilyName__c,
                       ProvenanceIndicationIndicatorId__c = x.ProvenanceIndicationIndicatorId__c,
                       RecordTypeId = x.RecordTypeId,
                       RejectReason__c = x.RejectReason__c,
                       RelationEntryScore__c = x.RelationEntryScore__c,
                       StageName = x.StageName,
                       StartedChannel__c = x.StartedChannel__c,
                       SystemModstamp = x.SystemModstamp.Value.ToUniversalTime(),
                       Type = x.Type,
                       IsWon = x.IsWon,
                       Name = x.Name,
                       realizedBy__c = x.realizedBy__c,
                       SignatureDate__c = x.SignatureDate__c,
                       StartedChannelForIndication__c = x.StartedChannelForIndication__c,
                       ToBeDeleted__c = x.ToBeDeleted__c
                   })
                );
                #endregion
                 
                await db.SaveChangesAsync(); //save WK_SF_OPPORTUNITY

                //RAW_SF_OPPORTUNITY
                db.Database.CommandTimeout = Convert.ToInt32(SalesforceService.GetAppSetting("Req:timeout"));
                nb = db.Database.ExecuteSqlCommand(GetSQL_Opportunity("RAW_SF_OPPORTUNITY"));
            }
            UpdateLog(idlogopp, nb, opportunities.Count());
            //UpdateLog(idlogopp, opportunities.Count(), null, "COLLECT");
            return idlogopp;
        }
        #endregion

        //WK Opportunity field histories
        #region WK_OPPORTUNITYFIELDHISTORY
        private static async Task WK_OPPORTUNITYFIELDHISTORY(string[] args, long idlogopp, DateTime execdateopp)
        {
            //Opportunity Field History
            long idopphistory = CreateLog(new Data_Log { ProcessingType = "COLLECT", ETLName = "PGMRUN_API_RAW_OPPORTUNITYHistory", FileOrTableName = "WK_OPPORTUNITYFIELDHISTORY", ProcessingStartDate = DateTime.Now, ProcessingEndDate = null, ProcessingStatus = null, NbRecordsProcessed = null });
            IdOverallProcess = idopphistory;

            //Get the latest SUCCESSFUL SF objects extraction date
            #region Dates
            DateTime lastExtract = GetLastSuccessfulSFExtractDate(args);
            DateTime lastExtractUtc = lastExtract.ToUniversalTime();
            DateTime execDateUtc = execdateopp.ToUniversalTime();
            string startDate = lastExtractUtc.ToString("yyyy-MM-ddTHH:mm:ssZ", CultureInfo.InvariantCulture);
            string execDate = execDateUtc.ToString("yyyy-MM-ddTHH:mm:ssZ", CultureInfo.InvariantCulture);
            #endregion
            //Delete all records before 
            DeleteOppHistories();

            #region API REST
            //Get the access token from the refresh token
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            sfRefreshToken sfAccessToken = await AcquireAccessTokenFromRefreshTokenAsync();
            ForceClient client = sfAccessToken.GetTheForceClient();            

            var opportunityhistories = new List<OpportunityFieldHistory>();
            var so = await client.QueryAsync<OpportunityFieldHistory>(string.Format(SalesforceService.GetAppSetting("Req:opportunityfieldhistory"), startDate, execDate));
            opportunityhistories.AddRange(so.Records);
            var nextRecordsUrl = so.NextRecordsUrl;
            if (!string.IsNullOrEmpty(nextRecordsUrl))
            {
                while (true)
                {
                    so = await client.QueryContinuationAsync<OpportunityFieldHistory>(nextRecordsUrl);
                    opportunityhistories.AddRange(so.Records);
                    if (string.IsNullOrEmpty(so.NextRecordsUrl))
                        break;
                    nextRecordsUrl = so.NextRecordsUrl;
                }
            }
            #endregion

            using (var db = new sfDataHub())
            {
                #region WK_SF_OPPORTUNITYFIELDHISTORY
                opportunityhistories.ForEach(x =>
                   db.WK_SF_OPPORTUNITYFIELDHISTORY.Add(new WK_SF_OPPORTUNITYFIELDHISTORY
                   {
                       Id_SF = x.Id,
                       CreatedByID = x.CreatedByID,
                       CreatedDate = x.CreatedDate.ToUniversalTime(),
                       Field = x.Field,
                       IsDeleted = x.IsDeleted,
                       NewValue = x.NewValue,
                       OldValue = x.OldValue,
                       OpportunityId = x.OpportunityId
                   })
                );
                #endregion

                await db.SaveChangesAsync();    //save WK_SF_OPPORTUNITYFIELDHISTORY
            }

            UpdateLog(idopphistory, null, opportunityhistories.Count());

            //UpdateLog(idopphistory, opportunityhistories.Count(), null, "COLLECT");
            //HandleOpportunities(lastExtract, idopphistory, idlogopp);            
        }
        #endregion

        //WK Opportunity field histories
        #region WK_OPPORTUNITYISDELETED
        private static async Task WK_OPPORTUNITYISDELETED(string[] args, long idlogopp, DateTime execdateopp)
        {
            //Opportunity Field History
            int nb = 0;
            long idoppisdeleted = CreateLog(new Data_Log { ProcessingType = "COLLECT", ETLName = "PGMRUN_API_RAW_OPPORTUNITYISDELETED", FileOrTableName = "RAW_SF_OPPORTUNITYISDELETED", ProcessingStartDate = DateTime.Now, ProcessingEndDate = null, ProcessingStatus = null, NbRecordsProcessed = null });
            IdOverallProcess = idoppisdeleted;

            //Get the latest SUCCESSFUL SF objects extraction date
            #region Dates
            DateTime lastExtract = GetLastSuccessfulSFExtractDate(args);
            DateTime lastExtractUtc = lastExtract.ToUniversalTime();
            DateTime execDateUtc = execdateopp.ToUniversalTime();
            string startDate = lastExtractUtc.ToString("yyyy-MM-ddTHH:mm:ssZ", CultureInfo.InvariantCulture);
            string execDate = execDateUtc.ToString("yyyy-MM-ddTHH:mm:ssZ", CultureInfo.InvariantCulture);
            #endregion
            //Delete all records before 
            DeleteOppIsDeleted();

            #region API REST
            //Get the access token from the refresh token
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            sfRefreshToken sfAccessToken = await AcquireAccessTokenFromRefreshTokenAsync();
            ForceClient client = sfAccessToken.GetTheForceClient();

            var opportunityisdeleted = new List<OpportunityIsDeleted>();
            var so = await client.QueryAsync<OpportunityIsDeleted>(string.Format(SalesforceService.GetAppSetting("Req:opportunityisdeleted")));
            opportunityisdeleted.AddRange(so.Records);
            var nextRecordsUrl = so.NextRecordsUrl;
            if (!string.IsNullOrEmpty(nextRecordsUrl))
            {
                while (true)
                {
                    so = await client.QueryContinuationAsync<OpportunityIsDeleted>(nextRecordsUrl);
                    opportunityisdeleted.AddRange(so.Records);
                    if (string.IsNullOrEmpty(so.NextRecordsUrl))
                        break;
                    nextRecordsUrl = so.NextRecordsUrl;
                }
            }
            #endregion

            using (var db = new sfDataHub())
            {
                #region WK_SF_OPPORTUNITYISDELETED
                opportunityisdeleted.ForEach(x =>
                   db.WK_SF_OPPORTUNITYISDELETED.Add(new WK_SF_OPPORTUNITYISDELETED
                   {
                       Id_SF = x.Id,
                       IsDeleted = x.IsDeleted
                   })
                );
                #endregion

                await db.SaveChangesAsync();    //save WK_SF_OPPORTUNITYISDELETED

                //RAW_SF_OPPORTUNITYISDELETED
                db.Database.CommandTimeout = Convert.ToInt32(SalesforceService.GetAppSetting("Req:timeout"));
                nb = db.Database.ExecuteSqlCommand(GetSQL_OpportunityIsDeleted("RAW_SF_OPPORTUNITYISDELETED"));
            }

            UpdateLog(idoppisdeleted, nb, opportunityisdeleted.Count());
        }
        #endregion
        
        //Not used anymore - INSERT INTO RAW_SF_Opportunity & RAW_SF_OPPORTUNITYHistory
        #region Not used anymore for handing hisrorized opportunities
        /*
        #region HandleOpportunities, both current & historized data
        private static void HandleOpportunities(DateTime lastExtract, long idopphistory, long idlogopp)
        {
            IdOverallProcess = idlogopp;
            using (var db = new sfDataHub())
            {
                //table containing all tracked fieldhistory delta, ordered by descending date       
                var src1 = db.Set<WK_SF_OPPORTUNITYFIELDHISTORY>(); //the fieldhistory delta
                var src2 = db.Set<param_opportunity_tracking>();
                var qry1 = src1.Join(src2,
                        field => field.Field,
                        track => track.Label,
                        (field, track) => new { raw_SFCRM_OpportunityFieldHistory = field, param_opportunity_tracking = track }
                    ).Select(x => x.raw_SFCRM_OpportunityFieldHistory);
                List<WK_SF_OPPORTUNITYFIELDHISTORY> fieldhistories = qry1.OrderBy(x => x.OpportunityId).ThenByDescending(y => y.CreatedDate).ToList();

                //qry2 = temporarily DELTA collected opportunities 
                List<WK_SF_OPPORTUNITY> qry2 = db.Set<WK_SF_OPPORTUNITY>().ToList();
                //qry3 = get the existing record before the change (get the lastmodified date for historization), an opportuniuty only lasts 90 days
                List<RAW_SF_OPPORTUNITY> qry3 = db.Set<RAW_SF_OPPORTUNITY>().Where(x => x.LastModifiedDate > DbFunctions.AddDays(lastExtract, -90) && x.LastModifiedDate<= lastExtract).ToList();

                //INSERT INTO RAW_SF_OPPORTUNITYHistory
                string id_opportunity = null;
                int n = 0;

                WK_SF_OPPORTUNITYHistory v = null;

                fieldhistories.ForEach(x =>
                {
                    //first time or if we move to another opportunity
                    if (id_opportunity == null || !id_opportunity.Equals(x.OpportunityId))
                    {
                        id_opportunity = x.OpportunityId;
                        //get the collected opportunities WK_SF_OPPORTUNITY
                        WK_SF_OPPORTUNITY o = qry2.Where(y => y.Id_SF.Equals(x.OpportunityId)).FirstOrDefault();
                        //WK_SF_OPPORTUNITYHistory v = new WK_SF_OPPORTUNITYHistory
                        v = new WK_SF_OPPORTUNITYHistory
                        {
                            #region Fields
                            Id_SF_FldHistOpp = x.Id_SF,
                            Id_SF = x.OpportunityId,
                            AccountId = x.Field.Equals("Account") ? x.OldValue : o.AccountId,
                            AdvisorCode__c = x.Field.Equals("AdvisorCode__c") ? x.OldValue : o.AdvisorCode__c,
                            CampaignId__c = o.CampaignId__c,
                            CloseDate = x.Field.Equals("CloseDate") ? DateTime.Parse(x.OldValue) : o.CloseDate,
                            CommercialOfferCode__c = x.Field.Equals("CommercialOfferCode__c") ? x.OldValue : o.CommercialOfferCode__c,
                            CommercialOfferName__c = x.Field.Equals("CommercialOfferName__c") ? x.OldValue : o.CommercialOfferName__c,
                            CompleteFileFlag__c = o.CompleteFileFlag__c,
                            CreatedById = x.CreatedByID,
                            CreatedDate = x.CreatedDate,
                            DeniedOpportunityReason__c = x.Field.Equals("DeniedOpportunityReason__c") ? x.OldValue : o.DeniedOpportunityReason__c,
                            DistributionChannel__c = x.Field.Equals("DistributionChannel__c") ? x.OldValue : o.DistributionChannel__c,
                            DistributorEntity__c = o.DistributorEntity__c,
                            DistributorNetwork__c = x.Field.Equals("DistributorNetwork__c") ? x.OldValue : o.DistributorNetwork__c,
                            FinalizedBy__c = x.Field.Equals("FinalizedBy__c") ? x.OldValue : o.FinalizedBy__c,
                            FormValidation__c = x.Field.Equals("FormValidation__c") ? ParseDate(x.OldValue) : o.FormValidation__c,
                            IDOppty__c = o.IDOppty__c,
                            IDProcessSous__c = x.Field.Equals("IDProcessSous__c") ? x.OldValue : o.IDProcessSous__c,
                            IdSource__c = o.IdSource__c,
                            Indication__c = x.Field.Equals("Indication__c") ? x.OldValue : o.Indication__c,
                            LastModifiedDate = o.LastModifiedDate,
                            LeadSource = x.Field.Equals("LeadSource") ? x.OldValue : o.LeadSource,
                            Manager__c = o.Manager__c,
                            NoIndication__c = x.Field.Equals("NoIndication__c") ? x.OldValue : o.NoIndication__c,
                            OwnerId = x.Field.Equals("Owner") ? x.OldValue : o.OwnerId,
                            PointOfSaleCode__c = x.Field.Equals("PointOfSaleCode__c") ? x.OldValue : o.PointOfSaleCode__c,
                            ProductFamilyCode__c = x.Field.Equals("ProductFamilyCode__c") ? x.OldValue : o.ProductFamilyCode__c,
                            ProductFamilyName__c = x.Field.Equals("ProductFamilyName__c") ? x.OldValue : o.ProductFamilyName__c,
                            ProvenanceIndicationIndicatorId__c = o.ProvenanceIndicationIndicatorId__c,
                            RecordTypeId = o.RecordTypeId,
                            RejectReason__c = x.Field.Equals("RejectReason__c") ? x.OldValue : o.RejectReason__c,
                            RelationEntryScore__c = o.RelationEntryScore__c,
                            StageName = x.Field.Equals("StageName") ? x.OldValue : o.StageName,
                            StartedChannel__c = x.Field.Equals("StartedChannel__c") ? x.OldValue : o.StartedChannel__c,
                            SystemModstamp = o.SystemModstamp,
                            Type = o.Type,
                            IsWon = o.IsWon,
                            Validity_StartDate = GetValidityStartDate(fieldhistories.ElementAtOrDefault(n + 1), qry3, x.OpportunityId, qry2),
                            Validity_EndDate = x.CreatedDate
                            #endregion
                        };
                        //db.Entry<WK_SF_OPPORTUNITYHistory>(v).State = EntityState.Added;
                        db.WK_SF_OPPORTUNITYHistory.Add(v);
                    }
                    else
                    {
                        //get the previous history, for creating the period
                        //WK_SF_OPPORTUNITYHistory o = db.WK_SF_OPPORTUNITYHistory.Local[n - 1];
                        WK_SF_OPPORTUNITYHistory o = v;
                        //WK_SF_OPPORTUNITYHistory v = new WK_SF_OPPORTUNITYHistory
                        v = new WK_SF_OPPORTUNITYHistory
                        {
                            #region Fields
                            Id_SF_FldHistOpp = x.Id_SF,
                            Id_SF = x.OpportunityId,
                            AccountId = x.Field.Equals("Account") ? x.OldValue : o.AccountId,
                            AdvisorCode__c = x.Field.Equals("AdvisorCode__c") ? x.OldValue : o.AdvisorCode__c,
                            CampaignId__c = o.CampaignId__c,
                            CloseDate = x.Field.Equals("CloseDate") ? DateTime.Parse(x.OldValue) : o.CloseDate,
                            CommercialOfferCode__c = x.Field.Equals("CommercialOfferCode__c") ? x.OldValue : o.CommercialOfferCode__c,
                            CommercialOfferName__c = x.Field.Equals("CommercialOfferName__c") ? x.OldValue : o.CommercialOfferName__c,
                            CompleteFileFlag__c = o.CompleteFileFlag__c,
                            CreatedById = x.CreatedByID,
                            CreatedDate = x.CreatedDate,
                            DeniedOpportunityReason__c = x.Field.Equals("DeniedOpportunityReason__c") ? x.OldValue : o.DeniedOpportunityReason__c,
                            DistributionChannel__c = x.Field.Equals("DistributionChannel__c") ? x.OldValue : o.DistributionChannel__c,
                            DistributorEntity__c = o.DistributorEntity__c,
                            DistributorNetwork__c = x.Field.Equals("DistributorNetwork__c") ? x.OldValue : o.DistributorNetwork__c,
                            FinalizedBy__c = x.Field.Equals("FinalizedBy__c") ? x.OldValue : o.FinalizedBy__c,
                            FormValidation__c = x.Field.Equals("FormValidation__c") ? ParseDate(x.OldValue) : o.FormValidation__c,
                            IDOppty__c = o.IDOppty__c,
                            IDProcessSous__c = x.Field.Equals("IDProcessSous__c") ? x.OldValue : o.IDProcessSous__c,
                            IdSource__c = o.IdSource__c,
                            Indication__c = x.Field.Equals("Indication__c") ? x.OldValue : o.Indication__c,
                            LastModifiedDate = o.LastModifiedDate,
                            LeadSource = x.Field.Equals("LeadSource") ? x.OldValue : o.LeadSource,
                            Manager__c = o.Manager__c,
                            NoIndication__c = x.Field.Equals("NoIndication__c") ? x.OldValue : o.NoIndication__c,
                            OwnerId = x.Field.Equals("Owner") ? x.OldValue : o.OwnerId,
                            PointOfSaleCode__c = x.Field.Equals("PointOfSaleCode__c") ? x.OldValue : o.PointOfSaleCode__c,
                            ProductFamilyCode__c = x.Field.Equals("ProductFamilyCode__c") ? x.OldValue : o.ProductFamilyCode__c,
                            ProductFamilyName__c = x.Field.Equals("ProductFamilyName__c") ? x.OldValue : o.ProductFamilyName__c,
                            ProvenanceIndicationIndicatorId__c = o.ProvenanceIndicationIndicatorId__c,
                            RecordTypeId = o.RecordTypeId,
                            RejectReason__c = x.Field.Equals("RejectReason__c") ? x.OldValue : o.RejectReason__c,
                            RelationEntryScore__c = o.RelationEntryScore__c,
                            StageName = x.Field.Equals("StageName") ? x.OldValue : o.StageName,
                            StartedChannel__c = x.Field.Equals("StartedChannel__c") ? x.OldValue : o.StartedChannel__c,
                            SystemModstamp = o.SystemModstamp,
                            Type = o.Type,
                            IsWon = o.IsWon,
                            Validity_StartDate = GetValidityStartDate(fieldhistories.ElementAtOrDefault(n + 1), qry3, x.OpportunityId, qry2),
                            Validity_EndDate = x.CreatedDate
                            #endregion
                        };
                        //db.Entry<WK_SF_OPPORTUNITYHistory>(v).State = EntityState.Added;
                        db.WK_SF_OPPORTUNITYHistory.Add(v);
                    }
                    n++;
                }
                );
                
                //WK_SF_OPPORTUNITYHistory
                db.SaveChanges();

                db.Database.CommandTimeout = Convert.ToInt32(SalesforceService.GetAppSetting("Req:timeout"));
                //RAW_SF_OPPORTUNITYHistory
                int nb1 = db.Database.ExecuteSqlCommand(GetSQL_OpportunityHistory("RAW_SF_OPPORTUNITYHistory"));
                //RAW_SF_OPPORTUNITY
                int nb2 = db.Database.ExecuteSqlCommand(GetSQL_Opportunity("RAW_SF_OPPORTUNITY"));

                UpdateLog(idopphistory, nb1, "OK", "INSERTION_RAW");
                UpdateLog(idlogopp, nb2, "OK", "INSERTION_RAW");
            }
        }
        #endregion

        #region GetValidityStartDate for opportunity histories
        private static DateTime GetValidityStartDate(WK_SF_OPPORTUNITYFIELDHISTORY f, List<RAW_SF_OPPORTUNITY> qry3, string OpportunityId, List<WK_SF_OPPORTUNITY> qry2)
        {
            WK_SF_OPPORTUNITY q;    //opportunity of the delta collected opportunities (qry2)
            RAW_SF_OPPORTUNITY r;    //to get the date before the collect
            //WK_SF_OPPORTUNITYFIELDHISTORY f = fieldhistories.ElementAtOrDefault(n + 1);  //next history (ordered by descending) to get the created date
            if (f != null)
            {
                if (f.OpportunityId.Equals(OpportunityId))  //reverse getting the created date   
                    return f.CreatedDate;
                //otherwise, we have moved to another history (opportunity)
                else
                {
                    //get the created date of the oppportunity for the last history (ordered by descending)
                    q = qry2.Where(y => y.Id_SF.Equals(OpportunityId)).FirstOrDefault();
                    if (q == null)
                    {
                        //get the date in the RAW_SF_OPPORTUNITY before the collect
                        r = qry3.Where(y => y.Id_SF.Equals(OpportunityId)).FirstOrDefault();
                        if (r == null)
                            return DateTime.Parse("1901-01-01 00:00:00");
                        else
                            return r.LastModifiedDate ?? DateTime.Parse("1901-01-01 00:00:00");
                    }
                    else return q.CreatedDate ?? DateTime.Parse("1901-01-01 00:00:00");
                }
            }
            else
            {
                //get the created date of the oppportunity for the last history (ordered by descending)
                q = qry2.Where(y => y.Id_SF.Equals(OpportunityId)).FirstOrDefault();
                if (q == null)
                {
                    //get the date in the RAW_SF_OPPORTUNITY before the collect
                    r = qry3.Where(y => y.Id_SF.Equals(OpportunityId)).FirstOrDefault();
                    if (r == null)
                        return DateTime.Parse("1901-01-01 00:00:00");
                    else
                        return r.LastModifiedDate ?? DateTime.Parse("1901-01-01 00:00:00");
                }
                else return q.CreatedDate ?? DateTime.Parse("1901-01-01 00:00:00");
            }
        }
        #endregion
        */
        #endregion
        //------------------------------------------------------------------------------------------------------------

        //Users----------------------------------------------------------------------------------------------------
        #region RAW_USER
        private static async Task RAW_USER(string[] args)
        {
            int nb = 0;
            long id = CreateLog(new Data_Log { ProcessingType = "COLLECT", ETLName = "PGMRUN_API_RAW_USER", FileOrTableName = "RAW_SF_USER", ProcessingStartDate = DateTime.Now, ProcessingEndDate = null, ProcessingStatus = null, NbRecordsProcessed = null, NbRecordsCollected = null });
            IdOverallProcess = id;

            #region Dates
            //Get the latest SUCCESSFUL SF objects extraction date
            DateTime lastExtract = GetLastSuccessfulSFExtractDate(args);
            DateTime lastExtractUtc = lastExtract.ToUniversalTime();
            string startDate = lastExtractUtc.ToString("yyyy-MM-ddTHH:mm:ssZ", CultureInfo.InvariantCulture);
            #endregion
            //Delete all records before
            DeleteWKRecords(args);

            #region API REST
            //Get the access token from the refresh token
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            sfRefreshToken sfAccessToken = await AcquireAccessTokenFromRefreshTokenAsync();
            ForceClient client = sfAccessToken.GetTheForceClient();

            var users = new List<User>();
            var so = await client.QueryAsync<User>(string.Format(SalesforceService.GetAppSetting("Req:user"), startDate));
            users.AddRange(so.Records);
            var nextRecordsUrl = so.NextRecordsUrl;
            if (!string.IsNullOrEmpty(nextRecordsUrl))
            {
                while (true)
                {
                    so = await client.QueryContinuationAsync<User>(nextRecordsUrl);
                    users.AddRange(so.Records);
                    if (string.IsNullOrEmpty(so.NextRecordsUrl))
                        break;
                    nextRecordsUrl = so.NextRecordsUrl;
                }
            }
            #endregion

            using (var db = new sfDataHub())
            {
                #region WK_SF_USER
                users.ForEach(x =>
                   db.WK_SF_USER.Add(new WK_SF_USER
                   {
                       Id_SF = x.Id,
                       Firstname = x.Firstname,
                       CreatedDate = x.CreatedDate.Value.ToUniversalTime(),
                       Lastname = x.Lastname,
                       Title = x.Title,
                       Phone = x.Phone,
                       Email = x.Email,
                       City = x.City,
                       Country = x.Country,
                       CountryCode = x.CountryCode,
                       Latitude = x.Latitude,
                       LastModifiedDate = x.LastModifiedDate.Value.ToUniversalTime(),
                       Longitude = x.Longitude,
                       PostalCode = x.PostalCode,
                       State = x.State,
                       StateCode = x.StateCode,
                       Street = x.Street,
                       UserRoleId = x.UserRoleId,
                       ProfileId = x.ProfileId,
                       QSProfile__c = x.QSProfile__c,
                       GenesysUserId__c = x.GenesysUserId__c
                   })
                );
                #endregion

                //save WK_SF_USER
                await db.SaveChangesAsync();
                //save RAW
                db.Database.CommandTimeout = Convert.ToInt32(SalesforceService.GetAppSetting("Req:timeout"));
                nb = db.Database.ExecuteSqlCommand(GetSQL_User("RAW_SF_USER"));
            }

            UpdateLog(id, nb, users.Count());
        }
        #endregion

        //Tasks-------------------------------------------------------------------------------------------------
        #region RAW_TASK
        private static async Task RAW_TASK(string[] args)
        {
            int nb = 0;
            long id = CreateLog(new Data_Log { ProcessingType = "COLLECT", ETLName = "PGMRUN_API_RAW_TASK", FileOrTableName = "RAW_SF_TASK", ProcessingStartDate = DateTime.Now, ProcessingEndDate = null, ProcessingStatus = null, NbRecordsProcessed = null, NbRecordsCollected = null });
            IdOverallProcess = id;

            #region Dates
            //Get the latest SUCCESSFUL SF objects extraction date
            DateTime lastExtract = GetLastSuccessfulSFExtractDate(args);
            DateTime lastExtractUtc = lastExtract.ToUniversalTime();
            string startDate = lastExtractUtc.ToString("yyyy-MM-ddTHH:mm:ssZ", CultureInfo.InvariantCulture);
            #endregion
            //Delete all records before
            DeleteWKRecords(args);

            #region API REST
            //Get the access token from the refresh token
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            sfRefreshToken sfAccessToken = await AcquireAccessTokenFromRefreshTokenAsync();
            ForceClient client = sfAccessToken.GetTheForceClient();

            var tasks = new List<TaskSF>();
            var so = await client.QueryAsync<TaskSF>(string.Format(SalesforceService.GetAppSetting("Req:task"), startDate));
            tasks.AddRange(so.Records);
            var nextRecordsUrl = so.NextRecordsUrl;
            if (!string.IsNullOrEmpty(nextRecordsUrl))
            {
                while (true)
                {
                    so = await client.QueryContinuationAsync<TaskSF>(nextRecordsUrl);
                    tasks.AddRange(so.Records);
                    if (string.IsNullOrEmpty(so.NextRecordsUrl))
                        break;
                    nextRecordsUrl = so.NextRecordsUrl;
                }
            }
            #endregion

            using (var db = new sfDataHub())
            {
                #region WK_SF_TASK
                tasks.ForEach(x =>
                    db.WK_SF_TASK.Add(new WK_SF_TASK
                    {
                        Id_SF = x.Id,
                        RecordTypeId = x.RecordTypeId,
                        WhatId = x.WhatId,
                        OwnerId = x.OwnerId,
                        Description = x.Description,
                        WhoId = x.WhoId,
                        Subject = x.Subject,
                        InteractionId__c = x.InteractionId__c,
                        CreatedById = x.CreatedById,
                        CreatedDate = x.CreatedDate.Value.ToUniversalTime(),
                        LastModifiedDate = x.LastModifiedDate.Value.ToUniversalTime(),
                        TaskSubType = x.TaskSubType,
                        Priority = x.Priority,
                        Status = x.Status,
                        Family__c = x.Family__c,
                        SubFamily__c = x.SubFamily__c,
                        InteractionDate__c = x.InteractionDate__c,
                        ScoreOfInteraction__c = x.ScoreOfInteraction__c,
                        NotationLabel__c = x.NotationLabel__c,
                        ResponseLink__c = x.ResponseLink__c,
                        ActivityDate = x.ActivityDate,
                        ConversationID__c = x.ConversationID__c,
                        CallType__c = x.CallType__c
                    })
                );
                #endregion
                //save WK_SF_TASK
                await db.SaveChangesAsync();
                //save RAW
                db.Database.CommandTimeout = Convert.ToInt32(SalesforceService.GetAppSetting("Req:timeout"));
                nb = db.Database.ExecuteSqlCommand(GetSQL_Task("RAW_SF_TASK"));
            }

            UpdateLog(id, nb, tasks.Count());
        }
        #endregion

        #region RAW_EMAILMESSAGE
        private static async Task RAW_EMAILMESSAGE(string[] args)
        {
            int nb = 0;
            long id = CreateLog(new Data_Log { ProcessingType = "COLLECT", ETLName = "PGMRUN_API_RAW_EMAILMESSAGE", FileOrTableName = "RAW_SF_EMAILMESSAGE", ProcessingStartDate = DateTime.Now, ProcessingEndDate = null, ProcessingStatus = null, NbRecordsProcessed = null, NbRecordsCollected = null });
            IdOverallProcess = id;

            #region Dates
            //Get the latest SUCCESSFUL SF objects extraction date
            DateTime lastExtract = GetLastSuccessfulSFExtractDate(args);
            DateTime lastExtractUtc = lastExtract.ToUniversalTime();
            string startDate = lastExtractUtc.ToString("yyyy-MM-ddTHH:mm:ssZ", CultureInfo.InvariantCulture);
            #endregion
            //Delete all records before
            DeleteWKRecords(args);

            #region API REST
            //Get the access token from the refresh token
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            sfRefreshToken sfAccessToken = await AcquireAccessTokenFromRefreshTokenAsync();
            ForceClient client = sfAccessToken.GetTheForceClient();

            var emails = new List<EmailMessage>();
            var so = await client.QueryAsync<EmailMessage>(string.Format(SalesforceService.GetAppSetting("Req:emailmessage"), startDate));
            emails.AddRange(so.Records);
            var nextRecordsUrl = so.NextRecordsUrl;
            if (!string.IsNullOrEmpty(nextRecordsUrl))
            {
                while (true)
                {
                    so = await client.QueryContinuationAsync<EmailMessage>(nextRecordsUrl);
                    emails.AddRange(so.Records);
                    if (string.IsNullOrEmpty(so.NextRecordsUrl))
                        break;
                    nextRecordsUrl = so.NextRecordsUrl;
                }
            }
            #endregion

            using (var db = new sfDataHub())
            {
                #region WK_SF_EMAILMESSAGE
                emails.ForEach(x =>
                    db.WK_SF_EMAILMESSAGE.Add(new WK_SF_EMAILMESSAGE
                    {
                        Id_SF = x.Id,
                        FromAddress = x.FromAddress,
                        FromName = x.FromName,
                        ParentId = x.ParentId,
                        Subject = x.Subject,
                        ToAddress = x.ToAddress,
                        CcAddress = x.CcAddress,
                        BccAddress = x.BccAddress,
                        TextBody = x.TextBody,
                        HtmlBody = x.HtmlBody,
                        CreatedById = x.CreatedById,
                        Incoming = x.Incoming,
                        MessageDate = x.MessageDate,
                        RelatedToId = x.RelatedToId,
                        CreatedDate = x.CreatedDate.Value.ToUniversalTime(),
                        LastModifiedDate = x.LastModifiedDate.Value.ToUniversalTime()
                    })
                );
                #endregion
                //save WK_SF_EMAILMESSAGE
                await db.SaveChangesAsync();
                //save RAW
                db.Database.CommandTimeout = Convert.ToInt32(SalesforceService.GetAppSetting("Req:timeout"));
                nb = db.Database.ExecuteSqlCommand(GetSQL_EmailMessage("RAW_SF_EMAILMESSAGE"));
            }

            UpdateLog(id, nb, emails.Count());
        }
        #endregion

        #region RAW_LIVECHATTRANSCRIPT
        private static async Task RAW_LIVECHATTRANSCRIPT(string[] args)
        {
            int nb = 0;
            long id = CreateLog(new Data_Log { ProcessingType = "COLLECT", ETLName = "PGMRUN_API_RAW_LIVECHATTRANSCRIPT", FileOrTableName = "RAW_SF_LIVECHATTRANSCRIPT", ProcessingStartDate = DateTime.Now, ProcessingEndDate = null, ProcessingStatus = null, NbRecordsProcessed = null, NbRecordsCollected = null });
            IdOverallProcess = id;

            #region Dates
            //Get the latest SUCCESSFUL SF objects extraction date
            DateTime lastExtract = GetLastSuccessfulSFExtractDate(args);
            DateTime lastExtractUtc = lastExtract.ToUniversalTime();
            string startDate = lastExtractUtc.ToString("yyyy-MM-ddTHH:mm:ssZ", CultureInfo.InvariantCulture);
            #endregion
            //Delete all records before
            DeleteWKRecords(args);

            #region API REST
            //Get the access token from the refresh token
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            sfRefreshToken sfAccessToken = await AcquireAccessTokenFromRefreshTokenAsync();
            ForceClient client = sfAccessToken.GetTheForceClient();

            var chats = new List<LiveChatTranscript>();
            var so = await client.QueryAsync<LiveChatTranscript>(string.Format(SalesforceService.GetAppSetting("Req:livechattranscript"), startDate));
            chats.AddRange(so.Records);
            var nextRecordsUrl = so.NextRecordsUrl;
            if (!string.IsNullOrEmpty(nextRecordsUrl))
            {
                while (true)
                {
                    so = await client.QueryContinuationAsync<LiveChatTranscript>(nextRecordsUrl);
                    chats.AddRange(so.Records);
                    if (string.IsNullOrEmpty(so.NextRecordsUrl))
                        break;
                    nextRecordsUrl = so.NextRecordsUrl;
                }
            }
            #endregion

            using (var db = new sfDataHub())
            {
                #region WK_SF_LIVECHATTRANSCRIPT
                chats.ForEach(x =>
                    db.WK_SF_LIVECHATTRANSCRIPT.Add(new WK_SF_LIVECHATTRANSCRIPT
                    {
                        Id_SF = x.Id,
                        StartTime = x.StartTime,
                        OwnerId = x.OwnerId,
                        CaseId = x.CaseId,
                        AccountId = x.AccountId,
                        Subject__c = x.Subject__c,
                        ChatDuration = x.ChatDuration,
                        WaitTime = x.WaitTime,                        
                        ConversationID__c = x.ConversationID__c,                        
                        CreatedDate = x.CreatedDate.Value.ToUniversalTime(),
                        LastModifiedDate = x.LastModifiedDate.Value.ToUniversalTime()
                    })
                );
                #endregion
                //save WK_SF_LIVECHATTRANSCRIPT
                await db.SaveChangesAsync();
                //save RAW
                db.Database.CommandTimeout = Convert.ToInt32(SalesforceService.GetAppSetting("Req:timeout"));
                nb = db.Database.ExecuteSqlCommand(GetSQL_LiveChatTranscript("RAW_SF_LIVECHATTRANSCRIPT"));
            }

            UpdateLog(id, nb, chats.Count());
        }
        #endregion

        //Profiles-------------------------------------------------------------------------------------------------
        #region RAW_PROFILE
        private static async Task RAW_PROFILE(string[] args)
        {
            int nb = 0;
            long id = CreateLog(new Data_Log { ProcessingType = "COLLECT", ETLName = "PGMRUN_API_RAW_PROFILE", FileOrTableName = "RAW_SF_PROFILE", ProcessingStartDate = DateTime.Now, ProcessingEndDate = null, ProcessingStatus = null, NbRecordsProcessed = null, NbRecordsCollected = null });
            IdOverallProcess = id;

            #region Dates
            //Get the latest SUCCESSFUL SF objects extraction date
            DateTime lastExtract = GetLastSuccessfulSFExtractDate(args);
            DateTime lastExtractUtc = lastExtract.ToUniversalTime();
            string startDate = lastExtractUtc.ToString("yyyy-MM-ddTHH:mm:ssZ", CultureInfo.InvariantCulture);
            #endregion
            //Delete all records before
            DeleteWKRecords(args);

            #region API REST
            //Get the access token from the refresh token
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            sfRefreshToken sfAccessToken = await AcquireAccessTokenFromRefreshTokenAsync();
            ForceClient client = sfAccessToken.GetTheForceClient();

            var profiles = new List<Profile>();
            var so = await client.QueryAsync<Profile>(string.Format(SalesforceService.GetAppSetting("Req:profile"), startDate));
            profiles.AddRange(so.Records);
            var nextRecordsUrl = so.NextRecordsUrl;
            if (!string.IsNullOrEmpty(nextRecordsUrl))
            {
                while (true)
                {
                    so = await client.QueryContinuationAsync<Profile>(nextRecordsUrl);
                    profiles.AddRange(so.Records);
                    if (string.IsNullOrEmpty(so.NextRecordsUrl))
                        break;
                    nextRecordsUrl = so.NextRecordsUrl;
                }
            }
            #endregion

            using (var db = new sfDataHub())
            {
                #region WK_SF_PROFILE
                profiles.ForEach(x =>
                   db.WK_SF_PROFILE.Add(new WK_SF_PROFILE
                   {
                       Id_SF = x.Id,
                       Name = x.Name,
                       CreatedDate = x.CreatedDate.Value.ToUniversalTime(),
                       LastModifiedDate = x.LastModifiedDate.Value.ToUniversalTime()
                   })
                );
                #endregion

                //save WK_SF_PROFILE
                await db.SaveChangesAsync();
                //save RAW
                db.Database.CommandTimeout = Convert.ToInt32(SalesforceService.GetAppSetting("Req:timeout"));
                nb = db.Database.ExecuteSqlCommand(GetSQL_Profile("RAW_SF_PROFILE"));
            }

            UpdateLog(id, nb, profiles.Count());
        }
        #endregion

        //Contacts-------------------------------------------------------------------------------------------------
        #region RAW_CONTACT
        private static async Task RAW_CONTACT(string[] args)
        {
            int nb = 0;
            long id = CreateLog(new Data_Log { ProcessingType = "COLLECT", ETLName = "PGMRUN_API_RAW_CONTACT", FileOrTableName = "RAW_SF_CONTACT", ProcessingStartDate = DateTime.Now, ProcessingEndDate = null, ProcessingStatus = null, NbRecordsProcessed = null, NbRecordsCollected = null });
            IdOverallProcess = id;

            #region Dates
            //Get the latest SUCCESSFUL SF objects extraction date
            DateTime lastExtract = GetLastSuccessfulSFExtractDate(args);
            DateTime lastExtractUtc = lastExtract.ToUniversalTime();
            string startDate = lastExtractUtc.ToString("yyyy-MM-ddTHH:mm:ssZ", CultureInfo.InvariantCulture);
            #endregion
            //Delete all records before
            DeleteWKRecords(args);

            #region API REST
            //Get the access token from the refresh token
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            sfRefreshToken sfAccessToken = await AcquireAccessTokenFromRefreshTokenAsync();
            ForceClient client = sfAccessToken.GetTheForceClient();

            var contacts = new List<Contact>();
            var so = await client.QueryAsync<Contact>(string.Format(SalesforceService.GetAppSetting("Req:contact"), startDate));
            contacts.AddRange(so.Records);
            var nextRecordsUrl = so.NextRecordsUrl;
            if (!string.IsNullOrEmpty(nextRecordsUrl))
            {
                while (true)
                {
                    so = await client.QueryContinuationAsync<Contact>(nextRecordsUrl);
                    contacts.AddRange(so.Records);
                    if (string.IsNullOrEmpty(so.NextRecordsUrl))
                        break;
                    nextRecordsUrl = so.NextRecordsUrl;
                }
            }
            #endregion

            using (var db = new sfDataHub())
            {
                #region WK_SF_CONTACT
                contacts.ForEach(x =>
                   db.WK_SF_CONTACT.Add(new WK_SF_CONTACT
                   {
                       Id_SF = x.Id,
                       Name = x.Name,
                       CreatedDate = x.CreatedDate.Value.ToUniversalTime(),
                       LastModifiedDate = x.LastModifiedDate.Value.ToUniversalTime(),
                       Email = x.Email,
                       IOBSPStart__c = x.IOBSPStart__c.HasValue ? x.IOBSPStart__c.Value.ToUniversalTime() : x.IOBSPStart__c,
                       IOBSPEnd__c = x.IOBSPEnd__c.HasValue ? x.IOBSPEnd__c.Value.ToUniversalTime() : x.IOBSPEnd__c,
                       Phone = x.Phone,
                       SellerHashedCuid__c = x.SellerHashedCuid__c                     
                   })
                );
                #endregion

                //save WK_SF_CONTACT
                await db.SaveChangesAsync();
                //save RAW
                db.Database.CommandTimeout = Convert.ToInt32(SalesforceService.GetAppSetting("Req:timeout"));
                nb = db.Database.ExecuteSqlCommand(GetSQL_Contact("RAW_SF_CONTACT"));
            }

            UpdateLog(id, nb, contacts.Count());
        }
        #endregion

        //Referentiel PicklistValueInfo
        #region RAW_REFERENTIEL
        private static async Task RAW_REFERENTIEL(string[] args)
        {
            int nbc = 0;
            int nbp = 0;
            long id = CreateLog(new Data_Log { ProcessingType = "COLLECT", ETLName = "PGMRUN_API_RAW_REFERENTIEL", FileOrTableName = "RAW_SF_REFERENTIEL", ProcessingStartDate = DateTime.Now, ProcessingEndDate = null, ProcessingStatus = null, NbRecordsProcessed = null, NbRecordsCollected = null });
            IdOverallProcess = id;
            
            //Delete all records before
            DeleteWKRecords(args);

            //Get the access token from the refresh token
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            sfRefreshToken sfAccessToken = await AcquireAccessTokenFromRefreshTokenAsync();
            
            //QUERY: Retrieve info for a given object
            string restQuery = sfAccessToken.InstanceUrl + "/services/data/" + sfAccessToken.ApiVersion + "/sobjects/#object#/describe";

            foreach (string sObjectSF in SalesforceService.GetAppSetting("List:referentiel").Split(';'))
            {
                int nbc_tmp = await RAW_REFERENTIEL_OBJECT(sObjectSF, restQuery.Replace("#object#", sObjectSF), sfAccessToken);
                nbc = nbc + nbc_tmp;
            }

            using (var db = new sfDataHub())
            {
                //save RAW
                db.Database.CommandTimeout = Convert.ToInt32(SalesforceService.GetAppSetting("Req:timeout"));
                nbp = db.Database.ExecuteSqlCommand(GetSQL_Referentiel("RAW_SF_REFERENTIEL"));
            }

            UpdateLog(id, nbp, nbc);
        }
        #endregion

        //Referentiel PicklistValueInfo
        #region RAW_REFERENTIEL_OBJECT
        private static async Task<int> RAW_REFERENTIEL_OBJECT(string sObjectSF, string sRestQuery, sfRefreshToken sfAccessToken)
        {
            int result = 0;

            //ForceClient client = sfAccessToken.GetTheForceClient();               
            HttpClient queryClient = new HttpClient();
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, sRestQuery);

            //add token to header
            request.Headers.Add("Authorization", "Bearer " + sfAccessToken.AccessToken);

            //return XML to the caller
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));

            //call endpoint async
            HttpResponseMessage response = await queryClient.SendAsync(request);

            string xmlresult = await response.Content.ReadAsStringAsync();
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(xmlresult);

            using (var db = new sfDataHub())
            {
                string sPrevFieldNameSF = "";
                int iIdxSF = 0;

                byte[] mapArrayValidFor;
                StringBuilder mapValidFor = new StringBuilder();

                foreach (XmlNode picklistvalue in xmldoc.SelectNodes("//picklistValues"))
                {
                    result = result + 1;

                    string sFieldNameSF = picklistvalue.ParentNode.SelectSingleNode("name").InnerText;
                    string sFieldLabelSF = picklistvalue.ParentNode.SelectSingleNode("label").InnerText;
                    string sCodeSF = picklistvalue.SelectSingleNode("value").InnerText;
                    string sLabelSF = picklistvalue.SelectSingleNode("label").InnerText;
                    string sIsDefault = picklistvalue.SelectSingleNode("defaultValue").InnerText;
                    string sIsActive = picklistvalue.SelectSingleNode("active").InnerText;

                    string sParentFieldNameSF = "";
                    string sValidFor = "";
                    
                    if (picklistvalue.ParentNode.SelectSingleNode("dependentPicklist") != null)
                    {
                        sParentFieldNameSF = picklistvalue.ParentNode.SelectSingleNode("controllerName").InnerText;
                        sValidFor = picklistvalue.SelectSingleNode("validFor").InnerText;
                    }

                    if (sFieldNameSF != sPrevFieldNameSF)
                        iIdxSF = 1;
                    else
                        iIdxSF = iIdxSF + 1;

                    sPrevFieldNameSF = sFieldNameSF;

                    // convert the value of validFor into binary array and then concatenate items of array to have one long binary string.
                    mapArrayValidFor = Encoding.UTF8.GetBytes(sValidFor);
                    mapValidFor.Clear();

                    for (int i = 0; i < mapArrayValidFor.Length; i++)
                    {
                        mapValidFor.Append(Convert.ToString(GetBase64Code(mapArrayValidFor[i]), 2).PadLeft(6, '0'));
                    }

                    string sValidForMap = mapValidFor.ToString();
                    string sValidForIdx = "";

                    for (int i = 0; i < sValidForMap.Length; i++)
                    {
                        if (sValidForMap.Substring(i, 1) == "1")
                            sValidForIdx = sValidForIdx + ( sValidForIdx == "" ? "" : ";" ) + Convert.ToString(i+1, 10);
                    }

                    #region WK_SF_REFERENTIEL
                    db.WK_SF_REFERENTIEL.Add(new WK_SF_REFERENTIEL
                    {
                        OBJECT_CODE_SF = sObjectSF,
                        FIELD_CODE_SF = sFieldNameSF,
                        FIELD_NAME_SF = sFieldLabelSF,
                        PARENT_FIELD_CODE_SF = sParentFieldNameSF,
                        IDX_SF = iIdxSF,
                        CODE_SF = sCodeSF,
                        LIBELLE_SF = sLabelSF,
                        IS_DEFAULT = sIsDefault == "true" ? true : false,
                        IS_ACTIVE = sIsActive == "true" ? true : false,
                        VALID_FOR = sValidForIdx,
                        VALID_FOR_MAP = sValidFor + " --> " + sValidForMap
                    });
                    #endregion
                }
                // save WK_SF_REFERENTIEL
                await db.SaveChangesAsync();
            }
            return result;
        }
        #endregion

        //Referentiel RecordType-------------------------------------------------------------------------------------------------
        #region RAW_RECORDTYPE
        private static async Task RAW_RECORDTYPE(string[] args)
        {
            int nb = 0;
            long id = CreateLog(new Data_Log { ProcessingType = "COLLECT", ETLName = "PGMRUN_API_RAW_RECORDTYPE", FileOrTableName = "RAW_SF_RECORDTYPE", ProcessingStartDate = DateTime.Now, ProcessingEndDate = null, ProcessingStatus = null, NbRecordsProcessed = null, NbRecordsCollected = null });
            IdOverallProcess = id;

            #region Dates
            //Get the latest SUCCESSFUL SF objects extraction date
            //DateTime lastExtract = GetLastSuccessfulSFExtractDate(args);
            //string startDate = lastExtract.ToString("yyyy-MM-ddTHH:mm:ssZ", CultureInfo.InvariantCulture);
            #endregion
            //Delete all records before
            DeleteWKRecords(args);

            #region API REST
            //Get the access token from the refresh token
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            sfRefreshToken sfAccessToken = await AcquireAccessTokenFromRefreshTokenAsync();
            ForceClient client = sfAccessToken.GetTheForceClient();

            var recordtypes = new List<RecordType>();
            var so = await client.QueryAsync<RecordType>(string.Format(SalesforceService.GetAppSetting("Req:recordtype")));
            recordtypes.AddRange(so.Records);
            var nextRecordsUrl = so.NextRecordsUrl;
            if (!string.IsNullOrEmpty(nextRecordsUrl))
            {
                while (true)
                {
                    so = await client.QueryContinuationAsync<RecordType>(nextRecordsUrl);
                    recordtypes.AddRange(so.Records);
                    if (string.IsNullOrEmpty(so.NextRecordsUrl))
                        break;
                    nextRecordsUrl = so.NextRecordsUrl;
                }
            }
            #endregion

            using (var db = new sfDataHub())
            {
                #region WK_SF_RECORDTYPE
                recordtypes.ForEach(x =>
                   db.WK_SF_RECORDTYPE.Add(new WK_SF_RECORDTYPE
                   {
                       Id_SF = x.Id,
                       SobjectType = x.SobjectType,
                       DeveloperName = x.DeveloperName,
                       Name = x.Name,
                       Description = x.Description,
                       NamespacePrefix = x.NamespacePrefix,
                       IsActive = x.IsActive,
                       IsPersonType = x.IsPersonType,
                       BusinessProcessId = x.BusinessProcessId,
                       CreatedById = x.CreatedById,
                       CreatedDate = x.CreatedDate,
                       LastModifiedById = x.LastModifiedById,
                       LastModifiedDate = x.LastModifiedDate,
                       SystemModstamp = x.SystemModstamp
                   })
                );
                #endregion

                //save WK_SF_RECORDTYPE
                await db.SaveChangesAsync();
                //save RAW
                db.Database.CommandTimeout = Convert.ToInt32(SalesforceService.GetAppSetting("Req:timeout"));
                nb = db.Database.ExecuteSqlCommand(GetSQL_RawRecordType("RAW_SF_RECORDTYPE"));
            }

            UpdateLog(id, nb, recordtypes.Count());
        }
        #endregion

        //------------------------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------------------------
        #region PV_ACCOUNT
        private static void PV_ACCOUNT()
        {
            long id = CreateLog(new Data_Log { ProcessingType = "INTEGRATION", ETLName = "PGMRUN_API_PV_ACCOUNT", FileOrTableName = "PV_SF_ACCOUNT", ProcessingStartDate = DateTime.Now, ProcessingEndDate = null, ProcessingStatus = null, NbRecordsProcessed = null, NbRecordsCollected = null });
            IdOverallProcess = id;
            int nb = 0;
            using (var db = new sfDataHub())
            {
                db.Database.CommandTimeout = Convert.ToInt32(SalesforceService.GetAppSetting("Req:timeout"));
                nb = db.Database.ExecuteSqlCommand(GetSQL_Account("PV_SF_ACCOUNT"));
                db.Database.ExecuteSqlCommand("TRUNCATE Table WK_SF_ACCOUNT");
            }
            UpdateLog(id, nb);
        }
        #endregion

        #region PV_CASE
        private static void PV_CASE()
        {
            long id = CreateLog(new Data_Log { ProcessingType = "INTEGRATION", ETLName = "PGMRUN_API_PV_CASE", FileOrTableName = "PV_SF_CASE", ProcessingStartDate = DateTime.Now, ProcessingEndDate = null, ProcessingStatus = null, NbRecordsProcessed = null, NbRecordsCollected = null });
            IdOverallProcess = id;
            int nb = 0;
            using (var db = new sfDataHub())
            {
                db.Database.CommandTimeout = Convert.ToInt32(SalesforceService.GetAppSetting("Req:timeout"));
                nb = db.Database.ExecuteSqlCommand(GetSQL_Case("PV_SF_CASE"));
                db.Database.ExecuteSqlCommand("TRUNCATE Table WK_SF_CASE");
            }
            UpdateLog(id, nb);
        }
        #endregion

        #region PV_OPPORTUNITY
        private static void PV_OPPORTUNITY()
        {
            long id = CreateLog(new Data_Log { ProcessingType = "INTEGRATION", ETLName = "PGMRUN_API_PV_OPPORTUNITY", FileOrTableName = "PV_SF_OPPORTUNITY", ProcessingStartDate = DateTime.Now, ProcessingEndDate = null, ProcessingStatus = null, NbRecordsProcessed = null, NbRecordsCollected = null });
            IdOverallProcess = id;
            int nb = 0;
            using (var db = new sfDataHub())
            {
                db.Database.CommandTimeout = Convert.ToInt32(SalesforceService.GetAppSetting("Req:timeout"));
                nb = db.Database.ExecuteSqlCommand(GetSQL_Opportunity("PV_SF_OPPORTUNITY"));
                db.Database.ExecuteSqlCommand("TRUNCATE Table WK_SF_OPPORTUNITY");
            }
            UpdateLog(id, nb);
        }
        #endregion

        #region PV_OPPORTUNITYHistory
        private static void PV_OPPORTUNITYHistory()
        {
            #region comment
            /*
            long id = CreateLog(new Data_Log { ProcessingType = "INTEGRATION", ETLName = "PGMRUN_API_PV_OPPORTUNITYHistory", FileOrTableName = "PV_SF_OPPORTUNITYHistory", ProcessingStartDate = DateTime.Now, ProcessingEndDate = null, ProcessingStatus = null, NbRecordsProcessed = null, NbRecordsCollected = null });
            int nb = 0;

            using (var db = new sfDataHub())
            {
                //update Validity_Startdate in PV_SF_Opportunity - somestimes dates are not synchronized
                db.Database.ExecuteSqlCommand(UpdateOppValidityStartdt());

                nb = db.Database.ExecuteSqlCommand(GetSQL_OpportunityHistory("PV_SF_OPPORTUNITYHistory"));
                db.Database.ExecuteSqlCommand("TRUNCATE Table WK_SF_OPPORTUNITYHistory");
                db.Database.ExecuteSqlCommand("TRUNCATE Table WK_SF_OPPORTUNITYFIELDHISTORY");
            }
            UpdateLog(id, nb);
            */
                    #endregion

            long id = CreateLog(new Data_Log { ProcessingType = "INTEGRATION", ETLName = "PGMRUN_API_PV_OPPORTUNITY_History", FileOrTableName = "PV_SF_OPPORTUNITY_History", ProcessingStartDate = DateTime.Now, ProcessingEndDate = null, ProcessingStatus = null, NbRecordsProcessed = null, NbRecordsCollected = null });
            IdOverallProcess = id;

            var nb = new SqlParameter
            {
                ParameterName = "nbRows",
                //Value = 0,
                Direction = System.Data.ParameterDirection.Output,
                DbType = System.Data.DbType.Int32
            };
            using (var db = new sfDataHub())
            {
                db.Database.CommandTimeout = Convert.ToInt32(SalesforceService.GetAppSetting("Req:timeout"));
                db.Database.ExecuteSqlCommand("exec PKGCM_PROC_SF_OPPORTUNITY_HISTORY @nbRows OUTPUT", nb);
            }
            if (nb.Value == null)
                UpdateLog(id, null);
            else
                UpdateLog(id, Convert.ToInt32(nb.Value));
        }
        #endregion

        #region PV_OPPORTUNITYISDELETED
        private static void PV_OPPORTUNITYISDELETED()
        {
            long id = CreateLog(new Data_Log { ProcessingType = "INTEGRATION", ETLName = "PGMRUN_API_PV_OPPORTUNITYISDELETED", FileOrTableName = "PV_SF_OPPORTUNITYISDELETED", ProcessingStartDate = DateTime.Now, ProcessingEndDate = null, ProcessingStatus = null, NbRecordsProcessed = null, NbRecordsCollected = null });
            IdOverallProcess = id;
            int nb = 0;
            using (var db = new sfDataHub())
            {
                db.Database.CommandTimeout = Convert.ToInt32(SalesforceService.GetAppSetting("Req:timeout"));
                nb = db.Database.ExecuteSqlCommand(GetSQL_OpportunityIsDeleted("PV_SF_OPPORTUNITYISDELETED"));
                db.Database.ExecuteSqlCommand("TRUNCATE Table WK_SF_OPPORTUNITYISDELETED");
            }
            UpdateLog(id, nb);
        }
        #endregion

        #region PV_USER
        private static void PV_USER()
        {
            long id = CreateLog(new Data_Log { ProcessingType = "INTEGRATION", ETLName = "PGMRUN_API_PV_USER", FileOrTableName = "PV_SF_USER", ProcessingStartDate = DateTime.Now, ProcessingEndDate = null, ProcessingStatus = null, NbRecordsProcessed = null, NbRecordsCollected = null });
            IdOverallProcess = id;
            int nb = 0;
            using (var db = new sfDataHub())
            {
                db.Database.CommandTimeout = Convert.ToInt32(SalesforceService.GetAppSetting("Req:timeout"));
                nb = db.Database.ExecuteSqlCommand(GetSQL_User("PV_SF_USER"));
                db.Database.ExecuteSqlCommand("TRUNCATE Table WK_SF_USER");
            }
            UpdateLog(id, nb);
        }
        #endregion

        #region PV_TASK
        private static void PV_TASK()
        {
            long id = CreateLog(new Data_Log { ProcessingType = "INTEGRATION", ETLName = "PGMRUN_API_PV_TASK", FileOrTableName = "PV_SF_TASK", ProcessingStartDate = DateTime.Now, ProcessingEndDate = null, ProcessingStatus = null, NbRecordsProcessed = null, NbRecordsCollected = null });
            IdOverallProcess = id;
            int nb = 0;
            using (var db = new sfDataHub())
            {
                db.Database.CommandTimeout = Convert.ToInt32(SalesforceService.GetAppSetting("Req:timeout"));
                nb = db.Database.ExecuteSqlCommand(GetSQL_Task("PV_SF_TASK"));
                db.Database.ExecuteSqlCommand("TRUNCATE Table WK_SF_TASK");
            }
            UpdateLog(id, nb);
        }
        #endregion

        #region PV_EMAILMESSAGE
        private static void PV_EMAILMESSAGE()
        {
            long id = CreateLog(new Data_Log { ProcessingType = "INTEGRATION", ETLName = "PGMRUN_API_PV_EMAILMESSAGE", FileOrTableName = "PV_SF_EMAILMESSAGE", ProcessingStartDate = DateTime.Now, ProcessingEndDate = null, ProcessingStatus = null, NbRecordsProcessed = null, NbRecordsCollected = null });
            IdOverallProcess = id;
            int nb = 0;
            using (var db = new sfDataHub())
            {
                db.Database.CommandTimeout = Convert.ToInt32(SalesforceService.GetAppSetting("Req:timeout"));
                nb = db.Database.ExecuteSqlCommand(GetSQL_EmailMessage("PV_SF_EMAILMESSAGE"));
                db.Database.ExecuteSqlCommand("TRUNCATE Table WK_SF_EMAILMESSAGE");
            }
            UpdateLog(id, nb);
        }
        #endregion

        #region PV_LIVECHATTRANSCRIPT
        private static void PV_LIVECHATTRANSCRIPT()
        {
            long id = CreateLog(new Data_Log { ProcessingType = "INTEGRATION", ETLName = "PGMRUN_API_PV_LIVECHATTRANSCRIPT", FileOrTableName = "PV_SF_LIVECHATTRANSCRIPT", ProcessingStartDate = DateTime.Now, ProcessingEndDate = null, ProcessingStatus = null, NbRecordsProcessed = null, NbRecordsCollected = null });
            IdOverallProcess = id;
            int nb = 0;
            using (var db = new sfDataHub())
            {
                db.Database.CommandTimeout = Convert.ToInt32(SalesforceService.GetAppSetting("Req:timeout"));
                nb = db.Database.ExecuteSqlCommand(GetSQL_LiveChatTranscript("PV_SF_LIVECHATTRANSCRIPT"));
                db.Database.ExecuteSqlCommand("TRUNCATE Table WK_SF_LIVECHATTRANSCRIPT");
            }
            UpdateLog(id, nb);
        }
        #endregion

        #region PV_PROFILE
        private static void PV_PROFILE()
        {
            long id = CreateLog(new Data_Log { ProcessingType = "INTEGRATION", ETLName = "PGMRUN_API_PV_PROFILE", FileOrTableName = "PV_SF_PROFILE", ProcessingStartDate = DateTime.Now, ProcessingEndDate = null, ProcessingStatus = null, NbRecordsProcessed = null, NbRecordsCollected = null });
            IdOverallProcess = id;
            int nb = 0;
            using (var db = new sfDataHub())
            {
                db.Database.CommandTimeout = Convert.ToInt32(SalesforceService.GetAppSetting("Req:timeout"));
                nb = db.Database.ExecuteSqlCommand(GetSQL_Profile("PV_SF_PROFILE"));
                db.Database.ExecuteSqlCommand("TRUNCATE Table WK_SF_PROFILE");
            }
            UpdateLog(id, nb);
        }
        #endregion

        #region PV_CONTACT
        private static void PV_CONTACT()
        {
            long id = CreateLog(new Data_Log { ProcessingType = "INTEGRATION", ETLName = "PGMRUN_API_PV_CONTACT", FileOrTableName = "PV_SF_CONTACT", ProcessingStartDate = DateTime.Now, ProcessingEndDate = null, ProcessingStatus = null, NbRecordsProcessed = null, NbRecordsCollected = null });
            IdOverallProcess = id;
            int nb = 0;
            using (var db = new sfDataHub())
            {
                db.Database.CommandTimeout = Convert.ToInt32(SalesforceService.GetAppSetting("Req:timeout"));
                nb = db.Database.ExecuteSqlCommand(GetSQL_Contact("PV_SF_CONTACT"));
                db.Database.ExecuteSqlCommand("TRUNCATE Table WK_SF_CONTACT");
            }
            UpdateLog(id, nb);
        }
        #endregion

        #region REF_RECORDTYPE
        private static void REF_RECORDTYPE()
        {
            long id = CreateLog(new Data_Log { ProcessingType = "INTEGRATION", ETLName = "PGMRUN_API_REF_RECORDTYPE", FileOrTableName = "REF_RECORDTYPE", ProcessingStartDate = DateTime.Now, ProcessingEndDate = null, ProcessingStatus = null, NbRecordsProcessed = null, NbRecordsCollected = null });
            IdOverallProcess = id;
            int nb = 0;
            using (var db = new sfDataHub())
            {
                db.Database.CommandTimeout = Convert.ToInt32(SalesforceService.GetAppSetting("Req:timeout"));
                nb = db.Database.ExecuteSqlCommand(GetSQL_RefRecordType("REF_RECORDTYPE"));
                db.Database.ExecuteSqlCommand("TRUNCATE Table WK_SF_RECORDTYPE");
            }
            UpdateLog(id, nb);
        }
        #endregion

        //------------------------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------------------------
        #region MERGE ACCOUNT
        private static string GetSQL_Account(string target)
        {
            string sql = @"ALTER TABLE " + target + @" SET (SYSTEM_VERSIONING = OFF);
                MERGE " + target + @" target 
                USING WK_SF_ACCOUNT src 
	                ON target.Id_SF = src.Id_SF 
                WHEN MATCHED AND 
	                (target.LastModifiedDate != src.LastModifiedDate)
                THEN UPDATE SET target.Validity_EndDate = src.LastModifiedDate;
                ALTER TABLE " + target + @" SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo." + target + @"History));
                MERGE " + target + @" target 
                USING WK_SF_ACCOUNT src 
	                ON target.Id_SF = src.Id_SF 
                WHEN NOT MATCHED BY TARGET THEN  
	                INSERT (Id_SF,BehaviouralScoring__c,CreatedDate,DeathDate__pc,DistributorNetwork__c,EmployeeType__pc,Firstname,GeolifeSegment__c,IDCustomerCID__c,IDCustomerPID__c,IDCustomer__pc,IDCustomerSAB__pc,LastModifiedDate,Lastname,OBFirstContactDate__c,OBSeniority__c,OBTerminationDate__c,OBTerminationReason__c,Occupation__pc,OptInDataTelco__c,OptInOrderTelco__c,OwnerId,ParentId,PersonBirthdate,PersonContactId,PersonLeadSource,RecordTypeId,Salutation,SystemModstamp,ShopNetworkMesh__c,ShopManagerLastName__c,ShopManagerFirstName__c,ShopCode__c,ShopBrand__c,Phone,Name,BillingStreet,BillingCity,BillingPostalCode,Preattribution__c,BillingCountryCode,BillingCountry,BillingStateCode,BillingState,MaritalStatus__pc,OccupationNiv1__pc,OccupationNiv3__pc,TotalDisposableIncome__pc,TotalIncome__pc,JustifiedIncomes__pc,EmploymentContractStartDate__pc,OFSeniority__c,PrimaryResidOccupationType__pc,PersonEmail,PersonMobilePhone, Validity_StartDate)
	                VALUES (src.Id_SF,src.BehaviouralScoring__c,src.CreatedDate,src.DeathDate__pc,src.DistributorNetwork__c,src.EmployeeType__pc,src.Firstname,src.GeolifeSegment__c,src.IDCustomerCID__c,src.IDCustomerPID__c,src.IDCustomer__pc,src.IDCustomerSAB__pc,src.InweboID__pc,src.LastModifiedDate,src.Lastname,src.OBFirstContactDate__c,src.OBSeniority__c,src.OBTerminationDate__c,src.OBTerminationReason__c,src.Occupation__pc,src.OptInDataTelco__c,src.OptInOrderTelco__c,src.OwnerId,src.ParentId,src.PersonBirthdate,src.PersonContactId,src.PersonLeadSource,src.RecordTypeId,src.Salutation,src.SystemModstamp,src.ShopNetworkMesh__c,src.ShopManagerLastName__c,src.ShopManagerFirstName__c,src.ShopCode__c,src.ShopBrand__c,src.Phone,src.Name,src.BillingStreet,src.BillingCity,src.BillingPostalCode,src.Preattribution__c,src.BillingCountryCode,src.BillingCountry,src.BillingStateCode,src.BillingState,src.MaritalStatus__pc,src.OccupationNiv1__pc,src.OccupationNiv3__pc,src.TotalDisposableIncome__pc,src.TotalIncome__pc,src.JustifiedIncomes__pc,src.EmploymentContractStartDate__pc,src.OFSeniority__c,src.PrimaryResidOccupationType__pc,src.PersonEmail,src.PersonMobilePhone, src.LastModifiedDate)
                WHEN MATCHED AND 
	                (target.LastModifiedDate != src.LastModifiedDate)
                THEN UPDATE SET
                         target.Id_SF                    = src.Id_SF,
                         target.BehaviouralScoring__c    = src.BehaviouralScoring__c,
                         target.CreatedDate              = src.CreatedDate,
                         target.DeathDate__pc            = src.DeathDate__pc,
                         target.DistributorNetwork__c    = src.DistributorNetwork__c,
                         target.EmployeeType__pc         = src.EmployeeType__pc,
                         target.Firstname                = src.Firstname,
                         target.GeolifeSegment__c        = src.GeolifeSegment__c,
                         target.IDCustomerCID__c         = src.IDCustomerCID__c,
                         target.IDCustomerPID__c         = src.IDCustomerPID__c,
                         target.IDCustomer__pc           = src.IDCustomer__pc,
                         target.IDCustomerSAB__pc        = src.IDCustomerSAB__pc,
                         target.LastModifiedDate         = src.LastModifiedDate,
                         target.Lastname                 = src.Lastname,
                         target.OBFirstContactDate__c    = src.OBFirstContactDate__c,
                         target.OBSeniority__c           = src.OBSeniority__c,
                         target.OBTerminationDate__c     = src.OBTerminationDate__c,
                         target.OBTerminationReason__c   = src.OBTerminationReason__c,
                         target.Occupation__pc           = src.Occupation__pc,
                         target.OptInDataTelco__c        = src.OptInDataTelco__c,
                         target.OptInOrderTelco__c       = src.OptInOrderTelco__c,
                         target.OwnerId                  = src.OwnerId,
                         target.ParentId                 = src.ParentId,
                         target.PersonBirthdate          = src.PersonBirthdate,
                         target.PersonContactId          = src.PersonContactId,
                         target.PersonLeadSource         = src.PersonLeadSource,
                         target.RecordTypeId             = src.RecordTypeId,
                         target.Salutation               = src.Salutation,
                         target.SystemModstamp           = src.SystemModstamp,
                         target.ShopNetworkMesh__c       = src.ShopNetworkMesh__c,
                         target.ShopManagerLastName__c   = src.ShopManagerLastName__c,
                         target.ShopManagerFirstName__c  = src.ShopManagerFirstName__c,
                         target.ShopCode__c              = src.ShopCode__c,
                         target.ShopBrand__c             = src.ShopBrand__c,
                         target.Phone                    = src.Phone,
                         target.Name                     = src.Name,
                         target.BillingStreet            = src.BillingStreet,
                         target.BillingCity              = src.BillingCity,
                         target.BillingPostalCode        = src.BillingPostalCode,
                         target.Preattribution__c        = src.Preattribution__c,
                         target.BillingCountryCode       = src.BillingCountryCode,
                         target.BillingCountry           = src.BillingCountry,
                         target.BillingStateCode         = src.BillingStateCode,
                         target.BillingState             = src.BillingState,
                         target.MaritalStatus__pc        = src.MaritalStatus__pc,
                         target.OccupationNiv1__pc       = src.OccupationNiv1__pc,
                         target.OccupationNiv3__pc       = src.OccupationNiv3__pc,
                         target.TotalDisposableIncome__pc= src.TotalDisposableIncome__pc,
                         target.TotalIncome__pc          = src.TotalIncome__pc,
                         target.JustifiedIncomes__pc     = src.JustifiedIncomes__pc,
                         target.EmploymentContractStartDate__pc = src.EmploymentContractStartDate__pc,
                         target.OFSeniority__c           = src.OFSeniority__c,
                         target.PrimaryResidOccupationType__pc=src.PrimaryResidOccupationType__pc,
                         target.PersonEmail              = src.PersonEmail,
                         target.PersonMobilePhone        = src.PersonMobilePhone,
                         target.Validity_StartDate       = src.LastModifiedDate,
                         target.Validity_EndDate         = default;";
            return sql;
        }
        #endregion

        #region MERGE CASE
        private static string GetSQL_Case(string target)
        {
            string sql = @"ALTER TABLE " + target + @" SET (SYSTEM_VERSIONING = OFF);
                MERGE " + target + @" target 
                USING WK_SF_CASE src 
	                ON target.Id_SF = src.Id_SF 
                WHEN MATCHED AND 
	                (target.LastModifiedDate != src.LastModifiedDate)
                THEN UPDATE SET target.Validity_EndDate = src.LastModifiedDate;
                ALTER TABLE " + target + @" SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo." + target + @"History));
                MERGE " + target + @" target 
                USING WK_SF_CASE src 
	                ON target.Id_SF = src.Id_SF 
                WHEN NOT MATCHED BY TARGET THEN  
	                INSERT (Id_SF,AccountId,ActionPlan__c,ARDate__c,CaseNumber,ClosedDate,CommercialGestAmount__c,CreatedById,CreatedDate,CustomerType__c,Description,DUT__c,Equipment__c,ExpiredMilestone__c,FirstClaim__c,Habilitation__c,Improvement__c,Indemnity__c,InitialCase__c,isDoneWithExpMilestone__c,IsDoneWithRespMilestone__c,IsDone__c,isNotDoneExpMilestone__c,IsNotDoneWithRespMilestone__c,IsNotDone__c,LastModifiedById,LastModifiedDate,LetterWaitingDate__c,LevelClaim__c,LostAmount__c,Metier__c,MilestoneStatus,Network__c,Origin,Origin__c,OwnerId,ParentId,ParentOppy__c,PrimaryQualification__c,PrimarySubject__c,Priority,Process__c,queueName__c,Reason,RecordTypeId,ResponseChannel__c,ResponseDate__c,RetrocessionAmount__c,SecondarySubject__c,SecondQualification__c,ShopSeller__c,StandBy__c,Status,SubType__c,TechCloseCase__c,Tech_PreviousOwner__c,Tech_ReviewDateExpired__c,TreatementReason__c,Type,IDCampagne__c,CallDetail__c,CallStatus__c,OBShop__c,Validity_StartDate)
	                VALUES (src.Id_SF,src.AccountId,src.ActionPlan__c,src.ARDate__c,src.CaseNumber,src.ClosedDate,src.CommercialGestAmount__c,src.CreatedById,src.CreatedDate,src.CustomerType__c,src.Description,src.DUT__c,src.Equipment__c,src.ExpiredMilestone__c,src.FirstClaim__c,src.Habilitation__c,src.Improvement__c,src.Indemnity__c,src.InitialCase__c,src.isDoneWithExpMilestone__c,src.IsDoneWithRespMilestone__c,src.IsDone__c,src.isNotDoneExpMilestone__c,src.IsNotDoneWithRespMilestone__c,src.IsNotDone__c,src.LastModifiedById,src.LastModifiedDate,src.LetterWaitingDate__c,src.LevelClaim__c,src.LostAmount__c,src.Metier__c,src.MilestoneStatus,src.Network__c,src.Origin,src.Origin__c,src.OwnerId,src.ParentId,src.ParentOppy__c,src.PrimaryQualification__c,src.PrimarySubject__c,src.Priority,src.Process__c,src.queueName__c,src.Reason,src.RecordTypeId,src.ResponseChannel__c,src.ResponseDate__c,src.RetrocessionAmount__c,src.SecondarySubject__c,src.SecondQualification__c,src.ShopSeller__c,src.StandBy__c,src.Status,src.SubType__c,src.TechCloseCase__c,src.Tech_PreviousOwner__c,src.Tech_ReviewDateExpired__c,src.TreatementReason__c,src.Type,src.IDCampagne__c,src.CallDetail__c,src.CallStatus__c,src.OBShop__c,src.LastModifiedDate) 
                WHEN MATCHED AND 
	                (target.LastModifiedDate != src.LastModifiedDate)
                THEN UPDATE SET
	                     target.AccountId                     = src.AccountId
                       , target.ActionPlan__c                 = src.ActionPlan__c
                       , target.ARDate__c                     = src.ARDate__c
                       , target.CaseNumber                    = src.CaseNumber
                       , target.ClosedDate                    = src.ClosedDate
                       , target.CommercialGestAmount__c       = src.CommercialGestAmount__c
                       , target.CreatedById                   = src.CreatedById
                       , target.CreatedDate                   = src.CreatedDate
                       , target.CustomerType__c               = src.CustomerType__c
                       , target.Description                   = src.Description
                       , target.DUT__c                        = src.DUT__c
                       , target.Equipment__c                  = src.Equipment__c
                       , target.ExpiredMilestone__c           = src.ExpiredMilestone__c
                       , target.FirstClaim__c                 = src.FirstClaim__c
                       , target.Habilitation__c               = src.Habilitation__c
                       , target.Improvement__c                = src.Improvement__c
                       , target.Indemnity__c                  = src.Indemnity__c
                       , target.InitialCase__c                = src.InitialCase__c
                       , target.isDoneWithExpMilestone__c     = src.isDoneWithExpMilestone__c
                       , target.IsDoneWithRespMilestone__c    = src.IsDoneWithRespMilestone__c
                       , target.IsDone__c                     = src.IsDone__c
                       , target.isNotDoneExpMilestone__c      = src.isNotDoneExpMilestone__c
                       , target.IsNotDoneWithRespMilestone__c = src.IsNotDoneWithRespMilestone__c
                       , target.IsNotDone__c                  = src.IsNotDone__c
                       , target.LastModifiedById              = src.LastModifiedById
                       , target.LastModifiedDate              = src.LastModifiedDate
                       , target.LetterWaitingDate__c          = src.LetterWaitingDate__c
                       , target.LevelClaim__c                 = src.LevelClaim__c
                       , target.LostAmount__c                 = src.LostAmount__c
                       , target.Metier__c                     = src.Metier__c
                       , target.MilestoneStatus               = src.MilestoneStatus
                       , target.Network__c                    = src.Network__c
                       , target.Origin                        = src.Origin
                       , target.Origin__c                     = src.Origin__c
                       , target.OwnerId                       = src.OwnerId
                       , target.ParentId                      = src.ParentId
                       , target.ParentOppy__c                 = src.ParentOppy__c
                       , target.PrimaryQualification__c       = src.PrimaryQualification__c
                       , target.PrimarySubject__c             = src.PrimarySubject__c
                       , target.Priority                      = src.Priority
                       , target.Process__c                    = src.Process__c
                       , target.queueName__c                  = src.queueName__c
                       , target.Reason                        = src.Reason
                       , target.RecordTypeId                  = src.RecordTypeId
                       , target.ResponseChannel__c            = src.ResponseChannel__c
                       , target.ResponseDate__c               = src.ResponseDate__c
                       , target.RetrocessionAmount__c         = src.RetrocessionAmount__c
                       , target.SecondarySubject__c           = src.SecondarySubject__c
                       , target.SecondQualification__c        = src.SecondQualification__c
                       , target.ShopSeller__c                 = src.ShopSeller__c
                       , target.StandBy__c                    = src.StandBy__c
                       , target.Status                        = src.Status
                       , target.SubType__c                    = src.SubType__c
                       , target.TechCloseCase__c              = src.TechCloseCase__c
                       , target.Tech_PreviousOwner__c         = src.Tech_PreviousOwner__c
                       , target.Tech_ReviewDateExpired__c     = src.Tech_ReviewDateExpired__c
                       , target.TreatementReason__c           = src.TreatementReason__c
                       , target.Type                          = src.Type
                       , target.IDCampagne__c                 = src.IDCampagne__c
                       , target.CallDetail__c                 = src.CallDetail__c
                       , target.CallStatus__c                 = src.CallStatus__c
                       , target.OBShop__c                     = src.OBShop__c
                       , target.Validity_StartDate            = src.LastModifiedDate
                       , target.Validity_EndDate              = default;";

            return sql;
        }
        #endregion

        #region MERGE OPPORTUNITY
        private static string GetSQL_Opportunity(string target)
        {
            string sql = @"MERGE " + target + @" target 
                USING WK_SF_OPPORTUNITY src 
	                ON target.Id_SF = src.Id_SF 
                WHEN NOT MATCHED BY TARGET THEN  
	                INSERT (Id_SF,AccountId,AdvisorCode__c,CampaignId__c,CloseDate,CommercialOfferCode__c,CommercialOfferName__c,CompleteFileFlag__c,CreatedById,CreatedDate,DeniedOpportunityReason__c,DistributionChannel__c,DistributorEntity__c,DistributorNetwork__c,FinalizedBy__c,FormValidation__c,IDOppty__c,IDProcessSous__c,IdSource__c,Indication__c,LastModifiedDate,LeadSource,Manager__c,NoIndication__c,OwnerId,PointOfSaleCode__c,ProductFamilyCode__c,ProductFamilyName__c,ProvenanceIndicationIndicatorId__c,RecordTypeId,RejectReason__c,RelationEntryScore__c,StageName,StartedChannel__c,SystemModstamp,Type,IsWon,Name,realizedBy__c,SignatureDate__c,StartedChannelForIndication__c,ToBeDeleted__c,Validity_StartDate)
	                VALUES (src.Id_SF,src.AccountId,src.AdvisorCode__c,src.CampaignId__c,src.CloseDate,src.CommercialOfferCode__c,src.CommercialOfferName__c,src.CompleteFileFlag__c,src.CreatedById,src.CreatedDate,src.DeniedOpportunityReason__c,src.DistributionChannel__c,src.DistributorEntity__c,src.DistributorNetwork__c,src.FinalizedBy__c,src.FormValidation__c,src.IDOppty__c,src.IDProcessSous__c,src.IdSource__c,src.Indication__c,src.LastModifiedDate,src.LeadSource,src.Manager__c,src.NoIndication__c,src.OwnerId,src.PointOfSaleCode__c,src.ProductFamilyCode__c,src.ProductFamilyName__c,src.ProvenanceIndicationIndicatorId__c,src.RecordTypeId,src.RejectReason__c,src.RelationEntryScore__c,src.StageName,src.StartedChannel__c,src.SystemModstamp,src.Type,src.IsWon,src.Name,src.realizedBy__c,src.SignatureDate__c,src.StartedChannelForIndication__c,src.ToBeDeleted__c,src.LastModifiedDate) 
                WHEN MATCHED AND 
	                (target.LastModifiedDate != src.LastModifiedDate)
                THEN UPDATE SET 
                         target.AccountId                          = src.AccountId
                       , target.AdvisorCode__c                     = src.AdvisorCode__c
                       , target.CampaignId__c                      = src.CampaignId__c
                       , target.CloseDate                          = src.CloseDate
                       , target.CommercialOfferCode__c             = src.CommercialOfferCode__c
                       , target.CommercialOfferName__c             = src.CommercialOfferName__c
                       , target.CompleteFileFlag__c                = src.CompleteFileFlag__c
                       , target.CreatedById                        = src.CreatedById
                       , target.CreatedDate                        = src.CreatedDate
                       , target.DeniedOpportunityReason__c         = src.DeniedOpportunityReason__c
                       , target.DistributionChannel__c             = src.DistributionChannel__c
                       , target.DistributorEntity__c               = src.DistributorEntity__c
                       , target.DistributorNetwork__c              = src.DistributorNetwork__c
                       , target.FinalizedBy__c                     = src.FinalizedBy__c
                       , target.FormValidation__c                  = src.FormValidation__c
                       , target.IDOppty__c                         = src.IDOppty__c
                       , target.IDProcessSous__c                   = src.IDProcessSous__c
                       , target.IdSource__c                        = src.IdSource__c
                       , target.Indication__c                      = src.Indication__c
                       , target.LastModifiedDate                   = src.LastModifiedDate
                       , target.LeadSource                         = src.LeadSource
                       , target.Manager__c                         = src.Manager__c
                       , target.NoIndication__c                    = src.NoIndication__c
                       , target.OwnerId                            = src.OwnerId
                       , target.PointOfSaleCode__c                 = src.PointOfSaleCode__c
                       , target.ProductFamilyCode__c               = src.ProductFamilyCode__c
                       , target.ProductFamilyName__c               = src.ProductFamilyName__c
                       , target.ProvenanceIndicationIndicatorId__c = src.ProvenanceIndicationIndicatorId__c
                       , target.RecordTypeId                       = src.RecordTypeId
                       , target.RejectReason__c                    = src.RejectReason__c
                       , target.RelationEntryScore__c              = src.RelationEntryScore__c
                       , target.StageName                          = src.StageName
                       , target.StartedChannel__c                  = src.StartedChannel__c
                       , target.SystemModstamp                     = src.SystemModstamp
                       , target.Type                               = src.Type
                       , target.IsWon                              = src.IsWon
                       , target.Name                               = src.Name
                       , target.realizedBy__c                      = src.realizedBy__c
                       , target.SignatureDate__c                   = src.SignatureDate__c
                       , target.StartedChannelForIndication__c     = src.StartedChannelForIndication__c
                       , target.ToBeDeleted__c                     = src.ToBeDeleted__c
                       , target.Validity_StartDate                 = src.LastModifiedDate;";
            return sql;
        }
        #endregion

        #region MERGE OPPORTUNITYISDELETED
        private static string GetSQL_OpportunityIsDeleted(string target)
        {
            string sql = @"MERGE " + target + @" target 
                USING WK_SF_OPPORTUNITYISDELETED src 
	                ON target.Id_SF = src.Id_SF 
                WHEN NOT MATCHED BY TARGET THEN  
	                INSERT (Id_SF,IsDeleted)
	                VALUES (src.Id_SF,src.IsDeleted);";
            return sql;
        }
        #endregion

        #region not used anymore MERGE OPPORTUNITY_HISTORY
        /*
        private static string GetSQL_OpportunityHistory(string target)
        {
            string sql = @"MERGE " + target + @" target 
                USING WK_SF_OPPORTUNITYHistory src 
	                ON target.Id_SF_FldHistOpp = src.Id_SF_FldHistOpp 
                WHEN NOT MATCHED BY TARGET THEN  
	                INSERT (Id_SF_FldHistOpp,Id_SF,AccountId,AdvisorCode__c,CampaignId__c,CloseDate,CommercialOfferCode__c,CommercialOfferName__c,CompleteFileFlag__c,CreatedById,CreatedDate,DeniedOpportunityReason__c,DistributionChannel__c,DistributorEntity__c,DistributorNetwork__c,FinalizedBy__c,FormValidation__c,IDOppty__c,IDProcessSous__c,IdSource__c,Indication__c,LastModifiedDate,LeadSource,Manager__c,NoIndication__c,OwnerId,PointOfSaleCode__c,ProductFamilyCode__c,ProductFamilyName__c,ProvenanceIndicationIndicatorId__c,RecordTypeId,RejectReason__c,RelationEntryScore__c,StageName,StartedChannel__c,SystemModstamp,Type,IsWon, Validity_StartDate, Validity_EndDate)
	                VALUES (src.Id_SF_FldHistOpp,src.Id_SF,src.AccountId,src.AdvisorCode__c,src.CampaignId__c,src.CloseDate,src.CommercialOfferCode__c,src.CommercialOfferName__c,src.CompleteFileFlag__c,src.CreatedById,src.CreatedDate,src.DeniedOpportunityReason__c,src.DistributionChannel__c,src.DistributorEntity__c,src.DistributorNetwork__c,src.FinalizedBy__c,src.FormValidation__c,src.IDOppty__c,src.IDProcessSous__c,src.IdSource__c,src.Indication__c,src.LastModifiedDate,src.LeadSource,src.Manager__c,src.NoIndication__c,src.OwnerId,src.PointOfSaleCode__c,src.ProductFamilyCode__c,src.ProductFamilyName__c,src.ProvenanceIndicationIndicatorId__c,src.RecordTypeId,src.RejectReason__c,src.RelationEntryScore__c,src.StageName,src.StartedChannel__c,src.SystemModstamp,src.Type,src.IsWon, src.Validity_StartDate, src.Validity_EndDate) 
                WHEN MATCHED AND 
	                (target.LastModifiedDate != src.LastModifiedDate)
                THEN
	                UPDATE SET 
                    target.LastModifiedDate    = src.LastModifiedDate 
	                ,target.AccountId		    = src.AccountId 
	                ,target.CloseDate	        =  src.CloseDate
	                ,target.LeadSource	        = src.LeadSource 
	                ,target.OwnerId		        = src.OwnerId
	                ,target.StageName 	        = src.StageName  
	                ,target.StartedChannel__c 		=  src.StartedChannel__c  
	                ,target.DistributionChannel__c 	=  src.DistributionChannel__c  
	                ,target.AdvisorCode__c 			=  src.AdvisorCode__c  
	                ,target.CommercialOfferCode__c 	=  src.CommercialOfferCode__c  
	                ,target.ProductFamilyCode__c 	=  src.ProductFamilyCode__c 
	                ,target.PointOfSaleCode__c 		=  src.PointOfSaleCode__c 
	                ,target.FinalizedBy__c 			=  src.FinalizedBy__c  
	                ,target.Indication__c 			=  src.Indication__c 
	                ,target.IDProcessSous__c 		=  src.IDProcessSous__c 
	                ,target.NoIndication__c 			    =  src.NoIndication__c  
	                ,target.DeniedOpportunityReason__c 		=  src.DeniedOpportunityReason__c 
	                ,target.RejectReason__c 		=  src.RejectReason__c  
	                ,target.CommercialOfferName__c 	=  src.CommercialOfferName__c  
	                ,target.ProductFamilyName__c 	=  src.ProductFamilyName__c  
	                ,target.DistributorNetwork__c 	=  src.DistributorNetwork__c  
	                ,target.FormValidation__c 		=  src.FormValidation__c  
	                ,target.Type 		=  src.Type  
	                ,target.IsWon 		=  src.IsWon
                    ,target.Validity_StartDate      =  src.Validity_StartDate
                    ,target.Validity_EndDate        =  src.Validity_EndDate;";
            return sql;
        }
        */
        #endregion

        #region MERGE USER
        private static string GetSQL_User(string target)
        {
            string sql = @"ALTER TABLE " + target + @" SET (SYSTEM_VERSIONING = OFF);
                MERGE " + target + @" target 
                USING WK_SF_USER src 
	                ON target.Id_SF = src.Id_SF 
                WHEN MATCHED AND 
	                (target.LastModifiedDate != src.LastModifiedDate)
                THEN UPDATE SET target.Validity_EndDate = src.LastModifiedDate;
                ALTER TABLE " + target + @" SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo." + target + @"History));
                MERGE " + target + @" target 
                USING WK_SF_USER src 
	                ON target.Id_SF = src.Id_SF 
                WHEN NOT MATCHED BY TARGET THEN  
	                INSERT (Id_SF, CreatedDate, Firstname, Lastname, Title, Phone, Email, City, Country, CountryCode, Latitude, LastModifiedDate, Longitude, PostalCode, State, StateCode, Street, UserRoleId, ProfileId, QSProfile__c, GenesysUserId__c, Validity_StartDate)
	                VALUES (src.Id_SF, src.CreatedDate, src.Firstname, src.Lastname, src.Title, src.Phone, src.Email, src.City, src.Country, src.CountryCode, src.Latitude, src.LastModifiedDate, src.Longitude, src.PostalCode, src.State, src.StateCode, src.Street, src.UserRoleId, src.ProfileId, src.QSProfile__c, src.GenesysUserId__c, src.LastModifiedDate) 
                WHEN MATCHED AND 
	                (target.LastModifiedDate != src.LastModifiedDate)
                THEN UPDATE SET
	                     target.CreatedDate        = src.CreatedDate
                       , target.Firstname          = src.Firstname
                       , target.Lastname           = src.Lastname
                       , target.Title              = src.Title
                       , target.Phone              = src.Phone
                       , target.Email              = src.Email
                       , target.City               = src.City
                       , target.Country            = src.Country
                       , target.CountryCode        = src.CountryCode
                       , target.Latitude           = src.Latitude
                       , target.LastModifiedDate   = src.LastModifiedDate
                       , target.Longitude          = src.Longitude
                       , target.PostalCode         = src.PostalCode
                       , target.State              = src.State
                       , target.StateCode          = src.StateCode
                       , target.Street             = src.Street 
                       , target.UserRoleId         = src.UserRoleId
                       , target.ProfileId          = src.ProfileId
                       , target.QSProfile__c       = src.QSProfile__c
                       , target.GenesysUserId__c   = src.GenesysUserId__c
                       , target.Validity_StartDate = src.LastModifiedDate
                       , target.Validity_EndDate   = default;";
            return sql;
        }
        #endregion

        #region MERGE TASK
        private static string GetSQL_Task(string target)
        {
            string sql = @"MERGE " + target + @" target 
                USING WK_SF_TASK src 
	                ON target.Id_SF = src.Id_SF 
                WHEN NOT MATCHED BY TARGET THEN  
	                INSERT (Id_SF,RecordTypeId,WhatId,OwnerId,Description,WhoId,Subject,InteractionId__c,CreatedById,CreatedDate,LastModifiedDate,TaskSubType,Priority,Status,Family__c,SubFamily__c,InteractionDate__c,ScoreOfInteraction__c,NotationLabel__c,ResponseLink__c,ActivityDate,ConversationID__c,CallType__c, Validity_StartDate)
	                VALUES (src.Id_SF,src.RecordTypeId,src.WhatId,src.OwnerId,src.Description,src.WhoId,src.Subject,src.InteractionId__c,src.CreatedById,src.CreatedDate,src.LastModifiedDate,src.TaskSubType,src.Priority,src.Status,src.Family__c,src.SubFamily__c,src.InteractionDate__c,src.ScoreOfInteraction__c,src.NotationLabel__c,src.ResponseLink__c,src.ActivityDate,src.ConversationID__c,src.CallType__c, src.LastModifiedDate) 
                WHEN MATCHED AND 
	                (target.LastModifiedDate != src.LastModifiedDate)
                THEN UPDATE SET
                         target.RecordTypeId          = src.RecordTypeId
                       , target.WhatId                = src.WhatId
                       , target.OwnerId               = src.OwnerId
                       , target.Description           = src.Description
                       , target.WhoId                 = src.WhoId
                       , target.Subject               = src.Subject
                       , target.InteractionId__c      = src.InteractionId__c
                       , target.CreatedById           = src.CreatedById
                       , target.CreatedDate           = src.CreatedDate
                       , target.LastModifiedDate      = src.LastModifiedDate
                       , target.TaskSubType           = src.TaskSubType
                       , target.Priority              = src.Priority
                       , target.Status                = src.Status
                       , target.Family__c             = src.Family__c
                       , target.SubFamily__c          = src.SubFamily__c
                       , target.InteractionDate__c    = src.InteractionDate__c
                       , target.ScoreOfInteraction__c = src.ScoreOfInteraction__c
                       , target.NotationLabel__c      = src.NotationLabel__c
                       , target.ResponseLink__c       = src.ResponseLink__c
                       , target.ActivityDate          = src.ActivityDate
                       , target.ConversationID__c     = src.ConversationID__c
                       , target.CallType__c           = src.CallType__c
                       , target.Validity_StartDate    = src.LastModifiedDate
                       , target.Validity_EndDate      = default;";
            return sql;
        }
        #endregion

        #region MERGE EMAILMESSAGE
        private static string GetSQL_EmailMessage(string target)
        {
            string sql = @"MERGE " + target + @" target 
                USING WK_SF_EMAILMESSAGE src 
	                ON target.Id_SF = src.Id_SF 
                WHEN NOT MATCHED BY TARGET THEN  
	                INSERT (Id_SF,FromAddress,FromName,ParentId,Subject,ToAddress,CcAddress,BccAddress,TextBody,HtmlBody,CreatedDate,LastModifiedDate,CreatedById,Incoming,MessageDate,RelatedToId, Validity_StartDate)
	                VALUES (src.Id_SF,src.FromAddress,src.FromName,src.ParentId,src.Subject,src.ToAddress,src.CcAddress,src.BccAddress,src.TextBody,src.HtmlBody,src.CreatedDate,src.LastModifiedDate,src.CreatedById,src.Incoming,src.MessageDate,src.RelatedToId, src.LastModifiedDate) 
                WHEN MATCHED AND 
	                (target.LastModifiedDate != src.LastModifiedDate)
                THEN UPDATE SET  
                         FromAddress               = src.FromAddress
                       , target.FromName           = src.FromName
                       , target.ParentId           = src.ParentId
                       , target.Subject            = src.Subject
                       , target.ToAddress          = src.ToAddress
                       , target.CcAddress          = src.CcAddress
                       , target.BccAddress         = src.BccAddress
                       , target.TextBody           = src.TextBody
                       , target.HtmlBody           = src.HtmlBody
                       , target.CreatedDate        = src.CreatedDate
                       , target.LastModifiedDate   = src.LastModifiedDate
                       , target.CreatedById        = src.CreatedById
                       , target.Incoming           = src.Incoming
                       , target.MessageDate        = src.MessageDate
                       , target.RelatedToId        = src.RelatedToId
                       , target.Validity_StartDate = src.LastModifiedDate
                       , target.Validity_EndDate   = default;";
            return sql;
        }
        #endregion

        #region MERGE LIVECHATTRANSCRIPT
        private static string GetSQL_LiveChatTranscript(string target)
        {
            string sql = @"ALTER TABLE " + target + @" SET (SYSTEM_VERSIONING = OFF);
                MERGE " + target + @" target 
                USING WK_SF_LIVECHATTRANSCRIPT src 
	                ON target.Id_SF = src.Id_SF 
                WHEN MATCHED AND 
	                (target.LastModifiedDate != src.LastModifiedDate)
                THEN UPDATE SET target.Validity_EndDate = src.LastModifiedDate;
                ALTER TABLE " + target + @" SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo." + target + @"History));
                MERGE " + target + @" target 
                USING WK_SF_LIVECHATTRANSCRIPT src 
	                ON target.Id_SF = src.Id_SF 
                WHEN NOT MATCHED BY TARGET THEN  
	                INSERT (Id_SF, CreatedDate, LastModifiedDate, StartTime, OwnerId, CaseId, AccountId, Subject__c, ChatDuration, WaitTime, ConversationID__c, Validity_StartDate)
	                VALUES (src.Id_SF, src.CreatedDate, src.LastModifiedDate, src.StartTime, src.OwnerId, src.CaseId, src.AccountId, src.Subject__c, src.ChatDuration, src.WaitTime, src.ConversationID__c, src.LastModifiedDate) 
                WHEN MATCHED AND 
	                (target.LastModifiedDate != src.LastModifiedDate)
                THEN UPDATE SET
	                     target.CreatedDate         = src.CreatedDate
                       , target.LastModifiedDate    = src.LastModifiedDate
                       , target.StartTime           = src.StartTime 
                       , target.OwnerId             = src.OwnerId
                       , target.CaseId              = src.CaseId
                       , target.AccountId           = src.AccountId 
                       , target.Subject__c          = src.Subject__c 
                       , target.ChatDuration        = src.ChatDuration 
                       , target.WaitTime            = src.WaitTime
                       , target.ConversationID__c   = src.ConversationID__c
                       , target.Validity_StartDate  = src.LastModifiedDate
                       , target.Validity_EndDate    = default;";
            return sql;
        }
        #endregion

        #region MERGE PROFILE
        private static string GetSQL_Profile(string target)
        {
            string sql = @"MERGE " + target + @" target 
                USING WK_SF_PROFILE src 
	                ON target.Id_SF = src.Id_SF 
                WHEN NOT MATCHED BY TARGET THEN  
	                INSERT (Id_SF,Name,CreatedDate,LastModifiedDate, Validity_StartDate)
	                VALUES (src.Id_SF,src.Name,src.CreatedDate,src.LastModifiedDate, src.LastModifiedDate) 
                WHEN MATCHED AND 
	                (target.LastModifiedDate != src.LastModifiedDate)
                THEN UPDATE SET
                         target.Name               = src.Name
                       , target.CreatedDate        = src.CreatedDate
                       , target.LastModifiedDate   = src.LastModifiedDate
                       , target.Validity_StartDate = src.LastModifiedDate
                       , target.Validity_EndDate   = default;";
            return sql;
        }
        #endregion

        #region MERGE CONTACT
        private static string GetSQL_Contact(string target)
        {
            string sql = @"ALTER TABLE " + target + @" SET (SYSTEM_VERSIONING = OFF);
                MERGE " + target + @" target 
                USING WK_SF_CONTACT src 
	                ON target.Id_SF = src.Id_SF 
                WHEN MATCHED AND 
	                (target.LastModifiedDate != src.LastModifiedDate)
                THEN UPDATE SET target.Validity_EndDate = src.LastModifiedDate;
                ALTER TABLE " + target + @" SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo." + target + @"History));
                MERGE " + target + @" target 
                USING WK_SF_CONTACT src 
	                ON target.Id_SF = src.Id_SF 
                WHEN NOT MATCHED BY TARGET THEN  
	                INSERT (Id_SF,Name,Phone,Email,IOBSPStart__c,IOBSPEnd__c,SellerHashedCuid__c,CreatedDate,LastModifiedDate, Validity_StartDate)
	                VALUES (src.Id_SF,src.Name,src.Phone,src.Email,src.IOBSPStart__c,src.IOBSPEnd__c,src.SellerHashedCuid__c,src.CreatedDate,src.LastModifiedDate, src.LastModifiedDate) 
                WHEN MATCHED AND 
	                (target.LastModifiedDate != src.LastModifiedDate)
                THEN UPDATE SET
                         target.Name                = src.Name
                       , target.Phone               = src.Phone
                       , target.Email               = src.Email
                       , target.IOBSPStart__c       = src.IOBSPStart__c
                       , target.IOBSPEnd__c         = src.IOBSPEnd__c
                       , target.SellerHashedCuid__c = src.SellerHashedCuid__c
                       , target.CreatedDate         = src.CreatedDate
                       , target.LastModifiedDate    = src.LastModifiedDate
                       , target.Validity_StartDate  = src.LastModifiedDate
                       , target.Validity_EndDate    = default;";
            return sql;
        }
        #endregion

        #region MERGE REFERENTIEL
        private static string GetSQL_Referentiel(string target)
        {
            string sql = @"WITH SPLIT_WK_SF_REFERENTIEL AS (
SELECT [ID]
     , [OBJECT_CODE_SF]
     , [FIELD_CODE_SF]
     , [FIELD_NAME_SF]
     , [PARENT_FIELD_CODE_SF]
     , CAST([IDX_SF] AS INT) AS [IDX_SF]
     , [CODE_SF]
     , [LIBELLE_SF]
     , [IS_DEFAULT]
     , [IS_ACTIVE]
     , [VALID_FOR]
     , [VALID_FOR_MAP]
     , CAST([value] AS INT) AS [PARENT_IDX_SF] /* Result of CROSS APPLY STRING_SPLIT */
  FROM WK_SF_REFERENTIEL
  CROSS APPLY STRING_SPLIT([VALID_FOR], ';')
),

PREPARE_RAW_SF_REFERENTIEL AS (
SELECT R.[OBJECT_CODE_SF]
     , R.[FIELD_CODE_SF]
     , R.[FIELD_NAME_SF]
     , R.[IDX_SF]
     , R.[CODE_SF]
     , R.[LIBELLE_SF]
     , R.[IS_DEFAULT]
     , R.[IS_ACTIVE]
     , R.[PARENT_FIELD_CODE_SF]
     , P.[FIELD_NAME_SF] AS [PARENT_FIELD_NAME_SF]
     , R.[PARENT_IDX_SF]
     , P.[CODE_SF] AS [PARENT_CODE_SF]
     , P.[LIBELLE_SF] AS [PARENT_LIBELLE_SF]
   FROM SPLIT_WK_SF_REFERENTIEL R
   LEFT JOIN SPLIT_WK_SF_REFERENTIEL P
     ON R.[OBJECT_CODE_SF] = P.[OBJECT_CODE_SF] AND 
        R.[PARENT_FIELD_CODE_SF] = P.[FIELD_CODE_SF] AND
        R.[PARENT_IDX_SF] = P.[IDX_SF]
)

MERGE " + target + @" target 
USING PREPARE_RAW_SF_REFERENTIEL src 
  ON target.[OBJECT_CODE_SF] = src.[OBJECT_CODE_SF] AND
     target.[FIELD_CODE_SF]  = src.[FIELD_CODE_SF]  AND
     target.[IDX_SF]         = src.[IDX_SF]         AND
     target.[PARENT_IDX_SF]  = src.[PARENT_IDX_SF]
WHEN MATCHED AND ( target.[FIELD_NAME_SF]        <> src.[FIELD_NAME_SF] OR
                   target.[CODE_SF]              <> src.[CODE_SF] OR
                   target.[LIBELLE_SF]           <> src.[LIBELLE_SF] OR
                   target.[IS_DEFAULT]           <> src.[IS_DEFAULT] OR
                   target.[IS_ACTIVE]            <> src.[IS_ACTIVE] OR
                   target.[PARENT_FIELD_CODE_SF] <> src.[PARENT_FIELD_CODE_SF] OR
                   target.[PARENT_FIELD_NAME_SF] <> src.[PARENT_FIELD_NAME_SF] OR
                   target.[PARENT_CODE_SF]       <> src.[PARENT_CODE_SF] OR
                   target.[PARENT_LIBELLE_SF]    <> src.[PARENT_LIBELLE_SF] ) THEN
UPDATE SET target.[FIELD_NAME_SF]        = src.[FIELD_NAME_SF]
         , target.[CODE_SF]              = src.[CODE_SF]
         , target.[LIBELLE_SF]           = src.[LIBELLE_SF]
         , target.[IS_DEFAULT]           = src.[IS_DEFAULT]
         , target.[IS_ACTIVE]            = src.[IS_ACTIVE]
         , target.[PARENT_FIELD_CODE_SF] = src.[PARENT_FIELD_CODE_SF]
         , target.[PARENT_FIELD_NAME_SF] = src.[PARENT_FIELD_NAME_SF]
         , target.[PARENT_CODE_SF]       = src.[PARENT_CODE_SF]
         , target.[PARENT_LIBELLE_SF]    = src.[PARENT_LIBELLE_SF]
WHEN NOT MATCHED BY TARGET THEN  
INSERT ( [OBJECT_CODE_SF], [FIELD_CODE_SF], [FIELD_NAME_SF], [IDX_SF], [CODE_SF], [LIBELLE_SF], [IS_DEFAULT], [IS_ACTIVE], [PARENT_FIELD_CODE_SF], [PARENT_FIELD_NAME_SF], [PARENT_IDX_SF], [PARENT_CODE_SF], [PARENT_LIBELLE_SF] )
VALUES ( src.[OBJECT_CODE_SF], src.[FIELD_CODE_SF], src.[FIELD_NAME_SF], src.[IDX_SF], src.[CODE_SF], src.[LIBELLE_SF], src.[IS_DEFAULT], src.[IS_ACTIVE], src.[PARENT_FIELD_CODE_SF], src.[PARENT_FIELD_NAME_SF], src.[PARENT_IDX_SF], src.[PARENT_CODE_SF], src.[PARENT_LIBELLE_SF] ) 
WHEN NOT MATCHED BY SOURCE THEN
DELETE;";

            return sql;
        }
        #endregion

        #region MERGE RAW_SF_RECORDTYPE
        private static string GetSQL_RawRecordType(string target)
        {
            string sql = @"MERGE " + target + @" target 
USING WK_SF_RECORDTYPE src 
  ON target.[Id_SF] = src.[Id_SF]
WHEN MATCHED AND ( target.[SobjectType]       <> src.[SobjectType] OR
                   target.[DeveloperName]     <> src.[DeveloperName] OR
                   target.[Name]              <> src.[Name] OR
                   target.[Description]       <> src.[Description] OR
                   target.[NamespacePrefix]   <> src.[NamespacePrefix] OR
                   target.[IsActive]          <> src.[IsActive] OR
                   target.[IsPersonType]      <> src.[IsPersonType] OR
                   target.[BusinessProcessId] <> src.[BusinessProcessId] OR
                   target.[CreatedById]       <> src.[CreatedById] OR
                   target.[CreatedDate]       <> src.[CreatedDate] OR
                   target.[LastModifiedById]  <> src.[LastModifiedById] OR
                   target.[LastModifiedDate]  <> src.[LastModifiedDate] OR
                   target.[SystemModstamp]    <> src.[SystemModstamp] ) THEN
UPDATE SET target.[SobjectType]       = src.[SobjectType]
         , target.[DeveloperName]     = src.[DeveloperName]
         , target.[Name]              = src.[Name]
         , target.[Description]       = src.[Description]
         , target.[NamespacePrefix]   = src.[NamespacePrefix]
         , target.[IsActive]          = src.[IsActive]
         , target.[IsPersonType]      = src.[IsPersonType]
         , target.[BusinessProcessId] = src.[BusinessProcessId]
         , target.[CreatedById]       = src.[CreatedById]
         , target.[CreatedDate]       = src.[CreatedDate]
         , target.[LastModifiedById]  = src.[LastModifiedById]
         , target.[LastModifiedDate]  = src.[LastModifiedDate]
         , target.[SystemModstamp]    = src.[SystemModstamp]
WHEN NOT MATCHED BY TARGET THEN  
INSERT ( [Id_SF], [SobjectType], [DeveloperName], [Name], [Description], [NamespacePrefix], [IsActive], [IsPersonType], [BusinessProcessId], [CreatedById], [CreatedDate], [LastModifiedById], [LastModifiedDate], [SystemModstamp] )
VALUES ( src.[Id_SF], src.[SobjectType], src.[DeveloperName], src.[Name], src.[Description], src.[NamespacePrefix], src.[IsActive], src.[IsPersonType], src.[BusinessProcessId], src.[CreatedById], src.[CreatedDate], src.[LastModifiedById], src.[LastModifiedDate], src.[SystemModstamp] ) 
WHEN NOT MATCHED BY SOURCE THEN
DELETE;";

            return sql;
        }
        #endregion

        #region MERGE REF_RECORDTYPE
        private static string GetSQL_RefRecordType(string target)
        {
            string sql = @"MERGE " + target + @" target 
USING WK_SF_RECORDTYPE src 
  ON target.[ID_SF] = src.[Id_SF]
WHEN MATCHED AND ( target.[OBJECTTYPE_SF]  <> src.[SobjectType] OR
                   target.[CODE_SF]        <> src.[DeveloperName] OR
                   target.[NAME_SF]        <> src.[Name] OR
                   target.[DESCRIPTION_SF] <> src.[Description] ) THEN
UPDATE SET target.[OBJECTTYPE_SF]  = src.[SobjectType]
         , target.[CODE_SF]        = src.[DeveloperName]
         , target.[NAME_SF]        = src.[Name]
         , target.[DESCRIPTION_SF] = src.[Description]
WHEN NOT MATCHED BY TARGET THEN  
INSERT ( [OBJECTTYPE_SF], [ID_SF], [CODE_SF], [NAME_SF], [DESCRIPTION_SF] )
VALUES ( src.[SobjectType], src.[Id_SF], src.[DeveloperName], src.[Name], src.[Description] ) 
WHEN NOT MATCHED BY SOURCE THEN
DELETE;";

            return sql;
        }
        #endregion

        #region UpdateOppValidityStartdt
        private static string UpdateOppValidityStartdt()
        {
            string sql = @"update main set main.Validity_StartDate = ofh.CreatedDate
from PV_SF_OPPORTUNITY main
	 inner join 
	 (
			select OpportunityId,  NewValue, CreatedDate, ROW_NUMBER() over ( partition by OpportunityId order by CreatedDate desc ) as flag_max_stade
			from WK_SF_OPPORTUNITYFIELDHISTORY
			where Field = 'StageName' 
	 ) ofh
	 on ofh.OpportunityId = main.Id_SF and ofh.flag_max_stade = 1
where main.Validity_StartDate  < ofh.CreatedDate
  and main.stageName = ofh.NewValue;";
            return sql;
        }
        #endregion

        //------------------------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------------------------
        #region Log handling
        private static long CreateLog(Data_Log log)
        {
            long i = 0;
            using (var db = new sfDataHub())
            {
                db.Data_Log.Add(new Data_Log
                {
                    ProcessingType = log.ProcessingType,
                    ETLName = log.ETLName,
                    FileOrTableName = log.FileOrTableName,
                    ProcessingStartDate = log.ProcessingStartDate,
                    ProcessingEndDate = log.ProcessingEndDate,
                    ProcessingStatus = log.ProcessingStatus,
                    NbRecordsProcessed = log.NbRecordsProcessed
                });
                db.SaveChanges();
                i = db.Data_Log.Local.OrderByDescending(w => w.Id).FirstOrDefault().Id;
            }
            return i;
        }
        private static void UpdateLog(long idlog, int? nbRecords)
        {
            using (var db = new sfDataHub())
            {
                Data_Log log = db.Data_Log.Where(x => x.Id.Equals(idlog)).FirstOrDefault();
                db.Data_Log.Attach(log);
                log.ProcessingEndDate = DateTime.Now;
                log.ProcessingStatus = "OK";
                log.NbRecordsProcessed = nbRecords;
                db.SaveChanges();
            }
        }
        private static void UpdateLog(long idlog, int? nbRecords, string status, string type)
        {
            using (var db = new sfDataHub())
            {
                Data_Log log = db.Data_Log.Where(x => x.Id.Equals(idlog)).FirstOrDefault();
                db.Data_Log.Attach(log);
                log.ProcessingEndDate = DateTime.Now;
                log.ProcessingStatus = status;
                if (type.Equals("COLLECT"))
                    log.NbRecordsCollected = nbRecords;
                else
                    log.NbRecordsProcessed = nbRecords;
                db.SaveChanges();
            }
        }
        private static void UpdateLog(long idlog, int? nbRecordsProcessed, int? nbRecordsCollected)
        {
            using (var db = new sfDataHub())
            {
                Data_Log log = db.Data_Log.Where(x => x.Id.Equals(idlog)).FirstOrDefault();
                db.Data_Log.Attach(log);
                log.ProcessingEndDate = DateTime.Now;
                log.ProcessingStatus = "OK";
                log.NbRecordsProcessed = nbRecordsProcessed;
                log.NbRecordsCollected = nbRecordsCollected;
                db.SaveChanges();
            }
        }
        #endregion

        #region GetLastSuccessfulSFExtractDate
        private static DateTime GetLastSuccessfulSFExtractDate(string[] args)
        {
            string batchname = GetBatchName(args);
            using (var db = new sfDataHub())
            {
                return db.Param_Collect.Where(x => x.domain.Equals("SF")
                    && x.batch_name.Equals(batchname)
                    && x.flag.Equals("OK")).OrderByDescending(y => y.last_extraction_date).FirstOrDefault().last_extraction_date;
            }
        }
        #endregion        

        #region Delete Records
        private static void DeleteWKRecords(string[] args)
        {
            using (var db = new sfDataHub())
                db.Database.ExecuteSqlCommand(GetTruncateWKString(args));
        }
        
        private static void DeleteOpps()
        {
            using (var db = new sfDataHub())
                db.Database.ExecuteSqlCommand("TRUNCATE Table WK_SF_OPPORTUNITY");
        }
        private static void DeleteOppHistories()
        {
            using (var db = new sfDataHub())
            {
                //db.Database.ExecuteSqlCommand("TRUNCATE Table WK_SF_OPPORTUNITYFIELDHISTORY");
                db.Database.ExecuteSqlCommand("TRUNCATE Table WK_SF_OPPORTUNITYHistory"); 
            }
        }

        private static void DeleteOppIsDeleted()
        {
            using (var db = new sfDataHub())
            {
                db.Database.ExecuteSqlCommand("TRUNCATE Table WK_SF_OPPORTUNITYISDELETED");
            }
        }
        #endregion

        public static long IdOverallProcess { get; set; }

        #region GetTruncateWKString & GetBatchName
        private static string GetTruncateWKString(string[] args)
        {
            return "TRUNCATE Table WK_SF" + args[0].Substring(3);
        }
        private static string GetBatchName(string[] args)
        {
            if (args[0].Equals("RAW_ACCOUNT"))
                return "PGMRUN_API_RAW_ACCOUNT";
            else if (args[0].Equals("RAW_CASE"))
                return "PGMRUN_API_RAW_CASE";
            else if (args[0].Equals("RAW_OPPORTUNITY_History"))
                return "PGMRUN_API_RAW_OPPORTUNITY_History";
            else if (args[0].Equals("RAW_USER"))
                return "PGMRUN_API_RAW_USER";
            else if (args[0].Equals("RAW_TASK"))
                return "PGMRUN_API_RAW_TASK";
            else if (args[0].Equals("RAW_EMAILMESSAGE"))
                return "PGMRUN_API_RAW_EMAILMESSAGE";
            else if (args[0].Equals("RAW_LIVECHATTRANSCRIPT"))
                return "PGMRUN_API_RAW_LIVECHATTRANSCRIPT";
            else if (args[0].Equals("RAW_PROFILE"))
                return "PGMRUN_API_RAW_PROFILE";
            else if (args[0].Equals("RAW_CONTACT"))
                return "PGMRUN_API_RAW_CONTACT";
            else if (args[0].Equals("RAW_REFERENTIEL"))
                return "PGMRUN_API_RAW_REFERENTIEL";
            else if (args[0].Equals("RAW_RECORDTYPE"))
                return "PGMRUN_API_RAW_RECORDTYPE";
            else
                return null;
        }
        #endregion

        #region GetProcessingType
        private static string GetProcessingType(string[] args)
        {
            if (args[0].Equals("WK_OPPORTUNITY") || args[0].Equals("WK_OPPORTUNITYFIELDHISTORY"))
                return "COLLECT";
            else if (args[0].Contains("RAW_"))
                return "COLLECT";
            else
                return "INTEGRATION";
        }
        #endregion

        #region UpdateParamCollect
        private static void UpdateParamCollect(DateTime execdate, string batchname, string execflag)
        {
            using (var db = new sfDataHub())
            {
                db.Param_Collect.Add(new Param_Collect
                {
                    domain = "SF",
                    batch_name = batchname,
                    last_extraction_date = execdate,
                    flag = execflag
                });
                db.SaveChanges();
            }
        }
        #endregion

        #region ParseDate
        public static DateTime? ParseDate(string dat)
        {
            if (String.IsNullOrEmpty(dat))
                return null;
            else return DateTime.Parse(dat);
        }
        #endregion


        //------------------------------------------------------------------------------------------------------------
        #region [Obsolete] GetSFObjects
        //------------------------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------------------------
        [Obsolete]
        private static async Task GetSFObjects()
        {
            /*
            //STARTING...
            long idprocess = CreateLog(new Data_Log { ProcessingType = "PKG_process_SFCRM_tables", ETLName = "sfExtraction", FileOrTableName = "so_objects", ProcessingStartDate = DateTime.Now, ProcessingEndDate = null, ProcessingStatus = null, NbRecordsProcessed = null });
            idOverallProcess = idprocess;
            */
            //Get the access token from the refresh token
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            sfRefreshToken sfAccessToken = await AcquireAccessTokenFromRefreshTokenAsync();
            ForceClient client = sfAccessToken.GetTheForceClient();
            /*
            //Get the latest SUCCESSFUL SF objects extraction date
            string extractDate = GetLastSuccessfulSFExtractDate().ToString("yyyy-MM-ddTHH:mm:ssZ", CultureInfo.InvariantCulture);
            */
            /*
            //------------------------------------------------------------------------------------------------------------
            //Accounts
            long idaccount = CreateLog(new Data_Log { ProcessingType = "PKG_process_SFCRM_tables", ETLName = "sfExtraction", FileOrTableName = "tmp_SFCRM_Account", ProcessingStartDate = DateTime.Now, ProcessingEndDate = null, ProcessingStatus = null, NbRecordsProcessed = null });

            var accounts = new List<Account>();
            var so = await client.QueryAsync<Account>(SalesforceService.GetAppSetting("Req:account")+extractDate);
            accounts.AddRange(so.Records);
            var nextRecordsUrl = so.NextRecordsUrl;
            if (!string.IsNullOrEmpty(nextRecordsUrl))
            {
                while (true)
                {
                    so = await client.QueryContinuationAsync<Account>(nextRecordsUrl);
                    accounts.AddRange(so.Records);
                    if (string.IsNullOrEmpty(so.NextRecordsUrl))
                        break;
                    nextRecordsUrl = so.NextRecordsUrl;
                }
            }
            int nbaccounts = accounts.Count();
            nextRecordsUrl = null;
            */
            /*
            //------------------------------------------------------------------------------------------------------------
            //Cases
            long idcase = CreateLog(new Data_Log { ProcessingType = "PKG_process_SFCRM_tables", ETLName = "sfExtraction", FileOrTableName = "tmp_SFCRM_Case", ProcessingStartDate = DateTime.Now, ProcessingEndDate = null, ProcessingStatus = null, NbRecordsProcessed = null });

            var cases = new List<Case>();
            var so1 = await client.QueryAsync<Case>(SalesforceService.GetAppSetting("Req:case") + extractDate);
            cases.AddRange(so1.Records);
            nextRecordsUrl = so1.NextRecordsUrl;
            if (!string.IsNullOrEmpty(nextRecordsUrl))
            {
                while(true)
                {
                    so1 = await client.QueryContinuationAsync<Case>(nextRecordsUrl);
                    cases.AddRange(so1.Records);
                    if (string.IsNullOrEmpty(so1.NextRecordsUrl))
                        break;
                    nextRecordsUrl = so1.NextRecordsUrl;
                }
            }
            int nbcases = cases.Count();
            nextRecordsUrl = null;
            */
            /*
            //------------------------------------------------------------------------------------------------------------
            //Opportunities
            long idopportunity = CreateLog(new Data_Log { ProcessingType = "PKG_process_SFCRM_tables", ETLName = "sfExtraction", FileOrTableName = "tmp_SFCRM_Opportunity", ProcessingStartDate = DateTime.Now, ProcessingEndDate = null, ProcessingStatus = null, NbRecordsProcessed = null });

            var opportunities = new List<Opportunity>();
            var so2 = await client.QueryAsync<Opportunity>(SalesforceService.GetAppSetting("Req:opportunity") + extractDate);
            opportunities.AddRange(so2.Records);
            nextRecordsUrl = so2.NextRecordsUrl;
            if (!string.IsNullOrEmpty(nextRecordsUrl))
            {
                while (true)
                {
                    so2 = await client.QueryContinuationAsync<Opportunity>(nextRecordsUrl);
                    opportunities.AddRange(so2.Records);
                    if (string.IsNullOrEmpty(so2.NextRecordsUrl))
                        break;
                    nextRecordsUrl = so2.NextRecordsUrl;
                }
            }
            int nbopportunities = opportunities.Count();
            nextRecordsUrl = null;
            */
            /*
            //------------------------------------------------------------------------------------------------------------
            //OpportunityFieldHistory
            long idopportunityhistory = CreateLog(new Data_Log { ProcessingType = "PKG_process_SFCRM_tables", ETLName = "sfExtraction", FileOrTableName = "raw_SFCRM_OpportunityFieldHistory", ProcessingStartDate = DateTime.Now, ProcessingEndDate = null, ProcessingStatus = null, NbRecordsProcessed = null });

            var opportunityhistories = new List<OpportunityFieldHistory>();
            var so3 = await client.QueryAsync<OpportunityFieldHistory>(SalesforceService.GetAppSetting("Req:opportunityfieldhistory") + extractDate);
            opportunityhistories.AddRange(so3.Records);
            nextRecordsUrl = so3.NextRecordsUrl;
            if (!string.IsNullOrEmpty(nextRecordsUrl))
            {
                while (true)
                {
                    so3 = await client.QueryContinuationAsync<OpportunityFieldHistory>(nextRecordsUrl);
                    opportunityhistories.AddRange(so3.Records);
                    if (string.IsNullOrEmpty(so3.NextRecordsUrl))
                        break;
                    nextRecordsUrl = so3.NextRecordsUrl;
                }
            }
            int nbopportunityhistories = opportunityhistories.Count();
            nextRecordsUrl = null;
            */
            /*
            //------------------------------------------------------------------------------------------------------------
            //------------------------------------------------------------------------------------------------------------
            using (var db = new sfDataHub())
            {

                #region accounts
                accounts.ForEach(x =>
                    db.tmp_SFCRM_Account.Add(new tmp_SFCRM_Account
                    {
                        Id_SF = x.Id,
                        BehaviouralScoring__c = x.BehaviouralScoring__c,
                        CreatedDate = x.CreatedDate,
                        DeathDate__pc = x.DeathDate__pc,
                        DistributorNetwork__c = x.DistributorNetwork__c,
                        EmployeeType__pc = x.EmployeeType__pc,
                        GeolifeSegment__c = x.GeolifeSegment__c,
                        IDCustomerCID__c = x.IDCustomerCID__c,
                        IDCustomerPID__c = x.IDCustomerPID__c,
                        IDCustomer__pc = x.IDCustomer__pc,
                        IDCustomerSAB__pc = x.IDCustomerSAB__pc,
                        LastModifiedDate = x.LastModifiedDate,
                        OBFirstContactDate__c = x.OBFirstContactDate__c,
                        OBSeniority__c = x.OBSeniority__c,
                        OBTerminationDate__c = x.OBTerminationDate__c,
                        OBTerminationReason__c = x.OBTerminationReason__c,
                        Occupation__pc = x.Occupation__pc,
                        OptInDataTelco__c = x.OptInDataTelco__c,
                        OptInOrderTelco__c = x.OptInOrderTelco__c,
                        OwnerId = x.OwnerId,
                        ParentId = x.ParentId,
                        PersonBirthdate = x.PersonBirthdate,
                        PersonLeadSource = x.PersonLeadSource,
                        RecordTypeId = x.RecordTypeId,
                        SystemModstamp = x.SystemModstamp
                    })
                );
                #endregion



                #region cases
                cases.ForEach(x =>
                   db.tmp_SFCRM_Case.Add(new tmp_SFCRM_Case
                   {
                       Id_SF = x.Id,
                       AccountId = x.AccountId,
                       ActionPlan__c = x.ActionPlan__c,
                       ARDate__c = x.ARDate__c,
                       CaseNumber = x.CaseNumber,
                       ClosedDate = x.ClosedDate,
                       CommercialGestAmount__c = x.CommercialGestAmount__c,
                       CreatedById = x.CreatedById,
                       CreatedDate = x.CreatedDate,
                       CustomerType__c = x.CustomerType__c,
                       Description = x.Description,
                       DUT__c = x.DUT__c,
                       ExpiredMilestone__c = x.ExpiredMilestone__c,
                       FirstClaim__c = x.FirstClaim__c,
                       Habilitation__c = x.Habilitation__c,
                       InitialCase__c = x.InitialCase__c,
                       isDoneWithExpMilestone__c = x.isDoneWithExpMilestone__c,
                       IsDoneWithRespMilestone__c = x.IsDoneWithRespMilestone__c,
                       IsDone__c = x.IsDone__c,
                       isNotDoneExpMilestone__c = x.isNotDoneExpMilestone__c,
                       IsNotDoneWithRespMilestone__c = x.IsNotDoneWithRespMilestone__c,
                       IsNotDone__c = x.IsNotDone__c,
                       LastModifiedDate = x.LastModifiedDate,
                       LevelClaim__c = x.LevelClaim__c,
                       LostAmount__c = x.LostAmount__c,
                       Metier__c = x.Metier__c,
                       MilestoneStatus = x.MilestoneStatus,
                       Network__c = x.Network__c,
                       Origin = x.Origin,
                       Origin__c = x.Origin__c,
                       OwnerId = x.OwnerId,
                       ParentId = x.ParentId,
                       ParentOppy__c = x.ParentOppy__c,
                       PrimaryQualification__c = x.PrimaryQualification__c,
                       PrimarySubject__c = x.PrimarySubject__c,
                       Priority = x.Priority,
                       Process__c = x.Process__c,
                       queueName__c = x.queueName__c,
                       Reason = x.Reason,
                       RecordTypeId = x.RecordTypeId,
                       ResponseChannel__c = x.ResponseChannel__c,
                       ResponseDate__c = x.ResponseDate__c,
                       RetrocessionAmount__c = x.RetrocessionAmount__c,
                       SecondarySubject__c = x.SecondarySubject__c,
                       SecondQualification__c = x.SecondQualification__c,
                       StandBy__c = x.StandBy__c,
                       Status = x.Status,
                       SubType__c = x.SubType__c,
                       TechCloseCase__c = x.TechCloseCase__c,
                       Tech_Number_casemilestones_closed__c = x.Tech_Number_casemilestones_closed__c,
                       Tech_PreviousOwner__c = x.Tech_PreviousOwner__c,
                       Tech_ReviewDateExpired__c = x.Tech_ReviewDateExpired__c,
                       TreatementReason__c = x.TreatementReason__c,
                       Type = x.Type
                   }) 
                );
                #endregion



                #region opportunities
                opportunities.ForEach(x =>
                   db.tmp_SFCRM_Opportunity.Add(new tmp_SFCRM_Opportunity
                   {
                       Id_SF = x.Id,
                       AccountId = x.AccountId,
                       AdvisorCode__c = x.AdvisorCode__c,
                       CampaignId__c = x.CampaignId__c,
                       CloseDate = x.CloseDate,
                       CommercialOfferCode__c = x.CommercialOfferCode__c,
                       CommercialOfferName__c = x.CommercialOfferName__c,
                       CompleteFileFlag__c = x.CompleteFileFlag__c,
                       CreatedById = x.CreatedById,
                       CreatedDate = x.CreatedDate,
                       DeniedOpportunityReason__c = x.DeniedOpportunityReason__c,
                       DistributionChannel__c = x.DistributionChannel__c,
                       DistributorEntity__c = x.DistributorEntity__c,
                       DistributorNetwork__c = x.DistributorNetwork__c,
                       FinalizedBy__c = x.FinalizedBy__c,
                       FormValidation__c = x.FormValidation__c,
                       IDOppty__c = x.IDOppty__c,
                       IDProcessSous__c = x.IDProcessSous__c,
                       IdSource__c = x.IdSource__c,
                       Indication__c = x.Indication__c,
                       LastModifiedDate = x.LastModifiedDate,
                       LeadSource = x.LeadSource,
                       Manager__c = x.Manager__c,
                       NoIndication__c = x.NoIndication__c,
                       OwnerId = x.OwnerId,
                       PointOfSaleCode__c = x.PointOfSaleCode__c,
                       ProductFamilyCode__c = x.ProductFamilyCode__c,
                       ProductFamilyName__c = x.ProductFamilyName__c,
                       ProvenanceIndicationIndicatorId__c = x.ProvenanceIndicationIndicatorId__c,
                       RecordTypeId = x.RecordTypeId,
                       RejectReason__c = x.RejectReason__c,
                       RelationEntryScore__c = x.RelationEntryScore__c,
                       StageName = x.StageName,
                       StartedChannel__c = x.StartedChannel__c,
                       SystemModstamp = x.SystemModstamp,
                       Type = x.Type
                   })
                );
                #endregion

                #region opportunityhistories
                opportunityhistories.ForEach(x =>
                   db.tmp_SFCRM_OpportunityFieldHistory.Add(new tmp_SFCRM_OpportunityFieldHistory
                   {
                       Id_SF = x.Id,
                       CreatedByID = x.CreatedByID,
                       CreatedDate = x.CreatedDate,
                       Field = x.Field,
                       IsDeleted = x.IsDeleted,
                       NewValue = x.NewValue,
                       OldValue = x.OldValue,
                       OpportunityId = x.OpportunityId
                   })
                );
                #endregion                

                await db.SaveChangesAsync();
            }
            */

            /*
            UpdateLog(idaccount, nbaccounts);
            UpdateLog(idcase, nbcases);
            UpdateLog(idopportunity, nbopportunities);
            UpdateLog(idopportunityhistory, nbopportunityhistories);
            */

            //HandleOpportunities();
            //HandleAccounts();
            //HandleCases();
            //UpdateLog(idprocess, null);
        }
        #endregion


        //------------------------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------------------------
        #region AcquireAccessTokenFromRefreshTokenAsync
        private static async Task<sfRefreshToken> AcquireAccessTokenFromRefreshTokenAsync()
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                using (AuthenticationClient authenticationClient = new AuthenticationClient())
                {
                    authenticationClient.ApiVersion = SalesforceService.GetAppSetting("Salesforce:ApiVersion");

                    await authenticationClient.TokenRefreshAsync(
                        SalesforceService.GetAppSetting("Salesforce:ConsumerKey"),
                        SalesforceService.GetAppSetting("Salesforce:RefreshTokenKey"),
                        string.Empty,
                        SalesforceService.GetAppSetting("Salesforce:Domain") + "/services/oauth2/token");

                    sfRefreshToken token = new sfRefreshToken(authenticationClient);
                    return token;
                }
            }
            catch (ForceException e)
            {
                if (e.Error != Error.InvalidGrant) { throw; }
                return null;
            }
        }
        #endregion
        //------------------------------------------------------------------------------------------------------------

        public static int GetBase64Code (int c)
        {
            // Numerical values
            if (c >= 48 && c <= 57 )
                return c + 4;

            // Upper case values
            if (c >= 65 && c <= 90)
                return c - 65;

            // Lower case values
            if (c >= 97 && c <= 122 )
                return c - 71;

            // +
            if (c == 43)
                return 62;

            // /
            if (c == 47)
                return 63;

            return -1;
        }
    }

    #region sfRefreshToken
    public class sfRefreshToken
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public string InstanceUrl { get; set; }
        public string ApiVersion { get; set; }

        /// <summary>
        /// Initializes a Salesforce token using an existing AuthenticationClient.
        /// </summary>
        /// <param name="authenticationClient">The AuthenticationClient from which to initialize the token.</param>
        public sfRefreshToken(AuthenticationClient authenticationClient)
        {
            if (authenticationClient == null)
            {
                throw new ArgumentNullException("authenticationClient");
            }

            this.AccessToken = authenticationClient.AccessToken;
            this.RefreshToken = authenticationClient.RefreshToken;
            this.InstanceUrl = authenticationClient.InstanceUrl;
            this.ApiVersion = authenticationClient.ApiVersion;
        }

        /// <summary>
        /// Get a ForceClient based on the Salesforce token.
        /// </summary>
        public ForceClient GetTheForceClient()
        {
            return new ForceClient(this.InstanceUrl, this.AccessToken, this.ApiVersion);
        }
    }
    #endregion

}
