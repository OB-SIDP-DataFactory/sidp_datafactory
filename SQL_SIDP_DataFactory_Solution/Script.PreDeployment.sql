﻿PRINT '********************************************'
PRINT 'Executing PreDeployment scripts started ...'
PRINT '********************************************'
PRINT 'Executing Truncate_WK_tables script ...'
:r .\data\Truncate_WK_tables.sql
USE [$(DataFactoryDatabaseName)]
GO
PRINT '*****************************************'
PRINT 'Executing PreDeployment scripts finished.'
PRINT '*****************************************'