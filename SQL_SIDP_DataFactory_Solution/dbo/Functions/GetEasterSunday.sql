﻿
CREATE FUNCTION [dbo].[GetEasterSunday] ( @EasterYear INT ) RETURNS DATE
AS 
BEGIN
   -- Calculation of the date of Easter Sunday - Butcher Method

   -- Dividend               | Divisor | Quotient | Rest | Expression
   -- ---------------------- ----------------------------------------------------------------------
   -- Year                   | 19      |          | n    | cycle de Méton
   -- Year                   | 100     | c        | u    | centaine et rang de l'année
   -- c                      | 4       | s        | t    | siècle bissextile
   -- c + 8                  | 25      | p        |      | cycle de proemptose
   -- c - p + 1              | 3       | q        |      | proemptose
   -- 19 n + c - s - q + 15  | 30      |          | e    | épacte
   -- u                      | 4       | b        | d    | année bissextile
   -- 2 t + 2 b - e - d + 32 | 7       |          | L    | lettre dominicale
   -- n + 11 e + 22 L        | 451     | h        |      | correction
   -- e + L - 7 h + 114      | 31      | m        | j    | mois et quantième du Samedi saint

    DECLARE @n INT = @EasterYear % 19
    DECLARE @c INT = @EasterYear / 100
    DECLARE @u INT = @EasterYear % 100
    DECLARE @s INT = @c / 4
    DECLARE @t INT = @c % 4
    DECLARE @p INT = (@c + 8) / 25
    DECLARE @q INT = (@c - @p + 1) / 3
    DECLARE @e INT = ((19 * @n) + @c - @s - @q + 15) % 30
    DECLARE @b INT = @u / 4
    DECLARE @d INT = @u % 4
    DECLARE @L INT = ((2 * @t) + (2 * @b) - @e - @d + 32) % 7
    DECLARE @h INT = (@n + (11 * @e) + (22 * @L)) / 451
    DECLARE @EasterMonth INT = (@e + @L - (7 * @h) + 114) / 31
    DECLARE @EasterDay INT = ((@e + @L - (7 * @h) + 114) % 31) + 1

	RETURN CAST(DATEFROMPARTS(@EasterYear, @EasterMonth, @EasterDay) AS DATE)
END