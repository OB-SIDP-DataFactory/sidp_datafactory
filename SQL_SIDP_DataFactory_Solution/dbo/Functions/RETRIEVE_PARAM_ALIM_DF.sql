﻿

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[RETRIEVE_PARAM_ALIM_DF] 
(
	-- Add the parameters for the function here
	@domain nvarchar(50), 
	@batch_name nvarchar(50)
)
RETURNS date
WITh EXECUTE AS CALLER
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ResultVar date

	-- Add the T-SQL statements to compute the return value here
	SELECT @ResultVar = DAT_VALD
	FROM  [dbo].[PARAM_ALIM]
	WHERE DOMN = @domain and NOM_BATCH = @batch_name 
		and STATUT = 'OK'
		and DAT_VALD = ( select max(DAT_VALD) from  [dbo].[PARAM_ALIM] where DOMN = @domain and NOM_BATCH = @batch_name and STATUT='OK');		
	-- Return the result of the function
	RETURN @ResultVar

END