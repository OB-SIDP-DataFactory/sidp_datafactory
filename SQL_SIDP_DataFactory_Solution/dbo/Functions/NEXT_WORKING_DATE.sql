-- =============================================
-- Author:		HBO
-- Create date: 2019-05-20
-- Description:	Get the next working date of given date
-- =============================================
CREATE FUNCTION NEXT_WORKING_DATE
(
	@DAY_DATE	AS	DATE,
	@OFFSET		AS	INT	
)
RETURNS DATE
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ResultVariable	DATE;

	WITH WORKED_DAYS AS
	(
		SELECT
			CAST([Date]	AS	DATE)	AS	StandardDate
		FROM
			dbo.REF_TEMPS
		WHERE
			1 = 1
			AND	BusinessDay = 1
			AND	[Date] BETWEEN DATEADD(DAY, -5, @DAY_DATE) AND DATEADD(DAY, 5, @DAY_DATE)
	),
	PREVIOUS_NEXT_WORKING_DAY AS
	(
		SELECT
			StandardDate,
			LAG(StandardDate,@OFFSET,NULL) OVER (ORDER BY StandardDate DESC)	AS	NEXT_WORKING_DAY
		FROM
			WORKED_DAYS
		WHERE
			1 = 1
	)
	SELECT
		@ResultVariable = NEXT_WORKING_DAY
	FROM
		PREVIOUS_NEXT_WORKING_DAY
	WHERE
		1 = 1
		AND DATEDIFF(DAY,StandardDate,@DAY_DATE) = 0

	-- Return the result of the function
	RETURN ISNULL(@ResultVariable, dbo.NEXT_WORKING_DATE(DATEADD(DAY,-1,@DAY_DATE),@OFFSET))

END
GO