﻿-- ======================================================================
-- Author:		Hamza BOUKRAA
-- Create date: 2019-06-18
-- Description:	Returns a table indicating if the processing is finished.
-- ======================================================================
CREATE FUNCTION dbo.SSRS_Process_Completion
(
	@ExecutionDate	AS	DATETIME2
)
RETURNS @returntable TABLE
(
	Frequency						CHAR(1),
	Domain							varchar(20),
	TotalProcessed					INT,
	ExpectedBatchCount				INT,
	LastProcessingEndDate			DATETIME2,
	ProcessingDateTime				DATETIME2,
	MinutesElapsedSinceCompletion	INT,
	FlagSendEmail					BIT
)
AS
BEGIN
	DECLARE
		@ExecutionDateTime		AS	DATETIME2 = ISNULL(@ExecutionDate, GETDATE());
	DECLARE
		@DailyStartDateTime		AS	DATETIME2 = DATEADD(HOUR, 20, CAST(dbo.PREVIOUS_WORKING_DATE(@ExecutionDateTime, IIF(DATEPART(HOUR, @ExecutionDateTime) < 20, 1, 0))	AS	DATETIME2)),
		@WeeklyStartDateTime	AS	DATETIME2 = DATEADD(HOUR, 20, CAST(dbo.PREVIOUS_WORKING_DATE(DATEADD(DAY, -DATEPART(DW, @ExecutionDateTime)+1 , @ExecutionDateTime), IIF(DATEPART(HOUR, @ExecutionDateTime) < 20, 1, 0))	AS	DATETIME2)),
		@MonthlyStartDateTime	AS	DATETIME2 = DATEADD(HOUR, 20, CAST(dbo.PREVIOUS_WORKING_DATE(DATEADD(DAY, 28, DATEADD(MONTH, DATEDIFF(MONTH, 0, @ExecutionDateTime)-1, 0)), IIF(DATEPART(HOUR, @ExecutionDateTime) < 20, 1, 0))	AS	DATETIME2));

	INSERT @returntable
	SELECT
		PA.Frequency,
		PA.Domain,
		SL.TotalProcessed,
		PA.ExpectedBatchCount,
		SL.LastProcessingEndDate,
		@ExecutionDateTime				AS	ProcessingDateTime,
		DATEDIFF(
			MINUTE,
			SL.LastProcessingEndDate,
			@ExecutionDateTime)			AS	MinutesElapsedSinceCompletion,
		IIF(
			(DATEDIFF(MINUTE, SL.LastProcessingEndDate, @ExecutionDateTime)<5) AND (SL.TotalProcessed = PA.ExpectedBatchCount),
			1,
			0)						AS	FlagSendEmail
	FROM
		(
			SELECT
				RT.FRQN						AS	Frequency,
				RT.DOMN						AS	Domain,
				COUNT(*)					AS	TotalProcessed,
				MAX(DL.ProcessingEndDate)	AS	LastProcessingEndDate
			FROM
				[$(DataHubDatabaseName)].dbo.Data_Log					AS	DL	WITH(NOLOCK)
				JOIN	dbo.REF_TRAITEMENT	AS	RT	WITH(NOLOCK)
						ON	DL.ETLName = RT.NOM_TRTM
							AND	DL.FileOrTableName = RT.NOM_TABL
							AND	DL.Id = (SELECT MAX(Id) FROM [$(DataHubDatabaseName)].dbo.Data_Log AS DLM WITH(NOLOCK) WHERE DLM.ETLName = RT.NOM_TRTM AND DLM.FileOrTableName = RT.NOM_TABL AND DLM.ProcessingEndDate IS NOT NULL)
			WHERE
				1 = 1
				AND	RT.FLG_ACTIF = 1
				AND DL.ProcessingEndDate IS NOT NULL
				AND	DL.ProcessingStatus = 'OK'
				AND
				(
					(
						RT.FRQN = 'Q'
						AND	DL.ProcessingStartDate >= @DailyStartDateTime
					)
					OR
					(
						RT.FRQN = 'H'
						AND	DL.ProcessingStartDate >= @WeeklyStartDateTime
					)
					OR
					(
						RT.FRQN = 'M'
						AND	DL.ProcessingStartDate >= @MonthlyStartDateTime
					)
				)
			GROUP BY
				RT.FRQN,
				RT.DOMN
		)							AS	SL
		LEFT JOIN
		(
			SELECT
				RT.FRQN		AS	Frequency,
				RT.DOMN		AS	Domain,
				COUNT(*)	AS	ExpectedBatchCount
			FROM
				dbo.REF_TRAITEMENT	AS	RT	WITH(NOLOCK)
			WHERE
				1 = 1
				AND	RT.FLG_ACTIF = 1
			GROUP BY
				RT.FRQN,
				RT.DOMN
		)	AS	PA
			ON	SL.Frequency = PA.Frequency
				AND	SL.Domain = PA.Domain
	RETURN
END
GO