﻿

CREATE PROC [dbo].[PKGCP_PROC_STOCK_CASE_REQUEST]
  @DATE_ALIM date
, @jours_histo int 
, @nbRows int OUTPUT
AS  BEGIN 
/*
declare @DATE_ALIM date;
declare @jours_histo int ;
set @DATE_ALIM ='2018-05-18';
set  @jours_histo=5;*/

-- ===============================================================================================================================
--tables input : [$(DataHubDatabaseName)].[dbo].[VW_PV_SF_CASE]
--               [DIM_TEMPS]
--table output : CP_STOCK_CASE_REQUEST
--description : créer une table avec l'historique des requêtes/demandes en stock pour chaque date de chargement et client
-- ==============================================================================================================================

if @DATE_ALIM is null 
  set @DATE_ALIM = GETDATE();
 
if @jours_histo is null 
  set @jours_histo = 1; --valeur 1 par défaut = traitement nominal par défaut

if object_id('tempdb..#cp_stock_case_request_old') is not null
begin drop table #cp_stock_case_request_old end

select DTE_ALIM
	  ,[IDE_PERS_SF]
	  ,[CAN_INT_ORI] 
	  ,[LIB_CAN_INT_ORI]
      ,[IDE_TYP_ENR]
      ,[COD_TYP_ENR]
      ,[LIB_TYP_ENR]
	  ,[TYP_REQ]
      ,[LIB_TYP_REQ]
	  ,[NB_REQ]
	  ,[DTE_CREA]
	  ,[DTE_FER_REQ]
	  ,[DELAI_TRAITEMENT]
	  ,[DELAI_MED_TRT_CANAL]
  into #cp_stock_case_request_old
  from [dbo].[CP_STOCK_CASE_REQUEST]
 where DTE_ALIM < cast( dateadd(day,-@jours_histo, @DATE_ALIM) as date);

truncate table [dbo].[CP_STOCK_CASE_REQUEST];

/* créer un sous-ensemble de la table dimension temps     */
/* liste des dates pour lesquelles le stock est recalculé */
with cp_wk_temps_case_request as (
select StandardDate 
  from dbo.DIM_TEMPS
 where cast( dateadd(day,-@jours_histo, @DATE_ALIM) as date) <= StandardDate and StandardDate < cast(@DATE_ALIM as date)
),

cp_stock_case_request_histo as (
select temps.StandardDate as date_alim
     , cas.*
  from cp_wk_temps_case_request as temps
  left join [$(DataHubDatabaseName)].[dbo].[VW_PV_SF_CASE] as cas
  on ( cast(cas.Validity_StartDate as date) <= temps.StandardDate and temps.StandardDate < cast(cas.Validity_EndDate as date) )
), 

cp_stock_case_request_new as (
select date_alim								AS DTE_ALIM 
      ,AccountId								AS IDE_PERS_SF
      ,Origin									AS CAN_INT_ORI 
	  ,Origin_label								AS LIB_CAN_INT_ORI 
	  ,RecordTypeId								AS IDE_TYP_ENR
	  ,RecordTypeCode							AS COD_TYP_ENR
      ,RecordTypeName							AS LIB_TYP_ENR
	  ,Type										AS TYP_REQ
      ,Type_label								AS LIB_TYP_REQ
	  ,COUNT(Id_SF)								AS NB_REQ
	  ,cast(CreatedDate as DATE)				AS DTE_CREA
	  ,cast(ClosedDate as DATE)					AS DTE_FER_REQ
	  ,DATEDIFF(DD, CreatedDate,date_alim)		AS DELAI_TRAITEMENT
	  ,PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY DATEDIFF(DD, CreatedDate,date_alim)) OVER 
			(PARTITION BY date_alim, CASE WHEN Origin_label LIKE 'Appel%' then '04' ELSE Origin END
			) AS DELAI_MED_TRT_CANAL 
	FROM cp_stock_case_request_histo
	WHERE RecordTypeCode= 'OBRequest'				-- <> 'OBMarketingRequest' 
	AND (cast(ClosedDate as DATE) IS NULL OR cast(ClosedDate as DATE) > date_alim) --Demande en cours
	AND cast(CreatedDate as DATE) <= date_alim --Demande créée avant la date d'observation
	AND cast(Validity_StartDate as DATE) <= date_alim AND date_alim < cast(Validity_EndDate as DATE)
	GROUP BY date_alim, 
			AccountId,
			Origin, 
			Origin_label,
			RecordTypeId,
			RecordTypeCode,
			RecordTypeName, 
			Type, 
			Type_label, 
			CreatedDate, 
			ClosedDate
)

insert into [dbo].[CP_STOCK_CASE_REQUEST] (
		[DTE_ALIM]
		,[IDE_PERS_SF]
		,[CAN_INT_ORI] 
		,[LIB_CAN_INT_ORI]
		,[IDE_TYP_ENR]
		,[COD_TYP_ENR]
		,[LIB_TYP_ENR]
		,[TYP_REQ]
		,[LIB_TYP_REQ]
		,[NB_REQ]	
		,[DTE_CREA]
		,[DTE_FER_REQ]
		,[DELAI_TRAITEMENT]
		,[DELAI_MED_TRT_CANAL]
 )
select *
 from ( select DTE_ALIM 
			   ,IDE_PERS_SF
			   ,CAN_INT_ORI 
			   ,LIB_CAN_INT_ORI 
			   ,IDE_TYP_ENR
			   ,COD_TYP_ENR
			   ,LIB_TYP_ENR
			   ,TYP_REQ
			   ,LIB_TYP_REQ
			   ,SUM(NB_REQ) as NB_REQ
			   ,DTE_CREA
			   ,DTE_FER_REQ
			   ,DELAI_TRAITEMENT
			   ,DELAI_MED_TRT_CANAL
			from cp_stock_case_request_new
			group by DTE_ALIM 
			   ,IDE_PERS_SF
			   ,CAN_INT_ORI 
			   ,LIB_CAN_INT_ORI 
			   ,IDE_TYP_ENR
			   ,COD_TYP_ENR
			   ,LIB_TYP_ENR
			   ,TYP_REQ
			   ,LIB_TYP_REQ
			   ,DTE_CREA
			   ,DTE_FER_REQ
			   ,DELAI_TRAITEMENT
			   ,DELAI_MED_TRT_CANAL
         union all
         select * 
			from #cp_stock_case_request_old 
		) cp_stock_case_request
	order by DTE_ALIM desc
;

select @nbRows = count(*) from [dbo].[CP_STOCK_CASE_REQUEST];

END