﻿-- =============================================
--tables input : [$(DataHubDatabaseName)].[dbo].[PV_MC_EMAILLOG]
--               [$(DataHubDatabaseName)].[dbo].[PV_MC_PARRAINAGE]
--				 [$(DataHubDatabaseName)].[dbo].[PV_SF_ACCOUNTLINK]
--              [$(DataHubDatabaseName)]. [exposition].[PV_SF_ACCOUNT]
--				[$(DataHubDatabaseName)]. [exposition].[PV_SF_OPPORTUNITY] 
--tables output : [SAS_Vue_MC_Parrain]
--description : construction d'un data set utilisé pour le section parrainage du rapport MC_Rapport (avec Parrainage)
-- =============================================
CREATE PROCEDURE [dbo].[PKGMC_PROC_vue_parrain] @nbRows int OUTPUT
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

truncate table [dbo].SAS_Vue_MC_Parrain;

with EMAILLOG as (
select distinct ID_CONTACT, eml.MESSAGE_NAME
from [$(DataHubDatabaseName)].[dbo].[PV_MC_EMAILLOG] eml
where eml.CAMPAIGN_NAME like '%PARRAINAGE%' and eml.MESSAGE_NAME not in ('C_ACOM_PARRAINAGE_Parrainage_EMAIL02_07102018','C_COM_TEASINGPARRAINAGE_PARRAINAGE_EMAIL01_07102018')
),
Message_par_pers as (
select distinct ID_CONTACT, 
       STUFF(
				(
					select '+'+ MESSAGE_NAME
					from EMAILLOG
					where ID_CONTACT = A.ID_CONTACT
					for xml path ('')
				)
				,1,1,''
			)as MESSAGE_NAME
from EMAILLOG as A
)
insert into [dbo].SAS_Vue_MC_Parrain
select enseignes.ID_Contact_MC, 
       enseignes.Date_Parrainage, 
	   enseignes.Message_Name, 
	   enseignes.Nb_Filleul_Enseigne, 
	   reussi.*
from 
	(
	select ID_CONTACT_PARRAIN as ID_Contact_MC, 
           DATE_PARRAINAGE as Date_Parrainage,
		   msg.MESSAGE_NAME as Message_Name,
		   COUNT(distinct EMAIL_FILLEUL) as Nb_Filleul_Enseigne,
		   row_number() over (partition by ID_CONTACT_PARRAIN order by DATE_PARRAINAGE desc) as Flag 
	FROM [$(DataHubDatabaseName)].[dbo].[PV_MC_PARRAINAGE] pa
	left join Message_par_pers msg on pa.[ID_CONTACT_PARRAIN] = msg.ID_CONTACT
	group by ID_CONTACT_PARRAIN,
			 DATE_PARRAINAGE,
			 msg.MESSAGE_NAME
	) enseignes

full outer join

	(
	SELECT distinct
      (case when PersonRole1__c = '09' then acc1.IDE_CONTACT_SF when [PersonRole2__c] = '09' then acc2.IDE_CONTACT_SF end) as ID_Contact_SF
     ,(case when PersonRole1__c = '09' then isnull(opp1.FLG_IND,'Non-renseigné') when [PersonRole2__c] = '09' then isnull(opp2.FLG_IND,'Non-renseigné') end) as Indication
     ,(case when PersonRole1__c = '09' then opp1.LIB_ENT_DIS when [PersonRole2__c] = '09' then opp2.LIB_ENT_DIS end) as Entite_Distrib
     ,(case when PersonRole1__c = '09' then isnull(acc1.QUA_EMP,'Non-renseigné') when [PersonRole2__c] = '09' then isnull(acc2.QUA_EMP,'Non-renseigné') end) as Qualification_Employe
	 ,(case when PersonRole1__c = '09' then (case when (acc1.SCO_PREAT is not null and charindex('.',acc1.SCO_PREAT)<>0)  then cast(substring(acc1.SCO_PREAT,1, charindex('.',acc1.SCO_PREAT)-1) as int) else acc1.SCO_PREAT end) when [PersonRole2__c] = '09' then (case when (acc2.SCO_PREAT is not null and charindex('.',acc2.SCO_PREAT)<>0)  then cast(substring(acc2.SCO_PREAT,1, charindex('.',acc2.SCO_PREAT)-1) as int) else acc2.SCO_PREAT end) end) as Pre_Attribution
	 ,(case when PersonRole1__c = '09' then ISNULL(acc1.LIB_SCO_COMP,'Non-renseigné') when [PersonRole2__c] = '09' then ISNULL(acc2.LIB_SCO_COMP,'Non-renseigné') end) as Score_Comportement
	 ,(case when PersonRole1__c = '09' then isnull(acc1.SEG_GEOLIFE,'Non-renseigné') when [PersonRole2__c] = '09' then isnull(acc2.SEG_GEOLIFE,'Non-renseigné') end) as Segment_Geolife
	 ,(case when PersonRole1__c = '09' then DATEDIFF(YEAR,acc1.DTE_NAIS,SYSDATETIME()) when [PersonRole2__c] = '09' then DATEDIFF(YEAR,acc2.DTE_NAIS,SYSDATETIME()) end) as Age
	 ,(case when PersonRole1__c = '09' then cast(opp1.SCO_ENT_REL as integer) when [PersonRole2__c] = '09' then cast(opp2.SCO_ENT_REL as integer) end) as Score_Entree_Relation
	 ,(case when PersonRole1__c = '09' then opp1.LIB_CAN_INT_ORI when [PersonRole2__c] = '09' then opp2.LIB_CAN_INT_ORI end) as Canal_Interaction_Orig
	 ,(case when PersonRole1__c = '09' then opp1.LIB_RES_DIS when [PersonRole2__c] = '09' then opp2.LIB_RES_DIS end) as Reseau_Distrib
	 ,(case when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange Bank' and opp1.LIB_CAN_INT_ORI in ('Appli OB','Web OB') then 'OB_full_selfcare' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange Bank' and opp2.LIB_CAN_INT_ORI in ('Appli OB','Web OB') then 'OB_full_selfcare'
      	  when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange Bank' and opp1.LIB_CAN_INT_ORI in ('Appel entrant CRC', 'Appel sortant CRC') then 'OB_CRC' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange Bank' and opp2.LIB_CAN_INT_ORI in ('Appel entrant CRC', 'Appel sortant CRC') then 'OB_CRC'
		  when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_DOS_COMP = '1' then 'Souscription boutique' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_DOS_COMP = '1' then 'Souscription boutique'
		  when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_DOS_COMP <> '1' and opp1.NUM_PROC_SOUS is not null then 'Repli Selfcare' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_DOS_COMP <> '1' and opp2.NUM_PROC_SOUS is not null then 'Repli Selfcare'
		  when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_IND = 'OUI' and opp1.NUM_PROC_SOUS is null and opp1.LIB_ENT_DIS in ('AD', 'GDT')  then 'Indication boutique' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_IND = 'OUI' and opp2.NUM_PROC_SOUS is null and opp2.LIB_ENT_DIS in ('AD', 'GDT')  then 'Indication boutique'
		  when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_IND = 'OUI' and opp1.NUM_PROC_SOUS is null and opp1.LIB_ENT_DIS in ('OF-WEB')  then 'Indication Digitale' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_IND = 'OUI' and opp2.NUM_PROC_SOUS is null and opp2.LIB_ENT_DIS in ('OF-WEB') then 'Indication Digitale'
		  when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange France' and opp1.LIB_CAN_INT_ORI = 'Web OB' and opp1.LIB_ENT_DIS in ('OF-WEB')  then 'Renvoi trafic' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange France' and opp2.LIB_CAN_INT_ORI = 'Web OB' and opp2.LIB_ENT_DIS in ('OF-WEB')  then 'Renvoi trafic'
	   end) as Reseau_Apporteur
	 ,COUNT( (case when PersonRole1__c = '10' and opp1.STADE_VENTE in ('07','09') then acc1.IDE_CLT_PHY when [PersonRole2__c] = '10' and opp1.STADE_VENTE in ('07','09') then acc2.IDE_CLT_PHY end) ) as Nb_Filleul_Transfo
	 ,COUNT( (case when PersonRole1__c = '10' and opp1.STADE_VENTE in ('10','11') then acc1.IDE_CLT_PHY when [PersonRole2__c] = '10' and opp1.STADE_VENTE in ('10','11') then acc2.IDE_CLT_PHY end) ) as Nb_Sortie_Negative

	FROM [$(DataHubDatabaseName)].[dbo].[PV_SF_ACCOUNTLINK] acclk
	INNER JOIN [$(DataHubDatabaseName)].[exposition].[PV_SF_ACCOUNT] acc1 on acclk.Person1__c = acc1.IDE_PERS_SF
	INNER JOIN [$(DataHubDatabaseName)].[exposition].[PV_SF_OPPORTUNITY] opp1 on opp1.IDE_PERS_SF = acc1.IDE_PERS_SF
	INNER JOIN [$(DataHubDatabaseName)].[exposition].[PV_SF_ACCOUNT] acc2 on acclk.Person2__c = acc2.IDE_PERS_SF
	INNER JOIN [$(DataHubDatabaseName)].[exposition].[PV_SF_OPPORTUNITY] opp2 on opp2.IDE_PERS_SF = acc2.IDE_PERS_SF
  
	WHERE LinkType__c = '06'
	and PersonRole1__c in ('09','10')
	and PersonRole2__c in ('09','10')
	and acc1.DTE_FIN_VAL = '9999-12-31 00:00:00.0000000'
	and acc2.DTE_FIN_VAL = '9999-12-31 00:00:00.0000000'
	and (case when PersonRole1__c = '10' and opp1.LIB_RES_DIS = 'Orange Bank' and opp1.LIB_CAN_INT_ORI in ('Appli OB','Web OB') then 'OB_full_selfcare' when PersonRole2__c = '10' and opp2.LIB_RES_DIS = 'Orange Bank' and opp2.LIB_CAN_INT_ORI in ('Appli OB','Web OB') then 'OB_full_selfcare'
      		when PersonRole1__c = '10' and opp1.LIB_RES_DIS = 'Orange Bank' and opp1.LIB_CAN_INT_ORI in ('Appel entrant CRC', 'Appel sortant CRC') then 'OB_CRC' when PersonRole2__c = '10' and opp2.LIB_RES_DIS = 'Orange Bank' and opp2.LIB_CAN_INT_ORI in ('Appel entrant CRC', 'Appel sortant CRC') then 'OB_CRC'
			when PersonRole1__c = '10' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_DOS_COMP = '1' then 'Souscription boutique' when PersonRole2__c = '10' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_DOS_COMP = '1' then 'Souscription boutique'
			when PersonRole1__c = '10' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_DOS_COMP <> '1' and opp1.NUM_PROC_SOUS is not null then 'Repli Selfcare' when PersonRole2__c = '10' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_DOS_COMP <> '1' and opp2.NUM_PROC_SOUS is not null then 'Repli Selfcare'
			when PersonRole1__c = '10' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_IND = 'OUI' and opp1.NUM_PROC_SOUS is null and opp1.LIB_ENT_DIS in ('AD', 'GDT')  then 'Indication boutique' when PersonRole2__c = '10' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_IND = 'OUI' and opp2.NUM_PROC_SOUS is null and opp2.LIB_ENT_DIS in ('AD', 'GDT')  then 'Indication boutique'
			when PersonRole1__c = '10' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_IND = 'OUI' and opp1.NUM_PROC_SOUS is null and opp1.LIB_ENT_DIS in ('OF-WEB')  then 'Indication Digitale' when PersonRole2__c = '10' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_IND = 'OUI' and opp2.NUM_PROC_SOUS is null and opp2.LIB_ENT_DIS in ('OF-WEB') then 'Indication Digitale'
			when PersonRole1__c = '10' and opp1.LIB_RES_DIS = 'Orange France' and opp1.LIB_CAN_INT_ORI = 'Web OB' and opp1.LIB_ENT_DIS in ('OF-WEB')  then 'Renvoi trafic' when PersonRole2__c = '10' and opp2.LIB_RES_DIS = 'Orange France' and opp2.LIB_CAN_INT_ORI = 'Web OB' and opp2.LIB_ENT_DIS in ('OF-WEB')  then 'Renvoi trafic'
	end) is not null
	and (case when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange Bank' and opp1.LIB_CAN_INT_ORI in ('Appli OB','Web OB') then 'OB_full_selfcare' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange Bank' and opp2.LIB_CAN_INT_ORI in ('Appli OB','Web OB') then 'OB_full_selfcare'
      		when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange Bank' and opp1.LIB_CAN_INT_ORI in ('Appel entrant CRC', 'Appel sortant CRC') then 'OB_CRC' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange Bank' and opp2.LIB_CAN_INT_ORI in ('Appel entrant CRC', 'Appel sortant CRC') then 'OB_CRC'
			when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_DOS_COMP = '1' then 'Souscription boutique' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_DOS_COMP = '1' then 'Souscription boutique'
			when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_DOS_COMP <> '1' and opp1.NUM_PROC_SOUS is not null then 'Repli Selfcare' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_DOS_COMP <> '1' and opp2.NUM_PROC_SOUS is not null then 'Repli Selfcare'
			when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_IND = 'OUI' and opp1.NUM_PROC_SOUS is null and opp1.LIB_ENT_DIS in ('AD', 'GDT')  then 'Indication boutique' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_IND = 'OUI' and opp2.NUM_PROC_SOUS is null and opp2.LIB_ENT_DIS in ('AD', 'GDT')  then 'Indication boutique'
			when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_IND = 'OUI' and opp1.NUM_PROC_SOUS is null and opp1.LIB_ENT_DIS in ('OF-WEB')  then 'Indication Digitale' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_IND = 'OUI' and opp2.NUM_PROC_SOUS is null and opp2.LIB_ENT_DIS in ('OF-WEB') then 'Indication Digitale'
			when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange France' and opp1.LIB_CAN_INT_ORI = 'Web OB' and opp1.LIB_ENT_DIS in ('OF-WEB')  then 'Renvoi trafic' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange France' and opp2.LIB_CAN_INT_ORI = 'Web OB' and opp2.LIB_ENT_DIS in ('OF-WEB')  then 'Renvoi trafic'
	end) is not null
	and (case when PersonRole1__c = '09' then opp1.FLG_GAGN when [PersonRole2__c] = '09' then opp2.FLG_GAGN end) = 'true'
	and (case when PersonRole1__c = '10' then opp1.EVE_ORI when [PersonRole2__c] = '10' then opp2.EVE_ORI end) = '08'
	and opp1.LABEL_OFF_COM = 'Compte bancaire' 
	and opp2.LABEL_OFF_COM = 'Compte bancaire'

	group by 
		  (case when PersonRole1__c = '09' then acc1.IDE_CONTACT_SF when [PersonRole2__c] = '09' then acc2.IDE_CONTACT_SF end) 
		 ,(case when PersonRole1__c = '09' then isnull(opp1.FLG_IND,'Non-renseigné') when [PersonRole2__c] = '09' then isnull(opp2.FLG_IND,'Non-renseigné') end)
		 ,(case when PersonRole1__c = '09' then opp1.LIB_ENT_DIS when [PersonRole2__c] = '09' then opp2.LIB_ENT_DIS end)
		 ,(case when PersonRole1__c = '09' then isnull(acc1.QUA_EMP,'Non-renseigné') when [PersonRole2__c] = '09' then isnull(acc2.QUA_EMP,'Non-renseigné') end)
		 ,(case when PersonRole1__c = '09' then (case when (acc1.SCO_PREAT is not null and charindex('.',acc1.SCO_PREAT)<>0)  then cast(substring(acc1.SCO_PREAT,1, charindex('.',acc1.SCO_PREAT)-1) as int) else acc1.SCO_PREAT end) when [PersonRole2__c] = '09' then (case when (acc2.SCO_PREAT is not null and charindex('.',acc2.SCO_PREAT)<>0)  then cast(substring(acc2.SCO_PREAT,1, charindex('.',acc2.SCO_PREAT)-1) as int) else acc2.SCO_PREAT end) end)
		 ,(case when PersonRole1__c = '09' then ISNULL(acc1.LIB_SCO_COMP,'Non-renseigné') when [PersonRole2__c] = '09' then ISNULL(acc2.LIB_SCO_COMP,'Non-renseigné') end)
		 ,(case when PersonRole1__c = '09' then isnull(acc1.SEG_GEOLIFE,'Non-renseigné') when [PersonRole2__c] = '09' then isnull(acc2.SEG_GEOLIFE,'Non-renseigné') end)
		 ,(case when PersonRole1__c = '09' then DATEDIFF(YEAR,acc1.DTE_NAIS,SYSDATETIME()) when [PersonRole2__c] = '09' then DATEDIFF(YEAR,acc2.DTE_NAIS,SYSDATETIME()) end)
		 ,(case when PersonRole1__c = '09' then cast(opp1.SCO_ENT_REL as integer) when [PersonRole2__c] = '09' then cast(opp2.SCO_ENT_REL as integer) end)
		 ,(case when PersonRole1__c = '09' then opp1.LIB_CAN_INT_ORI when [PersonRole2__c] = '09' then opp2.LIB_CAN_INT_ORI end)
		 ,(case when PersonRole1__c = '09' then opp1.LIB_RES_DIS when [PersonRole2__c] = '09' then opp2.LIB_RES_DIS end)
		 ,(case when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange Bank' and opp1.LIB_CAN_INT_ORI in ('Appli OB','Web OB') then 'OB_full_selfcare' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange Bank' and opp2.LIB_CAN_INT_ORI in ('Appli OB','Web OB') then 'OB_full_selfcare'
      		  when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange Bank' and opp1.LIB_CAN_INT_ORI in ('Appel entrant CRC', 'Appel sortant CRC') then 'OB_CRC' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange Bank' and opp2.LIB_CAN_INT_ORI in ('Appel entrant CRC', 'Appel sortant CRC') then 'OB_CRC'
			  when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_DOS_COMP = '1' then 'Souscription boutique' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_DOS_COMP = '1' then 'Souscription boutique'
			  when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_DOS_COMP <> '1' and opp1.NUM_PROC_SOUS is not null then 'Repli Selfcare' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_DOS_COMP <> '1' and opp2.NUM_PROC_SOUS is not null then 'Repli Selfcare'
			  when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_IND = 'OUI' and opp1.NUM_PROC_SOUS is null and opp1.LIB_ENT_DIS in ('AD', 'GDT')  then 'Indication boutique' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_IND = 'OUI' and opp2.NUM_PROC_SOUS is null and opp2.LIB_ENT_DIS in ('AD', 'GDT')  then 'Indication boutique'
			  when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_IND = 'OUI' and opp1.NUM_PROC_SOUS is null and opp1.LIB_ENT_DIS in ('OF-WEB')  then 'Indication Digitale' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_IND = 'OUI' and opp2.NUM_PROC_SOUS is null and opp2.LIB_ENT_DIS in ('OF-WEB') then 'Indication Digitale'
			  when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange France' and opp1.LIB_CAN_INT_ORI = 'Web OB' and opp1.LIB_ENT_DIS in ('OF-WEB')  then 'Renvoi trafic' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange France' and opp2.LIB_CAN_INT_ORI = 'Web OB' and opp2.LIB_ENT_DIS in ('OF-WEB')  then 'Renvoi trafic'
		   end)

	) reussi on enseignes.ID_Contact_MC = reussi.ID_Contact_SF and enseignes.Flag = 1

    -- Insert statements for procedure here
	SELECT @nbRows = COUNT(*) FROM SAS_Vue_MC_Parrain;
END