﻿

--DROP PROCEDURE [dbo].[PKG_QOD_GENERATION_CONTROLE_FORMAT]

CREATE PROCEDURE [dbo].[PKG_QOD_GENERATION_CONTROLE_TAILLE]
-- XSN - 13/12/2018 :
-- La procédure part du principe que la table source et la table cible présentent
-- des noms identiques pour les colonnes à comparer

-- Les contrôles sont générées doivent être revus avant d'être insérés
-- Le code des contrôles est généré suivant le masque suivant : {NOM_TABLE_CIBLE}_TECH_1.0.XX
-- CRITICITE_CONTROL = 1 - Bloquant
-- LIBELLE_CONTROL = Contrôle format du champ {NOM_TABLE_CIBLE}.{COLONNE_CIBLE}
-- DESC_CONTROL = Ce controle contrôle le format des données du champ {NOM_TABLE_SOURCE}.{COLONNE_CIBLE}
-- PERIMETRE_CONTROL = @PERIMETRE_CONTROL
-- FAMILLE_CONTROL = @PERIMETRE_CONTROL
-- CATEGORIE_CONTROL = Technique - Contrôle format
-- OUTPUT_MESSAGE = La valeur {VALEUR} ne correspond pas au type attendu {TYPE_CIBLE}
-- OUTPUT_LIB1 = {VALEUR_CONTROLEE}
-- OUTPUT_LIB2 = {{TYPE_CIBLE}
-- FLG_ACTIF = 1

-- Parametres table_source, table_cible
@Database_src as varchar(200),
@Database_cible as varchar(200),
@table_source as varchar(200),
@table_cible as varchar(200),
@req as varchar(3000) = NULL,  
@PERIMETRE_CONTROL as varchar(2000)  = NULL,
@FAMILLE_CONTROL as varchar(2000)  = NULL

AS BEGIN

DECLARE @table_cursor table (
table_catalog varchar(50),
table_name varchar(100), 
column_name varchar(100),
ordinal_position varchar(100),
data_type varchar(100),
character_maximum_length varchar(100),
numeric_precision varchar(100),
numeric_scale varchar(100)
)

--AS
--BEGIN

-- Variables locales
--SET @Database = 'SIDP_DataFactory'
--SET @table_cible = 'SIDP_QOD_LISTE_CONTROLE'
--SET @PERIMETRE_CONTROL = 'test'
--SET @FAMILLE_CONTROL = 'test'

--ecriture de la requete de récupération des types de colonnes cible depuis le dictionnaire de données
select @req = 'use '+@Database_cible+'

select 
table_catalog,
table_name,
column_name,
ordinal_position,
data_type,
character_maximum_length,
numeric_precision,
numeric_scale
from information_schema.columns
where table_name = '''+@table_cible+''''

-- exécution de la requete
insert into @table_cursor
exec(@req);

select len('jfemef')

-- Création des contrôles de taille des champs
select 
'1 - Bloquant' as CRITICITE_CONTROL,
'Contrôle taille du champ '+@Database_src+'/'+table_name+'.'+column_name as LIBELLE_CONTROL,
'Ce controle contrôle la taille du champ '+@Database_src+'/'+table_name+'.'+column_name as DESC_CONTROL,
-- ecriture du sql_control
'''SELECT '+column_name+' AS output_col1,
'+character_maximum_length+' AS output_col2,
NULL AS output_col3,
NULL AS output_col4,
NULL AS output_col5,
NULL AS output_col6,
NULL AS output_col7
FROM '+@Database_src+'.dbo.'+@table_source+'
where len('+column_name+') > '+character_maximum_length+'''' 
as SQL_CONTROL,
--fin sql_control
@PERIMETRE_CONTROL as PERIMETRE_CONTROL,
@FAMILLE_CONTROL as FAMILLE_CONTROL,
'Technique - Contrôle format' as CATEGORIE_CONTROL,
'''La valeur {''+cast(output_col1 as char)+''} présente une taille supérieur à {'+character_maximum_length+'}''' as OUTPUT_MESSAGE,
'VALEUR_CONTROLEE' as OUTPUT_LIB1,
1 as FLG_ACTIF
from @table_cursor
where character_maximum_length > 0
order by ordinal_position;

END