﻿
CREATE PROC [dbo].[PKGCM_PROC_REF_CLIENT] @DATE_ALIM DATE  
											    ,@jours_histo int    --= 1 traitement nominal par défaut
											    ,@nbRows int OUTPUT -- nb lignes processées

AS  BEGIN 

if @DATE_ALIM is null 
  set @DATE_ALIM = GETDATE();
 
if @jours_histo is null 
  set @jours_histo = 1; --valeur 1 par défaut = traitement nominal par défaut
  -- Opération temporaire pour homologation
Truncate table [dbo].[REF_CLIENT];
SET @DATE_ALIM = '2018-02-15';
SET @jours_histo = 110;
  -- Opération temporaire pour homologation
--------------------------------------///////////////// Step 1
--création d'un sous-ensemble de la table dimension temps

--purge
IF OBJECT_ID('tempdb..#WK_TEMPS_ACCOUNT') IS NOT NULL
BEGIN drop TABLE #WK_TEMPS_ACCOUNT END
 
select distinct 
       case when datediff(DAY,StandardDate,@DATE_ALIM) < 90 then StandardDate
		    else EOMONTH(StandardDate) end as Date
into #WK_TEMPS_ACCOUNT
from DIM_TEMPS
WHERE dateadd(DAY,- @jours_histo, @DATE_ALIM) <= StandardDate AND StandardDate < cast(@DATE_ALIM as date) --Modfif

--select * from #WK_TEMPS_ACCOUNT order by date desc 

--------------------------------------///////////////// Step 2

--Merge de la table _Client && _Client_History et insertion dans une table work

--purge
IF OBJECT_ID('tempdb..#WK_SF_CLIENTS') IS NOT NULL
BEGIN Drop TABLE #WK_SF_CLIENTS END

select * into #WK_SF_CLIENTS
from
(
select sf_client.*
from [$(DataHubDatabaseName)].[dbo].[VW_PV_SF_ACCOUNT]  sf_client
join ( select Id_SF as Id_SF_Opp, AccountId as Id_SF_Account, StartedChannel__c, StartedChannelForIndication__c, Indication__c, RelationEntryScore__c, CreatedDate
                   , row_number() over (partition by Id_SF order by Validity_StartDate desc) as Validity_Flag
                from [$(DataHubDatabaseName)].[dbo].[VW_PV_SF_OPPORTUNITY_HISTORY] 
               where StageName = '09' and CommercialOfferCode__c = 'OC80') sf_opport 
    on sf_opport.Id_SF_Account = sf_client.Id_SF and sf_opport.Validity_Flag = 1 --sf_client.Validity_Flag

where Validity_EndDate >= DATEADD(DAY,-@jours_histo,@DATE_ALIM) 
)r

--select * from #WK_SF_CLIENTS;

--------------------------------------///////////////// Step 3
--Photo sur la période requise et insertion dans une table res

--purge
IF OBJECT_ID('tempdb..#RES_CLIENTS') IS NOT NULL
BEGIN drop TABLE #RES_CLIENTS END

--insertion dans table work
select * into  #RES_CLIENTS
from
(
select d.Date as date_traitement
      ,c.*

from #WK_TEMPS_ACCOUNT d

outer apply 
	(
	select *
	from #WK_SF_CLIENTS
	where ( cast(Validity_StartDate as date) <= d.Date and d.Date <= cast(Validity_EndDate as date) ) --Modif
	) c
)r
/*Début de traitement*/
IF OBJECT_ID('tempdb..#REF_CLIENT') IS NOT NULL
BEGIN drop TABLE #REF_CLIENT END

SELECT 
	date_traitement,
	DWHCLICLI,
	Id_SF,
	IDCustomerCID__c,
	IDCustomerPID__c,
	IDCustomer__pc,
	OwnerId,
	ParentId,
	PersonContactId,
	LastModifiedById,
	MasterRecordId,
	TCHId__c,
	IDCustomerBA__c,
	IDCustomerSAB__c,
	CreateIDCustomerBA__c,
	RecordTypeId,
	CreatedById,
	JigsawCompanyId,
	CreateIDCustomer__pc,
	IDCustomerCID__pc,
	BehaviouralScoring__c,
	CreatedDate,
	DeathDate__pc,
	DistributorNetwork__c,
	EmployeeType__pc,
	FirstName,
	GeolifeSegment__c,
	LastModifiedDate,
	LastName,
	OBFirstContactDate__c,
	OBSeniority__c,
	DWHCLICLO,
	OBTerminationReason__c,
	Occupation__pc,
	OptInDataTelco__c,
	OptInOrderTelco__c,
	PersonBirthdate,
	PersonLeadSource,
	Salutation,
	SystemModstamp,
	ShopNetworkMesh__c,
	ShopManagerLastName__c,
	ShopManagerFirstName__c,
	ShopCode__c,
	ShopBrand__c,
	Phone,
	Name,
	BillingStreet,
	BillingCity,
	BillingPostalCode,
	Preattribution__c,
	BillingCountryCode,
	BillingCountry,
	BillingStateCode,
	BillingState,
	MaritalStatus__pc,
	OccupationNiv1__pc,
	OccupationNiv3__pc,
	TotalDisposableIncome__pc,
	TotalIncome__pc,
	JustifiedIncomes__pc,
	EmploymentContractStartDate__pc,
	OFSeniority__c,
	PrimaryResidOccupationType__pc,
	AccountSource,
	AdditionToAddress__c,
	AddresseState__c,
	Alert__c,
	AnnualRevenue,
	AntenuptialSettlement__pc,
	BankingActivityRate__c,
	BankRating__c,
	BanLimitDate__c,
	BillingGeocodeAccuracy,
	BillingLatitude,
	BillingLongitude,
	BirthCity__pc,
	BirthCountry__pc,
	BirthDepartment__pc,
	BirthPostalCode__pc,
	BloquedAccount__c,
	DWHCLITYI,
	DWHCLIINT,
	ClientProfileLastModif__pc,
	DependentChildrenNb__pc,
	EmploymentContractEndDate__pc,
	EmploymentContractType__pc,
	ErrorSavingRIB__c,
	Expenses__c,
	FacebookAlias__pc,
	Fax,
	FinancialPrivacy__c,
	FiscalCountry__c,
	IncomeGatheredOn__pc,
	Income__c,
	Industry,
	InseeCode__c,
	InvalideAdr__pc,
	InvestorProfile__pc,
	IsCustomerPortal,
	IsDeleted,
	IsPersonAccount,
	Jigsaw,
	LastActivityDate,
	LastReferencedDate,
	LastViewedDate,
	MaidenName__pc,
	MonthExpenses__pc,
	MonthFoncIncome__pc,
	MonthMobIncome__pc,
	MonthNetIncome__pc,
	MonthOtherExpenses__pc,
	MonthOtherIncomes__pc,
	MonthOtherLoans__pc,
	MonthPaidPension__pc,
	MonthReceivPension__pc,
	MonthSocBenefits__pc,
	MonthTaxes__pc,
	Nationality__pc,
	OccupationArea__c,
	OFFirstContactDate__c,
	OFTerminationDate__c,
	OFTerminationReason__c,
	OverdraftExcess__c,
	PersonalProperty__pc,
	PersonAssistantName,
	PersonAssistantPhone,
	PersonDepartment,
	PersonDoNotCall,
	PersonEmail,
	PersonEmailBouncedDate,
	PersonEmailBouncedReason,
	PersonHasOptedOutOfEmail,
	PersonHasOptedOutOfFax,
	PersonHomePhone,
	PersonLastCURequestDate,
	PersonLastCUUpdateDate,
	PersonMailingCity,
	PersonMailingCountry,
	PersonMailingCountryCode,
	PersonMailingGeocodeAccuracy,
	PersonMailingLatitude,
	PersonMailingLongitude,
	PersonMailingPostalCode,
	PersonMailingState,
	PersonMailingStateCode,
	PersonMailingStreet,
	PersonMobilePhone,
	PersonOtherCity,
	PersonOtherCountry,
	PersonOtherCountryCode,
	PersonOtherGeocodeAccuracy,
	PersonOtherLatitude,
	PersonOtherLongitude,
	PersonOtherPhone,
	PersonOtherPostalCode,
	PersonOtherState,
	PersonOtherStateCode,
	PersonOtherStreet,
	PersonTitle,
	PhoneEligibility__pc,
	PhoneticFirstname__pc,
	PhoneticLastName__pc,
	PhoneticMaidenName__pc,
	PhotoUrl,
	PreferredPhoneNumber__pc,
	PrimaryResidenceEntryDate__pc,
	RealEstate__pc,
	Recovery__c,
	RightOpposition__c,
	RunUnicityCheck__pc,
	SecondEmail__pc,
	SellerHashedCuid__pc,
	ShippingCity,
	ShippingCountry,
	ShippingCountryCode,
	ShippingGeocodeAccuracy,
	ShippingLatitude,
	ShippingLongitude,
	ShippingPostalCode,
	ShippingState,
	ShippingStateCode,
	ShippingStreet,
	SicDesc,
	SubscriberOwnerIndicator__c,
	TotalExpenses__pc,
	TwitterAlias__pc,
	Type,
	DWHCLIDOU,
	UndeliverabilityReason__pc,
	USIndicator__pc,
	USNationality__pc,
	VIPParnasseCustomer__pc,
	WealthTax__pc,
	Website,
	WrongAddress__c,
	AccountNumber,
	Sic,
	NumberOfEmployees,
	Ownership,
	TickerSymbol,
	Rating,
	Site,
	AttestedClient__c,
	AccountType__c,
	RealizedBy__c,
	TCHPartnerVisibility__c,
	SFMCTest__c,
	Agency__c,
	BusinessPhone__c,
	CodeNAFRev2__c,
	DistributorData__c,
	DistributorNetworks__c,
	FiveYearsRefusalDate__c,
	Email__c,
	RefusalFlag__c,
	SixMonthsRefusalDate__c,
	Entity__c,
	HomePhone__c,
	IsFiscalResidencyInFrance__c,
	IsPhoneCompatible__c,
	IsSIMCardCompatible__c,
	MobileOS__c,
	IncomeGatheredOn__c,
	LabelNAFRev2__c,
	LegalForm__c,
	MigratedClient__c,
	QuoteRisk__c,
	MobilePhone__c,
	USIndicator__c,
	NumSiret__c,
	PreferredPhoneNumber__c,
	PrimaryNetwork__c,
	SalesAgent__c,
	SegmentationDistributive__c,
	SegmentationSaving__c,
	SocialReason__c,
	TCHFirstEnrolement__c,
	TCH_RTName__c,
	Turnover__c,
	UndeliverabilityReason__c,
	VIPParnasseCustomer__c,
	TCHSalutation__pc,
	AcceptationMembershipDate__pc,
	Person__pc,
	RightOpposition__pc,
	ActivityBranch__pc,
	DWHCLIJUR,
	NoCall__pc,
	NoLetter__pc,
	NoOrangeEmail__pc,
	NoOrangeSMS__pc,
	NoPartnerEmail__pc,
	NoPartnerSMS__pc,
	AdditionToAddress__pc,
	ApplicationMembershipDate__pc,
	BIC__pc,
	CreditInProgressExceptOB__pc,
	DistributorNetwork__pc,
	FamilyAllocation__pc,
	HousingAllocation__pc,
	IBAN__pc,
	IsAMF__pc,
	IsContactAccountSync__pc,
	IsIOBSP__pc,
	IsTA3C__pc,
	JustifiedIncomesData__pc,
	Manager__pc,
	Prescripteur__pc,
	RCSNumber__pc,
	TaxResidenceInFrance__pc,
	DWHCLIIDT,
	DWHCLIIDN,
	DWHCLIDTX,
	DWHCLIETB,
	DWHCLISIG,
	DWHCLISRN,
	DWHCLICRE,
	DWHCLIRES,
	DWHCLIECO,
	DWHCLIREG,
	DWHCLIGEO,
	DWHCLIENT,
	DWHCLIACT,
	DWHCLICRD,
	DWHCLICOT,
	DWHCLIDIR,
	DWHCLIANF,
	DWHCLISER,
	DWHCLISEP,
	DWHCLIDBA,
	DWHCLICOL,
	DWHCLIGRI,
	DWHCLITRI,
	DWHCLIDLC,
	DWHCLIMOT,
	DWHCLIPRO,
	DWHCLIDOM,
	DWHCLIEST,
	DWHCLIRU2,
	DWHCLIBIL,
	DWHCLIBIM,
	DWHCLIATR,
	DWHCLIBLN,
	DWHCLISTA,
	DWHCLITIE,
	DWHCLIMUT,
	DWHCLIACC,
	DWHCLIDGI,
	DWHCLIDEP,
	DWHCLIRIS,
	DWHCLIFRE,
	0 as ACTIVITE_CAV,
	cast('Sans CAV' as varchar(50)) as LIB_ACTIVITE_CAV,
	0 as ACTIVITE_CSL,
	cast('Sans CSL' as varchar(50)) as LIB_ACTIVITE_CSL,
	case when datediff(month,cast(CreatedDate as date),cast(date_traitement as date)) < 4 then -1 else 0 end as ACTIVITE_GLOBAL,
	case when datediff(month,cast(CreatedDate as date),cast(date_traitement as date)) < 4 then 'Client récent' else 'Crédit Sec & Autre' end as LIB_ACTIVITE_GLOBAL
INTO #REF_CLIENT
FROM #RES_CLIENTS res
inner join [$(DataHubDatabaseName)].dbo.VW_PV_SA_Q_CLIENT cl 
      ON cl.DWHCLICLI=res.IDCustomerSAB__pc and cl.Validity_StartDate <= date_traitement and date_traitement < cl.Validity_EndDate  /*Test En homol < pour voir si ca match */
INNER JOIN (SELECT DWHCPTPPAL FROM [$(DataHubDatabaseName)].dbo.VW_PV_SA_Q_COMPTE a GROUP BY DWHCPTPPAL)CLIENT
    ON cl.DWHCLICLI=CLIENT.DWHCPTPPAL

/*Jointure entre ref_client et compte pour récupérer l'activité compte*/

IF OBJECT_ID('tempdb..#pivot') IS NOT NULL
BEGIN drop TABLE #pivot END

select distinct tmp.Date date_traitement,
                t.DWHCLICLI,
				cpt.[NUM_CPT_SAB],
				cpt.FLAG_CPT,
				cpt.ACTIVITE_CPT 
into #pivot
from #REF_CLIENT t 
inner join #WK_TEMPS_ACCOUNT tmp on t.date_traitement = tmp.Date
inner join [dbo].[MKT_COMPTE] cpt on t.DWHCLICLI = cpt.NUM_CLI_SAB and  t.date_traitement = cpt.[DTE_TRAITEMENT]
order by t.DWHCLICLI

--Pivot
IF OBJECT_ID('tempdb..#res_pivot') IS NOT NULL
BEGIN drop TABLE #res_pivot END
select * into #res_pivot from 
(
select date_traitement,
       DWHCLICLI,
	   FLAG_CPT ,
	   ACTIVITE_CPT
from #pivot
) as tsource 
pivot (sum (ACTIVITE_CPT) for FLAG_CPT in ([CAV],[CSL]) ) as t 
order by DWHCLICLI

/*Jointure entre ref_client et compte pour récupérer la date de creation du produit et Ancienneté*/
IF OBJECT_ID('tempdb..#Anciennete') IS NOT NULL
BEGIN drop TABLE #Anciennete END

select distinct tmp.Date date_traitement,
                t.DWHCLICLI,
				cpt.[NUM_CPT_SAB],
				cpt.FLAG_CPT,
				cpt.[DTE_OUV],
				DATEDIFF(MM,cpt.[DTE_OUV],cast(tmp.Date as date)) as Ancienté
Into #Anciennete
from #REF_CLIENT t 
inner join #WK_TEMPS_ACCOUNT tmp on t.date_traitement = tmp.Date
inner join [dbo].[MKT_COMPTE] cpt on t.DWHCLICLI = cpt.NUM_CLI_SAB and  t.date_traitement = cpt.[DTE_TRAITEMENT]
order by t.DWHCLICLI

/*Pivot Ancienneté*/
IF OBJECT_ID('tempdb..#res_pivot_Anciennete') IS NOT NULL
BEGIN drop TABLE #res_pivot_Anciennete END
select * into #res_pivot_Anciennete from 
(
select date_traitement,
       DWHCLICLI,
	   FLAG_CPT ,
	   Ancienté
from #Anciennete
) as tsource 
pivot (sum (Ancienté) for FLAG_CPT in ([CAV],[CSL]) ) as t 
order by DWHCLICLI

/*Update #REF_CLIENT*/
--Update (ACTIVITE_CAV,ACTIVITE_CSL) par rapport a l'activité:
Update #REF_CLIENT  
set 
ACTIVITE_CAV = ISnull(h.CAV,0), 
ACTIVITE_CSL = ISnull(h.CSL,0)
from #REF_CLIENT t 
inner join #res_pivot h on t.DWHCLICLI = h.DWHCLICLI and t.date_traitement = h.date_traitement

--Update (ACTIVITE_CAV,ACTIVITE_CSL) par rapport a l'ancienneté 
Update #REF_CLIENT  
set 
ACTIVITE_CAV = case when h.CAV < 4 and h.CAV is not null then -1 else ACTIVITE_CAV END,  
ACTIVITE_CSL = case when h.CSL < 4 and h.CSL is not null then -1 else ACTIVITE_CSL END
from #REF_CLIENT t 
inner join #res_pivot_Anciennete h on t.DWHCLICLI = h.DWHCLICLI and t.date_traitement = h.date_traitement

--select * from ##REF_CLIENT where DWHCLICLI = '1509780'

--Update (ACTIVITE_GLOBAL) par rapport a (ACTIVITE_CAV,ACTIVITE_CSL)
Update #REF_CLIENT  
set 
ACTIVITE_GLOBAL = case when ACTIVITE_CAV > ACTIVITE_CSL then ACTIVITE_CAV else ACTIVITE_CSL end 
from #REF_CLIENT t 
where ACTIVITE_GLOBAL <>-1

--Update (LIB_ACTIVITE_CAV,LIB_ACTIVITE_CSL,LIB_ACTIVITE_GLOBAL) 
--par rapport a (ACTIVITE_CAV,ACTIVITE_CSL,ACTIVITE_GLOBAL)
Update #REF_CLIENT  
set 
LIB_ACTIVITE_CAV =    case when ACTIVITE_CAV= 4  then 'Actif ++' 
                           when ACTIVITE_CAV= 3  then 'Actif +'
					       when ACTIVITE_CAV= 2  then 'Quasi Actif'
					       when ACTIVITE_CAV= 1  then 'Inactif'
					       when ACTIVITE_CAV= 0  then 'Sans CAV'
					       when ACTIVITE_CAV= -1 then 'CAV récent' 
						   else 'KO' END,

LIB_ACTIVITE_CSL =    case when ACTIVITE_CSL= 4 then  'Actif ++'  
                           when ACTIVITE_CSL= 3 then  'Actif +'
					       when ACTIVITE_CSL= 2 then  'Quasi Actif'
					       when ACTIVITE_CSL= 1 then  'Inactif'
					       when ACTIVITE_CSL= 0 then  'Sans CSL'
					       when ACTIVITE_CSL= -1 then 'CSL récent'
						   else 'KO' END,

LIB_ACTIVITE_GLOBAL = case when ACTIVITE_GLOBAL= 4  then 'Actif ++'   
                           when ACTIVITE_GLOBAL= 3  then 'Actif +'
					       when ACTIVITE_GLOBAL= 2  then 'Quasi Actif'
					       when ACTIVITE_GLOBAL= 1  then 'Inactif'
						   when ACTIVITE_GLOBAL= 0  then 'Crédit Sec & Autre'
						   when ACTIVITE_GLOBAL= -1 then 'Client récent' 
						   else 'KO'END
from #REF_CLIENT t
/*Fin de traitement */

 
--Nettoyage et insertion dans la table REF_Client
/*Suppression des données chargées en cas de reprise*/

delete from [dbo].[REF_CLIENT] where [DTE_TRAITEMENT] IN (select Date from #WK_TEMPS_ACCOUNT)

insert into [dbo].[REF_CLIENT]
(
 [DTE_TRAITEMENT]             
 ,[NUM_CLT_SAB]                
 ,[IDE_PERS_SF]                
 ,[NUM_CLT_DIS]                
 ,[NUM_PID]                    
 ,[IDE_CLT_PHY]                
 ,[IDE_USER_SF]                
 ,[IDE_PARENT]                 
 ,[IDE_CONTACT_SF]             
 ,[IDE_TYP_ENR]                
 ,[IDE_USER_CREA_SF]           
 ,[IDE_JIGSAW_COMPANY]         
 ,[IDE_USER_MOD_SF]            
 ,[IDE_ENR_PPAL]               
 ,[IDE_TCH]                    
 ,[NUM_CLT_BA]                 
 ,[NUM_CLT_SAB__c]             
 ,[IDE_CREA_CLT_BA]            
 ,[IDE_CREA_CLT]               
 ,[NUM_CLT_DIS_pc]             
 ,[SCO_COMP]                   
 ,[DTE_CREA]                   
 ,[DTE_DEC]                    
 ,[RES_DIS]                    
 ,[QUA_EMP]                    
 ,[PRENOM]                     
 ,[SEG_GEOLIFE]                
 ,[DTE_DER_MOD]                
 ,[NOM]                        
 ,[DTE_ENT_REL_OB]             
 ,[ANC_OB]                     
 ,[DTE_CLO_CLT]                
 ,[MOT_FIN_REL_OB]             
 ,[PROF]                       
 ,[FLG_OPT_IN_DON_TEL]         
 ,[FLG_OPT_IN_FACT_TEL]        
 ,[DTE_NAIS]                   
 ,[COD_CAN_INT]                
 ,[COD_CIV]                    
 ,[SystemModstamp]             
 ,[SS_RES_BOU]                 
 ,[NOM_RESP_BOU]               
 ,[PRE_RESP_BOU]               
 ,[COD_BOUT]                   
 ,[COD_ENS]                    
 ,[NUM_TEL]                    
 ,[NOM_COMPTE]                 
 ,[RUE_FACT]                   
 ,[VILLE_FACT]                 
 ,[COD_POS_FACT]               
 ,[SCO_PREAT]                  
 ,[COD_PAY_FACT]               
 ,[PAY_FACT]                   
 ,[COD_REG_PROV_FACT]          
 ,[REG_PROV_FACT]              
 ,[COD_STA_MAR]                
 ,[COD_CSP_NV1]                
 ,[COD_CSP_NV3]                
 ,[TOT_REV_DISP]               
 ,[TOT_REV]                    
 ,[REV_JUST_pc]                
 ,[DTE_DEB_CONT_TRA]           
 ,[ANC_OF]                     
 ,[COD_TYP_OCC]                
 ,[SRCE_CPT]                   
 ,[COMPL_ADR]                  
 ,[STAT_ADR]                   
 ,[ALERTE]                     
 ,[REV_ANN]                    
 ,[REG_MAT]                    
 ,[NIV_ACT_BANC]               
 ,[COT_BAN]                    
 ,[DTE_LIM_INT]                
 ,[GEO_FACT]                   
 ,[LAT_FACT]                   
 ,[LONG_FACT]                  
 ,[COM_NAIS]                   
 ,[PAY_NAIS]                   
 ,[DEP_NAIS]                   
 ,[COD_POS_NAIS]               
 ,[FLG_CPTE_BLOQ]              
 ,[TYP_INT]                    
 ,[IND_INT_CHQ]                
 ,[PROF_INV_MAJ]               
 ,[NBE_ENF_CHAR]               
 ,[DTE_FIN_CONT_TRA]           
 ,[TYP_CONT_TRA]               
 ,[ERR_ENR_RIB]                
 ,[CHAR_PERS]                  
 ,[ALIAS_FACEBOOK]             
 ,[FAX]                        
 ,[SEC_BANC]                   
 ,[PAY_RES_FISC]               
 ,[DTE_COL_REV_pc]             
 ,[REV_PERS]                   
 ,[INDUSTRIE]                  
 ,[COD_INSEE]                  
 ,[FLG_ADR_INV]                
 ,[PROF_INV]                   
 ,[CPT_POR_CLT]                
 ,[FLG_SUPP]                   
 ,[FLG_PERS_ACCNT]             
 ,[JIGSAW]                     
 ,[DTE_DER_ACT]                
 ,[DTE_DER_REF]                
 ,[DTE_DER_AFF]                
 ,[NOM_NAIS]                   
 ,[CHAR_MEN]                   
 ,[REV_FONC_MEN]               
 ,[REV_MOB_MEN]                
 ,[REV_MEN_NET]                
 ,[AUT_CHAR_MEN]               
 ,[AUT_REV_MEN]                
 ,[AUT_CRE_ENC_MEN]            
 ,[PEN_ALI_MENS_VER]           
 ,[PEN_ALI_MENS_REC]           
 ,[PREST_SOC_MEN]              
 ,[IMP_MEN]                    
 ,[NAT]                        
 ,[SEC_ACT]                    
 ,[DTE_ENT_REL_OF]             
 ,[DTE_FIN_REL_OF]             
 ,[MOT_FIN_REL_OF]             
 ,[DEP_DEC]                    
 ,[PAT_MOB]                    
 ,[NOM_ASS]                    
 ,[TEL_ASS]                    
 ,[SVC_PERS]                   
 ,[FLG_NOT_CALL]               
 ,[MAIL]                       
 ,[DTE_RET_MAIL]               
 ,[RAI_RET_MAIL]               
 ,[FLG_MAIL_OPT_OUT]           
 ,[FLG_FAX_OPT_OUT]            
 ,[DOMICILE_PERS]              
 ,[DTE_DER_STAY_IN_TOUCH_DEM]  
 ,[DTE_DER_STAY_IN_TOUCH_SAVE] 
 ,[VILLE_ENV_POS]              
 ,[PAY_ENV_POS]                
 ,[COD_PAY_ENV_POS]            
 ,[GEO_ENV_POS]                
 ,[LAT_ENV_POS]                
 ,[LONG_ENV_POS]               
 ,[COD_POS_ENV_POS]            
 ,[PROV_ENV_POS]               
 ,[COD_PROV_ENV_POS]           
 ,[RUE_ENV_POS]                
 ,[MOBILE_PERS]                
 ,[AUT_VILLE]                  
 ,[AUT_PAY]                    
 ,[AUT_COD_PAY]                
 ,[AUT_GEO_ACC]                
 ,[AUT_LAT]                    
 ,[AUT_LONG]                   
 ,[AUT_TEL]                    
 ,[AUT_COD_POS]                
 ,[AUT_PROV]                   
 ,[AUT_COD_PROV]               
 ,[AUT_RUE]                    
 ,[TITRE]                      
 ,[ELIG_MOB]                   
 ,[PRENOM_PHONETIC]            
 ,[NOM_PHONETIC]               
 ,[NOM_NAIS_PHONETIC]          
 ,[PHOTO_URL]                  
 ,[TEL_FAV_pc]                 
 ,[DTE_ENT_RES_PPAL]           
 ,[PAT_IMMO]                   
 ,[RECOUVREMENT]               
 ,[DROIT_OPPOS]                
 ,[RUN_UNICITY_CHECK]          
 ,[MAIL2_pc]                   
 ,[CUID_VEND_HASHE]            
 ,[VILLE_LIV]                  
 ,[PAY_LIV]                    
 ,[COD_PAY_LIV]                
 ,[GEO_ACC_LIV]                
 ,[LAT_LIV]                    
 ,[LONG_LIV]                   
 ,[COD_POS_LIV]                
 ,[PORV_LIV]                   
 ,[COD_PROV_LIV]               
 ,[RUE_LIV]                    
 ,[SIC_DESC]                   
 ,[FLG_SOUSC_TIT]              
 ,[TOT_CHAR]                   
 ,[ALIAS_TWITTER]              
 ,[TYP_REQ]                    
 ,[IND_DOU_CLT]                
 ,[MOT_RET_COU_pc]             
 ,[FLG_FATCA_pc]               
 ,[NAT_AMER]                   
 ,[CLT_PHY_VIP_FIRST_PARNASSE] 
 ,[ISF]                        
 ,[WEBSITE]                    
 ,[FLG_ADR_INC]                
 ,[NUM_PERS]                   
 ,[COD_SIC]                    
 ,[NBE_EMPLOYES]               
 ,[FORM_JUR_PM]                
 ,[SYMBOLE]                    
 ,[COT_PERS]                   
 ,[SITE]                       
 ,[CLT_AUTH]                   
 ,[MARCHE]                     
 ,[REAL_PAR]                   
 ,[PARTNER_VISIBILITY_TCH]     
 ,[SFMC_TEST]                  
 ,[AGENCE]                     
 ,[TEL_PRO]                    
 ,[COD_NAF_REV2]               
 ,[INFO_DIS]                   
 ,[RES_DIS_COMPLET]            
 ,[DTE_REFUS_CINQ_ANS]         
 ,[MAIL__c]                    
 ,[REFUS_SOUSCRIPTION]         
 ,[DTE_REFUS_SIX_MM]           
 ,[SOU_ENT]                    
 ,[DOMICILE]                   
 ,[FLG_RES_FISC_FR]            
 ,[FLG_COMP_TEL]               
 ,[COMP_SIM]                   
 ,[SYSTEME]                    
 ,[DTE_COL_REV]                
 ,[LIB_NAF_REV2]               
 ,[FORM_JUR]                   
 ,[CLT_MIGR]                   
 ,[COT_RISQ]                   
 ,[MOBILE]                     
 ,[FLG_FATCA]                  
 ,[NUM_SIRET]                  
 ,[TEL_FAV]                    
 ,[RES_DIS_PPAL]               
 ,[COM_EN_CHAR]                
 ,[SEG_DIST]                   
 ,[SEG_EPRGN]                  
 ,[RAI_SOC]                    
 ,[PRE_SOUS_TCH]               
 ,[NOM_RT_TCH]                 
 ,[CA]                         
 ,[MOT_RET_COU]                
 ,[CLT_VIP_FIRST_PARNASSE]     
 ,[CIV_TCH]                    
 ,[DTE_CONF_ADH]               
 ,[PERS]                       
 ,[DROIT_OPPOS_pc]             
 ,[SECT_ACT]                   
 ,[CAPA_JURI]                  
 ,[REF_APP_TEL_pc]             
 ,[REF_CON_COU_pc]             
 ,[REF_MAIL_pc]                
 ,[REF_SMS_pc]                 
 ,[REF_MAIL_pc_PAR]            
 ,[REF_SMS_PAR_pc]             
 ,[COMPL_ADR_pc]               
 ,[DTE_DEM_ADH]                
 ,[BIC]                        
 ,[CRE_ENC_HORS_OB]            
 ,[RES_DIS_pc]                 
 ,[ALLOC_FAM]                  
 ,[ALLOC_LOG]                  
 ,[IBAN]                       
 ,[FLG_CERTIF_AMF]             
 ,[FLG_SYNC_CONTACT_CPT]       
 ,[FLG_HABILIT_IOBSP]          
 ,[FLG_HABILIT_TA3C]           
 ,[INFO_REV_JUST]              
 ,[GERANT]                     
 ,[PRESCRIPTEUR]               
 ,[NUM_RCS]                    
 ,[RES_FISC_FR]                
 ,[IDE_NAT_TP]                 
 ,[IDE_NAT_NU]                 
 ,[DTE_ANA]                    
 ,[COD_ETA]                    
 ,[SIG_CLT]                    
 ,[COD_SIREN_CLT]              
 ,[COD_CLT_RES]                
 ,[COD_RES]                    
 ,[COD_QLT_CLT]                
 ,[SEC_ACT_REG]                
 ,[SEC_GEO]                    
 ,[ENT_LIE]                    
 ,[COT_ACT]                    
 ,[COT_CRE]                    
 ,[COT_INTERNE]                
 ,[COT_DIR]                    
 ,[AA_CA_ENT]                  
 ,[COD_SEG_RES]                
 ,[COD_SEG_POT]                
 ,[COD_DOU_REG]                
 ,[TIERS_COLL]                 
 ,[CLT_GRP_RISQ]               
 ,[TETE_GRP_RISQ]              
 ,[DTE_DEB_CON]                
 ,[MOTIF_CON]                  
 ,[PROC_CLT]                   
 ,[COT_IEDOM]                  
 ,[AUT_PROD]                   
 ,[ZON_RUB02]                  
 ,[AA_DER_BILAN]               
 ,[MM_DER_BILAN]               
 ,[DTE_ATT]                    
 ,[COD_BU_LINE]                
 ,[COD_STAT_CLT]               
 ,[NUM_CLT_REF]                
 ,[DTE_INIT_CLT]               
 ,[DTE_ACC_CLT]                
 ,[COD_DGI]                    
 ,[COD_BDF]                    
 ,[PERIMETRE_RISQ]             
 ,[FREQ]                       
 ,[ACTIVITE_CAV]               
 ,[ACTIVITE_CSL]               
 ,[ACTIVITE_GLOBAL]            
)
SELECT 
	date_traitement,
	DWHCLICLI,
	Id_SF,
	IDCustomerCID__c,
	IDCustomerPID__c,
	IDCustomer__pc,
	OwnerId,
	ParentId,
	PersonContactId,
	LastModifiedById,
	MasterRecordId,
	TCHId__c,
	IDCustomerBA__c,
	IDCustomerSAB__c,
	CreateIDCustomerBA__c,
	RecordTypeId,
	CreatedById,
	JigsawCompanyId,
	CreateIDCustomer__pc,
	IDCustomerCID__pc,
	BehaviouralScoring__c,
	CreatedDate,
	DeathDate__pc,
	DistributorNetwork__c,
	EmployeeType__pc,
	FirstName,
	GeolifeSegment__c,
	LastModifiedDate,
	LastName,
	OBFirstContactDate__c,
	OBSeniority__c,
	DWHCLICLO,
	OBTerminationReason__c,
	Occupation__pc,
	OptInDataTelco__c,
	OptInOrderTelco__c,
	PersonBirthdate,
	PersonLeadSource,
	Salutation,
	SystemModstamp,
	ShopNetworkMesh__c,
	ShopManagerLastName__c,
	ShopManagerFirstName__c,
	ShopCode__c,
	ShopBrand__c,
	Phone,
	Name,
	BillingStreet,
	BillingCity,
	BillingPostalCode,
	Preattribution__c,
	BillingCountryCode,
	BillingCountry,
	BillingStateCode,
	BillingState,
	MaritalStatus__pc,
	OccupationNiv1__pc,
	OccupationNiv3__pc,
	TotalDisposableIncome__pc,
	TotalIncome__pc,
	JustifiedIncomes__pc,
	EmploymentContractStartDate__pc,
	OFSeniority__c,
	PrimaryResidOccupationType__pc,
	AccountSource,
	AdditionToAddress__c,
	AddresseState__c,
	Alert__c,
	AnnualRevenue,
	AntenuptialSettlement__pc,
	BankingActivityRate__c,
	BankRating__c,
	BanLimitDate__c,
	BillingGeocodeAccuracy,
	BillingLatitude,
	BillingLongitude,
	BirthCity__pc,
	BirthCountry__pc,
	BirthDepartment__pc,
	BirthPostalCode__pc,
	BloquedAccount__c,
	DWHCLITYI,
	DWHCLIINT,
	ClientProfileLastModif__pc,
	DependentChildrenNb__pc,
	EmploymentContractEndDate__pc,
	EmploymentContractType__pc,
	ErrorSavingRIB__c,
	Expenses__c,
	FacebookAlias__pc,
	Fax,
	FinancialPrivacy__c,
	FiscalCountry__c,
	IncomeGatheredOn__pc,
	Income__c,
	Industry,
	InseeCode__c,
	InvalideAdr__pc,
	InvestorProfile__pc,
	IsCustomerPortal,
	IsDeleted,
	IsPersonAccount,
	Jigsaw,
	LastActivityDate,
	LastReferencedDate,
	LastViewedDate,
	MaidenName__pc,
	MonthExpenses__pc,
	MonthFoncIncome__pc,
	MonthMobIncome__pc,
	MonthNetIncome__pc,
	MonthOtherExpenses__pc,
	MonthOtherIncomes__pc,
	MonthOtherLoans__pc,
	MonthPaidPension__pc,
	MonthReceivPension__pc,
	MonthSocBenefits__pc,
	MonthTaxes__pc,
	Nationality__pc,
	OccupationArea__c,
	OFFirstContactDate__c,
	OFTerminationDate__c,
	OFTerminationReason__c,
	OverdraftExcess__c,
	PersonalProperty__pc,
	PersonAssistantName,
	PersonAssistantPhone,
	PersonDepartment,
	PersonDoNotCall,
	PersonEmail,
	PersonEmailBouncedDate,
	PersonEmailBouncedReason,
	PersonHasOptedOutOfEmail,
	PersonHasOptedOutOfFax,
	PersonHomePhone,
	PersonLastCURequestDate,
	PersonLastCUUpdateDate,
	PersonMailingCity,
	PersonMailingCountry,
	PersonMailingCountryCode,
	PersonMailingGeocodeAccuracy,
	PersonMailingLatitude,
	PersonMailingLongitude,
	PersonMailingPostalCode,
	PersonMailingState,
	PersonMailingStateCode,
	PersonMailingStreet,
	PersonMobilePhone,
	PersonOtherCity,
	PersonOtherCountry,
	PersonOtherCountryCode,
	PersonOtherGeocodeAccuracy,
	PersonOtherLatitude,
	PersonOtherLongitude,
	PersonOtherPhone,
	PersonOtherPostalCode,
	PersonOtherState,
	PersonOtherStateCode,
	PersonOtherStreet,
	PersonTitle,
	PhoneEligibility__pc,
	PhoneticFirstname__pc,
	PhoneticLastName__pc,
	PhoneticMaidenName__pc,
	PhotoUrl,
	PreferredPhoneNumber__pc,
	PrimaryResidenceEntryDate__pc,
	RealEstate__pc,
	Recovery__c,
	RightOpposition__c,
	RunUnicityCheck__pc,
	SecondEmail__pc,
	SellerHashedCuid__pc,
	ShippingCity,
	ShippingCountry,
	ShippingCountryCode,
	ShippingGeocodeAccuracy,
	ShippingLatitude,
	ShippingLongitude,
	ShippingPostalCode,
	ShippingState,
	ShippingStateCode,
	ShippingStreet,
	SicDesc,
	SubscriberOwnerIndicator__c,
	TotalExpenses__pc,
	TwitterAlias__pc,
	Type,
	DWHCLIDOU,
	UndeliverabilityReason__pc,
	USIndicator__pc,
	USNationality__pc,
	VIPParnasseCustomer__pc,
	WealthTax__pc,
	Website,
	WrongAddress__c,
	AccountNumber,
	Sic,
	NumberOfEmployees,
	Ownership,
	TickerSymbol,
	Rating,
	Site,
	AttestedClient__c,
	AccountType__c,
	RealizedBy__c,
	TCHPartnerVisibility__c,
	SFMCTest__c,
	Agency__c,
	BusinessPhone__c,
	CodeNAFRev2__c,
	DistributorData__c,
	DistributorNetworks__c,
	FiveYearsRefusalDate__c,
	Email__c,
	RefusalFlag__c,
	SixMonthsRefusalDate__c,
	Entity__c,
	HomePhone__c,
	IsFiscalResidencyInFrance__c,
	IsPhoneCompatible__c,
	IsSIMCardCompatible__c,
	MobileOS__c,
	IncomeGatheredOn__c,
	LabelNAFRev2__c,
	LegalForm__c,
	MigratedClient__c,
	QuoteRisk__c,
	MobilePhone__c,
	USIndicator__c,
	NumSiret__c,
	PreferredPhoneNumber__c,
	PrimaryNetwork__c,
	SalesAgent__c,
	SegmentationDistributive__c,
	SegmentationSaving__c,
	SocialReason__c,
	TCHFirstEnrolement__c,
	TCH_RTName__c,
	Turnover__c,
	UndeliverabilityReason__c,
	VIPParnasseCustomer__c,
	TCHSalutation__pc,
	AcceptationMembershipDate__pc,
	Person__pc,
	RightOpposition__pc,
	ActivityBranch__pc,
	DWHCLIJUR,
	NoCall__pc,
	NoLetter__pc,
	NoOrangeEmail__pc,
	NoOrangeSMS__pc,
	NoPartnerEmail__pc,
	NoPartnerSMS__pc,
	AdditionToAddress__pc,
	ApplicationMembershipDate__pc,
	BIC__pc,
	CreditInProgressExceptOB__pc,
	DistributorNetwork__pc,
	FamilyAllocation__pc,
	HousingAllocation__pc,
	IBAN__pc,
	IsAMF__pc,
	IsContactAccountSync__pc,
	IsIOBSP__pc,
	IsTA3C__pc,
	JustifiedIncomesData__pc,
	Manager__pc,
	Prescripteur__pc,
	RCSNumber__pc,
	TaxResidenceInFrance__pc,
	DWHCLIIDT,
	DWHCLIIDN,
	DWHCLIDTX,
	DWHCLIETB,
	DWHCLISIG,
	DWHCLISRN,
	DWHCLICRE,
	DWHCLIRES,
	DWHCLIECO,
	DWHCLIREG,
	DWHCLIGEO,
	DWHCLIENT,
	DWHCLIACT,
	DWHCLICRD,
	DWHCLICOT,
	DWHCLIDIR,
	DWHCLIANF,
	DWHCLISER,
	DWHCLISEP,
	DWHCLIDBA,
	DWHCLICOL,
	DWHCLIGRI,
	DWHCLITRI,
	DWHCLIDLC,
	DWHCLIMOT,
	DWHCLIPRO,
	DWHCLIDOM,
	DWHCLIEST,
	DWHCLIRU2,
	DWHCLIBIL,
	DWHCLIBIM,
	DWHCLIATR,
	DWHCLIBLN,
	DWHCLISTA,
	DWHCLITIE,
	DWHCLIMUT,
	DWHCLIACC,
	DWHCLIDGI,
	DWHCLIDEP,
	DWHCLIRIS,
	DWHCLIFRE,
	LIB_ACTIVITE_CAV,
	LIB_ACTIVITE_CSL,
	LIB_ACTIVITE_GLOBAL
FROM #REF_CLIENT
SELECT @nbRows = @@ROWCOUNT
------Suppression des enregistrement > 90 jrs 

delete from [dbo].[REF_CLIENT] where (DATEDIFF(DAY,[DTE_TRAITEMENT] ,dateadd(dd,-1,@date_alim)) >90 and [DTE_TRAITEMENT]  !=EOMONTH([DTE_TRAITEMENT] ))
END
GO
