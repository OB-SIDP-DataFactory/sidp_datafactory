﻿/**************************************************************************************************************************************************************************************/
CREATE  PROC [dbo].[PKGCP_SUIVI_ENROLEMENT]
   @date_obs date,
   @canal_orig varchar(255),
   @canal_fin varchar(255)
AS  BEGIN 

--declare @date_obs datetime  = '2017-05-24';
with 
cte_jour_alim_list as
(
select StandardDate as date_creat
     , canal_orig.code_canal_orig
     , canal_fin.code_canal_fin
  from dbo.DIM_TEMPS
  cross join ( select CODE_SF as code_canal_orig from dbo.DIM_CANAL where PARENT_ID is not null and CODE_SF in (select [Value] from dbo.FnSplit(@canal_orig, ',')) ) canal_orig
  cross join ( select CODE_SF as code_canal_fin from dbo.DIM_CANAL where PARENT_ID is not null and CODE_SF in (select [Value] from dbo.FnSplit(@canal_fin, ',')) ) canal_fin    
 where dateadd(day,-30, @date_obs) <= StandardDate and StandardDate <= @date_obs
),

cte as (
select date_creat
     , canal_orig
     , canal_fin
     , age_opport
     , stade_vente
     , count(Id_SF) as nb_opp_created
     , count(case when IsWon = 'true' and date_close = date_creat then Id_SF else null end) as nb_ouvert_meme_jour
     , count(case when IsWon = 'true' and date_close <= @date_obs then Id_SF else null end) as nb_ouvert_a_date
  from ( select Id_SF
              , cast(CreatedDate as date) as date_creat
              , cast(CloseDate as date) as date_close
              , DATEDIFF(DD, cast(CreatedDate as date), @date_obs) as age_opport
              , StageName as stade_vente
              , IsWon
			  , case when Indication__c = 'Oui' then StartedChannelForIndication__c else StartedChannel__c end as canal_orig 
              , case when IsWon = 'true' then LeadSource else '-1' end as canal_fin
              , row_number() over (partition by Id_SF order by Validity_StartDate desc) as flag_opport_last_state
           from [$(DataHubDatabaseName)].dbo.PV_SF_OPPORTUNITY_HISTORY
          where ( cast(Validity_StartDate as date) <= @date_obs and @date_obs <= cast(Validity_EndDate as date) and CommercialOfferCode__c = 'OC80' and CommercialOfferName__c is not null 
		  and cast(CreatedDate as date) >='2017-05-16' )
          ) oh 
 where flag_opport_last_state = 1 
   and date_creat >= dateadd(day,-30, @date_obs) and date_creat <= @date_obs
 group by date_creat
        , canal_orig
        , canal_fin
        , age_opport
        , stade_vente
),

cte_expand as (
select jour_alim_list.date_creat
     , jour_alim_list.code_canal_orig as canal_orig
     , jour_alim_list.code_canal_fin as canal_fin
     , coalesce(opport.age_opport, 9999) as age_opport
     , coalesce(opport.stade_vente, '-1') as stade_vente
     , coalesce(opport.nb_opp_created, 0) as nb_opp_created
     , coalesce(opport.nb_ouvert_meme_jour, 0) as nb_ouvert_meme_jour
     , coalesce(opport.nb_ouvert_a_date, 0) as nb_ouvert_a_date
  from cte_jour_alim_list as jour_alim_list
  left join cte as opport
    on opport.date_creat = jour_alim_list.date_creat and opport.canal_orig = jour_alim_list.code_canal_orig and opport.canal_fin = jour_alim_list.code_canal_fin
),

cte_cum as (
select date_creat
     , canal_orig
     , canal_fin
     , age_opport
     , stade_vente
     , nb_opp_created
     , nb_ouvert_meme_jour
     , nb_ouvert_a_date
     , SUM(nb_opp_created) OVER(PARTITION BY canal_orig, canal_fin ORDER BY date_creat, age_opport, stade_vente ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opp_created_cum
     , SUM(nb_ouvert_meme_jour) OVER(PARTITION BY canal_orig, canal_fin ORDER BY date_creat, age_opport, stade_vente ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_ouvert_meme_jour_cum
     , SUM(nb_ouvert_a_date) OVER(PARTITION BY canal_orig, canal_fin ORDER BY date_creat, age_opport, stade_vente ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_ouvert_a_date_cum
  FROM cte_expand ),

cte_fin as (
select opport.date_creat
     , opport.canal_orig
     , canal_orig.LIBELLE as lib_canal_orig
     , opport.canal_fin
     , canal_fin.LIBELLE as lib_canal_fin
     , case when opport.age_opport <= 0 then 0
            when opport.age_opport <= 1 then 1
            when opport.age_opport <= 3 then 3
            when opport.age_opport <= 5 then 5
            when opport.age_opport <= 10 then 10
            when opport.age_opport <= 20 then 20
            when opport.age_opport <= 30 then 30
            when opport.age_opport <= 60 then 60
            else 9999
       end as age_opport
     , case when opport.age_opport <= 0 then 'Jour même'
            when opport.age_opport <= 1 then '1 jour'
            when opport.age_opport <= 3 then '2-3 jours'
            when opport.age_opport <= 5 then '4-5 jours'
            when opport.age_opport <= 10 then '6-10 jours'
            when opport.age_opport <= 20 then '11-20 jours'
            when opport.age_opport <= 30 then '21-30 jours'
            when opport.age_opport <= 60 then '31-60 jours'
            else '60+ jours'
       end as lib_age_opport
     , opport.stade_vente
     , stade_vente.LIBELLE as lib_stade_vente
     , opport.nb_opp_created as nb_opp_created
     , opport.nb_ouvert_meme_jour as nb_ouvert_meme_jour
     , opport.nb_ouvert_a_date as nb_ouvert_a_date
  from cte_cum as opport
  left join dbo.DIM_CANAL canal_orig
    on canal_orig.CODE_SF = opport.canal_orig and canal_orig.PARENT_ID is not null
  left join dbo.DIM_CANAL canal_fin
    on canal_fin.CODE_SF = opport.canal_fin and canal_fin.PARENT_ID is not null
  left join dbo.DIM_STADE_VENTE stade_vente
    on stade_vente.CODE_SF = opport.stade_vente and stade_vente.PARENT_ID is null
)

select * from cte_fin

END