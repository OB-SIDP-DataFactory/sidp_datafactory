﻿-- =============================================
--tables input : [$(DataHubDatabaseName)].[exposition].[PV_SF_TASK]
--               [$(DataHubDatabaseName)].[exposition].[PV_SF_CASE]
--tables output : [SAS_Vue_Armatis_task]
--description : construction de la table sas utilisée pour le rapport Armatis
-- =============================================
CREATE PROCEDURE [dbo].[PKGMK_PROC_ARMATIS_task] @nbRows int OUTPUT
AS
BEGIN

-- vider la table et la reconstruire
truncate table [dbo].SAS_Vue_Armatis_task;

INSERT INTO [dbo].[SAS_Vue_Armatis_task]
           ([FILE_ATTENTE]
           ,[NIV_SAT]
           ,[DTE_CREA]
           ,[DTE_INTERACTION]
           ,[COD_TYP_ENR]
           ,[TYP_ENR_TASK]
           ,[OBJ_TASK]
           ,[SOUS_TYP_TASK]
           ,[TYP_APPEL]
           ,[IDE_USER_SF]
           ,[Lastname]
           ,[Firstname]
           ,[Nb_Task])
	select distinct
	   cas.FILE_ATTENTE,
	   task.NIV_SAT,
	   task.DTE_CREA,
       cast (task.DTE_INTERACTION as datetime) as DTE_INTERACTION,
	   task.COD_TYP_ENR,
	   task.LIB_TYP_ENR as TYP_ENR_TASK,
	   task.OBJ_TASK,
	   task.SOUS_TYP_TASK,
	   task.TYP_APPEL,
	   task.IDE_USER_SF,
	   usr.Lastname,
       usr.Firstname,
	   count (distinct task.IDE_TASK_SF) as Nb_Task
	FROM [$(DataHubDatabaseName)].[exposition].[PV_SF_TASK] task
	LEFT OUTER JOIN [$(DataHubDatabaseName)].[exposition].[PV_SF_CASE] cas ON task.IDE_REQ_SF = cas.IDE_REQ_SF
	LEFT JOIN [$(DataHubDatabaseName)].[dbo].[PV_SF_USER] usr on usr.Id_SF = task.IDE_USER_SF
	group by cas.FILE_ATTENTE,
	   task.NIV_SAT,
	   task.DTE_CREA,
	   task.DTE_INTERACTION,
	   task.COD_TYP_ENR,
	   task.LIB_TYP_ENR,
	   task.OBJ_TASK,
	   task.SOUS_TYP_TASK,
	   task.TYP_APPEL,
	   task.IDE_USER_SF,
	   usr.Lastname,
       usr.Firstname
	
	
	   
    -- Insert statements for procedure here
	SELECT @nbRows = COUNT(*) FROM SAS_Vue_Armatis_task;

END