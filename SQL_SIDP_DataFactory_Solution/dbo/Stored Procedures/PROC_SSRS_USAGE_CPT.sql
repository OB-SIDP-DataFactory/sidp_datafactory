﻿CREATE PROC [dbo].[PROC_SSRS_USAGE_CPT]

AS  BEGIN 

DECLARE @date_obs DATE;
SELECT  @date_obs = CAST(GETDATE() AS DATE);
DECLARE @date_max date;
SELECT  @date_max = dateadd(day, -1 ,GETDATE());
SET DATEFIRST 1; -->1    Lundi
DROP TABLE IF EXISTS #cte_usage_cpt_cp;
DROP TABLE IF EXISTS [dbo].[WK_SSRS_USAGE_CPT];
SELECT  Year_alim,Month_alim,Week_alim,jour_alim
    ,encours_CAV
	,encours_cred_CAV
	,encours_deb_CAV
	,encours_deb_non_auth_CAV
	,montant_dep_CSL
	,montant_ret_CSL
	,flux_net_CSL
	,encours_CSL
	--Week to date
	,sum(encours_CAV) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS encours_CAV_WTD --Week to date
	,sum(encours_cred_CAV) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS encours_cred_CAV_WTD --Week to date
	,sum(encours_deb_CAV) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS encours_deb_CAV_WTD --Week to date
	,sum(encours_deb_non_auth_CAV) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS encours_deb_non_auth_CAV_WTD --Week to date
	,sum(montant_dep_CSL) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS montant_dep_CSL_WTD --Week to date
	,sum(montant_ret_CSL) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS montant_ret_CSL_WTD --Week to date
	,sum(flux_net_CSL) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS flux_net_CSL_WTD --Week to date
	,sum(encours_CSL) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS encours_CSL_WTD --Week to date

	--Month to date
	,sum(encours_CAV) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS encours_CAV_MTD --Month to date
	,sum(encours_cred_CAV) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS encours_cred_CAV_MTD --Month to date
	,sum(encours_deb_CAV) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS encours_deb_CAV_MTD --Month to date
	,sum(encours_deb_non_auth_CAV) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS encours_deb_non_auth_CAV_MTD --Month to date
	,sum(montant_dep_CSL) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS montant_dep_CSL_MTD --Month to date
	,sum(montant_ret_CSL) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS montant_ret_CSL_MTD --Month to date
	,sum(flux_net_CSL) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS flux_net_CSL_MTD --Month to date
	,sum(encours_CSL) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS encours_CSL_MTD --Month to date
	--Full Month
	,sum(encours_CAV) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS encours_CAV_FM --Full Month
	,sum(encours_cred_CAV) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS encours_cred_CAV_FM --Full Month
	,sum(encours_deb_CAV) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS encours_deb_CAV_FM --Full Month
	,sum(encours_deb_non_auth_CAV) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS encours_deb_non_auth_CAV_FM --Full Month
	,sum(montant_dep_CSL) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS montant_dep_CSL_FM --Full Month
	,sum(montant_ret_CSL) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS montant_ret_CSL_FM --Full Month
	,sum(flux_net_CSL) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS flux_net_CSL_FM --Full Month
	,sum(encours_CSL) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS encours_CSL_FM --Full Month
	--Year to date
    ,sum(encours_CAV) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS encours_CAV_YTD  --Year to date
    ,sum(encours_cred_CAV) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS encours_cred_CAV_YTD  --Year to date
    ,sum(encours_deb_CAV) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS encours_deb_CAV_YTD  --Year to date
    ,sum(encours_deb_non_auth_CAV) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS encours_deb_non_auth_CAV_YTD  --Year to date
    ,sum(montant_dep_CSL) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS montant_dep_CSL_YTD  --Year to date
    ,sum(montant_ret_CSL) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS montant_ret_CSL_YTD  --Year to date
    ,sum(flux_net_CSL) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS flux_net_CSL_YTD  --Year to date
    ,sum(encours_CSL) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS encours_CSL_YTD  --Year to date
	--Full year
	,sum(encours_CAV) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS encours_CAV_FY  --Full year
	,sum(encours_cred_CAV) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS encours_cred_CAV_FY  --Full year
	,sum(encours_deb_CAV) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS encours_deb_CAV_FY  --Full year
	,sum(encours_deb_non_auth_CAV) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS encours_deb_non_auth_CAV_FY  --Full year
	,sum(montant_dep_CSL) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS montant_dep_CSL_FY  --Full year
	,sum(montant_ret_CSL) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS montant_ret_CSL_FY  --Full year
	,sum(flux_net_CSL) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS flux_net_CSL_FY  --Full year
	,sum(encours_CSL) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS encours_CSL_FY  --Full year
INTO #cte_usage_cpt_cp
FROM  ( 
     SELECT  T1.StandardDate as jour_alim,DATEPART(YEAR,T1.Date) AS Year_alim,DATEPART(MONTH,T1.Date) AS Month_alim,DATEPART(YEAR,DATEADD(day, (7 - DATEPART(dw, T1.StandardDate)), T1.StandardDate)) *100 + DATEPART(WEEK,DATEADD(day, (7 - DATEPART(dw, T1.StandardDate)), T1.StandardDate)) AS Week_alim
	, sum(case when code_sab_type_compte = '251180' then coalesce(encours_cred, 0) + coalesce(encours_deb, 0) else 0 end) as encours_CAV
	, sum(case when code_sab_type_compte = '251180' then encours_cred else 0 end) as encours_cred_CAV
	, sum(case when code_sab_type_compte = '251180' then encours_deb else 0 end) as encours_deb_CAV
	, sum(case when code_sab_type_compte = '251180' then encours_deb_non_auth else 0 end) as encours_deb_non_auth_CAV
	, sum(case when code_sab_type_compte in ('254181','254111') then montant_dep else 0 end) as montant_dep_CSL
	, sum(case when code_sab_type_compte in ('254181','254111') then montant_ret else 0 end) as montant_ret_CSL
	, (sum(case when code_sab_type_compte in ('254181','254111') then coalesce(montant_dep, 0) + coalesce(montant_ret, 0) else 0 end)*(-1)) as flux_net_CSL
	, sum(case when code_sab_type_compte in ('254181','254111') then coalesce(encours_cred, 0) + coalesce(encours_deb, 0) else 0 end) as encours_CSL
FROM [dbo].[DIM_TEMPS] T1 
LEFT OUTER JOIN  sas_vue_usage_cpt  ON T1.StandardDate=sas_vue_usage_cpt.jour_alim
WHERE  ( T1.StandardDate >= CAST(DATEADD(yyyy, DATEDIFF(YYYY, 0, DATEADD(yyyy, -2, @date_obs)), 0) AS DATE)
AND T1.StandardDate <=  DATEADD(day, (7 - DATEPART(dw, @date_obs)), @date_obs) )
GROUP BY  T1.StandardDate ,DATEPART(YEAR,T1.Date) ,DATEPART(MONTH,T1.Date) ,DATEPART(WEEK,T1.Date) 
) t;		
SELECT  
  CY.jour_alim
, CY.Year_alim
, CY.Month_alim
, CY.Week_alim
, coalesce(CY.encours_CAV,0) AS encours_CAV
, coalesce(CY.encours_CAV_WTD,0) AS encours_CAV_WTD
, coalesce(CY.encours_CAV_MTD,0) AS encours_CAV_MTD
, coalesce(CY.encours_CAV_YTD,0) AS encours_CAV_YTD
, coalesce(PM.encours_CAV_FM,0) AS encours_CAV_FM
, coalesce(PY.encours_CAV_FY,0) AS encours_CAV_FY
, coalesce(CY.encours_cred_CAV,0) AS encours_cred_CAV
, coalesce(CY.encours_cred_CAV_WTD,0) AS encours_cred_CAV_WTD
, coalesce(CY.encours_cred_CAV_MTD,0) AS encours_cred_CAV_MTD
, coalesce(CY.encours_cred_CAV_YTD,0) AS encours_cred_CAV_YTD
, coalesce(PM.encours_cred_CAV_FM,0) AS encours_cred_CAV_FM
, coalesce(PY.encours_cred_CAV_FY,0) AS encours_cred_CAV_FY
, coalesce(CY.encours_deb_CAV,0) AS encours_deb_CAV
, coalesce(CY.encours_deb_CAV_WTD,0) AS encours_deb_CAV_WTD
, coalesce(CY.encours_deb_CAV_MTD,0) AS encours_deb_CAV_MTD
, coalesce(CY.encours_deb_CAV_YTD,0) AS encours_deb_CAV_YTD
, coalesce(PM.encours_deb_CAV_FM,0) AS encours_deb_CAV_FM
, coalesce(PY.encours_deb_CAV_FY,0) AS encours_deb_CAV_FY
, coalesce(CY.encours_deb_non_auth_CAV,0) AS encours_deb_non_auth_CAV
, coalesce(CY.encours_deb_non_auth_CAV_WTD,0) AS encours_deb_non_auth_CAV_WTD
, coalesce(CY.encours_deb_non_auth_CAV_MTD,0) AS encours_deb_non_auth_CAV_MTD
, coalesce(CY.encours_deb_non_auth_CAV_YTD,0) AS encours_deb_non_auth_CAV_YTD
, coalesce(PM.encours_deb_non_auth_CAV_FM,0) AS encours_deb_non_auth_CAV_FM
, coalesce(PY.encours_deb_non_auth_CAV_FY,0) AS encours_deb_non_auth_CAV_FY
, coalesce(CY.montant_dep_CSL,0) AS montant_dep_CSL
, coalesce(CY.montant_dep_CSL_WTD,0) AS montant_dep_CSL_WTD
, coalesce(CY.montant_dep_CSL_MTD,0) AS montant_dep_CSL_MTD
, coalesce(CY.montant_dep_CSL_YTD,0) AS montant_dep_CSL_YTD
, coalesce(PM.montant_dep_CSL_FM,0) AS montant_dep_CSL_FM
, coalesce(PY.montant_dep_CSL_FY,0) AS montant_dep_CSL_FY
, coalesce(CY.montant_ret_CSL,0) AS montant_ret_CSL
, coalesce(CY.montant_ret_CSL_WTD,0) AS montant_ret_CSL_WTD
, coalesce(CY.montant_ret_CSL_MTD,0) AS montant_ret_CSL_MTD
, coalesce(CY.montant_ret_CSL_YTD,0) AS montant_ret_CSL_YTD
, coalesce(PM.montant_ret_CSL_FM,0) AS montant_ret_CSL_FM
, coalesce(PY.montant_ret_CSL_FY,0) AS montant_ret_CSL_FY
, coalesce(CY.flux_net_CSL,0) AS flux_net_CSL
, coalesce(CY.flux_net_CSL_WTD,0) AS flux_net_CSL_WTD
, coalesce(CY.flux_net_CSL_MTD,0) AS flux_net_CSL_MTD
, coalesce(CY.flux_net_CSL_YTD,0) AS flux_net_CSL_YTD
, coalesce(PM.flux_net_CSL_FM,0) AS flux_net_CSL_FM
, coalesce(PY.flux_net_CSL_FY,0) AS flux_net_CSL_FY
, coalesce(CY.encours_CSL,0) AS encours_CSL
, coalesce(CY.encours_CSL_WTD,0) AS encours_CSL_WTD
, coalesce(CY.encours_CSL_MTD,0) AS encours_CSL_MTD
, coalesce(CY.encours_CSL_YTD,0) AS encours_CSL_YTD
, coalesce(PM.encours_CSL_FM,0) AS encours_CSL_FM
, coalesce(PY.encours_CSL_FY,0) AS encours_CSL_FY
INTO [dbo].[WK_SSRS_USAGE_CPT]
FROM            #cte_usage_cpt_cp AS CY LEFT OUTER JOIN
                         #cte_usage_cpt_cp AS PM ON DATEADD(MONTH, - 1, CY.jour_alim) = PM.jour_alim LEFT OUTER JOIN
                         #cte_usage_cpt_cp AS PY ON DATEADD(YEAR, - 1, CY.jour_alim) = PY.jour_alim
ORDER BY CY.jour_alim;

END