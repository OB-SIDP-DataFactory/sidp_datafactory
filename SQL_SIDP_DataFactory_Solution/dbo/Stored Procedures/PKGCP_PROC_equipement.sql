﻿
CREATE PROC [dbo].[PKGCP_PROC_equipement]  @nbRows int OUTPUT -- nb lignes processées
AS  BEGIN

-- ===============================================================================================================================================================================
--tables input : [$(DataHubDatabaseName)].dbo.PV_FE_EQUIPMENT_SERVICE_ASSOCIATION
--				 [$(DataHubDatabaseName)].dbo.PV_FE_SERVICEPRODUCT
--tables output : [ssrs_vue_equipement]
--description : construction de la vue sas des assurances souscrites pour le cockpit
-- =============================================================================================================================================================================

declare @date_min date 
select @date_min = MIN(cast(date_souscription as date )) from ssrs_vue_equipement
--select @date_min

declare @date_max date 
select @date_max = MAX(cast(date_souscription as date )) from ssrs_vue_equipement
--select @date_max

delete from ssrs_vue_equipement
where date_souscription between @date_min and @date_max

insert into ssrs_vue_equipement
select cast(eqpser.CREATION_DATE as date) as date_souscription, eqpser.SERVICE_PRODUCT_ID as code_fe_assurance, ser.LABEL as type_assurance, COUNT(eqpser.SERVICE_PRODUCT_ID) as nb_assurances
from [$(DataHubDatabaseName)].dbo.[PV_FE_EQUIPMENTSVCASSOC] eqpser
	inner join [$(DataHubDatabaseName)].dbo.PV_FE_SERVICEPRODUCT ser
		on ser.ID_FE_SERVICEPRODUCT = eqpser.SERVICE_PRODUCT_ID
where cast(eqpser.CREATION_DATE as date) < cast(getdate() as date) 
group by cast(eqpser.CREATION_DATE as date), ser.LABEL, eqpser.SERVICE_PRODUCT_ID

-- select * from ssrs_vue_equipement

select @nbRows = COUNT(*) FROM ssrs_vue_equipement;

EXEC [dbo].[PROC_SSRS_VUE_EQUIPEMENT];
END
GO