﻿CREATE PROC [dbo].[PKGCM_PROC_canal_opport]
  @DATE_ALIM date
, @jours_histo int 
, @nbRows int OUTPUT
AS  BEGIN 
-- ===============================================================================================================================
--tables input : [$(DataHubDatabaseName)].[dbo].[PV_SF_OPPORTUNITY_HISTORY]
--tables output : WK_OPPORTUNITY_CANAL
--description : créer une table des opportunités avec le nombre de canaux de distribution pour chaque id opport
-- ==============================================================================================================================

if @DATE_ALIM is null 
  set @DATE_ALIM = GETDATE();
 
if @jours_histo is null 
  set @jours_histo = 1; --valeur 1 par défaut = traitement nominal par défaut

/* construction nombre de canaux de distribution pour chaque opportunité */
IF OBJECT_ID('[WK_OPPORTUNITY_CANAL]') IS NOT NULL
BEGIN
    TRUNCATE TABLE [WK_OPPORTUNITY_CANAL]
END;

insert into [WK_OPPORTUNITY_CANAL]
select Id_SF, count(distinct can.PARENT_ID) as nb_canaux_distrib
  from [$(DataHubDatabaseName)].[dbo].[PV_SF_OPPORTUNITY_HISTORY] op
  inner join DIM_CANAL can
    on can.CODE_SF = op.LeadSource and PARENT_ID is not null -- niveau canal d'interaction
 where coalesce(op.Validity_EndDate, '9999-12-31') >= dateadd(dd, -@jours_histo-90, @DATE_ALIM)
 group by op.Id_SF;

select @nbRows = count(*) from [WK_OPPORTUNITY_CANAL] ;

END 