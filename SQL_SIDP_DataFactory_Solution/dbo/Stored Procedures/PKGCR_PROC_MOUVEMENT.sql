﻿

/****** Object:  StoredProcedure [dbo].[PKGCR_PROC_MOUVEMENT]    Script Date: 17/11/2017 15:46:10 ******/

-- =============================================
-- Author:		<Yousra,LAZRAK>
-- Create date: <17/01/2017>
-- Description:	<Alimentation de la table CRE_MOUVEMENT contenant les opérations sur le compte Crédit >
-- Tables input : [$(DataHubDatabaseName)].[dbo].[PV_SA_MOUVEMENT]
-- Table output : CRE_MOUVEMENT
-- =============================================

CREATE PROCEDURE [dbo].[PKGCR_PROC_MOUVEMENT] 
(
   @date_alim DATE
    ,@nbRows int OUTPUT -- nb lignes processées
)
AS
BEGIN
--Insertion des informations dans la table CRE_MOUVEMENT
INSERT INTO [dbo].[CRE_MOUVEMENT] (
	DTE_ALIM,
	COD_OPE,
	NUM_OPE,
	COD_EVE,
	COD_SCH,
	NUM_PIE,
	NUM_ECR,
	COD_ANA,
	STR_ANA,
	COD_EXO,
	COD_BDF,
	NAT_OPE,
	NUM_CPT_SAB,
	MTT_OPE_DEV,
	SENS_OPE,
	DTE_OPE,
	DTE_CPT,
	DTE_VAL,
	DTE_TRT,
	COD_USER,
	COD_AGEN,
	COD_SVC,
	COD_SS_SVC,
	COD_ANN,
	COD_BAN_EIC,
	LIB_MVT1,
	LIB_MVT2,
	LIB_MVT3,
	COD_EME,
	DTE_TRT_MAD,
	DTE_REF_MAD,
	DTE_LIM_REJ_MAD,
	COD_USER_INIT_MAD,
	DTE_CREA_MAD,
	DTE_USAGE_MAD,
	COD_USER_MAD,
	REJ_ACC_MAD,
	DON_ANA,
	LIB_COMP1,
	LIB_COMP2,
	NUM_REM,
	NUM_CHQ,
	MTT_OPE_EUR,
	DEV,
	IDE_DO,
	IDE_CTP,
	NOM_CTP,
	NUM_CPT_CTP,
	BIC_ETA_CTP,
	COD_BAN_ETA_CTP,
	COD_PAY_ETA_CTP,
	FLG_CPT_CLT,
	FIX_DEV,
	COD_AGEN_CPT
	
)
SELECT @date_alim AS DTE_ALIM,
	COD_OPE,
	NUM_OPE,
	COD_EVE,
	COD_SCH,
	NUM_PIE,
	NUM_ECR,
	COD_ANA,
	STR_ANA,
	COD_EXO,
	COD_BDF,
	NAT_OPE,
	PV_SA_Q_MOUVEMENT.NUM_CPT,
	MTT_DEV,
	SEN_MTT,
	DTE_OPE,
	DTE_CPT,
	DTE_VAL,
	DTE_TRT,
	COD_UTI,
	COD_AGE,
	COD_SVC,
	COD_SOU_SVC,
	COD_ANN,
	COD_BAN_EIC,
	LIB_MVT_1,
	LIB_MVT_2,
	LIB_MVT_3,
	COD_EME,
	DTE_TRT_MAD,
	DTE_REF_MAD,
	DTE_LIM_REJ_MAD,
	COD_UTI_INI_MAD,
	DTE_CRE_MAD,
	DTE_UTI_MAD,
	COD_UTI_MAD,
	REJ_ACC_MAD,
	DON_ANA,
	LIB_COMP_1,
	LIB_COMP_2,
	NUM_REM,
	NUM_CHQ,
	MTT_EUR,
	DEV,
	IDE_DO,
	IDE_CTP,
	NOM_CTP,
	NUM_CPT_CTP,
	BIC_CTP,
	COD_BAN_CTP,
	COD_PAY_CTP,
	TOP_CLI,
	FIX_DEV,
	COD_AGE_OPE

FROM [$(DataHubDatabaseName)].dbo.PV_SA_Q_MOUVEMENT
 JOIN [$(DataHubDatabaseName)].dbo.PV_SA_Q_COMPTE
    ON PV_SA_Q_MOUVEMENT.NUM_CPT=PV_SA_Q_COMPTE.DWHCPTCOM
 JOIN [$(DataHubDatabaseName)].dbo.PV_SA_Q_COM
    ON PV_SA_Q_COMPTE.DWHCPTCOM=PV_SA_Q_COM.COMREFCOM
 JOIN [$(DataHubDatabaseName)].dbo.PV_FI_Q_DES
    ON PV_SA_Q_COM.COMREFREF=PV_FI_Q_DES.REF_CPT

		SELECT @nbRows = @@ROWCOUNT
;
END
