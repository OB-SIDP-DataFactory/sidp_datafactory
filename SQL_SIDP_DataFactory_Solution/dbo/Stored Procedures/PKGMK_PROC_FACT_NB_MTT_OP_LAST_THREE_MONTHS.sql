﻿

CREATE PROC [dbo].[PKGMK_PROC_FACT_NB_MTT_OP_LAST_THREE_MONTHS]
    @DATE_ALIM  DATE  
  , @mois_histo INT         -- valeur 0 par défaut = traitement nominal par défaut
  , @nbRows     INT OUTPUT  -- nb lignes processées
AS  
BEGIN 

if @DATE_ALIM is null 
  set @DATE_ALIM = GETDATE();
 
if @mois_histo is null 
  set @mois_histo = 0; --valeur 0 par défaut = traitement nominal par défaut

set  @DATE_ALIM = DATEADD(dd,-1,@DATE_ALIM); -- Pour gérer le dernier jours du mois (données a j-1 dans la table Quoti)
 --------------------------------------///////////////// Step 1
--calcul de la période de chargement
IF OBJECT_ID('tempdb..#WK_TEMPS_ACCOUNT') IS NOT NULL
BEGIN DROP TABLE #WK_TEMPS_ACCOUNT END
 
select Year, Month, max(StandardDate) as StandardDate
  into #WK_TEMPS_ACCOUNT
  from [dbo].DIM_TEMPS
 where StandardDate <= eomonth(@DATE_ALIM)
   and StandardDate >= dateadd(month, -@mois_histo, eomonth(@DATE_ALIM))
 group by Year, Month

 --select * from #WK_TEMPS_ACCOUNT

--nettoyage de la table cible (suppréssion des lignes a recharger)

delete from [dbo].[T_FACT_MKT_NB_MTT_OP_LAST_THREE_MONTHS]
where [MOIS_TRAITEMENT] IN (select concat(Year,Month) from #WK_TEMPS_ACCOUNT)

/*************** Table Intermédiaire ***************/
IF OBJECT_ID('tempdb..#T_FACT_MKT_NB_MTT_OP_LAST_THREE_MONTHS') IS NOT NULL
BEGIN DROP TABLE #T_FACT_MKT_NB_MTT_OP_LAST_THREE_MONTHS END

CREATE TABLE #T_FACT_MKT_NB_MTT_OP_LAST_THREE_MONTHS(
	[DTE_EOM] [date] NULL,
	[MOIS_TRAITEMENT] [varchar](20) NULL,
	[NUM_CPT_SAB] [varchar](20) NULL,
	[NB_OP_DEB] [int] NULL,
	[NB_OP_CRE] [int] NULL,
	[MTT_OP_DEB] [decimal](18, 2) NULL,
	[MTT_OP_CRE] [decimal](18, 2) NULL
)
/**************************************************/

declare @Mois_Traitement int ;
declare @Standardate date ;
declare Mois_traitement_Cursor CURSOR for select format(StandardDate,'yyyyMM') from #WK_TEMPS_ACCOUNT

OPEN Mois_traitement_Cursor;
FETCH NEXT FROM Mois_traitement_Cursor INTO @Mois_Traitement;
WHILE @@FETCH_STATUS = 0
   BEGIN
   set @Standardate = (select StandardDate from #WK_TEMPS_ACCOUNT where format(StandardDate,'yyyyMM') = @Mois_Traitement);
   
   Insert into #T_FACT_MKT_NB_MTT_OP_LAST_THREE_MONTHS
   (
    [DTE_EOM] 
   ,[MOIS_TRAITEMENT]
   ,[NUM_CPT_SAB]
   ,[NB_OP_DEB] 
   ,[NB_OP_CRE] 
   ,[MTT_OP_DEB] 
   ,[MTT_OP_CRE] 
   )
   select @Standardate,
          @Mois_Traitement,
          t.NUM_CPT_SAB,  
	      sum(case when t.FLAG_MTT_OPT = 1 and t.FLAG_INITIATIVE_CLIENT ='OUI' then t.NOMBRE_OP else 0 END) - sum(case when t.FLAG_INITIATIVE_CLIENT= 'DEDUIRE' and t.FLAG_OP_DEB_CRE='Debit' then  t.NOMBRE_OP else 0 END  ) as NB_OP_DEB ,
	      sum(case when t.FLAG_MTT_OPT = 0 and t.FLAG_INITIATIVE_CLIENT ='OUI' then t.NOMBRE_OP else 0 END) - sum(case when t.FLAG_INITIATIVE_CLIENT= 'DEDUIRE' and t.FLAG_OP_DEB_CRE='Credit' then t.NOMBRE_OP else 0 END ) as NB_OP_CRE,
	      sum(case when t.FLAG_MTT_OPT = 1 and t.FLAG_INITIATIVE_CLIENT ='OUI' then t.MONTANT_OP else 0 END) - sum(case when t.FLAG_INITIATIVE_CLIENT= 'DEDUIRE' and t.FLAG_OP_DEB_CRE='Debit' then  t.MONTANT_OP else 0 END  ) as MTT_OP_DEB ,
	      sum(case when t.FLAG_MTT_OPT = 0 and t.FLAG_INITIATIVE_CLIENT ='OUI' then ABS(t.MONTANT_OP) else 0 END) - sum(case when t.FLAG_INITIATIVE_CLIENT= 'DEDUIRE' and t.FLAG_OP_DEB_CRE='Credit' then t.MONTANT_OP else 0 END ) as MTT_OP_CRE
   from [dbo].[T_FACT_MKT_USAGE_CPT_MONTH] t
   where t.[MOIS_TRAITEMENT] >= format(cast(dateadd(mm,-3,@Standardate) as date),'yyyyMM') and t.[MOIS_TRAITEMENT] < format(cast(@Standardate as date),'yyyyMM')
   group by  t.NUM_CPT_SAB,t.FLAG_MTT_OPT,t.TYP_MVT,t.FLAG_INITIATIVE_CLIENT,t.FLAG_OP_DEB_CRE 
      FETCH NEXT FROM Mois_traitement_Cursor INTO @Mois_Traitement;
   END;
CLOSE Mois_traitement_Cursor;
DEALLOCATE Mois_traitement_Cursor;

/************ Insertion dans la table principale **************/

Insert into [dbo].[T_FACT_MKT_NB_MTT_OP_LAST_THREE_MONTHS]
   (
    [DTE_EOM] 
   ,[MOIS_TRAITEMENT]
   ,[NUM_CPT_SAB]
   ,[NB_OP_DEB] 
   ,[NB_OP_CRE] 
   ,[MTT_OP_DEB] 
   ,[MTT_OP_CRE] 
   )
select 
 [DTE_EOM] 
,[MOIS_TRAITEMENT]
,[NUM_CPT_SAB]
,sum([NB_OP_DEB])  as [NB_OP_DEB]  
,sum([NB_OP_CRE])  as [NB_OP_CRE] 
,sum([MTT_OP_DEB]) as [MTT_OP_DEB]
,sum([MTT_OP_CRE]) as [MTT_OP_CRE]
from #T_FACT_MKT_NB_MTT_OP_LAST_THREE_MONTHS
group by 
 [DTE_EOM] 
,[MOIS_TRAITEMENT]
,[NUM_CPT_SAB]

/*************************************************************/

DROP TABLE #T_FACT_MKT_NB_MTT_OP_LAST_THREE_MONTHS;

SELECT @nbRows = @@ROWCOUNT

END
GO