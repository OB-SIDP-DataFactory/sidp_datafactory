﻿CREATE PROC [dbo].[PKGCM_PROC_stock_opport]
  @DATE_ALIM date
, @jours_histo int 
, @nbRows int OUTPUT
AS  BEGIN 
-- ===============================================================================================================================
--tables input : [$(DataHubDatabaseName)].[dbo].[PV_SF_OPPORTUNITY_HISTORY]
--               [WK_OPPORTUNITY_CANAL]
--               [PV_SF_ACCOUNT]
--               [DIM_TEMPS]
--               [DIM_CANAL]
--tables output : WK_STOCK_OPPORTUNITY
--description : créer une table avec l'historique des opportunités en stock pour chaque date de chargement
-- ==============================================================================================================================

if @DATE_ALIM is null 
  set @DATE_ALIM = GETDATE();
 
if @jours_histo is null 
  set @jours_histo = 1; --valeur 1 par défaut = traitement nominal par défaut
DROP TABLE IF EXISTS [dbo].[wk_stock_opport_tmp];

delete from [dbo].[WK_STOCK_OPPORTUNITY] where date_alim >= cast( dateadd(day,-@jours_histo, @DATE_ALIM) as date);

/* créer un sous-ensemble de la table dimension temps     */
/* liste des dates pour lesquelles le stock est recalculé */
with wk_temps_opportunity as (
select StandardDate 
  from dbo.DIM_TEMPS
 where cast( dateadd(day,-@jours_histo, @DATE_ALIM) as date) <= StandardDate and StandardDate < cast(@DATE_ALIM as date)
),

wk_opport_histo as (
select temps.StandardDate as date_alim
     , opport.*
  from wk_temps_opportunity as temps
  left join    [$(DataHubDatabaseName)].[dbo].[PV_SF_OPPORTUNITY_HISTORY] as opport
  on ( cast(opport.Validity_StartDate as date) <= temps.StandardDate and temps.StandardDate <= cast(opport.Validity_EndDate as date) )
) 

select oh.date_alim
     , oh.Id_SF
     , oh.StageName
     , coalesce(oh.RejectReason__c, oh.DeniedOpportunityReason__c) as motif_non_aboutie
     , oh.Indication__c
     , case when oh.Indication__c = 'Oui' then 'Oui' else 'Non' end as flag_indication
     , oh.CreatedById
     , oh.CreatedDate as CreatedDate
     , oh.CloseDate
       --âge de l'opportunité en fct de la date d'obs pour opp en cours, date de clôture pour opp finalisées
     , case when StageName in ('09', '10', '11','13') then datediff(DD, oh.CreatedDate, oh.CloseDate) else DATEDIFF(DD, oh.CreatedDate, oh.date_alim) end as age_opport
     , oh.OwnerId
     , oh.AdvisorCode__c
     , oh.PointOfSaleCode__c
     , oh.CommercialOfferCode__c
     , oh.CommercialOfferName__c
     , oh.RelationEntryScore__c
     , case when StageName in ('09', '10', '11','13') and opp_can.nb_canaux_distrib = 1 and can.PARENT_ID = '01' then 1 else 0 end as flag_full_digi
     , case when StageName in ('09', '10', '11','13') and opp_can.nb_canaux_distrib = 1 and can.PARENT_ID = '02' then 1 else 0 end as flag_full_btq 
     , case when oh.Indication__c = 'Oui' then oh.StartedChannelForIndication__c else oh.StartedChannel__c end as StartedChannel__c 
     , oh.DistributionChannel__c
     , oh.DistributorNetwork__c
     , oh.DistributorEntity__c
     , oh.LeadSource
       --figer canal finalisation quand stage de vente = compte ouvert
     , case when oh.StageName in ('09','13') then oh.LeadSource else '00' end as canal_fin
       --figer sous réseau finalisation quand stage de vente = compte ouvert
     , case when oh.StageName in ('09','13') then oh.DistributorEntity__c else '00' end as sous_reseau_fin 
     , a.OptInDataTelco__c
     , a.OptInOrderTelco__c
     , oh.AccountId
     , oh.ProductFamilyCode__c
     , oh.ProductFamilyName__c
     , case when (a.Lastname like 'SID%') then 1 else 0 end as flag_sidp
     , oh.Validity_StartDate as last_update_date
     , case when row_number() over (partition by oh.date_alim, oh.Id_SF order by oh.Validity_StartDate desc) = 1 then 1 else 0 end as flag_opport_last_state
     , case when row_number() over (partition by oh.date_alim, oh.Id_SF, oh.StageName order by oh.Validity_StartDate desc) = 1 then 1 else 0 end as flag_opport_stage_last_state
  into wk_stock_opport_tmp
  from wk_opport_histo as oh
  left join [$(DataHubDatabaseName)].[dbo].[PV_SF_ACCOUNT] as a
    on oh.AccountId = a.Id_SF
  left join [dbo].[WK_OPPORTUNITY_CANAL] as opp_can
    on oh.Id_SF = opp_can.id_sf
  left join [dbo].[DIM_CANAL] as can
    on oh.LeadSource = can.CODE_SF and can.PARENT_ID is not null -- niveau canal d'interaction 
  where oh.CommercialOfferCode__c is not null and oh.CommercialOfferName__c is not null;
   
  	update  src
	set flag_opport_last_state_overall=0
	from [dbo].[WK_STOCK_OPPORTUNITY] src  inner join (select distinct id_sf from wk_stock_opport_tmp) as trg 
	on src.Id_SF=trg.Id_SF and src.flag_opport_last_state_overall=1;
	
    update  src
	set flag_opport_stage_last_state_overall=0
	from [dbo].[WK_STOCK_OPPORTUNITY] src  inner join (select distinct id_sf,StageName from wk_stock_opport_tmp) as trg  
	on src.Id_SF=trg.Id_SF and src.StageName=trg.StageName and src.flag_opport_stage_last_state_overall=1;

  insert into [dbo].[WK_STOCK_OPPORTUNITY] (
       date_alim
     , Id_SF
     , StageName
     , motif_non_aboutie
     , Indication__c
     , flag_indication
     , CreatedById
     , CreatedDate
     , CloseDate
     , age_opport
     , OwnerId
     , AdvisorCode__c
     , PointOfSaleCode__c
     , CommercialOfferCode__c
     , CommercialOfferName__c
     , RelationEntryScore__c
     , flag_full_digi
     , flag_full_btq
     , StartedChannel__c
     , DistributionChannel__c
     , DistributorNetwork__c
     , DistributorEntity__c
     , LeadSource
     , canal_fin
     , sous_reseau_fin
     , OptInDataTelco__c
     , OptInOrderTelco__c
     , AccountId
     , ProductFamilyCode__c
     , ProductFamilyName__c
     , flag_sidp
     , last_update_date
     , flag_opport_last_state
     , flag_opport_stage_last_state
     , flag_opport_last_state_overall
     , flag_opport_stage_last_state_overall )
select *
     , case when row_number() over (partition by Id_SF order by last_update_date,date_alim  desc) = 1 then 1 else 0 end as flag_opport_last_state_overall
     , case when row_number() over (partition by Id_SF, StageName order by last_update_date,date_alim desc) = 1 then 1 else 0 end as flag_opport_stage_last_state_overall
from wk_stock_opport_tmp ;

select @nbRows = count(*) from wk_stock_opport_tmp;

END