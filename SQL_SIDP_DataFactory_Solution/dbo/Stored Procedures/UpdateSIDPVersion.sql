﻿CREATE PROCEDURE [dbo].[UpdateSIDPVersion]
	@Application	AS	VARCHAR(10),
	@Version		AS	VARCHAR(10)
AS
BEGIN
	SET NOCOUNT ON;
	-- Declaring Variables

	DECLARE
		@CurrentVersion	AS	VARCHAR(10),
		@ProcessingDateTime AS DATETIME2 = GETDATE();


	--Get Current Version if Exists
	SELECT
		@CurrentVersion = V.[Version]
	FROM
		dbo.PARAM_SIDP_VERSIONS	AS	V	WITH(NOLOCK)
	WHERE
		1 = 1
		AND	YEAR(V.ValidityEndDate) = 9999
		AND	V.[Application] = @Application;

	-- Update Validity End Date for the last version
	IF(@CurrentVersion IS NOT NULL	AND	@CurrentVersion <> @Version)
	BEGIN
		UPDATE
			dbo.PARAM_SIDP_VERSIONS
		SET
			ValidityEndDate = @ProcessingDateTime
		WHERE
			[Application] = @Application
			AND	[Version] = @CurrentVersion
	END

	-- Insert New version parameters
	IF(@CurrentVersion IS NULL	OR	@CurrentVersion <> @Version)
	BEGIN
		INSERT INTO	dbo.PARAM_SIDP_VERSIONS
		(
			[Application],
			[Version],
			[ValidityStartDate]
		)
		VALUES
		(
			@Application,
			@Version,
			@ProcessingDateTime
		);
	END
	
	RETURN
END