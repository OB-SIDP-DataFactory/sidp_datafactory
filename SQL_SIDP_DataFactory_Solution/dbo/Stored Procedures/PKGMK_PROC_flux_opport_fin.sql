﻿
CREATE PROC [dbo].[PKGMK_PROC_flux_opport_fin]
    @nbRows int OUTPUT -- nb lignes processées  
AS
BEGIN 
-- ===============================================================================================================================================================================
--tables input : [WK_STOCK_OPPORTUNITY]
--               DIM_TEMPS
--               DIM_STADE_VENTE
--               DIM_CANAL
--               DIM_RESEAU
--tables output : [sas_vue_flux_opport_fin]
--description : construction de la vue sas des opportunités finalisées
--				calcul stade de vente précédent
-- =============================================================================================================================================================================

-- vider la table et la reconstruire
truncate table [dbo].[sas_vue_flux_opport_fin];

--flux opportunités finalisées + stade de vente précédent 
with cte_prec_stade as (
select Id_SF
     , StageName
     , LAG(StageName, 1, '00') OVER (PARTITION BY Id_SF ORDER BY last_update_date) AS prec_stade
  from [dbo].[WK_STOCK_OPPORTUNITY]
 where flag_opport_stage_last_state_overall = 1
)

--jointure tables dimension 
insert into [sas_vue_flux_opport_fin]
select d.CloseDate
     , d5a.StandardDate as jour_alim
     , d.Id_SF
     , d.AccountId
     , d1.LIBELLE as stade_vente
     , isnull(d1M.LIBELLE, 'Non renseigné') as motif_sans_suite
     , d5c.StandardDate as date_creat_opp
     , d.CloseDate as date_fin_opp
     , d.age_opport
     , d.flag_full_digi
     , d.flag_full_btq
     , isnull(d.flag_indication, 'false') as flag_indication
--   , isnull(d.OptInDataTelco__c, 'false') as flag_identif_OF
--   , isnull(d.OptInOrderTelco__c, 'false') as flag_facture_OF
     , isnull(dI7.LIBELLE, 'Non renseigné') as canal_interact_origine
     , isnull(dD7.LIBELLE, 'Non renseigné') as canal_distrib_origine
     , isnull(dSR8.LIBELLE, 'Non renseigné') as sous_reseau_origine
     , isnull(dR8.LIBELLE, 'Non renseigné') as reseau_origine
     , isnull(dI7F.LIBELLE, 'Non renseigné') as canal_interact_final
     , isnull(dD7F.LIBELLE, 'Non renseigné') as canal_distrib_final
     , isnull(dSR8F.LIBELLE, 'Non renseigné') as sous_reseau_final
     , isnull(dR8F.LIBELLE, 'Non renseigné') as reseau_final
     , d1p.LIBELLE as stade_vente_prec
     , flag_sidp
  from ( select * from [dbo].[WK_STOCK_OPPORTUNITY] where flag_opport_last_state_overall = 1 and StageName in ('09', '10', '11','13') ) d
  left join cte_prec_stade p
    on d.Id_SF = p.Id_SF and d.StageName = p.StageName
  left join DIM_TEMPS d5a
    on cast(d.CloseDate as date) = d5a.Date
  left join DIM_TEMPS d5c
    on cast(d.CreatedDate as date) = d5c.Date
  left join DIM_TEMPS d5f
    on cast(d.CloseDate as date) = d5f.Date
  left join DIM_STADE_VENTE d1
    on ( d.StageName = d1.CODE_SF and d1.PARENT_ID is null /* niveau stade de vente */ 	and d.CloseDate BETWEEN d1.Validity_StartDate and d1.Validity_EndDate )
  left join DIM_STADE_VENTE d1M
    on ( d.motif_non_aboutie = d1M.CODE_SF and d.StageName = d1M.PARENT_ID /* niveau motif non aboutie */ and d.CloseDate BETWEEN d1M.Validity_StartDate and d1M.Validity_EndDate )

  left join DIM_STADE_VENTE d1p
    on ( p.prec_stade = d1p.CODE_SF and d1p.PARENT_ID is null /* niveau stade de vente */ and cast(d.CloseDate as date ) BETWEEN d1p.Validity_StartDate and d1p.Validity_EndDate )

  /* contexte de distribution d'origine*/
  left join DIM_CANAL dI7 
    on ( d.StartedChannel__c = dI7.CODE_SF and dI7.PARENT_ID is not null /* niveau canal interaction */ and d.CloseDate BETWEEN dI7.Validity_StartDate and dI7.Validity_EndDate )
  left join DIM_CANAL dD7
    on ( dI7.PARENT_ID = dD7.CODE_SF and dD7.PARENT_ID is null /* niveau canal distribution */ and d.CloseDate BETWEEN dD7.Validity_StartDate and dD7.Validity_EndDate )

  left join DIM_RESEAU dSR8
    on ( d.DistributorEntity__c = dSR8.CODE_SF and dSR8.PARENT_ID is not null /* niveau sous-réseau */ and d.CloseDate BETWEEN dSR8.Validity_StartDate and dSR8.Validity_EndDate )
  left join DIM_RESEAU dR8
    on ( d.DistributorNetwork__c = dR8.CODE_SF and dR8.PARENT_ID is null /* niveau réseau */ and d.CloseDate BETWEEN dR8.Validity_StartDate and dR8.Validity_EndDate )

  /* contexte de distribution de finalisation*/
  left join DIM_CANAL dI7F 
    on ( d.canal_fin = dI7F.CODE_SF and dI7F.PARENT_ID is not null /* niveau canal interaction */ and d.CloseDate BETWEEN dI7F.Validity_StartDate and dI7F.Validity_EndDate )
  left join DIM_CANAL dD7F
    on ( dI7F.PARENT_ID = dD7F.CODE_SF and dD7F.PARENT_ID is null /* niveau canal distribution */ and d.CloseDate BETWEEN dD7F.Validity_StartDate and dD7F.Validity_EndDate )

  left join DIM_RESEAU dSR8F
    on ( d.sous_reseau_fin = dSR8F.CODE_SF and dSR8F.PARENT_ID is not null /* niveau sous-réseau */ and d.CloseDate BETWEEN dSR8F.Validity_StartDate and dSR8F.Validity_EndDate ) 
  left join DIM_RESEAU dR8F
    on ( dSR8F.PARENT_ID  = dR8F.CODE_SF and dR8F.PARENT_ID is null /* niveau réseau */ and d.CloseDate BETWEEN dR8F.Validity_StartDate and dR8F.Validity_EndDate )

where cast(d.CloseDate as date) < cast(getdate() as date) 

;

select @nbRows = count(*) from sas_vue_flux_opport_fin;
EXEC [dbo].[PROC_SSRS_OPPORT_OUV];
END 
GO