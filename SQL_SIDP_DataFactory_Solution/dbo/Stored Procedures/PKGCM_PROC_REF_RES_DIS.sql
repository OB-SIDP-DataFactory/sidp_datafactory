﻿

/****** Object:  StoredProcedure [dbo].[PKGCM_PROC_REF_RES_DIS]    Script Date: 17/11/2017 15:46:10 ******/

-- =============================================
-- Author:		<Liliane, Huang>
-- Create date: <15/01/2017>
-- Description:	<Alimentation de la table REF_RES_DIS contenant tous les réseaux de distribution d'une personne>
-- Table input : [$(DataHubDatabaseName)].[dbo].[PV_SF_ACCOUNT]
-- Table output : REF_RES_DIS
-- =============================================

CREATE PROCEDURE [dbo].[PKGCM_PROC_REF_RES_DIS] 
(
   @date_alim DATE
    ,@nbRows int OUTPUT -- nb lignes processées
)
AS
BEGIN

/*Test
declare @date_alim DATE
set @date_alim = '2017-10-31'*/

--Purge de la table temporaire
if OBJECT_ID('tempdb..#wk') is not null
begin drop table #wk end

--Création d'une table temporaire pour parser le champ Infos distributeurs, DistributorData__c 
SELECT * INTO #wk
FROM
(
	SELECT Id_SF,
		   DistributorData__c,
		   value,
		   SUBSTRING(value,PATINDEX('%Nom%',value)+5,(PATINDEX('%Principal%',value) - (PATINDEX('%Nom%',value)+5))-2) AS nom,
		   SUBSTRING(SUBSTRING(value,PATINDEX('%Nom%',value)+5,(PATINDEX('%Principal%',value) - (PATINDEX('%Nom%',value)+5))),1,charindex('|',SUBSTRING(value,PATINDEX('%Nom%',value)+5,(PATINDEX('%Principal%',value) - (PATINDEX('%Nom%',value)+5))))-1) AS lib_nom,
		   SUBSTRING(SUBSTRING(value,PATINDEX('%Nom%',value)+5,(PATINDEX('%Principal%',value) - (PATINDEX('%Nom%',value)+5))),charindex('|',SUBSTRING(value,PATINDEX('%Nom%',value)+5,(PATINDEX('%Principal%',value) - (PATINDEX('%Nom%',value)+5))))+1,2) AS code_nom,
		   SUBSTRING(value,PATINDEX('%Principal%',value)+11,(PATINDEX('%Identifiant%',value) - (PATINDEX('%Principal%',value)+13))) AS principal,
		   SUBSTRING(value,PATINDEX('%Identifiant%',value)+13,(PATINDEX('%Score%',value) - (PATINDEX('%Identifiant%',value)+13))-2) AS identifiant,
		   SUBSTRING(value,PATINDEX('%Score%',value)+7,4) AS score
	FROM [$(DataHubDatabaseName)].dbo.PV_SF_ACCOUNT 
	CROSS APPLY ..STRING_SPLIT(DistributorData__c, '-')
	WHERE value <> ''
) r
;

--Insertion des informations dans la table REF_RES_DIS
INSERT INTO [dbo].[REF_RES_DIS] (
	DTE_ALIM,
	IDE_PERS_SF,
	COD_RES_DIS,
	LIB_RES_DIS,
	FLG_RES_DIS_PPAL,
	IDE_CLT_RES,
	RES_SCO_PREAT
)
SELECT @date_alim AS DTE_ALIM,
	Id_SF,
	code_nom,
	lib_nom,
	principal,
	identifiant,
	score
FROM #wk

		SELECT @nbRows = @@ROWCOUNT
;
END
