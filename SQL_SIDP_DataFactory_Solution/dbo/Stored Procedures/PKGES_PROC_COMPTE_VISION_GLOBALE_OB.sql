﻿CREATE PROCEDURE [dbo].[PKGES_PROC_COMPTE_VISION_GLOBALE_OB]

@Datedebut date, 
@Datefin date   

AS BEGIN

select
sum(T.nb_opportOB) as nb_opportOB,
sum(T.CanalAppliFinalOB) CanalAppliFinalOB,sum(T.CanalAppliFinalSelfcareOB) CanalAppliFinalSelfcareOB,sum(T.CanalAppliFinalCRCOB) CanalAppliFinalCRCOB,
sum(T.CanalWebFinalOB) CanalWebFinalOB,sum(T.CanalWebFinalSelfaceOB) CanalWebFinalSelfaceOB ,sum(T.CanalWebFinalCRCOB) CanalWebFinalCRCOB
from (
Select 
-- Finalisé
count(distinct Wk.Id_SF) as nb_opportOB,
case when LeadSource='10' then count(distinct Wk.Id_SF) else 0 end as  CanalAppliFinalOB,
case when LeadSource='10' and StartedChannel__c  in('10' ,'11') then count(distinct Wk.Id_SF) else 0 end as  CanalAppliFinalSelfcareOB,
case when LeadSource='10' and StartedChannel__c  in('12' ,'13') then count(distinct Wk.Id_SF) else 0 end as  CanalAppliFinalCRCOB,
case when LeadSource='11' then count(distinct Wk.Id_SF) else 0 end as  CanalWebFinalOB,
case when LeadSource='11' and StartedChannel__c  in('10' ,'11') then count(distinct Wk.Id_SF) else 0 end as  CanalWebFinalSelfaceOB,
case when LeadSource='11' and StartedChannel__c  in('12' ,'13') then count(distinct Wk.Id_SF) else 0 end as  CanalWebFinalCRCOB
from [dbo].[WK_STOCK_OPPORTUNITY] as Wk
left outer join  [$(DataHubDatabaseName)].[dbo].[PV_SF_OPPORTUNITYISDELETED] as Wkd on Wk.Id_SF= Wkd.Id_SF
where Wkd.Id_SF is null and DistributorNetwork__c='02' --Orange Bank
and CommercialOfferCode__c='OC80' and  StageName ='09'
and  cast(CreatedDate as date) between @Datedebut and @Datefin
and flag_opport_last_state = 1
and StartedChannel__c in ('10','11','12','13')
and LeadSource in ('01','02','03','10','11','12','13')
and [date_alim]=(SELECT MAX([date_alim]) FROM [dbo].[WK_STOCK_OPPORTUNITY])
group by StartedChannel__c,StageName,LeadSource,DistributorNetwork__c
) as T

END;

