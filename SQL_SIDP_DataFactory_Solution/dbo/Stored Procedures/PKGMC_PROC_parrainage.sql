﻿CREATE PROCEDURE [dbo].[PKGMC_PROC_parrainage] @nbRows int OUTPUT
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    -- Insert statements for procedure here
INSERT INTO [dbo].[SAS_Vue_MC_Parrainage]
           ([Date_observation]
           ,[Nb_Filleul]
           ,[Nb_Parrain])

select cast (GETDATE() as date) as Date_observation,
	   Nb_Filleul,
	   COUNT(distinct IdSF_Parrain) as Nb_Parrain
from ( 	
	
	SELECT IdSF_Parrain,
	       COUNT(distinct IdSF_Filleul) as Nb_Filleul	
	from (	

		SELECT distinct
			  (case when PersonRole1__c = '09' then acc1.IDE_CLT_PHY when [PersonRole2__c] = '09' then acc2.IDE_CLT_PHY end) as IdSF_Parrain
			 ,(case when PersonRole1__c = '10' then acc1.IDE_CLT_PHY when [PersonRole2__c] = '10' then acc2.IDE_CLT_PHY end) as IdSF_Filleul
		  FROM [$(DataHubDatabaseName)].[dbo].[PV_SF_ACCOUNTLINK] acclk
		  INNER JOIN [$(DataHubDatabaseName)].[exposition].[PV_SF_ACCOUNT] acc1 on acclk.Person1__c = acc1.IDE_PERS_SF
		  INNER JOIN [$(DataHubDatabaseName)].[exposition].[PV_SF_OPPORTUNITY] opp1 on opp1.IDE_PERS_SF = acc1.IDE_PERS_SF
		  INNER JOIN [$(DataHubDatabaseName)].[exposition].[PV_SF_ACCOUNT] acc2 on acclk.Person2__c = acc2.IDE_PERS_SF
		  INNER JOIN [$(DataHubDatabaseName)].[exposition].[PV_SF_OPPORTUNITY] opp2 on opp2.IDE_PERS_SF = acc2.IDE_PERS_SF
  
		  WHERE LinkType__c = '06'
		  and PersonRole1__c in ('09','10')
		  and PersonRole2__c in ('09','10')
		  and acc1.DTE_FIN_VAL = '9999-12-31 00:00:00.0000000'
		  and acc2.DTE_FIN_VAL = '9999-12-31 00:00:00.0000000'
		  and (case when PersonRole1__c = '10' then opp1.LIB_STADE_VENTE when [PersonRole2__c] = '10' then opp2.LIB_STADE_VENTE end) in ('Compte ouvert','Dossier validé')
		  and (case when PersonRole1__c = '10' and opp1.LIB_RES_DIS = 'Orange Bank' and opp1.LIB_CAN_INT_ORI in ('Appli OB','Web OB') then 'OB_full_selfcare' when PersonRole2__c = '10' and opp2.LIB_RES_DIS = 'Orange Bank' and opp2.LIB_CAN_INT_ORI in ('Appli OB','Web OB') then 'OB_full_selfcare'
      			  when PersonRole1__c = '10' and opp1.LIB_RES_DIS = 'Orange Bank' and opp1.LIB_CAN_INT_ORI in ('Appel entrant CRC', 'Appel sortant CRC') then 'OB_CRC' when PersonRole2__c = '10' and opp2.LIB_RES_DIS = 'Orange Bank' and opp2.LIB_CAN_INT_ORI in ('Appel entrant CRC', 'Appel sortant CRC') then 'OB_CRC'
				  when PersonRole1__c = '10' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_DOS_COMP = '1' then 'Souscription boutique' when PersonRole2__c = '10' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_DOS_COMP = '1' then 'Souscription boutique'
				  when PersonRole1__c = '10' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_DOS_COMP <> '1' and opp1.NUM_PROC_SOUS is not null then 'Repli Selfcare' when PersonRole2__c = '10' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_DOS_COMP <> '1' and opp2.NUM_PROC_SOUS is not null then 'Repli Selfcare'
				  when PersonRole1__c = '10' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_IND = 'OUI' and opp1.NUM_PROC_SOUS is null and opp1.LIB_ENT_DIS in ('AD', 'GDT')  then 'Indication boutique' when PersonRole2__c = '10' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_IND = 'OUI' and opp2.NUM_PROC_SOUS is null and opp2.LIB_ENT_DIS in ('AD', 'GDT')  then 'Indication boutique'
				  when PersonRole1__c = '10' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_IND = 'OUI' and opp1.NUM_PROC_SOUS is null and opp1.LIB_ENT_DIS in ('OF-WEB')  then 'Indication Digitale' when PersonRole2__c = '10' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_IND = 'OUI' and opp2.NUM_PROC_SOUS is null and opp2.LIB_ENT_DIS in ('OF-WEB') then 'Indication Digitale'
				  when PersonRole1__c = '10' and opp1.LIB_RES_DIS = 'Orange France' and opp1.LIB_CAN_INT_ORI = 'Web OB' and opp1.LIB_ENT_DIS in ('OF-WEB')  then 'Renvoi trafic' when PersonRole2__c = '10' and opp2.LIB_RES_DIS = 'Orange France' and opp2.LIB_CAN_INT_ORI = 'Web OB' and opp2.LIB_ENT_DIS in ('OF-WEB')  then 'Renvoi trafic'
			end) is not null
		  and (case when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange Bank' and opp1.LIB_CAN_INT_ORI in ('Appli OB','Web OB') then 'OB_full_selfcare' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange Bank' and opp2.LIB_CAN_INT_ORI in ('Appli OB','Web OB') then 'OB_full_selfcare'
      			  when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange Bank' and opp1.LIB_CAN_INT_ORI in ('Appel entrant CRC', 'Appel sortant CRC') then 'OB_CRC' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange Bank' and opp2.LIB_CAN_INT_ORI in ('Appel entrant CRC', 'Appel sortant CRC') then 'OB_CRC'
				  when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_DOS_COMP = '1' then 'Souscription boutique' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_DOS_COMP = '1' then 'Souscription boutique'
				  when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_DOS_COMP <> '1' and opp1.NUM_PROC_SOUS is not null then 'Repli Selfcare' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_DOS_COMP <> '1' and opp2.NUM_PROC_SOUS is not null then 'Repli Selfcare'
				  when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_IND = 'OUI' and opp1.NUM_PROC_SOUS is null and opp1.LIB_ENT_DIS in ('AD', 'GDT')  then 'Indication boutique' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_IND = 'OUI' and opp2.NUM_PROC_SOUS is null and opp2.LIB_ENT_DIS in ('AD', 'GDT')  then 'Indication boutique'
				  when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_IND = 'OUI' and opp1.NUM_PROC_SOUS is null and opp1.LIB_ENT_DIS in ('OF-WEB')  then 'Indication Digitale' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_IND = 'OUI' and opp2.NUM_PROC_SOUS is null and opp2.LIB_ENT_DIS in ('OF-WEB') then 'Indication Digitale'
				  when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange France' and opp1.LIB_CAN_INT_ORI = 'Web OB' and opp1.LIB_ENT_DIS in ('OF-WEB')  then 'Renvoi trafic' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange France' and opp2.LIB_CAN_INT_ORI = 'Web OB' and opp2.LIB_ENT_DIS in ('OF-WEB')  then 'Renvoi trafic'
			end) is not null
		  and (case when PersonRole1__c = '09' then opp1.FLG_GAGN when [PersonRole2__c] = '09' then opp2.FLG_GAGN end) = 'true'
		  and opp1.LABEL_OFF_COM = 'Compte bancaire' 
		  and opp2.LABEL_OFF_COM = 'Compte bancaire'
		  and (case when PersonRole1__c = '10' then opp1.EVE_ORI when [PersonRole2__c] = '10' then opp2.EVE_ORI end) = '08'

			)a
	group by IdSF_Parrain
	)b
group by Nb_Filleul
order by Nb_Filleul

	
	SELECT @nbRows = COUNT(*) FROM [SAS_Vue_MC_Parrainage];

END
GO
