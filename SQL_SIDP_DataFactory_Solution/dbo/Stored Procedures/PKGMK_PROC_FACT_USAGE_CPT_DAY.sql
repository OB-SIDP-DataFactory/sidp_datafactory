﻿CREATE PROC [dbo].[PKGMK_PROC_FACT_USAGE_CPT_DAY] 
    @DATE_ALIM  DATE, 
    @nbRows     INT OUTPUT  -- nb lignes processées
AS  
BEGIN 

IF @DATE_ALIM IS NULL 
  SET @DATE_ALIM = GETDATE();

/* ========== CREATION ET INITIALISATION DE LA TABLE TMP #REF_TYP_MVT ========== */
IF OBJECT_ID('tempdb..#REF_TYP_MVT') IS NOT NULL
BEGIN DROP TABLE #REF_TYP_MVT END

CREATE TABLE #REF_TYP_MVT
(
  C_SOUS_TYP_MVT             INT,
  SOUS_TYP_MVT               VARCHAR(100),
  TYP_MVT                    VARCHAR(100), 
  C_FLAG_INITIATIVE_CLIENT   INT,
  FLAG_INITIATIVE_CLIENT     VARCHAR(100)
)

INSERT INTO #REF_TYP_MVT VALUES (1,'Virement permanent','Virement',1,'OUI');
INSERT INTO #REF_TYP_MVT VALUES (2,'Virement interne ponctuel','Virement',1,'OUI');
INSERT INTO #REF_TYP_MVT VALUES (3,'Virement externe','Virement',1,'OUI');
INSERT INTO #REF_TYP_MVT VALUES (4,'Virement SMS interne','Virement',1,'OUI');
INSERT INTO #REF_TYP_MVT VALUES (5,'Virement SMS externe','Virement',1,'OUI');
INSERT INTO #REF_TYP_MVT VALUES (6,'Rejet Virement interne ponctuel','Rejet virement',-1,'DEDUIRE');
INSERT INTO #REF_TYP_MVT VALUES (7,'Rejet Virement externe','Rejet virement',-1,'DEDUIRE');
INSERT INTO #REF_TYP_MVT VALUES (8,'Rejet Virement SMS interne','Rejet virement',-1,'DEDUIRE');
INSERT INTO #REF_TYP_MVT VALUES (9,'Rejet Virement SMS externe','Rejet virement',-1,'DEDUIRE');
INSERT INTO #REF_TYP_MVT VALUES (10,'Paiement CB Immédiat Achat','Paiement CB',1,'OUI');
INSERT INTO #REF_TYP_MVT VALUES (11,'Paiement CB Immédiat Retrait','Paiement CB',1,'OUI');
INSERT INTO #REF_TYP_MVT VALUES (12,'Paiement CB Différé (compte de passage)','Paiement CB',1,'OUI');
INSERT INTO #REF_TYP_MVT VALUES (13,'Paiement CB Différé (compte)','Paiement CB',1,'OUI');
INSERT INTO #REF_TYP_MVT VALUES (14,'Rembouresement Paiement CB Immédiat Achat','Remboursement Paiement CB',-1,'DEDUIRE');
INSERT INTO #REF_TYP_MVT VALUES (15,'Rembouresement Paiement CB Différé (compte de passage)','Remboursement Paiement CB',-1,'DEDUIRE');
INSERT INTO #REF_TYP_MVT VALUES (16,'Paiement mobile','Paiement mobile',1,'OUI');
INSERT INTO #REF_TYP_MVT VALUES (17,'Chèques émis','Chèque émis',1,'OUI');
INSERT INTO #REF_TYP_MVT VALUES (18,'Rejet Chèques émis','Rejet chèque émis',-1,'DEDUIRE');
INSERT INTO #REF_TYP_MVT VALUES (19,'Chèques encaissés','Chèque encaissé',1,'OUI');
INSERT INTO #REF_TYP_MVT VALUES (20,'Rejet chèques encaissés','Rejet chèque encaissé',-1,'DEDUIRE');
INSERT INTO #REF_TYP_MVT VALUES (21,'Prélèvement','Prélevement',1,'OUI');
INSERT INTO #REF_TYP_MVT VALUES (22,'Rejet prélèvement','Rejet prélevement',-1,'DEDUIRE');
INSERT INTO #REF_TYP_MVT VALUES (23,'Premier versement CSL','Premier versement',1,'OUI');
INSERT INTO #REF_TYP_MVT VALUES (24,'Premier versement CAV','Premier versement',0,'AUTRES');
INSERT INTO #REF_TYP_MVT VALUES (25,'Commissions interventions','Opération facturée',0,'AUTRES');
INSERT INTO #REF_TYP_MVT VALUES (26,'Frais inactivité','Opération facturée',0,'AUTRES');
INSERT INTO #REF_TYP_MVT VALUES (27,'Prime de bienvenue','Prime de bienvenue',0,'AUTRES');
INSERT INTO #REF_TYP_MVT VALUES (28,'Prime collaborateur','Prime collaborateur',0,'AUTRES');
INSERT INTO #REF_TYP_MVT VALUES (29,'Prime client orange','Prime client orange',0,'AUTRES');
INSERT INTO #REF_TYP_MVT VALUES (30,'Intérêts versés','Intérêts versés',0,'AUTRES');
INSERT INTO #REF_TYP_MVT VALUES (-1,'Autres','Autres',0,'AUTRES');  
  
  
/* ------------------ Step 1 ----------------- */
-- calcul de la période de chargement

IF OBJECT_ID('tempdb..#WK_TEMPS_ACCOUNT') IS NOT NULL
BEGIN DROP TABLE #WK_TEMPS_ACCOUNT END

SELECT StandardDate
  INTO #WK_TEMPS_ACCOUNT
  FROM [dbo].DIM_TEMPS
 WHERE StandardDate = DATEADD(dd,-1,@DATE_ALIM)
 
 
/* ----------------- Step 2 ----------------- */
-- Selection des comptes cibles utilisés pour filtrer les traitements sur la table des mouvements
-- Table des compte

IF OBJECT_ID('tempdb..#WK_COMPTES_N') IS NOT NULL
BEGIN DROP TABLE #WK_COMPTES_N END

SELECT DISTINCT StandardDate
               ,DWHCPTCOM AS NUM_CPT_SAB
			   ,DWHCPTPPAL AS [NUM_CLI_SAB]
			   ,CASE WHEN  DWHCPTRUB = '251180' THEN  'CAV' ELSE 'CSL' END AS FLAG_CPT 
INTO #WK_COMPTES_N
FROM [$(DataHubDatabaseName)].[dbo].[VW_PV_SA_Q_COMPTE] CO CROSS JOIN #WK_TEMPS_ACCOUNT t
WHERE CAST(Validity_StartDate AS DATE) <= t.StandardDate AND t.StandardDate < CAST(Validity_EndDate AS date)
AND DWHCPTRUB IN ('251180','254181','254111')

-- Table des comptes de passage
IF OBJECT_ID('tempdb..#WK_COMPTES_PASSAGE') IS NOT NULL
BEGIN DROP TABLE #WK_COMPTES_PASSAGE END

SELECT StandardDate,NUM_CPT_SAB,[NUM_CLI_SAB],CONCAT('203130EUR',[NUM_CLI_SAB]) AS Compte_Passage
INTO #WK_COMPTES_PASSAGE
FROM #WK_COMPTES_N
WHERE FLAG_CPT = 'CAV' ;

-- Concaténation des comptes et des comptes de passage
IF OBJECT_ID('tempdb..#WK_COMPTES') IS NOT NULL
BEGIN DROP TABLE #WK_COMPTES END;

WITH cte_res AS (
SELECT StandardDate,NUM_CPT_SAB, NUM_CPT_SAB AS Num_Cpt_OR_Num_Cpt_Pssg,[NUM_CLI_SAB], 0 AS FLAG FROM #WK_COMPTES_N
UNION ALL
SELECT StandardDate,NUM_CPT_SAB,Compte_Passage AS Num_Cpt_OR_Num_Cpt_Pssg,[NUM_CLI_SAB] , 1 AS FLAG  FROM #WK_COMPTES_PASSAGE
)
SELECT * into #WK_COMPTES FROM cte_res;

--- INIT : Recup des mouvements ---
IF OBJECT_ID('tempdb..#RES_MOUVEMENT_INIT') IS NOT NULL
BEGIN DROP TABLE #RES_MOUVEMENT_INIT END

SELECT REQ_MVT.NUM_CPT_SAB,
       REQ_MVT.NUM_CPT,
       REQ_MVT.StandardDate,
       REQ_MVT.DTE_OPE,
	   REQ_MVT.DTE_OPE_EOM AS DTE_OPE_EOM,
       REQ_MVT.COD_OPE,
       REQ_MVT.COD_ANA,	
       REQ_MVT.LIB_MVT_3,
       REQ_MVT.DON_ANA,
       REQ_MVT.COD_EVE,
	   REQ_MVT.MTT_EUR,
	   REQ_MVT.NUM_OPE,
	   REQ_MVT.NUM_PIE,
       --LEAD(REQ_MVT.DTE_OPE, 1, '9999-12-31') OVER (PARTITION BY REQ_MVT.NUM_CPT ORDER BY REQ_MVT.DTE_OPE) AS NEXT_DTE_OPE, -- à utiliser pour identifier la dernière opération des jours précédents	
	   REQ_MVT.FLG_OPE_PIE,
	   --ROW_NUMBER() OVER (PARTITION BY REQ_MVT.NUM_CPT_SAB ORDER BY REQ_MVT.DTE_OPE DESC) AS FLG_DER_OP,
       CASE 
           -- VIREMENTS
           WHEN REQ_MVT.COD_OPE = 'ERE' AND REQ_MVT.COD_EVE = 'VER' AND REQ_MVT.BIC_CTP = '' THEN  1
           WHEN REQ_MVT.COD_OPE IN ('AV0', 'A0V', 'R0V') AND REQ_MVT.NAT_OPE NOT IN ('PUE','PUI') AND (REQ_MVT.BIC_CTP = '' OR REQ_MVT.BIC_CTP LIKE 'GPBA%') THEN  2
           WHEN REQ_MVT.COD_OPE IN ('AV0', 'A0V', 'R0V') AND REQ_MVT.NAT_OPE NOT IN ('PUE','PUI') AND REQ_MVT.BIC_CTP != '' AND REQ_MVT.BIC_CTP NOT LIKE 'GPBA%' THEN  3
           WHEN REQ_MVT.COD_OPE IN ('AV0', 'A0V', 'R0V') AND REQ_MVT.NAT_OPE = 'PUI' THEN  4
           WHEN REQ_MVT.COD_OPE IN ('AV0', 'A0V', 'R0V') AND REQ_MVT.NAT_OPE = 'PUE' THEN  5    
           -- REJET VIREMENTS
           WHEN REQ_MVT.COD_OPE IN ('A1V', 'A2V', 'R1V','R2V') AND REQ_MVT.NAT_OPE NOT IN ('PUE','PUI') AND (REQ_MVT.BIC_CTP = '' OR REQ_MVT.BIC_CTP LIKE 'GPBA%') THEN  6
           WHEN REQ_MVT.COD_OPE IN ('A1V', 'A2V', 'R1V','R2V') AND REQ_MVT.NAT_OPE NOT IN ('PUE','PUI') AND REQ_MVT.BIC_CTP != '' AND REQ_MVT.BIC_CTP NOT LIKE 'GPBA%' THEN  7
           WHEN REQ_MVT.COD_OPE IN ('A1V', 'A2V', 'R1V','R2V') AND REQ_MVT.NAT_OPE = 'PUI' THEN  8
           WHEN REQ_MVT.COD_OPE IN ('A1V', 'A2V', 'R1V','R2V') AND REQ_MVT.NAT_OPE = 'PUE' THEN  9    
           -- PAIEMENT CB
           WHEN REQ_MVT.COD_OPE = 'CTB' AND REQ_MVT.COD_EVE = 'IMM' AND REQ_MVT.COD_ANA IN ('CTBA37','CTBA50') AND LEN(REQ_MVT.LIB_MVT_3) > 9 THEN  10
           WHEN REQ_MVT.COD_OPE = 'CTB' AND REQ_MVT.COD_EVE = 'IMM' AND REQ_MVT.COD_ANA IN ('CTBR47','CTBR90') AND LEN(REQ_MVT.LIB_MVT_3) > 9 THEN  11
           WHEN REQ_MVT.COD_OPE = 'CTB' AND REQ_MVT.COD_EVE = 'IMM' AND REQ_MVT.NUM_CPT LIKE '203130EUR%' /*AND LEN(REQ_MVT.LIB_MVT_3) > 9*/ THEN  12
           WHEN REQ_MVT.COD_OPE = 'CTB' AND REQ_MVT.COD_EVE = 'DIF' AND LEN(REQ_MVT.LIB_MVT_3) > 9 THEN  13
           -- REMBOURSEMENT PAIEMENT CB
           WHEN REQ_MVT.COD_OPE IN ('CTB') AND REQ_MVT.COD_ANA IN ('CTBA52','CTBA45') THEN  14
           WHEN REQ_MVT.COD_OPE IN ('CTB') AND REQ_MVT.COD_ANA IN ('CTBD52','CTBD45') THEN  15	
              --- Paiement mobile
           WHEN REQ_MVT.COD_OPE IN ('CTB') AND LEN(REQ_MVT.LIB_MVT_3) = 9 THEN  16	
              -- CHEQUES EMIS
           WHEN REQ_MVT.COD_OPE IN ('RI0') THEN  17	
              -- REJET CHEQUES EMIS    
           WHEN REQ_MVT.COD_OPE IN ('RI1') THEN  18	
              -- CHEQUES ENCAISSES
           WHEN REQ_MVT.COD_OPE IN ('AI0') THEN  19
              -- REJET CHEQUES ENCAISSES
           WHEN REQ_MVT.COD_OPE IN ('AI1') THEN  20    
           -- PRELEVEMENTS
           WHEN REQ_MVT.COD_OPE IN ('A0P','R0P') THEN  21
           -- REJET PRELEVEMENTS    
           WHEN REQ_MVT.COD_OPE IN ('A1P','A2P','R1P','R2P') THEN  22    
           -- PREMIER VERSEMENT
           WHEN REQ_MVT.COD_OPE IN ('ERE') AND REQ_MVT.COD_EVE = 'OUV' AND REQ_MVT.COD_ANA = 'VEREPA'  THEN  23
           WHEN REQ_MVT.COD_OPE IN ('A0V') AND REQ_MVT.COD_EVE = 'OUI' THEN  24
              
           /*----------------------- A REVOIR-----------------------------*/
           WHEN REQ_MVT.COD_OPE = 'FAC' THEN  25
           WHEN REQ_MVT.COD_OPE = 'FAC' AND REQ_MVT.COD_EVE IN ('FAC')  THEN  26
           --Fin Opérations facturées
            --REQ_MVT.COD_OPE IN ('FAC') and REQ_MVT.COD_EVE IN ('FAC') and SUBSTRING(DON_ANA,37,6)='FBAG80'
           /*-------------------------------------------------------------*/
           
              -- PRIME DE BIENVENUE
           WHEN REQ_MVT.COD_OPE IN ('*OF') AND REQ_MVT.COD_EVE = 'PBC' THEN  27
              -- PRIME COLLABORATEUR                  = 
           WHEN REQ_MVT.COD_OPE IN ('*OF') AND REQ_MVT.COD_EVE = 'PBO' THEN  28
           -- PRIME CLIENT ORANGE                  = 
           WHEN REQ_MVT.COD_OPE IN ('*OF') AND REQ_MVT.COD_EVE = 'POF' THEN  29
           -- INTERETS VERSES                      = 
           WHEN REQ_MVT.COD_OPE IN ('ERE') AND REQ_MVT.COD_EVE = 'ECH' AND REQ_MVT.COD_ANA IN ('INTERE')  THEN  30  -- A VERIFIER
           ELSE -1		   
       END AS C_SOUS_TYP_MVT 
  INTO #RES_MOUVEMENT_INIT		
  FROM (SELECT MO.NUM_CPT,MO.DTE_OPE,MO.NUM_PIE,MO.COD_OPE,MO.DON_ANA,MO.COD_EVE,MO.BIC_CTP,MO.NAT_OPE,MO.LIB_MVT_3,MO.COD_ANA,CO.NUM_CPT_SAB,CO.Num_Cpt_OR_Num_Cpt_Pssg
              , EOMONTH(MO.DTE_OPE) AS DTE_OPE_EOM		
              , ROW_NUMBER() OVER (PARTITION BY MO.NUM_CPT ORDER BY MO.DTE_OPE DESC, MO.NUM_PIE) AS FLG_OPE_PIE -- flag pour identifier la dernière opération de chaque jours
			  , TP.StandardDate, MTT_EUR, NUM_OPE
           FROM [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_MOUVEMENT] MO		
		   JOIN #WK_COMPTES CO ON CO.Num_Cpt_OR_Num_Cpt_Pssg = MO.NUM_CPT
		   JOIN #WK_TEMPS_ACCOUNT TP ON MO.DTE_OPE <= TP.StandardDate
       ) REQ_MVT
  --WHERE REQ_MVT.FLG_OPE_PIE = 1		
/******************* Step 3 *******************/
--Calcul des flags + pré calcul des indicateurs
IF OBJECT_ID('tempdb..#RES_MOUVEMENT') IS NOT NULL
BEGIN DROP TABLE #RES_MOUVEMENT END

SELECT MVT.NUM_CPT_SAB,
       MVT.DTE_OPE,
       MVT.MTT_EUR,
       MVT.COD_OPE,
       MVT.COD_EVE,
       MVT.COD_ANA,
       MVT.NUM_OPE,
       MVT.NUM_PIE,
       MVT.LIB_MVT_3,
	   MVT.C_SOUS_TYP_MVT,
       REF_TYP_MVT.SOUS_TYP_MVT,
	   REF_TYP_MVT.TYP_MVT,
	   REF_TYP_MVT.FLAG_INITIATIVE_CLIENT,
	   CASE 
           WHEN MVT.MTT_EUR >= 0 THEN 1 
           ELSE 0 
       END AS FLAG_MTT_OPT ,
       CASE
         WHEN MVT.C_SOUS_TYP_MVT IN (1,2,3,4,5,10,11,12,13,16,18,19,21) THEN
       	   CASE 
       	     WHEN MVT.MTT_EUR >= 0 THEN 'Debit'
       	     WHEN MVT.MTT_EUR < 0 THEN 'Credit'
       	     ELSE 'Autre'
       	   END
         WHEN MVT.C_SOUS_TYP_MVT IN (6,7,8,9,17,20,22) THEN
       	   CASE 
       	     WHEN MVT.MTT_EUR >= 0 THEN 'Credit'
       	     WHEN MVT.MTT_EUR < 0 THEN 'Debit'
       	     ELSE 'Autre'
       	   END
         ELSE 'Autre'
       END as FLAG_OP_DEB_CRE
INTO #RES_MOUVEMENT
FROM #RES_MOUVEMENT_INIT MVT
LEFT JOIN #REF_TYP_MVT REF_TYP_MVT
  ON MVT.C_SOUS_TYP_MVT = REF_TYP_MVT.C_SOUS_TYP_MVT
WHERE MVT.DTE_OPE = MVT.StandardDate  
  

/******************* Step 4 *******************/
--Pré Calcul des premières opérations 
IF OBJECT_ID('tempdb..#RES_MOUVEMENT_2') IS NOT NULL
BEGIN DROP TABLE #RES_MOUVEMENT_2  END

SELECT NUM_CPT_SAB as NUM_CPT,
	   MIN(CASE WHEN C_SOUS_TYP_MVT = 16 THEN DTE_OPE ELSE NULL END) AS DT_PRE_UTI_PM, 
	   --MIN(CASE WHEN COD_OPE = 'CTB' THEN DTE_OPE ELSE NULL END) AS DT_PRE_UTI_CB,
	   MIN(CASE WHEN C_SOUS_TYP_MVT IN (10,11,12,13) THEN DTE_OPE ELSE NULL END) AS DT_PRE_UTI_CB,
       MIN(CASE WHEN MTT_EUR > 0 THEN DTE_OPE ELSE NULL END) AS DT_PRE_OPE_DEB,
       MIN(CASE WHEN MTT_EUR < 0 THEN DTE_OPE ELSE NULL END) AS DT_PRE_OPE_CRE,
       MIN(CASE WHEN C_SOUS_TYP_MVT = 27 THEN DTE_OPE ELSE NULL END) AS DT_VRS_PRI_BVN,
	   MAX(CASE WHEN C_SOUS_TYP_MVT = 16 THEN DTE_OPE ELSE NULL END) AS DT_DER_UTI_PM, 
       --MAX(CASE WHEN COD_OPE = 'CTB' THEN DTE_OPE ELSE NULL END) AS DT_DER_UTI_CB,
       MAX(CASE WHEN C_SOUS_TYP_MVT IN (10,11,12,13) THEN DTE_OPE ELSE NULL END) AS DT_DER_UTI_CB,
	   MAX(CASE WHEN MTT_EUR > 0 THEN DTE_OPE ELSE NULL END) AS DT_DER_OPE_DEB,
       MAX(CASE WHEN MTT_EUR < 0 THEN DTE_OPE ELSE NULL END) AS DT_DER_OPE_CRE
  INTO #RES_MOUVEMENT_2
  FROM #RES_MOUVEMENT_INIT
GROUP BY NUM_CPT_SAB
/******************* Step 5 *******************/
--Pré Calcul de la dernière opération + libellé
IF OBJECT_ID('tempdb..#RES_MOUVEMENT_3') IS NOT NULL
BEGIN DROP TABLE #RES_MOUVEMENT_3  END

SELECT MVT.NUM_CPT_SAB as NUM_CPT,
       MVT.DTE_OPE as DT_DER_MVT,
       MVT.DTE_OPE_EOM as DTE_OPE_EOM,
       MVT.COD_OPE,
       MVT.COD_ANA,
       MVT.LIB_MVT_3,
       MVT.DON_ANA,
       MVT.COD_EVE,
       REF_TYP_MVT.SOUS_TYP_MVT AS SOUS_TYP_DER_MVT,
       REF_TYP_MVT.TYP_MVT AS TYP_DER_MVT,	 
       LEAD(MVT.DTE_OPE, 1, '9999-12-31') OVER (PARTITION BY MVT.NUM_CPT ORDER BY MVT.DTE_OPE) AS NEXT_DTE_OPE, -- à utiliser pour identifier la dernière opération des jours précédents	
	   MVT.FLG_OPE_PIE,
	   ROW_NUMBER() OVER (PARTITION BY MVT.NUM_CPT_SAB ORDER BY MVT.DTE_OPE DESC) AS FLG_DER_OP   
  INTO #RES_MOUVEMENT_3 		
  FROM #RES_MOUVEMENT_INIT MVT
  LEFT JOIN #REF_TYP_MVT REF_TYP_MVT
  ON MVT.C_SOUS_TYP_MVT = REF_TYP_MVT.C_SOUS_TYP_MVT
  WHERE MVT.FLG_OPE_PIE = 1	
  

 /******************* Step 5 *******************/
 --#res_Mouvement   : Calcul des flags + pré calcul des indicateurs
 --#res_Mouvement_2 : Pré Calcul des premières opérations 
 --#res_Mouvement_3 : Pré Calcul de la dernière opération + libellé
 --Calcul des indicateurs
 --Nettoyage en cas de rechaargement 
 DELETE FROM [dbo].[T_FACT_MKT_USAGE_CPT_DAY] WHERE [DTE_TRAITEMENT]  IN (SELECT StandardDate FROM #WK_TEMPS_ACCOUNT )

INSERT INTO [dbo].[T_FACT_MKT_USAGE_CPT_DAY]
  (DTE_TRAITEMENT,
   DTE_OPE,
   NUM_CPT_SAB,  
   TYP_MVT, 
   SOUS_TYP_MVT,
   NOMBRE_OP,
   MONTANT_OP,
   FLAG_MTT_OPT,
   FLAG_INITIATIVE_CLIENT,
   FLAG_OP_DEB_CRE,
   DT_DER_MVT,
   TYP_DER_MVT,
   SOUS_TYP_DER_MVT,
   DT_PRE_UTI_PM,
   DT_PRE_UTI_CB,
   DT_PRE_OPE_DEB,
   DT_PRE_OPE_CRE,
   DT_DER_UTI_PM,
   DT_DER_UTI_CB,
   DT_DER_OPE_DEB,
   DT_DER_OPE_CRE,
   DT_VRS_PRI_BVN
   )
SELECT c.StandardDate AS [DTE_TRAITEMENT],
       c.DTE_OPE,
       c.NUM_CPT_SAB,
       m.TYP_MVT,
       m.SOUS_TYP_MVT,
	   COUNT(m.NUM_OPE) AS NOMBRE_OP,
       SUM(m.MTT_EUR) AS MONTANT_OP,
       m.FLAG_MTT_OPT,
       m.FLAG_INITIATIVE_CLIENT,
       m.FLAG_OP_DEB_CRE,
       MAX(m3.DT_DER_MVT) AS [DT_DER_MVT],
       MAX(m3.TYP_DER_MVT) AS [TYP_DER_MVT],
       MAX(m3.SOUS_TYP_DER_MVT) as [SOUS_TYP_DER_MVT],
       MIN(CASE WHEN m2.DT_PRE_UTI_PM <= c.DTE_OPE THEN m2.DT_PRE_UTI_PM ELSE NULL END) AS [DT_PRE_UTI_PM],
       MIN(CASE WHEN m2.DT_PRE_UTI_CB <= c.DTE_OPE THEN m2.DT_PRE_UTI_CB ELSE NULL END) AS [DT_PRE_UTI_CB],   
       MIN(CASE WHEN m2.DT_PRE_OPE_DEB <= c.DTE_OPE THEN m2.DT_PRE_OPE_DEB ELSE NULL END) AS [DT_PRE_OPE_DEB],
       MIN(CASE WHEN m2.DT_PRE_OPE_CRE <= c.DTE_OPE THEN m2.DT_PRE_OPE_CRE ELSE NULL END) AS [DT_PRE_OPE_CRE],
       MAX(CASE WHEN m2.DT_DER_UTI_PM <= c.DTE_OPE THEN m2.DT_DER_UTI_PM ELSE NULL END) AS [DT_DER_UTI_PM],
       MAX(CASE WHEN m2.DT_DER_UTI_CB <= c.DTE_OPE THEN m2.DT_DER_UTI_CB ELSE NULL END) AS [DT_DER_UTI_CB],
       MAX(CASE WHEN m2.DT_DER_OPE_DEB <= c.DTE_OPE THEN m2.DT_DER_OPE_DEB ELSE NULL END) AS [DT_DER_OPE_DEB],
       MAX(CASE WHEN m2.DT_DER_OPE_CRE <= c.DTE_OPE THEN m2.DT_DER_OPE_CRE ELSE NULL END) AS [DT_DER_OPE_CRE],
       MIN(CASE WHEN m2.DT_VRS_PRI_BVN <= c.DTE_OPE THEN m2.DT_VRS_PRI_BVN ELSE NULL END) AS [DT_VRS_PRI_BVN]
       
FROM (SELECT DISTINCT c.StandardDate, c.NUM_CPT_SAB,t.StandardDate AS DTE_OPE
      FROM #WK_COMPTES c
      JOIN #WK_TEMPS_ACCOUNT t ON c.StandardDate = t.StandardDate) c
LEFT JOIN #RES_MOUVEMENT m
  ON c.NUM_CPT_SAB = m.NUM_CPT_SAB AND m.DTE_OPE = c.DTE_OPE --and c.Num_Cpt_OR_Num_Cpt_Pssg = m.NUM_CPT_SAB  
LEFT JOIN #RES_MOUVEMENT_2 m2
  ON m2.NUM_CPT = c.NUM_CPT_SAB
--left join #res_Mouvement_d md
--  on md.NUM_CPT = c.NUM_CPT_SAB
LEFT JOIN #RES_MOUVEMENT_3 m3
  ON m3.NUM_CPT = c.NUM_CPT_SAB AND ( m3.DT_DER_MVT = c.DTE_OPE or ( m3.DT_DER_MVT < c.DTE_OPE AND m3.NEXT_DTE_OPE > c.DTE_OPE ) ) AND m3.FLG_DER_OP = 1 --*-* Pour la récupérer la dernière op de la journée
GROUP BY c.StandardDate,c.DTE_OPE,c.NUM_CPT_SAB --,c.Num_Cpt_OR_Num_Cpt_Pssg 
     -- m.C_SOUS_TYP_MVT
     , m.TYP_MVT 
	 , m.SOUS_TYP_MVT  
	 , m.FLAG_MTT_OPT
	 , m.FLAG_INITIATIVE_CLIENT
	 , m.FLAG_OP_DEB_CRE    

SELECT @nbRows = @@ROWCOUNT

END