﻿
CREATE PROC [dbo].[PKGCR_PROC_PLAN_FINANCEMENT] (@date_alim DATE, @date_derniere_alim DATE, @nbRows int OUTPUT)
AS  
BEGIN 

-- =============================================
-- Description:	Alimentation de la table CRE_PLAN_FINANCEMENT contenant les équipements Crédit
-- Tables input : PV_FE_CONSUMERLOANFINANCIALPLAN
--				  PV_FE_SUBSCRIBEDCONSUMERLOAN
--				  PV_FE_SUBSCRIBEDPRODUCT
--				  PV_FE_ENROLMENTFOLDER
-- Table output : CRE_PLAN_FINANCEMENT				
-- =============================================
-- Suppression des données pour la gestion de reprise de chargement
DELETE FROM [dbo].[CRE_PLAN_FINANCEMENT]
WHERE [DTE_ALIM] >= @date_derniere_alim

INSERT INTO CRE_PLAN_FINANCEMENT
(
	   [IDE_FE_PLAN_FIN_PROD_CRE]
      ,[IDE_OPPRT_SF]
      ,[FRAIS_INSCR]
      ,[IDE_TYP_CONT_TRA_EMP]
      ,[LIST_GUA_ASS_EMP]
      ,[TX_RIS_ASS_1_EMP]
      ,[TX_RIS_ASS_2_EMP]
      ,[TYP_RIS_ASS_1_EMP]
      ,[TYP_RIS_ASS_2_EMP]
      ,[REV_NET_MM_EMP]
      ,[IDE_TYP_CONT_TRA_COEMP]
      ,[LIST_GUA_ASS_COEMP]
      ,[REV_NET_MM_COEMP]
      ,[TX_RIS_ASS_1_COEMP]
      ,[TX_RIS_ASS_2_COEMP]
      ,[TYP_RIS_ASS_1_COEMP]
      ,[TYP_RIS_ASS_2_COEMP]
      ,[DTE_CREA]
      ,[USR_CREA]
      ,[IDE_STA_PLAN_FIN]
      ,[IDE_TYP_FIN]
      ,[MTT_PREM_TRCH]
      ,[TRCH_PRD_DIFFERE]
      ,[ASS_PREMIUM]
      ,[MTT_TOT_ASS]
      ,[DTE_DER_MOD]
      ,[USR_DER_MOD]
      ,[MTT_CRE]
      ,[DUR_MM_CRE]
      ,[TX_NOM_CRE]
      ,[TX_TAEA_CRE]
      ,[TX_TAEG_CRE]
      ,[TRCH_MM_AVEC_ASS]
      ,[TRCH_MM_SANS_ASS]
      ,[DTE_FIN_VALID_TX]
      ,[IDE_PROD_SOUS_CRE]
      ,[PRD_TOT_MM_DIFFERE]
      ,[MTT_TOT_INT]
      ,[TYP_USR]
      ,[ASS_PREMIUM_EMP]
      ,[ASS_PREMIUM_COEMP]
      ,[TX_TAEA_RECOM]
      ,[MTT_TOT_ASS_RECOM]
      ,[DTE_ALIM]
	)
SELECT
       PF.[ID_FE_CONSUMERLOANFINANCIALPLAN]
	  ,EF.SALESFORCE_OPPORTUNITY_ID
      ,PF.[APPLICATION_FEES]
      ,PF.[BORRO_EMPLOY_CONTR_TYPE_ID]
      ,PF.[BORRO_INSUR_GUARANTEE_LIST]
      ,PF.[BORRO_INSUR_RISK_RATE_1]
      ,PF.[BORRO_INSUR_RISK_RATE_2]
      ,PF.[BORRO_INSUR_RISK_TYPE_1]
      ,PF.[BORRO_INSUR_RISK_TYPE_2]
      ,PF.[BORRO_NET_MONTHLY_INCOME]
      ,PF.[CO_BORRO_EMPLOY_CONTR_TYPE_ID]
      ,PF.[CO_BORRO_INSUR_GUARANTEE_LIST]
      ,PF.[CO_BORRO_NET_MONTHLY_INCOME]
      ,PF.[CO_BORRO_RISK_RATE_1]
      ,PF.[CO_BORRO_RISK_RATE_2]
      ,PF.[CO_BORRO_RISK_TYPE_1]
      ,PF.[CO_BORRO_RISK_TYPE_2]
      ,PF.[CREATION_DATE]
      ,PF.[CREATION_USER]
      ,PF.[FINANCIAL_PLAN_STATUS_ID]
      ,PF.[FINANCING_TYPE_ID]
      ,PF.[FIRST_INSTALLMENT_AMOUNT]
      ,PF.[INSTAL_DURING_DEFFERED_PERIOD]
      ,PF.[INSURANCE_PREMIUM]
      ,PF.[INSURANCE_TOTAL_COST]
      ,PF.[LAST_UPDATE_DATE]
      ,PF.[LAST_UPDATE_USER]
      ,PF.[LOAN_AMOUNT]
      ,PF.[LOAN_DURATION_MONTHS]
      ,PF.[LOAN_NOMINAL_RATE_CUSTOMER]
      ,PF.[LOAN_TAEA_RATE]
      ,PF.[LOAN_TAEG_RATE]
      ,PF.[MONTHLY_INSTAL_WITH_INSURAN]
      ,PF.[MONTHLY_INSTAL_WITHOUT_INSURAN]
      ,PF.[RATE_VALIDITY_END_DATE]
      ,PF.[SUBSCRIBED_CONSUMER_LOAN_ID]
      ,PF.[TOTAL_DEFFERED_PERIOD_MONTHS]
      ,PF.[TOTAL_INTEREST_AMOUNT]
      ,PF.[USER_TYPE]
      ,PF.[BORRO_INSURANCE_PREMIUM]
      ,PF.[CO_BORRO_INSURANCE_PREMIUM]
      ,PF.[RECOMMENDED_LOAN_TAEA_RATE]
      ,PF.[RECOMMENDED_INSUR_TOTAL_COST]
      ,dateadd (day,-1,@date_alim) as [DTE_ALIM]
	FROM [$(DataHubDatabaseName)].[dbo].[PV_FE_CONSUMERLOANFINANCIALPLAN] PF
	inner join [$(DataHubDatabaseName)].[dbo].[PV_FE_SUBSCRIBEDCONSUMERLOAN] SC
	ON PF.SUBSCRIBED_CONSUMER_LOAN_ID = SC.ID_FE_SUBSCRIBEDCONSUMERLOAN
	inner join [$(DataHubDatabaseName)].[dbo].[PV_FE_SUBSCRIBEDPRODUCT] SP
	ON SP.ID_FE_SUBSCRIBEDPRODUCT = SC.ID_FE_SUBSCRIBEDCONSUMERLOAN
	inner join [$(DataHubDatabaseName)].[dbo].[PV_FE_ENROLMENTFOLDER] EF
	ON EF.ID_FE_ENROLMENT_FOLDER = SP.ENROLMENT_FOLDER_ID
	WHERE @date_derniere_alim <= PF.Validity_StartDate and PF.Validity_StartDate < @date_alim

SELECT @nbRows = @@ROWCOUNT

END