﻿
CREATE PROCEDURE [dbo].[PKGCP_PROC_CSL_FLUX_STOCK_OPE]
@DTE_OBS date

AS BEGIN

/*
declare @Date_obs date; 
set @Date_obs='2018-05-15'
;*/
SET @DTE_OBS = CAST(@DTE_OBS as date);
SET DATEFIRST 1; -->1    Lundi

With 
cte_jour_alim_list AS
( SELECT T1.StandardDate AS jour_alim
    FROM [dbo].[DIM_TEMPS] T1
    WHERE T1.StandardDate >= CAST(DATEADD(day, -6, @DTE_OBS) AS DATE) AND T1.StandardDate <= @DTE_OBS
)
--select * from cte_jour_alim_list order by 1 desc ;
,CTE_ALL_CSL As
(
SELECT cpt.[DTE_TRAITEMENT]
      ,cpt.[IDE_OPPRT]
      ,cpt.[NUM_CPT_SAB]
      ,cpt.[NUM_CLI_SAB]
      ,cpt.[FLAG_CPT]
      ,cpt.[LABEL_PROD_COM]
      ,cpt.[TYP_CPT_SAB]
      ,cpt.[DTE_OUV]
      ,cpt.[DTE_CLO]
	  ,DATEDIFF(DD,opp.DTE_CREA,cpt.DTE_OUV) as DELAI_TRANSFORMATION
	  ,DATEDIFF(MM,cpt.DTE_OUV,DTE_TRAITEMENT) as ANCIENNETE
      ,cpt.[MTT_PREM_VER]
      ,cpt.[SLD_DTE_CPT]
      ,cpt.[MTT_AUT]
      ,cpt.[MTT_DEP]
      ,cpt.[FLG_DEP_FAC_CAI]
      ,cpt.[FLG_MOB_BAN]
      ,cpt.[ACTIVITE_CPT]
      ,cpt.[LIB_ACTIVITE_CPT]
	  ,opp.[DTE_CREA]
  FROM [dbo].[MKT_COMPTE] cpt
  left join [dbo].[MKT_OPPORTUNITE] opp ON cpt.IDE_OPPRT = opp.IDE_OPPRT
  where DTE_TRAITEMENT  >= CAST(DATEADD(day, -7, @DTE_OBS) AS DATE) AND DTE_TRAITEMENT <= @DTE_OBS
  AND FLAG_CPT = 'CSL'
  )
--select * from CTE_ALL_CSL order by 1 desc ;

--select COUNT(distinct [NUM_CPT_SAB]) from CTE_ALL_CSL where DTE_TRAITEMENT = '2018-02-09'

/*select DTE_TRAITEMENT,[NUM_CLI_SAB],count([NUM_CPT_SAB]) from CTE_ALL_CSL group by DTE_TRAITEMENT,[NUM_CLI_SAB] 
having count([NUM_CPT_SAB])>1
order by DTE_TRAITEMENT desc ;*/

,CTE_FLUX_CSL_CNT as
(
Select  [DTE_TRAITEMENT]
      , count(case when [DTE_TRAITEMENT] = [DTE_OUV] then [NUM_CPT_SAB] END) AS NB_OUVERTURE
      , count(case when [DTE_TRAITEMENT] = [DTE_CLO] then [NUM_CPT_SAB] END) AS NB_CLOTURE
	  , PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY DELAI_TRANSFORMATION)   
                            OVER (PARTITION BY [DTE_TRAITEMENT]) AS DELAI_MEDIANT_TRANSFORMATION_CONT  
      , PERCENTILE_DISC(0.5) WITHIN GROUP (ORDER BY DELAI_TRANSFORMATION)   
                            OVER (PARTITION BY [DTE_TRAITEMENT]) AS DELAI_MEDIANT_TRANSFORMATION_DISC 
	  , SUM(case when [DTE_TRAITEMENT] = [DTE_OUV] then ISNULL([MTT_PREM_VER],0) END ) AS MTT_VERSEMENT_OUV
	  , PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY ISNULL([MTT_PREM_VER],0))   
                            OVER (PARTITION BY [DTE_TRAITEMENT]) AS MTT_MEDIANT_CONT  
      , PERCENTILE_DISC(0.5) WITHIN GROUP (ORDER BY ISNULL([MTT_PREM_VER],0))   
                            OVER (PARTITION BY [DTE_TRAITEMENT]) AS MTT_MEDIANT_DISC 
FROM CTE_ALL_CSL
group by [DTE_TRAITEMENT],DELAI_TRANSFORMATION,[MTT_PREM_VER]
)

--select * from CTE_FLUX_CSL_CNT order by 1 desc ;

,CTE_FLUX_CSL as
(
Select  [DTE_TRAITEMENT]
      , SUM(NB_OUVERTURE) as NB_OUVERTURE
      , SUM(NB_CLOTURE) as NB_CLOTURE
	  , AVG(DELAI_MEDIANT_TRANSFORMATION_CONT) as DELAI_MEDIANT_TRANSFORMATION_CONT  
      , AVG(DELAI_MEDIANT_TRANSFORMATION_DISC) as DELAI_MEDIANT_TRANSFORMATION_DISC 
	  , SUM(MTT_VERSEMENT_OUV) as MTT_VERSEMENT_OUV
	  , AVG(MTT_MEDIANT_CONT) as MTT_MEDIANT_CONT  
      , AVG(MTT_MEDIANT_DISC) as MTT_MEDIANT_DISC 
FROM CTE_FLUX_CSL_CNT
group by [DTE_TRAITEMENT]
)

--select * from CTE_FLUX_CSL order by 1 desc;

,CTE_STOCK_CSL_COUNT as
(
Select [DTE_TRAITEMENT]
      , NUM_CPT_SAB
	  , DTE_OUV
	  , DTE_CLO
      ,count(distinct NUM_CPT_SAB) AS NB_CSL
      ,count(case when ACTIVITE_CPT = 4 then NUM_CPT_SAB END)      AS NB_CSL_ACTIF_PLUSPLUS
	  ,count(case when ACTIVITE_CPT = 3 then NUM_CPT_SAB END)      AS NB_CSL_ACTIF_PLUS  
      ,count(case when ACTIVITE_CPT = 2 then NUM_CPT_SAB END)      AS NB_CSL_QUASI_ACTIF 
	  ,count(case when ACTIVITE_CPT = 1 then NUM_CPT_SAB END)      AS NB_CSL_INACTIF 
	  ,count(case when ANCIENNETE <=3 then NUM_CPT_SAB END)        AS NB_CSL_RECENT  
      ,count(case when ACTIVITE_CPT IN (4,3) then NUM_CPT_SAB END) AS PART_CSL_ACTIF
	  ,count(case when ACTIVITE_CPT IN (2,1) then NUM_CPT_SAB END) AS PART_CSL_DISTANCIE
	  ,sum(ISNULL([SLD_DTE_CPT],0)) as ENCOURS
	  , PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY ISNULL([SLD_DTE_CPT],0))   
                            OVER (PARTITION BY [DTE_TRAITEMENT]) AS ENCOURS_MEDIAN_CONT  
      , PERCENTILE_DISC(0.5) WITHIN GROUP (ORDER BY ISNULL([SLD_DTE_CPT],0))   
                            OVER (PARTITION BY [DTE_TRAITEMENT]) AS ENCOURS_MEDIAN_DISC  
FROM CTE_ALL_CSL
where DTE_OUV <= [DTE_TRAITEMENT] and (DTE_CLO is null or DTE_CLO > [DTE_TRAITEMENT]) 
group by [DTE_TRAITEMENT], NUM_CPT_SAB
	  , DTE_OUV
	  , DTE_CLO,[SLD_DTE_CPT]
)  
--select * from CTE_STOCK_CSL_COUNT where [DTE_TRAITEMENT] = '2018-02-09' order by 3 desc ;

,CTE_STOCK_CSL as
(
Select [DTE_TRAITEMENT]
      ,sum(NB_CSL) NB_CSL
      ,sum(NB_CSL_ACTIF_PLUSPLUS) AS NB_CSL_ACTIF_PLUSPLUS
	  ,sum(NB_CSL_ACTIF_PLUS) AS NB_CSL_ACTIF_PLUS  
      ,sum(NB_CSL_QUASI_ACTIF) AS NB_CSL_QUASI_ACTIF 
	  ,sum(NB_CSL_INACTIF) AS NB_CSL_INACTIF 
	  ,sum(NB_CSL_RECENT) AS NB_CSL_RECENT  
      ,sum(PART_CSL_ACTIF) AS PART_CSL_ACTIF
	  ,sum(PART_CSL_DISTANCIE) AS PART_CSL_DISTANCIE
	  ,ABS(sum(ENCOURS)) as ENCOURS
	  ,ABS(ENCOURS_MEDIAN_CONT) as ENCOURS_MEDIAN_CONT  
      ,ABS(ENCOURS_MEDIAN_DISC) as ENCOURS_MEDIAN_DISC 
	  ,LAG(ABS(sum(ENCOURS)), 1,0) OVER (ORDER BY [DTE_TRAITEMENT]) AS PREVIOUS_ENCOURS    
FROM CTE_STOCK_CSL_COUNT
group by [DTE_TRAITEMENT],ENCOURS_MEDIAN_CONT,ENCOURS_MEDIAN_DISC
)  
--select * from CTE_STOCK_CSL order by [DTE_TRAITEMENT] desc  ;

/************/

/************/

,CTE_OPERATION as 
(
select t.DTE_TRAITEMENT
      ,sum(case when t.FLAG_MTT_OPT = 0  then ABS(t.MONTANT_OP) else 0 END) - sum(case when t.FLAG_INITIATIVE_CLIENT= 'DEDUIRE' and t.FLAG_OP_DEB_CRE='Credit' then t.MONTANT_OP else 0 END)
	  -sum(case when t.FLAG_MTT_OPT = 1  then t.MONTANT_OP else 0 END) - sum(case when t.FLAG_INITIATIVE_CLIENT= 'DEDUIRE' and t.FLAG_OP_DEB_CRE='Debit' then  t.MONTANT_OP else 0 END)
	   AS COLLECTE_NETTE
	  /*OPE CRE*/
	  ,sum(case when t.FLAG_MTT_OPT = 0  then ABS(t.MONTANT_OP) else 0 END) - sum(case when t.FLAG_INITIATIVE_CLIENT= 'DEDUIRE' and t.FLAG_OP_DEB_CRE='Credit' then t.MONTANT_OP else 0 END) AS MTT_OPE_CREDITRICES
	  ,sum(case when t.FLAG_MTT_OPT = 0  and t.SOUS_TYP_MVT='Virement permanent' then ABS(t.MONTANT_OP) else 0 END) AS MTT_VIREMENTS_PERMANENTS
	  ,sum(case when t.FLAG_MTT_OPT = 0  and t.SOUS_TYP_MVT='Virement interne ponctuel' then ABS(t.MONTANT_OP) else 0 END) - sum(case when t.SOUS_TYP_MVT='Rejet Virement interne ponctuel' then ABS(t.MONTANT_OP) else 0 END) AS MTT_VIREMENTS_PONCTUELS
	  ,sum(case when t.FLAG_MTT_OPT = 0  and t.TYP_MVT='Intérêts versés' then ABS(t.MONTANT_OP) else 0 END) AS MTT_INTERET_VERSES
	  ,sum(case when t.FLAG_MTT_OPT = 0  and t.SOUS_TYP_MVT NOT IN ('Intérêts versés','Virement permanent','Virement interne ponctuel') then ABS(t.MONTANT_OP) else 0 END) AS MTT_AUTRES
	  --,'' AS MTT_MOY_OPE_CREDITRICES
	  ,sum(case when t.FLAG_MTT_OPT = 0  then t.NOMBRE_OP else 0 END) - sum(case when t.FLAG_INITIATIVE_CLIENT= 'DEDUIRE' and t.FLAG_OP_DEB_CRE='Credit' then t.NOMBRE_OP else 0 END) AS NB_OPE_CREDITRICES  
	  ,sum(case when t.FLAG_MTT_OPT = 0  and t.SOUS_TYP_MVT='Virement permanent' then t.NOMBRE_OP else 0 END) AS NB_VIREMENTS_PERMANENTS
	  ,sum(case when t.FLAG_MTT_OPT = 0  and t.SOUS_TYP_MVT='Virement interne ponctuel' then t.NOMBRE_OP else 0 END) - sum(case when t.SOUS_TYP_MVT='Rejet Virement interne ponctuel' then t.NOMBRE_OP else 0 END) AS NB_VIREMENTS_PONCTUELS
	  ,sum(case when t.FLAG_MTT_OPT = 0  and t.TYP_MVT='Intérêts versés' then t.NOMBRE_OP else 0 END) AS NB_INTERET_VERSES
	  /*OPE DEB*/
	  ,sum(case when t.FLAG_MTT_OPT = 1  then t.MONTANT_OP else 0 END) - sum(case when t.FLAG_INITIATIVE_CLIENT= 'DEDUIRE' and t.FLAG_OP_DEB_CRE='Debit' then  t.MONTANT_OP else 0 END) AS MTT_OPE_DEBETRICES
	  --,'' AS MTT_MOY_OPE_DEBETRICES
	  ,sum(case when t.FLAG_MTT_OPT = 1  then t.NOMBRE_OP else 0 END) - sum(case when t.FLAG_INITIATIVE_CLIENT= 'DEDUIRE' and t.FLAG_OP_DEB_CRE='Debit' then  t.NOMBRE_OP else 0 END) AS NB_OPE_DEBETRICES 
from T_FACT_MKT_USAGE_CPT_DAY t
inner join CTE_ALL_CSL c  on  t.NUM_CPT_SAB = c.NUM_CPT_SAB and t.DTE_TRAITEMENT = c.DTE_TRAITEMENT
where c.DTE_OUV <= t.[DTE_TRAITEMENT] and (c.DTE_CLO is null or c.DTE_CLO > t.[DTE_TRAITEMENT]) --Compte ouvert !!! a tester
group by t.DTE_TRAITEMENT
)		

--select * from CTE_OPERATION;

select   x.jour_alim
       , flux.DTE_TRAITEMENT as flux_DTE_TRAITEMENT
	   , stock.DTE_TRAITEMENT as stock_DTE_TRAITEMENT
	   , ope.DTE_TRAITEMENT as ope_DTE_TRAITEMENT
        --FLUX
       , ISNULL(NB_OUVERTURE,0) as NB_OUVERTURE 
       , ISNULL(NB_CLOTURE                        ,0) as NB_CLOTURE
       , ISNULL(DELAI_MEDIANT_TRANSFORMATION_CONT ,0) as DELAI_MEDIANT_TRANSFORMATION_CONT
       , ISNULL(DELAI_MEDIANT_TRANSFORMATION_DISC ,0) as DELAI_MEDIANT_TRANSFORMATION_DISC
       , ISNULL(MTT_VERSEMENT_OUV				  ,0) as MTT_VERSEMENT_OUV
       , ISNULL(MTT_MEDIANT_CONT				  ,0) as MTT_MEDIANT_CONT
       , ISNULL(MTT_MEDIANT_DISC				  ,0) as MTT_MEDIANT_DISC
        --STOCK
       , ISNULL(NB_CSL                ,0) as NB_CSL 
       , ISNULL(NB_CSL_ACTIF_PLUSPLUS ,0) as NB_CSL_ACTIF_PLUSPLUS
       , ISNULL(NB_CSL_ACTIF_PLUS  	  ,0) as NB_CSL_ACTIF_PLUS  
       , ISNULL(NB_CSL_QUASI_ACTIF 	  ,0) as NB_CSL_QUASI_ACTIF 
       , ISNULL(NB_CSL_INACTIF 		  ,0) as NB_CSL_INACTIF 
       , ISNULL(NB_CSL_RECENT  		  ,0) as NB_CSL_RECENT  
       , ISNULL(PART_CSL_ACTIF		  ,0) as PART_CSL_ACTIF
       , ISNULL(PART_CSL_DISTANCIE	  ,0) as PART_CSL_DISTANCIE
       , ISNULL(ENCOURS	     		  ,0) as ENCOURS
       , ISNULL(ENCOURS_MEDIAN_CONT	  ,0) as ENCOURS_MEDIAN_CONT
       , ISNULL(ENCOURS_MEDIAN_DISC	  ,0) as ENCOURS_MEDIAN_DISC
       , ISNULL(PREVIOUS_ENCOURS	  ,0) as PREVIOUS_ENCOURS
		--OPERATION
	   , ISNULL(COLLECTE_NETTE          ,0) as COLLECTE_NETTE                
       , ISNULL(MTT_OPE_CREDITRICES		,0) as MTT_OPE_CREDITRICES
       , ISNULL(MTT_VIREMENTS_PERMANENTS,0) as MTT_VIREMENTS_PERMANENTS
       , ISNULL(MTT_VIREMENTS_PONCTUELS	,0) as MTT_VIREMENTS_PONCTUELS
       , ISNULL(MTT_INTERET_VERSES		,0) as MTT_INTERET_VERSES
       , ISNULL(MTT_AUTRES				,0) as MTT_AUTRES
       , ISNULL(NB_OPE_CREDITRICES		,0) as NB_OPE_CREDITRICES
       , ISNULL(NB_VIREMENTS_PERMANENTS	,0) as NB_VIREMENTS_PERMANENTS
       , ISNULL(NB_VIREMENTS_PONCTUELS	,0) as NB_VIREMENTS_PONCTUELS
       , ISNULL(NB_INTERET_VERSES		,0) as NB_INTERET_VERSES
       , ISNULL(MTT_OPE_DEBETRICES		,0) as MTT_OPE_DEBETRICES
       , ISNULL(NB_OPE_DEBETRICES		,0) as NB_OPE_DEBETRICES
        								
FROM cte_jour_alim_list x
LEFT join CTE_FLUX_CSL flux on x.jour_alim =   flux.DTE_TRAITEMENT
LEFT JOIN CTE_STOCK_CSL stock on x.jour_alim = stock.DTE_TRAITEMENT
LEFT JOIN CTE_OPERATION ope on x.jour_alim =   ope.DTE_TRAITEMENT
ORDER BY x.jour_alim	

END;