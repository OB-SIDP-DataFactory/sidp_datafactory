﻿CREATE PROCEDURE [dbo].[PKGRC_PROC_ventes]
    @nb_collected_rows int output
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
        -- supprimer dates à insérer eventuellement déjà présentes 

            declare @date_min date 
            select @date_min = MIN(cast(date_alim as date )) from [WK_STOCK_OPPORTUNITY]
            --select @date_min

            declare @date_max date 
            select @date_max = MAX(cast(date_alim as date )) from [WK_STOCK_OPPORTUNITY]
            --select @date_max

           truncate table SAS_Vue_RC_Ventes
        ;
with 
--selection des opportunités créées 

opport as 
( 
    select distinct Id_SF,opp.date_alim,case when opp.CommercialOfferCode__c is null then 0 else offer.PK_ID end as FK_OFFRE, 
			opp.CommercialOfferCode__c as OFFRE_CODE, 
            opp.CommercialOfferName__c as OFFRE_LABEL, 
            po.PK_ID as FK_PRODUIT_OFFRE,
            po.LIBELLE as PRODUIT_LABEL,
            c_int.PK_ID as FK_CANAL_INT, 
            c_int.LIBELLE as CANAL_INT_LABEL,
            c_dist.PK_ID as FK_CANAL_DIST,
            c_dist.LIBELLE as CANAL_DIST_LABEL, 
            opp.CreatedById,
            opp.AdvisorCode__c, 
            opp.PointOfSaleCode__c, 
            opp.last_update_date, 
            opp.DistributorNetwork__c, 
            opp.DistributorEntity__c,
            opp.ProductFamilyCode__c as PRODUCT_FAMILY_CODE,
            opp.ProductFamilyName__c as PRODUCT_FAMILY_NAME, 
            opp.CreatedDate,
            sdv.PK_ID as FK_STAGENAME, 
            sdv.LIBELLE as STAGENAME ,
            opp.flag_indication, 
            opp.flag_full_btq, 
            opp.flag_full_digi 

    from WK_STOCK_OPPORTUNITY opp 
    left join [dbo].[DIM_PRODUIT_OFFRE] offer on opp.CommercialOfferCode__c = offer.CODE_FE and offer.PARENT_ID is null and offer.Validity_StartDate <= opp.last_update_date  and offer.Validity_EndDate >= opp.last_update_date
    left join [dbo].[DIM_PRODUIT_OFFRE] po on po.PARENT_ID = opp.CommercialOfferCode__c and po.Validity_StartDate <= opp.last_update_date  and po.Validity_EndDate >= opp.last_update_date
    left join [dbo].[DIM_CANAL] c_int on c_int.CODE_SF = opp.StartedChannel__c and c_int.PARENT_ID is not null and c_int.Validity_StartDate <= opp.last_update_date  and c_int.Validity_EndDate >= opp.last_update_date
    left join [dbo].[DIM_CANAL] c_dist on c_dist.CODE_SF = c_int.PARENT_ID and c_dist.PARENT_ID is null and c_dist.Validity_StartDate <= opp.last_update_date  and c_dist.Validity_EndDate >= opp.last_update_date
    left join DIM_STADE_VENTE sdv ON (sdv.CODE_SF=opp.StageName and sdv.PARENT_ID is null) 
                
    where flag_opport_last_state_overall=1 

) , 

--selection des opportunités au stade de vente élaboration proposition  

opport_elab as 
( 
    select distinct Id_SF 

    from WK_STOCK_OPPORTUNITY opp 
    
    where StageName ='03' and flag_opport_stage_last_state=1 
    
) , 

--selection des opportunités au stade de vente compte ouvert 

opport_ouvert as 
( 
    select distinct Id_SF, date_alim,  case when opp.CommercialOfferCode__c is null then 0 else offer.PK_ID end as FK_OFFRE, 
            offer.LIBELLE as OFFRE_LABEL, 
            po.PK_ID as FK_PRODUIT_OFFRE,
            po.LIBELLE as PRODUIT_LABEL ,
            opp.CreatedDate,
            CloseDate,
            c_int.PK_ID as FK_CANAL_INT, 
            c_int.LIBELLE as CANAL_INT_LABEL,
            c_dist.PK_ID as FK_CANAL_DIST,
            c_dist.LIBELLE as CANAL_DIST_LABEL 
    from WK_STOCK_OPPORTUNITY opp 
    left join [$(DataHubDatabaseName)].[dbo].[PV_FE_ENROLMENTFOLDER] enr on opp.Id_SF = enr.SALESFORCE_OPPORTUNITY_ID 
    left join [$(DataHubDatabaseName)].[dbo].[PV_FE_SUBSCRIBEDPRODUCT] sub on enr.ID_FE_ENROLMENT_FOLDER = sub.ENROLMENT_FOLDER_ID 
    left join [$(DataHubDatabaseName)].[dbo].[PV_FE_COMMERCIALPRODUCT] cp on sub.COMMERCIAL_PRODUCT_ID = cp.ID_FE_COMMERCIALPRODUCT 
    left join [dbo].[DIM_PRODUIT_OFFRE] po on (cp.CODE  = po.CODE_FE and po.PARENT_ID = opp.CommercialOfferCode__c and po.Validity_StartDate <= opp.last_update_date  and po.Validity_EndDate >= opp.last_update_date) 
    left join [dbo].[DIM_PRODUIT_OFFRE] offer on po.PARENT_ID = offer.CODE_FE and offer.PARENT_ID is null and offer.Validity_StartDate <= opp.last_update_date  and offer.Validity_EndDate >= opp.last_update_date 
    left join [dbo].[DIM_CANAL] c_int on c_int.CODE_SF = opp.LeadSource and c_int.PARENT_ID is not null and c_int.Validity_StartDate <= opp.last_update_date  and c_int.Validity_EndDate >= opp.last_update_date
    left join [dbo].[DIM_CANAL] c_dist on c_dist.CODE_SF = c_int.PARENT_ID and c_dist.PARENT_ID is null and c_dist.Validity_StartDate <= opp.last_update_date  and c_dist.Validity_EndDate >= opp.last_update_date 
    
    where StageName in ('09','13') and flag_opport_last_state_overall=1 
        
) 

--insertion des lignes dans la vue opportunités 

insert into [dbo].[SAS_Vue_RC_Ventes] 
select distinct 
            CONCAT(opport.Id_SF,opport.FK_PRODUIT_OFFRE) as PK_ID, 
            opport.Id_SF as OPPORT_ID, 
            (case when opport.id_SF is not null then opport.id_SF end ) as FLAG_CREEES, 
            (case when opport_elab.id_SF is not null then opport_elab.id_SF end ) as FLAG_ELAB, 
            (case when (opport_ouvert.id_SF is not null and opport_ouvert.FK_PRODUIT_OFFRE is not null) then opport_ouvert.id_SF end ) as FLAG_OUVERT, 
            opport.FK_STAGENAME as FK_STAGENAME,
            opport.StageName as STAGENAME, 
            us.PK_ID as FK_USER,
            CONCAT(us.NOM_USER,' ',us.PRENOM_USER) as LOGIN_USER,
            us.PROFIL_USER,
            tps.PK_ID as FK_TEMPS, 
            tps.StandardDate as STANDARD_DATE,
            tps.Month as MONTH, 
            tps.MonthName as MONTH_NAME,
            tps.Year as YEAR, 
            CONCAT(tps.MonthName,' ',tps.Year) as MOIS_ANNEE,
            tps.IsoWeek as SEMAINE,
            opport.FK_OFFRE as FK_OFFRE, 
			opport.OFFRE_CODE as OFFRE_CODE, 
            opport.OFFRE_LABEL as OFFRE_LABEL, 
            opport.FK_PRODUIT_OFFRE  as FK_PRODUIT_OFFRE, 
            opport.PRODUIT_LABEL as PRODUIT_LABEL, 
            opport.PRODUCT_FAMILY_CODE as PRODUCT_FAMILY_CODE, 
            opport.PRODUCT_FAMILY_NAME as PRODUCT_FAMILY_NAME, 
            opport.FK_CANAL_INT as FK_CANAL_INT_INIT, 
            isnull(opport.CANAL_INT_LABEL,'Non-renseigné') as CANAL_INT_LABEL_INIT,
            opport.FK_CANAL_DIST as FK_CANAL_DIST_INIT,
            isnull(opport.CANAL_DIST_LABEL,'Non-renseigné') as CANAL_DIST_LABEL_INIT,
            opport_ouvert.FK_CANAL_INT as FK_CANAL_INT_FIN, 
            isnull(opport_ouvert.CANAL_INT_LABEL,'Non-renseigné') as CANAL_INT_LABEL_FIN, 
            opport_ouvert.FK_CANAL_DIST as FK_CANAL_DIST_FIN,
            isnull(opport_ouvert.CANAL_DIST_LABEL,'Non-renseigné') as CANAL_DIST_LABEL_FIN, 
            res.PK_ID as FK_RESEAU,
            isnull(res.LIBELLE,'Non-renseigné') as RESEAU_LABEL,
            sous_res.PK_ID as FK_SOUS_RESEAU,
            isnull(sous_res.LIBELLE,'Non-renseigné') as SOUS_RESEAU_LABEL,
            (case when opport_ouvert.id_SF is not null then DATEDIFF(day,opport_ouvert.CreatedDate,opport_ouvert.CloseDate) else null end) as TEMPS_TRANSFO,
            opport.flag_indication as FLAG_INDICATION, 
            sel.PK_ID as FK_VENDEUR, 
            sel.NAME as NOM_VENDEUR,
            bou.PK_ID as FK_BOUTIQUE, 
            bou.NAME as NOM_BOUTIQUE, 
            (case when opport.flag_full_digi=1 then 'Full digital' else (case when opport.flag_full_btq=1 then 'Full agence' else 'Multi canal' end) end) as CANAL 
    
            from opport 
            left join opport_elab on opport.Id_SF=opport_elab.Id_SF 
            left join opport_ouvert on opport.Id_SF=opport_ouvert.Id_SF and opport.FK_PRODUIT_OFFRE=opport_ouvert.FK_PRODUIT_OFFRE 
            left join [dbo].[DIM_USER] us on opport.CreatedById = us.CODE_SF_USER and us.Validity_StartDate <= opport.CreatedDate  and us.Validity_EndDate >= opport.CreatedDate 
            left join [dbo].[DIM_TEMPS] tps on cast(tps.Date as date) = cast(opport.CreatedDate as date) 
            left join [dbo].[DIM_RESEAU] res on res.CODE_SF = opport.DistributorNetwork__c and res.PARENT_ID is null and res.Validity_StartDate <= opport.last_update_date  and res.Validity_EndDate >= opport.last_update_date 
            left join [dbo].[DIM_RESEAU] sous_res on sous_res.CODE_SF = opport.DistributorEntity__c and sous_res.PARENT_ID=opport.DistributorNetwork__c and sous_res.Validity_StartDate <= opport.last_update_date  and sous_res.Validity_EndDate >= opport.last_update_date 
            left join [dbo].[DIM_VENDEUR] sel on opport.AdvisorCode__c=sel.SELLER_UID 
            left join [dbo].[DIM_BOUTIQUE] bou on opport.PointOfSaleCode__c=bou.SHOP_CODE 

            order by CONCAT(opport.Id_SF,opport.FK_PRODUIT_OFFRE)             
            
        select @nb_collected_rows = COUNT(*) from [dbo].[SAS_Vue_RC_Ventes];
END