﻿CREATE PROCEDURE [dbo].[PKGES_PROC_OPPORT_VISION_GLOBALE_OF]

@Datedebut date, 
@Datefin date   

AS BEGIN

select 
T.IndicationID,T.Indication,
sum(T.nb_opport) as nb_opport,
case when T.IndicationID=4 then sum(T.CanalBoutiqueEnCours)  else sum(T.CanalBoutiqueEnCours)+sum(T.CountApreciser) end as  CanalBoutiqueEnCours,
case when T.IndicationID=4 then sum(T.CanalDigitEnCours)+sum(T.CountApreciser) else sum(T.CanalDigitEnCours) end as  CanalDigitEnCours,
sum(T.CanalCRCEnCours) CanalCRCEnCours ,sum(T.TotalEnCours) TotalEnCours,
sum(T.CanalBoutiqueFinal) CanalBoutiqueFinal,sum(T.CanalDigitFinal) CanalDigitFinal,sum(T.TotalFinal) TotalFinal,
sum(T.Incident) Incident,sum(T.SansSuite) SansSuite ,sum(T.Refusee) Refusee,sum(T.Retaractaction) Retaractaction,sum(DetectionBoutiqueAD) DetectionBoutiqueAD,
sum(DetectionBoutiqueGDT) DetectionBoutiqueGDT,sum(DetectionDigital) DetectionDigital
from (
Select StageName,
case  when   flag_indication='Oui' and DistributorEntity__c='01' then 2
       when  flag_indication='Oui' and DistributorEntity__c='02' then 3
	   when  flag_indication='Oui' and DistributorEntity__c='04' then 4
	   when  flag_indication='Non' and DistributorEntity__c='04' then 5
	   when  flag_indication='Non'   and DistributorEntity__c in ('01','02') then 1 end as IndicationID,

case  when   flag_indication='Oui' and DistributorEntity__c='01' then 'Indication Boutique AD'
       when  flag_indication='Oui' and DistributorEntity__c='02' then 'Indication Boutique GDT'
	   when  flag_indication='Oui' and DistributorEntity__c='04' then 'Indication Digitale'
	   when  flag_indication='Non' and DistributorEntity__c='04' then 'Renvoi Trafic'
	   when  flag_indication='Non'   and DistributorEntity__c in ('01','02') then 'Souscription IOBSP' end as Indication,
-- En cours
count(distinct Wk.Id_SF) as nb_opport,
case when StageName ='01' and  LeadSource ='01' then count(distinct Wk.Id_SF) else 0 end as  CountApreciser,
case when StageName not in ('08','09','10','11','12') and  LeadSource in ('02','03') then count(distinct Wk.Id_SF) else 0 end as  CanalBoutiqueEnCours,
case when StageName not in ('08','09','10','11','12') and  LeadSource in('10','11') then count(distinct Wk.Id_SF) else 0 end as  CanalDigitEnCours,
case when StageName not in ('08','09','10','11','12') and  LeadSource in ('12','13') then count(distinct Wk.Id_SF) else 0 end as  CanalCRCEnCours,
case when StageName not in ('08','09','10','11','12') and LeadSource in ('01','02','03','10','11','12','13') then count(distinct Wk.Id_SF) else 0 end as  TotalEnCours,
-- Finalisé
case when StageName ='09' and  LeadSource in ('02','03') then count(distinct Wk.Id_SF) else 0 end as  CanalBoutiqueFinal,
case when StageName ='09' and  LeadSource in('10','11')  then count(distinct Wk.Id_SF) else 0 end as  CanalDigitFinal,
case when StageName ='09' and  LeadSource in ('02','03','10','11') then count(distinct Wk.Id_SF) else 0 end as  TotalFinal,
case when StageName ='08' then count(distinct Wk.Id_SF) else 0 end as  Incident,
case when StageName ='10' then count(distinct Wk.Id_SF) else 0 end as  Refusee,
case when StageName ='11' then count(distinct Wk.Id_SF) else 0 end as  SansSuite,
case when StageName ='12' then count(distinct Wk.Id_SF) else 0 end as  Retaractaction,
case when  StageName ='01' and flag_indication='Oui' and DistributorEntity__c='01'  and  LeadSource in ('01','02','03') then count(distinct Wk.Id_SF) else 0 end as  DetectionBoutiqueAD,
case when  StageName ='01' and flag_indication='Oui' and DistributorEntity__c='02'  and  LeadSource in ('01','02','03') then count(distinct Wk.Id_SF) else 0 end as  DetectionBoutiqueGDT,
case when  StageName ='01' and flag_indication='Oui' and DistributorEntity__c in('04')  and  LeadSource in('01','10','11') then count(distinct Wk.Id_SF) else 0 end as  DetectionDigital
from [dbo].[WK_STOCK_OPPORTUNITY] as Wk
left outer join  [$(DataHubDatabaseName)].[dbo].[PV_SF_OPPORTUNITYISDELETED] as Wkd on Wk.Id_SF= Wkd.Id_SF
where Wkd.Id_SF is null and DistributorNetwork__c='01' --Orange France
and CommercialOfferCode__c='OC80'
and cast(CreatedDate as date) between @Datedebut and @Datefin
and DistributorEntity__c IN ('01','02','04')
and flag_opport_last_state = 1
and LeadSource in ('01','02','03','10','11','12','13')
and [date_alim]=(SELECT MAX([date_alim]) FROM [dbo].[WK_STOCK_OPPORTUNITY])
group by flag_indication,DistributorEntity__c,StageName,LeadSource) as T
group by  T.IndicationID,T.Indication

END;
