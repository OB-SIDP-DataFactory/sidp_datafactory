﻿-- =============================================
-- Author:         <Author,,Name>
-- Create date:    <Create Date,,>
-- Description:    <Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PKGRC_PROC_stock_cases] 
    -- Add the parameters for the stored procedure here
    @nb_collected_rows int output
    
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

    if ((select count(*) from [dbo].[WK_etat_stock_cases]) >1)  
    begin
        --Etat actuel du stock : si la table des stocks n'est pas vide, on la vide puis on récupére le stock au dernier jour pour chaque recordtype 
        truncate table [dbo].[WK_etat_stock_cases]; 

        insert into [dbo].[WK_etat_stock_cases] 
        select PK_ID, [DATE], FK_RECORDTYPE, RECORDTYPE_LABEL, CASES_CREES, CASES_TRAITES, CASES_EN_COURS, CASES_CREES_CUMUL, CASES_TRAITES_CUMUL, CASES_TRAITES_WTD, CASES_TRAITES_MTD, CASES_TRAITES_YTD 
          from ( select * 
                      , row_number() over (partition by FK_RECORDTYPE order by [DATE] desc) as Last_Stock_Flag
                   from [dbo].[SAS_Vue_RC_Stock_Cases] ) as Stock_Cases
         where Last_Stock_Flag = 1; 

        delete from [dbo].[SAS_Vue_RC_Stock_Cases] where PK_ID in (select PK_ID from [dbo].[WK_etat_stock_cases]) ; 
    end
    else
    begin
        --Etat actuel du stock : si la table des stocks est vide, on met le stock à 0 pour chaque recordtype 
        insert into [dbo].[WK_etat_stock_cases] 
        select distinct concat('2016-12-01','-',FK_RECORDTYPE), '2016-12-01', FK_RECORDTYPE, RECORDTYPE_LABEL, 0,0,0,0,0,0,0,0 
          from [dbo].[SAS_Vue_RC_Cases]; 

        --on vide aussi la table SAS_VUe_RC_Stock_Cases 
        truncate table [dbo].[sas_vue_rc_stock_cases]; 
    end 
     
    --Une fois la table état du stock remplie, on charge la table de stock 
    if object_id('tempdb..#wk_RC_Stock_Cases') is not null
    begin drop table #wk_RC_Stock_Cases end;

    with date_alim as ( 
      select distinct StandardDate as StandardDate 
        from DIM_TEMPS
       where StandardDate >= (select max([DATE]) from WK_etat_stock_cases) and StandardDate < getdate() 
    ) 

    select concat(date_alim.StandardDate, '-', stock_cases.FK_RECORDTYPE) as PK_ID, 
           date_alim.StandardDate as [DATE],  
           stock_cases.FK_RECORDTYPE, 
           stock_cases.RECORDTYPE_LABEL, 
           max(case when date_alim.StandardDate = stock_cases.[DATE] then stock_cases.CASES_CREES else 0 end) as CASES_CREES_ETAT_STOCK, 
           max(case when date_alim.StandardDate = stock_cases.[DATE] then stock_cases.CASES_TRAITES else 0 end) as CASES_TRAITES_ETAT_STOCK, 
           max(case when date_alim.StandardDate = stock_cases.[DATE] then stock_cases.CASES_EN_COURS else 0 end) as CASES_EN_COURS_ETAT_STOCK, 
           max(case when date_alim.StandardDate = stock_cases.[DATE] then stock_cases.CASES_CREES_CUMUL else 0 end) as CASES_CREES_CUMUL_ETAT_STOCK, 
           max(case when date_alim.StandardDate = stock_cases.[DATE] then stock_cases.CASES_TRAITES_CUMUL else 0 end) as CASES_TRAITES_CUMUL_ETAT_STOCK, 
           max(case when date_alim.StandardDate = stock_cases.[DATE] then stock_cases.CASES_TRAITES_WTD else 0 end) as CASES_TRAITES_WTD_ETAT_STOCK, 
           max(case when date_alim.StandardDate = stock_cases.[DATE] then stock_cases.CASES_TRAITES_MTD else 0 end) as CASES_TRAITES_MTD_ETAT_STOCK, 
           max(case when date_alim.StandardDate = stock_cases.[DATE] then stock_cases.CASES_TRAITES_YTD else 0 end) as CASES_TRAITES_YTD_ETAT_STOCK,
           count(distinct case when cast(RC_Cases.CREATED_DATE as date) = date_alim.StandardDate then RC_Cases.CASENUMBER else NULL end) as CASES_CREES, 
           count(distinct case when cast(RC_Cases.CLOSED_DATE as date) = date_alim.StandardDate then RC_Cases.CASENUMBER else NULL end) as CASES_TRAITES, 
           count(distinct case when cast(RC_Cases.CREATED_DATE as date) <= date_alim.StandardDate and coalesce(cast(RC_Cases.CLOSED_DATE as date), '9999-12-31') > date_alim.StandardDate then RC_Cases.CASENUMBER else NULL end) as CASES_EN_COURS
      into #wk_RC_Stock_Cases
      from date_alim 
      left join [dbo].[WK_etat_stock_cases] stock_cases
        on date_alim.StandardDate >= stock_cases.[DATE] 
      left join [dbo].[SAS_Vue_RC_Cases] RC_Cases
        on RC_Cases.FK_RECORDTYPE = stock_cases.FK_RECORDTYPE and 
           cast(RC_Cases.CREATED_DATE as date) <= date_alim.StandardDate and
           coalesce(cast(RC_Cases.CLOSED_DATE as date), '9999-12-31') >= date_alim.StandardDate
    group by concat(date_alim.StandardDate, '-', stock_cases.FK_RECORDTYPE), date_alim.StandardDate, stock_cases.FK_RECORDTYPE, stock_cases.RECORDTYPE_LABEL;

    insert into [dbo].[SAS_Vue_RC_Stock_Cases]
    select PK_ID, 
           [DATE],  
           FK_RECORDTYPE, 
           RECORDTYPE_LABEL, 
           CASES_CREES, 
           CASES_TRAITES, 
           CASES_EN_COURS,
           LAG(CASES_EN_COURS, 1, 0) over (partition by FK_RECORDTYPE order by [DATE]) as STOCK_PRECEDENT,
           CASES_CREES + LAG(CASES_EN_COURS, 1, 0) over (partition by FK_RECORDTYPE order by [DATE]) as STOCK_EN_COURS,

           SUM(CASES_CREES_ETAT_STOCK + CASES_CREES) over (partition by FK_RECORDTYPE order by [DATE]) AS CASES_CREES_CUMUL,
           SUM(CASES_TRAITES_ETAT_STOCK + CASES_TRAITES) over (partition by FK_RECORDTYPE order by [DATE]) AS CASES_TRAITES_CUMUL,

           SUM(CASES_TRAITES_ETAT_STOCK + CASES_TRAITES) over (partition by FK_RECORDTYPE, DATEADD(WEEK,DATEDIFF(WEEK,0,[DATE]),0) order by [DATE]) AS CASES_TRAITES_CUMUL_WTD,
           SUM(CASES_TRAITES_ETAT_STOCK + CASES_TRAITES) over (partition by FK_RECORDTYPE, DATEADD(MONTH,DATEDIFF(MONTH,0,[DATE]),0) order by [DATE]) AS CASES_TRAITES_CUMUL_MTD,
           SUM(CASES_TRAITES_ETAT_STOCK + CASES_TRAITES) over (partition by FK_RECORDTYPE, DATEADD(YEAR,DATEDIFF(YEAR,0,[DATE]),0) order by [DATE]) AS CASES_TRAITES_CUMUL_YTD
      from #wk_RC_Stock_Cases;

    select @nb_collected_rows = COUNT(*) from [dbo].[SAS_Vue_RC_Stock_Cases]; 

END