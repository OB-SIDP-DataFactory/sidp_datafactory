﻿CREATE PROCEDURE [dbo].[PKGCR_PROC_VIE_CREDIT_PROD_ENCOURS_STOCK]
@DTE_OBS date

AS BEGIN

/*declare @DTE_OBS date;
SET @DTE_OBS = '2017-01-03';*/

SET @DTE_OBS = CAST(@DTE_OBS as date) ;
SET DATEFIRST 1;

WITH 
CTE_CRE_ALL AS (
SELECT CRE.[COD_CAN_INT_ORI]
      ,CRE.[LIB_CAN_INT_ORI]
      ,CRE.[DTE_DER_DEB]
	  ,CRE.[WeekOfYear]
      ,CRE.[IsoWeek]
      ,CRE.[Month]
	  ,CRE.[Year]
      ,CRE.[IsoWeekYear]
	  ,CRE.[NBE_CREDIT]
      ,CRE.[NBE_CREDIT_STOCK]
      ,CRE.[MTT_TOT_DEB]
	  ,CRE.[CRD]
FROM [dbo].[CRE_STOCK_CREDIT] CRE 
WHERE CRE.COD_CAN_INT_ORI IN ('11', '10', '12', '13', '14', '15')
AND CRE.[DTE_DER_DEB] <=@DTE_OBS /*S'arréter à la date du rapport (paramètre du rapport)*/
)
--Production Brute YTD
,Cte_Prod_Encours_Stock_Annee AS (
SELECT	SUM([NBE_CREDIT]) as NB_PROD_ANN
		,SUM([MTT_TOT_DEB]) as MONTANT_PROD_ANN
		,[COD_CAN_INT_ORI]
		,[LIB_CAN_INT_ORI] 
FROM CTE_CRE_ALL
WHERE [Year] = DATEPART(YEAR,@DTE_OBS)
AND [WeekOfYear] <= DATEPART(WEEK,@DTE_OBS)
GROUP BY COD_CAN_INT_ORI, LIB_CAN_INT_ORI
)

--Encours/Stocks Mensuel
,Cte_Prod_Encours_Stock_M as (
SELECT COD_CAN_INT_ORI,
       LIB_CAN_INT_ORI,
	   SUM(CRD) as MONTANT_ENCOURS_M,
	   SUM(NBE_CREDIT_STOCK) as STOCK_M
FROM CTE_CRE_ALL
WHERE ((year*100+WeekOfYear) <= (DATEPART(YEAR,@DTE_OBS)*100+DATEPART(WEEK,@DTE_OBS)))
GROUP BY COD_CAN_INT_ORI,LIB_CAN_INT_ORI
)

--Production Encours/Stocks Mois précédent
,Cte_Prod_Encours_Stock_M_Prec as (
SELECT COD_CAN_INT_ORI,
       LIB_CAN_INT_ORI,
	   SUM(CRD) as MONTANT_ENCOURS_M_PREC,
	   SUM(NBE_CREDIT_STOCK) as STOCK_M_PREC
FROM CTE_CRE_ALL
WHERE ((year*100+Month) <= (DATEPART(YEAR,@DTE_OBS)*100+DATEPART(MONTH,dateadd(MONTH,-1,@DTE_OBS))))
GROUP BY COD_CAN_INT_ORI,LIB_CAN_INT_ORI
)

--Production Encours du mois de l'année précédente
,Cte_Prod_Encours_Stock_M_Annee_Prec as (
SELECT COD_CAN_INT_ORI,
       LIB_CAN_INT_ORI,
	   SUM(CRD) as MONTANT_ENCOURS_M_ANN_PREC
FROM CTE_CRE_ALL
WHERE ((year*100+Month) <= (DATEPART(YEAR,dateadd(YEAR,-1,@DTE_OBS))*100+DATEPART(MONTH,@DTE_OBS)))
GROUP BY COD_CAN_INT_ORI,LIB_CAN_INT_ORI
)

--Fixer les canaux d'interaction d'origine dans le rapport
,CTE_CRE_lib AS (
SELECT DISTINCT [COD_CAN_INT_ORI],
				[LIB_CAN_INT_ORI]
FROM CTE_CRE_ALL
)

select
		lib.COD_CAN_INT_ORI,
		lib.LIB_CAN_INT_ORI,
		ISNULL(SUM(annee.NB_PROD_ANN),0) as NB_PROD_ANN,
		ISNULL(SUM(annee.MONTANT_PROD_ANN),0) as MONTANT_PROD_ANN,
		ISNULL(SUM(mois.MONTANT_ENCOURS_M) ,0) as MONTANT_ENCOURS_M,
		ISNULL(SUM(mois.STOCK_M) ,0) as STOCK_M,
		ISNULL(SUM(mois_prec.MONTANT_ENCOURS_M_PREC) ,0) as MONTANT_ENCOURS_M_PREC,
		ISNULL(SUM(mois_prec.STOCK_M_PREC) ,0) as STOCK_M_PREC,
		ISNULL(SUM(mois_annee_prec.MONTANT_ENCOURS_M_ANN_PREC),0) as MONTANT_ENCOURS_M_ANN_PREC
from CTE_CRE_lib lib 
left join Cte_Prod_Encours_Stock_Annee annee on lib.COD_CAN_INT_ORI = annee.COD_CAN_INT_ORI
left join Cte_Prod_Encours_Stock_M mois on lib.COD_CAN_INT_ORI = mois.COD_CAN_INT_ORI
left join Cte_Prod_Encours_Stock_M_Prec mois_prec on lib.COD_CAN_INT_ORI = mois_prec.COD_CAN_INT_ORI
left join Cte_Prod_Encours_Stock_M_Annee_Prec mois_annee_prec on lib.COD_CAN_INT_ORI = mois_annee_prec.COD_CAN_INT_ORI
group by lib.COD_CAN_INT_ORI,lib.LIB_CAN_INT_ORI

END;