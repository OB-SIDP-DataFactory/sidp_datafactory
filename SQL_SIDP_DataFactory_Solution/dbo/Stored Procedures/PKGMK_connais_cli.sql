﻿CREATE PROC [dbo].[PKGMK_PROC_connais_cli] @nbRows int OUTPUT -- nb lignes processées  
AS  BEGIN 
-- ===============================================================================================================================
--tables input : [$(DataHubDatabaseName)].dbo.VW_PV_SA_Q_CLIENT
--               [$(DataHubDatabaseName)].dbo.VW_PV_SA_Q_COMPTE
--               [$(DataHubDatabaseName)].dbo.VW_PV_SA_Q_CARTE
--               [$(DataHubDatabaseName)].dbo.VW_PV_SF_ACCOUNT
--               [$(DataHubDatabaseName)].dbo.VW_PV_SF_OPPORTUNITY_HISTORY
--               [DIM_TEMPS]
--               [DIM_CSP_CLI]
--               [DIM_CANAL]
--tables output : [sas_vue_cli]
--description : créer une table des clients avec un indicateur agrégé nb_clients et les axes d'analyse (dimensions changeantes)
-- ==============================================================================================================================

---------------------------------------------------------------------------------------------------
-- chargement #wk_connais_cli_init avec les infos à date des clients Orange Bank ayant un CAV
-- comptage des clients : type client SF = "Personne Physique Client" & présence d'un équipement --> pour le moment le CAV
---------------------------------------------------------------------------------------------------
if object_id('tempdb..#wk_connais_cli_init') is not null
begin drop table #wk_connais_cli_init end

--declare @RecordTypeClient varchar(18)
--select @RecordTypeClient = CODE_SF from [dbo].[DIM_RECORDTYPE] where LIBELLE = 'Personne Physique Client'

select sa_q_client.DWHCLICLI as NUM_CLI
     , sf_client.OBFirstContactDate__c as date_ouvert_cli 
     , sa_q_client.DWHCLIDCR as date_ouvert_cli_sab
     , sa_q_client.DWHCLICLO as date_ferm_cli
     , sa_q_client.DWHCLIDNA as date_nais_cli
     , row_number() over (partition by sa_q_client.DWHCLICLI order by sf_opport.CreatedDate, sa_q_compte.DWHCPTDAO, sa_q_carte.DWHCARREM) as FLAG_CLI
     , sa_q_compte.DWHCPTCOM as NUM_CPT
     , sa_q_compte.DWHCPTDAO as date_ouvert_cpt
     , sa_q_compte.DWHCPTDAC as date_ferm_cpt
     , sa_q_carte.DWHCARNUS as NUM_CAR 
     , sa_q_carte.DWHCARCOM + '|' + cast(sa_q_carte.DWHCARNUS as varchar(50)) + '|' + sa_q_carte.DWHCARCAR as ID_CAR 
     , sa_q_carte.type_carte as type_carte 
     , sa_q_carte.DWHCARREM as date_rem_car
     , sa_q_carte.DWHCARVAL as date_val_car 
     , sa_q_carte.car_val_start 
     , sa_q_carte.car_val_end
     , sa_q_carte.car_val_rn
     , sf_client.Id_SF_Account
     , sf_client.IDCustomerSAB__pc as Id_SF_SAB
     , sf_client.Occupation__pc
	 , sf_client.OccupationNiv1__pc 
	 , sf_client.OccupationNiv3__pc 
     , sf_client.OBFirstContactDate__c
     , sf_client.Preattribution__c
     , sf_opport.Id_SF_Opp
     , case when sf_opport.Indication__c='Oui' then sf_opport.StartedChannelForIndication__c else sf_opport.StartedChannel__c end as StartedChannel__c 
     , sf_opport.Indication__c
     , sf_opport.RelationEntryScore__c
	 , sa_q_csl.DWHCPTCOM as Num_compte_CSL
	 , sa_q_csl.DWHCPTDAO as Date_ouverture_CSL
	 , sa_q_csl.DWHCPTDAC as Date_fermeture_CSL
	 , case when sa_q_csl.DWHCPTCOM IS NULL then sa_q_compte.DWHCPTDAC
	        when sa_q_csl.DWHCPTCOM IS NOT NULL and sa_q_compte.DWHCPTDAC>=sa_q_csl.DWHCPTDAC then sa_q_compte.DWHCPTDAC
			when sa_q_csl.DWHCPTCOM IS NOT NULL and sa_q_compte.DWHCPTDAC<sa_q_csl.DWHCPTDAC then sa_q_csl.DWHCPTDAC 
	   end as Date_fermeture_dern_equ
  into #wk_connais_cli_init
  from ( select DWHCLICLI, DWHCLIDNA, DWHCLIDCR, DWHCLICLO
              , row_number() over (partition by DWHCLICLI order by Validity_StartDate desc) as Validity_Flag
           from [$(DataHubDatabaseName)].[dbo].[VW_PV_SA_Q_CLIENT]
       /* where DWHCLIRU2 = 'OBK' pour isoler les clients OBK */ ) sa_q_client

  join ( select DWHCPTPPAL, DWHCPTCOM, DWHCPTRUB, DWHCPTDAO, DWHCPTDAC
              , row_number() over (partition by DWHCPTCOM order by Validity_StartDate desc) as Validity_Flag
           from [$(DataHubDatabaseName)].[dbo].[VW_PV_SA_Q_COMPTE]
          where DWHCPTRUB in ('251180') /* pour isoler les CAV Orange Bank */ ) sa_q_compte
    on sa_q_compte.DWHCPTPPAL = sa_q_client.DWHCLICLI and sa_q_compte.Validity_Flag = sa_q_client.Validity_Flag

  left join ( select DWHCARCOM, DWHCARNUS, DWHCARCAR, DWHCARREM, DWHCARVAL
                   , case when DWHCARCAR = 'WMI' then 'PM' else 'CB' end as type_carte
                   , cast(Validity_StartDate as date) as car_val_start, cast(Validity_EndDate as date) as car_val_end
                   , row_number() over (partition by DWHCARCOM, DWHCARNUS order by Validity_StartDate) as car_val_rn
                from [$(DataHubDatabaseName)].[dbo].[VW_PV_SA_Q_CARTE]
               where DWHCARCET = '002' /* code carte active */ ) sa_q_carte
    on sa_q_carte.DWHCARCOM = sa_q_compte.DWHCPTCOM

  join ( select Id_SF as Id_SF_Account, IDCustomerSAB__pc, Occupation__pc, OccupationNiv1__pc, OccupationNiv3__pc, OBFirstContactDate__c, Preattribution__c, RecordTypeId
                   , row_number() over (partition by Id_SF order by Validity_StartDate desc) as Validity_Flag
                from [$(DataHubDatabaseName)].[dbo].[VW_PV_SF_ACCOUNT] ) sf_client
    on sf_client.IDCustomerSAB__pc = sa_q_client.DWHCLICLI and sf_client.Validity_Flag = sa_q_client.Validity_Flag --and sf_client.RecordTypeId = @RecordTypeClient

  join ( select Id_SF as Id_SF_Opp, AccountId as Id_SF_Account, StartedChannel__c, StartedChannelForIndication__c, Indication__c, RelationEntryScore__c, CreatedDate
                   , row_number() over (partition by Id_SF order by Validity_StartDate desc) as Validity_Flag
                from [$(DataHubDatabaseName)].[dbo].[VW_PV_SF_OPPORTUNITY_HISTORY] 
               where StageName = '09' and CommercialOfferCode__c = 'OC80') sf_opport
    on sf_opport.Id_SF_Account = sf_client.Id_SF_Account and sf_opport.Validity_Flag = sf_client.Validity_Flag

  left join ( select DWHCPTPPAL, DWHCPTCOM, DWHCPTRUB, DWHCPTDAO, DWHCPTDAC
				   , row_number() over (partition by DWHCPTCOM order by Validity_StartDate desc) as Validity_Flag
			from [$(DataHubDatabaseName)].[dbo].[VW_PV_SA_Q_COMPTE]
			where DWHCPTRUB in ('254111') /* pour isoler les CSL Orange Bank */ ) sa_q_csl
            on sa_q_csl.DWHCPTPPAL = sa_q_client.DWHCLICLI and sa_q_csl.Validity_Flag = sa_q_client.Validity_Flag

 where sa_q_client.Validity_Flag = 1;

---------------------------------------------------------------------------------------------------
-- chargement #wk_connais_cli_temps avec la liste des dates à charger
---------------------------------------------------------------------------------------------------
declare @date_min date;
select @date_min = min(cast(date_ouvert_cli as date)) from #wk_connais_cli_init;

declare @date_max date; 
select @date_max = getdate();

if object_id('tempdb..#wk_connais_cli_temps') is not null
begin drop table #wk_connais_cli_temps end

select PK_ID, StandardDate
  into #wk_connais_cli_temps
  from DIM_TEMPS
 where @date_min <= StandardDate and StandardDate < @date_max;

---------------------------------------------------------------------------------------------------
-- ventilation des clients (DWHCLIDCR, DWHCLICLO)
---------------------------------------------------------------------------------------------------
if object_id('tempdb..#wk_connais_cli_stock') is not null
begin drop table #wk_connais_cli_stock end

select d.StandardDate as date_alim
     , o.*

     , DATEDIFF(yy, o.date_nais_cli, d.StandardDate) -
       CASE WHEN MONTH(d.StandardDate) < MONTH(o.date_nais_cli) THEN 1
            WHEN MONTH(d.StandardDate) > MONTH(o.date_nais_cli) THEN 0
            WHEN DAY(d.StandardDate) < DAY(o.date_nais_cli) THEN 1
            ELSE 0 END AS age_cli -- différence en nombre complet d'années

     , CASE WHEN DATEPART(DAY, o.date_ouvert_cli) > DATEPART(DAY, d.StandardDate)
            THEN DATEDIFF(MONTH, o.date_ouvert_cli, d.StandardDate) - 1
            ELSE DATEDIFF(MONTH, o.date_ouvert_cli, d.StandardDate) END AS age_rel_cli -- différence en nombre complet de mois

     , case when d.StandardDate = o.date_ouvert_cli then 1 else 0 end as flag_cli_ouvert
     , case when d.StandardDate = o.Date_fermeture_dern_equ then 1 else 0 end as flag_cli_ferm
     , case when d.StandardDate < o.Date_fermeture_dern_equ or o.Date_fermeture_dern_equ is null then 1 else 0 end as flag_cli_stock
     , 0 as flag_car_pm_rem
     , 0 as flag_car_pm_val
     , 0 as flag_car_pm_stock
     , 0 as flag_car_cb_rem
     , 0 as flag_car_cb_val
     , 0 as flag_car_cb_stock
  into #wk_connais_cli_stock
  from #wk_connais_cli_temps d
  outer apply ( select * from #wk_connais_cli_init where FLAG_CLI = 1 and d.StandardDate >= date_ouvert_cli ) o
;

---------------------------------------------------------------------------------------------------
-- ventilation des cartes (DWHCARREM, DWHCARVAL)
---------------------------------------------------------------------------------------------------
if object_id('tempdb..#wk_connais_cli_car_stock') is not null
begin drop table #wk_connais_cli_car_stock end

select d.StandardDate as date_alim
     , o.*
     , 0 as age_cli
     , 0 as age_rel_cli
     , 0 as flag_cli_ouvert
     , 0 as flag_cli_ferm
     , 0 as flag_cli_stock
     , case when o.type_carte ='PM' and d.StandardDate = o.date_rem_car then 1 else 0 end as flag_car_pm_rem
     , case when o.type_carte ='PM' and d.StandardDate = o.date_val_car then 1 else 0 end as flag_car_pm_val
     , case when o.type_carte ='PM' then 1 else 0 end as flag_car_pm_stock
     , case when o.type_carte ='CB' and d.StandardDate = o.date_rem_car then 1 else 0 end as flag_car_cb_rem
     , case when o.type_carte ='CB' and d.StandardDate = o.date_val_car then 1 else 0 end as flag_car_cb_val
     , case when o.type_carte ='CB' then 1 else 0 end as flag_car_cb_stock
  into #wk_connais_cli_car_stock
  from #wk_connais_cli_temps d
  outer apply ( select * from #wk_connais_cli_init
                 where ( d.StandardDate >= car_val_start and d.StandardDate < car_val_end )
                    or ( cast(date_rem_car as date) <= d.StandardDate and d.StandardDate < cast(car_val_start as date) and car_val_rn = 1 ) ) o
;

---------------------------------------------------------------------------------------------------
-- reconstitution de wk_join_sources_cli
---------------------------------------------------------------------------------------------------
truncate table wk_join_sources_cli;

insert into wk_join_sources_cli
select date_alim
     , cast(date_alim as date) as jour_alim
     , NUM_CLI
     , max(NUM_CPT) as NUM_CPT
     , max(date_nais_cli) as date_nais_cli
     , max(age_cli) as age_cli
     , max(date_ouvert_cli) as date_ouvert_cli
     , max(age_rel_cli) as age_rel_cli
     , max(Id_SF_Account) as Id_SF_Account
     , max(Id_SF_SAB) as Id_SF_SAB
     , max(Occupation__pc) as Occupation__pc
	 , max(OccupationNiv1__pc) as OccupationNiv1__pc
	 , max(OccupationNiv3__pc) as OccupationNiv3__pc
	 , max(OBFirstContactDate__c) as OBFirstContactDate__c
     , max(Preattribution__c) as Preattribution__c
     , max(Id_SF_Opp) as Id_SF_Opp
     , max(StartedChannel__c) as StartedChannel__c
     , max(Indication__c) as Indication__c
     , max(RelationEntryScore__c) as RelationEntryScore__c
     , sum(flag_cli_ouvert) as flag_cli_ouvert
     , sum(flag_cli_ferm) as flag_cli_ferm
     , sum(flag_cli_stock) as flag_cli_stock
     , sum(flag_car_pm_rem) as flag_car_pm_rem
     , sum(flag_car_pm_val) as flag_car_pm_val
     , sum(flag_car_pm_stock) as flag_car_pm_stock
     , sum(flag_car_cb_rem) as flag_car_cb_rem
     , sum(flag_car_cb_val) as flag_car_cb_val
     , sum(flag_car_cb_stock) as flag_car_cb_stock
  from ( select * from #wk_connais_cli_stock
         union all
         select * from #wk_connais_cli_car_stock ) a
group by a.date_alim, a.NUM_CLI
;

---------------------------------------------------------------------------------------------------
-- reconstitution de sas_vue_cli
---------------------------------------------------------------------------------------------------
truncate table sas_vue_cli;

with wk_connais_cli_agg as
( select date_alim
       , age_cli
       , age_rel_cli
--     , date_ouvert_cli
--     , OBFirstContactDate__c
       , StartedChannel__c
       , Occupation__pc
	   , OccupationNiv1__pc
	   , OccupationNiv3__pc
       , RelationEntryScore__c
       , case when (Preattribution__c is not null and charindex('.',Preattribution__c)<>0)  then cast(substring(Preattribution__c,1, charindex('.',Preattribution__c)-1) as int) else Preattribution__c end as Preattribution__c
       , sum(flag_cli_stock) as nb_clients
       , sum(flag_cli_ouvert) as nb_clients_ouvert
       , sum(flag_cli_ferm) as nb_clients_ferm
       , case when flag_car_pm_stock > 0 then 1 else 0 end as flag_pm
       , case when flag_car_cb_stock > 0 then 1 else 0 end as flag_cb
       , sum(flag_car_pm_rem) as flag_pm_rem
       , sum(flag_car_cb_rem) as flag_cb_rem
       , sum(case when flag_car_pm_stock > 0 and flag_car_cb_stock > 0 then flag_cli_stock else 0 end) as flag_cli_eq_pm_cb
       , sum(case when flag_car_pm_stock > 0 and flag_car_cb_stock = 0 then flag_cli_stock else 0 end) as flag_cli_eq_pm
       , sum(case when flag_car_pm_stock = 0 and flag_car_cb_stock > 0 then flag_cli_stock else 0 end) as flag_cli_eq_cb
       , sum(case when flag_car_pm_stock = 0 and flag_car_cb_stock = 0 then flag_cli_stock else 0 end) as flag_cli_neq_pm_cb
    from wk_join_sources_cli
  group by date_alim, AGE_CLI, AGE_REL_CLI, /*date_ouvert_cli, OBFirstContactDate__c,*/ StartedChannel__c, Occupation__pc, OccupationNiv1__pc, OccupationNiv3__pc, RelationEntryScore__c, Preattribution__c, case when flag_car_pm_stock  > 0 then 1 else 0 end, case when flag_car_cb_stock  > 0 then 1 else 0 end
)

insert into sas_vue_cli
select d3.Date as date_obs_stock_cli
     , d3.StandardDate as jour_alim
     , t.nb_clients
     , t.nb_clients_ouvert
     , t.nb_clients_ferm
     , t.age_cli
     , t.age_rel_cli
--   , d3c.StandardDate as date_creation_cli
     , t.RelationEntryScore__c
     , t.Preattribution__c
     , isnull(d2.CSP_CLI, 'Non renseigné') as csp_cli
	 , isnull(dH1.LIB_CSP_NIV1, 'Non renseigné') as csp_cli_niv1
	 , isnull(dH3.LIB_CSP_NIV3, 'Non renseigné') as csp_cli_niv3
     , isnull(d7.LIBELLE, 'Non renseigné') as canal_origine
     , t.flag_pm
     , t.flag_cb
     , t.flag_pm_rem
     , t.flag_cb_rem
     , t.flag_cli_eq_pm_cb
     , t.flag_cli_eq_pm
     , t.flag_cli_eq_cb
     , t.flag_cli_neq_pm_cb
  from wk_connais_cli_agg t
  join dbo.[DIM_TEMPS] d3
    on t.date_alim = d3.StandardDate
  left join dbo.[DIM_CSP_CLI] d2 
    on t.Occupation__pc = d2.CSP_CLI and t.date_alim  BETWEEN d2.Validity_StartDate and d2.Validity_EndDate

  left join dbo.[DIM_CSP_NIV1] dH1 
	on t.OccupationNiv1__pc = dH1.COD_CSP_NIV1  

  left join dbo.[DIM_CSP_NIV3] dH3 
	on t.OccupationNiv3__pc = dH3.COD_CSP_NIV3 

  left join dbo.[DIM_CANAL] d7
    on ( t.StartedChannel__c = d7.CODE_SF and d7.PARENT_ID is not null /* niveau canal interaction */ and t.date_alim  BETWEEN d7.Validity_StartDate and d7.Validity_EndDate )
--join dbo.[DIM_TEMPS] d3c
--  on t.date_ouvert_cli = d3c.StandardDate
; 

select @nbRows = @@ROWCOUNT;

if object_id('tempdb..#wk_connais_cli_init') is not null
begin drop table #wk_connais_cli_init end

if object_id('tempdb..#wk_connais_cli_stock') is not null
begin drop table #wk_connais_cli_stock end

if object_id('tempdb..#wk_connais_cli_car_stock') is not null
begin drop table #wk_connais_cli_car_stock end

EXEC [dbo].[PROC_SSRS_SAS_VUE_CLI];
END
GO