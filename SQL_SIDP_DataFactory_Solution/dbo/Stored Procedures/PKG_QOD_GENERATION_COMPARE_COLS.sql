﻿CREATE PROCEDURE [dbo].[PKG_QOD_GENERATION_COMPARE_COLS]
-- Parametres table_source, table_cible
@table_source as varchar(200) = NULL,
@table_cible as varchar(200) = NULL

AS
BEGIN

DECLARE 

-- Variables locales

@sql_qod nvarchar(max),
@cursor_table_name varchar(200),
@cursor_col_name varchar(200),
@cursor_col_type varchar(200),
@cursor_col_len numeric,
@cursor_id int = 0
---- Curseur sur les colonnes d'une table sur laquelle on veut effectuer le contrôle
--DECLARE col_cursor CURSOR for 

--select TABLE_NAME, COLUMN_NAME, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH
--from INFORMATION_SCHEMA.COLUMNS 
--where TABLE_NAME = @table_cible  

-- --Boucle sur les colonnes
-- OPEN col_cursor

-- FETCH NEXT FROM col_cursor INTO @cursor_table_name, @cursor_col_name, @cursor_col_type , @cursor_col_len

--WHILE @@FETCH_STATUS = 0
--BEGIN 
--SET @cursor_id = @cursor_id + 1

--SET @sql_qod = (SELECT CASE @cursor_col_type 
--WHEN 'varchar' THEN 
--	'select src_len as output_col1, cib_len as output_col2, null as output_col3, null as output_col4, null as output_col5, null as output_col6,
--	null as output_col7 from ((select CHARACTER_MAXIMUM_LENGTH as src_len from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME=''' + @table_source + ''' and COLUMN_NAME=''' + @cursor_col_name
--	+ ''') src join (select CHARACTER_MAXIMUM_LENGTH as cib_len from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME=''' + @table_cible + ''' and COLUMN_NAME=''' + @cursor_col_name + ''') cible
--	on src.src_len < cible.cib_len)'

--WHEN 'nvarchar' THEN
--	'select src_len as output_col1, cib_len as output_col2, null as output_col3, null as output_col4, null as output_col5, null as output_col6,
--	null as output_col7 from ((select CHARACTER_MAXIMUM_LENGTH as src_len from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME=''' + @table_source + ''' and COLUMN_NAME=''' + @cursor_col_name
--	+ ''') src join (select CHARACTER_MAXIMUM_LENGTH as cib_len from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME=''' + @table_cible + ''' and COLUMN_NAME=''' + @cursor_col_name + ''') cible
--	on src.src_len < cible.cib_len)'

--ELSE
	
--   'select src_typ as output_col1, cib_typ as output_col2, null as output_col3, null as output_col4, null as output_col5, null as output_col6,
--	null as output_col7 from ((select DATA_TYPE as src_typ from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME=''' + @table_source + ''' and COLUMN_NAME=''' + @cursor_col_name
--	+ ''') src join (select CHARACTER_MAXIMUM_LENGTH as cib_typ from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME=''' + @table_cible + ''' and COLUMN_NAME=''' + @cursor_col_name + ''') cible
--	on src.src_typ != cible.cib_typ)'

--END
--)

--INSERT INTO [dbo].[SIDP_QOD_LISTE_CONTROLE]
--VALUES
--(
--	'DataHub_CONTROL_' + @table_cible + '_' + cast(@cursor_id as char),
--	'1- Bloquant',
--	'Comparaison type / longueur ' + @table_source + '.' + @cursor_col_name + 'et ' + @table_cible + '.' + @cursor_col_name,
--	'Ce contrôle permet la comparaison de type / longueur entre' +  @table_source + '.' + @cursor_col_name + 'et ' + @table_cible + '.' + @cursor_col_name,
--	@sql_qod,
--	'DATAHUB',
--	'Contrôle' + @table_cible,
--	'''Technique - Contrôle intégration',
--	'Différence de type / longueur entre ' + @table_source + ' et ' + @table_cible + ' et ',
--	NULL,
--	NULL,
--	NULL,
--	NULL,
--	NULL,
--	NULL,
--	NULL,
--	1,
--	NULL
--)
--FETCH NEXT FROM col_cursor INTO @cursor_table_name, @cursor_col_name, @cursor_col_type , @cursor_col_len
--END   

--fin de la procedure
END