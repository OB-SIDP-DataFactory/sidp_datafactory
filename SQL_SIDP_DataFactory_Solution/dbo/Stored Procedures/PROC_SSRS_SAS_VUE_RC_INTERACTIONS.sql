﻿CREATE PROC [dbo].[PROC_SSRS_SAS_VUE_RC_INTERACTIONS]

AS  BEGIN 

DECLARE @date_obs DATE;
SELECT  @date_obs = CAST(GETDATE() AS DATE);
DECLARE @date_max date;
SELECT  @date_max = dateadd(day, -1 ,GETDATE());
SET DATEFIRST 1; -->1    Lundi
DROP TABLE IF EXISTS #rq_cli_int_cp;
DROP TABLE IF EXISTS [dbo].[WK_SSRS_SAS_VUE_RC_INTERACTIONS];

SELECT Year_alim,Month_alim,Week_alim,jour_alim
    ,nb_interactions
	,nb_interactions_tel
	,nb_interactions_mail
	,nb_interactions_sms
	,nb_interactions_chat_crc
	,nb_interactions_chat_ia
	--Week to date
	,sum(nb_interactions) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_interactions_WTD --Week to date
	,sum(nb_interactions_tel) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_interactions_tel_WTD --Week to date
	,sum(nb_interactions_mail) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_interactions_mail_WTD --Week to date
	,sum(nb_interactions_sms) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_interactions_sms_WTD --Week to date
	,sum(nb_interactions_chat_crc) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_interactions_chat_crc_WTD --Week to date
	,sum(nb_interactions_chat_ia) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_interactions_chat_ia_WTD --Week to date
	
	--Month to date
	,sum(nb_interactions) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_interactions_MTD --Month to date
	,sum(nb_interactions_tel) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_interactions_tel_MTD --Month to date
	,sum(nb_interactions_mail) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_interactions_mail_MTD --Month to date
	,sum(nb_interactions_sms) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_interactions_sms_MTD --Month to date
	,sum(nb_interactions_chat_crc) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_interactions_chat_crc_MTD --Month to date
	,sum(nb_interactions_chat_ia) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_interactions_chat_ia_MTD --Month to date
	
	
	--Full Month
	,sum(nb_interactions) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_interactions_FM --Full Month
	,sum(nb_interactions_tel) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_interactions_tel_FM --Full Month
	,sum(nb_interactions_mail) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_interactions_mail_FM --Full Month
	,sum(nb_interactions_sms) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_interactions_sms_FM --Full Month
	,sum(nb_interactions_chat_crc) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_interactions_chat_crc_FM --Full Month
	,sum(nb_interactions_chat_ia) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_interactions_chat_ia_FM --Full Month
	
	--Year to date
    ,sum(nb_interactions) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_interactions_YTD  --Year to date
    ,sum(nb_interactions_tel) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_interactions_tel_YTD  --Year to date
    ,sum(nb_interactions_mail) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_interactions_mail_YTD  --Year to date
    ,sum(nb_interactions_sms) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_interactions_sms_YTD  --Year to date
    ,sum(nb_interactions_chat_crc) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_interactions_chat_crc_YTD  --Year to date
    ,sum(nb_interactions_chat_ia) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_interactions_chat_ia_YTD  --Year to date
   
	--Full year
	,sum(nb_interactions) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_interactions_FY  --Full year
	,sum(nb_interactions_tel) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_interactions_tel_FY  --Full year
	,sum(nb_interactions_mail) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_interactions_mail_FY  --Full year
	,sum(nb_interactions_sms) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_interactions_sms_FY  --Full year
	,sum(nb_interactions_chat_crc) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_interactions_chat_crc_FY  --Full year
	,sum(nb_interactions_chat_ia) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_interactions_chat_ia_FY  --Full year
INTO #rq_cli_int_cp
FROM  ( 
     SELECT  T1.StandardDate as jour_alim,DATEPART(YEAR,T1.Date) AS Year_alim,DATEPART(MONTH,T1.Date) AS Month_alim,DATEPART(YEAR,DATEADD(day, (7 - DATEPART(dw, T1.StandardDate)), T1.StandardDate)) *100 + DATEPART(WEEK,DATEADD(day, (7 - DATEPART(dw, T1.StandardDate)), T1.StandardDate)) AS Week_alim
	, sum(case when interaction_type='Téléphonie'  then 1 else 0 end)+sum(case when interaction_type='SMS' then 1 else 0 end)+sum(case when interaction_sous_type='Chat IA'  then 1 else 0 end)+sum(case when interaction_type='Email'  then 1 else 0 end) AS nb_interactions
    , sum(case when interaction_type='Téléphonie'  then 1 else 0 end) AS nb_interactions_tel
    , sum(case when interaction_type='Email'  then 1 else 0 end) AS nb_interactions_mail
    , sum(case when interaction_type='SMS' then 1 else 0 end) AS nb_interactions_sms
    , sum(case when interaction_sous_type='Chat CRC'  then 1 else 0 end) AS nb_interactions_chat_crc
    , sum(case when interaction_sous_type='Chat IA'  then 1 else 0 end) AS nb_interactions_chat_ia
FROM [dbo].[DIM_TEMPS] T1 
LEFT OUTER JOIN  SAS_Vue_RC_Interactions  ON T1.StandardDate=SAS_Vue_RC_Interactions.StandardDate
WHERE  ( T1.StandardDate >= CAST(DATEADD(yyyy, DATEDIFF(YYYY, 0, DATEADD(yyyy, -2, @date_obs)), 0) AS DATE)
AND T1.StandardDate <=  DATEADD(day, (7 - DATEPART(dw, @date_obs)), @date_obs) )
GROUP BY  T1.StandardDate ,DATEPART(YEAR,T1.Date) ,DATEPART(MONTH,T1.Date) ,DATEPART(WEEK,T1.Date) 
) t;

SELECT
  CY.jour_alim
, CY.Year_alim
, CY.Month_alim
, CY.Week_alim
, coalesce(CY.nb_interactions,0) AS nb_interactions
, coalesce(CY.nb_interactions_WTD,0) AS nb_interactions_WTD
, coalesce(CY.nb_interactions_MTD,0) AS nb_interactions_MTD
, coalesce(CY.nb_interactions_YTD,0) AS nb_interactions_YTD
, coalesce(PM.nb_interactions_FM,0) AS nb_interactions_FM
, coalesce(PY.nb_interactions_FY,0) AS nb_interactions_FY
, coalesce(CY.nb_interactions_tel,0) AS nb_interactions_tel
, coalesce(CY.nb_interactions_tel_WTD,0) AS nb_interactions_tel_WTD
, coalesce(CY.nb_interactions_tel_MTD,0) AS nb_interactions_tel_MTD
, coalesce(CY.nb_interactions_tel_YTD,0) AS nb_interactions_tel_YTD
, coalesce(PM.nb_interactions_tel_FM,0) AS nb_interactions_tel_FM
, coalesce(PY.nb_interactions_tel_FY,0) AS nb_interactions_tel_FY
, coalesce(CY.nb_interactions_mail,0) AS nb_interactions_mail
, coalesce(CY.nb_interactions_mail_WTD,0) AS nb_interactions_mail_WTD
, coalesce(CY.nb_interactions_mail_MTD,0) AS nb_interactions_mail_MTD
, coalesce(CY.nb_interactions_mail_YTD,0) AS nb_interactions_mail_YTD
, coalesce(PM.nb_interactions_mail_FM,0) AS nb_interactions_mail_FM
, coalesce(PY.nb_interactions_mail_FY,0) AS nb_interactions_mail_FY
, coalesce(CY.nb_interactions_sms,0) AS nb_interactions_sms
, coalesce(CY.nb_interactions_sms_WTD,0) AS nb_interactions_sms_WTD
, coalesce(CY.nb_interactions_sms_MTD,0) AS nb_interactions_sms_MTD
, coalesce(CY.nb_interactions_sms_YTD,0) AS nb_interactions_sms_YTD
, coalesce(PM.nb_interactions_sms_FM,0) AS nb_interactions_sms_FM
, coalesce(PY.nb_interactions_sms_FY,0) AS nb_interactions_sms_FY
, coalesce(CY.nb_interactions_chat_crc,0) AS nb_interactions_chat_crc
, coalesce(CY.nb_interactions_chat_crc_WTD,0) AS nb_interactions_chat_crc_WTD
, coalesce(CY.nb_interactions_chat_crc_MTD,0) AS nb_interactions_chat_crc_MTD
, coalesce(CY.nb_interactions_chat_crc_YTD,0) AS nb_interactions_chat_crc_YTD
, coalesce(PM.nb_interactions_chat_crc_FM,0) AS nb_interactions_chat_crc_FM
, coalesce(PY.nb_interactions_chat_crc_FY,0) AS nb_interactions_chat_crc_FY
, coalesce(CY.nb_interactions_chat_ia,0) AS nb_interactions_chat_ia
, coalesce(CY.nb_interactions_chat_ia_WTD,0) AS nb_interactions_chat_ia_WTD
, coalesce(CY.nb_interactions_chat_ia_MTD,0) AS nb_interactions_chat_ia_MTD
, coalesce(CY.nb_interactions_chat_ia_YTD,0) AS nb_interactions_chat_ia_YTD
, coalesce(PM.nb_interactions_chat_ia_FM,0) AS nb_interactions_chat_ia_FM
, coalesce(PY.nb_interactions_chat_ia_FY,0) AS nb_interactions_chat_ia_FY
INTO [dbo].[WK_SSRS_SAS_VUE_RC_INTERACTIONS]
FROM #rq_cli_int_cp CY --Current Year
 LEFT OUTER JOIN #rq_cli_int_cp PM --Previous Year
	 --IMPORTANT: Due to the LEFT OUTER JOIN, the first year will not be returned in the final resultset.
		  ON DATEADD(MONTH,-1,CY.jour_alim) = PM.jour_alim
	 LEFT OUTER JOIN #rq_cli_int_cp PY --Previous Year
	 --IMPORTANT: Due to the LEFT OUTER JOIN, the first year will not be returned in the final resultset.
		  ON DATEADD(YEAR,-1,CY.jour_alim) = PY.jour_alim
ORDER BY CY.jour_alim;
END