﻿CREATE PROCEDURE [dbo].[PKGES_PROC_SYNTHESE_OF]

AS BEGIN

select T.IndicationID,
	   T.Indication,
	   T.CloseDate_YearMonth,
	   sum(TotalFinalCloturé) as TotalCloturé
from (
Select StageName,FORMAT([CloseDate], 'MM/yyyy') as CloseDate_YearMonth,
case  when   flag_indication='Oui' and DistributorEntity__c='01' then 2
      when  flag_indication='Oui' and DistributorEntity__c='02' then 3
      when  flag_indication='Oui' and DistributorEntity__c='04' then 4
      when  flag_indication='Non' and DistributorEntity__c='04' then 5
      when  flag_indication='Non'   and DistributorEntity__c in ('01','02') then 1 end as IndicationID,

case  when   flag_indication='Oui' and DistributorEntity__c='01' then 'Indication Boutique AD'
      when  flag_indication='Oui' and DistributorEntity__c='02' then 'Indication Boutique GDT'
      when  flag_indication='Oui' and DistributorEntity__c='04' then 'Indication Digitale'
      when  flag_indication='Non' and DistributorEntity__c='04' then 'Renvoi Trafic'
      when  flag_indication='Non'   and DistributorEntity__c in ('01','02') then 'Souscription IOBSP' end as Indication,
-- Finalisé
case when LeadSource in ('02','03','10','11') then count(distinct Wk.Id_SF) else 0 end as  TotalFinalCloturé
from [dbo].[WK_STOCK_OPPORTUNITY] as Wk
left outer join  [$(DataHubDatabaseName)].[dbo].[PV_SF_OPPORTUNITYISDELETED] as Wkd on Wk.Id_SF= Wkd.Id_SF
where Wkd.Id_SF is null and DistributorNetwork__c='01' --Orange France
and CommercialOfferCode__c='OC80' and StageName ='09'
and [CloseDate] between  DATEADD(YEAR,-1,GETDATE()) and GETDATE()
and DistributorEntity__c IN ('01','02','04')
and flag_opport_last_state = 1
and [date_alim]=(select max([date_alim]) from [dbo].[WK_STOCK_OPPORTUNITY]) 
group by flag_indication,[CloseDate],DistributorEntity__c,StageName,LeadSource) as T
group by  T.IndicationID,T.Indication,T.CloseDate_YearMonth
order by T.IndicationID

END;
