﻿-- =============================================
--tables input : [SIDP_DataFactory].[dbo].[WK_MC_CIBLES]
--				 [$(DataHubDatabaseName)].[dbo].[PV_SF_ACCOUNTLINK]
--              [$(DataHubDatabaseName)].[exposition].[PV_SF_ACCOUNT]
--				[$(DataHubDatabaseName)].[exposition].[PV_SF_OPPORTUNITY]
--				 [SIDP_DataFactory].[dbo].[DIM_STADE_VTE] 
--tables output : [SAS_Vue_MC_Filleul]
--description : construction d'un data set utilisé pour le section parrainage du rapport MC_Rapport (avec Parrainage)
-- =============================================
CREATE PROCEDURE [dbo].[PKGMC_PROC_vue_filleul] @nbRows int OUTPUT
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

truncate table [dbo].SAS_Vue_MC_Filleul;

WITH CONTACTS AS (
select
	[a].[ID_contact_cibles], [a].[Date_1st], [a].[Nb_Contacts], [a].[Nb_Emails], [a].[Nb_SMS], [a].[Nb_Push], [a].[Nb_Call],
	isnull(sdv.COD_STA_VTE+' - '+sdv.LIB_STA_VTE,'Non-renseigné') as Stade_vente_debut
from (    
	select ID_CONTACT as ID_contact_cibles
		   , min(DATE) as Date_1st
		   , count(*) as Nb_Contacts
		   , sum(case when CANAL = 'Email' then 1 else 0 end) as Nb_Emails
		   , sum(case when CANAL = 'SMS'   then 1 else 0 end) as Nb_SMS
		   , sum(case when CANAL = 'Push'  then 1 else 0 end) as Nb_Push
		   , sum(case when CANAL = 'Call'  then 1 else 0 end) as Nb_Call		  
	from [dbo].[WK_MC_CIBLES]
	group by ID_CONTACT  
	   )a
inner join [dbo].[WK_MC_CIBLES] b on a.ID_contact_cibles = b.ID_CONTACT and a.Date_1st = b.DATE
left join [dbo].[DIM_STADE_VTE] sdv on sdv.COD_STA_VTE = b.STATUT_OPPORT
)
insert into [dbo].SAS_Vue_MC_Filleul
select distinct *
from (

	SELECT distinct
		  (case when PersonRole1__c = '10' then acc1.IDE_CONTACT_SF when [PersonRole2__c] = '10' then acc2.IDE_CONTACT_SF end) as ID_Contact_lk
		 ,(case when PersonRole1__c = '10' then ISNULL(acc1.LIB_SCO_COMP,'Non-renseigné') when [PersonRole2__c] = '10' then ISNULL(acc2.LIB_SCO_COMP,'Non-renseigné') end) as Score_comportement
		 ,(case when PersonRole1__c = '10' then isnull(acc1.SEG_GEOLIFE,'Non-renseigné') when [PersonRole2__c] = '10' then isnull(acc2.SEG_GEOLIFE,'Non-renseigné') end) as Segment_Geolife
		 ,(case when PersonRole1__c = '10' then (case when (acc1.SCO_PREAT is not null and charindex('.',acc1.SCO_PREAT)<>0)  then cast(substring(acc1.SCO_PREAT,1, charindex('.',acc1.SCO_PREAT)-1) as int) else acc1.SCO_PREAT end) when [PersonRole2__c] = '10' then (case when (acc2.SCO_PREAT is not null and charindex('.',acc2.SCO_PREAT)<>0)  then cast(substring(acc2.SCO_PREAT,1, charindex('.',acc2.SCO_PREAT)-1) as int) else acc2.SCO_PREAT end) end) as Pre_attribution
		 ,(case when PersonRole1__c = '10' then DATEDIFF(YEAR,acc1.DTE_NAIS,SYSDATETIME()) when [PersonRole2__c] = '10' then DATEDIFF(YEAR,acc2.DTE_NAIS,SYSDATETIME()) end) as Age
		 ,(case when PersonRole1__c = '10' then cast(opp1.SCO_ENT_REL as integer) when [PersonRole2__c] = '10' then cast(opp2.SCO_ENT_REL as integer) end) as Score_entree_relation
		 ,(case when PersonRole1__c = '10' then isnull(opp1.FLG_IND,'Non-renseigné') when [PersonRole2__c] = '10' then isnull(opp2.FLG_IND,'Non-renseigné') end) as Indication
		 ,(case when PersonRole1__c = '10' then opp1.LIB_CAN_INT_ORI when [PersonRole2__c] = '10' then opp2.LIB_CAN_INT_ORI end) as Canal_interaction_orig
		 ,(case when PersonRole1__c = '10' then opp1.LIB_RES_DIS when [PersonRole2__c] = '10' then opp2.LIB_RES_DIS end) as Reseau_distrib
		 ,(case when PersonRole1__c = '10' then opp1.LIB_ENT_DIS when [PersonRole2__c] = '10' then opp2.LIB_ENT_DIS end) as Entite_distrib
		 ,(case when PersonRole1__c = '10' and opp1.LIB_RES_DIS = 'Orange Bank' and opp1.LIB_CAN_INT_ORI in ('Appli OB','Web OB') then 'OB_full_selfcare' when PersonRole2__c = '10' and opp2.LIB_RES_DIS = 'Orange Bank' and opp2.LIB_CAN_INT_ORI in ('Appli OB','Web OB') then 'OB_full_selfcare'
      		  when PersonRole1__c = '10' and opp1.LIB_RES_DIS = 'Orange Bank' and opp1.LIB_CAN_INT_ORI in ('Appel entrant CRC', 'Appel sortant CRC') then 'OB_CRC' when PersonRole2__c = '10' and opp2.LIB_RES_DIS = 'Orange Bank' and opp2.LIB_CAN_INT_ORI in ('Appel entrant CRC', 'Appel sortant CRC') then 'OB_CRC'
			  when PersonRole1__c = '10' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_DOS_COMP = '1' then 'Souscription boutique' when PersonRole2__c = '10' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_DOS_COMP = '1' then 'Souscription boutique'
			  when PersonRole1__c = '10' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_DOS_COMP <> '1' and opp1.NUM_PROC_SOUS is not null then 'Repli Selfcare' when PersonRole2__c = '10' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_DOS_COMP <> '1' and opp2.NUM_PROC_SOUS is not null then 'Repli Selfcare'
			  when PersonRole1__c = '10' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_IND = 'OUI' and opp1.NUM_PROC_SOUS is null and opp1.LIB_ENT_DIS in ('AD', 'GDT')  then 'Indication boutique' when PersonRole2__c = '10' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_IND = 'OUI' and opp2.NUM_PROC_SOUS is null and opp2.LIB_ENT_DIS in ('AD', 'GDT')  then 'Indication boutique'
			  when PersonRole1__c = '10' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_IND = 'OUI' and opp1.NUM_PROC_SOUS is null and opp1.LIB_ENT_DIS in ('OF-WEB')  then 'Indication Digitale' when PersonRole2__c = '10' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_IND = 'OUI' and opp2.NUM_PROC_SOUS is null and opp2.LIB_ENT_DIS in ('OF-WEB') then 'Indication Digitale'
			  when PersonRole1__c = '10' and opp1.LIB_RES_DIS = 'Orange France' and opp1.LIB_CAN_INT_ORI = 'Web OB' and opp1.LIB_ENT_DIS in ('OF-WEB')  then 'Renvoi trafic' when PersonRole2__c = '10' and opp2.LIB_RES_DIS = 'Orange France' and opp2.LIB_CAN_INT_ORI = 'Web OB' and opp2.LIB_ENT_DIS in ('OF-WEB')  then 'Renvoi trafic'
		  end) as Reseau_apporteur
		 ,(case when PersonRole1__c = '10' then isnull(acc1.QUA_EMP,'Non-renseigné') when [PersonRole2__c] = '10' then isnull(acc2.QUA_EMP,'Non-renseigné') end) as Qualification_employe

		 ,(case when PersonRole1__c = '10' then opp1.NUM_OPPRT when [PersonRole2__c] = '10' then opp2.NUM_OPPRT end) as Num_opp
		 ,(case when PersonRole1__c = '10' then opp1.LIB_STADE_VENTE when [PersonRole2__c] = '10' then opp2.LIB_STADE_VENTE end) as Stade_vente_fin
	FROM [$(DataHubDatabaseName)].[dbo].[PV_SF_ACCOUNTLINK] acclk
	INNER JOIN [$(DataHubDatabaseName)].[exposition].[PV_SF_ACCOUNT] acc1 on acclk.Person1__c = acc1.IDE_PERS_SF
	INNER JOIN [$(DataHubDatabaseName)].[exposition].[PV_SF_OPPORTUNITY] opp1 on opp1.IDE_PERS_SF = acc1.IDE_PERS_SF
	INNER JOIN [$(DataHubDatabaseName)].[exposition].[PV_SF_ACCOUNT] acc2 on acclk.Person2__c = acc2.IDE_PERS_SF
	INNER JOIN [$(DataHubDatabaseName)].[exposition].[PV_SF_OPPORTUNITY] opp2 on opp2.IDE_PERS_SF = acc2.IDE_PERS_SF
 

	WHERE LinkType__c = '06'
	and PersonRole1__c in ('09','10')
	and PersonRole2__c in ('09','10')
	and acc1.DTE_FIN_VAL = '9999-12-31 00:00:00.0000000'
	and acc2.DTE_FIN_VAL = '9999-12-31 00:00:00.0000000'
	and (case when PersonRole1__c = '10' and opp1.LIB_RES_DIS = 'Orange Bank' and opp1.LIB_CAN_INT_ORI in ('Appli OB','Web OB') then 'OB_full_selfcare' when PersonRole2__c = '10' and opp2.LIB_RES_DIS = 'Orange Bank' and opp2.LIB_CAN_INT_ORI in ('Appli OB','Web OB') then 'OB_full_selfcare'
      		when PersonRole1__c = '10' and opp1.LIB_RES_DIS = 'Orange Bank' and opp1.LIB_CAN_INT_ORI in ('Appel entrant CRC', 'Appel sortant CRC') then 'OB_CRC' when PersonRole2__c = '10' and opp2.LIB_RES_DIS = 'Orange Bank' and opp2.LIB_CAN_INT_ORI in ('Appel entrant CRC', 'Appel sortant CRC') then 'OB_CRC'
			when PersonRole1__c = '10' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_DOS_COMP = '1' then 'Souscription boutique' when PersonRole2__c = '10' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_DOS_COMP = '1' then 'Souscription boutique'
			when PersonRole1__c = '10' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_DOS_COMP <> '1' and opp1.NUM_PROC_SOUS is not null then 'Repli Selfcare' when PersonRole2__c = '10' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_DOS_COMP <> '1' and opp2.NUM_PROC_SOUS is not null then 'Repli Selfcare'
			when PersonRole1__c = '10' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_IND = 'OUI' and opp1.NUM_PROC_SOUS is null and opp1.LIB_ENT_DIS in ('AD', 'GDT')  then 'Indication boutique' when PersonRole2__c = '10' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_IND = 'OUI' and opp2.NUM_PROC_SOUS is null and opp2.LIB_ENT_DIS in ('AD', 'GDT')  then 'Indication boutique'
			when PersonRole1__c = '10' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_IND = 'OUI' and opp1.NUM_PROC_SOUS is null and opp1.LIB_ENT_DIS in ('OF-WEB')  then 'Indication Digitale' when PersonRole2__c = '10' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_IND = 'OUI' and opp2.NUM_PROC_SOUS is null and opp2.LIB_ENT_DIS in ('OF-WEB') then 'Indication Digitale'
			when PersonRole1__c = '10' and opp1.LIB_RES_DIS = 'Orange France' and opp1.LIB_CAN_INT_ORI = 'Web OB' and opp1.LIB_ENT_DIS in ('OF-WEB')  then 'Renvoi trafic' when PersonRole2__c = '10' and opp2.LIB_RES_DIS = 'Orange France' and opp2.LIB_CAN_INT_ORI = 'Web OB' and opp2.LIB_ENT_DIS in ('OF-WEB')  then 'Renvoi trafic'
	end) is not null
	and (case when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange Bank' and opp1.LIB_CAN_INT_ORI in ('Appli OB','Web OB') then 'OB_full_selfcare' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange Bank' and opp2.LIB_CAN_INT_ORI in ('Appli OB','Web OB') then 'OB_full_selfcare'
      		when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange Bank' and opp1.LIB_CAN_INT_ORI in ('Appel entrant CRC', 'Appel sortant CRC') then 'OB_CRC' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange Bank' and opp2.LIB_CAN_INT_ORI in ('Appel entrant CRC', 'Appel sortant CRC') then 'OB_CRC'
			when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_DOS_COMP = '1' then 'Souscription boutique' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_DOS_COMP = '1' then 'Souscription boutique'
			when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_DOS_COMP <> '1' and opp1.NUM_PROC_SOUS is not null then 'Repli Selfcare' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_DOS_COMP <> '1' and opp2.NUM_PROC_SOUS is not null then 'Repli Selfcare'
			when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_IND = 'OUI' and opp1.NUM_PROC_SOUS is null and opp1.LIB_ENT_DIS in ('AD', 'GDT')  then 'Indication boutique' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_IND = 'OUI' and opp2.NUM_PROC_SOUS is null and opp2.LIB_ENT_DIS in ('AD', 'GDT')  then 'Indication boutique'
			when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange France' and opp1.FLG_IND = 'OUI' and opp1.NUM_PROC_SOUS is null and opp1.LIB_ENT_DIS in ('OF-WEB')  then 'Indication Digitale' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange France' and opp2.FLG_IND = 'OUI' and opp2.NUM_PROC_SOUS is null and opp2.LIB_ENT_DIS in ('OF-WEB') then 'Indication Digitale'
			when PersonRole1__c = '09' and opp1.LIB_RES_DIS = 'Orange France' and opp1.LIB_CAN_INT_ORI = 'Web OB' and opp1.LIB_ENT_DIS in ('OF-WEB')  then 'Renvoi trafic' when PersonRole2__c = '09' and opp2.LIB_RES_DIS = 'Orange France' and opp2.LIB_CAN_INT_ORI = 'Web OB' and opp2.LIB_ENT_DIS in ('OF-WEB')  then 'Renvoi trafic'
	end) is not null
	and (case when PersonRole1__c = '10' then opp1.EVE_ORI when [PersonRole2__c] = '10' then opp2.EVE_ORI end) = '08'
	and opp1.LABEL_OFF_COM = 'Compte bancaire' 
	and opp2.LABEL_OFF_COM = 'Compte bancaire'
	)a

left join CONTACTS FC ON a.ID_Contact_lk = FC.ID_contact_cibles
    -- Insert statements for procedure here
	SELECT @nbRows = COUNT(*) FROM SAS_Vue_MC_Filleul;
END