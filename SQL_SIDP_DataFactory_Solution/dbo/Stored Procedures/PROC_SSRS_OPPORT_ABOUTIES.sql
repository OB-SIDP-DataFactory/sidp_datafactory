﻿CREATE PROC [dbo].[PROC_SSRS_OPPORT_ABOUTIES]

AS  BEGIN 

DECLARE @date_obs DATE;
SELECT  @date_obs = CAST(GETDATE() AS DATE);
DECLARE @date_max date;
SELECT  @date_max = dateadd(day, -1 ,GETDATE());
SET DATEFIRST 1; -->1    Lundi
DROP TABLE IF EXISTS #rq_opp_abouties;
DROP TABLE IF EXISTS [dbo].[WK_SSRS_OPPORT_ABOUTIES];

SELECT Year_alim,Month_alim,Week_alim,jour_alim
    ,nb_opport_abouties
    ,nb_opport_abouties_OF
	,nb_opport_full_boutiques
	,nb_opport_repli_selfcare
	,nb_opport_indication_boutique
	,nb_opport_indication_digitale
	,nb_opport_renvoi_trafic
	,nb_opport_abouties_OB
	,nb_opport_abouties_Web_app
	,nb_opport_abouties_CRC
	--Week to date
	,sum(nb_opport_abouties_OF) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_abouties_OF_WTD --Week to date
	,sum(nb_opport_full_boutiques) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_full_boutiques_WTD --Week to date
	,sum(nb_opport_repli_selfcare) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_repli_selfcare_WTD --Week to date
	,sum(nb_opport_indication_boutique) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_indication_boutique_WTD --Week to date
	,sum(nb_opport_indication_digitale) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_indication_digitale_WTD --Week to date
	,sum(nb_opport_renvoi_trafic) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_renvoi_trafic_WTD --Week to date
	,sum(nb_opport_abouties_OB) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_abouties_OB_WTD --Week to date
	,sum(nb_opport_abouties_Web_app) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_abouties_Web_app_WTD --Week to date
	,sum(nb_opport_abouties) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_abouties_WTD --Week to date
	,sum(nb_opport_abouties_CRC) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_abouties_CRC_WTD --Week to date

	--Month to date
	,sum(nb_opport_abouties_OF) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_abouties_OF_MTD --Month to date
	,sum(nb_opport_full_boutiques) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_full_boutiques_MTD --Month to date
	,sum(nb_opport_repli_selfcare) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_repli_selfcare_MTD --Month to date
	,sum(nb_opport_indication_boutique) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_indication_boutique_MTD --Month to date
	,sum(nb_opport_indication_digitale) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_indication_digitale_MTD --Month to date
	,sum(nb_opport_renvoi_trafic) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_renvoi_trafic_MTD --Month to date
	,sum(nb_opport_abouties_OB) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_abouties_OB_MTD --Month to date
	,sum(nb_opport_abouties_Web_app) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_abouties_Web_app_MTD --Month to date
	,sum(nb_opport_abouties) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_abouties_MTD --Month to date
	,sum(nb_opport_abouties_CRC) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_abouties_CRC_MTD --Month to date

	
	--Full Month
	,sum(nb_opport_abouties_OF) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_opport_abouties_OF_FM --Full Month
	,sum(nb_opport_full_boutiques) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_opport_full_boutiques_FM --Full Month
	,sum(nb_opport_repli_selfcare) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_opport_repli_selfcare_FM --Full Month
	,sum(nb_opport_indication_boutique) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_opport_indication_boutique_FM --Full Month
	,sum(nb_opport_indication_digitale) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_opport_indication_digitale_FM --Full Month
	,sum(nb_opport_renvoi_trafic) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_opport_renvoi_trafic_FM --Full Month
	,sum(nb_opport_abouties_OB) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_opport_abouties_OB_FM --Full Month
	,sum(nb_opport_abouties_Web_app) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_opport_abouties_Web_app_FM --Full Month
	,sum(nb_opport_abouties) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_opport_abouties_FM --Full Month
	,sum(nb_opport_abouties_CRC) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_opport_abouties_CRC_FM --Full Month

	--Year to date
    ,sum(nb_opport_abouties_OF) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_abouties_OF_YTD  --Year to date
    ,sum(nb_opport_full_boutiques) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_full_boutiques_YTD  --Year to date
    ,sum(nb_opport_repli_selfcare) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_repli_selfcare_YTD  --Year to date
    ,sum(nb_opport_indication_boutique) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_indication_boutique_YTD  --Year to date
    ,sum(nb_opport_indication_digitale) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_indication_digitale_YTD  --Year to date
    ,sum(nb_opport_renvoi_trafic) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_renvoi_trafic_YTD  --Year to date
    ,sum(nb_opport_abouties_OB) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_abouties_OB_YTD  --Year to date
    ,sum(nb_opport_abouties_Web_app) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_abouties_Web_app_YTD  --Year to date
    ,sum(nb_opport_abouties) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_abouties_YTD  --Year to date
    ,sum(nb_opport_abouties_CRC) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_abouties_CRC_YTD  --Year to date

	--Full year
	,sum(nb_opport_abouties_OF) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_opport_abouties_OF_FY  --Full year
	,sum(nb_opport_full_boutiques) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_opport_full_boutiques_FY  --Full year
	,sum(nb_opport_repli_selfcare) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_opport_repli_selfcare_FY  --Full year
	,sum(nb_opport_indication_boutique) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_opport_indication_boutique_FY  --Full year
	,sum(nb_opport_indication_digitale) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_opport_indication_digitale_FY  --Full year
	,sum(nb_opport_renvoi_trafic) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_opport_renvoi_trafic_FY  --Full year
	,sum(nb_opport_abouties_OB) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_opport_abouties_OB_FY  --Full year
	,sum(nb_opport_abouties_Web_app) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_opport_abouties_Web_app_FY  --Full year
	,sum(nb_opport_abouties) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_opport_abouties_FY  --Full year
	,sum(nb_opport_abouties_CRC) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_opport_abouties_CRC_FY  --Full year
INTO #rq_opp_abouties
FROM  ( 
     SELECT  T1.StandardDate as jour_alim,DATEPART(YEAR,T1.Date) AS Year_alim,DATEPART(MONTH,T1.Date) AS Month_alim,DATEPART(YEAR,DATEADD(day, (7 - DATEPART(dw, T1.StandardDate)), T1.StandardDate)) *100 + DATEPART(WEEK,DATEADD(day, (7 - DATEPART(dw, T1.StandardDate)), T1.StandardDate)) AS Week_alim
	,count(*) as nb_opport_abouties  
	,sum(case when DistributorNetwork__c_label = 'Orange France' then 1 else 0 end) as nb_opport_abouties_OF   
	,sum(case when DistributorNetwork__c_label = 'Orange France' and CompleteFileFlag__c = '1' then 1 else 0 end) as nb_opport_full_boutiques
	,sum(case when DistributorNetwork__c_label = 'Orange France' and CompleteFileFlag__c <> '1' and IDProcessSous__c is not null then 1 else 0 end) as nb_opport_repli_selfcare
	,sum(case when DistributorNetwork__c_label = 'Orange France' and Indication__c = 'OUI' and IDProcessSous__c is null and DistributorEntity__c_label in ('AD', 'GDT')  then 1 else 0 end) as nb_opport_indication_boutique
	,sum(case when DistributorNetwork__c_label = 'Orange France'and Indication__c = 'OUI' and IDProcessSous__c is null and DistributorEntity__c_label in ('OF-WEB')  then 1 else 0 end) as nb_opport_indication_digitale
	,sum(case when DistributorNetwork__c_label = 'Orange France'and StartedChannel__c_label = 'Web OB' and DistributorEntity__c_label in ('OF-WEB')  then 1 else 0 end) as nb_opport_renvoi_trafic
	,sum(case when DistributorNetwork__c_label = 'Orange Bank' then 1 else 0 end) as nb_opport_abouties_OB
	,sum(case when DistributorNetwork__c_label = 'Orange Bank' and StartedChannel__c_label IN ('Appli OB','Web OB') then 1 else 0 end) as nb_opport_abouties_Web_app
	,sum(case when DistributorNetwork__c_label = 'Orange Bank' and StartedChannel__c_label IN ('Appel entrant CRC','Appel sortant CRC','Chat','E-mail') then 1 else 0 end) as nb_opport_abouties_CRC
FROM [dbo].[DIM_TEMPS] T1 
LEFT OUTER JOIN  [$(DataHubDatabaseName)].[dbo].VW_PV_SF_OPPORTUNITY  ON T1.StandardDate=cast(VW_PV_SF_OPPORTUNITY.CloseDate as date)
WHERE CommercialOfferCode__c ='OC80' and IsWon = 'true'and ( T1.StandardDate >= CAST(DATEADD(yyyy, DATEDIFF(YYYY, 0, DATEADD(yyyy, -2, @date_obs)), 0) AS DATE)
AND T1.StandardDate <=  DATEADD(day, (7 - DATEPART(dw, @date_obs)), @date_obs) )  and cast(VW_PV_SF_OPPORTUNITY.CloseDate as date) <= @date_max
GROUP BY  T1.StandardDate ,DATEPART(YEAR,T1.Date) ,DATEPART(MONTH,T1.Date) ,DATEPART(WEEK,T1.Date) 
) t	;

SELECT
  CY.jour_alim
, CY.Year_alim
, CY.Month_alim
, CY.Week_alim
, coalesce(CY.nb_opport_abouties,0) AS nb_opport_abouties
, coalesce(CY.nb_opport_abouties_WTD,0) AS nb_opport_abouties_WTD
, coalesce(CY.nb_opport_abouties_MTD,0) AS nb_opport_abouties_MTD
, coalesce(CY.nb_opport_abouties_YTD,0) AS nb_opport_abouties_YTD
, coalesce(PM.nb_opport_abouties_FM,0) AS nb_opport_abouties_FM
, coalesce(PY.nb_opport_abouties_FY,0) AS nb_opport_abouties_FY
, coalesce(CY.nb_opport_abouties_OF,0) AS nb_opport_abouties_OF
, coalesce(CY.nb_opport_abouties_OF_WTD,0) AS nb_opport_abouties_OF_WTD
, coalesce(CY.nb_opport_abouties_OF_MTD,0) AS nb_opport_abouties_OF_MTD
, coalesce(CY.nb_opport_abouties_OF_YTD,0) AS nb_opport_abouties_OF_YTD
, coalesce(PM.nb_opport_abouties_OF_FM,0) AS nb_opport_abouties_OF_FM
, coalesce(PY.nb_opport_abouties_OF_FY,0) AS nb_opport_abouties_OF_FY
, coalesce(CY.nb_opport_full_boutiques,0) AS nb_opport_full_boutiques
, coalesce(CY.nb_opport_full_boutiques_WTD,0) AS nb_opport_full_boutiques_WTD
, coalesce(CY.nb_opport_full_boutiques_MTD,0) AS nb_opport_full_boutiques_MTD
, coalesce(CY.nb_opport_full_boutiques_YTD,0) AS nb_opport_full_boutiques_YTD
, coalesce(PM.nb_opport_full_boutiques_FM,0) AS nb_opport_full_boutiques_FM
, coalesce(PY.nb_opport_full_boutiques_FY,0) AS nb_opport_full_boutiques_FY
, coalesce(CY.nb_opport_repli_selfcare,0) AS nb_opport_repli_selfcare
, coalesce(CY.nb_opport_repli_selfcare_WTD,0) AS nb_opport_repli_selfcare_WTD
, coalesce(CY.nb_opport_repli_selfcare_MTD,0) AS nb_opport_repli_selfcare_MTD
, coalesce(CY.nb_opport_repli_selfcare_YTD,0) AS nb_opport_repli_selfcare_YTD
, coalesce(PM.nb_opport_repli_selfcare_FM,0) AS nb_opport_repli_selfcare_FM
, coalesce(PY.nb_opport_repli_selfcare_FY,0) AS nb_opport_repli_selfcare_FY
, coalesce(CY.nb_opport_indication_boutique,0) AS nb_opport_indication_boutique
, coalesce(CY.nb_opport_indication_boutique_WTD,0) AS nb_opport_indication_boutique_WTD
, coalesce(CY.nb_opport_indication_boutique_MTD,0) AS nb_opport_indication_boutique_MTD
, coalesce(CY.nb_opport_indication_boutique_YTD,0) AS nb_opport_indication_boutique_YTD
, coalesce(PM.nb_opport_indication_boutique_FM,0) AS nb_opport_indication_boutique_FM
, coalesce(PY.nb_opport_indication_boutique_FY,0) AS nb_opport_indication_boutique_FY
, coalesce(CY.nb_opport_indication_digitale,0) AS nb_opport_indication_digitale
, coalesce(CY.nb_opport_indication_digitale_WTD,0) AS nb_opport_indication_digitale_WTD
, coalesce(CY.nb_opport_indication_digitale_MTD,0) AS nb_opport_indication_digitale_MTD
, coalesce(CY.nb_opport_indication_digitale_YTD,0) AS nb_opport_indication_digitale_YTD
, coalesce(PM.nb_opport_indication_digitale_FM,0) AS nb_opport_indication_digitale_FM
, coalesce(PY.nb_opport_indication_digitale_FY,0) AS nb_opport_indication_digitale_FY
, coalesce(CY.nb_opport_renvoi_trafic,0) AS nb_opport_renvoi_trafic
, coalesce(CY.nb_opport_renvoi_trafic_WTD,0) AS nb_opport_renvoi_trafic_WTD
, coalesce(CY.nb_opport_renvoi_trafic_MTD,0) AS nb_opport_renvoi_trafic_MTD
, coalesce(CY.nb_opport_renvoi_trafic_YTD,0) AS nb_opport_renvoi_trafic_YTD
, coalesce(PM.nb_opport_renvoi_trafic_FM,0) AS nb_opport_renvoi_trafic_FM
, coalesce(PY.nb_opport_renvoi_trafic_FY,0) AS nb_opport_renvoi_trafic_FY
, coalesce(CY.nb_opport_abouties_OB,0) AS nb_opport_abouties_OB
, coalesce(CY.nb_opport_abouties_OB_WTD,0) AS nb_opport_abouties_OB_WTD
, coalesce(CY.nb_opport_abouties_OB_MTD,0) AS nb_opport_abouties_OB_MTD
, coalesce(CY.nb_opport_abouties_OB_YTD,0) AS nb_opport_abouties_OB_YTD
, coalesce(PM.nb_opport_abouties_OB_FM,0) AS nb_opport_abouties_OB_FM
, coalesce(PY.nb_opport_abouties_OB_FY,0) AS nb_opport_abouties_OB_FY
, coalesce(CY.nb_opport_abouties_Web_app,0) AS nb_opport_abouties_Web_app
, coalesce(CY.nb_opport_abouties_Web_app_WTD,0) AS nb_opport_abouties_Web_app_WTD
, coalesce(CY.nb_opport_abouties_Web_app_MTD,0) AS nb_opport_abouties_Web_app_MTD
, coalesce(CY.nb_opport_abouties_Web_app_YTD,0) AS nb_opport_abouties_Web_app_YTD
, coalesce(PM.nb_opport_abouties_Web_app_FM,0) AS nb_opport_abouties_Web_app_FM
, coalesce(PY.nb_opport_abouties_Web_app_FY,0) AS nb_opport_abouties_Web_app_FY
, coalesce(CY.nb_opport_abouties_CRC,0) AS nb_opport_abouties_CRC
, coalesce(CY.nb_opport_abouties_CRC_WTD,0) AS nb_opport_abouties_CRC_WTD
, coalesce(CY.nb_opport_abouties_CRC_MTD,0) AS nb_opport_abouties_CRC_MTD
, coalesce(CY.nb_opport_abouties_CRC_YTD,0) AS nb_opport_abouties_CRC_YTD
, coalesce(PM.nb_opport_abouties_CRC_FM,0) AS nb_opport_abouties_CRC_FM
, coalesce(PY.nb_opport_abouties_CRC_FY,0) AS nb_opport_abouties_CRC_FY
INTO [dbo].[WK_SSRS_OPPORT_ABOUTIES]
FROM #rq_opp_abouties CY --Current Year
 LEFT OUTER JOIN #rq_opp_abouties PM --Previous Year
		  ON DATEADD(MONTH,-1,CY.jour_alim) = PM.jour_alim
	 LEFT OUTER JOIN #rq_opp_abouties PY --Previous Year
		  ON DATEADD(YEAR,-1,CY.jour_alim) = PY.jour_alim
ORDER BY CY.jour_alim;
END