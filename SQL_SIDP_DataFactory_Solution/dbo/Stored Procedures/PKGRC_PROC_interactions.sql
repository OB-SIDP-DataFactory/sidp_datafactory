﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PKGRC_PROC_interactions]
	-- Add the parameters for the stored procedure here
	--@date_alim varchar(50),
	@nb_collected_rows int output
	--,@nb_merged_rows int output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--set @date_alim = CAST(@date_alim as date);

    /*   
	---@date_alim : parametre de date de la collecte
	*/
	if OBJECT_ID(N'[dbo].[SAS_Vue_RC_Interactions]', N'U') is not null 
		begin
			truncate table [dbo].[SAS_Vue_RC_Interactions];
			insert into [dbo].[SAS_Vue_RC_Interactions]
			Select 
			inter.PK_ID,
			inter.INTERACTION_TYPE as INTERACTION_TYPE, 
			inter.INTERACTION_SOUS_TYPE as INTERACTION_SOUS_TYPE,
			tps.PK_ID as FK_TEMPS, 
			tps.MonthYear,
			tps.IsoWeek as WEEK_OF_YEAR,
			tps.StandardDate as STANDARD_DATE,
			us.PK_ID as FK_USER_OWNER, 
			us.CODE_GENESYS_USER, 
			US.PROFIL_USER, 
			concat(US.NOM_USER,' ',US.PRENOM_USER) as LOGIN_USER, 
			--null as SELLER_ID, 
			cas.Id_SF as CASE_ID,
			rec.PK_ID as FK_RECORDTYPE, 
			rec.LIBELLE as RECORDTYPE_LABEL,
			cas_type.PK_ID as FK_TYPE_CASE,
			cas_type.LIBELLE as TYPE_CASE_LABEL,
			cas_subtype.PK_ID as FK_SUBTYPE_CASE,
			cas_subtype.LIBELLE as SUBTYPE_CASE_LABEL,
			cas_status.PK_ID as FK_STATUS_CASE, 
			cas_status.LIBELLE as STATUS_CASE_LABEL, 
			accnt.Id_SF AS FK_ACCNT, 
			avg(inter.DMC) 
		
			from 
			[dbo].[WK_JOIN_INTERACTIONS] inter
			Left outer join [dbo].[DIM_TEMPS] tps on cast(tps.Date as date)= cast(inter.DATE_ID as date) 
			Left outer join [dbo].[DIM_USER] us on us.CODE_SF_USER = inter.OWNER_ID and us.Validity_StartDate <= inter.LASTMODIFIEDDATE  and us.Validity_EndDate >= inter.LASTMODIFIEDDATE 
			Left outer join [$(DataHubDatabaseName)].[dbo].[PV_SF_CASE] cas on cas.Id_SF = inter.CASE_ID 
			LEFT OUTER JOIN [dbo].[DIM_RECORDTYPE] rec on cas.RecordTypeId=rec.CODE_SF 
			LEFT OUTER JOIN [dbo].[DIM_CASE_TYPE] cas_type on cas_type.PARENT_ID is null and cas_type.CODE_SF = cas.Type and cas_type.Validity_StartDate <= inter.LASTMODIFIEDDATE  and cas_type.Validity_EndDate >= inter.LASTMODIFIEDDATE
			LEFT OUTER JOIN [dbo].[DIM_CASE_TYPE] cas_subtype on cas_subtype.PARENT_ID=Cas.Type and cas_subtype.CODE_SF=Cas.SubType__c and cas_subtype.Validity_StartDate <= inter.LASTMODIFIEDDATE  and cas_subtype.Validity_EndDate >= inter.LASTMODIFIEDDATE
			Left outer join [$(DataHubDatabaseName)].[dbo].[PV_SF_ACCOUNT] accnt on accnt.PersonContactId = inter.ACCNT_ID 
			LEFT OUTER JOIN [dbo].[DIM_CASE_STATUS] cas_status on cas.Status=cas_status.CODE_SF  

			group by 
			inter.PK_ID, inter.INTERACTION_TYPE, inter.INTERACTION_SOUS_TYPE, tps.PK_ID , 
			tps.MonthYear, tps.IsoWeek, tps.StandardDate, us.PK_ID,	us.CODE_GENESYS_USER, 
			US.PROFIL_USER, concat(US.NOM_USER,' ',US.PRENOM_USER), cas.Id_SF ,
			rec.PK_ID, rec.LIBELLE , cas_type.PK_ID , cas_type.LIBELLE , cas_subtype.PK_ID ,
			cas_subtype.LIBELLE , cas_status.PK_ID , cas_status.LIBELLE, accnt.Id_SF 

		end

		
	select @nb_collected_rows = COUNT(*) from [dbo].[SAS_Vue_RC_Interactions]; 

	EXEC [dbo].[PROC_SSRS_SAS_VUE_RC_INTERACTIONS];
END 
GO
