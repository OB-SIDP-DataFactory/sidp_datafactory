﻿
CREATE PROC [dbo].[PROC_SSRS_CRE_OPPORTUNITE]

AS  BEGIN 

DECLARE @date_obs DATE;
SELECT  @date_obs = CAST(GETDATE() AS DATE);
DECLARE @date_max date;
SELECT  @date_max = dateadd(day, -1 ,GETDATE());
SET DATEFIRST 1; -->1    Lundi
DROP TABLE IF EXISTS #rq_cre_enrolment_acc_cp;
DROP TABLE IF EXISTS [dbo].[WK_SSRS_CRE_OPPORTUNITE];

SELECT Year_alim,Month_alim,Week_alim,jour_alim
    ,nb_opp_CRE_accorde
	,nb_opp_CRE_accorde_digital
	,nb_opp_CRE_accorde_crc
	--Week to date
	,sum(nb_opp_CRE_accorde) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opp_CRE_accorde_WTD --Week to date
	,sum(nb_opp_CRE_accorde_digital) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opp_CRE_accorde_digital_WTD --Week to date
	,sum(nb_opp_CRE_accorde_crc) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opp_CRE_accorde_crc_WTD --Week to date
	--Month to date
	,sum(nb_opp_CRE_accorde) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opp_CRE_accorde_MTD --Month to date
	,sum(nb_opp_CRE_accorde_digital) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opp_CRE_accorde_digital_MTD --Month to date
	,sum(nb_opp_CRE_accorde_crc) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opp_CRE_accorde_crc_MTD --Month to date
	--Full Month
	,sum(nb_opp_CRE_accorde) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_opp_CRE_accorde_FM --Full Month
	,sum(nb_opp_CRE_accorde_digital) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_opp_CRE_accorde_digital_FM --Full Month
	,sum(nb_opp_CRE_accorde_crc) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_opp_CRE_accorde_crc_FM --Full Month

	--Year to date
    ,sum(nb_opp_CRE_accorde) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opp_CRE_accorde_YTD  --Year to date
    ,sum(nb_opp_CRE_accorde_digital) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opp_CRE_accorde_digital_YTD  --Year to date
    ,sum(nb_opp_CRE_accorde_crc) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opp_CRE_accorde_crc_YTD  --Year to date
	--Full year
	,sum(nb_opp_CRE_accorde) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_opp_CRE_accorde_FY  --Full year
	,sum(nb_opp_CRE_accorde_digital) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_opp_CRE_accorde_digital_FY  --Full year
	,sum(nb_opp_CRE_accorde_crc) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_opp_CRE_accorde_crc_FY  --Full year
INTO #rq_cre_enrolment_acc_cp
	FROM  ( 
     SELECT  T1.StandardDate as jour_alim,DATEPART(YEAR,T1.Date) AS Year_alim,DATEPART(MONTH,T1.Date) AS Month_alim,DATEPART(YEAR,DATEADD(day, (7 - DATEPART(dw, T1.StandardDate)), T1.StandardDate)) *100 + DATEPART(WEEK,DATEADD(day, (7 - DATEPART(dw, T1.StandardDate)), T1.StandardDate)) AS Week_alim
         , sum(case when STADE_VENTE = '14' then 1 end) as nb_opp_CRE_accorde
		 , sum(case when STADE_VENTE = '14' and COD_CAN_INT IN ('10','11') then 1 end) as nb_opp_CRE_accorde_digital
		 , sum(case when STADE_VENTE = '14' and COD_CAN_INT IN ('12','13','14','15') then 1 end) as nb_opp_CRE_accorde_crc
FROM [dbo].[DIM_TEMPS] T1 
LEFT OUTER JOIN  [dbo].[CRE_OPPORTUNITE]  ON T1.StandardDate=CAST(CRE_OPPORTUNITE.DTE_ACCORD AS DATE)
WHERE  ( T1.StandardDate >= CAST(DATEADD(yyyy, DATEDIFF(YYYY, 0, DATEADD(yyyy, -2, @date_obs)), 0) AS DATE)
AND T1.StandardDate <=  DATEADD(day, (7 - DATEPART(dw, @date_obs)), @date_obs) )
GROUP BY  T1.StandardDate ,DATEPART(YEAR,T1.Date) ,DATEPART(MONTH,T1.Date) ,DATEPART(WEEK,T1.Date) 
) t;

SELECT
CY.jour_alim,CY.Year_alim,CY.Month_alim,CY.Week_alim
, coalesce(CY.nb_opp_CRE_accorde,0)				AS nb_opp_CRE_accorde
, coalesce(CY.nb_opp_CRE_accorde_WTD,0)			AS nb_opp_CRE_accorde_WTD
, coalesce(CY.nb_opp_CRE_accorde_MTD,0)			AS nb_opp_CRE_accorde_MTD
, coalesce(CY.nb_opp_CRE_accorde_YTD,0)			AS nb_opp_CRE_accorde_YTD
, coalesce(PM.nb_opp_CRE_accorde_FM,0)			AS nb_opp_CRE_accorde_FM
, coalesce(PY.nb_opp_CRE_accorde_FY,0)			AS nb_opp_CRE_accorde_FY
, coalesce(CY.nb_opp_CRE_accorde_digital,0)		AS nb_opp_CRE_accorde_digital
, coalesce(CY.nb_opp_CRE_accorde_digital_WTD,0) AS nb_opp_CRE_accorde_digital_WTD
, coalesce(CY.nb_opp_CRE_accorde_digital_MTD,0) AS nb_opp_CRE_accorde_digital_MTD
, coalesce(CY.nb_opp_CRE_accorde_digital_YTD,0) AS nb_opp_CRE_accorde_digital_YTD
, coalesce(PM.nb_opp_CRE_accorde_digital_FM,0)  AS nb_opp_CRE_accorde_digital_FM
, coalesce(PY.nb_opp_CRE_accorde_digital_FY,0)  AS nb_opp_CRE_accorde_digital_FY
, coalesce(CY.nb_opp_CRE_accorde_crc,0)         AS nb_opp_CRE_accorde_crc
, coalesce(CY.nb_opp_CRE_accorde_crc_WTD,0)		AS nb_opp_CRE_accorde_crc_WTD
, coalesce(CY.nb_opp_CRE_accorde_crc_MTD,0)		AS nb_opp_CRE_accorde_crc_MTD
, coalesce(CY.nb_opp_CRE_accorde_crc_YTD,0)		AS nb_opp_CRE_accorde_crc_YTD
, coalesce(PM.nb_opp_CRE_accorde_crc_FM,0)		AS nb_opp_CRE_accorde_crc_FM
, coalesce(PY.nb_opp_CRE_accorde_crc_FY,0)		AS nb_opp_CRE_accorde_crc_FY
INTO [dbo].[WK_SSRS_CRE_OPPORTUNITE]
FROM #rq_cre_enrolment_acc_cp CY --Current Year
 LEFT OUTER JOIN #rq_cre_enrolment_acc_cp PM --Previous Year
		  ON DATEADD(MONTH,-1,CY.jour_alim) = PM.jour_alim
	 LEFT OUTER JOIN #rq_cre_enrolment_acc_cp PY --Previous Year
		  ON DATEADD(YEAR,-1,CY.jour_alim) = PY.jour_alim
ORDER BY CY.jour_alim;
END