﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PKGRC_PROC_cases] 
	-- Add the parameters for the stored procedure here
	@nb_collected_rows int output
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   	-- Add the T-SQL statements to compute the return value here
			truncate table [dbo].[SAS_Vue_RC_Cases];
			
			with interactions as (
select distinct cas.Id_SF,count(distinct inter.PK_ID) as NB_INTER from [$(DataHubDatabaseName)].[dbo].[PV_SF_CASE] cas 
JOIN [dbo].[WK_JOIN_INTERACTIONS] inter on inter.CASE_ID = cas.Id_SF 
group by cas.Id_SF) 
			
			insert into [dbo].[SAS_Vue_RC_Cases]
			
select 
					cas.Id_SF as PK_CASE, 
					usr.PK_ID as FK_USER,
					usr.PRENOM_USER, 
					usr.NOM_USER, 
					isnull(usr.PROFIL_USER,'* En file d''attente *') as PROFIL_USER, 
					tps.PK_ID as FK_TEMPS, 
					cast(tps.Date as date) as DATE,
					tps.IsoWeek as SEMAINE,

					tps.MonthYear as MOIS_ANNEE,
					task.NotationLabel__c as NOTATION_LABEL, 
					task.ScoreOfInteraction__c	as NOTE_INTERACTION,
					accnt.LastName as LASTNAME, 
					accnt.FirstName as FIRSTNAME, 
					accnt.Salutation as SALUTATION, 
					rec.PK_ID as FK_RECORDTYPE,
					ISNULL(rec.LIBELLE,'Non-renseigné') as RECORDTYPE_LABEL,
					accnt.Id as FK_ACCOUNT,
					cas.CaseNumber as CASENUMBER,
					cas.ParentId as PARENT_ID,
					case_type_cli.PK_ID as FK_TYPE_CLI,
					ISNULL(case_type_cli.LIBELLE,'Non-renseigné') as TYPE_CLIENT_LABEL,
					cre.PK_ID as FK_CREATEUR,
					concat(cre.PRENOM_USER,' ',cre.NOM_USER) as CREATEUR, 
					case_status.PK_ID as FK_STATUS,
					ISNULL(case_status.LIBELLE,'Non-renseigné') as STATUS_LABEL,
					case_attente.PK_ID as FK_ATTENTE,
					ISNULL(case_attente.LIBELLE,'Non-renseigné') as ATTENTE_LABEL,
					cas.CreatedDate as CREATED_DATE,
					cas.ClosedDate as CLOSED_DATE,
					cast(Cas.LastModifiedDate as date) as LAST_MODIFIED_DATE, 
					modif.PK_ID as FK_LASTMODIFIED,
					concat(modif.PRENOM_USER,' ',modif.NOM_USER) as USER_MODIF,
					case_canalO.PK_ID as FK_CANAL_ORIGINE,
					ISNULL(case_canalO.LIBELLE,'Non-renseigné') as CANAL_ORIGINE_LABEL ,
					cas.Network__c as NETWORK,
					cas_type.PK_ID as FK_TYPE_CASE,
					ISNULL(cas_type.LIBELLE,'Non-renseigné') as TYPE_CASE_LABEL,
					cas_subtype.PK_ID as FK_SUBTYPE_CASE,
					ISNULL(cas_subtype.LIBELLE,'Non-renseigné') as SUBTYPE_CASE_LABEL,
					case_pri.PK_ID as FK_PRIORITY,
					ISNULL(case_pri.LIBELLE,'Non-renseigné') as PRIORITY_LABEL,
					cas.Equipment__c as EQUIPMENT,
					case_subj1.PK_ID as FK_OBJ_PRINCIPAL,
					ISNULL(case_subj1.LIBELLE,'Non-renseigné') as OBJ_PRINCIPAL_LABEL,
					case_subj2.PK_ID as FK_OBJ_SECONDAIRE,
					ISNULL(case_subj2.LIBELLE,'Non-renseigné') as OBJ_SEC_LABEL,
					case_niv.PK_ID as FK_NIVEAU_CASE, 
					ISNULL(case_niv.LIBELLE,'Non-renseigné') as NIVEAU_LABEL, 
					(case cas.FirstClaim__c when '01' then 'Oui' when '02' then 'Non' else cas.Indemnity__c end) as FIRSTCLAIM, 
					cas.ARDate__c as ARDATE,
					cas.LetterWaitingDate__c as LETTERWAITING_DATE,
					cas.ResponseDate__c as RESPONSE_DATE,
					case_canal_rep.PK_ID as FK_CANAL_REPONSE,
					ISNULL(case_canal_rep.LIBELLE,'Non-renseigné') as CANAL_REPONSE_LABEL,
					case_orig.PK_ID as FK_ORIGINE_CASE,
					ISNULL(case_orig.LIBELLE,'Non-renseigné') as ORIGINE_LABEL,
					case_proc.PK_ID as FK_PROCESSUS_CASE,
					ISNULL(case_proc.LIBELLE,'Non-renseigné') as PROCESSUS_LABEL,
					case_met.PK_ID as FK_METIER_CASE,
					ISNULL(case_met.LIBELLE,'Non-renseigné') as METIER_LABEL,
					cas.Improvement__c as IMPROVEMENT,
					(case cas.Indemnity__c when '01' then 'Oui' when '02' then 'Non' else cas.Indemnity__c end) as INDEMNITY,
					cas.CommercialGestAmount__c as COMMERCIALGESTAMOUNT,
					cas.RetrocessionAmount__c as RETROCESSIONAMOUNT,
					cas.LostAmount__c as LOSTAMOUNT,
					case_qualif1.PK_ID as FK_QUALIF_REP1,
					ISNULL(case_qualif1.LIBELLE,'Non-renseigné') as QUALIF_REP1_LABEL,
					case_qualif2.PK_ID as FK_QUALIF_REP2, 
					ISNULL(case_qualif2.LIBELLE,'Non-renseigné') as QUALIF_REP2_LABEL, 
					sel.PK_ID as FK_VENDEUR, 
					sel.NAME as NOM_VENDEUR,
					bou.PK_ID as FK_BOUTIQUE, 
					bou.NAME as NOM_BOUTIQUE  ,			   
					case when cas.ClosedDate is not null then (case when cast(cas.CreatedDate as date)=cast(cas.ClosedDate as date) then 1 else sum(delai.BusinessDay) end) end as DELAI_MOYEN_TRAITEMENT, 
					(case when (cas.ParentId is null and cas.ClosedDate is not null and inter.NB_INTER=1) then 'Y' else 'N' end) AS FCR,  
					isnull(inter.NB_INTER,0) as TRACAGE 
					
					from 
			
					[$(DataHubDatabaseName)].[dbo].[PV_SF_CASE] cas 
		
					LEFT OUTER JOIN [dbo].[DIM_USER] usr on usr.CODE_SF_USER= cas.OwnerId and usr.Validity_StartDate <= Cas.LastModifiedDate  and usr.Validity_EndDate >= cas.LastModifiedDate
					LEFT OUTER JOIN [dbo].[DIM_TEMPS] tps on cast(tps.Date as date) = cast(cas.CreatedDate  as date)
					LEFT OUTER JOIN [$(DataHubDatabaseName)].[dbo].[PV_SF_TASK] task on task.WhatId = cas.Id_SF and task.RecordTypeId =(select distinct CODE_SF from [dbo].[DIM_RECORDTYPE] where LIBELLE = 'Satisfaction Client' ) 
					LEFT OUTER JOIN [$(DataHubDatabaseName)].[dbo].[PV_SF_ACCOUNT] accnt on accnt.Id_SF = cas.AccountId 
					LEFT OUTER JOIN INTERACTIONS inter on inter.Id_SF = cas.Id_SF
					LEFT OUTER JOIN [dbo].[DIM_RECORDTYPE] rec on rec.CODE_SF = cas.RecordTypeId and rec.Validity_StartDate <= cas.LastModifiedDate  and rec.Validity_EndDate >= cas.LastModifiedDate
					LEFT OUTER JOIN [dbo].[DIM_USER] cre on cre.CODE_SF_USER= cas.CreatedById and cre.Validity_StartDate <= Cas.LastModifiedDate  and cre.Validity_EndDate >= cas.LastModifiedDate
					LEFT OUTER JOIN [dbo].[DIM_USER] modif on modif.CODE_SF_USER= cas.LastModifiedById and modif.Validity_StartDate <= Cas.LastModifiedDate  and modif.Validity_EndDate >= cas.LastModifiedDate
					LEFT OUTER JOIN [dbo].[DIM_CASE_TYPE] cas_type on cas_type.PARENT_ID is null and cas_type.CODE_SF=Cas.Type and cas_type.Validity_StartDate <= cas.LastModifiedDate  and cas_type.Validity_EndDate >= cas.LastModifiedDate
					LEFT OUTER JOIN [dbo].[DIM_CASE_TYPE] cas_subtype on cas_subtype.PARENT_ID=cas.Type and cas_subtype.CODE_SF=Cas.SubType__c and cas_subtype.Validity_StartDate <= cas.LastModifiedDate  and cas_subtype.Validity_EndDate >= cas.LastModifiedDate
					LEFT OUTER JOIN [dbo].[DIM_CASE_STATUS] case_status on case_status.CODE_SF = cas.Status and case_status.Validity_StartDate <= cas.LastModifiedDate  and case_status.Validity_EndDate >= cas.LastModifiedDate
					LEFT OUTER JOIN [dbo].[DIM_CASE_CANAL_ORIGINE] case_canalO on case_canalO.CODE_SF = cas.Origin and case_canalO.Validity_StartDate <= cas.LastModifiedDate  and case_canalO.Validity_EndDate >= cas.LastModifiedDate
					LEFT OUTER JOIN [dbo].[DIM_CASE_PRIORITE] case_pri on case_pri.CODE_SF = cas.Priority and case_pri.Validity_StartDate <= cas.LastModifiedDate  and case_pri.Validity_EndDate >= cas.LastModifiedDate
					LEFT OUTER JOIN [dbo].[DIM_CASE_OBJET_PRINCIPAL] case_subj1 on case_subj1.CODE_SF = cas.PrimarySubject__c and case_subj1.Validity_StartDate <= cas.LastModifiedDate  and case_subj1.Validity_EndDate >= cas.LastModifiedDate
					LEFT OUTER JOIN [dbo].[DIM_CASE_NIVEAU] case_niv on case_niv.CODE_SF = cas.LevelClaim__c and case_niv.Validity_StartDate <= cas.LastModifiedDate  and case_niv.Validity_EndDate >= cas.LastModifiedDate
					LEFT OUTER JOIN [dbo].[DIM_CASE_CANAL_REPONSE] case_canal_rep on case_canal_rep.CODE_SF = cas.ResponseChannel__c and case_canal_rep.Validity_StartDate <= cas.LastModifiedDate  and case_canal_rep.Validity_EndDate >= cas.LastModifiedDate
					LEFT OUTER JOIN [dbo].[DIM_CASE_ORIGINE] case_orig on case_orig.CODE_SF = cas.Origin__c and case_orig.Validity_StartDate <= cas.LastModifiedDate  and case_orig.Validity_EndDate >= cas.LastModifiedDate
					LEFT OUTER JOIN [dbo].[DIM_CASE_PROCESSUS] case_proc on case_proc.CODE_SF = cas.Process__c and case_proc.Validity_StartDate <= cas.LastModifiedDate  and case_proc.Validity_EndDate >= cas.LastModifiedDate
					LEFT OUTER JOIN [dbo].[DIM_CASE_METIER] case_met on case_met.CODE_SF = cas.Metier__c and case_met.Validity_StartDate <= cas.LastModifiedDate  and case_met.Validity_EndDate >= cas.LastModifiedDate
					LEFT OUTER JOIN [dbo].[DIM_CASE_QUALIF_REP1] case_qualif1 on case_qualif1.CODE_SF = cas.PrimaryQualification__c and case_qualif1.Validity_StartDate <= cas.LastModifiedDate  and case_qualif1.Validity_EndDate >= cas.LastModifiedDate
					LEFT OUTER JOIN [dbo].[DIM_CASE_QUALIF_REP2] case_qualif2 on case_qualif2.CODE_SF = cas.SecondQualification__c and case_qualif2.Validity_StartDate <= cas.LastModifiedDate  and case_qualif2.Validity_EndDate >= cas.LastModifiedDate
					LEFT OUTER JOIN [dbo].[DIM_CASE_OBJET_SECONDAIRE] case_subj2 on case_subj2.CODE_SF = cas.SecondarySubject__c and case_subj2.Validity_StartDate <= cas.LastModifiedDate  and case_subj2.Validity_EndDate >= cas.LastModifiedDate
					LEFT OUTER JOIN [dbo].[DIM_CASE_TYPE_CLI] case_type_cli on case_type_cli.CODE_SF = cas.CustomerType__c and case_type_cli.Validity_StartDate <= cas.LastModifiedDate  and case_type_cli.Validity_EndDate >= cas.LastModifiedDate
					LEFT OUTER JOIN [dbo].[DIM_CASE_ATTENTE] case_attente on case_attente.CODE_SF = cas.StandBy__c and case_attente.Validity_StartDate <= cas.LastModifiedDate  and case_attente.Validity_EndDate >= cas.LastModifiedDate
					LEFT OUTER JOIN [dbo].[DIM_VENDEUR] sel on cas.ShopSeller__c=sel.FK_CONTACT and sel.Validity_StartDate <= cas.Validity_StartDate  and sel.Validity_EndDate >= cas.Validity_StartDate 
					LEFT OUTER JOIN [dbo].[DIM_BOUTIQUE] bou on Cas.OBShop__c =bou.ID_SF and bou.Validity_StartDate <= Cas.Validity_StartDate  and bou.Validity_EndDate >= Cas.Validity_StartDate 
					JOIN [dbo].[DIM_TEMPS] delai on cast(delai.Date as date) >= cast(cas.CreatedDate  as date) and cast(delai.Date as date) <= cast(coalesce(cas.ClosedDate, cas.CreatedDate) as date) 

					where cas.CreatedDate >= cast(DATEADD(month,-12, cast(getdate() as date)) as datetime) and cast(cas.CreatedDate as date) < cast(getdate() as date) 
			
					group by cas.Id_SF, 
					usr.PK_ID, usr.PRENOM_USER, usr.NOM_USER, usr.PROFIL_USER, 
					tps.PK_ID, tps.Date, tps.IsoWeek, tps.MonthYear, 
					task.NotationLabel__c, task.ScoreOfInteraction__c,
					Accnt.LastName, Accnt.FirstName, Accnt.Salutation, accnt.Id,
					cas.CaseNumber, cas.ParentId, 
					cas.FirstClaim__c, cas.ARDate__c, cas.LetterWaitingDate__c, cas.ResponseDate__c,
					cas.Improvement__c, cas.Indemnity__c, cas.CommercialGestAmount__c, 
					cas.RetrocessionAmount__c, cas.LostAmount__c, 
					cas.Network__c,  cas.Equipment__c, cas.CreatedById, 
					cas.CreatedDate, cas.ClosedDate, cast(cas.LastModifiedDate as date), 
					modif.PK_ID , concat(modif.PRENOM_USER,' ',modif.NOM_USER), 
					rec.PK_ID, rec.LIBELLE , 
					case_type_cli.PK_ID, case_type_cli.LIBELLE, 
					case_status.PK_ID, case_status.LIBELLE,
					case_attente.PK_ID, case_attente.LIBELLE, 
					case_canalO.PK_ID , case_canalO.LIBELLE,
					cas_type.PK_ID, cas_type.LIBELLE, 
					cas_subtype.PK_ID, cas_subtype.LIBELLE, 
					case_pri.PK_ID, case_pri.LIBELLE, 
					case_subj1.PK_ID, case_subj1.LIBELLE, 
					case_subj2.PK_ID, case_subj2.LIBELLE, 
					case_niv.PK_ID, case_niv.LIBELLE, 
					case_canal_rep.PK_ID, case_canal_rep.LIBELLE, 
					case_orig.PK_ID, case_orig.LIBELLE, 
					case_proc.PK_ID, case_proc.LIBELLE, 
					case_met.PK_ID, case_met.LIBELLE, 
					case_qualif1.PK_ID, case_qualif1.LIBELLE, 
					case_qualif2.PK_ID, case_qualif2.LIBELLE,
					cre.PK_ID,
					concat(cre.PRENOM_USER,' ',cre.NOM_USER),
					sel.PK_ID , sel.NAME , inter.NB_INTER 
					,bou.PK_ID , bou.NAME	 

	select @nb_collected_rows = COUNT(*) from [dbo].[SAS_Vue_RC_Cases];

	EXEC [dbo].[PROC_SSRS_SAS_VUE_RC_CASES];
END
GO