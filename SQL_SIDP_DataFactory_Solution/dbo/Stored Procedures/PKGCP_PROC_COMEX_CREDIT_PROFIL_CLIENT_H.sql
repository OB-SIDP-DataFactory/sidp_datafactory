﻿
CREATE PROCEDURE [dbo].[PKGCP_PROC_COMEX_CREDIT_PROFIL_CLIENT_H]
@Date_obs date

AS BEGIN

/*
declare @Date_obs date; 
set @Date_obs='2018-01-30';*/
DECLARE @weekobs int;
DECLARE @Anneeweekobs int;
DECLARE @weekPrecobs int;
DECLARE @AnneeweekPrecobs int;

SET @Date_obs = CAST(@Date_obs as date);
SET DATEFIRST 1; -->1    Lundi
-- Num de la Semaine en cours et année de la semaine en cours
SET @weekobs = (select IsoWeek from [dbo].[DIM_TEMPS] where CAST(Date as date) = @Date_obs);
SET @Anneeweekobs = (select DATEPART(YEAR,@Date_obs));

-- Num de la Semaine précédente et année de la semaine précédente
SET @weekPrecobs = (select IsoWeek from [dbo].[DIM_TEMPS] where CAST(Date as date) = DATEADD(week,-1,@Date_obs));
SET @AnneeweekPrecobs = DATEPART(YEAR,DATEADD(week,-1,@Date_obs)) ;

declare @FirstDOW date = DATEADD(day, (-1 * DATEPART(dw, @Date_obs)) + 1, @Date_obs);
declare @LastDOW date = DATEADD(day, (7 - DATEPART(dw, @Date_obs)), @Date_obs);
declare @FirstDOPW date = DATEADD(day, (-1 * DATEPART(dw, @Date_obs)) -6, @Date_obs); 
declare @LastDOPW date = DATEADD(day, (7 - DATEPART(dw, @Date_obs)) -7, @Date_obs); 
declare @var int =datediff(dd,@FirstDOPW,@date_obs);

With 
cte_jour_alim_list AS
( 	SELECT T1.StandardDate AS jour_alim,
			DATEPART(ISO_WEEK,T1.StandardDate) as [WeekOfYear]
    FROM dbo.[DIM_TEMPS] T1
    WHERE T1.StandardDate >= CAST(DATEADD(day, -@var, @Date_obs) AS DATE) AND T1.StandardDate <= @Date_obs --1e jours S-1 jusqu'à la date d'observation
)

--Opportunités
,cte_opp_all as (
select opp.Id_SF								as [IDE_OPPRT_SF]
	  ,opp.AccountId							as [IDE_PERS_SF]
	  ,opp.RelationEntryScore__c				as [SCO_ENT_REL]
	  ,opp.Cod_StageName						as [STADE_VENTE]
	  ,opp.Lib_StageName						as [LIB_STADE_VENTE]
	  ,opp.date_alim							as [DTE_ALIM]
	  ,DATEPART(ISO_WEEK,opp.[date_alim])		as [WeekOfYear]
	  ,opp.last_update_date						as [last_update_date]
	  ,rank() over (partition by opp.Id_SF, opp.date_alim order by opp.last_update_date desc) as rang
 FROM [dbo].[CRE_WK_STOCK_OPPORTUNITY] opp 
 LEFT JOIN [dbo].[CRE_CREDIT] cre 
 ON opp.Id_SF = cre.IDE_OPPRT_SF
 INNER JOIN cte_jour_alim_list alim
 ON opp.date_alim = alim.jour_alim
)
--select * from cte_opp_all

--Opportunités gagnées au stade de vente Crédit accordé
,cte_opp_gag as (
 select IDE_OPPRT_SF,
        MIN(last_update_date) as last_update_date
 from cte_opp_all
 where STADE_VENTE = '14' and rang = 1
 group by IDE_OPPRT_SF
)
--select * from cte_opp_gag

--Clients associés aux opportunités gagnées
,cte_client_ALL AS
( SELECT opp.[DTE_ALIM]
		,opp.[WeekOfYear]
		,opp.IDE_OPPRT_SF
		,ref.[IDE_PERS_SF]
		,ref.[DTE_TRAITEMENT]
		,CAST(ref.[DTE_NAIS] as date) as [DTE_NAIS]
		,DATEDIFF(YEAR, ref.DTE_NAIS, opp.[DTE_ALIM]) as AGE_CLIENT
		,dim_csp.LIB_CSP_NIV1 as LIB_CSP_NIV1
		,case when (ref.[SCO_PREAT] is not null and charindex('.',ref.[SCO_PREAT])<>0) then cast(substring(ref.[SCO_PREAT],1, charindex('.',ref.[SCO_PREAT])-1) as int) else ref.[SCO_PREAT] end as [SCO_PREAT]
		,opp.SCO_ENT_REL as COD_SCORE_EER
		,dim_eer.LIB_SCORE_EER as LIB_SCORE_EER
		,ref.[SEG_GEOLIFE]
		,rank() over (partition by opp.[DTE_ALIM], ref.IDE_PERS_SF order by ref.DTE_TRAITEMENT desc) as rang2
	FROM cte_opp_all opp 
	INNER JOIN cte_opp_gag opp_gag
	ON opp.IDE_OPPRT_SF=opp_gag.IDE_OPPRT_SF 
	AND opp.DTE_ALIM=cast(opp_gag.last_update_date as date) 
	INNER JOIN dbo.[REF_CLIENT] ref
	ON opp.IDE_PERS_SF = ref.IDE_PERS_SF
	AND ref.DTE_TRAITEMENT <= opp.DTE_ALIM  -- <= car historisation alim mensuelle  
	LEFT JOIN DIM_CSP_NIV1 dim_csp 
	ON ref.COD_CSP_NV1 = dim_csp.COD_CSP_NIV1
	LEFT JOIN [DIM_SCORE_EER] dim_eer 
	ON opp.SCO_ENT_REL = dim_eer.COD_SCORE_EER
	WHERE STADE_VENTE = '14'
	and rang = 1
)
--select distinct [IDE_PERS_SF] from cte_client_ALL

--Profil client créé
,cte_client_cnt AS ( 
SELECT	DTE_ALIM,
		WeekOfYear,
		COUNT([IDE_PERS_SF]) as NB_CLIENT, 
		AGE_CLIENT,
		PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY AGE_CLIENT) OVER (PARTITION BY DTE_ALIM) AS AGE_CLIENT_MEDIAN,
		CASE
			WHEN AGE_CLIENT <= 17 THEN 17
			WHEN 18 <= AGE_CLIENT and AGE_CLIENT <= 29 THEN 2
			WHEN 30 <= AGE_CLIENT and AGE_CLIENT <= 44 THEN 44
			WHEN 45 <= AGE_CLIENT and AGE_CLIENT <= 59 THEN 59
			WHEN 60 <= AGE_CLIENT and AGE_CLIENT <= 74 THEN 74
			WHEN AGE_CLIENT >= 75 THEN 75
			ELSE 9999
		END as AGE_CLIENT_CALCULE, 
		CASE
			WHEN AGE_CLIENT <= 17 THEN '17 ans et moins'
			WHEN 18 <= AGE_CLIENT and AGE_CLIENT <= 29 THEN 'Jeunes (18-29 ans)'
			WHEN 30 <= AGE_CLIENT and AGE_CLIENT <= 44 THEN 'Jeunes familles (30-44 ans)'
			WHEN 45 <= AGE_CLIENT and AGE_CLIENT <= 59 THEN 'Familles installées (45-59 ans)'
			WHEN 60 <= AGE_CLIENT and AGE_CLIENT <= 74 THEN 'Jeunes séniors (60-74 ans)'
			WHEN AGE_CLIENT >= 75 THEN '75 ans et plus'
			ELSE 'Non renseigné'
		END as LIB_AGE_CLIENT,
		LIB_CSP_NIV1,
		SCO_PREAT,
		CASE
			WHEN 0 <= SCO_PREAT and SCO_PREAT <= 10 THEN 10
			WHEN 11 <= SCO_PREAT and SCO_PREAT <= 40 THEN 40
			WHEN 41 <= SCO_PREAT and SCO_PREAT <= 99 THEN 99
			ELSE 9999
		END as SCO_PREAT_CALCULE,
		CASE
			WHEN 0 <= SCO_PREAT and SCO_PREAT <= 10 THEN 'Mauvais [0;10]'
			WHEN 11 <= SCO_PREAT and SCO_PREAT <= 40 THEN 'Neutre [11;40]'
			WHEN 41 <= SCO_PREAT and SCO_PREAT <= 99 THEN 'Bon [41;99]'
			ELSE 'Non renseigné'
		END as LIB_SCO_PREAT,
		COD_SCORE_EER,
		LIB_SCORE_EER,
		SEG_GEOLIFE,
		CASE
			WHEN SEG_GEOLIFE in ('1 - urbain dynamique', 'urbain dynamique - 1', 'urbain dynamique') THEN 'Urbain dynamique'
			WHEN SEG_GEOLIFE in ('2 - urbain familial aise', 'urbain familial aise - 2', 'urbain familial aise') THEN 'Urbain familial aise'
			WHEN SEG_GEOLIFE in ('3 - urbain classe moyenne', 'urbain classe moyenne - 3', 'urbain classe moyenne') THEN 'Urbain classe moyenne'
			WHEN SEG_GEOLIFE in ('4 - populaire', 'populaire - 4', 'populaire') THEN 'Populaire'
			WHEN SEG_GEOLIFE in ('5 - urbain defavorise', 'urbain defavorise - 5', 'urbain defavorise') THEN 'Urbain defavorise'
			WHEN SEG_GEOLIFE in ('6 - periurbain en croissance', 'periurbain en croissance - 6', 'periurbain en croissance') THEN 'Periurbain en croissance'
			WHEN SEG_GEOLIFE in ('7 - pavillonnaire familial aise', 'pavillonnaire familial aise - 7', 'pavillonnaire familial aise') THEN 'Pavillonnaire familial aise'
			WHEN SEG_GEOLIFE in ('8 - rural dynamique', 'rural dynamique - 8', 'rural dynamique') THEN 'Rural dynamique'
			WHEN SEG_GEOLIFE in ('9 - rural ouvrier', 'rural ouvrier - 9', 'rural ouvrier') THEN 'Rural ouvrier'
			WHEN SEG_GEOLIFE in ('10 - rural traditionnel', 'rural traditionnel - 10', 'rural traditionnel') THEN 'Rural traditionnel'
			WHEN SEG_GEOLIFE in ('11 - residence secondaire', 'residence secondaire - 11', 'residence secondaire') THEN 'Residence secondaire'
			WHEN SEG_GEOLIFE is null THEN 'Non renseigné'
		END as LIB_SEG_GEOLIFE
FROM cte_client_ALL c
WHERE rang2=1
GROUP BY [DTE_ALIM], WeekOfYear, AGE_CLIENT, LIB_CSP_NIV1, SCO_PREAT, COD_SCORE_EER, LIB_SCORE_EER, SEG_GEOLIFE
)
--select * from cte_client_cnt

,cte_age_list AS
( 
SELECT DISTINCT CASE
			WHEN AGE_CLIENT <= 17 THEN 17
			WHEN 18 <= AGE_CLIENT and AGE_CLIENT <= 29 THEN 2
			WHEN 30 <= AGE_CLIENT and AGE_CLIENT <= 44 THEN 44
			WHEN 45 <= AGE_CLIENT and AGE_CLIENT <= 59 THEN 59
			WHEN 60 <= AGE_CLIENT and AGE_CLIENT <= 74 THEN 74
			WHEN AGE_CLIENT >= 75 THEN 75
			ELSE 9999
		END as AGE_CLIENT_CALCULE, 
		CASE
			WHEN AGE_CLIENT <= 17 THEN '17 ans et moins'
			WHEN 18 <= AGE_CLIENT and AGE_CLIENT <= 29 THEN 'Jeunes (18-29 ans)'
			WHEN 30 <= AGE_CLIENT and AGE_CLIENT <= 44 THEN 'Jeunes familles (30-44 ans)'
			WHEN 45 <= AGE_CLIENT and AGE_CLIENT <= 59 THEN 'Familles installées (45-59 ans)'
			WHEN 60 <= AGE_CLIENT and AGE_CLIENT <= 74 THEN 'Jeunes séniors (60-74 ans)'
			WHEN AGE_CLIENT >= 75 THEN '75 ans et plus'
			ELSE 'Non renseigné'
		END as LIB_AGE_CLIENT
FROM cte_client_ALL
)

,cte_csp_list AS
(
SELECT distinct LIB_CSP_NIV1 from cte_client_ALL
)

,cte_preat_list AS
(
SELECT distinct CASE
			WHEN 0 <= SCO_PREAT and SCO_PREAT <= 10 THEN 10
			WHEN 11 <= SCO_PREAT and SCO_PREAT <= 40 THEN 40
			WHEN 41 <= SCO_PREAT and SCO_PREAT <= 99 THEN 99
			ELSE 9999
		END as SCO_PREAT_CALCULE,
		CASE
			WHEN 0 <= SCO_PREAT and SCO_PREAT <= 10 THEN 'Mauvais [0;10]'
			WHEN 11 <= SCO_PREAT and SCO_PREAT <= 40 THEN 'Neutre [11;40]'
			WHEN 41 <= SCO_PREAT and SCO_PREAT <= 99 THEN 'Bon [41;99]'
			ELSE 'Non renseigné'
		END as LIB_SCO_PREAT
from cte_client_ALL
)

,cte_eer_list AS
(
SELECT distinct COD_SCORE_EER, LIB_SCORE_EER from cte_client_ALL
)

,cte_geolife_list AS
(
SELECT distinct CASE
			WHEN SEG_GEOLIFE in ('1 - urbain dynamique', 'urbain dynamique - 1', 'urbain dynamique') THEN 'Urbain dynamique'
			WHEN SEG_GEOLIFE in ('2 - urbain familial aise', 'urbain familial aise - 2', 'urbain familial aise') THEN 'Urbain familial aise'
			WHEN SEG_GEOLIFE in ('3 - urbain classe moyenne', 'urbain classe moyenne - 3', 'urbain classe moyenne') THEN 'Urbain classe moyenne'
			WHEN SEG_GEOLIFE in ('4 - populaire', 'populaire - 4', 'populaire') THEN 'Populaire'
			WHEN SEG_GEOLIFE in ('5 - urbain defavorise', 'urbain defavorise - 5', 'urbain defavorise') THEN 'Urbain defavorise'
			WHEN SEG_GEOLIFE in ('6 - periurbain en croissance', 'periurbain en croissance - 6', 'periurbain en croissance') THEN 'Periurbain en croissance'
			WHEN SEG_GEOLIFE in ('7 - pavillonnaire familial aise', 'pavillonnaire familial aise - 7', 'pavillonnaire familial aise') THEN 'Pavillonnaire familial aise'
			WHEN SEG_GEOLIFE in ('8 - rural dynamique', 'rural dynamique - 8', 'rural dynamique') THEN 'Rural dynamique'
			WHEN SEG_GEOLIFE in ('9 - rural ouvrier', 'rural ouvrier - 9', 'rural ouvrier') THEN 'Rural ouvrier'
			WHEN SEG_GEOLIFE in ('10 - rural traditionnel', 'rural traditionnel - 10', 'rural traditionnel') THEN 'Rural traditionnel'
			WHEN SEG_GEOLIFE in ('11 - residence secondaire', 'residence secondaire - 11', 'residence secondaire') THEN 'Residence secondaire'
			WHEN SEG_GEOLIFE is null THEN 'Non renseigné'
		END as LIB_SEG_GEOLIFE
from cte_client_ALL
)

--Profil client créé Jour
,cte_client_cumul_jour as (
SELECT	[DTE_ALIM],
		WeekOfYear, 
		ISNULL(SUM(NB_CLIENT),0) as NB_CLIENT, 
		AVG(AGE_CLIENT_MEDIAN) as AGE_CLIENT_MEDIAN, 
		AGE_CLIENT_CALCULE, 
		LIB_AGE_CLIENT, 
		LIB_CSP_NIV1, 
		SCO_PREAT_CALCULE, 
		LIB_SCO_PREAT,
		COD_SCORE_EER,
		LIB_SCORE_EER, 
		LIB_SEG_GEOLIFE
FROM  cte_client_cnt c 
GROUP BY DTE_ALIM,
		WeekOfYear,
		AGE_CLIENT_CALCULE, 
		LIB_AGE_CLIENT, 
		LIB_CSP_NIV1, 
		SCO_PREAT_CALCULE, 
		LIB_SCO_PREAT,
		COD_SCORE_EER,
		LIB_SCORE_EER, 
		LIB_SEG_GEOLIFE
)
--select * from cte_client_cumul_jour

--Profil client créé Hebdo
,cte_client_cumul_hebdo as (
SELECT	WeekOfYear,
		ISNULL(SUM(NB_CLIENT),0) AS NB_CLIENT_HEBDO,
		AGE_CLIENT_CALCULE, 
		LIB_AGE_CLIENT, 
		LIB_CSP_NIV1, 
		SCO_PREAT_CALCULE, 
		LIB_SCO_PREAT, 
		COD_SCORE_EER,
		LIB_SCORE_EER,
		LIB_SEG_GEOLIFE
FROM cte_client_cumul_jour c
WHERE c.DTE_ALIM >= @FirstDOW AND c.DTE_ALIM <= @Date_obs 
GROUP BY WeekOfYear, 
		AGE_CLIENT_CALCULE, 
		LIB_AGE_CLIENT, 
		LIB_CSP_NIV1, 
		SCO_PREAT_CALCULE, 
		LIB_SCO_PREAT, 
		COD_SCORE_EER,
		LIB_SCORE_EER,
		LIB_SEG_GEOLIFE
)
--select * from cte_client_cumul_hebdo

--Profil client créé Hebdo préc
,cte_client_cumul_hebdo_prec as (
SELECT	WeekOfYear, 
		ISNULL(SUM(NB_CLIENT),0) AS NB_CLIENT_HEBDO_PREC,
		AGE_CLIENT_CALCULE, 
		LIB_AGE_CLIENT, 
		LIB_CSP_NIV1, 
		SCO_PREAT_CALCULE, 
		LIB_SCO_PREAT, 
		COD_SCORE_EER,
		LIB_SCORE_EER,
		LIB_SEG_GEOLIFE
FROM cte_client_cumul_jour c
WHERE c.DTE_ALIM >= @FirstDOPW AND c.DTE_ALIM <= @LastDOPW 
GROUP BY WeekOfYear, 
		AGE_CLIENT_CALCULE, 
		LIB_AGE_CLIENT, 
		LIB_CSP_NIV1, 
		SCO_PREAT_CALCULE, 
		LIB_SCO_PREAT, 
		COD_SCORE_EER,
		LIB_SCORE_EER,
		LIB_SEG_GEOLIFE
)
--select * from cte_client_cumul_hebdo_prec

select  x.jour_alim,
		ISNULL(age.AGE_CLIENT_CALCULE,9999) as AGE_CLIENT_CALCULE, 
		ISNULL(age.LIB_AGE_CLIENT,'Non renseigné') as LIB_AGE_CLIENT, 
		ISNULL(csp.LIB_CSP_NIV1,'Non renseigné') as LIB_CSP_NIV1, 
		ISNULL(preat.SCO_PREAT_CALCULE,9999) as SCO_PREAT_CALCULE, 
		ISNULL(preat.LIB_SCO_PREAT,'Non renseigné') as LIB_SCO_PREAT, 
		ISNULL(eer.COD_SCORE_EER,'9999') as COD_SCORE_EER,
		ISNULL(eer.LIB_SCORE_EER,'Non renseigné') as LIB_SCORE_EER,
		ISNULL(geolife.LIB_SEG_GEOLIFE,'Non renseigné') as LIB_SEG_GEOLIFE,
		ISNULL(j_median.AGE_CLIENT_MEDIAN,0) as AGE_CLIENT_MEDIAN,
		ISNULL(j.NB_CLIENT,0) as NB_CLIENT, 
		case when ROW_NUMBER() OVER(PARTITION BY age.AGE_CLIENT_CALCULE, csp.LIB_CSP_NIV1, preat.SCO_PREAT_CALCULE, eer.COD_SCORE_EER, geolife.LIB_SEG_GEOLIFE order by x.jour_alim desc)=1 
				then ISNULL(h.NB_CLIENT_HEBDO,0) 
				else 0 
		end as NB_CLIENT_HEBDO,
		case when ROW_NUMBER() OVER(PARTITION BY age.AGE_CLIENT_CALCULE, csp.LIB_CSP_NIV1, preat.SCO_PREAT_CALCULE, eer.COD_SCORE_EER, geolife.LIB_SEG_GEOLIFE order by x.jour_alim desc)=1 
				then ISNULL(hp.NB_CLIENT_HEBDO_PREC,0) 
				else 0 
		end as NB_CLIENT_HEBDO_PREC
FROM cte_jour_alim_list x
LEFT JOIN cte_age_list age on 1=1
LEFT JOIN cte_csp_list csp on 1=1
LEFT JOIN cte_preat_list preat on 1=1
LEFT JOIN cte_eer_list eer on 1=1
LEFT JOIN cte_geolife_list geolife on 1=1
LEFT JOIN cte_client_cumul_jour j on x.jour_alim=j.DTE_ALIM
AND ISNULL(age.AGE_CLIENT_CALCULE,9999) = ISNULL(j.AGE_CLIENT_CALCULE,9999)
AND ISNULL(csp.LIB_CSP_NIV1,'Non renseigné') = ISNULL(j.LIB_CSP_NIV1,'Non renseigné')
AND ISNULL(preat.SCO_PREAT_CALCULE,9999) =ISNULL( j.SCO_PREAT_CALCULE,9999)
AND ISNULL(eer.COD_SCORE_EER,'9999') = ISNULL(j.COD_SCORE_EER,'9999')
AND ISNULL(geolife.LIB_SEG_GEOLIFE,'Non renseigné') = ISNULL(j.LIB_SEG_GEOLIFE,'Non renseigné')
LEFT JOIN (select distinct DTE_ALIM, AGE_CLIENT_MEDIAN from cte_client_cumul_jour) j_median on x.jour_alim=j_median.DTE_ALIM
LEFT JOIN cte_client_cumul_hebdo h
ON  ISNULL(age.AGE_CLIENT_CALCULE,9999) = ISNULL(h.AGE_CLIENT_CALCULE,9999)
AND ISNULL(csp.LIB_CSP_NIV1,'Non renseigné') = ISNULL(h.LIB_CSP_NIV1,'Non renseigné')
AND ISNULL(preat.SCO_PREAT_CALCULE,9999) =ISNULL( h.SCO_PREAT_CALCULE,9999)
AND ISNULL(eer.COD_SCORE_EER,'9999') = ISNULL(h.COD_SCORE_EER,'9999')
AND ISNULL(geolife.LIB_SEG_GEOLIFE,'Non renseigné') = ISNULL(h.LIB_SEG_GEOLIFE,'Non renseigné')
LEFT JOIN cte_client_cumul_hebdo_prec hp
ON	ISNULL(age.AGE_CLIENT_CALCULE,9999) = ISNULL(hp.AGE_CLIENT_CALCULE,9999)
AND ISNULL(csp.LIB_CSP_NIV1,'Non renseigné') = ISNULL(hp.LIB_CSP_NIV1,'Non renseigné')
AND ISNULL(preat.SCO_PREAT_CALCULE,9999) = ISNULL(hp.SCO_PREAT_CALCULE,9999)
AND ISNULL(eer.COD_SCORE_EER,'9999') = ISNULL(hp.COD_SCORE_EER,'9999')
AND ISNULL(geolife.LIB_SEG_GEOLIFE,'Non renseigné') = ISNULL(hp.LIB_SEG_GEOLIFE,'Non renseigné')
where x.jour_alim BETWEEN DATEADD(dd,-6,@date_obs) and @date_obs
ORDER BY x.jour_alim
						

END;