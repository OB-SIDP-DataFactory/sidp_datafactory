﻿

CREATE PROC [dbo].[PKGMK_PROC_FACT_USAGE_CPT] 
    @DATE_ALIM  DATE  
  , @mois_histo INT         -- valeur 0 par défaut = traitement nominal par défaut
  , @nbRows     INT OUTPUT  -- nb lignes processées
AS  
BEGIN 
    declare @date_analyse as date
--------------------------------------///////////////// Step 1
--calcul de la période de chargement
IF OBJECT_ID('tempdb..#WK_TEMPS_ACCOUNT') IS NOT NULL
BEGIN DROP TABLE #WK_TEMPS_ACCOUNT END
 
select Year, Month, max(StandardDate) as StandardDate
  into #WK_TEMPS_ACCOUNT
  from [dbo].DIM_TEMPS
 where StandardDate <= eomonth(@DATE_ALIM)
   and StandardDate >= dateadd(month, -@mois_histo, eomonth(@DATE_ALIM))
 group by Year, Month

--nettoyage de la table cible (suppréssion des lignes a recharger)
delete from [dbo].[T_FACT_MKT_USAGE_CPT]
where [IDE_TPS] IN (select concat(Year,Month) from #WK_TEMPS_ACCOUNT)

--selection des comptes cibles utilisés pour filtrer les traitements sur la table des mouvements
SELECT distinct NUM_CPT_SAB
INTO #WK_COMPTES
FROM MKT_COMPTE CO
JOIN #WK_TEMPS_ACCOUNT t
ON CO.IDE_TPS = concat(t.Year, t.Month)
--------------------------------------///////////////// Step 2
--Calcul des flags + pré calcul des indicateurs

select MO.NUM_CPT
     , DTE_OPE
     , eomonth(DTE_OPE) as DTE_OPE_EOM
     , MTT_EUR
     , COD_OPE
     , COD_EVE
     , COD_ANA
     , substring(DON_ANA,31,1) as DON_ANA
     , NUM_OPE
     , NUM_PIE
     , case when MTT_EUR >= 0                                         then 1 else 0 end as FLAG_MTT_OPT       -- 1 si FLAG_MTT_OPT positif, 0 si FLAG_MTT_OPT négatif
     , case when COD_OPE in ('A1V','A2V','R1V','R2V')                 then 1 else 0 end as FLAG_REJ_VIR       -- 1 si rejet virement, 0 sinon
     , case when COD_OPE in ('AV0','A0V','R0V')                       then 1 else 0 end as FLAG_VIR           -- 1 si virement, 0 sinon
     , case when COD_OPE in ('A0P','R0P')                             then 1 else 0 end as FLAG_PRE           -- 1 si prélevement, 0 sinon
     , case when COD_OPE in ('A1P','A2P','R1P','R2P')                 then 1 else 0 end as FLAG_REJ_PRE       -- 1 si rejet prélevement, 0 sinon
     , case when COD_OPE in ('CTB') and substring(DON_ANA,31,1) = '1' then 1 else 0 end as FLAG_PAI_CB        -- 1 si paiement carte bancaire, 0 sinon
     , case when COD_OPE in ('CTB') and substring(DON_ANA,31,1) = '2' then 1 else 0 end as FLAG_PAI_MOB       -- 1 si paiement mobile, 0 sinon
     , case when COD_OPE in ('AI0')                                   then 1 else 0 end as FLAG_PAI_CHQ       -- 1 si paiement chèque, 0 sinon
     , case when COD_OPE in ('AI1')                                   then 1 else 0 end as FLAG_REJ_PAI_CHQ   -- 1 si rejet paiement chèque, 0 sinon
     , case when COD_OPE in ('RI0')                                   then 1 else 0 end as FLAG_CHQ_ENC       -- 1 si chèque encaissé, 0 sinon
     , case when COD_OPE in ('RI1')                                   then 1 else 0 end as FLAG_REJ_CHQ_ENC   -- 1 si rejet chèque encaissé, 0 sinon
     , case when COD_OPE in ('FAC')                                   then 1 else 0 end as FLAG_OPT_FAC       -- 1 si opération facturée, 0 sinon
  into #res_Mouvement
  from [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_MOUVEMENT] MO
  join #WK_TEMPS_ACCOUNT t on t.StandardDate = eomonth(DTE_OPE)
  join #WK_COMPTES CO ON CO.NUM_CPT_SAB = MO.NUM_CPT

--------------------------------------///////////////// Step 3
--Pré Calcul des premières opérations 

select MO.NUM_CPT
     , min(case when COD_OPE in ('CTB') and substring(DON_ANA,31,1) = '2' then DTE_OPE else null end) as [DT_PRE_PAI_MOB]
     , min(case when MTT_EUR > 0                                          then DTE_OPE else null end) as [DT_PRE_OPE_DEB]
     , min(case when MTT_EUR < 0                                          then DTE_OPE else null end) as [DT_PRE_OPE_CRE]
     , min(case when COD_OPE in ('*OF') and COD_EVE in ('PBC')            then DTE_OPE else null end) as [DT_VRS_PRI_BVN]
  into #res_Mouvement_2
  from [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_MOUVEMENT] MO
  join #WK_COMPTES CO ON CO.NUM_CPT_SAB = MO.NUM_CPT
 group by MO.NUM_CPT

--------------------------------------///////////////// Step 4
--Pré Calcul de la dernière opération + libellé

		
select m.NUM_CPT		
     , m.DTE_OPE as DT_DER_MVT 		
     , m.DTE_OPE_EOM as DTE_OPE_EOM		
     , m.COD_OPE		
     , m.DON_ANA		
     , m.COD_EVE		
     , case when m.COD_OPE in ('CTB') and substring(m.DON_ANA,31,1) = '1' then 'Paiement CB'		
            when m.COD_OPE in ('CTB') and substring(m.DON_ANA,31,1) = '2' then 'Paiement mobile'		
            when m.COD_OPE in ('AV0','A0V','R0V') then 'Virement'		
            when m.COD_OPE in ('A1V','A2V','R1V','R2V') then 'Rejet virement'		
            when m.COD_OPE in ('A0P','R0P') then 'Prélevement'		
            when m.COD_OPE in ('A1P','A2P','R1P','R2P') then 'Rejet prélevement'		
            when m.COD_OPE in ('AI0') then 'paiement chèque'		
            when m.COD_OPE in ('AI1') then 'Rejet paiement chèque'		
            when m.COD_OPE in ('RI0') then 'Chèque encaissé'		
            when m.COD_OPE in ('RI1') then 'Rejet chèque encaissé'		
            when m.COD_OPE in ('FAC') then 'Opération facturée'		
            when m.COD_OPE in ('*OF') and m.COD_EVE in ('PBC') then 'Prime de bienvenue'		
            else 'Autres'		
        end as TYP_DER_MVT		
     , lead(DTE_OPE_EOM, 1, '9999-12-31') over (partition by NUM_CPT order by DTE_OPE_EOM) as NEXT_DTE_OPE_EOM -- à utiliser pour identifier la dernière opération des mois précédents		
  into #res_Mouvement_3 		
  from ( select MO.NUM_CPT, MO.DTE_OPE, MO.NUM_PIE, MO.COD_OPE, MO.DON_ANA, MO.COD_EVE		
              , eomonth(MO.DTE_OPE) as DTE_OPE_EOM		
              , row_number() over (partition by MO.NUM_CPT, eomonth(MO.DTE_OPE) order by MO.DTE_OPE desc, MO.NUM_PIE) as FLG_OPE_PIE -- flag pour identifier la dernière opération de chaque mois		
           from [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_MOUVEMENT] MO		
		   JOIN #WK_COMPTES CO ON CO.NUM_CPT_SAB = MO.NUM_CPT
		   )m
           where m.FLG_OPE_PIE = 1		
--------------------------------------///////////////// Step 5
--Calcul des indicateurs

insert into [T_FACT_MKT_USAGE_CPT]
(  [IDE_TPS]
 , [IDE_CPT]
 , [DTE_DER_MVT]
 , [TYP_DER_MVT]
 , [DTE_PRE_PAI_MOB]
 , [DTE_PRE_OPE_DEB]
 , [DTE_PRE_OPE_CRE]
 , [NBE_OPE_DEB]
 , [MTT_OPE_DEB]
 , [NBE_OPE_CRE]
 , [MTT_OPE_CRE]
 , [NBE_VIR_EMI]
 , [MTT_VIR_EMI]
 , [NBE_VIR_REC]
 , [MTT_VIR_REC]
 , [NBE_PLV_DEB]
 , [MTT_PLV_DEB]
 , [NBE_PLV_CRE]
 , [MTT_PLV_CRE]
 , [NBE_PAI_CAR]
 , [MTT_PAI_CAR]
 , [NBE_PAI_CHQ]
 , [MTT_PAI_CHQ]
 , [NBE_CHQ_ENC]
 , [MTT_CHQ_ENC]
 , [NBE_OPT_FAC]
 , [MTT_OPT_FAC]
 , [DTE_VRS_PRI_BVN] )
select c.IDE_TPS
     , c.IDE_CPT
     , max(m3.DT_DER_MVT) as [DT_DER_MVT]
     , max(m3.TYP_DER_MVT) as [TYP_DER_MVT]
	 , max(CASE WHEN m2.DT_PRE_PAI_MOB <= c.DTE_OPE_EOM THEN m2.DT_PRE_PAI_MOB ELSE NULL END) as [DT_PRE_PAI_MOB]
     , max(CASE WHEN m2.DT_PRE_OPE_DEB <= c.DTE_OPE_EOM THEN m2.DT_PRE_OPE_DEB ELSE NULL END) as [DT_PRE_OPE_DEB]
     , max(CASE WHEN m2.DT_PRE_OPE_CRE <= c.DTE_OPE_EOM THEN m2.DT_PRE_OPE_CRE ELSE NULL END) as [DT_PRE_OPE_CRE]

     , count( case when FLAG_MTT_OPT = 1 then m.NUM_OPE else null end) 
     - count( case when FLAG_REJ_VIR = 1 and FLAG_MTT_OPT = 0 then m.NUM_OPE else null end) 
     - count( case when FLAG_REJ_CHQ_ENC = 1 and FLAG_MTT_OPT = 0 then m.NUM_OPE else null end)
     - count( case when FLAG_REJ_PRE = 1 and FLAG_MTT_OPT = 0 then m.NUM_OPE else null end) as [NB_OPE_DEB]

     , sum( case when FLAG_MTT_OPT = 1 then m.MTT_EUR else 0 end) 
     + sum( case when FLAG_REJ_VIR = 1 and FLAG_MTT_OPT = 0 then m.MTT_EUR else 0 end) 
     + sum( case when FLAG_REJ_CHQ_ENC = 1 and FLAG_MTT_OPT = 0 then m.MTT_EUR else 0 end)
     + sum( case when FLAG_REJ_PRE = 1 and FLAG_MTT_OPT = 0 then m.MTT_EUR else 0 end) as [MT_OPE_DEB]

     , count( case when FLAG_MTT_OPT = 0 then m.NUM_OPE else null end) 
     - count( case when FLAG_REJ_VIR = 1 and FLAG_MTT_OPT = 1 then m.NUM_OPE else null end) 
     - count( case when FLAG_REJ_CHQ_ENC = 1 and FLAG_MTT_OPT = 1 then m.NUM_OPE else null end)
     - count( case when FLAG_REJ_PRE = 1 and FLAG_MTT_OPT = 1 then m.NUM_OPE else null end) as [NB_OPE_CRE]

     , sum( case when FLAG_MTT_OPT = 0 then ABS(m.MTT_EUR) else 0 end) 
     + sum( case when FLAG_REJ_VIR = 1 and FLAG_MTT_OPT = 1 then m.MTT_EUR else 0 end) 
     + sum( case when FLAG_REJ_CHQ_ENC = 1 and FLAG_MTT_OPT = 1 then m.MTT_EUR else 0 end)
     + sum( case when FLAG_REJ_PRE = 1 and FLAG_MTT_OPT = 1 then m.MTT_EUR else 0 end) as [MT_OPE_CRE]

     , count( case when FLAG_VIR = 1 and FLAG_MTT_OPT = 1 then m.NUM_OPE else null end) 
     - count( case when FLAG_REJ_VIR = 1  and FLAG_MTT_OPT = 0 then m.NUM_OPE else null end) as [NB_VIR_EMI]

     , sum( case when FLAG_VIR = 1 and FLAG_MTT_OPT = 1  then m.MTT_EUR else 0 end) 
     + sum( case when FLAG_REJ_VIR = 1 and FLAG_MTT_OPT = 0 then m.MTT_EUR else 0 end) as [MT_VIR_EMI]

     , count( case when FLAG_VIR = 1 and FLAG_MTT_OPT = 0 then m.NUM_OPE else null end) 
     - count( case when FLAG_REJ_VIR = 1 and FLAG_MTT_OPT = 1 then m.NUM_OPE else null end) as [NB_VIR_REC]

     , sum( case when FLAG_VIR = 1 and FLAG_MTT_OPT = 0 then ABS(m.MTT_EUR) else 0 end) 
     + sum( case when FLAG_REJ_VIR = 1 and FLAG_MTT_OPT = 1 then ABS(m.MTT_EUR) else 0 end) as [MT_VIR_REC]

     , count( case when FLAG_PRE = 1 and FLAG_MTT_OPT = 1 then m.NUM_OPE else null end) 
     - count( case when FLAG_REJ_PRE = 1 and FLAG_MTT_OPT = 0 then m.NUM_OPE else null end) as [NB_PLV_DEB]

     , sum( case when FLAG_PRE = 1  and FLAG_MTT_OPT = 1 then m.MTT_EUR else 0 end) 
     - sum( case when FLAG_REJ_PRE = 1 and FLAG_MTT_OPT = 0 then m.MTT_EUR else 0 end) as [MT_PLV_DEB]

     , count( case when FLAG_PRE = 1 and FLAG_MTT_OPT = 0 then m.NUM_OPE else null end) 
     - count( case when FLAG_REJ_PRE = 1 and FLAG_MTT_OPT = 1 then m.NUM_OPE else null end) as [NB_PLV_CRE]

     , sum( case when FLAG_PRE = 1  and FLAG_MTT_OPT = 0 then m.MTT_EUR else 0 end) 
     - sum( case when FLAG_REJ_PRE = 1 and FLAG_MTT_OPT = 1 then m.MTT_EUR else 0 end) as [MT_PLV_CRE]

     , count( case when FLAG_PAI_CB = 1 then m.NUM_OPE else null end) as [NB_PAI_CAR]

     , sum( case when FLAG_PAI_CB = 1 then m.MTT_EUR else 0 end) as [MT_PAI_CAR]

     , count( case when FLAG_PAI_CHQ = 1 then m.NUM_OPE else null end) 
     - count( case when FLAG_REJ_PAI_CHQ = 1 then m.NUM_OPE else null end)  as [NB_PAI_CHQ]

     , sum( case when FLAG_PAI_CHQ = 1 then m.MTT_EUR else 0 end) 
     - sum( case when FLAG_REJ_PAI_CHQ = 1 then m.MTT_EUR else 0 end) as [MT_PAI_CHQ]

     , count( case when FLAG_CHQ_ENC = 1 then m.NUM_OPE else null end) 
     - count( case when FLAG_REJ_CHQ_ENC = 1 then m.NUM_OPE else null end) as [NB_CHQ_ENC]

     , sum( case when FLAG_CHQ_ENC = 1 then m.MTT_EUR else 0 end) 
     - sum( case when FLAG_REJ_CHQ_ENC = 1 then m.MTT_EUR else 0 end) as [MT_CHQ_ENC]

     , count( case when FLAG_OPT_FAC = 1 then m.NUM_OPE else null end) as [NB_OPT_FAC]

     , sum( case when FLAG_OPT_FAC = 1 then m.MTT_EUR else 0 end) as [MT_OPT_FAC]
     
	 , max(CASE WHEN m2.DT_VRS_PRI_BVN <= c.DTE_OPE_EOM THEN m2.DT_VRS_PRI_BVN ELSE NULL END) as [DT_PRE_OPE_CRE]

  from ( select c.IDE_TPS, c.IDE_CPT, c.NUM_CPT_SAB, t.StandardDate as DTE_OPE_EOM
           from dbo.MKT_COMPTE c
           join #WK_TEMPS_ACCOUNT t on c.IDE_TPS = concat(t.Year, t.Month) ) c
  left join #res_Mouvement m
    on c.NUM_CPT_SAB = m.NUM_CPT and m.DTE_OPE_EOM = c.DTE_OPE_EOM
  left join #res_Mouvement_2 m2
    on m2.NUM_CPT = c.NUM_CPT_SAB
  left join #res_Mouvement_3 m3
    on m3.NUM_CPT = c.NUM_CPT_SAB and ( m3.DTE_OPE_EOM = c.DTE_OPE_EOM or ( m3.DTE_OPE_EOM < c.DTE_OPE_EOM and m3.NEXT_DTE_OPE_EOM > c.DTE_OPE_EOM ) )
 group by c.IDE_TPS, c.IDE_CPT

SELECT @nbRows = @@ROWCOUNT

END