﻿CREATE PROC [dbo].[PKGMK_PROC_CLIENT] @DATE_ALIM DATE  
											    ,@mois_histo int    --traitement nominal par défaut
											    ,@nbRows int OUTPUT -- nb lignes processées

AS  BEGIN 
--------------------------------------///////////////// Step 1
--création d'un sous-ensemble de la table dimension temps

--purge
IF OBJECT_ID('#WK_TEMPS_ACCOUNT') IS NOT NULL
BEGIN truncate TABLE #WK_TEMPS_ACCOUNT END
 
select Year
      ,Month  
	  ,MAX(DIM_TEMPS.StandardDate) as StandardDate

into #WK_TEMPS_ACCOUNT

from DIM_TEMPS

WHERE StandardDate <= @DATE_ALIM
      AND 
	  StandardDate >= dateadd(month,- @mois_histo, @DATE_ALIM)

GROUP BY Year
        ,Month

--------------------------------------///////////////// Step 2
--Merge de la table _Client && _Client_History et insertion dans une table work
--purge
IF OBJECT_ID('#WK_SAB_CLIENTS') IS NOT NULL
BEGIN truncate TABLE #WK_SAB_CLIENTS END

--insertion dans table work
select * into #WK_SAB_CLIENTS
from
(
select Id_SF
      ,IDCustomer__pc
	  ,IDCustomerSAB__pc
	  ,Firstname
	  ,Lastname
      ,PersonBirthdate
	  ,Salutation
	  ,BillingPostalCode
	  ,BillingCountry
	  ,BillingCountryCode
	  ,MaritalStatus__pc
	  ,OccupationNiv1__pc
	  ,OccupationNiv3__pc
	  ,TotalDisposableIncome__pc
	  ,TotalIncome__pc
	  ,JustifiedIncomes__pc
	  ,EmploymentContractStartDate__pc
	  ,BehaviouralScoring__c
	  ,Preattribution__c
	  ,OptInDataTelco__c
	  ,OBSeniority__c
	  ,OFSeniority__c
	  ,GeolifeSegment__c
	  ,PrimaryResidOccupationType__pc
	  ,Validity_StartDate
	  ,Validity_EndDate

from [$(DataHubDatabaseName)].[dbo].[PV_SF_ACCOUNT]
  

 union all 

select  Id_SF
      ,IDCustomer__pc
	  ,IDCustomerSAB__pc
	  ,Firstname
	  ,Lastname
      ,PersonBirthdate
	  ,Salutation
	  ,BillingPostalCode
	  ,BillingCountry
	  ,BillingCountryCode
	  ,MaritalStatus__pc
	  ,OccupationNiv1__pc
	  ,OccupationNiv3__pc
	  ,TotalDisposableIncome__pc
	  ,TotalIncome__pc
	  ,JustifiedIncomes__pc
	  ,EmploymentContractStartDate__pc
	  ,BehaviouralScoring__c
	  ,Preattribution__c
	  ,OptInDataTelco__c
	  ,OBSeniority__c
	  ,OFSeniority__c
	  ,GeolifeSegment__c
	  ,PrimaryResidOccupationType__pc
	  ,Validity_StartDate
	  ,Validity_EndDate

from [$(DataHubDatabaseName)].[dbo].[PV_SF_ACCOUNTHistory]
   

where Validity_EndDate >= DATEADD(MONTH,-@mois_histo,@DATE_ALIM) 
)r

--------------------------------------///////////////// Step 3
--Photo sur la période requise et insertion dans une table res

--purge
IF OBJECT_ID('#RES_CLIENTS') IS NOT NULL
BEGIN truncate TABLE #RES_CLIENTS END

--insertion dans table work
select * into  #RES_CLIENTS
from
(
select d.StandardDate as date_alim
      ,c.Id_SF
	  ,c.IDCustomer__pc
	  ,c.IDCustomerSAB__pc
	  ,c.Firstname
	  ,c.Lastname
	  ,c.PersonBirthdate
	  ,c.Salutation
	  ,c.BillingPostalCode
	  ,c.BillingCountryCode
	  ,c.BillingCountry
	  ,c.MaritalStatus__pc
	  ,c.OccupationNiv1__pc
	  ,c.OccupationNiv3__pc
	  ,c.TotalDisposableIncome__pc
	  ,c.TotalIncome__pc
	  ,c.JustifiedIncomes__pc
	  ,c.EmploymentContractStartDate__pc
	  ,c.BehaviouralScoring__c
	  ,c.Preattribution__c
	  ,c.OptInDataTelco__c
	  ,c.OBSeniority__c
	  ,c.OFSeniority__c
	  ,c.GeolifeSegment__c
	  ,c.PrimaryResidOccupationType__pc
	  ,c.Validity_StartDate
	  ,c.Validity_EndDate

from #WK_TEMPS_ACCOUNT d

outer apply 
	(
	select Id_SF
		  ,IDCustomer__pc
		  ,IDCustomerSAB__pc
		  ,Firstname
		  ,Lastname
		  ,PersonBirthdate
		  ,Salutation
		  ,BillingPostalCode
		  ,BillingCountryCode
		  ,BillingCountry
		  ,MaritalStatus__pc
		  ,OccupationNiv1__pc
		  ,OccupationNiv3__pc
		  ,TotalDisposableIncome__pc
		  ,TotalIncome__pc
		  ,JustifiedIncomes__pc
		  ,EmploymentContractStartDate__pc
		  ,BehaviouralScoring__c
		  ,Preattribution__c
		  ,OptInDataTelco__c
		  ,OBSeniority__c
		  ,OFSeniority__c
		  ,GeolifeSegment__c
		  ,PrimaryResidOccupationType__pc
		  ,Validity_StartDate
		  ,Validity_EndDate

	from #WK_SAB_CLIENTS

	where ( cast(Validity_StartDate as date) <= d.StandardDate and d.StandardDate < cast(Validity_EndDate as date) )
	) c
)r
--------------------------------------///////////////// Step 4
--nettoyage + insertion dans la table cible
--nettoyage de la table cible (suppression des lignes a recharger)
delete from MKT_CLIENT
where [IDE_TPS] IN (SELECT CONCAT(Year,Month) FROM #WK_TEMPS_ACCOUNT)

--Insertion dans la table cible
insert into MKT_CLIENT (   [IDE_TPS]
						  ,[IDE_PERS_SF]
						  ,[IDE_CLT_PHY]
						  ,[NUM_CLT_SAB]
						  ,[PRENOM]
						  ,[NOM]
						  ,[DTE_NAISS]
						  ,[COD_CIV]
						  ,[LIB_CIV]
						  ,[COD_POS_FACT]
						  ,[COD_PAY_FACT]
						  ,[PAY_FACT]
						  ,[COD_STA_MAR]
						  ,[LIB_STA_MAR]
						  ,[COD_CSP_NV1]
						  ,[LIB_CSP_NV1]
						  ,[COD_CSP_NV3]
						  ,[LIB_CSP_NV3]
						  ,[TOT_REV_DISP]
						  ,[TOT_REV]
						  ,[FLG_REV_JUST]
						  ,[DTE_DEB_CONT_TRA]
						  ,[SCO_COMP]
						  ,[SCO_PREAT]
						  ,[FLG_OPT_IN_DON_TEL]
						  ,[ANC_OB]
						  ,[ANC_OF]
						  ,[SEG_GEOLIFE]
						  ,[LIB_CAT_SEG_GEOLIFE]
						  ,[COD_TYP_OCC]
						  ,[LIB_TYP_OCC]  
						   )

select t.ID_TPS
      ,clt.Id_SF
	  ,isnull(clt.IDCustomer__pc, 'NR') 
	  ,clt_sab.DWHCLICLI
	  ,clt.Firstname
	  ,clt.Lastname
	  ,clt.PersonBirthdate 
	  ,isnull(clt.Salutation, 'NR') 
	  ,isnull(c.LIB_CIV,'Non Renseigné') 
	  ,clt.BillingPostalCode
	  ,isnull(clt.BillingCountryCode, 'NR') 
	  ,isnull(clt.BillingCountry, 'Non Renseigné') 
	  ,clt.MaritalStatus__pc
	  ,isnull(sm.LIB_STA_MAR, 'Non Renseigné') 
	  ,clt.OccupationNiv1__pc
	  ,isnull(nv.LIB_CSP_NIV1,'Non Renseigné') 
	  ,clt.OccupationNiv3__pc
	  ,isnull(cnv.LIB_CSP_NIV3,'Non Renseigné') 
	  ,clt.TotalDisposableIncome__pc
	  ,clt.TotalIncome__pc as [REV]
	  ,case when clt.JustifiedIncomes__pc > 0 then 'Oui' when clt.JustifiedIncomes__pc = 0 then 'Non' else 'Non' end as [TOP_REV_JUS]
	  ,clt.EmploymentContractStartDate__pc 
	  ,isnull(clt.BehaviouralScoring__c, 'Non Renseigné') 
	  ,isnull(clt.Preattribution__c, 'Non Renseigné') 
	  ,case when OptInDataTelco__c = 'false'  then 'Non' else 'Oui' end as TOP_IDE_ORA
	  ,isnull(clt.OBSeniority__c, 'Non Renseigné')  
	  ,clt.OFSeniority__c
	  ,isnull(clt.GeolifeSegment__c, 'Non Renseigné') 
	  ,isnull(csg.LIB_CAT_SEG_GEO, 'Non Renseigné') 
	  ,isnull(clt.PrimaryResidOccupationType__pc, 'NR') 
	  ,isnull(typ.LIB_TYP_OCC, 'Non Renseigné') 

from #RES_CLIENTS clt

inner join [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_CLIENT] clt_sab    -- filtre sur les clients SF présents dans SAB
		  on clt_sab.DWHCLICLI = clt.IDCustomerSAB__pc

left join [dbo].DIM_CIVILITE c
          on c.COD_CIV = clt.Salutation

left join [dbo].DIM_STAT_MARITAL sm
          on sm.COD_STA_MAR = clt.MaritalStatus__pc

left join [dbo].DIM_CSP_NIV1 nv
          on nv.COD_CSP_NIV1 = clt.OccupationNiv1__pc

left join [dbo].DIM_CSP_NIV3 cnv
          on cnv.COD_CSP_NIV3 = clt.OccupationNiv3__pc

left join [dbo].DIM_SEG_GEOLIFE sg
          on sg.LIB_SEG_GEO = clt.GeolifeSegment__c

left join [dbo].DIM_CAT_SEG_GEOLIFE csg
          on csg.ID_CAT_SEG_GEO = sg.ID_CAT_SEG_GEO

left join [dbo].DIM_TYP_OCC_LOG_PPA typ
          on typ.COD_TYP_OCC = clt.PrimaryResidOccupationType__pc

left join [dbo].DIM_TEMPS_VUE_MKT t
          on year(t.StandardDate) = year(clt.date_alim) and month(t.StandardDate) = month(clt.date_alim)

SELECT @nbRows = @@ROWCOUNT

END