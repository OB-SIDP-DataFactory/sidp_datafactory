﻿
CREATE PROCEDURE [dbo].[PKGCP_PROC_CSL_PROFIL_CLIENT]
@DTE_OBS date

AS BEGIN

SET @DTE_OBS = CAST(@DTE_OBS as date);
SET DATEFIRST 1; -->1    Lundi

With 
cte_jour_alim_list AS
( 	SELECT T1.StandardDate AS jour_alim
    FROM dbo.[DIM_TEMPS] T1
    WHERE T1.StandardDate >= CAST(DATEADD(day, -6, @DTE_OBS) AS DATE) AND T1.StandardDate <= @DTE_OBS --7 jours glissants 
)
--***** Tous les clients *****--
,cte_client_ALL AS
( SELECT ref.[DTE_TRAITEMENT] 
        ,ref.[NUM_CLT_SAB]
		,cpt.[NUM_CLI_SAB]
		,cpt.IDE_OPPRT
		,ref.[IDE_PERS_SF] 
		,CAST(ref.[DTE_NAIS] as date) as [DTE_NAIS] 
		,DATEDIFF(YEAR, ref.DTE_NAIS, ref.[DTE_TRAITEMENT]) as AGE_CLIENT
		,ref.COD_CIV  as CIVILITE
		,dim_csp.LIB_CSP_NIV1 as LIB_CSP_NIV1 
		,case when (ref.[SCO_PREAT] is not null and charindex('.',ref.[SCO_PREAT])<>0) then cast(substring(ref.[SCO_PREAT],1, charindex('.',ref.[SCO_PREAT])-1) as int) else ref.[SCO_PREAT] end as [SCO_PREAT]
		,opp.[SCO_ENT_REL] as COD_SCORE_EER
		,dim_eer.LIB_SCORE_EER as LIB_SCORE_EER
		,ref.[SEG_GEOLIFE]
	FROM [REF_CLIENT] ref
    inner join [dbo].[MKT_COMPTE] cpt on ref.[NUM_CLT_SAB] = cpt.[NUM_CLI_SAB]  and ref.[DTE_TRAITEMENT] = cpt.[DTE_TRAITEMENT]
	left join [dbo].[MKT_OPPORTUNITE] opp ON cpt.IDE_OPPRT = opp.IDE_OPPRT
	LEFT JOIN DIM_CSP_NIV1 dim_csp 
	ON ref.COD_CSP_NV1 = dim_csp.COD_CSP_NIV1
	LEFT JOIN [DIM_SCORE_EER] dim_eer 
	ON opp.[SCO_ENT_REL] = dim_eer.COD_SCORE_EER
where ref.DTE_TRAITEMENT  >= CAST(DATEADD(day, -6, @DTE_OBS) AS DATE) AND ref.DTE_TRAITEMENT <= @DTE_OBS
AND FLAG_CPT = 'CSL'

)
--select * from cte_client_ALL ;

--Profil client créé
,cte_client_cnt AS ( 
SELECT	[DTE_TRAITEMENT],
		COUNT(distinct [IDE_PERS_SF]) as NB_CLIENT, 
		AGE_CLIENT,
		PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY AGE_CLIENT) OVER (PARTITION BY [DTE_TRAITEMENT]) AS AGE_CLIENT_MEDIAN,
		CASE
			WHEN CIVILITE ='Ms.' THEN 1
			WHEN CIVILITE ='Mr.' THEN 2
			ELSE 0
		END as RANG_CIVILITE,
		CASE
			WHEN CIVILITE ='Ms.' THEN 'Femmes'
			WHEN CIVILITE ='Mr.' THEN 'Hommes'
			ELSE 'Non renseigné'
		END as CIVILITE_CALCULEE,
		CASE
			WHEN AGE_CLIENT <= 17 THEN 17
			WHEN 18 <= AGE_CLIENT and AGE_CLIENT <= 29 THEN 2
			WHEN 30 <= AGE_CLIENT and AGE_CLIENT <= 44 THEN 44
			WHEN 45 <= AGE_CLIENT and AGE_CLIENT <= 59 THEN 59
			WHEN 60 <= AGE_CLIENT and AGE_CLIENT <= 74 THEN 74
			WHEN AGE_CLIENT >= 75 THEN 75
			ELSE 9999
		END as AGE_CLIENT_CALCULE, 
		CASE
			WHEN AGE_CLIENT <= 17 THEN '17 ans et moins'
			WHEN 18 <= AGE_CLIENT and AGE_CLIENT <= 29 THEN 'Jeunes (18-29 ans)'
			WHEN 30 <= AGE_CLIENT and AGE_CLIENT <= 44 THEN 'Jeunes familles (30-44 ans)'
			WHEN 45 <= AGE_CLIENT and AGE_CLIENT <= 59 THEN 'Familles installées (45-59 ans)'
			WHEN 60 <= AGE_CLIENT and AGE_CLIENT <= 74 THEN 'Jeunes séniors (60-74 ans)'
			WHEN AGE_CLIENT >= 75 THEN '75 ans et plus'
			ELSE 'Non renseigné'
		END as LIB_AGE_CLIENT,
		LIB_CSP_NIV1,
		SCO_PREAT,
		CASE
			WHEN 0 <= SCO_PREAT and SCO_PREAT <= 10 THEN 10
			WHEN 11 <= SCO_PREAT and SCO_PREAT <= 40 THEN 40
			WHEN 41 <= SCO_PREAT and SCO_PREAT <= 99 THEN 99
			ELSE 9999
		END as SCO_PREAT_CALCULE,
		CASE
			WHEN 0 <= SCO_PREAT and SCO_PREAT <= 10 THEN 'Mauvais [0;10]'
			WHEN 11 <= SCO_PREAT and SCO_PREAT <= 40 THEN 'Neutre [11;40]'
			WHEN 41 <= SCO_PREAT and SCO_PREAT <= 99 THEN 'Bon [41;99]'
			ELSE 'Non renseigné'
		END as LIB_SCO_PREAT,
		COD_SCORE_EER,
		LIB_SCORE_EER,
		SEG_GEOLIFE,
		CASE
			WHEN SEG_GEOLIFE in ('1 - urbain dynamique', 'urbain dynamique - 1', 'urbain dynamique') THEN 'Urbain dynamique'
			WHEN SEG_GEOLIFE in ('2 - urbain familial aise', 'urbain familial aise - 2', 'urbain familial aise') THEN 'Urbain familial aise'
			WHEN SEG_GEOLIFE in ('3 - urbain classe moyenne', 'urbain classe moyenne - 3', 'urbain classe moyenne') THEN 'Urbain classe moyenne'
			WHEN SEG_GEOLIFE in ('4 - populaire', 'populaire - 4', 'populaire') THEN 'Populaire'
			WHEN SEG_GEOLIFE in ('5 - urbain defavorise', 'urbain defavorise - 5', 'urbain defavorise') THEN 'Urbain defavorise'
			WHEN SEG_GEOLIFE in ('6 - periurbain en croissance', 'periurbain en croissance - 6', 'periurbain en croissance') THEN 'Periurbain en croissance'
			WHEN SEG_GEOLIFE in ('7 - pavillonnaire familial aise', 'pavillonnaire familial aise - 7', 'pavillonnaire familial aise') THEN 'Pavillonnaire familial aise'
			WHEN SEG_GEOLIFE in ('8 - rural dynamique', 'rural dynamique - 8', 'rural dynamique') THEN 'Rural dynamique'
			WHEN SEG_GEOLIFE in ('9 - rural ouvrier', 'rural ouvrier - 9', 'rural ouvrier') THEN 'Rural ouvrier'
			WHEN SEG_GEOLIFE in ('10 - rural traditionnel', 'rural traditionnel - 10', 'rural traditionnel') THEN 'Rural traditionnel'
			WHEN SEG_GEOLIFE in ('11 - residence secondaire', 'residence secondaire - 11', 'residence secondaire') THEN 'Residence secondaire'
			WHEN SEG_GEOLIFE is null THEN 'Non renseigné'
		END as LIB_SEG_GEOLIFE
FROM cte_client_ALL c
GROUP BY [DTE_TRAITEMENT],CIVILITE, AGE_CLIENT, LIB_CSP_NIV1, SCO_PREAT, COD_SCORE_EER, LIB_SCORE_EER, SEG_GEOLIFE
)
--select * from cte_client_cnt
,cte_sexe_list AS
( 
SELECT DISTINCT CASE
			WHEN CIVILITE ='Ms.' THEN 1
			WHEN CIVILITE ='Mr.' THEN 2
		END as RANG_CIVILITE,
		CASE
			WHEN CIVILITE ='Ms.' THEN 'Femmes'
			WHEN CIVILITE ='Mr.' THEN 'Hommes'
		END as CIVILITE_CALCULEE
FROM cte_client_ALL
)

--select * from cte_sexe_list;

,cte_age_list AS
( 
SELECT DISTINCT CASE
			WHEN AGE_CLIENT <= 17 THEN 17
			WHEN 18 <= AGE_CLIENT and AGE_CLIENT <= 29 THEN 2
			WHEN 30 <= AGE_CLIENT and AGE_CLIENT <= 44 THEN 44
			WHEN 45 <= AGE_CLIENT and AGE_CLIENT <= 59 THEN 59
			WHEN 60 <= AGE_CLIENT and AGE_CLIENT <= 74 THEN 74
			WHEN AGE_CLIENT >= 75 THEN 75
			ELSE 9999
		END as AGE_CLIENT_CALCULE, 
		CASE
			WHEN AGE_CLIENT <= 17 THEN '17 ans et moins'
			WHEN 18 <= AGE_CLIENT and AGE_CLIENT <= 29 THEN 'Jeunes (18-29 ans)'
			WHEN 30 <= AGE_CLIENT and AGE_CLIENT <= 44 THEN 'Jeunes familles (30-44 ans)'
			WHEN 45 <= AGE_CLIENT and AGE_CLIENT <= 59 THEN 'Familles installées (45-59 ans)'
			WHEN 60 <= AGE_CLIENT and AGE_CLIENT <= 74 THEN 'Jeunes séniors (60-74 ans)'
			WHEN AGE_CLIENT >= 75 THEN '75 ans et plus'
			ELSE 'Non renseigné'
		END as LIB_AGE_CLIENT
FROM cte_client_ALL
)

--select * from cte_age_list;

,cte_csp_list AS
(
SELECT distinct LIB_CSP_NIV1 from cte_client_ALL
)

--select * from cte_csp_list

,cte_preat_list AS
(
SELECT distinct CASE
			WHEN 0 <= SCO_PREAT and SCO_PREAT <= 10 THEN 10
			WHEN 11 <= SCO_PREAT and SCO_PREAT <= 40 THEN 40
			WHEN 41 <= SCO_PREAT and SCO_PREAT <= 99 THEN 99
			ELSE 9999
		END as SCO_PREAT_CALCULE,
		CASE
			WHEN 0 <= SCO_PREAT and SCO_PREAT <= 10 THEN 'Mauvais [0;10]'
			WHEN 11 <= SCO_PREAT and SCO_PREAT <= 40 THEN 'Neutre [11;40]'
			WHEN 41 <= SCO_PREAT and SCO_PREAT <= 99 THEN 'Bon [41;99]'
			ELSE 'Non renseigné'
		END as LIB_SCO_PREAT
from cte_client_ALL
)

--select * from cte_preat_list;

,cte_eer_list AS
(
SELECT distinct COD_SCORE_EER, LIB_SCORE_EER from cte_client_ALL
)

--select * from cte_eer_list;

,cte_geolife_list AS
(
SELECT distinct CASE
			WHEN SEG_GEOLIFE in ('1 - urbain dynamique', 'urbain dynamique - 1', 'urbain dynamique') THEN 'Urbain dynamique'
			WHEN SEG_GEOLIFE in ('2 - urbain familial aise', 'urbain familial aise - 2', 'urbain familial aise') THEN 'Urbain familial aise'
			WHEN SEG_GEOLIFE in ('3 - urbain classe moyenne', 'urbain classe moyenne - 3', 'urbain classe moyenne') THEN 'Urbain classe moyenne'
			WHEN SEG_GEOLIFE in ('4 - populaire', 'populaire - 4', 'populaire') THEN 'Populaire'
			WHEN SEG_GEOLIFE in ('5 - urbain defavorise', 'urbain defavorise - 5', 'urbain defavorise') THEN 'Urbain defavorise'
			WHEN SEG_GEOLIFE in ('6 - periurbain en croissance', 'periurbain en croissance - 6', 'periurbain en croissance') THEN 'Periurbain en croissance'
			WHEN SEG_GEOLIFE in ('7 - pavillonnaire familial aise', 'pavillonnaire familial aise - 7', 'pavillonnaire familial aise') THEN 'Pavillonnaire familial aise'
			WHEN SEG_GEOLIFE in ('8 - rural dynamique', 'rural dynamique - 8', 'rural dynamique') THEN 'Rural dynamique'
			WHEN SEG_GEOLIFE in ('9 - rural ouvrier', 'rural ouvrier - 9', 'rural ouvrier') THEN 'Rural ouvrier'
			WHEN SEG_GEOLIFE in ('10 - rural traditionnel', 'rural traditionnel - 10', 'rural traditionnel') THEN 'Rural traditionnel'
			WHEN SEG_GEOLIFE in ('11 - residence secondaire', 'residence secondaire - 11', 'residence secondaire') THEN 'Residence secondaire'
			WHEN SEG_GEOLIFE is null THEN 'Non renseigné'
		END as LIB_SEG_GEOLIFE
from cte_client_ALL
)

--select * from cte_geolife_list

--Profil client créé Jour
,cte_client_cumul_jour as (
SELECT	[DTE_TRAITEMENT], 
		ISNULL(SUM(NB_CLIENT),0) as NB_CLIENT, 
		RANG_CIVILITE,
		CIVILITE_CALCULEE,
		AVG(AGE_CLIENT_MEDIAN) as AGE_CLIENT_MEDIAN, 
		AGE_CLIENT_CALCULE, 
		LIB_AGE_CLIENT, 
		LIB_CSP_NIV1, 
		SCO_PREAT_CALCULE, 
		LIB_SCO_PREAT,
		COD_SCORE_EER,
		LIB_SCORE_EER,
		LIB_SEG_GEOLIFE
FROM  cte_client_cnt c 
GROUP BY [DTE_TRAITEMENT],
		RANG_CIVILITE,
		CIVILITE_CALCULEE,
		AGE_CLIENT_CALCULE, 
		LIB_AGE_CLIENT, 
		LIB_CSP_NIV1, 
		SCO_PREAT_CALCULE, 
		LIB_SCO_PREAT,
		COD_SCORE_EER,
		LIB_SCORE_EER, 
		LIB_SEG_GEOLIFE
)
--select * from cte_client_cumul_jour

select  x.jour_alim,
        ISNULL(sexe.RANG_CIVILITE,0) as RANG_CIVILITE,
		ISNULL(sexe.CIVILITE_CALCULEE,'Non renseigné') as CIVILITE_CALCULEE,
		ISNULL(age.AGE_CLIENT_CALCULE,9999) as AGE_CLIENT_CALCULE, 
		ISNULL(age.LIB_AGE_CLIENT,'Non renseigné') as LIB_AGE_CLIENT, 
		ISNULL(csp.LIB_CSP_NIV1,'Non renseigné') as LIB_CSP_NIV1, 
		ISNULL(preat.SCO_PREAT_CALCULE,9999) as SCO_PREAT_CALCULE, 
		ISNULL(preat.LIB_SCO_PREAT,'Non renseigné') as LIB_SCO_PREAT,
		ISNULL(eer.COD_SCORE_EER,'9999') as COD_SCORE_EER,
		ISNULL(eer.LIB_SCORE_EER,'Non renseigné') as LIB_SCORE_EER,
		ISNULL(geolife.LIB_SEG_GEOLIFE,'Non renseigné') as LIB_SEG_GEOLIFE,
		ISNULL(j_median.AGE_CLIENT_MEDIAN,0) as AGE_CLIENT_MEDIAN,
		ISNULL(j.NB_CLIENT,0) as NB_CLIENT
FROM cte_jour_alim_list x
LEFT join cte_sexe_list sexe on 1=1
LEFT JOIN cte_age_list age on 1=1
LEFT JOIN cte_csp_list csp on 1=1
LEFT JOIN cte_preat_list preat on 1=1
LEFT JOIN cte_eer_list eer on 1=1
LEFT JOIN cte_geolife_list geolife on 1=1
LEFT JOIN cte_client_cumul_jour j on x.jour_alim=j.[DTE_TRAITEMENT]
AND ISNULL(sexe.RANG_CIVILITE,0) = ISNULL(j.RANG_CIVILITE,0)
AND ISNULL(age.AGE_CLIENT_CALCULE,9999) = ISNULL(j.AGE_CLIENT_CALCULE,9999) 
AND ISNULL(csp.LIB_CSP_NIV1,'Non renseigné') = ISNULL(j.LIB_CSP_NIV1,'Non renseigné')
AND ISNULL(preat.SCO_PREAT_CALCULE,9999) = ISNULL(j.SCO_PREAT_CALCULE,9999)
AND ISNULL(eer.COD_SCORE_EER,'9999') = ISNULL(j.COD_SCORE_EER,'9999')
AND ISNULL(geolife.LIB_SEG_GEOLIFE,'Non renseigné') = ISNULL(j.LIB_SEG_GEOLIFE,'Non renseigné')
LEFT JOIN (select distinct [DTE_TRAITEMENT], AGE_CLIENT_MEDIAN from cte_client_cumul_jour) j_median on x.jour_alim=j_median.[DTE_TRAITEMENT]
ORDER BY x.jour_alim						

END;