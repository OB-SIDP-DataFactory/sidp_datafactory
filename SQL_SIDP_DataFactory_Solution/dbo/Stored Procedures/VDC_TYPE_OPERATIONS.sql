﻿CREATE PROC [dbo].[VDC_TYPE_OPERATIONS]
AS
BEGIN 
	DECLARE @DATE_REF as date;
	DECLARE @DATE_REF_PREC as date;
	SET @DATE_REF =  cast(CONVERT(varchar,dateadd(d,-(day(getdate())),getdate()),126) as date);
	SET @DATE_REF_PREC =EOMONTH(@DATE_REF,-1);
	DECLARE @mois_ref as integer;
	SET @mois_ref = format(@DATE_REF,'yyyyMM');
	DECLARE @mois_ref_prec as integer;
	SET @mois_ref_prec = format(DATEADD(MONTH,-1,@DATE_REF),'yyyyMM');


	DECLARE @DATE_REF_act as date;
	SET @DATE_REF_act = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) --First day of current month '2019-07-01';
	DECLARE @DATE_REF_act_prec as date;
	SET @DATE_REF_act_prec =  DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) --First day of previous month '2019-06-01';
	DECLARE @mois_ref_act as integer;
	SET @mois_ref_act = format(@DATE_REF_act,'yyyyMM');
	DECLARE @mois_ref_act_prec as integer;
	SET @mois_ref_act_prec = format(@DATE_REF_act_prec,'yyyyMM');
	DECLARE @dern_j_mois as date;
	SET @dern_j_mois = cast(DATEADD(DAY, DATEDIFF(day, 0, @DATE_REF_act) - 1, 0) as date);
	DECLARE @dern_j_mois_prec as date;
	SET @dern_j_mois_prec = cast(DATEADD(DAY, DATEDIFF(day, 0, @DATE_REF_act_prec) - 1, 0) as date);


	DROP TABLE IF EXISTS #VDC_TYPE_OPERATION;
	CREATE TABLE #VDC_TYPE_OPERATION(
		[Date_operation] [nvarchar](6) NULL,
		[Type_Op1] [nvarchar](10) NULL,
		[Type_Op2] [nvarchar](10) NULL,
		[Nb_Ope] [int] NULL,
		[Nb_Ope1] [int] NULL,
		[Nb_Ope2] [int] NULL,
		[Nb_Ope3] [int] NULL,
		[Nb_Ope4] [int] NULL,
		[Nb_Ope5] [int] NULL,
		[Nb_Ope6] [int] NULL,
		[Nb_Ope_France] [int] NULL,
		[Nb_Ope_ZoneEuro] [int] NULL,
		[Nb_Ope_HorsZoneEuro] [int] NULL,
		[Montant_en_k€] decimal(10,2) NULL,
		[Nb_moyen_Ope] decimal(10,2) NULL,
		[Montant_moyen_Ope] decimal(10,2) NULL,
		[Nb_médian_Ope] [int] NULL,
		[Montant_médian_Ope] decimal(10,2) NULL,
	) ON [PRIMARY];

	TRUNCATE TABLE [dbo].[VDC_TYPE_OPERATION];
	-- Méthode : pour produire les chiffres de septembre 2018, par exemple, il faut renseigner la date du 2018-09-30. 

	-- Création de la table des comptes CAV
	if OBJECT_ID('tempdb..#TAB_CAV') IS NOT NULL
	BEGIN
		DROP TABLE #TAB_CAV
	END  
	CREATE TABLE #TAB_CAV
	(
		DWHCPTCOM [VARCHAR](20),
		DWHCPTPPAL [VARCHAR](7),
		DWHCPTRUB [VARCHAR](10),
		DWHCPTDAO DATE,
		DWHCPTDAC DATE,
		DWHCPTCPT DECIMAL(18,3),
		DWHCPTMTA DECIMAL(18,3),
		ENROLMENT_FOLDER_ID DECIMAL(19,0),
		SALESFORCE_OPPORTUNITY_ID NVARCHAR(255)
	);

	if OBJECT_ID('tempdb..#TAB_MVT') IS NOT NULL
	-- Table mouvements
	BEGIN
		DROP TABLE #TAB_MVT
	END  
	CREATE TABLE #TAB_MVT
	(
	DWHCPTCOM [VARCHAR](20),
	DWHCPTPPAL [VARCHAR](7),
	DWHCPTRUB [VARCHAR](10),
	DTE_OPE DATE,
	COD_OPE [VARCHAR](3),
	COD_EVE [VARCHAR](3),
	DON_ANA [VARCHAR](80),
	COD_ANA [VARCHAR](6),
	MTT_EUR [DECIMAL](16,2)
	);

	if OBJECT_ID('tempdb..#TAB_MVT_CRED') IS NOT NULL
	-- Table virement créditeur
	begin drop table #TAB_MVT_CRED end  
	create table #TAB_MVT_CRED
	(
		DWHCPTCOM [VARCHAR](20),
		DWHCPTPPAL [VARCHAR](7),
		DWHCPTRUB [VARCHAR](10),
		DTE_OPE DATE,
		COD_OPE [VARCHAR](3),
		COD_EVE [VARCHAR](3),
		NAT_OPE [VARCHAR](3),
		DON_ANA [VARCHAR](80),
		COD_ANA [VARCHAR](6),
		MTT_EUR [DECIMAL](16,2),
		TYPE_MTT [VARCHAR](20)
	)

	if OBJECT_ID('tempdb..#tab_vir') IS NOT NULL
	-- Table virement
	begin drop table #tab_vir end  
	create table #tab_vir
	(
	DWHCPTCOM [VARCHAR](20),
	DWHCPTPPAL [VARCHAR](7),
	DWHCPTRUB [VARCHAR](10),
	DTE_OPE DATE,
	COD_OPE [VARCHAR](3),
	COD_EVE [VARCHAR](3),
	NAT_OPE [VARCHAR](3),
	DON_ANA [VARCHAR](80),
	COD_ANA [VARCHAR](6),
	MTT_EUR [DECIMAL](16,2),
	TYPE_MTT [VARCHAR](20)
	);


	if OBJECT_ID('tempdb..#TAB_MVT_ACT') IS NOT NULL
	-- Table mouvements CAV
	begin drop table #TAB_MVT_ACT end  
	create table #TAB_MVT_ACT
	(
	DWHCPTCOM [VARCHAR](20),
	DWHCPTPPAL [VARCHAR](7),
	DWHCPTRUB [VARCHAR](10),
	DTE_OPE DATE,
	COD_OPE [VARCHAR](3),
	COD_EVE [VARCHAR](3),
	DON_ANA [VARCHAR](80),
	COD_ANA [VARCHAR](6),
	MTT_EUR [DECIMAL](16,2)
	);

	if OBJECT_ID('tempdb..#TAB_MVT_ACT_PREC') IS NOT NULL
	-- Table mouvements CAV
	BEGIN
		DROP TABLE #TAB_MVT_ACT_PREC
	END  
	CREATE TABLE #TAB_MVT_ACT_PREC
	(
		DWHCPTCOM [VARCHAR](20),
		DWHCPTPPAL [VARCHAR](7),
		DWHCPTRUB [VARCHAR](10),
		DTE_OPE DATE,
		COD_OPE [VARCHAR](3),
		COD_EVE [VARCHAR](3),
		DON_ANA [VARCHAR](80),
		COD_ANA [VARCHAR](6),
		MTT_EUR [DECIMAL](16,2)
	);

	if OBJECT_ID('tempdb..#tab_activite') IS NOT NULL
	-- Table activité CAV
	begin drop table #tab_activite end  
	create table #tab_activite
	(
	num_cpt [varchar](20),
	id_client [varchar](7),
	cod_rub [varchar](7),
	Nv_Activite_cav [varchar](15),
	Tranche_Activite_cav [varchar](15)
	)
	if OBJECT_ID('tempdb..#tab_activite_prec') IS NOT NULL
	-- Table activité CAV
	begin drop table #tab_activite_prec end  
	create table #tab_activite_prec
	(
	num_cpt [varchar](20),
	id_client [varchar](7),
	cod_rub [varchar](7),
	Nv_Activite_cav [varchar](15),
	Tranche_Activite_cav [varchar](15)
	)

	if OBJECT_ID('tempdb..#tab_deja_facture') IS NOT NULL
	begin drop table #tab_deja_facture end  

	if OBJECT_ID('tempdb..#tab_deja_facture') IS NOT NULL
	begin drop table #tab_deja_facture end  
	create table #tab_deja_facture
	(num_cpt [varchar](20),
	 mtt_total [decimal](16, 2));


	 -- suppression de la table temporaire
	if OBJECT_ID('tempdb..#tab_facture_M_1') IS NOT NULL
	begin drop table #tab_facture_M_1 end  


	if OBJECT_ID('tempdb..#tab_facture_M_1') IS NOT NULL
	begin drop table #tab_facture_M_1 end  
	create table #tab_facture_M_1
	(num_cpt [varchar](20),
	 mtt_total [decimal](16, 2));

	-- Alimentation de la table des comptes CAV

	insert into #TAB_CAV (DWHCPTCOM, DWHCPTPPAL, DWHCPTRUB, DWHCPTDAO, DWHCPTDAC,DWHCPTCPT,DWHCPTMTA,ENROLMENT_FOLDER_ID, SALESFORCE_OPPORTUNITY_ID)
	(
		select
			DWHCPTCOM,
			DWHCPTPPAL,
			DWHCPTRUB,
			DWHCPTDAO,
			DWHCPTDAC,
			DWHCPTCPT,
			DWHCPTMTA,
			ENROLMENT_FOLDER_ID,
			SALESFORCE_OPPORTUNITY_ID

		from
			(select 
				DWHCPTPPAL, DWHCPTCOM, DWHCPTRUB, DWHCPTDAO, DWHCPTDAC,DWHCPTCPT,DWHCPTMTA
			   , row_number() over (partition by DWHCPTCOM order by Validity_StartDate desc) as Validity_Flag
			 from 
				[$(DataHubDatabaseName)].[dbo].[VW_PV_SA_Q_COMPTE]	WITH(NOLOCK) where DWHCPTRUB = '251180') cpt  
				inner join [$(DataHubDatabaseName)].[dbo].pv_fe_equipment e	WITH(NOLOCK) on e.equipment_number = cpt.DWHCPTCOM
				inner join [$(DataHubDatabaseName)].[dbo].PV_FE_ENROLMENTFOLDER f	WITH(NOLOCK) on f.ID_FE_ENROLMENT_FOLDER = e.ENROLMENT_FOLDER_ID
				inner join [$(DataHubDatabaseName)].[dbo].PV_SF_OPPORTUNITY o	WITH(NOLOCK) on o.Id_SF = f.SALESFORCE_OPPORTUNITY_ID and o.CommercialOfferCode__c = 'OC80' and o.StageName = '09'
		where cpt.Validity_Flag = 1
	);



	-- Alimentation de la table des mouvements
	insert into #TAB_MVT (DWHCPTCOM,DWHCPTPPAL,DWHCPTRUB ,DTE_OPE ,COD_OPE ,COD_EVE ,DON_ANA ,COD_ANA ,MTT_EUR )
	(
	select
	DWHCPTCOM,DWHCPTPPAL,DWHCPTRUB ,DTE_OPE ,COD_OPE ,COD_EVE ,DON_ANA ,COD_ANA ,MTT_EUR
	from
		#TAB_CAV cpt 
		inner join [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_MOUVEMENT] m1	WITH(NOLOCK) on m1.NUM_CPT = cpt.DWHCPTCOM and format(DTE_OPE,'yyyyMM') >= @mois_ref_prec and format(DTE_OPE,'yyyyMM') <= @mois_ref 
	where 
	cpt.DWHCPTDAO <= @DATE_REF and (cpt.DWHCPTDAC is null or (format(cpt.DWHCPTDAC,'yyyyMM') >= @mois_ref_prec and format(cpt.DWHCPTDAC,'yyyyMM') <= @mois_ref)  or cpt.DWHCPTDAC > @DATE_REF_PREC)		-- comptes ouverts ou clos dans le mois
	and
	(
	COD_OPE in ('A0P', 'R0P','A1P', 'A2P', 'R1P', 'R2P','A0V', 'R0V','A1V', 'A2V','R1V','R2V','AI1','RI0','AI0','RI1','CTB') )
	);

	-- Alimentation de la table des mouvements virements créditeurs
	insert into #TAB_MVT_CRED (DWHCPTCOM,DWHCPTPPAL,DWHCPTRUB ,DTE_OPE ,COD_OPE ,COD_EVE ,NAT_OPE,DON_ANA ,COD_ANA ,MTT_EUR,TYPE_MTT )
	-- virement autre que top-up ou 1er versement ou SMS ou demande argent
	(
	select
	DWHCPTCOM,DWHCPTPPAL,DWHCPTRUB ,DTE_OPE ,COD_OPE ,COD_EVE ,NAT_OPE,DON_ANA ,COD_ANA ,MTT_EUR,'Virement classique'
	from
		#TAB_CAV cpt 
		inner join [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_MOUVEMENT] m1	WITH(NOLOCK) on m1.NUM_CPT = cpt.DWHCPTCOM and format(DTE_OPE,'yyyyMM') >= @mois_ref_prec and format(DTE_OPE,'yyyyMM') <= @mois_ref 

	where 
	cpt.DWHCPTDAO <= @DATE_REF 
	and (cpt.DWHCPTDAC IS NULL or (format(cpt.DWHCPTDAC,'yyyyMM') >= @mois_ref_prec and format(cpt.DWHCPTDAC,'yyyyMM') <= @mois_ref)  or cpt.DWHCPTDAC > @DATE_REF_PREC)		-- comptes ouverts ou clos dans le mois
	and
	(
	--([COD_OPE] = ('CTB') and COD_EVE = 'IMM') OR
	COD_OPE in ('A0V', 'R0V','A1V', 'A2V','R1V','R2V') and (nat_ope not in ('OUI','PUI','PUE','RUI') or nat_ope IS NULL))
	);


	insert into #TAB_MVT_CRED (DWHCPTCOM,DWHCPTPPAL,DWHCPTRUB ,DTE_OPE ,COD_OPE ,COD_EVE ,NAT_OPE,DON_ANA ,COD_ANA ,MTT_EUR,TYPE_MTT)
	-- virement top-up 
	(
	select substring(m.DON_ANA,19,11),DWHCPTPPAL,DWHCPTRUB ,DTE_OPE ,COD_OPE ,COD_EVE ,NAT_OPE,DON_ANA ,COD_ANA ,-MTT_EUR,'Top-up'
	from
			[$(DataHubDatabaseName)].[dbo].PV_SA_Q_MOUVEMENT m	WITH(NOLOCK)
			inner join #TAB_CAV c  on c.DWHCPTCOM = substring(m.DON_ANA,19,11) 		
		where
			m.NUM_CPT = '70200043801'
			and format(m.DTE_OPE,'yyyyMM') >= @mois_ref_prec and format(m.DTE_OPE,'yyyyMM') <= @mois_ref 
			and COD_OPE = 'A0V' and COD_EVE = 'CLI'and NAT_OPE = 'OUI' and MTT_EUR > 0
	);


	insert into #TAB_MVT_CRED (DWHCPTCOM,DWHCPTPPAL,DWHCPTRUB ,DTE_OPE ,COD_OPE ,COD_EVE ,NAT_OPE,DON_ANA ,COD_ANA ,MTT_EUR,TYPE_MTT)
	-- virement 1er versement
	(

	select substring(m.DON_ANA,19,11),DWHCPTPPAL,DWHCPTRUB ,DTE_OPE ,COD_OPE ,COD_EVE , NAT_OPE, DON_ANA ,COD_ANA ,-MTT_EUR,'1er versement'
	from
			[$(DataHubDatabaseName)].[dbo].PV_SA_Q_MOUVEMENT m	WITH(NOLOCK)
			inner join #TAB_CAV c  on c.DWHCPTCOM = substring(m.DON_ANA,19,11) 
		
		where
			m.NUM_CPT = '70200039537'
			and format(m.DTE_OPE,'yyyyMM') >= @mois_ref_prec and format(m.DTE_OPE,'yyyyMM') <= @mois_ref 
			and COD_OPE = 'A0V' and COD_EVE = 'CLI'and NAT_OPE = 'OUI' and MTT_EUR > 0

	);



	insert into #TAB_MVT_CRED (DWHCPTCOM,DWHCPTPPAL,DWHCPTRUB ,DTE_OPE ,COD_OPE ,COD_EVE ,NAT_OPE,DON_ANA ,COD_ANA ,MTT_EUR,TYPE_MTT )
	-- virement SMS
	(
	select
	DWHCPTCOM,DWHCPTPPAL,DWHCPTRUB ,DTE_OPE ,COD_OPE ,COD_EVE ,NAT_OPE,DON_ANA ,COD_ANA ,MTT_EUR,'Virement SMS'
	from
		#TAB_CAV cpt 
		inner join [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_MOUVEMENT] m1	WITH(NOLOCK) on m1.NUM_CPT = cpt.DWHCPTCOM and format(m1.DTE_OPE,'yyyyMM') >= @mois_ref_prec and format(m1.DTE_OPE,'yyyyMM') <= @mois_ref  

	where 

	cpt.DWHCPTDAO <= @DATE_REF 
	and (cpt.DWHCPTDAC IS NULL or (format(cpt.DWHCPTDAC,'yyyyMM') >= @mois_ref_prec and format(cpt.DWHCPTDAC,'yyyyMM') <= @mois_ref)  or cpt.DWHCPTDAC > @DATE_REF_PREC)		-- comptes ouverts ou clos dans le mois
	and
	(
	--([COD_OPE] = ('CTB') and COD_EVE = 'IMM') OR
	COD_OPE in ('A0V', 'R0V','A1V', 'A2V','R1V','R2V') and (nat_ope in ('PUI','PUE')  )
	)
	)

	insert into #TAB_MVT_CRED (DWHCPTCOM,DWHCPTPPAL,DWHCPTRUB ,DTE_OPE ,COD_OPE ,COD_EVE ,NAT_OPE,DON_ANA ,COD_ANA ,MTT_EUR,TYPE_MTT )
	-- demande argent
	(
	select
	DWHCPTCOM,DWHCPTPPAL,DWHCPTRUB ,DTE_OPE ,COD_OPE ,COD_EVE ,NAT_OPE,DON_ANA ,COD_ANA ,MTT_EUR,'Demande argent'
	from
		#TAB_CAV cpt 
		inner join [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_MOUVEMENT] m1	WITH(NOLOCK) on m1.NUM_CPT = cpt.DWHCPTCOM and format(m1.DTE_OPE,'yyyyMM') >= @mois_ref_prec and format(m1.DTE_OPE,'yyyyMM') <= @mois_ref 

	where 
	cpt.DWHCPTDAO <= @DATE_REF 
	and (cpt.DWHCPTDAC IS NULL or (format(cpt.DWHCPTDAC,'yyyyMM') >= @mois_ref_prec and format(cpt.DWHCPTDAC,'yyyyMM') <= @mois_ref)  or cpt.DWHCPTDAC > @DATE_REF_PREC)		-- comptes ouverts ou clos dans le mois
	and
	(
	--([COD_OPE] = ('CTB') and COD_EVE = 'IMM') OR
	COD_OPE in ('A0V', 'R0V','A1V', 'A2V','R1V','R2V') and (nat_ope = ('RUI')  )
	)
	)

	-- Alimentation de la table des mouvements virements débiteurs/créditeurs
	insert into #tab_vir (DWHCPTCOM,DWHCPTPPAL,DWHCPTRUB ,DTE_OPE ,COD_OPE ,COD_EVE ,NAT_OPE,DON_ANA ,COD_ANA ,MTT_EUR,TYPE_MTT )
	-- virement autre que SMS
	(
	select
	DWHCPTCOM,DWHCPTPPAL,DWHCPTRUB ,DTE_OPE ,COD_OPE ,COD_EVE ,NAT_OPE,DON_ANA ,COD_ANA ,MTT_EUR,'Virement classique'
	from
		#TAB_CAV cpt 
		inner join [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_MOUVEMENT] m1	WITH(NOLOCK) on m1.NUM_CPT = cpt.DWHCPTCOM and format(m1.DTE_OPE,'yyyyMM') >= @mois_ref_prec and format(m1.DTE_OPE,'yyyyMM') <= @mois_ref 

	where 
	cpt.DWHCPTDAO <= @DATE_REF 
	and (cpt.DWHCPTDAC IS NULL or (format(cpt.DWHCPTDAC,'yyyyMM') >= @mois_ref_prec and format(cpt.DWHCPTDAC,'yyyyMM') <= @mois_ref)  or cpt.DWHCPTDAC > @DATE_REF_PREC)		-- comptes ouverts ou clos dans le mois
	and
	(
	--([COD_OPE] = ('CTB') and COD_EVE = 'IMM') OR
	COD_OPE in ('A0V', 'R0V','A1V', 'A2V','R1V','R2V') and (nat_ope not in ('PUI','PUE') or nat_ope IS NULL) )
	)


	insert into #tab_vir (DWHCPTCOM,DWHCPTPPAL,DWHCPTRUB ,DTE_OPE ,COD_OPE ,COD_EVE ,NAT_OPE,DON_ANA ,COD_ANA ,MTT_EUR,TYPE_MTT )
	-- virement SMS
	(
	select
	DWHCPTCOM,DWHCPTPPAL,DWHCPTRUB ,DTE_OPE ,COD_OPE ,COD_EVE ,NAT_OPE,DON_ANA ,COD_ANA ,MTT_EUR,'Virement SMS'
	from
		#TAB_CAV cpt 
		inner join [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_MOUVEMENT] m1	WITH(NOLOCK) on m1.NUM_CPT = cpt.DWHCPTCOM and format(m1.DTE_OPE,'yyyyMM') >= @mois_ref_prec and format(m1.DTE_OPE,'yyyyMM') <= @mois_ref  

	where 
	cpt.DWHCPTDAO <= @DATE_REF 
	and (cpt.DWHCPTDAC IS NULL or (format(cpt.DWHCPTDAC,'yyyyMM') >= @mois_ref_prec and format(cpt.DWHCPTDAC,'yyyyMM') <= @mois_ref)  or cpt.DWHCPTDAC > @DATE_REF_PREC)		-- comptes ouverts ou clos dans le mois
	and
	(
	COD_OPE in ('A0V', 'R0V','A1V', 'A2V','R1V','R2V') and (nat_ope in ('PUI','PUE')) )
	)


	/* ------------------------------- Operations debitrices -------------------------------- */
	--========================== MOYENNE & TOTAL MOUVEMENT DEBITEUR ===================================	
	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
	)
	select	
	date_operation,
	count(distinct IDClientSAB) as 'Nb de clients ayant réalisé une opé débitrice',
	sum(nb_op_cli) as 'Nb opé débitrices',
	sum((mtt_tt_cli)/1000) as 'Montant  opé débitrices en k€',
	avg(cast(nb_op_cli as decimal(6,2))) as 'Nb moyen opé débitrices',
	avg(mtt_tt_cli) as 'Montant moyen opé débitrices'
	from
	(select 
	format(DTE_OPE, 'yyyyMM') as date_operation
	,cpt.DWHCPTPPAL as IDClientSAB
	,sum(MTT_EUR)
	as mtt_tt_cli,
	sum(
		case 
			when COD_OPE in ('A0P', 'R0P') and MTT_EUR >0 	then 1																																	-- prelevement
			when COD_OPE in ('A0P', 'R0P','A1P', 'A2P', 'R1P', 'R2P') and MTT_EUR <0 	then -1																																	-- rejet prelevement
			when COD_OPE in ('A0V', 'R0V') and MTT_EUR >0  	then 1																																	-- virement debiteur
			when COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR <0 	then -1																																	-- rejet virement debiteur
			when COD_OPE in ('RI0') and MTT_EUR > 0 then 1																																							-- cheque emis
			when COD_OPE in ('AI1','RI0') and MTT_EUR <0 then -1																																							-- rejet cheque emis
			when COD_OPE = 'CTB' and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(COD_ANA,5,2) in ('37','50') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR >0 then 1				-- carte physique debiteur
			when COD_OPE = 'CTB' and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR <0 then -1					-- rejet carte physique débiteur
			-- when COD_OPE = 'CTB' and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(COD_ANA,5,2) in ('45','52') and substring(mouv.DON_ANA,64,2) = ('FC') and MTT_EUR <0 then -1			-- remboursement carte physique 
			when COD_OPE = 'CTB' and substring(DON_ANA,60,3) ='WMI'  and substring(COD_ANA,5,2) in ('37','50') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR >0 then 1						-- paiement mobile
			when COD_OPE = 'CTB' and substring(DON_ANA,60,3) ='WMI' and substring(COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR <0 then -1					-- rejet paiement mobile
			-- when COD_OPE = 'CTB' and substring(DON_ANA,60,3) ='WMI' and substring(COD_ANA,5,2) in ('45','52') and substring(mouv.DON_ANA,64,2) = ('FC') and MTT_EUR <0 then -1			-- remboursement carte mobile 
			when COD_OPE = 'CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) in ('47','90') and substring(mouv.DON_ANA,64,2) = ('RD') and MTT_EUR >0	then 1	-- retrait
			when COD_OPE = 'CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) = ('RD') and MTT_EUR <0 then -1 		-- rejet retrait
			when COD_OPE = 'CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) in ('47','56') and substring(mouv.DON_ANA,64,2) in ('CB','QC','RG') and MTT_EUR >0 then 1	-- autres paiements cartes
			when COD_OPE = 'CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) in ('CB','QC','RG') and MTT_EUR <0 then -1	-- annulation autres paiements cartes
	end) as nb_op_cli
	from 
		#TAB_MVT mouv
		inner join 
				#TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM
	where 
	((COD_OPE in ('A0P', 'R0P') and MTT_EUR >0)															-- prelevement
	OR (COD_OPE in ('A0P', 'R0P','A1P', 'A2P', 'R1P', 'R2P') and MTT_EUR <0) 														-- rejet prelevement
	OR (COD_OPE in ('A0V', 'R0V') and MTT_EUR >0 ) 														-- virement debiteur
	OR (COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR <0 )														-- rejet virement debiteur
	OR (COD_OPE in ('RI0') and MTT_EUR > 0 ) 																			-- cheque emis
	OR (COD_OPE in ('AI1','RI0') and MTT_EUR <0)  																			-- rejet cheque emis
	OR (COD_OPE = 'CTB' and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(COD_ANA,5,2) in ('37','50') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR >0) 			-- carte physique debiteur
	OR (COD_OPE = 'CTB' and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) in ('FA','CB','QC','RG') and MTT_EUR <0) 			-- rejet carte physique débiteur et autres paiements cartes
	OR (COD_OPE = 'CTB' and substring(DON_ANA,60,3) ='WMI'  and substring(COD_ANA,5,2) in ('37','50') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR >0) 					-- paiement mobile
	OR (COD_OPE = 'CTB' and substring(DON_ANA,60,3) ='WMI' and substring(COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR <0) 					-- rejet paiement mobile 
	OR (COD_OPE = 'CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) in ('47','90') and substring(mouv.DON_ANA,64,2) = ('RD') and MTT_EUR >0)	-- retrait
	OR (COD_OPE = 'CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) = ('RD') and MTT_EUR <0)  		-- rejet retrait
	OR (COD_OPE = 'CTB' and substring(DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD','WMI') and substring(COD_ANA,5,2) in ('45','52') and substring(mouv.DON_ANA,64,2) = ('FC') and MTT_EUR <0 )		-- remboursement cartes
	OR (COD_OPE = 'CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) in ('47','56') and substring(mouv.DON_ANA,64,2) in ('CB','QC','RG') and MTT_EUR >0)	-- autres paiements cartes
	)
	group by format(DTE_OPE, 'yyyyMM') ,cpt.DWHCPTPPAL) ope_groupe
	where nb_op_cli <> 0	
	group by date_operation;

	INSERT INTO #VDC_TYPE_OPERATION (
		[Date_operation] 
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope])
	select distinct
	ope_groupe.Mois_ope,	
	PERCENTILE_CONT(0.5) within group (order by ope_groupe.nb_op) over (partition by Mois_ope)  'Nb médian opé débitrices',
	PERCENTILE_CONT(0.5) within group (order by ope_groupe.mtt_tt) over (partition by Mois_ope)  'Montant médian opé débitrices'	
	from
	(
	select
	format(DTE_OPE, 'yyyyMM')  Mois_ope,
	cpt.DWHCPTPPAL as IDClientSAB,
	sum(MTT_EUR) as mtt_tt,
	sum(case
			when COD_OPE in ('A0P', 'R0P') and MTT_EUR >0 	then 1													-- prelevement
			when COD_OPE in ('A0P', 'R0P','A1P', 'A2P', 'R1P', 'R2P') and MTT_EUR <0 	then -1												-- rejet prelevement
			when COD_OPE in ('A0V', 'R0V') and MTT_EUR >0  	then 1													-- virement debiteur
			when COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR <0 	then -1												-- rejet virement debiteur
			when COD_OPE in ('RI0') and MTT_EUR > 0 then 1																			-- cheque emis
			when COD_OPE in ('AI1','RI0') and MTT_EUR < 0 then -1																		-- rejet cheque emis
			when COD_OPE = 'CTB' and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(COD_ANA,5,2) in ('37','50') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR >0 then 1				-- carte physique debiteur
			when COD_OPE = 'CTB' and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR <0 then -1					-- rejet carte physique débiteur
			-- when COD_OPE = 'CTB' and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(COD_ANA,5,2) in ('45','52') and substring(mouv.DON_ANA,64,2) = ('FC') and MTT_EUR <0 then -1			-- remboursement carte physique 
			when COD_OPE = 'CTB' and substring(DON_ANA,60,3) ='WMI'  and substring(COD_ANA,5,2) in ('37','50') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR >0 then 1						-- paiement mobile
			when COD_OPE = 'CTB' and substring(DON_ANA,60,3) ='WMI' and substring(COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR <0 then -1					-- rejet paiement mobile
			-- when COD_OPE = 'CTB' and substring(DON_ANA,60,3) ='WMI' and substring(COD_ANA,5,2) in ('45','52') and substring(mouv.DON_ANA,64,2) = ('FC') and MTT_EUR <0 then -1			-- remboursement carte mobile 
			when COD_OPE = 'CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) in ('47','90') and substring(mouv.DON_ANA,64,2) = ('RD') and MTT_EUR >0	then 1	-- retrait
			when COD_OPE = 'CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) = ('RD') and MTT_EUR <0 then -1 		-- rejet retrait
			when COD_OPE = 'CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) in ('47','56') and substring(mouv.DON_ANA,64,2) in ('CB','QC','RG') and MTT_EUR >0 then 1	-- autres paiements cartes
			when COD_OPE = 'CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) in ('CB','QC','RG') and MTT_EUR <0 then -1	-- annulation autres paiements cartes
	end) as nb_op
	from #TAB_MVT mouv
	inner join #TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM
	where ((COD_OPE in ('A0P', 'R0P') and MTT_EUR >0)															-- prelevement
	OR (COD_OPE in ('A0P', 'R0P','A1P', 'A2P', 'R1P', 'R2P') and MTT_EUR <0) 														-- rejet prelevement
	OR (COD_OPE in ('A0V', 'R0V') and MTT_EUR >0 ) 														-- virement debiteur
	OR (COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR <0 )														-- rejet virement debiteur
	OR (COD_OPE in ('RI0') and MTT_EUR > 0) 																			-- cheque emis
	OR (COD_OPE in ('AI1','RI0') and MTT_EUR < 0)  																			-- rejet cheque emis
	OR (COD_OPE = 'CTB' and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD','WMI') and substring(COD_ANA,5,2) in ('37','50') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR > 0) 			-- carte debiteur
	OR (COD_OPE = 'CTB' and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD','WMI') and substring(COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) in ('FA','CB','QC','RG','RD') and MTT_EUR <0) 	-- rejet carte autres annulations
	OR (COD_OPE = 'CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) in ('47','90') and substring(mouv.DON_ANA,64,2) = ('RD') and MTT_EUR > 0)	-- retrait
	OR (COD_OPE = 'CTB' and substring(DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD','WMI') and substring(COD_ANA,5,2) in ('45','52') and substring(mouv.DON_ANA,64,2) = ('FC') and MTT_EUR <0 )		-- remboursement cartes
	OR (COD_OPE = 'CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) in ('47','56') and substring(mouv.DON_ANA,64,2) in ('CB','QC','RG') and MTT_EUR >0)	-- autres paiements cartes
	)
	group by format(DTE_OPE, 'yyyyMM') , cpt.DWHCPTPPAL) ope_groupe					
	where nb_op <> 0;


	INSERT INTO [dbo].[VDC_TYPE_OPERATION] (
		 [Date_operation] 
		,[Type_Op1] 
		,[Type_Op2] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope] )
	select 	[date_operation],'NA' AS [Type_Op1], 'TOP_DEB' AS [Type_Op2]
		,SUM([Nb_Ope1]				 ) AS [Nb_Ope1]
		,SUM([Nb_Ope2] 			     ) AS [Nb_Ope2] 
		,SUM([Montant_en_k€] 		 ) AS [Montant_en_k€] 
		,SUM([Nb_moyen_Ope] 		 ) AS [Nb_moyen_Ope] 
		,SUM([Montant_moyen_Ope] 	 ) AS [Montant_moyen_Ope] 
		,SUM([Nb_médian_Ope] 		 ) AS [Nb_médian_Ope] 
		,SUM([Montant_médian_Ope] 	 ) AS [Montant_médian_Ope] 
	from #VDC_TYPE_OPERATION
	GROUP BY [date_operation];
	/* ------------------------------------------------------------------------------------------------------------------------------ */	

	--=============================						AU GLOBAL				  ====================================

	--============================= MOYENNE & TOTAL PAIEMENT CARTE PHYSIQUE DEBITEUR  ================================
	TRUNCATE TABLE #VDC_TYPE_OPERATION;
	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
	)
	select 
		date_operation,
		count(distinct IDClientSAB) as 'Nb de clients ayant fait un paiement carte physique',
		sum(nb_op_cli) as 'Nb paiements carte physique débit',
		sum((mtt_tt_cli)/1000) as 'Montant paiements carte physique débit en k€',
		avg(cast(nb_op_cli as decimal(6,2))) as 'Nb moyen de paiements carte physique débit',
		avg(mtt_tt_cli) as 'Montant moyen de paiements carte physique débit'
	from
		(select 
		format(DTE_OPE, 'yyyyMM') as date_operation
		,cpt.DWHCPTPPAL as IDClientSAB
		,sum(MTT_EUR) as mtt_tt_cli
		,sum(
		case 
			when COD_OPE = 'CTB' and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(COD_ANA,5,2) in ('37','50') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR >0 then 1				-- carte physique debiteur
			when COD_OPE = 'CTB' and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR <0 then -1					-- rejet carte physique débiteur		
	end) as nb_op_cli

		from 
			#TAB_MVT mouv
			inner join 
				#TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM
		where  
		(
		(COD_OPE = 'CTB' and substring(COD_ANA,5,2) in ('37','50')  and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR > 0 )			-- operations carte physique debiteur
		OR (COD_OPE = 'CTB' and substring(COD_ANA,5,2) in ('17')  and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR < 0 )			-- operations annulations carte physique debiteur
		OR (COD_OPE = 'CTB' and substring(DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(COD_ANA,5,2) in ('45','52') and substring(mouv.DON_ANA,64,2) = ('FC') and MTT_EUR <0 )		-- remboursement cartes
		)
		group by format(DTE_OPE, 'yyyyMM') ,cpt.DWHCPTPPAL) ope_groupe
	where nb_op_cli <> 0	
	group by date_operation;


	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope]
	)
	select distinct
	ope_groupe.Mois_ope,	
	PERCENTILE_CONT(0.5) within group (order by ope_groupe.nb_op) over (partition by Mois_ope)  'Nb médian de paiements carte physique débit',
	PERCENTILE_CONT(0.5) within group (order by ope_groupe.mtt_tt) over (partition by Mois_ope)  'Montant médian de paiements carte physique débit'
	from
	(
	select
	format(DTE_OPE, 'yyyyMM')  Mois_ope,
	cpt.DWHCPTPPAL,
	sum(MTT_EUR) as mtt_tt,
	sum(	case	when COD_OPE = 'CTB' and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(COD_ANA,5,2) in ('37','50') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR >0 then 1				-- carte physique debiteur
			when COD_OPE = 'CTB' and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR <0 then -1	end) as 	nb_op			-- rejet carte physique débiteur) as nb_op
	from #TAB_MVT mouv
		inner join #TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM

	where   (
	(COD_OPE = 'CTB' and substring(COD_ANA,5,2) in ('37','50')  and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR >0)				-- operations carte physique debiteur
	OR (COD_OPE = 'CTB' and substring(COD_ANA,5,2) = ('17')  and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR < 0)				-- annulations carte physique debiteur
	OR (COD_OPE = 'CTB' and substring(DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(COD_ANA,5,2) in ('45','52') and substring(mouv.DON_ANA,64,2) = ('FC') and MTT_EUR <0 )	-- remboursement cartes
	)	
	group by format(DTE_OPE, 'yyyyMM') , cpt.DWHCPTPPAL) ope_groupe		
	where nb_op <> 0;


	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Nb_Ope_France]
		,[Nb_Ope_ZoneEuro]
		,[Nb_Ope_HorsZoneEuro]
		)
	select 
	date_operation,
	sum([Nb_Ope_France]			) AS [Nb_Ope_France],
	sum([Nb_Ope_ZoneEuro]		) AS [Nb_Ope_ZoneEuro],
	sum([Nb_Ope_HorsZoneEuro]	) AS [Nb_Ope_HorsZoneEuro]
	from (
	select		
		date_operation,
		case when ope_groupe.[Zone paiement]='France' then sum(nb_op_cli) end as [Nb_Ope_France],
		case when ope_groupe.[Zone paiement]='Zone euro' then sum(nb_op_cli) end as [Nb_Ope_ZoneEuro],
		case when ope_groupe.[Zone paiement]='Hors zone euro' then sum(nb_op_cli) end as [Nb_Ope_HorsZoneEuro]
	from
	(select 
		format(DTE_OPE, 'yyyyMM') as date_operation
		,cpt.DWHCPTPPAL as IDClientSAB
		,case
			when rtrim(ltrim(substring(DON_ANA,74,2))) in ('FR','RE','GP','MQ','GF','YT') then 'France'
			when rtrim(ltrim(substring(DON_ANA,74,2))) in ('DE','AT','BE','ES','FI','IE','IT','LU','NL','PT','GR','SI','CY','MT','SK','EE','LV','LT') then 'Zone euro'
			else 'Hors zone euro'
		end as 'Zone paiement'
		,sum(MTT_EUR) as mtt_tt_cli
		,sum(
		case 
			when COD_OPE = 'CTB' and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(COD_ANA,5,2) in ('37','50') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR >0 then 1				-- carte physique debiteur
			when COD_OPE = 'CTB' and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR <0 then -1					-- rejet carte physique débiteur
		
	end) as nb_op_cli

		from 
			#TAB_MVT mouv
			inner join 
				#TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM

		where 
		 ((COD_OPE = 'CTB' and substring(COD_ANA,5,2) in ('37','50')  and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR > 0 )			-- operations carte physique debiteur
		OR (COD_OPE = 'CTB' and substring(COD_ANA,5,2) in ('17')  and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR < 0 )			-- operations annulations carte physique debiteur
		OR (COD_OPE = 'CTB' and substring(DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(COD_ANA,5,2) in ('45','52') and substring(mouv.DON_ANA,64,2) = ('FC') and MTT_EUR <0 )		-- remboursement cartes
		)
		group by format(DTE_OPE, 'yyyyMM') ,cpt.DWHCPTPPAL,	case
			when rtrim(ltrim(substring(DON_ANA,74,2))) in ('FR','RE','GP','MQ','GF','YT') then 'France'
			when rtrim(ltrim(substring(DON_ANA,74,2))) in ('DE','AT','BE','ES','FI','IE','IT','LU','NL','PT','GR','SI','CY','MT','SK','EE','LV','LT') then 'Zone euro'
			else 'Hors zone euro'
		end) ope_groupe
		where nb_op_cli <> 0
	group by date_operation,ope_groupe.[Zone paiement]) as T
	group by date_operation;

 

	INSERT INTO [dbo].[VDC_TYPE_OPERATION] (
		 [Date_operation] 
		,[Type_Op1] 
		,[Type_Op2] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Nb_Ope_France]
		,[Nb_Ope_ZoneEuro]
		,[Nb_Ope_HorsZoneEuro]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope] 
	)
	select 	[date_operation],'NA' AS [Type_Op1], 'TOT_CRT' AS [Type_Op2]
		,SUM([Nb_Ope1] 				 ) AS [Nb_Ope1] 
		,SUM([Nb_Ope2] 				 ) AS [Nb_Ope2] 
		,SUM([Nb_Ope_France] 	     ) AS [Nb_Ope_France] 
		,SUM([Nb_Ope_ZoneEuro] 	     ) AS [Nb_Ope_ZoneEuro] 
		,SUM([Nb_Ope_HorsZoneEuro] 	 ) AS [Nb_Ope_HorsZoneEuro] 
		,SUM([Montant_en_k€] 		 ) AS [Montant_en_k€] 
		,SUM([Nb_moyen_Ope] 		 ) AS [Nb_moyen_Ope] 
		,SUM([Montant_moyen_Ope] 	 ) AS [Montant_moyen_Ope] 
		,SUM([Nb_médian_Ope] 		 ) AS [Nb_médian_Ope] 
		,SUM([Montant_médian_Ope] 	 ) AS [Montant_médian_Ope] 
	from #VDC_TYPE_OPERATION
	GROUP BY [date_operation];


	--============================= MOYENNE & TOTAL PAIEMENT CARTE PHYSIQUE DEBITEUR  ================================
	TRUNCATE TABLE #VDC_TYPE_OPERATION;
	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Type_Op1] 
		,[Type_Op2] 
		,[Nb_Ope]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
	)	
	select 
		date_operation,
		ope_groupe.[Visa / Visa Premier],
		ope_groupe.[Immédiat / Différé],
		sum(nb_op_cli) as 'Nb paiements carte physique débit',
		sum((mtt_tt_cli)/1000) as 'Montant paiements carte physique débit en k€',
		avg(cast(nb_op_cli as decimal(6,2))) as 'Nb moyen de paiements carte physique débit',
		avg(mtt_tt_cli) as 'Montant moyen de paiements carte physique débit'
	from
		(select 
		format(DTE_OPE, 'yyyyMM') as date_operation
		,cpt.DWHCPTPPAL as IDClientSAB
		,case
			when substring(DON_ANA,60,3) in ('MGI','MGD') then 'PAI_CPTC'
			when substring(DON_ANA,60,3) in ('MPI','MPD') then 'PAI_CPTP'
		end as 'Visa / Visa Premier'
		,case
			when substring(DON_ANA,60,3) in ('MGI','MPI') then 'Deb_Imm'
			when substring(DON_ANA,60,3) in ('MGD','MPD') then 'Deb_Diff'
		end	as 'Immédiat / Différé'
		,sum(MTT_EUR) as mtt_tt_cli
		,sum(
		case 
			when COD_OPE = 'CTB' and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(COD_ANA,5,2) in ('37','50') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR >0 then 1				-- carte physique debiteur
			when COD_OPE = 'CTB' and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR <0 then -1					-- rejet carte physique débiteur		
	end) as nb_op_cli
		from 
			#TAB_MVT mouv
			inner join 
				#TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM
		where  
		 (
		(COD_OPE = 'CTB' and substring(COD_ANA,5,2) in ('37','50')  and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR > 0 )			-- operations carte physique debiteur
		OR (COD_OPE = 'CTB' and substring(COD_ANA,5,2) in ('17')  and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR < 0 )			-- operations annulations carte physique debiteur
		OR (COD_OPE = 'CTB' and substring(DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(COD_ANA,5,2) in ('45','52') and substring(mouv.DON_ANA,64,2) = ('FC') and MTT_EUR <0 )		-- remboursement cartes
		)
		group by format(DTE_OPE, 'yyyyMM') ,cpt.DWHCPTPPAL,
		case
			when substring(DON_ANA,60,3) in ('MGI','MGD') then 'PAI_CPTC'
			when substring(DON_ANA,60,3) in ('MPI','MPD') then 'PAI_CPTP'
		end,
		case
			when substring(DON_ANA,60,3) in ('MGI','MPI') then 'Deb_Imm'
			when substring(DON_ANA,60,3) in ('MGD','MPD') then 'Deb_Diff'
		end) ope_groupe
		where nb_op_cli <> 0
	group by date_operation, ope_groupe.[Visa / Visa Premier], ope_groupe.[Immédiat / Différé];

	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Type_Op1] 
		,[Type_Op2] 
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope]
	)
	select distinct
	ope_groupe.Mois_ope,	
	ope_groupe.[Visa / Visa Premier],
	ope_groupe.[Immédiat / Différé],
	PERCENTILE_CONT(0.5) within group (order by ope_groupe.nb_op) over (partition by Mois_ope, ope_groupe.[Visa / Visa Premier],ope_groupe.[Immédiat / Différé])  'Nb médian de paiements carte physique débit',
	PERCENTILE_CONT(0.5) within group (order by ope_groupe.mtt_tt) over (partition by Mois_ope, ope_groupe.[Visa / Visa Premier],ope_groupe.[Immédiat / Différé])  'Montant médian de paiements carte physique débit'
	from
	(
	select
	format(DTE_OPE, 'yyyyMM')  Mois_ope,
	cpt.DWHCPTPPAL,
	case
			when substring(DON_ANA,60,3) in ('MGI','MGD') then 'PAI_CPTC'
			when substring(DON_ANA,60,3) in ('MPI','MPD') then 'PAI_CPTP'
		end as 'Visa / Visa Premier'
		,case
			when substring(DON_ANA,60,3) in ('MGI','MPI') then 'Deb_Imm'
			when substring(DON_ANA,60,3) in ('MGD','MPD') then 'Deb_Diff'
		end	as 'Immédiat / Différé',
	sum(MTT_EUR) as mtt_tt,
	sum(	case	when COD_OPE = 'CTB' and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(COD_ANA,5,2) in ('37','50') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR >0 then 1				-- carte physique debiteur
			when COD_OPE = 'CTB' and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR <0 then -1	end) as 	nb_op			-- rejet carte physique débiteur) as nb_op
	from #TAB_MVT mouv
		inner join #TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM
	where   (
	(COD_OPE = 'CTB' and substring(COD_ANA,5,2) in ('37','50')  and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR >0)				-- operations carte physique debiteur
	OR (COD_OPE = 'CTB' and substring(COD_ANA,5,2) = ('17')  and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR < 0)				-- annulations carte physique debiteur
	OR (COD_OPE = 'CTB' and substring(DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(COD_ANA,5,2) in ('45','52') and substring(mouv.DON_ANA,64,2) = ('FC') and MTT_EUR <0 )	-- remboursement cartes
	)	
	group by format(DTE_OPE, 'yyyyMM') , cpt.DWHCPTPPAL,
	case
			when substring(DON_ANA,60,3) in ('MGI','MGD') then 'PAI_CPTC'
			when substring(DON_ANA,60,3) in ('MPI','MPD') then 'PAI_CPTP'
		end ,
		case
			when substring(DON_ANA,60,3) in ('MGI','MPI') then 'Deb_Imm'
			when substring(DON_ANA,60,3) in ('MGD','MPD') then 'Deb_Diff'
		end) ope_groupe		
	where nb_op <> 0;


	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Type_Op1] 
		,[Type_Op2] 
		,[Nb_Ope_France]
		,[Nb_Ope_ZoneEuro]
		,[Nb_Ope_HorsZoneEuro]
		)
	select 
	date_operation,
	[Visa / Visa Premier],
	[Immédiat / Différé],
	sum([Nb_Ope_France]			) AS [Nb_Ope_France],
	sum([Nb_Ope_ZoneEuro]		) AS [Nb_Ope_ZoneEuro],
	sum([Nb_Ope_HorsZoneEuro]	) AS [Nb_Ope_HorsZoneEuro]
	from (
	select		
		date_operation,
		ope_groupe.[Visa / Visa Premier],
		ope_groupe.[Immédiat / Différé],
		case when ope_groupe.[Zone paiement]='France' then sum(nb_op_cli) end as [Nb_Ope_France],
		case when ope_groupe.[Zone paiement]='Zone euro' then sum(nb_op_cli) end as [Nb_Ope_ZoneEuro],
		case when ope_groupe.[Zone paiement]='Hors zone euro' then sum(nb_op_cli) end as [Nb_Ope_HorsZoneEuro]
	from
		(select 
		format(DTE_OPE, 'yyyyMM') as date_operation
		,cpt.DWHCPTPPAL as IDClientSAB
			,case
			when substring(DON_ANA,60,3) in ('MGI','MGD') then 'PAI_CPTC'
			when substring(DON_ANA,60,3) in ('MPI','MPD') then 'PAI_CPTP'
		end as 'Visa / Visa Premier'
		,case
			when substring(DON_ANA,60,3) in ('MGI','MPI') then 'Deb_Imm'
			when substring(DON_ANA,60,3) in ('MGD','MPD') then 'Deb_Diff'
		end	as 'Immédiat / Différé'
		,case
			when rtrim(ltrim(substring(DON_ANA,74,2))) in ('FR','RE','GP','MQ','GF','YT') then 'France'
			when rtrim(ltrim(substring(DON_ANA,74,2))) in ('DE','AT','BE','ES','FI','IE','IT','LU','NL','PT','GR','SI','CY','MT','SK','EE','LV','LT') then 'Zone euro'
			else 'Hors zone euro'
		end as 'Zone paiement'
		,sum(MTT_EUR) as mtt_tt_cli
		,sum(
		case 
			when COD_OPE = 'CTB' and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(COD_ANA,5,2) in ('37','50') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR >0 then 1				-- carte physique debiteur
			when COD_OPE = 'CTB' and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR <0 then -1					-- rejet carte physique débiteur	
	end) as nb_op_cli
		from 
			#TAB_MVT mouv
			inner join 
				#TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM
		where  
		 ((COD_OPE = 'CTB' and substring(COD_ANA,5,2) in ('37','50')  and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR > 0 )			-- operations carte physique debiteur
		OR (COD_OPE = 'CTB' and substring(COD_ANA,5,2) in ('17')  and substring(DON_ANA,60,3) in ('MGI','MGD','MPI','MPD') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR < 0 )			-- operations annulations carte physique debiteur
		OR (COD_OPE = 'CTB' and substring(DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(COD_ANA,5,2) in ('45','52') and substring(mouv.DON_ANA,64,2) = ('FC') and MTT_EUR <0 )		-- remboursement cartes
		)
		group by format(DTE_OPE, 'yyyyMM') ,cpt.DWHCPTPPAL,	case
			when rtrim(ltrim(substring(DON_ANA,74,2))) in ('FR','RE','GP','MQ','GF','YT') then 'France'
			when rtrim(ltrim(substring(DON_ANA,74,2))) in ('DE','AT','BE','ES','FI','IE','IT','LU','NL','PT','GR','SI','CY','MT','SK','EE','LV','LT') then 'Zone euro'
			else 'Hors zone euro'
				end,case
			when substring(DON_ANA,60,3) in ('MGI','MGD') then 'PAI_CPTC'
			when substring(DON_ANA,60,3) in ('MPI','MPD') then 'PAI_CPTP'
		end 
		,case
			when substring(DON_ANA,60,3) in ('MGI','MPI') then 'Deb_Imm'
			when substring(DON_ANA,60,3) in ('MGD','MPD') then 'Deb_Diff'
		end	
		) ope_groupe
		where nb_op_cli <> 0
	group by date_operation,ope_groupe.[Zone paiement],	ope_groupe.[Visa / Visa Premier],ope_groupe.[Immédiat / Différé]
	) AS T group by date_operation,[Visa / Visa Premier],[Immédiat / Différé];


	INSERT INTO [dbo].[VDC_TYPE_OPERATION] (
		 [Date_operation] 
		,[Type_Op1] 
		,[Type_Op2] 
		,[Nb_Ope]
		,[Nb_Ope_France]
		,[Nb_Ope_ZoneEuro]
		,[Nb_Ope_HorsZoneEuro]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope] 
	)
	select 	 [date_operation]
			,[Type_Op1]
			,[Type_Op2]
			,SUM([Nb_Ope]				 ) AS [Nb_Ope]
			,SUM([Nb_Ope_France] 	     ) AS [Nb_Ope_France] 
			,SUM([Nb_Ope_ZoneEuro] 	     ) AS [Nb_Ope_ZoneEuro] 
			,SUM([Nb_Ope_HorsZoneEuro] 	 ) AS [Nb_Ope_HorsZoneEuro] 
			,SUM([Montant_en_k€] 		 ) AS [Montant_en_k€] 
			,SUM([Nb_moyen_Ope] 		 ) AS [Nb_moyen_Ope] 
			,SUM([Montant_moyen_Ope] 	 ) AS [Montant_moyen_Ope] 
			,SUM([Nb_médian_Ope] 		 ) AS [Nb_médian_Ope] 
			,SUM([Montant_médian_Ope] 	 ) AS [Montant_médian_Ope] 
	from #VDC_TYPE_OPERATION
	GROUP BY [date_operation],[Type_Op1],[Type_Op2];


	--==========================		AU GLOBAL	  ======================================

	--========================== MOYENNE & TOTAL RETRAIT ===================================
	TRUNCATE TABLE #VDC_TYPE_OPERATION;
	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
	)
	select		
		date_operation,
		count(distinct IDClientSAB) as 'Nb de clients ayant effectué un retrait',
		sum(nb_op_cli) as 'Nb retraits',
		sum((mtt_tt_cli)/1000) as 'Montant retraits en k€',
		avg(cast(nb_op_cli as decimal(6,2))) as 'Nb moyen de retraits',
		avg(mtt_tt_cli) as 'Montant moyen de retraits'
	from

		(select 
		format(DTE_OPE, 'yyyyMM') as date_operation
		,cpt.DWHCPTPPAL as IDClientSAB
		,sum(MTT_EUR) as mtt_tt_cli
		,sum(
		case 
			when COD_OPE = 'CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) in ('47','90','56') and substring(mouv.DON_ANA,64,2) in ('RD','RG') and MTT_EUR >0	then 1	-- retrait
			when COD_OPE = 'CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) = ('RD') and MTT_EUR <0 then -1 		-- rejet retrait
	end) as nb_op_cli

		from 
			#TAB_MVT mouv
			inner join #TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM
		where (
			(COD_OPE ='CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) in ('47','90','56') and substring(mouv.DON_ANA,64,2) in ('RD','RG') and MTT_EUR > 0)	-- retrait
			OR (COD_OPE ='CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) in ('RD','RG') and MTT_EUR <0)			-- annulation retrait
			)
		group by format(DTE_OPE, 'yyyyMM') ,cpt.DWHCPTPPAL) ope_groupe
		where nb_op_cli <> 0
	group by date_operation;

	INSERT INTO #VDC_TYPE_OPERATION (
		[Date_operation] 
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope])
	select distinct
	ope_groupe.Mois_ope,		
	PERCENTILE_CONT(0.5) within group (order by ope_groupe.nb_op) over (partition by Mois_ope)  'Nb médian de retraits',
	PERCENTILE_CONT(0.5) within group (order by ope_groupe.mtt_tt) over (partition by Mois_ope)  'Montant médian de retraits'
	from
	(
	select
	format(DTE_OPE, 'yyyyMM')  Mois_ope,
	cpt.DWHCPTPPAL,
	sum(MTT_EUR) as mtt_tt,
	sum(case when COD_OPE = 'CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) in ('47','90','56') and substring(mouv.DON_ANA,64,2) in ('RD','RG') and MTT_EUR >0	then 1	-- retrait
			when COD_OPE = 'CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) = ('RD') and MTT_EUR <0 then -1 		-- rejet retrait
			end) as nb_op
	from #TAB_MVT mouv
		inner join #TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM
	where 
	(
	(COD_OPE ='CTB' and substring(mouv.COD_ANA,5,2) in ('47','90','56') and substring(mouv.DON_ANA,64,2) in ('RD','RG') and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and MTT_EUR > 0)	-- operations retrait
	OR
	(COD_OPE ='CTB' and substring(mouv.COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) in ('RD','RG') and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and MTT_EUR < 0)	-- annulations retrait
	)
	group by format(DTE_OPE, 'yyyyMM') , cpt.DWHCPTPPAL) ope_groupe				
	where nb_op <> 0;


	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Nb_Ope_France]
		,[Nb_Ope_ZoneEuro]
		,[Nb_Ope_HorsZoneEuro]
		)
	select 
	date_operation,
	sum([Nb_Ope_France]			) AS [Nb_Ope_France],
	sum([Nb_Ope_ZoneEuro]		) AS [Nb_Ope_ZoneEuro],
	sum([Nb_Ope_HorsZoneEuro]	) AS [Nb_Ope_HorsZoneEuro]
	from (
	select		
		date_operation,
		case when ope_groupe.[Zone paiement]='France' then sum(nb_op_cli) end as [Nb_Ope_France],
		case when ope_groupe.[Zone paiement]='Zone euro' then sum(nb_op_cli) end as [Nb_Ope_ZoneEuro],
		case when ope_groupe.[Zone paiement]='Hors zone euro' then sum(nb_op_cli) end as [Nb_Ope_HorsZoneEuro]
	from
		(select 
		format(DTE_OPE, 'yyyyMM') as date_operation
		,cpt.DWHCPTPPAL as IDClientSAB
		,case
			when rtrim(ltrim(substring(DON_ANA,74,2))) in ('FR','RE','GP','MQ','GF','YT') then 'France'
			when rtrim(ltrim(substring(DON_ANA,74,2))) in ('DE','AT','BE','ES','FI','IE','IT','LU','NL','PT','GR','SI','CY','MT','SK','EE','LV','LT') then 'Zone euro'
			else 'Hors zone euro'
		end as 'Zone paiement'
		,sum(MTT_EUR) as mtt_tt_cli
		,sum(
		case 
			when COD_OPE = 'CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) in ('47','90','56') and substring(mouv.DON_ANA,64,2) in ('RD','RG') and MTT_EUR >0	then 1	-- retrait
			when COD_OPE = 'CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) = ('RD') and MTT_EUR <0 then -1 		-- rejet retrait
	end) as nb_op_cli

		from 
			#TAB_MVT mouv
			inner join #TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM

		where (
			(COD_OPE ='CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) in ('47','90','56') and substring(mouv.DON_ANA,64,2) in ('RD','RG') and MTT_EUR > 0)	-- retrait
			OR (COD_OPE ='CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) in ('RD','RG') and MTT_EUR <0)			-- annulation retrait
			)
		group by format(DTE_OPE, 'yyyyMM') ,cpt.DWHCPTPPAL
		,case
			when rtrim(ltrim(substring(DON_ANA,74,2))) in ('FR','RE','GP','MQ','GF','YT') then 'France'
			when rtrim(ltrim(substring(DON_ANA,74,2))) in ('DE','AT','BE','ES','FI','IE','IT','LU','NL','PT','GR','SI','CY','MT','SK','EE','LV','LT') then 'Zone euro'
			else 'Hors zone euro' end) ope_groupe
			where nb_op_cli <> 0
	group by date_operation, ope_groupe.[Zone paiement]) as T
	group by date_operation;

	INSERT INTO [dbo].[VDC_TYPE_OPERATION] (
		 [Date_operation] 
		,[Type_Op1] 
		,[Type_Op2] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Nb_Ope_France]
		,[Nb_Ope_ZoneEuro]
		,[Nb_Ope_HorsZoneEuro]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope] 
	)
	select 	[date_operation],'NA' AS [Type_Op1], 'TRE_CART' AS [Type_Op2]
		,SUM([Nb_Ope1] 				 ) AS [Nb_Ope1] 
		,SUM([Nb_Ope2] 				 ) AS [Nb_Ope2] 
		,SUM([Nb_Ope_France] 	     ) AS [Nb_Ope_France] 
		,SUM([Nb_Ope_ZoneEuro] 	     ) AS [Nb_Ope_ZoneEuro] 
		,SUM([Nb_Ope_HorsZoneEuro] 	 ) AS [Nb_Ope_HorsZoneEuro] 
		,SUM([Montant_en_k€] 		 ) AS [Montant_en_k€] 
		,SUM([Nb_moyen_Ope] 		 ) AS [Nb_moyen_Ope] 
		,SUM([Montant_moyen_Ope] 	 ) AS [Montant_moyen_Ope] 
		,SUM([Nb_médian_Ope] 		 ) AS [Nb_médian_Ope] 
		,SUM([Montant_médian_Ope] 	 ) AS [Montant_médian_Ope] 
	from #VDC_TYPE_OPERATION
	GROUP BY [date_operation];

	--==========================		EN DETAIL	  ======================================
	--========================== MOYENNE & TOTAL RETRAIT ===================================	
	TRUNCATE TABLE #VDC_TYPE_OPERATION;
	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Type_Op1] 
		,[Type_Op2] 
		,[Nb_Ope]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
	)	
	select		
		date_operation,
		ope_groupe.[Visa / Visa Premier],
		ope_groupe.[Immédiat / Différé],
		sum(nb_op_cli) as 'Nb retraits',
		sum((mtt_tt_cli)/1000) as 'Montant retraits en k€',
		avg(cast(nb_op_cli as decimal(6,2))) as 'Nb moyen de retraits',
		avg(mtt_tt_cli)	as 'Montant moyen de retraits'
	from
		(select 
		format(DTE_OPE, 'yyyyMM') as date_operation
		,cpt.DWHCPTPPAL as IDClientSAB
		,case
			when substring(DON_ANA,60,3) in ('MGI','MGD') then 'RET_CRTC'
			when substring(DON_ANA,60,3) in ('MPI','MPD') then 'RET_CRTP'
		end as 'Visa / Visa Premier'
			,case
			when substring(DON_ANA,60,3) in ('MGI','MPI') then 'Deb_Imm'
			when substring(DON_ANA,60,3) in ('MGD','MPD') then 'Deb_Diff'
		end	as 'Immédiat / Différé'
		,sum(MTT_EUR) as mtt_tt_cli
		,sum(
		case 
			when COD_OPE = 'CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) in ('47','90','56') and substring(mouv.DON_ANA,64,2) in ('RD','RG') and MTT_EUR >0	then 1	-- retrait
			when COD_OPE = 'CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) = ('RD') and MTT_EUR <0 then -1 		-- rejet retrait
	end) as nb_op_cli
		from 
			#TAB_MVT mouv
			inner join #TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM
		where 	(
			(COD_OPE ='CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) in ('47','90','56') and substring(mouv.DON_ANA,64,2) in ('RD','RG') and MTT_EUR > 0)	-- retrait
			OR (COD_OPE ='CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) in ('RD','RG') and MTT_EUR <0)			-- annulation retrait
			)
		group by format(DTE_OPE, 'yyyyMM') ,cpt.DWHCPTPPAL,
			case
				when substring(DON_ANA,60,3) in ('MGI','MGD') then 'RET_CRTC'
				when substring(DON_ANA,60,3) in ('MPI','MPD') then 'RET_CRTP'
				end,
				case
			when substring(DON_ANA,60,3) in ('MGI','MPI') then 'Deb_Imm'
			when substring(DON_ANA,60,3) in ('MGD','MPD') then 'Deb_Diff'
		end
			) ope_groupe
			where nb_op_cli <> 0
	group by date_operation, ope_groupe.[Visa / Visa Premier], ope_groupe.[Immédiat / Différé];

	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Type_Op1] 
		,[Type_Op2] 
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope]
	)
	select distinct
	ope_groupe.Mois_ope,		
	ope_groupe.[Visa / Visa Premier],
	ope_groupe.[Immédiat / Différé],
	PERCENTILE_CONT(0.5) within group (order by ope_groupe.nb_op) over (partition by Mois_ope,ope_groupe.[Visa / Visa Premier])  'Nb médian de retraits',
	PERCENTILE_CONT(0.5) within group (order by ope_groupe.mtt_tt) over (partition by Mois_ope,ope_groupe.[Visa / Visa Premier])  'Montant médian de retraits'
	from
	(
	select
	format(DTE_OPE, 'yyyyMM')  Mois_ope,
	cpt.DWHCPTPPAL,
	case
			when substring(DON_ANA,60,3) in ('MGI','MGD') then 'RET_CRTC'
			when substring(DON_ANA,60,3) in ('MPI','MPD') then 'RET_CRTP'
		end as 'Visa / Visa Premier'
	
			,case
			when substring(DON_ANA,60,3) in ('MGI','MPI') then 'Deb_Imm'
			when substring(DON_ANA,60,3) in ('MGD','MPD') then 'Deb_Diff'
		end	as 'Immédiat / Différé'
	,sum(MTT_EUR) as mtt_tt,
	sum(case when COD_OPE = 'CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) in ('47','90','56') and substring(mouv.DON_ANA,64,2) in ('RD','RG') and MTT_EUR >0	then 1	-- retrait
			when COD_OPE = 'CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) = ('RD') and MTT_EUR <0 then -1 		-- rejet retrait
			end) as nb_op
	from #TAB_MVT mouv
		inner join #TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM
	where 
	(
	(COD_OPE ='CTB' and substring(mouv.COD_ANA,5,2) in ('47','90','56') and substring(mouv.DON_ANA,64,2) in ('RD','RG') and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and MTT_EUR > 0)	-- operations retrait
	OR
	(COD_OPE ='CTB' and substring(mouv.COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) in ('RD','RG') and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and MTT_EUR < 0)	-- annulations retrait
	)
	group by format(DTE_OPE, 'yyyyMM') , cpt.DWHCPTPPAL, 
	case
			when substring(DON_ANA,60,3) in ('MGI','MGD') then 'RET_CRTC'
			when substring(DON_ANA,60,3) in ('MPI','MPD') then 'RET_CRTP'
			end,
			case
			when substring(DON_ANA,60,3) in ('MGI','MPI') then 'Deb_Imm'
			when substring(DON_ANA,60,3) in ('MGD','MPD') then 'Deb_Diff'
		end ) ope_groupe		
	where nb_op <> 0;
	

	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Type_Op1] 
		,[Type_Op2] 
		,[Nb_Ope_France]
		,[Nb_Ope_ZoneEuro]
		,[Nb_Ope_HorsZoneEuro]
		)
	select 
	date_operation,
	[Visa / Visa Premier],
	[Immédiat / Différé],
	sum([Nb_Ope_France]			) AS [Nb_Ope_France],
	sum([Nb_Ope_ZoneEuro]		) AS [Nb_Ope_ZoneEuro],
	sum([Nb_Ope_HorsZoneEuro]	) AS [Nb_Ope_HorsZoneEuro]
	from (
	select		
		date_operation,
		ope_groupe.[Visa / Visa Premier],
		ope_groupe.[Immédiat / Différé],
		case when ope_groupe.[Zone paiement]='France' then sum(nb_op_cli) end as [Nb_Ope_France],
		case when ope_groupe.[Zone paiement]='Zone euro' then sum(nb_op_cli) end as [Nb_Ope_ZoneEuro],
		case when ope_groupe.[Zone paiement]='Hors zone euro' then sum(nb_op_cli) end as [Nb_Ope_HorsZoneEuro]
	from
		(select 
		format(DTE_OPE, 'yyyyMM') as date_operation
		,cpt.DWHCPTPPAL as IDClientSAB
		,case
			when substring(DON_ANA,60,3) in ('MGI','MGD') then 'RET_CRTC'
			when substring(DON_ANA,60,3) in ('MPI','MPD') then 'RET_CRTP'
		end as 'Visa / Visa Premier'
			,case
			when substring(DON_ANA,60,3) in ('MGI','MPI') then 'Deb_Imm'
			when substring(DON_ANA,60,3) in ('MGD','MPD') then 'Deb_Diff'
		end	as 'Immédiat / Différé'
		,case
			when rtrim(ltrim(substring(DON_ANA,74,2))) in ('FR','RE','GP','MQ','GF','YT') then 'France'
			when rtrim(ltrim(substring(DON_ANA,74,2))) in ('DE','AT','BE','ES','FI','IE','IT','LU','NL','PT','GR','SI','CY','MT','SK','EE','LV','LT') then 'Zone euro'
			else 'Hors zone euro'
		end as 'Zone paiement'
		,sum(MTT_EUR) as mtt_tt_cli
		,sum(
		case 
			when COD_OPE = 'CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) in ('47','90','56') and substring(mouv.DON_ANA,64,2) in ('RD','RG') and MTT_EUR >0	then 1	-- retrait
			when COD_OPE = 'CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) = ('RD') and MTT_EUR <0 then -1 		-- rejet retrait
	end) as nb_op_cli
		from 
			#TAB_MVT mouv
			inner join #TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM
		where 	(
			(COD_OPE ='CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) in ('47','90','56') and substring(mouv.DON_ANA,64,2) in ('RD','RG') and MTT_EUR > 0)	-- retrait
			OR (COD_OPE ='CTB' and substring(mouv.DON_ANA,60,3) IN ('MGI','MGD','MPI','MPD') and substring(mouv.COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) in ('RD','RG') and MTT_EUR <0)			-- annulation retrait
			)
		group by format(DTE_OPE, 'yyyyMM') ,cpt.DWHCPTPPAL
		,case
			when substring(DON_ANA,60,3) in ('MGI','MGD') then 'RET_CRTC'
			when substring(DON_ANA,60,3) in ('MPI','MPD') then 'RET_CRTP'
		end 		
		,case
			when substring(DON_ANA,60,3) in ('MGI','MPI') then 'Deb_Imm'
			when substring(DON_ANA,60,3) in ('MGD','MPD') then 'Deb_Diff'
		end	
		,case
			when rtrim(ltrim(substring(DON_ANA,74,2))) in ('FR','RE','GP','MQ','GF','YT') then 'France'
			when rtrim(ltrim(substring(DON_ANA,74,2))) in ('DE','AT','BE','ES','FI','IE','IT','LU','NL','PT','GR','SI','CY','MT','SK','EE','LV','LT') then 'Zone euro'
			else 'Hors zone euro' end) ope_groupe
			where nb_op_cli <> 0
	group by date_operation, ope_groupe.[Visa / Visa Premier],ope_groupe.[Immédiat / Différé],ope_groupe.[Zone paiement]
	) as T 
	group by date_operation,[Visa / Visa Premier],[Immédiat / Différé];


	INSERT INTO [dbo].[VDC_TYPE_OPERATION] (
		 [Date_operation] 
		,[Type_Op1] 
		,[Type_Op2] 
		,[Nb_Ope]
		,[Nb_Ope_France]
		,[Nb_Ope_ZoneEuro]
		,[Nb_Ope_HorsZoneEuro]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope] 
	)
	select 	 [date_operation]
			,[Type_Op1]
			,[Type_Op2]
			,SUM([Nb_Ope]				 ) AS [Nb_Ope]
			,SUM([Nb_Ope_France] 	     ) AS [Nb_Ope_France] 
			,SUM([Nb_Ope_ZoneEuro] 	     ) AS [Nb_Ope_ZoneEuro] 
			,SUM([Nb_Ope_HorsZoneEuro] 	 ) AS [Nb_Ope_HorsZoneEuro] 
			,SUM([Montant_en_k€] 		 ) AS [Montant_en_k€] 
			,SUM([Nb_moyen_Ope] 		 ) AS [Nb_moyen_Ope] 
			,SUM([Montant_moyen_Ope] 	 ) AS [Montant_moyen_Ope] 
			,SUM([Nb_médian_Ope] 		 ) AS [Nb_médian_Ope] 
			,SUM([Montant_médian_Ope] 	 ) AS [Montant_médian_Ope] 
	from #VDC_TYPE_OPERATION
	GROUP BY [date_operation],[Type_Op1],[Type_Op2];


 	
	--============================= MOYENNE & TOTAL PAIEMENT CARTE MOBILE ================================
	TRUNCATE TABLE #VDC_TYPE_OPERATION;
	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
	)
	select
	date_operation,
	count(distinct IDClientSAB) as 'Nb de clients ayant fait un paiement mobile',
	sum(nb_op_cli) as 'Nb paiements mobile',
	(sum(mtt_tt_cli)/1000) as 'Montant paiements mobile en k€',
	avg(cast(nb_op_cli as decimal(6,2))) as 'Nb moyen de paiements mobile',
	avg(mtt_tt_cli) as 'Montant moyen de paiements mobile' 
	from
	(select 
	format(DTE_OPE, 'yyyyMM') as date_operation
	,cpt.DWHCPTPPAL as IDClientSAB
	,sum(MTT_EUR)
	as mtt_tt_cli,
	sum(
		case 
			when COD_OPE = 'CTB' and substring(DON_ANA,60,3) ='WMI'  and substring(COD_ANA,5,2) in ('37','50') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR >0 then 1						-- paiement mobile
			when COD_OPE = 'CTB' and substring(DON_ANA,60,3) ='WMI' and substring(COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR <0 then -1					-- rejet paiement mobile
	end) as nb_op_cli

	from 
		#TAB_MVT mouv
		inner join 	#TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM
	where 
	 (
	(COD_OPE = 'CTB' and substring(DON_ANA,60,3) = 'WMI' and substring(COD_ANA,5,2) in ('37','50') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR > 0)			-- operations carte mobile
	OR (COD_OPE = 'CTB' and substring(DON_ANA,60,3) = 'WMI' and substring(COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR < 0)			-- annulation operations carte mobile
	OR (COD_OPE = 'CTB' and substring(DON_ANA,60,3) = 'WMI' and substring(COD_ANA,5,2) in ('45','52') and substring(mouv.DON_ANA,64,2) = ('FC') and MTT_EUR <0 )	-- remboursement cartes
	)	
	group by format(DTE_OPE, 'yyyyMM') ,cpt.DWHCPTPPAL) ope_groupe
	where nb_op_cli <> 0
	group by date_operation;

	INSERT INTO #VDC_TYPE_OPERATION (
		[Date_operation] 
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope])
	select distinct
	ope_groupe.Mois_ope,		
	PERCENTILE_CONT(0.5) within group (order by ope_groupe.nb_op) over (partition by Mois_ope)  'Nb médian de paiements mobile',
	(PERCENTILE_CONT(0.5) within group (order by ope_groupe.mtt_tt) over (partition by Mois_ope)) 'Montant médian de paiements mobile'
	from
	(
	select
	format(DTE_OPE, 'yyyyMM')  Mois_ope,
	cpt.DWHCPTPPAL,
	sum(MTT_EUR) as mtt_tt,
	sum(case 		when COD_OPE = 'CTB' and substring(DON_ANA,60,3) ='WMI'  and substring(COD_ANA,5,2) in ('37','50') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR >0 then 1						-- paiement mobile
			when COD_OPE = 'CTB' and substring(DON_ANA,60,3) ='WMI' and substring(COD_ANA,5,2) = ('17') and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR <0 then -1 end					-- rejet paiement mobile end) as nb_op
	) as nb_op
	from #TAB_MVT mouv
			inner join #TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM
	where  (
	(COD_OPE = 'CTB' and substring(COD_ANA,5,2) in ('37','50')  and substring(DON_ANA,60,3) = 'WMI' and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR >0)				-- operations carte mobile debiteur
	OR (COD_OPE = 'CTB' and substring(COD_ANA,5,2) = ('17')  and substring(DON_ANA,60,3) = 'WMI' and substring(mouv.DON_ANA,64,2) = ('FA') and MTT_EUR < 0)				-- annulations carte mobile debiteur
	OR (COD_OPE = 'CTB' and substring(COD_ANA,5,2) in ('45','52') and substring(DON_ANA,60,3) = 'WMI' and substring(mouv.DON_ANA,64,2) = ('FC') and MTT_EUR <0 )	-- remboursement cartes
	)	
	group by format(DTE_OPE, 'yyyyMM') , cpt.DWHCPTPPAL) ope_groupe			
	where nb_op <> 0;	

	INSERT INTO [dbo].[VDC_TYPE_OPERATION] (
		 [Date_operation] 
		,[Type_Op1] 
		,[Type_Op2] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope] 
	)
	select 	[date_operation],'NA' AS [Type_Op1], 'PAI_MOB' AS [Type_Op2]
		,SUM([Nb_Ope1]				 ) AS [Nb_Ope1]
		,SUM([Nb_Ope2] 			     ) AS [Nb_Ope2] 
		,SUM([Montant_en_k€] 		 ) AS [Montant_en_k€] 
		,SUM([Nb_moyen_Ope] 		 ) AS [Nb_moyen_Ope] 
		,SUM([Montant_moyen_Ope] 	 ) AS [Montant_moyen_Ope] 
		,SUM([Nb_médian_Ope] 		 ) AS [Nb_médian_Ope] 
		,SUM([Montant_médian_Ope] 	 ) AS [Montant_médian_Ope] 
	from #VDC_TYPE_OPERATION
	GROUP BY [date_operation];



	--========================= MOYENNE & TOTAL VIREMENT DEBITEUR CLASSIQUE HORS SMS ====================================	

	TRUNCATE TABLE #VDC_TYPE_OPERATION;
	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
	)
	select 
	date_operation,
	count(distinct IDClientSAB) as 'Nb de clients ayant émis un virement classique hors SMS',
	sum(nb_op_cli) as 'Nb de virements émis classique hors SMS',
	sum((mtt_tt_cli)/1000) as 'Montant total de virements émis  classique hors SMS en k€',
	avg(cast(nb_op_cli as decimal(6,2))) as 'Nb moyen de virements émis classique hors SMS',
	avg(mtt_tt_cli) as 'Montant moyen de virements émis classique hors SMS'
	from
	(select 
	format(DTE_OPE, 'yyyyMM') as date_operation
	,cpt.DWHCPTPPAL as IDClientSAB
	,sum(MTT_EUR) as mtt_tt_cli,
	sum(
		case 
			when COD_OPE in ('A0V', 'R0V') and MTT_EUR >0 then 1
			when COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR <0 then -1
	end) as nb_op_cli
	from 
		#tab_vir mouv
		inner join #TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM
	where ((COD_OPE in ('A0V', 'R0V') and MTT_EUR >0) OR (COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR <0))			-- operations virements
	and mouv.TYPE_MTT = 'Virement classique'
	group by format(DTE_OPE, 'yyyyMM') ,cpt.DWHCPTPPAL) ope_groupe
	where nb_op_cli <> 0
	group by date_operation;

	INSERT INTO #VDC_TYPE_OPERATION (
		[Date_operation] 
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope])
	select distinct
	ope_groupe.Mois_ope,
	PERCENTILE_CONT(0.5) within group (order by ope_groupe.nb_op) over (partition by Mois_ope)  'Nb médian de virements émis classique hors SMS',
	(PERCENTILE_CONT(0.5) within group (order by ope_groupe.mtt_tt) over (partition by Mois_ope))  'Montant médian de virements émis classique hors SMS'
	from(
	select
	format(DTE_OPE, 'yyyyMM')  Mois_ope,
	cpt.DWHCPTPPAL,
	sum(MTT_EUR) as mtt_tt,
	sum(case
			when COD_OPE in ('A0V', 'R0V') and MTT_EUR >0 then 1
			when COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR <0 then -1
	end) as nb_op
	from
		#tab_vir mouv
		inner join #TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM
	where ((COD_OPE in ('A0V', 'R0V') and MTT_EUR >0) OR (COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR <0))			-- operations virements
	and mouv.TYPE_MTT = 'Virement classique'
	group by format(DTE_OPE, 'yyyyMM') , cpt.DWHCPTPPAL) ope_groupe
	where nb_op <> 0;

	INSERT INTO [dbo].[VDC_TYPE_OPERATION] (
		 [Date_operation] 
		,[Type_Op1] 
		,[Type_Op2] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope] 
	)
	select 	[date_operation],'NA' AS [Type_Op1], 'VIR_CL_EMI' AS [Type_Op2]
		,SUM([Nb_Ope1]				 ) AS [Nb_Ope1]
		,SUM([Nb_Ope2] 			     ) AS [Nb_Ope2] 
		,SUM([Montant_en_k€] 		 ) AS [Montant_en_k€] 
		,SUM([Nb_moyen_Ope] 		 ) AS [Nb_moyen_Ope] 
		,SUM([Montant_moyen_Ope] 	 ) AS [Montant_moyen_Ope] 
		,SUM([Nb_médian_Ope] 		 ) AS [Nb_médian_Ope] 
		,SUM([Montant_médian_Ope] 	 ) AS [Montant_médian_Ope] 
	from #VDC_TYPE_OPERATION
	GROUP BY [date_operation];


	--========================= MOYENNE & TOTAL VIREMENT DEBITEUR SMS ====================================	

	TRUNCATE TABLE #VDC_TYPE_OPERATION;
	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
	)
	select 
	date_operation,
	count(distinct IDClientSAB) as 'Nb de clients ayant émis un virement SMS',
	sum(nb_op_cli) as 'Nb de virements émis SMS',
	sum((mtt_tt_cli)/1000) as 'Montant total de virements émis SMS en k€',
	avg(cast(nb_op_cli as decimal(6,2))) as 'Nb moyen de virements émis SMS',
	avg(mtt_tt_cli) as 'Montant moyen de virements émis SMS'
	from
	(select 
	format(DTE_OPE, 'yyyyMM') as date_operation
	,cpt.DWHCPTPPAL as IDClientSAB
	,sum(MTT_EUR) as mtt_tt_cli,
	sum(
		case 
			when COD_OPE in ('A0V', 'R0V') and MTT_EUR >0 then 1
			when COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR <0 then -1
	end) as nb_op_cli
	from 
		#tab_vir mouv
		inner join #TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM
	where ((COD_OPE in ('A0V', 'R0V') and MTT_EUR >0) OR (COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR <0))			-- operations virements
	and mouv.TYPE_MTT = 'Virement SMS'
	group by format(DTE_OPE, 'yyyyMM') ,cpt.DWHCPTPPAL) ope_groupe
	where nb_op_cli <> 0
	group by date_operation;

	INSERT INTO #VDC_TYPE_OPERATION (
		[Date_operation] 
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope])
	select distinct
	ope_groupe.Mois_ope,	
	PERCENTILE_CONT(0.5) within group (order by ope_groupe.nb_op) over (partition by Mois_ope)  'Nb médian de virements SMS émis',
	(PERCENTILE_CONT(0.5) within group (order by ope_groupe.mtt_tt) over (partition by Mois_ope)) 'Montant médian de virements SMS émis'
	from
	(
	select
	format(DTE_OPE, 'yyyyMM')  Mois_ope,
	cpt.DWHCPTPPAL,
	sum(MTT_EUR) as mtt_tt,
	sum(case
			when COD_OPE in ('A0V', 'R0V') and MTT_EUR >0 then 1
			when COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR <0 then -1

	end) as nb_op

	from
		#tab_vir mouv
		inner join #TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM
	where ((COD_OPE in ('A0V', 'R0V') and MTT_EUR >0) OR (COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR <0))			-- operations virements
	and mouv.TYPE_MTT = 'Virement SMS'
	group by format(DTE_OPE, 'yyyyMM') , cpt.DWHCPTPPAL) ope_groupe
	where nb_op <> 0;

	INSERT INTO [dbo].[VDC_TYPE_OPERATION] (
		 [Date_operation] 
		,[Type_Op1] 
		,[Type_Op2] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope] 
	)
	select 	[date_operation],'NA' AS [Type_Op1], 'VIR_SMSEMI' AS [Type_Op2]
		,SUM([Nb_Ope1]				 ) AS [Nb_Ope1]
		,SUM([Nb_Ope2] 			     ) AS [Nb_Ope2] 
		,SUM([Montant_en_k€] 		 ) AS [Montant_en_k€] 
		,SUM([Nb_moyen_Ope] 		 ) AS [Nb_moyen_Ope] 
		,SUM([Montant_moyen_Ope] 	 ) AS [Montant_moyen_Ope] 
		,SUM([Nb_médian_Ope] 		 ) AS [Nb_médian_Ope] 
		,SUM([Montant_médian_Ope] 	 ) AS [Montant_médian_Ope] 
	from #VDC_TYPE_OPERATION
	GROUP BY [date_operation];



	--========================== MOYENNE & TOTAL CHEQUE EMIS ===================================
	TRUNCATE TABLE #VDC_TYPE_OPERATION;
	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
		)
	select 
	date_operation,
	count(distinct IDClientSAB) as 'Nb de clients ayant émis un chèque',
	sum(nb_op_cli) as 'Nb de chèques émis',
	sum((mtt_tt_cli)/1000) as 'Montant total de chèques émis en k€',
	avg(cast(nb_op_cli as decimal(6,2))) as 'Nb moyen de chèques émis',
	avg(mtt_tt_cli) as 'Montant moyen de chèques émis'
	from
	(select 
	format(DTE_OPE, 'yyyyMM') as date_operation
	,cpt.DWHCPTPPAL as IDClientSAB
	,sum(MTT_EUR)
	as mtt_tt_cli,
	sum(
		case 
			when COD_OPE in ('RI0') and MTT_EUR > 0 then 1
			when COD_OPE in ('AI1', 'RI0') and MTT_EUR < 0 then -1
	end) as nb_op_cli
	from 
		#TAB_MVT mouv
		inner join 
				#TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM

	where  COD_OPE in ('RI0', 'AI1')			-- operations cheques emis
	group by format(DTE_OPE, 'yyyyMM') ,cpt.DWHCPTPPAL) ope_groupe
	where nb_op_cli <> 0
	group by date_operation;

	INSERT INTO #VDC_TYPE_OPERATION (
		[Date_operation] 
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope])
	select distinct
	ope_groupe.Mois_ope,	
	PERCENTILE_CONT(0.5) within group (order by ope_groupe.nb_op) over (partition by Mois_ope)  'Nb médian de chèques émis',
	(PERCENTILE_CONT(0.5) within group (order by ope_groupe.mtt_tt) over (partition by Mois_ope)) 'Montant médian de chèques émis'
	from
	(
	select
	format(DTE_OPE, 'yyyyMM')  Mois_ope,
	cpt.DWHCPTPPAL,
	sum(MTT_EUR) as mtt_tt,
	sum(case
			when COD_OPE in ('RI0') and MTT_EUR > 0 then 1
			when COD_OPE in ('AI1','RI0') and MTT_EUR < 0 then -1
	end) as nb_op

	from #TAB_MVT mouv
			inner join #TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM
	where COD_OPE in ('RI0', 'AI1')			-- operations cheques emis
	group by format(DTE_OPE, 'yyyyMM') , cpt.DWHCPTPPAL) ope_groupe
	where nb_op <> 0;

	INSERT INTO [dbo].[VDC_TYPE_OPERATION] (
		 [Date_operation] 
		,[Type_Op1] 
		,[Type_Op2] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope] 
	)
	select 	[date_operation],'NA' AS [Type_Op1], 'CH_EMI' AS [Type_Op2]
		,SUM([Nb_Ope1]				 ) AS [Nb_Ope1]
		,SUM([Nb_Ope2] 			     ) AS [Nb_Ope2] 
		,SUM([Montant_en_k€] 		 ) AS [Montant_en_k€] 
		,SUM([Nb_moyen_Ope] 		 ) AS [Nb_moyen_Ope] 
		,SUM([Montant_moyen_Ope] 	 ) AS [Montant_moyen_Ope] 
		,SUM([Nb_médian_Ope] 		 ) AS [Nb_médian_Ope] 
		,SUM([Montant_médian_Ope] 	 ) AS [Montant_médian_Ope] 
	from #VDC_TYPE_OPERATION
	GROUP BY [date_operation];
	

 
	--========================= MOYENNE & TOTAL PRELEVEMENT ====================================	
	TRUNCATE TABLE #VDC_TYPE_OPERATION;
	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
		)
	Select date_operation,
	count(distinct IDClientSAB) as 'Nb de clients ayant mis en place un prélèvement',
	sum(nb_op_cli) as 'Nb de prélèvement',
	sum((mtt_tt_cli)/1000) as 'Montant total de prélèvement en k€',
	avg(cast(nb_op_cli as decimal(6,2))) as 'Nb moyen de prélèvement',
	avg(mtt_tt_cli) as 'Montant moyen de prélèvement'
	from
	(select 
	format(DTE_OPE, 'yyyyMM') as date_operation
	,cpt.DWHCPTPPAL as IDClientSAB
	,sum(MTT_EUR)
	as mtt_tt_cli,
	sum(
		case 
			when COD_OPE in ('A0P', 'R0P') and MTT_EUR >0 then 1
			when COD_OPE in ('A0P', 'R0P','A1P', 'A2P', 'R1P', 'R2P') and MTT_EUR <0 then -1
	end) as nb_op_cli
	from 
		#TAB_MVT mouv
		inner join 
				#TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM

	where  COD_OPE in ('A0P','R0P','A1P', 'A2P', 'R1P', 'R2P')			-- operations prelevements
	group by format(DTE_OPE, 'yyyyMM') ,cpt.DWHCPTPPAL) ope_groupe
	where nb_op_cli <> 0
	group by date_operation;

	INSERT INTO #VDC_TYPE_OPERATION (
		[Date_operation] 
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope])
	select distinct
	ope_groupe.Mois_ope,
	PERCENTILE_CONT(0.5) within group (order by ope_groupe.nb_op) over (partition by Mois_ope)  'Nb médian de prélèvement',
	(PERCENTILE_CONT(0.5) within group (order by ope_groupe.mtt_tt) over (partition by Mois_ope))  'Montant médian de prélèvement'
	from
	(
	select
	format(DTE_OPE, 'yyyyMM')  Mois_ope,
	cpt.DWHCPTPPAL,
	sum(MTT_EUR) as mtt_tt,
	sum(case
			when COD_OPE in ('A0P', 'R0P') and MTT_EUR >0 then 1
			when COD_OPE in ('A0P', 'R0P','A1P', 'A2P', 'R1P', 'R2P') and MTT_EUR <0 then -1
	end) as nb_op

	from
		#TAB_MVT mouv
		inner join #TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM

	where COD_OPE in ('A0P','R0P','A1P', 'A2P', 'R1P', 'R2P')			-- operations prelevements

	group by format(DTE_OPE, 'yyyyMM') , cpt.DWHCPTPPAL
	) ope_groupe
	where nb_op <> 0;

	INSERT INTO [dbo].[VDC_TYPE_OPERATION] (
		 [Date_operation] 
		,[Type_Op1] 
		,[Type_Op2] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope] 
	)
	select 	[date_operation],'NA' AS [Type_Op1], 'PRV_ENP' AS [Type_Op2]
		,SUM([Nb_Ope1]				 ) AS [Nb_Ope1]
		,SUM([Nb_Ope2] 			     ) AS [Nb_Ope2] 
		,SUM([Montant_en_k€] 		 ) AS [Montant_en_k€] 
		,SUM([Nb_moyen_Ope] 		 ) AS [Nb_moyen_Ope] 
		,SUM([Montant_moyen_Ope] 	 ) AS [Montant_moyen_Ope] 
		,SUM([Nb_médian_Ope] 		 ) AS [Nb_médian_Ope] 
		,SUM([Montant_médian_Ope] 	 ) AS [Montant_médian_Ope] 
	from #VDC_TYPE_OPERATION
	GROUP BY [date_operation];	

	/* ------------------------------- Operations creditrices -------------------------------- */
	

	--========================== MOYENNE & TOTAL MOUVEMENT CREDITEUR ===================================	
	TRUNCATE TABLE #VDC_TYPE_OPERATION;
	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope])
	select	
	date_operation,
	count(distinct IDClientSAB) as 'Nb de clients ayant réalisé une opé créditrice',
	sum(nb_op_cli) as 'Nb  opé créditrices',
	(-sum(mtt_tt_cli)/1000) as 'Montant total opé créditrices en k€',
	avg(cast(nb_op_cli as decimal(6,2))) as 'Nb moyen opé créditrices',
	(-avg(mtt_tt_cli)) 'Montant moyen opé créditrices'
	from
	(select 
	format(DTE_OPE, 'yyyyMM') as date_operation
	,cpt.DWHCPTPPAL as IDClientSAB
	,sum(MTT_EUR)
	as mtt_tt_cli,
	sum(
		case 
		when COD_OPE in ('A0V', 'R0V') and MTT_EUR <0 then 1							-- virement crediteur
		when COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR >0 then -1				-- rejet virement crediteur
		when COD_OPE = ('AI0') and MTT_EUR < 0 then 1 									-- cheque encaisse
		when COD_OPE in ('RI1', 'AI0') and MTT_EUR > 0 then -1							-- rejet cheque encaisse
	end) as nb_op_cli
	from 
		#TAB_MVT mouv
		inner join 
				#TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM
	where 
	 /* conditions code operations */
	(
		(COD_OPE in ('A0V', 'R0V') and MTT_EUR <0)						-- virement crediteur
	OR (COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR >0)			-- rejet virement crediteur
	OR (COD_OPE in ('AI0'))  											-- cheque encaisse
	OR (COD_OPE in ('RI1') and MTT_EUR > 0) 							-- rejet cheque encaisse
	)
	group by format(DTE_OPE, 'yyyyMM') ,cpt.DWHCPTPPAL) ope_groupe
	where nb_op_cli <> 0
	group by date_operation;

	INSERT INTO #VDC_TYPE_OPERATION (
		[Date_operation] 
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope])
	select distinct
	ope_groupe.Mois_ope,	
	PERCENTILE_CONT(0.5) within group (order by ope_groupe.nb_op) over (partition by Mois_ope)  'Nb médian opé créditrices',
	(-PERCENTILE_CONT(0.5) within group (order by ope_groupe.mtt_tt) over (partition by Mois_ope))  'Montant médian opé créditrices'	
	from
	(
	select
	format(DTE_OPE, 'yyyyMM')  Mois_ope,
	cpt.DWHCPTPPAL as IDClientSAB,
	sum(MTT_EUR) as mtt_tt,
	sum(case
		when COD_OPE in ('A0V', 'R0V') and MTT_EUR <0 then 1							-- virement crediteur
		when COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR >0 then -1						-- rejet virement crediteur
		when COD_OPE in ('AI0') and MTT_EUR < 0 then 1 													-- cheque encaisse
		when COD_OPE in ('RI1','AI0') and MTT_EUR > 0 then -1												-- rejet cheque encaisse
		end) as nb_op
	from #TAB_MVT mouv
	inner join #TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM
	where /* conditions code operations */
	(
		(COD_OPE in ('A0V', 'R0V') and MTT_EUR <0)						-- virement crediteur
	OR (COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR >0)						-- rejet virement crediteur
	OR (COD_OPE in ('AI0'))  											-- cheque encaisse
	OR (COD_OPE in ('RI1')) 											-- rejet cheque encaisse
	)
		group by format(DTE_OPE, 'yyyyMM') , cpt.DWHCPTPPAL ) ope_groupe				
	where nb_op <> 0;	


	INSERT INTO [dbo].[VDC_TYPE_OPERATION] (
		 [Date_operation] 
		,[Type_Op1] 
		,[Type_Op2] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope] 
	)
	select 	[date_operation],'NA' AS [Type_Op1], 'TOP_CRE' AS [Type_Op2]
		,SUM([Nb_Ope1]				 ) AS [Nb_Ope1]
		,SUM([Nb_Ope2] 			     ) AS [Nb_Ope2] 
		,SUM([Montant_en_k€] 		 ) AS [Montant_en_k€] 
		,SUM([Nb_moyen_Ope] 		 ) AS [Nb_moyen_Ope] 
		,SUM([Montant_moyen_Ope] 	 ) AS [Montant_moyen_Ope] 
		,SUM([Nb_médian_Ope] 		 ) AS [Nb_médian_Ope] 
		,SUM([Montant_médian_Ope] 	 ) AS [Montant_médian_Ope] 
	from #VDC_TYPE_OPERATION
	GROUP BY [date_operation];


	--========================= MOYENNE & TOTAL VIREMENT CREDITEUR - hors TOP-UP ====================================
	TRUNCATE TABLE #VDC_TYPE_OPERATION;	
	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
	)	
	--========================= MOYENNE & TOTAL VIREMENT CREDITEUR - classique ====================================	
	select	
	date_operation,
	count(distinct IDClientSAB) as 'Nb de clients ayant reçu un virement classique hors Top-up, SMS et demande d''argent',
	sum(nb_op_cli) as 'Nb de virements reçus hors Top-up, SMS et demande d''argent',
	(-sum((mtt_tt_cli)/1000)) as 'Montant virements reçus hors Top-up, SMS et demande d''argent en k€',
	avg(cast(nb_op_cli as decimal(6,2))) as 'Nb moyen de virements reçus hors Top-up, SMS et demande d''argent',
	(-avg(mtt_tt_cli)) as 'Montant moyen de virements reçus hors Top-up, SMS et demande d''argent'
	from
	(select 
	format(DTE_OPE, 'yyyyMM') as date_operation
	,cpt.DWHCPTPPAL as IDClientSAB
	,sum(MTT_EUR) as mtt_tt_cli,
	sum(
		case 
			when COD_OPE in ('A0V', 'R0V') and MTT_EUR <0 then 1
			when COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR >0 then -1
	end) as nb_op_cli
	from 
		#TAB_MVT_CRED mouv
		inner join 
				#TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM
	where  ((COD_OPE in ('A0V', 'R0V') and MTT_EUR <0) OR (COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR >0))			-- operations virements crediteurs
	and TYPE_MTT in ('Virement classique','1er versement')
	group by format(DTE_OPE, 'yyyyMM') ,cpt.DWHCPTPPAL) ope_groupe
	where nb_op_cli <> 0
	group by date_operation;

	INSERT INTO #VDC_TYPE_OPERATION (
		[Date_operation] 
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope])
	select distinct
	ope_groupe.Mois_ope,
	PERCENTILE_CONT(0.5) within group (order by ope_groupe.nb_op) over (partition by Mois_ope)  'Nb médian de virements classiques reçus',
	(-PERCENTILE_CONT(0.5) within group (order by ope_groupe.mtt_tt) over (partition by Mois_ope))  'Montant médian de virements classiques reçus'
	from
	(select 
	format(DTE_OPE, 'yyyyMM') as Mois_ope
	,cpt.DWHCPTPPAL as IDClientSAB
	,sum(MTT_EUR) as mtt_tt,
	sum(
		case 
			when COD_OPE in ('A0V', 'R0V') and MTT_EUR <0 then 1
			when COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR >0 then -1
	end) as nb_op
	from 
		#TAB_MVT_CRED mouv
		inner join 
				#TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM
	where ((COD_OPE in ('A0V', 'R0V') and MTT_EUR <0) OR (COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR >0))			-- operations virements crediteurs
	and TYPE_MTT in ('Virement classique','1er versement')
	group by format(DTE_OPE, 'yyyyMM') ,cpt.DWHCPTPPAL) ope_groupe
	where nb_op <> 0;
 
	INSERT INTO [dbo].[VDC_TYPE_OPERATION] (
		 [Date_operation] 
		,[Type_Op1] 
		,[Type_Op2] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope] 
	)
	select 	[date_operation],'NA' AS [Type_Op1], 'VIR_CLA_RE' AS [Type_Op2]  -- VIR_HTP
		,SUM([Nb_Ope1]				 ) AS [Nb_Ope1]
		,SUM([Nb_Ope2] 			     ) AS [Nb_Ope2] 
		,SUM([Montant_en_k€] 		 ) AS [Montant_en_k€] 
		,SUM([Nb_moyen_Ope] 		 ) AS [Nb_moyen_Ope] 
		,SUM([Montant_moyen_Ope] 	 ) AS [Montant_moyen_Ope] 
		,SUM([Nb_médian_Ope] 		 ) AS [Nb_médian_Ope] 
		,SUM([Montant_médian_Ope] 	 ) AS [Montant_médian_Ope] 
	from #VDC_TYPE_OPERATION
	GROUP BY [date_operation];

	--========================= MOYENNE & TOTAL VIREMENT TOP-UP ====================================
	TRUNCATE TABLE #VDC_TYPE_OPERATION;
	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
	)
	select		
	date_operation,
	count(distinct IDClientSAB) as 'Nb de clients ayant reçu un virement Top-Up',
	sum(nb_op_cli) as 'Nb de virements reçus Top-Up',
	(-sum(mtt_tt_cli)/1000) as 'Montant virements reçus Top-Up en k€',
	avg(cast(nb_op_cli as decimal(6,2))) as 'Nb moyen de virements reçus  Top-Up',
	(-avg(mtt_tt_cli)) as 'Montant moyen de virements reçus  Top-Up'
	from
	(select 
	format(DTE_OPE, 'yyyyMM') as date_operation
	,cpt.DWHCPTPPAL as IDClientSAB
	,sum(MTT_EUR)

	as mtt_tt_cli,

	sum(
		case 
			when COD_OPE in ('A0V', 'R0V') and MTT_EUR <0 then 1
			when COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR >0 then -1
	end) as nb_op_cli
	from 
		#TAB_MVT_CRED mouv
		inner join 
				#TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM

	where  ((COD_OPE in ('A0V', 'R0V') and MTT_EUR <0) OR (COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR >0))			-- operations virements crediteurs
	and TYPE_MTT = 'Top-up'
	group by format(DTE_OPE, 'yyyyMM') ,cpt.DWHCPTPPAL) ope_groupe
	where nb_op_cli <> 0
	group by date_operation;

	INSERT INTO #VDC_TYPE_OPERATION (
		[Date_operation] 
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope])
	select distinct
	ope_groupe.Mois_ope,	
	PERCENTILE_CONT(0.5) within group (order by ope_groupe.nb_op) over (partition by Mois_ope)  'Nb médian de virements reçus Top-up',
	(-PERCENTILE_CONT(0.5) within group (order by ope_groupe.mtt_tt) over (partition by Mois_ope))  'Montant médian de virements reçus Top-up'
	from

	(select 
	format(DTE_OPE, 'yyyyMM') as Mois_ope
	,cpt.DWHCPTPPAL as IDClientSAB
	,sum(MTT_EUR)

	as mtt_tt,

	sum(
		case 
			when COD_OPE in ('A0V', 'R0V') and MTT_EUR <0 then 1
			when COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR >0 then -1
	end) as nb_op
	from 
		#TAB_MVT_CRED mouv
		inner join 
				#TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM

	where  ((COD_OPE in ('A0V', 'R0V') and MTT_EUR <0) OR (COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR >0))			-- operations virements crediteurs
	and TYPE_MTT = 'Top-up'
	group by format(DTE_OPE, 'yyyyMM') ,cpt.DWHCPTPPAL) ope_groupe
	where nb_op <> 0;

	INSERT INTO [dbo].[VDC_TYPE_OPERATION] (
		 [Date_operation] 
		,[Type_Op1] 
		,[Type_Op2] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope] 
	)
	select 	[date_operation],'NA' AS [Type_Op1], 'TOP_UP' AS [Type_Op2]
		,SUM([Nb_Ope1]				 ) AS [Nb_Ope1]
		,SUM([Nb_Ope2] 			     ) AS [Nb_Ope2] 
		,SUM([Montant_en_k€] 		 ) AS [Montant_en_k€] 
		,SUM([Nb_moyen_Ope] 		 ) AS [Nb_moyen_Ope] 
		,SUM([Montant_moyen_Ope] 	 ) AS [Montant_moyen_Ope] 
		,SUM([Nb_médian_Ope] 		 ) AS [Nb_médian_Ope] 
		,SUM([Montant_médian_Ope] 	 ) AS [Montant_médian_Ope] 
	from #VDC_TYPE_OPERATION
	GROUP BY [date_operation];


	--========================= MOYENNE & TOTAL VIREMENT SMS ====================================
	TRUNCATE TABLE #VDC_TYPE_OPERATION;
	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
	)
	select 	
	date_operation,
	count(distinct IDClientSAB) as 'Nb de clients ayant reçu un virement SMS',
	sum(nb_op_cli) as 'Nb de virements reçus SMS',
	(-sum((mtt_tt_cli)/1000)) as 'Montant virements reçus SMS en k€',
	avg(cast(nb_op_cli as decimal(6,2))) as 'Nb moyen de virements reçus SMS',
	(-avg(mtt_tt_cli)) as 'Montant moyen de virements reçus SMS'
	from
	(select 
	format(DTE_OPE, 'yyyyMM') as date_operation
	,cpt.DWHCPTPPAL as IDClientSAB
	,sum(MTT_EUR) as mtt_tt_cli,
	sum(
		case 
			when COD_OPE in ('A0V', 'R0V') and MTT_EUR <0 then 1
			when COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR >0 then -1
	end) as nb_op_cli
	from 
		#TAB_MVT_CRED mouv
		inner join 
				#TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM
	where  ((COD_OPE in ('A0V', 'R0V') and MTT_EUR <0) OR (COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR >0))			-- operations virements crediteurs
	and TYPE_MTT = 'Virement SMS'
	group by format(DTE_OPE, 'yyyyMM') ,cpt.DWHCPTPPAL) ope_groupe
	where nb_op_cli <> 0
	group by date_operation;

	INSERT INTO #VDC_TYPE_OPERATION (
		[Date_operation] 
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope])
	select distinct
	ope_groupe.Mois_ope,	
	PERCENTILE_CONT(0.5) within group (order by ope_groupe.nb_op) over (partition by Mois_ope)  'Nb médian de virements SMS reçus',
	(-PERCENTILE_CONT(0.5) within group (order by ope_groupe.mtt_tt) over (partition by Mois_ope)) 'Montant médian de virements SMS reçus'
	from
	(select 
	format(DTE_OPE, 'yyyyMM') as Mois_ope
	,cpt.DWHCPTPPAL as IDClientSAB
	,sum(MTT_EUR) as mtt_tt,
	sum(
		case 
			when COD_OPE in ('A0V', 'R0V') and MTT_EUR <0 then 1
			when COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR >0 then -1
	end) as nb_op
	from 
		#TAB_MVT_CRED mouv
		inner join 
				#TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM
	where  ((COD_OPE in ('A0V', 'R0V') and MTT_EUR <0) OR (COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR >0))			-- operations virements crediteurs
	and TYPE_MTT = 'Virement SMS'
	group by format(DTE_OPE, 'yyyyMM') ,cpt.DWHCPTPPAL) ope_groupe
	where nb_op <> 0;

	INSERT INTO [dbo].[VDC_TYPE_OPERATION] (
		 [Date_operation] 
		,[Type_Op1] 
		,[Type_Op2] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope] 
	)
	select 	[date_operation],'NA' AS [Type_Op1], 'VIR_SMSREC' AS [Type_Op2]
		,SUM([Nb_Ope1]				 ) AS [Nb_Ope1]
		,SUM([Nb_Ope2] 			     ) AS [Nb_Ope2] 
		,SUM([Montant_en_k€] 		 ) AS [Montant_en_k€] 
		,SUM([Nb_moyen_Ope] 		 ) AS [Nb_moyen_Ope] 
		,SUM([Montant_moyen_Ope] 	 ) AS [Montant_moyen_Ope] 
		,SUM([Nb_médian_Ope] 		 ) AS [Nb_médian_Ope] 
		,SUM([Montant_médian_Ope] 	 ) AS [Montant_médian_Ope] 
	from #VDC_TYPE_OPERATION
	GROUP BY [date_operation];


	--========================= MOYENNE & TOTAL VIREMENT DEMANDE ARGENT ====================================
	TRUNCATE TABLE #VDC_TYPE_OPERATION;
	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
	)
	select 
	date_operation,
	count(distinct IDClientSAB) as 'Nb de clients ayant reçu une demande d''argent',
	sum(nb_op_cli) as 'Nb de virements reçus une demande d''argent',
	(-sum((mtt_tt_cli)/1000)) as 'Montant virements reçus une demande d''argent en k€',
	avg(cast(nb_op_cli as decimal(6,2))) as 'Nb moyen de virements reçus une demande d''argent',
	(-avg(mtt_tt_cli)) as 'Montant moyen de virements reçus une demande d''argent'
	from
	(select 
	format(DTE_OPE, 'yyyyMM') as date_operation
	,cpt.DWHCPTPPAL as IDClientSAB
	,sum(MTT_EUR) as mtt_tt_cli,

	sum(
		case 
			when COD_OPE in ('A0V', 'R0V') and MTT_EUR <0 then 1
			when COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR >0 then -1
	end) as nb_op_cli
	from 
		#TAB_MVT_CRED mouv
		inner join 
				#TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM
	where  ((COD_OPE in ('A0V', 'R0V') and MTT_EUR <0) OR (COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR >0))			-- operations virements crediteurs
	and TYPE_MTT = 'Demande argent'
	group by format(DTE_OPE, 'yyyyMM') ,cpt.DWHCPTPPAL) ope_groupe
	where nb_op_cli <> 0
	group by date_operation;

	INSERT INTO #VDC_TYPE_OPERATION (
		[Date_operation] 
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope])
	select distinct
	ope_groupe.Mois_ope,	
	PERCENTILE_CONT(0.5) within group (order by ope_groupe.nb_op) over (partition by Mois_ope)  'Nb médian de demandes d''argent reçus',
	(-PERCENTILE_CONT(0.5) within group (order by ope_groupe.mtt_tt) over (partition by Mois_ope)) 'Montant médian de demandes d''argent reçus'
	from
	(select 
	format(DTE_OPE, 'yyyyMM') as Mois_ope
	,cpt.DWHCPTPPAL as IDClientSAB
	,sum(MTT_EUR) as mtt_tt,
	sum(
		case 
			when COD_OPE in ('A0V', 'R0V') and MTT_EUR <0 then 1
			when COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR >0 then -1
	end) as nb_op
	from 
		#TAB_MVT_CRED mouv
		inner join 
				#TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM
	where ((COD_OPE in ('A0V', 'R0V') and MTT_EUR <0) OR (COD_OPE in ('A1V', 'A2V','R1V','R2V') and MTT_EUR >0))			-- operations virements crediteurs
	and TYPE_MTT = 'Demande argent'
	group by format(DTE_OPE, 'yyyyMM') ,cpt.DWHCPTPPAL) ope_groupe
	where nb_op <> 0;

	INSERT INTO [dbo].[VDC_TYPE_OPERATION] (
		 [Date_operation] 
		,[Type_Op1] 
		,[Type_Op2] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope] 
	)
	select 	[date_operation],'NA' AS [Type_Op1], 'VIR_DEM_AR' AS [Type_Op2]
		,SUM([Nb_Ope1]				 ) AS [Nb_Ope1]
		,SUM([Nb_Ope2] 			     ) AS [Nb_Ope2] 
		,SUM([Montant_en_k€] 		 ) AS [Montant_en_k€] 
		,SUM([Nb_moyen_Ope] 		 ) AS [Nb_moyen_Ope] 
		,SUM([Montant_moyen_Ope] 	 ) AS [Montant_moyen_Ope] 
		,SUM([Nb_médian_Ope] 		 ) AS [Nb_médian_Ope] 
		,SUM([Montant_médian_Ope] 	 ) AS [Montant_médian_Ope] 
	from #VDC_TYPE_OPERATION
	GROUP BY [date_operation];


	--========================= MOYENNE & TOTAL CHEQUE ENCAISSE ====================================
	TRUNCATE TABLE #VDC_TYPE_OPERATION;	
	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
	)
	select 
	date_operation,
	count(distinct IDClientSAB) as 'Nb de clients ayant encaissé un chèque',
	sum(nb_op_cli) as 'Nb chèques encaissés',
	cast(-sum(mtt_tt_cli)/1000 as decimal(10,2)) as 'Montant chèques encaissés en k€',
	avg(cast(nb_op_cli as decimal(6,2))) as 'Nb moyen de chèques encaissés',
	(-avg(mtt_tt_cli))as  'Montant moyen de chèques encaissés'

	from
	(select 
	format(DTE_OPE, 'yyyyMM') as date_operation
	,cpt.DWHCPTPPAL as IDClientSAB
	,sum(MTT_EUR) as mtt_tt_cli,

	sum(
		case 
			when COD_OPE in ('AI0') and MTT_EUR < 0 then 1
			when COD_OPE in ('RI1','AI0') and MTT_EUR > 0 then -1
	end) as nb_op_cli
	from 
		#TAB_MVT mouv
		inner join 
				#TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM

	where COD_OPE in ('AI0', 'RI1')			-- operations cheques encaisses
	group by format(DTE_OPE, 'yyyyMM') ,cpt.DWHCPTPPAL) ope_groupe
	where nb_op_cli <> 0
	group by date_operation;

	INSERT INTO #VDC_TYPE_OPERATION (
		[Date_operation] 
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope])
	select distinct
	ope_groupe.Mois_ope,	
	PERCENTILE_CONT(0.5) within group (order by ope_groupe.nb_op) over (partition by Mois_ope)  'Nb médian de chèques encaissés',
	(-PERCENTILE_CONT(0.5) within group (order by ope_groupe.mtt_tt) over (partition by Mois_ope))  'Montant médian de chèques encaissés'
	from
	(
	select
	format(DTE_OPE, 'yyyyMM')  Mois_ope,
	cpt.DWHCPTPPAL,
	sum(MTT_EUR) as mtt_tt,
	sum(case
			when COD_OPE = ('AI0') and MTT_EUR < 0 then 1
			when COD_OPE in ('RI1','AI0') and MTT_EUR > 0 then -1
	end) as nb_op
	from #TAB_MVT mouv
		inner join #TAB_CAV cpt on mouv.DWHCPTCOM = cpt.DWHCPTCOM
	where  
	 COD_OPE in ('AI0', 'RI1')			-- operations cheques encaisses
	group by format(DTE_OPE, 'yyyyMM') , cpt.DWHCPTPPAL) ope_groupe	
	where nb_op <> 0;

	INSERT INTO [dbo].[VDC_TYPE_OPERATION] (
		 [Date_operation] 
		,[Type_Op1] 
		,[Type_Op2] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope] 
	)
	select 	[date_operation],'NA' AS [Type_Op1], 'CH_ENC' AS [Type_Op2]
		,SUM([Nb_Ope1]				 ) AS [Nb_Ope1]
		,SUM([Nb_Ope2] 			     ) AS [Nb_Ope2] 
		,SUM([Montant_en_k€] 		 ) AS [Montant_en_k€] 
		,SUM([Nb_moyen_Ope] 		 ) AS [Nb_moyen_Ope] 
		,SUM([Montant_moyen_Ope] 	 ) AS [Montant_moyen_Ope] 
		,SUM([Nb_médian_Ope] 		 ) AS [Nb_médian_Ope] 
		,SUM([Montant_médian_Ope] 	 ) AS [Montant_médian_Ope] 
	from #VDC_TYPE_OPERATION
	GROUP BY [date_operation];


	/* ---------------------- ENCOURS & DECOUVERT ---------------------- */

	TRUNCATE TABLE #VDC_TYPE_OPERATION;	
	SELECT DWHCPTCPT,cpt.dwhcptdac,cpt.DWHCPTDAO 
	into #tab_cpt
	from
	[$(DataHubDatabaseName)].[dbo].[VW_PV_SA_Q_COMPTE] cpt
	INNER JOIN [$(DataHubDatabaseName)].[dbo].[PV_FE_EQUIPMENT] equ on equ.EQUIPMENT_NUMBER = cpt.DWHCPTCOM 
	inner join [$(DataHubDatabaseName)].[dbo].PV_FE_ENROLMENTFOLDER f on f.ID_FE_ENROLMENT_FOLDER = equ.ENROLMENT_FOLDER_ID
	inner join [$(DataHubDatabaseName)].[dbo].PV_SF_OPPORTUNITY o on o.Id_SF = f.SALESFORCE_OPPORTUNITY_ID and o.CommercialOfferCode__c = 'OC80' and o.StageName = '09'
	where DWHCPTRUB ='251180' 
	and cpt.DWHCPTDAO <= @DATE_REF
	and (cpt.dwhcptdac IS NULL or cpt.dwhcptdac > @DATE_REF)
	and cast(cpt.Validity_StartDate as date)<=@DATE_REF and cast(cpt.Validity_EndDate as date)>@DATE_REF;

	SELECT DWHCPTCPT,cpt.dwhcptdac,cpt.DWHCPTDAO 
	into #tab_cpt_prec
	from
	[$(DataHubDatabaseName)].[dbo].[VW_PV_SA_Q_COMPTE] cpt
	INNER JOIN [$(DataHubDatabaseName)].[dbo].[PV_FE_EQUIPMENT] equ on equ.EQUIPMENT_NUMBER = cpt.DWHCPTCOM 
	inner join [$(DataHubDatabaseName)].[dbo].PV_FE_ENROLMENTFOLDER f on f.ID_FE_ENROLMENT_FOLDER = equ.ENROLMENT_FOLDER_ID
	inner join [$(DataHubDatabaseName)].[dbo].PV_SF_OPPORTUNITY o on o.Id_SF = f.SALESFORCE_OPPORTUNITY_ID and o.CommercialOfferCode__c = 'OC80' and o.StageName = '09'
	where DWHCPTRUB ='251180' 
	and cpt.DWHCPTDAO <= @DATE_REF_PREC
	and (cpt.dwhcptdac IS NULL or cpt.dwhcptdac > @DATE_REF_PREC)
	and cast(cpt.Validity_StartDate as date)<=@DATE_REF_PREC and cast(cpt.Validity_EndDate as date)>@DATE_REF_PREC;


	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation]
		,[Montant_en_k€] 
		,[Montant_moyen_Ope]
		,[Nb_moyen_Ope]
	)
	select distinct @mois_ref as [Date_operation],
	(-sum(DWHCPTCPT)/1000) as 'Encours CAV (photo à date) en k€',
	sum((case when DWHCPTCPT > 0 then DWHCPTCPT else 0 end)/1000) as 'dont Montant total découvert en k€',
	sum(case when DWHCPTCPT > 0 then 1 else 0 end) as 'Nb de CAV à découvert'
	from #tab_cpt cpt
	where cpt.DWHCPTDAO <= @DATE_REF
	and (cpt.dwhcptdac IS NULL or cpt.dwhcptdac > @DATE_REF)
	union all
	select distinct  @mois_ref_prec as [Date_operation],
	(-sum(DWHCPTCPT)/1000) as 'Encours CAV (photo à date) en k€',
	sum ((case when DWHCPTCPT > 0 then DWHCPTCPT else 0 end)/1000) as 'dont Montant total découvert en k€',
	sum(case when DWHCPTCPT > 0 then 1 else 0 end) as 'Nb de CAV à découvert'
	from #tab_cpt_prec cpt
	where cpt.DWHCPTDAO <= @DATE_REF_PREC
	and (cpt.dwhcptdac IS NULL or cpt.dwhcptdac > @DATE_REF_PREC);

	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation]
		,[Nb_médian_Ope] 
	)
	select distinct @mois_ref as [Date_operation],
	(-PERCENTILE_CONT(0.5) within group (order by DWHCPTCPT) over ()) as 'Encours médian CAV'
	from #tab_cpt cpt
	where cpt.DWHCPTDAO <= @DATE_REF
	and (cpt.dwhcptdac IS NULL or cpt.dwhcptdac > @DATE_REF)
	union all
	select distinct @mois_ref_prec as [Date_operation],
	(-PERCENTILE_CONT(0.5) within group (order by DWHCPTCPT) over ()) as 'Encours médian CAV'
	from #tab_cpt_prec cpt
	where cpt.DWHCPTDAO <= @DATE_REF_PREC
	and (cpt.dwhcptdac IS NULL or cpt.dwhcptdac > @DATE_REF_PREC);

	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Montant_médian_Ope]
	)
	select  distinct @mois_ref as [Date_operation],
	(PERCENTILE_CONT(0.5) within group (order by DWHCPTCPT) over ()) as 'Montant médian découvert'
	from #tab_cpt cpt
	where cpt.DWHCPTCPT > 0
	and cpt.DWHCPTDAO <= @DATE_REF
	and (cpt.dwhcptdac IS NULL or cpt.dwhcptdac > @DATE_REF)
	union all
	select distinct  @mois_ref_prec as [Date_operation],
	(PERCENTILE_CONT(0.5) within group (order by DWHCPTCPT) over ()) as 'Montant médian découvert'
	from #tab_cpt_prec cpt 
	where cpt.DWHCPTCPT > 0
	and cpt.DWHCPTDAO <= @DATE_REF_PREC
	and (cpt.dwhcptdac IS NULL or cpt.dwhcptdac > @DATE_REF_PREC);

	INSERT INTO [dbo].[VDC_TYPE_OPERATION] (
		 [Date_operation] 
		,[Type_Op1] 
		,[Type_Op2] 	
		,[Montant_en_k€] 
		,[Nb_moyen_Ope] 
		,[Montant_moyen_Ope]
		,[Nb_médian_Ope] 
		,[Montant_médian_Ope] 
	)
	select 	[date_operation],'INF_GEN' AS [Type_Op1], 'ENC_DEC' AS [Type_Op2]
		,SUM([Montant_en_k€] 		 ) AS [Montant_en_k€] 
		,SUM([Nb_moyen_Ope] 		 ) AS [Nb_moyen_Ope] 
		,SUM([Montant_moyen_Ope] 	 ) AS [Montant_moyen_Ope] 
		,SUM([Nb_médian_Ope] 		 ) AS [Nb_médian_Ope] 
		,SUM([Montant_médian_Ope] 	 ) AS [Montant_médian_Ope] 
	from #VDC_TYPE_OPERATION
	GROUP BY [date_operation];




	-----   Infos générales   ----

	TRUNCATE TABLE #VDC_TYPE_OPERATION;
	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Nb_Ope]
	)
	select format(cast(ClosedDate as date),'yyyyMM'),count(*) as 'Nb de mobilités bancaires abouties'
	from [$(DataHubDatabaseName)].[dbo].PV_SF_CASE demande
	where 
	(SubType__c = '159' -- Domiciliation Bancaire
	or SubType__c = '186') -- Mobilité Bancaire)
	and Type = '02' -- Compte
	and Status = '03' -- Fermée
	and format(cast(ClosedDate as date),'yyyyMM') in(@mois_ref,@mois_ref_prec)
	group by format(cast(ClosedDate as date),'yyyyMM');

	/* Ouverture de comptes CAV */
	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Nb_Ope1]
	)
	select format(DWHCPTDAO,'yyyyMM'), count(distinct cpt.DWHCPTCOM) as 'Nombre ouvertures de compte' 
	from #TAB_CAV cpt	         
	where  format(DWHCPTDAO,'yyyyMM') in(@mois_ref,@mois_ref_prec)
	group by format(DWHCPTDAO,'yyyyMM')



	/* Cloture de comptes CAV */
	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Nb_Ope2]
	)
	select format(dwhcptdac,'yyyyMM'), count(distinct cpt.DWHCPTCOM) as 'Nombre clôtures de compte' from 
		#TAB_CAV cpt        
	where  format(dwhcptdac,'yyyyMM') in(@mois_ref,@mois_ref_prec)
	group by format(dwhcptdac,'yyyyMM')


	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Nb_Ope3]
	)
	select @mois_ref as [Date_operation],
	count(*) as 'Nb de clients facturables (<3 opés)'
	from
	(
	select CPT.DWHCPTCOM
	from #TAB_CAV cpt
		 inner join [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_MOUVEMENT] m1 on m1.NUM_CPT = cpt.DWHCPTCOM
	where DWHCPTDAO < cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF) + 1, 0) as date) and 
		(DWHCPTDAC IS NULL or DWHCPTDAC >= cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF) + 1, 0) as date) )
		and COD_OPE = 'FAC' and COD_EVE = 'FAC'
		and DTE_OPE >= cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF) + 1, 0) as date) -- 1er jour M+1
		and DTE_OPE <= cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF) + 2, -1) as date)  -- dernier jour M+1 -->selection des operations du mois M facturées à M+1
		and ltrim(rtrim(substring(DON_ANA,37,7))) = 'FBAG80'
	GROUP BY CPT.DWHCPTCOM
	having sum(MTT_EUR) > 0   -- pour exclure les régularisations
	) a;

	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Nb_Ope3]
	)
	select @mois_ref_prec as [Date_operation],
	count(*) as 'Nb de clients facturables (<3 opés)'
	from
	(
	select distinct
	CPT.DWHCPTCOM
	from #TAB_CAV cpt
		 inner join [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_MOUVEMENT] m1 on m1.NUM_CPT = cpt.DWHCPTCOM
	where DWHCPTDAO < cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF_PREC) + 1, 0) as date) and 
		(DWHCPTDAC IS NULL or DWHCPTDAC >= cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF_PREC) + 1, 0) as date) )
		and COD_OPE = 'FAC' and COD_EVE = 'FAC'
		and DTE_OPE >= cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF_PREC) + 1, 0) as date) -- 1er jour M+1
		and DTE_OPE <= cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF_PREC) + 2, -1) as date)  -- dernier jour M+1 -->selection des operations du mois M facturées à M+1
		and ltrim(rtrim(substring(DON_ANA,37,7))) = 'FBAG80'
	GROUP BY CPT.DWHCPTCOM
	having sum(MTT_EUR) > 0   -- pour exclure les régularisations
	) a;


	--  Nombre de clients en stock


	if OBJECT_ID('tempdb..#TAB_CPT_OBK') IS NOT NULL
	BEGIN
		DROP TABLE #TAB_CPT_OBK
	END  
	CREATE TABLE #TAB_CPT_OBK
	(
		DWHCPTPPAL [VARCHAR](7),
		DWHCPTCOM [VARCHAR](20),
		DWHCPTRUB [VARCHAR](10),
		DWHCPTDAO DATE,
		DAT_CLOT_CPT DATE
	)

	-- Alimentation CAV

	insert into #TAB_CPT_OBK (DWHCPTPPAL,DWHCPTCOM,DWHCPTRUB,DWHCPTDAO,DAT_CLOT_CPT)
	(
	select DWHCPTPPAL, DWHCPTCOM,DWHCPTRUB,DWHCPTDAO,DWHCPTDAC
	from
		(
		select 
			DWHCPTPPAL, DWHCPTCOM,DWHCPTRUB,DWHCPTDAO,DWHCPTDAC, row_number() over (partition by DWHCPTCOM order by Validity_StartDate desc) as Validity_Flag
		from 
			[$(DataHubDatabaseName)].[dbo].[VW_PV_SA_Q_COMPTE]
		where DWHCPTRUB = ('251180')
		) cpt 	
		INNER JOIN [$(DataHubDatabaseName)].[dbo].[PV_FE_EQUIPMENT] equ on equ.EQUIPMENT_NUMBER = cpt.DWHCPTCOM 
		inner join [$(DataHubDatabaseName)].[dbo].PV_FE_ENROLMENTFOLDER f on f.ID_FE_ENROLMENT_FOLDER = equ.ENROLMENT_FOLDER_ID
		inner join [$(DataHubDatabaseName)].[dbo].PV_SF_OPPORTUNITY o on o.Id_SF = f.SALESFORCE_OPPORTUNITY_ID and o.CommercialOfferCode__c = 'OC80' and o.StageName = '09'
	where cpt.Validity_Flag = 1 and  cpt.DWHCPTDAO <= @DATE_REF
	);

	-- Alimentation CSL

	insert into #TAB_CPT_OBK (DWHCPTPPAL,DWHCPTCOM,DWHCPTRUB,DWHCPTDAO,DAT_CLOT_CPT)
	(
	select DWHCPTPPAL, DWHCPTCOM,DWHCPTRUB,DWHCPTDAO,DWHCPTDAC
	from
		(
		select 
			DWHCPTPPAL, DWHCPTCOM,DWHCPTRUB,DWHCPTDAO,DWHCPTDAC, row_number() over (partition by DWHCPTCOM order by Validity_StartDate desc) as Validity_Flag
		from 
			[$(DataHubDatabaseName)].[dbo].[VW_PV_SA_Q_COMPTE]
		where DWHCPTRUB = ('254111')
		) cpt 
		INNER JOIN [$(DataHubDatabaseName)].[dbo].[PV_FE_EQUIPMENT] equ on equ.EQUIPMENT_NUMBER = cpt.DWHCPTCOM 
		inner join [$(DataHubDatabaseName)].[dbo].PV_FE_ENROLMENTFOLDER f on f.ID_FE_ENROLMENT_FOLDER = equ.ENROLMENT_FOLDER_ID
		inner join [$(DataHubDatabaseName)].[dbo].PV_SF_OPPORTUNITY o on o.Id_SF = f.SALESFORCE_OPPORTUNITY_ID and o.CommercialOfferCode__c = 'L80' and o.StageName = '09'
	where cpt.Validity_Flag = 1 and  cpt.DWHCPTDAO <= @DATE_REF
	);


	-- Alimentation Prêt Personnel
	insert into #TAB_CPT_OBK (DWHCPTPPAL,DWHCPTCOM,DWHCPTRUB,DWHCPTDAO,DAT_CLOT_CPT)
	(
	select DWHCPTPPAL, DWHCPTCOM,DWHCPTRUB,DWHCPTDAO,d.DTE_FIN_PRE
	from
		(
		select 
			DWHCPTPPAL, DWHCPTCOM,DWHCPTRUB,DWHCPTDAO,DWHCPTDAC, row_number() over (partition by DWHCPTCOM order by Validity_StartDate desc) as Validity_Flag
		from 
			[$(DataHubDatabaseName)].[dbo].[VW_PV_SA_Q_COMPTE]	WITH(NOLOCK)
		where DWHCPTRUB = '203120'
		) cpt 	
		INNER JOIN [$(DataHubDatabaseName)].[dbo].[PV_FE_EQUIPMENT] equ	WITH(NOLOCK) on equ.EQUIPMENT_NUMBER = cpt.DWHCPTCOM 
		inner join [$(DataHubDatabaseName)].[dbo].PV_FE_ENROLMENTFOLDER f	WITH(NOLOCK) on f.ID_FE_ENROLMENT_FOLDER = equ.ENROLMENT_FOLDER_ID
		INNER JOIN [$(DataHubDatabaseName)].[dbo].pv_sa_q_com com	WITH(NOLOCK) on cpt.DWHCPTCOM = com.COMREFCOM
		inner join [$(DataHubDatabaseName)].[dbo].PV_FI_Q_DES d	WITH(NOLOCK) on d.ref_cpt = com.COMREFREF and d.CAT_PRE = 'NFOB'  and d.DTE_REA IS NOT NULL
	where 
		cpt.Validity_Flag = 1 and  cpt.DWHCPTDAO <= @DATE_REF
	);

	-- Alimentation crédit affecté
	insert into #TAB_CPT_OBK (DWHCPTPPAL,DWHCPTCOM,DWHCPTRUB,DWHCPTDAO,DAT_CLOT_CPT)
	(
	select DWHCPTPPAL, DWHCPTCOM,DWHCPTRUB,DWHCPTDAO,d.DTE_FIN_PRE
	from
		(
		select 
			DWHCPTPPAL, DWHCPTCOM,DWHCPTRUB,DWHCPTDAO,DWHCPTDAC, row_number() over (partition by DWHCPTCOM order by Validity_StartDate desc) as Validity_Flag
		from 
			[$(DataHubDatabaseName)].[dbo].[VW_PV_SA_Q_COMPTE]	WITH(NOLOCK)
		where DWHCPTRUB = '203110'
		) cpt 	
		INNER JOIN [$(DataHubDatabaseName)].[dbo].[PV_FE_EQUIPMENT] equ	WITH(NOLOCK) on equ.EQUIPMENT_NUMBER = cpt.DWHCPTCOM 
		inner join [$(DataHubDatabaseName)].[dbo].PV_FE_ENROLMENTFOLDER f	WITH(NOLOCK) on f.ID_FE_ENROLMENT_FOLDER = equ.ENROLMENT_FOLDER_ID
		INNER JOIN [$(DataHubDatabaseName)].[dbo].pv_sa_q_com com	WITH(NOLOCK) on cpt.DWHCPTCOM = com.COMREFCOM
		inner join [$(DataHubDatabaseName)].[dbo].PV_FI_Q_DES d	WITH(NOLOCK) on d.ref_cpt = com.COMREFREF and d.DTE_REA IS NOT NULL
	where 
		cpt.Validity_Flag = 1 and  cpt.DWHCPTDAO <= @DATE_REF
	);
	-- Requête
	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Nb_Ope4]
	)
	select @mois_ref as Date_operation,
	count(stk_cli) as 'Nombre de clients en stock'
	from
		(
		select  DWHCPTPPAL stk_cli, max(dte_clo) cli_clo 
		from 
		(select 
			DWHCPTPPAL, isnull(DAT_CLOT_CPT, '9999-12-31') dte_clo, DWHCPTCOM, DWHCPTDAO
		  from 
			#TAB_CPT_OBK) a

	group by DWHCPTPPAL
	) stk where cli_clo >@DATE_REF
	union all
	select @mois_ref_prec as Date_operation,
	count(stk_cli) as 'Nombre de clients en stock'
	from
		(
		select  DWHCPTPPAL stk_cli, max(dte_clo) cli_clo 
		from 
		(select 
			DWHCPTPPAL, isnull(DAT_CLOT_CPT, '9999-12-31') dte_clo, DWHCPTCOM, DWHCPTDAO
		  from 
			#TAB_CPT_OBK WHERE DWHCPTDAO<= @DATE_REF_PREC) a

	group by DWHCPTPPAL
	) stk where cli_clo >@DATE_REF_PREC;



	INSERT INTO [dbo].[VDC_TYPE_OPERATION] (
		 [Date_operation] 
		,[Type_Op1] 
		,[Type_Op2] 
		,[Nb_Ope] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Nb_Ope3]
		,[Nb_Ope4]
	)
	select 	[date_operation],'INF_GEN' AS [Type_Op1], 'INF_GEN' AS [Type_Op2]
		,SUM([Nb_Ope]				 ) AS [Nb_Ope]
		,SUM([Nb_Ope1]				 ) AS [Nb_Ope1]
		,SUM([Nb_Ope2] 			     ) AS [Nb_Ope2] 
		,SUM([Nb_Ope3] 				 ) AS [Nb_Ope3] 
		,SUM([Nb_Ope4] 		         ) AS [Nb_Ope4] 
	from #VDC_TYPE_OPERATION
	GROUP BY [date_operation];




	--/* Comptage CAV & CSL */
	select DWHCPTCOM,DWHCPTRUB,DWHCPTDAO,DWHCPTDAC
	into #Cpt_cav_csl
	from 
		(select DWHCPTPPAL, DWHCPTCOM, DWHCPTRUB, DWHCPTDAO, DWHCPTDAC, row_number() over (partition by DWHCPTCOM order by Validity_StartDate desc) as Validity_Flag
		from [$(DataHubDatabaseName)].[dbo].[VW_PV_SA_Q_COMPTE]
		where DWHCPTRUB IN ('251180','254111')) cpt 
		INNER JOIN [$(DataHubDatabaseName)].[dbo].[PV_FE_EQUIPMENT] equ on equ.EQUIPMENT_NUMBER = cpt.DWHCPTCOM 
		inner join [$(DataHubDatabaseName)].[dbo].PV_FE_ENROLMENTFOLDER f on f.ID_FE_ENROLMENT_FOLDER = equ.ENROLMENT_FOLDER_ID
		inner join [$(DataHubDatabaseName)].[dbo].PV_SF_OPPORTUNITY o on o.Id_SF = f.SALESFORCE_OPPORTUNITY_ID and o.CommercialOfferCode__c in ('L80','OC80') and o.StageName = '09'
	where cpt.Validity_Flag = 1
	and cpt.DWHCPTDAO <= @DATE_REF
	and (cpt.DWHCPTDAC IS NULL or cpt.DWHCPTDAC >@DATE_REF_PREC);

	TRUNCATE TABLE #VDC_TYPE_OPERATION;
	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Nb_Ope]
	)
	select format(@DATE_REF,'yyyyMM') as Date_operation, count(distinct DWHCPTCOM) as StockProduitsCAV
	from #Cpt_cav_csl cpt where cpt.DWHCPTRUB ='251180' and cpt.DWHCPTDAO <= @DATE_REF and (cpt.DWHCPTDAC IS NULL or cpt.DWHCPTDAC >@DATE_REF)
	union all
	select format(@DATE_REF_PREC,'yyyyMM') as Date_operation, count(distinct DWHCPTCOM) as StockProduitsCAV
	from #Cpt_cav_csl cpt where cpt.DWHCPTRUB ='251180' and cpt.DWHCPTDAO <= @DATE_REF_PREC and (cpt.DWHCPTDAC IS NULL or cpt.DWHCPTDAC >@DATE_REF_PREC);

	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Nb_Ope1]
	)
	select format(@DATE_REF,'yyyyMM') as Date_operation, count(distinct DWHCPTCOM) as StockProduitsCSL
	from #Cpt_cav_csl cpt where cpt.DWHCPTRUB ='254111' and cpt.DWHCPTDAO <= @DATE_REF and (cpt.DWHCPTDAC IS NULL or cpt.DWHCPTDAC >@DATE_REF)
	union all
	select format(@DATE_REF_PREC,'yyyyMM') as Date_operation, count(distinct DWHCPTCOM) as StockProduitsCSL
	from #Cpt_cav_csl cpt where cpt.DWHCPTRUB ='254111' and cpt.DWHCPTDAO <= @DATE_REF_PREC and (cpt.DWHCPTDAC IS NULL or cpt.DWHCPTDAC >@DATE_REF_PREC);


	/* Comptage Credits Consommation */

	select des.DTE_ACC_TOKOS ,des.DTE_REA ,des.DTE_PRE_ECH,des.ref_cpt,DTE_FIN_PRE
	into  #cpt_Credits
	from 
		[$(DataHubDatabaseName)].[dbo].PV_FI_Q_DES des 	WITH(NOLOCK)
		inner JOIN [$(DataHubDatabaseName)].[dbo].PV_SA_Q_COM com	WITH(NOLOCK) on com.comrefref = des.REF_CPT
		inner join (
		select DWHCPTPPAL, DWHCPTCOM, DWHCPTRUB, DWHCPTDAO, DWHCPTDAC, row_number() over (partition by DWHCPTCOM order by Validity_StartDate desc) as Validity_Flag
		from [$(DataHubDatabaseName)].[dbo].[VW_PV_SA_Q_COMPTE]	WITH(NOLOCK)
		where DWHCPTRUB = ('203120')
		) c on c.DWHCPTCOM = com.COMREFCOM and c.Validity_Flag = 1
		inner join [$(DataHubDatabaseName)].[dbo].PV_FE_EQUIPMENT e	WITH(NOLOCK) on e.EQUIPMENT_NUMBER = c.DWHCPTCOM
		inner join [$(DataHubDatabaseName)].[dbo].PV_FE_ENROLMENTFOLDER f	WITH(NOLOCK) on f.ID_FE_ENROLMENT_FOLDER = e.ENROLMENT_FOLDER_ID
	where 
		CAT_PRE = 'NFOB'
		and DTE_ACC_TOKOS <= @DATE_REF				
		and isnull(DTE_FIN_PRE,'9999-12-31') > @DATE_REF_PREC;


	

	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Nb_Ope2]
	)
	select format(@DATE_REF,'yyyyMM') as Date_operation, count(distinct des.ref_cpt) as 'Nb crédits accordés'
	from #cpt_Credits des 
	where des.DTE_ACC_TOKOS <= @DATE_REF and isnull(DTE_FIN_PRE,'9999-12-31') > @DATE_REF
	and  (des.DTE_REA IS NULL or des.DTE_REA > @DATE_REF) 
	union all
	select format(@DATE_REF_PREC,'yyyyMM') as Date_operation, count(distinct des.ref_cpt) as 'Nb crédits accordés'
	from #cpt_Credits des 
	where des.DTE_ACC_TOKOS <= @DATE_REF_PREC and isnull(DTE_FIN_PRE,'9999-12-31') > @DATE_REF_PREC
	and  (des.DTE_REA IS NULL or des.DTE_REA > @DATE_REF_PREC) ;

	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Nb_Ope3]
	)		
	select format(@DATE_REF,'yyyyMM') as Date_operation, count(distinct des.ref_cpt) as 'Nb crédits décaissés'
	from #cpt_Credits des 
	where des.DTE_ACC_TOKOS <= @DATE_REF and isnull(DTE_FIN_PRE,'9999-12-31') > @DATE_REF
	and  des.DTE_REA <= @DATE_REF and  des.DTE_REA <= des.DTE_PRE_ECH 
	union all
	select format(@DATE_REF_PREC,'yyyyMM') as Date_operation, count(distinct des.ref_cpt) as 'Nb crédits décaissés'
	from #cpt_Credits des 
	where des.DTE_ACC_TOKOS <= @DATE_REF_PREC and isnull(DTE_FIN_PRE,'9999-12-31') > @DATE_REF_PREC
	and  des.DTE_REA <= @DATE_REF_PREC and  des.DTE_REA <= des.DTE_PRE_ECH ;


	INSERT INTO [dbo].[VDC_TYPE_OPERATION] (
		 [Date_operation] 
		,[Type_Op1] 
		,[Type_Op2] 
		,[Nb_Ope] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Nb_Ope3]
	)
	select 	[date_operation],'INF_GEN' AS [Type_Op1], 'STK_PRD' AS [Type_Op2]
		,SUM([Nb_Ope]				 ) AS [Nb_Ope]
		,SUM([Nb_Ope1]				 ) AS [Nb_Ope1]
		,SUM([Nb_Ope2] 			     ) AS [Nb_Ope2] 
		,SUM([Nb_Ope3] 		 ) AS [Nb_Ope3] 
	from #VDC_TYPE_OPERATION
	GROUP BY [date_operation];



	/* Ouverture de comptes CAV par distributeur */
	TRUNCATE TABLE #VDC_TYPE_OPERATION;
	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		 , [Nb_Ope]
		 , [Nb_Ope4]
		)
	select [Date_operation],[Nb_Ope1], [Nb_Ope3] from [dbo].[VDC_TYPE_OPERATION] 
	where [Type_Op1]='INF_GEN' and [Type_Op2]='INF_GEN'  and [Date_operation]= @mois_ref;

	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		,[Nb_Ope1]
		,[Nb_Ope2]
		,[Nb_Ope3]
		)select Date_Operation,
			case when DistributorNetwork__c = '01' then DWHCPTCOM end as 'Nombre ouvertures Orange France',
			case when DistributorNetwork__c = '02'then DWHCPTCOM end as 'Nombre ouvertures Orange Bank',
			case when DistributorNetwork__c = '03' then DWHCPTCOM end as 'Nombre ouvertures Groupama'
	from (
	select 
		format(DWHCPTDAO,'yyyyMM') as Date_Operation, opp.DistributorNetwork__c ,count(distinct cpt.DWHCPTCOM) as DWHCPTCOM 
	from #TAB_CAV cpt 
		inner join [$(DataHubDatabaseName)].[dbo].VW_PV_SF_OPPORTUNITY_HISTORY opp on opp.Id_SF = cpt.SALESFORCE_OPPORTUNITY_ID
	where  opp.DistributorNetwork__c in('01','02','03') and
		format(DWHCPTDAO,'yyyyMM') = @mois_ref and
		cast(opp.Validity_EndDate as date) > @DATE_REF and cast(opp.Validity_StartDate as date) <= @DATE_REF
	group by 
		format(DWHCPTDAO,'yyyyMM'), opp.DistributorNetwork__c
	) as t;

	insert into #tab_deja_facture (num_cpt, mtt_total)
	select distinct
		mouv.NUM_CPT as num_cpt,
		sum(mouv.MTT_EUR) as mtt_total
	from 
		[$(DataHubDatabaseName)].[dbo].[PV_SA_Q_MOUVEMENT] mouv
		inner join #TAB_CAV cpt on mouv.NUM_CPT = cpt.DWHCPTCOM
	where 
	 cpt.DWHCPTDAO < cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF) + 1, 0) as date) 
		and (cpt.DWHCPTDAC IS NULL or DWHCPTDAC >= cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF) + 1, 0) as date) )
		and mouv.COD_OPE = 'FAC' and mouv.COD_EVE = 'FAC'
		and DTE_OPE < cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF) + 1, 0) as date) -- 1er jour M+1
		and ltrim(rtrim(substring(mouv.DON_ANA,37,7))) = 'FBAG80'
	group by 	
		mouv.NUM_CPT
	having 
		sum(mouv.MTT_EUR)	> 0;		-- pour exclure les régularisations			

	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		 , [Nb_Ope_France]
		)
	select @mois_ref as [Date_operation],
	count(distinct a.compte) as 'Nombre de clients facturés pour la 1ère fois'
	from
	(select 
		mouv.NUM_CPT as compte,
		sum(mouv.MTT_EUR) as montant,
		case when fac.num_cpt IS NULL then '1' else '0' end as 'Top 1ère facturation'
	from 
		[$(DataHubDatabaseName)].[dbo].[PV_SA_Q_MOUVEMENT] mouv
		inner join #TAB_CAV cpt on mouv.NUM_CPT = cpt.DWHCPTCOM
		LEFT OUTER JOIN #tab_deja_facture fac on fac.num_cpt = mouv.NUM_CPT
	where 	
		mouv.COD_OPE = 'FAC' and mouv.COD_EVE = 'FAC'
		and mouv.DTE_OPE >= cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF) + 1, 0) as date) -- 1er jour M+1
		and mouv.DTE_OPE <= cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF) + 2, -1) as date)  -- dernier jour M+1 -->selection des operations du mois M facturées à M+1
		and ltrim(rtrim(substring(mouv.DON_ANA,37,7))) = 'FBAG80'
		and cpt.DWHCPTDAO < cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF) + 1, 0) as date) 
		and (cpt.DWHCPTDAC IS NULL or DWHCPTDAC >= cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF) + 1, 0) as date) )
	GROUP BY 
		mouv.NUM_CPT, case when fac.num_cpt IS NULL then '1' else '0' end
	having 
		sum(MTT_EUR) > 0 -- pour exclure les régularisations							
	) a
	where a.[Top 1ère facturation] = 1;

	insert into #tab_facture_M_1 (num_cpt, mtt_total)
	select 
		mouv.NUM_CPT as num_cpt,
		sum(mouv.MTT_EUR) as mtt_total
	from 
		[$(DataHubDatabaseName)].[dbo].[PV_SA_Q_MOUVEMENT] mouv
		inner join #TAB_CAV cpt on mouv.NUM_CPT = cpt.DWHCPTCOM
	where  mouv.COD_OPE = 'FAC' and mouv.COD_EVE = 'FAC'
		and DWHCPTDAO < cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF) + 1, 0) as date)
		and (DWHCPTDAC IS NULL or DWHCPTDAC >= cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF) + 1, 0) as date) )
		and mouv.DTE_OPE >= cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF), 0) as date) -- 1er jour M-1
		and mouv.DTE_OPE <= cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF) + 1, -1) as date)  -- dernier jour M-1 -->selection des operations du mois M-1 facturées à M
		and ltrim(rtrim(substring(mouv.DON_ANA,37,7))) = 'FBAG80'
	group by 	
		mouv.NUM_CPT
	having 
		sum(mouv.MTT_EUR) > 0;

	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		 ,[Nb_Ope_ZoneEuro]
		)
	select  @mois_ref as [Date_operation],count(a.compte) as 'Nombre de clients facturés 2 fois consécutives'
	from
	(
	select 
	mouv.NUM_CPT as compte,
	sum(MTT_EUR) as montant
	from [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_MOUVEMENT] mouv  
	INNER JOIN #tab_facture_M_1 fac on fac.num_cpt = mouv.NUM_CPT
	where 	
		COD_OPE = 'FAC' and COD_EVE = 'FAC'
		and DTE_OPE >= cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF) + 1, 0) as date) -- 1er jour M+1
		and DTE_OPE <= cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF) + 2, -1) as date)  -- dernier jour M+1 -->selection des operations du mois M facturées à M+1
		and ltrim(rtrim(substring(DON_ANA,37,7))) = 'FBAG80'
	GROUP BY 
		mouv.NUM_CPT
	having 
		sum(MTT_EUR) > 0 -- pour exclure les régularisations
	) a;

	--Montant moyen du découvert 
	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] 
		 ,[Nb_Ope5]
		)
	select [Date_operation],(1000*Montant_moyen_Ope)/Nb_moyen_Ope  as [Nb_Ope5]  from [dbo].[VDC_TYPE_OPERATION] 
	where [Type_Op1]='INF_GEN' and [Type_Op2]='ENC_DEC' and [Date_operation]= @mois_ref;

	INSERT INTO [dbo].[VDC_TYPE_OPERATION] (
		 [Date_operation] 
		,[Type_Op1] 
		,[Type_Op2] 
		,[Nb_Ope] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Nb_Ope3]
		,[Nb_Ope4]
		,[Nb_Ope5]
		,[Nb_Ope_France]
		,[Nb_Ope_ZoneEuro]
	)
	select 	[date_operation],'KPI_GEN' AS [Type_Op1], 'KPI_1' AS [Type_Op2]
		,SUM([Nb_Ope]				 ) AS [Nb_Ope]
		,SUM([Nb_Ope1]				 ) AS [Nb_Ope1]
		,SUM([Nb_Ope2] 			     ) AS [Nb_Ope2] 
		,SUM([Nb_Ope3] 				 ) AS [Nb_Ope3] 
		,SUM([Nb_Ope4] 				 ) AS [Nb_Ope4] 
		,SUM([Nb_Ope5] 				 ) AS [Nb_Ope5] 
		,SUM([Nb_Ope_France] 	     ) AS [Nb_Ope_France] 
		,SUM([Nb_Ope_ZoneEuro] 	     ) AS [Nb_Ope_ZoneEuro] 
	from #VDC_TYPE_OPERATION
	GROUP BY [date_operation];

	/*
	--Montant moyen du découvert 
	select (1000*Montant_moyen_Ope)/Nb_moyen_Ope   from [dbo].[VDC_TYPE_OPERATION] 
	where [Type_Op1]='INF_GEN' and [Type_Op2]='ENC_DEC' and [Date_operation]= @mois_ref;

	*/


	-- Alimentation des tables temporaires
	insert into #TAB_MVT_ACT (DWHCPTCOM,DWHCPTPPAL,DWHCPTRUB ,DTE_OPE ,COD_OPE ,COD_EVE ,DON_ANA ,COD_ANA ,MTT_EUR )
	(
	select
	DWHCPTCOM,DWHCPTPPAL,DWHCPTRUB ,DTE_OPE ,COD_OPE ,COD_EVE ,DON_ANA ,COD_ANA ,MTT_EUR
	from
		#TAB_CAV cpt 
		left join [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_MOUVEMENT] m1 WITH(NOLOCK) on m1.NUM_CPT = cpt.DWHCPTCOM --and format(DTE_OPE,'yyyyMM') = @dern_mois 
	where 
	(
	COD_OPE in ('A0P', 'R0P','A1P', 'A2P', 'R1P', 'R2P','A0V', 'R0V','A1V', 'A2V','R1V','R2V','AI1','RI0','AI0','RI1','ERE','CTB') )
	and m1.DTE_OPE >=  cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF_act) - 3, 0) as date) -- 1er jour M-3
	and m1.DTE_OPE <= cast(DATEADD(DAY, -1, DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF_act), 0)) as date) -- dernier jour M-1 -->selection des operations des 3 derniers mois
	);

	insert into #TAB_MVT_ACT_PREC (DWHCPTCOM,DWHCPTPPAL,DWHCPTRUB ,DTE_OPE ,COD_OPE ,COD_EVE ,DON_ANA ,COD_ANA ,MTT_EUR )
	(
	select
	DWHCPTCOM,DWHCPTPPAL,DWHCPTRUB ,DTE_OPE ,COD_OPE ,COD_EVE ,DON_ANA ,COD_ANA ,MTT_EUR
	from
		#TAB_CAV cpt 
		left outer join [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_MOUVEMENT] m1 WITH(NOLOCK) on m1.NUM_CPT = cpt.DWHCPTCOM --and format(DTE_OPE,'yyyyMM') = @dern_mois 
	where 
	(
	COD_OPE in ('A0P', 'R0P','A1P', 'A2P', 'R1P', 'R2P','A0V', 'R0V','A1V', 'A2V','R1V','R2V','AI1','RI0','AI0','RI1','ERE','CTB') )
	and m1.DTE_OPE >=  cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF_act_prec) - 3, 0) as date) -- 1er jour M-3
	and m1.DTE_OPE <= cast(DATEADD(DAY, -1, DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF_act_prec), 0)) as date) -- dernier jour M-1 -->selection des operations des 3 derniers mois
	);


	insert into #tab_activite (num_cpt, id_client, cod_rub, Nv_Activite_cav,Tranche_Activite_cav)
	select    
		DWHCPTCOM as num_cpt,
		IDClientSAB as id_client,
		DWHCPTRUB as cod_rub,
		(case
			when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl >= 30 AND Flg_R = 'A') then 'Actif++'
			when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  >= 12  and nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  <= 29  AND  Flg_R = 'A') then 'Actif+'
			when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  >= 1  and nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  <= 11  AND  Flg_R = 'A') then 'Quasi_Actif'
			when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl   < 1  AND  Flg_R = 'A') then 'Inactif'
			when  Flg_R = 'R' then 'Recent'	
			when  Flg_R = 'C' then 'Clos du mois'	
			else 'AUTRE' end
		) as Nv_Activite_cav
		,
		(case
		 when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl >= 90 ) then 'Tranche 90 et +'
		 when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  >= 75  and nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  <= 89) then 'Tranche 75-89'
		 when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  >= 60  and nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  <= 74) then 'Tranche 60-74'
		 when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  >= 45  and nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  <= 59) then 'Tranche 45-59'
		 when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  >= 30  and nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  <=44) then 'Tranche 30-44'
		 when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  >= 6  and nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  <= 29) then 'Tranche 6-29'
		 when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  >= 1  and nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  <= 5) then 'Tranche 1-5'
		 when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  < 1) then 'Tranche 0'
		 else 'AUTRE TRANCHE'
		end) as Tranche_Activite_cav
	from 
		(
		select
			cpt.DWHCPTCOM
			,cpt.DWHCPTPPAL as IDClientSAB
			,cpt.DWHCPTDAO as date_ouv
			,cpt.DWHCPTDAC as date_clo
			, cpt.DWHCPTRUB 
			,SUM(case when [COD_OPE] in ('A0P', 'R0P') and MTT_EUR >0 then 1 else 0 end) nb_prel
			,SUM(case when [COD_OPE] in ('A0P', 'R0P','A1P', 'A2P', 'R1P', 'R2P') and MTT_EUR <0 then 1 else 0 end) nb_prel_rej
			,SUM(case when [COD_OPE] in ('A0V', 'R0V') and MTT_EUR >0 then 1 else 0 end) nb_vir_debit
			,sum(case when [COD_OPE] in ('A1V', 'A2V','R1V','R2V') and MTT_EUR <0 then 1 else 0 end) nb_vir_deb_rej
			,SUM(case when [COD_OPE] ='CTB' and MTT_EUR > 0 then 1 else 0 end) as Nb_paiement_carte
			,SUM(case when [COD_OPE] ='CTB' and substring(COD_ANA,5,2) = '17' and MTT_EUR < 0 then 1 else 0 end) as Nb_paiement_carte_rej
			,sum(case when COD_OPE = 'RI0' and MTT_EUR > 0 then 1 else 0 end) nb_chq_emis
			,sum(case when COD_OPE IN ('AI1','RI0') and MTT_EUR < 0 then 1 else 0 end) nb_chq_emis_rej
			,sum(case when COD_OPE = 'ERE' and COD_EVE = 'OUV' then 1 else 0 end) nb_1er_ver_csl
			, case 
				when(DWHCPTDAO >= cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF_act) - 3, 0) as date) and (DWHCPTDAC IS NULL OR DWHCPTDAC > cast(DATEADD(DAY, -1, DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF_act), 0)) as date))) then 'R'		-- Client récent
				when(DWHCPTDAC >= cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF_act) - 1, 0) as date) and DWHCPTDAC <= cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF_act), -1) as date)) then 'C'												-- Client clos du mois
				else 'A' 
				end Flg_R
		from 
				#TAB_CAV cpt 
					left outer join #TAB_MVT_ACT mouv  on mouv.DWHCPTCOM = cpt.DWHCPTCOM 
														and COD_OPE in ('A0P', 'R0P', 'A0V', 'R0V','CTB', 'RI0', 'AI1', 'A1V', 'A2V','ERE','A1P', 'A2P', 'R1P', 'R2P','R1V','R2V') 
														and DTE_OPE >=  cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF_act) - 3, 0) as date) -- 1er jour M-3
														and DTE_OPE <= cast(DATEADD(DAY, -1, DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF_act), 0)) as date) -- dernier jour M-1 -->selection des operations des 3 derniers mois
		where 
			DWHCPTDAO < @DATE_REF_act	
			and (DWHCPTDAC IS NULL OR (DWHCPTDAC >= cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF_act) - 1, 0) as date) and DWHCPTDAC <= cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF_act), -1) as date)) OR cpt.DWHCPTDAC >= @DATE_REF_act)								-- compte de plus de 3 mois et non clos a la date de reference	
		group by
			cpt.DWHCPTCOM
			,cpt.DWHCPTPPAL
			,cpt.DWHCPTDAO 
			,cpt.DWHCPTDAC
			,cpt.DWHCPTRUB	
		) a;

	insert into #tab_activite_prec (num_cpt, id_client, cod_rub, Nv_Activite_cav,Tranche_Activite_cav)
	select    
		DWHCPTCOM as num_cpt,
		IDClientSAB as id_client,
		DWHCPTRUB as cod_rub,
		(case
			when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl >= 30 AND Flg_R = 'A') then 'Actif++'
			when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  >= 12  and nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  <= 29  AND  Flg_R = 'A') then 'Actif+'
			when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  >= 1  and nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  <= 11  AND  Flg_R = 'A') then 'Quasi_Actif'
			when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl   < 1  AND  Flg_R = 'A') then 'Inactif'
			when  Flg_R = 'R' then 'Recent'	
			when  Flg_R = 'C' then 'Clos du mois'	
			else 'AUTRE' end
		) as Nv_Activite_cav
		,
		(case
		 when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl >= 90 ) then 'Tranche 90 et +'
		 when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  >= 75  and nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  <= 89) then 'Tranche 75-89'
		 when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  >= 60  and nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  <= 74) then 'Tranche 60-74'
		 when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  >= 45  and nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  <= 59) then 'Tranche 45-59'
		 when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  >= 30  and nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  <=44) then 'Tranche 30-44'
		 when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  >= 6  and nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  <= 29) then 'Tranche 6-29'
		 when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  >= 1  and nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  <= 5) then 'Tranche 1-5'
		 when (nb_prel-nb_prel_rej+nb_vir_debit-nb_vir_deb_rej +Nb_paiement_carte - Nb_paiement_carte_rej +nb_chq_emis-nb_chq_emis_rej+nb_1er_ver_csl  < 1) then 'Tranche 0'
		 else 'AUTRE TRANCHE'
		end) as Tranche_Activite_cav
	from 
		(
		select
			cpt.DWHCPTCOM
			,cpt.DWHCPTPPAL as IDClientSAB
			,cpt.DWHCPTDAO as date_ouv
			,cpt.DWHCPTDAC as date_clo
			, cpt.DWHCPTRUB 
			,SUM(case when [COD_OPE] in ('A0P', 'R0P') and MTT_EUR >0 then 1 else 0 end) nb_prel
			,SUM(case when [COD_OPE] in ('A0P', 'R0P','A1P', 'A2P', 'R1P', 'R2P') and MTT_EUR <0 then 1 else 0 end) nb_prel_rej
			,SUM(case when [COD_OPE] in ('A0V', 'R0V') and MTT_EUR >0 then 1 else 0 end) nb_vir_debit
			,sum(case when [COD_OPE] in ('A1V', 'A2V','R1V','R2V') and MTT_EUR <0 then 1 else 0 end) nb_vir_deb_rej
			,SUM(case when [COD_OPE] ='CTB' and MTT_EUR > 0 then 1 else 0 end) as Nb_paiement_carte
			,SUM(case when [COD_OPE] ='CTB' and substring(COD_ANA,5,2) = '17' and MTT_EUR < 0 then 1 else 0 end) as Nb_paiement_carte_rej
			,sum(case when COD_OPE = 'RI0' and MTT_EUR > 0 then 1 else 0 end) nb_chq_emis
			,sum(case when COD_OPE IN ('AI1','RI0') and MTT_EUR < 0 then 1 else 0 end) nb_chq_emis_rej
			,sum(case when COD_OPE = 'ERE' and COD_EVE = 'OUV' then 1 else 0 end) nb_1er_ver_csl
			, case 
				when(DWHCPTDAO >= cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF_act_prec) - 3, 0) as date) and (DWHCPTDAC IS NULL OR DWHCPTDAC > cast(DATEADD(DAY, -1, DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF_act_prec), 0)) as date))) then 'R'		-- Client récent
				when(DWHCPTDAC >= cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF_act_prec) - 1, 0) as date) and DWHCPTDAC <= cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF_act_prec), -1) as date)) then 'C'												-- Client clos du mois
				else 'A' 
				end Flg_R
		from 
				#TAB_CAV cpt 
					left outer join #TAB_MVT_ACT_PREC mouv  on mouv.DWHCPTCOM = cpt.DWHCPTCOM 
														and COD_OPE in ('A0P', 'R0P', 'A0V', 'R0V','CTB', 'RI0', 'AI1', 'A1V', 'A2V','ERE','A1P', 'A2P', 'R1P', 'R2P','R1V','R2V') 
														and DTE_OPE >=  cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF_act_prec) - 3, 0) as date) -- 1er jour M-3
														and DTE_OPE <= cast(DATEADD(DAY, -1, DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF_act_prec), 0)) as date) -- dernier jour M-1 -->selection des operations des 3 derniers mois
		where 
			DWHCPTDAO < @DATE_REF_act_prec	
			and (DWHCPTDAC IS NULL OR (DWHCPTDAC >= cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF_act_prec) - 1, 0) as date) and DWHCPTDAC <= cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, @DATE_REF_act_prec), -1) as date)) OR cpt.DWHCPTDAC >= @DATE_REF_act_prec)								-- compte de plus de 3 mois et non clos a la date de reference	
		group by
			cpt.DWHCPTCOM
			,cpt.DWHCPTPPAL
			,cpt.DWHCPTDAO 
			,cpt.DWHCPTDAC
			,cpt.DWHCPTRUB	
		) a;

	-------------------------------------------------- Requête --------------------------------------------------

	-- Activité CAV
	TRUNCATE TABLE #VDC_TYPE_OPERATION;
	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] ,[Type_Op1],[Type_Op2]
		,[Nb_Ope] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Nb_Ope3]
		,[Nb_Ope4]
		)select Date_Operation,'INF_GEN' AS [Type_Op1], 'STK_CAV' AS [Type_Op2],
		case when Nv_Activite_cav = 'Actif++' then NombreCompteCAV end as 'Nombre de compte CAV Actif++',
		case when Nv_Activite_cav = 'Actif+' then NombreCompteCAV end as 'Nombre de compte CAV Actif+',
		case when Nv_Activite_cav = 'Quasi_Actif' then NombreCompteCAV end as 'Nombre de compte CAV Quasi_Actif',
		case when Nv_Activite_cav = 'Inactif' then NombreCompteCAV end as 'Nombre de compte CAV Inactif',
		case when Nv_Activite_cav = 'Recent' then NombreCompteCAV end as 'Nombre de compte CAV Recent'
	from (
	select @mois_ref as [Date_operation],
	act.Nv_Activite_cav , count(distinct act.num_cpt) as 'NombreCompteCAV'
	from #tab_activite act
	group by act.Nv_Activite_cav) AS act;

	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] ,[Type_Op1],[Type_Op2]
		,[Nb_Ope] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Nb_Ope3]
		,[Nb_Ope4]
		)select Date_Operation,'INF_GEN' AS [Type_Op1], 'STK_CAV' AS [Type_Op2],
		case when Nv_Activite_cav = 'Actif++' then NombreCompteCAV end as 'Nombre de compte CAV Actif++',
		case when Nv_Activite_cav = 'Actif+' then NombreCompteCAV end as 'Nombre de compte CAV Actif+',
		case when Nv_Activite_cav = 'Quasi_Actif' then NombreCompteCAV end as 'Nombre de compte CAV Quasi_Actif',
		case when Nv_Activite_cav = 'Inactif' then NombreCompteCAV end as 'Nombre de compte CAV Inactif',
		case when Nv_Activite_cav = 'Recent' then NombreCompteCAV end as 'Nombre de compte CAV Recent'
	from (
	select @mois_ref_prec as [Date_operation],
	act.Nv_Activite_cav , count(distinct act.num_cpt) as 'NombreCompteCAV'
	from #tab_activite_prec act
	group by act.Nv_Activite_cav) as cav;

	-- Activité CSL

	select  DWHCPTRUB,DWHCPTPPAL,DWHCPTCOM,DWHCPTDAO,DWHCPTDAC
	into  #activite_csl
	from
		(select DWHCPTPPAL, DWHCPTCOM, DWHCPTRUB, DWHCPTDAO, DWHCPTDAC, row_number() over (partition by DWHCPTCOM order by Validity_StartDate desc) as Validity_Flag
		from [$(DataHubDatabaseName)].[dbo].[VW_PV_SA_Q_COMPTE]
		where DWHCPTRUB IN ('254111')) cpt 	-- selection des CSL
		INNER JOIN [$(DataHubDatabaseName)].[dbo].[PV_FE_EQUIPMENT] equ on equ.EQUIPMENT_NUMBER = cpt.DWHCPTCOM 
		inner join [$(DataHubDatabaseName)].[dbo].PV_FE_ENROLMENTFOLDER f on f.ID_FE_ENROLMENT_FOLDER = equ.ENROLMENT_FOLDER_ID	
	where
		cpt.Validity_Flag = 1;

	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] ,[Type_Op1],[Type_Op2]
		,[Nb_Ope] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Nb_Ope3]
		,[Nb_Ope4]
		)
	select Date_Operation,'INF_GEN' AS [Type_Op1], 'STK_CSL' AS [Type_Op2],
		case when Nv_Activite_cav = 'Actif++' then NombreCompteCSL end as 'Nombre de compte CSL Actif++',
		case when Nv_Activite_cav = 'Actif+' then NombreCompteCSL end as 'Nombre de compte CSL Actif+',
		case when Nv_Activite_cav = 'Quasi_Actif' then NombreCompteCSL end as 'Nombre de compte CSL Quasi_Actif',
		case when Nv_Activite_cav = 'Inactif' then NombreCompteCSL end as 'Nombre de compte CSL Inactif',
		case when Nv_Activite_cav = 'Recent' then NombreCompteCSL end as 'Nombre de compte CSL Recent'
	from (
	select  @mois_ref as [Date_operation],actv.Nv_Activite_cav ,	count(distinct cpt.DWHCPTCOM) as 'NombreCompteCSL'
	from #activite_csl cpt
		left outer join #tab_activite actv on actv.id_client = cpt.DWHCPTPPAL	
	where cpt.DWHCPTDAO < @DATE_REF_act
		and (cpt.DWHCPTDAC IS NULL or cpt.DWHCPTDAC >=@DATE_REF_act)
	group by actv.Nv_Activite_cav) as csl;

	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] ,[Type_Op1],[Type_Op2]
		,[Nb_Ope] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Nb_Ope3]
		,[Nb_Ope4])
		select Date_Operation,'INF_GEN' AS [Type_Op1], 'STK_CSL' AS [Type_Op2],
		case when Nv_Activite_cav = 'Actif++' then NombreCompteCSL end as 'Nombre de compte CSL Actif++',
		case when Nv_Activite_cav = 'Actif+' then NombreCompteCSL end as 'Nombre de compte CSL Actif+',
		case when Nv_Activite_cav = 'Quasi_Actif' then NombreCompteCSL end as 'Nombre de compte CSL Quasi_Actif',
		case when Nv_Activite_cav = 'Inactif' then NombreCompteCSL end as 'Nombre de compte CSL Inactif',
		case when Nv_Activite_cav = 'Recent' then NombreCompteCSL end as 'Nombre de compte CSL Recent'
	from (
	select  @mois_ref_prec as [Date_operation],actv.Nv_Activite_cav ,	count(distinct cpt.DWHCPTCOM) as 'NombreCompteCSL'
	from #activite_csl cpt
		left outer join #tab_activite_prec actv on actv.id_client = cpt.DWHCPTPPAL	
	where cpt.DWHCPTDAO < @DATE_REF_act_prec
		and (cpt.DWHCPTDAC IS NULL or cpt.DWHCPTDAC >=@DATE_REF_act_prec)
	group by actv.Nv_Activite_cav) as csl;


	-- Activité Crédit 

		select des.DTE_ACC_TOKOS,des.DTE_REA,des.DTE_PRE_ECH,des.REF_CPT,CAT_PRE,DTE_FIN_PRE,DWHCPTPPAL
		into #activite_cred
		from 
		[$(DataHubDatabaseName)].[dbo].PV_FI_Q_DES des 
		inner JOIN [$(DataHubDatabaseName)].[dbo].PV_SA_Q_COM com on com.comrefref = des.REF_CPT
		inner join (select DWHCPTPPAL, DWHCPTCOM, DWHCPTRUB, DWHCPTDAO, DWHCPTDAC, row_number() over (partition by DWHCPTCOM order by Validity_StartDate desc) as Validity_Flag
		from [$(DataHubDatabaseName)].[dbo].[VW_PV_SA_Q_COMPTE]
		where DWHCPTRUB = ('203120')) cpt on cpt.DWHCPTCOM = com.comrefcom and cpt.Validity_Flag=1
		inner join [$(DataHubDatabaseName)].[dbo].PV_FE_EQUIPMENT e on e.EQUIPMENT_NUMBER = cpt.DWHCPTCOM
		inner join [$(DataHubDatabaseName)].[dbo].PV_FE_ENROLMENTFOLDER f on f.ID_FE_ENROLMENT_FOLDER = e.ENROLMENT_FOLDER_ID


	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] ,[Type_Op1],[Type_Op2]
		,[Nb_Ope] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Nb_Ope3]
		,[Nb_Ope4])
		select Date_Operation,'INF_GEN' AS [Type_Op1], 'CRE_ACC' AS [Type_Op2],
		case when Nv_Activite_cav = 'Actif++' then NbreCreditAcc end as 'Crédit accordé CAV Actif++',
		case when Nv_Activite_cav = 'Actif+' then NbreCreditAcc end as 'Crédit accordé CAV Actif+',
		case when Nv_Activite_cav = 'Quasi_Actif' then NbreCreditAcc end as 'Crédit accordé CAV Quasi_Actif',
		case when Nv_Activite_cav = 'Inactif' then NbreCreditAcc end as 'Crédit accordé CAV Inactif',
		case when Nv_Activite_cav = 'Recent' then NbreCreditAcc end as 'Crédit accordé CAV Recent'
	from (
	select  @mois_ref as [Date_operation],actv.Nv_Activite_cav ,	count(distinct cpt.REF_CPT) as 'NbreCreditAcc'
	from #activite_cred cpt
		left outer join #tab_activite actv on actv.id_client = cpt.DWHCPTPPAL
	where CAT_PRE = 'NFOB'
		and DTE_ACC_TOKOS <= @dern_j_mois				
		and isnull(DTE_FIN_PRE,'9999-12-31') > @dern_j_mois
		and cpt.DTE_ACC_TOKOS <= @dern_j_mois and (cpt.DTE_REA IS NULL or cpt.DTE_REA > @dern_j_mois)
	group by actv.Nv_Activite_cav) as acc;

	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] ,[Type_Op1],[Type_Op2]
		,[Nb_Ope] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Nb_Ope3]
		,[Nb_Ope4])
		select Date_Operation,'INF_GEN' AS [Type_Op1], 'CRE_ACC' AS [Type_Op2],
		case when Nv_Activite_cav = 'Actif++' then NbreCreditAcc end as 'Crédit accordé CAV Actif++',
		case when Nv_Activite_cav = 'Actif+' then NbreCreditAcc end as 'Crédit accordé CAV Actif+',
		case when Nv_Activite_cav = 'Quasi_Actif' then NbreCreditAcc end as 'Crédit accordé CAV Quasi_Actif',
		case when Nv_Activite_cav = 'Inactif' then NbreCreditAcc end as 'Crédit accordé CAV Inactif',
		case when Nv_Activite_cav = 'Recent' then NbreCreditAcc end as 'Crédit accordé CAV Recent'
	from (
	select  @mois_ref_prec as [Date_operation],actv.Nv_Activite_cav ,	count(distinct cpt.REF_CPT) as 'NbreCreditAcc'
	from #activite_cred cpt
		left outer join #tab_activite_prec actv on actv.id_client = cpt.DWHCPTPPAL
	where CAT_PRE = 'NFOB'
		and DTE_ACC_TOKOS <= @dern_j_mois_prec				
		and isnull(DTE_FIN_PRE,'9999-12-31') > @dern_j_mois_prec
		and cpt.DTE_ACC_TOKOS <= @dern_j_mois_prec and (cpt.DTE_REA IS NULL or cpt.DTE_REA > @dern_j_mois_prec)
	group by actv.Nv_Activite_cav) as acc;


	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] ,[Type_Op1],[Type_Op2]
		,[Nb_Ope] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Nb_Ope3]
		,[Nb_Ope4])
		select Date_Operation,'INF_GEN' AS [Type_Op1], 'CRE_DEC' AS [Type_Op2],
		case when Nv_Activite_cav = 'Actif++' then NbreCreditDec end as 'Crédit décaissé CAV Actif++',
		case when Nv_Activite_cav = 'Actif+' then NbreCreditDec end as 'Crédit décaissé CAV Actif+',
		case when Nv_Activite_cav = 'Quasi_Actif' then NbreCreditDec end as 'Crédit décaissé CAV Quasi_Actif',
		case when Nv_Activite_cav = 'Inactif' then NbreCreditDec end as 'Crédit décaissé CAV Inactif',
		case when Nv_Activite_cav = 'Recent' then NbreCreditDec end as 'Crédit décaissé CAV Recent'
	from (
	select  @mois_ref as [Date_operation],actv.Nv_Activite_cav ,	count(distinct cpt.REF_CPT) as 'NbreCreditDec'
	from #activite_cred cpt
		left outer join #tab_activite actv on actv.id_client = cpt.DWHCPTPPAL
	where CAT_PRE = 'NFOB'
		and DTE_ACC_TOKOS <= @dern_j_mois				
		and isnull(DTE_FIN_PRE,'9999-12-31') > @dern_j_mois
		and cpt.DTE_REA <= @dern_j_mois and  cpt.DTE_REA <= cpt.DTE_PRE_ECH
	group by actv.Nv_Activite_cav) as dec;

	INSERT INTO #VDC_TYPE_OPERATION (
		 [Date_operation] ,[Type_Op1],[Type_Op2]
		,[Nb_Ope] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Nb_Ope3]
		,[Nb_Ope4])
		select Date_Operation,'INF_GEN' AS [Type_Op1], 'CRE_DEC' AS [Type_Op2],
		case when Nv_Activite_cav = 'Actif++' then NbreCreditDec end as 'Crédit décaissé CAV Actif++',
		case when Nv_Activite_cav = 'Actif+' then NbreCreditDec end as 'Crédit décaissé CAV Actif+',
		case when Nv_Activite_cav = 'Quasi_Actif' then NbreCreditDec end as 'Crédit décaissé CAV Quasi_Actif',
		case when Nv_Activite_cav = 'Inactif' then NbreCreditDec end as 'Crédit décaissé CAV Inactif',
		case when Nv_Activite_cav = 'Recent' then NbreCreditDec end as 'Crédit décaissé CAV Recent'
	from (
	select  @mois_ref_prec as [Date_operation],actv.Nv_Activite_cav ,	count(distinct cpt.REF_CPT) as 'NbreCreditDec'
	from #activite_cred cpt
		left outer join #tab_activite_prec actv on actv.id_client = cpt.DWHCPTPPAL
	where CAT_PRE = 'NFOB'
		and DTE_ACC_TOKOS <= @dern_j_mois_prec				
		and isnull(DTE_FIN_PRE,'9999-12-31') > @dern_j_mois_prec
		and cpt.DTE_REA <= @dern_j_mois_prec and  cpt.DTE_REA <= cpt.DTE_PRE_ECH
	group by actv.Nv_Activite_cav) as dec;

	
	-- Tranche 90 +
	select @mois_ref as [Date_operation],'KPI_GEN' AS Type_Op1, 'KPI_1' AS Type_Op2,
	count( distinct act.id_client) as Nb_Ope6 --'Nb clients ayant fait plus de 90 opérations 3 derniers mois'
	into #tab_activite_KPI
	from #tab_activite act
		inner join #TAB_CAV c on c.DWHCPTCOM = act.num_cpt
	where 
		act.Tranche_Activite_cav = 'Tranche 90 et +'
		and c.DWHCPTDAO < @DATE_REF
		and (c.DWHCPTDAC IS NULL or c.DWHCPTDAC >=@DATE_REF)
	group by 
		act.Tranche_Activite_cav;

	UPDATE VDC
	SET VDC.Nb_Ope6=KPI.Nb_Ope6
	FROM [dbo].[VDC_TYPE_OPERATION] VDC INNER JOIN #tab_activite_KPI KPI ON VDC.Date_operation=KPI.Date_operation 
	AND VDC.Type_Op1=KPI.Type_Op1 
	AND VDC.Type_Op2=KPI.Type_Op2;

	INSERT INTO [dbo].[VDC_TYPE_OPERATION] (
		 [Date_operation] 
		,[Type_Op1] 
		,[Type_Op2] 
		,[Nb_Ope] 
		,[Nb_Ope1] 
		,[Nb_Ope2]
		,[Nb_Ope3]
		,[Nb_Ope4]
	)
	select 	[date_operation],[Type_Op1],[Type_Op2]
		,SUM([Nb_Ope]				 ) AS [Nb_Ope]
		,SUM([Nb_Ope1]				 ) AS [Nb_Ope1]
		,SUM([Nb_Ope2] 			     ) AS [Nb_Ope2] 
		,SUM([Nb_Ope3] 				 ) AS [Nb_Ope3] 
		,SUM([Nb_Ope4] 				 ) AS [Nb_Ope4]   
	from #VDC_TYPE_OPERATION
	GROUP BY [date_operation],[Type_Op1] ,[Type_Op2];
END
GO