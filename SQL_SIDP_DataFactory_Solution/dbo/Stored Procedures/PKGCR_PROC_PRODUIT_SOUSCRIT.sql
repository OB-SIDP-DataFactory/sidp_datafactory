﻿
-- =============================================
-- Description:	Alimentation de la table CRE_PRODUIT_SOUSCRIT contenant les produits souscrits Crédit
-- Tables input : PV_FE_SUBSCRIBEDCONSUMERLOAN
--				  PV_FE_SUBSCRIBEDPRODUCT
--				  PV_FE_ENROLMENTFOLDER
-- Table output : CRE_PRODUIT_SOUSCRIT				
-- =============================================
CREATE PROC [dbo].[PKGCR_PROC_PRODUIT_SOUSCRIT] (@date_alim DATE, @date_derniere_alim DATE, @nbRows int OUTPUT)
AS  
BEGIN 

-- Suppression des données pour la gestion de reprise de chargement
DELETE FROM [dbo].[CRE_PRODUIT_SOUSCRIT]
WHERE [DTE_ALIM] >= @date_derniere_alim

INSERT INTO [dbo].[CRE_PRODUIT_SOUSCRIT]
(
	   [IDE_FE_PROD_SOUS_CRE]
	  ,[IDE_OPPRT_SF]
      ,[NUM_CPT_SAB]
      ,[NUM_SOUS_FFI]
      ,[NUM_CONT_FFI]
      ,[NUM_MAN_SEPA]
      ,[DTE_FIN_PRD_RETRACT]
      ,[DTE_FIN_DEC_CRE]
      ,[DTE_DEB_DEC_CRE]
      ,[RENONC_DRO_RETRACT]
      ,[DTE_ACCEPT_ENG_CONF]
      ,[DTE_ACCEPT]
      ,[IDE_COUL_SCO_CRE]
      ,[VAL_SCO_CRE]
      ,[TX_DEBIT]
      ,[IDE_COUL_TX_DEBIT]
      ,[IDE_TYP_DEBIT_DIRECT_CPT]
      ,[IDE_COUL_SCO_FFI]
      ,[REFUS_COUV_ASS_EMP]
      ,[REFUS_COUV_ASS_COEMP]
      ,[IDE_TERR]
      ,[CLE_CONTROLE_DEBIT_DIRECT]
      ,[BBAN_DEBIT_DIRECT]
      ,[DTE_ALIM]  
	)
SELECT
       [ID_FE_SUBSCRIBEDCONSUMERLOAN]
	  ,EF.SALESFORCE_OPPORTUNITY_ID
      ,[ACCOUNT_NUMBER]
      ,[FRANFINANCE_APPLICATION_NUMBER]
      ,[FRANFINANCE_CONTRACT_NUMBER]
      ,[SEPA_MANDATE_NUMBER]
      ,[RETRACTION_PERIOD_END_DATE]
      ,[LOAN_DISBURSMENT_END_DATE]
      ,[LOAN_DISBURSMENT_START_DATE]
      ,[RETRACTION_RIGHT_RENOUNCED]
      ,[COMPLIANCE_COMMIT_ACCEPT_DATE]
      ,[APPROVAL_DATE]
      ,[LOAN_SCORE_COLOR_ID]
      ,[LOAN_SCORE_VALUE]
      ,[DEBT_RATE]
      ,[DEBT_RATE_COLOR_ID]
      ,[DIRECT_DEBIT_ACCOUNT_TYPE_ID]
      ,[FRANFINANCE_SCORE_COLOR_ID]
      ,[BORR_INSUR_COVERAGE_REFUSAL]
      ,[CO_BORR_INSUR_COVERAGE_REFUSAL]
      ,[DIRECT_DEBIT_COUNTRY_ID]
      ,[DIRECT_DEBIT_CONTROL_KEY]
      ,[DIRECT_DEBIT_BBAN]
      ,dateadd (day,-1, @date_alim) as [DTE_ALIM]
	FROM  [$(DataHubDatabaseName)].[dbo].[PV_FE_SUBSCRIBEDCONSUMERLOAN] SC
	inner join [$(DataHubDatabaseName)].[dbo].[PV_FE_SUBSCRIBEDPRODUCT] SP
	ON SP.ID_FE_SUBSCRIBEDPRODUCT = SC.ID_FE_SUBSCRIBEDCONSUMERLOAN
	inner join [$(DataHubDatabaseName)].[dbo].[PV_FE_ENROLMENTFOLDER] EF
	ON EF.ID_FE_ENROLMENT_FOLDER = SP.ENROLMENT_FOLDER_ID
    WHERE @date_derniere_alim <=SC.Validity_StartDate and SC.Validity_StartDate < @date_alim

SELECT @nbRows = @@ROWCOUNT

END