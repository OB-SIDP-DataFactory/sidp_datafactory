﻿CREATE PROCEDURE [dbo].[PKGRC_PROC_genesys] 
	-- Add the parameters for the stored procedure here
	@date_param date,
	@nbCollectedRows int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
			declare @date_min date 
			select @date_min = MIN(cast([DTE_IND] as date )) from [$(DataHubDatabaseName)].[dbo].[PV_GE_TELEPHONIE_2]
			--select @date_min

			declare @date_max date 
			select @date_max = MAX(cast([DTE_IND] as date )) from [$(DataHubDatabaseName)].[dbo].[PV_GE_TELEPHONIE_2]
			--select @date_max

			delete from [SAS_Vue_RC_Genesys]
			where STANDARD_DATE between @date_min and @date_max

    -- Insert statements for procedure here
	if OBJECT_ID(N'[dbo].[SAS_Vue_RC_Genesys]',N'U') is not null
	begin
		insert into [dbo].[SAS_Vue_RC_Genesys]
			Select 
			tps.StandardDate as STANDARD_DATE,
			tps.IsoWeek as SEMAINE,
			tps.MonthYear AS MOIS_ANNEE,
			Gen2.IDE_AXE_LOG_AGE as GENESYS_ID, 
			usr.CODE_SF_USER as OWNER_ID,
			usr.PROFIL_USER,
			CONCAT(usr.NOM_USER, ' ', usr.PRENOM_USER) as LOGIN_USER, 
			round(Gen2.NB_APP_REP_ACD/((Gen2.DUR_TOT_LOG- (Gen2.DUR_TOT_RET_PAU+Gen2.DUR_TOT_RET_REP+Gen2.DUR_TOT_RET_REU+Gen2.DUR_TOT_RET_SUI+Gen2.DUR_TOT_RET_GES+
			Gen2.DUR_TOT_RET_SAI+Gen2.DUR_TOT_RET_TEC+Gen2.DUR_TOT_RET_AUT+Gen2.DUR_TOT_RET_AUTRE1+Gen2.DUR_TOT_RET_AUTRE2+Gen2.DUR_TOT_RET_AUTRE3)- Gen2.DUR_TOT_COM_SOR)/3600),0) as NB_MOY_APP_HEURE, 
			Gen2.DUR_TOT_RET_PAU,
			Gen2.DUR_TOT_RET_REP,
			Gen2.DUR_TOT_RET_REU,
			Gen2.DUR_TOT_RET_SUI,
			Gen2.DUR_TOT_RET_GES, 
			Gen2.DUR_TOT_RET_SAI,
			Gen2.DUR_TOT_RET_TEC,
			Gen2.DUR_TOT_RET_AUT,
			Gen2.DUR_TOT_RET_AUTRE1,
			Gen2.DUR_TOT_RET_AUTRE2,
			Gen2.DUR_TOT_RET_AUTRE3,
			case when Gen2.NB_APP_REP = 0 then 0 else round((Gen2.DUR_TOT_RET_GES/ Gen2.NB_APP_REP),0) end AS MOY_POST_APP, 
			Gen2.DUR_TOT_LOG AS TPS_TVL_EFF, 
			round((Gen2.DUR_TOT_RET_PAU+Gen2.DUR_TOT_RET_REP+Gen2.DUR_TOT_RET_REU+Gen2.DUR_TOT_RET_SUI+Gen2.DUR_TOT_RET_GES+
			Gen2.DUR_TOT_RET_SAI+Gen2.DUR_TOT_RET_TEC+Gen2.DUR_TOT_RET_AUT+Gen2.DUR_TOT_RET_AUTRE1+Gen2.DUR_TOT_RET_AUTRE2+Gen2.DUR_TOT_RET_AUTRE3)/11,0) AS DUR_MOY_RET, 
			case when Gen2.NB_APP_GAR = 0 then 0 else round((Gen2.DUR_TOT_GAR/Gen2.NB_APP_GAR),0) end AS TPS_GAR_APL,
			case when Gen2.NB_APP_SON = 0 then 0 else (Gen2.NB_APP_REP/Gen2.NB_APP_SON) end AS TAU_DECR,
			Gen2.DUR_TOT_GAR as DUR_MOY_ATT,
			case when Gen2.NB_APP_ACD_PRE = 0 then 0 else (Gen2.NB_APP_ACD_PER_NON_REP/Gen2.NB_APP_ACD_PRE) end as TAU_ABA ,
			(case when Gen2.NB_APP_REP=0 then 0 else (Gen2.DUR_TOT_COM_ENT/Gen2.NB_APP_REP) end) as DMC, 
			(case when Gen2.NB_APP_REP=0 then 0 else (Gen2.DUR_TOT_COM_ENT/Gen2.NB_APP_REP) end)+Gen2.DUR_TOT_RET_GES as DMT 

			From 
			[$(DataHubDatabaseName)].[dbo].[PV_GE_TELEPHONIE_2] Gen2 
			LEFT OUTER JOIN [dbo].[DIM_USER] usr 
			on Gen2.IDE_AXE_LOG_AGE = usr.CODE_GENESYS_USER and cast(usr.Validity_StartDate as date) <= Gen2.DTE_IND and Gen2.DTE_IND <= cast(usr.Validity_EndDate as date)
			LEFT OUTER JOIN [dbo].[DIM_TEMPS] tps 
			on tps.StandardDate = cast(Gen2.DTE_IND as date)
			where Gen2.DTE_IND >= @date_param and Gen2.DTE_IND < GETDATE();
			-- plage d'alim: date dernière collecte Ok jusqu'à 00h date du jour (getdate)
	end
else
	begin
		Select 
			tps.StandardDate as STANDARD_DATE,
			tps.IsoWeek as SEMAINE,
			tps.MonthYear AS MOIS_ANNEE,
			Gen2.IDE_AXE_LOG_AGE as GENESYS_ID, 
			usr.CODE_SF_USER as OWNER_ID, 
			usr.PROFIL_USER,
			CONCAT(usr.NOM_USER, ' ', usr.PRENOM_USER) as LOGIN_USER,
			round(Gen2.NB_APP_REP_ACD/((Gen2.DUR_TOT_LOG- (Gen2.DUR_TOT_RET_PAU+Gen2.DUR_TOT_RET_REP+Gen2.DUR_TOT_RET_REU+Gen2.DUR_TOT_RET_SUI+Gen2.DUR_TOT_RET_GES+
			Gen2.DUR_TOT_RET_SAI+Gen2.DUR_TOT_RET_TEC+Gen2.DUR_TOT_RET_AUT+Gen2.DUR_TOT_RET_AUTRE1+Gen2.DUR_TOT_RET_AUTRE2+Gen2.DUR_TOT_RET_AUTRE3)- Gen2.DUR_TOT_COM_SOR)/3600),0) as NB_MOY_APP_HEURE, 
			Gen2.DUR_TOT_RET_PAU,
			Gen2.DUR_TOT_RET_REP,
			Gen2.DUR_TOT_RET_REU,
			Gen2.DUR_TOT_RET_SUI,
			Gen2.DUR_TOT_RET_GES, 
			Gen2.DUR_TOT_RET_SAI,
			Gen2.DUR_TOT_RET_TEC,
			Gen2.DUR_TOT_RET_AUT,
			Gen2.DUR_TOT_RET_AUTRE1,
			Gen2.DUR_TOT_RET_AUTRE2,
			Gen2.DUR_TOT_RET_AUTRE3,
			case when Gen2.NB_APP_REP = 0 then 0 else round((Gen2.DUR_TOT_RET_GES/ Gen2.NB_APP_REP),0) end AS MOY_POST_APP, 
			Gen2.DUR_TOT_LOG AS TPS_TVL_EFF, 
			round((Gen2.DUR_TOT_RET_PAU+Gen2.DUR_TOT_RET_REP+Gen2.DUR_TOT_RET_REU+Gen2.DUR_TOT_RET_SUI+Gen2.DUR_TOT_RET_GES+
			Gen2.DUR_TOT_RET_SAI+Gen2.DUR_TOT_RET_TEC+Gen2.DUR_TOT_RET_AUT+Gen2.DUR_TOT_RET_AUTRE1+Gen2.DUR_TOT_RET_AUTRE2+Gen2.DUR_TOT_RET_AUTRE3)/11,0) AS DUR_MOY_RET, 
			case when Gen2.NB_APP_GAR = 0 then 0 else round((Gen2.DUR_TOT_GAR/Gen2.NB_APP_GAR),0) end AS TPS_GAR_APL,
			case when Gen2.NB_APP_SON = 0 then 0 else (Gen2.NB_APP_REP/Gen2.NB_APP_SON) end AS TAU_DECR,
			Gen2.DUR_TOT_GAR as DUR_MOY_ATT,
			case when Gen2.NB_APP_ACD_PRE = 0 then 0 else (Gen2.NB_APP_ACD_PER_NON_REP/Gen2.NB_APP_ACD_PRE) end as TAU_ABA ,
			(case when Gen2.NB_APP_REP=0 then 0 else (Gen2.DUR_TOT_COM_ENT/Gen2.NB_APP_REP) end) as DMC, 
			(case when Gen2.NB_APP_REP=0 then 0 else (Gen2.DUR_TOT_COM_ENT/Gen2.NB_APP_REP) end)+Gen2.DUR_TOT_RET_GES as DMT 
			into [dbo].[SAS_Vue_RC_Genesys]
			From 
			[$(DataHubDatabaseName)].[dbo].[PV_GE_TELEPHONIE_2] Gen2 
			LEFT OUTER JOIN [dbo].[DIM_USER] usr 
			on Gen2.IDE_AXE_LOG_AGE = usr.CODE_GENESYS_USER and cast(usr.Validity_StartDate as date) <= Gen2.DTE_IND and Gen2.DTE_IND <= cast(usr.Validity_EndDate as date) 
			LEFT OUTER JOIN [dbo].[DIM_TEMPS] tps 
			on tps.StandardDate = cast(Gen2.DTE_IND as date)
			where Gen2.DTE_IND >= @date_param or Gen2.DTE_IND < GETDATE();
			-- plage d'alim: date dernière collecte Ok jusqu'à 00h date du jour (getdate)
	end

	select @nbCollectedRows = @@ROWCOUNT;
END