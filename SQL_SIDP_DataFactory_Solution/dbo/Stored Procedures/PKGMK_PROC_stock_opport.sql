﻿CREATE PROC [dbo].[PKGMK_PROC_stock_opport]
    @nbRows int OUTPUT -- nb lignes processées  
AS
BEGIN 

-- =====================================================================================================================
--tables input : [WK_STOCK_OPPORTUNITY]
--				 [DIM_TEMPS]
--				 [DIM_STADE_VENTE]
--				 [DIM_CANAL]
--				 [DIM_RESEAU]
--tables output : [sas_vue_stock_opport]
--description : créer une table agrégée avec le nombre d'opportunités en stock pour chaque jour d'alimentation
--				vider les tables temporaires utilisées
-- ======================================================================================================================

SET DATEFIRST 1; 

declare @date_min date 
select @date_min = MAX(cast(date_alim as date )) from [dbo].[sas_vue_stock_opport];

declare @date_max date 
select @date_max = MAX(cast(date_alim as date )) from [dbo].[WK_STOCK_OPPORTUNITY];

DROP TABLE IF EXISTS #WK_STOCK_OPPORTUNITY;

SELECT * INTO #WK_STOCK_OPPORTUNITY
from [dbo].[WK_STOCK_OPPORTUNITY]
where  date_alim between @date_min and @date_max;


delete from [dbo].[sas_vue_stock_opport]
where date_alim between @date_min and @date_max;

with
cte_stock_opport_agg as (
select date_alim
     , StageName
     , motif_non_aboutie
     , Indication__c
     , flag_indication
     , CreatedById
     , CreatedDate
     , CloseDate
     , age_opport
     , OwnerId
     , AdvisorCode__c
     , PointOfSaleCode__c
     , CommercialOfferCode__c
     , CommercialOfferName__c
     , RelationEntryScore__c
     , flag_full_digi
     , flag_full_btq
     , StartedChannel__c
     , DistributionChannel__c
     , DistributorNetwork__c
     , DistributorEntity__c
     , LeadSource
     , canal_fin
     , sous_reseau_fin
     , OptInDataTelco__c
     , OptInOrderTelco__c
     , flag_sidp
     , count(distinct Id_SF) AS nb_opport
  from #WK_STOCK_OPPORTUNITY
 where flag_opport_last_state = 1
group by date_alim
       , StageName
       , motif_non_aboutie
       , Indication__c
       , flag_indication
       , CreatedById
       , CreatedDate
       , CloseDate
       , age_opport
       , OwnerId
       , AdvisorCode__c
       , PointOfSaleCode__c
       , CommercialOfferCode__c
       , CommercialOfferName__c
       , RelationEntryScore__c
       , flag_full_digi
       , flag_full_btq
       , StartedChannel__c
       , DistributionChannel__c
       , DistributorNetwork__c
       , DistributorEntity__c
       , LeadSource
       , canal_fin
       , sous_reseau_fin
       , OptInDataTelco__c
       , OptInOrderTelco__c
       , flag_sidp
)

--jointure tables dimension 
INSERT INTO dbo.sas_vue_stock_opport
( date_alim
, jour_alim
, nb_opport
, stade_vente
, motif_sans_suite
, date_creat_opp
, date_fin_opp
, age_opport
, flag_full_digi
, flag_full_btq
, flag_indication
, flag_identif_OF
, flag_facture_OF
, canal_interact_origine
, canal_distrib_origine
, sous_reseau_origine
, reseau_origine
, canal_interact_final
, canal_distrib_final
, sous_reseau_final
, reseau_final
, flag_sidp 
, commercial_offer_code
, commercial_offer_name
, semaine_creation)
select date_alim
     , d5a.StandardDate as jour_alim
     , nb_opport
     , d1.LIBELLE as stade_vente
     , d1M.LIBELLE as motif_sans_suite
     , d5c.StandardDate as date_creat_opp
     , d.CloseDate as date_fin_opp
     , d.age_opport
     , d.flag_full_digi
     , d.flag_full_btq
     , isnull(d.flag_indication, 'false') as flag_indication
     , isnull(d.OptInDataTelco__c, 'false') as flag_identif_OF
     , isnull(d.OptInOrderTelco__c, 'false') as flag_facture_OF
     , isnull(dI7.LIBELLE, 'Non renseigné') as canal_interact_origine
     , isnull(dD7.LIBELLE, 'Non renseigné') as canal_distrib_origine
     , isnull(dSR8.LIBELLE, 'Non renseigné') as sous_reseau_origine
     , isnull(dR8.LIBELLE, 'Non renseigné') as reseau_origine
     , isnull(dI7F.LIBELLE, 'Non finalisées') as canal_interact_final
     , isnull(dD7F.LIBELLE, 'Non finalisées') as canal_distrib_final
     , isnull(dSR8F.LIBELLE, 'Non finalisées') as sous_reseau_final
     , isnull(dR8F.LIBELLE, 'Non finalisées') as reseau_final
     , flag_sidp 
	 , CommercialOfferCode__c
     , CommercialOfferName__c
	 , concat('S-',datediff(week,dateadd(dd, -@@datefirst, d5c.StandardDate), dateadd(dd, -@@datefirst, date_alim))) as semaine_creation 

  from cte_stock_opport_agg d 
  left join DIM_TEMPS d5a
    on cast(d.date_alim as date) = d5a.Date
  left join DIM_TEMPS d5c
    on cast(d.CreatedDate as date) = d5c.Date
  left join DIM_TEMPS d5f
    on cast(d.CloseDate as date) = d5f.Date
  left join DIM_STADE_VENTE d1
    on ( d.StageName = d1.CODE_SF and d1.PARENT_ID is null /* niveau stade de vente */ 	and d.date_alim BETWEEN d1.Validity_StartDate and d1.Validity_EndDate )
  left join DIM_STADE_VENTE d1M
    on ( d.motif_non_aboutie = d1M.CODE_SF and d.StageName = d1M.PARENT_ID /* niveau motif non aboutie */ and d.date_alim BETWEEN d1M.Validity_StartDate and d1M.Validity_EndDate )

  /* contexte de distribution d'origine*/
  left join DIM_CANAL dI7 
    on ( d.StartedChannel__c = dI7.CODE_SF and dI7.PARENT_ID is not null /* niveau canal interaction */ and d.date_alim BETWEEN dI7.Validity_StartDate and dI7.Validity_EndDate )
  left join DIM_CANAL dD7
    on ( dI7.PARENT_ID = dD7.CODE_SF and dD7.PARENT_ID is null /* niveau canal distribution */ and d.date_alim BETWEEN dD7.Validity_StartDate and dD7.Validity_EndDate )

  left join DIM_RESEAU dSR8
    on ( d.DistributorEntity__c = dSR8.CODE_SF and dSR8.PARENT_ID is not null /* niveau sous-réseau */ and d.date_alim BETWEEN dSR8.Validity_StartDate and dSR8.Validity_EndDate )
  left join DIM_RESEAU dR8
    on ( d.DistributorNetwork__c = dR8.CODE_SF and dR8.PARENT_ID is null /* niveau réseau */ and d.date_alim BETWEEN dR8.Validity_StartDate and dR8.Validity_EndDate )

  /* contexte de distribution de finalisation*/
  left join DIM_CANAL dI7F 
    on ( d.canal_fin = dI7F.CODE_SF and dI7F.PARENT_ID is not null /* niveau canal interaction */ and d.date_alim BETWEEN dI7F.Validity_StartDate and dI7F.Validity_EndDate )
  left join DIM_CANAL dD7F
    on ( dI7F.PARENT_ID = dD7F.CODE_SF and dD7F.PARENT_ID is null /* niveau canal distribution */ and d.date_alim BETWEEN dD7F.Validity_StartDate and dD7F.Validity_EndDate )

  left join DIM_RESEAU dSR8F
    on ( d.sous_reseau_fin = dSR8F.CODE_SF and dSR8F.PARENT_ID is not null /* niveau sous-réseau */ and d.date_alim BETWEEN dSR8F.Validity_StartDate and dSR8F.Validity_EndDate ) 
  left join DIM_RESEAU dR8F
    on ( dSR8F.PARENT_ID  = dR8F.CODE_SF and dR8F.PARENT_ID is null /* niveau réseau */ and d.date_alim BETWEEN dR8F.Validity_StartDate and dR8F.Validity_EndDate )
;	

select @nbRows = @@ROWCOUNT;

EXEC [dbo].[PROC_SSRS_OPPORT_ABOUTIES];
EXEC [dbo].[PROC_SSRS_SAS_VUE_STOCK_OPPORT];
END
GO