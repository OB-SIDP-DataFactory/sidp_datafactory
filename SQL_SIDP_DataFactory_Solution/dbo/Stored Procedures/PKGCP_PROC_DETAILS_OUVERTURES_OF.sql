﻿CREATE PROCEDURE [dbo].[PKGCP_PROC_DETAILS_OUVERTURES_OF]
@DTE_CLO_OPPRT date, 
@DTE_CREA date   
AS BEGIN
 
SELECT        1 as ID_Type,'Souscription Boutique' AS Type, CAST(DTE_CLO_OPPRT AS date) AS 'date clôture', YEAR(DTE_CLO_OPPRT) AS ANNEE,MONTH(DTE_CLO_OPPRT) AS MOIS, DATEPART(week, DTE_CLO_OPPRT) AS Semaine, NUM_OPPRT, DO, AD, Site_Libelle, COD_VENDEUR, 
                         ven.NAME, FLG_IND
FROM             [$(DataHubDatabaseName)].exposition.PV_SF_OPPORTUNITY AS opp LEFT OUTER JOIN
                          [$(DataHubDatabaseName)].dbo.REF_BOUTIQUE AS bou ON bou.Code_ADV_Orange = opp.COD_BOUT LEFT OUTER JOIN
                         DIM_VENDEUR AS ven ON ven.SELLER_UID = opp.COD_VENDEUR
WHERE        (FLG_DOS_COMP = 1) AND (LIB_STADE_VENTE = 'Compte ouvert') AND (COD_OFF_COM = 'OC80') AND (CASE WHEN @DTE_CLO_OPPRT IS NULL THEN CAST(getdate() AS date) 
                         ELSE CAST(DTE_CLO_OPPRT AS date) END >= CASE WHEN @DTE_CLO_OPPRT IS NULL THEN CAST(getdate() AS date) ELSE CAST(@DTE_CLO_OPPRT AS date) END) AND (CASE WHEN @DTE_CREA IS NULL 
                         THEN CAST(getdate() AS date) ELSE CAST(DTE_CREA AS date) END >= CASE WHEN @DTE_CREA IS NULL THEN CAST(getdate() AS date) ELSE CAST(@DTE_CREA AS date) END)
UNION ALL
SELECT        2 as ID_Type, 'Repli Selfcare' AS Type, CAST(DTE_CLO_OPPRT AS date) AS 'date clôture', YEAR(DTE_CLO_OPPRT) AS ANNEE,MONTH(DTE_CLO_OPPRT) AS MOIS, DATEPART(week, DTE_CLO_OPPRT) AS Semaine, NUM_OPPRT, DO, AD, Site_Libelle, COD_VENDEUR, 
                         ven.NAME, FLG_IND
FROM             [$(DataHubDatabaseName)].exposition.PV_SF_OPPORTUNITY AS opp LEFT OUTER JOIN
                          [$(DataHubDatabaseName)].dbo.REF_BOUTIQUE AS bou ON bou.Code_ADV_Orange = opp.COD_BOUT LEFT OUTER JOIN
                         DIM_VENDEUR AS ven ON ven.SELLER_UID = opp.COD_VENDEUR
WHERE        (FLG_DOS_COMP <> 1) AND (NUM_PROC_SOUS IS NOT NULL) AND (LIB_STADE_VENTE = 'Compte ouvert') AND (COD_OFF_COM = 'OC80') AND (CASE WHEN @DTE_CLO_OPPRT IS NULL THEN CAST(getdate() 
                         AS date) ELSE CAST(DTE_CLO_OPPRT AS date) END >= CASE WHEN @DTE_CLO_OPPRT IS NULL THEN CAST(getdate() AS date) ELSE CAST(@DTE_CLO_OPPRT AS date) END) AND 
                         (CASE WHEN @DTE_CREA IS NULL THEN CAST(getdate() AS date) ELSE CAST(DTE_CREA AS date) END >= CASE WHEN @DTE_CREA IS NULL THEN CAST(getdate() AS date) ELSE CAST(@DTE_CREA AS date) 
                         END)
UNION ALL
SELECT        3 as ID_Type,'Indication Boutique' AS Type, CAST(DTE_CLO_OPPRT AS date) AS 'date clôture', YEAR(DTE_CLO_OPPRT) AS ANNEE,MONTH(DTE_CLO_OPPRT) AS MOIS, DATEPART(week, DTE_CLO_OPPRT) AS Semaine, NUM_OPPRT, DO, AD, Site_Libelle, COD_VENDEUR, 
                         ven.NAME, FLG_IND
FROM             [$(DataHubDatabaseName)].exposition.PV_SF_OPPORTUNITY AS opp LEFT OUTER JOIN
                          [$(DataHubDatabaseName)].dbo.REF_BOUTIQUE AS bou ON bou.Code_ADV_Orange = opp.COD_BOUT LEFT OUTER JOIN
                         DIM_VENDEUR AS ven ON ven.SELLER_UID = opp.COD_VENDEUR
WHERE        (FLG_IND = 'OUI') AND (NUM_PROC_SOUS IS NULL) AND (LIB_ENT_DIS IN ('AD', 'GDT')) AND (LIB_STADE_VENTE = 'Compte ouvert') AND (COD_OFF_COM = 'OC80') AND (CASE WHEN @DTE_CLO_OPPRT IS NULL
                          THEN CAST(getdate() AS date) ELSE CAST(DTE_CLO_OPPRT AS date) END >= CASE WHEN @DTE_CLO_OPPRT IS NULL THEN CAST(getdate() AS date) ELSE CAST(@DTE_CLO_OPPRT AS date) END) AND 
                         (CASE WHEN @DTE_CREA IS NULL THEN CAST(getdate() AS date) ELSE CAST(DTE_CREA AS date) END >= CASE WHEN @DTE_CREA IS NULL THEN CAST(getdate() AS date) ELSE CAST(@DTE_CREA AS date) 
                         END)
UNION ALL
SELECT        4 as ID_Type,'Indication Digitale' AS Type, CAST(DTE_CLO_OPPRT AS date) AS 'date clôture', YEAR(DTE_CLO_OPPRT) AS ANNEE,MONTH(DTE_CLO_OPPRT) AS MOIS, DATEPART(week, DTE_CLO_OPPRT) AS Semaine, NUM_OPPRT, DO, AD, Site_Libelle, COD_VENDEUR, 
                         ven.NAME, FLG_IND
FROM             [$(DataHubDatabaseName)].exposition.PV_SF_OPPORTUNITY AS opp LEFT OUTER JOIN
                          [$(DataHubDatabaseName)].dbo.REF_BOUTIQUE AS bou ON bou.Code_ADV_Orange = opp.COD_BOUT LEFT OUTER JOIN
                         DIM_VENDEUR AS ven ON ven.SELLER_UID = opp.COD_VENDEUR
WHERE        (FLG_IND = 'OUI') AND (NUM_PROC_SOUS IS NULL) AND (LIB_ENT_DIS IN ('OF-WEB')) AND (LIB_STADE_VENTE = 'Compte ouvert') AND (COD_OFF_COM = 'OC80') AND (CASE WHEN @DTE_CLO_OPPRT IS NULL 
                         THEN CAST(getdate() AS date) ELSE CAST(DTE_CLO_OPPRT AS date) END >= CASE WHEN @DTE_CLO_OPPRT IS NULL THEN CAST(getdate() AS date) ELSE CAST(@DTE_CLO_OPPRT AS date) END) AND 
                         (CASE WHEN @DTE_CREA IS NULL THEN CAST(getdate() AS date) ELSE CAST(DTE_CREA AS date) END >= CASE WHEN @DTE_CREA IS NULL THEN CAST(getdate() AS date) ELSE CAST(@DTE_CREA AS date) 
                         END)
UNION ALL
SELECT       5 as ID_Type, 'Renvoi Trafic' AS Type, CAST(DTE_CLO_OPPRT AS date) AS 'date clôture', YEAR(DTE_CLO_OPPRT) AS ANNEE,MONTH(DTE_CLO_OPPRT) AS MOIS, DATEPART(week, DTE_CLO_OPPRT) AS Semaine, NUM_OPPRT, DO, AD, Site_Libelle, COD_VENDEUR, 
                         ven.NAME, FLG_IND
FROM             [$(DataHubDatabaseName)].exposition.PV_SF_OPPORTUNITY AS opp LEFT OUTER JOIN
                          [$(DataHubDatabaseName)].dbo.REF_BOUTIQUE AS bou ON bou.Code_ADV_Orange = opp.COD_BOUT LEFT OUTER JOIN
                         DIM_VENDEUR AS ven ON ven.SELLER_UID = opp.COD_VENDEUR
WHERE        (LIB_CAN_INT_ORI = 'Web OB') AND (LIB_ENT_DIS IN ('OF-WEB')) AND (LIB_STADE_VENTE = 'Compte ouvert') AND (COD_OFF_COM = 'OC80') AND (CASE WHEN @DTE_CLO_OPPRT IS NULL THEN CAST(getdate()
                          AS date) ELSE CAST(DTE_CLO_OPPRT AS date) END >= CASE WHEN @DTE_CLO_OPPRT IS NULL THEN CAST(getdate() AS date) ELSE CAST(@DTE_CLO_OPPRT AS date) END) AND 
                         (CASE WHEN @DTE_CREA IS NULL THEN CAST(getdate() AS date) ELSE CAST(DTE_CREA AS date) END >= CASE WHEN @DTE_CREA IS NULL THEN CAST(getdate() AS date) ELSE CAST(@DTE_CREA AS date) 
                         END)

END;