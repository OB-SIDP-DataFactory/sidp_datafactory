﻿-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PKGMC_PROC_clients]
    @nb_collected_rows int output
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

	-- suppression des ciblés qui sont toujours dans le parcours et qui ont été contactés la semaine passée 
--delete from [dbo].[SAS_Vue_MC_Clients] 
--where 
--ID_CONTACT in (select distinct ID_CONTACT from [dbo].WK_MC_CIBLES ) and TRANSFORME=0 ; 

truncate table [dbo].[SAS_Vue_MC_Clients] ; 

	-- pour chacun des ciblés, calcul de la date de premier contact et nb de contacts par canal depuis 

WITH CONTACTS AS
( select ID_CONTACT 
   
       , min(DATE) as DATE_FST
       , count(*) as NB_CONTACTS
       , sum(case when CANAL = 'Email' then 1 else 0 end) as NB_EMAILS
       , sum(case when CANAL = 'SMS'   then 1 else 0 end) as NB_SMS
       , sum(case when CANAL = 'Push'  then 1 else 0 end) as NB_PUSH
       , sum(case when CANAL = 'Call'  then 1 else 0 end) as NB_CALL
    from WK_MC_CIBLES
   group by ID_CONTACT  
),

	-- équipement des ciblés transformés 
   EQUIPMENT_ALL  AS 
	(select distinct ID_OPPORTUNITES
        , cp.LABEL
     from WK_MC_CIBLES cibles 
     left join [$(DataHubDatabaseName)].[dbo].[PV_FE_ENROLMENTFOLDER] enr on cibles.ID_OPPORTUNITES = enr.SALESFORCE_OPPORTUNITY_ID 
     left join [$(DataHubDatabaseName)].[dbo].[PV_FE_EQUIPMENT] equ on enr.ID_FE_ENROLMENT_FOLDER = equ.ENROLMENT_FOLDER_ID  
     join [$(DataHubDatabaseName)].[dbo].[PV_FE_COMMERCIALPRODUCT] cp on equ.PRODUCT_ID = cp.ID_FE_COMMERCIALPRODUCT
    ) ,
	EQUIPMENT_POS  AS
	(SELECT ID_OPPORTUNITES,LABEL
	, ROW_NUMBER() OVER(PARTITION BY  ID_OPPORTUNITES ORDER BY LABEL ) AS POSITION 
	FROM EQUIPMENT_ALL       
    ),
	EQUIPMENT  AS
	(
	SELECT DISTINCT  E1.ID_OPPORTUNITES,CASE WHEN E2.ID_OPPORTUNITES IS NULL THEN E1.LABEL ELSE E1.LABEL+' + '+E2.LABEL END AS LABEL
	FROM EQUIPMENT_POS as E1 LEFT OUTER JOIN EQUIPMENT_POS as E2 ON E1.ID_OPPORTUNITES=E2.ID_OPPORTUNITES AND E1.LABEL<>E2.LABEL
	WHERE E1.POSITION=1
	)

insert into [dbo].[SAS_Vue_MC_Clients] 

select distinct 
       cibles.ID_CONTACT, 
       cibles.ID_CONTACT as PK_ID, 
       cibles.TYPOLOGY_CAMPAIGN, 
       cibles.TYPOLOGY_NOTIFICATION,
       cast(cibles.DATE as date) as DATE, 
       replace(replace(concat(case when FC.NB_EMAILS = 0 then '-' else 'Email' end, '+',
                              case when FC.NB_SMS    = 0 then '-' else 'SMS'   end, '+',
                              case when FC.NB_PUSH   = 0 then '-' else 'Push'  end, '+',
                              case when FC.NB_CALL   = 0 then '-' else 'Call'  end ),'+-',''),'-+','') as CANAL, 
       isnull(acnt.GeolifeSegment__c,'Non-renseigné') as SEGMENT_GEOLIFE, 
	   case when (acnt.Preattribution__c is not null and charindex('.',acnt.Preattribution__c)<>0)  then cast(substring(acnt.Preattribution__c,1, charindex('.',acnt.Preattribution__c)-1) as int) else acnt.Preattribution__c end as PRE_ATTRIBUTION,
       isnull(sdv.COD_STA_VTE+' - '+sdv.LIB_STA_VTE,'Non-renseigné') AS STATUT_OPPORT, 
       sdv_to_date.COD_STA_VTE+' - '+sdv_to_date.LIB_STA_VTE AS STATUT_OPPORT_10, 
       cibles.OFFER_CODE as OFFER_CODE,
       cibles.OFFER_LABEL as OFFER_LABEL, 
       1 as CPT, 
	   
       (case when sdv_to_date.COD_STA_VTE = '09' then 1 else 0 end)  as TRANSFORME, 
       (case when sdv_to_date.COD_STA_VTE = '09' then 1 else 0 end)  as SORTIE_POSITIVE, 
       (case when sdv_to_date.COD_STA_VTE in ('10','11') then 1 else 0 end)  as SORTIE_NEGATIVE, 

       DATEDIFF(YEAR,acnt.PersonBirthdate,SYSDATETIME()) as AGE, 
       cast(opty_to_date.RelationEntryScore__c as integer) as SCORE_ENTREE_RELATION, 
       isnull(opty_to_date.Indication__c,'Non-renseigné') as INDICATION, 
       isnull(can.LIB_CAN_INT,'Non-renseigné') as CANAL_ORIGINE, 
       ISNULL(ss_res.LIB_SS_RES,'Non-renseigné') as ENTITE_DISTRIB, 
	   FC.NB_EMAILS AS NB_EMAILS, 
       FC.NB_SMS AS NB_SMS, 
       FC.NB_PUSH AS NB_PUSH, 
       FC.NB_CALL AS NB_APPELS, 
       FC.NB_CONTACTS, 
       EQU.LABEL as EQUIPEMENT ,
	   cibles.MESSAGE_NAME, 
	   case OriginalEvent__c when '01' then 'A préciser' when '02' then 'Transfert CRC OF' when '03' then 'Rebond CRC OB' when '04' then 'Web call back' when '05' then 'Campagne commerciale' when '06' then 'Pub/presse/TV' when '07' then 'Partenariat' when '08' then 'Parrainage' end as EVE_ORIGINE,
       acnt.EmployeeType__pc as QUALIFICATION_EMPLOYE
  from WK_MC_CIBLES cibles 
  join CONTACTS FC
    on FC.ID_CONTACT = cibles.ID_CONTACT and FC.DATE_FST = cibles.DATE
  left join [$(DataHubDatabaseName)].[dbo].[PV_SF_ACCOUNT] acnt
    on acnt.PersonContactId = cibles.ID_CONTACT 
  left join [DIM_STADE_VTE] sdv
    on sdv.COD_STA_VTE = cibles.STATUT_OPPORT 

 left join ( 
  select AccountId,Id_SF,StageName,StartedChannel__c,DistributorEntity__c,RelationEntryScore__c,Indication__c, OriginalEvent__c, row_number() over (partition by accountid order by createddate desc) as Validity_Flag 
  from [$(DataHubDatabaseName)].[dbo].[PV_SF_OPPORTUNITY] where CommercialOfferCode__c='OC80')  opty_to_date 
  on (opty_to_date.AccountId = acnt.Id_SF and opty_to_date.Validity_Flag=1) 

  left join [DIM_STADE_VTE] sdv_to_date
    on sdv_to_date.COD_STA_VTE = opty_to_date.StageName 
  
  left join [DIM_CANAL_INT] can
    on can.COD_CAN_INT = opty_to_date.StartedChannel__c  
  left join [DIM_SS_RES] ss_res
    on ss_res.COD_SS_RES = opty_to_date.DistributorEntity__c 
  left outer join EQUIPMENT EQU on EQU.ID_OPPORTUNITES = opty_to_date.ID_SF

where cibles.OFFER_CODE = 'OC80'
select @nb_collected_rows = COUNT(*) from [dbo].[SAS_Vue_MC_Clients];
END