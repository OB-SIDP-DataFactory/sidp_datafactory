﻿CREATE PROC [dbo].[PKGCP_SUIVI_CREDIT]
 @date_obs date
 
AS  BEGIN 
--declare @date_obs date = '2018-01-16';
--select DATEADD(DD,-30,@date_obs)
 

with  
cte_jour_alim_list AS
( SELECT T1.StandardDate AS jour_alim
    FROM [dbo].[DIM_TEMPS] T1
    WHERE T1.StandardDate >= CAST(DATEADD(day, -179, @date_obs) AS DATE) AND T1.StandardDate <= @date_obs
)
--select * from  cte_jour_alim_list order by jour_alim desc   ;

,Suivi_Credit_All as (
select m.[ID_CRE_OPPORTUNITE]
	   ,m.[DTE_ALIM]
	   ,m.[IDE_OPPRT_SF]
	   ,m.[STADE_VENTE]
	   ,m.[LIB_STADE_VENTE]
	   ,cast (m.[DTE_CREA] as date) as [DTE_CREA] 
	   ,case when m.AGE_OPPRT between 0 and 60 then 1
			 when m.AGE_OPPRT between 61 and 90 then 2
			 when m.AGE_OPPRT between 91 and 120 then 3
			 when m.AGE_OPPRT between 121 and 180 then 4
		     else 5 end  as  AGE_OPPORT_MOIS
	   ,case when cast (m.[DTE_CREA] as date) between DATEADD(DD,-30,@date_obs) and @date_obs then 1 else 0 end as [Flag_30_J]
	   ,m.[DTE_CLO_OPPRT]
	   ,m.[AGE_OPPRT]
	   ,m.[COD_OFF_COM] ---***
	   ,m.[LABEL_OFF_COM]
	   ,m.[COD_CAN_DIS]
	   ,m.[LIB_CAN_DIS]
	   ,m.[COD_RES_DIS]
	   ,m.[LIB_RES_DIS]
	   ,m.[ENT_DIS]
	   ,m.[LIB_ENT_DIS]
	   ,m.[COD_CAN_INT]
	   ,m.[LIB_CAN_INT]
	   ,m.[CAN_INT_FIN]
	   ,m.[SS_RES_FIN]
	   ,m.[DTE_DER_MOD]
	   ,m.[CAN_INT_ORI_IND]
	   ,m.[LIB_CAN_INT_ORI_IND]
	   ,m.[COD_CAN_INT_ORI]
	   ,m.[LIB_CAN_INT_ORI]
	   ,m.[DTE_ACCORD]
	   ,m.[DELAI_ACCORD]
	   ,m.[ENT_APPROB]
	   ,m.[TX_OFFRE]
	   ,m.[PRESCRIPTEUR]
	   ,m.[MTT_DEBLOQUE]
	   ,m.[DTE_DEBLOCAGE]
	   ,m.[DELAI_DEBLOCAGE]
	   ,m.[DTE_DEB]
	   ,m.[FLG_DER_ETAT_OPPRT]
	   ,m.[FLG_DER_STADE_VENTE_OPPRT]
 from [dbo].[CRE_OPPORTUNITE] m 
 where m.[FLG_DER_ETAT_OPPRT] = 1
 and cast (m.[DTE_CREA] as date) between DATEADD(dd,-179,cast(@date_obs as date)) and @date_obs  
)

--select  * from Suivi_Credit_All --where AGE_OPPORT_MOIS = 5;

--select DATEDIFF(DD,'2018-01-13','2017-08-07')
--select distinct STADE_VENTE,LIB_STADE_VENTE from Suivi_Credit_All ;

,cre as (select 
	 DTE_CREA,AGE_OPPORT_MOIS,[Flag_30_J]
	 /* Flux quotidiens global chaque jour*/
     , count(IDE_OPPRT_SF) as nb_opp_created--Opportunités nouvelles créées Q
	 , count(CASE when /*DTE_DER_DEB IS NULL and */STADE_VENTE='14' then [IDE_OPPRT_SF] end ) as nb_credits_accordes--Crédits accordés Q
	 , count(CASE when /*DTE_DER_DEB IS NOT NULL and*/ STADE_VENTE='15' then [IDE_OPPRT_SF] end) as nb_credits_decaisses--Crédits décaissés Q
	 /*Opportunités en cours Q*/
	 , count(case when stade_vente in ('01','02','03','04','05','06','07') then IDE_OPPRT_SF else null end) as nb_opport_encours--Opportunités en cours Q
	 , count(case when stade_vente = '01' then IDE_OPPRT_SF else null end) as nb_opport_detection--Détection Q
	 , count(case when stade_vente = '02' then IDE_OPPRT_SF else null end) as nb_opport_decouverte--Découverte Q
	 , count(case when stade_vente = '03' then IDE_OPPRT_SF else null end) as nb_opport_elaboration--Elaboration proposition Q
	 , count(case when stade_vente = '04' then IDE_OPPRT_SF else null end) as nb_opport_signee-- Affaire signée en attente Q
	 , count(case when stade_vente = '05' then IDE_OPPRT_SF else null end) as nb_opport_verification--Vérification banque Q
	 , count(case when stade_vente = '06' then IDE_OPPRT_SF else null end) as nb_opport_incomplet--Dossier incomplet Q
	 , count(case when stade_vente = '07' then IDE_OPPRT_SF else null end) as nb_opport_valide--Dossier validé Q
	 /*Opportunités perdues*/
	 , count(case when stade_vente in ('08','10','11','12','16') then IDE_OPPRT_SF else null end) as nb_opport_perdues--Opportunités perdues Q 
	 /*Flux quotidiens via OB-- Origine selfcare Q*/
	 , count(case when  COD_CAN_INT_ORI in ('10','11') then IDE_OPPRT_SF else NULL end) as nb_opp_created_ob_self--Opportunités créées Q
	 , count(case when /*DTE_DER_DEB IS NULL and */STADE_VENTE='14' and COD_CAN_INT_ORI  in ('10','11') then [IDE_OPPRT_SF] end ) as nb_credits_accordes_ob_self--Crédits accordés Selfcare Q
	 , count(case when /*DTE_DER_DEB IS NOT NULL and */STADE_VENTE='15' and COD_CAN_INT_ORI in ('10','11') then [IDE_OPPRT_SF] end ) as nb_credits_decaisses_ob_self--Crédits décaissés Selfcare Q
	 /*Flux quotidiens via OB-- Origine CRC*/
	 , count(case when  COD_CAN_INT_ORI in ('12','13','14','15') then IDE_OPPRT_SF else NULL end) as nb_opp_created_ob_CRC --Opportunités créées
	 , count(CASE when /*DTE_DER_DEB IS NULL and */STADE_VENTE='14' and COD_CAN_INT_ORI in ('12','13','14','15') then [IDE_OPPRT_SF] end ) as nb_credits_accordes_ob_CRC--Crédits accordés CRC Q
	 , count(CASE when /*DTE_DER_DEB IS NOT NULL and*/ STADE_VENTE='15' and COD_CAN_INT_ORI in ('12','13','14','15') then [IDE_OPPRT_SF] end ) as nb_credits_decaisses_ob_CRC--Crédits décaissés CRC Q
from Suivi_Credit_All
--where DTE_CREA >= DATEADD(DD,-30,@date_obs)
group by DTE_CREA,AGE_OPPORT_MOIS,[Flag_30_J]
)

--select * from cre order by DTE_CREA;

,cre_cum as (
select  DTE_CREA,AGE_OPPORT_MOIS,[Flag_30_J]
     /*Flux quotidiens global sur 30j*/
     , Sum(nb_opp_created) as nb_opp_created_cum--Opportunités nouvelles créées sur 30j
	 , Sum(nb_credits_accordes) as nb_credits_accordes_cum--Crédits accordés sur 30j
	 , Sum(nb_credits_decaisses) as nb_credits_decaisses_cum--Crédits décaissés sur 30j
	 /**Opportunités en cours sur 30j*/
	 , Sum(nb_opport_encours) as nb_opport_encours_cum--Opportunités en cours sur 30j
	 /*Opportunités en cours sur 30j*/
	 , Sum(nb_opport_detection) as nb_opport_detection_cum--Détection sur 30j
	 , Sum(nb_opport_decouverte) as nb_opport_decouverte_cum--Découverte sur 30j
	 , Sum(nb_opport_elaboration) as nb_opport_elaboration_cum--Elaboration proposition sur 30j 
	 , Sum(nb_opport_signee) as nb_opport_signee_cum--Affaire signée en attente sur 30j
	 , Sum(nb_opport_verification) as nb_opport_verification_cum--Vérification banque sur 30j
	 , Sum(nb_opport_incomplet) as nb_opport_incomplet_cum--Dossier incomplet sur 30 j
	 , Sum(nb_opport_valide) as nb_opport_valide_cum--Dossier validé sur 30j
	 /*Opportunités perdues sur 30j*/
	 , Sum(nb_opport_perdues) as nb_opport_perdues_cum--Opportunités perdues sur 30j
	 /*Flux quotidiens via OB--Origine selfcare*/
	 , Sum(nb_opp_created_ob_self) as nb_opp_created_ob_self_cum--Opportunités créées sur 30j
	 , Sum(nb_credits_accordes_ob_self) as nb_credits_accordes_ob_self_cum-- Crédits accordés self sur 30j
	 , Sum(nb_credits_decaisses_ob_self) as nb_credits_decaisses_ob_self_cum-- Crédits décaissés self sur 30j
	 /* Flux quotidiens via OB--Origine CRC*/
	 , Sum(nb_opp_created_ob_CRC) as nb_opp_created_ob_CRC_cum--Opportunités créés sur 30j
	 , Sum(nb_credits_accordes_ob_CRC) as nb_credits_accordes_ob_CRC_cum--Crédits accordés CRC sur 30j
	 , Sum(nb_credits_decaisses_ob_CRC) as nb_credits_decaisses_ob_CRC_cum--Crédits accordés CRC sur 30j

  from  cre
  group by 
     DTE_CREA ,AGE_OPPORT_MOIS,[Flag_30_J]
)

--select * from cre_cum where DTE_CREA = '2018-01-15';

,cre_fin as(

select   x.jour_alim
        ,c.DTE_CREA
        ,ISNULL(c.[Flag_30_J]                          ,1) as [Flag_30_J]
        ,ISNULL(c.AGE_OPPORT_MOIS                      ,0) as AGE_OPPORT_MOIS 
		,ISNULL(nb_opp_created_cum					   ,0) as nb_opp_created_cum
		,ISNULL(nb_credits_accordes_cum				   ,0) as nb_credits_accordes_cum
		,ISNULL(nb_credits_decaisses_cum			   ,0) as nb_credits_decaisses_cum
		,ISNULL(nb_opport_encours_cum				   ,0) as nb_opport_encours_cum
		,ISNULL(nb_opport_detection_cum				   ,0) as nb_opport_detection_cum
		,ISNULL(nb_opport_decouverte_cum			   ,0) as nb_opport_decouverte_cum
		,ISNULL(nb_opport_elaboration_cum			   ,0) as nb_opport_elaboration_cum
		,ISNULL(nb_opport_signee_cum				   ,0) as nb_opport_signee_cum
		,ISNULL(nb_opport_verification_cum			   ,0) as nb_opport_verification_cum
		,ISNULL(nb_opport_incomplet_cum				   ,0) as nb_opport_incomplet_cum
		,ISNULL(nb_opport_valide_cum				   ,0) as nb_opport_valide_cum
		,ISNULL(nb_opport_perdues_cum				   ,0) as nb_opport_perdues_cum
		,ISNULL(nb_opp_created_ob_self_cum			   ,0) as nb_opp_created_ob_self_cum
		,ISNULL(nb_credits_accordes_ob_self_cum		   ,0) as nb_credits_accordes_ob_self_cum
		,ISNULL(nb_credits_decaisses_ob_self_cum	   ,0) as nb_credits_decaisses_ob_self_cum
		,ISNULL(nb_opp_created_ob_CRC_cum			   ,0) as nb_opp_created_ob_CRC_cum
		,ISNULL(nb_credits_accordes_ob_CRC_cum		   ,0) as nb_credits_accordes_ob_CRC_cum
		,ISNULL(nb_credits_decaisses_ob_CRC_cum		   ,0) as nb_credits_decaisses_ob_CRC_cum
	
		from cte_jour_alim_list x 
		left join cre_cum c on  x.jour_alim = c.DTE_CREA
			
)
select   * from cre_fin order by jour_alim desc ;
END
