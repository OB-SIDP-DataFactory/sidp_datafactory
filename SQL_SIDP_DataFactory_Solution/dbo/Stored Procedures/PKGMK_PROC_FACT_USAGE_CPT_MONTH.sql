﻿

CREATE PROC [dbo].[PKGMK_PROC_FACT_USAGE_CPT_MONTH] 
    @DATE_ALIM  DATE  
  , @mois_histo INT         -- valeur 0 par défaut = traitement nominal par défaut
  , @nbRows     INT OUTPUT  -- nb lignes processées
AS  
BEGIN 

if @DATE_ALIM is null 
  set @DATE_ALIM = GETDATE();
 
if @mois_histo is null 
  set @mois_histo = 0; --valeur 0 par défaut = traitement nominal par défaut

  set  @DATE_ALIM = DATEADD(dd,-1,@DATE_ALIM); -- Pour gérer le dernier jours du mois (données a j-1 dans la table Quoti)
 --------------------------------------///////////////// Step 1
--calcul de la période de chargement
IF OBJECT_ID('tempdb..#WK_TEMPS_ACCOUNT') IS NOT NULL
BEGIN DROP TABLE #WK_TEMPS_ACCOUNT END

 
select Year, Month, max(StandardDate) as StandardDate
  into #WK_TEMPS_ACCOUNT
  from [dbo].DIM_TEMPS
 where StandardDate <= eomonth(@DATE_ALIM)
   and StandardDate >= dateadd(month, -@mois_histo, eomonth(@DATE_ALIM))
 group by Year, Month

 --select * from #WK_TEMPS_ACCOUNT

--nettoyage de la table cible (suppréssion des lignes a recharger)
delete from [dbo].[T_FACT_MKT_USAGE_CPT_MONTH]
where [MOIS_TRAITEMENT] IN (select concat(Year,Month) from #WK_TEMPS_ACCOUNT)

Insert into [dbo].[T_FACT_MKT_USAGE_CPT_MONTH]
(
  [MOIS_TRAITEMENT] 
 ,[NUM_CPT_SAB]     
 ,[TYP_MVT]         
 ,[SOUS_TYP_MVT]  
 ,[NOMBRE_OP]       
 ,[MONTANT_OP]   
 ,[FLAG_MTT_OPT] 
 ,[FLAG_INITIATIVE_CLIENT]
 ,[FLAG_OP_DEB_CRE]   
 ,[DT_DER_MVT]      
 ,[TYP_DER_MVT]     
 ,[SOUS_TYP_DER_MVT]
 ,[DT_PRE_UTI_PM]   
 ,[DT_PRE_UTI_CB]   
 ,[DT_PRE_OPE_DEB]  
 ,[DT_PRE_OPE_CRE]  
 ,[DT_DER_UTI_PM]   
 ,[DT_DER_UTI_CB]   
 ,[DT_DER_OPE_DEB]  
 ,[DT_DER_OPE_CRE]  
 ,[DT_VRS_PRI_BVN]       
)

select format(cast(StandardDate as date),'yyyyMM') as IDE_TPS      
	 , c.NUM_CPT_SAB 
	 , c.TYP_MVT 
	 , c.SOUS_TYP_MVT 
	 , Sum(c.NOMBRE_OP) as NOMBRE_OP
	 , Sum(c.MONTANT_OP) as MONTANT_OP 
	 , c.FLAG_MTT_OPT 
	 , c.FLAG_INITIATIVE_CLIENT
     , c.FLAG_OP_DEB_CRE   
     , max(m.DT_DER_MVT)       as [DT_DER_MVT] 
     , max(m.TYP_DER_MVT)      as [TYP_DER_MVT]
	 , max(m.SOUS_TYP_DER_MVT) as [SOUS_TYP_DER_MVT]
     , min(m.[DT_PRE_UTI_PM])    as [DT_PRE_UTI_PM]
	 , min(m.[DT_PRE_UTI_CB] )   as [DT_PRE_UTI_CB]    
     , min(m.[DT_PRE_OPE_DEB])   as [DT_PRE_OPE_DEB]
     , min(m.[DT_PRE_OPE_CRE])   as [DT_PRE_OPE_CRE]
     , max(m.[DT_DER_UTI_PM])    as [DT_DER_UTI_PM]
	 , max(m.[DT_DER_UTI_CB])    as [DT_DER_UTI_CB]
	 , max(m.[DT_DER_OPE_DEB])   as [DT_DER_OPE_DEB]
	 , max(m.[DT_DER_OPE_CRE])   as [DT_DER_OPE_CRE]
	 , Min(m.[DT_VRS_PRI_BVN])   as [DT_VRS_PRI_BVN]
from [dbo].[T_FACT_MKT_USAGE_CPT_DAY] c 
join #WK_TEMPS_ACCOUNT t on t.StandardDate = eomonth(c.[DTE_OPE])

left join (select m.NUM_CPT_SAB,
                  format(cast([DTE_OPE] as date),'yyyyMM') as TPS,
				  m.DT_DER_MVT DT_DER_MVT,
                  m.TYP_DER_MVT TYP_DER_MVT ,
				  m.SOUS_TYP_DER_MVT SOUS_TYP_DER_MVT,
		          m.DT_PRE_UTI_PM  DT_PRE_UTI_PM,
				  m.DT_PRE_UTI_CB DT_PRE_UTI_CB,
				  m.DT_PRE_OPE_DEB DT_PRE_OPE_DEB,
				  m.DT_PRE_OPE_CRE DT_PRE_OPE_CRE,
				  m.DT_DER_UTI_PM  DT_DER_UTI_PM,
				  m.DT_DER_UTI_CB  DT_DER_UTI_CB,
				  m.DT_DER_OPE_DEB DT_DER_OPE_DEB,
				  m.DT_DER_OPE_CRE DT_DER_OPE_CRE,
				  m.DT_VRS_PRI_BVN DT_VRS_PRI_BVN,
		          row_number() over (partition by m.NUM_CPT_SAB,format(cast([DTE_OPE] as date),'yyyyMM') order by m.[DTE_OPE] desc) as LAST
		   from  [dbo].[T_FACT_MKT_USAGE_CPT_DAY] m
		   ) m on m.NUM_CPT_SAB=c.NUM_CPT_SAB and format(cast(StandardDate as date),'yyyyMM')= m.TPS 
where m.LAST = 1

group by format(cast(StandardDate as date),'yyyyMM') 
		,c.NUM_CPT_SAB 
		,c.TYP_MVT 
		,c.SOUS_TYP_MVT  
		,c.FLAG_MTT_OPT
		,c.FLAG_INITIATIVE_CLIENT
        ,c.FLAG_OP_DEB_CRE     
  

SELECT @nbRows = @@ROWCOUNT

/*Solution Temporaire*/ 
  delete
  FROM [dbo].[T_FACT_MKT_USAGE_CPT_MONTH]
  where TYP_MVT is  null and DT_DER_MVT is not null
/***/

END