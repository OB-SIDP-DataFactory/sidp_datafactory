﻿CREATE PROC [dbo].[PKGMK_PROC_ferm_cpt] @nbRows int OUTPUT -- nb lignes insérées dans la table finale 
AS  BEGIN 
-- ===============================================================================================================================================================================
-- tables input [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_CLIENT]
--              [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_COMPTE]
--              [$(DataHubDatabaseName)].[dbo].[PV_SF_ACCOUNT]
--              [$(DataHubDatabaseName)].[dbo].[PV_SF_OPPORTUNITY_HISTORY]
--              [$(DataHubDatabaseName)].[dbo].[PV_FE_EQUIPMENT]
--              [$(DataHubDatabaseName)].[dbo].[PV_FE_COMMERCIALPRODUCT]
--              [$(DataHubDatabaseName)].[dbo].[PV_FE_COMMERCIALOFFER]
--              [MKT_FACTURATION_INACT] 
--              [DIM_TEMPS]
--              [DIM_MOTIF_FERMETURE]
--              [DIM_CSP_CLI]
--              [DIM_CANAL]
--              [DIM_RESEAU]
--              [DIM_PRODUIT_OFFRE]
-- tables output : [sas_vue_ferm_cpt]
-- description : construction de la vue sas des comptes fermés. flux : ajouter (append) les fermétures au niveau compte, pour toutes les dates où il existe au moins une clôture
-- périmètre restraint sur l'offre Orange Bank par une jointure avec les comptes ouverts via Front End
-- =============================================================================================================================================================================

---------------------------------------------------------------------------------------------------
-- chargement #wk_ferm_cpt_init avec les infos à date des comptes Orange Bank
---------------------------------------------------------------------------------------------------
if object_id('tempdb..#wk_ferm_cpt_init') is not null
begin drop table #wk_ferm_cpt_init end

declare @RecordTypeClient varchar(18)
select @RecordTypeClient = CODE_SF from [dbo].[DIM_RECORDTYPE] where LIBELLE = 'Personne Physique Client'

select sa_q_client.DWHCLICLI as NUM_CLI
     , sa_q_client.DWHCLIDNA as date_nais_cli
     , sa_q_compte.DWHCPTCOM as NUM_CPT
     , row_number() over (partition by sa_q_compte.DWHCPTCOM order by sf_opport.CreatedDate) as FLAG_CPT
     , sa_q_compte.DWHCPTDAO as date_ouvert_cpt
     , sa_q_compte.DWHCPTDAC as date_ferm_cpt
     , sa_q_compte.DWHCPTCLO as motif_ferm_cpt
     , sa_q_compte.DWHCPTRUB as code_sab_type_compte
     , p.CODE as code_fe_type_compte
     , o.CODE as code_fe_offre 
     , sf_client.Id_SF_Account
     , sf_client.IDCustomerSAB__pc as Id_SF_SAB
     , sf_client.Occupation__pc
	 , sf_client.OccupationNiv1__pc 
	 , sf_client.OccupationNiv3__pc 
     , sf_opport.Id_SF_Opp
	 , case when sf_opport.Indication__c='Oui' then sf_opport.StartedChannelForIndication__c else sf_opport.StartedChannel__c end as StartedChannel__c 
     , sf_opport.DistributionChannel__c
     , sf_opport.DistributorNetwork__c
     , f.cum_mtt_facture
  into #wk_ferm_cpt_init
  from ( select DWHCPTPPAL, DWHCPTCOM, DWHCPTRUB, DWHCPTDAO, DWHCPTDAC, DWHCPTCLO
              , row_number() over (partition by DWHCPTCOM order by Validity_StartDate desc) as Validity_Flag
           from [$(DataHubDatabaseName)].[dbo].[VW_PV_SA_Q_COMPTE]
          where DWHCPTRUB in ('251180', '254181', '254111') /* pour isoler les comptes Orange Bank */ 
            and DWHCPTDAC is not null /* pour isoler les comptes fermés */ ) sa_q_compte

  join ( select DWHCLICLI, DWHCLIDNA, DWHCLIDCR, DWHCLICLO
              , row_number() over (partition by DWHCLICLI order by Validity_StartDate desc) as Validity_Flag
           from [$(DataHubDatabaseName)].[dbo].[VW_PV_SA_Q_CLIENT] ) sa_q_client
    on sa_q_client.DWHCLICLI = sa_q_compte.DWHCPTPPAL and sa_q_client.Validity_Flag = sa_q_compte.Validity_Flag 

  join ( select Id_SF as Id_SF_Account, IDCustomerSAB__pc, Occupation__pc, OccupationNiv1__pc, OccupationNiv3__pc, OBFirstContactDate__c, Preattribution__c, RecordTypeId
              , row_number() over (partition by Id_SF order by Validity_StartDate desc) as Validity_Flag
           from [$(DataHubDatabaseName)].[dbo].[VW_PV_SF_ACCOUNT] ) sf_client
    on sf_client.IDCustomerSAB__pc = sa_q_client.DWHCLICLI and sf_client.Validity_Flag = sa_q_client.Validity_Flag and sf_client.RecordTypeId = @RecordTypeClient

  left join ( select Id_SF as Id_SF_Opp, AccountId as Id_SF_Account, StartedChannel__c, StartedChannelForIndication__c, Indication__c, DistributionChannel__c, DistributorNetwork__c, CreatedDate
                   , row_number() over (partition by Id_SF order by Validity_StartDate desc) as Validity_Flag
                from [$(DataHubDatabaseName)].[dbo].[VW_PV_SF_OPPORTUNITY_HISTORY] 
               where StageName = '09' and CommercialOfferCode__c = 'OC80' ) sf_opport
    on sf_opport.Id_SF_Account = sf_client.Id_SF_Account and sf_opport.Validity_Flag = 1

  left join [MKT_FACTURATION_INACT] f
    on sa_q_compte.DWHCPTCOM = f.NUM_CPT and cast(sa_q_compte.DWHCPTDAC as date) = f.date_alim -- cumul montant facturé dans les 12 mois glissantes précédant la clôture du compte

  left join [$(DataHubDatabaseName)].[dbo].[PV_FE_EQUIPMENT] b 
    on b.EQUIPMENT_NUMBER = sa_q_compte.DWHCPTCOM 
  left join [$(DataHubDatabaseName)].[dbo].[PV_FE_COMMERCIALPRODUCT] p
    on p.ID_FE_COMMERCIALPRODUCT = b.PRODUCT_ID
  left join [$(DataHubDatabaseName)].[dbo].[PV_FE_PRODUCTOFFERASSOC] pao
    on pao.COMMERCIAL_PRODUCT_ID = p.ID_FE_COMMERCIALPRODUCT
  left join [$(DataHubDatabaseName)].[dbo].[PV_FE_COMMERCIALOFFER] o
    on o.ID_FE_COMMERCIALOFFER = pao.COMMERCIAL_OFFER_ID 

 where sa_q_compte.Validity_Flag = 1;

---------------------------------------------------------------------------------------------------
-- supprimer dates à insérer eventuellement déjà présentes
---------------------------------------------------------------------------------------------------
declare @date_min date;
select @date_min = min(cast(date_ferm_cpt as date)) from #wk_ferm_cpt_init;

declare @date_max date; 
select @date_max = getdate();

delete from [sas_vue_ferm_cpt]
where date_alim >= @date_min and date_alim <= @date_max

---------------------------------------------------------------------------------------------------
-- reconstitution de sas_vue_ferm_cpt
---------------------------------------------------------------------------------------------------
--jointure tables dimension 
insert into [sas_vue_ferm_cpt]
select t.date_ferm_cpt as date_alim
     , t.NUM_CPT
     , t.Id_SF_Opp
     , t.Id_SF_Account
     , DATEDIFF(yy, t.date_nais_cli, t.date_ferm_cpt) - 
	   CASE WHEN MONTH(t.date_ferm_cpt) < MONTH(t.date_nais_cli) THEN 1
            WHEN MONTH(t.date_ferm_cpt) > MONTH(t.date_nais_cli) THEN 0
            WHEN DAY(t.date_ferm_cpt) < DAY(t.date_nais_cli) THEN 1
            ELSE 0 END AS age_cli -- différence en nombre complet d'années

      , CASE WHEN DATEPART(DAY, t.date_ouvert_cpt) > DATEPART(DAY, t.date_ferm_cpt)
            THEN DATEDIFF(MONTH, t.date_ouvert_cpt, t.date_ferm_cpt) - 1
            ELSE DATEDIFF(MONTH, t.date_ouvert_cpt, t.date_ferm_cpt) END AS age_cpt -- différence en nombre complet de mois

     , isnull(t.cum_mtt_facture, 0) as cum_mtt_facture
     , isnull(d1.MOTIF_FERMETURE, 'Non renseigné') as MOTIF_FERMETURE
     , isnull(d1.MOTIF_FERMETURE2, 'Non renseigné') as regroup_mot_ferm
     , isnull(d2.CSP_CLI, 'Non renseigné') as CSP_CLI
	 , isnull(dH1.LIB_CSP_NIV1, 'Non renseigné') as CSP_CLI_NIV1
     , isnull(dH3.LIB_CSP_NIV3, 'Non renseigné') as CSP_CLI_NIV3
     , d3.StandardDate as  date_fermeture
     , isnull(d7.LIBELLE, 'Non renseigné') as canal_interaction_origine
     , isnull(d8.LIBELLE, 'Non renseigné') as reseau_origine
     , isnull(d9.LIBELLE, 'Non renseigné') as type_compte
     , isnull(d9o.LIBELLE, 'Non renseigné') as offre_commerciale --prendre code offre table source, pas la correspondance donnée par la dimension
     , t.code_sab_type_compte
  from #wk_ferm_cpt_init t 
  left join [DIM_TEMPS] d3
    on t.date_ferm_cpt = d3.StandardDate
  left join [DIM_MOTIF_FERMETURE] d1
    on t.motif_ferm_cpt = d1.MOT_CLO and t.date_ferm_cpt BETWEEN d1.Validity_StartDate and d1.Validity_EndDate
  left join [DIM_CSP_CLI] d2 
    on t.Occupation__pc = d2.CSP_CLI and t.date_ferm_cpt BETWEEN d2.Validity_StartDate and d2.Validity_EndDate
  left join [DIM_CSP_NIV1] dH1 
    on t.OccupationNiv1__pc = dH1.COD_CSP_NIV1 
  left join [DIM_CSP_NIV3] dH3
    on t.OccupationNiv3__pc = dH3.COD_CSP_NIV3  
  left join [DIM_CANAL] d7
    on t.StartedChannel__c = d7.CODE_SF and d7.PARENT_ID is not null /* niveau canal interaction */ and t.date_ferm_cpt BETWEEN d7.Validity_StartDate and d7.Validity_EndDate
  left join [DIM_RESEAU] d8
    on t.DistributorNetwork__c = d8.CODE_SF and d8.PARENT_ID is null /* niveau réseau */ and t.date_ferm_cpt BETWEEN d8.Validity_StartDate and d8.Validity_EndDate
  left join [DIM_PRODUIT_OFFRE] d9
    on t.code_fe_type_compte = d9.CODE_FE and t.code_fe_offre = d9.PARENT_ID /* niveau produit commercial */
  left join [DIM_PRODUIT_OFFRE] d9o
    on t.code_fe_offre = d9o.CODE_FE and d9o.PARENT_ID is null /* niveau offre commerciale */
 where t.FLAG_CPT = 1
;

select @nbRows = @@ROWCOUNT;

END 
GO