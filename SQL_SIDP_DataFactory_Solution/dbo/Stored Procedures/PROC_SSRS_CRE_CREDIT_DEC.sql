﻿CREATE PROC [dbo].[PROC_SSRS_CRE_CREDIT_DEC]

AS  BEGIN 

DECLARE @date_obs DATE;
SELECT  @date_obs = CAST(GETDATE() AS DATE);
DECLARE @date_max date;
SELECT  @date_max = dateadd(day, -1 ,GETDATE());
SET DATEFIRST 1; -->1    Lundi
DROP TABLE IF EXISTS #rq_cre_dec_cp;
DROP TABLE IF EXISTS [dbo].[WK_SSRS_CRE_CREDIT_DEC];

SELECT Year_alim,Month_alim,Week_alim,jour_alim
    ,mtt_cre_decaisse
	--Week to date
	,sum(mtt_cre_decaisse) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS mtt_cre_decaisse_WTD --Week to date
	--Month to date
	,sum(mtt_cre_decaisse) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS mtt_cre_decaisse_MTD --Month to date
	--Full Month
	,sum(mtt_cre_decaisse) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS mtt_cre_decaisse_FM --Full Month
	--Year to date
    ,sum(mtt_cre_decaisse) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS mtt_cre_decaisse_YTD  --Year to date
	--Full year
	,sum(mtt_cre_decaisse) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS mtt_cre_decaisse_FY  --Full year
INTO #rq_cre_dec_cp
FROM  ( 
     SELECT  T1.StandardDate as jour_alim,DATEPART(YEAR,T1.Date) AS Year_alim,DATEPART(MONTH,T1.Date) AS Month_alim,DATEPART(YEAR,DATEADD(day, (7 - DATEPART(dw, T1.StandardDate)), T1.StandardDate)) *100 + DATEPART(WEEK,DATEADD(day, (7 - DATEPART(dw, T1.StandardDate)), T1.StandardDate)) AS Week_alim
	,sum(MTT_TOT_DEB) as mtt_cre_decaisse
FROM [dbo].[DIM_TEMPS] T1 
LEFT OUTER JOIN  [dbo].[CRE_CREDIT]  ON T1.StandardDate= CAST(CRE_CREDIT.DTE_DER_DEB AS DATE)
WHERE  ( T1.StandardDate >= CAST(DATEADD(yyyy, DATEDIFF(YYYY, 0, DATEADD(yyyy, -2, @date_obs)), 0) AS DATE)
AND T1.StandardDate <=  DATEADD(day, (7 - DATEPART(dw, @date_obs)), @date_obs) )
GROUP BY  T1.StandardDate ,DATEPART(YEAR,T1.Date) ,DATEPART(MONTH,T1.Date) ,DATEPART(WEEK,T1.Date) 
) t;	

SELECT
CY.jour_alim,CY.Year_alim,CY.Month_alim,CY.Week_alim
,coalesce(CY.mtt_cre_decaisse	  ,0) AS mtt_cre_decaisse
,coalesce(CY.mtt_cre_decaisse_WTD ,0) AS mtt_cre_decaisse_WTD
,coalesce(CY.mtt_cre_decaisse_MTD ,0) AS mtt_cre_decaisse_MTD
,coalesce(CY.mtt_cre_decaisse_YTD ,0) AS mtt_cre_decaisse_YTD
,coalesce(PM.mtt_cre_decaisse_FM ,0) AS mtt_cre_decaisse_FM
,coalesce(PY.mtt_cre_decaisse_FY ,0) AS mtt_cre_decaisse_FY
INTO [dbo].[WK_SSRS_CRE_CREDIT_DEC]
FROM #rq_cre_dec_cp CY --Current Year
 LEFT OUTER JOIN #rq_cre_dec_cp PM --Previous Year
		  ON DATEADD(MONTH,-1,CY.jour_alim) = PM.jour_alim
	 LEFT OUTER JOIN #rq_cre_dec_cp PY --Previous Year
		  ON DATEADD(YEAR,-1,CY.jour_alim) = PY.jour_alim
ORDER BY CY.jour_alim;
END