﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PKGMC_PROC_calllog]
	-- Add the parameters for the stored procedure here
	--@date_alim varchar(50),
	@nb_collected_rows int output
	--,@nb_merged_rows int output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--set @date_alim = CAST(@date_alim as date);

    /*   
	---@date_alim : parametre de date de la collecte
	*/
	
truncate table [dbo].[WK_MC_CALLLOG];

insert into [dbo].[WK_MC_CALLLOG] 

select 
task.Id_SF as ID_TASK, 
acnt.PersonContactId as ID_CONTACT, 
opp.Id_SF as ID_OPPORTUNITY, 
cas.Id_SF as ID_CASE, 
cas.IDCampagne__c as ID_CAMPAIGN, 
cas.Type as CASE_TYPE, 
cas.SubType__c as CASE_SUBTYPE, 
task.InteractionDate__c as CALL_DATE, 
RIGHT('0'+CAST(datepart(hour,task.InteractionDate__c) as nvarchar),2) as HEURE_APPEL, 
isnull(acnt.Preattribution__c,'Non-renseigné') as PRE_ATTRIBUTION, 
isnull(acnt.GeolifeSegment__c,'Non-renseigné') as SEGMENT_GEOLIFE, 
isnull(acnt.BehaviouralScoring__c,'Non-renseigné') as SCORE_COMPORTEMENT, 
DATEDIFF(YEAR,cast(acnt.PersonBirthdate as date),cast(getdate() as date)) as AGE, 
isnull(opp.StageName,'Non-renseigné') as STATUT_OPPORT , 
isnull(opp.RelationEntryScore__c,'Non-renseigné') as SCORE_ENTREE_RELATION, 
isnull(opp.CommercialOfferCode__c,'Non-renseigné') as OFFER_CODE, 
isnull(opp.CommercialOfferName__c,'Non-renseigné') as OFFER_NAME, 
isnull(can.LIBELLE,'Non-renseigné') as CANAL_ORIGINE, 
(case when cas.CallStatus__c in ('03','04') then 'Y' else 'N' end ) as FLG_CONTACTE , 
(case when cas.CallStatus__c = '03' then 'Y' else 'N' end ) as FLG_CONTACT_POSITIF, 
(case when cas.CallStatus__c = '05' and CallDetail__c in ('12','15','16','17') then 'Y' else 'N' end ) as FLG_CONTACT_BOUNCES, 
(case when cas.CallStatus__c = '06' then 'Y' else 'N' end ) as FLG_CONTACT_STOP, 
task.Validity_StartDate as DATE_CHARGEMENT  

from 
[$(DataHubDatabaseName)].[dbo].[PV_SF_TASK] task 

INNER JOIN [$(DataHubDatabaseName)].[dbo].[PV_SF_CASE] cas on (task.WhatId=cas.Id_SF and cas.IDCampagne__c is not null) 
LEFT JOIN [$(DataHubDatabaseName)].[dbo].[PV_SF_ACCOUNT] acnt on task.WhoId=acnt.PersonContactId   
LEFT JOIN [$(DataHubDatabaseName)].[dbo].[PV_SF_OPPORTUNITY] opp on cas.ParentOppy__c=opp.Id_SF 
LEFT JOIN [dbo].[DIM_RECORDTYPE] rec on task.RecordTypeId=rec.CODE_SF
LEFT JOIN [dbo].[DIM_CANAL] can on (opp.StartedChannel__c=can.CODE_SF and can.PARENT_ID is not null) 

where rec.LIBELLE='Téléphonie' 

select @nb_collected_rows = COUNT(*) from [dbo].[WK_MC_CALLLOG]; 

END