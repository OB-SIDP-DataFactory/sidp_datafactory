﻿
CREATE PROC [dbo].[PROC_SSRS_CRE_STOCK_OPPORTUNITE]

AS  BEGIN 

DECLARE @date_obs DATE;
SELECT  @date_obs = CAST(GETDATE() AS DATE);
DECLARE @date_max date;
SELECT  @date_max = dateadd(day, -1 ,GETDATE());
SET DATEFIRST 1; -->1    Lundi
DROP TABLE IF EXISTS #rq_cre_enrolment_cp;
DROP TABLE IF EXISTS [dbo].[WK_SSRS_CRE_STOCK_OPPORTUNITE];

SELECT Year_alim,Month_alim,Week_alim,jour_alim
    ,nb_opport_CRE_creees
	,nb_opport_CRE_encours
	,nb_opport_CRE_encours_digital
	,nb_opport_CRE_encours_crc
	,nb_opport_CRE_incident
	--Week to date
	,sum(nb_opport_CRE_creees) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_CRE_creees_WTD --Week to date
	,sum(nb_opport_CRE_encours) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_CRE_encours_WTD --Week to date
	,sum(nb_opport_CRE_encours_digital) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_CRE_encours_digital_WTD --Week to date
	,sum(nb_opport_CRE_encours_crc) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_CRE_encours_crc_WTD --Week to date
	,sum(nb_opport_CRE_incident) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_CRE_incident_WTD --Week to date
	--Month to date
	,sum(nb_opport_CRE_creees) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_CRE_creees_MTD --Month to date
	,sum(nb_opport_CRE_encours) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_CRE_encours_MTD --Month to date
	,sum(nb_opport_CRE_encours_digital) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_CRE_encours_digital_MTD --Month to date
	,sum(nb_opport_CRE_encours_crc) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_CRE_encours_crc_MTD --Month to date
	,sum(nb_opport_CRE_incident) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_CRE_incident_MTD --Month to date
	--Full Month
	,sum(nb_opport_CRE_creees) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_opport_CRE_creees_FM --Full Month
	,sum(nb_opport_CRE_encours) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_opport_CRE_encours_FM --Full Month
	,sum(nb_opport_CRE_encours_digital) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_opport_CRE_encours_digital_FM --Full Month
	,sum(nb_opport_CRE_encours_crc) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_opport_CRE_encours_crc_FM --Full Month
	,sum(nb_opport_CRE_incident) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_opport_CRE_incident_FM --Full Month
	--Year to date
    ,sum(nb_opport_CRE_creees) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_CRE_creees_YTD  --Year to date
    ,sum(nb_opport_CRE_encours) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_CRE_encours_YTD  --Year to date
    ,sum(nb_opport_CRE_encours_digital) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_CRE_encours_digital_YTD  --Year to date
    ,sum(nb_opport_CRE_encours_crc) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_CRE_encours_crc_YTD  --Year to date
    ,sum(nb_opport_CRE_incident) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_CRE_incident_YTD  --Year to date
	--Full year
	,sum(nb_opport_CRE_creees) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_opport_CRE_creees_FY  --Full year
	,sum(nb_opport_CRE_encours) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_opport_CRE_encours_FY  --Full year
	,sum(nb_opport_CRE_encours_digital) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_opport_CRE_encours_digital_FY  --Full year
	,sum(nb_opport_CRE_encours_crc) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_opport_CRE_encours_crc_FY  --Full year
	,sum(nb_opport_CRE_incident) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_opport_CRE_incident_FY  --Full year
INTO #rq_cre_enrolment_cp
	FROM  ( 
     SELECT  T1.StandardDate as jour_alim,DATEPART(YEAR,T1.Date) AS Year_alim,DATEPART(MONTH,T1.Date) AS Month_alim,DATEPART(YEAR,DATEADD(day, (7 - DATEPART(dw, T1.StandardDate)), T1.StandardDate)) *100 + DATEPART(WEEK,DATEADD(day, (7 - DATEPART(dw, T1.StandardDate)), T1.StandardDate)) AS Week_alim
	, sum(case when cast(DTE_CREA as date) = DTE_ALIM then NBE_OPPRT else 0 end) as nb_opport_CRE_creees
	, sum(case when STADE_VENTE IN ('01','02','03','04','05','06','07') then NBE_OPPRT else 0 end) as nb_opport_CRE_encours
	, sum(case when STADE_VENTE IN ('01','02','03','04','05','06','07') and COD_CAN_INT_ORI IN ('10','11') then NBE_OPPRT else 0 end) as nb_opport_CRE_encours_digital
	, sum(case when STADE_VENTE IN ('01','02','03','04','05','06','07') and COD_CAN_INT_ORI IN ('12','13','14','15')then NBE_OPPRT else 0 end) as nb_opport_CRE_encours_crc
	, sum(case when STADE_VENTE IN ('08') then NBE_OPPRT else 0 end) as nb_opport_CRE_incident
FROM [dbo].[DIM_TEMPS] T1 
LEFT OUTER JOIN  [dbo].[CRE_STOCK_OPPORTUNITE]  ON T1.StandardDate=CRE_STOCK_OPPORTUNITE.DTE_ALIM
WHERE  ( T1.StandardDate >= CAST(DATEADD(yyyy, DATEDIFF(YYYY, 0, DATEADD(yyyy, -2, @date_obs)), 0) AS DATE)
AND T1.StandardDate <=  DATEADD(day, (7 - DATEPART(dw, @date_obs)), @date_obs) )
GROUP BY  T1.StandardDate ,DATEPART(YEAR,T1.Date) ,DATEPART(MONTH,T1.Date) ,DATEPART(WEEK,T1.Date) 
) t;	

SELECT
  CY.jour_alim
, CY.Year_alim
, CY.Month_alim
, CY.Week_alim
, coalesce(CY.nb_opport_CRE_creees,0) AS nb_opport_CRE_creees
, coalesce(CY.nb_opport_CRE_creees_WTD,0) AS nb_opport_CRE_creees_WTD
, coalesce(CY.nb_opport_CRE_creees_MTD,0) AS nb_opport_CRE_creees_MTD
, coalesce(CY.nb_opport_CRE_creees_YTD,0) AS nb_opport_CRE_creees_YTD
, coalesce(PM.nb_opport_CRE_creees_FM,0) AS nb_opport_CRE_creees_FM
, coalesce(PY.nb_opport_CRE_creees_FY,0) AS nb_opport_CRE_creees_FY
, coalesce(CY.nb_opport_CRE_encours,0) AS nb_opport_CRE_encours
, coalesce(CY.nb_opport_CRE_encours_WTD,0) AS nb_opport_CRE_encours_WTD
, coalesce(CY.nb_opport_CRE_encours_MTD,0) AS nb_opport_CRE_encours_MTD
, coalesce(CY.nb_opport_CRE_encours_YTD,0) AS nb_opport_CRE_encours_YTD
, coalesce(PM.nb_opport_CRE_encours_FM,0) AS nb_opport_CRE_encours_FM
, coalesce(PY.nb_opport_CRE_encours_FY,0) AS nb_opport_CRE_encours_FY
, coalesce(CY.nb_opport_CRE_encours_digital,0) AS nb_opport_CRE_encours_digital
, coalesce(CY.nb_opport_CRE_encours_digital_WTD,0) AS nb_opport_CRE_encours_digital_WTD
, coalesce(CY.nb_opport_CRE_encours_digital_MTD,0) AS nb_opport_CRE_encours_digital_MTD
, coalesce(CY.nb_opport_CRE_encours_digital_YTD,0) AS nb_opport_CRE_encours_digital_YTD
, coalesce(PM.nb_opport_CRE_encours_digital_FM,0) AS nb_opport_CRE_encours_digital_FM
, coalesce(PY.nb_opport_CRE_encours_digital_FY,0) AS nb_opport_CRE_encours_digital_FY
, coalesce(CY.nb_opport_CRE_encours_crc,0) AS nb_opport_CRE_encours_crc
, coalesce(CY.nb_opport_CRE_encours_crc_WTD,0) AS nb_opport_CRE_encours_crc_WTD
, coalesce(CY.nb_opport_CRE_encours_crc_MTD,0) AS nb_opport_CRE_encours_crc_MTD
, coalesce(CY.nb_opport_CRE_encours_crc_YTD,0) AS nb_opport_CRE_encours_crc_YTD
, coalesce(PM.nb_opport_CRE_encours_crc_FM,0) AS nb_opport_CRE_encours_crc_FM
, coalesce(PY.nb_opport_CRE_encours_crc_FY,0) AS nb_opport_CRE_encours_crc_FY
, coalesce(CY.nb_opport_CRE_incident,0) AS nb_opport_CRE_incident
, coalesce(CY.nb_opport_CRE_incident_WTD,0) AS nb_opport_CRE_incident_WTD
, coalesce(CY.nb_opport_CRE_incident_MTD,0) AS nb_opport_CRE_incident_MTD
, coalesce(CY.nb_opport_CRE_incident_YTD,0) AS nb_opport_CRE_incident_YTD
, coalesce(PM.nb_opport_CRE_incident_FM,0) AS nb_opport_CRE_incident_FM
, coalesce(PY.nb_opport_CRE_incident_FY,0) AS nb_opport_CRE_incident_FY
INTO [dbo].[WK_SSRS_CRE_STOCK_OPPORTUNITE]
FROM #rq_cre_enrolment_cp CY --Current Year
 LEFT OUTER JOIN #rq_cre_enrolment_cp PM --Previous Year
		  ON DATEADD(MONTH,-1,CY.jour_alim) = PM.jour_alim
	 LEFT OUTER JOIN #rq_cre_enrolment_cp PY --Previous Year
		  ON DATEADD(YEAR,-1,CY.jour_alim) = PY.jour_alim
ORDER BY CY.jour_alim;
END