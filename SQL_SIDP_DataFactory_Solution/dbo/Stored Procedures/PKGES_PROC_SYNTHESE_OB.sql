﻿CREATE PROCEDURE [dbo].[PKGES_PROC_SYNTHESE_OB]
AS BEGIN

select T.OrigineID,T.Origine,T.CloseDate_YearMonth,sum(T.TotalFinal) as TotalCompteOuvert
from (
Select 
case when StartedChannel__c ='11' then 1 when StartedChannel__c ='10' then 2 when StartedChannel__c in ('12','13') then 3  end as OrigineID,
case when StartedChannel__c ='11' then 'Origine Web' when StartedChannel__c ='10' then 'Origine Appli' when StartedChannel__c in ('12','13') then 'Origine CRC'  end as Origine,
count(distinct Wk.Id_SF) as  TotalFinal,
FORMAT([CloseDate], 'MM/yyyy') as CloseDate_YearMonth
from [dbo].[WK_STOCK_OPPORTUNITY] as Wk
left outer join  [$(DataHubDatabaseName)].[dbo].[PV_SF_OPPORTUNITYISDELETED] as Wkd on Wk.Id_SF= Wkd.Id_SF
where Wkd.Id_SF is null and  DistributorNetwork__c='02' --Orange Bank
and StageName ='09' and CommercialOfferCode__c='OC80'
and [CloseDate] between  DATEADD(YEAR,-1,GETDATE()) and GETDATE()
and flag_opport_last_state = 1
and StartedChannel__c in ('10','11','12','13')
and [date_alim]=(select max([date_alim]) from [dbo].[WK_STOCK_OPPORTUNITY]) 
group by StartedChannel__c,StageName,LeadSource,[CloseDate]
) as T
group by T.OrigineID,T.Origine,T.CloseDate_YearMonth

order by T.OrigineID 

END;
