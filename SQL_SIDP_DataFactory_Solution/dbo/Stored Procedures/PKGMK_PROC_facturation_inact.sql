﻿
CREATE PROC PKGMK_PROC_facturation_inact @DATE_ALIM DATEtime2, @jours_histo int = 1, @nbRows int OUTPUT 
AS  BEGIN 
-- ===============================================================================================================================================================================
--tables input : [SIDP_DataHub_MVP1].[dbo].[PV_SA_Q_MOUVEMENT]
--				dbo.DIM_OPERATIONS
--tables output : [MKT_FACTURATION_INACT]
--description : construction d'une table avec 1 ligne par date alimentation x numéro de comtpe, 
-- avec le cumul sur 30 jours glissants des montants facturés pour chaque compte, jusqu'à la date_alim
-- =============================================================================================================================================================================

/************** créer un sous-ensemble de la table dimension temps **************/
--declare @DATE_ALIM date = '2017-02-13';
--declare @jours_histo int = 2;
-- select dateadd(day,-@jours_histo, @DATE_ALIM);
select PK_ID, StandardDate 
into #wk_temps_facturation
from dbo.DIM_TEMPS
where cast( dateadd(day,-@jours_histo, @DATE_ALIM) as date) <= StandardDate and StandardDate < cast(@DATE_ALIM as date) ; 

-- supprimer lignes insérées dans la même période pour pouvoir rééxécuter plusieures fois dans la journée
--declare @DATE_ALIM date = '2017-02-13';
--declare @jours_histo int = 2;
DELETE FROM MKT_FACTURATION_INACT
WHERE cast(date_alim as date)  >= cast( dateadd(day,-@jours_histo, @DATE_ALIM) as date) ; 

--declare @DATE_ALIM date = '2017-02-13';
--declare @jours_histo int = 2;
INSERT INTO MKT_FACTURATION_INACT
SELECT d.StandardDate as date_alim, NUM_CPT, cum_mtt_fact
FROM #wk_temps_facturation d
	OUTER APPLY 
	(
	SELECT d.StandardDate as date_alim, t1.NUM_CPT, sum(t1.MTT_EUR) as cum_mtt_fact --cumul sur 30j pour chaque date d'alimentation
	FROM 
	[$(DataHubDatabaseName)].[dbo].[PV_SA_Q_MOUVEMENT] t1
	INNER JOIN dbo.DIM_OPERATIONS t2
		ON  t1.COD_OPE = t2.COD_OPE
		and substring(t1.DON_ANA, 40,6) = t2.DON_ANA -- code facturation inactivité dans données analytiques
	WHERE t2.TYPE IN ('facturation')
		  AND DATEADD(day,-30,d.StandardDate) <= t1.DTE_OPE   and t1.DTE_OPE < d.StandardDate  -- cumul opérations dans les 30 jours qui précédent la date d'alimentation
	GROUP BY t1.NUM_CPT
	) tt
WHERE NUM_CPT IS NOT NULL
;
-- select * from MKT_FACTURATION_INACT
select @nbRows = COUNT(*) FROM MKT_FACTURATION_INACT ;

END 
GO