﻿
CREATE PROC PKGCM_PROC_all_FE @DATE_ALIM DATEtime2, @jours_histo int   = 1, --valeur 1 par défaut = traitement nominal par défaut
									@nbRows int OUTPUT -- nb lignes processées
AS  BEGIN 

/************** créer un sous-ensemble de la table dimension temps **************/
--declare @DATE_ALIM datetime  = '2017-05-10 19:09';
--declare @jours_histo int = 9; --nombre de jours reprise historique, 1=traitement nominal
-- select dateadd(day,-@jours_histo, @DATE_ALIM);
select PK_ID, StandardDate 
into #WK_TEMPS_fe
from DIM_TEMPS
where cast( dateadd(day,-@jours_histo, @DATE_ALIM) as date) <= StandardDate and StandardDate < cast(@DATE_ALIM as date) ; 
-- select * from #WK_TEMPS_fe

/****** historique table front end EQUIPMENT ******/
/*
SELECT * FROM [$(DataHubDatabaseName)].[dbo].[wk_PV_FE_EQUIPMENT]
-- table main + historique 
declare @DATE_ALIM date = '2017-04-21';
declare @jours_histo int = 2; 
IF OBJECT_ID('[FE_EQUIPMENT]') IS NOT NULL
BEGIN truncate TABLE [FE_EQUIPMENT] END
select EQUIPMENT_NUMBER, ENROLMENT_FOLDER_ID, Validity_StartDate, Validity_EndDate
into [FE_EQUIPMENT]
	from [$(DataHubDatabaseName)].[dbo].[wk_PV_FE_EQUIPMENT]
	where Validity_EndDate >= DATEADD(day,-@jours_histo,@DATE_ALIM) 
	union all 
	select EQUIPMENT_NUMBER, ENROLMENT_FOLDER_ID, Validity_StartDate, Validity_EndDate
	from [$(DataHubDatabaseName)].[dbo].[wk_PV_FE_EQUIPMENTHistory]
	where Validity_EndDate >= DATEADD(day,-@jours_histo,@DATE_ALIM) 
;
-- select * from [FE_EQUIPMENT]

-- isoler période de temps requise
IF OBJECT_ID('[ALL_FE_EQUIPMENT]') IS NOT NULL
BEGIN truncate TABLE ALL_FE_EQUIPMENT END
select d.StandardDate as date_alim, c.EQUIPMENT_NUMBER, c.ENROLMENT_FOLDER_ID, c.Validity_StartDate, c.Validity_EndDate
into ALL_FE_EQUIPMENT
from wk_temps d
outer apply 
	(
	select EQUIPMENT_NUMBER, ENROLMENT_FOLDER_ID, Validity_StartDate, Validity_EndDate
	from [FE_EQUIPMENT]
	where ( cast(Validity_StartDate as date) <= d.StandardDate and d.StandardDate < Validity_EndDate )
	) c
;
-- select * from [ALL_FE_EQUIPMENT]
*/

/****** historique table front end ENROLMENT FOLDER ******/
/*
-- table main + historique 
declare @DATE_ALIM date = '2017-04-21';
declare @jours_histo int = 2; 
IF OBJECT_ID('[FE_ENROLMENT_FOLDER]') IS NOT NULL
BEGIN truncate TABLE [FE_ENROLMENT_FOLDER] END
select *
into [FE_ENROLMENT_FOLDER]
	from [$(DataHubDatabaseName)].[dbo].[wk_PV_FE_ENROLMENT_FOLDER]
	union all 
	select *
	from [$(DataHubDatabaseName)].[dbo].[wk_PV_FE_ENROLMENT_FOLDER]
	--where condition profondeur historique
;

-- isoler période de temps requise
IF OBJECT_ID('[ALL_FE_ENROLMENT_FOLDER]') IS NOT NULL
BEGIN truncate TABLE [ALL_FE_ENROLMENT_FOLDER] END
*/
---------------------------------------------------------------*/

--select @nbRows = @@ROWCOUNT;
select @nbRows = count(*) from #WK_TEMPS_fe

END 
GO