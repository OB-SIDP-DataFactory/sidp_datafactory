﻿CREATE PROCEDURE [dbo].[PKGCP_PROC_ENROLEMENT_H_v1]

@Date_obs date
AS BEGIN

SET DATEFIRST 1; -->1    Lundi
--set @Date_obs ='2017-06-21';
declare @weekPreobs date=DATEADD(week,-1,@Date_obs);
declare @FirstDOW date=DATEADD(day, (-1 * DATEPART(dw, @Date_obs)) + 1, @Date_obs);
declare @FirstDOPW date=DATEADD(dd  , -7,@FirstDOW); 
declare @var int =datediff(dd,@FirstDOPW,@Date_obs);

with
cte_jour_alim_list AS(
select T1.StandardDate as jour_alim
from DIM_TEMPS T1
where T1.StandardDate<=@Date_obs and T1.StandardDate>=DATEADD(dd,-6,@Date_obs)
--and T1.StandardDate>= DATEADD(DAY,(-1*datepart(dw,@weekPreobs))+1,@weekPreobs)
)
--select * from cte_jour_alim_list

--Calcul du nombre de passage en dossier incomplet pour chaque opportunité 
,Cte_Opp_incomplet as (
select OpportunityId, count(NewValue) as NB_ITERATION
from [$(DataHubDatabaseName)].dbo.WK_SF_OPPORTUNITYFIELDHISTORY 
where Field='StageName'
and NewValue='06'--Dossier Incomplet
group by OpportunityId
) 
--select * from Cte_Opp_incomplet

,cte_opp_all as (
SELECT CAST(date_alim as date)                                               as DTE_ALIM
	  ,[LIBELLE]								                             
	  ,[Id_SF]                                                               as IDE_OPPRT_SF
	  ,[StageName]                                                           as STADE_VENTE
	  ,sdv.LIBELLE                                                           as LIB_STADE_VENTE
	  ,CAST([CreatedDate] as date)                                           as DTE_CREA
	  ,CAST([CloseDate] as date )                                            as DTE_CLO_OPPRT
	  ,DATEDIFF(DD, CAST([CreatedDate] AS DATE), cast([CloseDate] as date))  as DELAI_GAGNEES
	  ,/*CAST(*/last_update_date /*as date) */                               as DTE_DER_MOD
	  ,[age_opport]                                                          as AGE_OPPRT
	  ,DATEPART(ISO_WEEK,CAST(date_alim as date))                            as WeekOfYear
	  ,[DistributorNetwork__c]                                               as COD_RES_DIS
	  ,res_dis.LIB_RES_DIS                                                   as LIB_RES_DIS
	  ,[StartedChannel__c]                                                   as COD_CAN_INT_ORI
	  ,can_int_ori.LIB_CAN_INT                                               as LIB_CAN_INT_ORI
	  ,[DistributorEntity__c]                                                as ENT_DIS
	  ,ent_dis.LIB_CAN_INT                                                   as LIB_ENT_DIS 
	  ,[LeadSource]                                                          as COD_CAN_INT
	  ,can_int.LIB_CAN_INT                                                   as LIB_CAN_INT
	  ,[DistributionChannel__c]                                              as COD_CAN_DIS
	  ,can_dis.LIB_CAN_DIS                                                   as LIB_CAN_DIS
	  ,[flag_indication]                                                     as FLG_IND
	  ,[flag_full_digi]							   
      ,[flag_full_btq]						   
	  ,ISNULL(incplet.NB_ITERATION,0)                                        as NB_ITERATION
	  ,rank() over (partition by Opp.Id_SF, CAST(opp.date_alim as date) order by CAST(Opp.last_update_date as date) desc) as rang 
FROM [dbo].[WK_STOCK_OPPORTUNITY] opp	WITH(NOLOCK)
LEFT JOIN [dbo].[DIM_STADE_VENTE] sdv --Rajouter la nouvelle DIM_STADE_VTE lorsque livraison R2 crédit et nouvelles dim
ON opp.StageName = sdv.CODE_SF and sdv.PARENT_ID is null
LEFT JOIN [dbo].[DIM_RES_DIS] res_dis	WITH(NOLOCK)
ON opp.DistributorNetwork__c = res_dis.COD_RES_DIS
LEFT JOIN [dbo].[DIM_CANAL_INT] can_int_ori	WITH(NOLOCK)
ON opp.StartedChannel__c = can_int_ori.COD_CAN_INT
LEFT JOIN [dbo].[DIM_CANAL_INT] ent_dis	WITH(NOLOCK)
ON opp.DistributorEntity__c = ent_dis.COD_CAN_INT
LEFT JOIN [dbo].[DIM_CANAL_INT] can_int	WITH(NOLOCK)
ON opp.LeadSource = can_int.COD_CAN_INT
LEFT JOIN [dbo].[DIM_CANAL_DIS] can_dis	WITH(NOLOCK)
ON opp.DistributionChannel__c = can_dis.COD_CAN_DIS
LEFT JOIN Cte_Opp_incomplet incplet	WITH(NOLOCK)
on opp.Id_SF = incplet.OpportunityId
inner join cte_jour_alim_list on  cast(opp.date_alim as date) =cast(cte_jour_alim_list.jour_alim as date)
where flag_opport_last_state = 1  --and Id_SF='0065800000I4hSWAAZ'
and CommercialOfferCode__c='OC80' --Type CAV*
)
--select * from  cte_opp_all 

--Réclamations
,Cte_Reclamations_all as (
select distinct substring(IDE_REQ_SF,1,15)    as IDE_REQ_SF,
                cast(DTE_CREA as date)        as DTE_CREA_RECLA,
				DATEPART(ISO_WEEK,[DTE_CREA]) as WeekOfYear,
                TYP_REQ,
				CAN_INT_ORI,
				LIB_CAN_INT_ORI,
                LIB_SS_TYP,
				Min(DTE_DER_MOD) as DTE_DER_MOD

from [$(DataHubDatabaseName)].exposition.[PV_SF_CASE]
where TYP_REQ='02' and LIB_SS_TYP in ('Souscription/refus souscription')
group by IDE_REQ_SF,
         DTE_CREA,
		 DTE_DER_MOD,
		 TYP_REQ,
		 CAN_INT_ORI,
		 LIB_CAN_INT_ORI,
         LIB_SS_TYP

)
--select * from Cte_Reclamations_all 
,cte_opp_gag as (
select IDE_OPPRT_SF,
       MIN(DTE_DER_MOD) as DTE_DER_MOD
from cte_opp_all
where STADE_VENTE= '09' and rang=1 
group by IDE_OPPRT_SF
)

,cte_opp_per as (
select IDE_OPPRT_SF,
       STADE_VENTE,
	   MIN(DTE_DER_MOD) as DTE_DER_MOD
from cte_opp_all
where (STADE_VENTE in ('08','10','11','12'))
and rang=1
group by IDE_OPPRT_SF,STADE_VENTE
)

/*La semaine de date_obs -7 jours*/
, cte_opp_cav_distinct_crees as 
(select distinct
ide_opprt_sf
, WeekOfYear
, dte_crea
, cod_res_dis
, cod_can_int_ori
, ent_dis
, flg_ind
,rang
from cte_Opp_ALL
)

--Calcul du délai médian de transformation

,cte_Opp_OB_DELAI as (
SELECT distinct DTE_ALIM,WeekOfYear, COD_RES_DIS,COD_CAN_INT_ORI, STADE_VENTE,
case when [COD_RES_DIS] = '02' then PERCENTILE_CONT (0.5)  WITHIN GROUP (ORDER BY DELAI_GAGNEES) OVER (PARTITION BY COD_RES_DIS,DTE_ALIM, CASE WHEN STADE_VENTE='09' THEN STADE_VENTE END, CASE WHEN COD_RES_DIS = '02' THEN COD_RES_DIS END) end																														  AS DELAI_GAGNEES_OB, 
case when ([COD_RES_DIS] = '02' and [COD_CAN_INT_ORI] IN ('10','11')) then PERCENTILE_CONT (0.5)  WITHIN GROUP (ORDER BY DELAI_GAGNEES asc) OVER (PARTITION BY COD_RES_DIS,DTE_ALIM,CASE WHEN STADE_VENTE='09' THEN STADE_VENTE END, CASE WHEN COD_RES_DIS = '02' THEN COD_RES_DIS END,CASE WHEN [COD_CAN_INT_ORI] IN ('10','11') then 'OB_DIG' end ) end                 AS DELAI_GAGNEES_OB_Dig, 
case when ([COD_RES_DIS] = '02' and [COD_CAN_INT_ORI] IN ('12','13','14','15')) then PERCENTILE_CONT (0.5)  WITHIN GROUP (ORDER BY DELAI_GAGNEES) OVER (PARTITION BY COD_RES_DIS,DTE_ALIM,CASE WHEN STADE_VENTE='09' THEN STADE_VENTE END, CASE WHEN COD_RES_DIS = '02' THEN COD_RES_DIS END,CASE WHEN [COD_CAN_INT_ORI] IN ('12','13','14','15') then 'OB_CRC' end) end  AS DELAI_GAGNEES_OB_CRC
from cte_opp_all y inner join cte_opp_gag on y.IDE_OPPRT_SF=cte_opp_gag.IDE_OPPRT_SF and y.DTE_ALIM=CAST(cte_opp_gag.DTE_DER_MOD as date)
WHERE Cast(y.[DTE_ALIM] as date) BETWEEN DATEADD(dd,-6,@Date_obs)  and @Date_obs
and rang=1 
and DTE_CLO_OPPRT=@Date_obs)
--select * from cte_Opp_OB_DELAI where DTE_ALIM='2018-06-05' and STADE_VENTE='09'

,cte_Opp_OB_DELAI_Cumul as (
select DTE_alim,WeekOfYear,
       ISNULL(AVG(DELAI_GAGNEES_OB),0)             as DELAI_GAGNEES_OB, 
	   ISNULL(AVG(DELAI_GAGNEES_OB_Dig),0)         as DELAI_GAGNEES_OB_Dig, 
	   ISNULL(AVG(DELAI_GAGNEES_OB_CRC),0)         as DELAI_GAGNEES_OB_CRC
from cte_Opp_OB_DELAI
GROUP BY DTE_ALIM,WeekOfYear)
--select * from cte_Opp_OB_DELAI_Cumul

,cte_Opp_OF_DELAI as (
SELECT distinct DTE_ALIM,WeekOfYear, COD_RES_DIS,ENT_DIS, STADE_VENTE,
case when [COD_RES_DIS] = '01' then PERCENTILE_CONT (0.5)  WITHIN GROUP (ORDER BY DELAI_GAGNEES) OVER (PARTITION BY COD_RES_DIS,DTE_ALIM,CASE WHEN STADE_VENTE='09' THEN STADE_VENTE END, CASE WHEN COD_RES_DIS = '01' THEN COD_RES_DIS END) end                                                                                      AS DELAI_GAGNEES_OF, 
case when ([COD_RES_DIS] = '01' and [ENT_DIS] = '04') then PERCENTILE_CONT (0.5)  WITHIN GROUP (ORDER BY DELAI_GAGNEES) OVER (PARTITION BY COD_RES_DIS,DTE_ALIM,CASE WHEN STADE_VENTE='09' THEN STADE_VENTE END, CASE WHEN COD_RES_DIS = '01' THEN COD_RES_DIS END,CASE WHEN ENT_DIS= '04' then 'OF_DIG' end ) end                    AS DELAI_GAGNEES_OF_Dig,   
case when ([COD_RES_DIS] = '01' and [ENT_DIS] in ('01','02'))  then PERCENTILE_CONT (0.5)  WITHIN GROUP (ORDER BY DELAI_GAGNEES) OVER (PARTITION BY COD_RES_DIS,DTE_ALIM,CASE WHEN STADE_VENTE='09' THEN STADE_VENTE END, CASE WHEN COD_RES_DIS = '01' THEN COD_RES_DIS END,CASE WHEN ENT_DIS in ('01','02') then 'OF_Btq' end ) end  AS DELAI_GAGNEES_OF_Btq
from cte_opp_all y inner join cte_opp_gag on y.IDE_OPPRT_SF=cte_opp_gag.IDE_OPPRT_SF and y.DTE_ALIM=CAST(cte_opp_gag.DTE_DER_MOD as date)
WHERE Cast(y.[DTE_ALIM] as date) BETWEEN DATEADD(dd,-6,@Date_obs)  and @Date_obs
and rang=1
and DTE_CLO_OPPRT=@Date_obs)
--select * from cte_Opp_OF_DELAI --where DTE_ALIM='2018-06-05' and STADE_VENTE='09'

,cte_Opp_OF_DELAI_Cumul as (
select DTE_alim,WeekOfYear,
	   ISNULL(AVG(DELAI_GAGNEES_OF),0)             as DELAI_GAGNEES_OF, 
	   ISNULL(AVG(DELAI_GAGNEES_OF_Dig),0)         as DELAI_GAGNEES_OF_Dig, 
	   ISNULL(AVG(DELAI_GAGNEES_OF_Btq),0)         as DELAI_GAGNEES_OF_Btq
from cte_Opp_OF_DELAI
GROUP BY DTE_ALIM,WeekOfYear)
--select * from cte_Opp_OF_DELAI_Cumul

--Calcul du nb d'iterations
,cte_Opp_Calcul_Iter as (
SELECT DTE_ALIM,
       WeekOfYear,
	   case when y.[COD_RES_DIS] = '02' and STADE_VENTE='09' then COUNT(y.NB_ITERATION) end                                   AS NB_ITERATION_OB, 
	   case when y.[COD_RES_DIS] = '02' and STADE_VENTE='09'and y. NB_ITERATION = '0' then COUNT(y.NB_ITERATION) end          AS NB_ITERATION_OB_0, 
	   case when y.[COD_RES_DIS] = '02' and STADE_VENTE='09'and y. NB_ITERATION IN ('1','2') then COUNT(y.NB_ITERATION) end   AS NB_ITERATION_OB_1_2, 
	   case when y.[COD_RES_DIS] = '02' and STADE_VENTE='09' and y.NB_ITERATION > '2'  then COUNT(y.NB_ITERATION) end         AS NB_ITERATION_OB_plus_2, 
	   case when y.[COD_RES_DIS] = '01' and STADE_VENTE='09' then COUNT(y.NB_ITERATION) end                                   AS NB_ITERATION_OF, 
	   case when y.[COD_RES_DIS] = '01' and STADE_VENTE='09' and y.NB_ITERATION = '0' then COUNT(y.NB_ITERATION) end          AS NB_ITERATION_OF_0, 
	   case when y.[COD_RES_DIS] = '01' and STADE_VENTE='09' and y.NB_ITERATION IN ('1','2') then COUNT(y.NB_ITERATION) end   AS NB_ITERATION_OF_1_2, 
	   case when y.[COD_RES_DIS] = '01' and STADE_VENTE='09' and y.NB_ITERATION > '2'  then COUNT(y.NB_ITERATION) end         AS NB_ITERATION_OF_plus_2	  
FROM cte_Opp_ALL y left join (select OpportunityId, count(NewValue) as NB_ITERATION
                              from [$(DataHubDatabaseName)].dbo.WK_SF_OPPORTUNITYFIELDHISTORY 
                              where Field='StageName'
                              and NewValue='06'--Dossier Incomplet
                              group by OpportunityId) c  on  y.IDE_OPPRT_SF=c.OpportunityId 
					--inner join cte_opp_gag on y.IDE_OPPRT_SF=cte_opp_gag.IDE_OPPRT_SF and y.DTE_ALIM=CAST(cte_opp_gag.DTE_DER_MOD as date)

WHERE Cast(y.[DTE_ALIM] as date) BETWEEN DATEADD(dd,-6,@Date_obs)  and @Date_obs
AND y.STADE_VENTE = '09' AND DTE_CLO_OPPRT=@Date_obs and rang=1 
GROUP BY DTE_ALIM,
         [WeekOfYear],
		 COD_RES_DIS,
		 y.NB_ITERATION,
		 STADE_VENTE)
--select * from cte_Opp_Calcul_Iter

,cte_Opp_Calcul_Iter_Cumul as (
SELECT  DTE_ALIM,
        WeekOfYear,
		ISNULL(SUM(NB_ITERATION_OB),0)         as NB_ITERATION_OB, 
		ISNULL(SUM(NB_ITERATION_OB_0),0)       as NB_ITERATION_OB_0, 
		ISNULL(SUM(NB_ITERATION_OB_1_2),0)     as NB_ITERATION_OB_1_2, 
		ISNULL(SUM(NB_ITERATION_OB_plus_2),0)  as NB_ITERATION_OB_plus_2, 
		ISNULL(SUM(NB_ITERATION_OF),0)         as NB_ITERATION_OF, 
		ISNULL(SUM(NB_ITERATION_OF_0),0)       as NB_ITERATION_OF_0, 
		ISNULL(SUM(NB_ITERATION_OF_1_2),0)     as NB_ITERATION_OF_1_2, 
		ISNULL(SUM(NB_ITERATION_OF_plus_2),0)  as NB_ITERATION_OF_plus_2
FROM cte_Opp_Calcul_Iter
GROUP BY DTE_ALIM,
         [WeekOfYear])
--select * from cte_Opp_Calcul_Iter_Cumul

--Opport.CAV Créées
,Cte_Opp_CAV_Creees as (
SELECT 
	   [DTE_CREA],
	   DATEPART(ISO_WEEK,[DTE_CREA]) as [WeekOfYear_c],
	   case when ([COD_RES_DIS] in ('01','02') or [COD_CAN_INT_ORI] IN ('10','11','12','13','14','15') or [ENT_DIS] in ('01','02','04') or [FLG_IND] in ('Non','Oui')) then COUNT([IDE_OPPRT_SF]) end as NB_OPP_CREEES,
	   case when [COD_RES_DIS] = '02' then COUNT ([IDE_OPPRT_SF]) end                                                                                                                                 as NB_CREEES_OB,
       case when ([COD_RES_DIS] = '02' and [COD_CAN_INT_ORI] IN ('10','11')) then COUNT ([IDE_OPPRT_SF])  end                                                                                         as NB_CREEES_OB_Dig,
	   case when ([COD_RES_DIS] = '02' and [COD_CAN_INT_ORI] IN ('12','13','14','15')) then COUNT ([IDE_OPPRT_SF])  end                                                                               as NB_CREEES_OB_CRC,
	   case when ([COD_RES_DIS] = '01') then COUNT ([IDE_OPPRT_SF])  end                                                                                                                              as NB_CREEES_OF,
	   case when ([COD_RES_DIS] = '01' and [ENT_DIS]='04') then COUNT ([IDE_OPPRT_SF])  end                                                                                                           as NB_CREEES_OF_Dig,
	   case when ([COD_RES_DIS] = '01' and [ENT_DIS] in ('01','02')) then COUNT ([IDE_OPPRT_SF])  end                                                                                                 as NB_CREEES_OF_Btq,
	   case when ([COD_RES_DIS] = '01' and [FLG_IND] = 'Non') then COUNT ([IDE_OPPRT_SF])  end                                                                                                        as NB_CREEES_OF_SS,	   
	   case when ([COD_RES_DIS] = '01' and [FLG_IND] = 'Non' and [ENT_DIS] = '04') then COUNT ([IDE_OPPRT_SF])  end                                                                                   as NB_CREEES_OF_SS_Dig,
	   case when ([COD_RES_DIS] = '01' and [FLG_IND] = 'Non' and [ENT_DIS] in ('01','02')) then COUNT ([IDE_OPPRT_SF])  end                                                                           as NB_CREEES_OF_SS_Btq,
	   case when ([COD_RES_DIS] = '01' and [FLG_IND] = 'Oui') then COUNT ([IDE_OPPRT_SF])  end                                                                                                        as NB_CREEES_OF_AC,
	   case when ([COD_RES_DIS] = '01' and [FLG_IND] = 'Oui' and [ENT_DIS] = '04') then COUNT ([IDE_OPPRT_SF])  end                                                                                   as NB_CREEES_OF_AC_Dig,
	   case when ([COD_RES_DIS] = '01' and [FLG_IND] = 'Oui' and [ENT_DIS] in ('01','02')) then COUNT ([IDE_OPPRT_SF])  end                                                                           as NB_CREEES_OF_AC_Btq
FROM cte_opp_cav_distinct_crees
--cte_Opp_ALL
WHERE Cast([DTE_CREA] as date) BETWEEN DATEADD(dd,-6,@Date_obs)  and @Date_obs and rang=1
--AND IDE_OPPRT_SF = '0065800000I4hSWAAZ'
GROUP BY 
        [DTE_CREA],
		DATEPART(ISO_WEEK,[DTE_CREA]),
		[COD_RES_DIS],
		[COD_CAN_INT_ORI],
		[FLG_IND],
		[ENT_DIS]
)
--select * from Cte_Opp_CAV_Creees

--Opport.CAV Créées Cumul

,Cte_Opp_CAV_Creees_Cumul as (
SELECT [DTE_CREA],
		[WeekOfYear_c],
		ISNULL(SUM(NB_OPP_CREEES),0)       as NB_OPP_CREEES,
		ISNULL(SUM(NB_CREEES_OB),0)        as NB_CREEES_OB,
		ISNULL(SUM(NB_CREEES_OB_Dig),0)    as NB_CREEES_OB_Dig,
		ISNULL(SUM(NB_CREEES_OB_CRC),0)    as NB_CREEES_OB_CRC,
		ISNULL(SUM(NB_CREEES_OF),0)        as NB_CREEES_OF,
		ISNULL(SUM(NB_CREEES_OF_Dig),0)    as NB_CREEES_OF_Dig,
		ISNULL(SUM(NB_CREEES_OF_Btq),0)    as NB_CREEES_OF_Btq,
		ISNULL(SUM(NB_CREEES_OF_SS),0)     as NB_CREEES_OF_SS,
		ISNULL(SUM(NB_CREEES_OF_SS_Dig),0) as NB_CREEES_OF_SS_Dig,
		ISNULL(SUM(NB_CREEES_OF_SS_Btq),0) as NB_CREEES_OF_SS_Btq,
		ISNULL(SUM(NB_CREEES_OF_AC),0)     as NB_CREEES_OF_AC,
		ISNULL(SUM(NB_CREEES_OF_AC_Dig),0) as NB_CREEES_OF_AC_Dig,
		ISNULL(SUM(NB_CREEES_OF_AC_Btq),0) as NB_CREEES_OF_AC_Btq 
FROM Cte_Opp_CAV_Creees
GROUP BY [DTE_CREA],
		[WeekOfYear_c]
)
--select * from Cte_Opp_CAV_Creees_Cumul

--Opport CAV en cours de vie
,Cte_Opp_CAV_En_cours as (
SELECT DTE_ALIM,
       WeekOfYear,
       COUNT ([IDE_OPPRT_SF])                                                              as NB_OPP_ENCRS,
	   case when [COD_RES_DIS]='02' and STADE_VENTE = '01' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OB_DETEC,
       case when [COD_RES_DIS]='02' and STADE_VENTE = '02' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OB_DECOUV,
	   case when [COD_RES_DIS]='02' and STADE_VENTE = '03' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OB_ELAB,
	   case when [COD_RES_DIS]='02' and STADE_VENTE = '04' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OB_AFF_SIGN,
	   case when [COD_RES_DIS]='02' and STADE_VENTE = '05' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OB_VERIF,
	   case when [COD_RES_DIS]='02' and STADE_VENTE = '06' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OB_DOSS_INC,
	   case when [COD_RES_DIS]='02' and STADE_VENTE = '07' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OB_DOSS_VAL,
	   case when [COD_RES_DIS]='01' and STADE_VENTE = '01' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OF_DETEC,
	   case when [COD_RES_DIS]='01' and STADE_VENTE = '02' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OF_DECOUV,
	   case when [COD_RES_DIS]='01' and STADE_VENTE = '03' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OF_ELAB,
	   case when [COD_RES_DIS]='01' and STADE_VENTE = '04' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OF_AFF_SIGN,
	   case when [COD_RES_DIS]='01' and STADE_VENTE = '05' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OF_VERIF,
	   case when [COD_RES_DIS]='01' and STADE_VENTE = '06' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OF_DOSS_INC,
	   case when [COD_RES_DIS]='01' and STADE_VENTE = '07' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OF_DOSS_VAL
FROM cte_Opp_ALL
WHERE Cast([DTE_ALIM] as date) BETWEEN DATEADD(dd,-6,@Date_obs)  and @Date_obs
AND STADE_VENTE IN ('01','02','03','04','05','06','07') and COD_RES_DIS in ('01','02') and rang=1
AND DTE_CLO_OPPRT > @Date_obs and DTE_CREA <= @Date_obs
GROUP BY DTE_ALIM,
		 STADE_VENTE,
		 [WeekOfYear],
		 [COD_RES_DIS]
)
--select * from Cte_Opp_CAV_En_cours

--Opport CAV en cours de vie Cumul
,Cte_Opp_CAV_En_cours_Cumul as(
SELECT  DTE_ALIM,
        [WeekOfYear],
		ISNULL(SUM(NB_OPP_ENCRS),0)         as NB_OPP_ENCRS,
		ISNULL(SUM(NB_ENCRS_OB_DETEC),0)    as NB_ENCRS_OB_DETEC,
		ISNULL(SUM(NB_ENCRS_OB_DECOUV),0)   as NB_ENCRS_OB_DECOUV,
		ISNULL(SUM(NB_ENCRS_OB_ELAB),0)     as NB_ENCRS_OB_ELAB,
		ISNULL(SUM(NB_ENCRS_OB_AFF_SIGN),0) as NB_ENCRS_OB_AFF_SIGN,
		ISNULL(SUM(NB_ENCRS_OB_VERIF),0)    as NB_ENCRS_OB_VERIF,
		ISNULL(SUM(NB_ENCRS_OB_DOSS_INC),0) as NB_ENCRS_OB_DOSS_INC,
		ISNULL(SUM(NB_ENCRS_OB_DOSS_VAL),0) as NB_ENCRS_OB_DOSS_VAL,
		ISNULL(SUM(NB_ENCRS_OF_DETEC),0)    as NB_ENCRS_OF_DETEC,
		ISNULL(SUM(NB_ENCRS_OF_DECOUV),0)   as NB_ENCRS_OF_DECOUV,
		ISNULL(SUM(NB_ENCRS_OF_ELAB),0)     as NB_ENCRS_OF_ELAB,
		ISNULL(SUM(NB_ENCRS_OF_AFF_SIGN),0) as NB_ENCRS_OF_AFF_SIGN,
		ISNULL(SUM(NB_ENCRS_OF_VERIF),0)    as NB_ENCRS_OF_VERIF,
		ISNULL(SUM(NB_ENCRS_OF_DOSS_INC),0) as NB_ENCRS_OF_DOSS_INC,
		ISNULL(SUM(NB_ENCRS_OF_DOSS_VAL),0) as NB_ENCRS_OF_DOSS_VAL
FROM Cte_Opp_CAV_En_cours
GROUP BY DTE_ALIM,
         [WeekOfYear]
)
--select * from Cte_Opp_CAV_En_cours_Cumul

--Opport CAV gagnees

,Cte_Opp_CAV_Gagnees as (
SELECT DTE_ALIM,
       [WeekOfYear],
	   --DELAI_GAGNEES,
       COUNT (y.[IDE_OPPRT_SF])                                                                                                 as NB_OPP_GAGNEES,
       case when [COD_RES_DIS] = '02' then COUNT (y.[IDE_OPPRT_SF]) end                                                         as NB_GAGNEES_OB,
	   case when ([COD_RES_DIS] = '02' and [COD_CAN_INT_ORI] IN ('10','11')) then COUNT (y.[IDE_OPPRT_SF]) end                  as NB_GAGNEES_OB_Dig,
	   case when ([COD_RES_DIS] = '02' and [COD_CAN_INT_ORI] IN ('12','13','14','15')) then COUNT (y.[IDE_OPPRT_SF]) end        as NB_GAGNEES_OB_CRC,
	   case when [COD_RES_DIS] = '01' then COUNT (y.[IDE_OPPRT_SF]) end                                                         as NB_GAGNEES_OF,
	   case when ([COD_RES_DIS] = '01' and [ENT_DIS] = '04') then COUNT (y.[IDE_OPPRT_SF])  end                                 as NB_GAGNEES_OF_Dig,
	   case when ([COD_RES_DIS] = '01' and [ENT_DIS] in ('01','02')) then COUNT (y.[IDE_OPPRT_SF])  end                         as NB_GAGNEES_OF_Btq,
	   case when ([COD_RES_DIS] = '01' and [FLG_IND] = 'Non' and [ENT_DIS] = '04') then COUNT (y.[IDE_OPPRT_SF])  end           as NB_GAGNEES_OF_SS_Dig,
	   case when ([COD_RES_DIS] = '01' and [FLG_IND] = 'Non' and [ENT_DIS] in ('01','02')) then COUNT (y.[IDE_OPPRT_SF])  end   as NB_GAGNEES_OF_SS_Btq,
	   case when ([COD_RES_DIS] = '01' and [FLG_IND] = 'Oui' and [ENT_DIS] = '04') then COUNT (y.[IDE_OPPRT_SF])  end           as NB_GAGNEES_OF_AC_Dig,
	   case when ([COD_RES_DIS] = '01' and [FLG_IND] = 'Oui' and [ENT_DIS] in ('01','02')) then COUNT (y.[IDE_OPPRT_SF])  end   as NB_GAGNEES_OF_AC_Btq,
	   case when [COD_RES_DIS] = '03' then COUNT (y.[IDE_OPPRT_SF]) end                                                         as NB_GAGNEES_GP,
	   case when ([COD_RES_DIS] = '03' and [FLG_IND] = 'Oui') then COUNT (y.[IDE_OPPRT_SF])  end                                as NB_GAGNEES_GP_AC,
	   case when ([COD_RES_DIS] = '03' and [FLG_IND] = 'Non') then COUNT (y.[IDE_OPPRT_SF])  end                                as NB_GAGNEES_GP_SS 
	  
FROM cte_Opp_ALL y inner join cte_opp_gag on y.IDE_OPPRT_SF=cte_opp_gag.IDE_OPPRT_SF and y.DTE_ALIM=CAST(cte_opp_gag.DTE_DER_MOD as date)
WHERE Cast(y.[DTE_ALIM] as date) BETWEEN DATEADD(dd,-6,@Date_obs)  and @Date_obs
AND y.STADE_VENTE = '09' and rang=1 AND DTE_CLO_OPPRT=@Date_obs
GROUP BY DTE_ALIM,
         [WeekOfYear],
		 COD_RES_DIS,
		 COD_CAN_INT_ORI,
		 [FLG_IND], 
		 [ENT_DIS],  
		 NB_ITERATION,
		 STADE_VENTE
)
--select * from Cte_Opp_CAV_Gagnees

--Opport CAV gagnees Cumul

,Cte_Opp_CAV_Gagnees_Cumul as (
SELECT DTE_ALIM,
       [WeekOfYear],
		ISNULL(SUM(NB_OPP_GAGNEES),0)          as NB_OPP_GAGNEES,
		ISNULL(SUM(NB_GAGNEES_OB),0)           as NB_GAGNEES_OB ,
		ISNULL(SUM(NB_GAGNEES_OB_Dig),0)       as NB_GAGNEES_OB_Dig,
		ISNULL(SUM(NB_GAGNEES_OB_CRC),0)       as NB_GAGNEES_OB_CRC,
		ISNULL(SUM(NB_GAGNEES_OF),0)           as NB_GAGNEES_OF,
		ISNULL(SUM(NB_GAGNEES_OF_Dig),0)       as NB_GAGNEES_OF_Dig,
		ISNULL(SUM(NB_GAGNEES_OF_Btq),0)       as NB_GAGNEES_OF_Btq,
		ISNULL(SUM(NB_GAGNEES_OF_SS_Dig),0)    as NB_GAGNEES_OF_SS_Dig,
		ISNULL(SUM(NB_GAGNEES_OF_SS_Btq),0)    as NB_GAGNEES_OF_SS_Btq,
		ISNULL(SUM(NB_GAGNEES_OF_AC_Dig),0)    as NB_GAGNEES_OF_AC_Dig,
		ISNULL(SUM(NB_GAGNEES_OF_AC_Btq),0)    as NB_GAGNEES_OF_AC_Btq ,
		ISNULL(SUM(NB_GAGNEES_GP),0)           as NB_GAGNEES_GP,
		ISNULL(SUM(NB_GAGNEES_GP_AC),0)        as NB_GAGNEES_GP_AC,
		ISNULL(SUM(NB_GAGNEES_GP_SS),0)        as NB_GAGNEES_GP_SS
		
FROM Cte_Opp_CAV_Gagnees
GROUP BY DTE_ALIM,
         [WeekOfYear]

)
--select * from Cte_Opp_CAV_Gagnees_Cumul

--Opport CAV perdues
,Cte_Opp_CAV_Perdues as (
SELECT DTE_ALIM,
       [WeekOfYear],
	   --y.IDE_OPPRT_SF,
       COUNT (y.[IDE_OPPRT_SF])                                                                                                                         as NB_OPP_PERDUES,
       case when (COD_RES_DIS = '02' and y.STADE_VENTE = '08' and cast(y.DTE_DER_MOD as date)=@Date_obs) then COUNT (y.[IDE_OPPRT_SF])              end as NB_PERDUES_OB_INC_TECH,
	   case when (COD_RES_DIS = '02' and y.STADE_VENTE = '11' and DTE_CLO_OPPRT=@Date_obs) then COUNT (y.[IDE_OPPRT_SF])                            end as NB_PERDUES_OB_SANS_SUITE,
	   case when (COD_RES_DIS = '02' and y.STADE_VENTE = '10' /*and DTE_CLO_OPPRT=@Date_obs*/) then COUNT (y.[IDE_OPPRT_SF])                        end as NB_PERDUES_OB_AFF_REF,
	   case when (COD_RES_DIS = '02' and y.STADE_VENTE = '12' and DTE_CLO_OPPRT=@Date_obs) then COUNT (y.[IDE_OPPRT_SF])                            end as NB_PERDUES_OB_RETRACT,
	   case when (COD_RES_DIS = '01' and y.STADE_VENTE = '08' and cast(y.DTE_DER_MOD as date)=@Date_obs) then COUNT (y.[IDE_OPPRT_SF])              end as NB_PERDUES_OF_INC_TECH,
	   case when (COD_RES_DIS = '01' and y.STADE_VENTE = '11' and DTE_CLO_OPPRT=@Date_obs)  then COUNT (y.[IDE_OPPRT_SF])                           end as NB_PERDUES_OF_SANS_SUITE,
	   case when (COD_RES_DIS = '01' and y.STADE_VENTE = '10' and DTE_CLO_OPPRT=@Date_obs)  then COUNT (y.[IDE_OPPRT_SF])                           end as NB_PERDUES_OF_AFF_REF,
	   case when (COD_RES_DIS = '01' and y.STADE_VENTE = '12' and DTE_CLO_OPPRT=@Date_obs)  then COUNT (y.[IDE_OPPRT_SF])                           end as NB_PERDUES_OF_RETRACT
FROM cte_Opp_ALL y inner join cte_opp_per on y.IDE_OPPRT_SF=cte_opp_per.IDE_OPPRT_SF and y.DTE_ALIM=CAST(cte_opp_per.DTE_DER_MOD as date)
WHERE Cast([DTE_ALIM] as date) BETWEEN DATEADD(dd,-6,@Date_obs)  and @Date_obs
AND /*y.STADE_VENTE IN ('08','11','10','12') and*/ COD_RES_DIS IN ('01','02') and (DTE_CLO_OPPRT=@Date_obs  or cast(y.DTE_DER_MOD as date) =@Date_obs) and rang=1
GROUP BY DTE_ALIM,
        --y.IDE_OPPRT_SF,
        y.DTE_DER_MOD,
		DTE_CLO_OPPRT,
        [WeekOfYear], 
		COD_RES_DIS,
		y.STADE_VENTE
)
--select * from Cte_Opp_CAV_Perdues where DTE_ALIM='2018-06-05' and substring(IDE_OPPRT_SF,1,15) ='0061p00000gzD5g'
--Opport CAV perdues Cumul

,Cte_Opp_CAV_Perdues_Cumul as (
SELECT  DTE_ALIM,
        [WeekOfYear],
		ISNULL(SUM(NB_OPP_PERDUES),0)           as NB_OPP_PERDUES,
		ISNULL(SUM(NB_PERDUES_OB_INC_TECH),0)   as NB_PERDUES_OB_INC_TECH,
		ISNULL(SUM(NB_PERDUES_OB_SANS_SUITE),0) as NB_PERDUES_OB_SANS_SUITE,
		ISNULL(SUM(NB_PERDUES_OB_AFF_REF),0)    as NB_PERDUES_OB_AFF_REF,
		ISNULL(SUM(NB_PERDUES_OB_RETRACT),0)    as NB_PERDUES_OB_RETRACT,
		ISNULL(SUM(NB_PERDUES_OF_INC_TECH),0)   as NB_PERDUES_OF_INC_TECH,
		ISNULL(SUM(NB_PERDUES_OF_SANS_SUITE),0) as NB_PERDUES_OF_SANS_SUITE,
		ISNULL(SUM(NB_PERDUES_OF_AFF_REF),0)    as NB_PERDUES_OF_AFF_REF,
		ISNULL(SUM(NB_PERDUES_OF_RETRACT),0)    as NB_PERDUES_OF_RETRACT 
FROM Cte_Opp_CAV_Perdues
GROUP BY DTE_ALIM,
         [WeekOfYear]
)
--select * from Cte_Opp_CAV_Perdues_Cumul

--SAV Réclamations

,Cte_Reclamations as (
select  DTE_CREA_RECLA, 
        [WeekOfYear],
		count(IDE_REQ_SF)                                                          as NB_RECLA, 
		case when TYP_REQ='02'  then count(IDE_REQ_SF) end                         as NB_RECLA_ENROL, 
		case when TYP_REQ='02'  and  CAN_INT_ORI = '01' then count(IDE_REQ_SF) end as NB_RECLA_MOB_SMS, 
		case when TYP_REQ='02'  and  CAN_INT_ORI = '02' then count(IDE_REQ_SF) end as NB_RECLA_NET_CLT, 
		case when TYP_REQ='02'  and  CAN_INT_ORI = '03' then count(IDE_REQ_SF) end as NB_RECLA_COURRIER, 
		case when TYP_REQ='02'  and  CAN_INT_ORI = '04' then count(IDE_REQ_SF) end as NB_RECLA_APP_ENTR, 
		case when TYP_REQ='02'  and  CAN_INT_ORI = '05' then count(IDE_REQ_SF) end as NB_RECLA_APP_SORT, 
		case when TYP_REQ='02'  and  CAN_INT_ORI = '07' then count(IDE_REQ_SF) end as NB_RECLA_CHAT, 
		case when TYP_REQ='02'  and  CAN_INT_ORI = '08' then count(IDE_REQ_SF) end as NB_RECLA_RES_SOC, 
		case when TYP_REQ='02'  and  CAN_INT_ORI = '09' then count(IDE_REQ_SF) end as NB_RECLA_APP_ENTR_TELCO, 
		case when TYP_REQ='02'  and  CAN_INT_ORI = '10' then count(IDE_REQ_SF) end as NB_RECLA_APP_ENTR_Btq, 
		case when TYP_REQ='02'  and  CAN_INT_ORI = '11' then count(IDE_REQ_SF) end as NB_RECLA_MAIL, 
		case when TYP_REQ='02'  and  CAN_INT_ORI = '12' then count(IDE_REQ_SF) end as NB_RECLA_FOR_DEM, 
		case when TYP_REQ='02'  and  CAN_INT_ORI = '13' then count(IDE_REQ_SF) end as NB_RECLA_SFMC 
from Cte_Reclamations_all
WHERE Cast([DTE_CREA_RECLA] as date) BETWEEN DATEADD(dd,-6,@Date_obs)  and @Date_obs and DTE_CREA_RECLA=@Date_obs and cast(DTE_DER_MOD as date)=@Date_obs 
GROUP BY DTE_CREA_RECLA, 																																									   
        [WeekOfYear],																																										   
		TYP_REQ, 																																											   
		CAN_INT_ORI																																											   
) 																																															   
--select * from Cte_Reclamations 																																							   
																																															   
,Cte_Reclamations_Cumul as (																																								   
select DTE_CREA_RECLA,																																										   
        [WeekOfYear], 																																										   
		ISNULL(SUM(NB_RECLA),0)                as NB_RECLA, 																																   
		ISNULL(SUM(NB_RECLA_ENROL),0)          as NB_RECLA_ENROL, 
		ISNULL(SUM(NB_RECLA_MOB_SMS),0)        as NB_RECLA_MOB_SMS, 
		ISNULL(SUM(NB_RECLA_NET_CLT),0)        as NB_RECLA_NET_CLT, 
		ISNULL(SUM(NB_RECLA_COURRIER),0)       as NB_RECLA_COURRIER, 
		ISNULL(SUM(NB_RECLA_APP_ENTR),0)       as NB_RECLA_APP_ENTR, 
		ISNULL(SUM(NB_RECLA_APP_SORT),0)       as NB_RECLA_APP_SORT, 
		ISNULL(SUM(NB_RECLA_CHAT),0)           as NB_RECLA_CHAT, 
		ISNULL(SUM(NB_RECLA_RES_SOC),0)        as NB_RECLA_RES_SOC, 
		ISNULL(SUM(NB_RECLA_APP_ENTR_TELCO),0) as NB_RECLA_APP_ENTR_TELCO, 
		ISNULL(SUM(NB_RECLA_APP_ENTR_Btq),0)   as NB_RECLA_APP_ENTR_Btq, 
		ISNULL(SUM(NB_RECLA_MAIL),0)           as NB_RECLA_MAIL, 
		ISNULL(SUM(NB_RECLA_FOR_DEM),0)        as NB_RECLA_FOR_DEM, 
		ISNULL(SUM(NB_RECLA_SFMC),0)           as NB_RECLA_SFMC
from Cte_Reclamations
GROUP BY DTE_CREA_RECLA,
         [WeekOfYear]
) 
--select * from Cte_Reclamations_Cumul

/*Le 1er jour de la semaine de date_obs jusqu'à date_obs */

--Opport.CAV Créées Cumul Hebdo
,Cte_Opp_CAV_Creees_Cumul_hebdo as(
select  WeekOfYear_c,
        ISNULL(SUM(NB_OPP_CREEES),0)		as NB_OPP_CREEES_HEBDO, 
		ISNULL(SUM(NB_CREEES_OB),0)			as NB_CREEES_OB_HEBDO,
		ISNULL(SUM(NB_CREEES_OB_Dig),0)		as NB_CREEES_OB_Dig_HEBDO,
		ISNULL(SUM(NB_CREEES_OB_CRC),0)		as NB_CREEES_OB_CRC_HEBDO,
		ISNULL(SUM(NB_CREEES_OF),0)			as NB_CREEES_OF_HEBDO,
		ISNULL(SUM(NB_CREEES_OF_Dig),0)		as NB_CREEES_OF_Dig_HEBDO,
		ISNULL(SUM(NB_CREEES_OF_Btq),0)		as NB_CREEES_OF_Btq_HEBDO,
		ISNULL(SUM(NB_CREEES_OF_SS),0)		as NB_CREEES_OF_SS_HEBDO,
		ISNULL(SUM(NB_CREEES_OF_SS_Dig),0)	as NB_CREEES_OF_SS_Dig_HEBDO,
		ISNULL(SUM(NB_CREEES_OF_SS_Btq),0)	as NB_CREEES_OF_SS_Btq_HEBDO, 
		ISNULL(SUM(NB_CREEES_OF_AC),0)		as NB_CREEES_OF_AC_HEBDO,
		ISNULL(SUM(NB_CREEES_OF_AC_Dig),0)	as NB_CREEES_OF_AC_Dig_HEBDO,
		ISNULL(SUM(NB_CREEES_OF_AC_Btq),0)	as NB_CREEES_OF_AC_Btq_HEBDO
from  Cte_Opp_CAV_Creees_Cumul 
where  DTE_CREA >= @FirstDOW and DTE_CREA<=@Date_obs
group by WeekOfYear_c
)
--select * from Cte_Opp_CAV_Creees_Cumul_hebdo

--Opport CAV en cours de vie Cumul hebdo
,Cte_Opp_CAV_En_cours_Cumul_hebdo as(
SELECT  WeekOfYear,
		ISNULL(SUM(NB_OPP_ENCRS),0)           as NB_OPP_ENCRS_HEBDO,
		ISNULL(SUM(NB_ENCRS_OB_DETEC),0)      as NB_ENCRS_OB_DETEC_HEBDO,
		ISNULL(SUM(NB_ENCRS_OB_DECOUV),0)     as NB_ENCRS_OB_DECOUV_HEBDO,
		ISNULL(SUM(NB_ENCRS_OB_ELAB),0)       as NB_ENCRS_OB_ELAB_HEBDO,
		ISNULL(SUM(NB_ENCRS_OB_AFF_SIGN),0)   as NB_ENCRS_OB_AFF_SIGN_HEBDO,
		ISNULL(SUM(NB_ENCRS_OB_VERIF),0)      as NB_ENCRS_OB_VERIF_HEBDO,
		ISNULL(SUM(NB_ENCRS_OB_DOSS_INC),0)   as NB_ENCRS_OB_DOSS_INC_HEBDO,
		ISNULL(SUM(NB_ENCRS_OB_DOSS_VAL),0)   as NB_ENCRS_OB_DOSS_VAL_HEBDO,
		ISNULL(SUM(NB_ENCRS_OF_DETEC),0)      as NB_ENCRS_OF_DETEC_HEBDO,
		ISNULL(SUM(NB_ENCRS_OF_DECOUV),0)     as NB_ENCRS_OF_DECOUV_HEBDO,
		ISNULL(SUM(NB_ENCRS_OF_ELAB),0)       as NB_ENCRS_OF_ELAB_HEBDO,
		ISNULL(SUM(NB_ENCRS_OF_AFF_SIGN),0)   as NB_ENCRS_OF_AFF_SIGN_HEBDO,
		ISNULL(SUM(NB_ENCRS_OF_VERIF),0)      as NB_ENCRS_OF_VERIF_HEBDO,
		ISNULL(SUM(NB_ENCRS_OF_DOSS_INC),0)   as NB_ENCRS_OF_DOSS_INC_HEBDO,
		ISNULL(SUM(NB_ENCRS_OF_DOSS_VAL),0)   as NB_ENCRS_OF_DOSS_VAL_HEBDO
FROM Cte_Opp_CAV_En_cours_Cumul
where  DTE_ALIM >= @FirstDOW and DTE_ALIM<=@Date_obs
GROUP BY WeekOfYear
)
--select * from Cte_Opp_CAV_En_cours_Cumul_hebdo

--Opport CAV gagnees cumul hebdo

,Cte_Opp_CAV_Gagnees_Cumul_hebdo as (
SELECT  WeekOfYear,
		ISNULL(SUM(NB_OPP_GAGNEES),0)       as NB_OPP_GAGNEES_HEBDO,
		ISNULL(SUM(NB_GAGNEES_OB),0)        as NB_GAGNEES_OB_HEBDO ,
		ISNULL(SUM(NB_GAGNEES_OB_Dig),0)    as NB_GAGNEES_OB_Dig_HEBDO,
		ISNULL(SUM(NB_GAGNEES_OB_CRC),0)    as NB_GAGNEES_OB_CRC_HEBDO,
		ISNULL(SUM(NB_GAGNEES_OF),0)        as NB_GAGNEES_OF_HEBDO,
		ISNULL(SUM(NB_GAGNEES_OF_Dig),0)    as NB_GAGNEES_OF_Dig_HEBDO,
		ISNULL(SUM(NB_GAGNEES_OF_Btq),0)    as NB_GAGNEES_OF_Btq_HEBDO,
		ISNULL(SUM(NB_GAGNEES_OF_SS_Dig),0) as NB_GAGNEES_OF_SS_Dig_HEBDO,
		ISNULL(SUM(NB_GAGNEES_OF_SS_Btq),0) as NB_GAGNEES_OF_SS_Btq_HEBDO,
		ISNULL(SUM(NB_GAGNEES_OF_AC_Dig),0) as NB_GAGNEES_OF_AC_Dig_HEBDO,
		ISNULL(SUM(NB_GAGNEES_OF_AC_Btq),0) as NB_GAGNEES_OF_AC_Btq_HEBDO ,
		ISNULL(SUM(NB_GAGNEES_GP),0)        as NB_GAGNEES_GP_HEBDO,
		ISNULL(SUM(NB_GAGNEES_GP_AC),0)     as NB_GAGNEES_GP_AC_HEBDO,
		ISNULL(SUM(NB_GAGNEES_GP_SS),0)     as NB_GAGNEES_GP_SS_HEBDO
FROM Cte_Opp_CAV_Gagnees_Cumul
where  DTE_ALIM >= @FirstDOW and DTE_ALIM <=@Date_obs
GROUP BY WeekOfYear
)
--select * from Cte_Opp_CAV_Gagnees_Cumul_hebdo
--Calcul du délai médian de transformation OB hebdo
,cte_Opp_OB_DELAI_Cumul_hebdo as (
select WeekOfYear,
       ISNULL(SUM(DELAI_GAGNEES_OB)/(datediff(dd,@FirstDOW,@Date_obs)+1),0)             as DELAI_GAGNEES_OB_HEBDO, 
	   ISNULL(SUM(DELAI_GAGNEES_OB_Dig)/(datediff(dd,@FirstDOW,@Date_obs)+1),0)         as DELAI_GAGNEES_OB_Dig_HEBDO, 
	   ISNULL(SUM(DELAI_GAGNEES_OB_CRC)/(datediff(dd,@FirstDOW,@Date_obs)+1),0)         as DELAI_GAGNEES_OB_CRC_HEBDO
from cte_Opp_OB_DELAI_Cumul
where  DTE_ALIM >= @FirstDOW and DTE_ALIM <=@Date_obs
GROUP BY WeekOfYear)
--select * from cte_Opp_OB_DELAI_Cumul_hebdo

--Calcul du délai médian de transformation OF hebdo

,cte_Opp_OF_DELAI_Cumul_hebdo as (
select WeekOfYear,
	   ISNULL(SUM(DELAI_GAGNEES_OF)/(datediff(dd,@FirstDOW,@Date_obs)+1),0)             as DELAI_GAGNEES_OF_HEBDO, 
	   ISNULL(SUM(DELAI_GAGNEES_OF_Dig)/(datediff(dd,@FirstDOW,@Date_obs)+1),0)         as DELAI_GAGNEES_OF_Dig_HEBDO, 
	   ISNULL(SUM(DELAI_GAGNEES_OF_Btq)/(datediff(dd,@FirstDOW,@Date_obs)+1),0)         as DELAI_GAGNEES_OF_Btq_HEBDO
from cte_Opp_OF_DELAI_Cumul
where  DTE_ALIM >= @FirstDOW and DTE_ALIM <=@Date_obs
GROUP BY WeekOfYear)
--select * from cte_Opp_OF_DELAI_Cumul_hebdo

,cte_Opp_Calcul_Iter_Cumul_hebdo as (
SELECT  WeekOfYear,
		ISNULL(SUM(NB_ITERATION_OB),0)         as NB_ITERATION_OB_HEBDO, 
		ISNULL(SUM(NB_ITERATION_OB_0),0)       as NB_ITERATION_OB_0_HEBDO, 
		ISNULL(SUM(NB_ITERATION_OB_1_2),0)     as NB_ITERATION_OB_1_2_HEBDO, 
		ISNULL(SUM(NB_ITERATION_OB_plus_2),0)  as NB_ITERATION_OB_plus_2_HEBDO, 
		ISNULL(SUM(NB_ITERATION_OF),0)         as NB_ITERATION_OF_HEBDO, 
		ISNULL(SUM(NB_ITERATION_OF_0),0)       as NB_ITERATION_OF_0_HEBDO, 
		ISNULL(SUM(NB_ITERATION_OF_1_2),0)     as NB_ITERATION_OF_1_2_HEBDO, 
		ISNULL(SUM(NB_ITERATION_OF_plus_2),0)  as NB_ITERATION_OF_plus_2_HEBDO
FROM cte_Opp_Calcul_Iter_Cumul
where  DTE_ALIM >= @FirstDOW and DTE_ALIM <=@Date_obs
GROUP BY [WeekOfYear])
--select * from cte_Opp_Calcul_Iter_Cumul_hebdo

--Opport CAV perdues Cumul

,Cte_Opp_CAV_Perdues_Cumul_hebdo as (
SELECT  WeekOfYear,
		ISNULL(SUM(NB_OPP_PERDUES),0)           as NB_OPP_PERDUES_HEBDO,
		ISNULL(SUM(NB_PERDUES_OB_INC_TECH),0)   as NB_PERDUES_OB_INC_TECH_HEBDO,
		ISNULL(SUM(NB_PERDUES_OB_SANS_SUITE),0) as NB_PERDUES_OB_SANS_SUITE_HEBDO,
		ISNULL(SUM(NB_PERDUES_OB_AFF_REF),0)    as NB_PERDUES_OB_AFF_REF_HEBDO,
		ISNULL(SUM(NB_PERDUES_OB_RETRACT),0)    as NB_PERDUES_OB_RETRACT_HEBDO,
		ISNULL(SUM(NB_PERDUES_OF_INC_TECH),0)   as NB_PERDUES_OF_INC_TECH_HEBDO,
		ISNULL(SUM(NB_PERDUES_OF_SANS_SUITE),0) as NB_PERDUES_OF_SANS_SUITE_HEBDO,
		ISNULL(SUM(NB_PERDUES_OF_AFF_REF),0)    as NB_PERDUES_OF_AFF_REF_HEBDO,
		ISNULL(SUM(NB_PERDUES_OF_RETRACT),0)    as NB_PERDUES_OF_RETRACT_HEBDO 
FROM Cte_Opp_CAV_Perdues_Cumul
where  DTE_ALIM >= @FirstDOW and DTE_ALIM <=@Date_obs
GROUP BY WeekOfYear
)
--select * from Cte_Opp_CAV_Perdues_Cumul_hebdo

--SAV Réclamations Cumul

,Cte_Reclamations_Cumul_hebdo as (
select [WeekOfYear], 
		ISNULL(SUM(NB_RECLA),0)                as NB_RECLA_HEBDO, 
		ISNULL(SUM(NB_RECLA_ENROL),0)          as NB_RECLA_ENROL_HEBDO, 
		ISNULL(SUM(NB_RECLA_MOB_SMS),0)        as NB_RECLA_MOB_SMS_HEBDO, 
		ISNULL(SUM(NB_RECLA_NET_CLT),0)        as NB_RECLA_NET_CLT_HEBDO, 
		ISNULL(SUM(NB_RECLA_COURRIER),0)       as NB_RECLA_COURRIER_HEBDO, 
		ISNULL(SUM(NB_RECLA_APP_ENTR),0)       as NB_RECLA_APP_ENTR_HEBDO, 
		ISNULL(SUM(NB_RECLA_APP_SORT),0)       as NB_RECLA_APP_SORT_HEBDO, 
		ISNULL(SUM(NB_RECLA_CHAT),0)           as NB_RECLA_CHAT_HEBDO, 
		ISNULL(SUM(NB_RECLA_RES_SOC),0)        as NB_RECLA_RES_SOC_HEBDO, 
		ISNULL(SUM(NB_RECLA_APP_ENTR_TELCO),0) as NB_RECLA_APP_ENTR_TELCO_HEBDO, 
		ISNULL(SUM(NB_RECLA_APP_ENTR_Btq),0)   as NB_RECLA_APP_ENTR_Btq_HEBDO, 
		ISNULL(SUM(NB_RECLA_MAIL),0)           as NB_RECLA_MAIL_HEBDO, 
		ISNULL(SUM(NB_RECLA_FOR_DEM),0)        as NB_RECLA_FOR_DEM_HEBDO, 
		ISNULL(SUM(NB_RECLA_SFMC),0)           as NB_RECLA_SFMC_HEBDO
from Cte_Reclamations
where DTE_CREA_RECLA >= @FirstDOW and DTE_CREA_RECLA<=@Date_obs
GROUP BY [WeekOfYear])
--select * from Cte_Reclamations_Cumul_hebdo

/*Cumul Hebdo de la semaine précédente*/

--Opport.CAV Créées hebdo prec
,Cte_Opp_CAV_Creees_hebdo_prec as(
SELECT [DTE_CREA],
       DATEPART(ISO_WEEK,[DTE_CREA]) as [WeekOfYear_c],
	   case when ([COD_RES_DIS] in ('01','02') or [COD_CAN_INT_ORI] IN ('10','11','12','13','14','15') or [ENT_DIS] in ('01','02','04') or [FLG_IND] in ('Non','Oui')) then COUNT([IDE_OPPRT_SF]) end as NB_OPP_CREEES_HEBDO_PREC,
	   case when [COD_RES_DIS] = '02' then COUNT ([IDE_OPPRT_SF]) end                                                                                                                                 as NB_CREEES_OB_HEBDO_PREC,
       case when ([COD_RES_DIS] = '02' and [COD_CAN_INT_ORI] IN ('10','11')) then COUNT ([IDE_OPPRT_SF])  end                                                                                         as NB_CREEES_OB_Dig_HEBDO_PREC,
	   case when ([COD_RES_DIS] = '02' and [COD_CAN_INT_ORI] IN ('12','13','14','15')) then COUNT ([IDE_OPPRT_SF])  end                                                                               as NB_CREEES_OB_CRC_HEBDO_PREC,
	   case when ([COD_RES_DIS] = '01') then COUNT ([IDE_OPPRT_SF])  end                                                                                                                              as NB_CREEES_OF_HEBDO_PREC,
	   case when ([COD_RES_DIS] = '01' and [ENT_DIS]='04') then COUNT ([IDE_OPPRT_SF])  end                                                                                                           as NB_CREEES_OF_Dig_HEBDO_PREC,
	   case when ([COD_RES_DIS] = '01' and [ENT_DIS] in ('01','02')) then COUNT ([IDE_OPPRT_SF])  end                                                                                                 as NB_CREEES_OF_Btq_HEBDO_PREC,
	   case when ([COD_RES_DIS] = '01' and [FLG_IND] = 'Non') then COUNT ([IDE_OPPRT_SF])  end                                                                                                        as NB_CREEES_OF_SS_HEBDO_PREC,	   
	   case when ([COD_RES_DIS] = '01' and [FLG_IND] = 'Non' and [ENT_DIS] = '04') then COUNT ([IDE_OPPRT_SF])  end                                                                                   as NB_CREEES_OF_SS_Dig_HEBDO_PREC,
	   case when ([COD_RES_DIS] = '01' and [FLG_IND] = 'Non' and [ENT_DIS] in ('01','02')) then COUNT ([IDE_OPPRT_SF])  end                                                                           as NB_CREEES_OF_SS_Btq_HEBDO_PREC,
	   case when ([COD_RES_DIS] = '01' and [FLG_IND] = 'Oui') then COUNT ([IDE_OPPRT_SF])  end                                                                                                        as NB_CREEES_OF_AC_HEBDO_PREC,
	   case when ([COD_RES_DIS] = '01' and [FLG_IND] = 'Oui' and [ENT_DIS] = '04') then COUNT ([IDE_OPPRT_SF])  end                                                                                   as NB_CREEES_OF_AC_Dig_HEBDO_PREC,
	   case when ([COD_RES_DIS] = '01' and [FLG_IND] = 'Oui' and [ENT_DIS] in ('01','02')) then COUNT ([IDE_OPPRT_SF])  end                                                                           as NB_CREEES_OF_AC_Btq_HEBDO_PREC
FROM cte_opp_cav_distinct_crees
WHERE DTE_CREA between @FirstDOPW and DATEADD(DD,6, @FirstDOPW) and rang=1
GROUP BY [DTE_CREA],
		[WeekOfYear],
		[COD_RES_DIS],
		[COD_CAN_INT_ORI],
		[FLG_IND],
		[ENT_DIS]
)
--select * from Cte_Opp_CAV_Creees_hebdo_prec

,Cte_Opp_CAV_Creees_Cumul_hebdo_prec as(
SELECT  [WeekOfYear_c],
		ISNULL(SUM(NB_OPP_CREEES_HEBDO_PREC),0)       as NB_OPP_CREEES_HEBDO_PREC,
		ISNULL(SUM(NB_CREEES_OB_HEBDO_PREC),0)        as NB_CREEES_OB_HEBDO_PREC,
		ISNULL(SUM(NB_CREEES_OB_Dig_HEBDO_PREC),0)    as NB_CREEES_OB_Dig_HEBDO_PREC,
		ISNULL(SUM(NB_CREEES_OB_CRC_HEBDO_PREC),0)    as NB_CREEES_OB_CRC_HEBDO_PREC,
		ISNULL(SUM(NB_CREEES_OF_HEBDO_PREC),0)        as NB_CREEES_OF_HEBDO_PREC,
		ISNULL(SUM(NB_CREEES_OF_Dig_HEBDO_PREC),0)    as NB_CREEES_OF_Dig_HEBDO_PREC,
		ISNULL(SUM(NB_CREEES_OF_Btq_HEBDO_PREC),0)    as NB_CREEES_OF_Btq_HEBDO_PREC,
		ISNULL(SUM(NB_CREEES_OF_SS_HEBDO_PREC),0)     as NB_CREEES_OF_SS_HEBDO_PREC,
		ISNULL(SUM(NB_CREEES_OF_SS_Dig_HEBDO_PREC),0) as NB_CREEES_OF_SS_Dig_HEBDO_PREC,
		ISNULL(SUM(NB_CREEES_OF_SS_Btq_HEBDO_PREC),0) as NB_CREEES_OF_SS_Btq_HEBDO_PREC,
		ISNULL(SUM(NB_CREEES_OF_AC_HEBDO_PREC),0)     as NB_CREEES_OF_AC_HEBDO_PREC,
		ISNULL(SUM(NB_CREEES_OF_AC_Dig_HEBDO_PREC),0) as NB_CREEES_OF_AC_Dig_HEBDO_PREC,
		ISNULL(SUM(NB_CREEES_OF_AC_Btq_HEBDO_PREC),0) as NB_CREEES_OF_AC_Btq_HEBDO_PREC 
FROM Cte_Opp_CAV_Creees_hebdo_prec
GROUP BY [WeekOfYear_c]
)
--select * from Cte_Opp_CAV_Creees_Cumul_hebdo_prec

--Opport CAV en cours de vie hebdo prec
,Cte_Opp_CAV_En_cours_hebdo_prec as(
SELECT DTE_ALIM,
       WeekOfYear,
       COUNT ([IDE_OPPRT_SF])                                                              as NB_OPP_ENCRS_HEBDO_PREC,
	   case when [COD_RES_DIS]='02' and STADE_VENTE = '01' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OB_DETEC_HEBDO_PREC,
       case when [COD_RES_DIS]='02' and STADE_VENTE = '02' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OB_DECOUV_HEBDO_PREC,
	   case when [COD_RES_DIS]='02' and STADE_VENTE = '03' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OB_ELAB_HEBDO_PREC,
	   case when [COD_RES_DIS]='02' and STADE_VENTE = '04' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OB_AFF_SIGN_HEBDO_PREC,
	   case when [COD_RES_DIS]='02' and STADE_VENTE = '05' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OB_VERIF_HEBDO_PREC,
	   case when [COD_RES_DIS]='02' and STADE_VENTE = '06' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OB_DOSS_INC_HEBDO_PREC,
	   case when [COD_RES_DIS]='02' and STADE_VENTE = '07' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OB_DOSS_VAL_HEBDO_PREC,
	   case when [COD_RES_DIS]='01' and STADE_VENTE = '01' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OF_DETEC_HEBDO_PREC,
	   case when [COD_RES_DIS]='01' and STADE_VENTE = '02' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OF_DECOUV_HEBDO_PREC,
	   case when [COD_RES_DIS]='01' and STADE_VENTE = '03' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OF_ELAB_HEBDO_PREC,
	   case when [COD_RES_DIS]='01' and STADE_VENTE = '04' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OF_AFF_SIGN_HEBDO_PREC,
	   case when [COD_RES_DIS]='01' and STADE_VENTE = '05' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OF_VERIF_HEBDO_PREC,
	   case when [COD_RES_DIS]='01' and STADE_VENTE = '06' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OF_DOSS_INC_HEBDO_PREC,
	   case when [COD_RES_DIS]='01' and STADE_VENTE = '07' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OF_DOSS_VAL_HEBDO_PREC
FROM cte_Opp_ALL
WHERE DTE_ALIM between @FirstDOPW and DATEADD(DD,6, @FirstDOPW)
AND STADE_VENTE IN ('01','02','03','04','05','06','07') and COD_RES_DIS in ('01','02') and rang=1
AND DTE_CLO_OPPRT > @Date_obs and DTE_CREA <= @Date_obs
GROUP BY DTE_ALIM,
		 STADE_VENTE,
		 [WeekOfYear],
		 [COD_RES_DIS]
)
--select * from Cte_Opp_CAV_En_cours_hebdo_prec

,Cte_Opp_CAV_En_cours_Cumul_hebdo_prec as(
SELECT  WeekOfYear,
		ISNULL(SUM(NB_OPP_ENCRS_HEBDO_PREC),0)         as NB_OPP_ENCRS_HEBDO_PREC,
		ISNULL(SUM(NB_ENCRS_OB_DETEC_HEBDO_PREC),0)    as NB_ENCRS_OB_DETEC_HEBDO_PREC,
		ISNULL(SUM(NB_ENCRS_OB_DECOUV_HEBDO_PREC),0)   as NB_ENCRS_OB_DECOUV_HEBDO_PREC,
		ISNULL(SUM(NB_ENCRS_OB_ELAB_HEBDO_PREC),0)     as NB_ENCRS_OB_ELAB_HEBDO_PREC,
		ISNULL(SUM(NB_ENCRS_OB_AFF_SIGN_HEBDO_PREC),0) as NB_ENCRS_OB_AFF_SIGN_HEBDO_PREC,
		ISNULL(SUM(NB_ENCRS_OB_VERIF_HEBDO_PREC),0)    as NB_ENCRS_OB_VERIF_HEBDO_PREC,
		ISNULL(SUM(NB_ENCRS_OB_DOSS_INC_HEBDO_PREC),0) as NB_ENCRS_OB_DOSS_INC_HEBDO_PREC,
		ISNULL(SUM(NB_ENCRS_OB_DOSS_VAL_HEBDO_PREC),0) as NB_ENCRS_OB_DOSS_VAL_HEBDO_PREC,
		ISNULL(SUM(NB_ENCRS_OF_DETEC_HEBDO_PREC),0)    as NB_ENCRS_OF_DETEC_HEBDO_PREC,
		ISNULL(SUM(NB_ENCRS_OF_DECOUV_HEBDO_PREC),0)   as NB_ENCRS_OF_DECOUV_HEBDO_PREC,
		ISNULL(SUM(NB_ENCRS_OF_ELAB_HEBDO_PREC),0)     as NB_ENCRS_OF_ELAB_HEBDO_PREC,
		ISNULL(SUM(NB_ENCRS_OF_AFF_SIGN_HEBDO_PREC),0) as NB_ENCRS_OF_AFF_SIGN_HEBDO_PREC,
		ISNULL(SUM(NB_ENCRS_OF_VERIF_HEBDO_PREC),0)    as NB_ENCRS_OF_VERIF_HEBDO_PREC,
		ISNULL(SUM(NB_ENCRS_OF_DOSS_INC_HEBDO_PREC),0) as NB_ENCRS_OF_DOSS_INC_HEBDO_PREC,
		ISNULL(SUM(NB_ENCRS_OF_DOSS_VAL_HEBDO_PREC),0) as NB_ENCRS_OF_DOSS_VAL_HEBDO_PREC
FROM Cte_Opp_CAV_En_cours_hebdo_prec
GROUP BY WeekOfYear
)
--select * from Cte_Opp_CAV_En_cours_Cumul_hebdo_prec

--Opport CAV gagnees hebdo prec

,Cte_Opp_CAV_Gagnees_hebdo_prec as (
SELECT DTE_ALIM,
       [WeekOfYear],
	   --DELAI_GAGNEES,
       COUNT (y.[IDE_OPPRT_SF])                                                                                                                                                                            as NB_OPP_GAGNEES_HEBDO_PREC,
       case when [COD_RES_DIS] = '02' then COUNT (y.[IDE_OPPRT_SF]) end                                                                                                                                    as NB_GAGNEES_OB_HEBDO_PREC,
	   case when ([COD_RES_DIS] = '02' and [COD_CAN_INT_ORI] IN ('10','11')) then COUNT (y.[IDE_OPPRT_SF]) end                                                                                             as NB_GAGNEES_OB_Dig_HEBDO_PREC,
	   case when ([COD_RES_DIS] = '02' and [COD_CAN_INT_ORI] IN ('12','13','14','15')) then COUNT (y.[IDE_OPPRT_SF]) end                                                                                   as NB_GAGNEES_OB_CRC_HEBDO_PREC,
	   case when [COD_RES_DIS] = '01' then COUNT (y.[IDE_OPPRT_SF]) end                                                                                                                                    as NB_GAGNEES_OF_HEBDO_PREC,
	   case when ([COD_RES_DIS] = '01' and [ENT_DIS] = '04') then COUNT (y.[IDE_OPPRT_SF])  end                                                                                                            as NB_GAGNEES_OF_Dig_HEBDO_PREC,
	   case when ([COD_RES_DIS] = '01' and [ENT_DIS] in ('01','02')) then COUNT (y.[IDE_OPPRT_SF])  end                                                                                                    as NB_GAGNEES_OF_Btq_HEBDO_PREC,
	   case when ([COD_RES_DIS] = '01' and [FLG_IND] = 'Non' and [ENT_DIS] = '04') then COUNT (y.[IDE_OPPRT_SF])  end                                                                                      as NB_GAGNEES_OF_SS_Dig_HEBDO_PREC,
	   case when ([COD_RES_DIS] = '01' and [FLG_IND] = 'Non' and [ENT_DIS] in ('01','02')) then COUNT (y.[IDE_OPPRT_SF])  end                                                                              as NB_GAGNEES_OF_SS_Btq_HEBDO_PREC,
	   case when ([COD_RES_DIS] = '01' and [FLG_IND] = 'Oui' and [ENT_DIS] = '04') then COUNT (y.[IDE_OPPRT_SF])  end                                                                                      as NB_GAGNEES_OF_AC_Dig_HEBDO_PREC,
	   case when ([COD_RES_DIS] = '01' and [FLG_IND] = 'Oui' and [ENT_DIS] in ('01','02')) then COUNT (y.[IDE_OPPRT_SF])  end                                                                              as NB_GAGNEES_OF_AC_Btq_HEBDO_PREC,
	   case when [COD_RES_DIS] = '03' then COUNT (y.[IDE_OPPRT_SF]) end                                                                                                                                    as NB_GAGNEES_GP_HEBDO_PREC,
	   case when ([COD_RES_DIS] = '03' and [FLG_IND] = 'Oui') then COUNT (y.[IDE_OPPRT_SF])  end                                                                                                           as NB_GAGNEES_GP_AC_HEBDO_PREC,
	   case when ([COD_RES_DIS] = '03' and [FLG_IND] = 'Non') then COUNT (y.[IDE_OPPRT_SF])  end                                                                                                           as NB_GAGNEES_GP_SS_HEBDO_PREC 	   
FROM cte_Opp_ALL y inner join cte_opp_gag on y.IDE_OPPRT_SF=cte_opp_gag.IDE_OPPRT_SF and y.DTE_ALIM=CAST(cte_opp_gag.DTE_DER_MOD as date)
WHERE DTE_ALIM between @FirstDOPW and DATEADD(DD,6, @FirstDOPW)
AND y.STADE_VENTE = '09' and rang=1 AND DTE_CLO_OPPRT=@Date_obs
GROUP BY DTE_ALIM,
         [WeekOfYear],
		 COD_RES_DIS,
		 COD_CAN_INT_ORI,
		 [FLG_IND], 
		 [ENT_DIS],  
		 NB_ITERATION,
		 STADE_VENTE
)
--select * from Cte_Opp_CAV_Gagnees_hebdo_prec

,Cte_Opp_CAV_Gagnees_Cumul_hebdo_prec as (
SELECT  WeekOfYear,
		ISNULL(SUM(NB_OPP_GAGNEES_HEBDO_PREC),0)           as NB_OPP_GAGNEES_HEBDO_PREC,
		ISNULL(SUM(NB_GAGNEES_OB_HEBDO_PREC),0)            as NB_GAGNEES_OB_HEBDO_PREC ,
		ISNULL(SUM(NB_GAGNEES_OB_Dig_HEBDO_PREC),0)        as NB_GAGNEES_OB_Dig_HEBDO_PREC,
		ISNULL(SUM(NB_GAGNEES_OB_CRC_HEBDO_PREC),0)        as NB_GAGNEES_OB_CRC_HEBDO_PREC,
		ISNULL(SUM(NB_GAGNEES_OF_HEBDO_PREC),0)            as NB_GAGNEES_OF_HEBDO_PREC,
		ISNULL(SUM(NB_GAGNEES_OF_Dig_HEBDO_PREC),0)        as NB_GAGNEES_OF_Dig_HEBDO_PREC,
		ISNULL(SUM(NB_GAGNEES_OF_Btq_HEBDO_PREC),0)        as NB_GAGNEES_OF_Btq_HEBDO_PREC,
		ISNULL(SUM(NB_GAGNEES_OF_SS_Dig_HEBDO_PREC),0)     as NB_GAGNEES_OF_SS_Dig_HEBDO_PREC,
		ISNULL(SUM(NB_GAGNEES_OF_SS_Btq_HEBDO_PREC),0)     as NB_GAGNEES_OF_SS_Btq_HEBDO_PREC,
		ISNULL(SUM(NB_GAGNEES_OF_AC_Dig_HEBDO_PREC),0)     as NB_GAGNEES_OF_AC_Dig_HEBDO_PREC,
		ISNULL(SUM(NB_GAGNEES_OF_AC_Btq_HEBDO_PREC),0)     as NB_GAGNEES_OF_AC_Btq_HEBDO_PREC ,
		ISNULL(SUM(NB_GAGNEES_GP_HEBDO_PREC),0)            as NB_GAGNEES_GP_HEBDO_PREC,
		ISNULL(SUM(NB_GAGNEES_GP_AC_HEBDO_PREC),0)         as NB_GAGNEES_GP_AC_HEBDO_PREC,
		ISNULL(SUM(NB_GAGNEES_GP_SS_HEBDO_PREC),0)         as NB_GAGNEES_GP_SS_HEBDO_PREC
FROM Cte_Opp_CAV_Gagnees_hebdo_prec
GROUP BY WeekOfYear
)
--select * from Cte_Opp_CAV_Gagnees_Cumul_hebdo_prec
--Calcul du délai médian de transformation OB hebdo_prec

,cte_Opp_OB_DELAI_hebdo_prec as (
SELECT distinct DTE_ALIM,WeekOfYear, COD_RES_DIS,COD_CAN_INT_ORI, STADE_VENTE,
case when [COD_RES_DIS] = '02' then PERCENTILE_CONT (0.5)  WITHIN GROUP (ORDER BY DELAI_GAGNEES) OVER (PARTITION BY COD_RES_DIS,DTE_ALIM, CASE WHEN STADE_VENTE='09' THEN STADE_VENTE END, CASE WHEN COD_RES_DIS = '02' THEN COD_RES_DIS END) end																											                     AS DELAI_GAGNEES_OB_HEBDO_PREC, 
case when ([COD_RES_DIS] = '02' and [COD_CAN_INT_ORI] IN ('10','11')) then PERCENTILE_CONT (0.5)  WITHIN GROUP (ORDER BY DELAI_GAGNEES) OVER (PARTITION BY COD_RES_DIS,DTE_ALIM,CASE WHEN STADE_VENTE='09' THEN STADE_VENTE END, CASE WHEN COD_RES_DIS = '02' THEN COD_RES_DIS END,CASE WHEN [COD_CAN_INT_ORI] IN ('10','11') then COD_CAN_INT_ORI end ) end                     AS DELAI_GAGNEES_OB_Dig_HEBDO_PREC, 
case when ([COD_RES_DIS] = '02' and [COD_CAN_INT_ORI] IN ('12','13','14','15')) then PERCENTILE_CONT (0.5)  WITHIN GROUP (ORDER BY DELAI_GAGNEES) OVER (PARTITION BY COD_RES_DIS,DTE_ALIM,CASE WHEN STADE_VENTE='09' THEN STADE_VENTE END, CASE WHEN COD_RES_DIS = '02' THEN COD_RES_DIS END,CASE WHEN [COD_CAN_INT_ORI] IN ('12','13','14','15') then COD_CAN_INT_ORI end) end  AS DELAI_GAGNEES_OB_CRC_HEBDO_PREC
from cte_opp_all y inner join cte_opp_gag on y.IDE_OPPRT_SF=cte_opp_gag.IDE_OPPRT_SF and y.DTE_ALIM=CAST(cte_opp_gag.DTE_DER_MOD as date)
WHERE DTE_ALIM between @FirstDOPW and DATEADD(DD,6, @FirstDOPW)
and rang=1
and DTE_CLO_OPPRT=@Date_obs)
--select * from cte_Opp_OB_DELAI_hebdo_prec where DTE_ALIM='2018-06-05' and STADE_VENTE='09'

,cte_Opp_OB_DELAI_Cumul_hebdo_prec as (
select DTE_alim,WeekOfYear,
       ISNULL(SUM(DELAI_GAGNEES_OB_HEBDO_PREC)/7,0)             as DELAI_GAGNEES_OB_HEBDO_PREC, 
	   ISNULL(SUM(DELAI_GAGNEES_OB_Dig_HEBDO_PREC)/7,0)         as DELAI_GAGNEES_OB_Dig_HEBDO_PREC, 
	   ISNULL(SUM(DELAI_GAGNEES_OB_CRC_HEBDO_PREC)/7,0)         as DELAI_GAGNEES_OB_CRC_HEBDO_PREC
from cte_Opp_OB_DELAI_hebdo_prec
GROUP BY DTE_ALIM,WeekOfYear)
--select * from cte_Opp_OB_DELAI_Cumul_hebdo_prec

--Calcul du délai médian de transformation OF hebdo_prec

,cte_Opp_OF_DELAI_hebdo_prec as (
SELECT distinct DTE_ALIM,WeekOfYear, COD_RES_DIS,ENT_DIS, STADE_VENTE,
case when [COD_RES_DIS] = '01' then PERCENTILE_CONT (0.5)  WITHIN GROUP (ORDER BY DELAI_GAGNEES) OVER (PARTITION BY COD_RES_DIS,DTE_ALIM,CASE WHEN STADE_VENTE='09' THEN STADE_VENTE END, CASE WHEN COD_RES_DIS = '01' THEN COD_RES_DIS END) end                                                                                     AS DELAI_GAGNEES_OF_HEBDO_PREC, 
case when ([COD_RES_DIS] = '01' and [ENT_DIS] = '04') then PERCENTILE_CONT (0.5)  WITHIN GROUP (ORDER BY DELAI_GAGNEES) OVER (PARTITION BY COD_RES_DIS,DTE_ALIM,CASE WHEN STADE_VENTE='09' THEN STADE_VENTE END, CASE WHEN COD_RES_DIS = '01' THEN COD_RES_DIS END,CASE WHEN ENT_DIS= '04' then ENT_DIS end ) end                    AS DELAI_GAGNEES_OF_Dig_HEBDO_PREC,   
case when ([COD_RES_DIS] = '01' and [ENT_DIS] in ('01','02'))  then PERCENTILE_CONT (0.5)  WITHIN GROUP (ORDER BY DELAI_GAGNEES) OVER (PARTITION BY COD_RES_DIS,DTE_ALIM,CASE WHEN STADE_VENTE='09' THEN STADE_VENTE END, CASE WHEN COD_RES_DIS = '01' THEN COD_RES_DIS END,CASE WHEN ENT_DIS in ('01','02') then ENT_DIS end ) end  AS DELAI_GAGNEES_OF_Btq_HEBDO_PREC
from cte_opp_all y inner join cte_opp_gag on y.IDE_OPPRT_SF=cte_opp_gag.IDE_OPPRT_SF and y.DTE_ALIM=CAST(cte_opp_gag.DTE_DER_MOD as date)
WHERE DTE_ALIM between @FirstDOPW and DATEADD(DD,6, @FirstDOPW)
and rang=1
and DTE_CLO_OPPRT=@Date_obs)
--select * from cte_Opp_OF_DELAI_hebdo_prec where DTE_ALIM='2018-06-05' and STADE_VENTE='09'

,cte_Opp_OF_DELAI_Cumul_hebdo_prec as (
select DTE_alim,WeekOfYear,
	   ISNULL(SUM(DELAI_GAGNEES_OF_HEBDO_PREC)/7,0)             as DELAI_GAGNEES_OF_HEBDO_PREC, 
	   ISNULL(SUM(DELAI_GAGNEES_OF_Dig_HEBDO_PREC)/7,0)         as DELAI_GAGNEES_OF_Dig_HEBDO_PREC, 
	   ISNULL(SUM(DELAI_GAGNEES_OF_Btq_HEBDO_PREC)/7,0)         as DELAI_GAGNEES_OF_Btq_HEBDO_PREC
from cte_Opp_OF_DELAI_hebdo_prec
GROUP BY DTE_ALIM,WeekOfYear)
--select * from cte_Opp_OF_DELAI_Cumul_hebdo_prec

--Calcul du nb d'iterations
,cte_Opp_Calcul_Iter_hebdo_prec as (
SELECT DTE_ALIM,
       WeekOfYear,
	   case when y.[COD_RES_DIS] = '02' and STADE_VENTE='09' then COUNT(y.NB_ITERATION) end                                   AS NB_ITERATION_OB_HEBDO_PREC, 
	   case when y.[COD_RES_DIS] = '02' and STADE_VENTE='09'and y. NB_ITERATION = '0' then COUNT(y.NB_ITERATION) end          AS NB_ITERATION_OB_0_HEBDO_PREC, 
	   case when y.[COD_RES_DIS] = '02' and STADE_VENTE='09'and y. NB_ITERATION IN ('1','2') then COUNT(y.NB_ITERATION) end   AS NB_ITERATION_OB_1_2_HEBDO_PREC, 
	   case when y.[COD_RES_DIS] = '02' and STADE_VENTE='09' and y.NB_ITERATION > '2'  then COUNT(y.NB_ITERATION) end         AS NB_ITERATION_OB_plus_2_HEBDO_PREC, 
	   case when y.[COD_RES_DIS] = '01' and STADE_VENTE='09' then COUNT(y.NB_ITERATION) end                                   AS NB_ITERATION_OF_HEBDO_PREC, 
	   case when y.[COD_RES_DIS] = '01' and STADE_VENTE='09' and y.NB_ITERATION = '0' then COUNT(y.NB_ITERATION) end          AS NB_ITERATION_OF_0_HEBDO_PREC, 
	   case when y.[COD_RES_DIS] = '01' and STADE_VENTE='09' and y.NB_ITERATION IN ('1','2') then COUNT(y.NB_ITERATION) end   AS NB_ITERATION_OF_1_2_HEBDO_PREC, 
	   case when y.[COD_RES_DIS] = '01' and STADE_VENTE='09' and y.NB_ITERATION > '2'  then COUNT(y.NB_ITERATION) end         AS NB_ITERATION_OF_plus_2_HEBDO_PREC	  
FROM cte_Opp_ALL y left join (select OpportunityId, count(NewValue) as NB_ITERATION
                              from [$(DataHubDatabaseName)].dbo.WK_SF_OPPORTUNITYFIELDHISTORY 
                              where Field='StageName'
                              and NewValue='06'--Dossier Incomplet
                              group by OpportunityId) c  on  y.IDE_OPPRT_SF=c.OpportunityId 
					--inner join cte_opp_gag on y.IDE_OPPRT_SF=cte_opp_gag.IDE_OPPRT_SF and y.DTE_ALIM=CAST(cte_opp_gag.DTE_DER_MOD as date)

WHERE DTE_ALIM between @FirstDOPW and DATEADD(DD,6, @FirstDOPW)
AND y.STADE_VENTE = '09' AND DTE_CLO_OPPRT=@Date_obs and rang=1 
GROUP BY DTE_ALIM,
         [WeekOfYear],
		 COD_RES_DIS,
		 y.NB_ITERATION,
		 STADE_VENTE)
--select * from cte_Opp_Calcul_Iter_hebdo_prec

,cte_Opp_Calcul_Iter_Cumul_hebdo_prec as (
SELECT  DTE_ALIM,
        WeekOfYear,
		ISNULL(SUM(NB_ITERATION_OB_HEBDO_PREC),0)         as NB_ITERATION_OB_HEBDO_PREC, 
		ISNULL(SUM(NB_ITERATION_OB_0_HEBDO_PREC),0)       as NB_ITERATION_OB_0_HEBDO_PREC, 
		ISNULL(SUM(NB_ITERATION_OB_1_2_HEBDO_PREC),0)     as NB_ITERATION_OB_1_2_HEBDO_PREC, 
		ISNULL(SUM(NB_ITERATION_OB_plus_2_HEBDO_PREC),0)  as NB_ITERATION_OB_plus_2_HEBDO_PREC, 
		ISNULL(SUM(NB_ITERATION_OF_HEBDO_PREC),0)         as NB_ITERATION_OF_HEBDO_PREC, 
		ISNULL(SUM(NB_ITERATION_OF_0_HEBDO_PREC),0)       as NB_ITERATION_OF_0_HEBDO_PREC, 
		ISNULL(SUM(NB_ITERATION_OF_1_2_HEBDO_PREC),0)     as NB_ITERATION_OF_1_2_HEBDO_PREC, 
		ISNULL(SUM(NB_ITERATION_OF_plus_2_HEBDO_PREC),0)  as NB_ITERATION_OF_plus_2_HEBDO_PREC
FROM cte_Opp_Calcul_Iter_hebdo_prec
GROUP BY DTE_ALIM,
         [WeekOfYear])
--select * from cte_Opp_Calcul_Iter_Cumul_hebdo_prec

--Opport CAV perdues hebdo prec

,Cte_Opp_CAV_Perdues_hebdo_prec as (
SELECT DTE_ALIM,
       [WeekOfYear],
	   --y.IDE_OPPRT_SF,
       COUNT (y.[IDE_OPPRT_SF])                                                                                                                         as NB_OPP_PERDUES_HEBDO_PREC,
       case when (COD_RES_DIS = '02' and y.STADE_VENTE = '08' and cast(y.DTE_DER_MOD as date)=@Date_obs) then COUNT (y.[IDE_OPPRT_SF])              end as NB_PERDUES_OB_INC_TECH_HEBDO_PREC,
	   case when (COD_RES_DIS = '02' and y.STADE_VENTE = '11' and DTE_CLO_OPPRT=@Date_obs) then COUNT (y.[IDE_OPPRT_SF])                            end as NB_PERDUES_OB_SANS_SUITE_HEBDO_PREC,
	   case when (COD_RES_DIS = '02' and y.STADE_VENTE = '10' /*and DTE_CLO_OPPRT=@Date_obs*/) then COUNT (y.[IDE_OPPRT_SF])                        end as NB_PERDUES_OB_AFF_REF_HEBDO_PREC,
	   case when (COD_RES_DIS = '02' and y.STADE_VENTE = '12' and DTE_CLO_OPPRT=@Date_obs) then COUNT (y.[IDE_OPPRT_SF])                            end as NB_PERDUES_OB_RETRACT_HEBDO_PREC,
	   case when (COD_RES_DIS = '01' and y.STADE_VENTE = '08' and cast(y.DTE_DER_MOD as date)=@Date_obs) then COUNT (y.[IDE_OPPRT_SF])              end as NB_PERDUES_OF_INC_TECH_HEBDO_PREC,
	   case when (COD_RES_DIS = '01' and y.STADE_VENTE = '11' and DTE_CLO_OPPRT=@Date_obs)  then COUNT (y.[IDE_OPPRT_SF])                           end as NB_PERDUES_OF_SANS_SUITE_HEBDO_PREC,
	   case when (COD_RES_DIS = '01' and y.STADE_VENTE = '10' and DTE_CLO_OPPRT=@Date_obs)  then COUNT (y.[IDE_OPPRT_SF])                           end as NB_PERDUES_OF_AFF_REF_HEBDO_PREC,
	   case when (COD_RES_DIS = '01' and y.STADE_VENTE = '12' and DTE_CLO_OPPRT=@Date_obs)  then COUNT (y.[IDE_OPPRT_SF])                           end as NB_PERDUES_OF_RETRACT_HEBDO_PREC
FROM cte_Opp_ALL y inner join cte_opp_per on y.IDE_OPPRT_SF=cte_opp_per.IDE_OPPRT_SF and y.DTE_ALIM=CAST(cte_opp_per.DTE_DER_MOD as date)
WHERE DTE_ALIM between @FirstDOPW and DATEADD(DD,6, @FirstDOPW)
AND /*y.STADE_VENTE IN ('08','11','10','12') and*/ COD_RES_DIS IN ('01','02') and (DTE_CLO_OPPRT=@Date_obs  or cast(y.DTE_DER_MOD as date) =@Date_obs) and rang=1
GROUP BY DTE_ALIM,
        --y.IDE_OPPRT_SF,
        y.DTE_DER_MOD,
		DTE_CLO_OPPRT,
        [WeekOfYear], 
		COD_RES_DIS,
		y.STADE_VENTE
)
--select * from Cte_Opp_CAV_Perdues_hebdo_prec

,Cte_Opp_CAV_Perdues_Cumul_hebdo_prec as (
SELECT  WeekOfYear,
		ISNULL(SUM(NB_OPP_PERDUES_HEBDO_PREC),0)           as NB_OPP_PERDUES_HEBDO_PREC,
		ISNULL(SUM(NB_PERDUES_OB_INC_TECH_HEBDO_PREC),0)   as NB_PERDUES_OB_INC_TECH_HEBDO_PREC,
		ISNULL(SUM(NB_PERDUES_OB_SANS_SUITE_HEBDO_PREC),0) as NB_PERDUES_OB_SANS_SUITE_HEBDO_PREC,
		ISNULL(SUM(NB_PERDUES_OB_AFF_REF_HEBDO_PREC),0)    as NB_PERDUES_OB_AFF_REF_HEBDO_PREC,
		ISNULL(SUM(NB_PERDUES_OB_RETRACT_HEBDO_PREC),0)    as NB_PERDUES_OB_RETRACT_HEBDO_PREC,
		ISNULL(SUM(NB_PERDUES_OF_INC_TECH_HEBDO_PREC),0)   as NB_PERDUES_OF_INC_TECH_HEBDO_PREC,
		ISNULL(SUM(NB_PERDUES_OF_SANS_SUITE_HEBDO_PREC),0) as NB_PERDUES_OF_SANS_SUITE_HEBDO_PREC,
		ISNULL(SUM(NB_PERDUES_OF_AFF_REF_HEBDO_PREC),0)    as NB_PERDUES_OF_AFF_REF_HEBDO_PREC,
		ISNULL(SUM(NB_PERDUES_OF_RETRACT_HEBDO_PREC),0)    as NB_PERDUES_OF_RETRACT_HEBDO_PREC 
FROM Cte_Opp_CAV_Perdues_hebdo_prec
GROUP BY WeekOfYear
)
--select * from Cte_Opp_CAV_Perdues_Cumul_hebdo_prec

,Cte_Reclamations_hebdo_prec as (
select  DTE_CREA_RECLA, 
        [WeekOfYear],
		count(IDE_REQ_SF)                                                          as NB_RECLA_HEBDO_PREC, 
		case when TYP_REQ='02'  then count(IDE_REQ_SF) end                         as NB_RECLA_ENROL_HEBDO_PREC, 
		case when TYP_REQ='02'  and  CAN_INT_ORI = '01' then count(IDE_REQ_SF) end as NB_RECLA_MOB_SMS_HEBDO_PREC, 
		case when TYP_REQ='02'  and  CAN_INT_ORI = '02' then count(IDE_REQ_SF) end as NB_RECLA_NET_CLT_HEBDO_PREC, 
		case when TYP_REQ='02'  and  CAN_INT_ORI = '03' then count(IDE_REQ_SF) end as NB_RECLA_COURRIER_HEBDO_PREC, 
		case when TYP_REQ='02'  and  CAN_INT_ORI = '04' then count(IDE_REQ_SF) end as NB_RECLA_APP_ENTR_HEBDO_PREC, 
		case when TYP_REQ='02'  and  CAN_INT_ORI = '05' then count(IDE_REQ_SF) end as NB_RECLA_APP_SORT_HEBDO_PREC, 
		case when TYP_REQ='02'  and  CAN_INT_ORI = '07' then count(IDE_REQ_SF) end as NB_RECLA_CHAT_HEBDO_PREC, 
		case when TYP_REQ='02'  and  CAN_INT_ORI = '08' then count(IDE_REQ_SF) end as NB_RECLA_RES_SOC_HEBDO_PREC, 
		case when TYP_REQ='02'  and  CAN_INT_ORI = '09' then count(IDE_REQ_SF) end as NB_RECLA_APP_ENTR_TELCO_HEBDO_PREC, 
		case when TYP_REQ='02'  and  CAN_INT_ORI = '10' then count(IDE_REQ_SF) end as NB_RECLA_APP_ENTR_Btq_HEBDO_PREC, 
		case when TYP_REQ='02'  and  CAN_INT_ORI = '11' then count(IDE_REQ_SF) end as NB_RECLA_MAIL_HEBDO_PREC, 
		case when TYP_REQ='02'  and  CAN_INT_ORI = '12' then count(IDE_REQ_SF) end as NB_RECLA_FOR_DEM_HEBDO_PREC, 
		case when TYP_REQ='02'  and  CAN_INT_ORI = '13' then count(IDE_REQ_SF) end as NB_RECLA_SFMC_HEBDO_PREC 
from Cte_Reclamations_all
WHERE DTE_CREA_RECLA between @FirstDOPW and DATEADD(DD,6, @FirstDOPW) and DTE_CREA_RECLA=@Date_obs and cast(DTE_DER_MOD as date)=@Date_obs 
GROUP BY DTE_CREA_RECLA, 																																									   
        [WeekOfYear],																																										   
		TYP_REQ, 																																											   
		CAN_INT_ORI		
) 
--select * from Cte_Reclamations_hebdo_prec 

,Cte_Reclamations_Cumul_hebdo_prec as (
select [WeekOfYear], 
		ISNULL(SUM(NB_RECLA_HEBDO_PREC),0)                as NB_RECLA_HEBDO_PREC, 
		ISNULL(SUM(NB_RECLA_ENROL_HEBDO_PREC),0)          as NB_RECLA_ENROL_HEBDO_PREC, 
		ISNULL(SUM(NB_RECLA_MOB_SMS_HEBDO_PREC),0)        as NB_RECLA_MOB_SMS_HEBDO_PREC, 
		ISNULL(SUM(NB_RECLA_NET_CLT_HEBDO_PREC),0)        as NB_RECLA_NET_CLT_HEBDO_PREC, 
		ISNULL(SUM(NB_RECLA_COURRIER_HEBDO_PREC),0)       as NB_RECLA_COURRIER_HEBDO_PREC, 
		ISNULL(SUM(NB_RECLA_APP_ENTR_HEBDO_PREC),0)       as NB_RECLA_APP_ENTR_HEBDO_PREC, 
		ISNULL(SUM(NB_RECLA_APP_SORT_HEBDO_PREC),0)       as NB_RECLA_APP_SORT_HEBDO_PREC, 
		ISNULL(SUM(NB_RECLA_CHAT_HEBDO_PREC),0)           as NB_RECLA_CHAT_HEBDO_PREC, 
		ISNULL(SUM(NB_RECLA_RES_SOC_HEBDO_PREC),0)        as NB_RECLA_RES_SOC_HEBDO_PREC, 
		ISNULL(SUM(NB_RECLA_APP_ENTR_TELCO_HEBDO_PREC),0) as NB_RECLA_APP_ENTR_TELCO_HEBDO_PREC, 
		ISNULL(SUM(NB_RECLA_APP_ENTR_Btq_HEBDO_PREC),0)   as NB_RECLA_APP_ENTR_Btq_HEBDO_PREC, 
		ISNULL(SUM(NB_RECLA_MAIL_HEBDO_PREC),0)           as NB_RECLA_MAIL_HEBDO_PREC, 
		ISNULL(SUM(NB_RECLA_FOR_DEM_HEBDO_PREC),0)        as NB_RECLA_FOR_DEM_HEBDO_PREC, 
		ISNULL(SUM(NB_RECLA_SFMC_HEBDO_PREC),0)           as NB_RECLA_SFMC_HEBDO_PREC
from Cte_Reclamations_hebdo_prec
GROUP BY [WeekOfYear]
) 
--select * from Cte_Reclamations_Cumul_hebdo_prec

/******************_*_Requête_*_******************/
select distinct x.jour_alim,
       ISNULL(c.NB_OPP_CREEES,         0) as NB_OPP_CREEES,      
	   ISNULL(c.NB_CREEES_OB,		   0) as NB_CREEES_OB,		
	   ISNULL(c.NB_CREEES_OB_Dig,	   0) as NB_CREEES_OB_Dig,	
	   ISNULL(c.NB_CREEES_OB_CRC,	   0) as NB_CREEES_OB_CRC,	
	   ISNULL(c.NB_CREEES_OF,		   0) as NB_CREEES_OF,		
	   ISNULL(c.NB_CREEES_OF_Dig,	   0) as NB_CREEES_OF_Dig,	
	   ISNULL(c.NB_CREEES_OF_Btq,	   0) as NB_CREEES_OF_Btq,	
	   ISNULL(c.NB_CREEES_OF_SS,	   0) as NB_CREEES_OF_SS,	
	   ISNULL(c.NB_CREEES_OF_SS_Dig,   0) as NB_CREEES_OF_SS_Dig,
	   ISNULL(c.NB_CREEES_OF_SS_Btq,   0) as NB_CREEES_OF_SS_Btq,
	   ISNULL(c.NB_CREEES_OF_AC,	   0) as NB_CREEES_OF_AC,	
	   ISNULL(c.NB_CREEES_OF_AC_Dig,   0) as NB_CREEES_OF_AC_Dig,
	   ISNULL(c.NB_CREEES_OF_AC_Btq,   0) as NB_CREEES_OF_AC_Btq,

	   case when ROW_NUMBER() OVER(PARTITION BY ch.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(ch.NB_OPP_CREEES_HEBDO,0) else 0 end       as NB_OPP_CREEES_HEBDO,																											      
	   case when ROW_NUMBER() OVER(PARTITION BY ch.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(ch.NB_CREEES_OB_HEBDO,0) else 0 end        as NB_CREEES_OB_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY ch.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(ch.NB_CREEES_OB_Dig_HEBDO,0) else 0 end    as NB_CREEES_OB_Dig_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY ch.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(ch.NB_CREEES_OB_CRC_HEBDO,0) else 0 end    as NB_CREEES_OB_CRC_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY ch.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(ch.NB_CREEES_OF_HEBDO,0) else 0 end        as NB_CREEES_OF_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY ch.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(ch.NB_CREEES_OF_Dig_HEBDO,0) else 0 end    as NB_CREEES_OF_Dig_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY ch.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(ch.NB_CREEES_OF_Btq_HEBDO,0) else 0 end    as NB_CREEES_OF_Btq_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY ch.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(ch.NB_CREEES_OF_SS_HEBDO,0) else 0 end     as NB_CREEES_OF_SS_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY ch.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(ch.NB_CREEES_OF_SS_Dig_HEBDO,0) else 0 end as NB_CREEES_OF_SS_Dig_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY ch.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(ch.NB_CREEES_OF_SS_Btq_HEBDO,0) else 0 end as NB_CREEES_OF_SS_Btq_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY ch.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(ch.NB_CREEES_OF_AC_HEBDO,0) else 0 end     as NB_CREEES_OF_AC_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY ch.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(ch.NB_CREEES_OF_AC_Dig_HEBDO,0) else 0 end as NB_CREEES_OF_AC_Dig_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY ch.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(ch.NB_CREEES_OF_AC_Btq_HEBDO,0) else 0 end as NB_CREEES_OF_AC_Btq_HEBDO,
	   
	   case when ROW_NUMBER() OVER(PARTITION BY chp.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(chp.NB_OPP_CREEES_HEBDO_PREC,0) else 0 end       as NB_OPP_CREEES_HEBDO_PREC,																											      
	   case when ROW_NUMBER() OVER(PARTITION BY chp.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(chp.NB_CREEES_OB_HEBDO_PREC,0) else 0 end        as NB_CREEES_OB_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY chp.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(chp.NB_CREEES_OB_Dig_HEBDO_PREC,0) else 0 end    as NB_CREEES_OB_Dig_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY chp.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(chp.NB_CREEES_OB_CRC_HEBDO_PREC,0) else 0 end    as NB_CREEES_OB_CRC_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY chp.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(chp.NB_CREEES_OF_HEBDO_PREC,0) else 0 end        as NB_CREEES_OF_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY chp.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(chp.NB_CREEES_OF_Dig_HEBDO_PREC,0) else 0 end    as NB_CREEES_OF_Dig_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY chp.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(chp.NB_CREEES_OF_Btq_HEBDO_PREC,0) else 0 end    as NB_CREEES_OF_Btq_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY chp.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(chp.NB_CREEES_OF_SS_HEBDO_PREC,0) else 0 end     as NB_CREEES_OF_SS_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY chp.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(chp.NB_CREEES_OF_SS_Dig_HEBDO_PREC,0) else 0 end as NB_CREEES_OF_SS_Dig_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY chp.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(chp.NB_CREEES_OF_SS_Btq_HEBDO_PREC,0) else 0 end as NB_CREEES_OF_SS_Btq_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY chp.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(chp.NB_CREEES_OF_AC_HEBDO_PREC,0) else 0 end     as NB_CREEES_OF_AC_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY chp.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(chp.NB_CREEES_OF_AC_Dig_HEBDO_PREC,0) else 0 end as NB_CREEES_OF_AC_Dig_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY chp.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(chp.NB_CREEES_OF_AC_Btq_HEBDO_PREC,0) else 0 end as NB_CREEES_OF_AC_Btq_HEBDO_PREC,
	   
	  

	   ISNULL(v.NB_OPP_ENCRS,         0) as NB_OPP_ENCRS,
	   ISNULL(v.NB_ENCRS_OB_DETEC,	  0) as	NB_ENCRS_OB_DETEC,        
	   ISNULL(v.NB_ENCRS_OB_DECOUV,	  0) as	NB_ENCRS_OB_DECOUV,	 
	   ISNULL(v.NB_ENCRS_OB_ELAB,	  0) as	NB_ENCRS_OB_ELAB,	 
	   ISNULL(v.NB_ENCRS_OB_AFF_SIGN, 0) as	NB_ENCRS_OB_AFF_SIGN,
	   ISNULL(v.NB_ENCRS_OB_VERIF,	  0) as	NB_ENCRS_OB_VERIF,	 
	   ISNULL(v.NB_ENCRS_OB_DOSS_INC, 0) as	NB_ENCRS_OB_DOSS_INC,
	   ISNULL(v.NB_ENCRS_OB_DOSS_VAL, 0) as	NB_ENCRS_OB_DOSS_VAL,
	   ISNULL(v.NB_ENCRS_OF_DETEC,	  0) as	NB_ENCRS_OF_DETEC,	 
	   ISNULL(v.NB_ENCRS_OF_DECOUV,	  0) as	NB_ENCRS_OF_DECOUV,	 
	   ISNULL(v.NB_ENCRS_OF_ELAB,	  0) as	NB_ENCRS_OF_ELAB,	 
	   ISNULL(v.NB_ENCRS_OF_AFF_SIGN, 0) as	NB_ENCRS_OF_AFF_SIGN,
	   ISNULL(v.NB_ENCRS_OF_VERIF,	  0) as	NB_ENCRS_OF_VERIF,	 
	   ISNULL(v.NB_ENCRS_OF_DOSS_INC, 0) as	NB_ENCRS_OF_DOSS_INC,
	   ISNULL(v.NB_ENCRS_OF_DOSS_VAL, 0) as	NB_ENCRS_OF_DOSS_VAL,
	   
	   case when ROW_NUMBER() OVER(PARTITION BY vh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vh.NB_OPP_ENCRS_HEBDO,0) else 0 end         as NB_OPP_ENCRS_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY vh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vh.NB_ENCRS_OB_DETEC_HEBDO,0) else 0 end    as NB_ENCRS_OB_DETEC_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY vh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vh.NB_ENCRS_OB_DECOUV_HEBDO,0) else 0 end   as NB_ENCRS_OB_DECOUV_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY vh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vh.NB_ENCRS_OB_ELAB_HEBDO,0) else 0 end     as NB_ENCRS_OB_ELAB_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY vh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vh.NB_ENCRS_OB_AFF_SIGN_HEBDO,0) else 0 end as NB_ENCRS_OB_AFF_SIGN_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY vh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vh.NB_ENCRS_OB_VERIF_HEBDO,0) else 0 end    as NB_ENCRS_OB_VERIF_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY vh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vh.NB_ENCRS_OB_DOSS_INC_HEBDO,0) else 0 end as NB_ENCRS_OB_DOSS_INC_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY vh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vh.NB_ENCRS_OB_DOSS_VAL_HEBDO,0) else 0 end as NB_ENCRS_OB_DOSS_VAL_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY vh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vh.NB_ENCRS_OF_DETEC_HEBDO,0) else 0 end    as NB_ENCRS_OF_DETEC_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY vh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vh.NB_ENCRS_OF_DECOUV_HEBDO,0) else 0 end   as NB_ENCRS_OF_DECOUV_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY vh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vh.NB_ENCRS_OF_ELAB_HEBDO,0) else 0 end     as NB_ENCRS_OF_ELAB_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY vh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vh.NB_ENCRS_OF_AFF_SIGN_HEBDO,0) else 0 end as NB_ENCRS_OF_AFF_SIGN_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY vh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vh.NB_ENCRS_OF_VERIF_HEBDO,0) else 0 end    as NB_ENCRS_OF_VERIF_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY vh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vh.NB_ENCRS_OF_DOSS_INC_HEBDO,0) else 0 end as NB_ENCRS_OF_DOSS_INC_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY vh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vh.NB_ENCRS_OF_DOSS_VAL_HEBDO,0) else 0 end as NB_ENCRS_OF_DOSS_VAL_HEBDO,

	   case when ROW_NUMBER() OVER(PARTITION BY vhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vhp.NB_OPP_ENCRS_HEBDO_PREC,0) else 0 end         as NB_OPP_ENCRS_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY vhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vhp.NB_ENCRS_OB_DETEC_HEBDO_PREC,0) else 0 end    as NB_ENCRS_OB_DETEC_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY vhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vhp.NB_ENCRS_OB_DECOUV_HEBDO_PREC,0) else 0 end   as NB_ENCRS_OB_DECOUV_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY vhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vhp.NB_ENCRS_OB_ELAB_HEBDO_PREC,0) else 0 end     as NB_ENCRS_OB_ELAB_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY vhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vhp.NB_ENCRS_OB_AFF_SIGN_HEBDO_PREC,0) else 0 end as NB_ENCRS_OB_AFF_SIGN_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY vhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vhp.NB_ENCRS_OB_VERIF_HEBDO_PREC,0) else 0 end    as NB_ENCRS_OB_VERIF_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY vhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vhp.NB_ENCRS_OB_DOSS_INC_HEBDO_PREC,0) else 0 end as NB_ENCRS_OB_DOSS_INC_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY vhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vhp.NB_ENCRS_OB_DOSS_VAL_HEBDO_PREC,0) else 0 end as NB_ENCRS_OB_DOSS_VAL_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY vhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vhp.NB_ENCRS_OF_DETEC_HEBDO_PREC,0) else 0 end    as NB_ENCRS_OF_DETEC_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY vhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vhp.NB_ENCRS_OF_DECOUV_HEBDO_PREC,0) else 0 end   as NB_ENCRS_OF_DECOUV_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY vhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vhp.NB_ENCRS_OF_ELAB_HEBDO_PREC,0) else 0 end     as NB_ENCRS_OF_ELAB_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY vhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vhp.NB_ENCRS_OF_AFF_SIGN_HEBDO_PREC,0) else 0 end as NB_ENCRS_OF_AFF_SIGN_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY vhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vhp.NB_ENCRS_OF_VERIF_HEBDO_PREC,0) else 0 end    as NB_ENCRS_OF_VERIF_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY vhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vhp.NB_ENCRS_OF_DOSS_INC_HEBDO_PREC,0) else 0 end as NB_ENCRS_OF_DOSS_INC_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY vhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vhp.NB_ENCRS_OF_DOSS_VAL_HEBDO_PREC,0) else 0 end as NB_ENCRS_OF_DOSS_VAL_HEBDO_PREC,
	  
	   
	   ISNULL(g.NB_OPP_GAGNEES,         0) as  NB_OPP_GAGNEES,       
	   ISNULL(g.NB_GAGNEES_OB,			0) as  NB_GAGNEES_OB ,		
	   ISNULL(g.NB_GAGNEES_OB_Dig,		0) as  NB_GAGNEES_OB_Dig,	
	   ISNULL(g.NB_GAGNEES_OB_CRC,		0) as  NB_GAGNEES_OB_CRC,	
	   ISNULL(g.NB_GAGNEES_OF,			0) as  NB_GAGNEES_OF,		
	   ISNULL(g.NB_GAGNEES_OF_Dig,		0) as  NB_GAGNEES_OF_Dig,	
	   ISNULL(g.NB_GAGNEES_OF_Btq,		0) as  NB_GAGNEES_OF_Btq,	
	   ISNULL(g.NB_GAGNEES_OF_SS_Dig,	0) as  NB_GAGNEES_OF_SS_Dig,
	   ISNULL(g.NB_GAGNEES_OF_SS_Btq, 	0) as  NB_GAGNEES_OF_SS_Btq, 
	   ISNULL(g.NB_GAGNEES_OF_AC_Dig,	0) as  NB_GAGNEES_OF_AC_Dig,
	   ISNULL(g.NB_GAGNEES_OF_AC_Btq ,	0) as  NB_GAGNEES_OF_AC_Btq ,
	   ISNULL(g.NB_GAGNEES_GP,			0) as  NB_GAGNEES_GP,		
	   ISNULL(g.NB_GAGNEES_GP_AC,		0) as  NB_GAGNEES_GP_AC,	
	   ISNULL(g.NB_GAGNEES_GP_SS,		0) as  NB_GAGNEES_GP_SS,	
	   ISNULL(dOB.DELAI_GAGNEES_OB,		0) as  DELAI_GAGNEES_OB,	
	   ISNULL(dOB.DELAI_GAGNEES_OB_Dig,	0) as  DELAI_GAGNEES_OB_Dig,
	   ISNULL(dOB.DELAI_GAGNEES_OB_CRC,	0) as  DELAI_GAGNEES_OB_CRC,
	   ISNULL(dOF.DELAI_GAGNEES_OF,		0) as  DELAI_GAGNEES_OF,	
	   ISNULL(dOF.DELAI_GAGNEES_OF_Dig,	0) as  DELAI_GAGNEES_OF_Dig,
	   ISNULL(dOF.DELAI_GAGNEES_OF_Btq, 0) as  DELAI_GAGNEES_OF_Btq, 
	   ISNULL(It.NB_ITERATION_OB,		0) as  NB_ITERATION_OB,	
	   ISNULL(It.NB_ITERATION_OB_0,		0) as  NB_ITERATION_OB_0,	
	   ISNULL(It.NB_ITERATION_OB_1_2,	0) as  NB_ITERATION_OB_1_2,
	   ISNULL(It.NB_ITERATION_OB_plus_2,	0) as  NB_ITERATION_OB_plus_2,
	   ISNULL(It.NB_ITERATION_OF,		0) as  NB_ITERATION_OF,	
	   ISNULL(It.NB_ITERATION_OF_0,		0) as  NB_ITERATION_OF_0,	
	   ISNULL(It.NB_ITERATION_OF_1_2,	0) as  NB_ITERATION_OF_1_2,
	   ISNULL(It.NB_ITERATION_OF_plus_2,	0) as  NB_ITERATION_OF_plus_2,
	   case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(gh.NB_OPP_GAGNEES_HEBDO,0) else 0 end           as NB_OPP_GAGNEES_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(gh.NB_GAGNEES_OB_HEBDO ,0) else 0 end           as NB_GAGNEES_OB_HEBDO ,
	   case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(gh.NB_GAGNEES_OB_Dig_HEBDO,0) else 0 end        as NB_GAGNEES_OB_Dig_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(gh.NB_GAGNEES_OB_CRC_HEBDO,0) else 0 end        as NB_GAGNEES_OB_CRC_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(gh.NB_GAGNEES_OF_HEBDO,0) else 0 end            as NB_GAGNEES_OF_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(gh.NB_GAGNEES_OF_Dig_HEBDO,0) else 0 end        as NB_GAGNEES_OF_Dig_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(gh.NB_GAGNEES_OF_Btq_HEBDO,0) else 0 end        as NB_GAGNEES_OF_Btq_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(gh.NB_GAGNEES_OF_SS_Dig_HEBDO,0) else 0 end     as NB_GAGNEES_OF_SS_Dig_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(gh.NB_GAGNEES_OF_SS_Btq_HEBDO,0) else 0 end     as NB_GAGNEES_OF_SS_Btq_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(gh.NB_GAGNEES_OF_AC_Dig_HEBDO,0) else 0 end     as NB_GAGNEES_OF_AC_Dig_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(gh.NB_GAGNEES_OF_AC_Btq_HEBDO,0) else 0 end     as NB_GAGNEES_OF_AC_Btq_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(gh.NB_GAGNEES_GP_HEBDO,0) else 0 end            as NB_GAGNEES_GP_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(gh.NB_GAGNEES_GP_AC_HEBDO,0) else 0 end         as NB_GAGNEES_GP_AC_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(gh.NB_GAGNEES_GP_SS_HEBDO, 0) else 0 end        as NB_GAGNEES_GP_SS_HEBDO, 
	   case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(dOBh.DELAI_GAGNEES_OB_HEBDO, 0) else 0 end      as DELAI_GAGNEES_OB_HEBDO, 
	   case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(dOBh.DELAI_GAGNEES_OB_Dig_HEBDO, 0) else 0 end  as DELAI_GAGNEES_OB_Dig_HEBDO, 
	   case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(dOBh.DELAI_GAGNEES_OB_CRC_HEBDO, 0) else 0 end  as DELAI_GAGNEES_OB_CRC_HEBDO, 
	   case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(dOFh.DELAI_GAGNEES_OF_HEBDO, 0) else 0 end      as DELAI_GAGNEES_OF_HEBDO, 
	   case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(dOFh.DELAI_GAGNEES_OF_Dig_HEBDO, 0) else 0 end  as DELAI_GAGNEES_OF_Dig_HEBDO, 
	   case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(dOFh.DELAI_GAGNEES_OF_Btq_HEBDO, 0) else 0 end  as DELAI_GAGNEES_OF_Btq_HEBDO, 
	   case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(Ith.NB_ITERATION_OB_HEBDO, 0) else 0 end        as NB_ITERATION_OB_HEBDO, 
	   case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(Ith.NB_ITERATION_OB_0_HEBDO, 0) else 0 end      as NB_ITERATION_OB_0_HEBDO, 
	   case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(Ith.NB_ITERATION_OB_1_2_HEBDO, 0) else 0 end    as NB_ITERATION_OB_1_2_HEBDO, 
	   case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(Ith.NB_ITERATION_OB_plus_2_HEBDO,0) else 0 end  as NB_ITERATION_OB_plus_2_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(Ith.NB_ITERATION_OF_HEBDO, 0) else 0 end        as NB_ITERATION_OF_HEBDO, 
	   case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(Ith.NB_ITERATION_OF_0_HEBDO, 0) else 0 end      as NB_ITERATION_OF_0_HEBDO, 
	   case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(Ith.NB_ITERATION_OF_1_2_HEBDO, 0) else 0 end    as NB_ITERATION_OF_1_2_HEBDO, 
	   case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(Ith.NB_ITERATION_OF_plus_2_HEBDO,0) else 0 end  as NB_ITERATION_OF_plus_2_HEBDO,

	   
	   case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ghp.NB_OPP_GAGNEES_HEBDO_PREC,0) else 0 end             as NB_OPP_GAGNEES_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ghp.NB_GAGNEES_OB_HEBDO_PREC ,0) else 0 end             as NB_GAGNEES_OB_HEBDO_PREC ,
	   case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ghp.NB_GAGNEES_OB_Dig_HEBDO_PREC,0) else 0 end          as NB_GAGNEES_OB_Dig_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ghp.NB_GAGNEES_OB_CRC_HEBDO_PREC,0) else 0 end          as NB_GAGNEES_OB_CRC_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ghp.NB_GAGNEES_OF_HEBDO_PREC,0) else 0 end              as NB_GAGNEES_OF_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ghp.NB_GAGNEES_OF_Dig_HEBDO_PREC,0) else 0 end          as NB_GAGNEES_OF_Dig_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ghp.NB_GAGNEES_OF_Btq_HEBDO_PREC,0) else 0 end          as NB_GAGNEES_OF_Btq_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ghp.NB_GAGNEES_OF_SS_Dig_HEBDO_PREC,0) else 0 end       as NB_GAGNEES_OF_SS_Dig_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ghp.NB_GAGNEES_OF_SS_Btq_HEBDO_PREC,0) else 0 end       as NB_GAGNEES_OF_SS_Btq_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ghp.NB_GAGNEES_OF_AC_Dig_HEBDO_PREC,0) else 0 end       as NB_GAGNEES_OF_AC_Dig_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ghp.NB_GAGNEES_OF_AC_Btq_HEBDO_PREC,0) else 0 end       as NB_GAGNEES_OF_AC_Btq_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ghp.NB_GAGNEES_GP_HEBDO_PREC,0) else 0 end              as NB_GAGNEES_GP_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ghp.NB_GAGNEES_GP_AC_HEBDO_PREC,0) else 0 end           as NB_GAGNEES_GP_AC_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ghp.NB_GAGNEES_GP_SS_HEBDO_PREC, 0) else 0 end          as NB_GAGNEES_GP_SS_HEBDO_PREC, 
	   case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(dOBhp.DELAI_GAGNEES_OB_HEBDO_PREC, 0) else 0 end        as DELAI_GAGNEES_OB_HEBDO_PREC, 
	   case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(dOBhp.DELAI_GAGNEES_OB_Dig_HEBDO_PREC, 0) else 0 end    as DELAI_GAGNEES_OB_Dig_HEBDO_PREC, 
	   case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(dOBhp.DELAI_GAGNEES_OB_CRC_HEBDO_PREC, 0) else 0 end    as DELAI_GAGNEES_OB_CRC_HEBDO_PREC, 
	   case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(dOFhp.DELAI_GAGNEES_OF_HEBDO_PREC, 0) else 0 end        as DELAI_GAGNEES_OF_HEBDO_PREC, 
	   case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(dOFhp.DELAI_GAGNEES_OF_Dig_HEBDO_PREC, 0) else 0 end    as DELAI_GAGNEES_OF_Dig_HEBDO_PREC, 
	   case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(dOFhp.DELAI_GAGNEES_OF_Btq_HEBDO_PREC, 0) else 0 end    as DELAI_GAGNEES_OF_Btq_HEBDO_PREC, 
	   case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(Ithp.NB_ITERATION_OB_HEBDO_PREC, 0) else 0 end          as NB_ITERATION_OB_HEBDO_PREC, 
	   case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(Ithp.NB_ITERATION_OB_0_HEBDO_PREC, 0) else 0 end        as NB_ITERATION_OB_0_HEBDO_PREC, 
	   case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(Ithp.NB_ITERATION_OB_1_2_HEBDO_PREC, 0) else 0 end      as NB_ITERATION_OB_1_2_HEBDO_PREC, 
	   case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(Ithp.NB_ITERATION_OB_plus_2_HEBDO_PREC,0) else 0 end    as NB_ITERATION_OB_plus_2_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(Ithp.NB_ITERATION_OF_HEBDO_PREC, 0) else 0 end          as NB_ITERATION_OF_HEBDO_PREC, 
	   case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(Ithp.NB_ITERATION_OF_0_HEBDO_PREC, 0) else 0 end        as NB_ITERATION_OF_0_HEBDO_PREC, 
	   case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(Ithp.NB_ITERATION_OF_1_2_HEBDO_PREC, 0) else 0 end      as NB_ITERATION_OF_1_2_HEBDO_PREC, 
	   case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(Ithp.NB_ITERATION_OF_plus_2_HEBDO_PREC,0) else 0 end    as NB_ITERATION_OF_plus_2_HEBDO_PREC,
	   

	   ISNULL(p.NB_OPP_PERDUES,          0) as NB_OPP_PERDUES,         
	   ISNULL(p.NB_PERDUES_OB_INC_TECH,	 0) as NB_PERDUES_OB_INC_TECH,
	   ISNULL(p.NB_PERDUES_OB_SANS_SUITE,0) as NB_PERDUES_OB_SANS_SUITE,
	   ISNULL(p.NB_PERDUES_OB_AFF_REF,	 0) as NB_PERDUES_OB_AFF_REF,
	   ISNULL(p.NB_PERDUES_OB_RETRACT,	 0) as NB_PERDUES_OB_RETRACT,
	   ISNULL(p.NB_PERDUES_OF_INC_TECH,	 0) as NB_PERDUES_OF_INC_TECH,
	   ISNULL(p.NB_PERDUES_OF_SANS_SUITE,0) as NB_PERDUES_OF_SANS_SUITE,
	   ISNULL(p.NB_PERDUES_OF_AFF_REF,	 0) as NB_PERDUES_OF_AFF_REF,
	   ISNULL(p.NB_PERDUES_OF_RETRACT,	 0) as NB_PERDUES_OF_RETRACT,
	   case when ROW_NUMBER() OVER(PARTITION BY ph.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ph.NB_OPP_PERDUES_HEBDO,0) else 0 end             as NB_OPP_PERDUES_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY ph.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ph.NB_PERDUES_OB_INC_TECH_HEBDO,0) else 0 end     as NB_PERDUES_OB_INC_TECH_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY ph.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ph.NB_PERDUES_OB_SANS_SUITE_HEBDO,0) else 0 end   as NB_PERDUES_OB_SANS_SUITE_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY ph.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ph.NB_PERDUES_OB_AFF_REF_HEBDO,0) else 0 end      as NB_PERDUES_OB_AFF_REF_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY ph.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ph.NB_PERDUES_OB_RETRACT_HEBDO,0) else 0 end      as NB_PERDUES_OB_RETRACT_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY ph.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ph.NB_PERDUES_OF_INC_TECH_HEBDO,0) else 0 end     as NB_PERDUES_OF_INC_TECH_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY ph.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ph.NB_PERDUES_OF_SANS_SUITE_HEBDO,0) else 0 end   as NB_PERDUES_OF_SANS_SUITE_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY ph.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ph.NB_PERDUES_OF_AFF_REF_HEBDO,0) else 0 end      as NB_PERDUES_OF_AFF_REF_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY ph.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ph.NB_PERDUES_OF_RETRACT_HEBDO, 0) else 0 end     as NB_PERDUES_OF_RETRACT_HEBDO, 
	   case when ROW_NUMBER() OVER(PARTITION BY php.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(php.NB_OPP_PERDUES_HEBDO_PREC,0) else 0 end             as NB_OPP_PERDUES_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY php.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(php.NB_PERDUES_OB_INC_TECH_HEBDO_PREC,0) else 0 end     as NB_PERDUES_OB_INC_TECH_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY php.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(php.NB_PERDUES_OB_SANS_SUITE_HEBDO_PREC,0) else 0 end   as NB_PERDUES_OB_SANS_SUITE_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY php.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(php.NB_PERDUES_OB_AFF_REF_HEBDO_PREC,0) else 0 end      as NB_PERDUES_OB_AFF_REF_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY php.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(php.NB_PERDUES_OB_RETRACT_HEBDO_PREC,0) else 0 end      as NB_PERDUES_OB_RETRACT_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY php.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(php.NB_PERDUES_OF_INC_TECH_HEBDO_PREC,0) else 0 end     as NB_PERDUES_OF_INC_TECH_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY php.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(php.NB_PERDUES_OF_SANS_SUITE_HEBDO_PREC,0) else 0 end   as NB_PERDUES_OF_SANS_SUITE_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY php.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(php.NB_PERDUES_OF_AFF_REF_HEBDO_PREC,0) else 0 end      as NB_PERDUES_OF_AFF_REF_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY php.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(php.NB_PERDUES_OF_RETRACT_HEBDO_PREC, 0) else 0 end     as NB_PERDUES_OF_RETRACT_HEBDO_PREC, 

	   ISNULL(r.NB_RECLA,                 0) as NB_RECLA, 
	   ISNULL(r.NB_RECLA_ENROL,			  0) as NB_RECLA_ENROL,
	   ISNULL(r.NB_RECLA_MOB_SMS,		  0) as NB_RECLA_MOB_SMS,
	   ISNULL(r.NB_RECLA_NET_CLT,		  0) as NB_RECLA_NET_CLT,
	   ISNULL(r.NB_RECLA_COURRIER,		  0) as NB_RECLA_COURRIER,
	   ISNULL(r.NB_RECLA_APP_ENTR,		  0) as NB_RECLA_APP_ENTR,
	   ISNULL(r.NB_RECLA_APP_SORT,		  0) as NB_RECLA_APP_SORT,
	   ISNULL(r.NB_RECLA_CHAT,			  0) as NB_RECLA_CHAT,
	   ISNULL(r.NB_RECLA_RES_SOC,		  0) as NB_RECLA_RES_SOC,
	   ISNULL(r.NB_RECLA_APP_ENTR_TELCO,  0) as NB_RECLA_APP_ENTR_TELCO,
	   ISNULL(r.NB_RECLA_APP_ENTR_Btq,	  0) as NB_RECLA_APP_ENTR_Btq,
	   ISNULL(r.NB_RECLA_MAIL,			  0) as NB_RECLA_MAIL,
	   ISNULL(r.NB_RECLA_FOR_DEM,		  0) as NB_RECLA_FOR_DEM,
	   ISNULL(r.NB_RECLA_SFMC,			  0) as NB_RECLA_SFMC,

	   case when ROW_NUMBER() OVER(PARTITION BY rh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(rh.NB_RECLA_HEBDO,0) else 0 end                   as NB_RECLA_HEBDO, 
	   case when ROW_NUMBER() OVER(PARTITION BY rh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(rh.NB_RECLA_ENROL_HEBDO,0) else 0 end             as NB_RECLA_ENROL_HEBDO, 
	   case when ROW_NUMBER() OVER(PARTITION BY rh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(rh.NB_RECLA_MOB_SMS_HEBDO,0) else 0 end           as NB_RECLA_MOB_SMS_HEBDO, 
	   case when ROW_NUMBER() OVER(PARTITION BY rh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(rh.NB_RECLA_NET_CLT_HEBDO,0) else 0 end           as NB_RECLA_NET_CLT_HEBDO, 
	   case when ROW_NUMBER() OVER(PARTITION BY rh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(rh.NB_RECLA_COURRIER_HEBDO,0) else 0 end          as NB_RECLA_COURRIER_HEBDO, 
	   case when ROW_NUMBER() OVER(PARTITION BY rh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(rh.NB_RECLA_APP_ENTR_HEBDO,0) else 0 end          as NB_RECLA_APP_ENTR_HEBDO, 
	   case when ROW_NUMBER() OVER(PARTITION BY rh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(rh.NB_RECLA_APP_SORT_HEBDO,0) else 0 end          as NB_RECLA_APP_SORT_HEBDO, 
	   case when ROW_NUMBER() OVER(PARTITION BY rh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(rh.NB_RECLA_CHAT_HEBDO,0) else 0 end              as NB_RECLA_CHAT_HEBDO, 
	   case when ROW_NUMBER() OVER(PARTITION BY rh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(rh.NB_RECLA_RES_SOC_HEBDO,0) else 0 end           as NB_RECLA_RES_SOC_HEBDO, 
	   case when ROW_NUMBER() OVER(PARTITION BY rh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(rh.NB_RECLA_APP_ENTR_TELCO_HEBDO,0) else 0 end    as NB_RECLA_APP_ENTR_TELCO_HEBDO,
	   case when ROW_NUMBER() OVER(PARTITION BY rh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(rh.NB_RECLA_APP_ENTR_Btq_HEBDO,0) else 0 end      as NB_RECLA_APP_ENTR_Btq_HEBDO, 
	   case when ROW_NUMBER() OVER(PARTITION BY rh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(rh.NB_RECLA_MAIL_HEBDO,0) else 0 end              as NB_RECLA_MAIL_HEBDO, 
	   case when ROW_NUMBER() OVER(PARTITION BY rh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(rh.NB_RECLA_FOR_DEM_HEBDO,0) else 0 end           as NB_RECLA_FOR_DEM_HEBDO, 
	   case when ROW_NUMBER() OVER(PARTITION BY rh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(rh.NB_RECLA_SFMC_HEBDO,0) else 0 end              as NB_RECLA_SFMC_HEBDO,

	  

	   case when ROW_NUMBER() OVER(PARTITION BY rhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(rhp.NB_RECLA_HEBDO_PREC,0) else 0 end                   as NB_RECLA_HEBDO_PREC, 
	   case when ROW_NUMBER() OVER(PARTITION BY rhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(rhp.NB_RECLA_ENROL_HEBDO_PREC,0) else 0 end             as NB_RECLA_ENROL_HEBDO_PREC, 
	   case when ROW_NUMBER() OVER(PARTITION BY rhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(rhp.NB_RECLA_MOB_SMS_HEBDO_PREC,0) else 0 end           as NB_RECLA_MOB_SMS_HEBDO_PREC, 
	   case when ROW_NUMBER() OVER(PARTITION BY rhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(rhp.NB_RECLA_NET_CLT_HEBDO_PREC,0) else 0 end           as NB_RECLA_NET_CLT_HEBDO_PREC, 
	   case when ROW_NUMBER() OVER(PARTITION BY rhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(rhp.NB_RECLA_COURRIER_HEBDO_PREC,0) else 0 end          as NB_RECLA_COURRIER_HEBDO_PREC, 
	   case when ROW_NUMBER() OVER(PARTITION BY rhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(rhp.NB_RECLA_APP_ENTR_HEBDO_PREC,0) else 0 end          as NB_RECLA_APP_ENTR_HEBDO_PREC, 
	   case when ROW_NUMBER() OVER(PARTITION BY rhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(rhp.NB_RECLA_APP_SORT_HEBDO_PREC,0) else 0 end          as NB_RECLA_APP_SORT_HEBDO_PREC, 
	   case when ROW_NUMBER() OVER(PARTITION BY rhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(rhp.NB_RECLA_CHAT_HEBDO_PREC,0) else 0 end              as NB_RECLA_CHAT_HEBDO_PREC, 
	   case when ROW_NUMBER() OVER(PARTITION BY rhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(rhp.NB_RECLA_RES_SOC_HEBDO_PREC,0) else 0 end           as NB_RECLA_RES_SOC_HEBDO_PREC, 
	   case when ROW_NUMBER() OVER(PARTITION BY rhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(rhp.NB_RECLA_APP_ENTR_TELCO_HEBDO_PREC,0) else 0 end    as NB_RECLA_APP_ENTR_TELCO_HEBDO_PREC,
	   case when ROW_NUMBER() OVER(PARTITION BY rhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(rhp.NB_RECLA_APP_ENTR_Btq_HEBDO_PREC,0) else 0 end      as NB_RECLA_APP_ENTR_Btq_HEBDO_PREC, 
	   case when ROW_NUMBER() OVER(PARTITION BY rhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(rhp.NB_RECLA_MAIL_HEBDO_PREC,0) else 0 end              as NB_RECLA_MAIL_HEBDO_PREC, 
	   case when ROW_NUMBER() OVER(PARTITION BY rhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(rhp.NB_RECLA_FOR_DEM_HEBDO_PREC,0) else 0 end           as NB_RECLA_FOR_DEM_HEBDO_PREC, 
	   case when ROW_NUMBER() OVER(PARTITION BY rhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(rhp.NB_RECLA_SFMC_HEBDO_PREC,0) else 0 end              as NB_RECLA_SFMC_HEBDO_PREC,
	 

	    Case when ISNULL(c.NB_CREEES_OB,0) = 0 
			 then 0 
			 else CAST(g.NB_GAGNEES_OB as float) /  CAST(c.NB_CREEES_OB as float)
		END as TX_OB,
		Case when ISNULL(c.NB_CREEES_OB_Dig,0) = 0 
			 then 0 
			 else CAST(g.NB_GAGNEES_OB_Dig as float) /  CAST(c.NB_CREEES_OB_Dig as float)
		END as TX_OB_Dig,
		Case when ISNULL(c.NB_CREEES_OB_CRC,0) = 0 
			 then 0 
			 else CAST(g.NB_GAGNEES_OB_CRC as float) /  CAST(c.NB_CREEES_OB_CRC as float)
		END as TX_OB_CRC,
		Case when ISNULL(c.NB_CREEES_OF,0) = 0 
			 then 0 
			 else CAST(g.NB_GAGNEES_OF as float) /  CAST(c.NB_CREEES_OF as float)
		END as TX_OF,
		Case when ISNULL(c.NB_CREEES_OF_Dig,0) = 0 
			 then 0 
			 else CAST(g.NB_GAGNEES_OF_Dig as float) /  CAST(c.NB_CREEES_OF_Dig as float)
		END as TX_OF_Dig,
		Case when ISNULL(c.NB_CREEES_OF_Btq,0) = 0 
			 then 0 
			 else CAST(g.NB_GAGNEES_OF_Btq as float) /  CAST(c.NB_CREEES_OF_Btq as float)
		END as TX_OF_Btq,

		Case when ISNULL(ch.NB_CREEES_OB_HEBDO,0) = 0 
			 then 0 
			 else CAST(gh.NB_GAGNEES_OB_HEBDO as float) /  CAST(ch.NB_CREEES_OB_HEBDO as float)
		END as TX_OB_HEBDO,
		Case when ISNULL(ch.NB_CREEES_OB_Dig_HEBDO,0) = 0 
			 then 0 
			 else CAST(gh.NB_GAGNEES_OB_Dig_HEBDO as float) /  CAST(ch.NB_CREEES_OB_Dig_HEBDO as float)
		END as TX_OB_Dig_HEBDO,
		Case when ISNULL(ch.NB_CREEES_OB_CRC_HEBDO,0) = 0 
			 then 0 
			 else CAST(gh.NB_GAGNEES_OB_CRC_HEBDO as float) /  CAST(ch.NB_CREEES_OB_CRC_HEBDO as float)
		END as TX_OB_CRC_HEBDO,
		Case when ISNULL(ch.NB_CREEES_OF_HEBDO,0) = 0 
			 then 0 
			 else CAST(gh.NB_GAGNEES_OF_HEBDO as float) /  CAST(ch.NB_CREEES_OF_HEBDO as float)
		END as TX_OF_HEBDO,
		Case when ISNULL(ch.NB_CREEES_OF_Dig_HEBDO,0) = 0 
			 then 0 
			 else CAST(gh.NB_GAGNEES_OF_Dig_HEBDO as float) /  CAST(ch.NB_CREEES_OF_Dig_HEBDO as float)
		END as TX_OF_Dig_HEBDO,
		Case when ISNULL(ch.NB_CREEES_OF_Btq_HEBDO,0) = 0 
			 then 0 
			 else CAST(gh.NB_GAGNEES_OF_Btq_HEBDO as float) /  CAST(ch.NB_CREEES_OF_Btq_HEBDO as float)
		END as TX_OF_Btq_HEBDO,
		Case when ISNULL(chp.NB_CREEES_OB_HEBDO_PREC,0) = 0 
			 then 0 
			 else CAST(ghp.NB_GAGNEES_OB_HEBDO_PREC as float) /  CAST(chp.NB_CREEES_OB_HEBDO_PREC as float)
		END as TX_OB_HEBDO_PREC,
		Case when ISNULL(chp.NB_CREEES_OB_Dig_HEBDO_PREC,0) = 0 
			 then 0 
			 else CAST(ghp.NB_GAGNEES_OB_Dig_HEBDO_PREC as float) /  CAST(chp.NB_CREEES_OB_Dig_HEBDO_PREC as float)
		END as TX_OB_Dig_HEBDO_PREC,
		Case when ISNULL(chp.NB_CREEES_OB_CRC_HEBDO_PREC,0) = 0 
			 then 0 
			 else CAST(ghp.NB_GAGNEES_OB_CRC_HEBDO_PREC as float) /  CAST(chp.NB_CREEES_OB_CRC_HEBDO_PREC as float)
		END as TX_OB_CRC_HEBDO_PREC,
		Case when ISNULL(chp.NB_CREEES_OF_HEBDO_PREC,0) = 0 
			 then 0 
			 else CAST(ghp.NB_GAGNEES_OF_HEBDO_PREC as float) /  CAST(chp.NB_CREEES_OF_HEBDO_PREC as float)
		END as TX_OF_HEBDO_PREC,
		Case when ISNULL(chp.NB_CREEES_OF_Dig_HEBDO_PREC,0) = 0 
			 then 0 
			 else CAST(ghp.NB_GAGNEES_OF_Dig_HEBDO_PREC as float) /  CAST(chp.NB_CREEES_OF_Dig_HEBDO_PREC as float)
		END as TX_OF_Dig_HEBDO_PREC,
		Case when ISNULL(chp.NB_CREEES_OF_Btq_HEBDO_PREC,0) = 0 
			 then 0 
			 else CAST(ghp.NB_GAGNEES_OF_Btq_HEBDO_PREC as float) /  CAST(chp.NB_CREEES_OF_Btq_HEBDO_PREC as float)
		END as TX_OF_Btq_HEBDO_PREC
from cte_jour_alim_list x 
--Opport CAV créées
left join Cte_Opp_CAV_Creees_Cumul c on x.jour_alim = c.DTE_CREA
left join Cte_Opp_CAV_Creees_Cumul_hebdo ch on c.WeekOfYear_c=ch.WeekOfYear_c 
left join Cte_Opp_CAV_Creees_Cumul_hebdo_prec chp on  c.WeekOfYear_c=chp.WeekOfYear_c+1 
--Opport CAV en cours de vie
left join Cte_Opp_CAV_En_cours_Cumul v on x.jour_alim = v.DTE_ALIM
left join Cte_Opp_CAV_En_cours_Cumul_hebdo vh on v.WeekOfYear=vh.WeekOfYear 
left join Cte_Opp_CAV_En_cours_Cumul_hebdo_prec vhp on v.WeekOfYear=vhp.WeekOfYear +1 

--Opport CAV gagnees Calcul Delai OB 
left join cte_Opp_OB_DELAI_Cumul dOB on  x.jour_alim = dOB.DTE_ALIM
left join cte_Opp_OB_DELAI_Cumul_hebdo dOBh on dOB.WeekOfYear=dOBh.WeekOfYear
left join cte_Opp_OB_DELAI_Cumul_hebdo_prec dOBhp on dOB.WeekOfYear=dOBhp.WeekOfYear+1

--Opport CAV gagnees Calcul Delai OF 
left join cte_Opp_OF_DELAI_Cumul dOF on  x.jour_alim = dOF.DTE_ALIM
left join cte_Opp_OF_DELAI_Cumul_hebdo dOFh on dOF.WeekOfYear=dOFh.WeekOfYear
left join cte_Opp_OF_DELAI_Cumul_hebdo_prec dOFhp on dOF.WeekOfYear=dOFhp.WeekOfYear+1

--Opport CAV gagnees Calcul Iterations
left join cte_Opp_Calcul_Iter_Cumul It on x.jour_alim = It.DTE_ALIM
left join cte_Opp_Calcul_Iter_Cumul_hebdo Ith on It.WeekOfYear=Ith.WeekOfYear
left join cte_Opp_Calcul_Iter_Cumul_hebdo_prec Ithp on It.WeekOfYear=Ithp.WeekOfYear+1

--Opport CAV gagnees
left join Cte_Opp_CAV_Gagnees_Cumul g on x.jour_alim = g.DTE_ALIM
left join Cte_Opp_CAV_Gagnees_Cumul_hebdo gh on g.WeekOfYear=gh.WeekOfYear
left join Cte_Opp_CAV_Gagnees_Cumul_hebdo_prec ghp on g.WeekOfYear=ghp.WeekOfYear+1
--Opport CAV perdues
left join Cte_Opp_CAV_Perdues_Cumul p on x.jour_alim = p.DTE_ALIM
left join Cte_Opp_CAV_Perdues_Cumul_hebdo ph on p.WeekOfYear=ph.WeekOfYear
left join Cte_Opp_CAV_Perdues_Cumul_hebdo_prec php on p.WeekOfYear=php.WeekOfYear+1
--Réclamations
left join Cte_Reclamations_Cumul r on x.jour_alim = r.DTE_CREA_RECLA
left join Cte_Reclamations_Cumul_Hebdo rh on r.WeekOfYear = rh.WeekOfYear
left join Cte_Reclamations_Cumul_Hebdo_Prec rhp on r.WeekOfYear = rhp.WeekOfYear+1
where x.jour_alim BETWEEN DATEADD(dd,-6,@Date_obs)  and @Date_obs
order by x.jour_alim asc;

End;