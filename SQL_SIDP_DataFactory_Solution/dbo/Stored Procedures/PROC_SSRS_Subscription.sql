﻿CREATE PROCEDURE [dbo].[PROC_SSRS_Subscription]
	@ReportName			VARCHAR(50), 
	@SubscriptionName	VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE
		@ColumnNames AS NVARCHAR(MAX) = STUFF((SELECT DISTINCT ',' + QUOTENAME(c.[Key]) FROM dbo.PARAM_SSRS_SUBSCRIPTION c WHERE ReportName = @ReportName AND SubscriptionName = @SubscriptionName FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,'');

	DECLARE
		@Statement			NVARCHAR(MAX) = '
	SELECT
		*
	FROM
		(
			SELECT
				[Key],
				[Value]
			FROM
				dbo.PARAM_SSRS_SUBSCRIPTION
			WHERE
				1 = 1
				AND	ReportName = ''' + @ReportName + '''
				AND SubscriptionName = ''' + @SubscriptionName + '''
		)	AS	KeyValueTable
		PIVOT
		(
			MAX([Value])
			FOR [Key] IN (' + @ColumnNames + ')
		)	AS	PivotTable';
	DECLARE @TempSSRSSubsriptions AS TABLE
	(
		Bcc_Recipient_Address	VARCHAR(MAX),
		Email_Body				VARCHAR(MAX),
		Email_Subject			VARCHAR(MAX),
		Frequency				VARCHAR(MAX),
		Include_Link			VARCHAR(MAX),
		Include_Report			VARCHAR(MAX),
		Recipient_Address		VARCHAR(MAX)
	);
	
	INSERT INTO @TempSSRSSubsriptions EXEC sp_executesql @Statement;
	-- SIDP-488 : For monthly subscriptions, send only when the date is between the 3rd and the 11th worked day of the month.
	UPDATE
		@TempSSRSSubsriptions
	SET
		Recipient_Address = '',
		Bcc_Recipient_Address = ''
	WHERE
		Frequency = 'M'
		AND CAST(GETDATE() AS DATE) NOT IN
		(
			SELECT
				CAST([Date] AS DATE) AS MD
			FROM
				(
					SELECT
						RANK() OVER (PARTITION BY [Month] ORDER BY [Date]) AS RNK,
						[Date]
					FROM
						dbo.REF_TEMPS
					WHERE
						1 = 1
						AND	DATEDIFF(MONTH, GETDATE(), [Date]) = 0
						AND BusinessDay = 1
				) AS D
			WHERE
				1 = 1
				AND	RNK BETWEEN 3 AND 11
		);
	SELECT
		*
	FROM
		@TempSSRSSubsriptions;
END