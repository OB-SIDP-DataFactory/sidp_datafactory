﻿CREATE PROCEDURE [dbo].[ENC_ALIM_DIM_PRODUIT]
AS
BEGIN 

MERGE [ENC_DIM_PRODUIT]               AS target 
using [$(DataHubDatabaseName)].[dbo].[STG_ENC_DIM_PRODUIT] AS source 
ON target.RUB_COMPTABLE=source.RUB_COMPTABLE 
AND target.[LIB_PRODUIT_NIV1]=source.[LIB_PRODUIT_NIV1]
WHEN matched 
AND          Isnull(target.FLAG_ACTIF, 0)=1 THEN 
UPDATE 
SET              target.[LIB_PRODUIT_NIV1]=source.[LIB_PRODUIT_NIV1], 
                 target.[LIB_PRODUIT_NIV2]=source.[LIB_PRODUIT_NIV2], 
                 target.[LIB_PRODUIT_NIV3]=source.[LIB_PRODUIT_NIV3], 
                 target.[LIB_PRODUIT_NIV4]=source.[LIB_PRODUIT_NIV4], 
                 target.UPDATE_DATE=Getdate() 
WHEN NOT matched BY target  THEN 
INSERT 
                 ( 
                                  RUB_COMPTABLE, 
                                  [LIB_PRODUIT_NIV1], 
                                  [LIB_PRODUIT_NIV2], 
                                  [LIB_PRODUIT_NIV3], 
                                  [LIB_PRODUIT_NIV4], 
                                  INSERT_DATE, 
                                  FLAG_ACTIF 
                 ) 
                 VALUES 
                 ( 
                                  source.RUB_COMPTABLE, 
                                  source.[LIB_PRODUIT_NIV1], 
                                  source.[LIB_PRODUIT_NIV2], 
                                  source.[LIB_PRODUIT_NIV3], 
                                  source.[LIB_PRODUIT_NIV4], 
                                  Getdate(), 
                                  1 
                 ) 
				 ;
--WHEN NOT matched BY source  THEN 
--update 
--SET    target.flag_actif=0, 
--       target.UPDATE_DATE=getdate();


END 