﻿
CREATE PROCEDURE [dbo].[PKGCP_PROC_CSL_RECLAMATION]
@DTE_OBS date

AS BEGIN

SET @DTE_OBS = CAST(@DTE_OBS as date);
SET DATEFIRST 1; -->1    Lundi

With 
cte_jour_alim_list AS
( SELECT T1.StandardDate AS jour_alim
    FROM [dbo].[DIM_TEMPS] T1
    WHERE T1.StandardDate >= CAST(DATEADD(day, -6, @DTE_OBS) AS DATE) AND T1.StandardDate <= @DTE_OBS
)
,CTE_RECLAMATION_ALL AS 

(
select ref.DTE_TRAITEMENT,cpt.DTE_OUV,cpt.DTE_CLO,  
cas.* from [$(DataHubDatabaseName)].exposition.PV_SF_CASE cas
inner join REF_CLIENT ref on cas.IDE_PERS_SF = ref.IDE_PERS_SF and  (cas.DTE_DEBUT_VAL <= ref.DTE_TRAITEMENT and cas.DTE_FIN_VAL > ref.DTE_TRAITEMENT)
inner join [dbo].[MKT_COMPTE] cpt on ref.[NUM_CLT_SAB] = cpt.[NUM_CLI_SAB]  and ref.[DTE_TRAITEMENT] = cpt.[DTE_TRAITEMENT]
where cas.TYP_REQ = '05'
and cas.COD_TYP_ENR = 'OBClaim'
and ref.DTE_TRAITEMENT  >= CAST(DATEADD(day, -6, @DTE_OBS) AS DATE) AND ref.DTE_TRAITEMENT <= @DTE_OBS
and cpt.FLAG_CPT = 'CSL'
)

--select * from CTE_RECLAMATION_ALL order by 4,1 desc;

--distinct STATUT,LIB_STATUT
,CTE_RECLAMATION AS (
select 
DTE_TRAITEMENT,
Count(ID) as                                          NB_RECLA_RECUES,
count(case when OBJ_PPAL = '22' then ID END) as       FRAIS_INTERET,
count(case when OBJ_PPAL = '18' then ID END) as       FISCALITE_TRANSFERT_CLOTURE,
count(case when OBJ_PPAL = '07' then ID END) as       ERREUR_TRAITEMENT,
count(case when OBJ_PPAL = '08' then ID END) as       DELAI_TRAITEMENT,
count(case when OBJ_PPAL = '04' then ID END) as       CONSEILLER_VENTE,
count(case when OBJ_PPAL = '01' then ID END) as       INFORMATION_CONSEIL,
count(case when OBJ_PPAL = '02' then ID END) as       QUALITE_DOC,
count(case when STATUT IN ('03','04') then ID END) as NB_RECLA_TRAITEE
from CTE_RECLAMATION_ALL
group by DTE_TRAITEMENT
)

select x.jour_alim 
,ISNULL(rec.NB_RECLA_RECUES,0)   as NB_RECLA_RECUES           
,ISNULL(rec.FRAIS_INTERET,0) as FRAIS_INTERET
,ISNULL(rec.FISCALITE_TRANSFERT_CLOTURE,0) as FISCALITE_TRANSFERT_CLOTURE
,ISNULL(rec.ERREUR_TRAITEMENT,0) as ERREUR_TRAITEMENT
,ISNULL(rec.DELAI_TRAITEMENT,0) as DELAI_TRAITEMENT
,ISNULL(rec.CONSEILLER_VENTE,0) as CONSEILLER_VENTE
,ISNULL(rec.INFORMATION_CONSEIL,0) as INFORMATION_CONSEIL
,ISNULL(rec.QUALITE_DOC,0) as QUALITE_DOC
,ISNULL(rec.NB_RECLA_TRAITEE,0) as NB_RECLA_TRAITEE
from cte_jour_alim_list x 
left join CTE_RECLAMATION rec  on x.jour_alim = rec.DTE_TRAITEMENT
END;
