﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PKGRC_PROC_wk_join_interactions]
	-- Add the parameters for the stored procedure here
	--@date_alim varchar(50),
	@nb_collected_rows int output
	--,@nb_merged_rows int output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--set @date_alim = CAST(@date_alim as date);

    /*   
	---@date_alim : parametre de date de la collecte
	*/
			truncate table [dbo].[WK_JOIN_INTERACTIONS];
			insert into [dbo].[WK_JOIN_INTERACTIONS]
			
			/* RecordType = Téléphone + SMS */

			select 
			task.Id_SF as PK_ID, 
			rec.LIBELLE as INTERACTION_TYPE, 
			(case rec.LIBELLE when 'Téléphonie' then (case Task.CallType__c when 'Inbound' then 'Appel entrant' when 'Outbound' then 'Appel sortant' else 'Non-renseigné' end) 
			when 'SMS' then 'SMS' else 'Non-renseigné' end) as INTERACTION_SOUS_TYPE, 
			task.WhatId as CASE_ID,
			task.WhoId AS ACCNT_ID,
			task.CreatedDate AS DATE_ID,
			task.OwnerId AS OWNER_ID ,
			task.LastModifiedDate AS LASTMODIFIEDDATE , 
			0 as DMC 
			
			from
			[$(DataHubDatabaseName)].[dbo].[PV_SF_TASK] task 
						
			left join [dbo].[DIM_RECORDTYPE] rec on task.RecordTypeId = rec.CODE_SF and rec.Validity_StartDate <= task.LastModifiedDate  and rec.Validity_EndDate >= task.LastModifiedDate 

			where rec.LIBELLE in ('Téléphonie','SMS') 
			and task.CreatedDate >= cast(DATEADD(month,-12, cast(getdate() as date)) as datetime) and task.CreatedDate < cast(cast(getdate() as date) as datetime) 
			
			union 
			/* RecordType = Chat IA*/ 

			select 
			
			task.Id_SF as PK_ID, 
			rec.LIBELLE as INTERACTION_TYPE, 
			'Chat IA' as INTERACTION_SOUS_TYPE, 
			task.WhatId as CASE_ID, 
			task.WhoId as ACCNT_ID, 
			task.CreatedDate AS DATE_ID, 
			task.OwnerId AS OWNER_ID ,
			task.LastModifiedDate AS LASTMODIFIEDDATE , 
			0 as DMC  			 

			from
			[$(DataHubDatabaseName)].[dbo].[PV_SF_TASK] task 
						
			left join [dbo].[DIM_RECORDTYPE] rec on task.RecordTypeId = rec.CODE_SF and rec.Validity_StartDate <= task.LastModifiedDate  and rec.Validity_EndDate >= task.LastModifiedDate 
			
			where rec.LIBELLE = ('Chat')  
			and task.CreatedDate >= cast(DATEADD(month,-12, cast(getdate() as date)) as datetime) and task.CreatedDate < cast(cast(getdate() as date) as datetime) 
					
			union 

					/* RecordType = Chat CRC*/ 

			select 
			
			chat.Id_SF as PK_ID, 
			'Chat' as INTERACTION_TYPE, 
			'Chat CRC' as INTERACTION_SOUS_TYPE, 
			chat.CaseId as CASE_ID, 
			chat.AccountId as ACCNT_ID, 
			chat.CreatedDate AS DATE_ID, 
			chat.OwnerId AS OWNER_ID ,
			chat.LastModifiedDate AS LASTMODIFIEDDATE , 
			chat.ChatDuration as DMC  			 

			from
			[$(DataHubDatabaseName)].[dbo].[PV_SF_LIVECHATTRANSCRIPT] chat 
						
			where chat.CreatedDate >= cast(DATEADD(month,-12, cast(getdate() as date)) as datetime) and chat.CreatedDate < cast(cast(getdate() as date) as datetime) and 
			chat.OwnerId <> '0055800000158G2AAI' --pour exclure les profils technique 
					
			union 

			/* EMAILMESSAGE */

			select 
			email.Id_SF as PK_ID,
			'Email' as INTERACTION_TYPE, 
			(case when email.Incoming=1 then 'Email entrant' else 'Email sortant' end) as INTERACTION_SOUS_TYPE, 
			email.ParentId as CASE_ID, 
			email.RelatedToId as ACCNT_ID,
			email.CreatedDate as DATE_ID, 
			
			email.CreatedById AS OWNER_ID , 
			email.LastModifiedDate AS LASTMODIFIEDDATE ,
			0 as DMC 

			from 
			[$(DataHubDatabaseName)].[dbo].[PV_SF_EMAILMESSAGE] email 
			LEFT JOIN [dbo].[DIM_USER] usr on email.CreatedById=usr.CODE_SF_USER 
			
			where email.CreatedDate >= cast(DATEADD(month,-12, cast(getdate() as date)) as datetime) and email.CreatedDate < cast(cast(getdate() as date) as datetime) and 
			usr.CODE_PROFIL_USER not in ('00e5800000158tqAAA','00e58000000wavkAAA') --pour exclure les profils Tech Socle et Administrateur Système 
	
		
	select @nb_collected_rows = COUNT(*) from [dbo].[WK_JOIN_INTERACTIONS];

END