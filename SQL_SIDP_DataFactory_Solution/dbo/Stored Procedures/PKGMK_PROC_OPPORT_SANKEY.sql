﻿CREATE PROC [dbo].[PKGMK_PROC_OPPORT_SANKEY]  @nbRows int OUTPUT
AS  BEGIN
-- ===============================================================================================================================================================================
--tables input : [$(DataHubDatabaseName)].dbo.[PV_SF_OPPORTUNITY_HISTORY]
--				 DIM_STADE_VENTE
--               DIM_CANAL
--tables output : [sas_vue_opport_sankey]
--description : construction de la table sas utilisée pour la génération de l'exploration Sankey du parcours des opportunités
-- =============================================================================================================================================================================

-- vider la table et la reconstruire
truncate table [dbo].sas_vue_opport_sankey;
with cte_canal_fin as
(
	select 
		Id_SF
		, LeadSource as code_canal_interact_fin
		from [$(DataHubDatabaseName)].dbo.[PV_SF_OPPORTUNITY]
		where StageName in ('09', '10', '11','12','13','14','15','16') 
), 

cte_canal_orig as
(
	select 
		Id_SF
		, StartedChannel__c as code_canal_interact_orig --changer max par dernier par date
		, row_number() over (partition by Id_SF order by Validity_StartDate desc) as rang_canal_interact_orig
		from [$(DataHubDatabaseName)].dbo.[PV_SF_OPPORTUNITY]
)

insert into sas_vue_opport_sankey

select opp.Id_SF
	, StageName as code_stade_vente
	, std.LIB_STA_VTE as stade_vente
	, coalesce(cte_canal_orig.code_canal_interact_orig, 'NA') as code_canal_interact_orig
	, coalesce(can_orig.LIBELLE, 'NA') as canal_interact_orig
	, coalesce(cte_canal_fin.code_canal_interact_fin, 'NA') as code_canal_interact_fin
	, coalesce(can_fin.LIBELLE, 'NA') as canal_interact_fin
	, opp.Validity_StartDate as date_changement
	, opp.CreatedDate as date_creation_opp
	, datediff(week, dateadd(week, datediff(week, 0, dateadd(month, datediff(month, 0, opp.CreatedDate), 0)), 0), opp.CreatedDate - 1) + 1 as nb_semaine_mois_creation 
	, opp.DistributorEntity__c as cod_entite_distributrice 
	, ss_res.LIB_SS_RES as lib_ent_distrib 
	, opp.CommercialOfferCode__c as cod_offre_commerciale
	, opp.CommercialOfferName__c as lib_offre_commerciale 
	, opp.AccountId as id_client
	, acc.Preattribution__c as Preattribution
	, opp.AdvisorCode__c as code_vendeur
	, opp.CloseDate as date_cloture
	, opp.CompleteFileFlag__c as flag_dossier_complet
	, opp.CreditAmount__c as montt_credit
	, opp.DebitDay__c as jour_debit
	, opp.DeniedOpportunityReason__c as motif_aff_ref
	, opp.DepositAmount__c as mtt_depot
	, opp.Derogation__c as derogation
	, opp.DistributionChannel__c as canal_distrib
	, opp.DistributorNetwork__c as cod_reseau
	, res.LIB_RES_DIS as lib_reseau
	, opp.FFinalDecision__c as decision_final
	, opp.FinalizedBy__c as finalise_par
	, opp.FirstDueDate__c as date_pre_remb
	, opp.FormValidation__c as date_valid_form
	, opp.FPreScoreColor__c as couleur_prescore
	, opp.FundingSubject__c as objet_financement
	, opp.IDOppty__c as num_opport
	, opp.IDProcessSous__c as id_process_sous
	, opp.IncompleteFileReason__c as motif_dossier_inc
	, opp.Indication__c as indication
	, opp.IsWon as gagnee
	, opp.LastModifiedDate as date_der_modif
	, opp.LeadSource as canal_inter
	, opp.LoanAcceptationDate__c as date_accord
	, opp.Name as nom
	, opp.PointOfSaleCode__c as code_boutique
	, opp.realizedBy__c as realise_par
	, opp.RecordTypeId as type_enregistrement
	, opp.RejectReason__c as motif_sans_suite
	, opp.RelationEntryScore__c as score_entree_rel
	, opp.ReleaseAmount__c as mtt_debloque
	, opp.ReleaseDate__c as date_deblocage
	, opp.RetractationDate__c as date_retractation
	, opp.SignatureDate__c as date_sign
	, opp.StartedChannelForIndication__c as canal_origin_indication
	, opp.SubscribedAmount__c as mtt_souscrit
from [$(DataHubDatabaseName)].dbo.[PV_SF_OPPORTUNITY_HISTORY] opp
	left join [$(DataFactoryDatabaseName)].dbo.DIM_STADE_VTE std
		on ( opp.StageName = std.COD_STA_VTE )  -- niveau stade de vente 
	left join cte_canal_orig 
		on opp.Id_SF = cte_canal_orig.Id_SF --en cas de changement de canal d'origine, prendre le dernier par date de validité
	left join [$(DataFactoryDatabaseName)].dbo.DIM_CANAL can_orig
		on (cte_canal_orig.code_canal_interact_orig = can_orig.CODE_SF
			and can_orig.PARENT_ID is not null) -- niveau canal d'interaction
	left join cte_canal_fin 
		on opp.Id_SF = cte_canal_fin.Id_SF
	left join [$(DataFactoryDatabaseName)].dbo.DIM_CANAL can_fin
		on (cte_canal_fin.code_canal_interact_fin = can_fin.CODE_SF
			and can_fin.PARENT_ID is not null) -- niveau canal d'interaction
	left join [$(DataFactoryDatabaseName)].dbo.DIM_SS_RES ss_res 
		on opp.DistributorEntity__c = ss_res.COD_SS_RES -- niveau sous reseau 
	left join [$(DataFactoryDatabaseName)].dbo.DIM_RES_DIS res 
		on opp.DistributorNetwork__c = res.COD_RES_DIS -- niveau reseau 
	left join [$(DataHubDatabaseName)].[dbo].[PV_SF_ACCOUNT] acc
		on acc.Id_SF = opp.AccountId  --préatt

order by Id_SF, opp.Validity_StartDate 
;

-- select * from sas_vue_opport_sankey

select @nbRows = COUNT(*) FROM sas_vue_opport_sankey;

END
GO
