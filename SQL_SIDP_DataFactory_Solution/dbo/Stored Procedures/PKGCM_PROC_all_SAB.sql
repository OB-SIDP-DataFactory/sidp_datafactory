﻿CREATE PROC [dbo].[PKGCM_PROC_all_SAB]
	@DATE_ALIM DATEtime2, @jours_histo int   = 1, --valeur 1 par défaut = traitement nominal par défaut
	@nbRows int OUTPUT -- nb lignes processées
AS
BEGIN 
	-- =================================================================================================================================
	--tables input : [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_COMPTE], [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_COMPTEHistory]
	--				 [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_CARTE], [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_CARTEHistory]
	--				  DIM_TEMPS
	--tables output : WK_ALL_SAB_COMPTES
	--				  WK_ALL_SAB_CARTES
	--description : pour chaque table SAB, créer une table avec historique sur une période déterminée par @date_alim et @jours_histo
	-- ==================================================================================================================================
	-- variables comptage nombre lignes
	declare @nbRows_comptes int; 
	declare @nbRows_cartes int; 
	/************** créer un sous-ensemble de la table dimension temps **************/
	--declare @DATE_ALIM datetime  = '2017-03-01';
	--declare @jours_histo int = 2; --nombre de jours reprise historique, 1=traitement nominal
	-- select dateadd(day,-@jours_histo, @DATE_ALIM);
	select PK_ID, StandardDate 
	into #wk_temps_sab
	from DIM_TEMPS
	where cast( dateadd(day,-@jours_histo, @DATE_ALIM) as date) <= StandardDate and StandardDate < cast(@DATE_ALIM as date) ; 
	-- select * from #wk_temps_sab
	/****** historique table sab comptes ******/
	-- table main + historique
	--declare @DATE_ALIM datetime  = '2017-03-01';
	--declare @jours_histo int = 2; 
	IF OBJECT_ID('[WK_SAB_COMPTES]') IS NOT NULL
	BEGIN truncate TABLE [WK_SAB_COMPTES] END
	insert into [WK_SAB_COMPTES]
	select DWHCPTCOM, DWHCPTDAC, DWHCPTDAO, DWHCPTCLO, DWHCPTCPT, DWHCPTMTD, DWHCPTRUB, DWHCPTPPAL, Validity_StartDate, Validity_EndDate
		from [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_COMPTE] where DWHCPTRUB in ('251180', '254181', '254111') /* pour isoler les comptes Orange Bank */ 
		union all 
		select DWHCPTCOM, DWHCPTDAC, DWHCPTDAO,DWHCPTCLO, DWHCPTCPT, DWHCPTMTD, DWHCPTRUB, DWHCPTPPAL, Validity_StartDate, Validity_EndDate
		from [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_COMPTEHistory] 
		where Validity_EndDate >= DATEADD(day,-@jours_histo,@DATE_ALIM) and DWHCPTRUB in ('251180', '254181', '254111') /* pour isoler les comptes Orange Bank */ 
	; 
	-- select top 1000 * from [WK_SAB_COMPTES];

	-- isoler période de temps requise 
	IF OBJECT_ID('[WK_ALL_SAB_COMPTES]') IS NOT NULL
	BEGIN truncate TABLE [WK_ALL_SAB_COMPTES] END
	insert into [WK_ALL_SAB_COMPTES] (date_alim, DWHCPTCOM, DWHCPTDAC, DWHCPTDAO, DWHCPTCLO, DWHCPTCPT, DWHCPTRUB, DWHCPTMTD, DWHCPTPPAL, Validity_StartDate, Validity_EndDate)
	select d.StandardDate as date_alim, c.DWHCPTCOM, c.DWHCPTDAC, c.DWHCPTDAO, c.DWHCPTCLO, c.DWHCPTCPT, c.DWHCPTRUB, c.DWHCPTMTD, c.DWHCPTPPAL, c.Validity_StartDate, c.Validity_EndDate
	from #wk_temps_sab d
	outer apply 
		(
		select DWHCPTCOM, DWHCPTDAC, DWHCPTDAO,DWHCPTCLO, DWHCPTCPT, DWHCPTMTD, DWHCPTRUB, DWHCPTPPAL, Validity_StartDate, Validity_EndDate
		from [WK_SAB_COMPTES]
		where ( cast(Validity_StartDate as date) <= d.StandardDate and d.StandardDate < cast(Validity_EndDate as date) )
		) c
	;
	-- select top 1000 * from [WK_ALL_SAB_COMPTES];

	select @nbRows_comptes =  count(*) from [WK_ALL_SAB_COMPTES] ;
	/****** historique table sab cartes ******/
	-- table main + historique 
	--declare @DATE_ALIM datetime  = '2017-03-01';
	--declare @jours_histo int = 2; 
	IF OBJECT_ID('[WK_SAB_CARTES]') IS NOT NULL
	BEGIN TRUNCATE TABLE [WK_SAB_CARTES] END
	INSERT into [WK_SAB_CARTES]
	select DWHCARCOM, DWHCARNUS, DWHCARCAR, DWHCARCET, DWHCARREM, DWHCARVAL, Validity_StartDate, Validity_EndDate
	from [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_CARTE] where DWHCARCOM LIKE '75%'
	union all 
	select DWHCARCOM, DWHCARNUS, DWHCARCAR, DWHCARCET, DWHCARREM, DWHCARVAL, Validity_StartDate, Validity_EndDate
	from [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_CARTEHistory]
	where Validity_EndDate >= DATEADD(day,-@jours_histo,@DATE_ALIM)  and DWHCARCOM LIKE '75%'
	;
	-- select top 1000 * from [WK_SAB_CARTES]

	-- isoler période de temps requise
	/* type_carte : M = paiement mobile (code WMI), P = carte physique (tous les autres codes) */
	IF OBJECT_ID('[WK_ALL_SAB_CARTES]') IS NOT NULL
	BEGIN TRUNCATE  TABLE WK_ALL_SAB_CARTES END
	INSERT into WK_ALL_SAB_CARTES
	select 
		d.StandardDate as date_alim, c.DWHCARCOM, c.DWHCARNUS, c.DWHCARCAR, c.DWHCARCET,  
		case when DWHCARCAR = 'WMI' then 'M' else 'P' end as type_carte, c.DWHCARREM, c.DWHCARVAL, 
		c.Validity_StartDate, c.Validity_EndDate
	from #wk_temps_sab d
	outer apply 
		( select DWHCARCOM, DWHCARNUS, DWHCARCAR, DWHCARCET, DWHCARREM, DWHCARVAL, Validity_StartDate, Validity_EndDate
			from ( select DWHCARCOM, DWHCARNUS, DWHCARCAR, DWHCARCET, DWHCARREM, DWHCARVAL, Validity_StartDate, Validity_EndDate, row_number() over (partition by DWHCARCOM, DWHCARNUS order by Validity_StartDate) as Validity_RowNumber from [WK_SAB_CARTES] ) t
		   where ( ( cast(Validity_StartDate as date) <= d.StandardDate and d.StandardDate < cast(Validity_EndDate as date) ) or
				 ( cast(DWHCARREM as date) <= d.StandardDate and d.StandardDate < cast(Validity_StartDate as date) and Validity_RowNumber = 1 ) )
		) c
	;
	-- select top 1000 * from [WK_ALL_SAB_CARTES] where date_alim = '2017-05-02'
	-- select distinct date_alim from [WK_ALL_SAB_CARTES]

	select @nbRows_cartes=  count(*) from WK_ALL_SAB_CARTES ;
	-- nb lignes total
	select @nbRows = @nbRows_comptes + @nbRows_cartes; 
END