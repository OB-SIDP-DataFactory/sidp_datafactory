﻿

CREATE PROCEDURE [dbo].[PKGCP_PROC_CARE_H_new]
@Date_obs date

AS BEGIN

/*
declare @Date_obs date; 
set @Date_obs='2018-05-17';*/
SET @Date_obs = CAST(@Date_obs as date);
SET DATEFIRST 1; -->1    Lundi

With 
cte_date_obs AS
( SELECT @Date_obs AS StandardDate
       , DATEADD(day, (-1 * DATEPART(dw, @Date_obs)) + 1, @Date_obs) AS FirstDOW
       , DATEADD(day, (7 - DATEPART(dw, @Date_obs)), @Date_obs) AS LastDOW
	   , DATEADD(day, (-1 * DATEPART(dw, @Date_obs)) -6, @Date_obs) AS FirstDOPW
	   , DATEADD(day, (7 - DATEPART(dw, @Date_obs)) -7, @Date_obs) AS LastDOPW
)
, cte_jour_alim_list AS
( 	SELECT T1.StandardDate AS jour_alim
    FROM dbo.[DIM_TEMPS] T1
    WHERE T1.StandardDate > CAST(DATEADD(day, -7, @Date_obs) AS DATE) AND T1.StandardDate <= @Date_obs --7 jours glissants 
)
--***** Toutes les requetes *****--
, cte_Dde_ALL AS
( SELECT DTE_ALIM
	  ,[IDE_PERS_SF]
	  ,CASE WHEN [LIB_CAN_INT_ORI] LIKE 'Appel%' then '04' ELSE [CAN_INT_ORI] END as CANAL 
	  ,CASE WHEN [LIB_CAN_INT_ORI] LIKE 'Appel%' then 'Téléphonie' ELSE [LIB_CAN_INT_ORI] END as LIB_CANAL
      ,[IDE_TYP_ENR]
      ,[COD_TYP_ENR]
      ,[LIB_TYP_ENR]
	  ,[TYP_REQ]
      ,[LIB_TYP_REQ]
	  ,[NB_REQ]
	  ,[DELAI_TRAITEMENT]
	  ,[DELAI_MED_TRT_CANAL]
	FROM [CP_STOCK_CASE_REQUEST] 
	where ([CAN_INT_ORI] in ('07','11','12') or [LIB_CAN_INT_ORI] LIKE 'Appel%') --tous les types Appel, Chat, Email et Formulaire de demandes
	--and [TYP_REQ] in ('01','02','03','04','05','08','10','11','12','13','14','15','16','30','31','40')
)

,lst_Canal AS
( SELECT distinct CASE WHEN [LIB_CAN_INT_ORI] LIKE 'Appel%' then '04' ELSE [CAN_INT_ORI] END as CANAL 
	  ,CASE WHEN [LIB_CAN_INT_ORI] LIKE 'Appel%' then 'Téléphonie' ELSE [LIB_CAN_INT_ORI] END as LIB_CANAL
	   FROM [CP_STOCK_CASE_REQUEST]
	   WHERE ([CAN_INT_ORI] in ('07','11','12') or [LIB_CAN_INT_ORI] LIKE 'Appel%') 		
)

,lst_Typ_Req AS
( SELECT distinct TYP_REQ, LIB_TYP_REQ 
		FROM [CP_STOCK_CASE_REQUEST]
		--WHERE [TYP_REQ] in ('01','02','03','04','05','08','10','11','12','13','14','15','16','30','31','40')
		
)
--select * from  lst_Typ_Req;
	
--Requête Demande restant à traiter sur les 7 jours glissants précédent la journée d'observation
, cte_Dde_a_traiter_Cumul_Jour AS
( SELECT alim.jour_alim
		,can.CANAL
		,can.LIB_CANAL
		,req.LIB_TYP_REQ
		,SUM(ISNULL(dem.[NB_REQ],0)) as NB
	FROM cte_jour_alim_list alim
	LEFT JOIN lst_Canal can on 1=1
	LEFT JOIN lst_Typ_Req req on 1=1
	LEFT JOIN cte_Dde_ALL dem 
	on alim.jour_alim =  dem.DTE_ALIM
	and can.CANAL = dem.CANAL
	and req.LIB_TYP_REQ = dem.LIB_TYP_REQ 
	group by alim.jour_alim, can.CANAL, can.LIB_CANAL, req.LIB_TYP_REQ
)

-- Requête Délai traitement demande
, cte_Delai_traitmt_Dde AS
( SELECT distinct alim.jour_alim,
	can.CANAL,
	can.LIB_CANAL,
	dem.DELAI_MED_TRT_CANAL
	FROM cte_jour_alim_list alim
	LEFT JOIN lst_Canal can on 1=1
	LEFT JOIN cte_Dde_ALL dem 
	on alim.jour_alim =  dem.DTE_ALIM
	and can.CANAL = dem.CANAL
	and alim.jour_alim = dem.DTE_ALIM
)

select t1.jour_alim
	, t1.CANAL
	, t1.LIB_CANAL
	, t1.LIB_TYP_REQ
	, SUM(t1.NB) as NB
from cte_Dde_a_traiter_Cumul_Jour t1 
GROUP BY t1.jour_alim
	, t1.CANAL
	, t1.LIB_CANAL
	, t1.LIB_TYP_REQ
UNION ALL
select distinct t2.jour_alim, 
	   t2.CANAL as CANAL, 
	   t2.LIB_CANAL as LIB_CANAL,
	   'Delai' as LIB_TYP_REQ,
	   ISNULL(t2.DELAI_MED_TRT_CANAL,0) as NB
FROM cte_Delai_traitmt_Dde t2 

END;