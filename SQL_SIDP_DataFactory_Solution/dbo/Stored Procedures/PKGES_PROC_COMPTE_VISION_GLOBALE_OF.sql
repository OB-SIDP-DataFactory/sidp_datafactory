﻿CREATE PROCEDURE [dbo].[PKGES_PROC_COMPTE_VISION_GLOBALE_OF]
@Datedebut date, 
@Datefin date   

AS BEGIN

select 
sum(T.nb_opportOF) as nb_opportOF,
sum(T.CanalBoutiqueFinalOF) CanalBoutiqueFinalOF,
sum(T.IndicBoutCanalBoutiqueFinalOF) IndicBoutCanalBoutiqueFinalOF,
sum(T.IndicDigCanalBoutiqueFinalOF) IndicDigCanalBoutiqueFinalOF,
sum(T.SansIndicCanalBoutiqueFinalOF) SansIndicCanalBoutiqueFinalOF,
sum(T.CanalAppliFinalOF) CanalAppliFinalOF,
sum(T.IndicBoutCanalAppliFinalOF) IndicBoutCanalAppliFinalOF,
sum(T.IndicDigCanalAppliFinalOF) IndicDigCanalAppliFinalOF,
sum(T.SansIndicCanalAppliFinalOF) SansIndicCanalAppliFinalOF,
sum(T.RenvoiTraficCanalAppliFinalOF) RenvoiTraficCanalAppliFinalOF
from (
Select 
count(distinct Wk.Id_SF) as nb_opportOF,
case when LeadSource in ('01','02','03') then count(distinct Wk.Id_SF) else 0 end as  CanalBoutiqueFinalOF,
case when LeadSource in ('01','02','03') and flag_indication='Oui' and DistributorEntity__c in('01','02') then count(distinct Wk.Id_SF) else 0 end as  IndicBoutCanalBoutiqueFinalOF,
case when LeadSource in ('01','02','03') and flag_indication='Oui' and DistributorEntity__c='04' then count(distinct Wk.Id_SF) else 0 end as  IndicDigCanalBoutiqueFinalOF,
case when LeadSource in ('01','02','03') and flag_indication='Non'   and DistributorEntity__c in ('01','02') then count(distinct Wk.Id_SF) else 0 end as  SansIndicCanalBoutiqueFinalOF,
case when LeadSource in ('10','11') then count(distinct Wk.Id_SF) else 0 end as  CanalAppliFinalOF,
case when LeadSource in ('10','11') and flag_indication='Oui' and DistributorEntity__c in('01','02') then count(distinct Wk.Id_SF) else 0 end as  IndicBoutCanalAppliFinalOF,
case when LeadSource in ('10','11') and flag_indication='Oui' and DistributorEntity__c='04' then count(distinct Wk.Id_SF) else 0 end as  IndicDigCanalAppliFinalOF,
case when LeadSource in ('10','11') and flag_indication='Non'   and DistributorEntity__c in ('01','02') then count(distinct Wk.Id_SF) else 0 end as  SansIndicCanalAppliFinalOF,
case when LeadSource in ('10','11') and flag_indication='Non'   and DistributorEntity__c in ('04') then count(distinct Wk.Id_SF) else 0 end as  RenvoiTraficCanalAppliFinalOF
from [dbo].[WK_STOCK_OPPORTUNITY] as Wk
left outer join  [$(DataHubDatabaseName)].[dbo].[PV_SF_OPPORTUNITYISDELETED] as Wkd on Wk.Id_SF= Wkd.Id_SF
where Wkd.Id_SF is null and DistributorNetwork__c ='01' 
and CommercialOfferCode__c='OC80' and StageName ='09'
and  cast(CreatedDate as date) between @Datedebut and @Datefin
and DistributorEntity__c IN ('01','02','04')
and flag_opport_last_state = 1
and LeadSource in ('01','02','03','10','11','12','13')
and [date_alim]=(SELECT MAX([date_alim]) FROM [dbo].[WK_STOCK_OPPORTUNITY])
group by flag_indication,LeadSource,DistributorEntity__c) as T

END;
