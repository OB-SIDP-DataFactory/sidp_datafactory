﻿

CREATE PROCEDURE [dbo].[PKGCR_PROC_VIE_CREDIT_DECAISSE]
@DTE_OBS date

AS BEGIN

DECLARE @IsoWeekObs int;
DECLARE @IsoWeekYearObs int;

/*declare @DTE_OBS date;
SET @DTE_OBS = '2017-01-03';*/

SET @IsoWeekObs = (select IsoWeek from [dbo].[DIM_TEMPS] where CAST(Date as date) = CAST(@DTE_OBS as date) );
SET @IsoWeekYearObs = (select IsoWeekYear from [dbo].[DIM_TEMPS] where CAST(Date as date) = CAST(@DTE_OBS as date) );
SET @DTE_OBS = CAST(@DTE_OBS as date) ;
SET DATEFIRST 1; -->1    Lundi 

WITH 
CTE_CRE_ALL AS (
SELECT CRE.[COD_CAN_INT_ORI]
      ,CRE.[LIB_CAN_INT_ORI]
      ,CRE.[DTE_DER_DEB]
	  ,CRE.[WeekOfYear]
      ,CRE.[IsoWeek]
      ,CRE.[Month]
	  ,CRE.[Year]
      ,CRE.[IsoWeekYear]
      ,CRE.[NBE_CREDIT]
      ,CRE.[MTT_TOT_DEB]
FROM [dbo].[CRE_STOCK_CREDIT] CRE 
WHERE CRE.COD_CAN_INT_ORI IN ('11', '10', '12', '13', '14', '15')
AND CRE.[DTE_DER_DEB] <= @DTE_OBS /*S'arréter à la date du rapport (paramètre du rapport)*/
), 

--Production semaine
CTE_CRE_semaine AS (
SELECT	SUM([NBE_CREDIT]) as NB_CREDIT_DECAISSE_semaine
		,SUM([MTT_TOT_DEB]) MTT_CREDIT_EMPRUNTE_semaine
		,[COD_CAN_INT_ORI]
		,[LIB_CAN_INT_ORI] 
FROM CTE_CRE_ALL
WHERE [IsoWeekYear] = @IsoWeekYearObs
AND [IsoWeek]= @IsoWeekObs
GROUP BY COD_CAN_INT_ORI, LIB_CAN_INT_ORI 
),

--Production semaine précédente
CTE_CRE_semaine_prec AS (
SELECT	SUM([NBE_CREDIT]) as NB_CREDIT_DECAISSE_semaine_prec
		,SUM([MTT_TOT_DEB]) MTT_CREDIT_EMPRUNTE_semaine_prec
		,[COD_CAN_INT_ORI]
		,[LIB_CAN_INT_ORI] 
FROM CTE_CRE_ALL
WHERE [IsoWeekYear] = DATEPART(YEAR,DATEADD(week,-1,@DTE_OBS))
AND [IsoWeek] = DATEPART(ISO_WEEK,CAST(DATEADD(WEEK,-1,@DTE_OBS) AS DATE))
GROUP BY COD_CAN_INT_ORI, LIB_CAN_INT_ORI 
),

--Production mois
CTE_CRE_mois AS (
SELECT	SUM([NBE_CREDIT]) as NB_CREDIT_DECAISSE_mois
		,SUM([MTT_TOT_DEB]) MTT_CREDIT_EMPRUNTE_mois
		,[COD_CAN_INT_ORI]
		,[LIB_CAN_INT_ORI] 
FROM CTE_CRE_ALL
WHERE [Year] = DATEPART(YEAR,@DTE_OBS)
and [Month] = DATEPART(MONTH,@DTE_OBS)
and [WeekOfYear] <= DATEPART(WEEK,@DTE_OBS) 
GROUP BY COD_CAN_INT_ORI, LIB_CAN_INT_ORI 
),

--Production mois précédent
CTE_CRE_mois_prec AS (
SELECT	SUM([NBE_CREDIT]) as NB_CREDIT_DECAISSE_mois_prec
		,SUM([MTT_TOT_DEB]) MTT_CREDIT_EMPRUNTE_mois_prec
		,[COD_CAN_INT_ORI]
		,[LIB_CAN_INT_ORI] 
FROM CTE_CRE_ALL
WHERE [Year] = DATEPART(YEAR,dateadd(MONTH,-1,@DTE_OBS))
AND	[Month] = DATEPART(MONTH,dateadd(MONTH,-1,@DTE_OBS))
GROUP BY COD_CAN_INT_ORI, LIB_CAN_INT_ORI 
),

--Production mois de l'année précédente
CTE_CRE_mois_annee_prec as (
SELECT SUM([NBE_CREDIT]) as NB_CREDIT_DECAISSE_mois_annee_prec
		,SUM([MTT_TOT_DEB]) MTT_CREDIT_EMPRUNTE_mois_annee_prec
		,[COD_CAN_INT_ORI]
		,[LIB_CAN_INT_ORI] 
FROM CTE_CRE_ALL
WHERE [Year] = DATEPART(YEAR,dateadd(YEAR,-1,@DTE_OBS))   
AND [Month] = DATEPART(MONTH,@DTE_OBS)
GROUP BY COD_CAN_INT_ORI,LIB_CAN_INT_ORI
),

--Fixer les canaux d'interaction d'origine dans le rapport
CTE_CRE_lib AS (
SELECT DISTINCT [COD_CAN_INT_ORI],
				[LIB_CAN_INT_ORI]
FROM CTE_CRE_ALL
)

SELECT	lib.COD_CAN_INT_ORI,
		lib.LIB_CAN_INT_ORI,
		ISNULL(sem.NB_CREDIT_DECAISSE_semaine,0) as NB_CREDIT_DECAISSE_semaine,
		ISNULL(sem_prec.NB_CREDIT_DECAISSE_semaine_prec,0) as NB_CREDIT_DECAISSE_semaine_prec,
		Case when ISNULL(sem_prec.NB_CREDIT_DECAISSE_semaine_prec,0) = 0 
			 then 0 
			 else CAST((ISNULL(sem.NB_CREDIT_DECAISSE_semaine,0) - sem_prec.NB_CREDIT_DECAISSE_semaine_prec) as float) /  CAST(sem_prec.NB_CREDIT_DECAISSE_semaine_prec as float)
		END as Tx_S_Evol_Nb, 
		ISNULL(sem.MTT_CREDIT_EMPRUNTE_semaine,0) as MTT_CREDIT_EMPRUNTE_semaine, 		
		ISNULL(sem_prec.MTT_CREDIT_EMPRUNTE_semaine_prec,0) as MTT_CREDIT_EMPRUNTE_semaine_prec, 
		Case when ISNULL(sem_prec.MTT_CREDIT_EMPRUNTE_semaine_prec,0) = 0 
			 then 0 
			 else CAST((ISNULL(sem.MTT_CREDIT_EMPRUNTE_semaine,0) - sem_prec.MTT_CREDIT_EMPRUNTE_semaine_prec) as float) /  CAST(sem_prec.MTT_CREDIT_EMPRUNTE_semaine_prec as float)
		END as Tx_S_Evol_Mtt,
		ISNULL(mois.NB_CREDIT_DECAISSE_mois,0) as NB_CREDIT_DECAISSE_mois, 
		ISNULL(mois_prec.NB_CREDIT_DECAISSE_mois_prec,0) as NB_CREDIT_DECAISSE_mois_prec, 
		Case when ISNULL(mois_prec.NB_CREDIT_DECAISSE_mois_prec,0) = 0 
			 then 0 
			 else CAST((ISNULL(mois.NB_CREDIT_DECAISSE_mois,0) - mois_prec.NB_CREDIT_DECAISSE_mois_prec) as float) /  CAST(mois_prec.NB_CREDIT_DECAISSE_mois_prec as float)
		END as Tx_M_Evol_Nb,
		ISNULL(mois.MTT_CREDIT_EMPRUNTE_mois,0) as MTT_CREDIT_EMPRUNTE_mois, 
		ISNULL(mois_prec.MTT_CREDIT_EMPRUNTE_mois_prec,0) as MTT_CREDIT_EMPRUNTE_mois_prec, 
		Case when ISNULL(mois_prec.MTT_CREDIT_EMPRUNTE_mois_prec,0) = 0 
			 then 0 
			 else CAST((ISNULL(mois.MTT_CREDIT_EMPRUNTE_mois,0) - mois_prec.MTT_CREDIT_EMPRUNTE_mois_prec) as float) /  CAST(mois_prec.MTT_CREDIT_EMPRUNTE_mois_prec as float)
		END as Tx_M_Evol_Mtt,

		ISNULL(mois_annee_prec.NB_CREDIT_DECAISSE_mois_annee_prec,0) as NB_CREDIT_DECAISSE_mois_annee_prec, 
		ISNULL(mois_annee_prec.MTT_CREDIT_EMPRUNTE_mois_annee_prec,0) as MTT_CREDIT_EMPRUNTE_mois_annee_prec,
		Case when ISNULL(mois_annee_prec.NB_CREDIT_DECAISSE_mois_annee_prec,0) = 0 
			 then 0 
			 else CAST((ISNULL(mois.NB_CREDIT_DECAISSE_mois,0) - mois_annee_prec.NB_CREDIT_DECAISSE_mois_annee_prec) as float) /  CAST(mois_annee_prec.NB_CREDIT_DECAISSE_mois_annee_prec as float)
		END as Tx_M_N_Evol_Nb,
		Case when ISNULL(mois_annee_prec.MTT_CREDIT_EMPRUNTE_mois_annee_prec,0) = 0 
			 then 0 
			 else CAST((ISNULL(mois.MTT_CREDIT_EMPRUNTE_mois,0) - mois_annee_prec.MTT_CREDIT_EMPRUNTE_mois_annee_prec) as float) /  CAST(mois_annee_prec.MTT_CREDIT_EMPRUNTE_mois_annee_prec as float)
		END as Tx_M_N_Evol_Mtt
FROM CTE_CRE_lib lib
LEFT JOIN CTE_CRE_semaine sem
ON lib.COD_CAN_INT_ORI = sem.COD_CAN_INT_ORI
LEFT JOIN CTE_CRE_semaine_prec sem_prec
ON lib.COD_CAN_INT_ORI = sem_prec.COD_CAN_INT_ORI
LEFT JOIN CTE_CRE_mois mois
ON lib.COD_CAN_INT_ORI = mois.COD_CAN_INT_ORI
LEFT JOIN CTE_CRE_mois_prec mois_prec
ON lib.COD_CAN_INT_ORI = mois_prec.COD_CAN_INT_ORI
LEFT JOIN CTE_CRE_mois_annee_prec mois_annee_prec
ON lib.COD_CAN_INT_ORI = mois_annee_prec.COD_CAN_INT_ORI

END;