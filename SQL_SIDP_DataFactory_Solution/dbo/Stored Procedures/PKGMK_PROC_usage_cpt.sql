﻿CREATE PROCEDURE dbo.PKGMK_PROC_usage_cpt
	@nbRows int OUTPUT -- nb lignes traitées
AS
BEGIN
	-- =======================================================================
	--tables input : WK_ALL_SAB_COMPTES
	--				 WK_ALL_SAB_CARTES
	--				 WK_ALL_SFCRM_ACCOUNT
	--				 MKT_NB_OP
	--				 [$(DataHubDatabaseName)].dbo.PV_SA_Q_MOUVEMENT	
	--				 [$(DataHubDatabaseName)].dbo.PV_FE_EQUIPMENT
	--				 [$(DataHubDatabaseName)].dbo.PV_FE_ENROLMENTFOLDER
	--				 [$(DataHubDatabaseName)].dbo.PV_FE_COMMERCIALPRODUCT
	--				 [$(DataHubDatabaseName)].dbo.PV_FE_COMMERCIALOFFER
	--				 [$(DataHubDatabaseName)].dbo.PV_SF_OPPORTUNITY
	--               DIM_OPERATIONS
	--               DIM_TEMPS
	--               DIM_CSP_CLI
	--               DIM_CANAL
	--               DIM_RESEAU
	--               DIM_PRODUIT_OFFRE
	--tables output : sas_vue_usage_cpt
	--description : construction de la vue sas des comptes en stock
	-- =======================================================================
	/*
	I - préparation tables intermédiaires :
	1 - pivot moyens paiement : PM / CB : #pivot_sab_cartes
	2 - pivot nb opérations : PM / CB : #pivot_nb_ope
	3 - table montants dépôts, retraits
	II - JOINture de toutes les tables sources
	III - calcul indicateurs agrégés
	IV - JOINture avec les dimensions pour avoir les libellées
	*/
	/* 1 - preparation table pivot cartes */
	-- drop table #pivot_sab_cartes;
	SELECT
		date_alim,
		DWHCARCOM,
		P,
		M
	INTO
		#pivot_sab_cartes
	FROM
		(
			SELECT
				date_alim,
				DWHCARCOM,
				type_carte,
				DWHCARNUS
			FROM
				dbo.WK_ALL_SAB_CARTES	WITH(NOLOCK)
			WHERE
				1 = 1
				AND	ISNULL(DWHCARCET, '') = '002'
		)	AS	table_princ -- code carte active
		PIVOT
		(
			COUNT(DWHCARNUS)
			FOR	type_carte	IN	(P, M)
		)	AS	xx;
	-- SELECT * FROM #pivot_sab_cartes
	--SELECT date_alim, DWHCARCOM, COUNT(*)
	--FROM #pivot_sab_cartes
	--group by date_alim, DWHCARCOM
	--having COUNT(*) > 1
	--> 0 lignes OK : pas de doublons par date_alim, DWHCARCOM
	/* 2 - preparation table pivot nb opérations */
	-- drop table #pivot_nb_ope
	SELECT
		date_alim,
		NUM_CPT,
		CB,
		PM,
		virement,
		prélèvement
	INTO
		#pivot_nb_ope
	FROM
		(
			SELECT
				date_alim,
				NUM_CPT,
				TYPE,
				cum_nb_ope--, PK_ID
			FROM
				dbo.MKT_NB_OP	WITH(NOLOCK)
		)	AS	table_princ
		PIVOT
		--(count(PK_ID) for type in (CB, PM, virement, prélèvement))	AS	xx
		(
			SUM(cum_nb_ope)
			FOR	TYPE	IN	(CB, PM, virement, prélèvement)
		)	AS	xx;
	-- SELECT * FROM #pivot_nb_ope;
	--SELECT date_alim, NUM_CPT, COUNT(*)
	--FROM #pivot_nb_ope
	--group by date_alim, NUM_CPT
	--having COUNT(*) > 1
	--> 0 lignes OK : pas de doublons par date_alim, num_cpt
	/* 3 - preparation table montants dépôts, retraits */
	/* prendre dépôts & retraits pour les comptes epargne */
	--drop table #wk_dep_ret
	SELECT
		DTE_OPE,
		NUM_CPT,
		SUM(IIF(MTT_EUR <= 0, MTT_EUR, 0))	AS	montant_dep,
		SUM(IIF(MTT_EUR > 0, MTT_EUR, 0))	AS	montant_ret
	INTO
		#wk_dep_ret
	FROM
		[$(DataHubDatabaseName)].dbo.PV_SA_Q_MOUVEMENT	WITH(NOLOCK)
	--	LEFT JOIN DIM_OPERATIONS d
	--		on m.COD_EVE = d.COD_EVE
	--		AND m.COD_OPE = d.COD_OPE
	--WHERE d.TYPE = 'dep_ret'
	GROUP BY
		NUM_CPT,
		DTE_OPE;
	-- SELECT * FROM #wk_dep_ret;
	--SELECT DTE_OPE, NUM_CPT, COUNT(*)
	--FROM #wk_dep_ret
	--group by DTE_OPE, NUM_CPT
	--having COUNT(*) > 1
	--> 0 lignes OK : pas de doublons par date, num_cpt
	--SELECT date_alim, count(distinct DWHCPTCOM) FROM WK_ALL_SAB_COMPTES
	--WHERE DWHCPTDAC IS NULL --compte pas fermé au moment de l'alimentation
	--		AND DWHCPTCOM IS NOT NULL
	--group by date_alim
	--order by date_alim
	/* II - JOINture de toutes les tables sources */
	-- SELECT top 1000 * FROM WK_ALL_SAB_COMPTES order by DWHCPTCOM, date_alim;
	IF	(OBJECT_ID('wk_join_sources_usage_cpt') IS NOT NULL)
	BEGIN
		TRUNCATE TABLE	dbo.wk_join_sources_usage_cpt
	END
	INSERT INTO
		dbo.wk_join_sources_usage_cpt
	SELECT
		a.date_alim,
		a.DWHCPTCOM	AS	NUM_CPT_CLI,
		d.Id_SF		AS	Id_SF_Opp,
		e.Id_SF		AS	Id_SF_Account,
		IIF(
			a.DWHCPTCPT <= 0,
			a.DWHCPTCPT,
			0
		)	AS	encours_cred,
		IIF(
			a.DWHCPTCPT > 0,
			a.DWHCPTCPT,
			0
		)	AS	encours_deb,
		a.DWHCPTMTD	AS	encours_deb_non_auth,
		P.CODE		AS	code_fe_type_compte,
		IIF(
			d.Indication__c = 'Oui',
			d.StartedChannelForIndication__c,
			d.StartedChannel__c
		)			AS	StartedChannel__c,
		d.DistributionChannel__c,
		d.DistributorNetwork__c,
		e.Occupation__pc,
		e.OccupationNiv1__pc,
		e.OccupationNiv3__pc,
		(
			CAST(FORMAT(e.date_alim,'yyyymmdd') AS INT)
			- CAST(FORMAT(e.PersonBirthdate,'yyyymmdd') AS INT)
		) / 10000		AS	AGE_CLI, --différence en nombre complet d'années
		f.P				AS	nb_cb,
		f.M				AS	nb_pm,
		h.CB			AS	nb_ope_cb,
		h.PM			AS	nb_ope_pm,
		h.prélèvement	AS	nb_ope_prel,
		h.virement		AS	nb_ope_vir,
		g.montant_dep,
		g.montant_ret,
		a.DWHCPTRUB		AS	code_sab_type_compte
	FROM
		dbo.WK_ALL_SAB_COMPTES	AS	a  	WITH(NOLOCK)
		LEFT JOIN	[$(DataHubDatabaseName)].dbo.PV_FE_EQUIPMENT			AS	b	WITH(NOLOCK)
			ON	a.DWHCPTCOM = b.EQUIPMENT_NUMBER
		LEFT JOIN	[$(DataHubDatabaseName)].dbo.PV_FE_ENROLMENTFOLDER	AS	c 	WITH(NOLOCK)
			ON	b.ENROLMENT_FOLDER_ID = c.ID_FE_ENROLMENT_FOLDER
		LEFT JOIN	[$(DataHubDatabaseName)].dbo.PV_FE_COMMERCIALPRODUCT	AS	p	WITH(NOLOCK)
			ON	b.PRODUCT_ID = p.ID_FE_COMMERCIALPRODUCT
		LEFT JOIN	[$(DataHubDatabaseName)].dbo.PV_SF_OPPORTUNITY		AS	d 	WITH(NOLOCK)
			ON	c.SALESFORCE_OPPORTUNITY_ID = d.Id_SF
		JOIN
		(
			SELECT
				*
			FROM
				dbo.WK_ALL_SFCRM_ACCOUNT	WITH(NOLOCK)
			WHERE
				1 = 1
				AND	RecordTypeId = 
				(
					SELECT DISTINCT
						CODE_SF
					FROM
						dbo.DIM_RECORDTYPE	WITH(NOLOCK)
					WHERE
						LIBELLE = 'Personne Physique Client'
				)
		)	AS	e
			ON	a.DWHCPTPPAL = e.IDCustomerSAB__pc
				AND	CAST(a.date_alim	AS	date) = CAST(e.date_alim	AS	date)
		LEFT JOIN	#pivot_sab_cartes	AS	f	WITH(NOLOCK)
			ON	a.DWHCPTCOM = f.DWHCARCOM
				AND	CAST(a.date_alim	AS	date) = CAST(f.date_alim	AS	date)
		LEFT JOIN	#pivot_nb_ope		AS	h	WITH(NOLOCK)
			ON	a.DWHCPTCOM = h.NUM_CPT
				AND CAST(h.date_alim	AS	date) = CAST(a.date_alim	AS	date)
		LEFT JOIN	#wk_dep_ret			AS	g	WITH(NOLOCK)
			ON	a.DWHCPTCOM = g.NUM_CPT
				AND CAST(a.date_alim	AS	date) = g.DTE_OPE
	WHERE
		1 = 1
		AND	a.DWHCPTRUB in ('251180','254181','254111')
		AND   --  pour isoler les comptes Orange Bank
		(
			a.DWHCPTDAC > a.date_alim
			OR	a.DWHCPTDAC IS NULL
		);
	SELECT
		--axes d'analyse
		date_alim,
		code_sab_type_compte,
		code_fe_type_compte,
		StartedChannel__c,
		DistributionChannel__c,
		DistributorNetwork__c,
		Occupation__pc,
		OccupationNiv1__pc,
		OccupationNiv3__pc,
		AGE_CLI,
		nb_cb,
		nb_pm,
		nb_ope_cb,
		nb_ope_pm,
		nb_ope_prel,
		nb_ope_vir,
		--indicateurs
		SUM(nb_cb)					AS	nb_cb_sum,
		SUM(nb_pm)					AS	nb_pm_sum,
		SUM(nb_ope_cb)				AS	nb_ope_cb_sum,
		SUM(nb_ope_pm)				AS	nb_ope_pm_sum,
		SUM(nb_ope_prel)			AS	nb_ope_prel_sum,
		SUM(nb_ope_vir)				AS	nb_ope_vir_sum,
		COUNT(DISTINCT NUM_CPT_CLI)	AS	nb_comptes,
		SUM(encours_cred)			AS	encours_cred,
		SUM(encours_deb)			AS	encours_deb,
		SUM(encours_deb_non_auth)	AS	encours_deb_non_auth,
		SUM(montant_dep)			AS	montant_dep,
		SUM(montant_ret)			AS	montant_ret	
	INTO
		#stock_cpt
	FROM
		dbo.wk_join_sources_usage_cpt WITH(NOLOCK)
	GROUP BY
		date_alim,
		code_sab_type_compte,
		code_fe_type_compte,
		StartedChannel__c,
		DistributionChannel__c,
		DistributorNetwork__c,
		Occupation__pc,
		OccupationNiv1__pc,
		OccupationNiv3__pc,
		AGE_CLI,
		nb_cb, nb_pm,
		nb_ope_cb,
		nb_ope_pm,
		nb_ope_prel,
		nb_ope_vir;
	-- SELECT date_alim, SUM(nb_comptes) FROM #stock_cpt group by date_alim order by date_alim
	--SELECT * FROM #stock_cpt
	/* IV - JOINture avec les dimensions pour avoir les libellées */
	-- supprimer dates à insérer eventuellement déjà présentes
	DECLARE
		@date_min	DATE,
		@date_max	DATE
	SELECT
		@date_min = MIN(CAST(date_alim	AS	DATE)),
		@date_max = MAX(CAST(date_alim	AS	DATE))
	FROM
		dbo.wk_join_sources_usage_cpt WITH(NOLOCK);
	--SELECT @date_min
	--SELECT @date_max
	DELETE FROM
		dbo.sas_vue_usage_cpt
	WHERE
		jour_alim	BETWEEN	@date_min	AND	@date_max;
	INSERT INTO
		dbo.sas_vue_usage_cpt
	SELECT
		t.date_alim,
		d3.StandardDate									AS	jour_alim,
		t.nb_comptes,
		t.encours_cred,
		t.encours_deb,
		t.encours_deb_non_auth,
		t.montant_dep,
		t.montant_ret,
		t.AGE_CLI,
		ISNULL(nb_cb, 0)								AS	nb_cb,
		ISNULL(nb_pm, 0)								AS	nb_pm,
		ISNULL(t.nb_ope_cb, 0)							AS	nb_ope_cb,
		ISNULL(t.nb_ope_pm, 0)							AS	nb_ope_pm,
		ISNULL(t.nb_ope_prel, 0)						AS	nb_ope_prel,
		ISNULL(t.nb_ope_vir, 0)							AS	nb_ope_vir,
		ISNULL(nb_cb_sum, 0)							AS	nb_cb_sum,
		ISNULL(nb_pm_sum, 0)							AS	nb_pm_sum,
		ISNULL(t.nb_ope_cb_sum, 0)						AS	nb_ope_cb_sum,
		ISNULL(t.nb_ope_pm_sum, 0)						AS	nb_ope_pm_sum,
		ISNULL(t.nb_ope_prel_sum, 0)					AS	nb_ope_prel_sum,
		ISNULL(t.nb_ope_vir_sum, 0)						AS	nb_ope_vir_sum,
		ISNULL(d2.CSP_CLI, 'Non renseigné')				AS	CSP_CLI,
		ISNULL(dH1.LIB_CSP_NIV1, 'Non renseigné')		AS	CSP_CLI_NIV1,
		ISNULL(dH3.LIB_CSP_NIV3, 'Non renseigné')		AS	CSP_CLI_NIV3,
		ISNULL(d70.LIBELLE, 'Non renseigné')			AS	canal_interaction,
		ISNULL(dR8.LIBELLE, 'Non renseigné')			AS	reseau,
		ISNULL(t.code_fe_type_compte, 'Non renseigné')	AS	code_fe_type_compte,
		ISNULL(d9.LIBELLE, 'Non renseigné')				AS	type_compte,
		ISNULL(d9o.CODE_FE, 'Non renseigné')			AS	code_fe_offre,
		ISNULL(d9o.LIBELLE, 'Non renseigné')			AS	offre_commerciale, --prendre code offre table source, pas la correspondance donnée par la dimension
		t.code_sab_type_compte
	FROM
		#stock_cpt							AS	t	WITH(NOLOCK)
		LEFT JOIN	dbo.DIM_TEMPS			AS	d3 WITH(NOLOCK)
			ON 	t.date_alim = d3.StandardDate
		LEFT JOIN	dbo.DIM_CSP_CLI			AS	d2  WITH(NOLOCK)
			ON	t.Occupation__pc = d2.CSP_CLI
				AND t.date_alim BETWEEN d2.Validity_StartDate AND d2.Validity_EndDate
		LEFT JOIN	dbo.DIM_CSP_NIV1		AS	dH1  WITH(NOLOCK)
			ON	t.OccupationNiv1__pc = dH1.COD_CSP_NIV1
		LEFT JOIN	dbo.DIM_CSP_NIV3		AS	dH3  WITH(NOLOCK)
			on	t.OccupationNiv3__pc = dH3.COD_CSP_NIV3
		LEFT JOIN	dbo.DIM_CANAL d70 WITH(NOLOCK)
			on	t.StartedChannel__c = d70.CODE_SF
				AND	d70.PARENT_ID IS NOT NULL  --niveau canal interaction
				AND	t.date_alim	BETWEEN	d70.Validity_StartDate	AND	d70.Validity_EndDate
		LEFT JOIN	dbo.DIM_RESEAU			AS	dR8 WITH(NOLOCK)
			on	t.DistributorNetwork__c = dR8.CODE_SF
				AND	dR8.PARENT_ID IS NULL --niveau réseau
				AND	t.date_alim	BETWEEN	dR8.Validity_StartDate	AND	dR8.Validity_EndDate
		LEFT JOIN	dbo.DIM_PRODUIT_OFFRE	AS	d9 WITH(NOLOCK)
			on	t.code_fe_type_compte = d9.CODE_FE
				AND d9.PARENT_ID IS NOT NULL-- niveau produit commercial
				AND t.date_alim BETWEEN d9.Validity_StartDate AND d9.Validity_EndDate
		LEFT JOIN	dbo.DIM_PRODUIT_OFFRE	AS	d9o WITH(NOLOCK)
			on	d9.PARENT_ID = d9o.CODE_FE
				AND d9o.PARENT_ID IS NULL -- niveau offre commerciale
				AND t.date_alim BETWEEN d9.Validity_StartDate AND d9.Validity_EndDate;
	--SELECT * FROM sas_vue_usage_cpt;
	-- SELECT date_alim, SUM(nb_comptes) FROM sas_vue_usage_cpt group by date_alim order by date_alim
 	select @nbRows = @@ROWCOUNT;

	EXEC [dbo].[PROC_SSRS_USAGE_CPT];
END
GO