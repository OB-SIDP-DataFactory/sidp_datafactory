﻿CREATE PROCEDURE [dbo].[PKGCP_PROC_CARE_H]
@Date_obs date

AS BEGIN

/*
declare @Date_obs date; 
set @Date_obs='2018-05-15'
;*/
SET @Date_obs = CAST(@Date_obs as date);
SET DATEFIRST 1; -->1    Lundi

With 
cte_date_obs AS
( SELECT @Date_obs AS StandardDate
       , DATEADD(day, (-1 * DATEPART(dw, @Date_obs)) + 1, @Date_obs) AS FirstDOW
       , DATEADD(day, (7 - DATEPART(dw, @Date_obs)), @Date_obs) AS LastDOW
	   , DATEADD(day, (-1 * DATEPART(dw, @Date_obs)) -6, @Date_obs) AS FirstDOPW
	   , DATEADD(day, (7 - DATEPART(dw, @Date_obs)) -7, @Date_obs) AS LastDOPW
)
, cte_jour_alim_list AS
( 	SELECT T1.StandardDate AS jour_alim
    FROM dbo.[DIM_TEMPS] T1
    WHERE T1.StandardDate > CAST(DATEADD(day, -7, @Date_obs) AS DATE) AND T1.StandardDate <= @Date_obs --7 jours glissants 
)
--***** Toutes les requetes *****--
, cte_Dde_ALL AS
( SELECT [ID]
	  ,[IDE_REQ_SF]
      ,[IDE_PERS_SF]
      ,[DTE_AR]
      ,[NUM_REQ]
      ,cast([DTE_FER_REQ] as DATE) as DTE_FER_REQ
      ,[MTT_GESTE_CO]
      ,cast([DTE_CREA] as DATE) as DTE_CREA
      ,[DESC_REQ]
      ,[IDE_REQ_ORIG]
      ,CASE WHEN [LIB_CAN_INT_ORI] LIKE 'Appel%' then '04' ELSE [CAN_INT_ORI] END as CANAL
	  ,CASE WHEN [LIB_CAN_INT_ORI] LIKE 'Appel%' then 'Téléphonie' ELSE [LIB_CAN_INT_ORI] END as LIB_CANAL
      ,[IDE_TYP_ENR]
      ,[COD_TYP_ENR]
      ,[LIB_TYP_ENR]
	  ,[TYP_REQ]
      ,[LIB_TYP_REQ]
      ,cast([DTE_DEBUT_VAL] as DATE) as [DTE_DEBUT_VAL]
      ,cast([DTE_FIN_VAL] as DATE) as [DTE_FIN_VAL]
      ,[DTE_DEBUT_TRN]
      ,[DTE_FIN_TRN] 
	FROM [$(DataHubDatabaseName)].[exposition].[PV_SF_CASE] 
	where [COD_TYP_ENR]= 'OBRequest'				-- <> 'OBMarketingRequest' 
	and ([CAN_INT_ORI] in ('07','11','12') or [LIB_CAN_INT_ORI] LIKE 'Appel%') --tous les types Appel, Chat, Email et Formulaire de demandes
	--and [TYP_REQ] in ('01','02','03','04','05','08','10','11','12','13','14','15','16','30','31','40')
)
, lst_Typ_Req AS
( SELECT distinct TYP_REQ, LIB_TYP_REQ FROM cte_Dde_ALL
)
--select * from  lst_Typ_Req;
--Requête Demande restant à traiter sur les 7 jours glissants précédent la journée d'observation
, cte_Dde_a_traiter_Cumul_Jour AS
( SELECT x.jour_alim
		, CANAL
		, LIB_CANAL
		, l.LIB_TYP_REQ
		, SUM(CASE WHEN (c.DTE_FER_REQ IS NULL OR c.DTE_FER_REQ > x.jour_alim) AND c.TYP_REQ = l.TYP_REQ THEN 1 ELSE 0 END ) as NB
	FROM cte_jour_alim_list x
	LEFT JOIN lst_typ_req l on 1=1
	LEFT JOIN cte_Dde_ALL c on c.DTE_CREA <= x.jour_alim 
		AND c.DTE_DEBUT_VAL <= x.jour_alim and DTE_FIN_VAL > x.jour_alim -- Pour ne selectionner que la ligne d'enreg valide durant la période d'observation
	group by x.jour_alim, CANAL, LIB_CANAL, l.LIB_TYP_REQ
)
-- select * from cte_Dde_a_traiter_Cumul_Jour ;
-- Requête calcul Délai traitement demande
, cte_Delai_traitmt_Dde AS
( SELECT distinct x.jour_alim,
	c.CANAL,
	c.LIB_CANAL,
	PERCENTILE_CONT (0.5)  WITHIN GROUP (ORDER BY DATEDIFF(DD, c.DTE_CREA,x.jour_alim) ) OVER (PARTITION BY x.jour_alim, c.CANAL) AS DELAI_MED_TRAITEMENT
	FROM cte_jour_alim_list x
	LEFT JOIN cte_Dde_ALL c on c.DTE_CREA <= x.jour_alim 
		AND c.DTE_DEBUT_VAL <= x.jour_alim and DTE_FIN_VAL > x.jour_alim		-- Pour ne selectionner que la ligne d'enreg valide durant la période d'observation		
	WHERE c.DTE_FER_REQ IS NULL OR c.DTE_FER_REQ > x.jour_alim					-- Demande en cours
)

select t1.jour_alim,
		t1.CANAL,
		t1.LIB_CANAL,
		t1.LIB_TYP_REQ,
		ISNULL(t1.NB,0) as NB
from cte_Dde_a_traiter_Cumul_Jour t1 

UNION

select t3.jour_alim, 
	   t3.CANAL as CANAL, 
	   t3.LIB_CANAL as LIB_CANAL,
	   'Delai' as LIB_TYP_REQ,
	   ISNULL(DELAI_MED_TRAITEMENT,0) as NB
FROM cte_Delai_traitmt_Dde t3 

END;