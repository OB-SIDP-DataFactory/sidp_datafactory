﻿CREATE PROCEDURE [dbo].[ENC_ALIM_DIM_MARCHE]
AS
BEGIN 

declare @date_traitement smalldatetime =getdate()

MERGE ENC_DIM_MARCHE               AS target 
using ( select * FROM [$(DataHubDatabaseName)].[dbo].REF_MARCHE WHERE  @date_traitement between date_debut and date_fin ) AS source 
ON TARGET.CODE_MARCHE_N1=SOURCE.CODE_MARCHE_N1
  AND TARGET.CODE_MARCHE_N2=SOURCE.CODE_MARCHE_N2
  AND TARGET.CODE_MARCHE_N3=SOURCE.CODE_MARCHE_N3
  AND TARGET.CODE_RESPONSABLE=SOURCE.CODE_RESPONSABLE
  AND TARGET.FLAG_ACTIF=1
WHEN MATCHED THEN 
UPDATE 
SET TARGET.LIBELLE_MARCHE_N1=SOURCE.LIBELLE_MARCHE_N1,
	TARGET.LIBELLE_MARCHE_N2=SOURCE.LIBELLE_MARCHE_N2,
	TARGET.LIBELLE_MARCHE_N3=SOURCE.LIBELLE_MARCHE_N3,
	TARGET.LIBELLE_MARCHE=SOURCE.LIBELLE_MARCHE,
	TARGET.CODE_RESPONSABLE=SOURCE.CODE_RESPONSABLE,
	TARGET.UPDATE_DATE=GETDATE()

WHEN NOT matched BY TARGET  THEN 
INSERT 
                 ( 
				 CODE_MARCHE_N1,
				 LIBELLE_MARCHE_N1,
				 CODE_MARCHE_N2,
				 LIBELLE_MARCHE_N2,
				 CODE_MARCHE_N3,
				 LIBELLE_MARCHE_N3,
				 CODE_MARCHE,
				 LIBELLE_MARCHE,
				 CODE_RESPONSABLE,
				 INSERT_DATE,
				 FLAG_ACTIF
				 )
				 VALUES 
				 (
				 SOURCE.CODE_MARCHE_N1,
				 SOURCE.LIBELLE_MARCHE_N1,
				 SOURCE.CODE_MARCHE_N2,
				 SOURCE.LIBELLE_MARCHE_N2,
				 SOURCE.CODE_MARCHE_N3,
				 SOURCE.LIBELLE_MARCHE_N3,
				 SOURCE.CODE_MARCHE,
				 SOURCE.LIBELLE_MARCHE,
				 SOURCE.CODE_RESPONSABLE,
				 GETDATE(),
				 1
				 );

--WHEN NOT matched BY source  THEN 
--UPDATE 
--SET    target.flag_actif=0, 
--       target.UPDATE_DATE=getdate();


END 


