﻿
CREATE PROC [dbo].[PKGEC_PROC_ENCOURS_MARCHE] 
   

@Marche_N1 VARCHAR(255)

  
AS BEGIN

WITH
CTE_MARCHE_N1 AS
(
Select [Value] AS MARCHE from dbo.FnSplit(@Marche_N1, ',') 
),

CTE_MARCHE_N2 AS
(
SELECT distinct TM.LIBELLE_MARCHE_N2 AS Marche_N2, TM.LIBELLE_MARCHE_N1 AS Marche_N1
FROM [dbo].[T_FACT_ENCOURS] ENC
LEFT JOIN [dbo].[DIM_TRANSCO_MARCHE] TM
ON TM.CODE_MARCHE = ENC.CD_MARCHE
INNER JOIN CTE_MARCHE_N1 CM1
ON CM1.MARCHE = TM.LIBELLE_MARCHE_N1
where TM.LIBELLE_MARCHE_N2  not in ('Trésorerie' ,'Hors marché')

)

SELECT *
FROM CTE_MARCHE_N2

END;