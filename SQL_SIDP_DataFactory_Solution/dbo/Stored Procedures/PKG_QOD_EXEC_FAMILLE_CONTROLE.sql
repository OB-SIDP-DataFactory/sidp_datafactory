﻿CREATE PROCEDURE [dbo].[PKG_QOD_EXEC_FAMILLE_CONTROLE]
	@FAMILLE_CONTROLE as nvarchar(max),
	@ID_CONTROLE as integer=NULL
as

 --Curseur sur les contrôles de la famille
 DECLARE Control_cursor CURSOR for
 select ID_CONTROL from SIDP_QOD_LISTE_CONTROLE
 where ISNULL(FAMILLE_CONTROL, '') = @FAMILLE_CONTROLE 
	AND ISNULL(FLG_ACTIF,0)=1

 --Boucle sur les contrôle de la famille
OPEN Control_cursor
FETCH NEXT FROM Control_cursor INTO @ID_CONTROLE
WHILE @@FETCH_STATUS = 0
BEGIN 
 
	--Exécution du contrôle
	exec PKG_QOD_EXEC_CONTROLE @ID_CONTROL=@ID_CONTROLE;
 
FIN_BOUCLE:

FETCH NEXT FROM Control_cursor INTO @ID_CONTROLE
END
--fin de la procedure
