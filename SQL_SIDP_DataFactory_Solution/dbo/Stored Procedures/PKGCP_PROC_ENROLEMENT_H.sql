﻿

CREATE PROCEDURE [dbo].[PKGCP_PROC_ENROLEMENT_H]
@Date_obs date

AS BEGIN

/*declare @Date_obs date; 
set @Date_obs='2017-06-21';*/

DECLARE @weekobs int;
DECLARE @Anneeweekobs int ;
DECLARE @weekPrecobs int;
DECLARE @AnneeweekPrecobs int ;
DECLARE @LastDOLWobs date;

SET DATEFIRST 1; -->1    Lundi
SET @Date_obs = CAST(@Date_obs as date);
-- Num de la Semaine en cours et année de la semaine en cours
SET @weekobs = (select IsoWeek from [dbo].[DIM_TEMPS] where CAST(Date as date) = @Date_obs);
SET @Anneeweekobs = (select DATEPART(YEAR,@Date_obs));

-- Num de la Semaine précédente et année de la semaine précédente
SET @weekPrecobs = (select IsoWeek from [dbo].[DIM_TEMPS] where CAST(Date as date) = DATEADD(week,-1,@Date_obs));
SET @AnneeweekPrecobs = DATEPART(YEAR,DATEADD(week,-1,@Date_obs)) ;

--Dimanche de la semaine précédente
SET @LastDOLWobs = DATEADD(day, (7 - DATEPART(dw, CAST(DATEADD(WEEK,-1,@Date_Obs) AS DATE))), CAST(DATEADD(WEEK,-1,@Date_Obs) AS DATE)) ;
With 
cte_jour_alim_list AS
( 	SELECT T1.StandardDate AS jour_alim
    FROM [dbo].[DIM_TEMPS] T1
    WHERE T1.StandardDate > CAST(DATEADD(day, -8, @Date_obs) AS DATE) AND T1.StandardDate <= @Date_obs --8 jours glissants pour avoir le dimanche S-1
)

--Calcul du nombre de passage en dossier incomplet pour chaque opportunité 
,Cte_Opp_incomplet as (
select OpportunityId, count(NewValue) as NB_ITERATION
from [$(DataHubDatabaseName)].dbo.WK_SF_OPPORTUNITYFIELDHISTORY 
where Field='StageName'
and NewValue='06'
group by OpportunityId
) 

,cte_Opp_ALL as (
SELECT CAST(date_alim as date) as DTE_ALIM
	  ,[Id_SF] as IDE_OPPRT_SF
	  ,[StageName] as STADE_VENTE
	  ,sdv.LIBELLE as LIB_STADE_VENTE
	  ,CAST([CreatedDate] as date) as DTE_CREA
	  ,CAST([CloseDate] as date ) as DTE_CLO_OPPRT
	  ,DATEDIFF(DD, [CreatedDate], [CloseDate]) as DELAI_GAGNEES
	  ,CAST(last_update_date as date) as DTE_DER_MOD
	  ,[age_opport] as AGE_OPPRT
	  ,DATEPART(ISO_WEEK,[CreatedDate]) as WeekOfYear
	  ,[DistributorNetwork__c] as COD_RES_DIS
	  ,res_dis.LIB_RES_DIS as LIB_RES_DIS
	  ,[StartedChannel__c] as COD_CAN_INT_ORI
	  ,can_int_ori.LIB_CAN_INT as LIB_CAN_INT_ORI
	  ,[DistributorEntity__c] as ENT_DIS
	  ,ent_dis.LIB_CAN_INT as LIB_ENT_DIS 
	  ,[LeadSource] as COD_CAN_INT
	  ,can_int.LIB_CAN_INT as LIB_CAN_INT
	  ,[DistributionChannel__c] as COD_CAN_DIS
	  ,can_dis.LIB_CAN_DIS as LIB_CAN_DIS
	  ,[date_alim]
	  ,[flag_indication] as FLG_IND
	  ,[flag_full_digi]
      ,[flag_full_btq]
	  ,ISNULL(incplet.NB_ITERATION,0) as NB_ITERATION 
FROM [dbo].[WK_STOCK_OPPORTUNITY] opp
LEFT JOIN [dbo].[DIM_STADE_VENTE] sdv --Rajouter la nouvelle DIM_STADE_VTE lorsque livraison R2 crédit et nouvelles dim
ON opp.StageName = sdv.CODE_SF and sdv.PARENT_ID is null
LEFT JOIN [dbo].[DIM_RES_DIS] res_dis
ON opp.DistributorNetwork__c = res_dis.COD_RES_DIS
LEFT JOIN [dbo].[DIM_CANAL_INT] can_int_ori
ON opp.StartedChannel__c = can_int_ori.COD_CAN_INT
LEFT JOIN [dbo].[DIM_CANAL_INT] ent_dis
ON opp.DistributorEntity__c = ent_dis.COD_CAN_INT
LEFT JOIN [dbo].[DIM_CANAL_INT] can_int
ON opp.LeadSource = can_int.COD_CAN_INT
LEFT JOIN [dbo].[DIM_CANAL_DIS] can_dis
ON opp.DistributionChannel__c = can_dis.COD_CAN_DIS
LEFT JOIN Cte_Opp_incomplet incplet
on opp.Id_SF = incplet.OpportunityId
where 
--Filtrer la date de création sur 90 + 7 jours
CAST(DATEADD(day, -98, @Date_obs) AS DATE)  < [CreatedDate] and [CreatedDate] <= @date_obs
and flag_opport_last_state = 1
and CommercialOfferCode__c='OC80' --Type CAV
)
--select * from  cte_Opp_ALL ;

/*Les jours de la semaine en cours et ceux S-1*/
--Opport CAV creees
,Cte_Opp_Creees_Cumul_Jour_cnt as (
SELECT [DTE_CREA],
       [WeekOfYear],
	   case when ([COD_RES_DIS] in ('01','02') or [COD_CAN_INT_ORI] IN ('10','11','12','13','14','15') or [ENT_DIS] in ('01','02','04') or [FLG_IND] in ('Non','Oui')) then COUNT([IDE_OPPRT_SF]) end as NB_OPP_CREEES,
	   case when [COD_RES_DIS] = '02' then COUNT ([IDE_OPPRT_SF]) end as NB_CREEES_OB,
       case when ([COD_RES_DIS] = '02' and [COD_CAN_INT_ORI] IN ('10','11')) then COUNT ([IDE_OPPRT_SF])  end as NB_CREEES_OB_Dig,
	   case when ([COD_RES_DIS] = '02' and [COD_CAN_INT_ORI] IN ('12','13','14','15')) then COUNT ([IDE_OPPRT_SF])  end as NB_CREEES_OB_CRC,
	   case when ([COD_RES_DIS] = '01') then COUNT ([IDE_OPPRT_SF])  end as NB_CREEES_OF,
	   case when ([COD_RES_DIS] = '01' and [ENT_DIS]='04') then COUNT ([IDE_OPPRT_SF])  end as NB_CREEES_OF_Dig,
	   case when ([COD_RES_DIS] = '01' and [ENT_DIS] in ('01','02')) then COUNT ([IDE_OPPRT_SF])  end as NB_CREEES_OF_Btq,
	   case when ([COD_RES_DIS] = '01' and [FLG_IND] = 'Non') then COUNT ([IDE_OPPRT_SF])  end as NB_CREEES_OF_SS,	   
	   case when ([COD_RES_DIS] = '01' and [FLG_IND] = 'Non' and [ENT_DIS] = '04') then COUNT ([IDE_OPPRT_SF])  end as NB_CREEES_OF_SS_Dig,
	   case when ([COD_RES_DIS] = '01' and [FLG_IND] = 'Non' and [ENT_DIS] in ('01','02')) then COUNT ([IDE_OPPRT_SF])  end as NB_CREEES_OF_SS_Btq,
	   case when ([COD_RES_DIS] = '01' and [FLG_IND] = 'Oui') then COUNT ([IDE_OPPRT_SF])  end as NB_CREEES_OF_AC,
	   case when ([COD_RES_DIS] = '01' and [FLG_IND] = 'Oui' and [ENT_DIS] = '04') then COUNT ([IDE_OPPRT_SF])  end as NB_CREEES_OF_AC_Dig,
	   case when ([COD_RES_DIS] = '01' and [FLG_IND] = 'Oui' and [ENT_DIS] in ('01','02')) then COUNT ([IDE_OPPRT_SF])  end as NB_CREEES_OF_AC_Btq
FROM cte_Opp_ALL
WHERE @Date_obs = DTE_ALIM --Filtre pour supprimer les doublons : se placer au stock d'opportunités selon la date d'observation
GROUP BY [DTE_CREA],
		[WeekOfYear],
		[COD_RES_DIS],
		[COD_CAN_INT_ORI],
		[FLG_IND],
		[ENT_DIS]
)

--Opport CAV creees Jour
,Cte_Opp_Creees_Cumul_Jour as (
SELECT [DTE_CREA],
		[WeekOfYear],
		ISNULL(SUM(NB_OPP_CREEES),0) as NB_OPP_CREEES,
		ISNULL(SUM(NB_CREEES_OB),0) as NB_CREEES_OB,
		ISNULL(SUM(NB_CREEES_OB_Dig),0) as NB_CREEES_OB_Dig,
		ISNULL(SUM(NB_CREEES_OB_CRC),0) as NB_CREEES_OB_CRC,
		ISNULL(SUM(NB_CREEES_OF),0) as NB_CREEES_OF,
		ISNULL(SUM(NB_CREEES_OF_Dig),0) as NB_CREEES_OF_Dig,
		ISNULL(SUM(NB_CREEES_OF_Btq),0) as NB_CREEES_OF_Btq,
		ISNULL(SUM(NB_CREEES_OF_SS),0) as NB_CREEES_OF_SS,
		ISNULL(SUM(NB_CREEES_OF_SS_Dig),0) as NB_CREEES_OF_SS_Dig,
		ISNULL(SUM(NB_CREEES_OF_SS_Btq),0) as NB_CREEES_OF_SS_Btq,
		ISNULL(SUM(NB_CREEES_OF_AC),0) as NB_CREEES_OF_AC,
		ISNULL(SUM(NB_CREEES_OF_AC_Dig),0) as NB_CREEES_OF_AC_Dig,
		ISNULL(SUM(NB_CREEES_OF_AC_Btq),0) as NB_CREEES_OF_AC_Btq 
FROM Cte_Opp_Creees_Cumul_Jour_cnt
GROUP BY [DTE_CREA],
		[WeekOfYear]
)

--select * from Cte_Opp_Creees_Cumul_Jour

--Opport CAV en cours de vie
,Cte_Opp_En_cours_Cumul_Jour_cnt as (
SELECT DTE_ALIM,
       COUNT ([IDE_OPPRT_SF]) as NB_OPP_ENCRS,
       case when [COD_RES_DIS]='02' and STADE_VENTE = '02' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OB_DECOUV,
	   case when [COD_RES_DIS]='02' and STADE_VENTE = '03' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OB_ELAB,
	   case when [COD_RES_DIS]='02' and STADE_VENTE = '04' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OB_AFF_SIGN,
	   case when [COD_RES_DIS]='02' and STADE_VENTE = '05' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OB_VERIF,
	   case when [COD_RES_DIS]='02' and STADE_VENTE = '06' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OB_DOSS_INC,
	   case when [COD_RES_DIS]='02' and STADE_VENTE = '07' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OB_DOSS_VAL,
	   case when [COD_RES_DIS]='01' and STADE_VENTE = '01' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OF_DETEC,
	   case when [COD_RES_DIS]='01' and STADE_VENTE = '02' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OF_DECOUV,
	   case when [COD_RES_DIS]='01' and STADE_VENTE = '03' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OF_ELAB,
	   case when [COD_RES_DIS]='01' and STADE_VENTE = '04' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OF_AFF_SIGN,
	   case when [COD_RES_DIS]='01' and STADE_VENTE = '05' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OF_VERIF,
	   case when [COD_RES_DIS]='01' and STADE_VENTE = '06' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OF_DOSS_INC,
	   case when [COD_RES_DIS]='01' and STADE_VENTE = '07' then COUNT ([IDE_OPPRT_SF]) end as NB_ENCRS_OF_DOSS_VAL
FROM cte_Opp_ALL
WHERE CAST(DATEADD(day, -90, @Date_obs) AS DATE)  < DTE_CREA and DTE_CREA <= @date_obs 
and CAST(DATEADD(day, -8, @Date_obs) AS DATE)  < DTE_ALIM and DTE_ALIM <= @date_obs 
AND STADE_VENTE IN ('01','02','03','04','05','06','07')
GROUP BY DTE_ALIM,
		STADE_VENTE,
		[COD_RES_DIS]
)

--Opport CAV en cours de vie Jour
,Cte_Opp_En_cours_Cumul_Jour as (
SELECT DTE_ALIM,
		ISNULL(SUM(NB_OPP_ENCRS),0) as NB_OPP_ENCRS,
		ISNULL(SUM(NB_ENCRS_OB_DECOUV),0) as NB_ENCRS_OB_DECOUV,
		ISNULL(SUM(NB_ENCRS_OB_ELAB),0) as NB_ENCRS_OB_ELAB,
		ISNULL(SUM(NB_ENCRS_OB_AFF_SIGN),0) as NB_ENCRS_OB_AFF_SIGN,
		ISNULL(SUM(NB_ENCRS_OB_VERIF),0) as NB_ENCRS_OB_VERIF,
		ISNULL(SUM(NB_ENCRS_OB_DOSS_INC),0) as NB_ENCRS_OB_DOSS_INC,
		ISNULL(SUM(NB_ENCRS_OB_DOSS_VAL),0) as NB_ENCRS_OB_DOSS_VAL,
		ISNULL(SUM(NB_ENCRS_OF_DETEC),0) as NB_ENCRS_OF_DETEC,
		ISNULL(SUM(NB_ENCRS_OF_DECOUV),0) as NB_ENCRS_OF_DECOUV,
		ISNULL(SUM(NB_ENCRS_OF_ELAB),0) as NB_ENCRS_OF_ELAB,
		ISNULL(SUM(NB_ENCRS_OF_AFF_SIGN),0) as NB_ENCRS_OF_AFF_SIGN,
		ISNULL(SUM(NB_ENCRS_OF_VERIF),0) as NB_ENCRS_OF_VERIF,
		ISNULL(SUM(NB_ENCRS_OF_DOSS_INC),0) as NB_ENCRS_OF_DOSS_INC,
		ISNULL(SUM(NB_ENCRS_OF_DOSS_VAL),0) as NB_ENCRS_OF_DOSS_VAL
FROM Cte_Opp_En_cours_Cumul_Jour_cnt
GROUP BY DTE_ALIM
)

--Opport CAV gagnees
,Cte_Opp_Gagnees_Cumul_Jour_Cnt as (
SELECT DTE_ALIM,
	   DELAI_GAGNEES,
       COUNT ([IDE_OPPRT_SF]) as NB_OPP_GAGNEES,
       case when [COD_RES_DIS] = '02' then COUNT ([IDE_OPPRT_SF]) end as NB_GAGNEES_OB,
	   case when ([COD_RES_DIS] = '02' and [COD_CAN_INT_ORI] IN ('10','11')) then COUNT ([IDE_OPPRT_SF]) end as NB_GAGNEES_OB_Dig,
	   case when ([COD_RES_DIS] = '02' and [COD_CAN_INT_ORI] IN ('12','13','14','15')) then COUNT ([IDE_OPPRT_SF]) end as NB_GAGNEES_OB_CRC,
	   case when [COD_RES_DIS] = '01' then COUNT ([IDE_OPPRT_SF]) end as NB_GAGNEES_OF,
	   case when ([COD_RES_DIS] = '01' and [ENT_DIS] = '04') then COUNT ([IDE_OPPRT_SF])  end as NB_GAGNEES_OF_Dig,
	   case when ([COD_RES_DIS] = '01' and [ENT_DIS] in ('01','02')) then COUNT ([IDE_OPPRT_SF])  end as NB_GAGNEES_OF_Btq,
	   case when ([COD_RES_DIS] = '01' and [FLG_IND] = 'Non' and [ENT_DIS] = '04') then COUNT ([IDE_OPPRT_SF])  end as NB_GAGNEES_OF_SS_Dig,
	   case when ([COD_RES_DIS] = '01' and [FLG_IND] = 'Non' and [ENT_DIS] in ('01','02')) then COUNT ([IDE_OPPRT_SF])  end as NB_GAGNEES_OF_SS_Btq,
	   case when ([COD_RES_DIS] = '01' and [FLG_IND] = 'Oui' and [ENT_DIS] = '04') then COUNT ([IDE_OPPRT_SF])  end as NB_GAGNEES_OF_AC_Dig,
	   case when ([COD_RES_DIS] = '01' and [FLG_IND] = 'Oui' and [ENT_DIS] in ('01','02')) then COUNT ([IDE_OPPRT_SF])  end as NB_GAGNEES_OF_AC_Btq,
	   case when [COD_RES_DIS] = '03' then COUNT ([IDE_OPPRT_SF]) end as NB_GAGNEES_GP,
	   case when ([COD_RES_DIS] = '03' and [FLG_IND] = 'Oui') then COUNT ([IDE_OPPRT_SF])  end as NB_GAGNEES_GP_AC,
	   case when ([COD_RES_DIS] = '03' and [FLG_IND] = 'Non') then COUNT ([IDE_OPPRT_SF])  end as NB_GAGNEES_GP_SS ,
	   --Calcul du délai médian de transformation
		case when [COD_RES_DIS] = '02' then PERCENTILE_CONT (0.5)  WITHIN GROUP (ORDER BY DELAI_GAGNEES) OVER (PARTITION BY COD_RES_DIS, DTE_ALIM) end AS DELAI_GAGNEES_OB, 
		case when ([COD_RES_DIS] = '02' and [COD_CAN_INT_ORI] IN ('10','11')) then PERCENTILE_CONT (0.5)  WITHIN GROUP (ORDER BY DELAI_GAGNEES) OVER (PARTITION BY COD_RES_DIS, DTE_ALIM) end AS DELAI_GAGNEES_OB_Dig, 
		case when ([COD_RES_DIS] = '02' and [COD_CAN_INT_ORI] IN ('12','13','14','15')) then PERCENTILE_CONT (0.5)  WITHIN GROUP (ORDER BY DELAI_GAGNEES) OVER (PARTITION BY COD_RES_DIS, DTE_ALIM) end AS DELAI_GAGNEES_OB_CRC, 
		case when [COD_RES_DIS] = '01' then PERCENTILE_CONT (0.5)  WITHIN GROUP (ORDER BY DELAI_GAGNEES) OVER (PARTITION BY COD_RES_DIS, DTE_ALIM) end AS DELAI_GAGNEES_OF, 
		case when ([COD_RES_DIS] = '01' and [ENT_DIS] = '04') then PERCENTILE_CONT (0.5)  WITHIN GROUP (ORDER BY DELAI_GAGNEES) OVER (PARTITION BY COD_RES_DIS, DTE_ALIM) end AS DELAI_GAGNEES_OF_Dig,   
		case when ([COD_RES_DIS] = '01' and [ENT_DIS] in ('01','02'))  then PERCENTILE_CONT (0.5)  WITHIN GROUP (ORDER BY DELAI_GAGNEES) OVER (PARTITION BY COD_RES_DIS, DTE_ALIM) end AS DELAI_GAGNEES_OF_Btq, 
		--Calcul du nb d'iterations
		case when [COD_RES_DIS] = '02' then COUNT(NB_ITERATION) end AS NB_ITERATION_OB, 
		case when [COD_RES_DIS] = '02' and NB_ITERATION = 0 then COUNT(NB_ITERATION) end AS NB_ITERATION_OB_0, 
		case when [COD_RES_DIS] = '02' and NB_ITERATION IN (1,2) then COUNT(NB_ITERATION) end AS NB_ITERATION_OB_1_2, 
		case when [COD_RES_DIS] = '02' and NB_ITERATION > 2  then COUNT(NB_ITERATION) end AS NB_ITERATION_OB_plus_2, 
		case when [COD_RES_DIS] = '01' then COUNT(NB_ITERATION) end AS NB_ITERATION_OF, 
		case when [COD_RES_DIS] = '01' and NB_ITERATION = 0 then COUNT(NB_ITERATION) end AS NB_ITERATION_OF_0, 
		case when [COD_RES_DIS] = '01' and NB_ITERATION IN (1,2) then COUNT(NB_ITERATION) end AS NB_ITERATION_OF_1_2, 
		case when [COD_RES_DIS] = '01' and NB_ITERATION > 2  then COUNT(NB_ITERATION) end AS NB_ITERATION_OF_plus_2	  
FROM cte_Opp_ALL
WHERE CAST(DATEADD(day, -90, @Date_obs) AS DATE) < DTE_CREA and DTE_CREA <= @date_obs 
and CAST(DATEADD(day, -8, @Date_obs) AS DATE) < DTE_ALIM and DTE_ALIM <= @date_obs 
AND STADE_VENTE = '09'
GROUP BY DTE_ALIM,
		COD_RES_DIS,
		COD_CAN_INT_ORI,
		[FLG_IND], 
		[ENT_DIS], 
		DELAI_GAGNEES, 
		NB_ITERATION
)

--Opport CAV gagnees Jour
,Cte_Opp_Gagnees_Cumul_Jour as (
SELECT DTE_ALIM,
		ISNULL(SUM(NB_OPP_GAGNEES),0) as NB_OPP_GAGNEES,
		ISNULL(SUM(NB_GAGNEES_OB),0) as NB_GAGNEES_OB ,
		ISNULL(SUM(NB_GAGNEES_OB_Dig),0) as NB_GAGNEES_OB_Dig,
		ISNULL(SUM(NB_GAGNEES_OB_CRC),0) as NB_GAGNEES_OB_CRC,
		ISNULL(SUM(NB_GAGNEES_OF),0) as NB_GAGNEES_OF,
		ISNULL(SUM(NB_GAGNEES_OF_Dig),0) as NB_GAGNEES_OF_Dig,
		ISNULL(SUM(NB_GAGNEES_OF_Btq),0) as NB_GAGNEES_OF_Btq,
		ISNULL(SUM(NB_GAGNEES_OF_SS_Dig),0) as NB_GAGNEES_OF_SS_Dig,
		ISNULL(SUM(NB_GAGNEES_OF_SS_Btq),0) as NB_GAGNEES_OF_SS_Btq,
		ISNULL(SUM(NB_GAGNEES_OF_AC_Dig),0) as NB_GAGNEES_OF_AC_Dig,
		ISNULL(SUM(NB_GAGNEES_OF_AC_Btq),0) as NB_GAGNEES_OF_AC_Btq ,
		ISNULL(SUM(NB_GAGNEES_GP),0) as NB_GAGNEES_GP,
		ISNULL(SUM(NB_GAGNEES_GP_AC),0) as NB_GAGNEES_GP_AC,
		ISNULL(SUM(NB_GAGNEES_GP_SS),0) as NB_GAGNEES_GP_SS, 
		AVG(DELAI_GAGNEES_OB) as DELAI_GAGNEES_OB, 
		AVG(DELAI_GAGNEES_OB_Dig) as DELAI_GAGNEES_OB_Dig, 
		AVG(DELAI_GAGNEES_OB_CRC) as DELAI_GAGNEES_OB_CRC, 
		AVG(DELAI_GAGNEES_OF) as DELAI_GAGNEES_OF, 
		AVG(DELAI_GAGNEES_OF_Dig) as DELAI_GAGNEES_OF_Dig, 
		AVG(DELAI_GAGNEES_OF_Btq) as DELAI_GAGNEES_OF_Btq, 
		SUM(NB_ITERATION_OB) as NB_ITERATION_OB, 
		SUM(NB_ITERATION_OB_0) as NB_ITERATION_OB_0, 
		SUM(NB_ITERATION_OB_1_2) as NB_ITERATION_OB_1_2, 
		SUM(NB_ITERATION_OB_plus_2) as NB_ITERATION_OB_plus_2, 
		SUM(NB_ITERATION_OF) as NB_ITERATION_OF, 
		SUM(NB_ITERATION_OF_0) as NB_ITERATION_OF_0, 
		SUM(NB_ITERATION_OF_1_2) as NB_ITERATION_OF_1_2, 
		SUM(NB_ITERATION_OF_plus_2) as NB_ITERATION_OF_plus_2
FROM Cte_Opp_Gagnees_Cumul_Jour_Cnt
GROUP BY DTE_ALIM
)

--Opport CAV perdues
,Cte_Opp_Perdues_Cumul_Jour_Cnt as (
SELECT DTE_ALIM,
       COUNT ([IDE_OPPRT_SF]) as NB_OPP_PERDUES,
       case when (COD_RES_DIS = '02' and STADE_VENTE = '08') then COUNT ([IDE_OPPRT_SF])  end as NB_PERDUES_OB_INC_TECH,
	   case when (COD_RES_DIS = '02' and STADE_VENTE = '11') then COUNT ([IDE_OPPRT_SF])  end as NB_PERDUES_OB_SANS_SUITE,
	   case when (COD_RES_DIS = '02' and STADE_VENTE = '10') then COUNT ([IDE_OPPRT_SF])  end as NB_PERDUES_OB_AFF_REF,
	   case when (COD_RES_DIS = '02' and STADE_VENTE = '12') then COUNT ([IDE_OPPRT_SF])  end as NB_PERDUES_OB_RETRACT,
	   case when (COD_RES_DIS = '01' and STADE_VENTE = '08') then COUNT ([IDE_OPPRT_SF])  end as NB_PERDUES_OF_INC_TECH,
	   case when (COD_RES_DIS = '01' and STADE_VENTE = '11') then COUNT ([IDE_OPPRT_SF])  end as NB_PERDUES_OF_SANS_SUITE,
	   case when (COD_RES_DIS = '01' and STADE_VENTE = '10') then COUNT ([IDE_OPPRT_SF])  end as NB_PERDUES_OF_AFF_REF,
	   case when (COD_RES_DIS = '01' and STADE_VENTE = '12') then COUNT ([IDE_OPPRT_SF])  end as NB_PERDUES_OF_RETRACT
FROM cte_Opp_ALL
WHERE CAST(DATEADD(day, -90, @Date_obs) AS DATE) < DTE_CREA and DTE_CREA <= @date_obs 
and CAST(DATEADD(day, -8, @Date_obs) AS DATE) < DTE_ALIM and DTE_ALIM <= @date_obs 
AND STADE_VENTE IN ('08','11','10','12')
GROUP BY DTE_ALIM,
		COD_RES_DIS,
		STADE_VENTE
)

--Opport CAV perdues jour
,Cte_Opp_Perdues_Cumul_Jour as (
SELECT DTE_ALIM,
		ISNULL(SUM(NB_OPP_PERDUES),0) as NB_OPP_PERDUES,
		ISNULL(SUM(NB_PERDUES_OB_INC_TECH),0) as NB_PERDUES_OB_INC_TECH,
		ISNULL(SUM(NB_PERDUES_OB_SANS_SUITE),0) as NB_PERDUES_OB_SANS_SUITE,
		ISNULL(SUM(NB_PERDUES_OB_AFF_REF),0) as NB_PERDUES_OB_AFF_REF,
		ISNULL(SUM(NB_PERDUES_OB_RETRACT),0) as NB_PERDUES_OB_RETRACT,
		ISNULL(SUM(NB_PERDUES_OF_INC_TECH),0) as NB_PERDUES_OF_INC_TECH,
		ISNULL(SUM(NB_PERDUES_OF_SANS_SUITE),0) as NB_PERDUES_OF_SANS_SUITE,
		ISNULL(SUM(NB_PERDUES_OF_AFF_REF),0) as NB_PERDUES_OF_AFF_REF,
		ISNULL(SUM(NB_PERDUES_OF_RETRACT),0) as NB_PERDUES_OF_RETRACT 
FROM Cte_Opp_Perdues_Cumul_Jour_Cnt
GROUP BY DTE_ALIM
)

--Opport CAV créées final
,Cte_Opp_Creees_jour_final as (
select	x.jour_alim,
		--Opport CAV creees Jour
		ISNULL(SUM(c.NB_OPP_CREEES),0)			as NB_OPP_CREEES, 
		ISNULL(SUM(c.NB_CREEES_OB),0)			as NB_CREEES_OB,
		ISNULL(SUM(c.NB_CREEES_OB_Dig),0)		as NB_CREEES_OB_Dig,
		ISNULL(SUM(c.NB_CREEES_OB_CRC),0)		as NB_CREEES_OB_CRC,
		ISNULL(SUM(c.NB_CREEES_OF),0)			as NB_CREEES_OF,
		ISNULL(SUM(c.NB_CREEES_OF_Dig),0)		as NB_CREEES_OF_Dig,
		ISNULL(SUM(c.NB_CREEES_OF_Btq),0)		as NB_CREEES_OF_Btq,
		ISNULL(SUM(c.NB_CREEES_OF_SS),0)		as NB_CREEES_OF_SS,
		ISNULL(SUM(c.NB_CREEES_OF_SS_Dig),0)	as NB_CREEES_OF_SS_Dig,
		ISNULL(SUM(c.NB_CREEES_OF_SS_Btq),0)	as NB_CREEES_OF_SS_Btq, 
		ISNULL(SUM(c.NB_CREEES_OF_AC),0)		as NB_CREEES_OF_AC,
		ISNULL(SUM(c.NB_CREEES_OF_AC_Dig),0)	as NB_CREEES_OF_AC_Dig,
		ISNULL(SUM(c.NB_CREEES_OF_AC_Btq),0)	as NB_CREEES_OF_AC_Btq
from cte_jour_alim_list x 
left join Cte_Opp_Creees_Cumul_Jour c on CAST(DATEADD(day, -90, x.jour_alim) AS DATE) < c.DTE_CREA and c.DTE_CREA <= x.jour_alim
GROUP BY x.jour_alim
)

,Cte_Opp_Creees_hebdo_final as (
select	
	--Opport CAV creees Hebdo
		ISNULL(SUM(ch.NB_OPP_CREEES),0)			as NB_OPP_CREEES_HEBDO, 
		ISNULL(SUM(ch.NB_CREEES_OB),0)			as NB_CREEES_OB_HEBDO,
		ISNULL(SUM(ch.NB_CREEES_OB_Dig),0)		as NB_CREEES_OB_Dig_HEBDO,
		ISNULL(SUM(ch.NB_CREEES_OB_CRC),0)		as NB_CREEES_OB_CRC_HEBDO,
		ISNULL(SUM(ch.NB_CREEES_OF),0)			as NB_CREEES_OF_HEBDO,
		ISNULL(SUM(ch.NB_CREEES_OF_Dig),0)		as NB_CREEES_OF_Dig_HEBDO,
		ISNULL(SUM(ch.NB_CREEES_OF_Btq),0)		as NB_CREEES_OF_Btq_HEBDO,
		ISNULL(SUM(ch.NB_CREEES_OF_SS),0)		as NB_CREEES_OF_SS_HEBDO,
		ISNULL(SUM(ch.NB_CREEES_OF_SS_Dig),0)	as NB_CREEES_OF_SS_Dig_HEBDO,
		ISNULL(SUM(ch.NB_CREEES_OF_SS_Btq),0)	as NB_CREEES_OF_SS_Btq_HEBDO, 
		ISNULL(SUM(ch.NB_CREEES_OF_AC),0)		as NB_CREEES_OF_AC_HEBDO,
		ISNULL(SUM(ch.NB_CREEES_OF_AC_Dig),0)	as NB_CREEES_OF_AC_Dig_HEBDO,
		ISNULL(SUM(ch.NB_CREEES_OF_AC_Btq),0)	as NB_CREEES_OF_AC_Btq_HEBDO
from cte_jour_alim_list x 
left join Cte_Opp_Creees_Cumul_Jour ch  on CAST(DATEADD(day, -90, x.jour_alim) AS DATE) < ch.DTE_CREA and ch.DTE_CREA <= x.jour_alim and x.jour_alim = @Date_obs
)

,Cte_Opp_Creees_hebdo_prec_final as (
select	--Opport CAV creees S-1 Hebdo
		ISNULL(SUM(chp.NB_OPP_CREEES),0)		as NB_OPP_CREEES_HEBDO_PREC, 
		ISNULL(SUM(chp.NB_CREEES_OB),0)			as NB_CREEES_OB_HEBDO_PREC,
		ISNULL(SUM(chp.NB_CREEES_OB_Dig),0)		as NB_CREEES_OB_Dig_HEBDO_PREC,
		ISNULL(SUM(chp.NB_CREEES_OB_CRC),0)		as NB_CREEES_OB_CRC_HEBDO_PREC,
		ISNULL(SUM(chp.NB_CREEES_OF),0)			as NB_CREEES_OF_HEBDO_PREC,
		ISNULL(SUM(chp.NB_CREEES_OF_Dig),0)		as NB_CREEES_OF_Dig_HEBDO_PREC,
		ISNULL(SUM(chp.NB_CREEES_OF_Btq),0)		as NB_CREEES_OF_Btq_HEBDO_PREC,
		ISNULL(SUM(chp.NB_CREEES_OF_SS),0)		as NB_CREEES_OF_SS_HEBDO_PREC,
		ISNULL(SUM(chp.NB_CREEES_OF_SS_Dig),0)	as NB_CREEES_OF_SS_Dig_HEBDO_PREC,
		ISNULL(SUM(chp.NB_CREEES_OF_SS_Btq),0)	as NB_CREEES_OF_SS_Btq_HEBDO_PREC, 
		ISNULL(SUM(chp.NB_CREEES_OF_AC),0)		as NB_CREEES_OF_AC_HEBDO_PREC,
		ISNULL(SUM(chp.NB_CREEES_OF_AC_Dig),0)	as NB_CREEES_OF_AC_Dig_HEBDO_PREC,
		ISNULL(SUM(chp.NB_CREEES_OF_AC_Btq),0)	as NB_CREEES_OF_AC_Btq_HEBDO_PREC
from cte_jour_alim_list x 
left join Cte_Opp_Creees_Cumul_Jour chp on CAST(DATEADD(day, -90, x.jour_alim) AS DATE) < chp.DTE_CREA and chp.DTE_CREA <= x.jour_alim and x.jour_alim = @LastDOLWobs
)
--select * from Cte_Opp_Creees_hebdo_prec_final

--Opport CAV en cours de vie final
,Cte_Opp_En_cours_jour_final as (
select	x.jour_alim,
		--Opport CAV en cours de vie Jour
		ISNULL(SUM(v.NB_OPP_ENCRS),0)			as NB_OPP_ENCRS, 
		ISNULL(SUM(v.NB_ENCRS_OB_DECOUV),0)		as NB_ENCRS_OB_DECOUV,
		ISNULL(SUM(v.NB_ENCRS_OB_ELAB),0)		as NB_ENCRS_OB_ELAB,
		ISNULL(SUM(v.NB_ENCRS_OB_AFF_SIGN),0)	as NB_ENCRS_OB_AFF_SIGN,
		ISNULL(SUM(v.NB_ENCRS_OB_VERIF),0)		as NB_ENCRS_OB_VERIF,
		ISNULL(SUM(v.NB_ENCRS_OB_DOSS_INC),0)	as NB_ENCRS_OB_DOSS_INC, 
		ISNULL(SUM(v.NB_ENCRS_OB_DOSS_VAL),0)	as NB_ENCRS_OB_DOSS_VAL,
		ISNULL(SUM(v.NB_ENCRS_OF_DETEC),0)		as NB_ENCRS_OF_DETEC,
		ISNULL(SUM(v.NB_ENCRS_OF_DECOUV),0)		as NB_ENCRS_OF_DECOUV,
		ISNULL(SUM(v.NB_ENCRS_OF_ELAB),0)		as NB_ENCRS_OF_ELAB,
		ISNULL(SUM(v.NB_ENCRS_OF_AFF_SIGN),0)	as NB_ENCRS_OF_AFF_SIGN,
		ISNULL(SUM(v.NB_ENCRS_OF_VERIF),0)		as NB_ENCRS_OF_VERIF, 
		ISNULL(SUM(v.NB_ENCRS_OF_DOSS_INC),0)	as NB_ENCRS_OF_DOSS_INC,
		ISNULL(SUM(v.NB_ENCRS_OF_DOSS_VAL),0)	as NB_ENCRS_OF_DOSS_VAL
from cte_jour_alim_list x 
left join Cte_Opp_En_cours_Cumul_Jour v on v.DTE_ALIM = x.jour_alim
GROUP BY x.jour_alim
)

,Cte_Opp_En_cours_hebdo_final as (
select	--Opport CAV en cours de vie Hebdo
		ISNULL(SUM(vh.NB_OPP_ENCRS),0)			as NB_OPP_ENCRS_HEBDO, 
		ISNULL(SUM(vh.NB_ENCRS_OB_DECOUV),0)	as NB_ENCRS_OB_DECOUV_HEBDO,
		ISNULL(SUM(vh.NB_ENCRS_OB_ELAB),0)		as NB_ENCRS_OB_ELAB_HEBDO,
		ISNULL(SUM(vh.NB_ENCRS_OB_AFF_SIGN),0)	as NB_ENCRS_OB_AFF_SIGN_HEBDO,
		ISNULL(SUM(vh.NB_ENCRS_OB_VERIF),0)		as NB_ENCRS_OB_VERIF_HEBDO,
		ISNULL(SUM(vh.NB_ENCRS_OB_DOSS_INC),0)	as NB_ENCRS_OB_DOSS_INC_HEBDO, 
		ISNULL(SUM(vh.NB_ENCRS_OB_DOSS_VAL),0)	as NB_ENCRS_OB_DOSS_VAL_HEBDO,
		ISNULL(SUM(vh.NB_ENCRS_OF_DETEC),0)		as NB_ENCRS_OF_DETEC_HEBDO,
		ISNULL(SUM(vh.NB_ENCRS_OF_DECOUV),0)	as NB_ENCRS_OF_DECOUV_HEBDO,
		ISNULL(SUM(vh.NB_ENCRS_OF_ELAB),0)		as NB_ENCRS_OF_ELAB_HEBDO,
		ISNULL(SUM(vh.NB_ENCRS_OF_AFF_SIGN),0)	as NB_ENCRS_OF_AFF_SIGN_HEBDO,
		ISNULL(SUM(vh.NB_ENCRS_OF_VERIF),0)		as NB_ENCRS_OF_VERIF_HEBDO, 
		ISNULL(SUM(vh.NB_ENCRS_OF_DOSS_INC),0)	as NB_ENCRS_OF_DOSS_INC_HEBDO,
		ISNULL(SUM(vh.NB_ENCRS_OF_DOSS_VAL),0)	as NB_ENCRS_OF_DOSS_VAL_HEBDO
from cte_jour_alim_list x 
left join Cte_Opp_En_cours_Cumul_Jour vh on vh.DTE_ALIM = x.jour_alim and x.jour_alim = @Date_obs
) 

,Cte_Opp_En_cours_hebdo_prec_final as (
select  --Opport CAV en cours de vie S-1 Hebdo
		ISNULL(SUM(vhp.NB_OPP_ENCRS),0)				as NB_OPP_ENCRS_HEBDO_PREC, 
		ISNULL(SUM(vhp.NB_ENCRS_OB_DECOUV),0)		as NB_ENCRS_OB_DECOUV_HEBDO_PREC,
		ISNULL(SUM(vhp.NB_ENCRS_OB_ELAB),0)			as NB_ENCRS_OB_ELAB_HEBDO_PREC,
		ISNULL(SUM(vhp.NB_ENCRS_OB_AFF_SIGN),0)		as NB_ENCRS_OB_AFF_SIGN_HEBDO_PREC,
		ISNULL(SUM(vhp.NB_ENCRS_OB_VERIF),0)		as NB_ENCRS_OB_VERIF_HEBDO_PREC,
		ISNULL(SUM(vhp.NB_ENCRS_OB_DOSS_INC),0)		as NB_ENCRS_OB_DOSS_INC_HEBDO_PREC, 
		ISNULL(SUM(vhp.NB_ENCRS_OB_DOSS_VAL),0)		as NB_ENCRS_OB_DOSS_VAL_HEBDO_PREC,
		ISNULL(SUM(vhp.NB_ENCRS_OF_DETEC),0)		as NB_ENCRS_OF_DETEC_HEBDO_PREC,
		ISNULL(SUM(vhp.NB_ENCRS_OF_DECOUV),0)		as NB_ENCRS_OF_DECOUV_HEBDO_PREC,
		ISNULL(SUM(vhp.NB_ENCRS_OF_ELAB),0)			as NB_ENCRS_OF_ELAB_HEBDO_PREC,
		ISNULL(SUM(vhp.NB_ENCRS_OF_AFF_SIGN),0)		as NB_ENCRS_OF_AFF_SIGN_HEBDO_PREC,
		ISNULL(SUM(vhp.NB_ENCRS_OF_VERIF),0)		as NB_ENCRS_OF_VERIF_HEBDO_PREC, 
		ISNULL(SUM(vhp.NB_ENCRS_OF_DOSS_INC),0)		as NB_ENCRS_OF_DOSS_INC_HEBDO_PREC,
		ISNULL(SUM(vhp.NB_ENCRS_OF_DOSS_VAL),0)		as NB_ENCRS_OF_DOSS_VAL_HEBDO_PREC
from cte_jour_alim_list x 
left join Cte_Opp_En_cours_Cumul_Jour vhp on vhp.DTE_ALIM = x.jour_alim and  x.jour_alim = @LastDOLWobs
)

--Opport CAV gagnees final
,Cte_Opp_Gagnees_jour_final as (
select	x.jour_alim,
		--Opport CAV gagnees Jour
		ISNULL(SUM(g.NB_OPP_GAGNEES),0)			as NB_OPP_GAGNEES, 
		ISNULL(SUM(g.NB_GAGNEES_OB),0)			as NB_GAGNEES_OB,
		ISNULL(SUM(g.NB_GAGNEES_OB_Dig),0)		as NB_GAGNEES_OB_Dig,
		ISNULL(SUM(g.NB_GAGNEES_OB_CRC),0)		as NB_GAGNEES_OB_CRC,
		ISNULL(SUM(g.NB_GAGNEES_OF),0)			as NB_GAGNEES_OF,
		ISNULL(SUM(g.NB_GAGNEES_OF_Dig),0)		as NB_GAGNEES_OF_Dig,
		ISNULL(SUM(g.NB_GAGNEES_OF_Btq),0)		as NB_GAGNEES_OF_Btq,
		ISNULL(SUM(g.NB_GAGNEES_OF_SS_Dig),0)	as NB_GAGNEES_OF_SS_Dig,
		ISNULL(SUM(g.NB_GAGNEES_OF_SS_Btq),0)	as NB_GAGNEES_OF_SS_Btq,
		ISNULL(SUM(g.NB_GAGNEES_OF_AC_Dig),0)	as NB_GAGNEES_OF_AC_Dig,
		ISNULL(SUM(g.NB_GAGNEES_OF_AC_Btq),0)	as NB_GAGNEES_OF_AC_Btq,
		ISNULL(SUM(g.NB_GAGNEES_GP),0)			as NB_GAGNEES_GP,
		ISNULL(SUM(g.NB_GAGNEES_GP_AC),0)		as NB_GAGNEES_GP_AC,
		ISNULL(SUM(g.NB_GAGNEES_GP_SS),0)		as NB_GAGNEES_GP_SS,
		ISNULL(SUM(g.DELAI_GAGNEES_OB),0)		as DELAI_GAGNEES_OB, 
		ISNULL(SUM(g.DELAI_GAGNEES_OB_Dig),0)	as DELAI_GAGNEES_OB_Dig, 
		ISNULL(SUM(g.DELAI_GAGNEES_OB_CRC),0)	as DELAI_GAGNEES_OB_CRC, 
		ISNULL(SUM(g.DELAI_GAGNEES_OF),0)		as DELAI_GAGNEES_OF, 
		ISNULL(SUM(g.DELAI_GAGNEES_OF_Dig),0)	as DELAI_GAGNEES_OF_Dig, 
		ISNULL(SUM(g.DELAI_GAGNEES_OF_Btq),0)	as DELAI_GAGNEES_OF_Btq, 
		ISNULL(SUM(g.NB_ITERATION_OB),0)		as NB_ITERATION_OB, 
		ISNULL(SUM(g.NB_ITERATION_OB_0),0)		as NB_ITERATION_OB_0, 
		ISNULL(SUM(g.NB_ITERATION_OB_1_2),0)	as NB_ITERATION_OB_1_2, 
		ISNULL(SUM(g.NB_ITERATION_OB_plus_2),0) as NB_ITERATION_OB_plus_2, 
		ISNULL(SUM(g.NB_ITERATION_OF),0)		as NB_ITERATION_OF, 
		ISNULL(SUM(g.NB_ITERATION_OF_0),0)		as NB_ITERATION_OF_0, 
		ISNULL(SUM(g.NB_ITERATION_OF_1_2),0)	as NB_ITERATION_OF_1_2, 
		ISNULL(SUM(g.NB_ITERATION_OF_plus_2),0) as NB_ITERATION_OF_plus_2
from cte_jour_alim_list x 
left join Cte_Opp_Gagnees_Cumul_Jour g on g.DTE_ALIM = x.jour_alim
GROUP BY x.jour_alim
)

,Cte_Opp_Gagnees_hebdo_final as (
select	--Opport CAV gagnees Hebdo
		ISNULL(SUM(gh.NB_OPP_GAGNEES),0)			as NB_OPP_GAGNEES_HEBDO, 
		ISNULL(SUM(gh.NB_GAGNEES_OB),0)				as NB_GAGNEES_OB_HEBDO,
		ISNULL(SUM(gh.NB_GAGNEES_OB_Dig),0)			as NB_GAGNEES_OB_Dig_HEBDO,
		ISNULL(SUM(gh.NB_GAGNEES_OB_CRC),0)			as NB_GAGNEES_OB_CRC_HEBDO,
		ISNULL(SUM(gh.NB_GAGNEES_OF),0)				as NB_GAGNEES_OF_HEBDO,
		ISNULL(SUM(gh.NB_GAGNEES_OF_Dig),0)			as NB_GAGNEES_OF_Dig_HEBDO,
		ISNULL(SUM(gh.NB_GAGNEES_OF_Btq),0)			as NB_GAGNEES_OF_Btq_HEBDO,
		ISNULL(SUM(gh.NB_GAGNEES_OF_SS_Dig),0)		as NB_GAGNEES_OF_SS_Dig_HEBDO,
		ISNULL(SUM(gh.NB_GAGNEES_OF_SS_Btq),0)		as NB_GAGNEES_OF_SS_Btq_HEBDO,
		ISNULL(SUM(gh.NB_GAGNEES_OF_AC_Dig),0)		as NB_GAGNEES_OF_AC_Dig_HEBDO,
		ISNULL(SUM(gh.NB_GAGNEES_OF_AC_Btq),0)		as NB_GAGNEES_OF_AC_Btq_HEBDO,
		ISNULL(SUM(gh.NB_GAGNEES_GP),0)				as NB_GAGNEES_GP_HEBDO,
		ISNULL(SUM(gh.NB_GAGNEES_GP_AC),0)			as NB_GAGNEES_GP_AC_HEBDO,
		ISNULL(SUM(gh.NB_GAGNEES_GP_SS),0)			as NB_GAGNEES_GP_SS_HEBDO,
		ISNULL(SUM(gh.DELAI_GAGNEES_OB),0)			as DELAI_GAGNEES_OB_HEBDO, 
		ISNULL(SUM(gh.DELAI_GAGNEES_OB_Dig),0)		as DELAI_GAGNEES_OB_Dig_HEBDO,  
		ISNULL(SUM(gh.DELAI_GAGNEES_OB_CRC),0)		as DELAI_GAGNEES_OB_CRC_HEBDO,  
		ISNULL(SUM(gh.DELAI_GAGNEES_OF),0)			as DELAI_GAGNEES_OF_HEBDO, 
		ISNULL(SUM(gh.DELAI_GAGNEES_OF_Dig),0)		as DELAI_GAGNEES_OF_Dig_HEBDO,  
		ISNULL(SUM(gh.DELAI_GAGNEES_OF_Btq),0)		as DELAI_GAGNEES_OF_Btq_HEBDO, 
		ISNULL(SUM(gh.NB_ITERATION_OB),0)			as NB_ITERATION_OB_HEBDO, 
		ISNULL(SUM(gh.NB_ITERATION_OB_0),0)			as NB_ITERATION_OB_0_HEBDO, 
		ISNULL(SUM(gh.NB_ITERATION_OB_1_2),0)		as NB_ITERATION_OB_1_2_HEBDO, 
		ISNULL(SUM(gh.NB_ITERATION_OB_plus_2),0)	as NB_ITERATION_OB_plus_2_HEBDO, 
		ISNULL(SUM(gh.NB_ITERATION_OF),0)			as NB_ITERATION_OF_HEBDO, 
		ISNULL(SUM(gh.NB_ITERATION_OF_0),0)			as NB_ITERATION_OF_0_HEBDO, 
		ISNULL(SUM(gh.NB_ITERATION_OF_1_2),0)		as NB_ITERATION_OF_1_2_HEBDO, 
		ISNULL(SUM(gh.NB_ITERATION_OF_plus_2),0)	as NB_ITERATION_OF_plus_2_HEBDO
from cte_jour_alim_list x 
left join Cte_Opp_Gagnees_Cumul_Jour gh  on gh.DTE_ALIM = x.jour_alim and x.jour_alim = @Date_obs
)

,Cte_Opp_Gagnees_hebdo_prec_final as (
select	--Opport CAV gagnees S-1 Hebdo
		ISNULL(SUM(ghp.NB_OPP_GAGNEES),0)			as NB_OPP_GAGNEES_HEBDO_PREC, 
		ISNULL(SUM(ghp.NB_GAGNEES_OB),0)			as NB_GAGNEES_OB_HEBDO_PREC,
		ISNULL(SUM(ghp.NB_GAGNEES_OB_Dig),0)		as NB_GAGNEES_OB_Dig_HEBDO_PREC,
		ISNULL(SUM(ghp.NB_GAGNEES_OB_CRC),0)		as NB_GAGNEES_OB_CRC_HEBDO_PREC,
		ISNULL(SUM(ghp.NB_GAGNEES_OF),0)			as NB_GAGNEES_OF_HEBDO_PREC,
		ISNULL(SUM(ghp.NB_GAGNEES_OF_Dig),0)		as NB_GAGNEES_OF_Dig_HEBDO_PREC,
		ISNULL(SUM(ghp.NB_GAGNEES_OF_Btq),0)		as NB_GAGNEES_OF_Btq_HEBDO_PREC,
		ISNULL(SUM(ghp.NB_GAGNEES_OF_SS_Dig),0)		as NB_GAGNEES_OF_SS_Dig_HEBDO_PREC,
		ISNULL(SUM(ghp.NB_GAGNEES_OF_SS_Btq),0)		as NB_GAGNEES_OF_SS_Btq_HEBDO_PREC,
		ISNULL(SUM(ghp.NB_GAGNEES_OF_AC_Dig),0)		as NB_GAGNEES_OF_AC_Dig_HEBDO_PREC,
		ISNULL(SUM(ghp.NB_GAGNEES_OF_AC_Btq),0)		as NB_GAGNEES_OF_AC_Btq_HEBDO_PREC,
		ISNULL(SUM(ghp.NB_GAGNEES_GP),0)			as NB_GAGNEES_GP_HEBDO_PREC,
		ISNULL(SUM(ghp.NB_GAGNEES_GP_AC),0)			as NB_GAGNEES_GP_AC_HEBDO_PREC,
		ISNULL(SUM(ghp.NB_GAGNEES_GP_SS),0)			as NB_GAGNEES_GP_SS_HEBDO_PREC,
		ISNULL(SUM(ghp.DELAI_GAGNEES_OB),0)			as DELAI_GAGNEES_OB_HEBDO_PREC, 
		ISNULL(SUM(ghp.DELAI_GAGNEES_OB_Dig),0)		as DELAI_GAGNEES_OB_Dig_HEBDO_PREC, 
		ISNULL(SUM(ghp.DELAI_GAGNEES_OB_CRC),0)		as DELAI_GAGNEES_OB_CRC_HEBDO_PREC, 
		ISNULL(SUM(ghp.DELAI_GAGNEES_OF),0)			as DELAI_GAGNEES_OF_HEBDO_PREC, 
		ISNULL(SUM(ghp.DELAI_GAGNEES_OF_Dig),0)		as DELAI_GAGNEES_OF_Dig_HEBDO_PREC, 
		ISNULL(SUM(ghp.DELAI_GAGNEES_OF_Btq),0)		as DELAI_GAGNEES_OF_Btq_HEBDO_PREC, 
		ISNULL(SUM(ghp.NB_ITERATION_OB),0)			as NB_ITERATION_OB_HEBDO_PREC, 
		ISNULL(SUM(ghp.NB_ITERATION_OB_0),0)		as NB_ITERATION_OB_0_HEBDO_PREC, 
		ISNULL(SUM(ghp.NB_ITERATION_OB_1_2),0)		as NB_ITERATION_OB_1_2_HEBDO_PREC, 
		ISNULL(SUM(ghp.NB_ITERATION_OB_plus_2),0)	as NB_ITERATION_OB_plus_2_HEBDO_PREC, 
		ISNULL(SUM(ghp.NB_ITERATION_OF),0)			as NB_ITERATION_OF_HEBDO_PREC, 
		ISNULL(SUM(ghp.NB_ITERATION_OF_0),0)		as NB_ITERATION_OF_0_HEBDO_PREC, 
		ISNULL(SUM(ghp.NB_ITERATION_OF_1_2),0)		as NB_ITERATION_OF_1_2_HEBDO_PREC, 
		ISNULL(SUM(ghp.NB_ITERATION_OF_plus_2),0)	as NB_ITERATION_OF_plus_2_HEBDO_PREC
from cte_jour_alim_list x 
left join Cte_Opp_Gagnees_Cumul_Jour ghp on ghp.DTE_ALIM = x.jour_alim and  x.jour_alim = @LastDOLWobs
)

--Opport CAV perdues final
,Cte_Opp_Perdues_jour_final as (
select	x.jour_alim,
		--Opport CAV perdues Jour
		ISNULL(SUM(p.NB_OPP_PERDUES),0)				as NB_OPP_PERDUES, 
		ISNULL(SUM(p.NB_PERDUES_OB_INC_TECH),0)		as NB_PERDUES_OB_INC_TECH,
		ISNULL(SUM(p.NB_PERDUES_OB_SANS_SUITE),0)	as NB_PERDUES_OB_SANS_SUITE,
		ISNULL(SUM(p.NB_PERDUES_OB_AFF_REF),0)		as NB_PERDUES_OB_AFF_REF,
		ISNULL(SUM(p.NB_PERDUES_OB_RETRACT),0)		as NB_PERDUES_OB_RETRACT,
		ISNULL(SUM(p.NB_PERDUES_OF_INC_TECH),0)		as NB_PERDUES_OF_INC_TECH,
		ISNULL(SUM(p.NB_PERDUES_OF_SANS_SUITE),0)	as NB_PERDUES_OF_SANS_SUITE,
		ISNULL(SUM(p.NB_PERDUES_OF_AFF_REF),0)		as NB_PERDUES_OF_AFF_REF,
		ISNULL(SUM(p.NB_PERDUES_OF_RETRACT),0)		as NB_PERDUES_OF_RETRACT
from cte_jour_alim_list x 
left join Cte_Opp_Perdues_Cumul_Jour p on p.DTE_ALIM = x.jour_alim  
GROUP BY x.jour_alim
)

,Cte_Opp_Perdues_hebdo_final as (
select	--Opport CAV perdues Hebdo
		ISNULL(SUM(ph.NB_OPP_PERDUES),0)				as NB_OPP_PERDUES_HEBDO, 
		ISNULL(SUM(ph.NB_PERDUES_OB_INC_TECH),0)		as NB_PERDUES_OB_INC_TECH_HEBDO,
		ISNULL(SUM(ph.NB_PERDUES_OB_SANS_SUITE),0)		as NB_PERDUES_OB_SANS_SUITE_HEBDO,
		ISNULL(SUM(ph.NB_PERDUES_OB_AFF_REF),0)			as NB_PERDUES_OB_AFF_REF_HEBDO,
		ISNULL(SUM(ph.NB_PERDUES_OB_RETRACT),0)			as NB_PERDUES_OB_RETRACT_HEBDO,
		ISNULL(SUM(ph.NB_PERDUES_OF_INC_TECH),0)		as NB_PERDUES_OF_INC_TECH_HEBDO,
		ISNULL(SUM(ph.NB_PERDUES_OF_SANS_SUITE),0)		as NB_PERDUES_OF_SANS_SUITE_HEBDO,
		ISNULL(SUM(ph.NB_PERDUES_OF_AFF_REF),0)			as NB_PERDUES_OF_AFF_REF_HEBDO,
		ISNULL(SUM(ph.NB_PERDUES_OF_RETRACT),0)			as NB_PERDUES_OF_RETRACT_HEBDO
from cte_jour_alim_list x 
left join Cte_Opp_Perdues_Cumul_Jour ph  on ph.DTE_ALIM = x.jour_alim and x.jour_alim = @Date_obs
)

,Cte_Opp_Perdues_hebdo_prec_final as (
select	--Opport CAV perdues S-1 Hebdo
		ISNULL(SUM(php.NB_OPP_PERDUES),0)				as NB_OPP_PERDUES_HEBDO_PREC, 
		ISNULL(SUM(php.NB_PERDUES_OB_INC_TECH),0)		as NB_PERDUES_OB_INC_TECH_HEBDO_PREC,
		ISNULL(SUM(php.NB_PERDUES_OB_SANS_SUITE),0)		as NB_PERDUES_OB_SANS_SUITE_HEBDO_PREC,
		ISNULL(SUM(php.NB_PERDUES_OB_AFF_REF),0)		as NB_PERDUES_OB_AFF_REF_HEBDO_PREC,
		ISNULL(SUM(php.NB_PERDUES_OB_RETRACT),0)		as NB_PERDUES_OB_RETRACT_HEBDO_PREC,
		ISNULL(SUM(php.NB_PERDUES_OF_INC_TECH),0)		as NB_PERDUES_OF_INC_TECH_HEBDO_PREC,
		ISNULL(SUM(php.NB_PERDUES_OF_SANS_SUITE),0)		as NB_PERDUES_OF_SANS_SUITE_HEBDO_PREC,
		ISNULL(SUM(php.NB_PERDUES_OF_AFF_REF),0)		as NB_PERDUES_OF_AFF_REF_HEBDO_PREC,
		ISNULL(SUM(php.NB_PERDUES_OF_RETRACT),0)		as NB_PERDUES_OF_RETRACT_HEBDO_PREC
from cte_jour_alim_list x
left join Cte_Opp_Perdues_Cumul_Jour php on php.DTE_ALIM = x.jour_alim and  x.jour_alim = @LastDOLWobs 
)

--Réclamations
,Cte_Reclamations_all as (
select req.CaseNumber as NUM_RECLA,
		CAST(req.CreatedDate as date) as DTE_CREA_RECLA,
		req.Status as STAT_RECLA, --Non traité
		dim_stat.LIBELLE as LIB_STAT_RECLA, 
		req.Process__c as SS_TYP_RECLA,  --Souscription = '01'
		dim_proc.LIBELLE as LIB_SS_TYP_RECLA, 
		req.Origin as CAN_INT_ORI_RECLA, --prendre tous les canaux sauf fax et Email VIP
		dim_can_decl.LIBELLE as LIB_CAN_INT_ORI_RECLA,
		dim_rt.LIBELLE as TYPE_RECLA 
from [$(DataHubDatabaseName)].dbo.PV_SF_CASE req
left join DIM_RECORDTYPE dim_rt
on req.RecordTypeId = dim_rt.CODE_SF
left join DIM_CASE_STATUS dim_stat
on req.Status = dim_stat.CODE_SF
left join DIM_CASE_PROCESSUS dim_proc
on req.Process__c = dim_proc.CODE_SF
left join DIM_CASE_CANAL_ORIGINE dim_can_decl
on req.Origin = dim_can_decl.CODE_SF
where dim_rt.LIBELLE = 'Réclamation'
and req.Status not in ('03', '04')
)
--Nb de de réclamations
,Cte_Reclamations as (
select DTE_CREA_RECLA, 
		count(NUM_RECLA) as NB_RECLA, 
		case when SS_TYP_RECLA = '01' then count(NUM_RECLA) end as NB_RECLA_ENROL, 
		case when SS_TYP_RECLA = '01' and  CAN_INT_ORI_RECLA = '01' then count(NUM_RECLA) end as NB_RECLA_MOB_SMS, 
		case when SS_TYP_RECLA = '01' and  CAN_INT_ORI_RECLA = '02' then count(NUM_RECLA) end as NB_RECLA_NET_CLT, 
		case when SS_TYP_RECLA = '01' and  CAN_INT_ORI_RECLA = '03' then count(NUM_RECLA) end as NB_RECLA_COURRIER, 
		case when SS_TYP_RECLA = '01' and  CAN_INT_ORI_RECLA = '04' then count(NUM_RECLA) end as NB_RECLA_APP_ENTR, 
		case when SS_TYP_RECLA = '01' and  CAN_INT_ORI_RECLA = '05' then count(NUM_RECLA) end as NB_RECLA_APP_SORT, 
		case when SS_TYP_RECLA = '01' and  CAN_INT_ORI_RECLA = '07' then count(NUM_RECLA) end as NB_RECLA_CHAT, 
		case when SS_TYP_RECLA = '01' and  CAN_INT_ORI_RECLA = '08' then count(NUM_RECLA) end as NB_RECLA_RES_SOC, 
		case when SS_TYP_RECLA = '01' and  CAN_INT_ORI_RECLA = '09' then count(NUM_RECLA) end as NB_RECLA_APP_ENTR_TELCO, 
		case when SS_TYP_RECLA = '01' and  CAN_INT_ORI_RECLA = '10' then count(NUM_RECLA) end as NB_RECLA_APP_ENTR_Btq, 
		case when SS_TYP_RECLA = '01' and  CAN_INT_ORI_RECLA = '11' then count(NUM_RECLA) end as NB_RECLA_MAIL, 
		case when SS_TYP_RECLA = '01' and  CAN_INT_ORI_RECLA = '12' then count(NUM_RECLA) end as NB_RECLA_FOR_DEM, 
		case when SS_TYP_RECLA = '01' and  CAN_INT_ORI_RECLA = '13' then count(NUM_RECLA) end as NB_RECLA_SFMC 
from Cte_Reclamations_all
GROUP BY DTE_CREA_RECLA, 
		SS_TYP_RECLA, 
		CAN_INT_ORI_RECLA
) 

--Cumul Jour
,Cte_Reclamations_Jour as (
select DTE_CREA_RECLA, 
		ISNULL(SUM(NB_RECLA),0) as NB_RECLA, 
		ISNULL(SUM(NB_RECLA_ENROL),0) as NB_RECLA_ENROL, 
		ISNULL(SUM(NB_RECLA_MOB_SMS),0) as NB_RECLA_MOB_SMS, 
		ISNULL(SUM(NB_RECLA_NET_CLT),0) as NB_RECLA_NET_CLT, 
		ISNULL(SUM(NB_RECLA_COURRIER),0) as NB_RECLA_COURRIER, 
		ISNULL(SUM(NB_RECLA_APP_ENTR),0) as NB_RECLA_APP_ENTR, 
		ISNULL(SUM(NB_RECLA_APP_SORT),0) as NB_RECLA_APP_SORT, 
		ISNULL(SUM(NB_RECLA_CHAT),0) as NB_RECLA_CHAT, 
		ISNULL(SUM(NB_RECLA_RES_SOC),0) as NB_RECLA_RES_SOC, 
		ISNULL(SUM(NB_RECLA_APP_ENTR_TELCO),0) as NB_RECLA_APP_ENTR_TELCO, 
		ISNULL(SUM(NB_RECLA_APP_ENTR_Btq),0) as NB_RECLA_APP_ENTR_Btq, 
		ISNULL(SUM(NB_RECLA_MAIL),0) as NB_RECLA_MAIL, 
		ISNULL(SUM(NB_RECLA_FOR_DEM),0) as NB_RECLA_FOR_DEM, 
		ISNULL(SUM(NB_RECLA_SFMC),0) as NB_RECLA_SFMC
from Cte_Reclamations
where DTE_CREA_RECLA > CAST(DATEADD(day, -7, @Date_obs) AS DATE) AND DTE_CREA_RECLA <= @Date_obs --7 jours glissants
GROUP BY DTE_CREA_RECLA
) 

--Cumul hedbo
,Cte_Reclamations_Hebdo as (
select @Date_obs as datejoin, 
		ISNULL(SUM(NB_RECLA),0) as NB_RECLA_HEBDO, 
		ISNULL(SUM(NB_RECLA_ENROL),0) as NB_RECLA_ENROL_HEBDO, 
		ISNULL(SUM(NB_RECLA_MOB_SMS),0) as NB_RECLA_MOB_SMS_HEBDO, 
		ISNULL(SUM(NB_RECLA_NET_CLT),0) as NB_RECLA_NET_CLT_HEBDO, 
		ISNULL(SUM(NB_RECLA_COURRIER),0) as NB_RECLA_COURRIER_HEBDO, 
		ISNULL(SUM(NB_RECLA_APP_ENTR),0) as NB_RECLA_APP_ENTR_HEBDO, 
		ISNULL(SUM(NB_RECLA_APP_SORT),0) as NB_RECLA_APP_SORT_HEBDO, 
		ISNULL(SUM(NB_RECLA_CHAT),0) as NB_RECLA_CHAT_HEBDO, 
		ISNULL(SUM(NB_RECLA_RES_SOC),0) as NB_RECLA_RES_SOC_HEBDO, 
		ISNULL(SUM(NB_RECLA_APP_ENTR_TELCO),0) as NB_RECLA_APP_ENTR_TELCO_HEBDO, 
		ISNULL(SUM(NB_RECLA_APP_ENTR_Btq),0) as NB_RECLA_APP_ENTR_Btq_HEBDO, 
		ISNULL(SUM(NB_RECLA_MAIL),0) as NB_RECLA_MAIL_HEBDO, 
		ISNULL(SUM(NB_RECLA_FOR_DEM),0) as NB_RECLA_FOR_DEM_HEBDO, 
		ISNULL(SUM(NB_RECLA_SFMC),0) as NB_RECLA_SFMC_HEBDO
from Cte_Reclamations
where DATEPART(YEAR, DTE_CREA_RECLA) = @Anneeweekobs
AND DATEPART(ISO_WEEK, DTE_CREA_RECLA)= @weekobs
AND DTE_CREA_RECLA <= @Date_obs 
) 

--Cumul hedbo S-1
,Cte_Reclamations_Hebdo_Prec as (
select @Date_obs as datejoin,
		ISNULL(SUM(NB_RECLA),0) as NB_RECLA_HEBDO_PREC, 
		ISNULL(SUM(NB_RECLA_ENROL),0) as NB_RECLA_ENROL_HEBDO_PREC, 
		ISNULL(SUM(NB_RECLA_MOB_SMS),0) as NB_RECLA_MOB_SMS_HEBDO_PREC, 
		ISNULL(SUM(NB_RECLA_NET_CLT),0) as NB_RECLA_NET_CLT_HEBDO_PREC, 
		ISNULL(SUM(NB_RECLA_COURRIER),0) as NB_RECLA_COURRIER_HEBDO_PREC, 
		ISNULL(SUM(NB_RECLA_APP_ENTR),0) as NB_RECLA_APP_ENTR_HEBDO_PREC, 
		ISNULL(SUM(NB_RECLA_APP_SORT),0) as NB_RECLA_APP_SORT_HEBDO_PREC, 
		ISNULL(SUM(NB_RECLA_CHAT),0) as NB_RECLA_CHAT_HEBDO_PREC, 
		ISNULL(SUM(NB_RECLA_RES_SOC),0) as NB_RECLA_RES_SOC_HEBDO_PREC, 
		ISNULL(SUM(NB_RECLA_APP_ENTR_TELCO),0) as NB_RECLA_APP_ENTR_TELCO_HEBDO_PREC, 
		ISNULL(SUM(NB_RECLA_APP_ENTR_Btq),0) as NB_RECLA_APP_ENTR_Btq_HEBDO_PREC, 
		ISNULL(SUM(NB_RECLA_MAIL),0) as NB_RECLA_MAIL_HEBDO_PREC, 
		ISNULL(SUM(NB_RECLA_FOR_DEM),0) as NB_RECLA_FOR_DEM_HEBDO_PREC, 
		ISNULL(SUM(NB_RECLA_SFMC),0) as NB_RECLA_SFMC_HEBDO_PREC
from Cte_Reclamations
where DATEPART(YEAR,DTE_CREA_RECLA) = @AnneeweekPrecobs
AND DATEPART(ISO_WEEK,DTE_CREA_RECLA)= @weekPrecobs
) 
/******************_*_Requête_*_******************/

select x.jour_alim,
		c.NB_OPP_CREEES, 
		c.NB_CREEES_OB,
		c.NB_CREEES_OB_Dig,
		c.NB_CREEES_OB_CRC,
		c.NB_CREEES_OF,
		c.NB_CREEES_OF_Dig,
		c.NB_CREEES_OF_Btq,
		c.NB_CREEES_OF_SS,
		c.NB_CREEES_OF_SS_Dig,
		c.NB_CREEES_OF_SS_Btq, 
		c.NB_CREEES_OF_AC,
		c.NB_CREEES_OF_AC_Dig,
		c.NB_CREEES_OF_AC_Btq,
		ch.NB_OPP_CREEES_HEBDO, 
		ch.NB_CREEES_OB_HEBDO,
		ch.NB_CREEES_OB_Dig_HEBDO,
		ch.NB_CREEES_OB_CRC_HEBDO,
		ch.NB_CREEES_OF_HEBDO,
		ch.NB_CREEES_OF_Dig_HEBDO,
		ch.NB_CREEES_OF_Btq_HEBDO,
		ch.NB_CREEES_OF_SS_HEBDO,
		ch.NB_CREEES_OF_SS_Dig_HEBDO,
		ch.NB_CREEES_OF_SS_Btq_HEBDO,
		ch.NB_CREEES_OF_AC_HEBDO,
		ch.NB_CREEES_OF_AC_Dig_HEBDO,
		ch.NB_CREEES_OF_AC_Btq_HEBDO,
		chp.NB_OPP_CREEES_HEBDO_PREC, 
		chp.NB_CREEES_OB_HEBDO_PREC,
		chp.NB_CREEES_OB_Dig_HEBDO_PREC,
		chp.NB_CREEES_OB_CRC_HEBDO_PREC,
		chp.NB_CREEES_OF_HEBDO_PREC,
		chp.NB_CREEES_OF_Dig_HEBDO_PREC,
		chp.NB_CREEES_OF_Btq_HEBDO_PREC,
		chp.NB_CREEES_OF_SS_HEBDO_PREC,
		chp.NB_CREEES_OF_SS_Dig_HEBDO_PREC,
		chp.NB_CREEES_OF_SS_Btq_HEBDO_PREC,
		chp.NB_CREEES_OF_AC_HEBDO_PREC,
		chp.NB_CREEES_OF_AC_Dig_HEBDO_PREC,
		chp.NB_CREEES_OF_AC_Btq_HEBDO_PREC,
		v.NB_OPP_ENCRS, 
		v.NB_ENCRS_OB_DECOUV,
		v.NB_ENCRS_OB_ELAB,
		v.NB_ENCRS_OB_AFF_SIGN,
		v.NB_ENCRS_OB_VERIF,
		v.NB_ENCRS_OB_DOSS_INC, 
		v.NB_ENCRS_OB_DOSS_VAL,
		v.NB_ENCRS_OF_DETEC,
		v.NB_ENCRS_OF_DECOUV,
		v.NB_ENCRS_OF_ELAB,
		v.NB_ENCRS_OF_AFF_SIGN,
		v.NB_ENCRS_OF_VERIF, 
		v.NB_ENCRS_OF_DOSS_INC,
		v.NB_ENCRS_OF_DOSS_VAL,
		vh.NB_OPP_ENCRS_HEBDO, 
		vh.NB_ENCRS_OB_DECOUV_HEBDO,
		vh.NB_ENCRS_OB_ELAB_HEBDO,
		vh.NB_ENCRS_OB_AFF_SIGN_HEBDO,
		vh.NB_ENCRS_OB_VERIF_HEBDO,
		vh.NB_ENCRS_OB_DOSS_INC_HEBDO, 
		vh.NB_ENCRS_OB_DOSS_VAL_HEBDO,
		vh.NB_ENCRS_OF_DETEC_HEBDO,
		vh.NB_ENCRS_OF_DECOUV_HEBDO,
		vh.NB_ENCRS_OF_ELAB_HEBDO,
		vh.NB_ENCRS_OF_AFF_SIGN_HEBDO,
		vh.NB_ENCRS_OF_VERIF_HEBDO, 
		vh.NB_ENCRS_OF_DOSS_INC_HEBDO,
		vh.NB_ENCRS_OF_DOSS_VAL_HEBDO,
		vhp.NB_OPP_ENCRS_HEBDO_PREC, 
		vhp.NB_ENCRS_OB_DECOUV_HEBDO_PREC,
		vhp.NB_ENCRS_OB_ELAB_HEBDO_PREC,
		vhp.NB_ENCRS_OB_AFF_SIGN_HEBDO_PREC,
		vhp.NB_ENCRS_OB_VERIF_HEBDO_PREC,
		vhp.NB_ENCRS_OB_DOSS_INC_HEBDO_PREC, 
		vhp.NB_ENCRS_OB_DOSS_VAL_HEBDO_PREC,
		vhp.NB_ENCRS_OF_DETEC_HEBDO_PREC,
		vhp.NB_ENCRS_OF_DECOUV_HEBDO_PREC,
		vhp.NB_ENCRS_OF_ELAB_HEBDO_PREC,
		vhp.NB_ENCRS_OF_AFF_SIGN_HEBDO_PREC,
		vhp.NB_ENCRS_OF_VERIF_HEBDO_PREC, 
		vhp.NB_ENCRS_OF_DOSS_INC_HEBDO_PREC,
		vhp.NB_ENCRS_OF_DOSS_VAL_HEBDO_PREC,
		g.NB_OPP_GAGNEES, 
		g.NB_GAGNEES_OB,
		g.NB_GAGNEES_OB_Dig,
		g.NB_GAGNEES_OB_CRC,
		g.NB_GAGNEES_OF,
		g.NB_GAGNEES_OF_Dig,
		g.NB_GAGNEES_OF_Btq,
		g.NB_GAGNEES_OF_SS_Dig,
		g.NB_GAGNEES_OF_SS_Btq,
		g.NB_GAGNEES_OF_AC_Dig,
		g.NB_GAGNEES_OF_AC_Btq,
		g.NB_GAGNEES_GP,
		g.NB_GAGNEES_GP_AC,
		g.NB_GAGNEES_GP_SS,
		g.DELAI_GAGNEES_OB, 
		g.DELAI_GAGNEES_OB_Dig,
		g.DELAI_GAGNEES_OB_CRC,
		g.DELAI_GAGNEES_OF, 
		g.DELAI_GAGNEES_OF_Dig,
		g.DELAI_GAGNEES_OF_Btq,
		g.NB_ITERATION_OB, 
		g.NB_ITERATION_OB_0, 
		g.NB_ITERATION_OB_1_2, 
		g.NB_ITERATION_OB_plus_2, 
		g.NB_ITERATION_OF, 
		g.NB_ITERATION_OF_0, 
		g.NB_ITERATION_OF_1_2, 
		g.NB_ITERATION_OF_plus_2,
		gh.NB_OPP_GAGNEES_HEBDO, 
		gh.NB_GAGNEES_OB_HEBDO,
		gh.NB_GAGNEES_OB_Dig_HEBDO,
		gh.NB_GAGNEES_OB_CRC_HEBDO,
		gh.NB_GAGNEES_OF_HEBDO,
		gh.NB_GAGNEES_OF_Dig_HEBDO,
		gh.NB_GAGNEES_OF_Btq_HEBDO,
		gh.NB_GAGNEES_OF_SS_Dig_HEBDO,
		gh.NB_GAGNEES_OF_SS_Btq_HEBDO,
		gh.NB_GAGNEES_OF_AC_Dig_HEBDO,
		gh.NB_GAGNEES_OF_AC_Btq_HEBDO,
		gh.NB_GAGNEES_GP_HEBDO,
		gh.NB_GAGNEES_GP_AC_HEBDO,
		gh.NB_GAGNEES_GP_SS_HEBDO,
		gh.DELAI_GAGNEES_OB_HEBDO, 
		gh.DELAI_GAGNEES_OB_Dig_HEBDO, 
		gh.DELAI_GAGNEES_OB_CRC_HEBDO, 
		gh.DELAI_GAGNEES_OF_HEBDO, 
		gh.DELAI_GAGNEES_OF_Dig_HEBDO, 
		gh.DELAI_GAGNEES_OF_Btq_HEBDO,
		gh.NB_ITERATION_OB_HEBDO, 
		gh.NB_ITERATION_OB_0_HEBDO, 
		gh.NB_ITERATION_OB_1_2_HEBDO, 
		gh.NB_ITERATION_OB_plus_2_HEBDO, 
		gh.NB_ITERATION_OF_HEBDO, 
		gh.NB_ITERATION_OF_0_HEBDO, 
		gh.NB_ITERATION_OF_1_2_HEBDO, 
		gh.NB_ITERATION_OF_plus_2_HEBDO,
		ghp.NB_OPP_GAGNEES_HEBDO_PREC, 
		ghp.NB_GAGNEES_OB_HEBDO_PREC,
		ghp.NB_GAGNEES_OB_Dig_HEBDO_PREC,
		ghp.NB_GAGNEES_OB_CRC_HEBDO_PREC,
		ghp.NB_GAGNEES_OF_HEBDO_PREC,
		ghp.NB_GAGNEES_OF_Dig_HEBDO_PREC,
		ghp.NB_GAGNEES_OF_Btq_HEBDO_PREC,
		ghp.NB_GAGNEES_OF_SS_Dig_HEBDO_PREC,
		ghp.NB_GAGNEES_OF_SS_Btq_HEBDO_PREC,
		ghp.NB_GAGNEES_OF_AC_Dig_HEBDO_PREC,
		ghp.NB_GAGNEES_OF_AC_Btq_HEBDO_PREC,
		ghp.NB_GAGNEES_GP_HEBDO_PREC,
		ghp.NB_GAGNEES_GP_AC_HEBDO_PREC,
		ghp.NB_GAGNEES_GP_SS_HEBDO_PREC,
		ghp.DELAI_GAGNEES_OB_HEBDO_PREC, 
		ghp.DELAI_GAGNEES_OB_Dig_HEBDO_PREC, 
		ghp.DELAI_GAGNEES_OB_CRC_HEBDO_PREC, 
		ghp.DELAI_GAGNEES_OF_HEBDO_PREC, 
		ghp.DELAI_GAGNEES_OF_Dig_HEBDO_PREC, 
		ghp.DELAI_GAGNEES_OF_Btq_HEBDO_PREC,
		ghp.NB_ITERATION_OB_HEBDO_PREC, 
		ghp.NB_ITERATION_OB_0_HEBDO_PREC, 
		ghp.NB_ITERATION_OB_1_2_HEBDO_PREC, 
		ghp.NB_ITERATION_OB_plus_2_HEBDO_PREC, 
		ghp.NB_ITERATION_OF_HEBDO_PREC, 
		ghp.NB_ITERATION_OF_0_HEBDO_PREC, 
		ghp.NB_ITERATION_OF_1_2_HEBDO_PREC, 
		ghp.NB_ITERATION_OF_plus_2_HEBDO_PREC,
		Case when ISNULL(c.NB_CREEES_OB,0) = 0 
			 then 0 
			 else CAST(g.NB_GAGNEES_OB as float) /  CAST(c.NB_CREEES_OB as float)
		END as TX_OB,
		Case when ISNULL(ch.NB_CREEES_OB_HEBDO,0) = 0 
			 then 0 
			 else CAST(gh.NB_GAGNEES_OB_HEBDO as float) /  CAST(ch.NB_CREEES_OB_HEBDO as float)
		END as TX_OB_HEBDO,
		Case when ISNULL(chp.NB_CREEES_OB_HEBDO_PREC,0) = 0 
			 then 0 
			 else CAST(ghp.NB_GAGNEES_OB_HEBDO_PREC as float) /  CAST(chp.NB_CREEES_OB_HEBDO_PREC as float)
		END as TX_OB_HEBDO_PREC,
		Case when ISNULL(c.NB_CREEES_OB_Dig,0) = 0 
			 then 0 
			 else CAST(g.NB_GAGNEES_OB_Dig as float) /  CAST(c.NB_CREEES_OB_Dig as float)
		END as TX_OB_Dig,
		Case when ISNULL(ch.NB_CREEES_OB_Dig_HEBDO,0) = 0 
			 then 0 
			 else CAST(gh.NB_GAGNEES_OB_Dig_HEBDO as float) /  CAST(ch.NB_CREEES_OB_Dig_HEBDO as float)
		END as TX_OB_Dig_HEBDO,
		Case when ISNULL(chp.NB_CREEES_OB_Dig_HEBDO_PREC,0) = 0 
			 then 0 
			 else CAST(ghp.NB_GAGNEES_OB_Dig_HEBDO_PREC as float) /  CAST(chp.NB_CREEES_OB_Dig_HEBDO_PREC as float)
		END as TX_OB_Dig_HEBDO_PREC,
		Case when ISNULL(c.NB_CREEES_OB_CRC,0) = 0 
			 then 0 
			 else CAST(g.NB_GAGNEES_OB_CRC as float) /  CAST(c.NB_CREEES_OB_CRC as float)
		END as TX_OB_CRC,
		Case when ISNULL(ch.NB_CREEES_OB_CRC_HEBDO,0) = 0 
			 then 0 
			 else CAST(gh.NB_GAGNEES_OB_CRC_HEBDO as float) /  CAST(ch.NB_CREEES_OB_CRC_HEBDO as float)
		END as TX_OB_CRC_HEBDO,
		Case when ISNULL(chp.NB_CREEES_OB_CRC_HEBDO_PREC,0) = 0 
			 then 0 
			 else CAST(ghp.NB_GAGNEES_OB_CRC_HEBDO_PREC as float) /  CAST(chp.NB_CREEES_OB_CRC_HEBDO_PREC as float)
		END as TX_OB_CRC_HEBDO_PREC,
		Case when ISNULL(c.NB_CREEES_OF,0) = 0 
			 then 0 
			 else CAST(g.NB_GAGNEES_OF as float) /  CAST(c.NB_CREEES_OF as float)
		END as TX_OF,
		Case when ISNULL(ch.NB_CREEES_OF_HEBDO,0) = 0 
			 then 0 
			 else CAST(gh.NB_GAGNEES_OF_HEBDO as float) /  CAST(ch.NB_CREEES_OF_HEBDO as float)
		END as TX_OF_HEBDO,
		Case when ISNULL(chp.NB_CREEES_OF_HEBDO_PREC,0) = 0 
			 then 0 
			 else CAST(ghp.NB_GAGNEES_OF_HEBDO_PREC as float) /  CAST(chp.NB_CREEES_OF_HEBDO_PREC as float)
		END as TX_OF_HEBDO_PREC,
		Case when ISNULL(c.NB_CREEES_OF_Dig,0) = 0 
			 then 0 
			 else CAST(g.NB_GAGNEES_OF_Dig as float) /  CAST(c.NB_CREEES_OF_Dig as float)
		END as TX_OF_Dig,
		Case when ISNULL(ch.NB_CREEES_OF_Dig_HEBDO,0) = 0 
			 then 0 
			 else CAST(gh.NB_GAGNEES_OF_Dig_HEBDO as float) /  CAST(ch.NB_CREEES_OF_Dig_HEBDO as float)
		END as TX_OF_Dig_HEBDO,
		Case when ISNULL(chp.NB_CREEES_OF_Dig_HEBDO_PREC,0) = 0 
			 then 0 
			 else CAST(ghp.NB_GAGNEES_OF_Dig_HEBDO_PREC as float) /  CAST(chp.NB_CREEES_OF_Dig_HEBDO_PREC as float)
		END as TX_OF_Dig_HEBDO_PREC,
		Case when ISNULL(c.NB_CREEES_OF_Btq,0) = 0 
			 then 0 
			 else CAST(g.NB_GAGNEES_OF_Btq as float) /  CAST(c.NB_CREEES_OF_Btq as float)
		END as TX_OF_Btq,
		Case when ISNULL(ch.NB_CREEES_OF_Btq_HEBDO,0) = 0 
			 then 0 
			 else CAST(gh.NB_GAGNEES_OF_Btq_HEBDO as float) /  CAST(ch.NB_CREEES_OF_Btq_HEBDO as float)
		END as TX_OF_Btq_HEBDO,
		Case when ISNULL(chp.NB_CREEES_OF_Btq_HEBDO_PREC,0) = 0 
			 then 0 
			 else CAST(ghp.NB_GAGNEES_OF_Btq_HEBDO_PREC as float) /  CAST(chp.NB_CREEES_OF_Btq_HEBDO_PREC as float)
		END as TX_OF_Btq_HEBDO_PREC,
		p.NB_OPP_PERDUES, 
		p.NB_PERDUES_OB_INC_TECH,
		p.NB_PERDUES_OB_SANS_SUITE,
		p.NB_PERDUES_OB_AFF_REF,
		p.NB_PERDUES_OB_RETRACT,
		p.NB_PERDUES_OF_INC_TECH,
		p.NB_PERDUES_OF_SANS_SUITE,
		p.NB_PERDUES_OF_AFF_REF,
		p.NB_PERDUES_OF_RETRACT,
		ph.NB_OPP_PERDUES_HEBDO, 
		ph.NB_PERDUES_OB_INC_TECH_HEBDO,
		ph.NB_PERDUES_OB_SANS_SUITE_HEBDO,
		ph.NB_PERDUES_OB_AFF_REF_HEBDO,
		ph.NB_PERDUES_OB_RETRACT_HEBDO,
		ph.NB_PERDUES_OF_INC_TECH_HEBDO,
		ph.NB_PERDUES_OF_SANS_SUITE_HEBDO,
		ph.NB_PERDUES_OF_AFF_REF_HEBDO,
		ph.NB_PERDUES_OF_RETRACT_HEBDO,
		php.NB_OPP_PERDUES_HEBDO_PREC, 
		php.NB_PERDUES_OB_INC_TECH_HEBDO_PREC,
		php.NB_PERDUES_OB_SANS_SUITE_HEBDO_PREC,
		php.NB_PERDUES_OB_AFF_REF_HEBDO_PREC,
		php.NB_PERDUES_OB_RETRACT_HEBDO_PREC,
		php.NB_PERDUES_OF_INC_TECH_HEBDO_PREC,
		php.NB_PERDUES_OF_SANS_SUITE_HEBDO_PREC,
		php.NB_PERDUES_OF_AFF_REF_HEBDO_PREC,
		php.NB_PERDUES_OF_RETRACT_HEBDO_PREC, 
		ISNULL(r.NB_RECLA,0)					as NB_RECLA, 
		ISNULL(r.NB_RECLA_ENROL,0)				as NB_RECLA_ENROL, 
		ISNULL(r.NB_RECLA_MOB_SMS,0)			as NB_RECLA_MOB_SMS, 
		ISNULL(r.NB_RECLA_NET_CLT,0)			as NB_RECLA_NET_CLT, 
		ISNULL(r.NB_RECLA_COURRIER,0)			as NB_RECLA_COURRIER, 
		ISNULL(r.NB_RECLA_APP_ENTR,0)			as NB_RECLA_APP_ENTR, 
		ISNULL(r.NB_RECLA_APP_SORT,0)			as NB_RECLA_APP_SORT, 
		ISNULL(r.NB_RECLA_CHAT,0)				as NB_RECLA_CHAT, 
		ISNULL(r.NB_RECLA_RES_SOC,0)			as NB_RECLA_RES_SOC, 
		ISNULL(r.NB_RECLA_APP_ENTR_TELCO,0)		as NB_RECLA_APP_ENTR_TELCO, 
		ISNULL(r.NB_RECLA_APP_ENTR_Btq,0)		as NB_RECLA_APP_ENTR_Btq, 
		ISNULL(r.NB_RECLA_MAIL,0)				as NB_RECLA_MAIL, 
		ISNULL(r.NB_RECLA_FOR_DEM,0)			as NB_RECLA_FOR_DEM, 
		ISNULL(r.NB_RECLA_SFMC,0)				as NB_RECLA_SFMC, 
		ISNULL(rh.NB_RECLA_HEBDO,0)					as NB_RECLA_HEBDO, 
		ISNULL(rh.NB_RECLA_ENROL_HEBDO,0)			as NB_RECLA_ENROL_HEBDO, 
		ISNULL(rh.NB_RECLA_MOB_SMS_HEBDO,0)			as NB_RECLA_MOB_SMS_HEBDO, 
		ISNULL(rh.NB_RECLA_NET_CLT_HEBDO,0)			as NB_RECLA_NET_CLT_HEBDO, 
		ISNULL(rh.NB_RECLA_COURRIER_HEBDO,0)		as NB_RECLA_COURRIER_HEBDO, 
		ISNULL(rh.NB_RECLA_APP_ENTR_HEBDO,0)		as NB_RECLA_APP_ENTR_HEBDO, 
		ISNULL(rh.NB_RECLA_APP_SORT_HEBDO,0)		as NB_RECLA_APP_SORT_HEBDO, 
		ISNULL(rh.NB_RECLA_CHAT_HEBDO,0)			as NB_RECLA_CHAT_HEBDO, 
		ISNULL(rh.NB_RECLA_RES_SOC_HEBDO,0)			as NB_RECLA_RES_SOC_HEBDO, 
		ISNULL(rh.NB_RECLA_APP_ENTR_TELCO_HEBDO,0)	as NB_RECLA_APP_ENTR_TELCO_HEBDO, 
		ISNULL(rh.NB_RECLA_APP_ENTR_Btq_HEBDO,0)	as NB_RECLA_APP_ENTR_Btq_HEBDO, 
		ISNULL(rh.NB_RECLA_MAIL_HEBDO,0)			as NB_RECLA_MAIL_HEBDO, 
		ISNULL(rh.NB_RECLA_FOR_DEM_HEBDO,0)			as NB_RECLA_FOR_DEM_HEBDO, 
		ISNULL(rh.NB_RECLA_SFMC_HEBDO,0)			as NB_RECLA_SFMC_HEBDO, 
		ISNULL(rhp.NB_RECLA_HEBDO_PREC,0)					as NB_RECLA_HEBDO_PREC, 
		ISNULL(rhp.NB_RECLA_ENROL_HEBDO_PREC,0)				as NB_RECLA_ENROL_HEBDO_PREC, 
		ISNULL(rhp.NB_RECLA_MOB_SMS_HEBDO_PREC,0)			as NB_RECLA_MOB_SMS_HEBDO_PREC, 
		ISNULL(rhp.NB_RECLA_NET_CLT_HEBDO_PREC,0)			as NB_RECLA_NET_CLT_HEBDO_PREC, 
		ISNULL(rhp.NB_RECLA_COURRIER_HEBDO_PREC,0)			as NB_RECLA_COURRIER_HEBDO_PREC, 
		ISNULL(rhp.NB_RECLA_APP_ENTR_HEBDO_PREC,0)			as NB_RECLA_APP_ENTR_HEBDO_PREC, 
		ISNULL(rhp.NB_RECLA_APP_SORT_HEBDO_PREC,0)			as NB_RECLA_APP_SORT_HEBDO_PREC, 
		ISNULL(rhp.NB_RECLA_CHAT_HEBDO_PREC,0)				as NB_RECLA_CHAT_HEBDO_PREC, 
		ISNULL(rhp.NB_RECLA_RES_SOC_HEBDO_PREC,0)			as NB_RECLA_RES_SOC_HEBDO_PREC, 
		ISNULL(rhp.NB_RECLA_APP_ENTR_TELCO_HEBDO_PREC,0)	as NB_RECLA_APP_ENTR_TELCO_HEBDO_PREC, 
		ISNULL(rhp.NB_RECLA_APP_ENTR_Btq_HEBDO_PREC,0)		as NB_RECLA_APP_ENTR_Btq_HEBDO_PREC, 
		ISNULL(rhp.NB_RECLA_MAIL_HEBDO_PREC,0)				as NB_RECLA_MAIL_HEBDO_PREC, 
		ISNULL(rhp.NB_RECLA_FOR_DEM_HEBDO_PREC,0)			as NB_RECLA_FOR_DEM_HEBDO_PREC, 
		ISNULL(rhp.NB_RECLA_SFMC_HEBDO_PREC,0)				as NB_RECLA_SFMC_HEBDO_PREC
from cte_jour_alim_list x 
--Opport CAV créées
left join Cte_Opp_Creees_jour_final c on x.jour_alim = c.jour_alim
left join Cte_Opp_Creees_hebdo_final ch on 1=1
left join Cte_Opp_Creees_hebdo_prec_final chp on 1=1
--Opport CAV en cours de vie
left join Cte_Opp_En_cours_jour_final v on c.jour_alim = v.jour_alim
left join Cte_Opp_En_cours_hebdo_final vh on 1=1
left join Cte_Opp_En_cours_hebdo_prec_final vhp on 1=1
--Opport CAV gagnees
left join Cte_Opp_Gagnees_jour_final g on x.jour_alim = g.jour_alim 
left join Cte_Opp_Gagnees_hebdo_final gh on 1=1
left join Cte_Opp_Gagnees_hebdo_prec_final ghp on 1=1
--Opport CAV perdues
left join Cte_Opp_Perdues_jour_final p on x.jour_alim = p.jour_alim 
left join Cte_Opp_Perdues_hebdo_final ph on 1=1
left join Cte_Opp_Perdues_hebdo_prec_final php on 1=1
--Réclamations
left join Cte_Reclamations_Jour r on x.jour_alim = r.DTE_CREA_RECLA
left join Cte_Reclamations_Hebdo rh on r.DTE_CREA_RECLA = rh.datejoin
left join Cte_Reclamations_Hebdo_Prec rhp on r.DTE_CREA_RECLA = rhp.datejoin
where  x.jour_alim > CAST(DATEADD(day, -7, @Date_obs) AS DATE) AND x.jour_alim <= @Date_obs

;

END;