﻿
CREATE PROCEDURE [dbo].[PKGCP_PROC_CAV]
@DTE_OBS date

AS BEGIN

/*
declare @Date_obs date; 
set @Date_obs='2018-05-15'
;*/
SET @DTE_OBS = CAST(@DTE_OBS as date);
SET DATEFIRST 1; -->1    Lundi

With 
cte_jour_alim_list AS
( SELECT T1.StandardDate AS jour_alim
    FROM [dbo].[DIM_TEMPS] T1
    WHERE T1.StandardDate >= CAST(DATEADD(day, -6, @DTE_OBS) AS DATE) AND T1.StandardDate <= @DTE_OBS
)
--select * from cte_jour_alim_list order by 1 desc ;
,CTE_ALL_CAV As
(
SELECT cpt.[DTE_TRAITEMENT]
      ,cpt.[IDE_OPPRT]
      ,cpt.[NUM_CPT_SAB]
      ,cpt.[NUM_CLI_SAB]
      ,cpt.[FLAG_CPT]
      ,cpt.[LABEL_PROD_COM]
      ,cpt.[TYP_CPT_SAB]
      ,cpt.[DTE_OUV]
      ,cpt.[DTE_CLO]
	  ,DATEDIFF(DD,opp.DTE_CREA,cpt.DTE_OUV) as DELAI_TRANSFORMATION
	  ,DATEDIFF(MM,cpt.DTE_OUV,DTE_TRAITEMENT) as ANCIENNETE
      ,cpt.[MTT_PREM_VER]
      ,cpt.[SLD_DTE_CPT]
      ,cpt.[MTT_AUT]
      ,cpt.[MTT_DEP]
      ,cpt.[FLG_DEP_FAC_CAI]
      ,cpt.[FLG_MOB_BAN]
      ,cpt.[ACTIVITE_CPT]
      ,cpt.[LIB_ACTIVITE_CPT]
	  ,opp.[DTE_CREA]
  FROM [dbo].[MKT_COMPTE] cpt
  left join [dbo].[MKT_OPPORTUNITE] opp ON cpt.IDE_OPPRT = opp.IDE_OPPRT
  where DTE_TRAITEMENT  >= CAST(DATEADD(day, -7, @DTE_OBS) AS DATE) AND DTE_TRAITEMENT <= @DTE_OBS
  AND FLAG_CPT = 'CAV'
  )
--select * from CTE_ALL_CAV order by 1 desc ;

--select COUNT(distinct [NUM_CPT_SAB]) from CTE_ALL_CAV where DTE_TRAITEMENT = '2018-02-09'

,CTE_STOCK_CAV_COUNT as
(
Select [DTE_TRAITEMENT]
      , NUM_CPT_SAB
	  , DTE_OUV
	  , DTE_CLO
      ,count(distinct NUM_CPT_SAB)                                 AS NB_CAV
      ,count(case when ACTIVITE_CPT = 4 then NUM_CPT_SAB END)      AS NB_CAV_ACTIF_PLUSPLUS
	  ,count(case when ACTIVITE_CPT = 3 then NUM_CPT_SAB END)      AS NB_CAV_ACTIF_PLUS  
      ,count(case when ACTIVITE_CPT = 2 then NUM_CPT_SAB END)      AS NB_CAV_QUASI_ACTIF 
	  ,count(case when ACTIVITE_CPT = 1 then NUM_CPT_SAB END)      AS NB_CAV_INACTIF 
	  ,count(case when ANCIENNETE <=3 then NUM_CPT_SAB END)        AS NB_CAV_RECENT  
      ,count(case when ACTIVITE_CPT IN (4,3) then NUM_CPT_SAB END) AS PART_CAV_ACTIF
	  ,count(case when ACTIVITE_CPT IN (2,1) then NUM_CPT_SAB END) AS PART_CAV_DISTANCIE
	  ,sum(ISNULL([SLD_DTE_CPT],0)) as ENCOURS
	  , PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY ISNULL([SLD_DTE_CPT],0))   
                            OVER (PARTITION BY [DTE_TRAITEMENT]) AS ENCOURS_MEDIAN_CONT  
      , PERCENTILE_DISC(0.5) WITHIN GROUP (ORDER BY ISNULL([SLD_DTE_CPT],0))   
                            OVER (PARTITION BY [DTE_TRAITEMENT]) AS ENCOURS_MEDIAN_DISC  
FROM CTE_ALL_CAV
where DTE_OUV <= [DTE_TRAITEMENT] and (DTE_CLO is null or DTE_CLO > [DTE_TRAITEMENT]) 
group by [DTE_TRAITEMENT], NUM_CPT_SAB
	  , DTE_OUV
	  , DTE_CLO,[SLD_DTE_CPT]
)  
--select * from CTE_STOCK_CAV_COUNT where [DTE_TRAITEMENT] = '2018-02-09' order by 3 desc ;

,CTE_STOCK_CAV as
(
Select [DTE_TRAITEMENT]
      ,sum(NB_CAV)                                                  as NB_CAV
      ,sum(NB_CAV_ACTIF_PLUSPLUS)                                   AS NB_CAV_ACTIF_PLUSPLUS
	  ,sum(NB_CAV_ACTIF_PLUS)                                       AS NB_CAV_ACTIF_PLUS  
      ,sum(NB_CAV_QUASI_ACTIF)                                      AS NB_CAV_QUASI_ACTIF 
	  ,sum(NB_CAV_INACTIF)                                          AS NB_CAV_INACTIF 
	  ,sum(NB_CAV_RECENT)                                           AS NB_CAV_RECENT  
      ,sum(PART_CAV_ACTIF)                                          AS PART_CAV_ACTIF
	  ,sum(PART_CAV_DISTANCIE)                                      AS PART_CAV_DISTANCIE
	  ,ABS(sum(ENCOURS))                                            as ENCOURS
	  ,ABS(ENCOURS_MEDIAN_CONT)                                     as ENCOURS_MEDIAN_CONT  
      ,ABS(ENCOURS_MEDIAN_DISC)                                     as ENCOURS_MEDIAN_DISC 
	  ,LAG(ABS(sum(ENCOURS)), 1,0) OVER (ORDER BY [DTE_TRAITEMENT]) AS PREVIOUS_ENCOURS    
FROM CTE_STOCK_CAV_COUNT
group by [DTE_TRAITEMENT],ENCOURS_MEDIAN_CONT,ENCOURS_MEDIAN_DISC
)  

--select * from CTE_STOCK_CAV where DTE_TRAITEMENT = '2018-02-09' order by [DTE_TRAITEMENT] desc  ;

/*****Clôture*******/
,CTE_CLOTURE_CAV_CNT as
(
Select  [DTE_TRAITEMENT]
      , count(case when [DTE_TRAITEMENT] = [DTE_CLO] then [NUM_CPT_SAB] END) AS NB_CLOTURE
FROM CTE_ALL_CAV
group by [DTE_TRAITEMENT]
)

--select * from CTE_CLOTURE_CAV_CNT order by 1 desc ;

,CTE_CLOTURE_CAV as
(
Select  [DTE_TRAITEMENT]
      , SUM(NB_CLOTURE) as NB_CLOTURE
FROM CTE_CLOTURE_CAV_CNT
group by [DTE_TRAITEMENT]
)

--select * from CTE_CLOTURE_CAV order by 1 desc;

--Mobilité CARTE
,CTE_MOBILITE_CARTE as
(
select ref.DTE_TRAITEMENT
	  ,count(case when cas.STATUT = '01' then cas.IDE_REQ_SF END )         as NB_DEMANDES_MOB_EN_ATTENTES
	  ,count(case when cas.STATUT IN ('02','03') then cas.IDE_REQ_SF END ) as NB_DEMANDES_MOB_TRAITEES
from [$(DataHubDatabaseName)].exposition.PV_SF_CASE cas
inner join REF_CLIENT ref on cas.IDE_PERS_SF = ref.IDE_PERS_SF and  (cas.DTE_DEBUT_VAL <= ref.DTE_TRAITEMENT and cas.DTE_FIN_VAL > ref.DTE_TRAITEMENT)
inner join CTE_ALL_CAV c on ref.[NUM_CLT_SAB] = c.[NUM_CLI_SAB]  and ref.[DTE_TRAITEMENT] = c.[DTE_TRAITEMENT]
where cas.TYP_REQ = '02'
and cas.COD_TYP_ENR='OBRequest'
and cas.SS_TYP = '186'
group by ref.DTE_TRAITEMENT
)
--select * from CTE_MOBILITE_CARTE;

/************/
/**/
,CTE_OPERATION as 
(
select t.DTE_TRAITEMENT
	  /*OPE CRE*/
	  ,sum(case when t.FLAG_MTT_OPT = 0  then ABS(t.MONTANT_OP) else 0 END) 
		- sum(case when t.FLAG_INITIATIVE_CLIENT= 'DEDUIRE' and t.FLAG_OP_DEB_CRE='Credit' then t.MONTANT_OP else 0 END) AS MTT_OPE_CREDITRICES
	  ,sum (case when t.TYP_MVT='Chèque encaissé' then ABS(t.MONTANT_OP) else 0 END) 
		- sum (case when t.TYP_MVT='Rejet chèque encaissé' then ABS(t.MONTANT_OP) else 0 END)                   as MTT_CHEQUE_ENTRANT
	  ,sum(case when t.FLAG_MTT_OPT = 0  and t.SOUS_TYP_MVT='Virement externe' then ABS(t.MONTANT_OP) else 0 END) 
		- sum(case when t.FLAG_MTT_OPT = 1  and t.SOUS_TYP_MVT='Rejet Virement externe' then ABS(t.MONTANT_OP) else 0 END) as MTT_VIREMENT_EXTERNE_ENTRANT
	  ,sum(case when t.FLAG_MTT_OPT = 0  and t.SOUS_TYP_MVT IN ('Virement permanent','Virement interne ponctuel') then ABS(t.MONTANT_OP) else 0 END) 
		- sum(case when t.FLAG_MTT_OPT = 1  and t.SOUS_TYP_MVT='Rejet Virement interne ponctuel' then ABS(t.MONTANT_OP) else 0 END) as MTT_INTERNE_ENTRANT
	  ,sum (case when t.TYP_MVT IN ('Prime de bienvenue','Prime collaborateur','Prime client orange') then ABS(t.MONTANT_OP) else 0 END) as MTT_PRIMES_VERSEES
	  ,sum (case when t.TYP_MVT = 'Prime de bienvenue' then ABS(t.MONTANT_OP) else 0  END) as  MTT_PRIMES_BIENVENUE
	  ,sum (case when t.TYP_MVT = 'Prime client orange' then ABS(t.MONTANT_OP) else 0 END ) as MTT_PRIMES_CLEINT_OF
	  ,sum (case when t.TYP_MVT = 'Prime collaborateur' then ABS(t.MONTANT_OP) else 0 END ) as MTT_PRIMES_OFFRE_SALARIES
	  ,sum (case when t.FLAG_MTT_OPT = 0 and t.TYP_MVT not in ('Chèque encaissé','Rejet chèque encaissé',
										                       'Virement','Rejet virement',
										                       'Prime de bienvenue','Prime collaborateur','Prime client orange') 
										                       then ABS(t.MONTANT_OP) else 0 END ) as MTT_AUTRES
	  ,0 as MTT_MEDIAN_OPE_CRE
	  --Nombre 
	  ,sum(case when t.FLAG_MTT_OPT = 0  then t.NOMBRE_OP else 0 END) 
		- sum(case when t.FLAG_INITIATIVE_CLIENT= 'DEDUIRE' and t.FLAG_OP_DEB_CRE='Credit' then t.NOMBRE_OP else 0 END) AS NB_OPE_CREDITRICES  
	  ,sum (case when t.TYP_MVT='Chèque encaissé' then t.NOMBRE_OP  else 0 END) 
		- sum (case when t.TYP_MVT='Rejet chèque encaissé' then t.NOMBRE_OP  else 0 END) as NB_CHEQUE_ENCAISSE
	  ,sum(case when t.FLAG_MTT_OPT = 0  and t.SOUS_TYP_MVT='Virement externe' then t.NOMBRE_OP  else 0 END)	
		- sum(case when t.FLAG_MTT_OPT = 1  and t.SOUS_TYP_MVT='Rejet Virement externe' then t.NOMBRE_OP  else 0 END) as NB_VIREMENT_EXTERNE_ENTRANT
	  ,sum(case when t.FLAG_MTT_OPT = 0  and t.SOUS_TYP_MVT IN ('Virement permanent','Virement interne ponctuel') then t.NOMBRE_OP else 0 END) 
		- sum(case when t.FLAG_MTT_OPT = 1  and t.SOUS_TYP_MVT='Rejet Virement interne ponctuel' then t.NOMBRE_OP else 0 END) as NB_INTERNE_ENTRANT
	  ,sum (case when t.TYP_MVT IN ('Prime de bienvenue','Prime collaborateur','Prime client orange') then t.NOMBRE_OP else 0 END) as NB_PRIMES_VERSEES
	  ,sum (case when t.TYP_MVT = 'Prime de bienvenue' then  t.NOMBRE_OP else 0 END)  as NB_PRIMES_BIENVENUE
	  ,sum (case when t.TYP_MVT = 'Prime client orange' then t.NOMBRE_OP else 0 END) as NB_PRIMES_CLEINT_OF
	  ,sum (case when t.TYP_MVT = 'Prime collaborateur' then t.NOMBRE_OP else 0 END) as NB_PRIMES_OFFRE_SALARIES
	  ,sum (case when t.FLAG_MTT_OPT = 0 and t.TYP_MVT not in ('Chèque encaissé','Rejet chèque encaissé',
										                       'Virement','Rejet virement',
										                       'Prime de bienvenue','Prime collaborateur','Prime client orange') 
										                       then t.NOMBRE_OP else 0 END ) as NB_AUTRES
	  
	  /*OPE DEB*/
	  ,sum(case when t.FLAG_MTT_OPT = 1  then t.MONTANT_OP else 0 END) 
		- sum(case when t.FLAG_INITIATIVE_CLIENT= 'DEDUIRE' and t.FLAG_OP_DEB_CRE='Debit' then  t.MONTANT_OP else 0 END) AS MTT_OPE_DEBETRICES
	  ,sum (case when t.TYP_MVT='Chèque émis' then ABS(t.MONTANT_OP) else 0 END) 
		- sum (case when t.TYP_MVT='Rejet chèque émis' then ABS(t.MONTANT_OP) else 0 END)                      as MTT_CHEQUE_EMIS
	  ,sum (case when t.FLAG_MTT_OPT = 1 and t.TYP_MVT='Virement' and t.SOUS_TYP_MVT not in ('Virement SMS interne','Virement SMS externe') then ABS(t.MONTANT_OP) else 0 END) 
		- sum (case when t.FLAG_MTT_OPT = 0 and t.TYP_MVT='Rejet virement' and t.SOUS_TYP_MVT not in ('Rejet Virement SMS interne','Rejet Virement SMS externe') then ABS(t.MONTANT_OP) else 0 END) as MTT_VIREMENT_EMIS
	  ,sum (case when t.FLAG_MTT_OPT = 1 and t.TYP_MVT='Virement' and t.SOUS_TYP_MVT in ('Virement SMS interne','Virement SMS externe') then ABS(t.MONTANT_OP) else 0 END) 
		- sum (case when t.FLAG_MTT_OPT = 0 and t.TYP_MVT='Rejet virement' and t.SOUS_TYP_MVT in ('Rejet Virement SMS interne','Rejet Virement SMS externe') then ABS(t.MONTANT_OP) else 0 END) as MTT_VIREMENT_SMS_EMIS
	  ,sum (case when t.TYP_MVT='Prélevement' then ABS(t.MONTANT_OP) else 0 END) 
		- sum (case when t.TYP_MVT='Rejet prélevement' then ABS(t.MONTANT_OP) else 0 END) as MTT_PRELEVEMENT
	  ,sum (case when t.FLAG_MTT_OPT = 1 and t.TYP_MVT='Paiement CB' and t.SOUS_TYP_MVT in ('Paiement CB Différé (compte de passage)','Paiement CB Immédiat Achat','Paiement CB Immédiat Retrait') then ABS(t.MONTANT_OP) else 0 END) 
		- sum (case when t.FLAG_MTT_OPT = 0 and t.TYP_MVT='Remboursement Paiement CB' then ABS(t.MONTANT_OP) else 0 END) as MTT_CB
	  ,sum (case when t.TYP_MVT='Paiement mobile' then ABS(t.MONTANT_OP) else 0 END)       as MTT_PM
	  ,sum (case when t.SOUS_TYP_MVT='Frais inactivité' then ABS(t.MONTANT_OP) else 0 END) as MTT_NON_ACTIVITE
	  ,sum (case when t.TYP_MVT not in ('Chèque émis','Virement','Rejet virement','Prélevement','Rejet prélevement','Paiement CB','Remboursement Paiement CB','Paiement mobile','Opérations facturées') then ABS(t.MONTANT_OP) else 0 END) as MTT_AUTRE
	  ,0 as MTT_MEDIAN
	  --Nombre
	  ,sum(case when t.FLAG_MTT_OPT = 1  then t.NOMBRE_OP else 0 END) - sum(case when t.FLAG_INITIATIVE_CLIENT= 'DEDUIRE' and t.FLAG_OP_DEB_CRE='Debit' then  t.NOMBRE_OP else 0 END) AS NB_OPE_DEBETRICES 
	  ,sum (case when t.TYP_MVT='Chèque émis' then t.NOMBRE_OP else 0 END) - sum (case when t.TYP_MVT='Rejet chèque émis' then t.NOMBRE_OP else 0 END) as NB_CHEQUE_EMIS
	  ,sum (case when t.FLAG_MTT_OPT = 1 and t.TYP_MVT='Virement' and t.SOUS_TYP_MVT not in ('Virement SMS interne','Virement SMS externe') then t.NOMBRE_OP else 0 END) 
		- sum (case when t.FLAG_MTT_OPT = 0 and t.TYP_MVT='Rejet virement' and t.SOUS_TYP_MVT not in ('Rejet Virement SMS interne','Rejet Virement SMS externe') then t.NOMBRE_OP else 0 END) as NB_VIREMENT_EMIS
	  ,sum (case when t.FLAG_MTT_OPT = 1 and t.TYP_MVT='Virement' and t.SOUS_TYP_MVT in ('Virement SMS interne','Virement SMS externe') then t.NOMBRE_OP else 0 END) 
		- sum (case when t.FLAG_MTT_OPT = 0 and t.TYP_MVT='Rejet virement' and t.SOUS_TYP_MVT in ('Rejet Virement SMS interne','Rejet Virement SMS externe') then t.NOMBRE_OP else 0 END) as NB_VIREMENT_SMS_EMIS
	  ,sum (case when t.TYP_MVT='Prélevement' then t.NOMBRE_OP else 0 END) - sum (case when t.TYP_MVT='Rejet prélevement' then t.NOMBRE_OP else 0 END) as NB_PRELEVEMENT
	  ,sum (case when t.FLAG_MTT_OPT = 1 and t.TYP_MVT='Paiement CB' and t.SOUS_TYP_MVT in ('Paiement CB Différé (compte de passage)','Paiement CB Immédiat Achat','Paiement CB Immédiat Retrait') then t.NOMBRE_OP else 0 END) 
		- sum (case when t.FLAG_MTT_OPT = 0 and t.TYP_MVT='Remboursement Paiement CB' then t.NOMBRE_OP else 0 END) as NB_CB
	  ,sum (case when t.TYP_MVT='Paiement mobile' then t.NOMBRE_OP else 0 END) as NB_PM
	  ,sum (case when t.SOUS_TYP_MVT='Frais inactivité' then t.NOMBRE_OP else 0 END)  as NB_NON_ACTIVITE
	  ,sum (case when t.TYP_MVT not in ('Chèque émis','Virement','Rejet virement','Prélevement','Rejet prélevement','Paiement CB','Remboursement Paiement CB','Paiement mobile','Opérations facturées') then t.NOMBRE_OP else 0 END) as NB_AUTRE

from T_FACT_MKT_USAGE_CPT_DAY t
inner join CTE_ALL_CAV c  on  t.NUM_CPT_SAB = c.NUM_CPT_SAB and t.DTE_TRAITEMENT = c.DTE_TRAITEMENT
where c.DTE_OUV <= t.[DTE_TRAITEMENT] and (c.DTE_CLO is null or c.DTE_CLO > t.[DTE_TRAITEMENT]) --Compte ouvert !!! a tester
group by t.DTE_TRAITEMENT
)		

--select * from CTE_OPERATION;

select   x.jour_alim
	   , stock.DTE_TRAITEMENT
	   , ope.DTE_TRAITEMENT as DTE_TRAITEMENT_OPE
	   , clo.DTE_TRAITEMENT as DTE_TRAITEMENT_CLO
        --STOCK
       , ISNULL(stock.NB_CAV                 ,0) as NB_CAV
	   , ISNULL(stock.NB_CAV_ACTIF_PLUSPLUS	 ,0) as NB_CAV_ACTIF_PLUSPLUS
	   , ISNULL(stock.NB_CAV_ACTIF_PLUS  	 ,0) as NB_CAV_ACTIF_PLUS  
	   , ISNULL(stock.NB_CAV_QUASI_ACTIF 	 ,0) as NB_CAV_QUASI_ACTIF 
	   , ISNULL(stock.NB_CAV_INACTIF 		 ,0) as NB_CAV_INACTIF 
	   , ISNULL(stock.NB_CAV_RECENT  		 ,0) as NB_CAV_RECENT  
	   , ISNULL(stock.PART_CAV_ACTIF		 ,0) as PART_CAV_ACTIF
       , ISNULL(stock.PART_CAV_DISTANCIE	 ,0) as PART_CAV_DISTANCIE
       , ISNULL(stock.ENCOURS				 ,0) as ENCOURS
       , ISNULL(stock.ENCOURS_MEDIAN_CONT  	 ,0) as ENCOURS_MEDIAN_CONT  
       , ISNULL(stock.ENCOURS_MEDIAN_DISC 	 ,0) as ENCOURS_MEDIAN_DISC 
       , ISNULL(stock.PREVIOUS_ENCOURS    	 ,0) as PREVIOUS_ENCOURS    
       --CLOTURE
	   , ISNULL(clo.NB_CLOTURE,0)  as NB_CLOTURE
	   --MOBILITE
	   , ISNULL(NB_DEMANDES_MOB_EN_ATTENTES,0)  as NB_DEMANDES_MOB_EN_ATTENTES
	   , ISNULL(NB_DEMANDES_MOB_TRAITEES,0) as NB_DEMANDES_MOB_TRAITEES
		--OPERATION
	   , ISNULL(ope.MTT_OPE_CREDITRICES              ,0) as MTT_OPE_CREDITRICES
	   , ISNULL(ope.MTT_CHEQUE_ENTRANT				 ,0) as MTT_CHEQUE_ENTRANT
	   , ISNULL(ope.MTT_VIREMENT_EXTERNE_ENTRANT	 ,0) as MTT_VIREMENT_EXTERNE_ENTRANT
	   , ISNULL(ope.MTT_INTERNE_ENTRANT				 ,0) as MTT_INTERNE_ENTRANT
	   , ISNULL(ope.MTT_PRIMES_VERSEES				 ,0) as MTT_PRIMES_VERSEES
	   , ISNULL(ope.MTT_PRIMES_BIENVENUE			 ,0) as MTT_PRIMES_BIENVENUE
	   , ISNULL(ope.MTT_PRIMES_CLEINT_OF			 ,0) as MTT_PRIMES_CLEINT_OF
	   , ISNULL(ope.MTT_PRIMES_OFFRE_SALARIES		 ,0) as MTT_PRIMES_OFFRE_SALARIES
	   , ISNULL(ope.MTT_AUTRES						 ,0) as MTT_AUTRES
	   , ISNULL(ope.MTT_MEDIAN_OPE_CRE				 ,0) as MTT_MEDIAN_OPE_CRE
	   , ISNULL(ope.NB_OPE_CREDITRICES				 ,0) as NB_OPE_CREDITRICES
	   , ISNULL(ope.NB_CHEQUE_ENCAISSE				 ,0) as NB_CHEQUE_ENCAISSE
	   , ISNULL(ope.NB_VIREMENT_EXTERNE_ENTRANT		 ,0) as NB_VIREMENT_EXTERNE_ENTRANT
	   , ISNULL(ope.NB_INTERNE_ENTRANT				 ,0) as NB_INTERNE_ENTRANT
	   , ISNULL(ope.NB_PRIMES_VERSEES				 ,0) as NB_PRIMES_VERSEES
	   , ISNULL(ope.NB_PRIMES_BIENVENUE				 ,0) as NB_PRIMES_BIENVENUE
	   , ISNULL(ope.NB_PRIMES_CLEINT_OF				 ,0) as NB_PRIMES_CLEINT_OF
	   , ISNULL(ope.NB_PRIMES_OFFRE_SALARIES		 ,0) as NB_PRIMES_OFFRE_SALARIES
	   , ISNULL(ope.NB_AUTRES						 ,0) as NB_AUTRES
	   , ISNULL(ope.MTT_OPE_DEBETRICES				 ,0) as MTT_OPE_DEBETRICES
	   , ISNULL(ope.MTT_CHEQUE_EMIS					 ,0) as MTT_CHEQUE_EMIS
	   , ISNULL(ope.MTT_VIREMENT_EMIS				 ,0) as MTT_VIREMENT_EMIS
	   , ISNULL(ope.MTT_VIREMENT_SMS_EMIS			 ,0) as MTT_VIREMENT_SMS_EMIS
	   , ISNULL(ope.MTT_PRELEVEMENT					 ,0) as MTT_PRELEVEMENT
	   , ISNULL(ope.MTT_CB							 ,0) as MTT_CB
	   , ISNULL(ope.MTT_PM							 ,0) as MTT_PM
	   , ISNULL(ope.MTT_NON_ACTIVITE				 ,0) as MTT_NON_ACTIVITE
	   , ISNULL(ope.MTT_AUTRE						 ,0) as MTT_AUTRE
	   , ISNULL(ope.MTT_MEDIAN						 ,0) as MTT_MEDIAN
	   , ISNULL(ope.NB_OPE_DEBETRICES				 ,0) as NB_OPE_DEBETRICES
	   , ISNULL(ope.NB_CHEQUE_EMIS					 ,0) as NB_CHEQUE_EMIS
	   , ISNULL(ope.NB_VIREMENT_EMIS				 ,0) as NB_VIREMENT_EMIS
	   , ISNULL(ope.NB_VIREMENT_SMS_EMIS			 ,0) as NB_VIREMENT_SMS_EMIS
	   , ISNULL(ope.NB_PRELEVEMENT					 ,0) as NB_PRELEVEMENT
	   , ISNULL(ope.NB_CB							 ,0) as NB_CB
	   , ISNULL(ope.NB_PM							 ,0) as NB_PM
	   , ISNULL(ope.NB_NON_ACTIVITE					 ,0) as NB_NON_ACTIVITE
	   , ISNULL(ope.NB_AUTRE						 ,0) as NB_AUTRE
        
FROM cte_jour_alim_list x
LEFT JOIN CTE_STOCK_CAV stock on x.jour_alim = stock.DTE_TRAITEMENT
LEFT JOIN CTE_CLOTURE_CAV clo on x.jour_alim = clo.DTE_TRAITEMENT
LEFT JOIN CTE_MOBILITE_CARTE mb on x.jour_alim = mb.DTE_TRAITEMENT
LEFT JOIN CTE_OPERATION ope on x.jour_alim =   ope.DTE_TRAITEMENT
ORDER BY x.jour_alim		

END;