﻿CREATE PROC [dbo].[PROC_SSRS_SAS_VUE_CLI]

AS  BEGIN 

DECLARE @date_obs DATE;
SELECT  @date_obs = CAST(GETDATE() AS DATE);
DECLARE @date_max date;
SELECT  @date_max = dateadd(day, -1 ,GETDATE());
SET DATEFIRST 1; -->1    Lundi
DROP TABLE IF EXISTS #rq_cli_cp;
DROP TABLE IF EXISTS [dbo].[WK_SSRS_SAS_VUE_CLI];

SELECT Year_alim,Month_alim,Week_alim,jour_alim
    ,coalesce(nb_clients_ouvert,0) AS nb_clients_ouvert
	,coalesce(nb_clients_ferm,0) AS nb_clients_ferm
	,coalesce(nb_clients_eq_pm				,0) AS nb_clients_eq_pm 
	,coalesce(nb_clients_pre_attr		,0) AS nb_clients_pre_attr
	,coalesce(nb_clients_score_er_1star	,0) AS nb_clients_score_er_1star
	,coalesce(nb_clients_score_er_2star	,0) AS nb_clients_score_er_2star
	,coalesce(nb_clients_score_er_3star	,0) AS nb_clients_score_er_3star

    ,sum(coalesce(nb_clients_ouvert, 0)) OVER(ORDER BY jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_ouv_cli_cum
    ,sum(coalesce(nb_clients_ferm, 0)) OVER(ORDER BY jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_clients_clos
    ,sum(coalesce(nb_clients_ouvert,0) - coalesce(nb_clients_ferm, 0) ) OVER(ORDER BY jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_clients_parc

	--Week to date
	,sum(coalesce(nb_clients_ouvert,0)) OVER (PARTITION BY  Week_alim ORDER BY  Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_clients_ouvert_WTD --Week to date
	,sum(coalesce(nb_clients_ferm,0)) OVER (PARTITION BY  Week_alim ORDER BY  Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_clients_ferm_WTD --Week to date
	,sum(coalesce(nb_clients_eq_pm,0)) OVER (PARTITION BY  Week_alim ORDER BY  Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_clients_eq_pm_WTD --Week to date
	,sum(coalesce(nb_clients_pre_attr,0)) OVER (PARTITION BY  Week_alim ORDER BY  Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_clients_pre_attr_WTD --Week to date
	,sum(coalesce(nb_clients_score_er_1star,0)) OVER (PARTITION BY  Week_alim ORDER BY  Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_clients_score_er_1star_WTD --Week to date
	,sum(coalesce(nb_clients_score_er_2star,0)) OVER (PARTITION BY  Week_alim ORDER BY  Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_clients_score_er_2star_WTD --Week to date
	,sum(coalesce(nb_clients_score_er_3star,0)) OVER (PARTITION BY  Week_alim ORDER BY  Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_clients_score_er_3star_WTD --Week to date
	--Month to date
	,sum(coalesce(nb_clients_ouvert,0)) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_clients_ouvert_MTD --Month to date
	,sum(coalesce(nb_clients_ferm,0)) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_clients_ferm_MTD --Month to date
	,sum(coalesce(nb_clients_eq_pm,0)) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_clients_eq_pm_MTD --Month to date
	,sum(coalesce(nb_clients_pre_attr,0)) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_clients_pre_attr_MTD --Month to date
	,sum(coalesce(nb_clients_score_er_1star,0)) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_clients_score_er_1star_MTD --Month to date
	,sum(coalesce(nb_clients_score_er_2star,0)) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_clients_score_er_2star_MTD --Month to date
	,sum(coalesce(nb_clients_score_er_3star,0)) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_clients_score_er_3star_MTD --Month to date

	--Full Month
	,sum(coalesce(nb_clients_ouvert,0)) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_clients_ouvert_FM --Full Month
	,sum(coalesce(nb_clients_ferm,0)) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_clients_ferm_FM --Full Month
	,sum(coalesce(nb_clients_eq_pm,0)) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_clients_eq_pm_FM --Full Month
	,sum(coalesce(nb_clients_pre_attr,0)) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_clients_pre_attr_FM --Full Month
	,sum(coalesce(nb_clients_score_er_1star,0)) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_clients_score_er_1star_FM --Full Month
	,sum(coalesce(nb_clients_score_er_2star,0)) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_clients_score_er_2star_FM --Full Month
	,sum(coalesce(nb_clients_score_er_3star,0)) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_clients_score_er_3star_FM --Full Month
	--Year to date
    ,sum(coalesce(nb_clients_ouvert,0)) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_clients_ouvert_YTD  --Year to date
    ,sum(coalesce(nb_clients_ferm,0)) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_clients_ferm_YTD  --Year to date
    ,sum(coalesce(nb_clients_eq_pm,0)) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_clients_eq_pm_YTD  --Year to date
    ,sum(coalesce(nb_clients_pre_attr,0)) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_clients_pre_attr_YTD  --Year to date
    ,sum(coalesce(nb_clients_score_er_1star,0)) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_clients_score_er_1star_YTD  --Year to date
    ,sum(coalesce(nb_clients_score_er_2star,0)) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_clients_score_er_2star_YTD  --Year to date
    ,sum(coalesce(nb_clients_score_er_3star,0)) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_clients_score_er_3star_YTD  --Year to date
	--Full year
	,sum(coalesce(nb_clients_ouvert,0)) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_clients_ouvert_FY  --Full year
	,sum(coalesce(nb_clients_ferm,0)) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_clients_ferm_FY  --Full year
	,sum(coalesce(nb_clients_eq_pm,0)) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_clients_eq_pm_FY  --Full year
	,sum(coalesce(nb_clients_pre_attr,0)) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_clients_pre_attr_FY  --Full year
	,sum(coalesce(nb_clients_score_er_1star,0)) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_clients_score_er_1star_FY  --Full year
	,sum(coalesce(nb_clients_score_er_2star,0)) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_clients_score_er_2star_FY  --Full year
	,sum(coalesce(nb_clients_score_er_3star,0)) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_clients_score_er_3star_FY  --Full year
INTO #rq_cli_cp
	FROM  ( 
     SELECT  T1.StandardDate as jour_alim,DATEPART(YEAR,T1.Date) AS Year_alim,DATEPART(MONTH,T1.Date) AS Month_alim,DATEPART(YEAR,DATEADD(day, (7 - DATEPART(dw, T1.StandardDate)), T1.StandardDate)) *100 + DATEPART(WEEK,DATEADD(day, (7 - DATEPART(dw, T1.StandardDate)), T1.StandardDate)) AS Week_alim
         , sum(nb_clients_ouvert) AS nb_clients_ouvert
         , sum(nb_clients_ferm) AS nb_clients_ferm
         , sum(flag_pm_rem) AS nb_clients_eq_pm
         , sum(case when Preattribution__c is not null then nb_clients_ouvert else 0 end) AS nb_clients_pre_attr
         , sum(case when RelationEntryScore__c = 4 then nb_clients_ouvert else 0 end) AS nb_clients_score_er_1star
         , sum(case when RelationEntryScore__c = 3 then nb_clients_ouvert else 0 end) AS nb_clients_score_er_2star
         , sum(case when RelationEntryScore__c = 2 then nb_clients_ouvert else 0 end) AS nb_clients_score_er_3star
FROM (
SELECT  CASE WHEN StandardDate >= CAST(DATEADD(yyyy, DATEDIFF(YYYY, 0, DATEADD(yyyy, -2, @date_obs)), 0) AS DATE) THEN StandardDate
        ELSE CONVERT(DATE, '20151220', 112) end AS StandardDate, Date,StandardDate as StandardDate1
FROM [dbo].[DIM_TEMPS]) T1 
LEFT OUTER JOIN  sas_vue_cli  ON T1.StandardDate1=sas_vue_cli.jour_alim
WHERE  ( T1.StandardDate1 <=  DATEADD(day, (7 - DATEPART(dw, @date_obs)), @date_obs) )
GROUP BY  T1.StandardDate ,DATEPART(YEAR,T1.Date) ,DATEPART(MONTH,T1.Date) ,DATEPART(WEEK,T1.Date) 
) t	;	

SELECT
  CY.jour_alim
, CY.Year_alim
, CY.Month_alim
, CY.Week_alim
, coalesce(CY.nb_ouv_cli_cum,0) AS nb_ouv_cli_cum
, coalesce(CY.nb_clients_clos,0) AS nb_clients_clos
, coalesce(CY.nb_clients_parc,0) AS nb_clients_parc
, coalesce(CY.nb_clients_ouvert,0) AS nb_clients_ouvert
, coalesce(CY.nb_clients_ouvert_WTD,0) AS nb_clients_ouvert_WTD
, coalesce(CY.nb_clients_ouvert_MTD,0) AS nb_clients_ouvert_MTD
, coalesce(CY.nb_clients_ouvert_YTD,0) AS nb_clients_ouvert_YTD
, coalesce(PM.nb_clients_ouvert_FM,0) AS nb_clients_ouvert_FM
, coalesce(PY.nb_clients_ouvert_FY,0) AS nb_clients_ouvert_FY
, coalesce(CY.nb_clients_ferm,0) AS nb_clients_ferm
, coalesce(CY.nb_clients_ferm_WTD,0) AS nb_clients_ferm_WTD
, coalesce(CY.nb_clients_ferm_MTD,0) AS nb_clients_ferm_MTD
, coalesce(CY.nb_clients_ferm_YTD,0) AS nb_clients_ferm_YTD
, coalesce(PM.nb_clients_ferm_FM,0) AS nb_clients_ferm_FM
, coalesce(PY.nb_clients_ferm_FY,0) AS nb_clients_ferm_FY
, coalesce(CY.nb_clients_eq_pm,0) AS nb_clients_eq_pm
, coalesce(CY.nb_clients_eq_pm_WTD,0) AS nb_clients_eq_pm_WTD
, coalesce(CY.nb_clients_eq_pm_MTD,0) AS nb_clients_eq_pm_MTD
, coalesce(CY.nb_clients_eq_pm_YTD,0) AS nb_clients_eq_pm_YTD
, coalesce(PM.nb_clients_eq_pm_FM,0) AS nb_clients_eq_pm_FM
, coalesce(PY.nb_clients_eq_pm_FY,0) AS nb_clients_eq_pm_FY
, coalesce(CY.nb_clients_pre_attr,0) AS nb_clients_pre_attr
, coalesce(CY.nb_clients_pre_attr_WTD,0) AS nb_clients_pre_attr_WTD
, coalesce(CY.nb_clients_pre_attr_MTD,0) AS nb_clients_pre_attr_MTD
, coalesce(CY.nb_clients_pre_attr_YTD,0) AS nb_clients_pre_attr_YTD
, coalesce(PM.nb_clients_pre_attr_FM,0) AS nb_clients_pre_attr_FM
, coalesce(PY.nb_clients_pre_attr_FY,0) AS nb_clients_pre_attr_FY
, coalesce(CY.nb_clients_score_er_1star,0) AS nb_clients_score_er_1star
, coalesce(CY.nb_clients_score_er_1star_WTD,0) AS nb_clients_score_er_1star_WTD
, coalesce(CY.nb_clients_score_er_1star_MTD,0) AS nb_clients_score_er_1star_MTD
, coalesce(CY.nb_clients_score_er_1star_YTD,0) AS nb_clients_score_er_1star_YTD
, coalesce(PM.nb_clients_score_er_1star_FM,0) AS nb_clients_score_er_1star_FM
, coalesce(PY.nb_clients_score_er_1star_FY,0) AS nb_clients_score_er_1star_FY
, coalesce(CY.nb_clients_score_er_2star,0) AS nb_clients_score_er_2star
, coalesce(CY.nb_clients_score_er_2star_WTD,0) AS nb_clients_score_er_2star_WTD
, coalesce(CY.nb_clients_score_er_2star_MTD,0) AS nb_clients_score_er_2star_MTD
, coalesce(CY.nb_clients_score_er_2star_YTD,0) AS nb_clients_score_er_2star_YTD
, coalesce(PM.nb_clients_score_er_2star_FM,0) AS nb_clients_score_er_2star_FM
, coalesce(PY.nb_clients_score_er_2star_FY,0) AS nb_clients_score_er_2star_FY
, coalesce(CY.nb_clients_score_er_3star,0) AS nb_clients_score_er_3star
, coalesce(CY.nb_clients_score_er_3star_WTD,0) AS nb_clients_score_er_3star_WTD
, coalesce(CY.nb_clients_score_er_3star_MTD,0) AS nb_clients_score_er_3star_MTD
, coalesce(CY.nb_clients_score_er_3star_YTD,0) AS nb_clients_score_er_3star_YTD
, coalesce(PM.nb_clients_score_er_3star_FM,0) AS nb_clients_score_er_3star_FM
, coalesce(PY.nb_clients_score_er_3star_FY,0) AS nb_clients_score_er_3star_FY
INTO [dbo].[WK_SSRS_SAS_VUE_CLI]
FROM #rq_cli_cp CY --Current Year
 LEFT OUTER JOIN #rq_cli_cp PM --Previous Year
		  ON DATEADD(MONTH,-1,CY.jour_alim) = PM.jour_alim
	 LEFT OUTER JOIN #rq_cli_cp PY --Previous Year
		  ON DATEADD(YEAR,-1,CY.jour_alim) = PY.jour_alim
ORDER BY CY.jour_alim;
END