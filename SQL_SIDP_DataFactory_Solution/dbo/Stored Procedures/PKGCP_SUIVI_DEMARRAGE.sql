﻿/**************************************************************************************************************************************************************************************/
CREATE PROC [dbo].[PKGCP_SUIVI_DEMARRAGE]
   @date_obs date,
   @date_debut date,
   @date_fin date
AS  BEGIN 

with 
cte as (
SELECT cast([date_creat_opp] as date) as [date_opp]
      ,[nb_opport] as [nb_opport_cree]
      ,case when [stade_vente] in ('Compte ouvert'/*,'Affaire conclue'*/) and cast([date_fin_opp] as date) = cast([date_creat_opp] as date) then [nb_opport] else 0 end as [nb_opport_ferm_meme_jour]
      --,case when [stade_vente] in ('Compte ouvert'/*,'Affaire conclue'*/) and cast([date_fin_opp] as date) <= cast([jour_alim] as date) then [nb_opport] else 0 end as [nb_opport_ferm_a_date]
	  ,case when [stade_vente] = 'Compte ouvert' then [nb_opport] else 0 end as [nb_opport_ferm_a_date]
      ,0 as [nb_opport_ferm]
      ,[age_opport]
      ,[flag_full_digi]
      ,[flag_full_btq]
      ,[flag_indication]
      ,[flag_identif_OF]
      ,[flag_facture_OF]
      ,CASE WHEN [flag_indication] = 'Oui' AND [reseau_origine] = 'Orange France' THEN (CASE [sous_reseau_origine] WHEN 'AD' THEN 'Agence' WHEN 'GDT' THEN 'Agence' WHEN 'OF-WEB' THEN 'Digital' ELSE 'Autres' END) ELSE [canal_interact_origine] END AS canal_interact_origine
	  ,CASE WHEN [flag_indication] = 'Oui' AND [reseau_origine] = 'Orange France' THEN (CASE [sous_reseau_origine] WHEN 'AD' THEN 'Agence' WHEN 'GDT' THEN 'Agence' WHEN 'OF-WEB' THEN 'Digital' ELSE 'Autres' END) ELSE [canal_distrib_origine] END AS canal_distrib_origine
      ,[sous_reseau_origine]
      ,[reseau_origine]
      ,[canal_interact_final]
      ,[canal_distrib_final]
      ,[sous_reseau_final]
      ,[reseau_final]
  FROM [dbo].[sas_vue_stock_opport]
 WHERE [jour_alim] = @date_obs
   AND @date_debut <= cast([date_creat_opp] as date) and cast([date_creat_opp] as date) <= @date_fin 
   AND [commercial_offer_code] = 'OC80' 
UNION ALL
SELECT cast([date_fin_opp] as date) as [date_opp]
      ,0 as [nb_opport_cree]
      ,0 as [nb_opport_ferm_meme_jour]
      ,0 as [nb_opport_ferm_a_date]
      ,[nb_opport] as [nb_opport_ferm]
      ,[age_opport]
      ,[flag_full_digi]
      ,[flag_full_btq]
      ,[flag_indication]
      ,[flag_identif_OF]
      ,[flag_facture_OF]
      ,CASE WHEN [flag_indication] = 'Oui' AND [reseau_origine] = 'Orange France' THEN (CASE [sous_reseau_origine] WHEN 'AD' THEN 'Agence' WHEN 'GDT' THEN 'Agence' WHEN 'OF-WEB' THEN 'Digital' ELSE 'Autres' END) ELSE [canal_interact_origine] END AS canal_interact_origine
	  ,CASE WHEN [flag_indication] = 'Oui' AND [reseau_origine] = 'Orange France' THEN (CASE [sous_reseau_origine] WHEN 'AD' THEN 'Agence' WHEN 'GDT' THEN 'Agence' WHEN 'OF-WEB' THEN 'Digital' ELSE 'Autres' END) ELSE [canal_distrib_origine] END AS canal_distrib_origine
      ,[sous_reseau_origine]
      ,[reseau_origine]
      ,[canal_interact_final]
      ,[canal_distrib_final]
      ,[sous_reseau_final]
      ,[reseau_final]
  FROM [dbo].[sas_vue_stock_opport]
 WHERE [jour_alim] = @date_obs
   AND @date_debut <= cast([date_fin_opp] as date) and cast([date_fin_opp] as date) <= @date_fin
   AND [stade_vente] in ('Compte ouvert'/*,'Affaire conclue'*/)
   AND [commercial_offer_code] = 'OC80' 
)

select * from cte

END