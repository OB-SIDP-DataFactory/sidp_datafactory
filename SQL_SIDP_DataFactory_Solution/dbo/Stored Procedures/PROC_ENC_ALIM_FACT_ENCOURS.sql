﻿CREATE PROCEDURE [dbo].[Enc_alim_fact_encours] (@date_traitement DATE, 
                                               @ETLName         VARCHAR(150), 
                                               @FileOrTableName VARCHAR(150)) 
AS 
  BEGIN 
      DECLARE @LOG_IDENTITY INT; 

      INSERT INTO [$(DataHubDatabaseName)].[dbo].data_log 
                  (processingtype, 
                   etlname, 
                   fileortablename, 
                   processingstartdate) 
      VALUES      ( 'Alim enc_fact_encours', 
                    @ETLName, 
                    @FileOrTableName, 
                    @date_traitement ) 

      SET @LOG_IDENTITY= Scope_identity(); 

      DELETE FROM dbo.ENC_FACT_ENCOURS 
      WHERE
		ID_TECH_TEMPS IN
		(
			SELECT
				PK_ID
			FROM
				dbo.DIM_TEMPS	WITH(NOLOCK)
			WHERE
				DATEDIFF(DAY, StandardDate, @date_traitement) = 0
		);

	  With PV_SA_COMPTE_ALL
	  AS 
	  (
	  Select * FROM [$(DataHubDatabaseName)].[dbo].PV_SA_Q_COMPTE	WITH(NOLOCK)
	  UNION ALL 
	  Select * FROM [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_COMPTEHistory]	WITH(NOLOCK)
	  )
      INSERT INTO dbo.ENC_FACT_ENCOURS 
                  ([ID_TECH_PRODUIT], 
                   [ID_TECH_TEMPS], 
                   [ID_TECH_MARCHE], 
                   [ID_TECH_RESEAU], 
                   [SOLDE_COMPTABLE], 
                   [NUMERO_COMPTE], 
                   [SENS_DU_SOLDE]) 
      SELECT PRD.ID_TECH   AS ID_TECH_PRODUIT, 
             TPS.PK_ID     AS ID_TECH_TEMPS, 
             ISNULL(MAR.ID_TECH,-1)   AS ID_TECH_MARCHE, 
             ISNULL(RES.ID_TECH,-1)   AS ID_TECH_RESEAU, 
             CPT.DWHCPTCPT AS SOLDE_COMPTABLE, 
             CPT.DWHCPTCOM AS NUMERO_COMPTE, 
             CASE 
               WHEN CPT.DWHCPTCPT > 0 THEN 'DEBITEUR' 
               ELSE 'CREDITEUR' 
             END           AS SENS_DU_SOLDE 
      FROM   PV_SA_COMPTE_ALL AS CPT WITH(NOLOCK) 
             INNER JOIN [$(DataHubDatabaseName)].[dbo].[VW_PV_SA_Q_CLIENT] AS CLI WITH(NOLOCK) 
                     ON CPT.DWHCPTPPAL = CLI.DWHCLICLI 
             INNER JOIN dbo.ENC_DIM_PRODUIT AS PRD WITH(NOLOCK) 
                     ON CPT.DWHCPTRUB = PRD.RUB_COMPTABLE 
             LEFT JOIN dbo.ENC_DIM_MARCHE AS MAR WITH(NOLOCK) 
                    ON CLI.DWHCLIRES = MAR.CODE_RESPONSABLE 
             LEFT JOIN dbo.ENC_DIM_RESEAU AS RES WITH(NOLOCK) 
                    ON CLI.DWHCLIRU2 = RES.CODE_APPORTEUR 
             LEFT JOIN dbo.DIM_TEMPS AS TPS WITH(NOLOCK) 
                    ON TPS.StandardDate = @DATE_TRAITEMENT 
      WHERE  1 = 1 
				AND	CPT.Validity_StartDate<=@DATE_TRAITEMENT   AND	CPT.Validity_EndDate>@DATE_TRAITEMENT
				AND (CPT.DWHCPTDAC IS NULL OR CPT.DWHCPTDAC >@DATE_TRAITEMENT)
				AND CLI.Validity_StartDate <= @DATE_TRAITEMENT AND CLI.Validity_EndDate > @DATE_TRAITEMENT

      UPDATE [$(DataHubDatabaseName)].[dbo].Data_log 
      SET    processingenddate = Getdate(), 
             processingstatus = 'OK', 
             nbrecordsextracted = @@ROWCOUNT 
      WHERE  id = @LOG_IDENTITY; 
  END