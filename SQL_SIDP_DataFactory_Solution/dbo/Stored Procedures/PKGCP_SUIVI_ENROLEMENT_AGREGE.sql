﻿/**************************************************************************************************************************************************************************************/
CREATE PROC [dbo].[PKGCP_SUIVI_ENROLEMENT_AGREGE]
   @date_obs date,
   @canal_orig varchar(255),
   @canal_fin varchar(255)
AS  BEGIN 

--declare @date_obs datetime  = '2017-12-31';
--declare @canal_orig varchar(255) = '10';
--declare @canal_fin varchar(255) = '10';

with 
cte_jour_alim_list as
(
select StandardDate as date_creat
  from dbo.DIM_TEMPS
 where dateadd(day,-90, @date_obs) < StandardDate and StandardDate <= @date_obs
),
 
cte_canal_orig as
( 
select CODE_SF as code_canal_orig from dbo.DIM_CANAL where PARENT_ID is not null and CODE_SF in (select [Value] from dbo.FnSplit(@canal_orig, ','))
),

cte_canal_fin as
( 
select CODE_SF as code_canal_fin from dbo.DIM_CANAL where PARENT_ID is not null and CODE_SF in (select [Value] from dbo.FnSplit(@canal_fin, ',')) 
),

cte as (
select date_creat
     , count(Id_SF) as nb_opp_created
     , count(case when IsWon = 'true' and date_close = date_creat then Id_SF else null end) as nb_ouvert_meme_jour
     , count(case when IsWon = 'true' and date_close <= @date_obs then Id_SF else null end) as nb_ouvert_a_date 
	 , count(case when stade_vente in ('01','02','03','04','05','06','07') then Id_SF else null end) as nb_opport_encours
	 , count(case when stade_vente = '01' then Id_SF else null end) as nb_opport_detection
	 , count(case when stade_vente = '02' then Id_SF else null end) as nb_opport_decouverte
	 , count(case when stade_vente = '03' then Id_SF else null end) as nb_opport_elaboration
	 , count(case when stade_vente = '04' then Id_SF else null end) as nb_opport_signee
	 , count(case when stade_vente = '05' then Id_SF else null end) as nb_opport_verification
	 , count(case when stade_vente = '06' then Id_SF else null end) as nb_opport_incomplet
	 , count(case when stade_vente = '07' then Id_SF else null end) as nb_opport_validé
	 , count(case when stade_vente in ('08','10','11','12') then Id_SF else null end) as nb_opport_perdues    
	 , count(case when reseau_dis = '01' then Id_SF else null end) as nb_opp_created_of_global
     , count(case when IsWon = 'true' and date_close <= @date_obs and reseau_dis = '01' then Id_SF else null end) as nb_ouvert_a_date_of_global	 
	 , count(case when reseau_dis = '01' and entite_dis in ('01','02') and indication = 'Oui' then Id_SF else NULL end) as nb_opp_created_of_bout_indic
     , count(case when IsWon = 'true' and date_close <= @date_obs and reseau_dis = '01' and entite_dis in ('01','02') and indication = 'Oui' then Id_SF else null end) as nb_ouvert_a_date_of_bout_indic
	 , count(case when reseau_dis = '01' and entite_dis in ('01','02') and indication = 'Non' then Id_SF else NULL end) as nb_opp_created_of_bout_ssindic
     , count(case when IsWon = 'true' and date_close <= @date_obs and entite_dis = '01' and entite_dis in ('01','02') and indication = 'Non' then Id_SF else null end) as nb_ouvert_a_date_of_bout_ssindic	 
	 , count(case when reseau_dis = '01' and entite_dis = '04' and indication = 'Oui' then Id_SF else NULL end) as nb_opp_created_of_self_indic
     , count(case when IsWon = 'true' and date_close <= @date_obs and reseau_dis = '01' and entite_dis = '04' and indication = 'Oui' then Id_SF else null end)  as nb_ouvert_a_date_of_self_indic	
	 , count(case when reseau_dis = '01' and entite_dis = '04' and indication = 'Non' then Id_SF else NULL end) as nb_opp_created_of_self_ssindic
     , count(case when IsWon = 'true' and date_close <= @date_obs and reseau_dis = '01' and entite_dis = '04' and indication = 'Non' then Id_SF else null end)  as nb_ouvert_a_date_of_self_ssindic	 
	 , count(case when reseau_dis = '02' and canal_orig in ('10','11') then Id_SF else NULL end) as nb_opp_created_ob_self
     , count(case when IsWon = 'true' and date_close <= @date_obs and reseau_dis = '02' and canal_orig in ('10','11') then Id_SF else null end) as nb_ouvert_a_date_ob_self	
	 , count(case when reseau_dis = '02' and canal_orig in ('12','13','14','15') then Id_SF else NULL end) as nb_opp_created_ob_CRC
     , count(case when IsWon = 'true' and date_close <= @date_obs and reseau_dis = '02' and canal_orig in ('12','13','14','15') then Id_SF else null end) as nb_ouvert_a_date_ob_CRC
  from ( select Id_SF
              , cast(CreatedDate as date) as date_creat
              , cast(CloseDate as date) as date_close
              , DATEDIFF(DD, cast(CreatedDate as date), @date_obs) as age_opport
              , StageName as stade_vente
              , IsWon
			  , case when Indication__c = 'Oui' then StartedChannelForIndication__c else StartedChannel__c end as canal_orig 
              , case when IsWon = 'true' then LeadSource else '-1' end as canal_fin
              , row_number() over (partition by Id_SF order by Validity_StartDate desc) as flag_opport_last_state
			  , Indication__c as indication
			  , DistributorEntity__c as entite_dis
			  , DistributorNetwork__c as reseau_dis
           from [$(DataHubDatabaseName)].dbo.PV_SF_OPPORTUNITY_HISTORY
          where ( cast(Validity_StartDate as date) <= @date_obs and @date_obs <= cast(Validity_EndDate as date) and CommercialOfferCode__c = 'OC80' and CommercialOfferName__c is not null 
		  and cast(CreatedDate as date) >='2017-05-16' )
          ) oh 
 inner join cte_canal_orig
   on cte_canal_orig.code_canal_orig = oh.canal_orig
 inner join cte_canal_fin
   on cte_canal_fin.code_canal_fin = oh.canal_fin
 where flag_opport_last_state = 1 and dateadd(day,-90, @date_obs) < date_creat and date_creat <= @date_obs
 group by date_creat
),

cte_expand as (
select jour_alim_list.date_creat
     , coalesce(opport.nb_opp_created, 0) as nb_opp_created
     , coalesce(opport.nb_ouvert_meme_jour, 0) as nb_ouvert_meme_jour
     , coalesce(opport.nb_ouvert_a_date, 0) as nb_ouvert_a_date
	 , coalesce(opport.nb_opport_encours, 0)as nb_opport_encours
	 , coalesce(opport.nb_opport_detection, 0) as nb_opport_detection
	 , coalesce(opport.nb_opport_decouverte, 0) as nb_opport_decouverte
	 , coalesce(opport.nb_opport_elaboration, 0) as nb_opport_elaboration
	 , coalesce(opport.nb_opport_signee, 0) as nb_opport_signee
	 , coalesce(opport.nb_opport_verification, 0) as nb_opport_verification
	 , coalesce(opport.nb_opport_incomplet, 0) as nb_opport_incomplet
	 , coalesce(opport.nb_opport_validé, 0) as nb_opport_validé
	 , coalesce(opport.nb_opport_perdues, 0) as nb_opport_perdues 
	 , coalesce(opport.nb_opp_created_of_global, 0) as nb_opp_created_of_global
	 , coalesce(opport.nb_ouvert_a_date_of_global, 0) as nb_ouvert_a_date_of_global 
	 , coalesce(opport.nb_opp_created_of_bout_indic, 0) as nb_opp_created_of_bout_indic
	 , coalesce(opport.nb_ouvert_a_date_of_bout_indic, 0) as nb_ouvert_a_date_of_bout_indic	 
	 , coalesce(opport.nb_opp_created_of_bout_ssindic, 0) as nb_opp_created_of_bout_ssindic
	 , coalesce(opport.nb_ouvert_a_date_of_bout_ssindic, 0) as nb_ouvert_a_date_of_bout_ssindic	 
	 , coalesce(opport.nb_opp_created_of_self_indic, 0) as nb_opp_created_of_self_indic
	 , coalesce(opport.nb_ouvert_a_date_of_self_indic, 0) as nb_ouvert_a_date_of_self_indic	 
	 , coalesce(opport.nb_opp_created_of_self_ssindic, 0) as nb_opp_created_of_self_ssindic
	 , coalesce(opport.nb_ouvert_a_date_of_self_ssindic, 0) as nb_ouvert_a_date_of_self_ssindic	 
	 , coalesce(opport.nb_opp_created_ob_self, 0) as nb_opp_created_ob_self
	 , coalesce(opport.nb_ouvert_a_date_ob_self, 0) as nb_ouvert_a_date_ob_self	 
	 , coalesce(opport.nb_opp_created_ob_CRC, 0) as nb_opp_created_ob_CRC
	 , coalesce(opport.nb_ouvert_a_date_ob_CRC, 0) as nb_ouvert_a_date_ob_CRC
  from cte_jour_alim_list as jour_alim_list
  left join cte as opport
    on opport.date_creat = jour_alim_list.date_creat
),

cte_cum as (
select jour_alim_list.date_creat as date_creat
     , Sum(case when jour_alim_list.date_creat = @date_obs then nb_opp_created else 0 end) as nb_opp_created_cum
     , Sum(case when jour_alim_list.date_creat = @date_obs then nb_ouvert_meme_jour else 0 end) as nb_ouvert_meme_jour_cum
     , Sum(case when jour_alim_list.date_creat = @date_obs then nb_ouvert_a_date else 0 end) as nb_ouvert_a_date_cum
	 , Sum(case when jour_alim_list.date_creat = @date_obs then nb_opport_encours else 0 end) as nb_opport_encours_cum
	 , Sum(case when jour_alim_list.date_creat = @date_obs then nb_opport_detection else 0 end) as nb_opport_detection_cum
	 , Sum(case when jour_alim_list.date_creat = @date_obs then nb_opport_decouverte else 0 end) as nb_opport_decouverte_cum
	 , Sum(case when jour_alim_list.date_creat = @date_obs then nb_opport_elaboration else 0 end) as nb_opport_elaboration_cum
	 , Sum(case when jour_alim_list.date_creat = @date_obs then nb_opport_signee else 0 end) as nb_opport_signee_cum
	 , Sum(case when jour_alim_list.date_creat = @date_obs then nb_opport_verification else 0 end) as nb_opport_verification_cum
	 , Sum(case when jour_alim_list.date_creat = @date_obs then nb_opport_incomplet else 0 end) as nb_opport_incomplet_cum
	 , Sum(case when jour_alim_list.date_creat = @date_obs then nb_opport_validé else 0 end) as nb_opport_validé_cum
	 , Sum(case when jour_alim_list.date_creat = @date_obs then nb_opport_perdues else 0 end) as nb_opport_perdues_cum
	 , Sum(case when jour_alim_list.date_creat = @date_obs then nb_opp_created_of_global else 0 end) as nb_opp_created_of_global_cum
	 , Sum(case when jour_alim_list.date_creat = @date_obs then nb_ouvert_a_date_of_global else 0 end) as nb_ouvert_a_date_of_global_cum
	 , Sum(case when jour_alim_list.date_creat = @date_obs then nb_opp_created_of_bout_indic else 0 end) as nb_opp_created_of_bout_indic_cum
	 , Sum(case when jour_alim_list.date_creat = @date_obs then nb_ouvert_a_date_of_bout_indic else 0 end) as nb_ouvert_a_date_of_bout_indic_cum
	 , Sum(case when jour_alim_list.date_creat = @date_obs then nb_opp_created_of_bout_ssindic else 0 end) as nb_opp_created_of_bout_ssindic_cum
	 , Sum(case when jour_alim_list.date_creat = @date_obs then nb_ouvert_a_date_of_bout_ssindic else 0 end) as nb_ouvert_a_date_of_bout_ssindic_cum
	 , Sum(case when jour_alim_list.date_creat = @date_obs then nb_opp_created_of_self_indic else 0 end) as nb_opp_created_of_self_indic_cum
	 , Sum(case when jour_alim_list.date_creat = @date_obs then nb_ouvert_a_date_of_self_indic else 0 end) as nb_ouvert_a_date_of_self_indic_cum
	 , Sum(case when jour_alim_list.date_creat = @date_obs then nb_opp_created_of_self_ssindic else 0 end) as nb_opp_created_of_self_ssindic_cum
	 , Sum(case when jour_alim_list.date_creat = @date_obs then nb_ouvert_a_date_of_self_ssindic else 0 end) as nb_ouvert_a_date_of_self_ssindic_cum
	 , Sum(case when jour_alim_list.date_creat = @date_obs then nb_opp_created_ob_self else 0 end) as nb_opp_created_ob_self_cum
	 , Sum(case when jour_alim_list.date_creat = @date_obs then nb_ouvert_a_date_ob_self else 0 end) as nb_ouvert_a_date_ob_self_cum
	 , Sum(case when jour_alim_list.date_creat = @date_obs then nb_opp_created_ob_CRC else 0 end) as nb_opp_created_ob_CRC_cum
	 , Sum(case when jour_alim_list.date_creat = @date_obs then nb_ouvert_a_date_ob_CRC else 0 end) as nb_ouvert_a_date_ob_CRC_cum
  from cte_jour_alim_list as jour_alim_list
  left join cte_expand as opport
    on dateadd("d",-30,jour_alim_list.date_creat) < opport.date_creat and opport.date_creat <= jour_alim_list.date_creat
  group by 
     jour_alim_list.date_creat
),

cte_fin as (
select opport.date_creat
     , nb_opp_created
     , nb_ouvert_meme_jour
     , nb_ouvert_a_date
	 , nb_opport_encours
	 , nb_opport_detection
	 , nb_opport_decouverte
	 , nb_opport_elaboration
	 , nb_opport_signee
	 , nb_opport_verification
	 , nb_opport_incomplet
	 , nb_opport_validé
	 , nb_opport_perdues
	 , nb_opp_created_of_global
	 , nb_ouvert_a_date_of_global
	 , nb_opp_created_of_bout_indic
	 , nb_ouvert_a_date_of_bout_indic
	 , nb_opp_created_of_bout_ssindic
	 , nb_ouvert_a_date_of_bout_ssindic
	 , nb_opp_created_of_self_indic
	 , nb_ouvert_a_date_of_self_indic
	 , nb_opp_created_of_self_ssindic
	 , nb_ouvert_a_date_of_self_ssindic
	 , nb_opp_created_ob_self
	 , nb_ouvert_a_date_ob_self
	 , nb_opp_created_ob_CRC
	 , nb_ouvert_a_date_ob_CRC
	 , nb_opp_created_cum
     , nb_ouvert_meme_jour_cum
     , nb_ouvert_a_date_cum
	 , nb_opport_encours_cum
	 , nb_opport_detection_cum
	 , nb_opport_decouverte_cum
	 , nb_opport_elaboration_cum
	 , nb_opport_signee_cum
	 , nb_opport_verification_cum
	 , nb_opport_incomplet_cum
	 , nb_opport_validé_cum
	 , nb_opport_perdues_cum
	 , nb_opp_created_of_global_cum
	 , nb_ouvert_a_date_of_global_cum
	 , nb_opp_created_of_bout_indic_cum
	 , nb_ouvert_a_date_of_bout_indic_cum
	 , nb_opp_created_of_bout_ssindic_cum
	 , nb_ouvert_a_date_of_bout_ssindic_cum
	 , nb_opp_created_of_self_indic_cum
	 , nb_ouvert_a_date_of_self_indic_cum
	 , nb_opp_created_of_self_ssindic_cum
	 , nb_ouvert_a_date_of_self_ssindic_cum
	 , nb_opp_created_ob_self_cum
	 , nb_ouvert_a_date_ob_self_cum
	 , nb_opp_created_ob_CRC_cum
	 , nb_ouvert_a_date_ob_CRC_cum
	 from cte_expand as opport
  left join cte_cum as opport_cum
    on opport_cum.date_creat = opport.date_creat
)

select * from cte_fin

END