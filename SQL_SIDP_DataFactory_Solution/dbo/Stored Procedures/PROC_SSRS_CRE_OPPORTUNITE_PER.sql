﻿CREATE PROC  [dbo].[PROC_SSRS_CRE_OPPORTUNITE_PER]

AS  BEGIN 

DECLARE @date_obs DATE;
SELECT  @date_obs = CAST(GETDATE() AS DATE);
DECLARE @date_max date;
SELECT  @date_max = dateadd(day, -1 ,GETDATE());
SET DATEFIRST 1; -->1    Lundi
DROP TABLE IF EXISTS #rq_cre_enrolment_per_cp;
DROP TABLE IF EXISTS [dbo].[WK_SSRS_CRE_OPPORTUNITE_PER];

SELECT Year_alim,Month_alim,Week_alim,jour_alim
    ,nb_opp_CRE_perdues
	,nb_opp_CRE_perdues_retract
	,nb_opp_CRE_perdues_annul
	--Week to date
	,sum(nb_opp_CRE_perdues) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opp_CRE_perdues_WTD --Week to date
	,sum(nb_opp_CRE_perdues_retract) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opp_CRE_perdues_retract_WTD --Week to date
	,sum(nb_opp_CRE_perdues_annul) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opp_CRE_perdues_annul_WTD --Week to date
	--Month to date
	,sum(nb_opp_CRE_perdues) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opp_CRE_perdues_MTD --Month to date
	,sum(nb_opp_CRE_perdues_retract) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opp_CRE_perdues_retract_MTD --Month to date
	,sum(nb_opp_CRE_perdues_annul) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opp_CRE_perdues_annul_MTD --Month to date
	--Full Month
	,sum(nb_opp_CRE_perdues) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_opp_CRE_perdues_FM --Full Month
	,sum(nb_opp_CRE_perdues_retract) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_opp_CRE_perdues_retract_FM --Full Month
	,sum(nb_opp_CRE_perdues_annul) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_opp_CRE_perdues_annul_FM --Full Month
	--Year to date
    ,sum(nb_opp_CRE_perdues) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opp_CRE_perdues_YTD  --Year to date
    ,sum(nb_opp_CRE_perdues_retract) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opp_CRE_perdues_retract_YTD  --Year to date
    ,sum(nb_opp_CRE_perdues_annul) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opp_CRE_perdues_annul_YTD  --Year to date
	--Full year
	,sum(nb_opp_CRE_perdues) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_opp_CRE_perdues_FY  --Full year
	,sum(nb_opp_CRE_perdues_retract) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_opp_CRE_perdues_retract_FY  --Full year
	,sum(nb_opp_CRE_perdues_annul) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_opp_CRE_perdues_annul_FY  --Full year
INTO #rq_cre_enrolment_per_cp
	FROM  ( 
     SELECT  T1.StandardDate as jour_alim,DATEPART(YEAR,T1.Date) AS Year_alim,DATEPART(MONTH,T1.Date) AS Month_alim,DATEPART(YEAR,DATEADD(day, (7 - DATEPART(dw, T1.StandardDate)), T1.StandardDate)) *100 + DATEPART(WEEK,DATEADD(day, (7 - DATEPART(dw, T1.StandardDate)), T1.StandardDate)) AS Week_alim
		 , sum(case when STADE_VENTE IN ('10','11','12','16') then 1 end) as nb_opp_CRE_perdues
		 , sum(case when STADE_VENTE IN ('12') then 1 end) as nb_opp_CRE_perdues_retract
		 , sum(case when STADE_VENTE IN ('16') then 1 end) as nb_opp_CRE_perdues_annul
FROM [dbo].[DIM_TEMPS] T1 
LEFT OUTER JOIN  [dbo].[CRE_OPPORTUNITE]  ON T1.StandardDate=CAST(CRE_OPPORTUNITE.DTE_CLO_OPPRT AS DATE)
WHERE  ( T1.StandardDate >= CAST(DATEADD(yyyy, DATEDIFF(YYYY, 0, DATEADD(yyyy, -2, @date_obs)), 0) AS DATE)
AND T1.StandardDate <=  DATEADD(day, (7 - DATEPART(dw, @date_obs)), @date_obs) ) 
GROUP BY  T1.StandardDate ,DATEPART(YEAR,T1.Date) ,DATEPART(MONTH,T1.Date) ,DATEPART(WEEK,T1.Date) 
) t;

SELECT
  CY.jour_alim
, CY.Year_alim
, CY.Month_alim
, CY.Week_alim
, coalesce(CY.nb_opp_CRE_perdues,0) AS nb_opp_CRE_perdues
, coalesce(CY.nb_opp_CRE_perdues_WTD,0) AS nb_opp_CRE_perdues_WTD
, coalesce(CY.nb_opp_CRE_perdues_MTD,0) AS nb_opp_CRE_perdues_MTD
, coalesce(CY.nb_opp_CRE_perdues_YTD,0) AS nb_opp_CRE_perdues_YTD
, coalesce(PM.nb_opp_CRE_perdues_FM,0) AS nb_opp_CRE_perdues_FM
, coalesce(PY.nb_opp_CRE_perdues_FY,0) AS nb_opp_CRE_perdues_FY
, coalesce(CY.nb_opp_CRE_perdues_retract,0) AS nb_opp_CRE_perdues_retract
, coalesce(CY.nb_opp_CRE_perdues_retract_WTD,0) AS nb_opp_CRE_perdues_retract_WTD
, coalesce(CY.nb_opp_CRE_perdues_retract_MTD,0) AS nb_opp_CRE_perdues_retract_MTD
, coalesce(CY.nb_opp_CRE_perdues_retract_YTD,0) AS nb_opp_CRE_perdues_retract_YTD
, coalesce(PM.nb_opp_CRE_perdues_retract_FM,0) AS nb_opp_CRE_perdues_retract_FM
, coalesce(PY.nb_opp_CRE_perdues_retract_FY,0) AS nb_opp_CRE_perdues_retract_FY
, coalesce(CY.nb_opp_CRE_perdues_annul,0) AS nb_opp_CRE_perdues_annul
, coalesce(CY.nb_opp_CRE_perdues_annul_WTD,0) AS nb_opp_CRE_perdues_annul_WTD
, coalesce(CY.nb_opp_CRE_perdues_annul_MTD,0) AS nb_opp_CRE_perdues_annul_MTD
, coalesce(CY.nb_opp_CRE_perdues_annul_YTD,0) AS nb_opp_CRE_perdues_annul_YTD
, coalesce(PM.nb_opp_CRE_perdues_annul_FM,0) AS nb_opp_CRE_perdues_annul_FM
, coalesce(PY.nb_opp_CRE_perdues_annul_FY,0) AS nb_opp_CRE_perdues_annul_FY
INTO [dbo].[WK_SSRS_CRE_OPPORTUNITE_PER]
FROM #rq_cre_enrolment_per_cp CY --Current Year
 LEFT OUTER JOIN #rq_cre_enrolment_per_cp PM --Previous Year
		  ON DATEADD(MONTH,-1,CY.jour_alim) = PM.jour_alim
	 LEFT OUTER JOIN #rq_cre_enrolment_per_cp PY --Previous Year
		  ON DATEADD(YEAR,-1,CY.jour_alim) = PY.jour_alim
WHERE  CY.jour_alim <= @date_max
ORDER BY CY.jour_alim;
END