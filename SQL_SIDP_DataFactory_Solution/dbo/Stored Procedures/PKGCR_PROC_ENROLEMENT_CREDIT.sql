﻿

CREATE PROCEDURE [dbo].[PKGCR_PROC_ENROLEMENT_CREDIT]
@DTE_OBS date

AS BEGIN

DECLARE @weekobs int;
DECLARE @IsoWeekYear int;

/*DECLARE @DTE_OBS date;
SET @DTE_OBS='2018-01-03';*/

SET @weekobs = (select IsoWeek from [dbo].[DIM_TEMPS] where CAST(Date as date) = CAST(@DTE_OBS as date) );
SET @IsoWeekYear = (select IsoWeekYear from [dbo].[DIM_TEMPS] where CAST(Date as date) = CAST(@DTE_OBS as date) );
SET @DTE_OBS = CAST(@DTE_OBS as date) ;
SET DATEFIRST 1; -->1    Lundi 

With 
cte_jour_alim_list AS
( SELECT T1.StandardDate AS jour_alim
    FROM [dbo].[DIM_TEMPS] T1
    WHERE T1.StandardDate >= CAST(DATEADD(day, -6, @DTE_OBS) AS DATE) AND T1.StandardDate <= @DTE_OBS
),
cte_Cre_Opp_ALL as (
SELECT case when [COD_CAN_INT_ORI] IN ('11', '10', '12', '13', '14', '15') then COUNT([IDE_OPPRT_SF]) end as Cumul_Jour
	  ,case when [COD_CAN_INT_ORI] = '11'  then COUNT(IDE_OPPRT_SF) end as  Cumul_Jour_Web_OB
	  ,case when [COD_CAN_INT_ORI] = '10' then COUNT(IDE_OPPRT_SF) end as  Cumul_Jour_Appli_OB
	  ,case when [COD_CAN_INT_ORI] IN ('12', '13','14', '15') then COUNT(IDE_OPPRT_SF) end as  Cumul_Jour_CRC
	  ,[STADE_VENTE]
	  ,[LIB_STADE_VENTE] as LIBELLE
	  ,Cast([DTE_CREA] as date) [DTE_CREA]
	  ,DATEPART(ISO_WEEK,[DTE_CREA]) as [WeekOfYear]
	  ,[DTE_ALIM]
  FROM [dbo].[CRE_OPPORTUNITE] Opp
  where [FLG_DER_ETAT_OPPRT] = 1
  and  Cast([DTE_CREA] as date) <= @DTE_OBS  /*S'arréter à la date du rapport (paramètre du rapport) */
  group by 
       [COD_CAN_INT_ORI]
	  ,[STADE_VENTE]
	  ,[LIB_STADE_VENTE]
	  ,Cast([DTE_CREA] as date) 
	  ,DATEPART(ISO_WEEK,[DTE_CREA])
	  ,[DTE_ALIM] )

/*** Semaine encours ***/
,cte_cre_opp_Cumul_Hebdo as (
SELECT [STADE_VENTE],
	   SUM(Cumul_Jour) as Cumul_Hebdo
FROM cte_Cre_Opp_ALL
WHERE  [WeekOfYear] = @weekobs and YEAR(DATEADD(day, 26 - DATEPART(ISO_WEEK, [DTE_CREA]), [DTE_CREA])) = @IsoWeekYear
GROUP BY [STADE_VENTE]
)

/*** Semaine précédente ***/
,cte_cre_opp_Cumul_Hebdo_prec as (
SELECT [STADE_VENTE],
	   SUM(Cumul_Jour) as Cumul_Hebdo_prec
FROM cte_Cre_Opp_ALL
WHERE  DATEPART(YEAR,DTE_CREA) = DATEPART(YEAR,DATEADD(week,-1,@DTE_OBS))
AND  [WeekOfYear] = DATEPART(ISO_WEEK,CAST(DATEADD(WEEK,-1,@DTE_OBS) AS DATE))
GROUP BY [STADE_VENTE]
)

/***Mois encours***/
,cte_cre_opp_Cumul_Mensuel as ( 
SELECT [STADE_VENTE],
	   SUM(Cumul_Jour) as Cumul_Mensuel
FROM cte_Cre_Opp_ALL
WHERE DATEPART(MONTH,DTE_CREA) = DATEPART(MONTH,@DTE_OBS) 
  AND DTE_CREA <= DATEADD(day, (7 - DATEPART(dw, @DTE_OBS)), @DTE_OBS) 
  AND DATEPART(YEAR,DTE_CREA) = DATEPART(YEAR,@DTE_OBS)
GROUP BY [STADE_VENTE]
)

/***Mois précédent***/
,cte_cre_opp_Cumul_Mensuel_prec as ( 
SELECT [STADE_VENTE],
	   SUM(Cumul_Jour) as Cumul_Mensuel_prec
FROM cte_Cre_Opp_ALL
WHERE DATEPART(YEAR,DTE_CREA) = DATEPART(YEAR,dateadd(MONTH,-1,@DTE_OBS)) 
  AND DATEPART(MONTH,DTE_CREA) = DATEPART(MONTH,dateadd(MONTH,-1,@DTE_OBS))
GROUP BY [STADE_VENTE]
)

/***YTD***/
, cte_cre_opp_YTD as (
SELECT [STADE_VENTE],
       SUM(Cumul_Jour) as YTD
FROM cte_Cre_Opp_ALL
WHERE cast([DTE_CREA] as date)  between cast(cast(year(@DTE_OBS) as varchar(4)) + '0101' as date) and  DATEADD(day, (7 - DATEPART(dw, @DTE_OBS)), @DTE_OBS)
GROUP BY [STADE_VENTE]
)

, CTE_Stade_Vente as (
SELECT distinct [STADE_VENTE],LIB_STADE_VENTE from [dbo].[CRE_OPPORTUNITE]         
)

SELECT 
		x.jour_alim,
		z.[STADE_VENTE],
		z.LIB_STADE_VENTE, 
		ISNULL(SUM(a.Cumul_Jour_Web_OB),0) as Cumul_Jour_Web_OB,
		ISNULL(SUM(a.Cumul_Jour_Appli_OB),0) as Cumul_Jour_Appli_OB ,
		ISNULL(SUM(a.Cumul_Jour_CRC),0) as Cumul_Jour_CRC,
		a.DTE_CREA ,
		a.WeekOfYear,
		ISNULL(SUM(a.Cumul_Jour),0) as Cumul_Jour,
		case when ROW_NUMBER() OVER(PARTITION BY z.LIB_STADE_VENTE order by z.[STADE_VENTE], x.jour_alim desc)=1 then ISNULL(b.Cumul_Hebdo,0) else 0 end as Cumul_Hebdo,
		case when ROW_NUMBER() OVER(PARTITION BY z.LIB_STADE_VENTE order by z.[STADE_VENTE], x.jour_alim desc)=1 then ISNULL(c.Cumul_Hebdo_prec,0) else 0 end as Cumul_Hebdo_prec,
		case when ROW_NUMBER() OVER(PARTITION BY z.LIB_STADE_VENTE order by z.[STADE_VENTE], x.jour_alim desc)=1 then ISNULL(d.Cumul_Mensuel,0) else 0 end as Cumul_Mensuel,
		case when ROW_NUMBER() OVER(PARTITION BY z.LIB_STADE_VENTE order by z.[STADE_VENTE], x.jour_alim desc)=1 then ISNULL(e.Cumul_Mensuel_prec,0) else 0 end as Cumul_Mensuel_prec,
		case when ROW_NUMBER() OVER(PARTITION BY z.LIB_STADE_VENTE order by z.[STADE_VENTE], x.jour_alim desc)=1 then ISNULL(f.YTD,0) else 0 end as YTD 
FROM cte_jour_alim_list x 
left join CTE_Stade_Vente z on 1=1 
left join cte_Cre_Opp_ALL a on z.[STADE_VENTE] = a.STADE_VENTE and x.jour_alim = a.DTE_CREA
left join cte_cre_opp_Cumul_Hebdo b on z.[STADE_VENTE] =b.STADE_VENTE
left join cte_cre_opp_Cumul_Hebdo_prec c on z.[STADE_VENTE] =c.STADE_VENTE
left join cte_cre_opp_Cumul_Mensuel d on  z.[STADE_VENTE] =d.STADE_VENTE
left join cte_cre_opp_Cumul_Mensuel_prec e on  z.[STADE_VENTE] =e.STADE_VENTE
left join cte_cre_opp_YTD f on z.[STADE_VENTE] = f.STADE_VENTE
group by x.jour_alim,
		z.[STADE_VENTE],
		z.LIB_STADE_VENTE, 
		a.DTE_CREA,
		a.WeekOfYear, 
		b.Cumul_Hebdo, 
		c.Cumul_Hebdo_prec, 
		d.Cumul_Mensuel, 
		e.Cumul_Mensuel_prec, 
		f.YTD

END;