﻿

/****** Object:  StoredProcedure [dbo].[PKGCR_PROC_OPPORT_SOUSCRIPT_ASSOC]    Script Date: 17/11/2017 15:46:10 ******/

-- =============================================
-- Author:		<Liliane, Huang>
-- Create date: <11/01/2017>
-- Description:	<Alimentation de la table CRE_OPPORT_SOUSCRIPT_ASSOC permettant de faire la liaison entre 
--				les opportunités de type Crédit et les personnes ayant souscrit un crédit>
-- Table input : [$(DataHubDatabaseName)].[dbo].[PV_SF_OPPORTUNITYCONTACTROLE]
--				 [$(DataHubDatabaseName)].[dbo].[PV_SF_OPPORTUNITY_HISTORY]
--				 [DIM_RECORDTYPE]
--				 [$(DataHubDatabaseName)].[dbo].[PV_SF_CONTACT]
--				 [DIM_RECORDTYPE]
-- Table output : CRE_OPPORT_SOUSCRIPT_ASSOC
-- =============================================

CREATE PROCEDURE [dbo].[PKGCR_PROC_OPPORT_SOUSCRIPT_ASSOC] 
(
   @date_alim DATE
    ,@nbRows int OUTPUT -- nb lignes processées
)
AS
BEGIN

/*Test
declare @date_alim DATE
set @date_alim = '2018-01-19'*/
--Insertion des informations dans la table CRE_OPPORT_SOUSCRIPT_ASSOC
INSERT INTO [dbo].[CRE_OPPORT_SOUSCRIPT_ASSOC] (
	DTE_ALIM,
	IDE_OPPRT_CONTACT_ROLE_SF,
	IDE_OPPRT_SF,
	IDE_CONTACT,
	ROLE,
	FLG_PPAL,
	DTE_CREA,
	IDE_USER_CREA_SF,
	DTE_DER_MOD,
	IDE_USER_MOD_SF,
	SystemModstamp,
	FLG_SUPP
)
SELECT distinct @date_alim AS DTE_ALIM,
		ASS.Id_SF,
		ASS.OpportunityId,
		ASS.ContactId,
		ASS.Role,
		ASS.IsPrimary,
		ASS.CreatedDate,
		ASS.CreatedById,
		ASS.LastModifiedDate,
		ASS.LastModifiedById,
		ASS.SystemModstamp,
		ASS.IsDeleted
FROM [$(DataHubDatabaseName)].[dbo].[PV_SF_OPPORTUNITYCONTACTROLE] ASS
INNER JOIN [$(DataHubDatabaseName)].[dbo].[PV_SF_OPPORTUNITY_HISTORY] OPP
ON ASS.OpportunityId = OPP.Id_SF
INNER JOIN [dbo].[DIM_RECORDTYPE] DIM
ON OPP.RecordTypeId = DIM.CODE_SF AND DIM.LIBELLE = 'OB - Crédit'
INNER JOIN [$(DataHubDatabaseName)].[dbo].[PV_SF_CONTACT] CON
ON ASS.ContactId = CON.Id_SF
INNER JOIN [dbo].[DIM_RECORDTYPE] DIM2
ON CON.RecordTypeId = DIM2.CODE_SF AND DIM2.LIBELLE = 'Souscripteur'
ORDER BY ASS.Id_SF

		SELECT @nbRows = @@ROWCOUNT
;
END
