﻿CREATE PROC [dbo].[PROC_SSRS_OPPORT_OUV]

AS  BEGIN 

DECLARE @date_obs DATE;
SELECT  @date_obs = CAST(GETDATE() AS DATE);
DECLARE @date_max date;
SELECT  @date_max = dateadd(day, -1 ,GETDATE());
SET DATEFIRST 1; -->1    Lundi
DROP TABLE IF EXISTS #rq_opp_ouv;
DROP TABLE IF EXISTS [dbo].[WK_SSRS_OPPORT_OUVERT];

SELECT Year_alim,Month_alim,Week_alim,jour_alim
    ,nb_ouv_cpte
	,nb_ouv_cpte_canal_fin_digital
	,nb_ouv_cpte_canal_fin_boutique
	,nb_ouv_cpte_canal_fin_crc
	,nb_ouv_cpte_full_digital
	,nb_ouv_cpte_full_boutique
	,age_opport_digital
	,age_opport_boutique
	--Week to date
	,sum(nb_ouv_cpte) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_ouv_cpte_WTD --Week to date
	,sum(nb_ouv_cpte_canal_fin_digital) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_ouv_cpte_canal_fin_digital_WTD --Week to date
	,sum(nb_ouv_cpte_canal_fin_boutique) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_ouv_cpte_canal_fin_boutique_WTD --Week to date
	,sum(nb_ouv_cpte_canal_fin_crc) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_ouv_cpte_canal_fin_crc_WTD --Week to date
	,sum(nb_ouv_cpte_full_digital) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_ouv_cpte_full_digital_WTD --Week to date
	,sum(nb_ouv_cpte_full_boutique) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_ouv_cpte_full_boutique_WTD --Week to date
	,sum(age_opport_digital) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS age_opport_digital_WTD --Week to date
	,sum(age_opport_boutique) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS age_opport_boutique_WTD --Week to date

	--Month to date
	,sum(nb_ouv_cpte) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_ouv_cpte_MTD --Month to date
	,sum(nb_ouv_cpte_canal_fin_digital) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_ouv_cpte_canal_fin_digital_MTD --Month to date
	,sum(nb_ouv_cpte_canal_fin_boutique) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_ouv_cpte_canal_fin_boutique_MTD --Month to date
	,sum(nb_ouv_cpte_canal_fin_crc) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_ouv_cpte_canal_fin_crc_MTD --Month to date
	,sum(nb_ouv_cpte_full_digital) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_ouv_cpte_full_digital_MTD --Month to date
	,sum(nb_ouv_cpte_full_boutique) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_ouv_cpte_full_boutique_MTD --Month to date
	,sum(age_opport_digital) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS age_opport_digital_MTD --Month to date
	,sum(age_opport_boutique) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS age_opport_boutique_MTD --Month to date

	
	--Full Month
	,sum(nb_ouv_cpte) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_ouv_cpte_FM --Full Month
	,sum(nb_ouv_cpte_canal_fin_digital) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_ouv_cpte_canal_fin_digital_FM --Full Month
	,sum(nb_ouv_cpte_canal_fin_boutique) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_ouv_cpte_canal_fin_boutique_FM --Full Month
	,sum(nb_ouv_cpte_canal_fin_crc) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_ouv_cpte_canal_fin_crc_FM --Full Month
	,sum(nb_ouv_cpte_full_digital) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_ouv_cpte_full_digital_FM --Full Month
	,sum(nb_ouv_cpte_full_boutique) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_ouv_cpte_full_boutique_FM --Full Month
	,sum(age_opport_digital) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS age_opport_digital_FM --Full Month
	,sum(age_opport_boutique) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS age_opport_boutique_FM --Full Month

	--Year to date
    ,sum(nb_ouv_cpte) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_ouv_cpte_YTD  --Year to date
    ,sum(nb_ouv_cpte_canal_fin_digital) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_ouv_cpte_canal_fin_digital_YTD  --Year to date
    ,sum(nb_ouv_cpte_canal_fin_boutique) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_ouv_cpte_canal_fin_boutique_YTD  --Year to date
    ,sum(nb_ouv_cpte_canal_fin_crc) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_ouv_cpte_canal_fin_crc_YTD  --Year to date
    ,sum(nb_ouv_cpte_full_digital) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_ouv_cpte_full_digital_YTD  --Year to date
    ,sum(nb_ouv_cpte_full_boutique) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_ouv_cpte_full_boutique_YTD  --Year to date
    ,sum(age_opport_digital) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS age_opport_digital_YTD  --Year to date
    ,sum(age_opport_boutique) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS age_opport_boutique_YTD  --Year to date

	--Full year
	,sum(nb_ouv_cpte) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_ouv_cpte_FY  --Full year
	,sum(nb_ouv_cpte_canal_fin_digital) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_ouv_cpte_canal_fin_digital_FY  --Full year
	,sum(nb_ouv_cpte_canal_fin_boutique) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_ouv_cpte_canal_fin_boutique_FY  --Full year
	,sum(nb_ouv_cpte_canal_fin_crc) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_ouv_cpte_canal_fin_crc_FY  --Full year
	,sum(nb_ouv_cpte_full_digital) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_ouv_cpte_full_digital_FY  --Full year
	,sum(nb_ouv_cpte_full_boutique) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_ouv_cpte_full_boutique_FY  --Full year
	,sum(age_opport_digital) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS age_opport_digital_FY  --Full year
	,sum(age_opport_boutique) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS age_opport_boutique_FY  --Full year
INTO #rq_opp_ouv
FROM  ( 
     SELECT  T1.StandardDate as jour_alim,DATEPART(YEAR,T1.Date) AS Year_alim,DATEPART(MONTH,T1.Date) AS Month_alim,DATEPART(YEAR,DATEADD(day, (7 - DATEPART(dw, T1.StandardDate)), T1.StandardDate)) *100 + DATEPART(WEEK,DATEADD(day, (7 - DATEPART(dw, T1.StandardDate)), T1.StandardDate)) AS Week_alim
    , sum(case when stade_vente  in ('Compte ouvert','Affaire conclue') and CAST(jour_alim as date) = CAST(date_fin_opp as date) then 1 else 0 end) as nb_ouv_cpte
    , sum(case when stade_vente  in ('Compte ouvert','Affaire conclue')  and CAST(jour_alim as date) = CAST(date_fin_opp as date)  and  canal_distrib_final = 'Digital' then 1 else 0 end) as nb_ouv_cpte_canal_fin_digital 
    , sum(case when stade_vente  in ('Compte ouvert','Affaire conclue')  and CAST(jour_alim as date) = CAST(date_fin_opp as date) and  canal_distrib_final = 'Agence' then 1 else 0 end) as nb_ouv_cpte_canal_fin_boutique 
    , sum(case when stade_vente  in ('Compte ouvert','Affaire conclue')  and CAST(jour_alim as date) = CAST(date_fin_opp as date) and  canal_distrib_final = 'CRC' then 1 else 0 end) as nb_ouv_cpte_canal_fin_crc 
    , sum(case when stade_vente  in ('Compte ouvert','Affaire conclue')  and CAST(jour_alim as date) = CAST(date_fin_opp as date)  and  flag_full_digi='1' then 1 else 0 end) as nb_ouv_cpte_full_digital 
    , sum(case when stade_vente  in ('Compte ouvert','Affaire conclue')  and CAST(jour_alim as date) = CAST(date_fin_opp as date) and  flag_full_btq='1' then 1 else 0 end) as nb_ouv_cpte_full_boutique 
    , sum(case when stade_vente  in ('Compte ouvert','Affaire conclue')  and flag_full_digi='1' then  age_opport else 0 end) as age_opport_digital
    , sum(case when stade_vente  in ('Compte ouvert','Affaire conclue')  and flag_full_btq='1' then  age_opport else 0 end) as age_opport_boutique
FROM [dbo].[DIM_TEMPS] T1 
LEFT OUTER JOIN  sas_vue_flux_opport_fin  ON T1.StandardDate=sas_vue_flux_opport_fin.jour_alim
WHERE  ( T1.StandardDate >= CAST(DATEADD(yyyy, DATEDIFF(YYYY, 0, DATEADD(yyyy, -2, @date_obs)), 0) AS DATE)
AND T1.StandardDate <=  DATEADD(day, (7 - DATEPART(dw, @date_obs)), @date_obs) )
GROUP BY  T1.StandardDate ,DATEPART(YEAR,T1.Date) ,DATEPART(MONTH,T1.Date) ,DATEPART(WEEK,T1.Date) 
) t;		

SELECT
  CY.jour_alim
, CY.Year_alim
, CY.Month_alim
, CY.Week_alim
, coalesce(CY.nb_ouv_cpte,0) AS nb_ouv_cpte
, coalesce(CY.nb_ouv_cpte_WTD,0) AS nb_ouv_cpte_WTD
, coalesce(CY.nb_ouv_cpte_MTD,0) AS nb_ouv_cpte_MTD
, coalesce(CY.nb_ouv_cpte_YTD,0) AS nb_ouv_cpte_YTD
, coalesce(PM.nb_ouv_cpte_FM,0) AS nb_ouv_cpte_FM
, coalesce(PY.nb_ouv_cpte_FY,0) AS nb_ouv_cpte_FY
, coalesce(CY.nb_ouv_cpte_canal_fin_digital,0) AS nb_ouv_cpte_canal_fin_digital
, coalesce(CY.nb_ouv_cpte_canal_fin_digital_WTD,0) AS nb_ouv_cpte_canal_fin_digital_WTD
, coalesce(CY.nb_ouv_cpte_canal_fin_digital_MTD,0) AS nb_ouv_cpte_canal_fin_digital_MTD
, coalesce(CY.nb_ouv_cpte_canal_fin_digital_YTD,0) AS nb_ouv_cpte_canal_fin_digital_YTD
, coalesce(PM.nb_ouv_cpte_canal_fin_digital_FM,0) AS nb_ouv_cpte_canal_fin_digital_FM
, coalesce(PY.nb_ouv_cpte_canal_fin_digital_FY,0) AS nb_ouv_cpte_canal_fin_digital_FY
, coalesce(CY.nb_ouv_cpte_canal_fin_boutique,0) AS nb_ouv_cpte_canal_fin_boutique
, coalesce(CY.nb_ouv_cpte_canal_fin_boutique_WTD,0) AS nb_ouv_cpte_canal_fin_boutique_WTD
, coalesce(CY.nb_ouv_cpte_canal_fin_boutique_MTD,0) AS nb_ouv_cpte_canal_fin_boutique_MTD
, coalesce(CY.nb_ouv_cpte_canal_fin_boutique_YTD,0) AS nb_ouv_cpte_canal_fin_boutique_YTD
, coalesce(PM.nb_ouv_cpte_canal_fin_boutique_FM,0) AS nb_ouv_cpte_canal_fin_boutique_FM
, coalesce(PY.nb_ouv_cpte_canal_fin_boutique_FY,0) AS nb_ouv_cpte_canal_fin_boutique_FY
, coalesce(CY.nb_ouv_cpte_canal_fin_crc,0) AS nb_ouv_cpte_canal_fin_crc
, coalesce(CY.nb_ouv_cpte_canal_fin_crc_WTD,0) AS nb_ouv_cpte_canal_fin_crc_WTD
, coalesce(CY.nb_ouv_cpte_canal_fin_crc_MTD,0) AS nb_ouv_cpte_canal_fin_crc_MTD
, coalesce(CY.nb_ouv_cpte_canal_fin_crc_YTD,0) AS nb_ouv_cpte_canal_fin_crc_YTD
, coalesce(PM.nb_ouv_cpte_canal_fin_crc_FM,0) AS nb_ouv_cpte_canal_fin_crc_FM
, coalesce(PY.nb_ouv_cpte_canal_fin_crc_FY,0) AS nb_ouv_cpte_canal_fin_crc_FY
, coalesce(CY.nb_ouv_cpte_full_digital,0) AS nb_ouv_cpte_full_digital
, coalesce(CY.nb_ouv_cpte_full_digital_WTD,0) AS nb_ouv_cpte_full_digital_WTD
, coalesce(CY.nb_ouv_cpte_full_digital_MTD,0) AS nb_ouv_cpte_full_digital_MTD
, coalesce(CY.nb_ouv_cpte_full_digital_YTD,0) AS nb_ouv_cpte_full_digital_YTD
, coalesce(PM.nb_ouv_cpte_full_digital_FM,0) AS nb_ouv_cpte_full_digital_FM
, coalesce(PY.nb_ouv_cpte_full_digital_FY,0) AS nb_ouv_cpte_full_digital_FY
, coalesce(CY.nb_ouv_cpte_full_boutique,0) AS nb_ouv_cpte_full_boutique
, coalesce(CY.nb_ouv_cpte_full_boutique_WTD,0) AS nb_ouv_cpte_full_boutique_WTD
, coalesce(CY.nb_ouv_cpte_full_boutique_MTD,0) AS nb_ouv_cpte_full_boutique_MTD
, coalesce(CY.nb_ouv_cpte_full_boutique_YTD,0) AS nb_ouv_cpte_full_boutique_YTD
, coalesce(PM.nb_ouv_cpte_full_boutique_FM,0) AS nb_ouv_cpte_full_boutique_FM
, coalesce(PY.nb_ouv_cpte_full_boutique_FY,0) AS nb_ouv_cpte_full_boutique_FY
, coalesce(CY.age_opport_digital,0) AS age_opport_digital
, coalesce(CY.age_opport_digital_WTD,0) AS age_opport_digital_WTD
, coalesce(CY.age_opport_digital_MTD,0) AS age_opport_digital_MTD
, coalesce(CY.age_opport_digital_YTD,0) AS age_opport_digital_YTD
, coalesce(PM.age_opport_digital_FM,0) AS age_opport_digital_FM
, coalesce(PY.age_opport_digital_FY,0) AS age_opport_digital_FY
, coalesce(CY.age_opport_boutique,0) AS age_opport_boutique
, coalesce(CY.age_opport_boutique_WTD,0) AS age_opport_boutique_WTD
, coalesce(CY.age_opport_boutique_MTD,0) AS age_opport_boutique_MTD
, coalesce(CY.age_opport_boutique_YTD,0) AS age_opport_boutique_YTD
, coalesce(PM.age_opport_boutique_FM,0) AS age_opport_boutique_FM
, coalesce(PY.age_opport_boutique_FY,0) AS age_opport_boutique_FY
INTO [dbo].[WK_SSRS_OPPORT_OUVERT]
FROM #rq_opp_ouv CY --Current Year
 LEFT OUTER JOIN #rq_opp_ouv PM --Previous Year
		  ON DATEADD(MONTH,-1,CY.jour_alim) = PM.jour_alim
	 LEFT OUTER JOIN #rq_opp_ouv PY --Previous Year
		  ON DATEADD(YEAR,-1,CY.jour_alim) = PY.jour_alim
ORDER BY CY.jour_alim;
END