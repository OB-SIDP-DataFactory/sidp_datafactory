﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PKGMC_PROC_contacts]
	@nb_collected_rows int output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

			
truncate table [dbo].[SAS_Vue_MC_Contacts];

	WITH CAMPAGNE AS ( 
SELECT distinct cmp.ID_CAMPAIGN, cmp.CAMPAIGN_NAME, cmp.[START_DATE], cmp.END_DATE, cmp.TYPOLOGY_CAMPAIGN, cmp.TYPOLOGY_NOTIFICATION, cmp.RETARGETTING_COMMENTS, cmp.RETARGETTING_FACEBOOK, cmp.RETARGETTING_TWITTER, 
cmp.STATUS_CAMPAIGN, cmp.FLAG_ROI, cmp.CHANNEL, cmp.OFFER_CODES, cmp.OFFER_LABELS, cmp.FLAG_CLIENTS, cmp.FLAG_PROSPECTS 
FROM 
[$(DataHubDatabaseName)].[dbo].[PV_MC_CAMPAIGNMESSAGE] cmp 
WHERE cmp.TYPOLOGY_CAMPAIGN='JOURNEY-COMMERCIAL' or cmp.CAMPAIGN_NAME='P_ACOM_ENROLEMENTBIS_09042017' or CAMPAIGN_NAME like '%PARRAINAGE%'
), 
OUVERTURES AS ( 
select distinct opn.SubscriberKey,opn.SendID,opn.Device,count(*) as NB_OUV 
FROM 
[$(DataHubDatabaseName)].[dbo].[PV_MC_OPENS] opn 
where opn.IsUnique='True' 
GROUP BY opn.SubscriberKey,opn.SendID,opn.Device 
), 
CLICKS AS ( 
select distinct clk.SubscriberKey,clk.SendID,clk.Device,count(*) as NB_CLI 
FROM 
[$(DataHubDatabaseName)].[dbo].[PV_MC_CLICKS] clk 
where clk.IsUnique='True' 
GROUP BY clk.SubscriberKey,clk.SendID, clk.Device  
) 

insert into [dbo].[SAS_Vue_MC_Contacts]

	
	/* Infos contacts Email */
	

SELECT distinct 
CONCAT(cmp.ID_CAMPAIGN,REVERSE(SUBSTRING(SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)),1,CHARINDEX('_',SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)))-1))) as PK_ID, 
cmp.ID_CAMPAIGN,
cmp.CAMPAIGN_NAME,
cmp.[START_DATE], 
cmp.END_DATE, 
cmp.TYPOLOGY_CAMPAIGN, 
cmp.TYPOLOGY_NOTIFICATION, 
cmp.RETARGETTING_COMMENTS,
cmp.RETARGETTING_FACEBOOK, 
cmp.RETARGETTING_TWITTER, 
cmp.STATUS_CAMPAIGN, 
cmp.FLAG_ROI, 
cmp.CHANNEL, 
cmp.OFFER_CODES,
cmp.OFFER_LABELS, 
cmp.FLAG_CLIENTS, 
cmp.FLAG_PROSPECTS, 
REVERSE(SUBSTRING(SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)),1,CHARINDEX('_',SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)))-1)) as MESSAGE_NAME, 
cast(eml.DATE_TIME_SEND as date) as DATE_TIME_SEND, 
datepart(WEEKDAY,eml.DATE_TIME_SEND) as NUM_JOUR, 
(case datepart(WEEKDAY,eml.DATE_TIME_SEND) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end) as JOUR ,  
(case when LEN(CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar))= '1' then CONCAT('0',CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar)) else CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar) end) as HEURE_ENVOI, 
opn.Device as DEVICE, 
bnc.BounceCategory as BOUNCE_CATEGORY, 
(substring(bnc.EmailAddress,(charindex('@',bnc.EmailAddress)+1),LEN(bnc.EmailAddress)-charindex('.',reverse(bnc.EmailAddress))-charindex('@',bnc.EmailAddress))) AS DOMAINE, 

count(eml.ID_CONTACT) as NB_CONTACTS_SORTANTS, 
count(eml.ID_CONTACT)-count(bnc.SubscriberKey)  as NB_DELIVRES, 

0 as NB_OUVERTURES, 
SUM(isnull(NB_OUV,0)) as NB_OUVERTURES_UNIQUES,  
0 as NB_CLICS, 
SUM(isnull(NB_CLI,0)) as NB_CLICS_UNIQUES, 

count(bnc.SubscriberKey)  as NB_BOUNCES,  
count(uns.SubscriberKey) as NB_DESABONNEMENTS 

  FROM [$(DataHubDatabaseName)].[dbo].[PV_MC_EMAILLOG] eml  
 
  LEFT JOIN CAMPAGNE cmp on eml.ID_CAMPAIGN = cmp.ID_CAMPAIGN 
  LEFT JOIN [$(DataHubDatabaseName)].[dbo].[PV_MC_BOUNCES] bnc on bnc.SubscriberKey = eml.ID_CONTACT and bnc.SendID=eml.JobID 
  LEFT JOIN OUVERTURES opn on opn.SubscriberKey = eml.ID_CONTACT and opn.SendID=eml.JobID  
  LEFT JOIN CLICKS clk on clk.SubscriberKey = eml.ID_CONTACT and clk.SendID=eml.JobID 
  LEFT JOIN [$(DataHubDatabaseName)].[dbo].[PV_MC_UNSUBS] uns on uns.SubscriberKey = eml.ID_CONTACT and uns.SendID=eml.JobID 

WHERE cmp.CHANNEL='EMAIL' 

group by 
CONCAT(cmp.ID_CAMPAIGN,REVERSE(SUBSTRING(SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)),1,CHARINDEX('_',SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)))-1))),
cmp.ID_CAMPAIGN,cmp.CAMPAIGN_NAME,cmp.[START_DATE],cmp.END_DATE,cmp.TYPOLOGY_CAMPAIGN,cmp.TYPOLOGY_NOTIFICATION,cmp.RETARGETTING_COMMENTS,
cmp.RETARGETTING_FACEBOOK,cmp.RETARGETTING_TWITTER,cmp.STATUS_CAMPAIGN,cmp.FLAG_ROI,cmp.CHANNEL,cmp.OFFER_CODES,cmp.OFFER_LABELS,cmp.FLAG_CLIENTS,cmp.FLAG_PROSPECTS, 
REVERSE(SUBSTRING(SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)),1,CHARINDEX('_',SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)))-1)), 
cast(eml.DATE_TIME_SEND as date),datepart(WEEKDAY,eml.DATE_TIME_SEND),(case datepart(WEEKDAY,eml.DATE_TIME_SEND) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end), 
(case when LEN(CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar))= '1' then CONCAT('0',CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar)) else CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar) end),opn.Device, 
bnc.BounceCategory,(substring(bnc.EmailAddress,(charindex('@',bnc.EmailAddress)+1),LEN(bnc.EmailAddress)-charindex('.',reverse(bnc.EmailAddress))-charindex('@',bnc.EmailAddress))) 

UNION 

/* Infos contacts SMS */ 

SELECT distinct 

CONCAT(cmp.ID_CAMPAIGN,REVERSE(SUBSTRING(REVERSE(sms.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(sms.MESSAGE_NAME))-1))) as PK_ID, 
cmp.ID_CAMPAIGN,
cmp.CAMPAIGN_NAME,
cmp.[START_DATE], 
cmp.END_DATE, 
cmp.TYPOLOGY_CAMPAIGN, 
cmp.TYPOLOGY_NOTIFICATION, 
cmp.RETARGETTING_COMMENTS,
cmp.RETARGETTING_FACEBOOK, 
cmp.RETARGETTING_TWITTER, 
cmp.STATUS_CAMPAIGN, 
cmp.FLAG_ROI, 
cmp.CHANNEL, 
cmp.OFFER_CODES,
cmp.OFFER_LABELS, 
cmp.FLAG_CLIENTS, 
cmp.FLAG_PROSPECTS, 
REVERSE(SUBSTRING(REVERSE(sms.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(sms.MESSAGE_NAME))-1)) AS MESSAGE_NAME, 
cast(sms.DATE_TIME_SEND as date) as DATE_TIME_SEND, 
datepart(WEEKDAY,sms.DATE_TIME_SEND) as NUM_JOUR, 
(case datepart(WEEKDAY,sms.DATE_TIME_SEND) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end) as JOUR ,  
(case when LEN(CAST(datepart(hour,sms.DATE_TIME_SEND) as nvarchar))= '1' then CONCAT('0',CAST(datepart(hour,sms.DATE_TIME_SEND) as nvarchar)) else CAST(datepart(hour,sms.DATE_TIME_SEND) as nvarchar) end) as HEURE_ENVOI, 
NULL as DEVICE, 
NULL as BOUNCE_CATEGORY,   
NULL AS DOMAINE, 

count(case when sms.MESSAGE_TYPE='Outbound' then sms.ID_CONTACT end) as NB_CONTACTS_SORTANTS , 
count(case when sms.MESSAGE_TYPE='Outbound' and sms.STATUS='Delivered' then sms.ID_CONTACT end) as NB_DELIVRES, 
0 as NB_OUVERTURES , 
0 as NB_OUVERTURES_UNIQUES , 
0 as NB_CLICS , 
0 as NB_CLICS_UNIQUES , 
0  as NB_BOUNCES, 
count(case when sms.MESSAGE_TYPE='STOP' then sms.ID_CONTACT end) as NB_DESABONNEMENTS 

  FROM [$(DataHubDatabaseName)].[dbo].[PV_MC_SMSLOG] sms  
 
  LEFT JOIN CAMPAGNE cmp on sms.[ID_CAMPAIGN] = cmp.ID_CAMPAIGN  

WHERE cmp.CHANNEL='SMS' 

group by 
CONCAT(cmp.ID_CAMPAIGN,REVERSE(SUBSTRING(REVERSE(sms.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(sms.MESSAGE_NAME))-1))),cmp.ID_CAMPAIGN,cmp.CAMPAIGN_NAME,cmp.[START_DATE],cmp.END_DATE,cmp.TYPOLOGY_CAMPAIGN,cmp.TYPOLOGY_NOTIFICATION,cmp.RETARGETTING_COMMENTS,cmp.RETARGETTING_FACEBOOK,cmp.RETARGETTING_TWITTER,
cmp.STATUS_CAMPAIGN,cmp.FLAG_ROI,cmp.CHANNEL,cmp.OFFER_CODES,cmp.OFFER_LABELS,cmp.FLAG_CLIENTS,cmp.FLAG_PROSPECTS, 
REVERSE(SUBSTRING(REVERSE(sms.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(sms.MESSAGE_NAME))-1)),cast(sms.DATE_TIME_SEND as date),datepart(WEEKDAY,sms.DATE_TIME_SEND), 
(case datepart(WEEKDAY,sms.DATE_TIME_SEND) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end), 
(case when LEN(CAST(datepart(hour,sms.DATE_TIME_SEND) as nvarchar))= '1' then CONCAT('0',CAST(datepart(hour,sms.DATE_TIME_SEND) as nvarchar)) else CAST(datepart(hour,sms.DATE_TIME_SEND) as nvarchar) end) 
UNION 

/* Infos contacts Push */ 

SELECT  

CONCAT(cmp.ID_CAMPAIGN,REVERSE(SUBSTRING(REVERSE(push.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(push.MESSAGE_NAME))-1))) as PK_ID, 
cmp.ID_CAMPAIGN,
cmp.CAMPAIGN_NAME,
cmp.[START_DATE], 
cmp.END_DATE, 
cmp.TYPOLOGY_CAMPAIGN, 
cmp.TYPOLOGY_NOTIFICATION, 
cmp.RETARGETTING_COMMENTS,
cmp.RETARGETTING_FACEBOOK, 
cmp.RETARGETTING_TWITTER, 
cmp.STATUS_CAMPAIGN, 
cmp.FLAG_ROI, 
cmp.CHANNEL, 
cmp.OFFER_CODES,
cmp.OFFER_LABELS, 
cmp.FLAG_CLIENTS, 
cmp.FLAG_PROSPECTS, 
REVERSE(SUBSTRING(REVERSE(push.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(push.MESSAGE_NAME))-1)) AS MESSAGE_NAME, 
cast(push.DATE_TIME_SEND as date) as DATE_TIME_SEND, 
datepart(WEEKDAY,push.DATE_TIME_SEND) as NUM_JOUR, 
(case datepart(WEEKDAY,push.DATE_TIME_SEND) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end) as JOUR ,  
(case when LEN(CAST(datepart(hour,push.DATE_TIME_SEND) as nvarchar))= '1' then CONCAT('0',CAST(datepart(hour,push.DATE_TIME_SEND) as nvarchar)) else CAST(datepart(hour,push.DATE_TIME_SEND) as nvarchar) end) as HEURE_ENVOI, 
NULL as DEVICE, 
NULL as BOUNCE_CATEGORY, 
NULL AS DOMAINE, 

count(push.ID_CONTACT) as NB_CONTACTS_SORTANTS , 
count(case when push.STATUS='Success' then push.ID_CONTACT end) as NB_DELIVRES, 
count(case when push.MESSAGE_OPENED='yes' then push.ID_CONTACT end) as NB_OUVERTURES , 
count(distinct (case when push.MESSAGE_OPENED='yes' then push.ID_CONTACT end)) as NB_OUVERTURES_UNIQUES , 
0 as NB_CLICS , 
0 as NB_CLICS_UNIQUES , 
count(case when push.MESSAGE_OPENED='no' then push.ID_CONTACT end)  as NB_BOUNCES, 
0 as NB_DESABONNEMENTS 

  FROM [$(DataHubDatabaseName)].[dbo].[PV_MC_PUSHLOG] push 
 
  LEFT JOIN CAMPAGNE cmp on push.[ID_CAMPAIGN] = cmp.ID_CAMPAIGN 

WHERE cmp.CHANNEL='PUSH' 

group by 
CONCAT(cmp.ID_CAMPAIGN,REVERSE(SUBSTRING(REVERSE(push.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(push.MESSAGE_NAME))-1))),cmp.ID_CAMPAIGN,cmp.CAMPAIGN_NAME,cmp.[START_DATE],cmp.END_DATE,cmp.TYPOLOGY_CAMPAIGN,cmp.TYPOLOGY_NOTIFICATION,cmp.RETARGETTING_COMMENTS,cmp.RETARGETTING_FACEBOOK,cmp.RETARGETTING_TWITTER, 
cmp.STATUS_CAMPAIGN,cmp.FLAG_ROI,cmp.CHANNEL,cmp.OFFER_CODES,cmp.OFFER_LABELS,cmp.FLAG_CLIENTS,cmp.FLAG_PROSPECTS,REVERSE(SUBSTRING(REVERSE(push.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(push.MESSAGE_NAME))-1)), 
cast(push.DATE_TIME_SEND as date),datepart(WEEKDAY,push.DATE_TIME_SEND),
(case datepart(WEEKDAY,push.DATE_TIME_SEND) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end),  
(case when LEN(CAST(datepart(hour,push.DATE_TIME_SEND) as nvarchar))= '1' then CONCAT('0',CAST(datepart(hour,push.DATE_TIME_SEND) as nvarchar)) else CAST(datepart(hour,push.DATE_TIME_SEND) as nvarchar) end) 

UNION 

/* Infos contacts appels */ 

select distinct 

apl.[ID_CAMPAIGN] as PK_ID, 
cmp.ID_CAMPAIGN,
cmp.CAMPAIGN_NAME,
cmp.[START_DATE], 
cmp.END_DATE, 
cmp.TYPOLOGY_CAMPAIGN, 
cmp.TYPOLOGY_NOTIFICATION, 
cmp.RETARGETTING_COMMENTS,
cmp.RETARGETTING_FACEBOOK, 
cmp.RETARGETTING_TWITTER, 
cmp.STATUS_CAMPAIGN, 
cmp.FLAG_ROI, 
'CALL' as CHANNEL, 
cmp.OFFER_CODES, 
cmp.OFFER_LABELS, 
cmp.FLAG_CLIENTS, 
cmp.FLAG_PROSPECTS, 
NULL as MESSAGE_NAME, 
cast(apl.CALL_DATE as date) as DATE_TIME_SEND, 
datepart(WEEKDAY,apl.CALL_DATE) as NUM_JOUR, 
(case datepart(WEEKDAY,apl.CALL_DATE) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end) as JOUR , 
apl.HEURE_APPEL as HEURE_ENVOI, 
NULL as DEVICE, 
NULL as BOUNCE_CATEGORY, 
NULL as DOMAINE, 

count(apl.ID_CONTACT) as NB_CONTACTS_SORTANTS , 
count(case when apl.FLG_CONTACTE ='Y' then apl.ID_CONTACT end) as NB_DELIVRES, 
0 as NB_OUVERTURES , 
0 as NB_OUVERTURES_UNIQUES , 
0 as NB_CLICS , 
0 as NB_CLICS_UNIQUES , 
count(case when apl.FLG_CONTACT_BOUNCES ='Y' then apl.ID_CONTACT end) as NB_BOUNCES, 
count(case when apl.FLG_CONTACT_STOP ='Y' then apl.ID_CONTACT end) as NB_DESABONNEMENTS 

from 
[dbo].[WK_MC_CALLLOG] apl 
  
  LEFT JOIN CAMPAGNE cmp on cmp.ID_CAMPAIGN=apl.ID_CAMPAIGN  

group by 
apl.[ID_CAMPAIGN],cmp.ID_CAMPAIGN,cmp.CAMPAIGN_NAME,cmp.[START_DATE],cmp.END_DATE,cmp.TYPOLOGY_CAMPAIGN,cmp.TYPOLOGY_NOTIFICATION,cmp.RETARGETTING_COMMENTS,cmp.RETARGETTING_FACEBOOK,cmp.RETARGETTING_TWITTER,cmp.STATUS_CAMPAIGN, 
cmp.FLAG_ROI,cmp.OFFER_CODES,cmp.OFFER_LABELS,cmp.FLAG_CLIENTS,cmp.FLAG_PROSPECTS,cast(apl.CALL_DATE as date),datepart(WEEKDAY,apl.CALL_DATE),
(case datepart(WEEKDAY,apl.CALL_DATE) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end),apl.HEURE_APPEL
		select @nb_collected_rows = COUNT(*) from [dbo].[SAS_Vue_MC_Contacts];
END