﻿CREATE PROC [dbo].[PROC_SSRS_VUE_EQUIPEMENT]

AS  BEGIN 

DECLARE @date_obs DATE;
SELECT  @date_obs = CAST(GETDATE() AS DATE);
DECLARE @date_max date;
SELECT  @date_max = dateadd(day, -1 ,GETDATE());
SET DATEFIRST 1; -->1    Lundi
DROP TABLE IF EXISTS #rq_eq_assur_cp;
DROP TABLE IF EXISTS [dbo].[WK_SSRS_VUE_EQUIPEMENT];

SELECT Year_alim,Month_alim,Week_alim,jour_alim
    ,nb_assurances
	,nb_assurances_moy_pai
	,nb_assurances_prot_vie_cour
	,nb_assurances_prot_achats
	,nb_assurances_ident
	--Week to date
	,sum(nb_assurances) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_assurances_WTD --Week to date
	,sum(nb_assurances_moy_pai) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_assurances_moy_pai_WTD --Week to date
	,sum(nb_assurances_prot_vie_cour) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_assurances_prot_vie_cour_WTD --Week to date
	,sum(nb_assurances_prot_achats) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_assurances_prot_achats_WTD --Week to date
	,sum(nb_assurances_ident) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_assurances_ident_WTD --Week to date
	--Month to date
	,sum(nb_assurances) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_assurances_MTD --Month to date
	,sum(nb_assurances_moy_pai) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_assurances_moy_pai_MTD --Month to date
	,sum(nb_assurances_prot_vie_cour) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_assurances_prot_vie_cour_MTD --Month to date
	,sum(nb_assurances_prot_achats) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_assurances_prot_achats_MTD --Month to date
	,sum(nb_assurances_ident) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_assurances_ident_MTD --Month to date
	--Full Month
	,sum(nb_assurances) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_assurances_FM --Full Month
	,sum(nb_assurances_moy_pai) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_assurances_moy_pai_FM --Full Month
	,sum(nb_assurances_prot_vie_cour) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_assurances_prot_vie_cour_FM --Full Month
	,sum(nb_assurances_prot_achats) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_assurances_prot_achats_FM --Full Month
	,sum(nb_assurances_ident) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_assurances_ident_FM --Full Month
	--Year to date
    ,sum(nb_assurances) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_assurances_YTD  --Year to date
    ,sum(nb_assurances_moy_pai) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_assurances_moy_pai_YTD  --Year to date
    ,sum(nb_assurances_prot_vie_cour) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_assurances_prot_vie_cour_YTD  --Year to date
    ,sum(nb_assurances_prot_achats) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_assurances_prot_achats_YTD  --Year to date
    ,sum(nb_assurances_ident) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_assurances_ident_YTD  --Year to date
	--Full year
	,sum(nb_assurances) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_assurances_FY  --Full year
	,sum(nb_assurances_moy_pai) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_assurances_moy_pai_FY  --Full year
	,sum(nb_assurances_prot_vie_cour) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_assurances_prot_vie_cour_FY  --Full year
	,sum(nb_assurances_prot_achats) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_assurances_prot_achats_FY  --Full year
	,sum(nb_assurances_ident) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_assurances_ident_FY  --Full year
INTO #rq_eq_assur_cp
	FROM  ( 
     SELECT  T1.StandardDate as jour_alim,DATEPART(YEAR,T1.Date) AS Year_alim,DATEPART(MONTH,T1.Date) AS Month_alim,DATEPART(YEAR,DATEADD(day, (7 - DATEPART(dw, T1.StandardDate)), T1.StandardDate)) *100 + DATEPART(WEEK,DATEADD(day, (7 - DATEPART(dw, T1.StandardDate)), T1.StandardDate)) AS Week_alim
             , sum(nb_assurances) AS nb_assurances
                 , sum(case when code_fe_assurance = 1 then nb_assurances else 0 end) AS nb_assurances_moy_pai
                 , sum(case when code_fe_assurance = 2 then nb_assurances else 0 end) AS nb_assurances_prot_vie_cour
                 , sum(case when code_fe_assurance = 3 then nb_assurances else 0 end) AS nb_assurances_prot_achats
                 , sum(case when code_fe_assurance = 4 then nb_assurances else 0 end) AS nb_assurances_ident
FROM [dbo].[DIM_TEMPS] T1 
LEFT OUTER JOIN  [dbo].[ssrs_vue_equipement]  ON T1.StandardDate=ssrs_vue_equipement.date_souscription
WHERE  ( T1.StandardDate >= CAST(DATEADD(yyyy, DATEDIFF(YYYY, 0, DATEADD(yyyy, -2, @date_obs)), 0) AS DATE)
AND T1.StandardDate <=  DATEADD(day, (7 - DATEPART(dw, @date_obs)), @date_obs) )
GROUP BY  T1.StandardDate ,DATEPART(YEAR,T1.Date) ,DATEPART(MONTH,T1.Date) ,DATEPART(WEEK,T1.Date) 
) t	;

SELECT
  CY.jour_alim
, CY.Year_alim
, CY.Month_alim
, CY.Week_alim
, coalesce(CY.nb_assurances,0) AS nb_assurances
, coalesce(CY.nb_assurances_WTD,0) AS nb_assurances_WTD
, coalesce(CY.nb_assurances_MTD,0) AS nb_assurances_MTD
, coalesce(CY.nb_assurances_YTD,0) AS nb_assurances_YTD
, coalesce(PM.nb_assurances_FM,0) AS nb_assurances_FM
, coalesce(PY.nb_assurances_FY,0) AS nb_assurances_FY
, coalesce(CY.nb_assurances_moy_pai,0) AS nb_assurances_moy_pai
, coalesce(CY.nb_assurances_moy_pai_WTD,0) AS nb_assurances_moy_pai_WTD
, coalesce(CY.nb_assurances_moy_pai_MTD,0) AS nb_assurances_moy_pai_MTD
, coalesce(CY.nb_assurances_moy_pai_YTD,0) AS nb_assurances_moy_pai_YTD
, coalesce(PM.nb_assurances_moy_pai_FM,0) AS nb_assurances_moy_pai_FM
, coalesce(PY.nb_assurances_moy_pai_FY,0) AS nb_assurances_moy_pai_FY
, coalesce(CY.nb_assurances_prot_vie_cour,0) AS nb_assurances_prot_vie_cour
, coalesce(CY.nb_assurances_prot_vie_cour_WTD,0) AS nb_assurances_prot_vie_cour_WTD
, coalesce(CY.nb_assurances_prot_vie_cour_MTD,0) AS nb_assurances_prot_vie_cour_MTD
, coalesce(CY.nb_assurances_prot_vie_cour_YTD,0) AS nb_assurances_prot_vie_cour_YTD
, coalesce(PM.nb_assurances_prot_vie_cour_FM,0) AS nb_assurances_prot_vie_cour_FM
, coalesce(PY.nb_assurances_prot_vie_cour_FY,0) AS nb_assurances_prot_vie_cour_FY
, coalesce(CY.nb_assurances_prot_achats,0) AS nb_assurances_prot_achats
, coalesce(CY.nb_assurances_prot_achats_WTD,0) AS nb_assurances_prot_achats_WTD
, coalesce(CY.nb_assurances_prot_achats_MTD,0) AS nb_assurances_prot_achats_MTD
, coalesce(CY.nb_assurances_prot_achats_YTD,0) AS nb_assurances_prot_achats_YTD
, coalesce(PM.nb_assurances_prot_achats_FM,0) AS nb_assurances_prot_achats_FM
, coalesce(PY.nb_assurances_prot_achats_FY,0) AS nb_assurances_prot_achats_FY
, coalesce(CY.nb_assurances_ident,0) AS nb_assurances_ident
, coalesce(CY.nb_assurances_ident_WTD,0) AS nb_assurances_ident_WTD
, coalesce(CY.nb_assurances_ident_MTD,0) AS nb_assurances_ident_MTD
, coalesce(CY.nb_assurances_ident_YTD,0) AS nb_assurances_ident_YTD
, coalesce(PM.nb_assurances_ident_FM,0) AS nb_assurances_ident_FM
, coalesce(PY.nb_assurances_ident_FY,0) AS nb_assurances_ident_FY
INTO  [dbo].[WK_SSRS_VUE_EQUIPEMENT]
FROM #rq_eq_assur_cp CY --Current Year
 LEFT OUTER JOIN #rq_eq_assur_cp PM --Previous Year
		  ON DATEADD(MONTH,-1,CY.jour_alim) = PM.jour_alim
	 LEFT OUTER JOIN #rq_eq_assur_cp PY --Previous Year
		  ON DATEADD(YEAR,-1,CY.jour_alim) = PY.jour_alim
ORDER BY CY.jour_alim;
END