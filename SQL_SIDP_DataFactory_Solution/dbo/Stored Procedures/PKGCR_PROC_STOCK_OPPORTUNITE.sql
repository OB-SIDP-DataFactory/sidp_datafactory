﻿

CREATE PROC [dbo].[PKGCR_PROC_STOCK_OPPORTUNITE]
    @nbRows int OUTPUT -- nb lignes processées  
AS
BEGIN 
-- =====================================================================================================================
-- Author:		<Liliane, Huang>
-- Create date: <30/01/2018>
-- Description : <créer une table agrégée avec le nombre d'opportunités  de type Crédit en stock pour chaque jour d'alimentation>
--				vider les tables temporaires utilisées
-- Tables input : [CRE_WK_STOCK_OPPORTUNITY]
-- Table output : [CRE_STOCK_OPPORTUNITE]
-- ======================================================================================================================
--Insertion des données dans la table CRE_STOCK_OPPORTUNITE et aggrégation des opportunités
INSERT INTO dbo.CRE_STOCK_OPPORTUNITE
			( DTE_ALIM,
			NBE_OPPRT,
			STADE_VENTE,
			LIB_STADE_VENTE,
			FLG_IND,
			IDE_USER_CREA_SF,
			DTE_CREA,
			DTE_CLO_OPPRT,
			AGE_OPPRT,
			IDE_USER_SF,
			COD_VENDEUR,
			COD_BOUT,
			COD_OFF_COM,
			LABEL_OFF_COM,
			SCO_ENT_REL,
			CAN_INT_ORI_IND,
			LIB_CAN_INT_ORI_IND,
			COD_CAN_INT_ORI,
			LIB_CAN_INT_ORI,
			COD_CAN_DIS,
			LIB_CAN_DIS,
			COD_RES_DIS,
			LIB_RES_DIS,
			ENT_DIS,
			LIB_ENT_DIS,
			COD_CAN_INT,
			LIB_CAN_INT
)
select OPP.date_alim,
		count(distinct OPP.Id_SF) AS NBE_OPPRT,
		OPP.Cod_StageName,
		OPP.Lib_StageName,
		OPP.flag_indication,
		OPP.CreatedById,
		d5c.StandardDate as CreatedDate,
		d5f.StandardDate as CloseDate,
		OPP.age_opport,
		OPP.OwnerId,
		OPP.AdvisorCode__c,
		OPP.PointOfSaleCode__c,
		OPP.CommercialOfferCode__c,
		OPP.CommercialOfferName__c,
		OPP.RelationEntryScore__c,
		OPP.Cod_StartedChannelForIndication__c,
		OPP.Lib_StartedChannelForIndication__c,
		OPP.Cod_StartedChannel__c,
		OPP.Lib_StartedChannel__c,
		OPP.Cod_DistributionChannel__c,
		OPP.Lib_DistributionChannel__c,
		OPP.Cod_DistributorNetwork__c,
		OPP.Lib_DistributorNetwork__c,
		OPP.Cod_DistributorEntity__c,
		OPP.Lib_DistributorEntity__c,
		OPP.Cod_LeadSource,
		OPP.Lib_LeadSource
from [dbo].[CRE_WK_STOCK_OPPORTUNITY] opp
left join DIM_TEMPS d5c
	on cast(OPP.CreatedDate as date) = d5c.Date
left join DIM_TEMPS d5f
	on cast(OPP.CloseDate as date) = d5f.Date
where OPP.flag_opport_last_state = 1
group by OPP.date_alim,
		OPP.Cod_StageName,
		OPP.Lib_StageName,
		OPP.flag_indication,
		OPP.CreatedById,
		d5c.StandardDate,
		d5f.StandardDate,
		OPP.age_opport,
		OPP.OwnerId,
		OPP.AdvisorCode__c,
		OPP.PointOfSaleCode__c,
		OPP.CommercialOfferCode__c,
		OPP.CommercialOfferName__c,
		OPP.RelationEntryScore__c,
		OPP.Cod_StartedChannelForIndication__c,
		OPP.Lib_StartedChannelForIndication__c,
		OPP.Cod_StartedChannel__c,
		OPP.Lib_StartedChannel__c,
		OPP.Cod_DistributionChannel__c,
		OPP.Lib_DistributionChannel__c,
		OPP.Cod_DistributorNetwork__c,
		OPP.Lib_DistributorNetwork__c,
		OPP.Cod_DistributorEntity__c,
		OPP.Lib_DistributorEntity__c,
		OPP.Cod_LeadSource,
		OPP.Lib_LeadSource
;	

select @nbRows = @@ROWCOUNT;

EXEC [dbo].[PROC_SSRS_CRE_STOCK_OPPORTUNITE];

END
GO