﻿CREATE PROCEDURE [dbo].[PKGES_PROC_OPPORT_VISION_GLOBALE_OB]

@Datedebut date, 
@Datefin date   

AS BEGIN

select T.OrigineID,T.Origine,sum(T.TotalEnCours)+sum(T.TotalFinal)+sum(T.Incident)+sum(T.SansSuite)+sum(T.Refusee)+sum(T.Retaractaction) as nb_opport,
sum(T.CanalAppliEnCours) CanalAppliEnCours,sum(T.CanalCRCEnCours) CanalCRCEnCours ,sum(T.CanalWebEnCours) CanalWebEnCours,sum(T.TotalEnCours) TotalEnCours,
sum(T.CanalWebFinal) CanalWebFinal,sum(T.CanalAppliFinal) CanalAppliFinal,sum(T.TotalFinal) TotalFinal,
sum(T.Incident) Incident,sum(T.SansSuite) SansSuite ,sum(T.Refusee) Refusee,sum(T.Retaractaction) Retaractaction
from (
Select 
case when StartedChannel__c ='11' then 1 when StartedChannel__c ='10' then 2 when StartedChannel__c in ('12','13') then 3  end as OrigineID,
case when StartedChannel__c ='11' then 'Origine Web' when StartedChannel__c ='10' then 'Origine Appli' when StartedChannel__c in ('12','13') then 'Origine CRC'  end as Origine,
-- En cours
case when StageName not in ('08','09','10','11','12') and  LeadSource='10' then count(distinct Wk.Id_SF) else 0 end as  CanalAppliEnCours,
case when StageName not in ('08','09','10','11','12') and  LeadSource='11' then count(distinct Wk.Id_SF) else 0 end as  CanalWebEnCours,
case when StageName not in ('08','09','10','11','12') and  LeadSource in ('12','13') then count(distinct Wk.Id_SF) else 0 end as  CanalCRCEnCours,
case when StageName not in ('08','09','10','11','12') then count(distinct Wk.Id_SF) else 0 end as  TotalEnCours,
-- Finalisé
case when StageName ='09' and  LeadSource='10' then count(distinct Wk.Id_SF) else 0 end as  CanalAppliFinal,
case when StageName ='09' and  LeadSource='11' then count(distinct Wk.Id_SF) else 0 end as  CanalWebFinal,
case when StageName ='09' then count(distinct Wk.Id_SF) else 0 end as  TotalFinal,

case when StageName ='08' then count(distinct Wk.Id_SF) else 0 end as  Incident,
case when StageName ='10' then count(distinct Wk.Id_SF) else 0 end as  Refusee,
case when StageName ='11' then count(distinct Wk.Id_SF) else 0 end as  SansSuite,
case when StageName ='12' then count(distinct Wk.Id_SF) else 0 end as  Retaractaction
from [dbo].[WK_STOCK_OPPORTUNITY] as Wk
left outer join  [$(DataHubDatabaseName)].[dbo].[PV_SF_OPPORTUNITYISDELETED] as Wkd on Wk.Id_SF= Wkd.Id_SF
where Wkd.Id_SF is null and DistributorNetwork__c='02' --Orange Bank
and CommercialOfferCode__c='OC80'
and  cast(CreatedDate as date) between @Datedebut and @Datefin
and flag_opport_last_state = 1
and StartedChannel__c in ('10','11','12','13')
and LeadSource in ('01','02','03','10','11','12','13')
and [date_alim]=(SELECT MAX([date_alim]) FROM [dbo].[WK_STOCK_OPPORTUNITY])
group by StartedChannel__c,StageName,LeadSource
) as T
group by T.OrigineID,T.Origine

END;
