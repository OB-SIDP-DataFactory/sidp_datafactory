﻿CREATE PROC [dbo].[PKGCP_SUIVI_ENROLEMENT_BOUTIQUE]
   @date_obs date,
   @DO varchar(max),
   @AD varchar(max),
   @canal_fin varchar(255)
AS  BEGIN 
with 
cte_jour_alim_list as
(
select StandardDate as date_creat
     , t.code_canal_orig
     , t.code_canal_fin
	 , isnull(t.DO,'ND : Boutique inexistante dans REF_BOUTIQUE')as DO,isnull(t.AD,'ND : Boutique inexistante dans REF_BOUTIQUE') as AD
     , t.code_boutique
  from dbo.DIM_TEMPS
  cross join ( select distinct (case when opp.Indication__c='Oui' then opp.StartedChannelForIndication__c else opp.StartedChannel__c end) as code_canal_orig, case when IsWon = 'true' then opp.LeadSource else '-1' end as code_canal_fin, coalesce(opp.PointOfSaleCode__c,'ND') as code_boutique,bo.DO,bo.AD
                 from  [$(DataHubDatabaseName)].dbo.VW_PV_SF_OPPORTUNITY_HISTORY opp
                 join dbo.DIM_CANAL co on co.CODE_SF = opp.StartedChannel__c and co.PARENT_ID is not null and co.LIBELLE in ('Boutique IOBSP', 'Boutique non IOBSP')
                 join dbo.DIM_CANAL cf on cf.CODE_SF = case when IsWon = 'true' then opp.LeadSource else '-1' end and cf.PARENT_ID is not null and cf.CODE_SF in (select [Value] from dbo.FnSplit(@canal_fin, ','))
				 left outer join (select distinct DO, ISNULL(AD,'GDT') AS AD,Code_ADV_Orange  from  [$(DataHubDatabaseName)].dbo.REF_BOUTIQUE) bo on coalesce(opp.PointOfSaleCode__c,'ND')=bo.Code_ADV_Orange
				 left outer join (select distinct ShopCode__c from [$(DataHubDatabaseName)].dbo.PV_SF_ACCOUNT where RecordTypeId='01258000000gtzgAAA') as acct on coalesce(opp.PointOfSaleCode__c,'ND')=acct.ShopCode__c      
				 where  DistributorNetwork__c='01' and DistributorEntity__c in ('01','02') ) t
 where dateadd(day,-30, @date_obs) <= StandardDate and StandardDate <= @date_obs
),
cte as (
select date_creat
     , canal_orig
     , canal_fin
	 , DO, AD
     , code_boutique
     , age_opport
     , stade_vente
     , count(Id_SF) as nb_opp_created
     , count(case when IsWon = 'true' and date_close = date_creat then Id_SF else null end) as nb_ouvert_meme_jour
     , count(case when IsWon = 'true' and date_close <= @date_obs then Id_SF else null end) as nb_ouvert_a_date
  from ( select Id_SF
              , cast(CreatedDate as date) as date_creat
              , cast(CloseDate as date) as date_close
              , DATEDIFF(DD, cast(CreatedDate as date), @date_obs) as age_opport
              , StageName as stade_vente
              , IsWon
			  , isnull(bo.DO,'ND : Boutique inexistante dans REF_BOUTIQUE')as DO,isnull(bo.AD,'ND : Boutique inexistante dans REF_BOUTIQUE') as AD
              , case when Indication__c='Oui' then StartedChannelForIndication__c else StartedChannel__c end  as canal_orig
              , case when IsWon = 'true' then LeadSource else '-1' end as canal_fin
              , coalesce(PointOfSaleCode__c, 'ND') as code_boutique
              , row_number() over (partition by Id_SF order by Validity_StartDate desc) as flag_opport_last_state
           from [$(DataHubDatabaseName)].dbo.VW_PV_SF_OPPORTUNITY_HISTORY
		   left outer join (select distinct DO, ISNULL(AD,'GDT') AS AD,Code_ADV_Orange  from  [$(DataHubDatabaseName)].dbo.REF_BOUTIQUE) bo on coalesce(PointOfSaleCode__c,'ND')=bo.Code_ADV_Orange
           where ( cast(Validity_StartDate as date) <= @date_obs and @date_obs <= cast(Validity_EndDate as date) and CommercialOfferCode__c = 'OC80' and CommercialOfferName__c is not null and  DistributorNetwork__c='01' and DistributorEntity__c in ('01','02')
		  and cast(CreatedDate as date) >='2017-05-16')
		  ) oh
 where flag_opport_last_state = 1 
   and date_creat >= dateadd(day,-30, @date_obs) and date_creat <= @date_obs
 group by date_creat
        , canal_orig
        , canal_fin
        , code_boutique
        , age_opport
        , stade_vente,DO,AD
),

cte_expand as (
select jour_alim_list.date_creat
     , jour_alim_list.code_canal_orig as canal_orig
     , jour_alim_list.code_canal_fin as canal_fin
	 , jour_alim_list.DO,jour_alim_list.AD
     , jour_alim_list.code_boutique as code_boutique
     , coalesce(opport.age_opport, 9999) as age_opport
     , coalesce(opport.stade_vente, '-1') as stade_vente
     , coalesce(opport.nb_opp_created, 0) as nb_opp_created
     , coalesce(opport.nb_ouvert_meme_jour, 0) as nb_ouvert_meme_jour
     , coalesce(opport.nb_ouvert_a_date, 0) as nb_ouvert_a_date
  from cte_jour_alim_list as jour_alim_list
  left join cte as opport
    on opport.date_creat = jour_alim_list.date_creat and
       opport.canal_orig = jour_alim_list.code_canal_orig and
       opport.canal_fin = jour_alim_list.code_canal_fin and
       opport.code_boutique = jour_alim_list.code_boutique
),

cte_cum as (
select date_creat
     , canal_orig
     , canal_fin
	 , DO,AD
     , code_boutique
     , age_opport
     , stade_vente
     , nb_opp_created
     , nb_ouvert_meme_jour
     , nb_ouvert_a_date
     , SUM(nb_opp_created) OVER(PARTITION BY canal_orig, code_boutique, canal_fin ORDER BY date_creat, age_opport, stade_vente ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opp_created_cum
     , SUM(nb_ouvert_meme_jour) OVER(PARTITION BY canal_orig, code_boutique, canal_fin ORDER BY date_creat, age_opport, stade_vente ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_ouvert_meme_jour_cum
     , SUM(nb_ouvert_a_date) OVER(PARTITION BY canal_orig, code_boutique, canal_fin ORDER BY date_creat, age_opport, stade_vente ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_ouvert_a_date_cum
  FROM cte_expand ),

cte_fin as (
select opport.date_creat
     , opport.canal_orig, opport.DO, opport.AD
     , case when acct.ShopCode__c IS NULL then  'Boutique non IOBSP' Else 'Boutique IOBSP' end as lib_canal_orig
     , opport.canal_fin
     , canal_fin.LIBELLE as lib_canal_fin
     , opport.code_boutique
     , boutique.ShopBrand__c + ' - ' + boutique.Name as lib_boutique
     , case when opport.age_opport <= 0 then 0
            when opport.age_opport <= 1 then 1
            when opport.age_opport <= 3 then 3
            when opport.age_opport <= 5 then 5
            when opport.age_opport <= 10 then 10
            when opport.age_opport <= 20 then 20
            when opport.age_opport <= 30 then 30
            when opport.age_opport <= 60 then 60
            else 9999
       end as age_opport
     , case when opport.age_opport <= 0 then 'Jour même'
            when opport.age_opport <= 1 then '1 jour'
            when opport.age_opport <= 3 then '2-3 jours'
            when opport.age_opport <= 5 then '4-5 jours'
            when opport.age_opport <= 10 then '6-10 jours'
            when opport.age_opport <= 20 then '11-20 jours'
            when opport.age_opport <= 30 then '21-30 jours'
            when opport.age_opport <= 60 then '31-60 jours'
            else '60+ jours'
       end as lib_age_opport
     , opport.stade_vente
     , stade_vente.LIBELLE as lib_stade_vente
     , opport.nb_opp_created as nb_opp_created
     , opport.nb_ouvert_meme_jour as nb_ouvert_meme_jour
     , opport.nb_ouvert_a_date as nb_ouvert_a_date
  from cte_cum as opport
  left join (select distinct ShopCode__c from [$(DataHubDatabaseName)].dbo.PV_SF_ACCOUNT where RecordTypeId='01258000000gtzgAAA') as acct
    on opport.code_boutique=acct.ShopCode__c 
  left join dbo.DIM_CANAL canal_fin
    on canal_fin.CODE_SF = opport.canal_fin and canal_fin.PARENT_ID is not null
  left join dbo.DIM_STADE_VENTE stade_vente
    on stade_vente.CODE_SF = opport.stade_vente and stade_vente.PARENT_ID is null
  left join [$(DataHubDatabaseName)].dbo.PV_SF_ACCOUNT boutique
    on boutique.ShopCode__c = opport.code_boutique
)

select * from cte_fin  
 where canal_orig is not null and  DO in (select [Value] from dbo.FnSplit(@DO, ',')) and AD in (select [Value] from dbo.FnSplit(@AD, ',')) and code_boutique<>'92BAGFICT2'
 END
