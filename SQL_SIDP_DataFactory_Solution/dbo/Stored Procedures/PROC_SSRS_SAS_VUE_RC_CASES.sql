﻿CREATE PROC [dbo].[PROC_SSRS_SAS_VUE_RC_CASES]

AS  BEGIN 

DECLARE @date_obs DATE;
SELECT  @date_obs = CAST(GETDATE() AS DATE);
DECLARE @date_max date;
SELECT  @date_max = dateadd(day, -1 ,GETDATE());
SET DATEFIRST 1; -->1    Lundi
DROP TABLE IF EXISTS #rq_cli_case_cp;
DROP TABLE IF EXISTS [dbo].[WK_SSRS_SAS_VUE_RC_CASES];

SELECT Year_alim,Month_alim,Week_alim,jour_alim
    ,nb_demandes_clients
	,nb_reclamations_clients
	,flag_fcr
	,total_case_jour
	,nb_demandes_chq
	--Week to date
	,sum(nb_demandes_clients) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_demandes_clients_WTD --Week to date
	,sum(nb_reclamations_clients) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_reclamations_clients_WTD --Week to date
	,sum(flag_fcr) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS flag_fcr_WTD --Week to date
	,sum(total_case_jour) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS total_case_jour_WTD --Week to date
	,sum(nb_demandes_chq) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_demandes_chq_WTD --Week to date
	--Month to date
	,sum(nb_demandes_clients) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_demandes_clients_MTD --Month to date
	,sum(nb_reclamations_clients) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_reclamations_clients_MTD --Month to date
	,sum(flag_fcr) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS flag_fcr_MTD --Month to date
	,sum(total_case_jour) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS total_case_jour_MTD --Month to date
	,sum(nb_demandes_chq) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_demandes_chq_MTD --Month to date
	--Full Month
	,sum(nb_demandes_clients) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_demandes_clients_FM --Full Month
	,sum(nb_reclamations_clients) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_reclamations_clients_FM --Full Month
	,sum(flag_fcr) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS flag_fcr_FM --Full Month
	,sum(total_case_jour) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS total_case_jour_FM --Full Month
	,sum(nb_demandes_chq) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_demandes_chq_FM --Full Month
	--Year to date
    ,sum(nb_demandes_clients) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_demandes_clients_YTD  --Year to date
    ,sum(nb_reclamations_clients) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_reclamations_clients_YTD  --Year to date
    ,sum(flag_fcr) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS flag_fcr_YTD  --Year to date
    ,sum(total_case_jour) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS total_case_jour_YTD  --Year to date
    ,sum(nb_demandes_chq) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_demandes_chq_YTD  --Year to date
	--Full year
	,sum(nb_demandes_clients) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_demandes_clients_FY  --Full year
	,sum(nb_reclamations_clients) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_reclamations_clients_FY  --Full year
	,sum(flag_fcr) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS flag_fcr_FY  --Full year
	,sum(total_case_jour) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS total_case_jour_FY  --Full year
	,sum(nb_demandes_chq) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_demandes_chq_FY  --Full year
INTO #rq_cli_case_cp
FROM  ( 
     SELECT  T1.StandardDate as jour_alim,DATEPART(YEAR,T1.Date) AS Year_alim,DATEPART(MONTH,T1.Date) AS Month_alim,DATEPART(YEAR,DATEADD(day, (7 - DATEPART(dw, T1.StandardDate)), T1.StandardDate)) *100 + DATEPART(WEEK,DATEADD(day, (7 - DATEPART(dw, T1.StandardDate)), T1.StandardDate)) AS Week_alim
     ,sum(case when RECORDTYPE_LABEL='Demande'  then 1 else 0 END) AS nb_demandes_clients
    , sum(case when RECORDTYPE_LABEL='Réclamation'  then 1 else 0 END) AS nb_reclamations_clients
    , sum(case when FCR='Y' then 1 else 0 END) as flag_fcr
    , sum(case when FK_STATUS=3 then 1 else 0 END) as total_case_jour
    , sum(case when TYPE_CASE_LABEL = 'Chèque' and FK_SUBTYPE_CASE=104 then 1 else 0 END) as nb_demandes_chq
FROM [dbo].[DIM_TEMPS] T1 
LEFT OUTER JOIN  [SAS_Vue_RC_Cases]  ON T1.StandardDate=[SAS_Vue_RC_Cases].[DATE]
WHERE  ( T1.StandardDate >= CAST(DATEADD(yyyy, DATEDIFF(YYYY, 0, DATEADD(yyyy, -2, @date_obs)), 0) AS DATE)
AND T1.StandardDate <=  DATEADD(day, (7 - DATEPART(dw, @date_obs)), @date_obs) )
GROUP BY  T1.StandardDate ,DATEPART(YEAR,T1.Date) ,DATEPART(MONTH,T1.Date) ,DATEPART(WEEK,T1.Date) 
) t;

SELECT
  CY.jour_alim
, CY.Year_alim
, CY.Month_alim
, CY.Week_alim
, coalesce(CY.nb_demandes_clients,0) AS nb_demandes_clients
, coalesce(CY.nb_demandes_clients_WTD,0) AS nb_demandes_clients_WTD
, coalesce(CY.nb_demandes_clients_MTD,0) AS nb_demandes_clients_MTD
, coalesce(CY.nb_demandes_clients_YTD,0) AS nb_demandes_clients_YTD
, coalesce(PM.nb_demandes_clients_FM,0) AS nb_demandes_clients_FM
, coalesce(PY.nb_demandes_clients_FY,0) AS nb_demandes_clients_FY
, coalesce(CY.nb_reclamations_clients,0) AS nb_reclamations_clients
, coalesce(CY.nb_reclamations_clients_WTD,0) AS nb_reclamations_clients_WTD
, coalesce(CY.nb_reclamations_clients_MTD,0) AS nb_reclamations_clients_MTD
, coalesce(CY.nb_reclamations_clients_YTD,0) AS nb_reclamations_clients_YTD
, coalesce(PM.nb_reclamations_clients_FM,0) AS nb_reclamations_clients_FM
, coalesce(PY.nb_reclamations_clients_FY,0) AS nb_reclamations_clients_FY
, coalesce(CY.flag_fcr,0) AS flag_fcr
, coalesce(CY.flag_fcr_WTD,0) AS flag_fcr_WTD
, coalesce(CY.flag_fcr_MTD,0) AS flag_fcr_MTD
, coalesce(CY.flag_fcr_YTD,0) AS flag_fcr_YTD
, coalesce(PM.flag_fcr_FM,0) AS flag_fcr_FM
, coalesce(PY.flag_fcr_FY,0) AS flag_fcr_FY
, coalesce(CY.total_case_jour,0) AS total_case_jour
, coalesce(CY.total_case_jour_WTD,0) AS total_case_jour_WTD
, coalesce(CY.total_case_jour_MTD,0) AS total_case_jour_MTD
, coalesce(CY.total_case_jour_YTD,0) AS total_case_jour_YTD
, coalesce(PM.total_case_jour_FM,0) AS total_case_jour_FM
, coalesce(PY.total_case_jour_FY,0) AS total_case_jour_FY
, coalesce(CY.nb_demandes_chq,0) AS nb_demandes_chq
, coalesce(CY.nb_demandes_chq_WTD,0) AS nb_demandes_chq_WTD
, coalesce(CY.nb_demandes_chq_MTD,0) AS nb_demandes_chq_MTD
, coalesce(CY.nb_demandes_chq_YTD,0) AS nb_demandes_chq_YTD
, coalesce(PM.nb_demandes_chq_FM,0) AS nb_demandes_chq_FM
, coalesce(PY.nb_demandes_chq_FY,0) AS nb_demandes_chq_FY
INTO  [dbo].[WK_SSRS_SAS_VUE_RC_CASES]
FROM #rq_cli_case_cp CY --Current Year
 LEFT OUTER JOIN #rq_cli_case_cp PM --Previous Year
		  ON DATEADD(MONTH,-1,CY.jour_alim) = PM.jour_alim
	 LEFT OUTER JOIN #rq_cli_case_cp PY --Previous Year
		  ON DATEADD(YEAR,-1,CY.jour_alim) = PY.jour_alim
ORDER BY CY.jour_alim;
END