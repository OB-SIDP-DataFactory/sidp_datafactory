﻿CREATE PROC [dbo].[PKGCR_PROC_ASSURANCES] (@date_alim DATE, @date_derniere_alim DATE, @nbRows int OUTPUT)
AS  
BEGIN 

-- =============================================
-- Description:	Alimentation de la table CRE_ASSURANCES contenant les équipements Crédit
-- Tables input : PV_FE_CONSUMERLOANEQUIPMENT
--				  PV_FE_EQUIPMENTSVCASSOC 
--				  PV_FE_SERVICEPRODUCT
-- Table output : CRE_ASSURANCES			
-- =============================================

-- Nettoyage des données déjà chargées
DELETE FROM [dbo].[CRE_ASSURANCES]
WHERE [DTE_ALIM] >= @date_derniere_alim

INSERT INTO [dbo].[CRE_ASSURANCES]
(
      [IDE_FE_EQU_PROD_CRE]      
      ,[IDE_FE_SVC_PROD]
      ,[COD_SVC_PROD]
      ,[COD_CONV_SVC_PROD]
      ,[COD_CORE_BANKING]
      ,[DTE_CREA]
      ,[USR_CREA]
      ,[LABEL_SVC_PROD]
      ,[DTE_DER_MOD]
      ,[USR_DER_MOD]
      ,[IDE_COD_PRI]
      ,[TYP_USR]
      ,[DTE_ALIM]
	  )
SELECT
       EQ.ID_FE_CONSUMERLOANEQUIPMENT
	  ,SP.[ID_FE_SERVICEPRODUCT]
      ,SP.[CODE]
      ,SP.[CONVENTION_CODE]
      ,SP.[CORE_BANKING_CODE]
      ,SP.[CREATION_DATE]
      ,SP.[CREATION_USER]
      ,SP.[LABEL]
      ,SP.[LAST_UPDATE_DATE]
      ,SP.[LAST_UPDATE_USER]
      ,SP.[PRICE_CODE_ID]
      ,SP.[USER_TYPE]
      ,dateadd (day,-1,@date_alim) AS DATE_ALIM
  FROM [$(DataHubDatabaseName)].[dbo].[PV_FE_CONSUMERLOANEQUIPMENT] EQ
  INNER JOIN [$(DataHubDatabaseName)].[dbo].[PV_FE_EQUIPMENTSVCASSOC] ES
  ON EQ.ID_FE_CONSUMERLOANEQUIPMENT = ES.EQUIPMENT_ID
  INNER JOIN [$(DataHubDatabaseName)].[dbo].[PV_FE_SERVICEPRODUCT] SP  
  ON SP.ID_FE_SERVICEPRODUCT = ES.SERVICE_PRODUCT_ID 
  WHERE @date_derniere_alim <= SP.Validity_StartDate and SP.Validity_StartDate < @date_alim

SELECT @nbRows = @@ROWCOUNT

END