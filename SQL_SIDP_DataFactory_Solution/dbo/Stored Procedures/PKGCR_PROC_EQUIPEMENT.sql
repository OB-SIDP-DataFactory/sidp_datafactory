﻿CREATE PROC [dbo].[PKGCR_PROC_EQUIPEMENT] (@date_alim DATE, @date_derniere_alim DATE, @nbRows int OUTPUT)
AS  
BEGIN 
-- =============================================
-- Description:	Alimentation de la table CRE_EQUIPEMENT contenant les équipements Crédit
-- Tables input : PV_FE_CONSUMERLOANEQUIPMENT
--				  PV_FE_EQUIPMENT 
--				  PV_FE_ENROLMENTFOLDER
-- Table output : CRE_EQUIPEMENT				
-- =============================================
-- Nettoyage des données déjà chargées
DELETE FROM [dbo].[CRE_EQUIPEMENT]
WHERE [DTE_ALIM] >= @date_derniere_alim

INSERT INTO [dbo].[CRE_EQUIPEMENT]
(
[IDE_FE_EQU_PROD_CRE]
	  ,[IDE_OPPRT_SF]
      ,[FRAIS_INSCR]
      ,[DTE_FIN_CONT]
      ,[IDE_TYP_FIN]
      ,[PREM_DTE_TRCH]
      ,[DTE_FIN_CRE]
      ,[NUM_SOUS_FFI]
      ,[NUM_CONT_FFI]
      ,[MTT_CRE_SOUSC]
      ,[ASS_PREMIUM]
      ,[MTT_TOT_ASS]
      ,[MTT_DER_TRCH_PAYE_AVEC_ASS]
      ,[MTT_DER_TRCH_PAYE_SANS_ASS]
      ,[DER_DTE_TRCH_PAYE]
      ,[DTE_FIN_ACTIV_CRE]
      ,[DTE_DEB_ACTIV_CRE]
      ,[MTT_DEC_CRE]
      ,[DTE_DEC_CRE]
      ,[TX_NOM_CRE]
      ,[NBR_TRCH_CRE]
      ,[CRE_EXCPT]
      ,[IDE_SIT_CRE_PROD]
      ,[IDE_STAT_CRE_PROD]
      ,[TX_TAEA_CRE]
      ,[TX_TAEG_CRE]
      ,[TRCH_MM_AVEC_ASS]
      ,[TRCH_MM_SANS_ASS]
      ,[NBRE_ECH_REST]
      ,[NBRE_TRCH_REPORTE]
      ,[DTE_FIN_PRD_RETRACT]
      ,[PRD_TOT_MM_DIFFERE]
      ,[CAP_RES_DU]
      ,[MTT_TOT_INT]
      ,[NBRE_TOT_TRCH]
      ,[TRCH_A_VENIR_AVEC_ASS]
      ,[TRCH_A_VENIR_SANS_ASS]
      ,[DTE_TRCH_A_VENIR]
      ,[IDE_RAISON_CRE_TERM]
      ,[DTE_ACCEPT]
      ,[DTE_RET]
      ,[MAIL_DISP_DEC_ENV]
      ,[IDE_TERR]
      ,[CLE_CONTROLE_DEBIT_DIRECT]
      ,[BBAN_DEBIT_DIRECT]
	  ,[DTE_ALIM]
	  )
SELECT
       [ID_FE_CONSUMERLOANEQUIPMENT]
	  ,EF.SALESFORCE_OPPORTUNITY_ID
      ,[APPLICATION_FEES]
      ,[CONTRACT_TERM_DATE]
      ,[FINANCING_TYPE_ID]
      ,[FIRST_INSTALLMENT_DATE]
      ,[FORECASTED_LOAN_END_DATE]
      ,[FRANFINANCE_APPLICATION_NUMBER]
      ,[FRANFINANCE_CONTRACT_NUMBER]
      ,[GRANTED_LOAN_AMOUNT]
      ,[INSURANCE_PREMIUM]
      ,[INSURANCE_TOTAL_COST]
      ,[LAST_PAID_INST_AMOU_WITH_IN]
      ,[LAST_PAID_INST_AMOU_WITHOU_INS]
      ,[LAST_PAID_INST_DATE]
      ,[LOAN_ACTIVATION_END_DATE]
      ,[LOAN_ACTIVATION_START_DATE]
      ,[LOAN_DISBURSMENT_AMOUNT]
      ,[LOAN_DISBURSMENT_DATE]
      ,[LOAN_NOMINAL_RATE]
      ,[LOAN_NUMBER_OF_INSTALLMENT]
      ,[LOAN_OUTSTANDING_CAPITAL]
      ,[CONSUMER_LOAN_SITUATION_ID]
      ,[CONSUMER_LOAN_STATUS_ID]
      ,[LOAN_TAEA_RATE]
      ,[LOAN_TAEG_RATE]
      ,[MONTHLY_INSTAL_WITH_INSURAN]
      ,[MONTHLY_INSTAL_WITHOUT_INSURAN]
      ,[NUMBER_OF_DUE_INSTALLMENT]
      ,[NBR_OF_POSTPONED_INSTALLMENT]
      ,[RETRACTION_PERIOD_END_DATE]
      ,[TOTAL_DEFFERED_PERIOD_MONTHS]
      ,[TOTAL_DUE_AMOUNT]
      ,[TOTAL_INTEREST_AMOUNT]
      ,[TOTAL_NUMBER_OF_INSTALLMENT]
      ,[UPCOMING_INSTAL_WITH_INSUR]
      ,[UPCOMING_INSTAL_WITHOUT_INSUR]
      ,[UPCOMING_INSTALLMENT_DATE]
      ,[CONSUMER_LOAN_TERM_REASON_ID]
      ,[APPROVAL_DATE]
      ,[WITHDRAWAL_DAY]
      ,[DISBUR_AVAILABILITY_EMAIL_SENT]
      ,[DIRECT_DEBIT_COUNTRY_ID]
      ,[DIRECT_DEBIT_CONTROL_KEY]
      ,[DIRECT_DEBIT_BBAN]
      ,dateadd (day,-1,@date_alim) AS [DATE_ALIM]
FROM [$(DataHubDatabaseName)].[dbo].[PV_FE_CONSUMERLOANEQUIPMENT] LE
inner join [$(DataHubDatabaseName)].[dbo].[PV_FE_EQUIPMENT] EQ
ON EQ.ID_FE_EQUIPMENT = LE.ID_FE_CONSUMERLOANEQUIPMENT
inner join [$(DataHubDatabaseName)].[dbo].[PV_FE_ENROLMENTFOLDER] EF
ON EF.ID_FE_ENROLMENT_FOLDER = EQ.ENROLMENT_FOLDER_ID
WHERE @date_derniere_alim <= LE.Validity_StartDate and LE.Validity_StartDate < @date_alim

SELECT @nbRows = @@ROWCOUNT

END