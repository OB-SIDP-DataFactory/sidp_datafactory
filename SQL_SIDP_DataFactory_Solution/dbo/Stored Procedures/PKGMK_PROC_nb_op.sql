﻿

CREATE PROC [dbo].[PKGMK_PROC_nb_op] @DATE_ALIM DATEtime2 , @jours_histo int = 1 , @nbRows int OUTPUT -- nb lignes processées 
AS  BEGIN 

-- ===============================================================================================================================================================================
--tables input : [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_MOUVEMENT]	
--               [DIM_OPERATIONS]
--tables output : [MKT_NB_OP]
--description : construction de la vue sas des comptes en stock 
-- =============================================================================================================================================================================
/************** créer un sous-ensemble de la table dimension temps **************/
-- aligner le contenu sur le contenu des tables WK_ALL_SAB_*
select PK_ID, StandardDate 
into #wk_temps_nb_op
from DIM_TEMPS
where StandardDate in ( select distinct date_alim from WK_ALL_SAB_COMPTES );

/************** alimenter la table pour la période séléctionnée **************/
IF OBJECT_ID('[MKT_NB_OP]') IS NOT NULL
BEGIN TRUNCATE TABLE MKT_NB_OP END
 --declare @DATE_ALIM date = '2017-02-13';
 --declare @jours_histo int = 1000;
INSERT INTO MKT_NB_OP
(date_alim, NUM_CPT, TYPE, cum_nb_ope) 
	SELECT d.StandardDate as date_alim, tt.NUM_CPT, tt.TYPE, max(cum_nb_ope) as cum_nb_ope 

	FROM #wk_temps_nb_op d
	OUTER APPLY 
	(

	SELECT d.StandardDate as date_alim, t1.NUM_CPT, t2.TYPE, count(NUM_CPT) as cum_nb_ope
	FROM [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_MOUVEMENT] t1
		INNER JOIN DIM_OPERATIONS t2
			ON  t1.COD_OPE = t2.COD_OPE
			AND SUBSTRING(t1.DON_ANA,31,1) = t2.DON_ANA --code carte dans données analytiques
	WHERE ( t2.TYPE IN ('CB', 'PM')
			AND DATEADD(day,-30,d.StandardDate) <= t1.DTE_OPE   and t1.DTE_OPE < d.StandardDate --opérations dans les 30 jours qui précédant la date d'alimentation de la vue
			) 
	GROUP BY /*d.StandardDate,*/ t1.NUM_CPT, t2.TYPE

	UNION ALL

	SELECT d.StandardDate as date_alim, t1.NUM_CPT,t2.TYPE, count(NUM_CPT) as cum_nb_ope
	FROM [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_MOUVEMENT] t1
		INNER JOIN DIM_OPERATIONS t2
			on t1.COD_OPE = t2.COD_OPE
		WHERE ( t2.TYPE in ('virement', 'prélèvement')
				AND DATEADD(day,-30,d.StandardDate) <= t1.DTE_OPE   and t1.DTE_OPE < d.StandardDate --opérations dans les 30 jours précédant la date d'alimentation de la vue
				) 
	GROUP BY /*d.StandardDate,*/ t1.NUM_CPT, t2.TYPE
	) tt
group by d.StandardDate, tt.NUM_CPT, tt.TYPE
;

select @nbRows = @@ROWCOUNT;

END 
GO