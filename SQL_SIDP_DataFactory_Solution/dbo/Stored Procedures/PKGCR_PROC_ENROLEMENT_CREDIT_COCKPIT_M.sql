﻿

CREATE PROCEDURE [dbo].[PKGCR_PROC_ENROLEMENT_CREDIT_COCKPIT_M]
@Date_obs date

AS BEGIN

SET @Date_obs=cast(@Date_obs as date);
;WITH Cte_Cre_Opp_ALL as (
SELECT [IDE_OPPRT_SF]
	  ,[STADE_VENTE]
	  ,LIB_STADE_VENTE
	  ,Cast([DTE_CREA] as date) [DTE_CREA]
	  ,[DELAI_ACCORD]
	  ,[DELAI_DEBLOCAGE]
	  ,AGE_OPPRT
	  ,COD_CAN_INT_ORI
	  ,[COD_RES_DIS]
	  ,[DTE_ALIM]
FROM [dbo].[CRE_OPPORTUNITE] Opp
where DTE_ALIM = (select MAX(DTE_ALIM) from CRE_OPPORTUNITE) and [FLG_DER_ETAT_OPPRT]=1
),

Cte_jour_alim_list AS
(
SELECT  cast(Date as date) AS DateCalc, Month as MonthCalc, Year as YearCalc,Opp.IDE_OPPRT_SF,Opp.STADE_VENTE,Opp.LIB_STADE_VENTE
,Opp.DELAI_ACCORD,Opp.DELAI_DEBLOCAGE,Opp.AGE_OPPRT,Opp.COD_CAN_INT_ORI,Opp.COD_RES_DIS,Opp.DTE_ALIM
FROM [dbo].[DIM_TEMPS] T1 Left outer join Cte_Cre_Opp_ALL Opp on cast(Date as date)=Opp.DTE_CREA
where cast(Date as date) between DATEADD(MONTH,-12,@Date_obs)  and @Date_obs
),
--Opport credit creees
Cte_Opp_Cre_Creees_cumul as (
SELECT DateCalc,MonthCalc,YearCalc,
	   COUNT ([IDE_OPPRT_SF]) as NB_OPP_CRE_CREEES,
       case when COD_RES_DIS = '01' then COUNT ([IDE_OPPRT_SF])  end as NB_OF,
	   case when COD_RES_DIS = '02' then COUNT ([IDE_OPPRT_SF])  end as NB_OB,
	   case when COD_CAN_INT_ORI IN ('10','11') then COUNT ([IDE_OPPRT_SF])  end as NB_Digital,
	   case when COD_CAN_INT_ORI IN ('12','13','14','15') then COUNT ([IDE_OPPRT_SF])  end as NB_CRC
FROM Cte_jour_alim_list
--WHERE  [WeekOfYear] = @weekobs
GROUP BY DateCalc,MonthCalc,YearCalc,COD_RES_DIS,COD_CAN_INT_ORI
) 
,Cte_Opp_Cre_Creees as(
SELECT  DateCalc,MonthCalc,YearCalc,
	   SUM(NB_OPP_CRE_CREEES) as NB_OPP_CRE_CREEES,
       SUM(NB_OF)as NB_OF ,
	   SUM(NB_OB) as NB_OB,
	   SUM(NB_Digital) as NB_Digital,
	   SUM(NB_CRC) as NB_CRC 
FROM Cte_Opp_Cre_Creees_cumul
GROUP BY  DateCalc,MonthCalc,YearCalc
)
--Opport crédit en cours de vie
,Cte_Opp_Cre_En_cours_De_Vie_Cumul as (
SELECT DateCalc,MonthCalc,YearCalc,
       COUNT ([IDE_OPPRT_SF])													as NB_OPP_CRE_EN_COURS,
       case when STADE_VENTE = '01' then COUNT ([IDE_OPPRT_SF]) end				as NB_DETECTION,
	   case when STADE_VENTE IN ('02','03') then COUNT ([IDE_OPPRT_SF])  end      as NB_AVANT_SIGNATURE,
	   case when STADE_VENTE IN ('04','05','06','07') then COUNT  ([IDE_OPPRT_SF]) end as NB_APRES_SIGNATURE,
	   SUM(AGE_OPPRT)															as ANCIENNETE
FROM Cte_jour_alim_list
WHERE  STADE_VENTE IN ('01','02','03','04','05','06','07')
GROUP BY DateCalc,MonthCalc,YearCalc,STADE_VENTE
)
,Cte_Opp_Cre_En_cours_De_Vie as (
SELECT DateCalc,MonthCalc,YearCalc,
	   SUM(NB_OPP_CRE_EN_COURS)    as NB_OPP_CRE_EN_COURS,
       SUM(NB_DETECTION)           as NB_DETECTION ,
	   SUM(NB_AVANT_SIGNATURE)     as NB_AVANT_SIGNATURE,
	   SUM(NB_APRES_SIGNATURE)     as NB_APRES_SIGNATURE,
	   SUM(ANCIENNETE)             as ANCIENNETE
FROM Cte_Opp_Cre_En_cours_De_Vie_Cumul
GROUP BY DateCalc,MonthCalc,YearCalc
)
--Opport credit gnagnees
,Cte_Opp_Cre_Gagnees_Cumul as (
SELECT DateCalc,MonthCalc,YearCalc,
       COUNT ([IDE_OPPRT_SF])                                        as NB_OPP_CRE_GAGNEES,
       case when COD_RES_DIS = '01' then COUNT ([IDE_OPPRT_SF])  end as NB_OF,
	   case when COD_RES_DIS = '02' then COUNT ([IDE_OPPRT_SF])  end as NB_OB,
	   case when STADE_VENTE = '14' then COUNT ([IDE_OPPRT_SF])  end as NB_CREDIT_ACCORDE,
	   case when STADE_VENTE = '15' then COUNT ([IDE_OPPRT_SF])  end as NB_CREDIT_DECAISSE,
	   Sum([DELAI_ACCORD])										   as DELAI_ACCORD_PRET,
	   Sum([DELAI_DEBLOCAGE])								       as DELAI_DECAISSEMENT
FROM Cte_jour_alim_list
WHERE  STADE_VENTE IN ('14','15')
GROUP BY DateCalc,MonthCalc,YearCalc,COD_RES_DIS,STADE_VENTE
)
,Cte_Opp_Cre_Gagnees as (
SELECT DateCalc,MonthCalc,YearCalc,
	   SUM(NB_OPP_CRE_GAGNEES)     as NB_OPP_CRE_GAGNEES,
       SUM(NB_OF)                  as NB_OF ,
	   SUM(NB_OB)                  as NB_OB,
	   SUM(NB_CREDIT_ACCORDE)      as NB_CREDIT_ACCORDE,
	   SUM(NB_CREDIT_DECAISSE)     as NB_CREDIT_DECAISSE,
	   SUM(DELAI_ACCORD_PRET)      as DELAI_ACCORD_PRET,
	   SUM(DELAI_DECAISSEMENT)     as DELAI_DECAISSEMENT
FROM Cte_Opp_Cre_Gagnees_Cumul
GROUP BY DateCalc,MonthCalc,YearCalc
)
--Opport credit perdues
,Cte_Opp_Cre_Perdues_Cumul as (
SELECT DateCalc,MonthCalc,YearCalc,
       COUNT ([IDE_OPPRT_SF]) as NB_OPP_CRE_PERDUES,
       case when STADE_VENTE = '16' then COUNT ([IDE_OPPRT_SF])  end as NB_ANNULATION,
	   case when STADE_VENTE = '16' then COUNT ([IDE_OPPRT_SF])  end as NB_ANNULATION_CLIENT,
	   case when STADE_VENTE = '12' then COUNT ([IDE_OPPRT_SF])  end as NB_RETRACTATION,
	   case when STADE_VENTE = '10' then COUNT ([IDE_OPPRT_SF])  end as NB_AFFAIRE_REFUSEE,
	   case when STADE_VENTE = '08' then COUNT ([IDE_OPPRT_SF])  end as NB_INCIDENT_TECH,
	   case when STADE_VENTE = '11' then COUNT ([IDE_OPPRT_SF])  end as NB_SANS_SUITE
FROM Cte_jour_alim_list
WHERE  STADE_VENTE IN ('16','12','10','08','11')
GROUP BY DateCalc,MonthCalc,YearCalc,STADE_VENTE
)
,Cte_Opp_Cre_Perdues as (
SELECT  DateCalc,MonthCalc,YearCalc,
	   SUM(NB_OPP_CRE_PERDUES)     as NB_OPP_CRE_PERDUES,
       SUM(NB_ANNULATION)          as NB_ANNULATION ,
	   SUM(NB_ANNULATION_CLIENT)   as NB_ANNULATION_CLIENT,
	   SUM(NB_RETRACTATION)        as NB_RETRACTATION,
	   SUM(NB_AFFAIRE_REFUSEE)     as NB_AFFAIRE_REFUSEE,
	   SUM(NB_INCIDENT_TECH)       as NB_INCIDENT_TECH,
	   SUM(NB_SANS_SUITE)          as NB_SANS_SUITE 
FROM Cte_Opp_Cre_Perdues_Cumul
GROUP BY  DateCalc,MonthCalc,YearCalc
)
,CalculatedDataset
AS
(
		SELECT 
		 s.DateCalc,cast(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,s.DateCalc)+1,0)) as date) as LastDateMonth,s.NB_OPP_CRE_CREEES
		,SUM(s.NB_OPP_CRE_CREEES) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_OPP_CRE_CREEES_MTD --Month to date
		,SUM(s.NB_OF) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_OF_MTD --Month to date
		,SUM(s.NB_OB) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_OB_MTD --Month to date
		,SUM(s.NB_Digital) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_Digital_MTD --Month to date
		,SUM(s.NB_CRC) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_CRC_MTD --Month to date
		,SUM(s.NB_OPP_CRE_CREEES) OVER (PARTITION BY s.YearCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_OPP_CRE_CREEES_YTD --Year to date
		,SUM(s.NB_OF) OVER (PARTITION BY s.YearCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_OF_YTD --Year to date
		,SUM(s.NB_OB) OVER (PARTITION BY s.YearCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_OB_YTD --Year to date
		,SUM(s.NB_Digital) OVER (PARTITION BY s.YearCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_Digital_YTD --Year to date
		,SUM(s.NB_CRC) OVER (PARTITION BY s.YearCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_CRC_YTD --Year to date
		,case when s.DateCalc=cast(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,s.DateCalc)+1,0)) as date) then SUM(s.NB_OPP_CRE_CREEES) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc  ) else 0 end AS NB_OPP_CRE_CREEES_FM --Full month
		,case when s.DateCalc=cast(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,s.DateCalc)+1,0)) as date) then SUM(s.NB_OF) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc  )else 0 end  AS NB_OF_FM --Full month
		,case when s.DateCalc=cast(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,s.DateCalc)+1,0)) as date) then SUM(s.NB_OB) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc  ) else 0 end AS NB_OB_FM --Full month
		,case when s.DateCalc=cast(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,s.DateCalc)+1,0)) as date) then SUM(s.NB_Digital) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc  ) else 0 end AS NB_Digital_FM --Full month
		,case when s.DateCalc=cast(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,s.DateCalc)+1,0)) as date) then SUM(s.NB_CRC) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc  ) else 0 end AS NB_CRC_FM --Full month
		,SUM(v.NB_OPP_CRE_EN_COURS) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_OPP_CRE_EN_COURS_MTD --Month to date
		,SUM(v.NB_DETECTION) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_DETECTION_MTD --Month to date
		,SUM(v.NB_AVANT_SIGNATURE) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_AVANT_SIGNATURE_MTD --Month to date
		,SUM(v.NB_APRES_SIGNATURE) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_APRES_SIGNATURE_MTD --Month to date
		,SUM(v.ANCIENNETE) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS ANCIENNETE_MTD --Month to date
		,SUM(v.NB_OPP_CRE_EN_COURS) OVER (PARTITION BY s.YearCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_OPP_CRE_EN_COURS_YTD --Year to date
		,SUM(v.NB_DETECTION) OVER (PARTITION BY s.YearCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_DETECTION_YTD --Year to date
		,SUM(v.NB_AVANT_SIGNATURE) OVER (PARTITION BY s.YearCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_AVANT_SIGNATURE_YTD --Year to date
		,SUM(v.NB_APRES_SIGNATURE) OVER (PARTITION BY s.YearCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_APRES_SIGNATURE_YTD --Year to date
		,SUM(v.ANCIENNETE) OVER (PARTITION BY s.YearCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS ANCIENNETE_YTD --Year to date
		,case when s.DateCalc=cast(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,s.DateCalc)+1,0)) as date) then SUM(v.NB_OPP_CRE_EN_COURS) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc  ) else 0 end AS NB_OPP_CRE_EN_COURS_FM --Full month
		,case when s.DateCalc=cast(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,s.DateCalc)+1,0)) as date) then SUM(v.NB_DETECTION) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc  ) else 0 end AS NB_DETECTION_FM --Full month
		,case when s.DateCalc=cast(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,s.DateCalc)+1,0)) as date) then SUM(v.NB_AVANT_SIGNATURE) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc  )else 0 end  AS NB_AVANT_SIGNATURE_FM --Full month
		,case when s.DateCalc=cast(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,s.DateCalc)+1,0)) as date) then SUM(v.NB_APRES_SIGNATURE) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc  ) else 0 end AS NB_APRES_SIGNATURE_FM --Full month
		,case when s.DateCalc=cast(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,s.DateCalc)+1,0)) as date) then SUM(v.ANCIENNETE) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc  ) else 0 end AS ANCIENNETE_FM --Full month
		,SUM(g.NB_OPP_CRE_GAGNEES) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_OPP_CRE_GAGNEES_MTD --Month to date
		,SUM(g.NB_OF) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_OPP_CRE_GAGNEES_OF_MTD --Month to date
		,SUM(g.NB_OB) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_OPP_CRE_GAGNEES_OB_MTD --Month to date
		,SUM(g.NB_CREDIT_ACCORDE) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_CREDIT_ACCORDE_MTD --Month to date
		,SUM(g.NB_CREDIT_DECAISSE) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_CREDIT_DECAISSE_MTD --Month to date
		,SUM(g.DELAI_ACCORD_PRET) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS DELAI_ACCORD_PRET_MTD --Month to date
		,SUM(g.DELAI_DECAISSEMENT) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS DELAI_DECAISSEMENT_MTD --Month to date
		,SUM(g.NB_OPP_CRE_GAGNEES) OVER (PARTITION BY s.YearCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_OPP_CRE_GAGNEES_YTD --Year to date
		,SUM(g.NB_OF) OVER (PARTITION BY s.YearCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_OPP_CRE_GAGNEES_OF_YTD --Year to date
		,SUM(g.NB_OB) OVER (PARTITION BY s.YearCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_OPP_CRE_GAGNEES_OB_YTD --Year to date
		,SUM(g.NB_CREDIT_ACCORDE) OVER (PARTITION BY s.YearCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_CREDIT_ACCORDE_YTD --Year to date
		,SUM(g.NB_CREDIT_DECAISSE) OVER (PARTITION BY s.YearCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_CREDIT_DECAISSE_YTD --Year to date
		,SUM(g.DELAI_ACCORD_PRET) OVER (PARTITION BY s.YearCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS DELAI_ACCORD_PRET_YTD --Year to date
		,SUM(g.DELAI_DECAISSEMENT) OVER (PARTITION BY s.YearCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS DELAI_DECAISSEMENT_YTD --Year to date
		,case when s.DateCalc=cast(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,s.DateCalc)+1,0)) as date) then SUM(g.NB_OPP_CRE_GAGNEES) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc  ) else 0 end AS NB_OPP_CRE_GAGNEES_FM --Full month
		,case when s.DateCalc=cast(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,s.DateCalc)+1,0)) as date) then SUM(g.NB_OF) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc  ) else 0 end AS NB_OPP_CRE_GAGNEES_OF_FM --Full month
		,case when s.DateCalc=cast(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,s.DateCalc)+1,0)) as date) then SUM(g.NB_OB) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc  ) else 0 end AS NB_OPP_CRE_GAGNEES_OB_FM --Full month
		,case when s.DateCalc=cast(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,s.DateCalc)+1,0)) as date) then SUM(g.NB_CREDIT_ACCORDE) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc  ) else 0 end AS NB_CREDIT_ACCORDE_FM --Full month
		,case when s.DateCalc=cast(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,s.DateCalc)+1,0)) as date) then SUM(g.NB_CREDIT_DECAISSE) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc  )else 0 end  AS NB_CREDIT_DECAISSE_FM --Full month
		,case when s.DateCalc=cast(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,s.DateCalc)+1,0)) as date) then SUM(g.DELAI_ACCORD_PRET) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc  ) else 0 end AS DELAI_ACCORD_PRET_FM --Full month
		,case when s.DateCalc=cast(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,s.DateCalc)+1,0)) as date) then SUM(g.DELAI_DECAISSEMENT) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc  ) else 0 end AS DELAI_DECAISSEMENT_FM --Full month
		,SUM(p.NB_OPP_CRE_PERDUES) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_OPP_CRE_PERDUES_MTD --Month to date
		,SUM(p.NB_ANNULATION) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_ANNULATION_MTD --Month to date
		,SUM(p.NB_ANNULATION_CLIENT) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_ANNULATION_CLIENT_MTD --Month to date
		,SUM(p.NB_RETRACTATION) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_RETRACTATION_MTD --Month to date
		,SUM(p.NB_AFFAIRE_REFUSEE) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_AFFAIRE_REFUSEE_MTD --Month to date
		,SUM(p.NB_INCIDENT_TECH) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_INCIDENT_TECH_MTD --Month to date
		,SUM(p.NB_SANS_SUITE) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_SANS_SUITE_MTD --Month to date
		,SUM(p.NB_OPP_CRE_PERDUES) OVER (PARTITION BY s.YearCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_OPP_CRE_PERDUES_YTD --Year to date
		,SUM(p.NB_ANNULATION) OVER (PARTITION BY s.YearCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_ANNULATION_YTD --Year to date
		,SUM(p.NB_ANNULATION_CLIENT) OVER (PARTITION BY s.YearCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_ANNULATION_CLIENT_YTD --Year to date
		,SUM(p.NB_RETRACTATION) OVER (PARTITION BY s.YearCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_RETRACTATION_YTD --Year to date
		,SUM(p.NB_AFFAIRE_REFUSEE) OVER (PARTITION BY s.YearCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_AFFAIRE_REFUSEE_YTD --Year to date
		,SUM(p.NB_INCIDENT_TECH) OVER (PARTITION BY s.YearCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_INCIDENT_TECH_YTD --Year to date
		,SUM(p.NB_SANS_SUITE) OVER (PARTITION BY s.YearCalc ORDER BY s.YearCalc, s.MonthCalc, s.DateCalc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS NB_SANS_SUITE_YTD --Year to date
		,case when s.DateCalc=cast(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,s.DateCalc)+1,0)) as date) then SUM(p.NB_OPP_CRE_PERDUES) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc  ) else 0 end AS NB_OPP_CRE_PERDUES_FM --Full month
		,case when s.DateCalc=cast(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,s.DateCalc)+1,0)) as date) then SUM(p.NB_ANNULATION) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc  ) else 0 end AS NB_ANNULATION_FM --Full month
		,case when s.DateCalc=cast(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,s.DateCalc)+1,0)) as date) then SUM(p.NB_ANNULATION_CLIENT) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc  ) else 0 end AS NB_ANNULATION_CLIENT_FM --Full month
		,case when s.DateCalc=cast(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,s.DateCalc)+1,0)) as date) then SUM(p.NB_RETRACTATION) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc  ) else 0 end AS NB_RETRACTATION_FM --Full month
		,case when s.DateCalc=cast(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,s.DateCalc)+1,0)) as date) then SUM(p.NB_AFFAIRE_REFUSEE) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc  ) else 0 end AS NB_AFFAIRE_REFUSEE_FM --Full month
		,case when s.DateCalc=cast(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,s.DateCalc)+1,0)) as date) then SUM(p.NB_INCIDENT_TECH) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc  ) else 0 end AS NB_INCIDENT_TECH_FM --Full month
		,case when s.DateCalc=cast(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,s.DateCalc)+1,0)) as date) then SUM(p.NB_SANS_SUITE) OVER (PARTITION BY s.YearCalc, s.MonthCalc ORDER BY s.YearCalc, s.MonthCalc  ) else 0 end AS NB_SANS_SUITE_FM --Full month
		 
	 FROM Cte_Opp_Cre_Creees s 
     left outer join Cte_Opp_Cre_En_cours_De_Vie v on s.DateCalc=v.DateCalc 
	 left outer join Cte_Opp_Cre_Gagnees g on s.DateCalc=g.DateCalc 
	 left outer join Cte_Opp_Cre_Perdues p on s.DateCalc=p.DateCalc 
)

SELECT 
			CY.[DateCalc]
			,'M'+CAST(DATEDIFF(MONTH,@Date_obs,CY.LastDateMonth) AS varchar(3))  as  Mois
			, DATEDIFF(MONTH,@Date_obs,CY.LastDateMonth)*-1 as OrderID
            ,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_OPP_CRE_CREEES_MTD,0) else 0 end as NB_OPP_CRE_CREEES_MTD--Cumul MTD
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_OPP_CRE_CREEES_YTD,0) else 0 end as NB_OPP_CRE_CREEES_YTD--Cumul YTD
			,case when CY.[DateCalc]=@Date_obs then isnull( PM.NB_OPP_CRE_CREEES_MTD,0) else 0 end as NB_OPP_CRE_CREEES_MTDM1 --Cumul MTD-1
			,case when CY.[DateCalc]=@Date_obs then isnull( PY.NB_OPP_CRE_CREEES_YTD,0) else 0 end as NB_OPP_CRE_CREEES_YTDPY1 -- Cumul YTD-1
		    ,isnull (CY.NB_OPP_CRE_CREEES_FM,0)   AS NB_OPP_CRE_CREEES_MOIS
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_OF_MTD,0) else 0 end as NB_OF_MTD--Cumul MTD
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_OF_YTD,0) else 0 end as NB_OF_YTD--Cumul YTD
			,case when CY.[DateCalc]=@Date_obs then isnull( PM.NB_OF_MTD,0) else 0 end as NB_OF_MTDM1 --Cumul MTD-1
			,case when CY.[DateCalc]=@Date_obs then isnull( PY.NB_OF_YTD,0) else 0 end as NB_OF_YTDY1 -- Cumul YTD-1
			,isnull (CY.NB_OF_FM,0)   AS NB_OF_MOIS
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_OB_MTD,0) else 0 end as NB_OB_MTD--Cumul MTD
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_OB_YTD,0) else 0 end as NB_OB_YTD--Cumul YTD
			,case when CY.[DateCalc]=@Date_obs then isnull( PM.NB_OB_MTD,0) else 0 end as NB_OB_MTDM1 --Cumul MTD-1
			,case when CY.[DateCalc]=@Date_obs then isnull( PY.NB_OB_YTD,0) else 0 end as NB_OB_YTDY1 -- Cumul YTD-1
			,isnull (CY.NB_OB_FM,0)   AS NB_OB_MOIS
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_Digital_MTD,0) else 0 end as NB_Digital_MTD--Cumul MTD
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_Digital_YTD,0) else 0 end as NB_Digital_YTD--Cumul YTD
			,case when CY.[DateCalc]=@Date_obs then isnull( PM.NB_Digital_MTD,0) else 0 end as NB_Digital_MTDM1 --Cumul MTD-1
			,case when CY.[DateCalc]=@Date_obs then isnull( PY.NB_Digital_YTD,0) else 0 end as NB_Digital_YTDY1 -- Cumul YTD-1
            ,isnull (CY.NB_Digital_FM,0)   AS NB_Digital_MOIS
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_CRC_MTD,0) else 0 end as NB_CRC_MTD--Cumul MTD
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_CRC_YTD,0) else 0 end as NB_CRC_YTD--Cumul YTD
			,case when CY.[DateCalc]=@Date_obs then isnull( PM.NB_CRC_MTD,0) else 0 end as NB_CRC_MTDM1 --Cumul MTD-1
			,case when CY.[DateCalc]=@Date_obs then isnull( PY.NB_CRC_YTD,0) else 0 end as NB_CRC_YTDY1 -- Cumul YTD-1
			,isnull (CY.NB_CRC_FM,0)   AS NB_CRC_MOIS
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_OPP_CRE_EN_COURS_MTD,0) else 0 end as NB_OPP_CRE_EN_COURS_MTD--Cumul MTD
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_OPP_CRE_EN_COURS_YTD,0) else 0 end as NB_OPP_CRE_EN_COURS_YTD--Cumul YTD
			,case when CY.[DateCalc]=@Date_obs then isnull( PM.NB_OPP_CRE_EN_COURS_MTD,0) else 0 end as NB_OPP_CRE_EN_COURS_MTDM1 --Cumul MTD-1
			,case when CY.[DateCalc]=@Date_obs then isnull( PY.NB_OPP_CRE_EN_COURS_YTD,0) else 0 end as NB_OPP_CRE_EN_COURS_YTDPY1 -- Cumul YTD-1
			,isnull (CY.NB_OPP_CRE_EN_COURS_FM,0)   AS NB_OPP_CRE_EN_COURS_MOIS
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_DETECTION_MTD,0) else 0 end as NB_DETECTION_MTD --Cumul MTD
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_DETECTION_YTD,0) else 0 end as NB_DETECTION_YTD --Cumul YTD
			,case when CY.[DateCalc]=@Date_obs then isnull( PM.NB_DETECTION_MTD,0) else 0 end as NB_DETECTION_MTDM1 --Cumul MTD-1
			,case when CY.[DateCalc]=@Date_obs then isnull( PY.NB_DETECTION_YTD,0) else 0 end as NB_DETECTION_YTDPY1 -- Cumul YTD-1
			,isnull (CY.NB_DETECTION_FM,0)   AS NB_DETECTION_MOIS
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_AVANT_SIGNATURE_MTD,0) else 0 end as NB_AVANT_SIGNATURE_MTD--Cumul MTD
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_AVANT_SIGNATURE_YTD,0) else 0 end as NB_AVANT_SIGNATURE_YTD--Cumul YTD
			,case when CY.[DateCalc]=@Date_obs then isnull( PM.NB_AVANT_SIGNATURE_MTD,0) else 0 end as NB_AVANT_SIGNATURE_MTDM1 --Cumul MTD-1
			,case when CY.[DateCalc]=@Date_obs then isnull( PY.NB_AVANT_SIGNATURE_YTD,0) else 0 end as NB_AVANT_SIGNATURE_YTDPY1 -- Cumul YTD-1
			,isnull (CY.NB_AVANT_SIGNATURE_FM,0)   AS NB_AVANT_SIGNATURE_MOIS
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_APRES_SIGNATURE_MTD,0) else 0 end as NB_APRES_SIGNATURE_MTD--Cumul MTD
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_APRES_SIGNATURE_YTD,0) else 0 end as NB_APRES_SIGNATURE_YTD--Cumul YTD
			,case when CY.[DateCalc]=@Date_obs then isnull( PM.NB_APRES_SIGNATURE_MTD,0) else 0 end as NB_APRES_SIGNATURE_MTDM1 --Cumul MTD-1
			,case when CY.[DateCalc]=@Date_obs then isnull( PY.NB_APRES_SIGNATURE_YTD,0) else 0 end as NB_APRES_SIGNATURE_YTDPY1 -- Cumul YTD-1
			,isnull (CY.NB_APRES_SIGNATURE_FM,0)   AS NB_APRES_SIGNATURE_MOIS
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.ANCIENNETE_MTD,0) else 0 end as ANCIENNETE_MTD--Cumul MTD
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.ANCIENNETE_YTD,0) else 0 end as ANCIENNETE_YTD--Cumul YTD
			,case when CY.[DateCalc]=@Date_obs then isnull( PM.ANCIENNETE_MTD,0) else 0 end as ANCIENNETE_MTDM1 --Cumul MTD-1
			,case when CY.[DateCalc]=@Date_obs then isnull( PY.ANCIENNETE_YTD,0) else 0 end as ANCIENNETE_YTDPY1 -- Cumul YTD-1
			,isnull (CY.ANCIENNETE_FM,0)  AS ANCIENNETE_MOIS	
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_OPP_CRE_GAGNEES_MTD,0) else 0 end as NB_OPP_CRE_GAGNEES_MTD--Cumul MTD
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_OPP_CRE_GAGNEES_YTD,0) else 0 end as NB_OPP_CRE_GAGNEES_YTD--Cumul YTD
			,case when CY.[DateCalc]=@Date_obs then isnull( PM.NB_OPP_CRE_GAGNEES_MTD,0) else 0 end as NB_OPP_CRE_GAGNEES_MTD1--Cumul MTD-1
			,case when CY.[DateCalc]=@Date_obs then isnull( PY.NB_OPP_CRE_GAGNEES_YTD,0) else 0 end as NB_OPP_CRE_GAGNEES_YTD1-- Cumul YTD-1
			,isnull (CY.NB_OPP_CRE_GAGNEES_FM,0)  AS NB_OPP_CRE_GAGNEES_MOIS
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_OPP_CRE_GAGNEES_OF_MTD,0) else 0 end as NB_OPP_CRE_GAGNEES_OF_MTD--Cumul MTD
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_OPP_CRE_GAGNEES_OF_YTD,0) else 0 end as NB_OPP_CRE_GAGNEES_OF_YTD--Cumul YTD
			,case when CY.[DateCalc]=@Date_obs then isnull( PM.NB_OPP_CRE_GAGNEES_OF_MTD,0) else 0 end as NB_OPP_CRE_GAGNEES_OF_MTD1--Cumul MTD-1
			,case when CY.[DateCalc]=@Date_obs then isnull( PY.NB_OPP_CRE_GAGNEES_OF_YTD,0) else 0 end as NB_OPP_CRE_GAGNEES_OF_YTD1-- Cumul YTD-1
			,isnull (CY.NB_OPP_CRE_GAGNEES_OF_FM,0)  AS NB_OPP_CRE_GAGNEES_OF_MOIS
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_OPP_CRE_GAGNEES_OB_MTD,0) else 0 end as NB_OPP_CRE_GAGNEES_OB_MTD--Cumul MTD
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_OPP_CRE_GAGNEES_OB_YTD,0) else 0 end as NB_OPP_CRE_GAGNEES_OB_YTD--Cumul YTD
			,case when CY.[DateCalc]=@Date_obs then isnull( PM.NB_OPP_CRE_GAGNEES_OB_MTD,0) else 0 end as NB_OPP_CRE_GAGNEES_OB_MTD1--Cumul MTD-1
			,case when CY.[DateCalc]=@Date_obs then isnull( PY.NB_OPP_CRE_GAGNEES_OB_YTD,0) else 0 end as NB_OPP_CRE_GAGNEES_OB_YTD1-- Cumul YTD-1
			,isnull (CY.NB_OPP_CRE_GAGNEES_OB_FM ,0) AS NB_OPP_CRE_GAGNEES_OB_MOIS
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_CREDIT_ACCORDE_MTD,0) else 0 end as NB_CREDIT_ACCORDE_MTD--Cumul MTD
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_CREDIT_ACCORDE_YTD,0) else 0 end as NB_CREDIT_ACCORDE_YTD--Cumul YTD
			,case when CY.[DateCalc]=@Date_obs then isnull( PM.NB_CREDIT_ACCORDE_MTD,0) else 0 end as NB_CREDIT_ACCORDE_MTD1--Cumul MTD-1
			,case when CY.[DateCalc]=@Date_obs then isnull( PY.NB_CREDIT_ACCORDE_YTD,0) else 0 end as NB_CREDIT_ACCORDE_YTD1-- Cumul YTD-1
			,isnull (CY.NB_CREDIT_ACCORDE_FM ,0) AS NB_CREDIT_ACCORDE_MOIS
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_CREDIT_DECAISSE_MTD,0) else 0 end as NB_CREDIT_DECAISSE_MTD--Cumul MTD
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_CREDIT_DECAISSE_YTD,0) else 0 end as NB_CREDIT_DECAISSE_YTD--Cumul YTD
			,case when CY.[DateCalc]=@Date_obs then isnull( PM.NB_CREDIT_DECAISSE_MTD,0) else 0 end as NB_CREDIT_DECAISSE_MTD1--Cumul MTD-1
			,case when CY.[DateCalc]=@Date_obs then isnull( PY.NB_CREDIT_DECAISSE_YTD,0) else 0 end as NB_CREDIT_DECAISSE_YTD1-- Cumul YTD-1
			,isnull (CY.NB_CREDIT_DECAISSE_FM ,0) AS NB_CREDIT_DECAISSE_MOIS
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.DELAI_ACCORD_PRET_MTD,0) else 0 end as DELAI_ACCORD_PRET_MTD--Cumul MTD
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.DELAI_ACCORD_PRET_YTD,0) else 0 end as DELAI_ACCORD_PRET_YTD--Cumul YTD
			,case when CY.[DateCalc]=@Date_obs then isnull( PM.DELAI_ACCORD_PRET_MTD,0) else 0 end as DELAI_ACCORD_PRET_MTD1--Cumul MTD-1
			,case when CY.[DateCalc]=@Date_obs then isnull( PY.DELAI_ACCORD_PRET_YTD,0) else 0 end as DELAI_ACCORD_PRET_YTD1-- Cumul YTD-1
			,isnull (CY.DELAI_ACCORD_PRET_FM ,0) AS DELAI_ACCORD_PRET_MOIS
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.DELAI_DECAISSEMENT_MTD,0) else 0 end as DELAI_DECAISSEMENT_MTD--Cumul MTD
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.DELAI_DECAISSEMENT_YTD,0) else 0 end as DELAI_DECAISSEMENT_YTD--Cumul YTD
			,case when CY.[DateCalc]=@Date_obs then isnull( PM.DELAI_DECAISSEMENT_MTD,0) else 0 end as DELAI_DECAISSEMENT_MTD1--Cumul MTD-1
			,case when CY.[DateCalc]=@Date_obs then isnull( PY.DELAI_DECAISSEMENT_YTD,0) else 0 end as DELAI_DECAISSEMENT_YTD1-- Cumul YTD-1
			,isnull (CY.DELAI_DECAISSEMENT_FM ,0) AS DELAI_DECAISSEMENT_MOIS

			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_OPP_CRE_PERDUES_MTD,0) else 0 end as NB_OPP_CRE_PERDUES_MTD--Cumul MTD
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_OPP_CRE_PERDUES_YTD,0) else 0 end as NB_OPP_CRE_PERDUES_YTD--Cumul YTD
			,case when CY.[DateCalc]=@Date_obs then isnull( PM.NB_OPP_CRE_PERDUES_MTD,0) else 0 end as NB_OPP_CRE_PERDUES_MTD1--Cumul MTD-1
			,case when CY.[DateCalc]=@Date_obs then isnull( PY.NB_OPP_CRE_PERDUES_YTD,0) else 0 end as NB_OPP_CRE_PERDUES_YTD1-- Cumul YTD-1
			,isnull (CY.NB_OPP_CRE_PERDUES_FM ,0)   AS NB_OPP_CRE_PERDUES_MOIS
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_ANNULATION_MTD,0) else 0 end as NB_ANNULATION_MTD--Cumul MTD
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_ANNULATION_YTD,0) else 0 end as NB_ANNULATION_YTD--Cumul YTD
			,case when CY.[DateCalc]=@Date_obs then isnull( PM.NB_ANNULATION_MTD,0) else 0 end as NB_ANNULATION_MTD1--Cumul MTD-1
			,case when CY.[DateCalc]=@Date_obs then isnull( PY.NB_ANNULATION_YTD,0) else 0 end as NB_ANNULATION_YTD1-- Cumul YTD-1
			,isnull (CY.NB_ANNULATION_FM  ,0)           AS NB_ANNULATION_MOIS
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_ANNULATION_CLIENT_MTD,0) else 0 end as NB_ANNULATION_CLIENT_MTD--Cumul MTD
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_ANNULATION_CLIENT_YTD,0) else 0 end as NB_ANNULATION_CLIENT_YTD--Cumul YTD
			,case when CY.[DateCalc]=@Date_obs then isnull( PM.NB_ANNULATION_CLIENT_MTD,0) else 0 end as NB_ANNULATION_CLIENT_MTD1--Cumul MTD-1
			,case when CY.[DateCalc]=@Date_obs then isnull( PY.NB_ANNULATION_CLIENT_YTD,0) else 0 end as NB_ANNULATION_CLIENT_YTD1-- Cumul YTD-1
			,isnull (CY.NB_ANNULATION_CLIENT_FM ,0)            AS NB_ANNULATION_CLIENT_MOIS
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_RETRACTATION_MTD,0) else 0 end as NB_RETRACTATION_MTD--Cumul MTD
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_RETRACTATION_YTD,0) else 0 end as NB_RETRACTATION_YTD--Cumul YTD
			,case when CY.[DateCalc]=@Date_obs then isnull( PM.NB_RETRACTATION_MTD,0) else 0 end as NB_RETRACTATION_MTD1--Cumul MTD-1
			,case when CY.[DateCalc]=@Date_obs then isnull( PY.NB_RETRACTATION_YTD,0) else 0 end as NB_RETRACTATION_YTD1-- Cumul YTD-1
			,isnull (CY.NB_RETRACTATION_FM ,0)            AS NB_RETRACTATION_MOIS
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_AFFAIRE_REFUSEE_MTD,0) else 0 end as NB_AFFAIRE_REFUSEE_MTD--Cumul MTD
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_AFFAIRE_REFUSEE_YTD,0) else 0 end as NB_AFFAIRE_REFUSEE_YTD--Cumul YTD
			,case when CY.[DateCalc]=@Date_obs then isnull( PM.NB_AFFAIRE_REFUSEE_MTD,0) else 0 end as NB_AFFAIRE_REFUSEE_MTD1--Cumul MTD-1
			,case when CY.[DateCalc]=@Date_obs then isnull( PY.NB_AFFAIRE_REFUSEE_YTD,0) else 0 end as NB_AFFAIRE_REFUSEE_YTD1-- Cumul YTD-1
			,isnull (CY.NB_AFFAIRE_REFUSEE_FM  ,0)           AS NB_AFFAIRE_REFUSEE_MOIS
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_INCIDENT_TECH_MTD,0) else 0 end as NB_INCIDENT_TECH_MTD--Cumul MTD
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_INCIDENT_TECH_YTD,0) else 0 end as NB_INCIDENT_TECH_YTD--Cumul YTD
			,case when CY.[DateCalc]=@Date_obs then isnull( PM.NB_INCIDENT_TECH_MTD,0) else 0 end as NB_INCIDENT_TECH_MTD1--Cumul MTD-1
			,case when CY.[DateCalc]=@Date_obs then isnull( PY.NB_INCIDENT_TECH_YTD,0) else 0 end as NB_INCIDENT_TECH_YTD1-- Cumul YTD-1
			,isnull (CY.NB_INCIDENT_TECH_FM ,0)            AS NB_INCIDENT_TECH_MOIS
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_SANS_SUITE_MTD,0) else 0 end as NB_SANS_SUITE_MTD--Cumul MTD
			,case when CY.[DateCalc]=@Date_obs then isnull( CY.NB_SANS_SUITE_YTD,0) else 0 end as NB_SANS_SUITE_YTD--Cumul YTD
			,case when CY.[DateCalc]=@Date_obs then isnull( PM.NB_SANS_SUITE_MTD,0) else 0 end as NB_SANS_SUITE_MTD1--Cumul MTD-1
			,case when CY.[DateCalc]=@Date_obs then isnull( PY.NB_SANS_SUITE_YTD,0) else 0 end as NB_SANS_SUITE_YTD1-- Cumul YTD-1
			,isnull (CY.NB_SANS_SUITE_FM ,0)            AS NB_SANS_SUITE_MOIS
			
FROM CalculatedDataset CY --Current Year
     LEFT OUTER JOIN CalculatedDataset PM --Previous month
		  ON DATEADD(MONTH,-1,CY.[DateCalc]) = PM.[DateCalc]
	 LEFT OUTER JOIN CalculatedDataset PY --Previous Year
		  ON DATEADD(YEAR,-1,CY.[DateCalc]) = PY.[DateCalc]

where  CY.[DateCalc] IN (@Date_obs ,CY.LastDateMonth )
ORDER BY CY.[DateCalc]  DESC

END;