﻿

CREATE PROC [dbo].[PKGMK_PROC_COMPTE]  @DATE_ALIM DATE  
                                   ,@jours_histo INT    --valeur 1 par défaut = traitement nominal par défaut
								   ,@nbRows INT OUTPUT -- nb lignes processées
AS  
BEGIN 

IF @DATE_ALIM IS NULL 
  SET @DATE_ALIM = GETDATE();
 
IF @jours_histo IS NULL 
  SET @jours_histo = 1; --valeur 1 par défaut = traitement nominal par défaut

--------------------------------------///////////////// Step 1
--création d'un sous-ensemble de la table dimension temps

-- PURGE AVANT INSERT
IF OBJECT_ID('tempdb..#WK_TEMPS_ACCOUNT') IS NOT NULL
BEGIN DROP TABLE #WK_TEMPS_ACCOUNT END

SELECT StandardDate
INTO #WK_TEMPS_ACCOUNT
FROM DIM_TEMPS
WHERE StandardDate < CAST(@DATE_ALIM AS DATE) AND StandardDate >= DATEADD(DAY,- @jours_histo, @DATE_ALIM)
--------------------------------------///////////////// Step 2
--Merge de la table _Compte && _Compte_History et insertion dans une table work

-- PURGE AVANT INSERT
IF OBJECT_ID('tempdb..#WK_SAB_COMPTES') IS NOT NULL
BEGIN Drop TABLE #WK_SAB_COMPTES END

-- INSERTION DANS TABLE WORK
SELECT DWHCPTCOM
	  ,DWHCPTPPAL 
	  ,DWHCPTRUB
      ,DWHCPTDAC
	  ,DWHCPTDAO
	  ,DWHCPTCPT
	  ,DWHCPTMTA
	  ,DWHCPTMTD
	  ,Validity_StartDate
	  ,Validity_EndDate
INTO #WK_SAB_COMPTES	
FROM [$(DataHubDatabaseName)].[dbo].[VW_PV_SA_Q_COMPTE]
WHERE Validity_EndDate >= DATEADD(DAY,- @jours_histo,@DATE_ALIM) AND DWHCPTRUB IN ('251180','254181','254111')  --'251180' = CAV

--select * from #WK_SAB_COMPTES
--------------------------------------///////////////// Step 3
--Photo sur la période requise et insertion dans une table res

-- PURGE AVANT INSERT
IF OBJECT_ID('tempdb..#RES_COMPTES') IS NOT NULL
BEGIN Drop TABLE #RES_COMPTES END

-- INSERTION DANS TABLE WORK
SELECT * INTO #RES_COMPTES
FROM
(
SELECT d.StandardDate AS Date_traitement
      ,c.DWHCPTCOM
	  ,c.DWHCPTPPAL
	  ,c.DWHCPTRUB
	  ,c.DWHCPTDAC
	  ,c.DWHCPTDAO
	  ,c.DWHCPTCPT
	  ,c.DWHCPTMTA
	  ,c.DWHCPTMTD
	  ,c.Validity_StartDate
	  ,c.Validity_EndDate

FROM #WK_TEMPS_ACCOUNT d
-- MDI 06/07/18 : Récupération également des comptes cloturés
/*
outer apply 
	(
	select DWHCPTCOM 
	      ,DWHCPTPPAL
          ,DWHCPTRUB
		  ,DWHCPTDAC
		  ,DWHCPTDAO
		  ,DWHCPTCPT
		  ,DWHCPTMTA
		  ,DWHCPTMTD
		  ,Validity_StartDate
		  ,Validity_EndDate

	from #WK_SAB_COMPTES

	where (cast(Validity_StartDate as date) <= d.StandardDate and d.StandardDate < cast(Validity_EndDate as date) )  
	) c
)r
*/
OUTER APPLY 
	(
	SELECT DWHCPTCOM 
	      ,DWHCPTPPAL
          ,DWHCPTRUB
		  ,DWHCPTDAC
		  ,DWHCPTDAO
		  ,DWHCPTCPT
		  ,DWHCPTMTA
		  ,DWHCPTMTD
		  ,Validity_StartDate
		  ,Validity_EndDate
          , ROW_NUMBER() OVER(PARTITION BY DWHCPTCOM ORDER BY Validity_EndDate DESC) LAST_VER
	FROM [$(DataHubDatabaseName)].[dbo].[VW_PV_SA_Q_COMPTE]
	WHERE DWHCPTRUB IN ('251180','254181','254111')
     AND (CAST(Validity_StartDate AS DATE) <= d.StandardDate)
	) c
WHERE LAST_VER = 1
)r
--select cast('9999-12-31' as date) 
--------------------------------------///////////////// Step 4
-- nettoyage + insertion dans la table cible
-- nettoyage de la table cible (suppréssion des lignes a recharger)
DELETE FROM [dbo].[MKT_COMPTE]
WHERE [DTE_TRAITEMENT] IN (SELECT StandardDate FROM #WK_TEMPS_ACCOUNT)
-- ALIMENTATION DE LA TABLE CIBLE MKT_COMPTE
INSERT INTO [dbo].[MKT_COMPTE] (
       [DTE_TRAITEMENT]     
      ,[IDE_OPPRT]      
      ,[NUM_CPT_SAB]    
      ,[NUM_CLI_SAB]    
      ,[RUB_CPT_SAB]    
      ,[FLAG_CPT]       
      ,[COD_PROD_COM]   
      ,[LABEL_PROD_COM] 
      ,[TYP_CPT_SAB]    
      ,[DTE_OUV]        
      ,[DTE_CLO]        
      ,[MTT_PREM_VER]   
      ,[SLD_DTE_CPT]    
      ,[MTT_AUT]               
      ,[MTT_DEP]        
      ,[FLG_DEP_FAC_CAI]
      ,[FLG_MOB_BAN]    
      ,[ACTIVITE_CPT]
	  ,[LIB_ACTIVITE_CPT])
SELECT 	c.Date_traitement
	   ,o.IDE_OPPRT
	   ,c.DWHCPTCOM   [NUM_CPT_SAB]
	   ,c.DWHCPTPPAL  [NUM_CLI_SAB]
	   ,c.DWHCPTRUB   [RUB_CPT_SAB]
	   ,CASE WHEN  DWHCPTRUB = '251180' THEN 'CAV' ELSE 'CSL' END AS FLAG_CPT
	   ,p.CODE
	   ,p.LABEL
	   ,pc.PLAN_DE_COMPTES_CODE_PRODUIT
	   ,c.DWHCPTDAO
	   ,c.DWHCPTDAC
	   --,a.FIRST_DEPOSIT_AMOUNT
	   , REQ_PREM_VERS.MTT_EUR
	   ,c.DWHCPTCPT
	   ,c.DWHCPTMTA
	   ,c.DWHCPTMTD
	   ,CASE WHEN c.DWHCPTMTD > 0 THEN 'Oui' ELSE 'Non' END AS TOP_DEP_FAC_CAI
	   ,CASE WHEN a.BANK_DOMICILIATION > 0 THEN 'Oui' ELSE 'Non' END AS TOP_MOB_BAN
	   ,CASE 
	         /*CAV*/
		     WHEN DWHCPTRUB = '251180' AND ISNULL(x.NB_OP_DEB,0)  >= 30 THEN 4
	         WHEN DWHCPTRUB = '251180' AND ISNULL(x.NB_OP_DEB,0) >=6 and isnull(x.NB_OP_DEB,0) <=29 THEN 3
			 WHEN DWHCPTRUB = '251180' AND ISNULL(x.NB_OP_DEB,0) >=1 and isnull(x.NB_OP_DEB,0) <=5 THEN 2
			 WHEN DWHCPTRUB = '251180' AND ISNULL(x.NB_OP_DEB,0) =0  THEN 1
			 /*CSL*/
			 WHEN DWHCPTRUB <> '251180' AND ISNULL(x.NB_OP_CRE,0) >= 3 THEN 4
	         WHEN DWHCPTRUB <> '251180' AND ISNULL(x.NB_OP_CRE,0) = 2  THEN 3
			 WHEN DWHCPTRUB <> '251180' AND ISNULL(x.NB_OP_CRE,0) = 1  THEN 2
			 WHEN DWHCPTRUB <> '251180' AND ISNULL(x.NB_OP_CRE,0) = 0  THEN 1
	    END AS ACTIVITE
	   ,CASE
	         -- MDI 04/07/18 : MISE À JOUR DES LIBELLÉS D'ACTIVITÉ
	         /*CAV*/
			 WHEN DWHCPTRUB = '251180' AND ISNULL(x.NB_OP_DEB,0)  >= 30 THEN 'Actif ++' -- 'Très Actif'
	         WHEN DWHCPTRUB = '251180' AND ISNULL(x.NB_OP_DEB,0) >=6 AND ISNULL(X.NB_OP_DEB,0) <=29 THEN 'Actif +' -- 'Semi Actif'
			 WHEN DWHCPTRUB = '251180' AND ISNULL(x.NB_OP_DEB,0) >=1 AND ISNULL(X.NB_OP_DEB,0) <=5 THEN 'Quasi Actif' -- 'Peu Actif'
			 WHEN DWHCPTRUB = '251180' AND ISNULL(x.NB_OP_DEB,0) =0  THEN 'Inactif'
			 /*CSL*/
			 WHEN DWHCPTRUB <> '251180' AND ISNULL(x.NB_OP_CRE,0) >= 3 THEN 'Actif ++'     -- 'Très Actif'
	         WHEN DWHCPTRUB <> '251180' AND ISNULL(x.NB_OP_CRE,0) = 2  THEN 'Actif +'      -- 'Semi Actif'
			 WHEN DWHCPTRUB <> '251180' AND ISNULL(x.NB_OP_CRE,0) = 1  THEN 'Quasi Actif'  -- 'Peu Actif'
			 WHEN DWHCPTRUB <> '251180' AND ISNULL(x.NB_OP_CRE,0) = 0  THEN  'Inactif'
	    END  as LIB_ACTIVITE
FROM #RES_COMPTES c
LEFT JOIN (SELECT t.[MOIS_TRAITEMENT], 
                  t.NUM_CPT_SAB,
                  SUM(t.NB_OP_DEB) AS NB_OP_DEB ,
	              SUM(t.NB_OP_CRE) AS NB_OP_CRE
           FROM [dbo].[T_FACT_MKT_NB_MTT_OP_LAST_THREE_MONTHS] t
		   GROUP BY t.[MOIS_TRAITEMENT],t.NUM_CPT_SAB) x ON x.NUM_CPT_SAB = c.DWHCPTCOM AND FORMAT(CAST(c.Date_traitement AS DATE),'yyyyMM') = x.[MOIS_TRAITEMENT]
LEFT JOIN [$(DataHubDatabaseName)].[dbo].PV_FE_EQUIPMENT e
           ON e.EQUIPMENT_NUMBER = c.DWHCPTCOM
LEFT JOIN [$(DataHubDatabaseName)].[dbo].PV_FE_ENROLMENTFOLDER ef
           ON ef.ID_FE_ENROLMENT_FOLDER = e.ENROLMENT_FOLDER_ID
LEFT JOIN [$(DataHubDatabaseName)].[dbo].PV_FE_SUBSCRIBEDPRODUCT sp   --Pour le mm compte deux ID_FE_SUBSCRIBEDPRODUCT (210920,218237) correspondant au meme ENROLMENT_FOLDER_ID 101328
           ON sp.ENROLMENT_FOLDER_ID = ef.ID_FE_ENROLMENT_FOLDER AND sp.COMMERCIAL_PRODUCT_ID IN ('1','21') -- filtre sur les CAV et CSL
LEFT JOIN [$(DataHubDatabaseName)].[dbo].PV_FE_COMMERCIALPRODUCT p
           ON p.ID_FE_COMMERCIALPRODUCT = e.PRODUCT_ID
LEFT JOIN [dbo].MKT_OPPORTUNITE o
           ON o.IDE_OPPRT_SF = ef.SALESFORCE_OPPORTUNITY_ID
LEFT JOIN [$(DataHubDatabaseName)].[dbo].PV_FE_SBDCHKACCOUNT a
          ON a.ID_FE_CHKACCOUNT = sp.ID_FE_SUBSCRIBEDPRODUCT
LEFT JOIN [$(DataHubDatabaseName)].[dbo].REF_PLAN_DE_COMPTES pc
		  ON pc.PLAN_DE_COMPTES_COMPTE_OBLIGATOIRE = c.DWHCPTRUB
LEFT JOIN (SELECT NUM_CPT, MTT_EUR
           FROM [$(DataHubDatabaseName)].[dbo].PV_SA_Q_MOUVEMENT
           WHERE 
		   /* Règle Premier versement CSL */
		   COD_OPE = 'ERE' AND COD_EVE = 'OUV' AND COD_ANA = 'VEREPA') REQ_PREM_VERS
		  ON c.DWHCPTCOM = REQ_PREM_VERS.NUM_CPT
		
SELECT @nbRows = @@ROWCOUNT

-- SUPPRESSION DES ENREGISTREMENTS > 90 JRS 
DELETE FROM [dbo].[MKT_COMPTE] 
  WHERE (DATEDIFF(DAY,[DTE_TRAITEMENT],DATEADD(dd,-1,@date_alim)) > 90 AND [DTE_TRAITEMENT] !=EOMONTH([DTE_TRAITEMENT]));

END