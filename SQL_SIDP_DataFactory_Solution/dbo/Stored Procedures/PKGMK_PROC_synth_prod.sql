﻿CREATE PROC [dbo].[PKGMK_PROC_synth_prod] @nbRows int OUTPUT -- nb lignes processées 
AS  BEGIN 

-- ===============================================================================================================================================================
--tables input :  [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_COMPTE]
--                [$(DataHubDatabaseName)].[dbo].[PV_FE_EQUIPMENT]
--                [$(DataHubDatabaseName)].[dbo].PV_FE_COMMERCIALPRODUCT
--                [$(DataHubDatabaseName)].[dbo].PV_FE_COMMERCIALOFFER
--                [DIM_PRODUIT_OFFRE]
--tables output : wk_synthese_prod : table niveau compte, remplacée avec chaque traitement
--                sas_vue_synthese_prod : table niveau agrégé, remplacée avec chaque traitement
--description : créer une agrégé avec des indicateurs : ouvertures, fermetures, ouvertures cumulées, fermetures cumulées, stock calculé; par type de compte 
-- ==============================================================================================================================================================

---------------------------------------------------------------------------------------------------
-- chargement #wk_synth_prod_init avec les infos à date des comptes Orange Bank
---------------------------------------------------------------------------------------------------
if object_id('tempdb..#wk_synth_prod_init') is not null
begin drop table #wk_synth_prod_init end

--declare @RecordTypeClient varchar(18)
--select @RecordTypeClient = CODE_SF from [dbo].[DIM_RECORDTYPE] where LIBELLE = 'Personne Physique Client'

select DWHCPTCOM as NUM_CPT, DWHCPTRUB as code_sab_type_compte, DWHCPTDAO as date_ouvert, DWHCPTDAC as date_ferm
  into #wk_synth_prod_init
  from ( select DWHCPTCOM, DWHCPTRUB, DWHCPTDAO, DWHCPTDAC, DWHCPTPPAL
              , row_number() over (partition by [DWHCPTCOM] order by [Validity_StartDate] desc) as [Validity_Flag]
           from [$(DataHubDatabaseName)].[dbo].[VW_PV_SA_Q_COMPTE]
           where DWHCPTRUB in ('251180', '254181', '254111') /* pour isoler les comptes Orange Bank */ ) sa_q_compte

  join ( select DWHCLICLI, DWHCLIDNA, DWHCLIDCR, DWHCLICLO
              , row_number() over (partition by DWHCLICLI order by Validity_StartDate desc) as Validity_Flag
           from [$(DataHubDatabaseName)].[dbo].[VW_PV_SA_Q_CLIENT] ) sa_q_client
    on sa_q_client.DWHCLICLI = sa_q_compte.DWHCPTPPAL and sa_q_client.Validity_Flag = sa_q_compte.Validity_Flag 

  join ( select Id_SF as Id_SF_Account, IDCustomerSAB__pc, Occupation__pc, OccupationNiv1__pc, OccupationNiv3__pc, OBFirstContactDate__c, Preattribution__c, RecordTypeId
              , row_number() over (partition by Id_SF order by Validity_StartDate desc) as Validity_Flag
           from [$(DataHubDatabaseName)].[dbo].[VW_PV_SF_ACCOUNT] ) sf_client
    on sf_client.IDCustomerSAB__pc = sa_q_client.DWHCLICLI and sf_client.Validity_Flag = sa_q_client.Validity_Flag --and sf_client.RecordTypeId = @RecordTypeClient

  join ( select Id_SF,AccountId as Id_SF_Account, DistributorNetwork__c, CompleteFileFlag__c, IDProcessSous__c, Indication__c, DistributorEntity__c, StartedChannel__c  
                from [$(DataHubDatabaseName)].[dbo].[PV_SF_OPPORTUNITY] where CommercialOfferCode__c='OC80' and StageName='09') sf_opp
    on sf_opp.Id_SF_Account = sf_client.Id_SF_Account 

 where sa_q_compte.[Validity_Flag] = 1;

---------------------------------------------------------------------------------------------------
-- chargement #wk_temps_synth_prod avec la liste des dates à charger
---------------------------------------------------------------------------------------------------
declare @date_min date;
select @date_min = min(cast(date_ouvert as date)) from #wk_synth_prod_init;

declare @date_max date; 
select @date_max = getdate();

if object_id('tempdb..#wk_temps_synth_prod') is not null
begin drop table #wk_temps_synth_prod end

select PK_ID, StandardDate 
  into #wk_temps_synth_prod
  from DIM_TEMPS
 where @date_min <= StandardDate and StandardDate <= @date_max;

---------------------------------------------------------------------------------------------------
-- ventilation des comptes par date d'ouverture
---------------------------------------------------------------------------------------------------
if object_id('tempdb..#wk_synth_prod_o') is not null
begin drop table #wk_synth_prod_o end

select d.StandardDate as date_alim
     , o.NUM_CPT
     , case when o.NUM_CPT is not null then 1 else 0 end as flag_ouvert
     , 0 as flag_ferm
     , o.code_sab_type_compte
  into #wk_synth_prod_o
  from #wk_temps_synth_prod d
  outer apply ( select * from #wk_synth_prod_init where d.StandardDate =  date_ouvert ) o
;

---------------------------------------------------------------------------------------------------
-- ventilation des comptes par date de cloture
---------------------------------------------------------------------------------------------------
if object_id('tempdb..#wk_synth_prod_c') is not null
begin drop table #wk_synth_prod_c end

select d.StandardDate as date_alim
     , c.NUM_CPT
     , 0 as flag_ouvert
     , case when c.NUM_CPT is not null then 1 else 0 end as flag_ferm
     , c.code_sab_type_compte
  into #wk_synth_prod_c
  from #wk_temps_synth_prod d
  outer apply ( select * from #wk_synth_prod_init where d.StandardDate =  date_ferm ) c
;

---------------------------------------------------------------------------------------------------
-- reconstitution de wk_synthese_prod
---------------------------------------------------------------------------------------------------
truncate table wk_synthese_prod;

insert into wk_synthese_prod
select date_alim
     , cast(date_alim as date) as jour_alim
     , NUM_CPT
     , sum(flag_ouvert)
     , sum(flag_ferm)
     , max(code_sab_type_compte)
  from ( select * from #wk_synth_prod_o
         union all
         select * from #wk_synth_prod_c ) a
 group by a.date_alim, a.NUM_CPT
;

---------------------------------------------------------------------------------------------------
-- reconstitution de sas_vue_synthese_prod
---------------------------------------------------------------------------------------------------
truncate table sas_vue_synthese_prod;

with cte_synthese_prod 
as ( 
  select jour_alim, code_sab_type_compte,
         sum(flag_ouvert) as ouvert_jour, sum(flag_ferm) as ferm_jour
    from wk_synthese_prod
   group by jour_alim, code_sab_type_compte
)

insert into sas_vue_synthese_prod 
select jour_alim
     , code_sab_type_compte
     , ouvert_jour
     , ferm_jour
     , sum(ouvert_jour) over (partition by code_sab_type_compte order by jour_alim rows between unbounded preceding and current row) as ouvert_cumul
     , sum(ferm_jour) over (partition by code_sab_type_compte order by jour_alim rows between unbounded preceding and current row) as ferm_cumul
     , sum(ouvert_jour-ferm_jour) over (partition by code_sab_type_compte order by jour_alim rows between unbounded preceding and current row) as stock_calcul 
  from cte_synthese_prod
;

select @nbRows = COUNT(*) from sas_vue_synthese_prod;

if object_id('tempdb..#wk_synth_prod_init') is not null
begin drop table #wk_synth_prod_init end

if object_id('tempdb..#wk_temps_synth_prod') is not null
begin drop table #wk_temps_synth_prod end

if object_id('tempdb..#wk_synth_prod_o') is not null
begin drop table #wk_synth_prod_o end

if object_id('tempdb..#wk_synth_prod_c') is not null
begin drop table #wk_synth_prod_c end

EXEC [dbo].[PROC_SSRS_SYNTHESE_PROD];
END
GO 
