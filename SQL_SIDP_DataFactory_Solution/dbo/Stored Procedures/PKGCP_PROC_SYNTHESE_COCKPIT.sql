﻿-- ===============================================================================================================================================================================
--tables input : [$(DataHubDatabaseName)].dbo.PV_SF_ACCOUNT
--                 [$(DataHubDatabaseName)].dbo.PV_SF_OPPORTUNITY_HISTORY
--                 [$(DataHubDatabaseName)].dbo.PV_SF_EQUIPMENT
--                 SAS_VUE_STOCK_OPPORT
--                 SAS_VUE_FLUX_OPPORT_FIN
--                 SAS_VUE_RC_INTERACTIONS
--                 SAS_VUE_RC_CASES
--                 SAS_VUE_SYNTHESE_PROD
--                 SAS_VUE_USAGE_CPT
--description : génération de la vue pré-agrégée qui alimente le rapport SSRS "Synthese Cockpit"
-- =============================================================================================================================================================================

CREATE  PROC [dbo].[PKGCP_PROC_SYNTHESE_COCKPIT]
   @date_obs date = '2017-06-16'
AS  BEGIN 

SET DATEFIRST 1; -->1    Lundi

WITH
cte_jour_alim_list AS
( SELECT jour_alim2 as jour_alim
       , max(nb_days) as nb_days
       , max(current_day) as current_day
       , max(current_week) as current_week
       , max(current_month) as current_month
       , max(current_year) as current_year
    from ( SELECT [StandardDate] as jour_alim
                , case when [StandardDate] >= dateadd(day, (-1 * DATEPART(dw, @date_obs)) + 1, @date_obs) then [StandardDate] --> for current week, no aggregation
                       when [StandardDate] >= DateAdd(mm, DateDiff(mm, 0, @date_obs), 0) then DateAdd(mm, DateDiff(mm, 0, [StandardDate]), 0) --> for current month (excluding current week), aggregation
                       when [StandardDate] >= DateAdd(yyyy, DateDiff(yyyy, 0, @date_obs), 0) then DateAdd(yyyy, DateDiff(yyyy, 0, [StandardDate]), 0) --> for current year (excluding current week and month), aggregation
                       else CONVERT(date, '20151220', 112) end as jour_alim2 --> date corresponding to beginning of time

                , case when [StandardDate] >= dateadd(day, (-1 * DATEPART(dw, @date_obs)) + 1, @date_obs) then 1 --> for current week, no aggregation
                       when [StandardDate] >= DateAdd(mm, DateDiff(mm, 0, @date_obs), 0) then DateDiff(dd, DateAdd(mm, DateDiff(mm, 0, [StandardDate]), 0), dateadd(day, (-1 * DATEPART(dw, @date_obs)) + 1, @date_obs)) --> for current month (excluding current week), aggregation
                       when [StandardDate] >= DateAdd(yyyy, DateDiff(yyyy, 0, @date_obs), 0) then DateDiff(dd, DateAdd(yyyy, DateDiff(yyyy, 0, [StandardDate]), 0), CASE WHEN DateAdd(mm, DateDiff(mm, 0, @date_obs), 0) > dateadd(day, (-1 * DATEPART(dw, @date_obs)) + 1, @date_obs) THEN dateadd(day, (-1 * DATEPART(dw, @date_obs)) + 1, @date_obs) ELSE DateAdd(mm, DateDiff(mm, 0, @date_obs), 0) END) --> for current year (excluding current week and month), aggregation
                       else DateDiff(dd, CONVERT(date, '20151220', 112), DateAdd(yyyy, DateDiff(yyyy, 0, @date_obs), 0)) end as nb_days --> date corresponding to beginning of time

                 , case when [StandardDate] = @date_obs then 1 else 0 end AS current_day
                 , case when [StandardDate] >= dateadd(day, (-1 * DATEPART(dw, @date_obs)) + 1, @date_obs) then 1 else 0 end AS current_week
                 , case when [StandardDate] >= DateAdd(mm, DateDiff(mm, 0, @date_obs), 0) and [StandardDate] < DateAdd(mm, DateDiff(mm, 0, @date_obs) + 1, 0) then 1 else 0 end AS current_month
                 , case when [StandardDate] >= DateAdd(yyyy, DateDiff(yyyy, 0, @date_obs), 0) and [StandardDate] < DateAdd(yyyy, DateDiff(yyyy, 0, @date_obs) + 1, 0) then 1 else 0 end AS current_year
             FROM [dbo].[DIM_TEMPS]
            where [StandardDate] <= dateadd(day, (7 - DATEPART(dw, @date_obs)), @date_obs)) t
  group by jour_alim2
),

cte_usage_cpt_cp AS
( SELECT jour_alim2 as jour_alim
--     , sum(nb_comptes_CAV) as nb_comptes_CAV
       , sum(encours_CAV) as encours_CAV
       , sum(encours_cred_CAV) as encours_cred_CAV
       , sum(encours_deb_CAV) as encours_deb_CAV
       , sum(encours_deb_non_auth_CAV) as encours_deb_non_auth_CAV
--     , sum(nb_comptes_CSL) as nb_comptes_CSL
       , sum(montant_dep_CSL) as montant_dep_CSL
       , sum(montant_ret_CSL) as montant_ret_CSL
       , sum(flux_net_CSL) as flux_net_CSL
       , sum(encours_CSL) as encours_CSL
    from ( SELECT jour_alim
                , case when jour_alim >= dateadd(day, (-1 * DATEPART(dw, @date_obs)) + 1, @date_obs) then jour_alim --> for current week, no aggregation
                       when jour_alim >= DateAdd(mm, DateDiff(mm, 0, @date_obs), 0) then DateAdd(mm, DateDiff(mm, 0, jour_alim), 0) --> for current month (excluding current week), aggregation
                       when jour_alim >= DateAdd(yyyy, DateDiff(yyyy, 0, @date_obs), 0) then DateAdd(yyyy, DateDiff(yyyy, 0, jour_alim), 0) --> for current year (excluding current week and month), aggregation
                       else CONVERT(date, '20151220', 112) end as jour_alim2 --> date corresponding to beginning of time

--               , case when code_sab_type_compte = '251180' then nb_comptes else 0 end as nb_comptes_CAV
                 , case when code_sab_type_compte = '251180' then coalesce(encours_cred, 0) + coalesce(encours_deb, 0) else 0 end as encours_CAV
                 , case when code_sab_type_compte = '251180' then encours_cred else 0 end as encours_cred_CAV
                 , case when code_sab_type_compte = '251180' then encours_deb else 0 end as encours_deb_CAV
                 , case when code_sab_type_compte = '251180' then encours_deb_non_auth else 0 end as encours_deb_non_auth_CAV
--               , case when code_sab_type_compte in ('254181','254111') then nb_comptes else 0 end as nb_comptes_CSL
                 , case when code_sab_type_compte in ('254181','254111') then montant_dep else 0 end as montant_dep_CSL
                 , case when code_sab_type_compte in ('254181','254111') then montant_ret else 0 end as montant_ret_CSL
                 , case when code_sab_type_compte in ('254181','254111') then coalesce(montant_dep, 0) + coalesce(montant_ret, 0) else 0 end as flux_net_CSL
                 , case when code_sab_type_compte in ('254181','254111') then coalesce(encours_cred, 0) + coalesce(encours_deb, 0) else 0 end as encours_CSL
            FROM sas_vue_usage_cpt
            where jour_alim <= dateadd(day, (7 - DATEPART(dw, @date_obs)), @date_obs)) t
  group by jour_alim2
),

cte_synthese_prod AS
( SELECT jour_alim2 as jour_alim
       , sum(nb_ouvertures_CAV) as nb_ouvertures_CAV
       , sum(nb_fermetures_CAV) as nb_fermetures_CAV
       , sum(nb_ouvertures_CSL) as nb_ouvertures_CSL
       , sum(nb_fermetures_CSL) as nb_fermetures_CSL
   from ( SELECT jour_alim
                , case when jour_alim >= dateadd(day, (-1 * DATEPART(dw, @date_obs)) + 1, @date_obs) then jour_alim --> for current week, no aggregation
                       when jour_alim >= DateAdd(mm, DateDiff(mm, 0, @date_obs), 0) then DateAdd(mm, DateDiff(mm, 0, jour_alim), 0) --> for current month (excluding current week), aggregation
                       when jour_alim >= DateAdd(yyyy, DateDiff(yyyy, 0, @date_obs), 0) then DateAdd(yyyy, DateDiff(yyyy, 0, jour_alim), 0) --> for current year (excluding current week and month), aggregation
                       else CONVERT(date, '20151220', 112) end as jour_alim2 --> date corresponding to beginning of time

                 , case when code_sab_type_compte = '251180' then ouvert_jour else 0 end as nb_ouvertures_CAV
                 , case when code_sab_type_compte = '251180' then ferm_jour else 0 end as nb_fermetures_CAV
                 , case when code_sab_type_compte in ('254181','254111') then ouvert_jour else 0 end as nb_ouvertures_CSL
                 , case when code_sab_type_compte in ('254181','254111') then ferm_jour else 0 end as nb_fermetures_CSL
            FROM sas_vue_synthese_prod 
            where jour_alim <= dateadd(day, (7 - DATEPART(dw, @date_obs)), @date_obs)) t
  group by jour_alim2
),

rq_cli_int_cp AS
(
--SET DATEFIRST 1;    
--declare @date_obs date  = '2017-04-09';
  SELECT jour_alim2 as jour_alim
       , sum(nb_interactions_tel)+sum(nb_interactions_sms)+sum(nb_interactions_mail)+sum(nb_interactions_chat_ia) as nb_interactions
       , sum(nb_interactions_tel) as nb_interactions_tel
       , sum(nb_interactions_sms) as nb_interactions_sms
       , sum(nb_interactions_mail) as nb_interactions_mail
       , sum(nb_interactions_chat_crc) as nb_interactions_chat_crc
       , sum(nb_interactions_chat_ia) as nb_interactions_chat_ia
    from ( SELECT StandardDate
                , case when StandardDate >= dateadd(day, (-1 * DATEPART(dw, @date_obs)) + 1, @date_obs) then StandardDate --> for current week, no aggregation
                       when StandardDate >= DateAdd(mm, DateDiff(mm, 0, @date_obs), 0) then DateAdd(mm, DateDiff(mm, 0, StandardDate), 0) --> for current month (excluding current week), aggregation
                       when StandardDate >= DateAdd(yyyy, DateDiff(yyyy, 0, @date_obs), 0) then DateAdd(yyyy, DateDiff(yyyy, 0, StandardDate), 0) --> for current year (excluding current week and month), aggregation
                       else CONVERT(date, '20151220', 112) end as jour_alim2 --> date corresponding to beginning of time

                , case when interaction_type='Téléphonie'  then 1 else 0 end AS nb_interactions_tel
                , case when interaction_type='Email'  then 1 else 0 end AS nb_interactions_mail
                , case when interaction_type='SMS' then 1 else 0 end AS nb_interactions_sms
                , case when interaction_sous_type='Chat CRC'  then 1 else 0 end AS nb_interactions_chat_crc
                , case when interaction_sous_type='Chat IA'  then 1 else 0 end AS nb_interactions_chat_ia
             FROM SAS_Vue_RC_Interactions
            where StandardDate <= dateadd(day, (7 - DATEPART(dw, @date_obs)), @date_obs)) t
  group by jour_alim2
),

rq_cli_case_cp AS
(
--SET DATEFIRST 1;    
--declare @date_obs date  = '2017-04-09';
  SELECT jour_alim2 as jour_alim
       , sum(nb_demandes_clients) as nb_demandes_clients
       , sum(nb_reclamations_clients) as nb_reclamations_clients
       , sum(flag_fcr) as flag_fcr
       , SUM(total_case_jour) as total_case_jour
       , SUM(nb_demandes_chq) as nb_demandes_chq

    from ( SELECT [DATE]
                , case when [DATE] >= dateadd(day, (-1 * DATEPART(dw, @date_obs)) + 1, @date_obs) then [DATE] --> for current week, no aggregation
                       when [DATE] >= DateAdd(mm, DateDiff(mm, 0, @date_obs), 0) then DateAdd(mm, DateDiff(mm, 0, [DATE]), 0) --> for current month (excluding current week), aggregation
                       when [DATE] >= DateAdd(yyyy, DateDiff(yyyy, 0, @date_obs), 0) then DateAdd(yyyy, DateDiff(yyyy, 0, [DATE]), 0) --> for current year (excluding current week and month), aggregation
                       else CONVERT([DATE], '20151220', 112) end as jour_alim2 --> date corresponding to beginning of time

                 , case when RECORDTYPE_LABEL='Demande'  then 1 else 0 end AS nb_demandes_clients
                 , case when RECORDTYPE_LABEL='Réclamation'  then 1 else 0 end AS nb_reclamations_clients
                 , case when FCR='Y' then 1 else 0 end as flag_fcr
                 , case when FK_STATUS=3 then 1 else 0 end as total_case_jour
                 , case when TYPE_CASE_LABEL = 'Chèque' and FK_SUBTYPE_CASE=104 then 1 else 0 end as nb_demandes_chq

            FROM [SAS_Vue_RC_Cases]
            where [DATE] <= dateadd(day, (7 - DATEPART(dw, @date_obs)), @date_obs)) t
  group by jour_alim2
), 

rq_cli_enrolment_cp AS
(
--SET DATEFIRST 1;    
--declare @date_obs date  = '2017-10-13';
    SELECT jour_alim2 as jour_alim
       , sum(nb_opport_en_cours) as nb_opport_en_cours
       , sum(nb_opport_canal_ori_digital) as nb_opport_canal_ori_digital
       , sum(nb_opport_canal_ori_boutique) as nb_opport_canal_ori_boutique
       , sum(nb_opport_canal_ori_crc) as nb_opport_canal_ori_crc
       , sum(nb_opport_suite_indication) as nb_opport_suite_indication

    from ( 
        
       SELECT date_alim
                , case when date_alim >= dateadd(day, (-1 * DATEPART(dw, @date_obs)) + 1, @date_obs) then date_alim --> for current week, no aggregation
                       when date_alim >= DateAdd(mm, DateDiff(mm, 0, @date_obs), 0) then DateAdd(mm, DateDiff(mm, 0, date_alim), 0) --> for current month (excluding current week), aggregation
                       when date_alim >= DateAdd(yyyy, DateDiff(yyyy, 0, @date_obs), 0) then DateAdd(yyyy, DateDiff(yyyy, 0, date_alim), 0) --> for current year (excluding current week and month), aggregation
                       else CONVERT(date, '20151220', 112) end as jour_alim2 --> date corresponding to beginning of time

                 , case when stade_vente  <> 'Compte ouvert' and stade_vente  <> 'Affaire refusée' and stade_vente  <> 'Sans suite' and stade_vente  <> 'Affaire conclue' and stade_vente  <> 'Incident technique' and stade_vente  <> 'Rétractation' then nb_opport else 0 end as nb_opport_en_cours --> Ticket num 36 : Exclure le stade de vente "Incident technique" du comptage des opportunités en cours (Ahmed)
                 , case when stade_vente  <> 'Compte ouvert' and stade_vente  <> 'Affaire refusée' and stade_vente  <> 'Sans suite' and stade_vente  <> 'Affaire conclue' and stade_vente  <> 'Incident technique' and stade_vente  <> 'Rétractation' and canal_distrib_origine = 'Digital' then nb_opport else 0 end as nb_opport_canal_ori_digital
                 , case when stade_vente  <> 'Compte ouvert' and stade_vente  <> 'Affaire refusée' and stade_vente  <> 'Sans suite' and stade_vente  <> 'Affaire conclue' and stade_vente  <> 'Incident technique' and stade_vente  <> 'Rétractation' and canal_distrib_origine = 'Agence' then nb_opport else 0 end as nb_opport_canal_ori_boutique
                 , case when stade_vente  <> 'Compte ouvert' and stade_vente  <> 'Affaire refusée' and stade_vente  <> 'Sans suite' and stade_vente  <> 'Affaire conclue' and stade_vente  <> 'Incident technique' and stade_vente  <> 'Rétractation' and canal_distrib_origine = 'Crc' then nb_opport else 0 end as nb_opport_canal_ori_crc
                 
                 , case when flag_indication = 'Oui' then nb_opport else 0 end as nb_opport_suite_indication

            FROM sas_vue_stock_opport
            where cast(date_creat_opp as date) >='2017-05-16' and date_alim <= dateadd(day, (7 - DATEPART(dw, @date_obs)), @date_obs)) t
  group by jour_alim2
),

rq_opp_ouv as
(
--SET DATEFIRST 1;    
--declare @date_obs date  = '2017-10-13';
    SELECT jour_alim2 as jour_alim
            , sum(nb_ouv_cpte) as nb_ouv_cpte
            , sum(nb_ouv_cpte_canal_fin_digital) as nb_ouv_cpte_canal_fin_digital
            , sum(nb_ouv_cpte_canal_fin_boutique) as nb_ouv_cpte_canal_fin_boutique
            , sum(nb_ouv_cpte_canal_fin_crc) as nb_ouv_cpte_canal_fin_crc
            , sum(nb_ouv_cpte_full_digital) as nb_ouv_cpte_full_digital
            , sum(nb_ouv_cpte_full_boutique) as nb_ouv_cpte_full_boutique
            , sum(age_opport_digital) as age_opport_digital
            , sum(age_opport_boutique) as age_opport_boutique
            
            --, delai_moyen_full_digital as delai_moyen_full_digital = age_opport_digital / nb_ouv_cpte
            --, delai_moyen_full_boutique as delai_moyen_full_boutique = age_opport_boutique / nb_ouv_cpte
    from ( 
        
       SELECT jour_alim
                , case when jour_alim >= dateadd(day, (-1 * DATEPART(dw, @date_obs)) + 1, @date_obs) then jour_alim --> for current week, no aggregation
                       when jour_alim >= DateAdd(mm, DateDiff(mm, 0, @date_obs), 0) then DateAdd(mm, DateDiff(mm, 0, jour_alim), 0) --> for current month (excluding current week), aggregation
                       when jour_alim >= DateAdd(yyyy, DateDiff(yyyy, 0, @date_obs), 0) then DateAdd(yyyy, DateDiff(yyyy, 0, jour_alim), 0) --> for current year (excluding current week and month), aggregation
                       else CONVERT(date, '20151220', 112) end as jour_alim2 --> date corresponding to beginning of time
                 , case when stade_vente  in ('Compte ouvert','Affaire conclue') and CAST(jour_alim as date) = CAST(date_fin_opp as date) then 1 else 0 end as nb_ouv_cpte

                 , case when stade_vente  in ('Compte ouvert','Affaire conclue')  and CAST(jour_alim as date) = CAST(date_fin_opp as date)  and  canal_distrib_final = 'Digital' then 1 else 0 end as nb_ouv_cpte_canal_fin_digital 
                 , case when stade_vente  in ('Compte ouvert','Affaire conclue')  and CAST(jour_alim as date) = CAST(date_fin_opp as date) and  canal_distrib_final = 'Agence' then 1 else 0 end as nb_ouv_cpte_canal_fin_boutique 
                 , case when stade_vente  in ('Compte ouvert','Affaire conclue')  and CAST(jour_alim as date) = CAST(date_fin_opp as date) and  canal_distrib_final = 'CRC' then 1 else 0 end as nb_ouv_cpte_canal_fin_crc 

                 , case when stade_vente  in ('Compte ouvert','Affaire conclue')  and CAST(jour_alim as date) = CAST(date_fin_opp as date)  and  flag_full_digi='1' then 1 else 0 end as nb_ouv_cpte_full_digital 
                 , case when stade_vente  in ('Compte ouvert','Affaire conclue')  and CAST(jour_alim as date) = CAST(date_fin_opp as date) and  flag_full_btq='1' then 1 else 0 end as nb_ouv_cpte_full_boutique 
                 , case when stade_vente  in ('Compte ouvert','Affaire conclue')  and flag_full_digi='1' then  age_opport else 0 end as age_opport_digital
                 , case when stade_vente  in ('Compte ouvert','Affaire conclue')  and flag_full_btq='1' then  age_opport else 0 end as age_opport_boutique

        FROM sas_vue_flux_opport_fin
        where cast(date_creat_opp as date) >='2017-05-16' and jour_alim <= dateadd(day, (7 - DATEPART(dw, @date_obs)), @date_obs)) t
    group by jour_alim2
),
rq_cli_cp AS
(
select jour_alim2 as jour_alim
     , sum(nb_clients_ouvert) as nb_clients
     , sum(nb_clients_ferm) as nb_clients_ferm_dern_cpte
     , sum(nb_clients_pre_attr) as nb_clients_pre_attr
     , sum(nb_clients_score_er_1star) as nb_clients_score_er_1star
     , sum(nb_clients_score_er_2star) as nb_clients_score_er_2star
     , sum(nb_clients_score_er_3star) as nb_clients_score_er_3star
     , sum(flag_pm_rem) as nb_clients_eq_pm
  from ( 
    select jour_alim
         , case when jour_alim >= dateadd(day, (-1 * DATEPART(dw, @date_obs)) + 1, @date_obs) then jour_alim --> for current week, no aggregation
                when jour_alim >= DateAdd(mm, DateDiff(mm, 0, @date_obs), 0) then DateAdd(mm, DateDiff(mm, 0, jour_alim), 0) --> for current month (excluding current week), aggregation
                when jour_alim >= DateAdd(yyyy, DateDiff(yyyy, 0, @date_obs), 0) then DateAdd(yyyy, DateDiff(yyyy, 0, jour_alim), 0) --> for current year (excluding current week and month), aggregation
                else CONVERT(date, '20151220', 112) end as jour_alim2 --> date corresponding to beginning of time
         
         , nb_clients_ouvert
         , nb_clients_ferm
         , flag_pm_rem
         , case when Preattribution__c is not null then nb_clients_ouvert else 0 end AS nb_clients_pre_attr
         , case when RelationEntryScore__c = 4 then nb_clients_ouvert else 0 end AS nb_clients_score_er_1star
         , case when RelationEntryScore__c = 3 then nb_clients_ouvert else 0 end AS nb_clients_score_er_2star
         , case when RelationEntryScore__c = 2 then nb_clients_ouvert else 0 end AS nb_clients_score_er_3star
      from sas_vue_cli
     where jour_alim <= dateadd(day, (7 - DATEPART(dw, @date_obs)), @date_obs)
  ) t
 group by jour_alim2
),

rq_eq_assur_cp AS
(
--SET DATEFIRST 1;    
--declare @date_obs date  = '2017-06-18';
 SELECT jour_alim2 as jour_alim
       , sum(nb_assurances) as nb_assurances
       , sum(nb_assurances_moy_pai) as nb_assurances_moy_pai
       , sum(nb_assurances_prot_vie_cour) as nb_assurances_prot_vie_cour
       , sum(nb_assurances_prot_achats) as nb_assurances_prot_achats
       , sum(nb_assurances_ident) as nb_assurances_ident

   from ( SELECT cast(date_souscription as date) as Validity_StartDate
                , case when date_souscription >= dateadd(day, (-1 * DATEPART(dw, @date_obs)) + 1, @date_obs) then cast(date_souscription as date) --> for current week, no aggregation
                       when date_souscription >= DateAdd(mm, DateDiff(mm, 0, @date_obs), 0) then cast(DateAdd(mm, DateDiff(mm, 0, date_souscription), 0) AS date) --> for current month (excluding current week), aggregation
                       when date_souscription >= DateAdd(yyyy, DateDiff(yyyy, 0, @date_obs), 0) then cast(DateAdd(yyyy, DateDiff(yyyy, 0, date_souscription), 0) as date) --> for current year (excluding current week and month), aggregation
                       else CONVERT(date, '20151220', 112) end as jour_alim2 --> date corresponding to beginning of time

                 , nb_assurances
                 , case when code_fe_assurance = 1 then nb_assurances else 0 end AS nb_assurances_moy_pai
                 , case when code_fe_assurance = 2 then nb_assurances else 0 end AS nb_assurances_prot_vie_cour
                 , case when code_fe_assurance = 3 then nb_assurances else 0 end AS nb_assurances_prot_achats
                 , case when code_fe_assurance = 4 then nb_assurances else 0 end AS nb_assurances_ident

            FROM ssrs_vue_equipement
            where date_souscription  <= dateadd(day, (7 - DATEPART(dw, @date_obs)), @date_obs)) t
  group by jour_alim2
)
SELECT jour_alim_list.jour_alim
     , jour_alim_list.nb_days
     , row_number() over (order by case when cli_enrolment_cp.jour_alim is not null then 1 else 2 end, jour_alim_list.jour_alim desc) as current_day
     , jour_alim_list.current_week
     , jour_alim_list.current_month
     , jour_alim_list.current_year
--   , coalesce(usage_cpt_cp.nb_comptes_CAV, 0) as nb_comptes_CAV
     , sum(coalesce(synthese_prod.nb_ouvertures_CAV,0) - coalesce(synthese_prod.nb_fermetures_CAV, 0) ) OVER(ORDER BY jour_alim_list.jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_comptes_CAV
     , coalesce(synthese_prod.nb_ouvertures_CAV, 0) as nb_ouvertures_CAV
     , coalesce(synthese_prod.nb_fermetures_CAV, 0) as nb_fermetures_CAV
     , coalesce(usage_cpt_cp.encours_CAV, 0) as encours_CAV
     , coalesce(usage_cpt_cp.encours_cred_CAV, 0) as encours_cred_CAV
     , coalesce(usage_cpt_cp.encours_deb_CAV, 0) as encours_deb_CAV
     , coalesce(usage_cpt_cp.encours_deb_non_auth_CAV, 0) as encours_deb_non_auth_CAV

--   , coalesce(usage_cpt_cp.nb_comptes_CSL, 0) as nb_comptes_CSL
     , sum(coalesce(synthese_prod.nb_ouvertures_CSL,0) - coalesce(synthese_prod.nb_fermetures_CSL, 0) ) OVER(ORDER BY jour_alim_list.jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_comptes_CSL
     , coalesce(synthese_prod.nb_ouvertures_CSL, 0) as nb_ouvertures_CSL
     , coalesce(synthese_prod.nb_fermetures_CSL, 0) as nb_fermetures_CSL
     , coalesce(usage_cpt_cp.montant_dep_CSL, 0) as montant_dep_CSL
     , coalesce(usage_cpt_cp.montant_ret_CSL, 0) as montant_ret_CSL
     , coalesce(usage_cpt_cp.flux_net_CSL, 0) as flux_net_CSL
     , coalesce(usage_cpt_cp.encours_CSL, 0) as encours_CSL
     
     , coalesce(cli_case_cp.nb_demandes_clients, 0) as nb_demandes_clients
     , coalesce(cli_case_cp.nb_reclamations_clients, 0) as nb_reclamations_clients
     , coalesce(cli_case_cp.flag_fcr, 0) as flag_fcr
     , coalesce(cli_case_cp.total_case_jour, 0) as total_case_jour
     , coalesce(cli_case_cp.nb_demandes_chq, 0) as nb_demandes_chq

     , coalesce(cli_int_cp.nb_interactions, 0) as nb_interactions
     , coalesce(cli_int_cp.nb_interactions_tel, 0) as nb_interactions_tel
     , coalesce(cli_int_cp.nb_interactions_sms, 0) as nb_interactions_sms
     , coalesce(cli_int_cp.nb_interactions_mail, 0) as nb_interactions_mail
     , coalesce(cli_int_cp.nb_interactions_chat_crc, 0) as nb_interactions_chat_crc
     , coalesce(cli_int_cp.nb_interactions_chat_ia, 0) as nb_interactions_chat_ia

     , coalesce(cli_enrolment_cp.nb_opport_en_cours, 0) as nb_opport_en_cours
     , coalesce(cli_enrolment_cp.nb_opport_canal_ori_digital, 0) as nb_opport_canal_ori_digital
     , coalesce(cli_enrolment_cp.nb_opport_canal_ori_boutique, 0) as nb_opport_canal_ori_boutique
     , coalesce(cli_enrolment_cp.nb_opport_canal_ori_crc, 0) as nb_opport_canal_ori_crc
     , coalesce(cli_enrolment_cp.nb_opport_suite_indication, 0) as nb_opport_suite_indication
     
     , coalesce(cli_opp_ouv.nb_ouv_cpte, 0) as nb_ouv_cpte
     , coalesce(cli_opp_ouv.nb_ouv_cpte_canal_fin_digital, 0) as nb_ouv_cpte_canal_fin_digital
     , coalesce(cli_opp_ouv.nb_ouv_cpte_canal_fin_boutique, 0) as nb_ouv_cpte_canal_fin_boutique
     , coalesce(cli_opp_ouv.nb_ouv_cpte_canal_fin_crc, 0) as nb_ouv_cpte_canal_fin_crc
     , coalesce(cli_opp_ouv.nb_ouv_cpte_full_digital, 0) as nb_ouv_cpte_full_digital
     , coalesce(cli_opp_ouv.nb_ouv_cpte_full_boutique, 0) as nb_ouv_cpte_full_boutique
     , coalesce(cli_opp_ouv.age_opport_digital, 0) as age_opport_digital
     , coalesce(cli_opp_ouv.age_opport_boutique, 0) as age_opport_boutique

     , coalesce(cli_cp.nb_clients, 0) as nb_clients
     , sum(coalesce(cli_cp.nb_clients, 0)) OVER(ORDER BY jour_alim_list.jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_ouv_cli_cum
     , coalesce(cli_cp.nb_clients_pre_attr, 0) as nb_clients_pre_attr
     , coalesce(cli_cp.nb_clients_score_er_1star, 0) as nb_clients_score_er_1star
     , coalesce(cli_cp.nb_clients_score_er_2star, 0) as nb_clients_score_er_2star
     , coalesce(cli_cp.nb_clients_score_er_3star, 0) as nb_clients_score_er_3star

     , coalesce(cli_cp.nb_clients_ferm_dern_cpte, 0) as nb_clients_ferm_dern_cpte
     , sum(coalesce(cli_cp.nb_clients_ferm_dern_cpte, 0)) OVER(ORDER BY jour_alim_list.jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_clients_clos

     , sum(coalesce(cli_cp.nb_clients,0) - coalesce(cli_cp.nb_clients_ferm_dern_cpte, 0) ) OVER(ORDER BY jour_alim_list.jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_clients_parc

     , coalesce(cli_cp.nb_clients_eq_pm, 0) as nb_clients_eq_pm

     , coalesce(eq_assur_cp.nb_assurances, 0) as nb_assurances
     , coalesce(eq_assur_cp.nb_assurances_moy_pai, 0) as nb_assurances_moy_pai
     , coalesce(eq_assur_cp.nb_assurances_prot_vie_cour, 0) as nb_assurances_prot_vie_cour
     , coalesce(eq_assur_cp.nb_assurances_prot_achats, 0) as nb_assurances_prot_achats
     , coalesce(eq_assur_cp.nb_assurances_ident, 0) as nb_assurances_ident 
     
FROM cte_jour_alim_list as jour_alim_list
LEFT JOIN cte_usage_cpt_cp as usage_cpt_cp
    ON usage_cpt_cp.jour_alim = jour_alim_list.jour_alim
LEFT JOIN cte_synthese_prod as synthese_prod
    ON synthese_prod.jour_alim = jour_alim_list.jour_alim
LEFT JOIN rq_cli_case_cp as cli_case_cp
    ON cli_case_cp.jour_alim = jour_alim_list.jour_alim
LEFT JOIN rq_cli_int_cp as cli_int_cp
    ON cli_int_cp.jour_alim = jour_alim_list.jour_alim
LEFT JOIN rq_cli_enrolment_cp as cli_enrolment_cp
    ON cli_enrolment_cp.jour_alim = jour_alim_list.jour_alim
LEFT JOIN rq_opp_ouv as cli_opp_ouv
    ON cli_opp_ouv.jour_alim = jour_alim_list.jour_alim
LEFT JOIN rq_cli_cp as cli_cp
    ON cli_cp.jour_alim = jour_alim_list.jour_alim
LEFT JOIN rq_eq_assur_cp as eq_assur_cp
    ON eq_assur_cp.jour_alim = jour_alim_list.jour_alim
order by jour_alim_list.jour_alim

END