﻿CREATE PROCEDURE [dbo].[PKGMC_PROC_contacts_M]
	@nb_collected_rows int output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
			
truncate table [dbo].[SAS_Vue_MC_Contacts_M];
WITH CAMPAGNE AS ( 
SELECT distinct cmp.ID_CAMPAIGN, cmp.CAMPAIGN_NAME, cmp.END_DATE, cmp.TYPOLOGY_CAMPAIGN, cmp.TYPOLOGY_NOTIFICATION, cmp.RETARGETTING_COMMENTS, cmp.RETARGETTING_FACEBOOK, cmp.RETARGETTING_TWITTER, 
cmp.STATUS_CAMPAIGN, cmp.FLAG_ROI, cmp.CHANNEL, cmp.OFFER_CODES, cmp.OFFER_LABELS, cmp.FLAG_CLIENTS, cmp.FLAG_PROSPECTS 
FROM 
[$(DataHubDatabaseName)].[dbo].[PV_MC_CAMPAIGNMESSAGE] cmp 
WHERE cmp.TYPOLOGY_CAMPAIGN='JOURNEY-COMMERCIAL' or cmp.CAMPAIGN_NAME='P_ACOM_ENROLEMENTBIS_09042017' or CAMPAIGN_NAME like '%PARRAINAGE%' 
),
CAMPAGNE_CALL AS (
SELECT distinct cmp.ID_CAMPAIGN, cmp.CAMPAIGN_NAME, cmp.END_DATE, cmp.TYPOLOGY_CAMPAIGN, cmp.TYPOLOGY_NOTIFICATION, cmp.RETARGETTING_COMMENTS, cmp.RETARGETTING_FACEBOOK, cmp.RETARGETTING_TWITTER, 
cmp.STATUS_CAMPAIGN, cmp.FLAG_ROI, cmp.OFFER_CODES, cmp.OFFER_LABELS, cmp.FLAG_CLIENTS, cmp.FLAG_PROSPECTS 
FROM 
[$(DataHubDatabaseName)].[dbo].[PV_MC_CAMPAIGNMESSAGE] cmp 
WHERE cmp.TYPOLOGY_CAMPAIGN='JOURNEY-COMMERCIAL' or cmp.CAMPAIGN_NAME='P_ACOM_ENROLEMENTBIS_09042017' or CAMPAIGN_NAME like '%PARRAINAGE%' 
),
OUVERTURES AS ( 
select distinct opn.SubscriberKey,opn.SendID,
FIRST_VALUE(opn.Device) over (partition by opn.SubscriberKey, opn.SendID order by opn.Device) as Device, 
FIRST_VALUE(opn.OperatingSystem) over (partition by opn.SubscriberKey, opn.SendID order by opn.OperatingSystem) as OperatingSystem 
FROM 
[$(DataHubDatabaseName)].[dbo].[PV_MC_OPENS] opn 
where opn.IsUnique='True' 
)

INSERT INTO [dbo].[SAS_Vue_MC_Contacts_M]
           ([PK_ID]
           ,[ID_CAMPAIGN]
           ,[CAMPAIGN_NAME]
           ,[END_DATE]
           ,[TYPOLOGY_CAMPAIGN]
           ,[TYPOLOGY_NOTIFICATION]
           ,[RETARGETTING_COMMENTS]
           ,[RETARGETTING_FACEBOOK]
           ,[RETARGETTING_TWITTER]
           ,[STATUS_CAMPAIGN]
           ,[FLAG_ROI]
           ,[CHANNEL]
           ,[OFFER_CODES]
           ,[OFFER_LABELS]
           ,[FLAG_CLIENTS]
           ,[FLAG_PROSPECTS]
           ,[MESSAGE_NAME]
           ,[DATE_TIME_SEND]
           ,[NUM_JOUR]
           ,[JOUR]
           ,[HEURE_ENVOI]
           ,[DEVICE]
           ,[SYSTEM_OPERATION]
           ,[BOUNCE_CATEGORY]
           ,[DOMAINE]
           ,[COUNT_TYPE]
           ,[COUNT_RESULT])
/*  Email - Contacts Sortants */

SELECT distinct 

CONCAT(cmp.ID_CAMPAIGN,REVERSE(SUBSTRING(SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)),1,CHARINDEX('_',SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)))-1))) as PK_ID, 
cmp.ID_CAMPAIGN,
cmp.CAMPAIGN_NAME,
cmp.END_DATE, 
cmp.TYPOLOGY_CAMPAIGN, 
cmp.TYPOLOGY_NOTIFICATION, 
cmp.RETARGETTING_COMMENTS,
cmp.RETARGETTING_FACEBOOK, 
cmp.RETARGETTING_TWITTER, 
cmp.STATUS_CAMPAIGN, 
cmp.FLAG_ROI, 
cmp.CHANNEL, 
cmp.OFFER_CODES,
cmp.OFFER_LABELS, 
cmp.FLAG_CLIENTS, 
cmp.FLAG_PROSPECTS, 
REVERSE(SUBSTRING(SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)),1,CHARINDEX('_',SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)))-1)) as MESSAGE_NAME, 
cast(eml.DATE_TIME_SEND as date) as DATE_TIME_SEND, 
datepart(WEEKDAY,eml.DATE_TIME_SEND) as NUM_JOUR, 
(case datepart(WEEKDAY,eml.DATE_TIME_SEND) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end) as JOUR ,  
(case when LEN(CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar))= '1' then CONCAT('0',CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar)) else CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar) end) as HEURE_ENVOI, 
opn.Device as DEVICE, 
opn.OperatingSystem as SYSTEM_OPERATION,
NULL as BOUNCE_CATEGORY, 
NULL AS DOMAINE, 
'NB_CONTACTS_SORTANTS' AS COUNT_TYPE,
COUNT(eml.ID_CONTACT) as COUNT_RESULT

  FROM [$(DataHubDatabaseName)].[dbo].[PV_MC_EMAILLOG] eml 
  JOIN CAMPAGNE cmp on eml.ID_CAMPAIGN = cmp.ID_CAMPAIGN
  LEFT JOIN OUVERTURES opn on opn.SubscriberKey = eml.ID_CONTACT and opn.SendID=eml.JobID

WHERE cmp.CHANNEL='EMAIL' 

group by 
CONCAT(cmp.ID_CAMPAIGN,REVERSE(SUBSTRING(SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)),1,CHARINDEX('_',SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)))-1))),
cmp.ID_CAMPAIGN,cmp.CAMPAIGN_NAME,cmp.END_DATE,cmp.TYPOLOGY_CAMPAIGN,cmp.TYPOLOGY_NOTIFICATION,cmp.RETARGETTING_COMMENTS,
cmp.RETARGETTING_FACEBOOK,cmp.RETARGETTING_TWITTER,cmp.STATUS_CAMPAIGN,cmp.FLAG_ROI,cmp.CHANNEL,cmp.OFFER_CODES,cmp.OFFER_LABELS,cmp.FLAG_CLIENTS,cmp.FLAG_PROSPECTS, 
REVERSE(SUBSTRING(SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)),1,CHARINDEX('_',SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)))-1)), 
cast(eml.DATE_TIME_SEND as date),datepart(WEEKDAY,eml.DATE_TIME_SEND),(case datepart(WEEKDAY,eml.DATE_TIME_SEND) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end), 
(case when LEN(CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar))= '1' then CONCAT('0',CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar)) else CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar) end),opn.Device, 
opn.OperatingSystem

UNION

/*  Email - BOUNCES */
SELECT distinct 
CONCAT(cmp.ID_CAMPAIGN,REVERSE(SUBSTRING(SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)),1,CHARINDEX('_',SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)))-1))) as PK_ID, 
cmp.ID_CAMPAIGN,
cmp.CAMPAIGN_NAME,
cmp.END_DATE, 
cmp.TYPOLOGY_CAMPAIGN, 
cmp.TYPOLOGY_NOTIFICATION, 
cmp.RETARGETTING_COMMENTS,
cmp.RETARGETTING_FACEBOOK, 
cmp.RETARGETTING_TWITTER, 
cmp.STATUS_CAMPAIGN, 
cmp.FLAG_ROI, 
cmp.CHANNEL, 
cmp.OFFER_CODES,
cmp.OFFER_LABELS, 
cmp.FLAG_CLIENTS, 
cmp.FLAG_PROSPECTS, 
REVERSE(SUBSTRING(SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)),1,CHARINDEX('_',SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)))-1)) as MESSAGE_NAME, 
cast(eml.DATE_TIME_SEND as date) as DATE_TIME_SEND, 
datepart(WEEKDAY,eml.DATE_TIME_SEND) as NUM_JOUR, 
(case datepart(WEEKDAY,eml.DATE_TIME_SEND) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end) as JOUR ,  
(case when LEN(CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar))= '1' then CONCAT('0',CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar)) else CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar) end) as HEURE_ENVOI, 
opn.Device as DEVICE, 
opn.OperatingSystem as SYSTEM_OPERATION,
bnc.BounceCategory as BOUNCE_CATEGORY, 
(substring(bnc.EmailAddress,(charindex('@',bnc.EmailAddress)+1),LEN(bnc.EmailAddress)-charindex('.',reverse(bnc.EmailAddress))-charindex('@',bnc.EmailAddress))) AS DOMAINE, 
'NB_BOUNCES' AS COUNT_TYPE,
COUNT(bnc.SubscriberKey) as COUNT_RESULT

  FROM [$(DataHubDatabaseName)].[dbo].[PV_MC_EMAILLOG] eml 
  JOIN CAMPAGNE cmp on eml.ID_CAMPAIGN = cmp.ID_CAMPAIGN 
  JOIN [$(DataHubDatabaseName)].[dbo].[PV_MC_BOUNCES] bnc on bnc.SubscriberKey = eml.ID_CONTACT and bnc.SendID=eml.JobID 
  LEFT JOIN OUVERTURES opn on opn.SubscriberKey = eml.ID_CONTACT and opn.SendID=eml.JobID

WHERE cmp.CHANNEL='EMAIL'

group by 
CONCAT(cmp.ID_CAMPAIGN,REVERSE(SUBSTRING(SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)),1,CHARINDEX('_',SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)))-1))),
cmp.ID_CAMPAIGN,cmp.CAMPAIGN_NAME,cmp.END_DATE,cmp.TYPOLOGY_CAMPAIGN,cmp.TYPOLOGY_NOTIFICATION,cmp.RETARGETTING_COMMENTS,
cmp.RETARGETTING_FACEBOOK,cmp.RETARGETTING_TWITTER,cmp.STATUS_CAMPAIGN,cmp.FLAG_ROI,cmp.CHANNEL,cmp.OFFER_CODES,cmp.OFFER_LABELS,cmp.FLAG_CLIENTS,cmp.FLAG_PROSPECTS, 
REVERSE(SUBSTRING(SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)),1,CHARINDEX('_',SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)))-1)), 
cast(eml.DATE_TIME_SEND as date),datepart(WEEKDAY,eml.DATE_TIME_SEND),(case datepart(WEEKDAY,eml.DATE_TIME_SEND) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end), 
(case when LEN(CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar))= '1' then CONCAT('0',CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar)) else CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar) end),
bnc.BounceCategory, 
(substring(bnc.EmailAddress,(charindex('@',bnc.EmailAddress)+1),LEN(bnc.EmailAddress)-charindex('.',reverse(bnc.EmailAddress))-charindex('@',bnc.EmailAddress))),opn.Device, 
opn.OperatingSystem

UNION

/*  Email - OUVERTURES UNIQUES */
SELECT distinct 
CONCAT(cmp.ID_CAMPAIGN,REVERSE(SUBSTRING(SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)),1,CHARINDEX('_',SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)))-1))) as PK_ID, 
cmp.ID_CAMPAIGN,
cmp.CAMPAIGN_NAME,
cmp.END_DATE, 
cmp.TYPOLOGY_CAMPAIGN, 
cmp.TYPOLOGY_NOTIFICATION, 
cmp.RETARGETTING_COMMENTS,
cmp.RETARGETTING_FACEBOOK, 
cmp.RETARGETTING_TWITTER, 
cmp.STATUS_CAMPAIGN, 
cmp.FLAG_ROI, 
cmp.CHANNEL, 
cmp.OFFER_CODES,
cmp.OFFER_LABELS, 
cmp.FLAG_CLIENTS, 
cmp.FLAG_PROSPECTS, 
REVERSE(SUBSTRING(SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME))
,1,CHARINDEX('_',SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)))-1)) as MESSAGE_NAME, 
cast(eml.DATE_TIME_SEND as date) as DATE_TIME_SEND, 
datepart(WEEKDAY,eml.DATE_TIME_SEND) as NUM_JOUR, 
(case datepart(WEEKDAY,eml.DATE_TIME_SEND) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end) as JOUR ,  
(case when LEN(CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar))= '1' then CONCAT('0',CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar)) else CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar) end) as HEURE_ENVOI, 
opn.Device as DEVICE, 
opn.OperatingSystem as SYSTEM_OPERATION,
NULL as BOUNCE_CATEGORY, 
NULL AS DOMAINE, 
'NB_OUVERTURES_UNIQUES' AS COUNT_TYPE,
COUNT(opn.SubscriberKey) as COUNT_RESULT

  FROM [$(DataHubDatabaseName)].[dbo].[PV_MC_EMAILLOG] eml 
  JOIN CAMPAGNE cmp on eml.ID_CAMPAIGN = cmp.ID_CAMPAIGN 
  JOIN [$(DataHubDatabaseName)].[dbo].PV_MC_OPENS opn on opn.SubscriberKey = eml.ID_CONTACT and opn.SendID=eml.JobID

WHERE cmp.CHANNEL='EMAIL' and IsUnique='True'

group by 
CONCAT(cmp.ID_CAMPAIGN,REVERSE(SUBSTRING(SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)),1,CHARINDEX('_',SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)))-1))),
cmp.ID_CAMPAIGN,cmp.CAMPAIGN_NAME,cmp.END_DATE,cmp.TYPOLOGY_CAMPAIGN,cmp.TYPOLOGY_NOTIFICATION,cmp.RETARGETTING_COMMENTS,
cmp.RETARGETTING_FACEBOOK,cmp.RETARGETTING_TWITTER,cmp.STATUS_CAMPAIGN,cmp.FLAG_ROI,cmp.CHANNEL,cmp.OFFER_CODES,cmp.OFFER_LABELS,cmp.FLAG_CLIENTS,cmp.FLAG_PROSPECTS, 
REVERSE(SUBSTRING(SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)),1,CHARINDEX('_',SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)))-1)), 
cast(eml.DATE_TIME_SEND as date),datepart(WEEKDAY,eml.DATE_TIME_SEND),(case datepart(WEEKDAY,eml.DATE_TIME_SEND) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end), 
(case when LEN(CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar))= '1' then CONCAT('0',CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar)) else CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar) end),
opn.Device, 
opn.OperatingSystem

UNION

/*  Email - CLICS UNIQUES */
SELECT distinct 
CONCAT(cmp.ID_CAMPAIGN,REVERSE(SUBSTRING(SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)),1,CHARINDEX('_',SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)))-1))) as PK_ID, 
cmp.ID_CAMPAIGN,
cmp.CAMPAIGN_NAME,
cmp.END_DATE, 
cmp.TYPOLOGY_CAMPAIGN, 
cmp.TYPOLOGY_NOTIFICATION, 
cmp.RETARGETTING_COMMENTS,
cmp.RETARGETTING_FACEBOOK, 
cmp.RETARGETTING_TWITTER, 
cmp.STATUS_CAMPAIGN, 
cmp.FLAG_ROI, 
cmp.CHANNEL, 
cmp.OFFER_CODES,
cmp.OFFER_LABELS, 
cmp.FLAG_CLIENTS, 
cmp.FLAG_PROSPECTS, 
REVERSE(SUBSTRING(SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME))
,1,CHARINDEX('_',SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)))-1)) as MESSAGE_NAME, 
cast(eml.DATE_TIME_SEND as date) as DATE_TIME_SEND, 
datepart(WEEKDAY,eml.DATE_TIME_SEND) as NUM_JOUR, 
(case datepart(WEEKDAY,eml.DATE_TIME_SEND) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end) as JOUR ,  
(case when LEN(CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar))= '1' then CONCAT('0',CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar)) else CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar) end) as HEURE_ENVOI, 
clk.Device as DEVICE, 
clk.OperatingSystem as SYSTEM_OPERATION,
NULL as BOUNCE_CATEGORY, 
NULL AS DOMAINE, 
'NB_CLICS_UNIQUES' AS COUNT_TYPE,
COUNT(clk.SubscriberKey) as COUNT_RESULT

  FROM [$(DataHubDatabaseName)].[dbo].[PV_MC_EMAILLOG] eml 
  JOIN CAMPAGNE cmp on eml.ID_CAMPAIGN = cmp.ID_CAMPAIGN 
  JOIN [$(DataHubDatabaseName)].[dbo].PV_MC_CLICKS clk on clk.SubscriberKey = eml.ID_CONTACT and clk.SendID=eml.JobID 

WHERE cmp.CHANNEL='EMAIL' and IsUnique='True' 

group by 
CONCAT(cmp.ID_CAMPAIGN,REVERSE(SUBSTRING(SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)),1,CHARINDEX('_',SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)))-1))),
cmp.ID_CAMPAIGN,cmp.CAMPAIGN_NAME,cmp.END_DATE,cmp.TYPOLOGY_CAMPAIGN,cmp.TYPOLOGY_NOTIFICATION,cmp.RETARGETTING_COMMENTS,
cmp.RETARGETTING_FACEBOOK,cmp.RETARGETTING_TWITTER,cmp.STATUS_CAMPAIGN,cmp.FLAG_ROI,cmp.CHANNEL,cmp.OFFER_CODES,cmp.OFFER_LABELS,cmp.FLAG_CLIENTS,cmp.FLAG_PROSPECTS, 
REVERSE(SUBSTRING(SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)),1,CHARINDEX('_',SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)))-1)), 
cast(eml.DATE_TIME_SEND as date),datepart(WEEKDAY,eml.DATE_TIME_SEND),(case datepart(WEEKDAY,eml.DATE_TIME_SEND) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end), 
(case when LEN(CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar))= '1' then CONCAT('0',CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar)) else CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar) end),
clk.Device, 
clk.OperatingSystem

UNION

/*  Email - DESABONNEMENTS */
SELECT distinct 
CONCAT(cmp.ID_CAMPAIGN,REVERSE(SUBSTRING(SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)),1,CHARINDEX('_',SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)))-1))) as PK_ID, 
cmp.ID_CAMPAIGN,
cmp.CAMPAIGN_NAME,
cmp.END_DATE, 
cmp.TYPOLOGY_CAMPAIGN, 
cmp.TYPOLOGY_NOTIFICATION, 
cmp.RETARGETTING_COMMENTS,
cmp.RETARGETTING_FACEBOOK, 
cmp.RETARGETTING_TWITTER, 
cmp.STATUS_CAMPAIGN, 
cmp.FLAG_ROI, 
cmp.CHANNEL, 
cmp.OFFER_CODES,
cmp.OFFER_LABELS, 
cmp.FLAG_CLIENTS, 
cmp.FLAG_PROSPECTS, 
REVERSE(SUBSTRING(SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME))
,1,CHARINDEX('_',SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)))-1)) as MESSAGE_NAME, 
cast(eml.DATE_TIME_SEND as date) as DATE_TIME_SEND, 
datepart(WEEKDAY,eml.DATE_TIME_SEND) as NUM_JOUR, 
(case datepart(WEEKDAY,eml.DATE_TIME_SEND) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end) as JOUR ,  
(case when LEN(CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar))= '1' then CONCAT('0',CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar)) else CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar) end) as HEURE_ENVOI, 
NULL as DEVICE, 
NULL as SYSTEM_OPERATION,
NULL as BOUNCE_CATEGORY, 
NULL AS DOMAINE, 
'NB_DESABONNEMENTS' AS COUNT_TYPE,
COUNT(uns.SubscriberKey) as COUNT_RESULT

  FROM [$(DataHubDatabaseName)].[dbo].[PV_MC_EMAILLOG] eml 
  JOIN CAMPAGNE cmp on eml.ID_CAMPAIGN = cmp.ID_CAMPAIGN 
  JOIN [$(DataHubDatabaseName)].[dbo].[PV_MC_UNSUBS] uns on uns.SubscriberKey = eml.ID_CONTACT and uns.SendID=eml.JobID 

WHERE cmp.CHANNEL='EMAIL' 

group by 
CONCAT(cmp.ID_CAMPAIGN,REVERSE(SUBSTRING(SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)),1,CHARINDEX('_',SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)))-1))),
cmp.ID_CAMPAIGN,cmp.CAMPAIGN_NAME,cmp.END_DATE,cmp.TYPOLOGY_CAMPAIGN,cmp.TYPOLOGY_NOTIFICATION,cmp.RETARGETTING_COMMENTS,
cmp.RETARGETTING_FACEBOOK,cmp.RETARGETTING_TWITTER,cmp.STATUS_CAMPAIGN,cmp.FLAG_ROI,cmp.CHANNEL,cmp.OFFER_CODES,cmp.OFFER_LABELS,cmp.FLAG_CLIENTS,cmp.FLAG_PROSPECTS, 
REVERSE(SUBSTRING(SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)),1,CHARINDEX('_',SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)))-1)), 
cast(eml.DATE_TIME_SEND as date),datepart(WEEKDAY,eml.DATE_TIME_SEND),(case datepart(WEEKDAY,eml.DATE_TIME_SEND) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end), 
(case when LEN(CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar))= '1' then CONCAT('0',CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar)) else CAST(datepart(hour,eml.DATE_TIME_SEND) as nvarchar) end)

UNION

/*  SMS - NB_CONTACTS_SORTANTS */
SELECT distinct 

CONCAT(cmp.ID_CAMPAIGN,REVERSE(SUBSTRING(REVERSE(sms.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(sms.MESSAGE_NAME))-1))) as PK_ID, 
cmp.ID_CAMPAIGN,
cmp.CAMPAIGN_NAME,
cmp.END_DATE, 
cmp.TYPOLOGY_CAMPAIGN, 
cmp.TYPOLOGY_NOTIFICATION, 
cmp.RETARGETTING_COMMENTS,
cmp.RETARGETTING_FACEBOOK, 
cmp.RETARGETTING_TWITTER, 
cmp.STATUS_CAMPAIGN, 
cmp.FLAG_ROI, 
cmp.CHANNEL, 
cmp.OFFER_CODES,
cmp.OFFER_LABELS, 
cmp.FLAG_CLIENTS, 
cmp.FLAG_PROSPECTS, 
REVERSE(SUBSTRING(REVERSE(sms.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(sms.MESSAGE_NAME))-1)) AS MESSAGE_NAME, 
cast(sms.DATE_TIME_SEND as date) as DATE_TIME_SEND, 
datepart(WEEKDAY,sms.DATE_TIME_SEND) as NUM_JOUR, 
(case datepart(WEEKDAY,sms.DATE_TIME_SEND) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end) as JOUR ,  
(case when LEN(CAST(datepart(hour,sms.DATE_TIME_SEND) as nvarchar))= '1' then CONCAT('0',CAST(datepart(hour,sms.DATE_TIME_SEND) as nvarchar)) else CAST(datepart(hour,sms.DATE_TIME_SEND) as nvarchar) end) as HEURE_ENVOI, 
NULL as DEVICE, 
NULL as SYSTEM_OPERATION,
NULL as BOUNCE_CATEGORY,   
NULL AS DOMAINE, 
'NB_CONTACTS_SORTANTS' AS COUNT_TYPE,
count(case when sms.MESSAGE_TYPE='Outbound' then sms.ID_CONTACT end) as COUNT_RESULT

  FROM [$(DataHubDatabaseName)].[dbo].[PV_MC_SMSLOG] sms  
  JOIN CAMPAGNE cmp on sms.[ID_CAMPAIGN] = cmp.ID_CAMPAIGN

WHERE cmp.CHANNEL='SMS' 

group by 
CONCAT(cmp.ID_CAMPAIGN,REVERSE(SUBSTRING(REVERSE(sms.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(sms.MESSAGE_NAME))-1))),cmp.ID_CAMPAIGN,cmp.CAMPAIGN_NAME,cmp.END_DATE,cmp.TYPOLOGY_CAMPAIGN,cmp.TYPOLOGY_NOTIFICATION,cmp.RETARGETTING_COMMENTS,cmp.RETARGETTING_FACEBOOK,cmp.RETARGETTING_TWITTER,
cmp.STATUS_CAMPAIGN,cmp.FLAG_ROI,cmp.CHANNEL,cmp.OFFER_CODES,cmp.OFFER_LABELS,cmp.FLAG_CLIENTS,cmp.FLAG_PROSPECTS, 
REVERSE(SUBSTRING(REVERSE(sms.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(sms.MESSAGE_NAME))-1)),cast(sms.DATE_TIME_SEND as date),datepart(WEEKDAY,sms.DATE_TIME_SEND), 
(case datepart(WEEKDAY,sms.DATE_TIME_SEND) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end), 
(case when LEN(CAST(datepart(hour,sms.DATE_TIME_SEND) as nvarchar))= '1' then CONCAT('0',CAST(datepart(hour,sms.DATE_TIME_SEND) as nvarchar)) else CAST(datepart(hour,sms.DATE_TIME_SEND) as nvarchar) end) 

UNION

/*  SMS - NB_DELIVRES */
SELECT distinct 

CONCAT(cmp.ID_CAMPAIGN,REVERSE(SUBSTRING(REVERSE(sms.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(sms.MESSAGE_NAME))-1))) as PK_ID, 
cmp.ID_CAMPAIGN,
cmp.CAMPAIGN_NAME,
cmp.END_DATE, 
cmp.TYPOLOGY_CAMPAIGN, 
cmp.TYPOLOGY_NOTIFICATION, 
cmp.RETARGETTING_COMMENTS,
cmp.RETARGETTING_FACEBOOK, 
cmp.RETARGETTING_TWITTER, 
cmp.STATUS_CAMPAIGN, 
cmp.FLAG_ROI, 
cmp.CHANNEL, 
cmp.OFFER_CODES,
cmp.OFFER_LABELS, 
cmp.FLAG_CLIENTS, 
cmp.FLAG_PROSPECTS, 
REVERSE(SUBSTRING(REVERSE(sms.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(sms.MESSAGE_NAME))-1)) AS MESSAGE_NAME, 
cast(sms.DATE_TIME_SEND as date) as DATE_TIME_SEND, 
datepart(WEEKDAY,sms.DATE_TIME_SEND) as NUM_JOUR, 
(case datepart(WEEKDAY,sms.DATE_TIME_SEND) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end) as JOUR ,  
(case when LEN(CAST(datepart(hour,sms.DATE_TIME_SEND) as nvarchar))= '1' then CONCAT('0',CAST(datepart(hour,sms.DATE_TIME_SEND) as nvarchar)) else CAST(datepart(hour,sms.DATE_TIME_SEND) as nvarchar) end) as HEURE_ENVOI, 
NULL as DEVICE, 
NULL as SYSTEM_OPERATION,
NULL as BOUNCE_CATEGORY,   
NULL AS DOMAINE, 

'NB_DELIVRES' AS COUNT_TYPE,
count(case when sms.MESSAGE_TYPE='Outbound' and sms.STATUS='Delivered' then sms.ID_CONTACT end) as COUNT_RESULT

  FROM [$(DataHubDatabaseName)].[dbo].[PV_MC_SMSLOG] sms  
 
  LEFT JOIN CAMPAGNE cmp on sms.[ID_CAMPAIGN] = cmp.ID_CAMPAIGN  

WHERE cmp.CHANNEL='SMS' 

group by 
CONCAT(cmp.ID_CAMPAIGN,REVERSE(SUBSTRING(REVERSE(sms.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(sms.MESSAGE_NAME))-1))),cmp.ID_CAMPAIGN,cmp.CAMPAIGN_NAME,cmp.END_DATE,cmp.TYPOLOGY_CAMPAIGN,cmp.TYPOLOGY_NOTIFICATION,cmp.RETARGETTING_COMMENTS,cmp.RETARGETTING_FACEBOOK,cmp.RETARGETTING_TWITTER,
cmp.STATUS_CAMPAIGN,cmp.FLAG_ROI,cmp.CHANNEL,cmp.OFFER_CODES,cmp.OFFER_LABELS,cmp.FLAG_CLIENTS,cmp.FLAG_PROSPECTS, 
REVERSE(SUBSTRING(REVERSE(sms.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(sms.MESSAGE_NAME))-1)),cast(sms.DATE_TIME_SEND as date),datepart(WEEKDAY,sms.DATE_TIME_SEND), 
(case datepart(WEEKDAY,sms.DATE_TIME_SEND) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end), 
(case when LEN(CAST(datepart(hour,sms.DATE_TIME_SEND) as nvarchar))= '1' then CONCAT('0',CAST(datepart(hour,sms.DATE_TIME_SEND) as nvarchar)) else CAST(datepart(hour,sms.DATE_TIME_SEND) as nvarchar) end) 

UNION

/*  SMS - DESABONNEMENTS */
SELECT distinct 

CONCAT(cmp.ID_CAMPAIGN,REVERSE(SUBSTRING(REVERSE(sms.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(sms.MESSAGE_NAME))-1))) as PK_ID, 
cmp.ID_CAMPAIGN,
cmp.CAMPAIGN_NAME,
cmp.END_DATE, 
cmp.TYPOLOGY_CAMPAIGN, 
cmp.TYPOLOGY_NOTIFICATION, 
cmp.RETARGETTING_COMMENTS,
cmp.RETARGETTING_FACEBOOK, 
cmp.RETARGETTING_TWITTER, 
cmp.STATUS_CAMPAIGN, 
cmp.FLAG_ROI, 
cmp.CHANNEL, 
cmp.OFFER_CODES,
cmp.OFFER_LABELS, 
cmp.FLAG_CLIENTS, 
cmp.FLAG_PROSPECTS, 
REVERSE(SUBSTRING(REVERSE(sms.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(sms.MESSAGE_NAME))-1)) AS MESSAGE_NAME, 
cast(sms.DATE_TIME_SEND as date) as DATE_TIME_SEND, 
datepart(WEEKDAY,sms.DATE_TIME_SEND) as NUM_JOUR, 
(case datepart(WEEKDAY,sms.DATE_TIME_SEND) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end) as JOUR ,  
(case when LEN(CAST(datepart(hour,sms.DATE_TIME_SEND) as nvarchar))= '1' then CONCAT('0',CAST(datepart(hour,sms.DATE_TIME_SEND) as nvarchar)) else CAST(datepart(hour,sms.DATE_TIME_SEND) as nvarchar) end) as HEURE_ENVOI, 
NULL as DEVICE, 
NULL as SYSTEM_OPERATION,
NULL as BOUNCE_CATEGORY,   
NULL AS DOMAINE, 

'NB_DESABONNEMENTS' AS COUNT_TYPE,
count(case when sms.MESSAGE_TYPE='STOP' then sms.ID_CONTACT end) as COUNT_RESULT

  FROM [$(DataHubDatabaseName)].[dbo].[PV_MC_SMSLOG] sms  
 
  LEFT JOIN CAMPAGNE cmp on sms.[ID_CAMPAIGN] = cmp.ID_CAMPAIGN  

WHERE cmp.CHANNEL='SMS' 

group by 
CONCAT(cmp.ID_CAMPAIGN,REVERSE(SUBSTRING(REVERSE(sms.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(sms.MESSAGE_NAME))-1))),cmp.ID_CAMPAIGN,cmp.CAMPAIGN_NAME,cmp.END_DATE,cmp.TYPOLOGY_CAMPAIGN,cmp.TYPOLOGY_NOTIFICATION,cmp.RETARGETTING_COMMENTS,cmp.RETARGETTING_FACEBOOK,cmp.RETARGETTING_TWITTER,
cmp.STATUS_CAMPAIGN,cmp.FLAG_ROI,cmp.CHANNEL,cmp.OFFER_CODES,cmp.OFFER_LABELS,cmp.FLAG_CLIENTS,cmp.FLAG_PROSPECTS, 
REVERSE(SUBSTRING(REVERSE(sms.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(sms.MESSAGE_NAME))-1)),cast(sms.DATE_TIME_SEND as date),datepart(WEEKDAY,sms.DATE_TIME_SEND), 
(case datepart(WEEKDAY,sms.DATE_TIME_SEND) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end), 
(case when LEN(CAST(datepart(hour,sms.DATE_TIME_SEND) as nvarchar))= '1' then CONCAT('0',CAST(datepart(hour,sms.DATE_TIME_SEND) as nvarchar)) else CAST(datepart(hour,sms.DATE_TIME_SEND) as nvarchar) end) 

UNION

/*  PUSH - NB_CONTACTS_SORTANTS */
SELECT  

CONCAT(cmp.ID_CAMPAIGN,REVERSE(SUBSTRING(REVERSE(push.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(push.MESSAGE_NAME))-1))) as PK_ID, 
cmp.ID_CAMPAIGN,
cmp.CAMPAIGN_NAME,
cmp.END_DATE, 
cmp.TYPOLOGY_CAMPAIGN, 
cmp.TYPOLOGY_NOTIFICATION, 
cmp.RETARGETTING_COMMENTS,
cmp.RETARGETTING_FACEBOOK, 
cmp.RETARGETTING_TWITTER, 
cmp.STATUS_CAMPAIGN, 
cmp.FLAG_ROI, 
cmp.CHANNEL, 
cmp.OFFER_CODES,
cmp.OFFER_LABELS, 
cmp.FLAG_CLIENTS, 
cmp.FLAG_PROSPECTS, 
REVERSE(SUBSTRING(REVERSE(push.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(push.MESSAGE_NAME))-1)) AS MESSAGE_NAME, 
cast(push.DATE_TIME_SEND as date) as DATE_TIME_SEND, 
datepart(WEEKDAY,push.DATE_TIME_SEND) as NUM_JOUR, 
(case datepart(WEEKDAY,push.DATE_TIME_SEND) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end) as JOUR ,  
(case when LEN(CAST(datepart(hour,push.DATE_TIME_SEND) as nvarchar))= '1' then CONCAT('0',CAST(datepart(hour,push.DATE_TIME_SEND) as nvarchar)) else CAST(datepart(hour,push.DATE_TIME_SEND) as nvarchar) end) as HEURE_ENVOI, 
NULL as DEVICE, 
NULL as SYSTEM_OPERATION,
NULL as BOUNCE_CATEGORY, 
NULL AS DOMAINE, 

'NB_CONTACTS_SORTANTS' AS COUNT_TYPE,
count(push.ID_CONTACT) as COUNT_RESULT

  FROM [$(DataHubDatabaseName)].[dbo].[PV_MC_PUSHLOG] push 
  JOIN CAMPAGNE cmp on push.[ID_CAMPAIGN] = cmp.ID_CAMPAIGN 

WHERE cmp.CHANNEL='PUSH' 

group by 
CONCAT(cmp.ID_CAMPAIGN,REVERSE(SUBSTRING(REVERSE(push.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(push.MESSAGE_NAME))-1))),cmp.ID_CAMPAIGN,cmp.CAMPAIGN_NAME,cmp.END_DATE,cmp.TYPOLOGY_CAMPAIGN,cmp.TYPOLOGY_NOTIFICATION,cmp.RETARGETTING_COMMENTS,cmp.RETARGETTING_FACEBOOK,cmp.RETARGETTING_TWITTER, 
cmp.STATUS_CAMPAIGN,cmp.FLAG_ROI,cmp.CHANNEL,cmp.OFFER_CODES,cmp.OFFER_LABELS,cmp.FLAG_CLIENTS,cmp.FLAG_PROSPECTS,REVERSE(SUBSTRING(REVERSE(push.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(push.MESSAGE_NAME))-1)), 
cast(push.DATE_TIME_SEND as date),datepart(WEEKDAY,push.DATE_TIME_SEND),
(case datepart(WEEKDAY,push.DATE_TIME_SEND) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end),  
(case when LEN(CAST(datepart(hour,push.DATE_TIME_SEND) as nvarchar))= '1' then CONCAT('0',CAST(datepart(hour,push.DATE_TIME_SEND) as nvarchar)) else CAST(datepart(hour,push.DATE_TIME_SEND) as nvarchar) end) 

UNION

/*  PUSH - NB_BOUNCES */
SELECT  

CONCAT(cmp.ID_CAMPAIGN,REVERSE(SUBSTRING(REVERSE(push.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(push.MESSAGE_NAME))-1))) as PK_ID, 
cmp.ID_CAMPAIGN,
cmp.CAMPAIGN_NAME,
cmp.END_DATE, 
cmp.TYPOLOGY_CAMPAIGN, 
cmp.TYPOLOGY_NOTIFICATION, 
cmp.RETARGETTING_COMMENTS,
cmp.RETARGETTING_FACEBOOK, 
cmp.RETARGETTING_TWITTER, 
cmp.STATUS_CAMPAIGN, 
cmp.FLAG_ROI, 
cmp.CHANNEL, 
cmp.OFFER_CODES,
cmp.OFFER_LABELS, 
cmp.FLAG_CLIENTS, 
cmp.FLAG_PROSPECTS, 
REVERSE(SUBSTRING(REVERSE(push.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(push.MESSAGE_NAME))-1)) AS MESSAGE_NAME, 
cast(push.DATE_TIME_SEND as date) as DATE_TIME_SEND, 
datepart(WEEKDAY,push.DATE_TIME_SEND) as NUM_JOUR, 
(case datepart(WEEKDAY,push.DATE_TIME_SEND) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end) as JOUR ,  
(case when LEN(CAST(datepart(hour,push.DATE_TIME_SEND) as nvarchar))= '1' then CONCAT('0',CAST(datepart(hour,push.DATE_TIME_SEND) as nvarchar)) else CAST(datepart(hour,push.DATE_TIME_SEND) as nvarchar) end) as HEURE_ENVOI, 
NULL as DEVICE, 
NULL as SYSTEM_OPERATION,
NULL as BOUNCE_CATEGORY, 
NULL AS DOMAINE, 

'NB_BOUNCES' AS COUNT_TYPE,
count(case when push.MESSAGE_OPENED='no' then push.ID_CONTACT end) as COUNT_RESULT

  FROM [$(DataHubDatabaseName)].[dbo].[PV_MC_PUSHLOG] push 
  JOIN CAMPAGNE cmp on push.[ID_CAMPAIGN] = cmp.ID_CAMPAIGN 

WHERE cmp.CHANNEL='PUSH' 

group by 
CONCAT(cmp.ID_CAMPAIGN,REVERSE(SUBSTRING(REVERSE(push.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(push.MESSAGE_NAME))-1))),cmp.ID_CAMPAIGN,cmp.CAMPAIGN_NAME,cmp.END_DATE,cmp.TYPOLOGY_CAMPAIGN,cmp.TYPOLOGY_NOTIFICATION,cmp.RETARGETTING_COMMENTS,cmp.RETARGETTING_FACEBOOK,cmp.RETARGETTING_TWITTER, 
cmp.STATUS_CAMPAIGN,cmp.FLAG_ROI,cmp.CHANNEL,cmp.OFFER_CODES,cmp.OFFER_LABELS,cmp.FLAG_CLIENTS,cmp.FLAG_PROSPECTS,REVERSE(SUBSTRING(REVERSE(push.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(push.MESSAGE_NAME))-1)), 
cast(push.DATE_TIME_SEND as date),datepart(WEEKDAY,push.DATE_TIME_SEND),
(case datepart(WEEKDAY,push.DATE_TIME_SEND) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end),  
(case when LEN(CAST(datepart(hour,push.DATE_TIME_SEND) as nvarchar))= '1' then CONCAT('0',CAST(datepart(hour,push.DATE_TIME_SEND) as nvarchar)) else CAST(datepart(hour,push.DATE_TIME_SEND) as nvarchar) end) 

UNION

/*  PUSH - NB_DELIVRES */
SELECT  

CONCAT(cmp.ID_CAMPAIGN,REVERSE(SUBSTRING(REVERSE(push.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(push.MESSAGE_NAME))-1))) as PK_ID, 
cmp.ID_CAMPAIGN,
cmp.CAMPAIGN_NAME,
cmp.END_DATE, 
cmp.TYPOLOGY_CAMPAIGN, 
cmp.TYPOLOGY_NOTIFICATION, 
cmp.RETARGETTING_COMMENTS,
cmp.RETARGETTING_FACEBOOK, 
cmp.RETARGETTING_TWITTER, 
cmp.STATUS_CAMPAIGN, 
cmp.FLAG_ROI, 
cmp.CHANNEL, 
cmp.OFFER_CODES,
cmp.OFFER_LABELS, 
cmp.FLAG_CLIENTS, 
cmp.FLAG_PROSPECTS, 
REVERSE(SUBSTRING(REVERSE(push.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(push.MESSAGE_NAME))-1)) AS MESSAGE_NAME, 
cast(push.DATE_TIME_SEND as date) as DATE_TIME_SEND, 
datepart(WEEKDAY,push.DATE_TIME_SEND) as NUM_JOUR, 
(case datepart(WEEKDAY,push.DATE_TIME_SEND) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end) as JOUR ,  
(case when LEN(CAST(datepart(hour,push.DATE_TIME_SEND) as nvarchar))= '1' then CONCAT('0',CAST(datepart(hour,push.DATE_TIME_SEND) as nvarchar)) else CAST(datepart(hour,push.DATE_TIME_SEND) as nvarchar) end) as HEURE_ENVOI, 
NULL as DEVICE, 
NULL as SYSTEM_OPERATION,
NULL as BOUNCE_CATEGORY, 
NULL AS DOMAINE, 

'NB_DELIVRES' AS COUNT_TYPE,
count(case when push.STATUS='Success' then push.ID_CONTACT end) as COUNT_RESULT

  FROM [$(DataHubDatabaseName)].[dbo].[PV_MC_PUSHLOG] push 
  JOIN CAMPAGNE cmp on push.[ID_CAMPAIGN] = cmp.ID_CAMPAIGN 

WHERE cmp.CHANNEL='PUSH' 

group by 
CONCAT(cmp.ID_CAMPAIGN,REVERSE(SUBSTRING(REVERSE(push.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(push.MESSAGE_NAME))-1))),cmp.ID_CAMPAIGN,cmp.CAMPAIGN_NAME,cmp.END_DATE,cmp.TYPOLOGY_CAMPAIGN,cmp.TYPOLOGY_NOTIFICATION,cmp.RETARGETTING_COMMENTS,cmp.RETARGETTING_FACEBOOK,cmp.RETARGETTING_TWITTER, 
cmp.STATUS_CAMPAIGN,cmp.FLAG_ROI,cmp.CHANNEL,cmp.OFFER_CODES,cmp.OFFER_LABELS,cmp.FLAG_CLIENTS,cmp.FLAG_PROSPECTS,REVERSE(SUBSTRING(REVERSE(push.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(push.MESSAGE_NAME))-1)), 
cast(push.DATE_TIME_SEND as date),datepart(WEEKDAY,push.DATE_TIME_SEND),
(case datepart(WEEKDAY,push.DATE_TIME_SEND) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end),  
(case when LEN(CAST(datepart(hour,push.DATE_TIME_SEND) as nvarchar))= '1' then CONCAT('0',CAST(datepart(hour,push.DATE_TIME_SEND) as nvarchar)) else CAST(datepart(hour,push.DATE_TIME_SEND) as nvarchar) end) 

UNION

/*  PUSH - NB_OUVERTURES_UNIQUES */
SELECT  

CONCAT(cmp.ID_CAMPAIGN,REVERSE(SUBSTRING(REVERSE(push.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(push.MESSAGE_NAME))-1))) as PK_ID, 
cmp.ID_CAMPAIGN,
cmp.CAMPAIGN_NAME,
cmp.END_DATE, 
cmp.TYPOLOGY_CAMPAIGN, 
cmp.TYPOLOGY_NOTIFICATION, 
cmp.RETARGETTING_COMMENTS,
cmp.RETARGETTING_FACEBOOK, 
cmp.RETARGETTING_TWITTER, 
cmp.STATUS_CAMPAIGN, 
cmp.FLAG_ROI, 
cmp.CHANNEL, 
cmp.OFFER_CODES,
cmp.OFFER_LABELS, 
cmp.FLAG_CLIENTS, 
cmp.FLAG_PROSPECTS, 
REVERSE(SUBSTRING(REVERSE(push.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(push.MESSAGE_NAME))-1)) AS MESSAGE_NAME, 
cast(push.DATE_TIME_SEND as date) as DATE_TIME_SEND, 
datepart(WEEKDAY,push.DATE_TIME_SEND) as NUM_JOUR, 
(case datepart(WEEKDAY,push.DATE_TIME_SEND) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end) as JOUR ,  
(case when LEN(CAST(datepart(hour,push.DATE_TIME_SEND) as nvarchar))= '1' then CONCAT('0',CAST(datepart(hour,push.DATE_TIME_SEND) as nvarchar)) else CAST(datepart(hour,push.DATE_TIME_SEND) as nvarchar) end) as HEURE_ENVOI, 
NULL as DEVICE, 
NULL as SYSTEM_OPERATION,
NULL as BOUNCE_CATEGORY, 
NULL AS DOMAINE, 

'NB_OUVERTURES_UNIQUES' AS COUNT_TYPE,
count(distinct (case when push.MESSAGE_OPENED='yes' then push.ID_CONTACT end)) as COUNT_RESULT

  FROM [$(DataHubDatabaseName)].[dbo].[PV_MC_PUSHLOG] push 
  JOIN CAMPAGNE cmp on push.[ID_CAMPAIGN] = cmp.ID_CAMPAIGN 

WHERE cmp.CHANNEL='PUSH' 

group by 
CONCAT(cmp.ID_CAMPAIGN,REVERSE(SUBSTRING(REVERSE(push.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(push.MESSAGE_NAME))-1))),cmp.ID_CAMPAIGN,cmp.CAMPAIGN_NAME,cmp.END_DATE,cmp.TYPOLOGY_CAMPAIGN,cmp.TYPOLOGY_NOTIFICATION,cmp.RETARGETTING_COMMENTS,cmp.RETARGETTING_FACEBOOK,cmp.RETARGETTING_TWITTER, 
cmp.STATUS_CAMPAIGN,cmp.FLAG_ROI,cmp.CHANNEL,cmp.OFFER_CODES,cmp.OFFER_LABELS,cmp.FLAG_CLIENTS,cmp.FLAG_PROSPECTS,REVERSE(SUBSTRING(REVERSE(push.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(push.MESSAGE_NAME))-1)), 
cast(push.DATE_TIME_SEND as date),datepart(WEEKDAY,push.DATE_TIME_SEND),
(case datepart(WEEKDAY,push.DATE_TIME_SEND) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end),  
(case when LEN(CAST(datepart(hour,push.DATE_TIME_SEND) as nvarchar))= '1' then CONCAT('0',CAST(datepart(hour,push.DATE_TIME_SEND) as nvarchar)) else CAST(datepart(hour,push.DATE_TIME_SEND) as nvarchar) end) 

UNION

/*  CALL - NB_CONTACTS_SORTANTS */
select distinct 

apl.[ID_CAMPAIGN] as PK_ID, 
cmp.ID_CAMPAIGN,
cmp.CAMPAIGN_NAME,
cmp.END_DATE, 
cmp.TYPOLOGY_CAMPAIGN, 
cmp.TYPOLOGY_NOTIFICATION, 
cmp.RETARGETTING_COMMENTS,
cmp.RETARGETTING_FACEBOOK, 
cmp.RETARGETTING_TWITTER, 
cmp.STATUS_CAMPAIGN, 
cmp.FLAG_ROI, 
'CALL' as CHANNEL, 
cmp.OFFER_CODES, 
cmp.OFFER_LABELS, 
cmp.FLAG_CLIENTS, 
cmp.FLAG_PROSPECTS, 
NULL as MESSAGE_NAME, 
cast(apl.CALL_DATE as date) as DATE_TIME_SEND, 
datepart(WEEKDAY,apl.CALL_DATE) as NUM_JOUR, 
(case datepart(WEEKDAY,apl.CALL_DATE) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end) as JOUR , 
apl.HEURE_APPEL as HEURE_ENVOI, 
NULL as DEVICE, 
NULL as SYSTEM_OPERATION,
NULL as BOUNCE_CATEGORY, 
NULL as DOMAINE, 

'NB_CONTACTS_SORTANTS' AS COUNT_TYPE,
count(apl.ID_CONTACT) as COUNT_RESULT

from [dbo].[WK_MC_CALLLOG] apl 
LEFT JOIN CAMPAGNE_CALL cmp on cmp.ID_CAMPAIGN=apl.ID_CAMPAIGN  

group by 
apl.[ID_CAMPAIGN],cmp.ID_CAMPAIGN,cmp.CAMPAIGN_NAME,cmp.END_DATE,cmp.TYPOLOGY_CAMPAIGN,cmp.TYPOLOGY_NOTIFICATION,cmp.RETARGETTING_COMMENTS,cmp.RETARGETTING_FACEBOOK,cmp.RETARGETTING_TWITTER,cmp.STATUS_CAMPAIGN, 
cmp.FLAG_ROI,cmp.OFFER_CODES,cmp.OFFER_LABELS,cmp.FLAG_CLIENTS,cmp.FLAG_PROSPECTS,cast(apl.CALL_DATE as date),datepart(WEEKDAY,apl.CALL_DATE),
(case datepart(WEEKDAY,apl.CALL_DATE) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end),apl.HEURE_APPEL

UNION

/*  CALL - NB_DELIVRES */
select distinct 

apl.[ID_CAMPAIGN] as PK_ID, 
cmp.ID_CAMPAIGN,
cmp.CAMPAIGN_NAME,
cmp.END_DATE, 
cmp.TYPOLOGY_CAMPAIGN, 
cmp.TYPOLOGY_NOTIFICATION, 
cmp.RETARGETTING_COMMENTS,
cmp.RETARGETTING_FACEBOOK, 
cmp.RETARGETTING_TWITTER, 
cmp.STATUS_CAMPAIGN, 
cmp.FLAG_ROI, 
'CALL' as CHANNEL, 
cmp.OFFER_CODES, 
cmp.OFFER_LABELS, 
cmp.FLAG_CLIENTS, 
cmp.FLAG_PROSPECTS, 
NULL as MESSAGE_NAME, 
cast(apl.CALL_DATE as date) as DATE_TIME_SEND, 
datepart(WEEKDAY,apl.CALL_DATE) as NUM_JOUR, 
(case datepart(WEEKDAY,apl.CALL_DATE) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end) as JOUR , 
apl.HEURE_APPEL as HEURE_ENVOI, 
NULL as DEVICE, 
NULL as SYSTEM_OPERATION,
NULL as BOUNCE_CATEGORY, 
NULL as DOMAINE, 

'NB_DELIVRES' AS COUNT_TYPE,
count(case when apl.FLG_CONTACTE ='Y' then apl.ID_CONTACT end) as COUNT_RESULT

from [dbo].[WK_MC_CALLLOG] apl 
LEFT JOIN CAMPAGNE_CALL cmp on cmp.ID_CAMPAIGN=apl.ID_CAMPAIGN  

group by 
apl.[ID_CAMPAIGN],cmp.ID_CAMPAIGN,cmp.CAMPAIGN_NAME,cmp.END_DATE,cmp.TYPOLOGY_CAMPAIGN,cmp.TYPOLOGY_NOTIFICATION,cmp.RETARGETTING_COMMENTS,cmp.RETARGETTING_FACEBOOK,cmp.RETARGETTING_TWITTER,cmp.STATUS_CAMPAIGN, 
cmp.FLAG_ROI,cmp.OFFER_CODES,cmp.OFFER_LABELS,cmp.FLAG_CLIENTS,cmp.FLAG_PROSPECTS,cast(apl.CALL_DATE as date),datepart(WEEKDAY,apl.CALL_DATE),
(case datepart(WEEKDAY,apl.CALL_DATE) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end),apl.HEURE_APPEL

UNION

/*  CALL - NB_BOUNCES */
select distinct 

apl.[ID_CAMPAIGN] as PK_ID, 
cmp.ID_CAMPAIGN,
cmp.CAMPAIGN_NAME,
cmp.END_DATE, 
cmp.TYPOLOGY_CAMPAIGN, 
cmp.TYPOLOGY_NOTIFICATION, 
cmp.RETARGETTING_COMMENTS,
cmp.RETARGETTING_FACEBOOK, 
cmp.RETARGETTING_TWITTER, 
cmp.STATUS_CAMPAIGN, 
cmp.FLAG_ROI, 
'CALL' as CHANNEL, 
cmp.OFFER_CODES, 
cmp.OFFER_LABELS, 
cmp.FLAG_CLIENTS, 
cmp.FLAG_PROSPECTS, 
NULL as MESSAGE_NAME, 
cast(apl.CALL_DATE as date) as DATE_TIME_SEND, 
datepart(WEEKDAY,apl.CALL_DATE) as NUM_JOUR, 
(case datepart(WEEKDAY,apl.CALL_DATE) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end) as JOUR , 
apl.HEURE_APPEL as HEURE_ENVOI, 
NULL as DEVICE, 
NULL as SYSTEM_OPERATION,
NULL as BOUNCE_CATEGORY, 
NULL as DOMAINE, 

'NB_BOUNCES' AS COUNT_TYPE,
count(case when apl.FLG_CONTACT_BOUNCES ='Y' then apl.ID_CONTACT end) as COUNT_RESULT

from [dbo].[WK_MC_CALLLOG] apl 
LEFT JOIN CAMPAGNE_CALL cmp on cmp.ID_CAMPAIGN=apl.ID_CAMPAIGN  

group by 
apl.[ID_CAMPAIGN],cmp.ID_CAMPAIGN,cmp.CAMPAIGN_NAME,cmp.END_DATE,cmp.TYPOLOGY_CAMPAIGN,cmp.TYPOLOGY_NOTIFICATION,cmp.RETARGETTING_COMMENTS,cmp.RETARGETTING_FACEBOOK,cmp.RETARGETTING_TWITTER,cmp.STATUS_CAMPAIGN, 
cmp.FLAG_ROI,cmp.OFFER_CODES,cmp.OFFER_LABELS,cmp.FLAG_CLIENTS,cmp.FLAG_PROSPECTS,cast(apl.CALL_DATE as date),datepart(WEEKDAY,apl.CALL_DATE),
(case datepart(WEEKDAY,apl.CALL_DATE) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end),apl.HEURE_APPEL

UNION

/*  CALL - NB_DESABONNEMENTS */
select distinct 

apl.[ID_CAMPAIGN] as PK_ID, 
cmp.ID_CAMPAIGN,
cmp.CAMPAIGN_NAME,
cmp.END_DATE, 
cmp.TYPOLOGY_CAMPAIGN, 
cmp.TYPOLOGY_NOTIFICATION, 
cmp.RETARGETTING_COMMENTS,
cmp.RETARGETTING_FACEBOOK, 
cmp.RETARGETTING_TWITTER, 
cmp.STATUS_CAMPAIGN, 
cmp.FLAG_ROI, 
'CALL' as CHANNEL, 
cmp.OFFER_CODES, 
cmp.OFFER_LABELS, 
cmp.FLAG_CLIENTS, 
cmp.FLAG_PROSPECTS, 
NULL as MESSAGE_NAME, 
cast(apl.CALL_DATE as date) as DATE_TIME_SEND, 
datepart(WEEKDAY,apl.CALL_DATE) as NUM_JOUR, 
(case datepart(WEEKDAY,apl.CALL_DATE) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end) as JOUR , 
apl.HEURE_APPEL as HEURE_ENVOI, 
NULL as DEVICE, 
NULL as SYSTEM_OPERATION,
NULL as BOUNCE_CATEGORY, 
NULL as DOMAINE, 

'NB_DESABONNEMENTS' AS COUNT_TYPE,
count(case when apl.FLG_CONTACT_STOP ='Y' then apl.ID_CONTACT end)  as COUNT_RESULT

from [dbo].[WK_MC_CALLLOG] apl 
LEFT JOIN CAMPAGNE_CALL cmp on cmp.ID_CAMPAIGN=apl.ID_CAMPAIGN  

group by 
apl.[ID_CAMPAIGN],cmp.ID_CAMPAIGN,cmp.CAMPAIGN_NAME,cmp.END_DATE,cmp.TYPOLOGY_CAMPAIGN,cmp.TYPOLOGY_NOTIFICATION,cmp.RETARGETTING_COMMENTS,cmp.RETARGETTING_FACEBOOK,cmp.RETARGETTING_TWITTER,cmp.STATUS_CAMPAIGN, 
cmp.FLAG_ROI,cmp.OFFER_CODES,cmp.OFFER_LABELS,cmp.FLAG_CLIENTS,cmp.FLAG_PROSPECTS,cast(apl.CALL_DATE as date),datepart(WEEKDAY,apl.CALL_DATE),
(case datepart(WEEKDAY,apl.CALL_DATE) when 1 then 'Dimanche' when 2 then 'Lundi' when 3 then 'Mardi' when 4 then 'Mercredi' when 5 then 'Jeudi' when 6 then 'Vendredi' when 7 then 'Samedi' end),apl.HEURE_APPEL
	select @nb_collected_rows = COUNT(*) from [dbo].[SAS_Vue_MC_Contacts_M];
END