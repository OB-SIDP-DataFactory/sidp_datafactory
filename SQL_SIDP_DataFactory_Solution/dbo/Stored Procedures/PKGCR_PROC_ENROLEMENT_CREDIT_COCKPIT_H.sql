﻿
CREATE PROCEDURE [dbo].[PKGCR_PROC_ENROLEMENT_CREDIT_COCKPIT_H]
@Date_obs date

AS BEGIN

DECLARE @weekobs int;
DECLARE @Anneeweekobs int ;
DECLARE @weekPrecobs int;
DECLARE @AnneeweekPrecobs int ;

SET @Date_obs = CAST(@Date_obs as date);
-- Num de la Semaine en cours et année de la semaine en cours
SET @weekobs = (select IsoWeek from [dbo].[DIM_TEMPS] where CAST(Date as date) = @Date_obs );
SET @Anneeweekobs = (select DATEPART(YEAR,@Date_obs));

-- Num de la Semaine précédente et année de la semaine précédente
SET @weekPrecobs = (select IsoWeek from [dbo].[DIM_TEMPS] where CAST(Date as date) = DATEADD(week,-1,@Date_obs));
SET @AnneeweekPrecobs = DATEPART(YEAR,DATEADD(week,-1,@Date_obs)) ;

SET DATEFIRST 1; -->1    Lundi

With 
cte_date_obs AS
( SELECT @Date_obs AS StandardDate
       , DATEADD(day, (-1 * DATEPART(dw, @Date_obs)) + 1, @Date_obs) AS FirstDOW
       , DATEADD(day, (7 - DATEPART(dw, @Date_obs)), @Date_obs) AS LastDOW
       , CAST(DATEADD(mm  , DATEDIFF(mm  , 0, @Date_obs), 0) AS DATE) AS FirstDOM
       , CAST(DATEADD(yyyy, DATEDIFF(yyyy, 0, @Date_obs), 0) AS DATE) AS FirstDOY
       , CAST(DATEADD(mm  , DATEDIFF(mm  , 0, DATEADD(mm  , -1, @Date_obs)), 0) AS DATE) AS FirstDOPM
       , CAST(DATEADD(yyyy, DATEDIFF(YYYY, 0, DATEADD(yyyy, -1, @Date_obs)), 0) AS DATE) AS FirstDOPY
       , CONVERT(DATE, '20151220', 112) AS StartDate ),

cte_jour_alim_list AS
( SELECT T1.StandardDate AS jour_alim
    FROM [dbo].[DIM_TEMPS] T1
    JOIN cte_date_obs T2
    ON ( T1.StandardDate >= T2.FirstDOW AND T1.StandardDate <= T2.LastDOW )
),
cte_Cre_Opp_ALL as (
SELECT [IDE_OPPRT_SF]
	  ,[STADE_VENTE]
	  ,LIB_STADE_VENTE
	  ,Cast([DTE_CREA] as date) [DTE_CREA]
	  ,cast([DTE_CLO_OPPRT] as date ) as [DTE_CLO_OPPRT]
	  ,CAST([DTE_DEBLOCAGE] as date) as [DTE_DEBLOCAGE]
	  ,CAST(DTE_ACCORD as date) as [DTE_ACC_CRE]
	  ,[DELAI_ACCORD]
	  ,[DELAI_DEBLOCAGE]
	  ,AGE_OPPRT
	  ,DATEPART(ISO_WEEK,[DTE_CREA]) as [WeekOfYear]
	  ,[COD_RES_DIS]
	  ,[COD_CAN_INT_ORI]
	  ,[DTE_ALIM]
FROM [dbo].[CRE_OPPORTUNITE] Opp
where DTE_ALIM = (select MAX(DTE_ALIM) from CRE_OPPORTUNITE)and [FLG_DER_ETAT_OPPRT]=1)
--select * from  cte_Cre_Opp_ALL ;

/*Les jours de la semaine*/
--Opport credit creees
,Cte_Opp_Cre_Creees_Cumul_Jour_cnt as (
SELECT [DTE_CREA],
       [WeekOfYear],
	   COUNT ([IDE_OPPRT_SF]) as NB_OPP_CRE_CREEES,
       case when COD_RES_DIS = '01' then COUNT ([IDE_OPPRT_SF])  end as NB_OF,
	   case when COD_RES_DIS = '02' then COUNT ([IDE_OPPRT_SF])  end as NB_OB,
	   case when [COD_CAN_INT_ORI] IN ('10','11') then COUNT ([IDE_OPPRT_SF])  end as NB_Digital,
	   case when [COD_CAN_INT_ORI] IN ('12','13','14','15') then COUNT ([IDE_OPPRT_SF])  end as NB_CRC
FROM cte_Cre_Opp_ALL
WHERE  [WeekOfYear] = @weekobs and DATEPART(YEAR,[DTE_CREA]) = @Anneeweekobs
GROUP BY [DTE_CREA],[WeekOfYear],COD_RES_DIS,[COD_CAN_INT_ORI]
)
,Cte_Opp_Cre_Creees_Cumul_Jour as(
SELECT [DTE_CREA],[WeekOfYear],
	   SUM(NB_OPP_CRE_CREEES) as NB_OPP_CRE_CREEES,
       SUM(NB_OF)as NB_OF ,
	   SUM(NB_OB) as NB_OB,
	   SUM(NB_Digital) as NB_Digital,
	   SUM(NB_CRC) as NB_CRC 
FROM Cte_Opp_Cre_Creees_Cumul_Jour_cnt
GROUP BY [DTE_CREA],[WeekOfYear]
)

--Opport crédit en cours de vie
,Cte_Opp_Cre_En_cours_De_Vie_Cumul_Jour_cnt as (
SELECT [DTE_CREA],
	   WeekOfYear,
       COUNT ([IDE_OPPRT_SF])                                                          as NB_OPP_CRE_EN_COURS,
       case when STADE_VENTE = '01' then COUNT ([IDE_OPPRT_SF]) end					   as NB_DETECTION,
	   case when STADE_VENTE IN ('02','03') then COUNT ([IDE_OPPRT_SF])  end           as NB_AVANT_SIGNATURE,
	   case when STADE_VENTE IN ('04','05','06','07') then COUNT ([IDE_OPPRT_SF])  end as NB_APRES_SIGNATURE,
	   SUM(AGE_OPPRT)																   as ANCIENNETE
FROM cte_Cre_Opp_ALL
WHERE  [WeekOfYear] = @weekobs and DATEPART(YEAR,[DTE_CREA]) = @Anneeweekobs
       AND STADE_VENTE IN ('01','02','03','04','05','06','07')
GROUP BY [DTE_CREA],STADE_VENTE,WeekOfYear
)
,Cte_Opp_Cre_En_cours_De_Vie_Cumul_Jour as (
SELECT [DTE_CREA],[WeekOfYear],
	   SUM(NB_OPP_CRE_EN_COURS)    as NB_OPP_CRE_EN_COURS,
       SUM(NB_DETECTION)           as NB_DETECTION ,
	   SUM(NB_AVANT_SIGNATURE)     as NB_AVANT_SIGNATURE,
	   SUM(NB_APRES_SIGNATURE)     as NB_APRES_SIGNATURE,
	   SUM(ANCIENNETE)             as ANCIENNETE
FROM Cte_Opp_Cre_En_cours_De_Vie_Cumul_Jour_cnt
GROUP BY [DTE_CREA],[WeekOfYear]
)
--Opport credit gnagnees
,Cte_Opp_Cre_Gagnees_Cumul_Jour_Cnt as (
SELECT [DTE_CREA],
       [WeekOfYear],
       COUNT ([IDE_OPPRT_SF])                                        as NB_OPP_CRE_GAGNEES,
       case when COD_RES_DIS = '01' then COUNT ([IDE_OPPRT_SF])  end as NB_OF,
	   case when COD_RES_DIS = '02' then COUNT ([IDE_OPPRT_SF])  end as NB_OB,
	   case when STADE_VENTE = '14' then COUNT ([IDE_OPPRT_SF])  end as NB_CREDIT_ACCORDE,
	   case when STADE_VENTE = '15' then COUNT ([IDE_OPPRT_SF])  end as NB_CREDIT_DECAISSE,
	   SUM([DELAI_ACCORD])											as DELAI_ACCORD_PRET,
	   SUM([DELAI_DEBLOCAGE])								        as DELAI_DECAISSEMENT
FROM cte_Cre_Opp_ALL
WHERE  [WeekOfYear] = @weekobs and DATEPART(YEAR,[DTE_CREA]) = @Anneeweekobs
       AND STADE_VENTE IN ('14','15')
GROUP BY [DTE_CREA],[WeekOfYear],COD_RES_DIS,STADE_VENTE
)
,Cte_Opp_Cre_Gagnees_Cumul_Jour as (
SELECT [DTE_CREA],[WeekOfYear],
	   SUM(NB_OPP_CRE_GAGNEES)     as NB_OPP_CRE_GAGNEES,
       SUM(NB_OF)                  as NB_OF ,
	   SUM(NB_OB)                  as NB_OB,
	   SUM(NB_CREDIT_ACCORDE)      as NB_CREDIT_ACCORDE,
	   SUM(NB_CREDIT_DECAISSE)     as NB_CREDIT_DECAISSE,
	   SUM(DELAI_ACCORD_PRET)      as DELAI_ACCORD_PRET,
	   SUM(DELAI_DECAISSEMENT)     as DELAI_DECAISSEMENT
FROM Cte_Opp_Cre_Gagnees_Cumul_Jour_Cnt
GROUP BY [DTE_CREA],[WeekOfYear]
)

--Opport credit perdues
,Cte_Opp_Cre_Perdues_Cumul_Jour_Cnt as (
SELECT [DTE_CREA],
	   [WeekOfYear],
       COUNT ([IDE_OPPRT_SF]) as NB_OPP_CRE_PERDUES,
       case when STADE_VENTE = '16' then COUNT ([IDE_OPPRT_SF])  end as NB_ANNULATION,
	   case when STADE_VENTE = '16' then COUNT ([IDE_OPPRT_SF])  end as NB_ANNULATION_CLIENT,
	   case when STADE_VENTE = '12' then COUNT ([IDE_OPPRT_SF])  end as NB_RETRACTATION,
	   case when STADE_VENTE = '10' then COUNT ([IDE_OPPRT_SF])  end as NB_AFFAIRE_REFUSEE,
	   case when STADE_VENTE = '08' then COUNT ([IDE_OPPRT_SF])  end as NB_INCIDENT_TECH,
	   case when STADE_VENTE = '11' then COUNT ([IDE_OPPRT_SF])  end as NB_SANS_SUITE
FROM cte_Cre_Opp_ALL
WHERE  [WeekOfYear] = @weekobs and DATEPART(YEAR,[DTE_CREA]) = @Anneeweekobs
       AND STADE_VENTE IN ('16','12','10','08','11')
GROUP BY [DTE_CREA],[WeekOfYear],STADE_VENTE
)
,Cte_Opp_Cre_Perdues_Cumul_Jour as (
SELECT [DTE_CREA],[WeekOfYear],
	   SUM(NB_OPP_CRE_PERDUES)     as NB_OPP_CRE_PERDUES,
       SUM(NB_ANNULATION)          as NB_ANNULATION ,
	   SUM(NB_ANNULATION_CLIENT)   as NB_ANNULATION_CLIENT,
	   SUM(NB_RETRACTATION)        as NB_RETRACTATION,
	   SUM(NB_AFFAIRE_REFUSEE)     as NB_AFFAIRE_REFUSEE,
	   SUM(NB_INCIDENT_TECH)       as NB_INCIDENT_TECH,
	   SUM(NB_SANS_SUITE)          as NB_SANS_SUITE 
FROM Cte_Opp_Cre_Perdues_Cumul_Jour_Cnt
GROUP BY [DTE_CREA],[WeekOfYear]
)

/*Cumul Hebdo de la semaine en cours*/
--Opport credit creees
,Cte_Opp_Cre_Creees_Cumul_Hebdo as (
SELECT [WeekOfYear],
	   SUM(NB_OPP_CRE_CREEES) as NB_OPP_CRE_CREEES_HEBDO,
       SUM(NB_OF)as NB_OF_HEBDO ,
	   SUM(NB_OB) as NB_OB_HEBDO,
	   SUM(NB_Digital) as NB_Digital_HEBDO,
	   SUM(NB_CRC) as NB_CRC_HEBDO 
FROM Cte_Opp_Cre_Creees_Cumul_Jour
GROUP BY [WeekOfYear]
)
--Opport crédit en cours de vie
,Cte_Opp_Cre_En_cours_De_Vie_Cumul_Hebdo as (
SELECT [WeekOfYear],
	   SUM(NB_OPP_CRE_EN_COURS)    as NB_OPP_CRE_EN_COURS_HEBDO,
       SUM(NB_DETECTION)           as NB_DETECTION_HEBDO ,
	   SUM(NB_AVANT_SIGNATURE)     as NB_AVANT_SIGNATURE_HEBDO,
	   SUM(NB_APRES_SIGNATURE)     as NB_APRES_SIGNATURE_HEBDO,
	   SUM(ANCIENNETE)             as ANCIENNETE_HEBDO
FROM Cte_Opp_Cre_En_cours_De_Vie_Cumul_Jour
GROUP BY [WeekOfYear]
)
--Opport credit gnagnees
,Cte_Opp_Cre_Gagnees_Cumul_Hebdo as (
SELECT [WeekOfYear],
	   SUM(NB_OPP_CRE_GAGNEES)     as NB_OPP_CRE_GAGNEES_HEBDO,
       SUM(NB_OF)                  as NB_OF_HEBDO ,
	   SUM(NB_OB)                  as NB_OB_HEBDO,
	   SUM(NB_CREDIT_ACCORDE)      as NB_CREDIT_ACCORDE_HEBDO,
	   SUM(NB_CREDIT_DECAISSE)     as NB_CREDIT_DECAISSE_HEBDO,
	   SUM(DELAI_ACCORD_PRET)      as DELAI_ACCORD_PRET_HEBDO,
	   SUM(DELAI_DECAISSEMENT)     as DELAI_DECAISSEMENT_HEBDO
FROM Cte_Opp_Cre_Gagnees_Cumul_Jour
GROUP BY [WeekOfYear]
)
--Opport credit perdues
,Cte_Opp_Cre_Perdues_Cumul_Hebdo as (
SELECT [WeekOfYear],
	   SUM(NB_OPP_CRE_PERDUES)     as NB_OPP_CRE_PERDUES_HEBDO,
       SUM(NB_ANNULATION)          as NB_ANNULATION_HEBDO ,
	   SUM(NB_ANNULATION_CLIENT)   as NB_ANNULATION_CLIENT_HEBDO,
	   SUM(NB_RETRACTATION)        as NB_RETRACTATION_HEBDO,
	   SUM(NB_AFFAIRE_REFUSEE)     as NB_AFFAIRE_REFUSEE_HEBDO,
	   SUM(NB_INCIDENT_TECH)       as NB_INCIDENT_TECH_HEBDO,
	   SUM(NB_SANS_SUITE)          as NB_SANS_SUITE_HEBDO 
FROM Cte_Opp_Cre_Perdues_Cumul_Jour
GROUP BY [WeekOfYear]
)

/*Les jours de la semaine précédente*/
--Opport credit creees
,Cte_Opp_Cre_Creees_Cumul_Jour_Semaine_Prec_Cnt as (
SELECT [DTE_CREA],
       [WeekOfYear],
	   COUNT ([IDE_OPPRT_SF]) as NB_OPP_CRE_CREEES,
       case when COD_RES_DIS = '01' then COUNT ([IDE_OPPRT_SF])  end as NB_OF,
	   case when COD_RES_DIS = '02' then COUNT ([IDE_OPPRT_SF])  end as NB_OB,
	   case when [COD_CAN_INT_ORI] IN ('10','11') then COUNT ([IDE_OPPRT_SF])  end as NB_Digital,
	   case when [COD_CAN_INT_ORI] IN ('12','13','14','15') then COUNT ([IDE_OPPRT_SF])  end as NB_CRC
FROM cte_Cre_Opp_ALL
WHERE  [WeekOfYear] =  @weekPrecobs   and DATEPART(YEAR,[DTE_CREA])=@AnneeweekPrecobs
GROUP BY [DTE_CREA],[WeekOfYear],COD_RES_DIS,[COD_CAN_INT_ORI]
)
--select * from Cte_Opp_Cre_Creees_Cumul_Jour_Semaine_Prec_Cnt;
,Cte_Opp_Cre_Creees_Cumul_Jour_Semaine_Prec as (
SELECT [DTE_CREA],[WeekOfYear],	  
	   SUM(NB_OPP_CRE_CREEES) as NB_OPP_CRE_CREEES,
       SUM(NB_OF)as NB_OF ,
	   SUM(NB_OB) as NB_OB,
	   SUM(NB_Digital) as NB_Digital,
	   SUM(NB_CRC) as NB_CRC 
FROM Cte_Opp_Cre_Creees_Cumul_Jour_Semaine_Prec_Cnt
GROUP BY [DTE_CREA],[WeekOfYear]
)
--Opport crédit en cours de vie
,Cte_Opp_Cre_En_cours_De_Vie_Cumul_Jour_Semaine_Prec_Cnt as (
SELECT [DTE_CREA],
	   WeekOfYear,
       COUNT ([IDE_OPPRT_SF]) as NB_OPP_CRE_EN_COURS,
       case when STADE_VENTE = '01' then COUNT ([IDE_OPPRT_SF])  end as NB_DETECTION,
	   case when STADE_VENTE IN ('02','03') then COUNT ([IDE_OPPRT_SF])  end as NB_AVANT_SIGNATURE,
	   case when STADE_VENTE IN ('04','05','06','07') then COUNT ([IDE_OPPRT_SF])  end as NB_APRES_SIGNATURE,
	   /*DATEDIFF(DD,[DTE_CREA],GETDATE())*/ SUM(AGE_OPPRT) as ANCIENNETE
FROM cte_Cre_Opp_ALL
WHERE  [WeekOfYear] =  @weekPrecobs   and DATEPART(YEAR,[DTE_CREA])=@AnneeweekPrecobs
       AND STADE_VENTE IN ('01','02','03','04','05','06','07')
GROUP BY [DTE_CREA],STADE_VENTE,WeekOfYear
)
,Cte_Opp_Cre_En_cours_De_Vie_Cumul_Jour_Semaine_Prec as (
SELECT [DTE_CREA],[WeekOfYear],
	   SUM(NB_OPP_CRE_EN_COURS)    as NB_OPP_CRE_EN_COURS,
       SUM(NB_DETECTION)           as NB_DETECTION,
	   SUM(NB_AVANT_SIGNATURE)     as NB_AVANT_SIGNATURE,
	   SUM(NB_APRES_SIGNATURE)     as NB_APRES_SIGNATURE,
	   SUM(ANCIENNETE)             as ANCIENNETE
FROM Cte_Opp_Cre_En_cours_De_Vie_Cumul_Jour_Semaine_Prec_Cnt
GROUP BY [DTE_CREA],[WeekOfYear]
)
--Opport credit gnagnees
,Cte_Opp_Cre_Gagnees_Cumul_Jour_Semaine_Prec_Cnt as (
SELECT [DTE_CREA],
       [WeekOfYear],
       COUNT ([IDE_OPPRT_SF]) as NB_OPP_CRE_GAGNEES,
       case when COD_RES_DIS = '01' then COUNT ([IDE_OPPRT_SF])  end as NB_OF,
	   case when COD_RES_DIS = '02' then COUNT ([IDE_OPPRT_SF])  end as NB_OB,
	   case when STADE_VENTE = '14' then COUNT ([IDE_OPPRT_SF])  end as NB_CREDIT_ACCORDE,
	   case when STADE_VENTE = '15' then COUNT ([IDE_OPPRT_SF])  end as NB_CREDIT_DECAISSE,
	   SUM([DELAI_ACCORD])											as DELAI_ACCORD_PRET,
	   SUM([DELAI_DEBLOCAGE])											as DELAI_DECAISSEMENT
FROM cte_Cre_Opp_ALL
WHERE  [WeekOfYear] =  @weekPrecobs   and DATEPART(YEAR,[DTE_CREA])=@AnneeweekPrecobs
       AND STADE_VENTE IN ('14','15')
GROUP BY [DTE_CREA],[WeekOfYear],COD_RES_DIS,STADE_VENTE
)
,Cte_Opp_Cre_Gagnees_Cumul_Jour_Semaine_Prec as (
SELECT [DTE_CREA],[WeekOfYear],
	   SUM(NB_OPP_CRE_GAGNEES)     as NB_OPP_CRE_GAGNEES,
       SUM(NB_OF)                  as NB_OF ,
	   SUM(NB_OB)                  as NB_OB,
	   SUM(NB_CREDIT_ACCORDE)      as NB_CREDIT_ACCORDE,
	   SUM(NB_CREDIT_DECAISSE)     as NB_CREDIT_DECAISSE,
	   SUM(DELAI_ACCORD_PRET)      as DELAI_ACCORD_PRET,
	   SUM(DELAI_DECAISSEMENT)     as DELAI_DECAISSEMENT
FROM Cte_Opp_Cre_Gagnees_Cumul_Jour_Semaine_Prec_Cnt
GROUP BY [DTE_CREA],[WeekOfYear]
)
--Opport credit perdues
,Cte_Opp_Cre_Perdues_Cumul_Jour_Semaine_Prec_Cnt as (
SELECT [DTE_CREA],
	   [WeekOfYear],
       COUNT ([IDE_OPPRT_SF]) as NB_OPP_CRE_PERDUES,
       case when STADE_VENTE = '16' then COUNT ([IDE_OPPRT_SF])  end as NB_ANNULATION,
	   case when STADE_VENTE = '16' then COUNT ([IDE_OPPRT_SF])  end as NB_ANNULATION_CLIENT,
	   case when STADE_VENTE = '12' then COUNT ([IDE_OPPRT_SF])  end as NB_RETRACTATION,
	   case when STADE_VENTE = '10' then COUNT ([IDE_OPPRT_SF])  end as NB_AFFAIRE_REFUSEE,
	   case when STADE_VENTE = '08' then COUNT ([IDE_OPPRT_SF])  end as NB_INCIDENT_TECH,
	   case when STADE_VENTE = '11' then COUNT ([IDE_OPPRT_SF])  end as NB_SANS_SUITE
FROM cte_Cre_Opp_ALL
WHERE  [WeekOfYear] =  @weekPrecobs   and DATEPART(YEAR,[DTE_CREA])=@AnneeweekPrecobs
       AND STADE_VENTE IN ('16','12','10','08','11')
GROUP BY [DTE_CREA],[WeekOfYear],STADE_VENTE
)
,Cte_Opp_Cre_Perdues_Cumul_Jour_Semaine_Prec as (
SELECT [DTE_CREA],[WeekOfYear],
	   SUM(NB_OPP_CRE_PERDUES)     as NB_OPP_CRE_PERDUES,
       SUM(NB_ANNULATION)          as NB_ANNULATION ,
	   SUM(NB_ANNULATION_CLIENT)   as NB_ANNULATION_CLIENT,
	   SUM(NB_RETRACTATION)        as NB_RETRACTATION,
	   SUM(NB_AFFAIRE_REFUSEE)     as NB_AFFAIRE_REFUSEE,
	   SUM(NB_INCIDENT_TECH)       as NB_INCIDENT_TECH,
	   SUM(NB_SANS_SUITE)          as NB_SANS_SUITE 
FROM Cte_Opp_Cre_Perdues_Cumul_Jour_Semaine_Prec_Cnt
GROUP BY [DTE_CREA],[WeekOfYear] 
)

/*Cumul Hebdo de la semaine précédente*/
--Opport credit creees
,Cte_Opp_Cre_Creees_Cumul_Hebdo_Prec as (
SELECT [WeekOfYear],	   @weekobs as [WeekOfYearJoin] ,
	   SUM(NB_OPP_CRE_CREEES) as NB_OPP_CRE_CREEES_HEBDO_PREC,
       SUM(NB_OF)as NB_OF_HEBDO_PREC ,
	   SUM(NB_OB) as NB_OB_HEBDO_PREC,
	   SUM(NB_Digital) as NB_Digital_HEBDO_PREC,
	   SUM(NB_CRC) as NB_CRC_HEBDO_PREC 
FROM Cte_Opp_Cre_Creees_Cumul_Jour_Semaine_Prec
GROUP BY [WeekOfYear]
)
--Opport crédit en cours de vie
,Cte_Opp_Cre_En_cours_De_Vie_Cumul_Hebdo_Prec as (
SELECT [WeekOfYear],	   @weekobs as [WeekOfYearJoin] ,
	   SUM(NB_OPP_CRE_EN_COURS)    as NB_OPP_CRE_EN_COURS_HEBDO_PREC,
       SUM(NB_DETECTION)           as NB_DETECTION_HEBDO_PREC,
	   SUM(NB_AVANT_SIGNATURE)     as NB_AVANT_SIGNATURE_HEBDO_PREC,
	   SUM(NB_APRES_SIGNATURE)     as NB_APRES_SIGNATURE_HEBDO_PREC,
	   SUM(ANCIENNETE)             as ANCIENNETE_HEBDO_PREC
FROM Cte_Opp_Cre_En_cours_De_Vie_Cumul_Jour_Semaine_Prec
GROUP BY [WeekOfYear]
)
--Opport credit gnagnees
,Cte_Opp_Cre_Gagnees_Cumul_Hebdo_Prec as (
SELECT [WeekOfYear],	   @weekobs as [WeekOfYearJoin] ,
	   SUM(NB_OPP_CRE_GAGNEES)     as NB_OPP_CRE_GAGNEES_HEBDO_PREC,
       SUM(NB_OF)                  as NB_OF_HEBDO_PREC ,
	   SUM(NB_OB)                  as NB_OB_HEBDO_PREC,
	   SUM(NB_CREDIT_ACCORDE)      as NB_CREDIT_ACCORDE_HEBDO_PREC,
	   SUM(NB_CREDIT_DECAISSE)     as NB_CREDIT_DECAISSE_HEBDO_PREC,
	   SUM(DELAI_ACCORD_PRET)      as DELAI_ACCORD_PRET_HEBDO_PREC,
	   SUM(DELAI_DECAISSEMENT)     as DELAI_DECAISSEMENT_HEBDO_PREC
FROM Cte_Opp_Cre_Gagnees_Cumul_Jour_Semaine_Prec
GROUP BY [WeekOfYear]
)
--Opport credit perdues
,Cte_Opp_Cre_Perdues_Cumul_Hebdo_Prec as (
SELECT [WeekOfYear],	   @weekobs as [WeekOfYearJoin] ,
	   SUM(NB_OPP_CRE_PERDUES)     as NB_OPP_CRE_PERDUES_HEBDO_PREC,
       SUM(NB_ANNULATION)          as NB_ANNULATION_HEBDO_PREC ,
	   SUM(NB_ANNULATION_CLIENT)   as NB_ANNULATION_CLIENT_HEBDO_PREC,
	   SUM(NB_RETRACTATION)        as NB_RETRACTATION_HEBDO_PREC,
	   SUM(NB_AFFAIRE_REFUSEE)     as NB_AFFAIRE_REFUSEE_HEBDO_PREC,
	   SUM(NB_INCIDENT_TECH)       as NB_INCIDENT_TECH_HEBDO_PREC,
	   SUM(NB_SANS_SUITE)          as NB_SANS_SUITE_HEBDO_PREC 
FROM Cte_Opp_Cre_Perdues_Cumul_Jour_Semaine_Prec
GROUP BY [WeekOfYear] 
)

/******************_*_Requête_*_******************/
--Opport credit creees
select x.jour_alim,
a.DTE_CREA,isnull(a.WeekOfYear,@weekobs) WeekOfYear,
ISNULL(a.NB_OPP_CRE_CREEES,0) as NB_OPP_CRE_CREEES, 
ISNULL(a.NB_OF,0) as NB_OF_1,
ISNULL(a.NB_OB,0) as NB_OB_1,
ISNULL(a.NB_Digital,0) as NB_Digital,
ISNULL(a.NB_CRC,0) as NB_CRC,
ISNULL(b.NB_OPP_CRE_CREEES_HEBDO,0)           as NB_OPP_CRE_CREEES_HEBDO,
ISNULL(b.NB_OF_HEBDO,0)                       as NB_OF_1_HEBDO,
ISNULL(b.NB_OB_HEBDO,0)                       as NB_OB_1_HEBDO,
ISNULL(b.NB_Digital_HEBDO,0)                  as NB_Digital_HEBDO,
ISNULL(b.NB_CRC_HEBDO,0)                      as NB_CRC_HEBDO,
ISNULL(c.NB_OPP_CRE_CREEES_HEBDO_PREC,0)      as NB_OPP_CRE_CREEES_HEBDO_PREC,
ISNULL(c.NB_OF_HEBDO_PREC,0)                  as NB_OF_1_HEBDO_PREC,
ISNULL(c.NB_OB_HEBDO_PREC,0)                  as NB_OB_1_HEBDO_PREC,
ISNULL(c.NB_Digital_HEBDO_PREC,0)             as NB_Digital_HEBDO_PREC,
ISNULL(c.NB_CRC_HEBDO_PREC,0)                 as NB_CRC_HEBDO_PREC,
--Opport crédit en cours de vie
ISNULL(v.NB_OPP_CRE_EN_COURS,0)               as NB_OPP_CRE_EN_COURS, 
ISNULL(v.NB_DETECTION,0)                      as NB_DETECTION,
ISNULL(v.NB_AVANT_SIGNATURE,0)                as NB_AVANT_SIGNATURE,
ISNULL(v.NB_APRES_SIGNATURE,0)                as NB_APRES_SIGNATURE,
ISNULL(v.ANCIENNETE,0)                        as ANCIENNETE,
ISNULL(vh.NB_OPP_CRE_EN_COURS_HEBDO,0)       as NB_OPP_CRE_EN_COURS_HEBDO,
ISNULL(vh.NB_DETECTION_HEBDO,0)              as NB_DETECTION_HEBDO,
ISNULL(vh.NB_AVANT_SIGNATURE_HEBDO,0)        as NB_AVANT_SIGNATURE_HEBDO,
ISNULL(vh.NB_APRES_SIGNATURE_HEBDO,0)        as NB_APRES_SIGNATURE_HEBDO,
ISNULL(vh.ANCIENNETE_HEBDO,0)                as ANCIENNETE_HEBDO,
ISNULL(vhp.NB_OPP_CRE_EN_COURS_HEBDO_PREC,0) as NB_OPP_CRE_EN_COURS_HEBDO_PREC,
ISNULL(vhp.NB_DETECTION_HEBDO_PREC,0)        as NB_DETECTION_HEBDO_PREC,
ISNULL(vhp.NB_AVANT_SIGNATURE_HEBDO_PREC,0)  as NB_AVANT_SIGNATURE_HEBDO_PREC,
ISNULL(vhp.NB_APRES_SIGNATURE_HEBDO_PREC,0)  as NB_APRES_SIGNATURE_HEBDO_PREC,
ISNULL(vhp.ANCIENNETE_HEBDO_PREC,0)          as ANCIENNETE_HEBDO_PREC,
--Opport credit gnagnees
ISNULL(g.NB_OPP_CRE_GAGNEES,0)              as NB_OPP_CRE_GAGNEES, 
ISNULL(g.NB_OF,0)                           as NB_OF_3,
ISNULL(g.NB_OB,0)                           as NB_OB_3,
ISNULL(g.NB_CREDIT_ACCORDE,0)               as NB_CREDIT_ACCORDE,
ISNULL(g.NB_CREDIT_DECAISSE,0)              as NB_CREDIT_DECAISSE,
ISNULL(g.DELAI_ACCORD_PRET,0)               as DELAI_ACCORD_PRET,
ISNULL(g.DELAI_DECAISSEMENT,0)              as DELAI_DECAISSEMENT,
ISNULL(gh.NB_OPP_CRE_GAGNEES_HEBDO,0)              as NB_OPP_CRE_GAGNEES_HEBDO,
ISNULL(gh.NB_OF_HEBDO,0)                           as NB_OF_3_HEBDO,
ISNULL(gh.NB_OB_HEBDO,0)                           as NB_OB_3_HEBDO,
ISNULL(gh.NB_CREDIT_ACCORDE_HEBDO,0)               as NB_CREDIT_ACCORDE_HEBDO,
ISNULL(gh.NB_CREDIT_DECAISSE_HEBDO,0)              as NB_CREDIT_DECAISSE_HEBDO,
ISNULL(gh.DELAI_ACCORD_PRET_HEBDO,0)               as DELAI_ACCORD_PRET_HEBDO,
ISNULL(gh.DELAI_DECAISSEMENT_HEBDO,0)              as DELAI_DECAISSEMENT_HEBDO,
ISNULL(ghp.NB_OPP_CRE_GAGNEES_HEBDO_PREC,0)        as NB_OPP_CRE_GAGNEES_HEBDO_PREC,
ISNULL(ghp.NB_OF_HEBDO_PREC,0)                     as NB_OF_3_HEBDO_PREC,
ISNULL(ghp.NB_OB_HEBDO_PREC,0)                     as NB_OB_3_HEBDO_PREC,
ISNULL(ghp.NB_CREDIT_ACCORDE_HEBDO_PREC,0)         as NB_CREDIT_ACCORDE_HEBDO_PREC,
ISNULL(ghp.NB_CREDIT_DECAISSE_HEBDO_PREC,0)        as NB_CREDIT_DECAISSE_HEBDO_PREC,
ISNULL(ghp.DELAI_ACCORD_PRET_HEBDO_PREC,0)         as DELAI_ACCORD_PRET_HEBDO_PREC,
ISNULL(ghp.DELAI_DECAISSEMENT_HEBDO_PREC,0)        as DELAI_DECAISSEMENT_HEBDO_PREC,
--Opport credit perdues
ISNULL(p.NB_OPP_CRE_PERDUES,0)                      as NB_OPP_CRE_PERDUES, 
ISNULL(p.NB_ANNULATION,0)                           as NB_ANNULATION,
ISNULL(p.NB_ANNULATION_CLIENT,0)                    as NB_ANNULATION_CLIENT,
ISNULL(p.NB_RETRACTATION,0)                         as NB_RETRACTATION,
ISNULL(p.NB_AFFAIRE_REFUSEE,0)                      as NB_AFFAIRE_REFUSEE,
ISNULL(p.NB_INCIDENT_TECH,0)                        as NB_INCIDENT_TECH,
ISNULL(p.NB_SANS_SUITE,0)                           as NB_SANS_SUITE,
ISNULL(ph.NB_OPP_CRE_PERDUES_HEBDO,0)         as NB_OPP_CRE_PERDUES_HEBDO,
ISNULL(ph.NB_ANNULATION_HEBDO,0)              as NB_ANNULATION_HEBDO,
ISNULL(ph.NB_ANNULATION_CLIENT_HEBDO,0)       as NB_ANNULATION_CLIENT_HEBDO,
ISNULL(ph.NB_RETRACTATION_HEBDO,0)            as NB_RETRACTATION_HEBDO,
ISNULL(ph.NB_AFFAIRE_REFUSEE_HEBDO,0)         as NB_AFFAIRE_REFUSEE_HEBDO,
ISNULL(ph.NB_INCIDENT_TECH_HEBDO,0)           as NB_INCIDENT_TECH_HEBDO,
ISNULL(ph.NB_SANS_SUITE_HEBDO,0)              as NB_SANS_SUITE_HEBDO,
ISNULL(php.NB_OPP_CRE_PERDUES_HEBDO_PREC,0)   as NB_OPP_CRE_PERDUES_HEBDO_PREC,
ISNULL(php.NB_ANNULATION_HEBDO_PREC,0)        as NB_ANNULATION_HEBDO_PREC,
ISNULL(php.NB_ANNULATION_CLIENT_HEBDO_PREC,0) as NB_ANNULATION_CLIENT_HEBDO_PREC,
ISNULL(php.NB_RETRACTATION_HEBDO_PREC,0)      as NB_RETRACTATION_HEBDO_PREC,
ISNULL(php.NB_AFFAIRE_REFUSEE_HEBDO_PREC,0)   as NB_AFFAIRE_REFUSEE_HEBDO_PREC,
ISNULL(php.NB_INCIDENT_TECH_HEBDO_PREC,0)     as NB_INCIDENT_TECH_HEBDO_PREC,
ISNULL(php.NB_SANS_SUITE_HEBDO_PREC,0)        as NB_SANS_SUITE_HEBDO_PREC
from cte_jour_alim_list x 
left join Cte_Opp_Cre_Creees_Cumul_Jour a on x.jour_alim = a.DTE_CREA
left join Cte_Opp_Cre_Creees_Cumul_Hebdo b  on isnull(a.WeekOfYear,@weekobs) = b.WeekOfYear
left join Cte_Opp_Cre_Creees_Cumul_Hebdo_Prec c on isnull(a.WeekOfYear,@weekobs) = c.WeekOfYearJoin
--Opport crédit en cours de vie
left join Cte_Opp_Cre_En_cours_De_Vie_Cumul_Jour v on x.jour_alim = v.DTE_CREA
left join Cte_Opp_Cre_En_cours_De_Vie_Cumul_Hebdo vh  on isnull(a.WeekOfYear,@weekobs) = vh.WeekOfYear
left join Cte_Opp_Cre_En_cours_De_Vie_Cumul_Hebdo_Prec vhp on isnull(a.WeekOfYear,@weekobs) = vhp.WeekOfYearJoin
--Opport credit gnagnees
left join Cte_Opp_Cre_Gagnees_Cumul_Jour g on x.jour_alim = g.DTE_CREA
left join Cte_Opp_Cre_Gagnees_Cumul_Hebdo gh  on isnull(a.WeekOfYear,@weekobs) = gh.WeekOfYear
left join Cte_Opp_Cre_Gagnees_Cumul_Hebdo_Prec ghp on isnull(a.WeekOfYear,@weekobs) = ghp.WeekOfYearJoin
--Opport credit perdues
left join Cte_Opp_Cre_Perdues_Cumul_Jour p on x.jour_alim = p.DTE_CREA
left join Cte_Opp_Cre_Perdues_Cumul_Hebdo ph  on isnull(a.WeekOfYear,@weekobs) = ph.WeekOfYear
left join Cte_Opp_Cre_Perdues_Cumul_Hebdo_Prec php on isnull(a.WeekOfYear,@weekobs) = php.WeekOfYearJoin ;

END;