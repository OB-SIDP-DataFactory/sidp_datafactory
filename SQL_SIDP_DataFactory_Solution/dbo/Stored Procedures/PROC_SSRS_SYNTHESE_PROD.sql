﻿CREATE PROC [dbo].[PROC_SSRS_SYNTHESE_PROD]

AS  BEGIN 

DECLARE @date_obs DATE;
SELECT  @date_obs = CAST(GETDATE() AS DATE);
DECLARE @date_max date;
SELECT  @date_max = dateadd(day, -1 ,GETDATE());
SET DATEFIRST 1; -->1    Lundi
DROP TABLE IF EXISTS #cte_synthese_prod;
DROP TABLE IF EXISTS [dbo].[WK_SSRS_SYNTHESE_PROD];

SELECT Year_alim,Month_alim,Week_alim,jour_alim
    ,nb_ouvertures_CAV
	,nb_fermetures_CAV
	,nb_ouvertures_CSL
	,nb_fermetures_CSL
	,sum(coalesce(nb_ouvertures_CAV,0) - coalesce(nb_fermetures_CAV, 0) ) OVER(ORDER BY jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_comptes_CAV
    ,sum(coalesce(nb_ouvertures_CSL,0) - coalesce(nb_fermetures_CSL, 0) ) OVER(ORDER BY jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_comptes_CSL
	--Week to date
	,sum(nb_ouvertures_CAV) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_ouvertures_CAV_WTD --Week to date
	,sum(nb_fermetures_CAV) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_fermetures_CAV_WTD --Week to date
	,sum(nb_ouvertures_CSL) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_ouvertures_CSL_WTD --Week to date
	,sum(nb_fermetures_CSL) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_fermetures_CSL_WTD --Week to date
	--Month to date
	,sum(nb_ouvertures_CAV) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_ouvertures_CAV_MTD --Month to date
	,sum(nb_fermetures_CAV) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_fermetures_CAV_MTD --Month to date
	,sum(nb_ouvertures_CSL) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_ouvertures_CSL_MTD --Month to date
	,sum(nb_fermetures_CSL) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_fermetures_CSL_MTD --Month to date
	--Full Month
	,sum(nb_ouvertures_CAV) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_ouvertures_CAV_FM --Full Month
	,sum(nb_fermetures_CAV) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_fermetures_CAV_FM --Full Month
	,sum(nb_ouvertures_CSL) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_ouvertures_CSL_FM --Full Month
	,sum(nb_fermetures_CSL) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_fermetures_CSL_FM --Full Month
	--Year to date
    ,sum(nb_ouvertures_CAV) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_ouvertures_CAV_YTD  --Year to date
    ,sum(nb_fermetures_CAV) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_fermetures_CAV_YTD  --Year to date
    ,sum(nb_ouvertures_CSL) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_ouvertures_CSL_YTD  --Year to date
    ,sum(nb_fermetures_CSL) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_fermetures_CSL_YTD  --Year to date
	--Full year
	,sum(nb_ouvertures_CAV) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_ouvertures_CAV_FY  --Full year
	,sum(nb_fermetures_CAV) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_fermetures_CAV_FY  --Full year
	,sum(nb_ouvertures_CSL) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_ouvertures_CSL_FY  --Full year
	,sum(nb_fermetures_CSL) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_fermetures_CSL_FY  --Full year
INTO #cte_synthese_prod
FROM  ( 
     SELECT  T1.StandardDate as jour_alim,DATEPART(YEAR,T1.Date) AS Year_alim,DATEPART(MONTH,T1.Date) AS Month_alim,DATEPART(YEAR,DATEADD(day, (7 - DATEPART(dw, T1.StandardDate)), T1.StandardDate)) *100 + DATEPART(WEEK,DATEADD(day, (7 - DATEPART(dw, T1.StandardDate)), T1.StandardDate)) AS Week_alim
    , sum(case when code_sab_type_compte = '251180' then ouvert_jour else 0 end) as nb_ouvertures_CAV
    , sum(case when code_sab_type_compte = '251180' then ferm_jour else 0 end) as nb_fermetures_CAV
    , sum(case when code_sab_type_compte in ('254181','254111') then ouvert_jour else 0 end) as nb_ouvertures_CSL
    , sum(case when code_sab_type_compte in ('254181','254111') then ferm_jour else 0 end) as nb_fermetures_CSL
FROM [dbo].[DIM_TEMPS] T1 
LEFT OUTER JOIN  sas_vue_synthese_prod  ON T1.StandardDate=sas_vue_synthese_prod.jour_alim
WHERE  ( T1.StandardDate >= CAST(DATEADD(yyyy, DATEDIFF(YYYY, 0, DATEADD(yyyy, -2, @date_obs)), 0) AS DATE)
AND T1.StandardDate <=  DATEADD(day, (7 - DATEPART(dw, @date_obs)), @date_obs) )
GROUP BY  T1.StandardDate ,DATEPART(YEAR,T1.Date) ,DATEPART(MONTH,T1.Date) ,DATEPART(WEEK,T1.Date) 
) t;

SELECT
  CY.jour_alim
, CY.Year_alim
, CY.Month_alim
, CY.Week_alim
, coalesce(CY.nb_comptes_CAV,0) AS nb_comptes_CAV
, coalesce(CY.nb_comptes_CSL,0) AS nb_comptes_CSL
, coalesce(CY.nb_ouvertures_CAV,0) AS nb_ouvertures_CAV
, coalesce(CY.nb_ouvertures_CAV_WTD,0) AS nb_ouvertures_CAV_WTD
, coalesce(CY.nb_ouvertures_CAV_MTD,0) AS nb_ouvertures_CAV_MTD
, coalesce(CY.nb_ouvertures_CAV_YTD,0) AS nb_ouvertures_CAV_YTD
, coalesce(PM.nb_ouvertures_CAV_FM,0) AS nb_ouvertures_CAV_FM
, coalesce(PY.nb_ouvertures_CAV_FY,0) AS nb_ouvertures_CAV_FY
, coalesce(CY.nb_fermetures_CAV,0) AS nb_fermetures_CAV
, coalesce(CY.nb_fermetures_CAV_WTD,0) AS nb_fermetures_CAV_WTD
, coalesce(CY.nb_fermetures_CAV_MTD,0) AS nb_fermetures_CAV_MTD
, coalesce(CY.nb_fermetures_CAV_YTD,0) AS nb_fermetures_CAV_YTD
, coalesce(PM.nb_fermetures_CAV_FM,0) AS nb_fermetures_CAV_FM
, coalesce(PY.nb_fermetures_CAV_FY,0) AS nb_fermetures_CAV_FY
, coalesce(CY.nb_ouvertures_CSL,0) AS nb_ouvertures_CSL
, coalesce(CY.nb_ouvertures_CSL_WTD,0) AS nb_ouvertures_CSL_WTD
, coalesce(CY.nb_ouvertures_CSL_MTD,0) AS nb_ouvertures_CSL_MTD
, coalesce(CY.nb_ouvertures_CSL_YTD,0) AS nb_ouvertures_CSL_YTD
, coalesce(PM.nb_ouvertures_CSL_FM,0) AS nb_ouvertures_CSL_FM
, coalesce(PY.nb_ouvertures_CSL_FY,0) AS nb_ouvertures_CSL_FY
, coalesce(CY.nb_fermetures_CSL,0) AS nb_fermetures_CSL
, coalesce(CY.nb_fermetures_CSL_WTD,0) AS nb_fermetures_CSL_WTD
, coalesce(CY.nb_fermetures_CSL_MTD,0) AS nb_fermetures_CSL_MTD
, coalesce(CY.nb_fermetures_CSL_YTD,0) AS nb_fermetures_CSL_YTD
, coalesce(PM.nb_fermetures_CSL_FM,0) AS nb_fermetures_CSL_FM
, coalesce(PY.nb_fermetures_CSL_FY,0) AS nb_fermetures_CSL_FY
INTO  [dbo].[WK_SSRS_SYNTHESE_PROD]
FROM #cte_synthese_prod CY --Current Year
 LEFT OUTER JOIN #cte_synthese_prod PM --Previous Year
		  ON DATEADD(MONTH,-1,CY.jour_alim) = PM.jour_alim
	 LEFT OUTER JOIN #cte_synthese_prod PY --Previous Year
		  ON DATEADD(YEAR,-1,CY.jour_alim) = PY.jour_alim
ORDER BY CY.jour_alim;
END