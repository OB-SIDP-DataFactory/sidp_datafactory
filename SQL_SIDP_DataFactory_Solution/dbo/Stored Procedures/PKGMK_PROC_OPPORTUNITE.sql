﻿

CREATE PROC [dbo].[PKGMK_PROC_OPPORTUNITE] 
								   @nbRows int OUTPUT -- nb lignes processées

AS  
BEGIN 
--------------------------------------///////////////// Step 1
-- Sélection du dernier état des opportunités et enrichissement avec les dimensions

--purge
IF OBJECT_ID('#WK_ALL_OPPORTUNITES') IS NOT NULL
BEGIN truncate TABLE #WK_ALL_OPPORTUNITES END

--insertion dans table work
SELECT * INTO #WK_ALL_OPPORTUNITES
FROM
(
SELECT		OP.[Id]
			,OP.[IDOppty__c]
			,OP.[Id_SF] as ID_SF
			,OP.[AccountId]
			,OP.[DistributorNetwork__c]
			,RE.LIB_RES_DIS
			,CD.LIB_CAN_DIS as LIB_CAN_DIS_ORI
			,OP.[DistributionChannel__c]
			,CIO.LIB_CAN_INT as LIB_CAN_INT_ORI
			,OP.StartedChannel__c
			,CI.LIB_CAN_INT as LIB_CAN_INT
			,OP.[LeadSource]
			,OP.[RelationEntryScore__c]
			,OP.[CloseDate]
			,OP.[CreatedDate]
			,OP.AdvisorCode__c
FROM [$(DataHubDatabaseName)].[dbo].[PV_SF_OPPORTUNITY_HISTORY] OP
  		inner join [$(DataHubDatabaseName)].[dbo].[PV_FE_ENROLMENTFOLDER]	EF 
			on EF.SALESFORCE_OPPORTUNITY_ID = OP.Id_SF
		inner join [$(DataHubDatabaseName)].[dbo].[PV_FE_EQUIPMENT]	EQ 
			on EQ.ENROLMENT_FOLDER_ID = EF.ID_FE_ENROLMENT_FOLDER AND EQ.PRODUCT_ID IN ('1','21') -- filtre sur les opportunités liées à un CAV ou CSL*/
		left join [dbo].[DIM_RES_DIS] RE
			on RE.COD_RES_DIS = OP.DistributorNetwork__c
		left join [dbo].[DIM_CANAL_DIS] CD
			on CD.COD_CAN_DIS = OP.DistributionChannel__c
		left join [dbo].[DIM_CANAL_INT] CIO
			on CIO.COD_CAN_INT= OP.StartedChannel__c
		left join [dbo].[DIM_CANAL_INT] CI
			on CI.COD_CAN_INT= OP.LeadSource
WHERE OP.Validity_EndDate = '9999-12-31 00:00:00.0000000'
AND OP.StageName = '09'
) AO
;
--------------------------------------///////////////// Step 2
-- Insertion des nouvelles opportunités

MERGE INTO [dbo].[MKT_OPPORTUNITE] OP
	USING #WK_ALL_OPPORTUNITES WKOP
	ON (OP.IDE_OPPRT_SF = WKOP.ID_SF)
	WHEN NOT MATCHED THEN
	INSERT (
      [NUM_OPPRT]
      ,[IDE_OPPRT_SF]
      ,[IDE_PERS_SF]
      ,[LIB_RES_DIS]
      ,[COD_RES_DIS]
      ,[COD_CAN_DIS_ORI]
      ,[LIB_CAN_DIS_ORI]
      ,[COD_CAN_INT_ORI]
      ,[LIB_CAN_INT_ORI]
      ,[COD_CAN_INT]
      ,[LIB_CAN_INT]
      ,[SCO_ENT_REL]
      ,[DTE_OPPRT_GAG]
      ,[DTE_CREA]
      ,[COD_VENDEUR]
	  )
	VALUES
	(
		WKOP.[IDOppty__c]
		,WKOP.[ID_SF]
		,WKOP.[AccountId]
		,ISNULL(WKOP.LIB_RES_DIS,'Non renseigné')
		,ISNULL(WKOP.[DistributorNetwork__c],'NR')
		,ISNULL(WKOP.[DistributionChannel__c],'Non renseigné')
		,ISNULL(WKOP.LIB_CAN_DIS_ORI,'Non renseigné')
		,ISNULL(WKOP.StartedChannel__c,'NR')
		,ISNULL(WKOP.LIB_CAN_INT_ORI,'Non renseigné')
		,ISNULL(WKOP.[LeadSource],'NR')
		,ISNULL(WKOP.LIB_CAN_INT,'Non renseigné')
		,ISNULL(WKOP.[RelationEntryScore__c],'Non renseigné')
		,WKOP.[CloseDate]
		,WKOP.[CreatedDate]
		,ISNULL(WKOP.AdvisorCode__c,'Non renseigné')
	)
;
SELECT @nbRows = @@ROWCOUNT

END