﻿
CREATE PROCEDURE [dbo].[PKGES_PROC_ABO_EXECUTIVE_SUMMARY] (
    @p_ReportName VARCHAR(255)
  , @p_ReportDate DATE
  , @p_ReportServerShare VARCHAR(255)
)
AS
BEGIN

SET LANGUAGE FRENCH;

DECLARE @ReportDate DATE;
SELECT @ReportDate = COALESCE(@p_ReportDate, MAX([date_alim])) FROM [dbo].[WK_STOCK_OPPORTUNITY];

SELECT CAST('2017-11-02' AS DATE) AS P_Datedebut
     , @ReportDate AS P_Datefin 
     , @p_ReportServerShare as Path
     , @p_ReportName + '_' + SUBSTRING(CONVERT(VARCHAR(20), @ReportDate, 120), 1, 10) AS FileName;

END