﻿CREATE PROC [dbo].[PROC_SSRS_SAS_VUE_STOCK_OPPORT]

AS  BEGIN 

DECLARE @date_obs DATE;
SELECT  @date_obs = CAST(GETDATE() AS DATE);
DECLARE @date_max date;
SELECT  @date_max = dateadd(day, -1 ,GETDATE());
SET DATEFIRST 1; -->1    Lundi
DROP TABLE IF EXISTS #rq_cli_enrolment_cp;
DROP TABLE IF EXISTS [dbo].[WK_SSRS_SAS_VUE_STOCK_OPPORT];

SELECT Year_alim,Month_alim,Week_alim,jour_alim
    ,nb_opport_CAV_creees
	,nb_opport_en_cours
	,nb_opport_canal_ori_digital
	,nb_opport_canal_ori_boutique
	,nb_opport_canal_ori_crc
	,nb_opport_suite_indication
	--Week to date
	,sum(nb_opport_CAV_creees) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_CAV_creees_WTD --Week to date
	,sum(nb_opport_en_cours) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_en_cours_WTD --Week to date
	,sum(nb_opport_canal_ori_digital) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_canal_ori_digital_WTD --Week to date
	,sum(nb_opport_canal_ori_boutique) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_canal_ori_boutique_WTD --Week to date
	,sum(nb_opport_canal_ori_crc) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_canal_ori_crc_WTD --Week to date
	,sum(nb_opport_suite_indication) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_suite_indication_WTD --Week to date
	
	--Month to date
	,sum(nb_opport_CAV_creees) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_CAV_creees_MTD --Month to date
	,sum(nb_opport_en_cours) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_en_cours_MTD --Month to date
	,sum(nb_opport_canal_ori_digital) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_canal_ori_digital_MTD --Month to date
	,sum(nb_opport_canal_ori_boutique) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_canal_ori_boutique_MTD --Month to date
	,sum(nb_opport_canal_ori_crc) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_canal_ori_crc_MTD --Month to date
	,sum(nb_opport_suite_indication) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_suite_indication_MTD --Month to date
	
	
	--Full Month
	,sum(nb_opport_CAV_creees) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_opport_CAV_creees_FM --Full Month
	,sum(nb_opport_en_cours) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_opport_en_cours_FM --Full Month
	,sum(nb_opport_canal_ori_digital) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_opport_canal_ori_digital_FM --Full Month
	,sum(nb_opport_canal_ori_boutique) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_opport_canal_ori_boutique_FM --Full Month
	,sum(nb_opport_canal_ori_crc) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_opport_canal_ori_crc_FM --Full Month
	,sum(nb_opport_suite_indication) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS nb_opport_suite_indication_FM --Full Month
	
	--Year to date
    ,sum(nb_opport_CAV_creees) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_CAV_creees_YTD  --Year to date
    ,sum(nb_opport_en_cours) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_en_cours_YTD  --Year to date
    ,sum(nb_opport_canal_ori_digital) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_canal_ori_digital_YTD  --Year to date
    ,sum(nb_opport_canal_ori_boutique) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_canal_ori_boutique_YTD  --Year to date
    ,sum(nb_opport_canal_ori_crc) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_canal_ori_crc_YTD  --Year to date
    ,sum(nb_opport_suite_indication) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS nb_opport_suite_indication_YTD  --Year to date
   
	--Full year
	,sum(nb_opport_CAV_creees) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_opport_CAV_creees_FY  --Full year
	,sum(nb_opport_en_cours) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_opport_en_cours_FY  --Full year
	,sum(nb_opport_canal_ori_digital) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_opport_canal_ori_digital_FY  --Full year
	,sum(nb_opport_canal_ori_boutique) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_opport_canal_ori_boutique_FY  --Full year
	,sum(nb_opport_canal_ori_crc) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_opport_canal_ori_crc_FY  --Full year
	,sum(nb_opport_suite_indication) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS nb_opport_suite_indication_FY  --Full year
INTO #rq_cli_enrolment_cp
FROM  ( 
     SELECT  T1.StandardDate as jour_alim,DATEPART(YEAR,T1.Date) AS Year_alim,DATEPART(MONTH,T1.Date) AS Month_alim,DATEPART(YEAR,DATEADD(day, (7 - DATEPART(dw, T1.StandardDate)), T1.StandardDate)) *100 + DATEPART(WEEK,DATEADD(day, (7 - DATEPART(dw, T1.StandardDate)), T1.StandardDate)) AS Week_alim
	, sum(case when date_creat_opp = date_alim and commercial_offer_code = 'OC80' then nb_opport else 0 end) as nb_opport_CAV_creees
	, sum(case when commercial_offer_code in ('CB80','OC80','L80') and stade_vente  <> 'Compte ouvert' and stade_vente  <> 'Affaire refusée' and stade_vente  <> 'Sans suite' and stade_vente  <> 'Affaire conclue' and stade_vente  <> 'Incident technique' then nb_opport else 0 end) as nb_opport_en_cours --> Ticket num 36 : Exclure le stade de vente "Incident technique" du comptage des opportunités en cours (Ahmed)
	, sum(case when commercial_offer_code in ('CB80','OC80','L80') and stade_vente  <> 'Compte ouvert' and stade_vente  <> 'Affaire refusée' and stade_vente  <> 'Sans suite' and stade_vente  <> 'Affaire conclue' and stade_vente  <> 'Incident technique' and canal_distrib_origine = 'Digital' then nb_opport else 0 end) as nb_opport_canal_ori_digital
	, sum(case when commercial_offer_code in ('CB80','OC80','L80') and stade_vente  <> 'Compte ouvert' and stade_vente  <> 'Affaire refusée' and stade_vente  <> 'Sans suite' and stade_vente  <> 'Affaire conclue' and stade_vente  <> 'Incident technique' and canal_distrib_origine = 'Agence' then nb_opport else 0 end) as nb_opport_canal_ori_boutique
	, sum(case when commercial_offer_code in ('CB80','OC80','L80') and stade_vente  <> 'Compte ouvert' and stade_vente  <> 'Affaire refusée' and stade_vente  <> 'Sans suite' and stade_vente  <> 'Affaire conclue' and stade_vente  <> 'Incident technique' and canal_distrib_origine = 'Crc' then nb_opport else 0 end) as nb_opport_canal_ori_crc
	, sum(case when commercial_offer_code in ('CB80','OC80','L80') and flag_indication = 'Oui' then nb_opport else 0 end) as nb_opport_suite_indication
FROM [dbo].[DIM_TEMPS] T1 
LEFT OUTER JOIN  sas_vue_stock_opport  ON T1.StandardDate=sas_vue_stock_opport.date_alim
WHERE ( cast(sas_vue_stock_opport.date_creat_opp as date) >='2017-05-16' and T1.StandardDate <= dateadd(day, (7 - DATEPART(dw, @date_obs)), @date_obs))
GROUP BY  T1.StandardDate ,DATEPART(YEAR,T1.Date) ,DATEPART(MONTH,T1.Date) ,DATEPART(WEEK,T1.Date) 
) t;		
SELECT
  CY.jour_alim
, CY.Year_alim
, CY.Month_alim
, CY.Week_alim
, coalesce(CY.nb_opport_CAV_creees,0) AS nb_opport_CAV_creees
, coalesce(CY.nb_opport_CAV_creees_WTD,0) AS nb_opport_CAV_creees_WTD
, coalesce(CY.nb_opport_CAV_creees_MTD,0) AS nb_opport_CAV_creees_MTD
, coalesce(CY.nb_opport_CAV_creees_YTD,0) AS nb_opport_CAV_creees_YTD
, coalesce(PM.nb_opport_CAV_creees_FM,0) AS nb_opport_CAV_creees_FM
, coalesce(PY.nb_opport_CAV_creees_FY,0) AS nb_opport_CAV_creees_FY
, coalesce(CY.nb_opport_en_cours,0) AS nb_opport_en_cours
, coalesce(CY.nb_opport_en_cours_WTD,0) AS nb_opport_en_cours_WTD
, coalesce(CY.nb_opport_en_cours_MTD,0) AS nb_opport_en_cours_MTD
, coalesce(CY.nb_opport_en_cours_YTD,0) AS nb_opport_en_cours_YTD
, coalesce(PM.nb_opport_en_cours_FM,0) AS nb_opport_en_cours_FM
, coalesce(PY.nb_opport_en_cours_FY,0) AS nb_opport_en_cours_FY
, coalesce(CY.nb_opport_canal_ori_digital,0) AS nb_opport_canal_ori_digital
, coalesce(CY.nb_opport_canal_ori_digital_WTD,0) AS nb_opport_canal_ori_digital_WTD
, coalesce(CY.nb_opport_canal_ori_digital_MTD,0) AS nb_opport_canal_ori_digital_MTD
, coalesce(CY.nb_opport_canal_ori_digital_YTD,0) AS nb_opport_canal_ori_digital_YTD
, coalesce(PM.nb_opport_canal_ori_digital_FM,0) AS nb_opport_canal_ori_digital_FM
, coalesce(PY.nb_opport_canal_ori_digital_FY,0) AS nb_opport_canal_ori_digital_FY
, coalesce(CY.nb_opport_canal_ori_boutique,0) AS nb_opport_canal_ori_boutique
, coalesce(CY.nb_opport_canal_ori_boutique_WTD,0) AS nb_opport_canal_ori_boutique_WTD
, coalesce(CY.nb_opport_canal_ori_boutique_MTD,0) AS nb_opport_canal_ori_boutique_MTD
, coalesce(CY.nb_opport_canal_ori_boutique_YTD,0) AS nb_opport_canal_ori_boutique_YTD
, coalesce(PM.nb_opport_canal_ori_boutique_FM,0) AS nb_opport_canal_ori_boutique_FM
, coalesce(PY.nb_opport_canal_ori_boutique_FY,0) AS nb_opport_canal_ori_boutique_FY
, coalesce(CY.nb_opport_canal_ori_crc,0) AS nb_opport_canal_ori_crc
, coalesce(CY.nb_opport_canal_ori_crc_WTD,0) AS nb_opport_canal_ori_crc_WTD
, coalesce(CY.nb_opport_canal_ori_crc_MTD,0) AS nb_opport_canal_ori_crc_MTD
, coalesce(CY.nb_opport_canal_ori_crc_YTD,0) AS nb_opport_canal_ori_crc_YTD
, coalesce(PM.nb_opport_canal_ori_crc_FM,0) AS nb_opport_canal_ori_crc_FM
, coalesce(PY.nb_opport_canal_ori_crc_FY,0) AS nb_opport_canal_ori_crc_FY
, coalesce(CY.nb_opport_suite_indication,0) AS nb_opport_suite_indication
, coalesce(CY.nb_opport_suite_indication_WTD,0) AS nb_opport_suite_indication_WTD
, coalesce(CY.nb_opport_suite_indication_MTD,0) AS nb_opport_suite_indication_MTD
, coalesce(CY.nb_opport_suite_indication_YTD,0) AS nb_opport_suite_indication_YTD
, coalesce(PM.nb_opport_suite_indication_FM,0) AS nb_opport_suite_indication_FM
, coalesce(PY.nb_opport_suite_indication_FY,0) AS nb_opport_suite_indication_FY
INTO [dbo].[WK_SSRS_SAS_VUE_STOCK_OPPORT]
FROM #rq_cli_enrolment_cp CY --Current Year
 LEFT OUTER JOIN #rq_cli_enrolment_cp PM --Previous Year
		  ON DATEADD(MONTH,-1,CY.jour_alim) = PM.jour_alim
	 LEFT OUTER JOIN #rq_cli_enrolment_cp PY --Previous Year
		  ON DATEADD(YEAR,-1,CY.jour_alim) = PY.jour_alim
ORDER BY CY.jour_alim;
END