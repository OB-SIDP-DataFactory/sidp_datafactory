﻿
/****** Object:  StoredProcedure [dbo].[PKGEC_PROC_FACT_ENCOURS]    Script Date: 17/11/2017 15:46:10 ******/

-- =============================================
-- Author:		<Liliane, Huang>
-- Create date: <17/11/2017>
-- Description:	<Alimentation de la table T_FACT_ENCOURS>
-- =============================================

CREATE PROCEDURE [dbo].[PKGEC_PROC_FACT_ENCOURS] 
(
   @date_alim DATE
    ,@nbRows int OUTPUT -- nb lignes processées
)
AS
BEGIN

-- variables comptage nombre lignes
declare @nbRows_compte int; 
declare @nbRows_credit int; 

/*Test
declare @date_alim DATE
set @date_alim = '2017-10-31'*/
--STEP 1 
--1er flux pour calculer l'encours des comptes 

--Purge de la table temporaire
IF OBJECT_ID('tempdb..#wk_temp_encours_cpt1') IS NOT NULL
BEGIN truncate TABLE #wk_temp_encours_cpt1 END

--Récupération des informations de référentiel transco encours produit associé au compte
SELECT * INTO #wk_temp_encours_cpt1
FROM
(
SELECT CPT.DWHCPTNBR as NBE_TITULAIRE
      ,CPT.DWHCPTCPT as SOLDE_CPT
      ,CPT.DWHCPTDAC as DT_CLOTURE
	  ,CPT.DWHCPTPPAL as DWHCPTPPAL
	  ,CPT.DWHCPTCOM as DWHCPTCOM
	  ,CPT.DWHCPTRUB as DWHCPTRUB
	  ,CASE 
			WHEN CPT.DWHCPTCPT < 0 THEN 1 --Ressources
			WHEN CPT.DWHCPTCPT > 0 THEN 0 --Emplois
			ELSE NULL 
		END AS FLAG_ER
	  ,DEN.CODE_RUBRIQUE_COMPTABLE_SAB as CODE_RUBRIQUE_COMPTABLE_SAB
	  ,ECH.DWHENCTCD as DWHENCTCD
	  ,ISNULL(DEN.CODE_PRODUIT_SIDP,DEN_EL.CODE_PRODUIT_SIDP) as CD_PRODUIT_ENCOURS
  FROM [$(DataHubDatabaseName)].[dbo].[VW_PV_SA_Q_COMPTE] CPT
  --Récupération du code produit encours pour tous les codes regroupement SIDP différent de ELANCIO
  LEFT JOIN [dbo].[DIM_TRANSCO_ENCOURS] DEN
  ON CPT.DWHCPTRUB = DEN.CODE_RUBRIQUE_COMPTABLE_SAB AND DEN.CODE_REGROUPEMENT_SIDP <> 'EL'
  LEFT JOIN [$(DataHubDatabaseName)].[dbo].[VW_PV_SA_M_ENCRS_ECHL] ECH --Mensuel
  ON CPT.DWHCPTCOM = ECH.DWHENCCOM 
  --Filtre sur la période de validité 
  AND ECH.Validity_StartDate <= @date_alim AND ECH.Validity_EndDate > @date_alim
  --Récupération du code produit encours pour tous les codes regroupement SIDP ELANCIO
  LEFT JOIN [dbo].[DIM_TRANSCO_ENCOURS] DEN_EL
  ON CPT.DWHCPTRUB = DEN_EL.CODE_RUBRIQUE_COMPTABLE_SAB AND ECH.DWHENCTCD = DEN_EL.CODE_GRILLE_SAB AND DEN_EL.CODE_REGROUPEMENT_SIDP = 'EL'
  WHERE CPT.Validity_StartDate <= @date_alim AND CPT.Validity_EndDate > @date_alim 
  --Encours avec la date de clôture est null ou supérieure à la date d'alimentation
  AND (CPT.DWHCPTDAC IS NULL OR CPT.DWHCPTDAC > @date_alim)
  --Filtre pour que le code produit encours soit toujours renseigné
  AND (DEN.CODE_REGROUPEMENT_SIDP IS NOT NULL OR DEN_EL.CODE_REGROUPEMENT_SIDP IS NOT NULL)
) r
; 

--Purge de la table temporaire
IF OBJECT_ID('tempdb..#wk_temp_encours_cpt2') IS NOT NULL
BEGIN truncate TABLE #wk_temp_encours_cpt2 END

--Récupération des informations de référentiel transco elancio associé au compte
SELECT * INTO #wk_temp_encours_cpt2
FROM
(SELECT wk1.NBE_TITULAIRE as NBE_TITULAIRE
      ,wk1.SOLDE_CPT as SOLDE_CPT
      ,wk1.DT_CLOTURE as DT_CLOTURE
	  ,CASE 
			WHEN wk1.NBE_TITULAIRE = 1 THEN CLI.DWHCLIRES 
			WHEN wk1.NBE_TITULAIRE > 1 THEN CLI2.DWHCLIRES 
			ELSE NULL
		END AS CD_RESPONSABLE
	  ,wk1.CD_PRODUIT_ENCOURS as CD_PRODUIT_ENCOURS
	  ,wk1.FLAG_ER as FLAG_ER
	  ,ISNULL(DEL.CODE_PRODUIT_SIDP,'Non renseigné') as CD_PRODUIT_ELANCIO
	  ,CASE 
			WHEN wk1.NBE_TITULAIRE = 1 THEN CLI.DWHCLIRU2 
			WHEN wk1.NBE_TITULAIRE > 1 THEN CLI2.DWHCLIRU2 
			ELSE NULL
		END AS CD_APPORTEUR
  FROM #wk_temp_encours_cpt1 wk1
  LEFT JOIN [$(DataHubDatabaseName)].[dbo].[VW_PV_SA_Q_CLIENT] CLI
  ON wk1.DWHCPTPPAL = CLI.DWHCLICLI
  AND CLI.Validity_StartDate <= @date_alim AND CLI.Validity_EndDate > @date_alim
  --Dans le cas où DWHCPTNBR > 1, récupérer le code responsable et le code apporteur du tiers de référence
  LEFT JOIN [$(DataHubDatabaseName)].[dbo].[VW_PV_SA_Q_CLIENT] CLI2
  ON CLI.[DWHCLITIE] = CLI2.[DWHCLICLI] 
  AND CLI2.Validity_StartDate <= @date_alim AND CLI2.Validity_EndDate > @date_alim 
  --Récupération du code produit elancio
  LEFT JOIN [dbo].[DIM_TRANSCO_ELANCIO] DEL
  ON wk1.DWHCPTRUB = DEL.CODE_RUBRIQUE_COMPTABLE_SAB AND wk1.DWHENCTCD = DEL.CODE_GRILLE_SAB 
) r
; 

--Récupération des informations de référentiel transco marché associé au compte + calcul de l'encours
--Insertion des informations dans la table T_FACT_ENCOURS
INSERT INTO [dbo].[T_FACT_ENCOURS] ([DT_ALIM],
								[CD_MARCHE],
								[CD_PRODUIT_ENCOURS],
								[CD_PRODUIT_ELANCIO],
								[CD_RESEAU],
								[FLAG_ER], 
								[ENCOURS],
								[DT_DER_MOIS],
								[DER_MOIS_ENCOURS]
								)
SELECT @date_alim AS DT_ALIM
	,ISNULL(DMA.CODE_MARCHE,'Non renseigné') as CD_MARCHE
	,wk2.CD_PRODUIT_ENCOURS
	,wk2.CD_PRODUIT_ELANCIO
	,ISNULL(DRE.CODE_RESEAU,'Non renseigné') as CD_RESEAU
	,wk2.FLAG_ER
	,SUM(wk2.SOLDE_CPT) AS ENCOURS
	,NULL as DT_DER_MOIS
	,NULL as DER_MOIS_ENCOURS
FROM #wk_temp_encours_cpt2 wk2
INNER JOIN [dbo].[DIM_TRANSCO_MARCHE] DMA
ON wk2.CD_RESPONSABLE = DMA.CODE_RESPONSABLE
LEFT JOIN [dbo].[DIM_TRANSCO_RESEAU] DRE
ON wk2.CD_APPORTEUR = DRE.CODE_APPORTEUR
WHERE wk2.FLAG_ER IS NOT NULL
GROUP BY --@date_alim, 
		DMA.CODE_MARCHE, 
		wk2.CD_PRODUIT_ENCOURS, 
		wk2.CD_PRODUIT_ELANCIO, 
		DRE.CODE_RESEAU, 
		wk2.FLAG_ER

		SELECT @nbRows_compte = @@ROWCOUNT
;
--STEP 2 
--2ème flux pour calculer l'encours des dossiers de crédits 

--Purge de la table temporaire
IF OBJECT_ID('tempdb..#wk_temp_encours_crd1') IS NOT NULL
BEGIN truncate TABLE #wk_temp_encours_crd1 END

--Récupération des informations de référentiel transco marché et transco encours produit associé au crédit
SELECT * INTO #wk_temp_encours_crd1
FROM
(
SELECT CRD.DWHOPEVAL as SOLDE_CPT
      ,CRD.DWHOPEFIN as DT_CLOTURE
	  ,ISNULL(DMA.CODE_MARCHE,'Non renseigné') AS CD_MARCHE
	  ,ISNULL(DEN.CODE_PRODUIT_SIDP,'Non renseigné') as CD_PRODUIT_ENCOURS
	  ,CASE 
			WHEN CRD.DWHOPEVAL < 0 THEN 1 --Ressources
			WHEN CRD.DWHOPEVAL > 0 THEN 0 --Emplois
			ELSE NULL
		END AS FLAG_ER
	 ,ISNULL(DRE.CODE_RESEAU,'Non renseigné') AS CD_RESEAU
  FROM [$(DataHubDatabaseName)].[dbo].[VW_PV_SA_Q_OPCREDIT] CRD 
  LEFT JOIN [$(DataHubDatabaseName)].[dbo].[VW_PV_SA_Q_CLIENT] CLI
  ON CRD.DWHOPECON = CLI.DWHCLICLI 
  AND CLI.Validity_StartDate <= @date_alim AND CLI.Validity_EndDate > @date_alim
  INNER JOIN [dbo].[DIM_TRANSCO_MARCHE] DMA
  ON CLI.DWHCLIRES = DMA.CODE_RESPONSABLE
  INNER JOIN [dbo].[DIM_TRANSCO_ENCOURS] DEN
  ON CRD.DWHOPEOPR = DEN.CODE_OPERATION_SAB AND CRD.DWHOPENAT = DEN.CODE_NATURE_CREDIT_SAB 
  LEFT JOIN [dbo].[DIM_TRANSCO_RESEAU] DRE
  ON CLI.DWHCLIRU2 = DRE.CODE_APPORTEUR
  WHERE CRD.Validity_StartDate <= @date_alim AND CRD.Validity_EndDate > @date_alim
  AND (CRD.DWHOPEFIN IS NULL OR CRD.DWHOPEFIN > @date_alim)
) r
; 

--Calcul de l'encours associé au crédit
--Insertion des informations dans la table T_FACT_ENCOURS
INSERT INTO [dbo].[T_FACT_ENCOURS] ([DT_ALIM],
								[CD_MARCHE],
								[CD_PRODUIT_ENCOURS],
								[CD_PRODUIT_ELANCIO],
								[CD_RESEAU],
								[FLAG_ER], 
								[ENCOURS],
								[DT_DER_MOIS],
								[DER_MOIS_ENCOURS]
								)
SELECT @date_alim
	,wk1.CD_MARCHE
	,wk1.CD_PRODUIT_ENCOURS
	,'Non renseigné' as CD_PRODUIT_ELANCIO
	,wk1.CD_RESEAU
	,wk1.FLAG_ER
	,SUM(wk1.SOLDE_CPT) AS ENCOURS
	,NULL as DT_DER_MOIS
	,NULL as DER_MOIS_ENCOURS
FROM #wk_temp_encours_crd1 wk1
WHERE wk1.FLAG_ER IS NOT NULL
GROUP BY --@date_alim, 
		wk1.CD_MARCHE, 
		wk1.CD_PRODUIT_ENCOURS, 
		wk1.CD_RESEAU, 
		wk1.FLAG_ER

		SELECT @nbRows_credit = @@ROWCOUNT
;

--STEP 3
--Calcul de l'encours du dernier jour du mois précédent à partir de T_FACT_ENCOURS
--MAJ de la date du dernier jour du mois précédent + l'encours du dernier jour du mois précédent
UPDATE FEC
SET FEC.DT_DER_MOIS = FEC_M.DT_ALIM,
	FEC.DER_MOIS_ENCOURS = FEC_M.ENCOURS
FROM [dbo].[T_FACT_ENCOURS] FEC 
LEFT JOIN [dbo].[T_FACT_ENCOURS] FEC_M
ON FEC.CD_MARCHE = FEC_M.CD_MARCHE
AND FEC.CD_PRODUIT_ENCOURS = FEC_M.CD_PRODUIT_ENCOURS
AND FEC.CD_PRODUIT_ELANCIO = FEC_M.CD_PRODUIT_ELANCIO
AND FEC.CD_RESEAU = FEC_M.CD_RESEAU
AND FEC.FLAG_ER = FEC_M.FLAG_ER
AND FEC_M.DT_ALIM = EOMONTH(FEC.DT_ALIM, -1)
;

SELECT @nbRows = @nbRows_compte + @nbRows_credit

END