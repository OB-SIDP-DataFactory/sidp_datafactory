﻿CREATE PROCEDURE [dbo].[PKG_QOD_EXEC_FileOrTableName]
@FileOrTableName as nvarchar(max),
@ID_CONTROLE as integer=NULL
as

 --Curseur sur les contrôles 
 DECLARE Control_cursor CURSOR for
 select ID_CONTROL from SIDP_QOD_LISTE_CONTROLE_FILEORTABLENAME
 where [FileOrTableName] = @FileOrTableName 
	AND ISNULL(FLG_ACTIF,0)=1

 --Boucle sur les contrôles 
OPEN Control_cursor
FETCH NEXT FROM Control_cursor INTO @ID_CONTROLE
WHILE @@FETCH_STATUS = 0
BEGIN 
 
	--Exécution du contrôle
	exec PKG_QOD_EXEC_CONTROLE @ID_CONTROL=@ID_CONTROLE;
 
FIN_BOUCLE:

FETCH NEXT FROM Control_cursor INTO @ID_CONTROLE
END


