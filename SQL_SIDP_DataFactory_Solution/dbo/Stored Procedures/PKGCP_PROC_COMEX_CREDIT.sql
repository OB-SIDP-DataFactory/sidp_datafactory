﻿CREATE PROCEDURE [dbo].[PKGCP_PROC_COMEX_CREDIT] 
	@date_obs date --='2018-01-30'
AS
BEGIN
	SET DATEFIRST 1; -->1    Lundi
	declare @weekPreobs date=DATEADD(week,-1,@date_obs);
	declare @FirstDOW date=DATEADD(day, (-1 * DATEPART(dw, @Date_obs)) + 1, @Date_obs);
	declare @FirstDOPW date=DATEADD(dd  , -7,@FirstDOW); 
	declare @var int =datediff(dd,@FirstDOPW,@date_obs);

	with
	cte_jour_alim_list AS(
	select T1.StandardDate as jour_alim
	from DIM_TEMPS T1
	where T1.StandardDate<=@date_obs and T1.StandardDate>=DATEADD(dd,-@var,@date_obs)--T1.StandardDate>=DATEADD(dd,-6,@date_obs)
	--and T1.StandardDate>= DATEADD(DAY,(-1*datepart(dw,@weekPreobs))+1,@weekPreobs)
	)
	--select * from cte_jour_alim_list
	,cte_cre_all as (
	select Opp.[IDE_OPPRT_SF]
	,[STADE_VENTE]
	,[LIB_STADE_VENTE]
	,[MOT_AFF_REF]
	,[LIB_MOT_AFF_REF]
	,CAST(Opp.[DTE_CREA] as date) [DTE_CREA]
	,CAST([DTE_CLO_OPPRT] as date ) as [DTE_CLO_OPPRT]
	,CAST([DTE_DEBLOCAGE] as date) as [DTE_DEBLOCAGE]
	,CAST(DTE_ACCORD as date) as [DTE_ACC_CRE]
	,[DELAI_ACCORD]
	,[DELAI_DEBLOCAGE]
	,[AGE_OPPRT]
	, DATEPART(ISO_WEEK,Opp.[DTE_ALIM]) as [WeekOfYear]
	,[COD_RES_DIS]
	,[COD_CAN_INT_ORI]
	,Opp.[DTE_ALIM]
	,[MTT_OPPRT]
	,[MTT_DEBLOQUE]
	,[COD_OFF_COM]
	,[LABEL_OFF_COM]
	,[MTT_NML]
	,[MTT_TOT_DEB]
	, rank() over (partition by Opp.[IDE_OPPRT_SF],Opp.DTE_CREA/* Opp.DTE_ALIM*/ order by Opp.DTE_DER_MAJ asc) as rang

	FROM [dbo].[CRE_OPPORTUNITE] Opp left join [dbo].[CRE_CREDIT] Cre on Opp.IDE_OPPRT_SF=Cre.IDE_OPPRT_SF
	INNER JOIN cte_jour_alim_list on CAST(Opp.[DTE_CREA] as date)=cte_jour_alim_list.jour_alim
	--and [FLG_DER_ETAT_OPPRT]=1 and Cast([DTE_CREA] as date) BETWEEN DATEADD(dd,-6,@date_obs)  and @date_obs
	)
	--select * from cte_cre_all

	,cte_opp_all as (
	select Ops.Id_SF                            as [IDE_OPPRT_SF]
	,Cod_StageName                        as [STADE_VENTE]
	,Lib_StageName                        as [LIB_STADE_VENTE]
	,[Cod_DeniedOpportunityReason__c]     as [MOT_AFF_REF]
	,[Lib_DeniedOpportunityReason__c]     as [LIB_MOT_AFF_REF]
	,CAST(Ops.[CreatedDate] as date)      as [DTE_CREA]
	,CAST([CloseDate] as date )           as [DTE_CLO_OPPRT]
	,CAST([ReleaseDate__c] as date)       as [DTE_DEBLOCAGE]
	,CAST(LoanAcceptationDate__c as date) as [DTE_ACC_CRE]
	,[delai_accord]
	,[delai_deblocage]
	,age_opport                           as [AGE_OPPRT]
	, DATEPART(ISO_WEEK,Ops.[date_alim])  as [WeekOfYear]
	,[Cod_DistributorNetwork__c]          as [COD_RES_DIS]
	,[Cod_LeadSource]                     as [COD_CAN_INT_ORI]
	,[Cod_DistributionChannel__c]         as [Cod_DistributionChannel__c]
	,[date_alim]                          as [DTE_ALIM]
	,[Amount]                             as [MTT_OPPRT]
	,[ReleaseAmount__c]                   as [MTT_DEBLOQUE]
	,[CommercialOfferCode__c]             as [COD_OFF_COM]
	,[CommercialOfferName__c]             as [LABEL_OFF_COM]
	,Ops.last_update_date                 as [last_update_date]
	,Ops.SignatureDate__c                 as [SignatureDate__c] 
	,[MTT_NML]
	,[MTT_TOT_DEB]
	,CRD
	,CAST(CloseDate as date)              as [CloseDate]
	,RecordTypeId
	,Lib_RecordType
	,MTT_TOT_DU
	,TX_INT_PAL
	,CAST(DTE_DER_DEB as date) as DTE_DER_DEB
	, rank() over (partition by Ops.Id_SF, Ops.date_alim order by Ops.last_update_date desc) as rang
	FROM [dbo].[CRE_WK_STOCK_OPPORTUNITY] Ops left join [dbo].[CRE_CREDIT] Cre on Ops.Id_SF=Cre.IDE_OPPRT_SF
	INNER JOIN cte_jour_alim_list on Ops.date_alim=cte_jour_alim_list.jour_alim
	--WHERE Cast(Ops.[CreatedDate] as Date) <= cast(@Date_Obs as Date)
	--AND Cast(Ops.[date_alim] as Date) = cast(@Date_Obs as Date)
	)
	--select *, DATEDIFF(DD,DTE_ACC_CRE,DTE_DEBLOCAGE) as delai from cte_opp_all where STADE_VENTE='15' and COD_RES_DIS in ('01','02') and DTE_ALIM='2018-06-06' order by delai asc 

	,cte_opp_gag as (
	select IDE_OPPRT_SF,
	MIN(last_update_date) as last_update_date
	from cte_opp_all
	where STADE_VENTE in ('14','15') and rang=1
	group by IDE_OPPRT_SF)
	--select * from cte_opp_gag
	/*where IDE_OPPRT_SF = '0061p00000gyg8eAAA'
	select * from cte_opp_all where DTE_ALIM = '2018-06-06' and stade_vente = 15 and IDE_OPPRT_SF in ('0061p00000gyg8eAAA','0061p00000gz7E1AAI')
	cte_opp_all y inner join cte_opp_gag on y.IDE_OPPRT_SF=cte_opp_gag.IDE_OPPRT_SF and y.DTE_ALIM=cast(cte_opp_gag.last_update_date as date) 
	where STADE_VENTE ='15' and DTE_ALIM='2018-06-06'
	-- 0061p00000gz7E1AAI
	--0061p00000gyg8eAAA*/
	,cte_opp_per as (
	select IDE_OPPRT_SF,
	--MIN(last_update_date) as last_update_date
	CloseDate
	from cte_opp_all
	where STADE_VENTE IN('16','12','10','11') --and rang=1
	group by IDE_OPPRT_SF,CloseDate)
	--select * from cte_opp_per

	,cte_opp_acc as (
	select IDE_OPPRT_SF,
	MIN(last_update_date) as last_update_date
	from cte_opp_all
	where STADE_VENTE ='14'  and rang=1
	group by IDE_OPPRT_SF)
	--select * from cte_opp_acc

	,cte_opp_dec as (
	select IDE_OPPRT_SF,
	MIN(last_update_date) as last_update_date
	from cte_opp_all
	where STADE_VENTE ='15'  and rang=1
	group by IDE_OPPRT_SF)
	--select * from cte_opp_dec 
	/*select IDE_OPPRT_SF, stade_vente, MTT_NML, DTE_ACC_CRE, MTT_DEBLOQUE, DTE_DEBLOCAGE
	from cte_opp_all
	where STADE_VENTE in ('14','15') 
	and rang=1
	order by IDE_OPPRT_SF
	*/
	,cte_opp_acc_parc as (
	select DTE_ALIM,
	case when STADE_VENTE ='14' and DTE_DEBLOCAGE IS NULL AND Lib_RecordType='OB - Crédit' and rang=1 then count(IDE_OPPRT_SF) end as NB_CRE_ACCORDES_NNDEC_PARC,
	case when STADE_VENTE ='15' and DTE_DEBLOCAGE IS not NULL and DTE_DEBLOCAGE<=@date_obs and Lib_RecordType='OB - Crédit' and rang=1 then count(IDE_OPPRT_SF) end as NB_CRE_DECAISSES_PARC, 
	case when STADE_VENTE ='14' and DTE_DEBLOCAGE IS NULL AND Lib_RecordType='OB - Crédit' and rang=1 then MTT_NML end as MTT_NML,
	case when STADE_VENTE ='15' and DTE_DEBLOCAGE IS not NULL and DTE_DEBLOCAGE<=@date_obs and Lib_RecordType='OB - Crédit' and rang=1 then MTT_DEBLOQUE end as MTT_DEBLOQUE --,
	--CASE when STADE_VENTE ='15' and  DTE_DEBLOCAGE IS not NULL and DTE_DEBLOCAGE<=@date_obs and Lib_RecordType='OB - Crédit' and rang=1  then CRD end as Encours_CREDIT_DEC

	from cte_opp_all 
	where STADE_VENTE in ('14','15')  
	AND DTE_ACC_CRE <= @date_obs
	group by /*IDE_OPPRT_SF,*/DTE_ALIM,STADE_VENTE,rang,MTT_NML,MTT_DEBLOQUE,Lib_RecordType,DTE_DEBLOCAGE--,CRD
	)
	--select * from cte_opp_acc_parc 
	,cte_opp_acc_parc_Cumul as (
	select DTE_ALIM,
	SUM(NB_CRE_ACCORDES_NNDEC_PARC)                      as NB_CRE_ACCORDES_NNDEC_PARC,
	SUM(NB_CRE_DECAISSES_PARC)                           as NB_CRE_DECAISSES_PARC,
	--SUM(NB_CRE_ACCORDES_PARC)-SUM(NB_CRE_DECAISSES_PARC) as NB_CRE_ACCORDES_NNDEC_PARC,
	--SUM(MTT_DEBLOQUE)                                    as MTT_CRE_DEC_PARC, 
	SUM(MTT_NML*NB_CRE_ACCORDES_NNDEC_PARC)              as MTT_CRE_ACCOR_NNDEC_PARC--,
	--SUM(Encours_CREDIT_DEC)                              as Encours_CREDIT_DEC
	   
	from cte_opp_acc_parc
	group by DTE_ALIM
	)
	--select * from cte_opp_acc_parc_Cumul  
	/*La semaine de date_obs -7 jours*/

	--Opport.Crédits Créées
	,cte_Opp_Cre_Crees as (
	select DTE_CREA,
	--WeekOfYear,
	COUNT([IDE_OPPRT_SF])                                       as NB_OPP_CRE_CREEES,
	case when [COD_RES_DIS]='01' then COUNT([IDE_OPPRT_SF]) end AS NB_OF,
	case when [COD_RES_DIS]='02' then COUNT([IDE_OPPRT_SF]) end AS NB_OB
	from cte_cre_all
	where Cast([DTE_CREA] as date) BETWEEN DATEADD(dd,-6,@date_obs)  and @date_obs
	and rang = 1
	group by DTE_CREA,WeekOfYear,COD_RES_DIS)
	--select * from cte_Opp_Cre_Crees
	--Opport.Crédits Créées Cumul
	,cte_Opp_Cre_Crees_Cumul AS(
	select DTE_CREA,
	DATEPART(ISO_WEEK,DTE_CREA) as WeekOfYear_c,
	SUM(NB_OPP_CRE_CREEES) as NB_OPP_CRE_CREEES,
	SUM(NB_OF)             as NB_OF,
	SUM(NB_OB)             as NB_OB
	from cte_Opp_Cre_Crees
	group by DTE_CREA,DATEPART(ISO_WEEK,DTE_CREA))
	--select * from cte_Opp_Cre_Crees_Cumul
	--Opport.Crédit En cours de vie
	,Cte_Opp_Cre_En_cours_De_Vie as(
	select DTE_ALIM,
	WeekOfYear,
	--STADE_VENTE,
	CASE when (STADE_VENTE in('01','04','06','07') or (STADE_VENTE='05' and SignatureDate__c is not null) or (STADE_VENTE in('02','03') and SignatureDate__c is null))  then  COUNT([IDE_OPPRT_SF]) end                                                                AS NB_OPP_CRE_EN_COURS,
	CASE when STADE_VENTE='01' then COUNT([IDE_OPPRT_SF])                    end                                                                                                                                                                                       AS NB_DETECTION,
	CASE when STADE_VENTE in('02','03') and SignatureDate__c is null then COUNT([IDE_OPPRT_SF])            end                                                                                                                                                         AS NB_AVANT_SIGNATURE, 
	CASE when STADE_VENTE in('04','06','07') then COUNT([IDE_OPPRT_SF]) end                                                                                                                                                                                            AS NB_APRES_SIGNATURE1,
	CASE when STADE_VENTE in('05') and SignatureDate__c is not null  then COUNT([IDE_OPPRT_SF]) end                                                                                                                                                                    AS NB_APRES_SIGNATURE2,
	--CASE when STADE_VENTE in ('01','02','03','04','05','06','07') then PERCENTILE_CONT (0.5)  WITHIN GROUP (ORDER BY DATEDIFF(DD,DTE_CREA,@date_obs) asc ) OVER (PARTITION BY CASE WHEN STADE_VENTE in ('01','02','03','04','05','06','07') THEN 'STADE_VENTE' END)  end AS ANCIENNETE--,
	DATEDIFF(DD,DTE_CREA,@date_obs)                                                                                                                                                                                                                                    AS ANCIENNETE,
	CASE when STADE_VENTE = '08' then COUNT ([IDE_OPPRT_SF])                 end                                                                                                                                                                                       AS NB_INCIDENT_TECH  
	from cte_opp_all 
	WHERE STADE_VENTE in ('01','02','03','04','05','06','07','08') and rang = 1 
	-- and Cast(DTE_CREA as Date) <= cast(@Date_Obs as Date)-- and  Cast([date_alim] as date) BETWEEN DATEADD(dd,-6,@date_obs)  and @date_obs
	group by IDE_OPPRT_SF,DTE_ALIM,DTE_CREA,WeekOfYear,STADE_VENTE,AGE_OPPRT,SignatureDate__c
	)
	--select * from Cte_Opp_Cre_En_cours_De_Vie where DTE_ALIM='2018-06-03'
 
	,Cte_Opp_Cre_En_cours_De_Vie_Anc1_Cumul as (
	select distinct DTE_ALIM
	,WeekOfYear
	,PERCENTILE_CONT (0.5) WITHIN GROUP (ORDER BY ANCIENNETE asc) OVER (PARTITION BY DTE_ALIM, WeekOfYear) as ANCIENNETE 
	from Cte_Opp_Cre_En_cours_De_Vie
	)
	--select * from Cte_Opp_Cre_En_cours_De_Vie_Anc_Cumul
 
	,Cte_Opp_Cre_En_cours_De_Vie_Cumul as(
	select DTE_ALIM,
	WeekOfYear,
	SUM(NB_OPP_CRE_EN_COURS)                                                                         as NB_OPP_CRE_EN_COURS,
	SUM(NB_DETECTION)                                                                                as NB_DETECTION,
	SUM(NB_AVANT_SIGNATURE)                                                                          as NB_AVANT_SIGNATURE,
	SUM(NB_APRES_SIGNATURE1)+SUM(NB_APRES_SIGNATURE2)                                                as NB_APRES_SIGNATURE,
	--AVG(ANCIENNETE)                                                                                  as ANCIENNETE_MED,
	SUM(NB_INCIDENT_TECH)                                                                            as NB_INCIDENT_TECH 
	from Cte_Opp_Cre_En_cours_De_Vie
	group by DTE_ALIM,WeekOfYear)
	--select * from Cte_Opp_Cre_En_cours_De_Vie_Cumul
	,Cte_Opp_Cre_En_cours_De_Vie_Anc_Cumul as (
	select T.*,
	T1.ANCIENNETE AS ANCIENNETE_MED
	FROM Cte_Opp_Cre_En_cours_De_Vie_Cumul T
	LEFT JOIN Cte_Opp_Cre_En_cours_De_Vie_Anc1_Cumul T1
	ON  T.DTE_ALIM=T1.DTE_ALIM
	)
	--select * from Cte_Opp_Cre_En_cours_De_Vie_Anc_Cumul

	--Opport. Crédit Gagnées
	,Cte_Opp_Cre_Gagnees as(
	select --DTE_CREA,
	DTE_ALIM,
	WeekOfYear,
	CASE when STADE_VENTE='14' and COD_RES_DIS in ('01','02') then  COUNT(y.[IDE_OPPRT_SF])                                 end AS NB_OPP_CRE_ACCO_GAGNEES,
	CASE when STADE_VENTE='14' and COD_RES_DIS='02' then COUNT(y.[IDE_OPPRT_SF])                                            end AS NB_OB_OPP_CRE_ACCO_GAGNEES,
	CASE when STADE_VENTE='14' and COD_RES_DIS='01' then COUNT(y.[IDE_OPPRT_SF])                                            end AS NB_OF_OPP_CRE_ACCO_GAGNEES,
	--CASE when STADE_VENTE='14' and COD_RES_DIS in ('01','02') then DATEDIFF(DD,DTE_CREA,DTE_ACC_CRE)                        end AS DELAI,
	CASE when (STADE_VENTE='14' and COD_RES_DIS in ('01','02')) then PERCENTILE_CONT (0.5)  WITHIN GROUP (ORDER BY DATEDIFF(DD,DTE_CREA,DTE_ACC_CRE)-1 asc) OVER (PARTITION BY STADE_VENTE,DTE_ALIM,CASE WHEN STADE_VENTE='14' THEN STADE_VENTE END, CASE WHEN COD_RES_DIS IN ('01','02') THEN 'COD_RES_DIS'  end ) end AS DELAI
		
	from cte_opp_all y inner join cte_opp_gag on y.IDE_OPPRT_SF=cte_opp_gag.IDE_OPPRT_SF and y.DTE_ALIM=cast(cte_opp_gag.last_update_date as date) 
	where STADE_VENTE in ('14') --and  Cast([DTE_CREA] as date) BETWEEN DATEADD(dd,-6,@date_obs)  and @date_obs
	and rang = 1 
	group by /*DTE_CREA*/ DTE_CREA,DTE_ACC_CRE,DTE_DEBLOCAGE,DTE_ALIM,WeekOfYear,COD_RES_DIS,STADE_VENTE)
	--select * from Cte_Opp_Cre_Gagnees where DTE_ALIM='2018-06-06'
	,Cte_Opp_Cre_Gagnees_Cumul as(
	select --DTE_CREA,
	DTE_ALIM,
	WeekOfYear,
	SUM(NB_OPP_CRE_ACCO_GAGNEES)                                                                                     as NB_OPP_CRE_ACCO_GAGNEES,
	SUM(NB_OB_OPP_CRE_ACCO_GAGNEES)                                                                                  as NB_OB_OPP_CRE_ACCO_GAGNEES,
	SUM(NB_OF_OPP_CRE_ACCO_GAGNEES)                                                                                  as NB_OF_OPP_CRE_ACCO_GAGNEES,
	--IIF(SUM(NB_OPP_CRE_ACCO_GAGNEES)=0,0,SUM(DELAI)/SUM(NB_OPP_CRE_ACCO_GAGNEES))                                    as DELAI_MOY,
	AVG(DELAI) as DELAI_MOY

	from Cte_Opp_Cre_Gagnees
	group by /*DTE_CREA*/ DTE_ALIM,WeekOfYear/*,last_update_date*/)
	--select * from Cte_Opp_Cre_Gagnees_Cumul

	,Cte_Opp_Cre_Gagnees_Cre_DEC as(
	select  DTE_ALIM,
	WeekOfYear,
	/*SUM(*/CASE when STADE_VENTE='15' and COD_RES_DIS in ('01','02') then  1 else 0 end/*)*/                                 AS NB_OPP_CRE_DEC_GAGNEES,
	/*sum(*/CASE when STADE_VENTE='15' and COD_RES_DIS='02' then 1 else 0 end/*)*/                                            AS NB_OB_OPP_CRE_DEC_GAGNEES,
	/*sum(*/CASE when STADE_VENTE='15' and COD_RES_DIS='01' then 1 else 0 end/*)*/                                            AS NB_OF_OPP_CRE_DEC_GAGNEES,
	--CASE when STADE_VENTE='15' and COD_RES_DIS in ('01','02') then DATEDIFF(DD,DTE_ACC_CRE,DTE_DEBLOCAGE)                 end AS DEC_PRET
	/*CASE when (STADE_VENTE='15' and COD_RES_DIS in ('01','02')) then */
	DATEDIFF(DD,DTE_ACC_CRE,DTE_DEBLOCAGE) as delai
	--PERCENTILE_CONT (0.5)  WITHIN GROUP (ORDER BY DATEDIFF(DD,DTE_ACC_CRE,DTE_DEBLOCAGE) asc) OVER (PARTITION BY STADE_VENTE,DTE_ALIM, CASE WHEN COD_RES_DIS in ('01','02') then 'COD_RES_DIS' end ) AS DEC_PRET 
	from cte_opp_all
	where STADE_VENTE ='15' and DTE_ALIM=DTE_DER_DEB
	and rang = 1 
	and cod_res_dis in ('01','02')

	)
	--select * from Cte_Opp_Cre_Gagnees_Cre_DEC
	,Cte_Opp_Cre_Gagnees_Cre_Dec1_Cumul as (
	select distinct dte_alim
	,WeekOfYear
	,PERCENTILE_CONT (0.5) WITHIN GROUP (ORDER BY DELAI asc) OVER (PARTITION BY DTE_ALIM, WeekOfYear) as DEC_PRET_MOY from Cte_Opp_Cre_Gagnees_Cre_DEC
	)
	--select * from Cte_Opp_Cre_Gagnees_Cre_Dec1_Cumul

	,Cte_Opp_Cre_Gagnees_Cre_DEC_Cumul as(
	select DTE_ALIM,
	WeekOfYear,
	SUM(NB_OPP_CRE_DEC_GAGNEES)                                                                                      as NB_OPP_CRE_DEC_GAGNEES,
	SUM(NB_OB_OPP_CRE_DEC_GAGNEES)                                                                                   as NB_OB_OPP_CRE_DEC_GAGNEES,
	SUM(NB_OF_OPP_CRE_DEC_GAGNEES)                                                                                   as NB_OF_OPP_CRE_DEC_GAGNEES

	from Cte_Opp_Cre_Gagnees_Cre_DEC 
	group by /*DTE_CREA*/ DTE_ALIM,WeekOfYear/*,last_update_dte*/)
	--select * from Cte_Opp_Cre_Gagnees_Cre_DEC_Cumul

	,Cte_Opp_Cre_Gagnees_Cre_Decp_Cumul as (
	select Cte_Opp_Cre_Gagnees_Cre_Dec_Cumul.*, 
	test.DEC_PRET_MOY from Cte_Opp_Cre_Gagnees_Cre_Dec_Cumul 
	left join Cte_Opp_Cre_Gagnees_Cre_Dec1_Cumul test
	on Cte_Opp_Cre_Gagnees_Cre_Dec_Cumul.dte_alim=test.DTE_ALIM )

	--select * from Cte_Opp_Cre_Gagnees_Cre_Decp_Cumul

	--Opport.Crédit Perdues
	,Cte_Opp_Cre_Perdues as (
	SELECT --[DTE_CREA],
	DTE_ALIM,
	y.CloseDate,
	WeekOfYear,
	y.IDE_OPPRT_SF,
	COUNT(y.[IDE_OPPRT_SF])																			                 as NB_OPP_CRE_PERDUES,
	case when STADE_VENTE = '16' then COUNT (y.[IDE_OPPRT_SF])									                    end  as NB_ANNULATION,
	case when STADE_VENTE = '16' and DATEDIFF(MM,SignatureDate__c,DTE_CLO_OPPRT)<=6 then COUNT (y.[IDE_OPPRT_SF])	end  as NB_ANNULATION_CLIENT,
	case when STADE_VENTE = '16' and DATEDIFF(DD,SignatureDate__c,DTE_CLO_OPPRT)>14 then COUNT (y.[IDE_OPPRT_SF])	end  as NB_ANNULATION_DEL_RETRACTION,
	case when STADE_VENTE = '12' then COUNT (y.[IDE_OPPRT_SF])									                    end as NB_RETRACTATION,
	case when STADE_VENTE = '10' then COUNT (y.[IDE_OPPRT_SF])									                    end as NB_AFFAIRE_REFUSEE,
	case when STADE_VENTE = '10' and MOT_AFF_REF in ('01','06') then COUNT (y.[IDE_OPPRT_SF])                      end as NB_AFFAIRE_REFUSEE_RISQUE,
	--case when STADE_VENTE = '10' and MOT_AFF_REF='02' then COUNT (y.[IDE_OPPRT_SF])              end as NB_AFFAIRE_REFUSEE_SECFI,--
	--case when STADE_VENTE = '10' and MOT_AFF_REF='03' then COUNT (y.[IDE_OPPRT_SF])              end as NB_AFFAIRE_REFUSEE_Fraude,--
	case when STADE_VENTE = '10' and MOT_AFF_REF='04' then COUNT (y.[IDE_OPPRT_SF])                                end as NB_AFFAIRE_REFUSEE_Suspicion,
	--case when STADE_VENTE = '10' and MOT_AFF_REF='07' then COUNT (y.[IDE_OPPRT_SF])              end as NB_AFFAIRE_REFUSEE_Refus_dossier_Credit,--
	case when STADE_VENTE = '10' and MOT_AFF_REF='08' then COUNT (y.[IDE_OPPRT_SF])                                end as NB_AFFAIRE_REFUSEE_FICP,
	case when STADE_VENTE = '10' and MOT_AFF_REF='09' then COUNT (y.[IDE_OPPRT_SF])                                end as NB_AFFAIRE_REFUSEE_Refus_Engagement,
	case when STADE_VENTE ='10'  and MOT_AFF_REF in ('02','03','07') then COUNT (y.[IDE_OPPRT_SF])                 end as NB_AFFAIRE_REFUSEE_Autres_Motifs,
	--case when STADE_VENTE = '08' then COUNT (y.[IDE_OPPRT_SF])                                   end as NB_INCIDENT_TECH,
	case when STADE_VENTE = '06' then COUNT (y.[IDE_OPPRT_SF])                                                     end as NB_DOS_NN_CONFORME,
	case when STADE_VENTE = '11' then COUNT (y.[IDE_OPPRT_SF])                                                     end as NB_SANS_SUITE 
	--MIN(last_update_date)                                                                          as last_update_date 
	from cte_opp_all y inner join cte_opp_per on y.IDE_OPPRT_SF=cte_opp_per.IDE_OPPRT_SF and y.DTE_ALIM=cast(cte_opp_per.CloseDate as date)                                   
	where STADE_VENTE IN('16','12','10','06','11') --and  Cast([DTE_CREA] as date) BETWEEN DATEADD(dd,-6,@date_obs)  and @date_obs
	and rang = 1
	group by /*DTE_CREA*/ DTE_ALIM,y.CloseDate,WeekOfYear,STADE_VENTE,MOT_AFF_REF,SignatureDate__c,DTE_CLO_OPPRT,y.IDE_OPPRT_SF)
	--select * from Cte_Opp_Cre_Perdues where DTE_ALIM='2018-06-06'
	,Cte_Opp_Cre_Perdues_Cumul as(
	SELECT --[DTE_CREA],
	DTE_ALIM,
	CloseDate,
	WeekOfYear,
	SUM(NB_OPP_CRE_PERDUES)                      as NB_OPP_CRE_PERDUES,
	SUM(NB_ANNULATION)                           as NB_ANNULATION,
	SUM(NB_ANNULATION_CLIENT)                    as NB_ANNULATION_CLIENT,
	SUM(NB_ANNULATION_DEL_RETRACTION)            as NB_ANNULATION_DEL_RETRACTION,
	SUM(NB_RETRACTATION)                         as NB_RETRACTATION,
	SUM(NB_AFFAIRE_REFUSEE)                      as NB_AFFAIRE_REFUSEE,
	SUM(NB_AFFAIRE_REFUSEE_RISQUE)               as NB_AFFAIRE_REFUSEE_RISQUE,
	--SUM(NB_AFFAIRE_REFUSEE_SECFI)                as NB_AFFAIRE_REFUSEE_SECFI,
	--SUM(NB_AFFAIRE_REFUSEE_Fraude)               as NB_AFFAIRE_REFUSEE_Fraude,
	SUM(NB_AFFAIRE_REFUSEE_Suspicion)            as NB_AFFAIRE_REFUSEE_Suspicion,
	--SUM(NB_AFFAIRE_REFUSEE_Refus_dossier_Credit) as NB_AFFAIRE_REFUSEE_Refus_dossier_Credit,
	SUM(NB_AFFAIRE_REFUSEE_FICP)                 as NB_AFFAIRE_REFUSEE_FICP,
	SUM(NB_AFFAIRE_REFUSEE_Refus_Engagement)     as NB_AFFAIRE_REFUSEE_Refus_Engagement,
	SUM(NB_AFFAIRE_REFUSEE_Autres_Motifs)        as NB_AFFAIRE_REFUSEE_Autres_Motifs,
	--SUM(NB_INCIDENT_TECH)                        as NB_INCIDENT_TECH,
	SUM(NB_DOS_NN_CONFORME)                      as NB_DOS_NN_CONFORME,
	SUM(NB_SANS_SUITE)                           as NB_SANS_SUITE
	--last_update_date                             as last_update_date
	from Cte_Opp_Cre_Perdues
	group by /*[DTE_CREA]*/ DTE_ALIM,WeekOfYear,CloseDate/*,last_update_date*/)
	--select * from Cte_Opp_Cre_Perdues_Cumul

	--Crédits Conso.
	,ff_acc as
	(
	select t2.date_alim, DATEPART(ISO_WEEK,date_alim)  as [WeekOfYear], t1.IDE_OPPRT_SF, t1.MTT_NML, t1.MTT_TOT_DEB, t1.COD_OBJ_FIN, T1.DTE_ACC_TOKOS, t1.DTE_DER_DEB, t1.DELAI_DECAISSEMENT, t1.TX_INT_PAL, t1.TX_VEN, t2.Cod_StageName,t2.Lib_StageName, t2.last_update_date
	,CAST(LoanAcceptationDate__c as date) as [DTE_ACC_CRE],DTE_CREA,Cod_DistributionChannel__c
	,DATEDIFF(DD,cast(CreatedDate as date),CAST(DTE_ACC_TOKOS as date)) as delai_accord
   
	
	from  [dbo].[CRE_CREDIT] t1 left join (select *, rank() over (partition by date_alim, Id_SF order by last_update_date desc) as rang 
	from dbo.CRE_WK_STOCK_OPPORTUNITY) t2 on t1.IDE_OPPRT_SF = t2.Id_SF
	where t1.DTE_ACC_TOKOS = t2.date_alim
	and t2.Cod_StageName = '14'
	and rang=1
	and COD_OBJ_FIN in ('00086','00087','00085','00082') 
	--order by t2.date_alim, t1.IDE_OPPRT_SF
	)
	--select * from ff_acc
	,ind_ff_acc as
	(
	select date_alim
	,WeekOfYear
	, count(ff_acc.IDE_OPPRT_SF)                                                                                                                                                                                      as NB_CRE_CONSO_ACCORDES
	,case when count(ff_ACC.IDE_OPPRT_SF) = 0 then 0.00 else convert(decimal(7,2),(cast(SUM(case when ff_acc.delai_accord between 0 and 2 then 1 else 0 end) as decimal(7,2)) / count(ff_acc.IDE_OPPRT_SF))*100) end  as Tx_CRE_CONSO_ACCORDES_2j
	,case when count(ff_acc.IDE_OPPRT_SF) = 0 then 0.00 else convert(decimal(7,2),(cast(SUM(case when [Cod_DistributionChannel__c]='01' then 1 else 0 end) as decimal(7,2)) / count(ff_acc.IDE_OPPRT_SF))*100) end    as Tx_CRE_CONSO_ACCORDES_Self
	   
	,CASE when count(ff_acc.IDE_OPPRT_SF) = 0 then 0 else sum(ff_acc.MTT_NML) end                                                                                                                               as MTT_CRE_ACCORDE
	,convert(decimal(15,2),case when count(ff_acc.IDE_OPPRT_SF) = 0 then 0 else sum(ff_acc.MTT_NML) / count(ff_acc.IDE_OPPRT_SF) end)                                                                           as MTT_MOY_ACCORDE
	,CASE when count(ff_acc.IDE_OPPRT_SF) = 0 then 0 else convert(decimal(7,2),sum(case when ff_acc.TX_INT_PAL > ff_acc.TX_VEN then ff_acc.TX_INT_PAL else ff_acc.TX_VEN end) / count(ff_acc.IDE_OPPRT_SF)) end as TX_ACC
	,COUNT(COD_OBJ_FIN)                                                        as NB_OBJ_FIN
	, sum(case when ff_acc.COD_OBJ_FIN in ('00086','00087') then 1 else 0 end) as NB_AUTO_VN_VO
	, sum(case when ff_acc.COD_OBJ_FIN in ('00082') then 1 else 0 end)         as NB_TRAV
	, sum(case when ff_acc.COD_OBJ_FIN in ('00085') then 1 else 0 end)         as NB_PROJ
	, sum(case when ff_acc.COD_OBJ_FIN in ('00086','00087','00085','00082') then  ff_acc.MTT_NML else 0 end ) AS MTT_OBJ_FIN
	, sum(case when ff_acc.COD_OBJ_FIN in ('00086','00087') then ff_acc.MTT_NML else 0 end)                   as MTT_AUTO_VN_VO
	, sum(case when ff_acc.COD_OBJ_FIN in ('00085') then ff_acc.MTT_NML else 0 end)                           as MTT_PROJ
	, sum(case when ff_acc.COD_OBJ_FIN in ('00082') then ff_acc.MTT_NML else 0 end)                           as MTT_TRAV
	from cte_jour_alim_list 
	left join ff_acc on cte_jour_alim_list.jour_alim = ff_acc.date_alim
	group by date_alim,WeekOfYear)
	--select * from ind_ff_acc

	,ind_ff_acc_cumul as
	(
	select date_alim 
	,WeekOfYear
	,SUM(NB_CRE_CONSO_ACCORDES)       as NB_CRE_CONSO_ACCORDES
	,SUM(Tx_CRE_CONSO_ACCORDES_2j)    as Tx_CRE_CONSO_ACCORDES_2j
	,SUM(Tx_CRE_CONSO_ACCORDES_Self)  as Tx_CRE_CONSO_ACCORDES_Self
	,SUM(MTT_CRE_ACCORDE)             as MTT_CRE_ACCORDE
	,SUM(MTT_MOY_ACCORDE)             as MTT_MOY_ACCORDE
	,SUM(TX_ACC)                      as TX_ACC
	,SUM(NB_OBJ_FIN)                  as NB_OBJ_FIN
	,SUM(NB_AUTO_VN_VO)               as NB_AUTO_VN_VO
	,SUM(NB_TRAV)                     as NB_TRAV
	,SUM(NB_PROJ)                     as NB_PROJ
	,SUM(MTT_OBJ_FIN)                 as MTT_OBJ_FIN
	,SUM(MTT_AUTO_VN_VO)              as MTT_AUTO_VN_VO
	,SUM(MTT_TRAV)                    as MTT_TRAV
	,SUM(MTT_PROJ)                    as MTT_PROJ
	from ind_ff_acc
	group by date_alim,WeekOfYear
	)
	--select * from ind_ff_acc_cumul

	,ff_dec as
	(
	select t2.date_alim,DATEPART(ISO_WEEK,date_alim)  as [WeekOfYear], t1.IDE_OPPRT_SF, t1.MTT_NML, t1.MTT_TOT_DEB, t1.COD_OBJ_FIN, T1.DTE_ACC_TOKOS, t1.DTE_DER_DEB, t1.DELAI_DECAISSEMENT, t1.TX_INT_PAL, t1.TX_VEN, t2.Cod_StageName,t2.Lib_StageName, t2.last_update_date
	from  [dbo].[CRE_CREDIT] t1 left join (select *, rank() over (partition by date_alim, Id_SF order by last_update_date desc) as rang 
	from dbo.CRE_WK_STOCK_OPPORTUNITY) t2 on t1.IDE_OPPRT_SF = t2.Id_SF
	where t1.DTE_DER_DEB = t2.date_alim
	and t2.Cod_StageName = '15'
	and rang=1
	and COD_OBJ_FIN in ('00086','00087','00085','00082') 
	)
	--select * from ff_dec

	, ind_ff_dec as (
	select date_alim
	, WeekOfYear
	, count(ff_dec.IDE_OPPRT_SF)                                                                                                                                                                                                as NB_CRE_CONSO_DECAISSES
	, case when count(ff_dec.IDE_OPPRT_SF) = 0 then 0.00 else convert(decimal(7,2),(cast(SUM(case when ff_dec.DELAI_DECAISSEMENT between 0 and 8 then 1 else 0 end) as decimal(7,2)) / count(ff_dec.IDE_OPPRT_SF))*100) end     as RATIO_CRE_CONSO_DECAISSES_8j
	, case when count(ff_dec.IDE_OPPRT_SF) = 0 then 0.00 else convert(decimal(7,2),(1-(cast(SUM(case when ff_dec.DELAI_DECAISSEMENT between 0 and 8 then 1 else 0 end) as decimal(7,2)) / count(ff_dec.IDE_OPPRT_SF)))*100) end as RATIO_CRE_CONSO_DECAISSES_9j
	, case when count(ff_dec.IDE_OPPRT_SF) = 0 then 0.00 else convert(decimal(7,2),SUM(case when MTT_TOT_DEB = MTT_NML then 1 else 0 end)/count(ff_dec.IDE_OPPRT_SF)*100) end                                                   as RATIO_CRE_CONSO_DECAISSES_TOT
	, case when count(ff_dec.IDE_OPPRT_SF) = 0 then 0.00 else convert(decimal(7,2),SUM(case when MTT_TOT_DEB < MTT_NML then 1 else 0 end)/count(ff_dec.IDE_OPPRT_SF)*100) end                                                   as RATIO_CRE_CONSO_DECAISSES_PART
	, case when count(ff_dec.IDE_OPPRT_SF) = 0 then 0 else sum(ff_dec.MTT_TOT_DEB) end                                                                                                                                          as MTT_CRE_DECAISSE
	, convert(decimal(15,2),case when count(ff_dec.IDE_OPPRT_SF) = 0 then 0 else sum(ff_dec.MTT_TOT_DEB) / count(ff_dec.IDE_OPPRT_SF) end)                                                                                      as MTT_MOY_DECAISSE
	, case when count(ff_dec.IDE_OPPRT_SF) = 0 then 0 else convert(decimal(7,2),sum(case when ff_dec.TX_INT_PAL > ff_dec.TX_VEN then ff_dec.TX_INT_PAL else ff_dec.TX_VEN end) / count(ff_dec.IDE_OPPRT_SF)) end                as TX_CLIENT_CRE_DEC
	
	, COUNT(COD_OBJ_FIN) as NB_OBJ_FIN_DEC
	, sum(case when ff_dec.COD_OBJ_FIN in ('00086','00087') then 1 else 0 end)                                as NB_AUTO_VN_VO_DEC
	, sum(case when ff_dec.COD_OBJ_FIN in ('00082') then 1 else 0 end)                                        as NB_TRAV_DEC
	, sum(case when ff_dec.COD_OBJ_FIN in ('00085') then 1 else 0 end)                                        as NB_PROJ_DEC

	, sum(case when ff_dec.COD_OBJ_FIN in ('00086','00087','00085','00082') then  ff_dec.MTT_NML else 0 end ) AS MTT_OBJ_FIN_DEC
	, sum(case when ff_dec.COD_OBJ_FIN in ('00086','00087') then ff_dec.mtt_tot_deb else 0 end)               as MTT_AUTO_VN_VO_DEC
	, sum(case when ff_dec.COD_OBJ_FIN in ('00085') then ff_dec.mtt_tot_deb else 0 end)                       as MTT_PROJ_DEC
	, sum(case when ff_dec.COD_OBJ_FIN in ('00082') then ff_dec.mtt_tot_deb else 0 end)                       as MTT_TRAV_DEC

	from cte_jour_alim_list 
	left join ff_dec on cte_jour_alim_list.jour_alim = ff_dec.date_alim
	group by date_alim,WeekOfYear

	)
	--select * from ind_ff_dec

	, ind_ff_dec_cumul as (
	select date_alim 
	,WeekOfYear
	,SUM(NB_CRE_CONSO_DECAISSES)         as NB_CRE_CONSO_DECAISSES
	,SUM(RATIO_CRE_CONSO_DECAISSES_8j)   as RATIO_CRE_CONSO_DECAISSES_8j
	,SUM(RATIO_CRE_CONSO_DECAISSES_9j)   as RATIO_CRE_CONSO_DECAISSES_9j
	,SUM(RATIO_CRE_CONSO_DECAISSES_TOT)  as RATIO_CRE_CONSO_DECAISSES_TOT
	,SUM(RATIO_CRE_CONSO_DECAISSES_PART) as RATIO_CRE_CONSO_DECAISSES_PART
	,SUM(MTT_CRE_DECAISSE)               as MTT_CRE_DECAISSE
	,SUM(MTT_MOY_DECAISSE)               as MTT_MOY_DECAISSE
	,SUM(TX_CLIENT_CRE_DEC)              as TX_CLIENT_CRE_DEC
	,SUM(NB_OBJ_FIN_DEC)                 as NB_OBJ_FIN_DEC
	,SUM(NB_AUTO_VN_VO_DEC)              as NB_AUTO_VN_VO_DEC
	,SUM(NB_TRAV_DEC)                    as NB_TRAV_DEC
	,SUM(NB_PROJ_DEC)                    as NB_PROJ_DEC
	,SUM(MTT_OBJ_FIN_DEC)                as MTT_OBJ_FIN_DEC
	,SUM(MTT_AUTO_VN_VO_DEC)             as MTT_AUTO_VN_VO_DEC
	,SUM(MTT_TRAV_DEC)                   as MTT_TRAV_DEC
	,SUM(MTT_PROJ_DEC)                   as MTT_PROJ_DEC

	from ind_ff_dec
	group by date_alim,WeekOfYear
	)
	--select * from ind_ff_dec_cumul

	, ind_pret_parc as
	(
	select cte_jour_alim_list.jour_alim
	, sum(case when DTE_ACC_TOKOS is not null and DTE_ACC_TOKOS <= cte_jour_alim_list.jour_alim and (DTE_DER_DEB is null or DTE_DER_DEB  > cte_jour_alim_list.jour_alim)  then 1 else 0 end) as NB_CRE_ACCORDES_NNDEC_PARC
	, sum(case when DTE_ACC_TOKOS is not null and DTE_ACC_TOKOS <= cte_jour_alim_list.jour_alim and (DTE_DER_DEB is null or DTE_DER_DEB  > cte_jour_alim_list.jour_alim) then MTT_NML/1000 else 0 end) as MTT_CRE_ACCOR_NNDEC_PARC
	, sum(case when DTE_DER_DEB is not null and DTE_DER_DEB <= cte_jour_alim_list.jour_alim then 1 else 0 end) as NB_CRE_DECAISSES_PARC
	, sum(case when DTE_DER_DEB is not null and DTE_DER_DEB <= cte_jour_alim_list.jour_alim then MTT_NML/1000 else 0 end) as MTT_CRE_DECAISSES_PARC
	from [dbo].[CRE_CREDIT] inner join cte_jour_alim_list on 1=1
	group by cte_jour_alim_list.jour_alim
	)
	--select * from ind_pret_parc

	/*Le 1er jour de la semaine de date_obs jusqu'à date_obs */
	--Opport.Crédits Créées
	,cte_Opp_Cre_Crees_Cumul_Hebdo AS(
	select WeekOfYear_c,
	SUM(NB_OPP_CRE_CREEES) as NB_OPP_CRE_CREEES_HEBDO,
	SUM(NB_OF)             as NB_OF_HEBDO,
	SUM(NB_OB)             as NB_OB_HEBDO
	from cte_Opp_Cre_Crees_Cumul
	where DTE_CREA >= @FirstDOW and DTE_CREA<=@date_obs
	group by WeekOfYear_c)
	--select * from cte_Opp_Cre_Crees_Cumul_Hebdo

	--Opport.Crédit En cours de vie
	,Cte_Opp_Cre_En_cours_De_Vie_Cumul_Hebdo as(
	select WeekOfYear,
	SUM(NB_OPP_CRE_EN_COURS)                                                                         as NB_OPP_CRE_EN_COURS_HEBDO,
	SUM(NB_DETECTION)                                                                                as NB_DETECTION_HEBDO,
	SUM(NB_AVANT_SIGNATURE)                                                                          as NB_AVANT_SIGNATURE_HEBDO,
	SUM(NB_APRES_SIGNATURE)                                                                          as NB_APRES_SIGNATURE_HEBDO,
	--SUM(ANCIENNETE_MED)/(datediff(dd,@FirstDOW,@date_obs) +1 )                                     as ANCIENNETE_MED_HEBDO,
	SUM(NB_INCIDENT_TECH)                                                                            as NB_INCIDENT_TECH_HEBDO 
	from Cte_Opp_Cre_En_cours_De_Vie_Cumul
	where DTE_ALIM >= @FirstDOW and DTE_ALIM<=@date_obs
	group by WeekOfYear)
	--select * from Cte_Opp_Cre_En_cours_De_Vie_Cumul_Hebdo

	,Cte_Opp_Cre_En_cours_De_Vie_Anc_Cumul_Hebdo as (
	select WeekOfYear,
	SUM(NB_OPP_CRE_EN_COURS)                                                                         as NB_OPP_CRE_EN_COURS_HEBDO,
	SUM(NB_DETECTION)                                                                                as NB_DETECTION_HEBDO,
	SUM(NB_AVANT_SIGNATURE)                                                                          as NB_AVANT_SIGNATURE_HEBDO,
	SUM(NB_APRES_SIGNATURE)                                                                          as NB_APRES_SIGNATURE_HEBDO,
	SUM(ANCIENNETE_MED)/datediff(dd,@FirstDOW,@date_obs)                                            as ANCIENNETE_MED_HEBDO, 
	SUM(NB_INCIDENT_TECH)                                                                            as NB_INCIDENT_TECH_HEBDO 
	from Cte_Opp_Cre_En_cours_De_Vie_Anc_Cumul
	where DTE_ALIM >= @FirstDOW and DTE_ALIM<=@date_obs
	group by WeekOfYear
	)
	--select * from Cte_Opp_Cre_En_cours_De_Vie_Anc_Cumul_Hebdo 

	--Opport. Crédit Gagnées
	,Cte_Opp_Cre_Gagnees_Cumul_Hebdo as(
	select --DTE_CREA,
	WeekOfYear,
	SUM(NB_OPP_CRE_ACCO_GAGNEES)         as NB_OPP_CRE_ACCO_GAGNEES_HEBDO,
	SUM(NB_OB_OPP_CRE_ACCO_GAGNEES)      as NB_OB_OPP_CRE_ACCO_GAGNEES_HEBDO,
	SUM(NB_OF_OPP_CRE_ACCO_GAGNEES)      as NB_OF_OPP_CRE_ACCO_GAGNEES_HEBDO,
	SUM(DELAI_MOY)                       as DELAI_MOY_HEBDO

	from Cte_Opp_Cre_Gagnees_Cumul
	where DTE_ALIM >= @FirstDOW and DTE_ALIM<=@date_obs
	group by WeekOfYear/*,last_update_date*/)
	--select * from Cte_Opp_Cre_Gagnees_Cumul_Hebdo

	,Cte_Opp_Cre_Gagnees_Cre_DECp_Cumul_Hebdo as(
	select WeekOfYear,
	SUM(NB_OPP_CRE_DEC_GAGNEES)                                                                                      as NB_OPP_CRE_DEC_GAGNEES_HEBDO,
	SUM(NB_OB_OPP_CRE_DEC_GAGNEES)                                                                                   as NB_OB_OPP_CRE_DEC_GAGNEES_HEBDO,
	SUM(NB_OF_OPP_CRE_DEC_GAGNEES)                                                                                   as NB_OF_OPP_CRE_DEC_GAGNEES_HEBDO,
	--IIF(SUM(NB_OPP_CRE_DEC_GAGNEES)=0,0,SUM(DEC_PRET)/SUM(NB_OPP_CRE_DEC_GAGNEES))                                   as DEC_PRET_MOY
	SUM(DEC_PRET_MOY)/datediff(dd,@FirstDOW,@date_obs)                                                               as DEC_PRET_MOY_HEBDO
	--last_update_date        as last_update_date
	from Cte_Opp_Cre_Gagnees_Cre_Decp_Cumul
	where DTE_ALIM >= @FirstDOW and DTE_ALIM<=@date_obs
	group by /*DTE_CREA*/ WeekOfYear/*,last_update_date*/)
	--select * from Cte_Opp_Cre_Gagnees_Cre_Decp_Cumul_Hebdo

	--Opport.Crédit Perdues
	,Cte_Opp_Cre_Perdues_Cumul_Hebdo as(
	SELECT --[DTE_CREA],
	WeekOfYear,
	SUM(NB_OPP_CRE_PERDUES)                      as NB_OPP_CRE_PERDUES_HEBDO,
	SUM(NB_ANNULATION)                           as NB_ANNULATION_HEBDO,
	SUM(NB_ANNULATION_CLIENT)                    as NB_ANNULATION_CLIENT_HEBDO,
	SUM(NB_ANNULATION_DEL_RETRACTION)            as NB_ANNULATION_DEL_RETRACTION_HEBDO,
	SUM(NB_RETRACTATION)                         as NB_RETRACTATION_HEBDO,
	SUM(NB_AFFAIRE_REFUSEE)                      as NB_AFFAIRE_REFUSEE_HEBDO,
	SUM(NB_AFFAIRE_REFUSEE_RISQUE)               as NB_AFFAIRE_REFUSEE_RISQUE_HEBDO,
	--SUM(NB_AFFAIRE_REFUSEE_SECFI)                as NB_AFFAIRE_REFUSEE_SECFI_HEBDO,
	--SUM(NB_AFFAIRE_REFUSEE_Fraude)               as NB_AFFAIRE_REFUSEE_Fraude_HEBDO,
	SUM(NB_AFFAIRE_REFUSEE_Suspicion)            as NB_AFFAIRE_REFUSEE_Suspicion_HEBDO,
	--SUM(NB_AFFAIRE_REFUSEE_Refus_dossier_Credit) as NB_AFFAIRE_REFUSEE_Refus_dossier_Credit_HEBDO,
	SUM(NB_AFFAIRE_REFUSEE_FICP)                 as NB_AFFAIRE_REFUSEE_FICP_HEBDO,
	SUM(NB_AFFAIRE_REFUSEE_Refus_Engagement)     as NB_AFFAIRE_REFUSEE_Refus_Engagement_HEBDO,
	SUM(NB_AFFAIRE_REFUSEE_Autres_Motifs)        as NB_AFFAIRE_REFUSEE_Autres_Motifs_HEBDO,
	--SUM(NB_INCIDENT_TECH)                        as NB_INCIDENT_TECH_HEBDO,
	SUM(NB_DOS_NN_CONFORME)                      as NB_DOS_NN_CONFORME_HEBDO,
	SUM(NB_SANS_SUITE)                           as NB_SANS_SUITE_HEBDO
	--last_update_date                             as last_update_date_HEBDO
	from Cte_Opp_Cre_Perdues_Cumul
	where DTE_ALIM >= @FirstDOW and DTE_ALIM<=@date_obs
	group by WeekOfYear/*,last_update_date*/)
	--select * from Cte_Opp_Cre_Perdues_Cumul_Hebdo

	--Crédits Conso.
	,ind_ff_acc_cumul_Hebdo as (
	select  WeekOfYear
	,SUM(NB_CRE_CONSO_ACCORDES)                                        as NB_CRE_CONSO_ACCORDES_HEBDO
	,SUM(Tx_CRE_CONSO_ACCORDES_2j)/datediff(dd,@FirstDOW,@date_obs)    as Tx_CRE_CONSO_ACCORDES_2j_HEBDO
	,SUM(Tx_CRE_CONSO_ACCORDES_Self)/datediff(dd,@FirstDOW,@date_obs)  as Tx_CRE_CONSO_ACCORDES_Self_HEBDO
	,SUM(MTT_CRE_ACCORDE)                                              as MTT_CRE_ACCORDE_HEBDO
	,SUM(MTT_MOY_ACCORDE)                                              as MTT_MOY_ACCORDE_HEBDO
	,SUM(TX_ACC)/datediff(dd,@FirstDOW,@date_obs)                      as TX_ACC_HEBDO
	,SUM(NB_OBJ_FIN)                                                   as NB_OBJ_FIN_HEBDO
	,SUM(NB_AUTO_VN_VO)                                                as NB_AUTO_VN_VO_HEBDO
	,SUM(NB_TRAV)                                                      as NB_TRAV_HEBDO
	,SUM(NB_PROJ)                                                      as NB_PROJ_HEBDO
	,SUM(MTT_OBJ_FIN)                                                  as MTT_OBJ_FIN_HEBDO
	,SUM(MTT_AUTO_VN_VO)                                               as MTT_AUTO_VN_VO_HEBDO
	,SUM(MTT_TRAV)                                                     as MTT_TRAV_HEBDO
	,SUM(MTT_PROJ)                                                     as MTT_PROJ_HEBDO
	from ind_ff_acc_cumul
	where date_alim >= @FirstDOW and date_alim<=@date_obs
	group by WeekOfYear/*,last_update_date*/)
	--select * from ind_ff_acc_cumul_Hebdo 

	,ind_ff_dec_cumul_Hebdo as(
	select WeekOfYear
	,SUM(NB_CRE_CONSO_DECAISSES)                                          as NB_CRE_CONSO_DECAISSES_HEBDO
	,SUM(RATIO_CRE_CONSO_DECAISSES_8j)/datediff(dd,@FirstDOW,@date_obs)   as RATIO_CRE_CONSO_DECAISSES_8j_HEBDO
	,SUM(RATIO_CRE_CONSO_DECAISSES_9j)/datediff(dd,@FirstDOW,@date_obs)    as RATIO_CRE_CONSO_DECAISSES_9j_HEBDO
	,SUM(RATIO_CRE_CONSO_DECAISSES_TOT)/datediff(dd,@FirstDOW,@date_obs)  as RATIO_CRE_CONSO_DECAISSES_TOT_HEBDO
	,SUM(RATIO_CRE_CONSO_DECAISSES_PART)/datediff(dd,@FirstDOW,@date_obs) as RATIO_CRE_CONSO_DECAISSES_PART_HEBDO
	,SUM(MTT_CRE_DECAISSE)                                                 as MTT_CRE_DECAISSE_HEBDO
	,SUM(MTT_MOY_DECAISSE)                                                 as MTT_MOY_DECAISSE_HEBDO
	,SUM(TX_CLIENT_CRE_DEC)                                                as TX_CLIENT_CRE_DEC_HEBDO
	,SUM(NB_OBJ_FIN_DEC)                                                   as NB_OBJ_FIN_DEC_HEBDO
	,SUM(NB_AUTO_VN_VO_DEC)                                                as NB_AUTO_VN_VO_DEC_HEBDO
	,SUM(NB_TRAV_DEC)                                                      as NB_TRAV_DEC_HEBDO
	,SUM(NB_PROJ_DEC)                                                      as NB_PROJ_DEC_HEBDO
	,SUM(MTT_OBJ_FIN_DEC)                                                  as MTT_OBJ_FIN_DEC_HEBDO
	,SUM(MTT_AUTO_VN_VO_DEC)                                               as MTT_AUTO_VN_VO_DEC_HEBDO
	,SUM(MTT_TRAV_DEC)                                                     as MTT_TRAV_DEC_HEBDO
	,SUM(MTT_PROJ_DEC)                                                     as MTT_PROJ_DEC_HEBDO

	from ind_ff_dec_cumul
	where date_alim >= @FirstDOW and date_alim<=@date_obs
	group by WeekOfYear/*,last_update_date*/)
	--select * from ind_ff_dec_cumul_Hebdo
	/*Cumul Hebdo de la semaine précédente*/
	--Opport credit creees
	,cte_Opp_Cre_Crees_Hebdo_Prec AS(
	select DTE_CREA,
	--WeekOfYear,
	COUNT([IDE_OPPRT_SF])                                       as NB_OPP_CRE_CREEES_Hebdo_Prec,
	case when [COD_RES_DIS]='01' then COUNT([IDE_OPPRT_SF]) end AS NB_OF_Hebdo_Prec,
	case when [COD_RES_DIS]='02' then COUNT([IDE_OPPRT_SF]) end AS NB_OB_Hebdo_Prec
	from cte_cre_all
	where DTE_CREA between @FirstDOPW and DATEADD(DD,6, @FirstDOPW)
	and rang = 1
	group by DTE_CREA,WeekOfYear,COD_RES_DIS)
	--select * from cte_Opp_Cre_Crees_Hebdo_Prec
	,cte_Opp_Cre_Crees_Cumul_Hebdo_Prec AS(
	select DATEPART(ISO_WEEK,DTE_CREA) as WeekOfYear_c,
	SUM(NB_OPP_CRE_CREEES_Hebdo_Prec) as NB_OPP_CRE_CREEES_Hebdo_Prec,
	SUM(NB_OF_Hebdo_Prec)             as NB_OF_Hebdo_Prec,
	SUM(NB_OB_Hebdo_Prec)             as NB_OB_Hebdo_Prec
	from cte_Opp_Cre_Crees_Hebdo_Prec
	group by DATEPART(ISO_WEEK,DTE_CREA))
	--select * from cte_Opp_Cre_Crees_Cumul_Hebdo_Prec

	--Opport.Crédit En cours de vie
	,Cte_Opp_Cre_En_cours_De_Vie_Hebdo_Prec as(
	select DTE_ALIM,
	WeekOfYear,
	--STADE_VENTE,
	CASE when (STADE_VENTE in('01','04','06','07') or (STADE_VENTE='05' and SignatureDate__c is not null) or (STADE_VENTE in('02','03') and SignatureDate__c is null))  then  COUNT([IDE_OPPRT_SF]) end                                                                AS NB_OPP_CRE_EN_COURS_HEBDO_PREC,
	CASE when STADE_VENTE='01' then COUNT([IDE_OPPRT_SF])                    end                                                                                                                                                                                       AS NB_DETECTION_HEBDO_PREC,
	CASE when STADE_VENTE in('02','03') and SignatureDate__c is null then COUNT([IDE_OPPRT_SF])            end                                                                                                                                                         AS NB_AVANT_SIGNATURE_HEBDO_PREC, 
	CASE when STADE_VENTE in('04','06','07') then COUNT([IDE_OPPRT_SF]) end                                                                                                                                                                                            AS NB_APRES_SIGNATURE1_HEBDO_PREC,
	CASE when STADE_VENTE in('05') and SignatureDate__c is not null  then COUNT([IDE_OPPRT_SF]) end                                                                                                                                                                    AS NB_APRES_SIGNATURE2_HEBDO_PREC,
	--CASE when STADE_VENTE in ('01','02','03','04','05','06','07') then PERCENTILE_CONT (0.5)  WITHIN GROUP (ORDER BY DATEDIFF(DD,DTE_CREA,@date_obs) asc ) OVER (PARTITION BY CASE WHEN STADE_VENTE in ('01','02','03','04','05','06','07') THEN STADE_VENTE END)  end AS ANCIENNETE_HEBDO_PREC,
	DATEDIFF(DD,DTE_CREA,@date_obs)                                                                                                                                                                                                                                    AS ANCIENNETE_HEBDO_PREC,
	CASE when STADE_VENTE = '08' then COUNT ([IDE_OPPRT_SF])                 end                                                                                                                                                                                       AS NB_INCIDENT_TECH_HEBDO_PREC  
	from cte_opp_all 
	WHERE STADE_VENTE in ('01','02','03','04','05','06','07','08') and rang = 1 
	and DTE_ALIM between @FirstDOPW and DATEADD(DD,6, @FirstDOPW)
	-- and Cast(DTE_CREA as Date) <= cast(@Date_Obs as Date)-- and  Cast([date_alim] as date) BETWEEN DATEADD(dd,-6,@date_obs)  and @date_obs
	group by IDE_OPPRT_SF,DTE_ALIM,DTE_CREA,WeekOfYear,STADE_VENTE,AGE_OPPRT,SignatureDate__c
	)
	--select * from Cte_Opp_Cre_En_cours_De_Vie_Hebdo_Prec

	,Cte_Opp_Cre_En_cours_De_Vie_Anc1_Cumul_Hebdo_Prec as (
	select distinct DTE_ALIM
	,WeekOfYear
	,PERCENTILE_CONT (0.5) WITHIN GROUP (ORDER BY ANCIENNETE_HEBDO_PREC asc) OVER (PARTITION BY DTE_ALIM, WeekOfYear) as ANCIENNETE_Hebdo_Prec 
	from Cte_Opp_Cre_En_cours_De_Vie_Hebdo_Prec
	)
	--select * from Cte_Opp_Cre_En_cours_De_Vie_Anc1_Cumul_Hebdo_Prec

	,Cte_Opp_Cre_En_cours_De_Vie_Cumul_Hebdo_Prec as(
	select WeekOfYear,
	SUM(NB_OPP_CRE_EN_COURS_HEBDO_PREC)                                                                         as NB_OPP_CRE_EN_COURS_HEBDO_PREC,
	SUM(NB_DETECTION_HEBDO_PREC)                                                                                as NB_DETECTION_HEBDO_PREC,
	SUM(NB_AVANT_SIGNATURE_HEBDO_PREC)                                                                          as NB_AVANT_SIGNATURE_HEBDO_PREC,
	SUM(NB_APRES_SIGNATURE1_HEBDO_PREC)+SUM(NB_APRES_SIGNATURE2_HEBDO_PREC)                                     as NB_APRES_SIGNATURE_HEBDO_PREC,
	--SUM(ANCIENNETE_HEBDO_PREC)/7                                                                                  as ANCIENNETE_MED_HEBDO_PREC,
	SUM(NB_INCIDENT_TECH_HEBDO_PREC)                                                                            as NB_INCIDENT_TECH_HEBDO_PREC 
	from Cte_Opp_Cre_En_cours_De_Vie_Hebdo_Prec 
	group by WeekOfYear
	)
	--select * from Cte_Opp_Cre_En_cours_De_Vie_Cumul_Hebdo_Prec

	,Cte_Opp_Cre_En_cours_De_Vie_Anc_Cumul_Hebdo_Prec as (
	select T.*,
	SUM(T1.ANCIENNETE_Hebdo_Prec)/7        as ANCIENNETE_MED_HEBDO_Prec
	from Cte_Opp_Cre_En_cours_De_Vie_Cumul_Hebdo_Prec T 
	left join Cte_Opp_Cre_En_cours_De_Vie_Anc1_Cumul_Hebdo_Prec T1
	on T.WeekOfYear=T1.WeekOfYear 
	group by T.WeekOfYear,
	T.NB_OPP_CRE_EN_COURS_HEBDO_PREC,
	T.NB_DETECTION_HEBDO_PREC,
	T.NB_AVANT_SIGNATURE_HEBDO_PREC,
	T.NB_APRES_SIGNATURE_HEBDO_PREC,
	T.NB_INCIDENT_TECH_HEBDO_PREC
	)
	--select * from Cte_Opp_Cre_En_cours_De_Vie_Anc_Cumul_Hebdo_Prec

	--Opport. Crédit Gagnées
	,Cte_Opp_Cre_Gagnees_Hebdo_Prec as(
	select DTE_ALIM,
	WeekOfYear,
	CASE when STADE_VENTE='14' and COD_RES_DIS in ('01','02') then  COUNT(y.[IDE_OPPRT_SF])                                 end AS NB_OPP_CRE_ACCO_GAGNEES_HEBDO_PREC,
	CASE when STADE_VENTE='14' and COD_RES_DIS='02' then COUNT(y.[IDE_OPPRT_SF])                                            end AS NB_OB_OPP_CRE_ACCO_GAGNEES_HEBDO_PREC,
	CASE when STADE_VENTE='14' and COD_RES_DIS='01' then COUNT(y.[IDE_OPPRT_SF])                                            end AS NB_OF_OPP_CRE_ACCO_GAGNEES_HEBDO_PREC,
	CASE when STADE_VENTE='14' and COD_RES_DIS in ('01','02') then DATEDIFF(DD,DTE_CREA,DTE_ACC_CRE)                        end AS DELAI_HEBDO_PREC

		
	--MIN(last_update_date)                                      as last_update_date_Hebdo_Prec
	from cte_opp_all y inner join cte_opp_gag on y.IDE_OPPRT_SF=cte_opp_gag.IDE_OPPRT_SF and y.DTE_ALIM=cast(cte_opp_gag.last_update_date as date)
	where STADE_VENTE in ('14') and DTE_ALIM between @FirstDOPW and DATEADD(DD,6, @FirstDOPW)
	and rang = 1
	group by /*DTE_CREA*/DTE_ALIM,WeekOfYear,COD_RES_DIS,STADE_VENTE,DTE_CREA,DTE_ACC_CRE,DTE_DEBLOCAGE)
	--select * from Cte_Opp_Cre_Gagnees_Hebdo_Prec
	,Cte_Opp_Cre_Gagnees_Cumul_Hebdo_Prec as(
	select WeekOfYear,
	SUM(NB_OPP_CRE_ACCO_GAGNEES_HEBDO_PREC)                                                                                                 as NB_OPP_CRE_ACCO_GAGNEES_HEBDO_PREC,
	SUM(NB_OB_OPP_CRE_ACCO_GAGNEES_HEBDO_PREC)                                                                                              as NB_OB_OPP_CRE_ACCO_GAGNEES_HEBDO_PREC,
	SUM(NB_OF_OPP_CRE_ACCO_GAGNEES_HEBDO_PREC)                                                                                              as NB_OF_OPP_CRE_ACCO_GAGNEES_HEBDO_PREC,
	IIF(SUM(NB_OPP_CRE_ACCO_GAGNEES_HEBDO_PREC)=0,0,SUM(DELAI_HEBDO_PREC)/SUM(NB_OPP_CRE_ACCO_GAGNEES_HEBDO_PREC))                          as DELAI_MOY_HEBDO_PREC
		
	from Cte_Opp_Cre_Gagnees_Hebdo_Prec
	group by WeekOfYear/*,last_update_date_HEBDO_Prec*/)
	--select * from Cte_Opp_Cre_Gagnees_Cumul_Hebdo_Prec

	,Cte_Opp_Cre_Gagnees_Cre_DEC_Hebdo_Prec as(
	select  DTE_ALIM,
	WeekOfYear,
	CASE when STADE_VENTE='15' and COD_RES_DIS in ('01','02') then  1 else 0 end                                 AS NB_OPP_CRE_DEC_GAGNEES_HEBDO_PREC,
	CASE when STADE_VENTE='15' and COD_RES_DIS='02' then  1 else 0 end                                           AS NB_OB_OPP_CRE_DEC_GAGNEES_HEBDO_PREC,
	CASE when STADE_VENTE='15' and COD_RES_DIS='01' then  1 else 0 end                                           AS NB_OF_OPP_CRE_DEC_GAGNEES_HEBDO_PREC,
	DATEDIFF(DD,DTE_ACC_CRE,DTE_DEBLOCAGE)                                                                       AS DELAI_HEBDO_PREC

	--CASE when STADE_VENTE='15' and COD_RES_DIS in ('01','02') then DATEDIFF(DD,DTE_ACC_CRE,DTE_DEBLOCAGE)                 end AS DEC_PRET
	--CASE when (STADE_VENTE='15' and COD_RES_DIS in ('01','02')) then PERCENTILE_CONT (0.5)  WITHIN GROUP (ORDER BY DATEDIFF(DD,DTE_ACC_CRE,DTE_DEBLOCAGE) asc) OVER (PARTITION BY STADE_VENTE,DTE_ALIM,CASE WHEN STADE_VENTE='15' THEN STADE_VENTE end ) end AS DEC_PRET_HEBDO_PREC 

	from cte_opp_all
	where STADE_VENTE ='15' and DTE_ALIM= DTE_DER_DEB --and  Cast([DTE_CREA] as date) BETWEEN DATEADD(dd,-6,@date_obs)  and @date_obs
	and DTE_ALIM between @FirstDOPW and DATEADD(DD,6, @FirstDOPW)
	and rang = 1 
	)
	--select * from Cte_Opp_Cre_Gagnees_Cre_DEC_Hebdo_Prec

	,Cte_Opp_Cre_Gagnees_Cre_Dec1_Cumul_Hebdo_Prec as (
	select distinct dte_alim
	,WeekOfYear
	,PERCENTILE_CONT (0.5) WITHIN GROUP (ORDER BY DELAI_HEBDO_PREC asc) OVER (PARTITION BY DTE_ALIM, WeekOfYear) as DEC_PRET_MOY from Cte_Opp_Cre_Gagnees_Cre_DEC_Hebdo_Prec
	)
	--select * from Cte_Opp_Cre_Gagnees_Cre_Dec1_Cumul_Hebdo_Prec
	,Cte_Opp_Cre_Gagnees_Cre_DEC_Cumul_Hebdo_Prec as(
	select WeekOfYear,
	SUM(NB_OPP_CRE_DEC_GAGNEES_HEBDO_PREC)                                                                           as NB_OPP_CRE_DEC_GAGNEES_HEBDO_PREC,
	SUM(NB_OB_OPP_CRE_DEC_GAGNEES_HEBDO_PREC)                                                                        as NB_OB_OPP_CRE_DEC_GAGNEES_HEBDO_PREC,
	SUM(NB_OF_OPP_CRE_DEC_GAGNEES_HEBDO_PREC)                                                                        as NB_OF_OPP_CRE_DEC_GAGNEES_HEBDO_PREC
	--SUM(DEC_PRET_HEBDO_PREC)/7                                                                                         as DEC_PRET_MOY_HEBDO_PREC
	from Cte_Opp_Cre_Gagnees_Cre_DEC_Hebdo_Prec
	group by /*DTE_CREA*/ WeekOfYear/*,last_update_date*/)
	--select * from Cte_Opp_Cre_Gagnees_Cre_Dec_Cumul_Hebdo_Prec

	,Cte_Opp_Cre_Gagnees_Cre_DECp_Cumul_Hebdo_Prec as(
	select t.*
	,SUM(test.DEC_PRET_MOY)/7 as DEC_PRET_MOY_HEBDO_PREC 
	from Cte_Opp_Cre_Gagnees_Cre_DEC_Cumul_Hebdo_Prec t left join Cte_Opp_Cre_Gagnees_Cre_Dec1_Cumul_Hebdo_Prec test
	on t.WeekOfYear=test.WeekOfYear
	group by t.WeekOfYear,
	t.NB_OPP_CRE_DEC_GAGNEES_HEBDO_PREC,
	t.NB_OB_OPP_CRE_DEC_GAGNEES_HEBDO_PREC,
	t.NB_OF_OPP_CRE_DEC_GAGNEES_HEBDO_PREC
	)
	--select * from Cte_Opp_Cre_Gagnees_Cre_DEC_Cumul_Hebdo_Prec--A voir urgent

	----Opport.Crédit Perdues
	,Cte_Opp_Cre_Perdues_Hebdo_Prec as (
	SELECT --[DTE_CREA],
	DTE_ALIM,
	WeekOfYear,
	COUNT(y.[IDE_OPPRT_SF])																			                 as NB_OPP_CRE_PERDUES_HEBDO_PREC,
	case when STADE_VENTE = '16' then COUNT (y.[IDE_OPPRT_SF])									                    end  as NB_ANNULATION_HEBDO_PREC,
	case when STADE_VENTE = '16' and DATEDIFF(MM,SignatureDate__c,DTE_CLO_OPPRT)<=6 then COUNT (y.[IDE_OPPRT_SF])	end  as NB_ANNULATION_CLIENT_HEBDO_PREC,
	case when STADE_VENTE = '16' and DATEDIFF(DD,SignatureDate__c,DTE_CLO_OPPRT)>14 then COUNT (y.[IDE_OPPRT_SF])	end  as NB_ANNULATION_DEL_RETRACTION_HEBDO_PREC,
	case when STADE_VENTE = '12' then COUNT (y.[IDE_OPPRT_SF])									                    end as NB_RETRACTATION_HEBDO_PREC,
	case when STADE_VENTE = '10' then COUNT (y.[IDE_OPPRT_SF])									                    end as NB_AFFAIRE_REFUSEE_HEBDO_PREC,
	case when STADE_VENTE = '10' and MOT_AFF_REF in ('01','06') then COUNT (y.[IDE_OPPRT_SF])                      end as NB_AFFAIRE_REFUSEE_RISQUE_HEBDO_PREC,
	--case when STADE_VENTE = '10' and MOT_AFF_REF='02' then COUNT (y.[IDE_OPPRT_SF])              end as NB_AFFAIRE_REFUSEE_SECFI_HEBDO_PREC,--
	--case when STADE_VENTE = '10' and MOT_AFF_REF='03' then COUNT (y.[IDE_OPPRT_SF])              end as NB_AFFAIRE_REFUSEE_Fraude_HEBDO_PREC,--
	case when STADE_VENTE = '10' and MOT_AFF_REF='04' then COUNT (y.[IDE_OPPRT_SF])                                end as NB_AFFAIRE_REFUSEE_Suspicion_HEBDO_PREC,
	--case when STADE_VENTE = '10' and MOT_AFF_REF='07' then COUNT (y.[IDE_OPPRT_SF])              end as NB_AFFAIRE_REFUSEE_Refus_dossier_Credit_HEBDO_PREC,--
	case when STADE_VENTE = '10' and MOT_AFF_REF='08' then COUNT (y.[IDE_OPPRT_SF])                                end as NB_AFFAIRE_REFUSEE_FICP_HEBDO_PREC,
	case when STADE_VENTE = '10' and MOT_AFF_REF='09' then COUNT (y.[IDE_OPPRT_SF])                                end as NB_AFFAIRE_REFUSEE_Refus_Engagement_HEBDO_PREC,
	case when STADE_VENTE ='10'  and MOT_AFF_REF in ('02','03','07') then COUNT (y.[IDE_OPPRT_SF])                 end as NB_AFFAIRE_REFUSEE_Autres_Motifs_HEBDO_PREC,
	--case when STADE_VENTE = '08' then COUNT (y.[IDE_OPPRT_SF])                                   end as NB_INCIDENT_TECH_HEBDO_PREC,
	case when STADE_VENTE = '06' then COUNT (y.[IDE_OPPRT_SF])                                                     end as NB_DOS_NN_CONFORME_HEBDO_PREC,
	case when STADE_VENTE = '11' then COUNT (y.[IDE_OPPRT_SF])                                                     end as NB_SANS_SUITE_HEBDO_PREC 
	--MIN(last_update_date)                                                                          as last_update_date 
	from cte_opp_all y inner join cte_opp_per on y.IDE_OPPRT_SF=cte_opp_per.IDE_OPPRT_SF and y.DTE_ALIM=cast(cte_opp_per.CloseDate as date)
	where STADE_VENTE IN('16','12','10','11') and DTE_ALIM between @FirstDOPW and DATEADD(DD,6, @FirstDOPW)
	and rang = 1
	group by /*DTE_CREA*/DTE_ALIM,WeekOfYear,STADE_VENTE,MOT_AFF_REF,SignatureDate__c,DTE_CLO_OPPRT)
	--select * from Cte_Opp_Cre_Perdues_Hebdo_Prec
	,Cte_Opp_Cre_Perdues_Cumul_Hebdo_Prec as(
	SELECT WeekOfYear,
	SUM(NB_OPP_CRE_PERDUES_HEBDO_PREC)                      as NB_OPP_CRE_PERDUES_HEBDO_PREC,
	SUM(NB_ANNULATION_HEBDO_PREC)                           as NB_ANNULATION_HEBDO_PREC,
	SUM(NB_ANNULATION_CLIENT_HEBDO_PREC)                    as NB_ANNULATION_CLIENT_HEBDO_PREC,
	SUM(NB_ANNULATION_DEL_RETRACTION_HEBDO_PREC)            as NB_ANNULATION_DEL_RETRACTION_HEBDO_PREC,
	SUM(NB_RETRACTATION_HEBDO_PREC)                         as NB_RETRACTATION_HEBDO_PREC,
	SUM(NB_AFFAIRE_REFUSEE_HEBDO_PREC)                      as NB_AFFAIRE_REFUSEE_HEBDO_PREC,
	SUM(NB_AFFAIRE_REFUSEE_RISQUE_HEBDO_PREC)               as NB_AFFAIRE_REFUSEE_RISQUE_HEBDO_PREC,
	--SUM(NB_AFFAIRE_REFUSEE_SECFI_HEBDO_PREC)                as NB_AFFAIRE_REFUSEE_SECFI_HEBDO_PREC,
	--SUM(NB_AFFAIRE_REFUSEE_Fraude_HEBDO_PREC)               as NB_AFFAIRE_REFUSEE_Fraude_HEBDO_PREC,
	SUM(NB_AFFAIRE_REFUSEE_Suspicion_HEBDO_PREC)            as NB_AFFAIRE_REFUSEE_Suspicion_HEBDO_PREC,
	--SUM(NB_AFFAIRE_REFUSEE_Refus_dossier_Credit_HEBDO_PREC) as NB_AFFAIRE_REFUSEE_Refus_dossier_Credit_HEBDO_PREC,
	SUM(NB_AFFAIRE_REFUSEE_FICP_HEBDO_PREC)                 as NB_AFFAIRE_REFUSEE_FICP_HEBDO_PREC,
	SUM(NB_AFFAIRE_REFUSEE_Refus_Engagement_HEBDO_PREC)     as NB_AFFAIRE_REFUSEE_Refus_Engagement_HEBDO_PREC,
	SUM(NB_AFFAIRE_REFUSEE_Autres_Motifs_HEBDO_PREC)        as NB_AFFAIRE_REFUSEE_Autres_Motifs_HEBDO_PREC,
	--SUM(NB_INCIDENT_TECH_HEBDO_PREC)                        as NB_INCIDENT_TECH_HEBDO_PREC,
	SUM(NB_DOS_NN_CONFORME_HEBDO_PREC)                      as NB_DOS_NN_CONFORME_HEBDO_PREC,
	SUM(NB_SANS_SUITE_HEBDO_PREC)                           as NB_SANS_SUITE_HEBDO_PREC
	--last_update_date_HEBDO_PREC                             as last_update_date_HEBDO_PREC   
	from Cte_Opp_Cre_Perdues_Hebdo_Prec
	group by WeekOfYear/*,last_update_date_HEBDO_Prec*/)
	--select * from Cte_Opp_Cre_Perdues_Cumul_Hebdo_Prec

	--Crédits Conso.
	,ind_ff_acc_Hebdo_Prec as (
	select date_alim
	,WeekOfYear
	,count(ff_acc.IDE_OPPRT_SF)                                                                                                                                                                                       as NB_CRE_CONSO_ACCORDES_HEBDO_PREC
	,case when count(ff_ACC.IDE_OPPRT_SF) = 0 then 0.00 else convert(decimal(7,2),(cast(SUM(case when ff_acc.delai_accord between 0 and 2 then 1 else 0 end) as decimal(7,2)) / count(ff_acc.IDE_OPPRT_SF))*100) end  as Tx_CRE_CONSO_ACCORDES_2j_HEBDO_PREC
	,case when count(ff_acc.IDE_OPPRT_SF) = 0 then 0.00 else convert(decimal(7,2),(cast(SUM(case when [Cod_DistributionChannel__c]='01' then 1 else 0 end) as decimal(7,2)) / count(ff_acc.IDE_OPPRT_SF))*100) end    as Tx_CRE_CONSO_ACCORDES_Self_HEBDO_PREC
	,CASE when count(ff_acc.IDE_OPPRT_SF) = 0 then 0 else sum(ff_acc.MTT_NML) end                                                                                                                                     as MTT_CRE_ACCORDE_HEBDO_PREC
	,convert(decimal(15,2),case when count(ff_acc.IDE_OPPRT_SF) = 0 then 0 else sum(ff_acc.MTT_NML) / count(ff_acc.IDE_OPPRT_SF) end)                                                                                 as MTT_MOY_ACCORDE_HEBDO_PREC
	,CASE when count(ff_acc.IDE_OPPRT_SF) = 0 then 0 else convert(decimal(7,2),sum(case when ff_acc.TX_INT_PAL > ff_acc.TX_VEN then ff_acc.TX_INT_PAL else ff_acc.TX_VEN end) / count(ff_acc.IDE_OPPRT_SF)) end       as TX_ACC_HEBDO_PREC
	,COUNT(COD_OBJ_FIN)                                                                                      as NB_OBJ_FIN_HEBDO_PREC
	,sum(case when ff_acc.COD_OBJ_FIN in ('00086','00087') then 1 else 0 end)                                as NB_AUTO_VN_VO_HEBDO_PREC
	,sum(case when ff_acc.COD_OBJ_FIN in ('00082') then 1 else 0 end)                                        as NB_TRAV_HEBDO_PREC
	,sum(case when ff_acc.COD_OBJ_FIN in ('00085') then 1 else 0 end)                                        as NB_PROJ_HEBDO_PREC
	,sum(case when ff_acc.COD_OBJ_FIN in ('00086','00087','00085','00082') then  ff_acc.MTT_NML else 0 end ) AS MTT_OBJ_FIN_HEBDO_PREC
	,sum(case when ff_acc.COD_OBJ_FIN in ('00086','00087') then ff_acc.MTT_NML else 0 end)                   as MTT_AUTO_VN_VO_HEBDO_PREC
	,sum(case when ff_acc.COD_OBJ_FIN in ('00085') then ff_acc.MTT_NML else 0 end)                           as MTT_PROJ_HEBDO_PREC
	,sum(case when ff_acc.COD_OBJ_FIN in ('00082') then ff_acc.MTT_NML else 0 end)                           as MTT_TRAV_HEBDO_PREC
	from cte_jour_alim_list 
	left join ff_acc on cte_jour_alim_list.jour_alim = ff_acc.date_alim
	where date_alim between @FirstDOPW and DATEADD(DD,6, @FirstDOPW) 
	group by date_alim,WeekOfYear)
	--select * from ind_ff_acc_Hebdo_Prec
	,ind_ff_acc_cumul_Hebdo_Prec as (
	select WeekOfYear
	,SUM(NB_CRE_CONSO_ACCORDES_HEBDO_PREC)         as NB_CRE_CONSO_ACCORDES_HEBDO_PREC
	,SUM(Tx_CRE_CONSO_ACCORDES_2j_HEBDO_PREC)/7    as Tx_CRE_CONSO_ACCORDES_2j_HEBDO_PREC
	,SUM(Tx_CRE_CONSO_ACCORDES_Self_HEBDO_PREC)/7  as Tx_CRE_CONSO_ACCORDES_Self_HEBDO_PREC
	,SUM(MTT_CRE_ACCORDE_HEBDO_PREC)               as MTT_CRE_ACCORDE_HEBDO_PREC
	,SUM(MTT_MOY_ACCORDE_HEBDO_PREC)               as MTT_MOY_ACCORDE_HEBDO_PREC
	,SUM(TX_ACC_HEBDO_PREC)/7                      as TX_ACC_HEBDO_PREC
	,SUM(NB_OBJ_FIN_HEBDO_PREC)                    as NB_OBJ_FIN_HEBDO_PREC
	,SUM(NB_AUTO_VN_VO_HEBDO_PREC)                 as NB_AUTO_VN_VO_HEBDO_PREC
	,SUM(NB_TRAV_HEBDO_PREC)                       as NB_TRAV_HEBDO_PREC
	,SUM(NB_PROJ_HEBDO_PREC)                       as NB_PROJ_HEBDO_PREC
	,SUM(MTT_OBJ_FIN_HEBDO_PREC)                   as MTT_OBJ_FIN_HEBDO_PREC
	,SUM(MTT_AUTO_VN_VO_HEBDO_PREC)                as MTT_AUTO_VN_VO_HEBDO_PREC
	,SUM(MTT_TRAV_HEBDO_PREC)                      as MTT_TRAV_HEBDO_PREC
	,SUM(MTT_PROJ_HEBDO_PREC)                      as MTT_PROJ_HEBDO_PREC
	from ind_ff_acc_Hebdo_Prec
	group by WeekOfYear/*,last_update_date_HEBDO_Prec*/)
	--select * from ind_ff_acc_cumul_Hebdo_Prec
	,ind_ff_dec_Hebdo_Prec as(
	select date_alim
	, WeekOfYear
	, count(ff_dec.IDE_OPPRT_SF)                                                                                                                                                                                                as NB_CRE_CONSO_DECAISSES_HEBDO_PREC
	, case when count(ff_dec.IDE_OPPRT_SF) = 0 then 0.00 else convert(decimal(7,2),(cast(SUM(case when ff_dec.DELAI_DECAISSEMENT between 0 and 8 then 1 else 0 end) as decimal(7,2)) / count(ff_dec.IDE_OPPRT_SF))*100) end     as RATIO_CRE_CONSO_DECAISSES_8j_HEBDO_PREC
	, case when count(ff_dec.IDE_OPPRT_SF) = 0 then 0.00 else convert(decimal(7,2),(1-(cast(SUM(case when ff_dec.DELAI_DECAISSEMENT between 0 and 8 then 1 else 0 end) as decimal(7,2)) / count(ff_dec.IDE_OPPRT_SF)))*100) end as RATIO_CRE_CONSO_DECAISSES_9j_HEBDO_PREC
	, case when count(ff_dec.IDE_OPPRT_SF) = 0 then 0.00 else convert(decimal(7,2),SUM(case when MTT_TOT_DEB = MTT_NML then 1 else 0 end)/count(ff_dec.IDE_OPPRT_SF)*100) end                                                   as RATIO_CRE_CONSO_DECAISSES_TOT_HEBDO_PREC
	, case when count(ff_dec.IDE_OPPRT_SF) = 0 then 0.00 else convert(decimal(7,2),SUM(case when MTT_TOT_DEB < MTT_NML then 1 else 0 end)/count(ff_dec.IDE_OPPRT_SF)*100) end                                                   as RATIO_CRE_CONSO_DECAISSES_PART_HEBDO_PREC
	, case when count(ff_dec.IDE_OPPRT_SF) = 0 then 0 else sum(ff_dec.MTT_TOT_DEB) end                                                                                                                                          as MTT_CRE_DECAISSE_HEBDO_PREC
	, convert(decimal(15,2),case when count(ff_dec.IDE_OPPRT_SF) = 0 then 0 else sum(ff_dec.MTT_TOT_DEB) / count(ff_dec.IDE_OPPRT_SF) end)                                                                                      as MTT_MOY_DECAISSE_HEBDO_PREC
	, case when count(ff_dec.IDE_OPPRT_SF) = 0 then 0 else convert(decimal(7,2),sum(case when ff_dec.TX_INT_PAL > ff_dec.TX_VEN then ff_dec.TX_INT_PAL else ff_dec.TX_VEN end) / count(ff_dec.IDE_OPPRT_SF)) end                as TX_CLIENT_CRE_DEC_HEBDO_PREC
	
	, COUNT(COD_OBJ_FIN)                                                                                      as NB_OBJ_FIN_DEC_HEBDO_PREC
	, sum(case when ff_dec.COD_OBJ_FIN in ('00086','00087') then 1 else 0 end)                                as NB_AUTO_VN_VO_DEC_HEBDO_PREC
	, sum(case when ff_dec.COD_OBJ_FIN in ('00082') then 1 else 0 end)                                        as NB_TRAV_DEC_HEBDO_PREC
	, sum(case when ff_dec.COD_OBJ_FIN in ('00085') then 1 else 0 end)                                        as NB_PROJ_DEC_HEBDO_PREC

	, sum(case when ff_dec.COD_OBJ_FIN in ('00086','00087','00085','00082') then  ff_dec.MTT_NML else 0 end ) AS MTT_OBJ_FIN_DEC_HEBDO_PREC
	, sum(case when ff_dec.COD_OBJ_FIN in ('00086','00087') then ff_dec.mtt_tot_deb else 0 end)               as MTT_AUTO_VN_VO_DEC_HEBDO_PREC
	, sum(case when ff_dec.COD_OBJ_FIN in ('00085') then ff_dec.mtt_tot_deb else 0 end)                       as MTT_PROJ_DEC_HEBDO_PREC
	, sum(case when ff_dec.COD_OBJ_FIN in ('00082') then ff_dec.mtt_tot_deb else 0 end)                       as MTT_TRAV_DEC_HEBDO_PREC

	from cte_jour_alim_list left join ff_dec on cte_jour_alim_list.jour_alim = ff_dec.date_alim
	where  date_alim between @FirstDOPW and DATEADD(DD,6, @FirstDOPW)
	group by date_alim,WeekOfYear)
	--SELECT * FROM ind_ff_dec_Hebdo_Prec
	,ind_ff_dec_Cumul_Hebdo_Prec as(
	select WeekOfYear
	,SUM(NB_CRE_CONSO_DECAISSES_HEBDO_PREC)           as NB_CRE_CONSO_DECAISSES_HEBDO_PREC
	,SUM(RATIO_CRE_CONSO_DECAISSES_8j_HEBDO_PREC)/7   as RATIO_CRE_CONSO_DECAISSES_8j_HEBDO_PREC
	,SUM(RATIO_CRE_CONSO_DECAISSES_9j_HEBDO_PREC)/7   as RATIO_CRE_CONSO_DECAISSES_9j_HEBDO_PREC
	,SUM(RATIO_CRE_CONSO_DECAISSES_TOT_HEBDO_PREC)/7  as RATIO_CRE_CONSO_DECAISSES_TOT_HEBDO_PREC
	,SUM(RATIO_CRE_CONSO_DECAISSES_PART_HEBDO_PREC)/7 as RATIO_CRE_CONSO_DECAISSES_PART_HEBDO_PREC
	,SUM(MTT_CRE_DECAISSE_HEBDO_PREC)                 as MTT_CRE_DECAISSE_HEBDO_PREC
	,SUM(MTT_MOY_DECAISSE_HEBDO_PREC)                 as MTT_MOY_DECAISSE_HEBDO_PREC
	,SUM(TX_CLIENT_CRE_DEC_HEBDO_PREC)/7              as TX_CLIENT_CRE_DEC_HEBDO_PREC
	,SUM(NB_OBJ_FIN_DEC_HEBDO_PREC)                   as NB_OBJ_FIN_DEC_HEBDO_PREC
	,SUM(NB_AUTO_VN_VO_DEC_HEBDO_PREC)                as NB_AUTO_VN_VO_DEC_HEBDO_PREC
	,SUM(NB_TRAV_DEC_HEBDO_PREC)                      as NB_TRAV_DEC_HEBDO_PREC
	,SUM(NB_PROJ_DEC_HEBDO_PREC)                      as NB_PROJ_DEC_HEBDO_PREC
	,SUM(MTT_OBJ_FIN_DEC_HEBDO_PREC)                  as MTT_OBJ_FIN_DEC_HEBDO_PREC
	,SUM(MTT_AUTO_VN_VO_DEC_HEBDO_PREC)               as MTT_AUTO_VN_VO_DEC_HEBDO_PREC
	,SUM(MTT_TRAV_DEC_HEBDO_PREC)                     as MTT_TRAV_DEC_HEBDO_PREC
	,SUM(MTT_PROJ_DEC_HEBDO_PREC)                     as MTT_PROJ_DEC_HEBDO_PREC

	from ind_ff_dec_Hebdo_Prec
	group by WeekOfYear)
	--select * from ind_ff_dec_Cumul_Hebdo_Prec
	/******************_*_Requête_*_******************/
	----Opport.Crédits Créées

	select distinct x.jour_alim,
	ISNULL(a.NB_OPP_CRE_CREEES,0) as NB_OPP_CRE_CREEES,
	ISNULL(a.NB_OF,0) as NB_OF,
	ISNULL(a.NB_OB,0) as NB_OB,
	case when ROW_NUMBER() OVER(PARTITION BY b.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(b.NB_OPP_CRE_CREEES_HEBDO,0) else 0 end as NB_OPP_CRE_CREEES_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY b.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(b.NB_OF_HEBDO,0)             else 0 end as NB_OF_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY b.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(b.NB_OB_HEBDO,0)             else 0 end as NB_OB_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY c.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(c.NB_OPP_CRE_CREEES_Hebdo_Prec,0)else 0 end  as NB_OPP_CRE_CREEES_Hebdo_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY c.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(c.NB_OF_Hebdo_Prec,0)            else 0 end  as NB_OF_Hebdo_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY c.WeekOfYear_c order by x.jour_alim desc)=1 then ISNULL(c.NB_OB_Hebdo_Prec,0)            else 0 end  as NB_OB_Hebdo_Prec,
	----Opport.Crédit En cours de vie
	ISNULL(v.NB_OPP_CRE_EN_COURS,0) as NB_OPP_CRE_EN_COURS,
	ISNULL(v.NB_DETECTION,0)        as NB_DETECTION,
	ISNULL(v.NB_AVANT_SIGNATURE,0)  as NB_AVANT_SIGNATURE,
	ISNULL(v.NB_APRES_SIGNATURE,0)  as NB_APRES_SIGNATURE,
	ISNULL(v.ANCIENNETE_MED,0)      as ANCIENNETE_MED,
	ISNULL(v.NB_INCIDENT_TECH,0)    as NB_INCIDENT_TECH, 

	case when ROW_NUMBER() OVER(PARTITION BY vh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vh.NB_OPP_CRE_EN_COURS_HEBDO,0)else 0 end  as NB_OPP_CRE_EN_COURS_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY vh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vh.NB_DETECTION_HEBDO,0)       else 0 end  as NB_DETECTION_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY vh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vh.NB_AVANT_SIGNATURE_HEBDO,0) else 0 end  as NB_AVANT_SIGNATURE_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY vh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vh.NB_APRES_SIGNATURE_HEBDO,0) else 0 end  as NB_APRES_SIGNATURE_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY vh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vh.ANCIENNETE_MED_HEBDO,0)     else 0 end  as ANCIENNETE_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY vh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vh.NB_INCIDENT_TECH_HEBDO,0)   else 0 end  as NB_INCIDENT_TECH_HEBDO,

	case when ROW_NUMBER() OVER(PARTITION BY vhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vhp.NB_OPP_CRE_EN_COURS_HEBDO_Prec,0)else 0 end  as NB_OPP_CRE_EN_COURS_HEBDO_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY vhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vhp.NB_DETECTION_HEBDO_Prec,0)       else 0 end  as NB_DETECTION_HEBDO_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY vhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vhp.NB_AVANT_SIGNATURE_HEBDO_Prec,0) else 0 end  as NB_AVANT_SIGNATURE_HEBDO_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY vhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vhp.NB_APRES_SIGNATURE_HEBDO_Prec,0) else 0 end  as NB_APRES_SIGNATURE_HEBDO_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY vhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vhp.ANCIENNETE_MED_HEBDO_Prec,0)     else 0 end  as ANCIENNETE_HEBDO_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY vh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(vhp.NB_INCIDENT_TECH_HEBDO_PREC ,0)   else 0 end  as NB_INCIDENT_TECH_HEBDO_Prec,
	--Opport. Crédit Gagnées
	ISNULL(g.NB_OPP_CRE_ACCO_GAGNEES,0)         as NB_OPP_CRE_ACCO_GAGNEES, 
	ISNULL(g.NB_OB_OPP_CRE_ACCO_GAGNEES,0)      as NB_OB_OPP_CRE_ACCO_GAGNEES,
	ISNULL(g.NB_OF_OPP_CRE_ACCO_GAGNEES,0)      as NB_OF_OPP_CRE_ACCO_GAGNEES,
	ISNULL(g.DELAI_MOY,0)                       as DELAI_MOY,
	ISNULL(gd.NB_OPP_CRE_DEC_GAGNEES,0)          as NB_OPP_CRE_DEC_GAGNEES,
	ISNULL(gd.NB_OB_OPP_CRE_DEC_GAGNEES,0)       as NB_OB_OPP_CRE_DEC_GAGNEES,
	ISNULL(gd.NB_OF_OPP_CRE_DEC_GAGNEES,0)       as NB_OF_OPP_CRE_DEC_GAGNEES,
	ISNULL(gd.DEC_PRET_MOY,0)                    as DEC_PRET_MOY,
	--ISNULL(g.last_update_date,CAST('19000101' as date))   as last_update_date,
	   
	case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(gh.NB_OPP_CRE_ACCO_GAGNEES_HEBDO,0)          else 0 end as NB_OPP_CRE_ACCO_GAGNEES_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(gh.NB_OB_OPP_CRE_ACCO_GAGNEES_HEBDO,0)       else 0 end as NB_OB_OPP_CRE_ACCO_GAGNEES_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(gh.NB_OF_OPP_CRE_ACCO_GAGNEES_HEBDO,0)       else 0 end as NB_OF_OPP_CRE_ACCO_GAGNEES_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(gh.DELAI_MOY_HEBDO,0)                        else 0 end as DELAI_MOY_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY gdh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(gdh.NB_OPP_CRE_DEC_GAGNEES_HEBDO,0)         else 0 end as NB_OPP_CRE_DEC_GAGNEES_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY gdh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(gdh.NB_OB_OPP_CRE_DEC_GAGNEES_HEBDO,0)      else 0 end as NB_OB_OPP_CRE_DEC_GAGNEES_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY gdh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(gdh.NB_OF_OPP_CRE_DEC_GAGNEES_HEBDO,0)      else 0 end as NB_OF_OPP_CRE_DEC_GAGNEES_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY gdh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(gdh.DEC_PRET_MOY_HEBDO,0)                   else 0 end as DEC_PRET_MOY_HEBDO,

	--case when ROW_NUMBER() OVER(PARTITION BY gh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(gh.last_update_date_HEBDO,CAST('19000101' as date))  else CAST('19000101' as date) end as last_update_date_HEBDO,
	   

	case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ghp.NB_OPP_CRE_ACCO_GAGNEES_HEBDO_PREC,0)          else 0 end as NB_OPP_CRE_ACCO_GAGNEES_HEBDO_PREC,
	case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ghp.NB_OB_OPP_CRE_ACCO_GAGNEES_HEBDO_PREC,0)       else 0 end as NB_OB_OPP_CRE_ACCO_GAGNEES_HEBDO_PREC,
	case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ghp.NB_OF_OPP_CRE_ACCO_GAGNEES_HEBDO_PREC,0)       else 0 end as NB_OF_OPP_CRE_ACCO_GAGNEES_HEBDO_PREC,
	case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ghp.DELAI_MOY_HEBDO_PREC,0)                        else 0 end as DELAI_MOY_HEBDO_PREC,
	case when ROW_NUMBER() OVER(PARTITION BY gdhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(gdhp.NB_OPP_CRE_DEC_GAGNEES_HEBDO_PREC,0)         else 0 end as NB_OPP_CRE_DEC_GAGNEES_HEBDO_PREC,
	case when ROW_NUMBER() OVER(PARTITION BY gdhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(gdhp.NB_OB_OPP_CRE_DEC_GAGNEES_HEBDO_PREC,0)      else 0 end as NB_OB_OPP_CRE_DEC_GAGNEES_HEBDO_PREC,
	case when ROW_NUMBER() OVER(PARTITION BY gdhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(gdhp.NB_OF_OPP_CRE_DEC_GAGNEES_HEBDO_PREC,0)      else 0 end as NB_OF_OPP_CRE_DEC_GAGNEES_HEBDO_PREC,
	case when ROW_NUMBER() OVER(PARTITION BY gdhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(gdhp.DEC_PRET_MOY_HEBDO_PREC,0)                   else 0 end as DEC_PRET_MOY_HEBDO_PREC,
	--case when ROW_NUMBER() OVER(PARTITION BY ghp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ghp.last_update_date_HEBDO_Prec,CAST('19000101' as date))  else CAST('19000101' as date) end as last_update_date_HEBDO_Prec,
	   

	----Opport.Crédit Perdues
	ISNULL(p.NB_OPP_CRE_PERDUES,0)                  as NB_OPP_CRE_PERDUES, 
	ISNULL(p.NB_ANNULATION,0)                       as NB_ANNULATION,
	ISNULL(p.NB_ANNULATION_CLIENT,0)                as NB_ANNULATION_CLIENT,
	ISNULL(p.NB_ANNULATION_DEL_RETRACTION,0)        as NB_ANNULATION_DEL_RETRACTION,
	ISNULL(p.NB_RETRACTATION,0)                     as NB_RETRACTATION,
	ISNULL(p.NB_AFFAIRE_REFUSEE,0)                  as NB_AFFAIRE_REFUSEE,
	ISNULL(p.NB_AFFAIRE_REFUSEE_RISQUE,0)           as NB_AFFAIRE_REFUSEE_RISQUE, 
	--ISNULL(p.NB_AFFAIRE_REFUSEE_SECFI,0) as NB_AFFAIRE_REFUSEE_SECFI,
	--ISNULL(p.NB_AFFAIRE_REFUSEE_Fraude,0) as NB_AFFAIRE_REFUSEE_Fraude,
	ISNULL(p.NB_AFFAIRE_REFUSEE_Suspicion,0)        as NB_AFFAIRE_REFUSEE_Suspicion,
	--ISNULL(p.NB_AFFAIRE_REFUSEE_Refus_dossier_Credit,0) as NB_AFFAIRE_REFUSEE_Refus_dossier_Credit,
	ISNULL(p.NB_AFFAIRE_REFUSEE_FICP,0)             as NB_AFFAIRE_REFUSEE_FICP, 
	ISNULL(p.NB_AFFAIRE_REFUSEE_Refus_Engagement,0) as NB_AFFAIRE_REFUSEE_Refus_Engagement, 
	ISNULL(p.NB_AFFAIRE_REFUSEE_Autres_Motifs,0)    as NB_AFFAIRE_REFUSEE_Autres_Motifs,
	--ISNULL(p.NB_INCIDENT_TECH,0) as NB_INCIDENT_TECH, 
	ISNULL(p.NB_DOS_NN_CONFORME,0)                  as NB_DOS_NN_CONFORME,
	ISNULL(p.NB_SANS_SUITE,0)                       as NB_SANS_SUITE,
	--ISNULL(p.last_update_date,CAST('19000101' as date)) as last_update_date ,

	 
	  

	case when ROW_NUMBER() OVER(PARTITION BY ph.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ph.NB_OPP_CRE_PERDUES_HEBDO,0)                      else 0 end as NB_OPP_CRE_PERDUES_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY ph.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ph.NB_ANNULATION_HEBDO,0)                           else 0 end as NB_ANNULATION_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY ph.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ph.NB_ANNULATION_CLIENT_HEBDO,0)                    else 0 end as NB_ANNULATION_CLIENT_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY ph.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ph.NB_ANNULATION_DEL_RETRACTION_HEBDO,0)            else 0 end as NB_ANNULATION_DEL_RETRACTION_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY ph.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ph.NB_RETRACTATION_HEBDO,0)                         else 0 end as NB_RETRACTATION_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY ph.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ph.NB_AFFAIRE_REFUSEE_HEBDO,0)                      else 0 end as NB_AFFAIRE_REFUSEE_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY ph.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ph.NB_AFFAIRE_REFUSEE_RISQUE_HEBDO,0)               else 0 end as NB_AFFAIRE_REFUSEE_RISQUE_HEBDO,
	--case when ROW_NUMBER() OVER(PARTITION BY ph.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ph.NB_AFFAIRE_REFUSEE_SECFI_HEBDO,0)                else 0 end as NB_AFFAIRE_REFUSEE_SECFI_HEBDO,
	--case when ROW_NUMBER() OVER(PARTITION BY ph.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ph.NB_AFFAIRE_REFUSEE_Fraude_HEBDO,0)               else 0 end as NB_AFFAIRE_REFUSEE_Fraude_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY ph.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ph.NB_AFFAIRE_REFUSEE_Suspicion_HEBDO,0)            else 0 end as NB_AFFAIRE_REFUSEE_Suspicion_HEBDO,
	--case when ROW_NUMBER() OVER(PARTITION BY ph.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ph.NB_AFFAIRE_REFUSEE_Refus_dossier_Credit_HEBDO,0) else 0 end as NB_AFFAIRE_REFUSEE_Refus_dossier_Credit_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY ph.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ph.NB_AFFAIRE_REFUSEE_FICP_HEBDO,0)                 else 0 end as NB_AFFAIRE_REFUSEE_FICP_HEBDO, 
	case when ROW_NUMBER() OVER(PARTITION BY ph.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ph.NB_AFFAIRE_REFUSEE_Refus_Engagement_HEBDO,0)     else 0 end as NB_AFFAIRE_REFUSEE_Refus_Engagement_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY ph.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ph.NB_AFFAIRE_REFUSEE_Autres_Motifs_HEBDO,0)        else 0 end as NB_AFFAIRE_REFUSEE_Autres_Motifs_HEBDO,
	--case when ROW_NUMBER() OVER(PARTITION BY ph.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ph.NB_INCIDENT_TECH_HEBDO,0)                        else 0 end as NB_INCIDENT_TECH_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY ph.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ph.NB_DOS_NN_CONFORME_HEBDO,0)                      else 0 end as NB_DOS_NN_CONFORME_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY ph.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ph.NB_SANS_SUITE_HEBDO,0)                           else 0 end as NB_SANS_SUITE_HEBDO,
	--case when ROW_NUMBER() OVER(PARTITION BY ph.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(ph.last_update_date_HEBDO,CAST('19000101' AS date)) else CAST('19000101' as date) end as last_update_date_HEBDO,
		

	case when ROW_NUMBER() OVER(PARTITION BY php.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(php.NB_OPP_CRE_PERDUES_Hebdo_Prec,0)                      else 0 end  as NB_OPP_CRE_PERDUES_Hebdo_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY php.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(php.NB_ANNULATION_Hebdo_Prec,0)                           else 0 end  as NB_ANNULATION_Hebdo_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY php.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(php.NB_ANNULATION_CLIENT_Hebdo_Prec,0)                    else 0 end  as NB_ANNULATION_CLIENT_Hebdo_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY php.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(php.NB_ANNULATION_DEL_RETRACTION_HEBDO_Prec,0)            else 0 end as NB_ANNULATION_DEL_RETRACTION_HEBDO_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY php.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(php.NB_RETRACTATION_Hebdo_Prec,0)                         else 0 end  as NB_RETRACTATION_Hebdo_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY php.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(php.NB_AFFAIRE_REFUSEE_Hebdo_Prec,0)                      else 0 end  as NB_AFFAIRE_REFUSEE_Hebdo_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY php.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(php.NB_AFFAIRE_REFUSEE_RISQUE_Hebdo_Prec,0)               else 0 end  as NB_AFFAIRE_REFUSEE_RISQUE_Hebdo_Prec,
	--case when ROW_NUMBER() OVER(PARTITION BY php.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(php.NB_AFFAIRE_REFUSEE_SECFI_Hebdo_Prec,0)                else 0 end  as NB_AFFAIRE_REFUSEE_SECFI_Hebdo_Prec,
	--case when ROW_NUMBER() OVER(PARTITION BY php.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(php.NB_AFFAIRE_REFUSEE_Fraude_Hebdo_Prec,0)               else 0 end  as NB_AFFAIRE_REFUSEE_Fraude_Hebdo_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY php.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(php.NB_AFFAIRE_REFUSEE_Suspicion_Hebdo_Prec,0)            else 0 end  as NB_AFFAIRE_REFUSEE_Suspicion_Hebdo_Prec,
	--case when ROW_NUMBER() OVER(PARTITION BY php.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(php.NB_AFFAIRE_REFUSEE_Refus_dossier_Credit_Hebdo_Prec,0) else 0 end  as NB_AFFAIRE_REFUSEE_Refus_dossier_Credit_Hebdo_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY php.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(php.NB_AFFAIRE_REFUSEE_FICP_Hebdo_Prec,0)                 else 0 end  as NB_AFFAIRE_REFUSEE_FICP_Hebdo_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY php.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(php.NB_AFFAIRE_REFUSEE_Refus_Engagement_Hebdo_Prec,0)     else 0 end  as NB_AFFAIRE_REFUSEE_Refus_Engagement_Hebdo_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY php.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(php.NB_AFFAIRE_REFUSEE_Autres_Motifs_HEBDO_PREC,0)        else 0 end as NB_AFFAIRE_REFUSEE_Autres_Motifs_HEBDO_Prec,
	-- case when ROW_NUMBER() OVER(PARTITION BY php.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(php.NB_INCIDENT_TECH_Hebdo_Prec,0)                        else 0 end  as NB_INCIDENT_TECH_Hebdo_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY php.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(php.NB_DOS_NN_CONFORME_HEBDO_PREC,0)                      else 0 end as NB_DOS_NN_CONFORME_HEBDO_PREP,
	case when ROW_NUMBER() OVER(PARTITION BY php.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(php.NB_SANS_SUITE_Hebdo_Prec,0)                           else 0 end  as NB_SANS_SUITE_Hebdo_Prec,
	--case when ROW_NUMBER() OVER(PARTITION BY php.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(php.last_update_date_HEBDO_Prec,CAST('19000101' AS date)) else CAST('19000101' as date) end as last_update_date_HEBDO_Prec,
		

	--Crédits Conso.
	ISNULL(ca.NB_CRE_CONSO_ACCORDES,0)      as NB_CRE_CONSO_ACCORDES, 
	ISNULL(ca.TX_CRE_CONSO_ACCORDES_2j,0)   as TX_CRE_CONSO_ACCORDES_2j,
	ISNULL(ca.TX_CRE_CONSO_ACCORDES_Self,0) as TX_CRE_CONSO_ACCORDES_Self,
	ISNULL(ca.MTT_CRE_ACCORDE,0)            as MTT_CRE_ACCORDE,
	ISNULL(ca.MTT_MOY_ACCORDE,0)            as MTT_MOY_ACCORDE,
	ISNULL(ca.TX_ACC,0)                     as TX_ACC,
	ISNULL(ca.NB_OBJ_FIN,0)                 as NB_OBJ_FIN,
	ISNULL(ca.NB_AUTO_VN_VO,0)              as NB_AUTO_VN_VO,
	ISNULL(ca.NB_TRAV,0)                    as NB_TRAV,
	ISNULL(ca.NB_PROJ,0)                    as NB_PROJ,
	ISNULL(ca.MTT_OBJ_FIN,0)                as MTT_OBJ_FIN,
	ISNULL(ca.MTT_AUTO_VN_VO,0)             as MTT_AUTO_VN_VO,
	ISNULL(ca.MTT_TRAV,0)                   as MTT_TRAV,
	ISNULL(ca.MTT_PROJ,0)                   as MTT_PROJ,
	   

	ISNULL(cd.NB_CRE_CONSO_DECAISSES,0)         as NB_CRE_CONSO_DECAISSES,
	ISNULL(cd.RATIO_CRE_CONSO_DECAISSES_8j,0)   as RATIO_CRE_CONSO_DECAISSES_8j,
	ISNULL(cd.RATIO_CRE_CONSO_DECAISSES_9j,0)   as RATIO_CRE_CONSO_DECAISSES_9j,
	ISNULL(cd.RATIO_CRE_CONSO_DECAISSES_TOT,0)  as RATIO_CRE_CONSO_DECAISSES_TOT,
	ISNULL(cd.RATIO_CRE_CONSO_DECAISSES_PART,0) as RATIO_CRE_CONSO_DECAISSES_PART,
	ISNULL(cd.MTT_CRE_DECAISSE,0)           as MTT_CRE_DECAISSE,
	ISNULL(cd.MTT_MOY_DECAISSE,0)           as MTT_MOY_DECAISSE,
	ISNULL(cd.TX_CLIENT_CRE_DEC,0)          as TX_CLIENT_CRE_DEC,
	ISNULL(cd.NB_OBJ_FIN_DEC,0)             as NB_OBJ_FIN_DEC,
	ISNULL(cd.NB_AUTO_VN_VO_DEC,0)          as NB_AUTO_VN_VO_DEC,
	ISNULL(cd.NB_TRAV_DEC,0)                as NB_TRAV_DEC,
	ISNULL(cd.NB_PROJ_DEC,0)                as NB_PROJ_DEC,
	ISNULL(cd.MTT_OBJ_FIN_DEC,0)            as MTT_OBJ_FIN_DEC,
	ISNULL(cd.MTT_AUTO_VN_VO_DEC,0)         as MTT_AUTO_VN_VO_DEC,
	ISNULL(cd.MTT_TRAV_DEC,0)               as MTT_TRAV_DEC,
	ISNULL(cd.MTT_PROJ_DEC,0)               as MTT_PROJ_DEC,
	   
	   
	case when ROW_NUMBER() OVER(PARTITION BY cah.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cah.NB_CRE_CONSO_ACCORDES_HEBDO,0)       else 0 end as NB_CRE_CONSO_ACCORDES_HEBDO, 
	case when ROW_NUMBER() OVER(PARTITION BY cah.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cah.TX_CRE_CONSO_ACCORDES_2j_HEBDO,0)    else 0 end as TX_CRE_CONSO_ACCORDES_2j_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY cah.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cah.TX_CRE_CONSO_ACCORDES_Self_HEBDO,0)  else 0 end as TX_CRE_CONSO_ACCORDES_Self_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY cah.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cah.MTT_CRE_ACCORDE_HEBDO,0)             else 0 end as MTT_CRE_ACCORDE_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY cah.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cah.MTT_MOY_ACCORDE_HEBDO,0)             else 0 end as MTT_MOY_ACCORDE_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY cah.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cah.TX_ACC_HEBDO,0)                      else 0 end as TX_ACC_HEBDO,

	case when ROW_NUMBER() OVER(PARTITION BY cah.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cah.NB_OBJ_FIN_HEBDO,0)                  else 0 end as NB_OBJ_FIN_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY cah.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cah.NB_AUTO_VN_VO_HEBDO,0)               else 0 end as NB_AUTO_VN_VO_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY cah.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cah.NB_TRAV_HEBDO,0)                     else 0 end as NB_TRAV_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY cah.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cah.NB_PROJ_HEBDO,0)                     else 0 end as NB_PROJ_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY cah.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cah.MTT_OBJ_FIN_HEBDO,0)                 else 0 end as MTT_OBJ_FIN_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY cah.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cah.MTT_AUTO_VN_VO_HEBDO,0)              else 0 end as MTT_AUTO_VN_VO_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY cah.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cah.MTT_TRAV_HEBDO,0)                    else 0 end as MTT_TRAV_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY cah.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cah.MTT_PROJ_HEBDO,0)				     else 0 end as MTT_PROJ_HEBDO,
	   
	   
	case when ROW_NUMBER() OVER(PARTITION BY cdh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdh.NB_CRE_CONSO_DECAISSES_HEBDO,0)              else 0 end as NB_CRE_CONSO_DECAISSES_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY cdh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdh.RATIO_CRE_CONSO_DECAISSES_8j_HEBDO,0)        else 0 end as RATIO_CRE_CONSO_DECAISSES_8j_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY cdh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdh.RATIO_CRE_CONSO_DECAISSES_9j_HEBDO,0)		 else 0 end as RATIO_CRE_CONSO_DECAISSES_9j_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY cdh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdh.RATIO_CRE_CONSO_DECAISSES_TOT_HEBDO,0)	     else 0 end as RATIO_CRE_CONSO_DECAISSES_TOT_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY cdh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdh.RATIO_CRE_CONSO_DECAISSES_PART_HEBDO,0)	     else 0 end as RATIO_CRE_CONSO_DECAISSES_PART_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY cdh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdh.MTT_CRE_DECAISSE_HEBDO,0)                    else 0 end as MTT_CRE_DECAISSE_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY cdh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdh.MTT_MOY_DECAISSE_HEBDO,0)                    else 0 end as MTT_MOY_DECAISSE_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY cdh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdh.TX_CLIENT_CRE_DEC_HEBDO,0)                   else 0 end as TX_CLIENT_CRE_DEC_HEBDO,

	case when ROW_NUMBER() OVER(PARTITION BY cdh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdh.NB_OBJ_FIN_DEC_HEBDO,0)              else 0 end as NB_OBJ_FIN_DEC_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY cdh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdh.NB_AUTO_VN_VO_DEC_HEBDO,0)           else 0 end as NB_AUTO_VN_VO_DEC_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY cdh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdh.NB_TRAV_DEC_HEBDO,0)                 else 0 end as NB_TRAV_DEC_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY cdh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdh.NB_PROJ_DEC_HEBDO,0)                 else 0 end as NB_PROJ_DEC_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY cdh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdh.MTT_OBJ_FIN_DEC_HEBDO,0)             else 0 end as MTT_OBJ_FIN_DEC_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY cdh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdh.MTT_AUTO_VN_VO_DEC_HEBDO,0)          else 0 end as MTT_AUTO_VN_VO_DEC_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY cdh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdh.MTT_TRAV_DEC_HEBDO,0)                else 0 end as MTT_TRAV_DEC_HEBDO,
	case when ROW_NUMBER() OVER(PARTITION BY cdh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdh.MTT_PROJ_DEC_HEBDO,0)                else 0 end as MTT_PROJ_DEC_HEBDO,
	   

	case when ROW_NUMBER() OVER(PARTITION BY cahp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cahp.NB_CRE_CONSO_ACCORDES_HEBDO_PREC,0)       else 0 end as NB_CRE_CONSO_ACCORDES_HEBDO_PREC, 
	case when ROW_NUMBER() OVER(PARTITION BY cahp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cahp.TX_CRE_CONSO_ACCORDES_2j_HEBDO_PREC,0)    else 0 end as TX_CRE_CONSO_ACCORDES_2j_HEBDO_PREC,
	case when ROW_NUMBER() OVER(PARTITION BY cahp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cahp.TX_CRE_CONSO_ACCORDES_Self_HEBDO_PREC,0)  else 0 end as TX_CRE_CONSO_ACCORDES_Self_HEBDO_PREC,
	case when ROW_NUMBER() OVER(PARTITION BY cahp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cahp.MTT_CRE_ACCORDE_HEBDO_PREC,0)             else 0 end as MTT_CRE_ACCORDE_HEBDO_PREC,
	case when ROW_NUMBER() OVER(PARTITION BY cahp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cahp.MTT_MOY_ACCORDE_HEBDO_PREC,0)             else 0 end as MTT_MOY_ACCORDE_HEBDO_PREC,
	case when ROW_NUMBER() OVER(PARTITION BY cahp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cahp.TX_ACC_HEBDO_PREC,0)                      else 0 end as TX_ACC_HEBDO_PREC,

	case when ROW_NUMBER() OVER(PARTITION BY cahp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cahp.NB_OBJ_FIN_HEBDO_PREC,0)                  else 0 end as NB_OBJ_FIN_HEBDO_PREC,
	case when ROW_NUMBER() OVER(PARTITION BY cahp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cahp.NB_AUTO_VN_VO_HEBDO_PREC,0)               else 0 end as NB_AUTO_VN_VO_HEBDO_PREC,
	case when ROW_NUMBER() OVER(PARTITION BY cahp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cahp.NB_TRAV_HEBDO_PREC,0)                     else 0 end as NB_TRAV_HEBDO_PREC,
	case when ROW_NUMBER() OVER(PARTITION BY cahp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cahp.NB_PROJ_HEBDO_PREC,0)                     else 0 end as NB_PROJ_HEBDO_PREC,
	case when ROW_NUMBER() OVER(PARTITION BY cahp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cahp.MTT_OBJ_FIN_HEBDO_PREC,0)                 else 0 end as MTT_OBJ_FIN_HEBDO_PREC,
	case when ROW_NUMBER() OVER(PARTITION BY cahp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cahp.MTT_AUTO_VN_VO_HEBDO_PREC,0)              else 0 end as MTT_AUTO_VN_VO_HEBDO_PREC,
	case when ROW_NUMBER() OVER(PARTITION BY cahp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cahp.MTT_TRAV_HEBDO_PREC,0)                    else 0 end as MTT_TRAV_HEBDO_PREC,
	case when ROW_NUMBER() OVER(PARTITION BY cahp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cahp.MTT_PROJ_HEBDO_PREC,0)				    else 0 end as MTT_PROJ_HEBDO_PREC,
	   
	  
	case when ROW_NUMBER() OVER(PARTITION BY cdhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdhp.NB_CRE_CONSO_DECAISSES_Hebdo_Prec,0)        else 0 end as NB_CRE_CONSO_DECAISSES_Hebdo_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY cdh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdhp.RATIO_CRE_CONSO_DECAISSES_8j_HEBDO_Prec,0)   else 0 end as RATIO_CRE_CONSO_DECAISSES_8j_HEBDO_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY cdh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdhp.RATIO_CRE_CONSO_DECAISSES_9j_HEBDO_Prec,0)   else 0 end as RATIO_CRE_CONSO_DECAISSES_9j_HEBDO_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY cdh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdhp.RATIO_CRE_CONSO_DECAISSES_TOT_HEBDO_Prec,0)  else 0 end as RATIO_CRE_CONSO_DECAISSES_TOT_HEBDO_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY cdh.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdhp.RATIO_CRE_CONSO_DECAISSES_PART_HEBDO_Prec,0) else 0 end as RATIO_CRE_CONSO_DECAISSES_PART_HEBDO_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY cdhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdhp.MTT_CRE_DECAISSE_Hebdo_Prec,0)              else 0 end as MTT_CRE_DECAISSE_Hebdo_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY cdhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdhp.MTT_MOY_DECAISSE_Hebdo_Prec,0)              else 0 end as MTT_MOY_DECAISSE_Hebdo_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY cdhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdhp.TX_CLIENT_CRE_DEC_Hebdo_Prec,0)             else 0 end as TX_CLIENT_CRE_DEC_Hebdo_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY cdhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdhp.NB_OBJ_FIN_DEC_Hebdo_Prec,0)                else 0 end as NB_OBJ_FIN_DEC_Hebdo_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY cdhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdhp.NB_AUTO_VN_VO_DEC_Hebdo_Prec,0)             else 0 end as NB_AUTO_VN_VO_DEC_Hebdo_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY cdhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdhp.NB_TRAV_DEC_Hebdo_Prec,0)                   else 0 end as NB_TRAV_DEC_Hebdo_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY cdhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdhp.NB_PROJ_DEC_Hebdo_Prec,0)                   else 0 end as NB_PROJ_DEC_Hebdo_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY cdhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdhp.MTT_OBJ_FIN_DEC_Hebdo_Prec,0)               else 0 end as MTT_OBJ_FIN_DEC_Hebdo_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY cdhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdhp.MTT_AUTO_VN_VO_DEC_Hebdo_Prec,0)            else 0 end as MTT_AUTO_VN_VO_DEC_Hebdo_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY cdhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdhp.MTT_TRAV_DEC_Hebdo_Prec,0)                  else 0 end as MTT_TRAV_DEC_Hebdo_Prec,
	case when ROW_NUMBER() OVER(PARTITION BY cdhp.WeekOfYear order by x.jour_alim desc)=1 then ISNULL(cdhp.MTT_PROJ_DEC_Hebdo_Prec,0)                  else 0 end as MTT_PROJ_DEC_Hebdo_Prec,

	   
	--parc
	ISNULL(pa.NB_CRE_ACCORDES_NNDEC_PARC,0)                    as NB_CRE_ACCORDES_NNDEC_PARC,
	ISNULL(pa.MTT_CRE_ACCOR_NNDEC_PARC,0)                      as MTT_CRE_ACCOR_NNDEC_PARC,
	ISNULL(pa.NB_CRE_DECAISSES_PARC,0)                         as NB_CRE_DECAISSES_PARC,
	ISNULL(pa.MTT_CRE_DECAISSES_PARC,0)                        as MTT_CRE_DECAISSES_PARC
	      	   
	from  cte_jour_alim_list x
	left join cte_Opp_Cre_Crees_Cumul a on x.jour_alim=a.DTE_CREA
	left join cte_Opp_Cre_Crees_Cumul_Hebdo b on a.WeekOfYear_c=b.WeekOfYear_c
	left join cte_Opp_Cre_Crees_Cumul_Hebdo_Prec c on a.WeekOfYear_c=c.WeekOfYear_c+1

	----Opport.Crédit En cours de vie
	left join Cte_Opp_Cre_En_cours_De_Vie_Anc_Cumul v on x.jour_alim=v.DTE_ALIM
	left join Cte_Opp_Cre_En_cours_De_Vie_Anc_Cumul_Hebdo vh on v.WeekOfYear=vh.WeekOfYear
	left join Cte_Opp_Cre_En_cours_De_Vie_Anc_Cumul_Hebdo_Prec  vhp on v.WeekOfYear=vhp.WeekOfYear+1

	--Opport. Crédit Gagnées
	left join Cte_Opp_Cre_Gagnees_Cumul g on x.jour_alim=g.DTE_ALIM
	left join Cte_Opp_Cre_Gagnees_Cumul_Hebdo gh on g.WeekOfYear=gh.WeekOfYear
	left join Cte_Opp_Cre_Gagnees_Cumul_Hebdo_Prec  ghp on g.WeekOfYear=ghp.WeekOfYear+1

	--Opport. Crédit Gagnées Dec
	left join Cte_Opp_Cre_Gagnees_Cre_Decp_Cumul gd on x.jour_alim=gd.DTE_ALIM
	left join Cte_Opp_Cre_Gagnees_Cre_DECp_Cumul_Hebdo gdh on gd.WeekOfYear=gdh.WeekOfYear
	left join Cte_Opp_Cre_Gagnees_Cre_DECp_Cumul_Hebdo_Prec gdhp on gd.WeekOfYear=gdhp.WeekOfYear+1

	--Opport.Crédit Perdues

	left join Cte_Opp_Cre_Perdues_Cumul p on x.jour_alim=p.closedate
	left join Cte_Opp_Cre_Perdues_Cumul_Hebdo ph on p.WeekOfYear=ph.WeekOfYear
	left JOIN Cte_Opp_Cre_Perdues_Cumul_Hebdo_Prec php on p.WeekOfYear=php.WeekOfYear+1

	--Crédits Conso.
	left join ind_ff_acc_cumul ca on x.jour_alim=ca.date_alim
	left join ind_ff_acc_cumul_Hebdo cah on ca.WeekOfYear=cah.WeekOfYear
	left join ind_ff_acc_cumul_Hebdo_Prec cahp on ca.WeekOfYear=cahp.WeekOfYear+1

	left join ind_ff_dec_cumul cd on x.jour_alim=cd.date_alim
	left join ind_ff_dec_Cumul_Hebdo  cdh on cd.WeekOfYear=cdh.WeekOfYear
	left join ind_ff_dec_Cumul_Hebdo_Prec  cdhp on cd.WeekOfYear=cdhp.WeekOfYear+1

	--Parc
	left join ind_pret_parc pa on x.jour_alim=pa.jour_alim
	where x.jour_alim BETWEEN DATEADD(dd,-6,@date_obs)  and @date_obs;

END
GO