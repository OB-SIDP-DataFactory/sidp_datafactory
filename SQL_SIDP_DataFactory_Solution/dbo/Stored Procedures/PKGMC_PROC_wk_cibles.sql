﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PKGMC_PROC_wk_cibles] 
	@nb_collected_rows int output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	 declare @date_min date 
     select @date_min = isnull(MAX(cast(DATE_CHARGEMENT as date )),'2017-01-01') from [dbo].[WK_MC_CIBLES] ; 

     declare @date_max date 
     select @date_max = MAX(cast(Startdt as date )) from [$(DataHubDatabaseName)].[dbo].[PV_MC_EMAILLOG] ; 

	
	--delete from [dbo].[WK_MC_CIBLES] where ID_CONTACT in (select ID_CONTACT from [dbo].SAS_Vue_MC_Clients where TRANSFORME=1) ; 
									
WITH CAMPAGNE AS ( 
SELECT distinct cmp.ID_CAMPAIGN, cmp.TYPOLOGY_CAMPAIGN, cmp.TYPOLOGY_NOTIFICATION, cmp.CHANNEL, cmp.OFFER_CODES, cmp.OFFER_LABELS  
FROM 
[$(DataHubDatabaseName)].[dbo].[PV_MC_CAMPAIGNMESSAGE] cmp 
WHERE cmp.TYPOLOGY_CAMPAIGN='JOURNEY-COMMERCIAL' or cmp.CAMPAIGN_NAME='P_ACOM_ENROLEMENTBIS_09042017' or CAMPAIGN_NAME like '%PARRAINAGE%'
)

			insert into [dbo].[WK_MC_CIBLES] 

/* Infos ciblés Email */

SELECT  distinct 
(cmp.ID_CAMPAIGN+eml.ID_CONTACT+REVERSE(SUBSTRING(REVERSE(eml.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))-1))) as PK_ID, 
cmp.ID_CAMPAIGN, 
cmp.TYPOLOGY_CAMPAIGN, 
cmp.TYPOLOGY_NOTIFICATION, 
eml.ID_CONTACT, 
REVERSE(SUBSTRING(SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)),1,CHARINDEX('_',SUBSTRING(REVERSE(eml.MESSAGE_NAME),CHARINDEX('_',REVERSE(eml.MESSAGE_NAME))+1,LEN(eml.MESSAGE_NAME)))-1)) as MESSAGE_NAME, 
eml.SEGMENT_GEOLIFE, 
eml.PRE_ATTRIBUTION, 
cmp.CHANNEL as CANAL, 
eml.ID_OPPORTUNITES, 
RIGHT('0'+eml.STATUT_OPPORTS,2) as STATUT_OPPORTS, 
cmp.OFFER_CODES,
cmp.OFFER_LABELS, 
eml.DATE_TIME_SEND as DATE, 
eml.Startdt as DATE_CHARGEMENT 

   FROM [$(DataHubDatabaseName)].[dbo].[PV_MC_EMAILLOG] eml 
 
  LEFT JOIN CAMPAGNE cmp on eml.ID_CAMPAIGN = cmp.ID_CAMPAIGN 

  WHERE cast(eml.Startdt as date) > @date_min and cast(eml.Startdt as date) <= @date_max and cmp.CHANNEL='EMAIL' 
	
	UNION 

/* Infos ciblés SMS */ 

  	SELECT  distinct 
(cmp.ID_CAMPAIGN+sms.ID_CONTACT+REVERSE(SUBSTRING(REVERSE(sms.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(sms.MESSAGE_NAME))-1))) as PK_ID, 
cmp.ID_CAMPAIGN, 
cmp.TYPOLOGY_CAMPAIGN, 
cmp.TYPOLOGY_NOTIFICATION, 
sms.ID_CONTACT, 
REVERSE(SUBSTRING(REVERSE(sms.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(sms.MESSAGE_NAME))-1)) as MESSAGE_NAME, 
sms.SEGMENT_GEOLIFE, 
sms.PRE_ATTRIBUTION, 
cmp.CHANNEL as CANAL, 
sms.ID_OPPORTUNITES, 
RIGHT('0'+sms.STATUT_OPPORTS,2) as STATUT_OPPORTS, 
cmp.OFFER_CODES,
cmp.OFFER_LABELS,  
sms.DATE_TIME_SEND as DATE, 
sms.Startdt as DATE_CHARGEMENT 

  FROM [$(DataHubDatabaseName)].[dbo].[PV_MC_SMSLOG] sms 
 
  LEFT JOIN CAMPAGNE cmp  on sms.ID_CAMPAIGN=cmp.ID_CAMPAIGN 
 
  WHERE cast(sms.Startdt as date) > @date_min and cast(sms.Startdt as date) <= @date_max and cmp.CHANNEL='SMS' 

	UNION 

/* Infos ciblés Push */

SELECT  distinct 
(cmp.ID_CAMPAIGN+push.ID_CONTACT+REVERSE(SUBSTRING(REVERSE(push.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(push.MESSAGE_NAME))-1))) as PK_ID, 
cmp.ID_CAMPAIGN, 
cmp.TYPOLOGY_CAMPAIGN, 
cmp.TYPOLOGY_NOTIFICATION, 
push.ID_CONTACT, 
REVERSE(SUBSTRING(REVERSE(push.MESSAGE_NAME),1,CHARINDEX('_',REVERSE(push.MESSAGE_NAME))-1)) as MESSAGE_NAME, 
push.SEGMENT_GEOLIFE, 
push.PRE_ATTRIBUTION, 
cmp.CHANNEL as CANAL, 
push.ID_OPPORTUNITES, 
RIGHT('0'+push.STATUT_OPPORTS,2) as STATUT_OPPORTS, 
cmp.OFFER_CODES,
cmp.OFFER_LABELS, 
push.DATE_TIME_SEND as DATE, 
push.Startdt as DATE_CHARGEMENT 

  FROM [$(DataHubDatabaseName)].[dbo].[PV_MC_PUSHLOG] push 
 
  LEFT JOIN CAMPAGNE cmp on push.ID_CAMPAIGN=cmp.ID_CAMPAIGN 
 
   WHERE cast(push.Startdt as date) > @date_min and cast(push.Startdt as date) <= @date_max and cmp.CHANNEL='PUSH' 
   
  UNION 

/* Infos ciblés appels */ 

  	SELECT  distinct 
(cmp.ID_CAMPAIGN+cal.ID_CONTACT) as PK_ID, 
cmp.ID_CAMPAIGN, 
cmp.TYPOLOGY_CAMPAIGN, 
cmp.TYPOLOGY_NOTIFICATION, 
cal.ID_CONTACT, 
' ' as message_name, 
cal.SEGMENT_GEOLIFE, 
cal.PRE_ATTRIBUTION, 
'CALL' as CANAL, 
cal.ID_OPPORTUNITY as ID_OPPORTUNITES, 
cal.STATUT_OPPORT, 
cmp.OFFER_CODES,
cmp.OFFER_LABELS, 
cal.CALL_DATE as DATE, 
cal.DATE_CHARGEMENT 

  FROM [dbo].[WK_MC_CALLLOG] cal 
 
	INNER JOIN CAMPAGNE cmp on cal.ID_CAMPAIGN=cmp.ID_CAMPAIGN 

	WHERE cast(cal.DATE_CHARGEMENT as date) > @date_min and cast(cal.DATE_CHARGEMENT as date) <= @date_max 
	
		select @nb_collected_rows = COUNT(*) from [dbo].[WK_MC_CIBLES]; 
END