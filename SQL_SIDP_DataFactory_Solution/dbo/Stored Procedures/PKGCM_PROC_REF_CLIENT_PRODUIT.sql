﻿

/****** Object:  StoredProcedure [dbo].[PKGCR_PROC_REF_CLIENT_PRODUIT]    Script Date: 17/11/2017 15:46:10 ******/

-- =============================================
-- Author:		<Yousra, LAZRAK>
-- Create date: <09/02/2018>
-- Description:	<Alimentation de la table REF_CLIENT_PRODUIT contenant les produits bancaires détenus par le client>
-- Tables input : [PV_SA_Q_CLIENT]
--                [PV_SA_Q_COMPTE]
--                [PV_SF_ACCOUNT]
--                [PV_SF_OPPORTUNITY]
-- Table output : REF_CLIENT_PRODUIT
-- =============================================

CREATE PROCEDURE [dbo].[PKGCM_PROC_REF_CLIENT_PRODUIT] 
(
     @date_alim date
	, @nbRows int OUTPUT -- nb lignes processées
)
AS
BEGIN

truncate table REF_CLIENT_PRODUIT
--Insertion des informations dans la table	REF_CLIENT_PRODUIT
INSERT INTO [dbo].[REF_CLIENT_PRODUIT] (
	DTE_ALIM,		
	NUM_CPT_SAB,
	NUM_CLT_SAB,
	ROLE_CLT,
	IDE_PERS_SF,
	IDE_OPPRT_SF
)
SELECT  @date_alim AS DTE_ALIM,
	   DWHCPTCOM,
       DWHCPTPPAL,
	   NULL as Role_CLT,
	   Id_SF,
	   '-1' as ID_OPPORT_SF
  from [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_CLIENT]
	inner JOIN [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_COMPTE]  ON PV_SA_Q_CLIENT.DWHCLICLI=PV_SA_Q_COMPTE.DWHCPTPPAL
	inner JOIN [$(DataHubDatabaseName)].[dbo].[PV_SF_ACCOUNT] as PV_SF_ACCOUNT ON  PV_SA_Q_CLIENT.DWHCLICLI=PV_SF_ACCOUNT.IDCustomerSAB__pc

	/*Nouvelle jointure*/
	join ( select Id_SF as Id_SF_Opp, AccountId as Id_SF_Account, StartedChannel__c, StartedChannelForIndication__c, Indication__c, RelationEntryScore__c, CreatedDate
                   , row_number() over (partition by Id_SF order by Validity_StartDate desc) as Validity_Flag
                from [$(DataHubDatabaseName)].[dbo].[VW_PV_SF_OPPORTUNITY_HISTORY] 
               where StageName = '09' and CommercialOfferCode__c = 'OC80') sf_opport 
    on sf_opport.Id_SF_Account = PV_SF_ACCOUNT.Id_SF and sf_opport.Validity_Flag = 1 --sf_client.Validity_Flag

		SELECT @nbRows = @@ROWCOUNT;
END
