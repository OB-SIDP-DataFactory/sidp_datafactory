﻿CREATE PROCEDURE [dbo].[PKG_QOD_GENERATION_VOLUMETRIE_COLLECT]

-- Parametres taux de variation à ne pas dépasser
@POURCENTVARIATIONMAX AS varchar(200) = '30'

AS
BEGIN

DECLARE 

-- Variables locales

@sql_qod nvarchar(max),
@cursor_ETLName varchar(200),
@cursor_FileOrTableName varchar(200),
@cursor_PERM varchar(200),
@cursor_id int = 0
---- Curseur sur les colonnes d'une table sur laquelle on veut effectuer le contrôle
--DECLARE Table_cursor CURSOR for 

--SELECT DISTINCT L.ETLName, L.FileOrTableName, R.PERM 
--FROM [$(DataHubDatabaseName)].[dbo].[Data_Log] L
--LEFT OUTER JOIN [dbo].[REF_TRAITEMENT] R ON (L.ETLName = R.NOM_TRTM AND L.FileOrTableName = R.NOM_TABL)
--WHERE DOMN = 'DataHub'
--AND UPPER(L.ProcessingType) like 'COLLECT%'
-- --Boucle sur les noms de tables
-- OPEN Table_cursor

-- FETCH NEXT FROM Table_cursor INTO @cursor_ETLName, @cursor_FileOrTableName , @cursor_PERM

--WHILE @@FETCH_STATUS = 0
--BEGIN 
--SET @cursor_id = @cursor_id + 1

--SET @sql_qod = ('
--SELECT 
--	TODAY.ProcessingType as output_col1,
--	''' + @cursor_ETLName + ''' as output_col2,
--	'''+ @cursor_FileOrTableName + ''' as output_col3,
--	TODAY.NBR_LIGNE as output_col4,
--	YESTERDAY.NBR_LIGNE as output_col5,
--	LASTWEEK.NBR_LIGNE as output_col6,
--	'+ @POURCENTVARIATIONMAX + ' AS output_col7
--FROM
--(
--	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
--	FROM SIDP_DataHub.dbo.Data_Log
--	WHERE ETLName = ''' + @cursor_ETLName + '''
--	AND FileOrTablename= '''+ @cursor_FileOrTableName + ''' 
--	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
--) TODAY
--INNER JOIN 
--(
--	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
--	FROM SIDP_DataHub.dbo.Data_Log
--	WHERE ETLName = ''' + @cursor_ETLName + '''
--	AND FileOrTablename= '''+ @cursor_FileOrTableName + ''' 
--	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
--) YESTERDAY 
--ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-('+ @POURCENTVARIATIONMAX +'*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
--INNER JOIN 
--(
--	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
--	FROM SIDP_DataHub.dbo.Data_Log
--	WHERE ETLName = ''' + @cursor_ETLName + '''
--	AND FileOrTablename= '''+ @cursor_FileOrTableName + '''  
--	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
--) LASTWEEK 
--ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-('+ @POURCENTVARIATIONMAX +'*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
--')

--print(@sql_qod)

--INSERT INTO [dbo].[SIDP_QOD_LISTE_CONTROLE]
--VALUES
--(
--	'VOL_COLLECT_' + @cursor_FileOrTableName,
--	'2 - Warning',
--	N'Contrôle de la volumétrie des données pour la collecte de la table ' +  @cursor_FileOrTableName,
--	N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à '+ @POURCENTVARIATIONMAX + N'% pour la table ' +  @cursor_FileOrTableName + ' dans le package ' + @cursor_ETLName,
--	@sql_qod,
--	@cursor_PERM,
--	'VOL_COLLECT_' + @cursor_PERM,
--	'Technique - Contrôle volumétrie',
--	N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente''',
--	'Processing Type',
--	'Nom du package',
--	'Nom de la table',
--	'Nb lignes aujourdhui',
--	'Nb lignes la veille',
--	'Nb ligne la semaine précédente',
--	'Pourcentage dépassé',
--	1,
--	NULL
--)

--FETCH NEXT FROM Table_cursor INTO @cursor_ETLName, @cursor_FileOrTableName , @cursor_PERM
--END   

--fin de la procedure
END