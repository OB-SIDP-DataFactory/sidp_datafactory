﻿CREATE PROCEDURE [dbo].[ENC_ALIM_DIM_RESEAU]
AS
BEGIN 

declare @date_traitement smalldatetime =getdate()

MERGE ENC_DIM_RESEAU             AS target 
using (SELECT * FROM [$(DataHubDatabaseName)].[dbo].REF_RESEAU	WITH(NOLOCK) WHERE  @date_traitement between date_debut and date_fin ) AS source 
ON target.CODE_APPORTEUR=source.CODE_APPORTEUR
  and target.CODE_RESEAU=source.CODE_RESEAU
  and target.FLAG_ACTIF=1
WHEN MATCHED THEN 
UPDATE 
SET target.LIBELLE_RESEAU_N1=source.LIBELLE_RESEAU_N1,
	target.LIBELLE_RESEAU_N2=source.LIBELLE_RESEAU_N2,
	target.LIBELLE_RESEAU=source.LIBELLE_RESEAU,
	target.UPDATE_DATE=getdate()

WHEN NOT matched BY TARGET  THEN 
INSERT 
                 ( 
				 CODE_RESEAU_N1,
				 LIBELLE_RESEAU_N1,
				 CODE_RESEAU_N2,
				 LIBELLE_RESEAU_N2,
				 CODE_RESEAU,
				 LIBELLE_RESEAU,
				 CODE_APPORTEUR,
				 INSERT_DATE,
				 FLAG_ACTIF
				 )
				 VALUES 
				 (
				  CODE_RESEAU_N1,
				  LIBELLE_RESEAU_N1,
				  CODE_RESEAU_N2,
				  LIBELLE_RESEAU_N2,
				  CODE_RESEAU,
				  LIBELLE_RESEAU,
				  CODE_APPORTEUR,
				  GETDATE(),
				  1
				 );

--WHEN NOT matched BY source  THEN 
--UPDATE 
--SET    target.flag_actif=0, 
--       target.UPDATE_DATE=getdate();


END 


