﻿create PROC [dbo].[PKGCR_PROC_STOCK_OPPORT]
  @DATE_ALIM date
, @jours_histo int 
, @nbRows int OUTPUT
AS  BEGIN 

-- ===============================================================================================================================
--tables input : [$(DataHubDatabaseName)].[dbo].[PV_SF_OPPORTUNITY_HISTORY]
--               [CRE_WK_OPPORTUNITY_CANAL]
--               [PV_SF_ACCOUNT]
--               [DIM_TEMPS]
--               [DIM_RECORDTYPE]
--				 [DIM_MOTIF_REF]
--				 [DIM_CANAL_DIS]
--				 [DIM_SS_RES]
--				 [DIM_RES_DIS]
--				 [DIM_CANAL_INT]
--				 [DIM_MOTIF_SS]
--				 [DIM_STADE_VTE]
--table output : CRE_WK_STOCK_OPPORTUNITY
--description : créer une table avec l'historique des opportunités de type crédit en stock pour chaque date de chargement
-- ==============================================================================================================================

if @DATE_ALIM is null 
  set @DATE_ALIM = GETDATE();
 
if @jours_histo is null 
  set @jours_histo = 1; --valeur 1 par défaut = traitement nominal par défaut
DROP TABLE IF EXISTS [dbo].[cre_wk_stock_opport_new];
 delete from [dbo].[CRE_WK_STOCK_OPPORTUNITY] where date_alim >= cast( dateadd(day,-@jours_histo, @DATE_ALIM) as date);
 
/* créer un sous-ensemble de la table dimension temps     */
/* liste des dates pour lesquelles le stock est recalculé */
with cre_wk_temps_opportunity as (
select StandardDate 
  from dbo.DIM_TEMPS
 where cast( dateadd(day,-@jours_histo, @DATE_ALIM) as date) <= StandardDate and StandardDate < cast(@DATE_ALIM as date)
),

cre_wk_opport_histo as (
select temps.StandardDate as date_alim
     , opport.*
  from cre_wk_temps_opportunity as temps
  left join [$(DataHubDatabaseName)].[dbo].[PV_SF_OPPORTUNITY_HISTORY] as opport
  on ( cast(opport.Validity_StartDate as date) <= temps.StandardDate and temps.StandardDate <= cast(opport.Validity_EndDate as date) )
)

select oh.date_alim
    , oh.Id_SF
    , oh.StageName as Cod_StageName
	, dim_sv.LIB_STA_VTE as Lib_StageName
    , case when oh.Indication__c = 'Oui' then 'Oui' else 'Non' end as flag_indication
    , oh.CreatedById
    , oh.CreatedDate as CreatedDate
    , oh.CloseDate
    --âge de l'opportunité en fct de la date d'obs pour opp en cours, date de clôture pour opp finalisées
    , case when oh.StageName in ('10','11','12','15','16') then datediff(DD, oh.CreatedDate, oh.CloseDate) else DATEDIFF(DD, oh.CreatedDate, oh.date_alim) end as age_opport
    , oh.OwnerId
    , oh.AdvisorCode__c
    , oh.PointOfSaleCode__c
    , oh.CommercialOfferCode__c
    , oh.CommercialOfferName__c
    , oh.RelationEntryScore__c
    , case when oh.StageName in ('10','11','12','15','16') and opp_can.nb_canaux_distrib = 1 and oh.DistributionChannel__c = '01' then 1 else 0 end as flag_full_digi
    , case when oh.StageName in ('10','11','12','15','16') and opp_can.nb_canaux_distrib = 1 and oh.DistributionChannel__c = '02' then 1 else 0 end as flag_full_btq 
	, case when oh.StageName in ('10','11','12','15','16') and opp_can.nb_canaux_distrib = 1 and oh.DistributionChannel__c = '03' then 1 else 0 end as flag_full_crc 
    , oh.DistributionChannel__c as Cod_DistributionChannel__c
	, dim_cd.LIB_CAN_DIS as Lib_DistributionChannel__c
    , oh.DistributorNetwork__c as Cod_DistributorNetwork__c
	, dim_r.LIB_RES_DIS as Lib_DistributorNetwork__c
    , oh.DistributorEntity__c as Cod_DistributorEntity__c
	, dim_sr.LIB_SS_RES as Lib_DistributorEntity__c
    , oh.LeadSource as Cod_LeadSource
	, dim_ci.LIB_CAN_INT as Lib_LeadSource
    --figer canal finalisation quand stage de vente = compte ouvert
    , case when oh.StageName in ('10','11','12','15','16') then oh.LeadSource else '00' end as canal_fin
    --figer sous réseau finalisation quand stage de vente = compte ouvert
    , case when oh.StageName in ('10','11','12','15','16') then oh.DistributorEntity__c else '00' end as sous_reseau_fin 
    , a.OptInDataTelco__c
    , a.OptInOrderTelco__c
    , oh.AccountId
    , oh.ProductFamilyCode__c
    , oh.ProductFamilyName__c
    , oh.Validity_StartDate as last_update_date
	, oh.IsDeleted
	, oh.RecordTypeId
	, dim_rt.LIBELLE as Lib_RecordType
	, oh.IsPrivate
	, oh.Name
	, oh.Amount
	, oh.Probability
	, oh.ExpectedRevenue
	, oh.TotalOpportunityQuantity
	, oh.Type
	, oh.NextStep
	, oh.IsClosed
	, oh.IsWon
	, oh.ForecastCategory
	, oh.ForecastCategoryName
	, oh.CampaignId
	, oh.HasOpportunityLineItem
	, oh.Pricebook2Id
	, oh.LastModifiedDate
	, oh.LastModifiedById
	, oh.SystemModstamp
	, oh.LastActivityDate
	, oh.FiscalQuarter
	, oh.FiscalYear
	, oh.Fiscal
	, oh.LastViewedDate
	, oh.LastReferencedDate
	, oh.ContractId
	, oh.HasOpenActivity
	, oh.HasOverdueTask
	, oh.TCHCompleteFile__c
	, oh.DeniedOpportunityReason__c as Cod_DeniedOpportunityReason__c
	, dim_mr.LIB_MOT_REF as Lib_DeniedOpportunityReason__c
	, oh.CampaignId__c
	, oh.TCHFromMarketingCase__c
	, oh.realizedBy__c
	, oh.FinalizedBy__c
	, oh.FormValidation__c
	, oh.IDOppty__c
	, oh.NoIndication__c
	, oh.OriginalEvent__c
	, oh.RejectReason__c as Cod_RejectReason__c
	, dim_ms.LIB_MOT_SS as Lib_RejectReason__c
	, oh.SendingInformations__c
	, oh.SignatureDate__c
	, oh.TCHNotifDuplicateProspect__c
	, oh.FolderAlias__c
	, oh.IDProcessSous__c
	, oh.RecoveryLink__c
	, oh.TCHExpiredIndication__c
	, oh.TCHExpiredSubscription__c
	, oh.TCHManualAnalysis__c
	, oh.TCHOppCreation__c
	, oh.ToBeDeleted__c
	, oh.IdSource__c
	, oh.Manager__c
	, oh.ProvenanceIndicationIndicatorId__c
	, oh.StartedChannelForIndication__c as Cod_StartedChannelForIndication__c
	, dim_ci2.LIB_CAN_INT as Lib_StartedChannelForIndication__c
	, oh.StartedChannel__c as Cod_StartedChannel__c
	, dim_ci3.LIB_CAN_INT as Lib_StartedChannel__c
	, oh.TCHId__c
	, oh.CompleteFileFlag__c
	, oh.ContractNumberDistrib__c
	, oh.OptInTelcoData__c
	, oh.SubscriberHolder__c
	, oh.CreditAmount__c
	, oh.CustomerAdviserId__c
	, oh.DebitDay__c
	, oh.DepositAmount__c
	, oh.Derogation__c
	, oh.FFinalDecision__c
	, oh.FPreScoreColor__c
	, oh.FirstDueDate__c
	, oh.ForbiddenWords__c
	, oh.FundingSubject__c
	, oh.HasForbiddenWord__c
	, oh.IncompleteFileReason__c
	, oh.LoanAcceptationDate__c
	, case when oh.LoanAcceptationDate__c is not null then DATEDIFF(DD, oh.CreatedDate, oh.LoanAcceptationDate__c) end as delai_accord
	, oh.ObjectInApproval__c
	, oh.OffersRate__c
	, oh.PrescriberContact__c
	, oh.ReleaseAmount__c
	, oh.ReleaseDate__c
	, case when oh.ReleaseDate__c is not null then DATEDIFF(DD,oh.LoanAcceptationDate__c, oh.ReleaseDate__c) end as delai_deblocage
	, oh.StartDate__c
	, oh.SubscribedAmount__c
	, oh.TCHCategory__c
	--, oh.TracingProductFamily__c
	, oh.TracingProduct__c
	, oh.RetractationDate__c
    , case when row_number() over (partition by oh.date_alim, oh.Id_SF order by oh.Validity_StartDate desc) = 1 then 1 else 0 end as flag_opport_last_state
    , case when row_number() over (partition by oh.date_alim, oh.Id_SF, oh.StageName order by oh.Validity_StartDate desc) = 1 then 1 else 0 end as flag_opport_stage_last_state
  into cre_wk_stock_opport_new
  from cre_wk_opport_histo as oh
  inner join [dbo].[DIM_RECORDTYPE] as dim_rt
	on oh.RecordTypeId = dim_rt.CODE_SF
  left join [$(DataHubDatabaseName)].[dbo].[PV_SF_ACCOUNT] as a
    on oh.AccountId = a.Id_SF
  left join [dbo].[CRE_WK_OPPORTUNITY_CANAL] as opp_can 
    on oh.Id_SF = opp_can.id_sf
  left join [dbo].[DIM_MOTIF_REF] as dim_mr
	on oh.DeniedOpportunityReason__c = dim_mr.COD_MOT_REF
  left join [dbo].[DIM_CANAL_DIS] as dim_cd
	on oh.DistributionChannel__c = dim_cd.COD_CAN_DIS
  left join [dbo].[DIM_SS_RES] as dim_sr
	on oh.DistributorEntity__c = dim_sr.COD_SS_RES
  left join [dbo].[DIM_RES_DIS] as dim_r
	on oh.DistributorNetwork__c = dim_r.COD_RES_DIS
  left join [dbo].[DIM_CANAL_INT] as dim_ci
	on oh.LeadSource = dim_ci.COD_CAN_INT
  left join [dbo].[DIM_MOTIF_SS] as dim_ms
	on oh.RejectReason__c = dim_ms.COD_MOT_SS
  left join [dbo].[DIM_STADE_VTE] as dim_sv
	on oh.StageName = dim_sv.COD_STA_VTE
  left join [dbo].[DIM_CANAL_INT] as dim_ci2		
	on oh.StartedChannelForIndication__c = dim_ci2.COD_CAN_INT	
  left join [dbo].[DIM_CANAL_INT] as dim_ci3		
	on oh.StartedChannel__c = dim_ci3.COD_CAN_INT	
  where oh.CommercialOfferCode__c is not null and oh.CommercialOfferName__c is not null 
		and dim_rt.LIBELLE = 'OB - Crédit';
		
	update  src
	set flag_opport_last_state_overall=0
	from [dbo].[CRE_WK_STOCK_OPPORTUNITY] src  inner join (select distinct id_sf from cre_wk_stock_opport_new) as trg 
	on src.Id_SF=trg.Id_SF and src.flag_opport_last_state_overall=1;
	
    update  src
	set flag_opport_stage_last_state_overall=0
	from [dbo].[CRE_WK_STOCK_OPPORTUNITY] src  inner join (select distinct id_sf,Cod_StageName from cre_wk_stock_opport_new) as trg  
	on src.Id_SF=trg.Id_SF and src.Cod_StageName=trg.Cod_StageName and src.flag_opport_stage_last_state_overall=1;

insert into [dbo].[CRE_WK_STOCK_OPPORTUNITY] (
    date_alim
	, Id_SF
	, Cod_StageName
	, Lib_StageName
	, flag_indication
	, CreatedById
	, CreatedDate
	, CloseDate
	, age_opport
	, OwnerId
	, AdvisorCode__c
	, PointOfSaleCode__c
	, CommercialOfferCode__c
	, CommercialOfferName__c
	, RelationEntryScore__c
	, flag_full_digi
	, flag_full_btq
	, flag_full_crc
	, Cod_DistributionChannel__c
	, Lib_DistributionChannel__c
	, Cod_DistributorNetwork__c
	, Lib_DistributorNetwork__c
	, Cod_DistributorEntity__c
	, Lib_DistributorEntity__c
	, Cod_LeadSource
	, Lib_LeadSource
	, canal_fin
	, sous_reseau_fin
	, OptInDataTelco__c
	, OptInOrderTelco__c
	, AccountId
	, ProductFamilyCode__c
	, ProductFamilyName__c
	, last_update_date
	, IsDeleted
	, RecordTypeId
	, Lib_RecordType
	, IsPrivate
	, Name
	, Amount
	, Probability
	, ExpectedRevenue
	, TotalOpportunityQuantity
	, Type
	, NextStep
	, IsClosed
	, IsWon
	, ForecastCategory
	, ForecastCategoryName
	, CampaignId
	, HasOpportunityLineItem
	, Pricebook2Id
	, LastModifiedDate
	, LastModifiedById
	, SystemModstamp
	, LastActivityDate
	, FiscalQuarter
	, FiscalYear
	, Fiscal
	, LastViewedDate
	, LastReferencedDate
	, ContractId
	, HasOpenActivity
	, HasOverdueTask
	, TCHCompleteFile__c
	, Cod_DeniedOpportunityReason__c
	, Lib_DeniedOpportunityReason__c
	, CampaignId__c
	, TCHFromMarketingCase__c
	, realizedBy__c
	, FinalizedBy__c
	, FormValidation__c
	, IDOppty__c
	, NoIndication__c
	, OriginalEvent__c
	, Cod_RejectReason__c
	, Lib_RejectReason__c
	, SendingInformations__c
	, SignatureDate__c
	, TCHNotifDuplicateProspect__c
	, FolderAlias__c
	, IDProcessSous__c
	, RecoveryLink__c
	, TCHExpiredIndication__c
	, TCHExpiredSubscription__c
	, TCHManualAnalysis__c
	, TCHOppCreation__c
	, ToBeDeleted__c
	, IdSource__c
	, Manager__c
	, ProvenanceIndicationIndicatorId__c
	, Cod_StartedChannelForIndication__c
	, Lib_StartedChannelForIndication__c
	, Cod_StartedChannel__c
	, Lib_StartedChannel__c
	, TCHId__c
	, CompleteFileFlag__c
	, ContractNumberDistrib__c
	, OptInTelcoData__c
	, SubscriberHolder__c
	, CreditAmount__c
	, CustomerAdviserId__c
	, DebitDay__c
	, DepositAmount__c
	, Derogation__c
	, FFinalDecision__c
	, FPreScoreColor__c
	, FirstDueDate__c
	, ForbiddenWords__c
	, FundingSubject__c
	, HasForbiddenWord__c
	, IncompleteFileReason__c
	, LoanAcceptationDate__c
	, delai_accord
	, ObjectInApproval__c
	, OffersRate__c
	, PrescriberContact__c
	, ReleaseAmount__c
	, ReleaseDate__c
	, delai_deblocage
	, StartDate__c
	, SubscribedAmount__c
	, TCHCategory__c
	--, TracingProductFamily__c
	, TracingProduct__c
	, RetractationDate__c
	, flag_opport_last_state
	, flag_opport_stage_last_state
    , flag_opport_last_state_overall
    , flag_opport_stage_last_state_overall )
select *
     , case when row_number() over (partition by Id_SF order by last_update_date,date_alim  desc) = 1 then 1 else 0 end as flag_opport_last_state_overall
     , case when row_number() over (partition by Id_SF, Cod_StageName  order by last_update_date,date_alim desc) = 1 then 1 else 0 end as flag_opport_stage_last_state_overall
from cre_wk_stock_opport_new ;
 
 select @nbRows =count(*) from cre_wk_stock_opport_new;

END