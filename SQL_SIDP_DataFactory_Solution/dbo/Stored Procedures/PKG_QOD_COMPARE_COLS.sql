﻿CREATE PROCEDURE [dbo].[PKG_QOD_COMPARE_COLS]
    -- Parametres table_source, table_cible
    @table_source AS VARCHAR(200) = NULL,
    @table_cible AS VARCHAR(200) = NULL
AS
BEGIN
    DECLARE
    @sql_qod NVARCHAR(MAX),
    @cursor_table_name VARCHAR(200),
    @cursor_col_name VARCHAR(200),
    @cursor_col_type VARCHAR(200),
    @cursor_col_len NUMERIC,
    @cursor_id INT = 0;
    -- Curseur sur les colonnes d'une table sur laquelle on veut effectuer le contrôle
    DECLARE col_cursor CURSOR FOR
    SELECT
        TABLE_NAME,
        COLUMN_NAME,
        DATA_TYPE,
        CHARACTER_MAXIMUM_LENGTH
    FROM
        INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = @table_cible;

    --Boucle sur les colonnes
    OPEN col_cursor
    FETCH NEXT FROM col_cursor INTO @cursor_table_name, @cursor_col_name, @cursor_col_type, @cursor_col_len
    WHILE @@FETCH_STATUS = 0
    BEGIN
        SET @cursor_id = @cursor_id + 1

        SET @sql_qod = 
        (
            SELECT
                CASE            @cursor_col_type
                    WHEN        'varchar'
                        THEN    'select src_len as output_col1, cib_len as output_col2, null as output_col3, null as output_col4, null as output_col5, null as output_col6,	null as output_col7 from ((select CHARACTER_MAXIMUM_LENGTH as src_len from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = ''' + @table_source + ''' and COLUMN_NAME = ''' + @cursor_col_name	+ ''') src join (select CHARACTER_MAXIMUM_LENGTH as cib_len from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = ''' + @table_cible + ''' and COLUMN_NAME = ''' + @cursor_col_name + ''') cible on src.src_len < cible.cib_len)'
                    WHEN        'nvarchar'
                        THEN    'select src_len as output_col1, cib_len as output_col2, null as output_col3, null as output_col4, null as output_col5, null as output_col6,	null as output_col7 from ((select CHARACTER_MAXIMUM_LENGTH as src_len from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = ''' + @table_source + ''' and COLUMN_NAME = ''' + @cursor_col_name	+ ''') src join (select CHARACTER_MAXIMUM_LENGTH as cib_len from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = ''' + @table_cible + ''' and COLUMN_NAME = ''' + @cursor_col_name + ''') cible on src.src_len < cible.cib_len)'
                    ELSE        'select src_typ as output_col1, cib_typ as output_col2, null as output_col3, null as output_col4, null as output_col5, null as output_col6,	null as output_col7 from ((select DATA_TYPE as src_typ from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = ''' + @table_source + ''' and COLUMN_NAME = ''' + @cursor_col_name + ''') src join (select CHARACTER_MAXIMUM_LENGTH as cib_typ from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = ''' + @table_cible + ''' and COLUMN_NAME = ''' + @cursor_col_name + ''') cible on src.src_typ != cible.cib_typ)'
                END
        );
        INSERT INTO [dbo].[SIDP_QOD_LISTE_CONTROLE]
        (
            [COD_CONTROL],
            [CRITICITE_CONTROL],
            [LIBELLE_CONTROL],
            [DESC_CONTROL],
            [SQL_CONTROL],
            [PERIMETRE_CONTROL],
            [FAMILLE_CONTROL],
            [CATEGORIE_CONTROL],
            [OUTPUT_message],
            [OUTPUT_LIB1],
            [OUTPUT_LIB2],
            [OUTPUT_LIB3],
            [OUTPUT_LIB4],
            [OUTPUT_LIB5],
            [OUTPUT_LIB6],
            [OUTPUT_LIB7],
            [FLG_ACTIF]
        )
        VALUES
        (
            CAST('DataHub_CONTROL_' + @table_cible + '_' + cast(@cursor_id AS CHAR) AS VARCHAR(80)),
            CAST('1- Bloquant'	AS	VARCHAR(30)),
            CAST(('comparaison type / longueur ' + @table_source + '.' + @cursor_col_name + 'et ' + @table_cible + '.' + @cursor_col_name) AS VARCHAR(150)),
            'Ce controle permet la comparaison de type / longueur entre' +  @table_source + '.' + @cursor_col_name + 'et ' + @table_cible + '.' + @cursor_col_name,
            @sql_qod,
            'DataHub',
            CAST(('Contrôle' + @table_cible) AS VARCHAR(50)),
            'Technique - Contrôle intégration',
            'Différence de type / longueur entre ' + @table_source + ' et ' + @table_cible,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            1
        )
        FETCH NEXT FROM col_cursor INTO @cursor_table_name, @cursor_col_name, @cursor_col_type, @cursor_col_len;
    END
END
GO