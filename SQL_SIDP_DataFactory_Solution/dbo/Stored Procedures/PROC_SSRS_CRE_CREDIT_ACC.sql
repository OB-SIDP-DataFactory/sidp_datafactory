﻿CREATE PROC [dbo].[PROC_SSRS_CRE_CREDIT_ACC]
AS
BEGIN
	DECLARE
		@date_obs	DATE,
		@date_max	DATE;
	SELECT
		@date_obs = CAST(GETDATE() AS DATE),
		@date_max = dateadd(day, -1, GETDATE());
	SET DATEFIRST 1; -->1    Lundi
	DROP TABLE IF EXISTS #rq_cre_acc_cp;
	DROP TABLE IF EXISTS [dbo].[WK_SSRS_CRE_CREDIT_ACC];

	SELECT
		Year_alim,
		Month_alim,
		Week_alim,
		jour_alim,
		mtt_cre_accorde,
		sum(mtt_cre_accorde) OVER (PARTITION BY Week_alim ORDER BY Week_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS mtt_cre_accorde_WTD, --Week to date
		sum(mtt_cre_accorde) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS mtt_cre_accorde_MTD, --Month to date
		sum(mtt_cre_accorde) OVER (PARTITION BY Year_alim,Month_alim ORDER BY Year_alim, Month_alim)  AS mtt_cre_accorde_FM, --Full Month
		sum(mtt_cre_accorde) OVER (PARTITION BY Year_alim ORDER BY Year_alim, Month_alim, jour_alim ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS mtt_cre_accorde_YTD,  --Year to date
		sum(mtt_cre_accorde) OVER (PARTITION BY Year_alim ORDER BY Year_alim)  AS mtt_cre_accorde_FY  --Full year
	INTO
		#rq_cre_acc_cp
	FROM
		( 
			SELECT
				T1.StandardDate as jour_alim,
				DATEPART(YEAR, T1.Date) AS Year_alim,
				DATEPART(MONTH, T1.Date) AS Month_alim,
				DATEPART(YEAR, DATEADD(day, (7 - DATEPART(dw, T1.StandardDate)), T1.StandardDate)) * 100 + DATEPART(WEEK,DATEADD(day, (7 - DATEPART(dw, T1.StandardDate)), T1.StandardDate)) AS Week_alim,
				sum(MTT_NML) as mtt_cre_accorde
			FROM
				[dbo].[DIM_TEMPS]	AS	T1 WITH(NOLOCK)
				LEFT JOIN	[dbo].[CRE_CREDIT]	AS	C	WITH(NOLOCK)
					ON	T1.StandardDate = CAST(C.DTE_ACC_TOKOS AS DATE)
			WHERE
				1 = 1
				AND	T1.StandardDate >= CAST(DATEADD(yyyy, DATEDIFF(YYYY, 0, DATEADD(yyyy, -2, @date_obs)), 0) AS DATE)
				AND	T1.StandardDate <= DATEADD(day, (7 - DATEPART(dw, @date_obs)), @date_obs)
			GROUP BY
				T1.StandardDate,
				DATEPART(YEAR, T1.Date),
				DATEPART(MONTH, T1.Date),
				DATEPART(WEEK, T1.Date)
		)	AS	t;

	SELECT
		CY.jour_alim,
		CY.Year_alim,
		CY.Month_alim,
		CY.Week_alim,
		coalesce(CY.mtt_cre_accorde,0) AS mtt_cre_accorde,
		coalesce(CY.mtt_cre_accorde_WTD,0) AS mtt_cre_accorde_WTD,
		coalesce(CY.mtt_cre_accorde_MTD,0) AS mtt_cre_accorde_MTD,
		coalesce(CY.mtt_cre_accorde_YTD,0) AS mtt_cre_accorde_YTD,
		coalesce(PM.mtt_cre_accorde_FM,0) AS mtt_cre_accorde_FM,
		coalesce(PY.mtt_cre_accorde_FY,0) AS mtt_cre_accorde_FY
	INTO
		[dbo].[WK_SSRS_CRE_CREDIT_ACC]
	FROM
		#rq_cre_acc_cp CY --Current Year
		LEFT JOIN	#rq_cre_acc_cp PM --Previous Year
			ON	DATEADD(MONTH,-1,CY.jour_alim) = PM.jour_alim
		LEFT JOIN	#rq_cre_acc_cp PY --Previous Year
			ON	DATEADD(YEAR,-1,CY.jour_alim) = PY.jour_alim
	ORDER BY
		CY.jour_alim
END