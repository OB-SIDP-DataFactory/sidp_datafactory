﻿CREATE PROC [dbo].[PKGCM_PROC_all_account] 
	@DATE_ALIM DATEtime2, 
	@jours_histo int   = 1, --valeur 1 par défaut = traitement nominal par défaut
	@nbRows int OUTPUT -- nb lignes processées
AS  BEGIN 

-- ===============================================================================================================================
--tables input : [$(DataHubDatabaseName)].[dbo].[PV_SF_ACCOUNT]
--				 [$(DataHubDatabaseName)].[dbo].[PV_SF_ACCOUNTHistory] 
--				  DIM_TEMPS
--tables output : WK_ALL_SFCRM_ACCOUNT
--description : créer une table des personnes (Account) avec historique sur une période déterminée par @date_alim et @jours_histo
-- ==============================================================================================================================

/************** créer un sous-ensemble de la table dimension temps **************/
-- declare @DATE_ALIM datetime  = '2017-05-10 19:09';
-- declare @jours_histo int = 9; --nombre de jours reprise historique, 1=traitement nominal
select PK_ID, StandardDate 
into #WK_TEMPS_ACCOUNT
from DIM_TEMPS
where cast(dateadd(day,-@jours_histo, @DATE_ALIM) as date) <= StandardDate and StandardDate < cast(@DATE_ALIM as date) ; 

/****** historique table salesforce clients ******/
-- table main + historique 
-- declare @DATE_ALIM datetime  = '2017-02-13';
-- declare @jours_histo int = 2;
IF OBJECT_ID('[WK_SFCRM_ACCOUNT]') IS NOT NULL
BEGIN TRUNCATE TABLE [WK_SFCRM_ACCOUNT] END
insert into [WK_SFCRM_ACCOUNT]
select Id_SF, IDCustomerSAB__pc, PersonBirthdate, OBFirstContactDate__c, OBTerminationDate__c, Occupation__pc, OccupationNiv1__pc, OccupationNiv3__pc , OptInDataTelco__c, OptInOrderTelco__c,Preattribution__c, RecordTypeId, Validity_StartDate, Validity_EndDate, CreatedDate
from [$(DataHubDatabaseName)].[dbo].[PV_SF_ACCOUNT]
where Validity_EndDate >= DATEADD(day,-@jours_histo,@DATE_ALIM)
union all 
select Id_SF, IDCustomerSAB__pc, PersonBirthdate, OBFirstContactDate__c, OBTerminationDate__c, Occupation__pc, OccupationNiv1__pc, OccupationNiv3__pc , OptInDataTelco__c, OptInOrderTelco__c,Preattribution__c, RecordTypeId, Validity_StartDate, Validity_EndDate, CreatedDate
from [$(DataHubDatabaseName)].[dbo].[PV_SF_ACCOUNTHistory]
where Validity_EndDate >= DATEADD(day,-@jours_histo,@DATE_ALIM) 
;
-- select * from [WK_SFCRM_ACCOUNT]

--SET NOCOUNT ON;

-- isoler période de temps requise
IF OBJECT_ID('[WK_ALL_SFCRM_ACCOUNT]') IS NOT NULL
BEGIN TRUNCATE TABLE [WK_ALL_SFCRM_ACCOUNT] END
insert into WK_ALL_SFCRM_ACCOUNT
select d.StandardDate as date_alim, Id_SF, IDCustomerSAB__pc, PersonBirthdate, OBFirstContactDate__c, OBTerminationDate__c, Occupation__pc, OccupationNiv1__pc, OccupationNiv3__pc , OptInDataTelco__c, OptInOrderTelco__c,Preattribution__c, RecordTypeId, Validity_StartDate, Validity_EndDate
from #WK_TEMPS_ACCOUNT d
outer apply 
	(
	select Id_SF, IDCustomerSAB__pc, PersonBirthdate, OBFirstContactDate__c, OBTerminationDate__c, Occupation__pc, OccupationNiv1__pc, OccupationNiv3__pc , OptInDataTelco__c, OptInOrderTelco__c, Validity_StartDate, Preattribution__c, RecordTypeId, CreatedDate, Validity_EndDate
	from ( select Id_SF, IDCustomerSAB__pc, PersonBirthdate, OBFirstContactDate__c, OBTerminationDate__c, Occupation__pc, OccupationNiv1__pc, OccupationNiv3__pc , OptInDataTelco__c, OptInOrderTelco__c, Preattribution__c, RecordTypeId, CreatedDate, Validity_StartDate, Validity_EndDate, row_number() over (partition by Id_SF order by Validity_StartDate) as Validity_RowNumber from [WK_SFCRM_ACCOUNT] ) t
	where ( ( cast(Validity_StartDate as date) <= d.StandardDate and d.StandardDate < cast(Validity_EndDate as date) ) or
	         ( cast(CreatedDate as date) <= d.StandardDate and d.StandardDate < cast(Validity_StartDate as date) and Validity_RowNumber = 1 ) )
	) c
;
-- select * from [WK_ALL_SFCRM_ACCOUNT]

select @nbRows = COUNT(*) from WK_ALL_SFCRM_ACCOUNT

END;