﻿-- =============================================
--tables input : [$(DataHubDatabaseName)].[exposition].[PV_SF_CASE]
--				 [$(DataHubDatabaseName)].[exposition].[PV_SF_OPPORTUNITY]
--tables output : [SAS_Vue_Armatis]
--description : construction de la table sas utilisée pour le rapport Armatis
-- =============================================
CREATE PROCEDURE [dbo].[PKGMK_PROC_ARMATIS] @nbRows int OUTPUT
AS
BEGIN

-- vider la table et la reconstruire
truncate table [dbo].SAS_Vue_Armatis;

insert into SAS_Vue_Armatis

	-- interfering with SELECT statements.
	select distinct
       cas.DTE_CREA as Date_ouverture,
	   cas.DTE_FER_REQ,
	   cas.DTE_DER_MOD,
       cas.LIB_STATUT,
       cas.LIB_STAT_APPEL,
	   cas.JAL_DEP,
	   cas.IDE_USER_SF,
	   cas.PROP_PREC,
	   cas.LIB_CAN_INT_ORI,
	   cas.LIB_TYP_ENR as TYP_ENR_CASE,
	   cas.LIB_SS_TYP,
	   cas.FILE_ATTENTE,
	   cas.LIB_DET_STAT_APPEL,
	   cas.LIB_TYP_REQ,
	   opp.LIB_STADE_VENTE,
	   usr.Lastname,
       usr.Firstname,
	   COUNT (distinct cas.IDE_REQ_SF) as Nb_Req
	   FROM [$(DataHubDatabaseName)].[exposition].[PV_SF_CASE] cas
	LEFT JOIN [$(DataHubDatabaseName)].[exposition].[PV_SF_OPPORTUNITY] opp ON cas.IDE_OPPRT_SF = opp.IDE_OPPRT_SF
	LEFT JOIN [$(DataHubDatabaseName)].[dbo].[PV_SF_USER] usr on usr.Id_SF = cas.IDE_USER_SF
	where cas.DTE_FIN_VAL = '9999-12-31 00:00:00.0000000'
	group by   cas.DTE_CREA,
	   cas.DTE_FER_REQ,
	   cas.DTE_DER_MOD,
       cas.LIB_STATUT,
       cas.LIB_STAT_APPEL,
	   cas.JAL_DEP,
	   cas.IDE_USER_SF,
	   cas.PROP_PREC,
	   cas.LIB_CAN_INT_ORI,
	   cas.LIB_TYP_ENR,
	   cas.LIB_SS_TYP,
	   cas.FILE_ATTENTE,
	   cas.LIB_DET_STAT_APPEL,
	   cas.LIB_TYP_REQ,
	   opp.LIB_STADE_VENTE,
	   usr.Lastname,
       usr.Firstname
	
	
	   
    -- Insert statements for procedure here
	SELECT @nbRows = COUNT(*) FROM SAS_Vue_Armatis;

END