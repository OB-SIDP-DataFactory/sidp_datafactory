﻿
CREATE PROCEDURE [dbo].[PKG_QOD_EXECUTE_AUTO_ALL_FAMILLE_CONTROLE]

AS
BEGIN

DECLARE 

-- Variables locales
@FAMILLE_CONTROL varchar(50),
@cursor_id int = 0
-- Curseur sur les famille_control sur laquelle on veut effectuer le contrôle
DECLARE Table_cursor CURSOR for 

SELECT DISTINCT FAMILLE_CONTROL 
FROM [dbo].[SIDP_QOD_LISTE_CONTROLE]
WHERE ISNULL(FLG_ACTIF,0)=1

 --Boucle sur les famille_control
 OPEN Table_cursor

 FETCH NEXT FROM Table_cursor INTO @FAMILLE_CONTROL

WHILE @@FETCH_STATUS = 0
BEGIN 
SET @cursor_id = @cursor_id + 1

print('dbo.PKG_QOD_EXEC_FAMILLE_CONTROLE '''+ @FAMILLE_CONTROL +'''')
EXEC dbo.PKG_QOD_EXEC_FAMILLE_CONTROLE  @FAMILLE_CONTROL 

FETCH NEXT FROM Table_cursor INTO @FAMILLE_CONTROL 
END   

--fin de la procedure
END