﻿

CREATE PROCEDURE [dbo].[PKG_QOD_CONTROLE_TABLE_ORACLE_VS_SQLSERVER]
@Database_src as varchar(200),
@Database_cible as varchar(200),
@table_source as varchar(200),
@table_cible as varchar(200),
@reqSQL as varchar(3000) = NULL,  
@reqOra as varchar(3000) = NULL,
@PERIMETRE_CONTROL as varchar(2000)  = NULL,
@FAMILLE_CONTROL as varchar(2000)  = NULL
-- Dans cette procédure nous considérerons la table source comme la source Oracle
-- @Database_src  = nom du schema Oracle
-- @Database_cible = nom de la database SQL Server
-- @table_source : table Oracle
-- @table_cible : table SQL Server

AS BEGIN

DECLARE @table_cursor_oracle table (
table_name varchar(100), 
column_name varchar(100),
data_type varchar(100)
)

DECLARE @table_cursor_sql table (
table_catalog varchar(50),
table_name varchar(100), 
column_name varchar(100),
ordinal_position varchar(100),
data_type varchar(100),
character_maximum_length varchar(100),
numeric_precision varchar(100),
numeric_scale varchar(100)
)
--ecriture de la requete de récupération des types de colonnes cible 
--depuis le dictionnaire de données Oracle

select @reqOra =  'select * from openquery(OAV,''select 
table_name, 
column_name, 
data_type
from DBA_tab_columns
where owner = '''''+@Database_src+'''''
and table_name = '''''+@table_source+''''''')'

insert into @table_cursor_oracle
exec (@reqOra)

--ecriture de la requete de récupération des types de colonnes cible 
--depuis le dictionnaire de données SQL Server

select @reqSQL = 'use '+@Database_cible+'
select 
table_catalog,
table_name,
column_name,
ordinal_position,
data_type,
character_maximum_length,
numeric_precision,
numeric_scale
from information_schema.columns
where table_name = '''+@table_cible+''''

-- exécution de la requete
insert into @table_cursor_sql
exec(@reqSQL);

-- requete de récupération de delta des colonnes
-- si une colonne dans la source Oracle n'est pas reprise alors celle-ci ressort
select
table_name_oracle,
column_name_oracle,
data_type_oracle,
max(table_name_sql),
max(column_name_sql),
max(data_type_sql)
from (
select 
o.table_name as table_name_oracle, 
o.column_name as column_name_oracle,
o.data_type as data_type_oracle,
s.table_name as table_name_sql, 
s.column_name as column_name_sql,
s.data_type as data_type_sql
 from
@table_cursor_oracle o
join ..DATATYPE_ORACLE_TO_SQLSERVER t on o.data_type = t.data_type_oracle
left join @table_cursor_sql s 
on o.column_name = s.column_name and s.data_type = t.data_type_sqlserver
) rapp
group by table_name_oracle,
column_name_oracle,
data_type_oracle
having max(table_name_sql) is null

end