﻿

CREATE PROC [dbo].[PKGCR_PROC_STOCK_CREDIT]
	@nbRows int OUTPUT -- nb lignes processées  
AS
BEGIN 
-- =====================================================================================================================
-- Author:		<Liliane, Huang>
-- Create date: <16/02/2018>
-- Description : <créer une table agrégée avec le nombre de crédits desirio en stock pour chaque jour d'alimentation>
--				vider les tables temporaires utilisées
-- Tables input : [CRE_CREDIT]
--					[CRE_OPPORTUNITE]
-- Table output : [CRE_STOCK_CREDIT]
-- ======================================================================================================================
--Insertion des données dans la table CRE_STOCK_CREDIT et aggrégation des crédits
INSERT INTO dbo.CRE_STOCK_CREDIT
			( [DTE_ALIM]
			  ,[COD_CAN_INT_ORI]
			  ,[LIB_CAN_INT_ORI]
			  ,[DTE_DER_DEB]
			  ,[WeekOfYear]
			  ,[IsoWeek]
			  ,[Month]
			  ,[Year]
			  ,[IsoWeekYear]
			  ,[NBE_CREDIT]
			  ,[NBE_CREDIT_STOCK]
			  ,[MTT_TOT_DEB]
			  ,[CRD]
)
SELECT CRE.[DTE_ALIM],
		OPP.[COD_CAN_INT_ORI],
		OPP.[LIB_CAN_INT_ORI],
		Cast(CRE.DTE_DER_DEB as date) as DTE_DER_DEB,
		tps.[WeekOfYear],
		tps.[IsoWeek],
		tps.[Month],
		tps.[Year],
		tps.[IsoWeekYear],
		COUNT(DISTINCT CRE.ID_CRE_CREDIT) AS NBE_CREDIT,
		SUM(CASE WHEN CRD <> 0 THEN 1 ELSE 0 END ) AS NBE_CREDIT_STOCK,
		SUM(CRE.[MTT_TOT_DEB]) AS MTT_TOT_DEB,
		SUM(CRE.[CRD]) AS CRE
FROM [dbo].[CRE_CREDIT] CRE
INNER JOIN [dbo].[CRE_OPPORTUNITE] OPP
ON CRE.IDE_OPPRT_SF = OPP.IDE_OPPRT_SF
LEFT JOIN DIM_TEMPS tps
ON CAST(CRE.DTE_DER_DEB as date) = tps.Date
WHERE [DTE_DER_DEB] IS NOT NULL
AND OPP.FLG_DER_ETAT_OPPRT = 1
GROUP BY CRE.[DTE_ALIM],
		OPP.[COD_CAN_INT_ORI],
		OPP.[LIB_CAN_INT_ORI],
		Cast(CRE.DTE_DER_DEB as date),
		tps.[WeekOfYear],
		tps.[IsoWeek],
		tps.[Month],
		tps.[Year],
		tps.[IsoWeekYear]

;	

select @nbRows = @@ROWCOUNT;

END