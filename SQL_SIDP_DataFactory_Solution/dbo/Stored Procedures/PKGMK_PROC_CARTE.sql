﻿CREATE PROC [dbo].[PKGMK_PROC_CARTE]   @DATE_ALIM DATE  
                                     ,@jours_histo int   --valeur 1 par défaut = traitement nominal par défaut
								     ,@nbRows int OUTPUT -- nb lignes processées
AS  
BEGIN 

if @DATE_ALIM is null 
  set @DATE_ALIM = GETDATE();
 
if @jours_histo is null 
  set @jours_histo = 1; --valeur 1 par défaut = traitement nominal par défaut

--------------------------------------///////////////// Step 1
--création d'un sous-ensemble de la table dimension temps
--purge
IF OBJECT_ID('tempdb..#WK_TEMPS_ACCOUNT') IS NOT NULL
BEGIN drop TABLE #WK_TEMPS_ACCOUNT END
 
select distinct 
       case when datediff(DAY,StandardDate,@DATE_ALIM) < 90 then StandardDate
		    else EOMONTH(StandardDate) end as StandardDate
into #WK_TEMPS_ACCOUNT
from DIM_TEMPS 
WHERE dateadd(DAY,- @jours_histo, @DATE_ALIM) <= StandardDate AND StandardDate < cast(@DATE_ALIM as date)

--select * from #WK_TEMPS_ACCOUNT

--------------------------------------///////////////// Step 2
--Merge de la table _Carte && _Carte_History et insertion dans une table work

--purge
IF OBJECT_ID('tempdb..#WK_SAB_CARTES') IS NOT NULL
BEGIN Drop TABLE #WK_SAB_CARTES END

select * into  #WK_SAB_CARTES
from 
(
select DWHCARCAR
      ,DWHCARCOM
	  ,DWHCARLIB
	  ,DWHCAROPT
	  ,DWHCARACH
	  ,DWHCARMRA
	  ,DWHCARENR
	  ,Validity_StartDate
	  ,Validity_EndDate
from [$(DataHubDatabaseName)].[dbo].[VW_PV_SA_Q_CARTE]
where Validity_EndDate >= DATEADD(day,-@jours_histo,@DATE_ALIM)
and DWHCARCET ='002' -- filtre carte active
)r

--------------------------------------///////////////// Step 3
--Photo sur la période requise et insertion dans une table res

--purge
IF OBJECT_ID('tempdb..#RES_CARTES') IS NOT NULL
BEGIN DROP TABLE #RES_CARTES END

--insertion dans table work
select * into #RES_CARTES
from
(
select StandardDate as Date_traitement
      ,c.DWHCARCAR
	  ,DWHCARCOM
	  ,c.DWHCARLIB
	  ,c.DWHCAROPT
	  ,c.DWHCARACH
	  ,c.DWHCARMRA
	  ,c.DWHCARENR
	  ,c.Validity_StartDate
	  ,c.Validity_EndDate

from #WK_TEMPS_ACCOUNT d

outer apply 
	(
	select DWHCARCAR
	      ,DWHCARCOM
		  ,DWHCARLIB
		  ,DWHCAROPT
		  ,DWHCARACH
		  ,DWHCARMRA
		  ,DWHCARENR
		  ,Validity_StartDate
		  ,Validity_EndDate

	from #WK_SAB_CARTES

	where (cast(Validity_StartDate as date) <= d.StandardDate and d.StandardDate <= cast(Validity_EndDate as date) )
	) c
)r

--select * from  #RES_CARTES;

--------------------------------------///////////////// Step 4
--nettoyage + insertion dans la table cible
--nettoyage de la table cible (suppréssion des lignes a recharger)

delete from MKT_CARTE where [DTE_TRAITEMENT] in (select StandardDate from #WK_TEMPS_ACCOUNT)
--Insertion dans la table cible
insert into [dbo].[MKT_CARTE]   
(  [DTE_TRAITEMENT]  
  ,[NUM_CPT_SAB]     
  ,[NUM_CLI]         
  ,[COD_CAR]         
  ,[LIB_COD_CAR]     
  ,[OPT_DEB]         
  ,[LIB_OPT_DEB]     
  ,[FLG_PROT_VIE_COU]
  ,[FLG_PROT_ACH]    
  ,[FLG_PROT_IDEN]   
  ,[FLG_MOY_PAI]     
  ,[MTT_PLD_ACH]     
  ,[MTT_PLD_RET]     
  ,[DTE_CMD]          )

select     c.Date_traitement
		  ,cp.DWHCPTCOM 
		  ,cp.DWHCPTPPAL
		  ,c.DWHCARCAR
		  ,c.DWHCARLIB
		  ,c.DWHCAROPT
		  ,o.LIB_OPT_DEB
		  ,case when se.ID_FE_SERVICEPRODUCT = 2 then 'Oui' else 'Non' end as TOP_PRO_VIE_COU
		  ,case when se.ID_FE_SERVICEPRODUCT = 3 then 'Oui' else 'Non' end as TOP_PRO_ACH
		  ,case when se.ID_FE_SERVICEPRODUCT = 4 then 'Oui' else 'Non' end as TOP_PRO_IDE
		  ,case when se.ID_FE_SERVICEPRODUCT = 1 then 'Oui' else 'Non' end as TOP_PRO_PAI
		  ,c.DWHCARACH
		  ,c.DWHCARMRA
		  ,c.DWHCARENR
from #RES_CARTES c

inner join [$(DataHubDatabaseName)].[dbo].[PV_SA_Q_COMPTE] cp 
		  on cp.DWHCPTCOM = c.DWHCARCOM and DWHCPTRUB in ('251180','254181','254111')

left join [dbo].DIM_OPT_DEB o
          on o.COD_OPT_DEB = c.DWHCAROPT

left join [$(DataHubDatabaseName)].[dbo].PV_FE_EQUIPMENT e
          on e.EQUIPMENT_NUMBER = cp.DWHCPTCOM

left join [$(DataHubDatabaseName)].[dbo].PV_FE_EQUIPMENTSVCASSOC es
          on es.EQUIPMENT_ID = e.ID_FE_EQUIPMENT

left join [$(DataHubDatabaseName)].[dbo].PV_FE_SERVICEPRODUCT se
          on se.ID_FE_SERVICEPRODUCT = es.SERVICE_PRODUCT_ID

SELECT @nbRows = @@ROWCOUNT

------Suppression des enregistrement > 90 jrs 
delete from [dbo].[MKT_CARTE]  where (DATEDIFF(DAY,[DTE_TRAITEMENT],@date_alim)>90 and [DTE_TRAITEMENT] !=EOMONTH([DTE_TRAITEMENT]))

END