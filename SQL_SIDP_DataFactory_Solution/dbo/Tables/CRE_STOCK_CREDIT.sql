﻿CREATE TABLE [dbo].[CRE_STOCK_CREDIT] (
    [ID_CRE_STOCK_CREDIT] INT             IDENTITY (1, 1) NOT NULL,
    [DTE_ALIM]            DATE            NULL,
    [COD_CAN_INT_ORI]     NVARCHAR(80)   NULL,
    [LIB_CAN_INT_ORI]     NVARCHAR(80)   NULL,
    [DTE_DER_DEB]         DATE            NULL,
    [WeekOfYear]          INT             NULL,
    [IsoWeek]             INT             NULL,
    [Month]               INT             NULL,
    [Year]                INT             NULL,
    [IsoWeekYear]         INT             NULL,
    [NBE_CREDIT]          INT             NULL,
    [NBE_CREDIT_STOCK]    INT             NULL,
    [MTT_TOT_DEB]         DECIMAL (11, 2) NULL,
    [CRD]                 DECIMAL (11, 2) NULL,
    CONSTRAINT [PK_CRE_STOCK_CREDIT] PRIMARY KEY CLUSTERED ([ID_CRE_STOCK_CREDIT] ASC)
);

