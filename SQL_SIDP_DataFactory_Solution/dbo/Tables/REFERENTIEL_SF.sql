﻿CREATE TABLE [dbo].[REFERENTIEL_SF] (
    [ID_REFERENTIEL_SF]  BIGINT         IDENTITY (1, 1) NOT NULL,
    [COD_OBJ_SF]         NVARCHAR(255) NULL,
    [COD_CHMP_SF]        NVARCHAR(255) NULL,
    [NOM_CHMP_SF]        NVARCHAR(255) NULL,
    [COD_SF]             NVARCHAR(255) NULL,
    [LIBL_SF]            NVARCHAR(255) NULL,
    [COD_CHMP_PARN_SF]   NVARCHAR(255) NULL,
    [NOM_CHMP_PARN_SF]   NVARCHAR(255) NULL,
    [COD_PARN_SF]        NVARCHAR(255) NULL,
    [DAT_CRTN_ENRG]      DATETIME       NOT NULL,
    [DAT_DERN_MODF_ENRG] DATETIME       NOT NULL,
    [DAT_DEBT_VALD]      DATE           NOT NULL,
    [DAT_FIN_VALD]       DATE           NOT NULL,
    [FLG_ACTIF]          BIT            NOT NULL
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REFERENTIEL_SF', @level2type = N'COLUMN', @level2name = N'FLG_ACTIF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de fin de validité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REFERENTIEL_SF', @level2type = N'COLUMN', @level2name = N'DAT_FIN_VALD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de début de validité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REFERENTIEL_SF', @level2type = N'COLUMN', @level2name = N'DAT_DEBT_VALD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REFERENTIEL_SF', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REFERENTIEL_SF', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code parent SF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REFERENTIEL_SF', @level2type = N'COLUMN', @level2name = N'COD_PARN_SF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom du champ parent SF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REFERENTIEL_SF', @level2type = N'COLUMN', @level2name = N'NOM_CHMP_PARN_SF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code du champ parent SF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REFERENTIEL_SF', @level2type = N'COLUMN', @level2name = N'COD_CHMP_PARN_SF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé SF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REFERENTIEL_SF', @level2type = N'COLUMN', @level2name = N'LIBL_SF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code SF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REFERENTIEL_SF', @level2type = N'COLUMN', @level2name = N'COD_SF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom du champ SF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REFERENTIEL_SF', @level2type = N'COLUMN', @level2name = N'NOM_CHMP_SF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code du champ SF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REFERENTIEL_SF', @level2type = N'COLUMN', @level2name = N'COD_CHMP_SF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code d''objet SF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REFERENTIEL_SF', @level2type = N'COLUMN', @level2name = N'COD_OBJ_SF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant technique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REFERENTIEL_SF', @level2type = N'COLUMN', @level2name = N'ID_REFERENTIEL_SF';
GO
CREATE NONCLUSTERED COLUMNSTORE INDEX IX_NCCS_REF_SF_OBJ_CHM
ON dbo.REFERENTIEL_SF (COD_OBJ_SF, COD_CHMP_SF, COD_SF, LIBL_SF, DAT_DEBT_VALD, DAT_FIN_VALD)
GO