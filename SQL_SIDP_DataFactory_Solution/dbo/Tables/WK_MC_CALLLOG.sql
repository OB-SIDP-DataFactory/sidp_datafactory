﻿CREATE TABLE [dbo].[WK_MC_CALLLOG] (
    [ID_TASK]               NVARCHAR(50)  NULL,
    [ID_CONTACT]            NVARCHAR(50)  NULL,
    [ID_OPPORTUNITY]        NVARCHAR(50)  NULL,
    [ID_CASE]               NVARCHAR(50)  NULL,
    [ID_CAMPAIGN]           NVARCHAR(50)  NULL,
    [CASE_TYPE]             NVARCHAR(50)  NULL,
    [CASE_SUBTYPE]          NVARCHAR(50)  NULL,
    [CALL_DATE]             DATE           NULL,
    [HEURE_APPEL]           NVARCHAR(50)  NULL,
    [PRE_ATTRIBUTION]       NVARCHAR(255) NULL,
    [SEGMENT_GEOLIFE]       NVARCHAR(255) NULL,
    [SCORE_DE_COMPORTEMENT] NVARCHAR(255) NULL,
    [AGE]                   INT            NULL,
    [STATUT_OPPORT]         NVARCHAR(50)  NULL,
    [SCORE_ENTREE_RELATION] NVARCHAR(50)  NULL,
    [OFFER_CODE]            NVARCHAR(50)  NULL,
    [OFFER_NAME]            NVARCHAR(50)  NULL,
    [CANAL_ORIGINE]         NVARCHAR(50)  NULL,
    [FLG_CONTACTE]          NCHAR(1)   NULL,
    [FLG_CONTACT_POSITIF]   NCHAR(1)   NULL,
    [FLG_CONTACT_BOUNCES]   NCHAR(1)   NULL,
    [FLG_CONTACT_STOP]      NCHAR(1)   NULL,
    [DATE_CHARGEMENT]       DATE           NULL
);

