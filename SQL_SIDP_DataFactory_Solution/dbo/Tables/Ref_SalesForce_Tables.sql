﻿CREATE TABLE [dbo].[Ref_SalesForce_Tables]
(
	[Id]				INT				IDENTITY (1, 1)						NOT NULL,
	[EnvironmentId]		INT													NOT NULL,
	[TableName]			VARCHAR(100)										NOT NULL,
	[IsEnabled]			BIT				DEFAULT (1)							NOT NULL,
	[ValidityStartDate] DATETIME2(7)										NOT NULL,
	[ValidityEndDate]	DATETIME2(7)	DEFAULT ('9999-12-31 23:59:59.999')	NOT NULL
)
GO