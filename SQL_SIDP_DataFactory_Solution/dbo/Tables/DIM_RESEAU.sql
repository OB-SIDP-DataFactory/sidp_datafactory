﻿CREATE TABLE [dbo].[DIM_RESEAU] (
    [PK_ID]              INT           IDENTITY (1, 1) NOT NULL,
    [CODE_SF]            VARCHAR(50)  NULL,
    [LIBELLE]            VARCHAR(50)  NULL,
    [PARENT_ID]          VARCHAR(50)  NULL,
    [Validity_StartDate] DATETIME2 (7) NOT NULL,
    [Validity_EndDate]   DATETIME2 (7) DEFAULT ('9999-12-31') NOT NULL,
    CONSTRAINT [PK_DIM_RESEAU] PRIMARY KEY CLUSTERED ([PK_ID] ASC)
)
GO