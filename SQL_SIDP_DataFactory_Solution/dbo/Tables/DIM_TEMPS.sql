﻿CREATE TABLE [dbo].[DIM_TEMPS] (
    [PK_ID]        INT          NOT NULL,
    [Date]         DATETIME     NOT NULL,
    [Day]          CHAR(2)     NOT NULL,
    [DaySuffix]    VARCHAR(4)  NOT NULL,
    [DayOfWeek]    VARCHAR(9)  NOT NULL,
    [DOWInMonth]   TINYINT      NOT NULL,
    [DayOfYear]    INT          NOT NULL,
    [WeekOfYear]   TINYINT      NOT NULL,
    [WeekOfMonth]  TINYINT      NOT NULL,
    [IsoWeek]      TINYINT      NOT NULL,
    [IsoWeekYear]  CHAR(4)     NOT NULL,
    [Month]        CHAR(2)     NOT NULL,
    [MonthName]    VARCHAR(9)  NOT NULL,
    [MonthYear]    VARCHAR(50) NULL,
    [Quarter]      TINYINT      NOT NULL,
    [QuarterName]  VARCHAR(6)  NOT NULL,
    [Year]         CHAR(4)     NOT NULL,
    [BusinessDay]  TINYINT      NOT NULL,
    [StandardDate] DATE         NULL,
    CONSTRAINT [PK_DIM_TEMPS] PRIMARY KEY CLUSTERED ([PK_ID] ASC)
);
GO
CREATE NONCLUSTERED INDEX IX_NC_DIM_TMP_STD_DTE
ON dbo.DIM_TEMPS (StandardDate)
INCLUDE ([Date])
GO
CREATE NONCLUSTERED INDEX IX_NC_DIM_TMP_DTE_BUS_WK
ON dbo.DIM_TEMPS (BusinessDay, [DayOfWeek])
INCLUDE (MonthYear, [Year], StandardDate)
GO
CREATE NONCLUSTERED INDEX IX_NC_DIM_TMP_DTE_WK
ON [dbo].[DIM_TEMPS] ([Date])
INCLUDE ([IsoWeek])
GO
CREATE NONCLUSTERED INDEX IX_NC_DIM_TMP_DTE_WK_YR
ON [dbo].[DIM_TEMPS] ([Date])
INCLUDE ([IsoWeekYear])
GO
