﻿CREATE TABLE [dbo].[SIDP_QOD_EXECUTION] (
    [RUN_ID]     INT      IDENTITY (1, 1) NOT NULL,
    [DATE_EXEC]  DATETIME NULL,
    [ID_CONTROL] INT      NULL,
    PRIMARY KEY CLUSTERED ([RUN_ID] ASC)
)
GO
CREATE NONCLUSTERED INDEX [IX_NC_QOD_EXE_CNT]
ON [dbo].[SIDP_QOD_EXECUTION] ([ID_CONTROL],[DATE_EXEC])
INCLUDE ([RUN_ID])
GO
