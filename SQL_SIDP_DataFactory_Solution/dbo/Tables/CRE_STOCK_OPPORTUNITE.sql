﻿CREATE TABLE [dbo].[CRE_STOCK_OPPORTUNITE] (
    [ID_CRE_STOCK_OPPORTUNITE] INT            IDENTITY (1, 1) NOT NULL,
    [DTE_ALIM]                 DATE           NULL,
    [NBE_OPPRT]                INT            NULL,
    [STADE_VENTE]              NVARCHAR(40)  NULL,
    [LIB_STADE_VENTE]          NVARCHAR(80)  NULL,
    [FLG_IND]                  VARCHAR(3)    NOT NULL,
    [IDE_USER_CREA_SF]         NVARCHAR(18)  NULL,
    [DTE_CREA]                 DATE           NULL,
    [DTE_CLO_OPPRT]            DATE           NULL,
    [AGE_OPPRT]                INT            NULL,
    [IDE_USER_SF]              NVARCHAR(18)  NULL,
    [COD_VENDEUR]              NVARCHAR(80)  NULL,
    [COD_BOUT]                 NVARCHAR(80)  NULL,
    [COD_OFF_COM]              NVARCHAR(80)  NULL,
    [LABEL_OFF_COM]            NVARCHAR(80)  NULL,
    [SCO_ENT_REL]              NVARCHAR(255) NULL,
    [CAN_INT_ORI_IND]          NVARCHAR(255) NULL,
    [LIB_CAN_INT_ORI_IND]      NVARCHAR(80)  NULL,
    [COD_CAN_INT_ORI]          NVARCHAR(255) NULL,
    [LIB_CAN_INT_ORI]          NVARCHAR(80)  NULL,
    [COD_CAN_DIS]              NVARCHAR(255) NULL,
    [LIB_CAN_DIS]              NVARCHAR(80)  NULL,
    [COD_RES_DIS]              NVARCHAR(80)  NULL,
    [LIB_RES_DIS]              NVARCHAR(80)  NULL,
    [ENT_DIS]                  NVARCHAR(80)  NULL,
    [LIB_ENT_DIS]              NVARCHAR(80)  NULL,
    [COD_CAN_INT]              NVARCHAR(80)  NULL,
    [LIB_CAN_INT]              NVARCHAR(80)  NULL,
    CONSTRAINT [PK_CRE_STOCK_OPPORTUNITE] PRIMARY KEY CLUSTERED ([ID_CRE_STOCK_OPPORTUNITE] ASC)
)
GO
CREATE NONCLUSTERED INDEX IX_NC_CRE_STK_OPP_DTE_ALM
ON [dbo].[CRE_STOCK_OPPORTUNITE] ([DTE_ALIM])
INCLUDE ([NBE_OPPRT], [STADE_VENTE], [DTE_CREA], [COD_CAN_INT_ORI])
GO