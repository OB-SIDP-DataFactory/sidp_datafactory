﻿CREATE TABLE [dbo].[MKT_NB_OP] (
    [PK_ID]      INT          IDENTITY (1, 1) NOT NULL,
    [date_alim]  DATE         NULL,
    [NUM_CPT]    VARCHAR(20) NULL,
    [TYPE]       VARCHAR(20) NULL,
    [cum_nb_ope] INT          NULL
);
GO
CREATE NONCLUSTERED INDEX IX_NC_MKT_OP_TYPE
ON dbo.MKT_NB_OP (TYPE ASC)
INCLUDE (date_alim, NUM_CPT, cum_nb_ope)
GO