﻿CREATE TABLE [dbo].[Ref_FrontEnd_Tables](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EnvironmentId] [int] NOT NULL,
	[TableName] [varchar](100) NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[ValidityStartDate] [datetime2](7) NOT NULL,
	[ValidityEndDate] [datetime2](7) NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Ref_FrontEnd_Tables] ADD  DEFAULT ((1)) FOR [IsEnabled]
GO

ALTER TABLE [dbo].[Ref_FrontEnd_Tables] ADD  DEFAULT ('9999-12-31 23:59:59.999') FOR [ValidityEndDate]
GO