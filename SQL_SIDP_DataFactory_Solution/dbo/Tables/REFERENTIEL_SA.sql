﻿CREATE TABLE [dbo].[REFERENTIEL_SA] (
    [ID_REFERENTIEL_SA]  BIGINT         IDENTITY (1, 1) NOT NULL,
    [SOUR_DONN]          NVARCHAR(10)  NULL,
    [LIBL_SOUR_DONN]     NVARCHAR(50)  NULL,
    [COL1]               NVARCHAR(255) NULL,
    [COL2]               NVARCHAR(255) NULL,
    [COL3]               NVARCHAR(255) NULL,
    [COL4]               NVARCHAR(255) NULL,
    [COL5]               NVARCHAR(255) NULL,
    [DAT_CRTN_ENRG]      DATETIME       NOT NULL,
    [DAT_DERN_MODF_ENRG] DATETIME       NOT NULL,
    [DAT_DEBT_VALD]      DATE           NOT NULL,
    [DAT_FIN_VALD]       DATE           NOT NULL,
    [FLG_ACTIF]          BIT            NOT NULL
)
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REFERENTIEL_SA', @level2type = N'COLUMN', @level2name = N'FLG_ACTIF'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de fin de validité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REFERENTIEL_SA', @level2type = N'COLUMN', @level2name = N'DAT_FIN_VALD'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de début de validité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REFERENTIEL_SA', @level2type = N'COLUMN', @level2name = N'DAT_DEBT_VALD'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REFERENTIEL_SA', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REFERENTIEL_SA', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Colonne 5', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REFERENTIEL_SA', @level2type = N'COLUMN', @level2name = N'COL5'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Colonne 4', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REFERENTIEL_SA', @level2type = N'COLUMN', @level2name = N'COL4'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Colonne 3', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REFERENTIEL_SA', @level2type = N'COLUMN', @level2name = N'COL3'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Colonne 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REFERENTIEL_SA', @level2type = N'COLUMN', @level2name = N'COL2'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Colonne 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REFERENTIEL_SA', @level2type = N'COLUMN', @level2name = N'COL1'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Source de données', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REFERENTIEL_SA', @level2type = N'COLUMN', @level2name = N'SOUR_DONN'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant technique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REFERENTIEL_SA', @level2type = N'COLUMN', @level2name = N'ID_REFERENTIEL_SA'
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Libellé source de données', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REFERENTIEL_SA', @level2type = N'COLUMN', @level2name = N'LIBL_SOUR_DONN'
GO