﻿CREATE TABLE [dbo].[sas_vue_ferm_cpt] (
    [date_alim]                 DATE            NULL,
    [NUM_CPT_CLI]               VARCHAR(20)    NOT NULL,
    [Id_SF_Opp]                 NVARCHAR(50)   NULL,
    [Id_SF_Account]             NVARCHAR(50)   NULL,
    [AGE_CLI]                   INT             NULL,
    [AGE_CPT]                   INT             NULL,
    [cum_mtt_facture]           DECIMAL (16, 2) NULL,
    [MOTIF_FERMETURE]           VARCHAR(50)    NULL,
    [regroup_mot_ferm]          VARCHAR(50)    NULL,
    [CSP_CLI]                   VARCHAR(50)    NULL,
    [CSP_CLI_NIV1]              NVARCHAR(255)  NULL,
    [CSP_CLI_NIV3]              NVARCHAR(255)  NULL,
    [date_fermeture]            DATE            NULL,
    [canal_interaction_origine] VARCHAR(50)    NULL,
    [reseau_origine]            VARCHAR(50)    NULL,
    [type_compte]               NVARCHAR(50)   NULL,
    [offre_commerciale]         NVARCHAR(50)   NULL,
    [code_sab_type_compte]      NVARCHAR(10)   NULL,
    CONSTRAINT [PK_sas_vue_ferm_cpt] PRIMARY KEY CLUSTERED ([NUM_CPT_CLI] ASC)
);


