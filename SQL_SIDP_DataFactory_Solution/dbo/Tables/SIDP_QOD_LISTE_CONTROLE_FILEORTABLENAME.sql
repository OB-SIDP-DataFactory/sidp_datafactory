﻿CREATE TABLE [dbo].[SIDP_QOD_LISTE_CONTROLE_FILEORTABLENAME] (
    [ID_CONTROL]      INT            NOT NULL,
    [COD_CONTROL]     VARCHAR(80)   NOT NULL,
    [FAMILLE_CONTROL] NVARCHAR(50)  NULL,
    [FileOrTableName] NVARCHAR(255) NULL,
    [FLG_ACTIF]       INT            NULL
);

