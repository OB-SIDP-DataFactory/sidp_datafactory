﻿CREATE TABLE [dbo].[WK_SAB_CARTES] (
    [DWHCARCOM]          VARCHAR(20) NULL,
    [DWHCARNUS]          INT          NULL,
    [DWHCARCAR]          VARCHAR(6)  NULL,
    [DWHCARCET]          VARCHAR(3)  NULL,
    [DWHCARREM]          DATE         NULL,
    [DWHCARVAL]          DATE         NULL,
    [Validity_StartDate] DATETIME     NULL,
    [Validity_EndDate]   DATETIME     NULL
);


