﻿CREATE TABLE [dbo].[REF_CODE] (
    [CODE]               NVARCHAR(255) NULL,
    [END_DATE]           DATETIME       NULL,
    [PRIORITY]           NUMERIC (19)   NULL,
    [REF_FAMILY_ID]      NUMERIC (19)   NULL,
    [START_DATE]         DATETIME       NULL,
    [CODE_ID]            NUMERIC (19)   NULL,
    [DAT_CRTN_ENRG]      DATETIME       NOT NULL,
    [DAT_DERN_MODF_ENRG] DATETIME       NOT NULL,
    [DAT_DEBT_VALD]      DATE           NOT NULL,
    [DAT_FIN_VALD]       DATE           NOT NULL,
    [FLG_ENRG_ACTIF]     BIT            NOT NULL
);

