﻿CREATE TABLE [dbo].[wk_synthese_prod] (
    [date_alim]            DATE           NULL,
    [jour_alim]            DATE           NULL,
    [NUM_CPT]              VARCHAR(20)   NULL,
    [flag_ouvert]          INT            NULL,
    [flag_ferm]            INT            NULL,
    [code_sab_type_compte] NVARCHAR(10)   NULL
);

