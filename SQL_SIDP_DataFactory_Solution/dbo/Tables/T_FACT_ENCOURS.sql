﻿CREATE TABLE [dbo].[T_FACT_ENCOURS] (
    [PK_T_FACT_ENCOURS]  INT             IDENTITY (1, 1) NOT NULL,
    [DT_ALIM]            DATE            NULL,
    [CD_MARCHE]          VARCHAR(50)    NULL,
    [CD_PRODUIT_ENCOURS] VARCHAR(50)    NULL,
    [CD_PRODUIT_ELANCIO] VARCHAR(50)    NULL,
    [CD_RESEAU]          VARCHAR(50)    NULL,
    [FLAG_ER]            INT             NULL,
    [ENCOURS]            DECIMAL (18, 3) NULL,
    [DT_DER_MOIS]        DATE            NULL,
    [DER_MOIS_ENCOURS]   DECIMAL (18, 3) NULL,
    CONSTRAINT [T_FACT_ENCOURS_PK] PRIMARY KEY CLUSTERED ([PK_T_FACT_ENCOURS] ASC)
);

