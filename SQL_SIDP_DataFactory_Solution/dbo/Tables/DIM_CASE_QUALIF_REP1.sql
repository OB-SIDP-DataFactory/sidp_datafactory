﻿CREATE TABLE [dbo].[DIM_CASE_QUALIF_REP1] (
    [PK_ID]              INT            IDENTITY (1, 1) NOT NULL,
    [CODE_SF]            NVARCHAR(255) NULL,
    [LIBELLE]            NVARCHAR(255) NULL,
    [Validity_StartDate] DATETIME       CONSTRAINT [DF_DIM_CASE_QUALIF_REP1_Validity_StartDate] DEFAULT ('2017-01-01') NULL,
    [Validity_EndDate]   DATETIME       CONSTRAINT [DF_DIM_CASE_QUALIF_REP1_Validity_End_Date] DEFAULT ('9999-12-31') NULL,
    PRIMARY KEY CLUSTERED ([PK_ID] ASC)
);

