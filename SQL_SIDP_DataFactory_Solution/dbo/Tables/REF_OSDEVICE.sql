﻿CREATE TABLE [dbo].[REF_OSDEVICE] (
    [ID]                    BIGINT         IDENTITY (1, 1) NOT NULL,
    [REF_OSDEVICE_ID]       NUMERIC (38)   NULL,
    [WIRECARD_CARD_PROGRAM] NVARCHAR(255) NULL,
    [DAT_CRTN_ENRG]         DATETIME       NOT NULL,
    [DAT_DERN_MODF_ENRG]    DATETIME       NOT NULL,
    [DAT_DEBT_VALD]         DATE           NOT NULL,
    [DAT_FIN_VALD]          DATE           NOT NULL,
    [FLG_ENRG_ACTIF]        BIT            NOT NULL
);

