﻿CREATE TABLE [dbo].[SAS_Vue_MC_Parrainage]
(
    [Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Date_observation] [date] NOT NULL,
	[Nb_Filleul] [smallint] NULL,
	[Nb_Parrain] [int] NULL
)