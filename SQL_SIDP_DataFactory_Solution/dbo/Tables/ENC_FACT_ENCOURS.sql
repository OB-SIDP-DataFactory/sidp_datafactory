﻿CREATE TABLE [dbo].[ENC_FACT_ENCOURS](
	[ID_TECH] [INT] IDENTITY(1,1) NOT NULL,
	[ID_TECH_PRODUIT] [INT] NULL,
	[ID_TECH_TEMPS] [INT] NULL,
	[ID_TECH_MARCHE] [INT] NULL,
	[ID_TECH_RESEAU] [INT] NULL,
	[SOLDE_COMPTABLE] DECIMAL(18, 3) NULL,
	[NUMERO_COMPTE] [VARCHAR](30) NULL,
	[SENS_DU_SOLDE] [VARCHAR](150) NULL
)
GO
ALTER TABLE [dbo].[ENC_FACT_ENCOURS]
ADD CONSTRAINT [FK_MarcheEncours]
FOREIGN KEY([ID_TECH_MARCHE])
REFERENCES [dbo].[ENC_DIM_MARCHE] ([ID_TECH])
GO
ALTER TABLE [dbo].[ENC_FACT_ENCOURS]
CHECK CONSTRAINT [FK_MarcheEncours]
GO
ALTER TABLE [dbo].[ENC_FACT_ENCOURS]
ADD CONSTRAINT [FK_ProductEncours]
FOREIGN KEY([ID_TECH_PRODUIT])
REFERENCES [dbo].[ENC_DIM_PRODUIT] ([ID_TECH])
GO
ALTER TABLE [dbo].[ENC_FACT_ENCOURS]
CHECK CONSTRAINT [FK_ProductEncours]
GO
ALTER TABLE [dbo].[ENC_FACT_ENCOURS]
ADD CONSTRAINT [FK_ReseauEncours]
FOREIGN KEY([ID_TECH_RESEAU])
REFERENCES [dbo].[ENC_DIM_RESEAU] ([ID_TECH])
GO
ALTER TABLE [dbo].[ENC_FACT_ENCOURS]
CHECK CONSTRAINT [FK_ReseauEncours]
GO