﻿CREATE TABLE [dbo].[SAS_Vue_MC_Parrain] (
    [Id]                     BIGINT          IDENTITY (1, 1) NOT NULL,
    [ID_Contact_MC]          NVARCHAR(18)   NULL,
    [Date_Parrainage]        DATETIME        NULL,
    [Message_Name]           NVARCHAR(4000) NULL,
    [Nb_Filleul_Enseigne]    INT             NULL,
    [ID_Contact_SF]          NVARCHAR(18)   NULL,
    [Indication]             NVARCHAR(80)   NULL,
    [Entite_Distrib]         NVARCHAR(255)  NULL,
    [Qualification_Employe]  NVARCHAR(80)   NULL,
    [Pre_Attribution]        INT             NULL,
    [Score_Comportement]     NVARCHAR(80)   NULL,
    [Segment_Geolife]        NVARCHAR(260)  NULL,
    [Age]                    INT             NULL,
    [Score_Entree_Relation]  INT             NULL,
    [Canal_Interaction_Orig] NVARCHAR(255)  NULL,
    [Reseau_Distrib]         NVARCHAR(255)  NULL,
    [Reseau_Apporteur]       NVARCHAR(255)  NULL,
    [Nb_Filleul_Transfo]     INT             NULL,
    [Nb_Sortie_Negative]     INT             NULL
);

