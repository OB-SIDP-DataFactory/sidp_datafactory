﻿CREATE TABLE [dbo].[DIM_CASE_TYPE] (
    [PK_ID]              INT            IDENTITY (1, 1) NOT NULL,
    [CODE_SF]            NVARCHAR(18)  NULL,
    [LIBELLE]            NVARCHAR(255) NULL,
    [PARENT_ID]          NVARCHAR(18)  NULL,
    [Validity_StartDate] DATETIME       CONSTRAINT [DF_DIM_CASE_TYPE_Validity_StartDate] DEFAULT ('2017-01-01') NULL,
    [Validity_EndDate]   DATETIME       CONSTRAINT [DF_DIM_CASE_TYPE_Validity_EndDate] DEFAULT ('9999-12-31') NULL,
    PRIMARY KEY CLUSTERED ([PK_ID] ASC)
);


