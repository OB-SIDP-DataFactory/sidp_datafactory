﻿CREATE TABLE [dbo].[REF_BIC_BANQUE] (
    [NATR_IDNT_IBAN] VARCHAR (15)  NULL,
    [COD_BIC]        VARCHAR (11)  NULL,
    [LIBL_BANQ]      VARCHAR (105) NULL,
    [COD_PAYS]       VARCHAR (2)   NULL,
    [PAYS]           VARCHAR (70)  NULL
);

