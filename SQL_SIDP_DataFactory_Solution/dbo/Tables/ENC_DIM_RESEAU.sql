﻿CREATE TABLE [dbo].[ENC_DIM_RESEAU](
	[ID_TECH] [INT] IDENTITY(1,1) NOT NULL,
	[CODE_RESEAU_N1] [VARCHAR](50) NULL,
	[LIBELLE_RESEAU_N1] [VARCHAR](250) NULL,
	[CODE_RESEAU_N2] [VARCHAR](50) NULL,
	[LIBELLE_RESEAU_N2] [VARCHAR](250) NULL,
	[CODE_RESEAU] [VARCHAR](50) NULL,
	[LIBELLE_RESEAU] [VARCHAR](250) NULL,
	[CODE_APPORTEUR] [VARCHAR](150) NULL,
	[INSERT_DATE] [SMALLDATETIME] NULL,
	[UPDATE_DATE] [SMALLDATETIME] NULL,
	[FLAG_ACTIF] [INT] NULL, 
    CONSTRAINT [PK_ENC_DIM_RESEAU] PRIMARY KEY ([ID_TECH])
)
GO
ALTER TABLE [dbo].[ENC_DIM_RESEAU] ADD  CONSTRAINT [DF_ENC_DIM_RESEAU_FLAG_ACTIF]  DEFAULT ((1)) FOR [FLAG_ACTIF]
GO