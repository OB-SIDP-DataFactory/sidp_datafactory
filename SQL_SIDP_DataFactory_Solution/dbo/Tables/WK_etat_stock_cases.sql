﻿CREATE TABLE [dbo].[WK_etat_stock_cases] (
    [PK_ID]               NVARCHAR(50) NULL,
    [DATE]                DATE          NULL,
    [FK_RECORDTYPE]       INT           NULL,
    [RECORDTYPE_LABEL]    VARCHAR(100) NULL,
    [CASES_CREES]         INT           NULL,
    [CASES_TRAITES]       INT           NULL,
    [CASES_EN_COURS]      INT           NULL,
    [CASES_CREES_CUMUL]   INT           NULL,
    [CASES_TRAITES_CUMUL] INT           NULL,
    [CASES_TRAITES_WTD]   INT           NULL,
    [CASES_TRAITES_MTD]   INT           NULL,
    [CASES_TRAITES_YTD]   INT           NULL
);

