﻿CREATE TABLE [dbo].[WK_ALL_SAB_CARTES] (
    [date_alim]          DATE         NULL,
    [DWHCARCOM]          VARCHAR(20) NULL,
    [DWHCARNUS]          INT          NULL,
    [DWHCARCAR]          VARCHAR(6)  NULL,
    [DWHCARCET]          VARCHAR(3)  NULL,
    [type_carte]         CHAR(1)  NULL,
    [DWHCARREM]          DATE         NULL,
    [DWHCARVAL]          DATE         NULL,
    [Validity_StartDate] DATETIME     NULL,
    [Validity_EndDate]   DATETIME     NULL
);
GO
CREATE NONCLUSTERED INDEX IX_NC_WK_ALL_SAB_CARCET
ON dbo.WK_ALL_SAB_CARTES (DWHCARCET ASC)
INCLUDE (date_alim, DWHCARCOM, type_carte, DWHCARNUS)
GO