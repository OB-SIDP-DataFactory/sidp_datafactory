﻿CREATE TABLE [dbo].[CP_STOCK_CASE_REQUEST] (
    [ID_CP_STOCK_CASE_REQUEST] INT            IDENTITY (1, 1) NOT NULL,
    [DTE_ALIM]                 DATE           NULL,
    [IDE_PERS_SF]              NVARCHAR(50)  NOT NULL,
    [CAN_INT_ORI]              NVARCHAR(40)  NULL,
    [LIB_CAN_INT_ORI]          NVARCHAR(255) NULL,
    [IDE_TYP_ENR]              NVARCHAR(18)  NULL,
    [COD_TYP_ENR]              NVARCHAR(255) NULL,
    [LIB_TYP_ENR]              NVARCHAR(255) NULL,
    [TYP_REQ]                  NVARCHAR(40)  NULL,
    [LIB_TYP_REQ]              NVARCHAR(255) NULL,
    [NB_REQ]                   INT            NULL,
    [DTE_CREA]                 DATE           NULL,
    [DTE_FER_REQ]              DATE           NULL,
    [DELAI_TRAITEMENT]         INT            NULL,
    [DELAI_MED_TRT_CANAL]      INT            NULL,
    CONSTRAINT [PK_CP_STOCK_CASE_REQUEST] PRIMARY KEY CLUSTERED ([ID_CP_STOCK_CASE_REQUEST] ASC)
);

