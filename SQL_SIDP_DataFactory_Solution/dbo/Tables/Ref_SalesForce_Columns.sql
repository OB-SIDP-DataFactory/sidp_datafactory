﻿CREATE TABLE [dbo].[Ref_SalesForce_Columns]
(
	[Id]				INT				IDENTITY (1, 1)						NOT NULL,
	[TableId]			INT													NOT NULL,
	[ColumnName]		VARCHAR(100)										NOT NULL,
	[ColumnDescription] VARCHAR(500)										NULL,
	[ColumnDataType]	VARCHAR(50)											NOT NULL,
	[IsEnabled]			BIT				DEFAULT (1)							NOT NULL,
	[ValidityStartDate] DATETIME2(7)										NOT NULL,
	[ValidityEndDate]	DATETIME2(7)	DEFAULT ('9999-12-31 23:59:59.999')	NOT NULL
)
GO
CREATE NONCLUSTERED INDEX [IX_NC_REF_SF_COL]
ON [dbo].[Ref_SalesForce_Columns] ([TableId],[ColumnName])
GO