﻿CREATE TABLE [dbo].[MKT_FACTURATION_INACT] (
    [PK_ID]           INT             IDENTITY (1, 1) NOT NULL,
    [date_alim]       DATE            NOT NULL,
    [NUM_CPT]         VARCHAR(20)    NOT NULL,
    [cum_mtt_facture] DECIMAL (16, 2) NULL,
    CONSTRAINT [PK_MKT_FACTURATION_INACT] PRIMARY KEY CLUSTERED ([date_alim] ASC, [NUM_CPT] ASC)
);


