﻿CREATE TABLE [dbo].[Ref_SalesForce_Environments]
(
	[Id]				INT				IDENTITY (1, 1)						NOT NULL,
	[SourceSystem]		VARCHAR(20)											NOT NULL,
	[EnvironmentName]	VARCHAR(50)											NOT NULL,
	[ConnectionString]	VARCHAR(1000)										NOT NULL,
	[IsEnabled]			BIT				DEFAULT (1)							NOT NULL,
	[ValidityStartDate] DATETIME2(7)										NOT NULL,
	[ValidityEndDate]	DATETIME2(7)	DEFAULT ('9999-12-31 23:59:59.999')	NOT NULL
)
GO