﻿CREATE TABLE [dbo].[sas_vue_flux_opport_fin] (
    [CloseDate]              DATETIME      NULL,
    [jour_alim]              DATE          NULL,
    [Id_SF]                  NVARCHAR(50) NOT NULL,
    [AccountId]              NVARCHAR(18) NULL,
    [stade_vente]            VARCHAR(50)  NULL,
    [motif_sans_suite]       VARCHAR(50)  NOT NULL,
    [date_creat_opp]         DATE          NULL,
    [date_fin_opp]           DATETIME      NULL,
    [age_opport]             INT           NULL,
    [flag_full_digi]         INT           NOT NULL,
    [flag_full_btq]          INT           NOT NULL,
    [flag_indication]        VARCHAR(3)   NOT NULL,
    [canal_interact_origine] VARCHAR(50)  NOT NULL,
    [canal_distrib_origine]  VARCHAR(50)  NOT NULL,
    [sous_reseau_origine]    VARCHAR(50)  NOT NULL,
    [reseau_origine]         VARCHAR(50)  NOT NULL,
    [canal_interact_final]   VARCHAR(50)  NOT NULL,
    [canal_distrib_final]    VARCHAR(50)  NOT NULL,
    [sous_reseau_final]      VARCHAR(50)  NOT NULL,
    [reseau_final]           VARCHAR(50)  NOT NULL,
    [stade_vente_prec]       VARCHAR(50)  NULL,
    [flag_sidp]              INT           NOT NULL,
    CONSTRAINT [PK_sas_vue_flux_opport_fin] PRIMARY KEY CLUSTERED ([Id_SF] ASC)
);
GO
CREATE NONCLUSTERED INDEX IX_NC_SAS_V_FLX_OPP_FIN_ALM
ON dbo.sas_vue_flux_opport_fin ([jour_alim])
INCLUDE ([stade_vente], [date_fin_opp], [age_opport], [flag_full_digi], [flag_full_btq], [canal_distrib_final])
GO