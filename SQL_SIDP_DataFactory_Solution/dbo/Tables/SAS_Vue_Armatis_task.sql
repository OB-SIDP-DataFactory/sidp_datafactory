﻿CREATE TABLE [dbo].[SAS_Vue_Armatis_task] (
    [Id]              BIGINT          IDENTITY (1, 1) NOT NULL,
    [FILE_ATTENTE]    NVARCHAR(255)  NULL,
    [NIV_SAT]         NVARCHAR(1300) NULL,
    [DTE_CREA]        DATETIME        NULL,
    [DTE_INTERACTION] DATETIME   NULL,
    [COD_TYP_ENR]     NVARCHAR(255)  NULL,
    [TYP_ENR_TASK]    NVARCHAR(255)  NULL,
    [OBJ_TASK]        NVARCHAR(255)  NULL,
    [SOUS_TYP_TASK]   NVARCHAR(40)   NULL,
    [TYP_APPEL]       NVARCHAR(50)   NULL,
    [IDE_USER_SF]     NVARCHAR(18)   NULL,
    [Lastname]           NVARCHAR(80)  NULL,
    [Firstname]          NVARCHAR(40)  NULL,
    [Nb_Task]         INT             NULL
);

