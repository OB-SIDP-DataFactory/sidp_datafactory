﻿CREATE TABLE [dbo].[WK_ALL_SAB_COMPTES] (
    [date_alim]          DATE            NULL,
    [DWHCPTCOM]          VARCHAR(20)    NULL,
    [DWHCPTDAC]          DATE            NULL,
    [DWHCPTDAO]          DATE            NULL,
    [DWHCPTCLO]          VARCHAR(6)     NULL,
    [DWHCPTCPT]          DECIMAL (18, 3) NULL,
    [DWHCPTRUB]          VARCHAR(10)    NULL,
    [DWHCPTMTD]          DECIMAL (18, 3) NULL,
    [DWHCPTPPAL]         VARCHAR(7)     NULL,
    [Validity_StartDate] DATETIME        NULL,
    [Validity_EndDate]   DATETIME        NULL
);


