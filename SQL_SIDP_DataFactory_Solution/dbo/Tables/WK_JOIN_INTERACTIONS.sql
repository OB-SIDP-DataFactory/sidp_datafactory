﻿CREATE TABLE [dbo].[WK_JOIN_INTERACTIONS] (
    [PK_ID]                 NVARCHAR(18) NOT NULL,
    [INTERACTION_TYPE]      NVARCHAR(55) NULL,
    [INTERACTION_SOUS_TYPE] NVARCHAR(55) NULL,
    [CASE_ID]               NVARCHAR(50) NULL,
    [ACCNT_ID]              NVARCHAR(18) NULL,
    [DATE_ID]               DATETIME2 (6) NULL,
    [OWNER_ID]              NVARCHAR(18) NULL,
    [LASTMODIFIEDDATE]      DATETIME      NULL,
    [DMC]                   INT           NULL
);

