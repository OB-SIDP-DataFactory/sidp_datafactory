﻿CREATE TABLE [dbo].[wk_join_dim_cli] (
    [date_obs_stock_cli]    DATETIME       NULL,
    [jour_alim]             DATE           NULL,
    [nb_clients]            INT            NULL,
    [AGE_CLI]               INT            NULL,
    [AGE_REL_CLI]           INT            NULL,
    [date_creation_cli]     DATE           NULL,
    [RelationEntryScore__c] NVARCHAR(255) NULL,
    [Preattribution__c]     NVARCHAR(20)  NULL,
    [id_dim_csp_cli]        INT            NULL,
    [id_dim_temps]          INT            NULL,
    [id_dim_canal]          INT            NULL
);


