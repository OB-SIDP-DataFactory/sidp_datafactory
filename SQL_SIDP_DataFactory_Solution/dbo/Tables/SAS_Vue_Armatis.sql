﻿CREATE TABLE [dbo].[SAS_Vue_Armatis] (
    [Id]                 BIGINT         IDENTITY (1, 1) NOT NULL,
    [Date_ouverture]     DATETIME       NULL,
    [DTE_FER_REQ]        DATETIME       NULL,
    [DTE_DER_MOD]        DATETIME       NULL,
    [LIB_STATUT]         NVARCHAR(255) NULL,
    [LIB_STAT_APPEL]     NVARCHAR(255) NULL,
    [JAL_DEP]            NVARCHAR(10)  NULL,
    [IDE_USER_SF]        NVARCHAR(18)  NULL,
    [PROP_PREC]          NVARCHAR(255) NULL,
    [LIB_CAN_INT_ORI]    NVARCHAR(255) NULL,
    [TYP_ENR_CASE]       NVARCHAR(255) NULL,
    [LIB_SS_TYP]         NVARCHAR(255) NULL,
    [FILE_ATTENTE]       NVARCHAR(255) NULL,
    [LIB_DET_STAT_APPEL] NVARCHAR(255) NULL,
    [LIB_TYP_REQ]        NVARCHAR(255) NULL,
    [LIB_STADE_VENTE]    NVARCHAR(255) NULL,
	[Lastname]           NVARCHAR(80)  NULL,
    [Firstname]          NVARCHAR(40)  NULL,
    [Nb_Req]             INT           NULL
);

