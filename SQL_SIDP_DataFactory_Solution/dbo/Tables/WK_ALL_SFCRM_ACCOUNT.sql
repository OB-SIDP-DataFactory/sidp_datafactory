﻿CREATE TABLE [dbo].[WK_ALL_SFCRM_ACCOUNT] (
    [date_alim]             DATE           NULL,
    [Id_SF]                 NVARCHAR(50)  NULL,
    [IDCustomerSAB__pc]     NVARCHAR(15)  NULL,
    [PersonBirthdate]       DATETIME       NULL,
    [OBFirstContactDate__c] DATETIME       NULL,
    [OBTerminationDate__c]  DATETIME       NULL,
    [Occupation__pc]        NVARCHAR(255) NULL,
    [OccupationNiv1__pc]    NVARCHAR(255) NULL,
    [OccupationNiv3__pc]    NVARCHAR(255) NULL,
    [OptInDataTelco__c]     NVARCHAR(10)  NULL,
    [OptInOrderTelco__c]    NVARCHAR(10)  NULL,
    [Preattribution__c]     NVARCHAR(20)  NULL,
    [RecordTypeId]          NVARCHAR(18)  NULL,
    [Validity_StartDate]    DATETIME2 (7)  NULL,
    [Validity_EndDate]      DATETIME2 (7)  NULL
);


