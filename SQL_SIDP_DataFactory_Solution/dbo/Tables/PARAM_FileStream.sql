﻿CREATE TABLE [dbo].[PARAM_FileStream] (
    [ID]                          INT            IDENTITY (1, 1) NOT NULL,
    [Nbr_Mois]                    INT            NOT NULL,
    [Dossier_Source]              NVARCHAR(50)  NULL,
    [CNX_FilestreamShare_Archive] NVARCHAR(255) NULL,
    [Commentaire]                 NVARCHAR(255) NULL
);

