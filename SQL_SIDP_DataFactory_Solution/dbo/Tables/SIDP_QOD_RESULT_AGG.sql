﻿CREATE TABLE [dbo].[SIDP_QOD_RESULT_AGG] (
    [RUN_ID]           INT            NULL,
    [DATE_DEB]         DATETIME       NULL,
    [DATE_FIN]         DATETIME       NULL,
    [DUREE_EXEC]       INT            NULL,
    [ID_CONTROL]       INT            NULL,
    [NB_LIGNES_RESULT] INT            NULL,
    [message]          NVARCHAR(MAX) NULL,
    [traite]           VARCHAR(200)  NULL
)
GO
CREATE NONCLUSTERED INDEX [IX_NC_QOD_RES_AGG_RUN]
ON [dbo].[SIDP_QOD_RESULT_AGG] ([RUN_ID],[DATE_DEB])
INCLUDE ([NB_LIGNES_RESULT])
GO