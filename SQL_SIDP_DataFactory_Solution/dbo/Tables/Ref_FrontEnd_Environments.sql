﻿CREATE TABLE [dbo].[Ref_FrontEnd_Environments](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SourceSystem] [varchar](20) NOT NULL,
	[EnvironmentName] [varchar](50) NOT NULL,
	[ConnectionString] [varchar](1000) NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[ValidityStartDate] [datetime2](7) NOT NULL,
	[ValidityEndDate] [datetime2](7) NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Ref_FrontEnd_Environments] ADD  DEFAULT ((1)) FOR [IsEnabled]
GO

ALTER TABLE [dbo].[Ref_FrontEnd_Environments] ADD  DEFAULT ('9999-12-31 23:59:59.999') FOR [ValidityEndDate]
GO
