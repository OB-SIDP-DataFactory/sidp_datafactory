﻿CREATE TABLE [dbo].[T_FACT_MKT_NB_MTT_OP_LAST_THREE_MONTHS] (
    [DTE_EOM]         DATE            NULL,
    [MOIS_TRAITEMENT] VARCHAR(20)    NULL,
    [NUM_CPT_SAB]     VARCHAR(20)    NULL,
    [NB_OP_DEB]       INT             NULL,
    [NB_OP_CRE]       INT             NULL,
    [MTT_OP_DEB]      DECIMAL (18, 2) NULL,
    [MTT_OP_CRE]      DECIMAL (18, 2) NULL
);

