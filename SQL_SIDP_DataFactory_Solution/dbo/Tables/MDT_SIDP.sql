﻿CREATE TABLE [dbo].[MDT_SIDP] (
    [RELEASE]     NVARCHAR(10)  NULL,
    [NAME_SCHEMA] NVARCHAR(10)  NULL,
    [NAME_TABLE]  NVARCHAR(100) NULL,
    [ID_COLUMN]   INT            NULL,
    [NAME_COLUMN] NVARCHAR(50)  NULL,
    [DATA_TYPE]   NVARCHAR(20)  NULL,
    [MAX_LENGTH]  INT            NULL,
    [PRECISION]   INT            NULL,
    [SCALE]       INT            NULL,
    [NULLABLE]    BIT            NULL,
    [FORMAT]      NVARCHAR(20)  NULL
);


