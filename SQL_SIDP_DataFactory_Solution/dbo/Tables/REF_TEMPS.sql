﻿CREATE TABLE dbo.REF_TEMPS (
    PK_ID			INT          NOT NULL,
    [Date]			DATETIME     NOT NULL,
    [Day]			CHAR(2)     NOT NULL,
    DaySuffix		VARCHAR(4)  NOT NULL,
    [DayOfWeek]		VARCHAR(9)  NOT NULL,
    DOWInMonth		TINYINT      NOT NULL,
    [DayOfYear]		INT          NOT NULL,
    WeekOfYear		TINYINT      NOT NULL,
    WeekOfMonth		TINYINT      NOT NULL,
    IsoWeek			TINYINT      NOT NULL,
    IsoWeekYear		CHAR(4)     NOT NULL,
    [Month]			CHAR(2)     NOT NULL,
    [MonthName]		VARCHAR(9)  NOT NULL,
    MonthYear		VARCHAR(50) NULL,
    [Quarter]		TINYINT      NOT NULL,
    QuarterName		VARCHAR(6)  NOT NULL,
    [Year]			CHAR(4)     NOT NULL,
    BusinessDay		TINYINT      NOT NULL,
    StandardDate	DATE         NULL
)
GO
CREATE NONCLUSTERED INDEX [IX_NC_REF_TMP_STD_DTE]
ON dbo.REF_TEMPS([StandardDate])
GO
CREATE NONCLUSTERED INDEX [IX_NC_REF_TMP_DTE_BUS]
ON dbo.REF_TEMPS([BusinessDay], [Date])
GO
CREATE NONCLUSTERED INDEX [IX_NC_REF_TMP_BUS_DAY]
ON [dbo].[REF_TEMPS] ([BusinessDay])
INCLUDE ([Date],[Month])
GO
