﻿CREATE TABLE [dbo].[REF_BOUTIQUE](
	[ID_REF_BOUTIQUE] [bigint] IDENTITY(1,1) NOT NULL,
	[COD_BOUT_ADV_ORNG] [nvarchar](50) NOT NULL,
	[TYP_BOUT] [nvarchar](100) NULL,
	[LIBL_SIT] [nvarchar](100) NULL,
	[DO] [nvarchar](255) NULL,
	[AD] [nvarchar](255) NULL,
	[ADRS_SIT] [nvarchar](255) NULL,
	[COD_POST_SIT] [nvarchar](5) NULL,
	[VILL_SIT] [nvarchar](40) NULL,
	[LIBL_TYP_SIT] [nvarchar](100) NULL,
	[DAT_CRTN_ENRG] [datetime] NOT NULL,
	[DAT_DERN_MODF_ENRG] [datetime] NOT NULL,
	[DAT_DEBT_VALD] [date] NOT NULL,
	[DAT_FIN_VALD] [date] NOT NULL,
	[FLG_ACTIF] [bit] NOT NULL
); 
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifiant technique' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'REF_BOUTIQUE', @level2type=N'COLUMN',@level2name=N'ID_REF_BOUTIQUE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Boutique ADV Orange' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'REF_BOUTIQUE', @level2type=N'COLUMN',@level2name=N'COD_BOUT_ADV_ORNG'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type Boutique' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'REF_BOUTIQUE', @level2type=N'COLUMN',@level2name=N'TYP_BOUT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé Site ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'REF_BOUTIQUE', @level2type=N'COLUMN',@level2name=N'LIBL_SIT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'REF_BOUTIQUE', @level2type=N'COLUMN',@level2name=N'DO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'AD' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'REF_BOUTIQUE', @level2type=N'COLUMN',@level2name=N'AD'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Adresse Site' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'REF_BOUTIQUE', @level2type=N'COLUMN',@level2name=N'ADRS_SIT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code Postal Site' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'REF_BOUTIQUE', @level2type=N'COLUMN',@level2name=N'COD_POST_SIT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ville Site' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'REF_BOUTIQUE', @level2type=N'COLUMN',@level2name=N'VILL_SIT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Libellé Type Site' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'REF_BOUTIQUE', @level2type=N'COLUMN',@level2name=N'LIBL_TYP_SIT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date de création de l''enregistrement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'REF_BOUTIQUE', @level2type=N'COLUMN',@level2name=N'DAT_CRTN_ENRG'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date de dernière modification de l''enregistrement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'REF_BOUTIQUE', @level2type=N'COLUMN',@level2name=N'DAT_DERN_MODF_ENRG'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date de début de validité' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'REF_BOUTIQUE', @level2type=N'COLUMN',@level2name=N'DAT_DEBT_VALD'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date de fin de validité' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'REF_BOUTIQUE', @level2type=N'COLUMN',@level2name=N'DAT_FIN_VALD'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag Actif' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'REF_BOUTIQUE', @level2type=N'COLUMN',@level2name=N'FLG_ACTIF'
GO


