﻿CREATE TABLE [dbo].[PARAM_EXTRACT_SIDP]
(
    [ExtractId]   BIGINT        IDENTITY (1, 1) NOT NULL,
    [ExtractName] NVARCHAR(50)  NOT NULL,
    [ObjectNameOrQuery]  VARCHAR(MAX)  NOT NULL,
    [DestinationPath] VARCHAR(MAX) NULL,
    [Timestamped] BIT NOT NULL DEFAULT 0, 
    [Separator] NCHAR(1) NOT NULL DEFAULT N';', 
    [Quoted] BIT NOT NULL DEFAULT 1, 
    [QuoteCharacter] NCHAR(1) NOT NULL DEFAULT N'″', 
    [Enabled]     BIT       DEFAULT 1 NOT NULL,
    [ProcessingFrequency] NCHAR(1) NOT NULL DEFAULT N'D', 
    [LastProcessingDateTime] DATETIME2 NULL, 
    CONSTRAINT [PK_PARAM_EXTRACT_SIDP] PRIMARY KEY CLUSTERED ([ExtractId] ASC)
) 
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Processing Frequency :H (Hourly); D (Daily); W (Weekly); M (Monthly); Y (Yearly)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PARAM_EXTRACT_SIDP', @level2type = N'COLUMN', @level2name = N'ProcessingFrequency';
GO