﻿CREATE TABLE [dbo].[ssrs_vue_equipement] (
    [date_souscription] DATE           NULL,
    [code_fe_assurance] INT            NOT NULL,
    [type_assurance]    NVARCHAR(255) NULL,
    [nb_assurances]     INT            NULL
);

