﻿CREATE TABLE [dbo].[CRE_CLIENT]
(
    [ID_CRE_CLIENT]    INT            IDENTITY(1, 1) NOT NULL,
    [DTE_ALIM]         DATE           NULL,
    [DTE_ANA]          DATE           NULL,
    [COD_ETA]          INT            NULL,
    [NUM_CLT_SAB]      VARCHAR(7)     NULL,
    [COD_AGEN]         INT            NULL,
    [COD_ETAT]         VARCHAR(4)     NULL,
    [NOM_CLT]          VARCHAR(32)    NULL,
    [PRENOM_CLT]       VARCHAR(32)    NULL,
    [SIG_CLT]          VARCHAR(12)    NULL,
    [COD_SIREN_CLT]    VARCHAR(9)     NULL,
    [COD_SIRET_CLT]    INT            NULL,
    [DTE_CREA_CLT]     DATE           NULL,
    [DTE_CREA_ENT]     DATE           NULL,
    [DTE_NAIS_CLT]     DATE           NULL,
    [COD_PAY_NAT]      VARCHAR(3)     NULL,
    [COD_CLT_RES]      CHAR(1)        NULL,
    [COD_PAY_RES]      VARCHAR(3)     NULL,
    [COD_RES]          VARCHAR(3)     NULL,
    [COD_QLT_CLT]      VARCHAR(3)     NULL,
    [SEC_ACT]          VARCHAR(6)     NULL,
    [COD_PCS_CLT]      VARCHAR(4)     NULL,
    [SEC_ACT_REG]      VARCHAR(6)     NULL,
    [CAT_CLT]          VARCHAR(3)     NULL,
    [SEC_GEO]          VARCHAR(3)     NULL,
    [ENT_LIE]          VARCHAR(3)     NULL,
    [COT_ACT]          CHAR(1)        NULL,
    [COT_CRE]          VARCHAR(3)     NULL,
    [COT_INTERNE]      VARCHAR(3)     NULL,
    [COT_DIR]          CHAR(1)        NULL,
    [COD_PROF]         VARCHAR(3)     NULL,
    [NBE_ENF]          INT            NULL,
    [COD_REG_MAT]      CHAR(2)        NULL,
    [EFF_ENT]          INT            NULL,
    [CA_ENT]           DECIMAL(18, 3) NULL,
    [AA_CA_ENT]        DATE           NULL,
    [CAPA_JURI]        CHAR(2)        NULL,
    [MTT_REV]          DECIMAL(18, 3) NULL,
    [MTT_CHARGE]       DECIMAL(18, 3) NULL,
    [MTT_PAT]          DECIMAL(18, 3) NULL,
    [COD_SEG_RES]      VARCHAR(3)     NULL,
    [COD_SEG_POT]      VARCHAR(3)     NULL,
    [COD_DOU_REG]      CHAR(1)        NULL,
    [IND_DOU_CLT]      CHAR(2)        NULL,
    [IND_INT_CHQ]      CHAR(1)        NULL,
    [TYP_INT]          CHAR(2)        NULL,
    [DTE_CLO_CLT]      DATE           NULL,
    [FLG_DECES_CLT]    CHAR(1)        NULL,
    [PAY_NAIS]         VARCHAR(3)     NULL,
    [SIT_FAM]          CHAR(2)        NULL,
    [TIERS_COLL]       CHAR(1)        NULL,
    [CLT_GRP_RISQ]     VARCHAR(7)     NULL,
    [TETE_GRP_RISQ]    VARCHAR(7)     NULL,
    [DTE_DEB_CON]      DATE           NULL,
    [MOTIF_CON]        VARCHAR(6)     NULL,
    [PROC_CLT]         VARCHAR(6)     NULL,
    [COT_IEDOM]        CHAR(2)        NULL,
    [AUT_PROD]         CHAR(1)        NULL,
    [COD_VAL01]        VARCHAR(6)     NULL,
    [COD_VAL02]        VARCHAR(6)     NULL,
    [COD_VAL03]        VARCHAR(6)     NULL,
    [COD_VAL04]        VARCHAR(6)     NULL,
    [COD_VAL05]        VARCHAR(6)     NULL,
    [COD_VAL06]        VARCHAR(6)     NULL,
    [COD_VAL07]        VARCHAR(6)     NULL,
    [COD_VAL08]        VARCHAR(6)     NULL,
    [COD_VAL09]        VARCHAR(6)     NULL,
    [COD_VAL10]        VARCHAR(6)     NULL,
    [COD_VAL11]        VARCHAR(6)     NULL,
    [COD_VAL12]        VARCHAR(6)     NULL,
    [ZON_LANG01]       CHAR(1)        NULL,
    [ZON_LANG02]       CHAR(1)        NULL,
    [ZON_RUB01]        VARCHAR(20)    NULL,
    [ZON_RUB02]        VARCHAR(15)    NULL,
    [DTE_CLT01]        DATE           NULL,
    [DTE_CLT02]        DATE           NULL,
    [MTT_CLT01]        DECIMAL(18, 3) NULL,
    [AA_DER_BILAN]     INT            NULL,
    [MM_DER_BILAN]     INT            NULL,
    [DTE_ATT]          DATE           NULL,
    [COD_BU_LINE]      VARCHAR(6)     NULL,
    [COD_STAT_CLT]     CHAR(1)        NULL,
    [NUM_CLT_REF]      VARCHAR(7)     NULL,
    [DTE_INIT_CLT]     DATE           NULL,
    [DTE_ACC_CLT]      DATE           NULL,
    [COD_DGI]          CHAR(1)        NULL,
    [FORM_JURI]        VARCHAR(4)     NULL,
    [NOM_CLT_JF]       VARCHAR(32)    NULL,
    [NUM_VOIE_ADR]     VARCHAR(38)    NULL,
    [COD_POS_CRI]      VARCHAR(12)    NULL,
    [VILLE_ADR]        VARCHAR(32)    NULL,
    [COD_BDF]          CHAR(2)        NULL,
    [COD_DPT_NAIS]     CHAR(2)        NULL,
    [VILLE_NAIS]       VARCHAR(32)    NULL,
    [COD_PAY_NAIS_ISO] CHAR(2)        NULL,
    [IDE_NAT_TP]       VARCHAR(3)     NULL,
    [IDE_NAT_NU]       VARCHAR(17)    NULL,
    [COD_PAY_RES_ISO]  CHAR(2)        NULL,
    [COD_PAY_NAT_ISO]  CHAR(2)        NULL,
    [PERIMETRE_RISQ]   CHAR(1)        NULL,
    [FREQ]             CHAR(1)        NULL,
    CONSTRAINT [PK_CRE_CLIENT] PRIMARY KEY CLUSTERED ([ID_CRE_CLIENT] ASC)
)
GO