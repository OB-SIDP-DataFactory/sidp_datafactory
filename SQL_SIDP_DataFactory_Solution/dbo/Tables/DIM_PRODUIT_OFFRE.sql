﻿CREATE TABLE [dbo].[DIM_PRODUIT_OFFRE] (
    [PK_ID]              INT           IDENTITY (1, 1) NOT NULL,
    [CODE_FE]            NVARCHAR(50) NULL,
    [LIBELLE]            NVARCHAR(50) NULL,
    [PARENT_ID]          NVARCHAR(50) NULL,
    [Validity_StartDate] DATETIME      CONSTRAINT [DF_DIM_PRODUIT_OFFRE_Validity_StartDate] DEFAULT ('2017-01-01') NULL,
    [Validity_EndDate]   DATETIME      CONSTRAINT [DF_DIM_PRODUIT_OFFRE_Validity_EndDate] DEFAULT ('9999-12-31') NULL,
    CONSTRAINT [PK_DIM_PRODUIT_OFFRE] PRIMARY KEY CLUSTERED ([PK_ID] ASC)
);
GO
CREATE NONCLUSTERED INDEX IX_NC_DIM_PRODUIT_OFFRE_PID
ON dbo.DIM_PRODUIT_OFFRE (PARENT_ID)
INCLUDE (CODE_FE, LIBELLE)
GO