﻿CREATE TABLE [dbo].[ECKERT_SAB] (
    [id]              INT          IDENTITY (1, 1) NOT NULL,
    [id_client_sab]   VARCHAR(50) NULL,
    [date_chargement] DATETIME     NULL,
    [flag_traiter]    BIT          NULL
);

