﻿CREATE TABLE [dbo].[VDC_TYPE_OPERATION] (
    [Date_operation]      NVARCHAR(6)    NULL,
    [Type_Op1]            NVARCHAR(10)   NULL,
    [Type_Op2]            NVARCHAR(10)   NULL,
    [Nb_Ope]              INT             NULL,
    [Nb_Ope1]             INT             NULL,
    [Nb_Ope2]             INT             NULL,
    [Nb_Ope3]             INT             NULL,
    [Nb_Ope4]             INT             NULL,
    [Nb_Ope5]             INT             NULL,
    [Nb_Ope6]             INT             NULL,
    [Nb_Ope_France]       INT             NULL,
    [Nb_Ope_ZoneEuro]     INT             NULL,
    [Nb_Ope_HorsZoneEuro] INT             NULL,
    [Montant_en_k€]       DECIMAL (10, 2) NULL,
    [Nb_moyen_Ope]        DECIMAL (10, 2) NULL,
    [Montant_moyen_Ope]   DECIMAL (10, 2) NULL,
    [Nb_médian_Ope]       INT             NULL,
    [Montant_médian_Ope]  DECIMAL (10, 2) NULL
);

