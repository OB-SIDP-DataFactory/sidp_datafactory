﻿CREATE TABLE [dbo].[ECKERT_PRV_VIE] (
    [NUM_CLIENT]        INT           NULL,
    [ID_DEMANDE]        VARCHAR(30)  NULL,
    [DESCRIPTION]       VARCHAR(500) NULL,
    [Code_ORIGIN_DEM]   VARCHAR(10)  NULL,
    [ORIGIN_DEM]        VARCHAR(150) NULL,
    [DATE_MVT]          VARCHAR(10)          NULL,
    [DATE_TRAIT]        DATETIME      NULL,
    [ID_TECH]           INT           IDENTITY (1, 1) NOT NULL,
    [DATE_CREATION_DEM] VARCHAR(10)          NULL,
    CONSTRAINT [PK_ID_TECH] PRIMARY KEY CLUSTERED ([ID_TECH] ASC)
);

