﻿CREATE TABLE [dbo].[REF_CONS_LOAN_REJECT_REASON] (
    [ID]                 BIGINT         IDENTITY (1, 1) NOT NULL,
    [REJECT_REASON_ID]   DECIMAL (38)   NOT NULL,
    [FRANFINANCE_CODE]   NVARCHAR(255) NULL,
    [REF_FAMILY_ID]      DECIMAL (19)   NULL,
    [CODE]               NVARCHAR(255) NULL,
    [PRIORITY]           DECIMAL (19)   NULL,
    [REF_LANG_ID]        DECIMAL (19)   NULL,
    [MESSAGE]            NVARCHAR(255) NULL,
    [DAT_CRTN_ENRG]      DATETIME       NOT NULL,
    [DAT_DERN_MODF_ENRG] DATETIME       NOT NULL,
    [DAT_DEBT_VALD]      DATE           NOT NULL,
    [DAT_FIN_VALD]       DATE           NOT NULL,
    [FLG_ENRG_ACTIF]     BIT            NOT NULL
);

