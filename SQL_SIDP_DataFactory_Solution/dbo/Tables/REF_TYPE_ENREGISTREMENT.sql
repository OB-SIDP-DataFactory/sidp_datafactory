﻿CREATE TABLE [dbo].[REF_TYPE_ENREGISTREMENT] (
    [ID_REF_TYPE_ENREGISTREMENT] BIGINT         IDENTITY (1, 1) NOT NULL,
    [TYP_OBJT_SF]                NVARCHAR(255) NULL,
    [IDNT_SF]                    NVARCHAR(18)  NULL,
    [COD_SF]                     NVARCHAR(255) NULL,
    [NOM_SF]                     NVARCHAR(255) NULL,
    [DAT_CRTN_ENRG]              DATETIME       NOT NULL,
    [DAT_DERN_MODF_ENRG]         DATETIME       NOT NULL,
    [DAT_DEBT_VALD]              DATE           NOT NULL,
    [DAT_FIN_VALD]               DATE           NOT NULL,
    [FLG_ACTIF]                  BIT            NOT NULL
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_TYPE_ENREGISTREMENT', @level2type = N'COLUMN', @level2name = N'FLG_ACTIF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de fin de validité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_TYPE_ENREGISTREMENT', @level2type = N'COLUMN', @level2name = N'DAT_FIN_VALD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de début de validité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_TYPE_ENREGISTREMENT', @level2type = N'COLUMN', @level2name = N'DAT_DEBT_VALD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de dernière modification de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_TYPE_ENREGISTREMENT', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de création de l''enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_TYPE_ENREGISTREMENT', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Nom SF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_TYPE_ENREGISTREMENT', @level2type = N'COLUMN', @level2name = N'NOM_SF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code SF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_TYPE_ENREGISTREMENT', @level2type = N'COLUMN', @level2name = N'COD_SF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant SF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_TYPE_ENREGISTREMENT', @level2type = N'COLUMN', @level2name = N'IDNT_SF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type objet SF', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_TYPE_ENREGISTREMENT', @level2type = N'COLUMN', @level2name = N'TYP_OBJT_SF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant technique', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_TYPE_ENREGISTREMENT', @level2type = N'COLUMN', @level2name = N'ID_REF_TYPE_ENREGISTREMENT';

