﻿CREATE TABLE [dbo].[DIM_MOTIF_FERMETURE] (
    [PK_ID]              INT           IDENTITY (1, 1) NOT NULL,
    [MOT_CLO]            VARCHAR(6)   NULL,
    [MOTIF_FERMETURE]    VARCHAR(50)  NULL,
    [MOTIF_FERMETURE2]   VARCHAR(50)  NULL,
    [Validity_StartDate] DATETIME2 (7) NOT NULL,
    [Validity_EndDate]   DATETIME2 (7) DEFAULT ('9999-12-31') NOT NULL,
    CONSTRAINT [PK_DIM_MOTIF_FERMETURE] PRIMARY KEY CLUSTERED ([PK_ID] ASC)
);

