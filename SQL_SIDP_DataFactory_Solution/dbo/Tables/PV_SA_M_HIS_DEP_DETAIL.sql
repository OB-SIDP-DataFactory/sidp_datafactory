﻿CREATE TABLE [dbo].[PV_SA_M_HIS_DEP_DETAIL] (
    [annee]         INT           NULL,
    [mois]          INT           NULL,
    [DWHCPTCOM]     NVARCHAR(20) NULL,
    [DWHCPTPPAL]    NVARCHAR(7)  NULL,
    [DAT_DEBT_VALD] DATE          NULL,
    [DAT_FIN_VALD]  DATE          NULL,
    [nbr_jour]      INT           NULL,
    [DTE_UPDATE]    DATE          DEFAULT (getdate()) NULL
);

