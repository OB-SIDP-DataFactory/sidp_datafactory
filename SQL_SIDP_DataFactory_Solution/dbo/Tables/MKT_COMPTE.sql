﻿CREATE TABLE [dbo].[MKT_COMPTE] (
    [DTE_TRAITEMENT]   DATE            NULL,
    [IDE_OPPRT]        INT             NULL,
    [NUM_CPT_SAB]      VARCHAR(20)    NULL,
    [NUM_CLI_SAB]      VARCHAR(7)     NULL,
    [RUB_CPT_SAB]      VARCHAR(20)    NULL,
    [FLAG_CPT]         VARCHAR(3)     NOT NULL,
    [COD_PROD_COM]     NVARCHAR(255)  NULL,
    [LABEL_PROD_COM]   NVARCHAR(255)  NULL,
    [TYP_CPT_SAB]      VARCHAR(255)   NULL,
    [DTE_OUV]          DATE            NULL,
    [DTE_CLO]          DATE            NULL,
    [MTT_PREM_VER]     DECIMAL (19, 2) NULL,
    [SLD_DTE_CPT]      DECIMAL (18, 3) NULL,
    [MTT_AUT]          DECIMAL (18, 3) NULL,
    [MTT_DEP]          DECIMAL (18, 3) NULL,
    [FLG_DEP_FAC_CAI]  VARCHAR(20)    NULL,
    [FLG_MOB_BAN]      VARCHAR(20)    NULL,
    [ACTIVITE_CPT]     INT             NULL,
    [LIB_ACTIVITE_CPT] VARCHAR(50)    NULL
);


