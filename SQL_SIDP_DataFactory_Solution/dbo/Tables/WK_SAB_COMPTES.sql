﻿CREATE TABLE [dbo].[WK_SAB_COMPTES] (
    [DWHCPTCOM]          VARCHAR(20)    NULL,
    [DWHCPTDAC]          DATE            NULL,
    [DWHCPTDAO]          DATE            NULL,
    [DWHCPTCLO]          VARCHAR(6)     NULL,
    [DWHCPTCPT]          DECIMAL (18, 3) NULL,
    [DWHCPTMTD]          DECIMAL (18, 3) NULL,
    [DWHCPTRUB]          VARCHAR(10)    NULL,
    [DWHCPTPPAL]         VARCHAR(7)     NULL,
    [Validity_StartDate] DATETIME        NULL,
    [Validity_EndDate]   DATETIME        NULL
);


