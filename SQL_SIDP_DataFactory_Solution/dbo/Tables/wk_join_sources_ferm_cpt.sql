﻿CREATE TABLE [dbo].[wk_join_sources_ferm_cpt] (
    [NUM_CPT_CLI]            VARCHAR(20)    NULL,
    [Id_SF_Opp]              NVARCHAR(50)   NULL,
    [Id_SF_Account]          NVARCHAR(50)   NULL,
    [MOT_CLO]                VARCHAR(6)     NULL,
    [DTE_CLO]                DATE            NULL,
    [StartedChannel__c]      NVARCHAR(255)  NULL,
    [DistributionChannel__c] NVARCHAR(255)  NULL,
    [DistributorNetwork__c]  NVARCHAR(80)   NULL,
    [Occupation__pc]         NVARCHAR(255)  NULL,
    [code_fe_type_compte]    NVARCHAR(255)  NULL,
    [code_fe_offre]          NVARCHAR(255)  NULL,
    [AGE_CLI]                INT             NULL,
    [AGE_CPT]                INT             NULL,
    [cum_mtt_facture]        DECIMAL (16, 2) NULL
);


