﻿CREATE TABLE dbo.wk_join_sources_usage_cpt (
    date_alim              DATE            NULL,
    NUM_CPT_CLI            VARCHAR(20)    NULL,
    Id_SF_Opp              NVARCHAR(50)   NULL,
    Id_SF_Account          NVARCHAR(50)   NULL,
    encours_cred           DECIMAL (18, 3) NULL,
    encours_deb            DECIMAL (18, 3) NULL,
    encours_deb_non_auth   DECIMAL (18, 3) NULL,
    code_fe_type_compte    NVARCHAR(255)  NULL,
    StartedChannel__c      NVARCHAR(255)  NULL,
    DistributionChannel__c NVARCHAR(255)  NULL,
    DistributorNetwork__c  NVARCHAR(80)   NULL,
    Occupation__pc         NVARCHAR(255)  NULL,
    OccupationNiv1__pc     NVARCHAR(255)  NULL,
    OccupationNiv3__pc     NVARCHAR(255)  NULL,
    AGE_CLI                INT             NULL,
    nb_cb                  INT             NULL,
    nb_pm                  INT             NULL,
    nb_ope_cb              INT             NULL,
    nb_ope_pm              INT             NULL,
    nb_ope_prel            INT             NULL,
    nb_ope_vir             INT             NULL,
    montant_dep            DECIMAL (38, 2) NULL,
    montant_ret            DECIMAL (38, 2) NULL,
    code_sab_type_compte   VARCHAR(10)    NULL
);
GO
CREATE NONCLUSTERED INDEX IX_NC_wk_JOIN_SRC_DTE_ALIM
ON dbo.wk_join_sources_usage_cpt (date_alim ASC)
GO