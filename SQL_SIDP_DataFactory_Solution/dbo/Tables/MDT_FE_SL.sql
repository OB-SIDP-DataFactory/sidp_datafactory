﻿CREATE TABLE [dbo].[MDT_FE_SL] (
    [RELEASE]        NVARCHAR(10)  NULL,
    [NAME_SCHEMA]    NVARCHAR(10)  NULL,
    [NAME_TABLE]     NVARCHAR(100) NULL,
    [ID_COLUMN]      INT            NULL,
    [NAME_COLUMN]    NVARCHAR(50)  NULL,
    [DATA_TYPE]      NVARCHAR(20)  NULL,
    [DATA_PRECISION] INT            NULL,
    [DATA_SCALE]     INT            NULL,
    [CHAR_LENGTH]    INT            NULL,
    [NULLABLE]       NVARCHAR(10)  NULL,
    [FORMAT]         NVARCHAR(20)  NULL
);


