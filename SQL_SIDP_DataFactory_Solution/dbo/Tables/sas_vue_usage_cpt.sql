﻿CREATE TABLE [dbo].[sas_vue_usage_cpt] (
    [date_alim]            DATE            NOT NULL,
    [jour_alim]            DATE            NULL,
    [nb_comptes]           INT             NULL,
    [encours_cred]         DECIMAL (38, 3) NULL,
    [encours_deb]          DECIMAL (38, 3) NULL,
    [encours_deb_non_auth] DECIMAL (38, 3) NULL,
    [montant_dep]          DECIMAL (38, 2) NULL,
    [montant_ret]          DECIMAL (38, 2) NULL,
    [age_cli]              INT             NULL,
    [nb_cb]                INT             NULL,
    [nb_pm]                INT             NULL,
    [nb_ope_cb]            INT             NOT NULL,
    [nb_ope_pm]            INT             NOT NULL,
    [nb_ope_prel]          INT             NOT NULL,
    [nb_ope_vir]           INT             NOT NULL,
    [nb_cb_sum]            INT             NULL,
    [nb_pm_sum]            INT             NULL,
    [nb_ope_cb_sum]        INT             NULL,
    [nb_ope_pm_sum]        INT             NULL,
    [nb_ope_prel_sum]      INT             NULL,
    [nb_ope_vir_sum]       INT             NULL,
    [CSP_CLI]              VARCHAR(50)    NOT NULL,
    [CSP_CLI_NIV1]         NVARCHAR(255)  NULL,
    [CSP_CLI_NIV3]         NVARCHAR(255)  NULL,
    [canal_interaction]    VARCHAR(50)    NOT NULL,
    [reseau]               VARCHAR(50)    NOT NULL,
    [code_fe_type_compte]  NVARCHAR(255)  NOT NULL,
    [type_compte]          NVARCHAR(50)   NOT NULL,
    [code_fe_offre]        NVARCHAR(50)   NULL,
    [offre_commerciale]    NVARCHAR(50)   NOT NULL,
    [code_sab_type_compte] VARCHAR(10)    NOT NULL
)
GO
CREATE NONCLUSTERED INDEX IX_NC_sas_vue_uscpt_jalim_ENC
ON dbo.sas_vue_usage_cpt (jour_alim)
INCLUDE (encours_cred, encours_deb, encours_deb_non_auth, montant_dep, montant_ret, code_sab_type_compte)
GO