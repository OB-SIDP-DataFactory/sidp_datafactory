﻿CREATE TABLE [dbo].[sas_vue_synthese_prod] (
    [jour_alim]            DATETIME      NULL,
    [code_sab_type_compte] NVARCHAR(10) NULL,
    [ouvert_jour]          FLOAT (53)    NULL,
    [ferm_jour]            FLOAT (53)    NULL,
    [ouvert_cumul]         FLOAT (53)    NULL,
    [ferm_cumul]           FLOAT (53)    NULL,
    [stock_calcul]         FLOAT (53)    NULL
);


