﻿CREATE TABLE [dbo].[SAS_Vue_RC_Interactions] (
    [PK_ID]                 NVARCHAR(18)  NULL,
    [interaction_type]      NVARCHAR(255) NULL,
    [interaction_sous_type] NVARCHAR(255) NULL,
    [FK_TEMPS]              INT            NULL,
    [MONTHYEAR]             VARCHAR(50)   NULL,
    [WeekOfYear]            TINYINT        NULL,
    [StandardDate]          DATE           NULL,
    [FK_USER_OWNER]         INT            NULL,
    [CODE_GENESYS_USER]     VARCHAR(50)   NULL,
    [PROFIL_USER]           VARCHAR(50)   NULL,
    [LOGIN_USER]            VARCHAR(101)  NOT NULL,
    [CASE_ID]               NVARCHAR(50)  NULL,
    [FK_RECORDTYPE]         INT            NULL,
    [RECORDTYPE_LABEL]      NVARCHAR(255) NULL,
    [FK_TYPE_CASE]          INT            NULL,
    [TYPE_CASE_LABEL]       NVARCHAR(255) NULL,
    [FK_SUBTYPE_CASE]       INT            NULL,
    [SUBTYPE_CASE_LABEL]    NVARCHAR(255) NULL,
    [FK_STATUS_CASE]        INT            NULL,
    [STATUS_CASE_LABEL]     NVARCHAR(255) NULL,
    [FK_ACCNT]              NVARCHAR(50)  NULL,
    [DMC]                   INT            NULL
);
GO
CREATE NONCLUSTERED INDEX IX_NC_SAS_V_INT_DTE
ON [dbo].[SAS_Vue_RC_Interactions] ([StandardDate])
INCLUDE ([interaction_type],[interaction_sous_type])
GO