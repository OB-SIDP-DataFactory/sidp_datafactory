﻿CREATE TABLE [dbo].[sas_vue_stock_opport] (
    [date_alim]              DATE          NULL,
    [jour_alim]              DATE          NULL,
    [nb_opport]              INT           NULL,
    [stade_vente]            VARCHAR(50)  NULL,
    [motif_sans_suite]       VARCHAR(50)  NULL,
    [date_creat_opp]         DATE          NULL,
    [date_fin_opp]           DATETIME      NULL,
    [age_opport]             INT           NULL,
    [flag_full_digi]         INT           NOT NULL,
    [flag_full_btq]          INT           NOT NULL,
    [flag_indication]        VARCHAR(3)   NOT NULL,
    [flag_identif_OF]        NVARCHAR(10) NOT NULL,
    [flag_facture_OF]        NVARCHAR(10) NOT NULL,
    [canal_interact_origine] VARCHAR(50)  NOT NULL,
    [canal_distrib_origine]  VARCHAR(50)  NOT NULL,
    [sous_reseau_origine]    VARCHAR(50)  NOT NULL,
    [reseau_origine]         VARCHAR(50)  NOT NULL,
    [canal_interact_final]   VARCHAR(50)  NOT NULL,
    [canal_distrib_final]    VARCHAR(50)  NOT NULL,
    [sous_reseau_final]      VARCHAR(50)  NOT NULL,
    [reseau_final]           VARCHAR(50)  NOT NULL,
    [flag_sidp]              INT           NULL,
    [commercial_offer_code]  NVARCHAR(80) NULL,
    [commercial_offer_name]  NVARCHAR(80) NULL,
    [semaine_creation]       VARCHAR(50)  NULL
);
GO
CREATE NONCLUSTERED INDEX IX_NC_SAS_V_STO_OPP_DTE_ALIM
ON dbo.sas_vue_stock_opport (date_alim)
GO
CREATE NONCLUSTERED INDEX IX_NC_SAS_V_STO_OPP_DTE_ALIM_CRE_NB_OPP
ON dbo.sas_vue_stock_opport (date_alim, date_creat_opp)
INCLUDE (nb_opport, stade_vente, flag_indication, canal_distrib_origine, commercial_offer_code)
GO