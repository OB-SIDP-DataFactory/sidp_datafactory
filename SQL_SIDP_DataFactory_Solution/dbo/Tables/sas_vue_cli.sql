﻿CREATE TABLE [dbo].[sas_vue_cli] (
    [date_obs_stock_cli]    DATETIME       NULL,
    [jour_alim]             DATE           NULL,
    [nb_clients]            INT            NULL,
    [nb_clients_ouvert]     INT            NULL,
    [nb_clients_ferm]       INT            NULL,
    [AGE_CLI]               INT            NULL,
    [AGE_REL_CLI]           INT            NULL,
    [RelationEntryScore__c] NVARCHAR(255) NULL,
    [Preattribution__c]     INT            NULL,
    [csp_cli]               VARCHAR(50)   NULL,
    [csp_cli_niv1]          NVARCHAR(255) NULL,
    [csp_cli_niv3]          NVARCHAR(255) NULL,
    [canal_origine]         VARCHAR(50)   NULL,
    [flag_pm]               INT            NULL,
    [flag_cb]               INT            NULL,
    [flag_pm_rem]           INT            NULL,
    [flag_cb_rem]           INT            NULL,
    [flag_cli_eq_pm_cb]     INT            NULL,
    [flag_cli_eq_pm]        INT            NULL,
    [flag_cli_eq_cb]        INT            NULL,
    [flag_cli_neq_pm_cb]    INT            NULL
)
GO