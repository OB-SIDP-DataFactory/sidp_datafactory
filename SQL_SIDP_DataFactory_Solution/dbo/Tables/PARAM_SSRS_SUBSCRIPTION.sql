﻿CREATE TABLE [dbo].[PARAM_SSRS_SUBSCRIPTION]
(
    [ReportSubscriptionId]	BIGINT			IDENTITY (1, 1)	NOT NULL,
    [ReportName]			VARCHAR(50)		NOT NULL,
    [ReportPath]			VARCHAR(MAX)	NOT NULL,
    [SubscriptionName]		VARCHAR(50)		NOT NULL,
    [Key]					VARCHAR(100)	NOT NULL,
    [Value]					VARCHAR(MAX)	NOT NULL,
    CONSTRAINT [PK_PARAM_SSRS_SUBSCRIPTION] PRIMARY KEY CLUSTERED ([ReportSubscriptionId] ASC)
) 
GO