﻿CREATE TABLE [dbo].[MKT_CARTE] (
    [DTE_TRAITEMENT]   DATE            NULL,
    [NUM_CPT_SAB]      VARCHAR(20)    NULL,
    [NUM_CLI]          VARCHAR(7)     NULL,
    [COD_CAR]          VARCHAR(6)     NULL,
    [LIB_COD_CAR]      VARCHAR(42)    NULL,
    [OPT_DEB]          CHAR(1)     NULL,
    [LIB_OPT_DEB]      NVARCHAR(255)  NULL,
    [FLG_PROT_VIE_COU] VARCHAR(3)     NOT NULL,
    [FLG_PROT_ACH]     VARCHAR(3)     NOT NULL,
    [FLG_PROT_IDEN]    VARCHAR(3)     NOT NULL,
    [FLG_MOY_PAI]      VARCHAR(3)     NOT NULL,
    [MTT_PLD_ACH]      DECIMAL (18, 3) NULL,
    [MTT_PLD_RET]      DECIMAL (18, 3) NULL,
    [DTE_CMD]          DATE            NULL
);


