﻿CREATE TABLE [dbo].[CRE_OPPORT_SOUSCRIPT_ASSOC] (
    [ID_CRE_OPPORT_SOUSCRIPT_ASSOC] BIGINT        IDENTITY (1, 1) NOT NULL,
    [DTE_ALIM]                      DATE          NULL,
    [IDE_OPPRT_CONTACT_ROLE_SF]     NVARCHAR(50) NOT NULL,
    [IDE_OPPRT_SF]                  NVARCHAR(18) NULL,
    [IDE_CONTACT]                   NVARCHAR(18) NULL,
    [ROLE]                          NVARCHAR(40) NULL,
    [FLG_PPAL]                      NVARCHAR(10) NULL,
    [DTE_CREA]                      DATETIME      NULL,
    [IDE_USER_CREA_SF]              NVARCHAR(18) NULL,
    [DTE_DER_MOD]                   DATETIME      NULL,
    [IDE_USER_MOD_SF]               NVARCHAR(18) NULL,
    [SystemModstamp]                DATETIME      NULL,
    [FLG_SUPP]                      NVARCHAR(10) NULL,
    CONSTRAINT [PK_CRE_OPPORT_SOUSCRIPT_ASSOC] PRIMARY KEY CLUSTERED ([ID_CRE_OPPORT_SOUSCRIPT_ASSOC] ASC)
);

