﻿CREATE TABLE [dbo].[SIDP_QOD_RESULT_DETAILLE] (
    [RUN_ID]      INT            NULL,
    [ID_CONTROL]  INT            NULL,
    [message]     NVARCHAR(MAX) NULL,
    [OUTPUT_COL1] VARCHAR(MAX)  NULL,
    [OUTPUT_COL2] VARCHAR(MAX)  NULL,
    [OUTPUT_COL3] VARCHAR(MAX)  NULL,
    [OUTPUT_COL4] VARCHAR(MAX)  NULL,
    [OUTPUT_COL5] VARCHAR(MAX)  NULL,
    [OUTPUT_COL6] VARCHAR(MAX)  NULL,
    [OUTPUT_COL7] VARCHAR(MAX)  NULL
);
GO
CREATE NONCLUSTERED INDEX IX_NC_QOD_RES_DET
ON [dbo].[SIDP_QOD_RESULT_DETAILLE] ([RUN_ID],[ID_CONTROL])
GO