﻿CREATE TABLE [dbo].[INFOS_INCIDENT_TRAITEMENT] (
    [DAT_TRTM]           DATE            NULL,
    [FRQN_TRTM]          CHAR (1)        NULL,
    [ORGN]               NVARCHAR (20)   NULL,
    [DSCR]               NVARCHAR (1000) NULL,
    [PLN_ACTN_MOYE_TERM] NVARCHAR (1000) NULL,
    [FICH_JIRA]          NVARCHAR (20)   NULL,
    [HEUR_ESTM_RETB]     TIME (7)        NULL,
    [DAT_CRTN_ENRG]      DATETIME        CONSTRAINT [DF_DAT_CRTN_ENRG] DEFAULT (getdate()) NULL,
    [IDNT_CRTN_ENRG]     NVARCHAR (30)   NULL
);


GO