﻿CREATE TABLE [dbo].[DIM_CASE_OBJET_SECONDAIRE] (
    [PK_ID]              INT			IDENTITY(1, 1)			NOT NULL,
    [CODE_SF]            NVARCHAR(255)							NULL,
    [LIBELLE]            NVARCHAR(255)							NULL,
    [Validity_StartDate] DATETIME		DEFAULT('2017-01-01')	NULL,
    [Validity_EndDate]   DATETIME		DEFAULT('9999-12-31')	NULL,
    PRIMARY KEY CLUSTERED ([PK_ID] ASC)
)
GO