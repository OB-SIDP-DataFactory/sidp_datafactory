﻿CREATE TABLE [dbo].[T_FACT_MKT_USAGE_CPT_MONTH] (
    [MOIS_TRAITEMENT]        NVARCHAR(4000) NULL,
    [NUM_CPT_SAB]            VARCHAR(20)    NULL,
    [TYP_MVT]                VARCHAR(80)    NULL,
    [SOUS_TYP_MVT]           VARCHAR(255)   NULL,
    [NOMBRE_OP]              INT             NULL,
    [MONTANT_OP]             DECIMAL (18, 2) NULL,
    [FLAG_MTT_OPT]           INT             NULL,
    [FLAG_INITIATIVE_CLIENT] VARCHAR(20)    NULL,
    [FLAG_OP_DEB_CRE]        VARCHAR(20)    NULL,
    [DT_DER_MVT]             DATE            NULL,
    [TYP_DER_MVT]            VARCHAR(80)    NULL,
    [SOUS_TYP_DER_MVT]       VARCHAR(255)   NULL,
    [DT_PRE_UTI_PM]          DATE            NULL,
    [DT_PRE_UTI_CB]          DATE            NULL,
    [DT_PRE_OPE_DEB]         DATE            NULL,
    [DT_PRE_OPE_CRE]         DATE            NULL,
    [DT_DER_UTI_PM]          DATE            NULL,
    [DT_DER_UTI_CB]          DATE            NULL,
    [DT_DER_OPE_DEB]         DATE            NULL,
    [DT_DER_OPE_CRE]         DATE            NULL,
    [DT_VRS_PRI_BVN]         DATE            NULL
);

