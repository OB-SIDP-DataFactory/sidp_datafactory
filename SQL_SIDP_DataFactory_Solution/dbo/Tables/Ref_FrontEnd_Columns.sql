﻿CREATE TABLE [dbo].[Ref_FrontEnd_Columns](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TableId] [int] NOT NULL,
	[ColumnName] [varchar](100) NOT NULL,
	[ColumnDescription] [varchar](500) NULL,
	[ColumnDataType] [varchar](50) NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[ValidityStartDate] [datetime2](7) NOT NULL,
	[ValidityEndDate] [datetime2](7) NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Ref_FrontEnd_Columns] ADD  DEFAULT ((1)) FOR [IsEnabled]
GO

ALTER TABLE [dbo].[Ref_FrontEnd_Columns] ADD  DEFAULT ('9999-12-31 23:59:59.999') FOR [ValidityEndDate]
GO
