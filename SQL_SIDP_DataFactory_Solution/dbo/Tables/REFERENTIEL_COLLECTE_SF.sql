﻿CREATE TABLE [dbo].[REFERENTIEL_COLLECTE_SF](
	[Id]				INT				IDENTITY (1, 1)						NOT NULL,
	[Source]			VARCHAR(20)											NOT NULL,
	[Environment]		VARCHAR(20)											NOT NULL,
	[TableName]			VARCHAR(100)										NOT NULL,
	[ColumnName]		VARCHAR(100)										NOT NULL,
	[ColumnDescription] VARCHAR(500)										NULL,
	[ColumnDataType]	VARCHAR(50)											NOT NULL,
	[ColumnActive]		BIT				DEFAULT (1)							NOT NULL,
	[ValidityStartDate] DATETIME2(7)										NOT NULL,
	[ValidityEndDate]	DATETIME2(7)	DEFAULT ('9999-12-31 23:59:59.999')	NOT NULL
)
GO
CREATE NONCLUSTERED INDEX [IX_NC_REF_COL_SF]
ON [dbo].[REFERENTIEL_COLLECTE_SF] ([Source],[Environment],[TableName],[ColumnName])
GO