﻿CREATE TABLE [dbo].[WK_STOCK_OPPORTUNITY] (
    [date_alim]                            DATE           NULL,
    [Id_SF]                                NVARCHAR(50)  NULL,
    [StageName]                            NVARCHAR(40)  NULL,
    [motif_non_aboutie]                    NVARCHAR(255) NULL,
    [Indication__c]                        NVARCHAR(80)  NULL,
    [flag_indication]                      VARCHAR(3)    NOT NULL,
    [CreatedById]                          NVARCHAR(18)  NULL,
    [CreatedDate]                          DATETIME       NULL,
    [CloseDate]                            DATETIME       NULL,
    [age_opport]                           INT            NULL,
    [OwnerId]                              NVARCHAR(18)  NULL,
    [AdvisorCode__c]                       NVARCHAR(80)  NULL,
    [PointOfSaleCode__c]                   NVARCHAR(80)  NULL,
    [CommercialOfferCode__c]               NVARCHAR(80)  NULL,
    [CommercialOfferName__c]               NVARCHAR(80)  NULL,
    [RelationEntryScore__c]                NVARCHAR(255) NULL,
    [flag_full_digi]                       INT            NOT NULL,
    [flag_full_btq]                        INT            NOT NULL,
    [StartedChannel__c]                    NVARCHAR(255) NULL,
    [DistributionChannel__c]               NVARCHAR(255) NULL,
    [DistributorNetwork__c]                NVARCHAR(80)  NULL,
    [DistributorEntity__c]                 NVARCHAR(80)  NULL,
    [LeadSource]                           NVARCHAR(80)  NULL,
    [canal_fin]                            NVARCHAR(80)  NULL,
    [sous_reseau_fin]                      NVARCHAR(80)  NULL,
    [OptInDataTelco__c]                    NVARCHAR(10)  NULL,
    [OptInOrderTelco__c]                   NVARCHAR(10)  NULL,
    [AccountId]                            NVARCHAR(18)  NULL,
    [ProductFamilyCode__c]                 NVARCHAR(80)  NULL,
    [ProductFamilyName__c]                 NVARCHAR(80)  NULL,
    [flag_sidp]                            INT            NOT NULL,
    [last_update_date]                     DATETIME2 (7)  NULL,
    [flag_opport_last_state]               INT            NOT NULL,
    [flag_opport_stage_last_state]         INT            NOT NULL,
    [flag_opport_last_state_overall]       INT            NOT NULL,
    [flag_opport_stage_last_state_overall] INT            NOT NULL
)
GO
CREATE NONCLUSTERED INDEX [IX_NC_WK_STK_OPP_DTE]
ON [dbo].[WK_STOCK_OPPORTUNITY] ([date_alim])
GO
/*
CREATE NONCLUSTERED INDEX IX_NC_WK_STK_OPP_COM_OFF
ON [dbo].[WK_STOCK_OPPORTUNITY] ([CommercialOfferCode__c], [flag_opport_last_state_overall], [StageName], [last_update_date])
INCLUDE ([date_alim], [Id_SF], [CreatedDate], [CloseDate], [LeadSource])
GO
CREATE NONCLUSTERED INDEX IX_NC_WK_STK_FLG_COM_LSO
ON [dbo].[WK_STOCK_OPPORTUNITY] ([flag_opport_last_state_overall])
INCLUDE ([date_alim], [Id_SF], [StageName], [flag_indication], [CreatedById], [CreatedDate], [AdvisorCode__c], [PointOfSaleCode__c], [CommercialOfferCode__c], [CommercialOfferName__c], [flag_full_digi], [flag_full_btq], [StartedChannel__c], [DistributorNetwork__c], [DistributorEntity__c], [ProductFamilyCode__c], [ProductFamilyName__c], [last_update_date])
GO
CREATE NONCLUSTERED INDEX IX_NC_WK_STK_OPP_FLG_LSO_STG
ON [dbo].[WK_STOCK_OPPORTUNITY] ([flag_opport_last_state_overall], [StageName])
INCLUDE ([date_alim], [Id_SF], [CreatedDate], [CloseDate], [CommercialOfferCode__c], [LeadSource], [last_update_date])
GO
CREATE NONCLUSTERED INDEX IX_NC_WK_STK_OPP_FLG_LSO_STG_DTE
ON [dbo].[WK_STOCK_OPPORTUNITY] ([flag_opport_last_state_overall], [StageName], [CloseDate])
INCLUDE ([Id_SF], [motif_non_aboutie], [flag_indication], [CreatedDate], [age_opport], [flag_full_digi], [flag_full_btq], [StartedChannel__c], [DistributorNetwork__c], [DistributorEntity__c], [canal_fin], [sous_reseau_fin], [AccountId], [flag_sidp])
GO
CREATE NONCLUSTERED INDEX IX_NC_WK_STK_OPP_FLG_SLS
ON [dbo].[WK_STOCK_OPPORTUNITY] ([flag_opport_stage_last_state_overall])
INCLUDE ([Id_SF], [StageName], [last_update_date])
GO
CREATE NONCLUSTERED INDEX IX_NC_WK_STK_OPP_STG_FLG_LSO
ON [dbo].[WK_STOCK_OPPORTUNITY] ([StageName], [flag_opport_stage_last_state])
INCLUDE ([Id_SF])
GO
*/