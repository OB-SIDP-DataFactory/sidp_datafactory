﻿CREATE TABLE [dbo].[WK_SSRS_SAS_VUE_RC_INTERACTIONS](
	[jour_alim] [date] NULL,
	[Year_alim] [int] NULL,
	[Month_alim] [int] NULL,
	[Week_alim] [int] NULL,
	[nb_interactions] [int] NULL,
	[nb_interactions_WTD] [int] NULL,
	[nb_interactions_MTD] [int] NULL,
	[nb_interactions_YTD] [int] NULL,
	[nb_interactions_FM] [int] NULL,
	[nb_interactions_FY] [int] NULL,
	[nb_interactions_tel] [int] NULL,
	[nb_interactions_tel_WTD] [int] NULL,
	[nb_interactions_tel_MTD] [int] NULL,
	[nb_interactions_tel_YTD] [int] NULL,
	[nb_interactions_tel_FM] [int] NULL,
	[nb_interactions_tel_FY] [int] NULL,
	[nb_interactions_mail] [int] NULL,
	[nb_interactions_mail_WTD] [int] NULL,
	[nb_interactions_mail_MTD] [int] NULL,
	[nb_interactions_mail_YTD] [int] NULL,
	[nb_interactions_mail_FM] [int] NULL,
	[nb_interactions_mail_FY] [int] NULL,
	[nb_interactions_sms] [int] NULL,
	[nb_interactions_sms_WTD] [int] NULL,
	[nb_interactions_sms_MTD] [int] NULL,
	[nb_interactions_sms_YTD] [int] NULL,
	[nb_interactions_sms_FM] [int] NULL,
	[nb_interactions_sms_FY] [int] NULL,
	[nb_interactions_chat_crc] [int] NULL,
	[nb_interactions_chat_crc_WTD] [int] NULL,
	[nb_interactions_chat_crc_MTD] [int] NULL,
	[nb_interactions_chat_crc_YTD] [int] NULL,
	[nb_interactions_chat_crc_FM] [int] NULL,
	[nb_interactions_chat_crc_FY] [int] NULL,
	[nb_interactions_chat_ia] [int] NULL,
	[nb_interactions_chat_ia_WTD] [int] NULL,
	[nb_interactions_chat_ia_MTD] [int] NULL,
	[nb_interactions_chat_ia_YTD] [int] NULL,
	[nb_interactions_chat_ia_FM] [int] NULL,
	[nb_interactions_chat_ia_FY] [int] NULL
)