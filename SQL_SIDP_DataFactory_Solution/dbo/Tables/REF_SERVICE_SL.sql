﻿CREATE TABLE [dbo].[REF_SERVICE_SL] (
    [IDNT_REF_SERVICE_SL] INT            IDENTITY (1, 1) NOT NULL,
    [IDNT_SERV_SL]        DECIMAL (19)   NULL,
    [COD]                 NVARCHAR(255) NULL,
    [COD_CONV]            NVARCHAR(255) NULL,
    [COD_COR_BANK]        NVARCHAR(255) NULL,
    [DAT_CRTN]            DATETIME2 (6)  NULL,
    [IDNT_CRTN]           NVARCHAR(255) NULL,
    [LABL]                NVARCHAR(255) NULL,
    [DAT_DERN_MODF]       DATETIME2 (6)  NULL,
    [IDNT_DERN_MODF]      NVARCHAR(255) NULL,
    [IDNT_COD_PRX]        DECIMAL (19)   NULL,
    [TYP_UTLS]            DECIMAL (19)   NULL,
    [DAT_CRTN_ENRG]       DATETIME       NULL,
    [DAT_DERN_MODF_ENRG]  DATETIME       NULL,
    [DAT_DEBT_VALD]       DATE           NOT NULL,
    [DAT_FIN_VALD]        DATE           NOT NULL,
    [FLG_ENRG_COUR]       BIT            NULL
);
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Flag Courant Actif', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_SERVICE_SL', @level2type = N'COLUMN', @level2name = N'FLG_ENRG_COUR';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de fin de validité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_SERVICE_SL', @level2type = N'COLUMN', @level2name = N'DAT_FIN_VALD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date de début de validité', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_SERVICE_SL', @level2type = N'COLUMN', @level2name = N'DAT_DEBT_VALD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere modification enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_SERVICE_SL', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date creation enregistrement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_SERVICE_SL', @level2type = N'COLUMN', @level2name = N'DAT_CRTN_ENRG';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Type utilisateur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_SERVICE_SL', @level2type = N'COLUMN', @level2name = N'TYP_UTLS';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant Code prix', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_SERVICE_SL', @level2type = N'COLUMN', @level2name = N'IDNT_COD_PRX';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_SERVICE_SL', @level2type = N'COLUMN', @level2name = N'IDNT_DERN_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date derniere modification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_SERVICE_SL', @level2type = N'COLUMN', @level2name = N'DAT_DERN_MODF';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Label', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_SERVICE_SL', @level2type = N'COLUMN', @level2name = N'LABL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_SERVICE_SL', @level2type = N'COLUMN', @level2name = N'IDNT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date creation', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_SERVICE_SL', @level2type = N'COLUMN', @level2name = N'DAT_CRTN';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code Core Banking', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_SERVICE_SL', @level2type = N'COLUMN', @level2name = N'COD_COR_BANK';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code convention', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_SERVICE_SL', @level2type = N'COLUMN', @level2name = N'COD_CONV';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_SERVICE_SL', @level2type = N'COLUMN', @level2name = N'COD';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant service SL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_SERVICE_SL', @level2type = N'COLUMN', @level2name = N'IDNT_SERV_SL';
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifiant référentiel Service SL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'REF_SERVICE_SL', @level2type = N'COLUMN', @level2name = N'IDNT_REF_SERVICE_SL';

