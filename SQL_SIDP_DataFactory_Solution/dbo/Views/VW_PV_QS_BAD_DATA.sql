﻿CREATE VIEW [dbo].[VW_PV_QS_BAD_DATA]
AS
WITH CONTROLS	AS
(
	SELECT
		COUNT(*)	AS	CONTROL_COUNT,
		D.ID,
		C.CONTROL_CODE
	FROM
		dbo.PV_QS_DATA	AS	D	WITH(NOLOCK)
		LEFT JOIN dbo.REF_QS_RECORD_CONTROLS	AS	R	WITH(NOLOCK)
			ON	R.RECORD_ID = D.ID
		LEFT JOIN dbo.REF_QS_CONTROLS	AS	C	WITH(NOLOCK)
			ON	C.ID = R.CONTROL_REF_ID
	GROUP BY
		D.ID,
		C.CONTROL_CODE
),
RECORDS_KO	AS
(
	SELECT DISTINCT
		C.ID,
		STUFF(
			(
				SELECT
					', ' + CR.CONTROL_CODE + ' (' + CAST(CR.CONTROL_COUNT AS CHAR(1)) + ')'
				FROM
					CONTROLS	AS	CR
				WHERE
					1 = 1
					AND	CR.CONTROL_COUNT > 1
					AND	CR.ID = C.ID
			 FOR XML PATH('')),
	1,
	1,
	'') KO_MESSAGE
	FROM
		CONTROLS	AS	C
	WHERE
		1 = 1
		AND	CONTROL_COUNT > 1
)
SELECT
	D.ID,
	RIGHT(R.KO_MESSAGE, LEN(R.KO_MESSAGE) - 1) AS	KO_MESSAGE,
	D.[TOKEN],
	D.[ID_CLIENT],
	D.[CODE_STATUT],
	D.[NOM_STATUT],
	D.[LIBELLE],
	D.[CODE_SOUS_STATUT],
	D.[LIBELLE_1],
	D.[CIVILITE],
	D.[NOM],
	D.[NOM_JEUNE_FILLE],
	D.[PRENOM],
	D.[DATE_NAISSANCE],
	D.[MARITAL_STATUS],
	D.[CODEPOSTAL1],
	D.[VILLE1],
	D.[TEL_PORTABLE_1],
	D.[TEL_PORTABLE_2],
	D.[CIVILITE2],
	D.[NOM2],
	D.[NOM_JEUNE_FILLE2],
	D.[PRENOM2],
	D.[DATE_NAISSANCE2],
	D.[MARITAL_STATUS2],
	D.[CODEPOSTAL2],
	D.[VILLE2],
	D.[TEL_PORTABLE2_1],
	D.[TEL_PORTABLE2_2],
	D.[TYPE_LADRAD_V2],
	D.[DEEP_LEARNING_DOCUMENT_CODE],
	D.[NOMFICHIER],
	D.[LAST_SCORE_DATE],
	D.[SCORE],
	D.[holder],
	D.[holder_addressCity],
	D.[holder_addressStreet],
	D.[holder_birthDate],
	D.[holder_birthPlace],
	D.[holder_city],
	D.[holder_deliveryDate],
	D.[holder_deliveryOrganism],
	D.[holder_firstName],
	D.[holder_firstName_L2],
	D.[holder_fullAddress],
	D.[holder_initials],
	D.[holder_lastName],
	D.[holder_lastName_L2],
	D.[holder_sex],
	D.[holder_size],
	D.[holder_validityDate],
	D.[idNumber],
	D.[identificationNumberOfTheNationalRegister],
	D.[issuingCountry],
	D.[mrz],
	D.[mrz_checksum],
	D.[mrz_idNumber],
	D.[mrz_line1],
	D.[mrz_line2],
	D.[mrz_line3],
	D.[mrzHolder],
	D.[mrzHolder_birthDate],
	D.[mrzHolder_expirationDate],
	D.[mrzHolder_firstName],
	D.[mrzHolder_gender],
	D.[mrzHolder_lastName],
	D.[mrzHolder_nationality],
	D.[ssType],
	D.[type],
	D.[canal],
	D.[ANOTHER_UPLOADED_IDE_DETECTION],
	D.[CROPPED_PICTURE_EXTRACT_DETECTION],
	D.[FACEMATCHING],
	D.[ID_NUMBER_VALIDATION],
	D.[conformity.color],
	D.[conformity.mrzAlignment],
	D.[conformity.photoLocation],
	D.[conformity.visualSecurity],
	D.[holder.birthDate],
	D.[holder.city],
	D.[holder.deliveryDate],
	D.[holder.deliveryDate.2],
	D.[holder.deliveryOrganism],
	D.[holder.deliveryOrganism.2],
	D.[holder.firstName],
	D.[holder.firstName.2],
	D.[holder.initials],
	D.[holder.lastName],
	D.[holder.sex],
	D.[holder.validityDate],
	D.[idNumber_1],
	D.[missing.page],
	D.[mrz.checksum],
	D.[mrz.syntaxe],
	D.[mrzHolder.birthDate],
	D.[mrzHolder.expirationDate],
	D.[mrzHolder.firstName],
	D.[mrzHolder.gender],
	D.[mrzHolder.lastName],
	D.[mrzHolder.nameInversion],
	D.[tech.invalidDoc]
FROM
	RECORDS_KO	AS	R
	JOIN	dbo.PV_QS_DATA	AS	D	WITH(NOLOCK)
		ON	R.ID = D.ID
