﻿CREATE VIEW [dbo].[DIM_TEMPS_VUE_MKT]
AS
SELECT CAST([Year]+[Month] AS INT) as ID_TPS,
	[Month] ,
	[MonthName],
	[MonthYear],
	[Quarter],
	[QuarterName],
	[Year],
	[StandardDate]
	FROM [dbo].[DIM_TEMPS]
	WHERE [Day] = '01'