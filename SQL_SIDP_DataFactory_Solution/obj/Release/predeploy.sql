﻿/*
 Pre-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be executed before the build script.	
 Use SQLCMD syntax to include a file in the pre-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the pre-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

PRINT 'Executing PreDeployment scripts ...'

USE [$(DataFactoryDatabaseName)]
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WK_ALL_SAB_CARTES]') AND type in (N'U'))
BEGIN
    TRUNCATE TABLE [dbo].[WK_ALL_SAB_CARTES]
END
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WK_ALL_SAB_COMPTES]') AND type in (N'U'))
BEGIN
    TRUNCATE TABLE [dbo].[WK_ALL_SAB_COMPTES]
END
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WK_ALL_SFCRM_ACCOUNT]') AND type in (N'U'))
BEGIN
    TRUNCATE TABLE [dbo].[WK_ALL_SFCRM_ACCOUNT]
END
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WK_ALL_SFCRM_OPPORTUNITY]') AND type in (N'U'))
BEGIN
    TRUNCATE TABLE [dbo].[WK_ALL_SFCRM_OPPORTUNITY]
END
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WK_etat_stock_cases]') AND type in (N'U'))
BEGIN
    TRUNCATE TABLE [dbo].[WK_etat_stock_cases]
END
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wk_join_dim_cli]') AND type in (N'U'))
BEGIN
    TRUNCATE TABLE [dbo].[wk_join_dim_cli]
END
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wk_join_dim_flux_opport_fin]') AND type in (N'U'))
BEGIN
    TRUNCATE TABLE [dbo].[wk_join_dim_flux_opport_fin]
END
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wk_join_dim_stock_opport]') AND type in (N'U'))
BEGIN
    TRUNCATE TABLE [dbo].[wk_join_dim_stock_opport]
END
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WK_JOIN_INTERACTIONS]') AND type in (N'U'))
BEGIN
    TRUNCATE TABLE [dbo].[WK_JOIN_INTERACTIONS]
END
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wk_join_sources_cli]') AND type in (N'U'))
BEGIN
    TRUNCATE TABLE [dbo].[wk_join_sources_cli]
END
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wk_join_sources_ferm_cpt]') AND type in (N'U'))
BEGIN
    TRUNCATE TABLE [dbo].[wk_join_sources_ferm_cpt]
END
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WK_JOIN_SOURCES_OPPORT]') AND type in (N'U'))
BEGIN
    TRUNCATE TABLE [dbo].[WK_JOIN_SOURCES_OPPORT]
END
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wk_join_sources_usage_cpt]') AND type in (N'U'))
BEGIN
    TRUNCATE TABLE [dbo].[wk_join_sources_usage_cpt]
END
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WK_OPPORTUNITY_CANAL]') AND type in (N'U'))
BEGIN
    TRUNCATE TABLE [dbo].[WK_OPPORTUNITY_CANAL]
END
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WK_SAB_CARTES]') AND type in (N'U'))
BEGIN
    TRUNCATE TABLE [dbo].[WK_SAB_CARTES]
END
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WK_SAB_COMPTES]') AND type in (N'U'))
BEGIN
    TRUNCATE TABLE [dbo].[WK_SAB_COMPTES]
END
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WK_SFCRM_ACCOUNT]') AND type in (N'U'))
BEGIN
    TRUNCATE TABLE [dbo].[WK_SFCRM_ACCOUNT]
END
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WK_SFCRM_OPPORTUNITY]') AND type in (N'U'))
BEGIN
    TRUNCATE TABLE [dbo].[WK_SFCRM_OPPORTUNITY]
END
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WK_STOCK_OPPORT]') AND type in (N'U'))
BEGIN
    TRUNCATE TABLE [dbo].[WK_STOCK_OPPORT]
END
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WK_STOCK_OPPORT_HISTO]') AND type in (N'U'))
BEGIN
    TRUNCATE TABLE [dbo].[WK_STOCK_OPPORT_HISTO]
END
GO



GO
