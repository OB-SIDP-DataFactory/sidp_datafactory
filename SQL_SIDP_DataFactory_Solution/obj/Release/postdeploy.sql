﻿/*
Modèle de script de post-déploiement							
--------------------------------------------------------------------------------------
 Ce fichier contient des instructions SQL qui seront ajoutées au script de compilation.		
 Utilisez la syntaxe SQLCMD pour inclure un fichier dans le script de post-déploiement.			
 Exemple :      :r .\monfichier.sql								
 Utilisez la syntaxe SQLCMD pour référencer une variable dans le script de post-déploiement.		
 Exemple :      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

PRINT 'Executing PostDeployment scripts ...'

/* Integration Services Applicative Account needs to be granted acces to database to perform data loads */

/* Create the login if it does not exist */
USE [master]
GO

IF NOT EXISTS ( SELECT name FROM master.sys.server_principals WHERE name = '$(IsAppAccount)')
BEGIN
    CREATE LOGIN [$(IsAppAccount)] FROM WINDOWS WITH DEFAULT_DATABASE=[master]
END
GO

/* Create the user if it does not exist */
USE [$(DatabaseName)]
GO

IF NOT EXISTS ( SELECT name FROM sys.database_principals WHERE name = '$(IsAppAccount)')
BEGIN
    CREATE USER [$(IsAppAccount)] FOR LOGIN [$(IsAppAccount)] 
END
GO

/* Grant access to user */
ALTER USER [$(IsAppAccount)] WITH DEFAULT_SCHEMA=[dbo]
GO

ALTER ROLE [db_owner] ADD MEMBER [$(IsAppAccount)]
GO


/* Reporting Services Applicative Account needs to be granted acces to database */

/* Create the login if it does not exist */
USE [master]
GO

IF NOT EXISTS ( SELECT name FROM master.sys.server_principals WHERE name = '$(RsAppAccount)')
BEGIN
    CREATE LOGIN [$(RsAppAccount)] FROM WINDOWS WITH DEFAULT_DATABASE=[master]
END
GO

/* Create the user if it does not exist */
USE [$(DatabaseName)]
GO

IF NOT EXISTS ( SELECT name FROM sys.database_principals WHERE name = '$(RsAppAccount)')
BEGIN
    CREATE USER [$(RsAppAccount)] FOR LOGIN [$(RsAppAccount)] 
END
GO

/* Grant access to user */
ALTER USER [$(RsAppAccount)] WITH DEFAULT_SCHEMA=[dbo]
GO

ALTER ROLE [db_owner] ADD MEMBER [$(RsAppAccount)]
GO


USE [master]
GO

IF NOT EXISTS ( SELECT name FROM master.sys.server_principals WHERE name = '$(SasSqlAccount)')
BEGIN
    CREATE LOGIN [$(SasSqlAccount)] WITH PASSWORD=N'$(SasSqlAccountPwd)', DEFAULT_DATABASE=[$(DatabaseName)], CHECK_EXPIRATION=OFF, CHECK_POLICY=ON
END
GO

USE [$(DatabaseName)]
GO

IF NOT EXISTS ( SELECT name FROM sys.database_principals WHERE name = '$(SasSqlAccount)')
BEGIN
    CREATE USER [$(SasSqlAccount)] FOR LOGIN [$(SasSqlAccount)] 
END
GO

ALTER USER [$(SasSqlAccount)] WITH DEFAULT_SCHEMA=[dbo]
GO

ALTER ROLE [db_owner] ADD MEMBER [$(SasSqlAccount)]
GO


USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_AGE_CLI]
GO

INSERT INTO [dbo].[DIM_AGE_CLI] ([AGE_CLI], [AGE_CLI_MIN], [AGE_CLI_MAX])
VALUES ('16-17 ans', 16, 17)
     , ('18-24 ans', 18, 24)
     , ('25-39 ans', 25, 39)
     , ('40-59 ans', 40, 59)
     , ('60-69 ans', 60, 69)
     , ('70+ ans', 70, NULL )
GO


USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_AGE_CPT]
GO

INSERT INTO [dbo].[DIM_AGE_CPT] ([AGE_CPT], [AGE_CPT_MIN], [AGE_CPT_MAX])
VALUES ('<3 mois', NULL, 2)
     , ('3-12 mois', 3, 12)
     , ('>12 mois', 13, NULL)
GO


USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_AGE_OPPORT]
GO

INSERT INTO [dbo].[DIM_AGE_OPPORT] ([AGE_OPPORT], [AGE_OPPORT_MIN], [AGE_OPPORT_MAX])
VALUES ('1 jour', 0, 1)
     , ('2-3 jours', 2, 3)
     , ('4-5 jours', 4, 5)
     , ('6-10 jours', 6, 10)
     , ('11-20 jours', 11, 20)
     , ('21-30 jours', 21, 30)
     , ('31-60 jours', 31, 60)
     , ('60+ jours', 60, NULL )
GO


USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_CANAL]
GO

INSERT INTO [dbo].[DIM_CANAL] ([CODE_SF], [LIBELLE], [PARENT_ID], [Validity_StartDate])
VALUES ('01', 'Digital', NULL, '2017-01-01')
     , ('02', 'Agence', NULL, '2017-01-01')
     , ('03', 'CRC', NULL, '2017-01-01')
     , ('04', 'Autre', NULL, '2017-01-01')
     , ('04', 'E-shop', '01', '2017-01-01')
     , ('05', 'Orange&moi web', '01', '2017-01-01')
     , ('06', 'Orange&moi appli', '01', '2017-01-01')
     , ('07', 'Orange Cash', '01', '2017-01-01')
     , ('08', 'Portail OF', '01', '2017-01-01')
     , ('10', 'Appli OB', '01', '2017-01-01')
     , ('11', 'Web OB', '01', '2017-01-01')
     , ('02', 'Boutique IOBSP', '02', '2017-01-01')
     , ('03', 'Boutique non IOBSP', '02', '2017-01-01')
     , ('12', 'Appel entrant', '03', '2017-01-01')
     , ('13', 'Appel sortant', '03', '2017-01-01')
     , ('14', 'Chat', '03', '2017-01-01')
     , ('15', 'E-mail', '03', '2017-01-01')
     , ('01', 'A préciser', '04', '2017-01-01')
     , ('09', 'TV', '04', '2017-01-01')
     , ('16', 'Courrier/fax', '04', '2017-01-01')
     , ('-1', 'Non renseigné', '-1', '2017-01-01')
GO


USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_CSP_CLI]
GO

INSERT INTO [dbo].[DIM_CSP_CLI] ([CODE_SF_CSP], [CSP_CLI] ,[Validity_StartDate])
VALUES ('01', 'Agriculteurs exploitants', '2017-01-01')
     , ('02', 'Artisans, commerçants et chefs d''entreprise', '2017-01-01')
     , ('03', 'Cadres et professions intellectuelles supérieures', '2017-01-01')
     , ('04', 'Professions intermédiaires', '2017-01-01')
     , ('05', 'Employés', '2017-01-01')
     , ('06', 'Ouvriers', '2017-01-01')
     , ('07', 'Retraités', '2017-01-01')
     , ('08', 'Autres personnes sans activité professionnelle', '2017-01-01')
GO


USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_FACTURATION_CPT]
GO

INSERT INTO [dbo].[DIM_FACTURATION_CPT] ([FACTURATION_CPT], [FACTURATION_CPT_MIN], [FACTURATION_CPT_MAX])
VALUES ('0 €', NULL, NULL)
     , ('5 €', 5, 5)
     , ('10 à 30 €', 10, 30)
     , ('35 à 55 €', 35, 55)
     , ('60 €', 60, NULL)
GO


USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_MOTIF_FERMETURE]
GO

INSERT INTO [dbo].[DIM_MOTIF_FERMETURE] ([MOT_CLO], [MOTIF_FERMETURE], [MOTIF_FERMETURE2], [Validity_StartDate])
VALUES ('CREDIT', 'FIN DE CREDIT', 'Autre', '2003-01-01')
     , ('ALERTE', 'COMPTES DOUTEUX', 'Autre', '2003-01-01')
     , ('DC', 'DECES DU TITULAIRE ', 'Autre', '2003-01-01')
     , ('DECES', 'DECES', 'Autre', '2003-01-01')
     , ('SAICLO', 'CLOTURE SAISIE ADMINISTRATIVE', 'Autre', '2003-01-01')
     , ('TARIFI', 'TARIFIC + ABONNEMENT', 'Autre', '2003-01-01')
     , ('TRANSF', 'TRANSFERT INTERAGENCE', 'Autre', '2003-01-01')
     , ('8', 'CLOTURE APRES 8 ANS             ', 'Autre', '2003-01-01')
     , ('-2', 'CLOTURE AVANT 2 ANS             ', 'Autre', '2003-01-01')
     , ('CE', 'CESSATION ACTIVITE NON SALARIE  ', 'Autre', '2003-01-01')
     , ('CH', 'CHOMAGE                         ', 'Autre', '2003-01-01')
     , ('DS', 'DECES DU CONJOINT               ', 'Autre', '2003-01-01')
     , ('EX', 'EXPIRATION ASSURANCE CHOMAGE    ', 'Autre', '2003-01-01')
     , ('IN', 'INVALIDITE DU TITULAIRE         ', 'Autre', '2003-01-01')
     , ('MR', 'MOTIF REGLEMENTAIRE             ', 'Autre', '2003-01-01')
     , ('NI', 'TITULAIRE NON IMPOSABLE         ', 'Autre', '2003-01-01')
     , ('RE', 'RETRAIT ESPECES AVANT 8 ANS     ', 'Autre', '2003-01-01')
     , ('RT', 'RETRAIT TITRES AVANT 8 ANS      ', 'Autre', '2003-01-01')
     , ('25', 'CLOTURE ENTRE 2 ET 5 ANS        ', 'Autre', '2003-01-01')
     , ('58', 'CLOTURE ENTRE 5 ET 8 ANS        ', 'Autre', '2003-01-01')
     , ('CC   ', 'CHOIX DU CLIENT            ', 'Autre', '2003-01-01')
     , ('BANQUE', 'A LA DEMANDE DE LA BANQUE', 'Clôture', '2003-01-01')
     , ('RECOUV', 'RECOUVREMENT', 'Clôture', '2003-01-01')
     , ('CC', 'CHOIX DU CLIENT', 'Résiliation', '2003-01-01')
     , ('CHANGE', 'CHANGEMENT GAMME', 'Résiliation', '2003-01-01')
     , ('CLIENT', 'A LA DEMANDE DU CLIENT', 'Résiliation', '2003-01-01')
     , ('COMPRO', 'COMPTE PROFESSIONNEL', 'Résiliation', '2003-01-01')
     , ('DESOLI', 'DESOLIDARISATION', 'Résiliation', '2003-01-01')
     , ('DORMAN', 'COMPTE SANS MOUV', 'Résiliation', '2003-01-01')
     , ('DYSFON', 'DYSF - ERREUR BANQUE', 'Résiliation', '2003-01-01')
     , ('INSATI', 'INSATISFAIT DE L''OFFRE', 'Résiliation', '2003-01-01')
     , ('RECLAM', 'SUITE A RECLAMATION', 'Résiliation', '2003-01-01')
     , ('RESEAU', 'ERREUR RESEAU', 'Résiliation', '2003-01-01')
     , ('RETRAC', 'RETRACTATION CLIENT', 'Résiliation', '2003-01-01')
     , ('SANMOT', 'SANS MOTIF-EXPLICATI', 'Résiliation', '2003-01-01')
     , ('SUCCES', 'SUCCESSION', 'Résiliation', '2003-01-01')
     , ('TITRES', 'TITRES', 'Résiliation', '2003-01-01')
     , ('TUTELL', 'TUTELLE', 'Résiliation', '2003-01-01')
     , ('DE', 'DEMANDE DU CLIENT               ', 'Résiliation', '2003-01-01')
     , (' TC   ', 'TRANSFERT A LA CONCURRENCE ', 'Résiliation', '2003-01-01')
GO


USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_MOYENS_PAIEMENT]
GO

INSERT INTO [dbo].[DIM_MOYENS_PAIEMENT] ([CODE_MOYENS_PAIEMENT], [MOYENS_PAIEMENT], [Validity_StartDate])
VALUES ('P', 'Carte bancaire exclusivement', '2017-01-01')
     , ('M', 'Paiement mobile exclusivement', '2017-01-01')
     , ('M P', 'Carte bancaire et paiement mobile', '2017-01-01')
     , ('NA', 'Aucun moyen de paiement', '2017-01-01')
GO


USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_OPERATIONS]
GO

INSERT INTO [dbo].[DIM_OPERATIONS] ([COD_OPE], [DON_ANA], [COD_EVE], [LIB_OPE], [TYPE])
VALUES 
		('AV0',NULL,NULL,'Virement sortant','virement')
		,('A0V',NULL,NULL,'Virement sortant','virement')
		,('R0V',NULL,NULL,'Virement entrant','virement')
		,('R0P',NULL,NULL,'Prélèvement Sortant','prélèvement')
		,('A0P',NULL,NULL,'Prélèvement Entrant','prélèvement')
		,('CTB','','IMM','Paiement carte bancaire','CB')
		,('CTB','1','IMM','Paiement carte bancaire','CB')
		,('CTB','2','IMM','Paiement mobile','PM')
		,('FAC','FBAG80','FAC','Facturation pour inactivité','facturation')
		,('ERE',NULL,'VER','Mouvement sur compte epargne','dep_ret')
GO


USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_RESEAU]
GO

INSERT INTO DIM_RESEAU ([CODE_SF], [LIBELLE], [PARENT_ID], [Validity_StartDate])
VALUES ('01', 'Orange France', NULL, '2017-01-01')
     , ('02', 'Orange Bank', NULL, '2017-01-01')
     , ('03', 'Groupama', NULL, '2017-01-01')
     , ('01', 'AD', '01', '2017-01-01')
     , ('02', 'GDT', '01', '2017-01-01')
     , ('03', 'Orange Bank', '02', '2017-01-01')
     , ('04', 'OF-WEB', '01', '2017-01-01')
     , ('05', 'Caisses régionales', '03', '2017-01-01')
     , ('06', 'GAN Patrimoine', '03', '2017-01-01')
     , ('07', 'GAN Prévoyance', '03', '2017-01-01')
     , ('08', 'GAN Assurance', '03', '2017-01-01')
     , ('09', 'DO', '04', '2017-01-01')
GO


USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_STADE_VENTE]
GO

INSERT INTO [dbo].[DIM_STADE_VENTE] ([CODE_SF], [LIBELLE] ,[PARENT_ID] ,[Validity_StartDate])
VALUES ('01', 'Détection', NULL, '2017-01-01')
     , ('02', 'Découverte', NULL, '2017-01-01')
     , ('03', 'Elaboration proposition', NULL, '2017-01-01')
     , ('04', 'Affaire signée en attente', NULL, '2017-01-01')
     , ('05', 'Vérification Banque', NULL, '2017-01-01')
     , ('06', 'Dossier incomplet', NULL, '2017-01-01')
     , ('07', 'Dossier validé', NULL, '2017-01-01')
     , ('08', 'Incident technique', NULL, '2017-01-01')
     , ('09', 'Compte ouvert', NULL, '2017-01-01')
     , ('10', 'Affaire refusée', NULL, '2017-01-01')
     , ('11', 'Sans suite', NULL, '2017-01-01')
     , ('12', 'Rétractation', NULL, '2017-01-01')
     , ('13', 'Affaire conclue', NULL, '2017-01-01')
     , ('01', 'Risque', '10', '2017-01-01')
     , ('02', 'Contacter SECFI', '10', '2017-01-01')
     , ('03', 'Fraude', '10', '2017-01-01')
     , ('04', 'Suspicion', '10', '2017-01-01')
     , ('05', 'Refus entrée en relation', '10', '2017-01-01')
     , ('06', 'Risque', '10', '2017-01-01')
	 , ('07', 'Refus dossier Crédit', '10', '2017-01-01')
	 , ('08', 'FICP', '10', '2017-01-01')
     , ('09', 'Refus Engagement', '10', '2017-01-01')
     , ('01', 'Indication expirée', '11', '2017-01-01')
     , ('02', 'Dossier de souscription expiré', '11', '2017-01-01')
     , ('03', 'Refus client', '11', '2017-01-01')
	 , ('04', 'Modifications de données sensibles', '11', '2017-01-01')

GO


USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_USAGE_CB]
GO

INSERT INTO [dbo].[DIM_USAGE_CB] ([USAGE_CB], [NB_OP_MIN], [NB_OP_MAX])
VALUES ('0 opérations', 0, 0)
     , ('1-5 opérations', 1, 5)
     , ('6-10 opérations', 6, 10)
     , ('11-15 opérations', 11, 15)
     , ('16- 20 opérations', 16, 20)
     , ('20+ opérations', 21, NULL)
GO


USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_USAGE_PM]
GO

INSERT INTO [dbo].[DIM_USAGE_PM] ([USAGE_PM], [NB_OP_MIN], [NB_OP_MAX])
VALUES ('0 opérations', 0, 0)
     , ('1-5 opérations', 1, 5)
     , ('6-10 opérations', 6, 10)
     , ('11-15 opérations', 11, 15)
     , ('16- 20 opérations', 16, 20)
     , ('20+ opérations', 21, NULL)
GO


USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_USAGE_PRELEVEMENT]
GO

INSERT INTO [dbo].[DIM_USAGE_PRELEVEMENT] ([USAGE_PRELEVEMENT], [NB_OP_MIN], [NB_OP_MAX])
VALUES ('0 opérations', 0, 0)
     , ('1 opération', 1, 1)
     , ('2 opérations', 2, 2)
     , ('3 opérations', 3, 3)
     , ('4 opérations',4, 4 )
     , ('5 opérations', 5, 5)
     , ('5+ opérations',6, NULL)
GO


USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_USAGE_VIREMENT]
GO

INSERT INTO [dbo].[DIM_USAGE_VIREMENT] ([USAGE_VIREMENT], [NB_OP_MIN], [NB_OP_MAX])
VALUES ('0 opérations', 0, 0)
     , ('1 opération', 1, 1)
     , ('2 opérations', 2, 2)
     , ('3 opérations', 3, 3)
     , ('4 opérations',4, 4 )
     , ('5 opérations', 5, 5)
     , ('5+ opérations',6, NULL)
GO


USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_TEMPS]
GO

SET DATEFIRST 1;

WITH IDX_INIT AS
( SELECT '1' AS IDX UNION
  SELECT '2' AS IDX UNION
  SELECT '3' AS IDX UNION
  SELECT '4' AS IDX UNION
  SELECT '5' AS IDX UNION
  SELECT '6' AS IDX UNION
  SELECT '7' AS IDX UNION
  SELECT '8' AS IDX UNION
  SELECT '9' AS IDX UNION
  SELECT '0' AS IDX
)

INSERT INTO [dbo].[DIM_TEMPS]
( [PK_ID], --TO MAKE THE ID THE YYYYMMDD FORMAT UNCOMMENT THIS LINE... Comment for autoincrementing.
  [Date]
, [Day]
, [DaySuffix]
, [DayOfWeek]
, [DOWInMonth]
, [DayOfYear]
, [WeekOfYear]
, [WeekOfMonth] 
, [IsoWeek] 
, [IsoWeekYear] 
, [Month]
, [MonthName]
, [MonthYear]
, [Quarter]
, [QuarterName]
, [Year]
, [BusinessDay]
, [StandardDate]
)
SELECT CONVERT(VARCHAR, D.[Date], 112), --TO MAKE THE ID THE YYYYMMDD FORMAT UNCOMMENT THIS LINE COMMENT FOR AUTOINCREMENT
       D.[Date] AS [Date]
     , SUBSTRING(CONVERT(VARCHAR, D.[Date], 112), 7, 2) AS [Day]
     , CASE WHEN DATEPART(DAY, D.[Date]) IN (11,12,13) THEN CAST(DATEPART(DAY, D.[Date]) AS VARCHAR) + 'th'
            WHEN RIGHT(DATEPART(DAY, D.[Date]),1) = 1 THEN CAST(DATEPART(DAY, D.[Date]) AS VARCHAR) + 'st'
            WHEN RIGHT(DATEPART(DAY, D.[Date]),1) = 2 THEN CAST(DATEPART(DAY, D.[Date]) AS VARCHAR) + 'nd'
            WHEN RIGHT(DATEPART(DAY, D.[Date]),1) = 3 THEN CAST(DATEPART(DAY, D.[Date]) AS VARCHAR) + 'rd'
            ELSE CAST(DATEPART(DAY, D.[Date]) AS VARCHAR) + 'th' 
       END AS [DaySuffix]
     , DATENAME(WEEKDAY, D.[Date]) AS [DayOfWeek]
     , ROW_NUMBER() OVER (PARTITION BY SUBSTRING(CONVERT(VARCHAR, D.[Date], 112), 1, 6), DATEPART(DW,  D.[Date]) ORDER BY D.[Date]) AS [DOWInMonth]
     , DATEPART(dy, D.[Date]) AS [DayOfYear]               -- Day of the year. 0 - 365/366
     , DATEPART(ww, D.[Date]) AS [WeekOfYear]              -- 0-52/53
     , DATEPART(ww, D.[Date]) + 1 - DATEPART(ww,CAST(DATEPART(mm, D.[Date]) AS VARCHAR) + '/1/' + CAST(DATEPART(yy, D.[Date]) AS VARCHAR)) AS [WeekOfMonth]
     , DATEPART(isoww, D.[Date]) AS [IsoWeek]        -- 0-52/53
     , CASE WHEN DATEPART(isoww, D.[Date]) >= 52 AND DATEPART(mm, D.[Date]) = 1 THEN DATEPART(YEAR, D.[Date]) - 1
            WHEN DATEPART(isoww, D.[Date]) = 1 AND DATEPART(mm, D.[Date]) = 12 THEN DATEPART(YEAR, D.[Date]) + 1
            ELSE DATEPART(YEAR, D.[Date])
       END AS [IsoWeekYear]
     , SUBSTRING(CONVERT(VARCHAR, D.[Date], 112), 5, 2) AS [Month] 
     , DATENAME(MONTH, D.[Date]) AS [MonthName]
     , CONCAT(DATENAME(MONTH, D.[Date]), ' ', DATEPART(YEAR, D.[Date])) AS [MonthYear]
     , DATEPART(qq, D.[Date]) AS [Quarter]                 -- Calendar quarter
     , CASE DATEPART(qq, D.[Date]) 
            WHEN 1 THEN 'First'
            WHEN 2 THEN 'Second'
            WHEN 3 THEN 'Third'
            WHEN 4 THEN 'Fourth'
            END AS [QuarterName]
     , DATEPART(YEAR, D.[Date]) AS [Year]
     , CASE WHEN /* Sundays */         DATENAME(WEEKDAY, D.[Date]) = 'Sunday' OR
                 /* Fixed Holidays */  CONCAT(DATEPART(d, D.[Date]), '/', DATEPART(m, D.[Date])) IN ('1/1', '1/5', '8/5', '14/7', '15/8', '1/11', '11/11', '25/12') OR
                 /* Easter Holidays */ D.[Date] IN (DATEADD(d,  1, [dbo].[GetEasterSunday](DATEPART(YEAR, D.[Date]))),
                                                    DATEADD(d, 39, [dbo].[GetEasterSunday](DATEPART(YEAR, D.[Date]))),
                                                    DATEADD(d, 50, [dbo].[GetEasterSunday](DATEPART(YEAR, D.[Date]))))
            THEN 0 ELSE 1 END AS [BusinessDay] 
     , D.[Date] AS [StandardDate]
  FROM ( SELECT CAST('1/1/1900' AS DATETIME) + CAST(CONCAT(I1.IDX, I2.IDX, I3.IDX, I4.IDX, I5.IDX) AS INTEGER) AS [Date] 
           FROM IDX_INIT I1
           CROSS JOIN IDX_INIT I2
           CROSS JOIN IDX_INIT I3
           CROSS JOIN IDX_INIT I4
           CROSS JOIN IDX_INIT I5 ) AS D
 WHERE D.[Date] < CAST('1/1/2050' AS DATETIME)
GO


USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_CASE_ATTENTE]
GO

INSERT INTO [dbo].[DIM_CASE_ATTENTE] ([CODE_SF], [LIBELLE]) VALUES 
(N'01', N'Client '),
(N'02', N'Partenaire'),
(N'03', N'Autres'),
(N'04', N'Services internes'),
(N'05', N'Services externes / Admnistration ')
GO

USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_CASE_CANAL_ORIGINE]
GO

INSERT INTO [dbo].[DIM_CASE_CANAL_ORIGINE] ([CODE_SF], [LIBELLE]) VALUES 
(N'01', N'Mobile/SMS')
,(N'02', N'Internet / Espace Client')
,(N'03', N'Courriers')
,(N'04', N'Appel entrant')
,(N'05', N'Appel sortant')
,(N'06', N'Fax')
,(N'07', N'Chat')
,(N'08', N'Réseaux sociaux')
,(N'09', N'Appel entrant transfert Telco')
,(N'10', N'Appel entrant (boutiques)')
,(N'11', N'Email')
,(N'12', N'Formulaire de demandes')
,(N'13', N'SFMC')
GO

USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_CASE_CANAL_REPONSE]
GO

INSERT INTO [dbo].[DIM_CASE_CANAL_REPONSE] ([CODE_SF], [LIBELLE]) VALUES 
(N'01', N'Mobile/SMS')
,(N'02', N'Internet / Espace Client')
,(N'03', N'Courriers')
,(N'04', N'Appel entrant')
,(N'05', N'Appel sortant')
,(N'06', N'Fax')
,(N'07', N'Chat')
,(N'08', N'Réseaux sociaux')
GO

USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_CASE_METIER]
GO

INSERT INTO [dbo].[DIM_CASE_METIER] ([CODE_SF], [LIBELLE]) VALUES 
(N'01', N'Gestion des comptes')
,(N'02', N'Moyens de paiement')
,(N'03', N'Engagement')
,(N'04', N'Back office Crédit')
,(N'05', N'Relations Client')
GO


USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_CASE_MOTIF]
GO

INSERT INTO [dbo].[DIM_CASE_MOTIF] ([CODE_SF], [LIBELLE]) VALUES 
(N'01', N'Favorable')
,(N'02', N'Défavorable')
GO

USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_CASE_NIVEAU]
GO

INSERT INTO [dbo].[DIM_CASE_NIVEAU] ([CODE_SF], [LIBELLE]) VALUES 
(N'01', N'1')
,(N'02', N'2')
,(N'03', N'2 DG')
,(N'04', N'3')
GO

USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_CASE_OBJET_PRINCIPAL]
GO

INSERT INTO [dbo].[DIM_CASE_OBJET_PRINCIPAL] ([CODE_SF], [LIBELLE]) VALUES (N'01', N'Information/conseil')
,(N'02', N'Qualité de l''offre/documentation contractuelle')
,(N'03', N'Dysfonctionnement site/mise à jour')
,(N'04', N'Accueil conseiller/méthode vente')
,(N'05', N'Remboursement cotisation')
,(N'06', N'Remboursement frais irrégularités/incidents')
,(N'07', N'Erreur de traitement')
,(N'08', N'Délai de traitement')
,(N'09', N'Inscription fichiers BDF (FCC, FICP, FNCI)')
,(N'10', N'Fraude/opération non autorisée')
,(N'11', N'Incidents sur compte/irrégularités')
,(N'12', N'Clôture du compte')
,(N'13', N'Mobilité bancaire')
,(N'14', N'Droit au compte/SBB/SAP')
,(N'15', N'Remboursement tarification')
,(N'16', N'Opposition')
,(N'17', N'Dysfonctionnements DAB')
,(N'18', N'Fiscalité/ transfert /clôture')
,(N'19', N'Mise en place/Déblocage des fonds/refus d''octroi de crédit')
,(N'20', N'Rupture de crédit ou d''autorisation de découvert')
,(N'21', N'Surendettement, demande de réaménagement de dettes')
GO

USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_CASE_OBJET_SECONDAIRE]
GO

INSERT INTO [dbo].[DIM_CASE_OBJET_SECONDAIRE] ([CODE_SF], [LIBELLE]) VALUES (N'01', N'Information/conseil')
,(N'02', N'Qualité de l''offre/documentation contractuelle')
,(N'03', N'Dysfonctionnement site/mise à jour')
,(N'04', N'Accueil conseiller/méthode vente')
,(N'05', N'Remboursement cotisation')
,(N'06', N'Remboursement frais irrégularités/incidents')
,(N'07', N'Erreur de traitement')
,(N'08', N'Délai de traitement')
,(N'09', N'Inscription fichiers BDF (FCC, FICP, FNCI)')
,(N'10', N'Fraude/opération non autorisée')
,(N'11', N'Incidents sur compte/irrégularités')
,(N'12', N'Clôture du compte')
,(N'13', N'Mobilité bancaire')
,(N'14', N'Droit au compte/SBB/SAP')
,(N'15', N'Remboursement tarification')
,(N'16', N'Opposition')
,(N'17', N'Dysfonctionnements DAB')
,(N'18', N'Fiscalité/ transfert /clôture')
,(N'19', N'Mise en place/Déblocage des fonds/refus d''octroi de crédit')
,(N'20', N'Rupture de crédit ou d''autorisation de découvert')
,(N'21', N'Surendettement, demande de réaménagement de dettes')
GO

USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_CASE_ORIGINE]
GO

INSERT INTO [dbo].[DIM_CASE_ORIGINE] ([CODE_SF], [LIBELLE]) VALUES (N'01', N'Client incompréhension')
,(N'02', N'Banque dysfonct informatique')
,(N'03', N'Banque erreur opérationnelle')
,(N'04', N'Banque délai')
,(N'05', N'Réseau erreur')
,(N'06', N'Réseau retard')
,(N'07', N'Presta erreur')
,(N'08', N'Presta retard')
,(N'09', N'Evènement externe')
GO

USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_CASE_PLAN_ACTION]
GO

INSERT INTO [dbo].[DIM_CASE_PLAN_ACTION] ([CODE_SF], [LIBELLE]) VALUES (N'01', N'Clôture client côté R')
,(N'02', N'Interdiction bancaire')
,(N'03', N'Clôture compte débiteur non régularisé')
,(N'04', N'Clôture interne banque')
,(N'05', N'Transfert judiciaire')
,(N'06', N'FICP')
,(N'07', N'Retour prestataire traitement judiciaire')
,(N'08', N'Compte créditeur avec ou sans prêt ')
,(N'09', N'Compte clos avec ou sans prêt')
,(N'10', N'Compte débiteur avec ou sans prêt')
,(N'11', N'Compte débiteur <100€ avec ou sans prêt')
,(N'12', N'Contrôle LCB-FT')
GO

USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_CASE_PRIORITE]
GO

INSERT INTO [dbo].[DIM_CASE_PRIORITE] ([CODE_SF], [LIBELLE]) VALUES (N'01', N'Normal')
,(N'02', N'Urgent')
,(N'03', N'Très urgent')
GO

USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_CASE_PROCESSUS]
GO

INSERT INTO [dbo].[DIM_CASE_PROCESSUS] ([CODE_SF], [LIBELLE]) VALUES (N'01', N'Souscription')
,(N'02', N'Gestion')
,(N'03', N'Clôture')
GO

USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_CASE_QUALIF_REP1]
GO

INSERT INTO [dbo].[DIM_CASE_QUALIF_REP1] ([CODE_SF], [LIBELLE]) VALUES (N'01', N'Réponse positive')
,(N'02', N'Réponse négative')
,(N'03', N'Accord transactionnel')
GO

USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_CASE_QUALIF_REP2]
GO

INSERT INTO [dbo].[DIM_CASE_QUALIF_REP2] ([CODE_SF], [LIBELLE]) VALUES (N'01', N'Réponse positive')
,(N'02', N'Réponse négative')
,(N'03', N'Accord transactionnel')
GO

USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_CASE_STATUS]
GO

INSERT INTO [dbo].[DIM_CASE_STATUS] ([CODE_SF], [LIBELLE]) VALUES (N'01', N'A faire')
,(N'02', N'En cours')
,(N'03', N'Fermée')
,(N'04', N'Abandonnée')
,(N'05', N'A valider')
,(N'06', N'En attente')
,(N'07', N'Contrôle LCB-FT terminé')
,(N'08', N'Contacts Argumenté positif')
,(N'09', N'Contacts Argumentés négatifs')
,(N'10', N'Contacts Non Argumentés')
,(N'11', N'Stop ')
GO

USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_CASE_TYPE]
GO

SET IDENTITY_INSERT [dbo].[DIM_CASE_TYPE] ON 

INSERT [dbo].[DIM_CASE_TYPE] ([PK_ID], [CODE_SF], [LIBELLE], [PARENT_ID]) VALUES 

(1, N'01', N'Site Internet/Appli/SVI', NULL)
,(2, N'02', N'Compte', NULL)
,(3, N'03', N'Moyens de paiement', NULL)
,(4, N'04', N'Distributeur billets', NULL)
,(5, N'05', N'Epargne', NULL)
,(6, N'06', N'Crédit consommation', NULL)
,(7, N'07', N'Banque au quotidien', NULL)
,(8, N'08', N'Crédits conso', NULL)
,(9, N'09', N'Demander en interne ', NULL)
,(10, N'10', N'Accessibilité', NULL)
,(11, N'11', N'Assurance', NULL)
,(12, N'12', N'Carte et Mobile', NULL)
,(13, N'13', N'Chèque', NULL)
,(14, N'14', N'Souscription', NULL)
,(15, N'15', N'Virements', NULL)
,(16, N'16', N'Fin de relation', NULL)
,(17, N'17', N'Enregistrer', NULL)
,(18, N'18', N'Exécuter', NULL)
,(19, N'19', N'Faire un courrier', NULL)
,(20, N'20', N'Mise à jour client', NULL)
,(21, N'21', N'Modifier', NULL)
,(22, N'22', N'Régulariser', NULL)
,(23, N'23', N'Vérifier', NULL)
,(24, N'24', N'Analyse Souscription Compte', NULL)
,(25, N'25', N'Analyse premier versement online', NULL)
,(26, N'26', N'Analyse premier versement boutique', NULL)
,(27, N'27', N'Appel sortant Marketing 1', NULL)
,(28, N'28', N'Appel Sortant Marketing 2 ', NULL)
,(29, N'29', N'Call Back', NULL)
,(30, N'30', N'Espèces', NULL)
,(31, N'31', N'Prélèvement', NULL)
,(32, N'32', N'Analyse Souscription Crédit ', NULL)
,(33, N'001', N'Disponibilité du site/Connexion', N'01')
,(34, N'002', N'Fonctionnement/Navigation', N'01')
,(35, N'003', N'Souscription/refus souscription', N'02')
,(36, N'004', N'Tarification compte', N'02')
,(37, N'005', N'Fonctionnement/vie du compte/clôture', N'02')
,(38, N'006', N'Facilité caisse', N'02')
,(39, N'007', N'Mobile', N'03')
,(40, N'008', N'Carte bancaire', N'03')
,(41, N'009', N'Chèque/chéquier', N'03')
,(42, N'010', N'Virement', N'03')
,(43, N'010', N'Virement', N'15')
,(44, N'011', N'Prélèvement', N'03')
,(45, N'011', N'Prélèvement', N'31')
,(46, N'012', N'Espèces', N'03')
,(47, N'013', N'Assurance moyens de paiement', N'03')
,(48, N'014', N'Retrait DAB', N'04')
,(49, N'015', N'Carte avalée ou refusée', N'04')
,(50, N'016', N'Billets non délivrés', N'04')
,(51, N'017', N'Compte sur livret', N'05')
,(52, N'018', N'Livret A/LDD/Livret jeune/LEP', N'05')
,(53, N'019', N'PEL/CEL', N'05')
,(54, N'020', N'Compte Titres / PEA', N'05')
,(55, N'021', N'OPCVM', N'05')
,(56, N'022', N'Autres placements', N'05')
,(57, N'023', N'Souscription', N'06')
,(58, N'024', N'Fonctionnement/incident de remboursement', N'06')
,(59, N'025', N'Remboursement anticipé', N'06')
,(60, N'026', N'Assurance emprunteur', N'06')
,(61, N'027', N'Demande de dérogation', N'07')
,(62, N'027', N'Demande de dérogation', N'08')
,(63, N'027', N'Demande de dérogation', N'05')
,(64, N'028', N'Infos refus', N'07')
,(65, N'028', N'Infos refus', N'08')
,(66, N'028', N'Infos refus', N'05')
,(67, N'029', N'Souscription outil', N'07')
,(68, N'029', N'Souscription outil', N'08')
,(69, N'029', N'Souscription outil', N'05')
,(70, N'030', N'Souscription produit', N'07')
,(71, N'030', N'Souscription produit', N'08')
,(72, N'030', N'Souscription produit', N'05')
,(73, N'031', N'Levée de score', N'08')
,(74, N'032', N'Offre', N'08')
,(75, N'032', N'Offre', N'05')
,(76, N'033', N'Geste commercial', N'09')
,(77, N'034', N'Rétrocession', N'09')
,(78, N'035', N'Paramétrer les accès mobile', N'10')
,(79, N'036', N'Paramétrer les accès internet', N'10')
,(80, N'037', N'Pouvoir contacter ma banque', N'10')
,(81, N'038', N'Consultation', N'10')
,(82, N'039', N'Sinistre', N'11')
,(83, N'040', N'Modification contrat', N'11')
,(84, N'041', N'Ajout assuré', N'11')
,(85, N'042', N'Intervention', N'12')
,(86, N'043', N'Contestation', N'12')
,(87, N'044', N'Opposition', N'12')
,(88, N'044', N'Opposition', N'31')
,(89, N'045', N'Activer/Désactiver paiement mobile', N'12')
,(90, N'046', N'Activer/Désactiver option CB', N'12')
,(91, N'047', N'Changement type de carte', N'12')
,(92, N'048', N'Demande d''information', N'12')
,(93, N'048', N'Demande d''information', N'13')
,(94, N'048', N'Demande d''information', N'02')
,(95, N'048', N'Demande d''information', N'08')
,(96, N'048', N'Demande d''information', N'14')
,(97, N'048', N'Demande d''information', N'15')
,(98, N'048', N'Demande d''information', N'31')
,(99, N'049', N'Chèque de banque', N'13')
,(100, N'050', N'Opposition chèque', N'13')
,(101, N'051', N'Anomalie', N'13')
,(102, N'051', N'Anomalie', N'15')
,(103, N'051', N'Anomalie', N'31')
,(104, N'052', N'Commande chéquier/BRD', N'13')
,(105, N'053', N'Remise chèque', N'13')
,(106, N'054', N'Régularisation chèque', N'13')
,(107, N'055', N'Ajout intervenant', N'02')
,(108, N'056', N'Mise à jour Clients/Comptes', N'02')
,(109, N'057', N'Clôture', N'02')
,(110, N'057', N'Clôture', N'05')
,(111, N'058', N'Demande envoi documents', N'02')
,(112, N'059', N'Succession', N'02')
,(113, N'060', N'PND/NPAI', N'02')
,(114, N'061', N'Juri01', N'02')
,(115, N'062', N'Juri12', N'02')
,(116, N'063', N'Dénonciation renonciation convention', N'02')
,(117, N'064', N'Tutelle', N'02')
,(118, N'065', N'MAD Promesse de versement', N'02')
,(119, N'066', N'Etude plafond carte', N'02')
,(120, N'067', N'Avis de sort', N'02')
,(121, N'069', N'Action sur débiteur', N'02')
,(122, N'070', N'Propos FDC/CB', N'02')
,(123, N'071', N'Virement/Prélèvement surendettement', N'02')
,(124, N'072', N'Contentieux', N'02')
,(125, N'073', N'Franfinance ', N'02')
,(126, N'074', N'Commission de surendettement', N'02')
,(127, N'075', N'Désarchivage', N'02')
,(128, N'076', N'Certificat non paiement', N'02')
,(129, N'077', N'BDF', N'02')
,(130, N'078', N'Levée BDF', N'02')
,(131, N'079', N'Restitution chq impayé', N'02')
,(132, N'080', N'Blocage provision chq', N'02')
,(133, N'081', N'Ouverture manuelle', N'08')
,(134, N'082', N'Rétractation', N'08')
,(135, N'083', N'RAT/RAP par prélévement', N'08')
,(136, N'084', N'Sinistre assurance', N'08')
,(137, N'085', N'Repos échéance', N'08')
,(138, N'086', N'Util. / augment. crédit de poche ', N'08')
,(139, N'087', N'SAV Crédit amortissable', N'08')
,(140, N'088', N'SAV Crédit de poche', N'08')
,(141, N'089', N'Mes projets', N'05')
,(142, N'090', N'Virement étranger Virement hors SEPA', N'15')
,(143, N'091', N'Clôture initiative client', N'02')
,(144, N'091', N'Clôture initiative client', N'16')
,(145, N'092', N'Interdiction bancaire', N'02')
,(146, N'093', N'Clôture initiative banque', N'02')
,(147, N'094', N'Pré-contentieux', N'02')
,(148, N'095', N'FICP', N'02')
,(149, N'096', N'Surendettement - Compte créditeur', N'02')
,(150, N'097', N'Surendettement - Compte clos', N'02')
,(151, N'098', N'Surendettement - Compte débiteur', N'02')
,(152, N'099', N'Surendettement - Compte débiteur < 100€', N'02')
,(153, N'100', N'Blocage moyens de paiement', N'17')
,(154, N'100', N'Blocage moyens de paiement', N'18')
,(155, N'100', N'Blocage moyens de paiement', N'19')
,(156, N'100', N'Blocage moyens de paiement', N'20')
,(157, N'100', N'Blocage moyens de paiement', N'21')
,(158, N'100', N'Blocage moyens de paiement', N'22')
,(159, N'100', N'Blocage moyens de paiement', N'23')
,(160, N'101', N'Chèque impayé', N'17')
,(161, N'101', N'Chèque impayé', N'18')
,(162, N'101', N'Chèque impayé', N'19')
,(163, N'101', N'Chèque impayé', N'20')
,(164, N'101', N'Chèque impayé', N'21')
,(165, N'101', N'Chèque impayé', N'22')
,(166, N'101', N'Chèque impayé', N'23')
,(167, N'102', N'Clôture CAV EFFICO', N'17')
,(168, N'102', N'Clôture CAV EFFICO', N'18')
,(169, N'102', N'Clôture CAV EFFICO', N'19')
,(170, N'102', N'Clôture CAV EFFICO', N'20')
,(171, N'102', N'Clôture CAV EFFICO', N'21')
,(172, N'102', N'Clôture CAV EFFICO', N'22')
,(173, N'102', N'Clôture CAV EFFICO', N'23')
,(174, N'103', N'Clôture de compte', N'17')
,(175, N'103', N'Clôture de compte', N'18')
,(176, N'103', N'Clôture de compte', N'19')
,(177, N'103', N'Clôture de compte', N'20')
,(178, N'103', N'Clôture de compte', N'21')
,(179, N'103', N'Clôture de compte', N'22')
,(180, N'103', N'Clôture de compte', N'23')
,(181, N'104', N'Clôture GRP', N'17')
,(182, N'104', N'Clôture GRP', N'18')
,(183, N'104', N'Clôture GRP', N'19')
,(184, N'104', N'Clôture GRP', N'20')
,(185, N'104', N'Clôture GRP', N'21')
,(186, N'104', N'Clôture GRP', N'22')
,(187, N'104', N'Clôture GRP', N'23')
,(188, N'106', N'Cotation', N'17')
,(189, N'106', N'Cotation', N'18')
,(190, N'106', N'Cotation', N'19')
,(191, N'106', N'Cotation', N'20')
,(192, N'106', N'Cotation', N'21')
,(193, N'106', N'Cotation', N'22')
,(194, N'106', N'Cotation', N'23')
,(195, N'107', N'Courrier de clôture', N'17')
,(196, N'107', N'Courrier de clôture', N'18')
,(197, N'107', N'Courrier de clôture', N'19')
,(198, N'107', N'Courrier de clôture', N'20')
,(199, N'107', N'Courrier de clôture', N'21')
,(200, N'107', N'Courrier de clôture', N'22')
,(201, N'107', N'Courrier de clôture', N'23')
,(202, N'108', N'Déclaration BDF', N'17')
,(203, N'108', N'Déclaration BDF', N'18')
,(204, N'108', N'Déclaration BDF', N'19')
,(205, N'108', N'Déclaration BDF', N'20')
,(206, N'108', N'Déclaration BDF', N'21')
,(207, N'108', N'Déclaration BDF', N'22')
,(208, N'108', N'Déclaration BDF', N'23')
,(209, N'109', N'Déclaration effective FICP', N'17')
,(210, N'109', N'Déclaration effective FICP', N'18')
,(211, N'109', N'Déclaration effective FICP', N'19')
,(212, N'109', N'Déclaration effective FICP', N'20')
,(213, N'109', N'Déclaration effective FICP', N'21')
,(214, N'109', N'Déclaration effective FICP', N'22')
,(215, N'109', N'Déclaration effective FICP', N'23')
,(216, N'110', N'Désarchiver dossier', N'17')
,(217, N'110', N'Désarchiver dossier', N'18')
,(218, N'110', N'Désarchiver dossier', N'19')
,(219, N'110', N'Désarchiver dossier', N'20')
,(220, N'110', N'Désarchiver dossier', N'21')
,(221, N'110', N'Désarchiver dossier', N'22')
,(222, N'110', N'Désarchiver dossier', N'23')
,(223, N'111', N'Entrée module CTX', N'17')
,(224, N'111', N'Entrée module CTX', N'18')
,(225, N'111', N'Entrée module CTX', N'19')
,(226, N'111', N'Entrée module CTX', N'20')
,(227, N'111', N'Entrée module CTX', N'21')
,(228, N'111', N'Entrée module CTX', N'22')
,(229, N'111', N'Entrée module CTX', N'23')
,(230, N'112', N'Envoi document prestataire', N'17')
,(231, N'112', N'Envoi document prestataire', N'18')
,(232, N'112', N'Envoi document prestataire', N'19')
,(233, N'112', N'Envoi document prestataire', N'20')
,(234, N'112', N'Envoi document prestataire', N'21')
,(235, N'112', N'Envoi document prestataire', N'22')
,(236, N'112', N'Envoi document prestataire', N'23')
,(237, N'113', N'Facturation', N'17')
,(238, N'113', N'Facturation', N'18')
,(239, N'113', N'Facturation', N'19')
,(240, N'113', N'Facturation', N'20')
,(241, N'113', N'Facturation', N'21')
,(242, N'113', N'Facturation', N'22')
,(243, N'113', N'Facturation', N'23')
,(244, N'114', N'Facturation à supprimer', N'17')
,(245, N'114', N'Facturation à supprimer', N'18')
,(246, N'114', N'Facturation à supprimer', N'19')
,(247, N'114', N'Facturation à supprimer', N'20')
,(248, N'114', N'Facturation à supprimer', N'21')
,(249, N'114', N'Facturation à supprimer', N'22')
,(250, N'114', N'Facturation à supprimer', N'23')
,(251, N'115', N'Facturation à remettre en place', N'17')
,(252, N'115', N'Facturation à remettre en place', N'18')
,(253, N'115', N'Facturation à remettre en place', N'19')
,(254, N'115', N'Facturation à remettre en place', N'20')
,(255, N'115', N'Facturation à remettre en place', N'21')
,(256, N'115', N'Facturation à remettre en place', N'22')
,(257, N'115', N'Facturation à remettre en place', N'23')
,(258, N'116', N'FCI', N'17')
,(259, N'116', N'FCI', N'18')
,(260, N'116', N'FCI', N'19')
,(261, N'116', N'FCI', N'20')
,(262, N'116', N'FCI', N'21')
,(263, N'116', N'FCI', N'22')
,(264, N'116', N'FCI', N'23')
,(265, N'117', N'Information préalable au FICP', N'17')
,(266, N'117', N'Information préalable au FICP', N'18')
,(267, N'117', N'Information préalable au FICP', N'19')
,(268, N'117', N'Information préalable au FICP', N'20')
,(269, N'117', N'Information préalable au FICP', N'21')
,(270, N'117', N'Information préalable au FICP', N'22')
,(271, N'117', N'Information préalable au FICP', N'23')
,(272, N'118', N'Inscription FICP', N'17')
,(273, N'118', N'Inscription FICP', N'18')
,(274, N'118', N'Inscription FICP', N'19')
,(275, N'118', N'Inscription FICP', N'20')
,(276, N'118', N'Inscription FICP', N'21')
,(277, N'118', N'Inscription FICP', N'22')
,(278, N'118', N'Inscription FICP', N'23')
,(279, N'120', N'Levée interdiction bancaire', N'17')
,(280, N'120', N'Levée interdiction bancaire', N'18')
,(281, N'120', N'Levée interdiction bancaire', N'19')
,(282, N'120', N'Levée interdiction bancaire', N'20')
,(283, N'120', N'Levée interdiction bancaire', N'21')
,(284, N'120', N'Levée interdiction bancaire', N'22')
,(285, N'120', N'Levée interdiction bancaire', N'23')
,(286, N'121', N'Notification de recevabilité', N'17')
,(287, N'121', N'Notification de recevabilité', N'18')
,(288, N'121', N'Notification de recevabilité', N'19')
,(289, N'121', N'Notification de recevabilité', N'20')
,(290, N'121', N'Notification de recevabilité', N'21')
,(291, N'121', N'Notification de recevabilité', N'22')
,(292, N'121', N'Notification de recevabilité', N'23')
,(293, N'122', N'OD', N'17')
,(294, N'122', N'OD', N'18')
,(295, N'122', N'OD', N'19')
,(296, N'122', N'OD', N'20')
,(297, N'122', N'OD', N'21')
,(298, N'122', N'OD', N'22')
,(299, N'122', N'OD', N'23')
,(300, N'123', N'Pré-clôture EFFICO', N'17')
,(301, N'123', N'Pré-clôture EFFICO', N'18')
,(302, N'123', N'Pré-clôture EFFICO', N'19')
,(303, N'123', N'Pré-clôture EFFICO', N'20')
,(304, N'123', N'Pré-clôture EFFICO', N'21')
,(305, N'123', N'Pré-clôture EFFICO', N'22')
,(306, N'123', N'Pré-clôture EFFICO', N'23')
,(307, N'124', N'Pré-clôture GRP', N'17')
,(308, N'124', N'Pré-clôture GRP', N'18')
,(309, N'124', N'Pré-clôture GRP', N'19')
,(310, N'124', N'Pré-clôture GRP', N'20')
,(311, N'124', N'Pré-clôture GRP', N'21')
,(312, N'124', N'Pré-clôture GRP', N'22')
,(313, N'124', N'Pré-clôture GRP', N'23')
,(314, N'125', N'Priorisation clôture', N'17')
,(315, N'125', N'Priorisation clôture', N'18')
,(316, N'125', N'Priorisation clôture', N'19')
,(317, N'125', N'Priorisation clôture', N'20')
,(318, N'125', N'Priorisation clôture', N'21')
,(319, N'125', N'Priorisation clôture', N'22')
,(320, N'125', N'Priorisation clôture', N'23')
,(321, N'126', N'Réarchivage dossier', N'17')
,(322, N'126', N'Réarchivage dossier', N'18')
,(323, N'126', N'Réarchivage dossier', N'19')
,(324, N'126', N'Réarchivage dossier', N'20')
,(325, N'126', N'Réarchivage dossier', N'21')
,(326, N'126', N'Réarchivage dossier', N'22')
,(327, N'126', N'Réarchivage dossier', N'23')
,(328, N'127', N'Recevabilité client et déclaration', N'17')
,(329, N'127', N'Recevabilité client et déclaration', N'18')
,(330, N'127', N'Recevabilité client et déclaration', N'19')
,(331, N'127', N'Recevabilité client et déclaration', N'20')
,(332, N'127', N'Recevabilité client et déclaration', N'21')
,(333, N'127', N'Recevabilité client et déclaration', N'22')
,(334, N'127', N'Recevabilité client et déclaration', N'23')
,(335, N'128', N'Recevabilité client et déclaration BDF', N'17')
,(336, N'128', N'Recevabilité client et déclaration BDF', N'18')
,(337, N'128', N'Recevabilité client et déclaration BDF', N'19')
,(338, N'128', N'Recevabilité client et déclaration BDF', N'20')
,(339, N'128', N'Recevabilité client et déclaration BDF', N'21')
,(340, N'128', N'Recevabilité client et déclaration BDF', N'22')
,(341, N'128', N'Recevabilité client et déclaration BDF', N'23')
,(342, N'129', N'Relevé de compte', N'17')
,(343, N'129', N'Relevé de compte', N'18')
,(344, N'129', N'Relevé de compte', N'19')
,(345, N'129', N'Relevé de compte', N'20')
,(346, N'129', N'Relevé de compte', N'21')
,(347, N'129', N'Relevé de compte', N'22')
,(348, N'129', N'Relevé de compte', N'23')
,(349, N'130', N'Résiliation Compléo', N'17')
,(350, N'130', N'Résiliation Compléo', N'18')
,(351, N'130', N'Résiliation Compléo', N'19')
,(352, N'130', N'Résiliation Compléo', N'20')
,(353, N'130', N'Résiliation Compléo', N'21')
,(354, N'130', N'Résiliation Compléo', N'22')
,(355, N'130', N'Résiliation Compléo', N'23')
,(356, N'131', N'SC136', N'17')
,(357, N'131', N'SC136', N'18')
,(358, N'131', N'SC136', N'19')
,(359, N'131', N'SC136', N'20')
,(360, N'131', N'SC136', N'21')
,(361, N'131', N'SC136', N'22')
,(362, N'131', N'SC136', N'23')
,(363, N'132', N'SC146 / SC107 / SC 182', N'17')
,(364, N'132', N'SC146 / SC107 / SC 182', N'18')
,(365, N'132', N'SC146 / SC107 / SC 182', N'19')
,(366, N'132', N'SC146 / SC107 / SC 182', N'20')
,(367, N'132', N'SC146 / SC107 / SC 182', N'21')
,(368, N'132', N'SC146 / SC107 / SC 182', N'22')
,(369, N'132', N'SC146 / SC107 / SC 182', N'23')
,(370, N'133', N'Statut crédit', N'17')
,(371, N'133', N'Statut crédit', N'18')
,(372, N'133', N'Statut crédit', N'19')
,(373, N'133', N'Statut crédit', N'20')
,(374, N'133', N'Statut crédit', N'21')
,(375, N'133', N'Statut crédit', N'22')
,(376, N'133', N'Statut crédit', N'23')
,(377, N'134', N'Suppression FDC', N'17')
,(378, N'134', N'Suppression FDC', N'18')
,(379, N'134', N'Suppression FDC', N'19')
,(380, N'134', N'Suppression FDC', N'20')
,(381, N'134', N'Suppression FDC', N'21')
,(382, N'134', N'Suppression FDC', N'22')
,(383, N'134', N'Suppression FDC', N'23')
,(384, N'135', N'Supprimer les relevés', N'17')
,(385, N'135', N'Supprimer les relevés', N'18')
,(386, N'135', N'Supprimer les relevés', N'19')
,(387, N'135', N'Supprimer les relevés', N'20')
,(388, N'135', N'Supprimer les relevés', N'21')
,(389, N'135', N'Supprimer les relevés', N'22')
,(390, N'135', N'Supprimer les relevés', N'23')
,(391, N'136', N'Suspension Compléo', N'17')
,(392, N'136', N'Suspension Compléo', N'18')
,(393, N'136', N'Suspension Compléo', N'19')
,(394, N'136', N'Suspension Compléo', N'20')
,(395, N'136', N'Suspension Compléo', N'21')
,(396, N'136', N'Suspension Compléo', N'22')
,(397, N'136', N'Suspension Compléo', N'23')
,(398, N'137', N'Synapse', N'17')
,(399, N'137', N'Synapse', N'18')
,(400, N'137', N'Synapse', N'19')
,(401, N'137', N'Synapse', N'20')
,(402, N'137', N'Synapse', N'21')
,(403, N'137', N'Synapse', N'22')
,(404, N'137', N'Synapse', N'23')
,(405, N'138', N'Vérifier la clôture de compte', N'17')
,(406, N'138', N'Vérifier la clôture de compte', N'18')
,(407, N'138', N'Vérifier la clôture de compte', N'19')
,(408, N'138', N'Vérifier la clôture de compte', N'20')
,(409, N'138', N'Vérifier la clôture de compte', N'21')
,(410, N'138', N'Vérifier la clôture de compte', N'22')
,(411, N'138', N'Vérifier la clôture de compte', N'23')
,(412, N'139', N'LCB-FT à contrôler', N'24')
,(413, N'140', N'Ouverture en dégradée', N'18')
,(414, N'141', N'Information prospect', N'19')
,(415, N'142', N'1ère analyse PJ', N'24')
,(416, N'143', N'1ère analyse PJ+BDF', N'24')
,(417, N'144', N'1ère analyse BDF', N'24')
,(418, N'144', N'1ère analyse BDF', N'32')
,(419, N'145', N'Nième analyse PJ', N'24')
,(420, N'146', N'Nième analyse PJ+BDF', N'24')
,(421, N'147', N'Nième analyse BDF', N'24')
,(422, N'147', N'Nième analyse BDF', N'32')
,(423, N'148', N'1ère analyse premier versement online', N'25')
,(424, N'149', N'Nième analyse premier versement online', N'25')
,(425, N'150', N'1ère analyse premier versement boutique', N'26')
,(426, N'151', N'Nième analyse premier versement boutique', N'26')
,(427, N'152', N'Réclamation', N'01')
,(428, N'152', N'Réclamation', N'02')
,(429, N'152', N'Réclamation', N'03')
,(430, N'152', N'Réclamation', N'04')
,(431, N'152', N'Réclamation', N'05')
,(432, N'152', N'Réclamation', N'06')
,(433, N'153', N'SV01_SV02_RELANCE_INDICATION_DECOUVERTE', N'27')
,(434, N'153', N'SV01_SV02_RELANCE_INDICATION_DECOUVERTE', N'29')
,(435, N'154', N'SV03_ELABORATION_PROPOSITION', N'27')
,(436, N'155', N'SV06_RELANCE_SOUSC_DOSSIER_INCOMPLET', N'27')
,(437, N'156', N'SV07_RELANCE_SOUSC_DOSSIER_VALIDE', N'27')
,(438, N'157', N'RELANCE', N'28')
,(439, N'158', N'SV06_RELANCE', N'28')
,(440, N'159', N'Domiciliation bancaire', N'02')
,(441, N'160', N'Chèque EIC', N'13')
,(442, N'161', N'Copie Chèque', N'13')
,(443, N'162', N'Suivi des opérations', N'13')
,(444, N'163', N'Analyse octroi moyens de paiement', N'02')
,(445, N'164', N'Suivi contestation', N'12')
,(446, N'165', N'Résiliation', N'12')
,(447, N'166', N'Mandat Cash', N'30')
,(448, N'167', N'Encaissement chèque étranger', N'13')
,(449, N'168', N'Clôture initiative banque SECFI', N'02')
,(450, N'169', N'Versement client', N'17')
,(451, N'169', N'Versement client', N'18')
,(452, N'169', N'Versement client', N'19')
,(453, N'169', N'Versement client', N'20')
,(454, N'169', N'Versement client', N'21')
,(455, N'169', N'Versement client', N'22')
,(456, N'169', N'Versement client', N'23')
,(457, N'170', N'Clotûre SECFI', N'17')
,(458, N'170', N'Clotûre SECFI', N'18')
,(459, N'170', N'Clotûre SECFI', N'19')
,(460, N'170', N'Clotûre SECFI', N'20')
,(461, N'170', N'Clotûre SECFI', N'21')
,(462, N'170', N'Clotûre SECFI', N'22')
,(463, N'170', N'Clotûre SECFI', N'23')
,(464, N'171', N'Carte en tirage abusif', N'12')
,(465, N'172', N'Information préalable à l''opposition carte tirage abusif', N'17')
,(466, N'172', N'Information préalable à l''opposition carte tirage abusif', N'18')
,(467, N'172', N'Information préalable à l''opposition carte tirage abusif', N'19')
,(468, N'172', N'Information préalable à l''opposition carte tirage abusif', N'20')
,(469, N'172', N'Information préalable à l''opposition carte tirage abusif', N'21')
,(470, N'172', N'Information préalable à l''opposition carte tirage abusif', N'22')
,(471, N'172', N'Information préalable à l''opposition carte tirage abusif', N'23')
,(472, N'173', N'Déclaration incident carte tirage abusif', N'17')
,(473, N'173', N'Déclaration incident carte tirage abusif', N'18')
,(474, N'173', N'Déclaration incident carte tirage abusif', N'19')
,(475, N'173', N'Déclaration incident carte tirage abusif', N'20')
,(476, N'173', N'Déclaration incident carte tirage abusif', N'21')
,(477, N'173', N'Déclaration incident carte tirage abusif', N'22')
,(478, N'173', N'Déclaration incident carte tirage abusif', N'23')
,(479, N'174', N'1ère analyse dossier', N'32')
,(480, N'175', N'1ère analyse dossier + BDF', N'32')
,(481, N'176', N'Nième analyse dossier', N'32')
,(482, N'177', N'Nième analyse dossier+BDF', N'32')
,(483, N'178', N'Annulation', N'08')
,(484, N'179', N'Déblocage des fonds', N'08')
,(485, N'180', N'RAT/RAP spécial/Chèque', N'08')
,(486, N'181', N'Demande envoi documents', N'08')
,(487, N'182', N'Mise à jour Crédit', N'08')
,(488, N'183', N'Report/Evolutivité SAV', N'08')
,(489, N'184', N'Succession', N'08')
,(490, N'185', N'Assurance Crédit conso', N'08')
,(491, N'186', N'Mobilité bancaire', N'08')
,(492, N'187', N'Suivi dossier Réclamation', N'01')
,(493, N'187', N'Suivi dossier Réclamation', N'02')
,(494, N'187', N'Suivi dossier Réclamation', N'03')
,(495, N'187', N'Suivi dossier Réclamation', N'04')
,(496, N'187', N'Suivi dossier Réclamation', N'05')
,(497, N'187', N'Suivi dossier Réclamation', N'06')

GO
SET IDENTITY_INSERT [dbo].[DIM_CASE_TYPE] OFF
GO

USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_CASE_TYPE_CLI]
GO

INSERT INTO [dbo].[DIM_CASE_TYPE_CLI] ([CODE_SF], [LIBELLE]) VALUES (N'01', N'Particulier')
,(N'02', N'Professionnel Personne Physique')
,(N'03', N'Personne Morale')
GO

USE [$(DataFactoryDatabaseName)]
GO

truncate table [dbo].[DIM_PRODUIT_OFFRE]
GO

INSERT INTO [dbo].[DIM_PRODUIT_OFFRE] ([CODE_FE], [LIBELLE], [PARENT_ID]) VALUES (N'OC80', N'Compte bancaire', NULL)

,(N'L80', N'Livret', NULL)

,(N'CB80', N'Carte bancaire', NULL)

,(N'C80', N'Compte bancaire Orange Bank', N'OC80')

,(N'OC1', N'Carte Visa Orange Bank', N'OC80')

,(N'OC1', N'Carte Visa Orange Bank', N'CB80')

,(N'L80', N'Compte sur livret Orange Bank', N'L80')
GO

USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_CAT_SEG_GEOLIFE]
GO

INSERT INTO [dbo].[DIM_CAT_SEG_GEOLIFE]
           ([LIB_CAT_SEG_GEO])
     VALUES
           ('Urbain')
		   ,('Péri-Urbain')
		   ,('Rural')
		   ,('Autres')
GO


USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_SEG_GEOLIFE]
GO

  INSERT INTO [dbo].[DIM_SEG_GEOLIFE] ([LIB_SEG_GEO],[ID_CAT_SEG_GEO])
  VALUES 
  ('1 - urbain dynamique',1),
('urbain dynamique - 1',1),
('2 - urbain familial aise',1),
('urbain familial aise - 2',1),
('3 - urbain classe moyenne',1),
('urbain classe moyenne - 3',1),
('4 - populaire',1),
('populaire - 4',1),
('5 - urbain defavorise',1),
('urbain defavorise - 5',1),
('6 - periurbain en croissance',2),
('periurbain en croissance - 6',2),
('7 - pavillonnaire familial aise',2),
('pavillonnaire familial aise - 7',2),
('8 - rural dynamique',3),
('rural dynamique - 8',3),
('9 - rural ouvrier',3),
('rural ouvrier - 9',3),
('10 - rural traditionnel',3),
('rural traditionnel - 10',3),
('residence secondaire - 11',4),
(NULL,4)

GO

USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_OPT_DEB]
GO

INSERT INTO [dbo].[DIM_OPT_DEB]
           ([COD_OPT_DEB]
           ,[ABR_OPT_DEB]
           ,[LIB_OPT_DEB])
     VALUES
           ('D','DIFFERE','DEBIT DIFFERE')
		   ,('I','IMMEDIAT','DEBIT IMMEDIAT')
		   ,('1','SUR DIFF M+1','SUR DIFFÉRÉ M+1')
		   ,('2','SUR DIFF M+2','SUR DIFFÉRÉ M+2')
		   ,('3','SUR DIFF M+3','SUR DIFFÉRÉ M+3')
GO

USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_CSP_NIV1]
GO

INSERT INTO [dbo].[DIM_CSP_NIV1] ([COD_CSP_NIV1], [LIB_CSP_NIV1]) VALUES 
(N'01', N'Agriculteurs exploitants')
,(N'02', N'Artisans, commerçants et chefs d''entreprise')
,(N'03', N'Cadres et professions intellectuelles supérieures')
,(N'04', N'Professions intermédiaires')
,(N'05', N'Employés')
,(N'06', N'Ouvriers')
,(N'07', N'Retraités')
,(N'08', N'Autres personnes sans activité professionnelle')
GO

USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[DIM_CSP_NIV3]
GO


INSERT INTO [dbo].[DIM_CSP_NIV3] ([COD_CSP_NIV3], [LIB_CSP_NIV3], [COD_CSP_NIV1]) VALUES 
(N'01', N'Agriculteurs sur petite exploitation',N'01')
,(N'02', N'Agriculteurs sur moyenne exploitation',N'01')
,(N'03', N'Agriculteurs sur grande exploitation',N'01') 
,(N'04', N'Artisans',N'02')
,(N'05', N'Commerçants et assimilés',N'02')
,(N'06', N'Chefs d''entreprise de 10 salariés ou plus',N'02') 
,(N'07', N'Professions libérales',N'03') 
,(N'08', N'Cadres de la fonction publique',N'03')
,(N'09', N'Professeurs, professions scientifiques',N'03')
,(N'10', N'Professions de l''information, des arts et des spectacles',N'03')
,(N'11', N'Cadres administratifs et commerciaux d''entreprise',N'03')
,(N'12', N'Ingénieurs et cadres techniques d''entreprise',N'03') 
,(N'13', N'Professeurs des écoles, instituteurs et assimilés',N'04')
,(N'14', N'Professions intermédiaires de la santé et du travail social',N'04')
,(N'15', N'Clergé, religieux',N'04')
,(N'16', N'Professions intermédiaires administratives de la fonction publique',N'04')
,(N'17', N'Professions intermédiaires administratives et commerciales des entreprises',N'04')
,(N'18', N'Techniciens',N'04')
,(N'19', N'Contremaîtres, agents de maîtrise',N'04')
,(N'20', N'Employés civils et agents de service de la fonction publique',N'05')
,(N'21', N'Policiers et militaires',N'05')
,(N'22', N'Employés administratifs d''entreprise',N'05')
,(N'23', N'Employés de commerce',N'05')
,(N'24', N'Personnels des services directs aux particuliers',N'05')
,(N'25', N'Ouvriers qualifiés de type industriel',N'06')
,(N'26', N'Ouvriers qualifiés de type artisanal',N'06')
,(N'27', N'Chauffeurs',N'06')
,(N'28', N'Ouvriers qualifiés de la manutention, du magasinage et du transport',N'06')
,(N'29', N'Ouvriers non qualifiés de type industriel',N'06')
,(N'30', N'Ouvriers non qualifiés de type artisanal',N'06')
,(N'31', N'Ouvriers agricoles',N'06')
,(N'32', N'Anciens agriculteurs exploitants',N'07')
,(N'33', N'Anciens artisans, commerçants, chefs d''entreprise',N'07')
,(N'34', N'Anciens cadres',N'07')
,(N'35', N'Anciennes professions intermédiaires',N'07')
,(N'36', N'Anciens employés',N'07')
,(N'37', N'Anciens ouvriers',N'07')
,(N'38', N'Chômeurs n''ayant jamais travaillé',N'08')
,(N'39', N'Militaires du contingent',N'08')
,(N'40', N'Elèves, étudiants',N'08')
,(N'41', N'Personnes diverses sans activité professionnelle de moins de 60 ans (sauf retraités)',N'08')
,(N'42', N'Personnes diverses sans activité professionnelle de 60 ans et plus (sauf retraités)',N'08')

GO

/*--------------------------------------------*/
/*------LOAD_MDT_MATCH_FE_SL_SIDP-------------*/
/*--------------------------------------------*/

/*Script contenant les correspondances entre les noms des tables SIDP et tables FE_SL*/


USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[MDT_MATCH_FE_SL_SIDP]
GO

INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES ('PV_FE_CARD_PRODUCT', N'CARD_PRODUCT')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES ('PV_FE_CARDPRODUCT_V2', N'CARD_PRODUCT')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES ('PV_FE_CARDEQUIPMENT_V2', N'CARD_EQUIPMENT')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES ('PV_FE_COMMERCIALOFFER', N'COMMERCIAL_OFFER')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES ('PV_FE_COMMERCIALOFFER_V2', N'COMMERCIAL_OFFER')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES ('PV_FE_COMMERCIALPRODUCT', N'COMMERCIAL_PRODUCT')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES ('PV_FE_COMMERCIALPRODUCT_V2', N'COMMERCIAL_PRODUCT')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES ('PV_FE_CONSUMERLOANEQUIPMENT', N'CONSUMER_LOAN_EQUIPMENT')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES ('PV_FE_CONSUMERLOANEQUIPMENT_V2', N'CONSUMER_LOAN_EQUIPMENT')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_CONSUMERLOANFINANCIALPLAN', N'CONSUMER_LOAN_FINANCIAL_PLAN')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_CONSUMERLOANFINANCIALPLAN_V2', N'CONSUMER_LOAN_FINANCIAL_PLAN')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_CONSUMERLOANPRODUCT', N'CONSUMER_LOAN_PRODUCT')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_CONSUMERLOANPRODUCT_V2', N'CONSUMER_LOAN_PRODUCT')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_CUSTOMER', N'CUSTOMER')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_CUSTOMER_V2', N'CUSTOMER')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_CUSTOMERENRFOLDERASSOC', N'CUSTOMER_ENR_FOLDER_ASSOC')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_CUSTOMERENRFOLDERASSOC_V2', N'CUSTOMER_ENR_FOLDER_ASSOC')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_CUSTOMERSUBPRODUCTASSOC', N'CUSTOMER_SUB_PRODUCT_ASSOC')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_CUSTOMERSUBPRODUCTASSOC_V2', N'CUSTOMER_SUB_PRODUCT_ASSOC')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_ENROLMENTFOLDER', N'ENROLMENT_FOLDER')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_ENROLMENTFOLDER_V2', N'ENROLMENT_FOLDER')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_EQUIPMENT', N'EQUIPMENT')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_EQUIPMENT_V2', N'EQUIPMENT')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_EQUIPMENTSVCASSOC', N'EQUIPMENT_SERVICE_ASSOCIATION')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_EQUIPMENTSVCASSOC_V2', N'EQUIPMENT_SERVICE_ASSOCIATION')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_MOBILE_PAYMENT_DEVICE', N'MOBILE_PAYMENT_DEVICE')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_MOBILEPAYMENTDEVICE_V2', N'MOBILE_PAYMENT_DEVICE')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_PRODUCTOFFERASSOC', N'PRODUCT_OFFER_ASSOC')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_PRODUCTOFFERASSOC_V2', N'PRODUCT_OFFER_ASSOC')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_SBDCHKACCOUNT', N'SUBSCRIBED_CHECK_ACCOUNT')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_SBDCHKACCOUNT_V2', N'SUBSCRIBED_CHECK_ACCOUNT')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_SBDSERVICESASSOC', N'SBD_SERVICES_ASSOC')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_SBDSERVICESASSOC_V2', N'SBD_SERVICES_ASSOC')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_SERVICEPRODUCT', N'SERVICE_PRODUCT')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_SERVICEPRODUCT_V2', N'SERVICE_PRODUCT')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_SUBSCRIBEDCONSUMERLOAN', N'SUBSCRIBED_CONSUMER_LOAN')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_SUBSCRIBEDCONSUMERLOAN_V2', N'SUBSCRIBED_CONSUMER_LOAN')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_SUBSCRIBEDPRODUCT', N'SUBSCRIBED_PRODUCT')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_SUBSCRIBEDPRODUCT_V2', N'SUBSCRIBED_PRODUCT')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_SVCPRODUCTASSOC', N'SVC_PRODUCT_ASSOCIATION')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'PV_FE_SVCPRODUCTASSOC_V2', N'SVC_PRODUCT_ASSOCIATION')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'WK_FE_REF_CHANNEL', N'REF_CHANNEL')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'WK_FE_REF_CHECKING_ACCOUNT_TYPE', N'REF_CHECKING_ACCOUNT_TYPE')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'WK_FE_REF_CODE', N'REF_CODE')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'WK_FE_REF_CONS_LOAN_REJECT_REASON', N'REF_CONS_LOAN_REJECT_REASON')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'WK_FE_REF_CONSU_LOAN_FIN_PLAN_STATUS', N'REF_CONSU_LOAN_FIN_PLAN_STATUS')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'WK_FE_REF_CONSUMER_LOAN_DEROGATION', N'REF_CONSUMER_LOAN_DEROGATION')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'WK_FE_REF_CONSUMER_LOAN_MGT_DECISION', N'REF_CONSUMER_LOAN_MGT_DECISION')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'WK_FE_REF_CONSUMER_LOAN_MGT_RT_ST', N'REF_CONSUMER_LOAN_MGT_RT_ST')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'WK_FE_REF_CONSUMER_LOAN_SCORE_COLOR', N'REF_CONSUMER_LOAN_SCORE_COLOR')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'WK_FE_REF_CONSUMER_LOAN_SITUATION', N'REF_CONSUMER_LOAN_SITUATION')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'WK_FE_REF_CONSUMER_LOAN_STATUS', N'REF_CONSUMER_LOAN_STATUS')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'WK_FE_REF_CONSUMER_LOAN_TERM_REASON', N'REF_CONSUMER_LOAN_TERM_REASON')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'WK_FE_REF_DAY_OF_MONTH', N'REF_DAY_OF_MONTH')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'WK_FE_REF_DISTRIBUTION_NETWORK', N'REF_DISTRIBUTION_NETWORK')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'WK_FE_REF_EMPLOYMENT_CONTRACT', N'REF_EMPLOYMENT_CONTRACT')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'WK_FE_REF_FINANCING_TYPE', N'REF_FINANCING_TYPE')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'WK_FE_REF_FINANCING_TYPE_FAMILY', N'REF_FINANCING_TYPE_FAMILY')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'WK_FE_REF_OSDEVICE', N'REF_OSDEVICE')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'WK_FE_REF_PERSON_RELATIONSHIP', N'REF_PERSON_RELATIONSHIP')
GO
INSERT [dbo].[MDT_MATCH_FE_SL_SIDP] ([NAME_TABLE_SIDP], [NAME_TABLE_FE_SL]) VALUES (N'WK_FE_REF_SALUTATION', N'REF_SALUTATION')
GO


/*--------------------------------------------*/
/*------MDT_MATCH_FORMAT_FE_SL_SIDP-------------*/
/*--------------------------------------------*/

/*Script contenant les correspondances entre les formats des tables sql server SIDP et tables oracle FE_SL*/


USE [$(DataFactoryDatabaseName)]
GO

TRUNCATE TABLE [dbo].[MDT_MATCH_FORMAT_FE_SL_SIDP]
GO

INSERT [dbo].[MDT_MATCH_FORMAT_FE_SL_SIDP] ([FORMAT_SIDP], [FORMAT_FE_SL]) VALUES (N'DECIMAL', N'NUMBER')
GO
INSERT [dbo].[MDT_MATCH_FORMAT_FE_SL_SIDP] ([FORMAT_SIDP], [FORMAT_FE_SL]) VALUES (N'NVARCHAR', N'VARCHAR')
GO
INSERT [dbo].[MDT_MATCH_FORMAT_FE_SL_SIDP] ([FORMAT_SIDP], [FORMAT_FE_SL]) VALUES (N'NVARCHAR', N'VARCHAR2')
GO
INSERT [dbo].[MDT_MATCH_FORMAT_FE_SL_SIDP] ([FORMAT_SIDP], [FORMAT_FE_SL]) VALUES (N'DATETIME2(6)', N'TIMESTAMP(6)')
GO
INSERT [dbo].[MDT_MATCH_FORMAT_FE_SL_SIDP] ([FORMAT_SIDP], [FORMAT_FE_SL]) VALUES (N'DATE', N'DATE')
GO
INSERT [dbo].[MDT_MATCH_FORMAT_FE_SL_SIDP] ([FORMAT_SIDP], [FORMAT_FE_SL]) VALUES (N'NVARCHAR', N'CLOB')
GO
INSERT [dbo].[MDT_MATCH_FORMAT_FE_SL_SIDP] ([FORMAT_SIDP], [FORMAT_FE_SL]) VALUES (N'VARBINARY(MAX)', N'BLOB')
GO
INSERT [dbo].[MDT_MATCH_FORMAT_FE_SL_SIDP] ([FORMAT_SIDP], [FORMAT_FE_SL]) VALUES (N'NVARCHAR', N'CHAR')
GO
INSERT [dbo].[MDT_MATCH_FORMAT_FE_SL_SIDP] ([FORMAT_SIDP], [FORMAT_FE_SL]) VALUES (N'FLOAT', N'FLOAT')
GO




USE [$(DataFactoryDatabaseName)]
GO

--********************************************
-- Report : R_Etat_Avancement_Chargement_SIDP
--********************************************
----********************
---- Daily Subscription
----********************
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Daily Subscription' AND	[Key] = 'Recipient_Address')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'meryem.guerrouani@orangebank.com;ariane.peyronne@orangebank.com;samir.aitidir@orangebank.com;guillaume.perrocheau@orangebank.com;datafactory@orangebank.com;n2incidentstechniques@orangebank.com;christophe.sol@orangebank.com;jessica.ndjiki@orangebank.com'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Daily Subscription'
			AND	[Key] = 'Recipient_Address'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Daily Subscription',
			'Recipient_Address',
			'meryem.guerrouani@orangebank.com;ariane.peyronne@orangebank.com;samir.aitidir@orangebank.com;guillaume.perrocheau@orangebank.com;datafactory@orangebank.com;n2incidentstechniques@orangebank.com;christophe.sol@orangebank.com;jessica.ndjiki@orangebank.com'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Daily Subscription' AND	[Key] = 'Bcc_Recipient_Address')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'hamza.boukraa.ext@orangebank.com'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Daily Subscription'
			AND	[Key] = 'Bcc_Recipient_Address'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Daily Subscription',
			'Bcc_Recipient_Address',
			'hamza.boukraa.ext@orangebank.com'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Daily Subscription' AND	[Key] = 'Include_Report')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'True'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Daily Subscription'
			AND	[Key] = 'Include_Report'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Daily Subscription',
			'Include_Report',
			'True'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Daily Subscription' AND	[Key] = 'Email_Subject')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'Etat d''avancement des traitements quotidiens de SIDP'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Daily Subscription'
			AND	[Key] = 'Email_Subject'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Daily Subscription',
			'Email_Subject',
			'Etat d''avancement des traitements quotidiens de SIDP'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Daily Subscription' AND	[Key] = 'Email_Body')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = '<html><body>Bonjour,<br/><br/>Veuillez trouver ci-dessous l''état d''avancement des traitements quotidiens de SIDP.<br/><br/>Ce rapport est consultable en suivant <a href=''https://obsidprs.intra.orangebank.com/ReportServer_INSTP00/Pages/ReportViewer.aspx?/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP&P_FRQN=Q''>ce lien</a>.<br/><br/>Cordialement,<br/>DataFactory Team</body></html>'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Daily Subscription'
			AND	[Key] = 'Email_Body'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Daily Subscription',
			'Email_Body',
			'<html><body>Bonjour,<br/><br/>Veuillez trouver ci-dessous l''état d''avancement des traitements quotidiens de SIDP.<br/><br/>Ce rapport est consultable en suivant <a href=''https://obsidprs.intra.orangebank.com/ReportServer_INSTP00/Pages/ReportViewer.aspx?/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP&P_FRQN=Q''>ce lien</a>.<br/><br/>Cordialement,<br/>DataFactory Team</body></html>'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Daily Subscription' AND	[Key] = 'Include_Link')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'False'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Daily Subscription'
			AND	[Key] = 'Include_Link'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Daily Subscription',
			'Include_Link',
			'False'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Daily Subscription' AND	[Key] = 'Frequency')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'Q'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Daily Subscription'
			AND	[Key] = 'Frequency'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Daily Subscription',
			'Frequency',
			'Q'
		)
	END
GO
----*************************
---- Daily Early Subscription
----*************************
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Daily Early Subscription' AND	[Key] = 'Recipient_Address')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'meryem.guerrouani@orangebank.com;ariane.peyronne@orangebank.com;samir.aitidir@orangebank.com;guillaume.perrocheau@orangebank.com;datafactory@orangebank.com;n2incidentstechniques@orangebank.com;christophe.sol@orangebank.com;jessica.ndjiki@orangebank.com'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Daily Early Subscription'
			AND	[Key] = 'Recipient_Address'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Daily Early Subscription',
			'Recipient_Address',
			'meryem.guerrouani@orangebank.com;ariane.peyronne@orangebank.com;samir.aitidir@orangebank.com;guillaume.perrocheau@orangebank.com;datafactory@orangebank.com;n2incidentstechniques@orangebank.com;christophe.sol@orangebank.com;jessica.ndjiki@orangebank.com'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Daily Early Subscription' AND	[Key] = 'Bcc_Recipient_Address')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'hamza.boukraa.ext@orangebank.com'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Daily Early Subscription'
			AND	[Key] = 'Bcc_Recipient_Address'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Daily Early Subscription',
			'Bcc_Recipient_Address',
			'hamza.boukraa.ext@orangebank.com'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Daily Early Subscription' AND	[Key] = 'Include_Report')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'True'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Daily Early Subscription'
			AND	[Key] = 'Include_Report'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Daily Early Subscription',
			'Include_Report',
			'True'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Daily Early Subscription' AND	[Key] = 'Email_Subject')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'Etat d''avancement des traitements quotidiens de SIDP'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Daily Early Subscription'
			AND	[Key] = 'Email_Subject'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Daily Early Subscription',
			'Email_Subject',
			'Etat d''avancement des traitements quotidiens de SIDP'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Daily Early Subscription' AND	[Key] = 'Email_Body')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = '<html><body>Bonjour,<br/><br/>Veuillez trouver ci-dessous l''état d''avancement des traitements quotidiens de SIDP.<br/><br/>Ce rapport est consultable en suivant <a href=''https://obsidprs.intra.orangebank.com/ReportServer_INSTP00/Pages/ReportViewer.aspx?/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP&P_FRQN=Q''>ce lien</a>.<br/><br/>Cordialement,<br/>DataFactory Team</body></html>'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Daily Early Subscription'
			AND	[Key] = 'Email_Body'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Daily Early Subscription',
			'Email_Body',
			'<html><body>Bonjour,<br/><br/>Veuillez trouver ci-dessous l''état d''avancement des traitements quotidiens de SIDP.<br/><br/>Ce rapport est consultable en suivant <a href=''https://obsidprs.intra.orangebank.com/ReportServer_INSTP00/Pages/ReportViewer.aspx?/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP&P_FRQN=Q''>ce lien</a>.<br/><br/>Cordialement,<br/>DataFactory Team</body></html>'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Daily Early Subscription' AND	[Key] = 'Include_Link')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'False'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Daily Early Subscription'
			AND	[Key] = 'Include_Link'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Daily Early Subscription',
			'Include_Link',
			'False'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Daily Early Subscription' AND	[Key] = 'Frequency')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'Q'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Daily Early Subscription'
			AND	[Key] = 'Frequency'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Daily Early Subscription',
			'Frequency',
			'Q'
		)
	END
GO
----********************
---- Weekly Subscription
----********************
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Weekly Subscription' AND	[Key] = 'Recipient_Address')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'meryem.guerrouani@orangebank.com;ariane.peyronne@orangebank.com;samir.aitidir@orangebank.com;guillaume.perrocheau@orangebank.com;datafactory@orangebank.com;n2incidentstechniques@orangebank.com;christophe.sol@orangebank.com;jessica.ndjiki@orangebank.com'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Weekly Subscription'
			AND	[Key] = 'Recipient_Address'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Weekly Subscription',
			'Recipient_Address',
			'meryem.guerrouani@orangebank.com;ariane.peyronne@orangebank.com;samir.aitidir@orangebank.com;guillaume.perrocheau@orangebank.com;datafactory@orangebank.com;n2incidentstechniques@orangebank.com;christophe.sol@orangebank.com;jessica.ndjiki@orangebank.com'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Weekly Subscription' AND	[Key] = 'Bcc_Recipient_Address')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'hamza.boukraa.ext@orangebank.com'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Weekly Subscription'
			AND	[Key] = 'Bcc_Recipient_Address'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Weekly Subscription',
			'Bcc_Recipient_Address',
			'hamza.boukraa.ext@orangebank.com'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Weekly Subscription' AND	[Key] = 'Include_Report')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'True'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Weekly Subscription'
			AND	[Key] = 'Include_Report'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Weekly Subscription',
			'Include_Report',
			'True'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Weekly Subscription' AND	[Key] = 'Email_Subject')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'Etat d''avancement des traitements hebdomadaires de SIDP'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Weekly Subscription'
			AND	[Key] = 'Email_Subject'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Weekly Subscription',
			'Email_Subject',
			'Etat d''avancement des traitements hebdomadaires de SIDP'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Weekly Subscription' AND	[Key] = 'Email_Body')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = '<html><body>Bonjour,<br/><br/>Veuillez trouver ci-dessous l''état d''avancement des traitements hebdomadaires de SIDP.<br/><br/>Ce rapport est consultable en suivant <a href=''https://obsidprs.intra.orangebank.com/ReportServer_INSTP00/Pages/ReportViewer.aspx?/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP&P_FRQN=H''>ce lien</a>.<br/><br/>Cordialement,<br/>DataFactory Team</body></html>'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Weekly Subscription'
			AND	[Key] = 'Email_Body'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Weekly Subscription',
			'Email_Body',
			'<html><body>Bonjour,<br/><br/>Veuillez trouver ci-dessous l''état d''avancement des traitements hebdomadaires de SIDP.<br/><br/>Ce rapport est consultable en suivant <a href=''https://obsidprs.intra.orangebank.com/ReportServer_INSTP00/Pages/ReportViewer.aspx?/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP&P_FRQN=H''>ce lien</a>.<br/><br/>Cordialement,<br/>DataFactory Team</body></html>'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Weekly Subscription' AND	[Key] = 'Include_Link')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'False'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Weekly Subscription'
			AND	[Key] = 'Include_Link'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Weekly Subscription',
			'Include_Link',
			'False'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Weekly Subscription' AND	[Key] = 'Frequency')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'H'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Weekly Subscription'
			AND	[Key] = 'Frequency'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Weekly Subscription',
			'Frequency',
			'H'
		)
	END
GO
----**************************
---- Weekly Early Subscription
----**************************
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Weekly Early Subscription' AND	[Key] = 'Recipient_Address')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'meryem.guerrouani@orangebank.com;ariane.peyronne@orangebank.com;samir.aitidir@orangebank.com;guillaume.perrocheau@orangebank.com;datafactory@orangebank.com;n2incidentstechniques@orangebank.com;christophe.sol@orangebank.com;jessica.ndjiki@orangebank.com'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Weekly Early Subscription'
			AND	[Key] = 'Recipient_Address'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Weekly Early Subscription',
			'Recipient_Address',
			'meryem.guerrouani@orangebank.com;ariane.peyronne@orangebank.com;samir.aitidir@orangebank.com;guillaume.perrocheau@orangebank.com;datafactory@orangebank.com;n2incidentstechniques@orangebank.com;christophe.sol@orangebank.com;jessica.ndjiki@orangebank.com'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Weekly Early Subscription' AND	[Key] = 'Bcc_Recipient_Address')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'hamza.boukraa.ext@orangebank.com'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Weekly Early Subscription'
			AND	[Key] = 'Bcc_Recipient_Address'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Weekly Early Subscription',
			'Bcc_Recipient_Address',
			'hamza.boukraa.ext@orangebank.com'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Weekly Early Subscription' AND	[Key] = 'Include_Report')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'True'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Weekly Early Subscription'
			AND	[Key] = 'Include_Report'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Weekly Early Subscription',
			'Include_Report',
			'True'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Weekly Early Subscription' AND	[Key] = 'Email_Subject')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'Etat d''avancement des traitements hebdomadaires de SIDP'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Weekly Early Subscription'
			AND	[Key] = 'Email_Subject'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Weekly Early Subscription',
			'Email_Subject',
			'Etat d''avancement des traitements hebdomadaires de SIDP'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Weekly Early Subscription' AND	[Key] = 'Email_Body')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = '<html><body>Bonjour,<br/><br/>Veuillez trouver ci-dessous l''état d''avancement des traitements hebdomadaires de SIDP.<br/><br/>Ce rapport est consultable en suivant <a href=''https://obsidprs.intra.orangebank.com/ReportServer_INSTP00/Pages/ReportViewer.aspx?/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP&P_FRQN=H''>ce lien</a>.<br/><br/>Cordialement,<br/>DataFactory Team</body></html>'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Weekly Early Subscription'
			AND	[Key] = 'Email_Body'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Weekly Early Subscription',
			'Email_Body',
			'<html><body>Bonjour,<br/><br/>Veuillez trouver ci-dessous l''état d''avancement des traitements hebdomadaires de SIDP.<br/><br/>Ce rapport est consultable en suivant <a href=''https://obsidprs.intra.orangebank.com/ReportServer_INSTP00/Pages/ReportViewer.aspx?/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP&P_FRQN=H''>ce lien</a>.<br/><br/>Cordialement,<br/>DataFactory Team</body></html>'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Weekly Early Subscription' AND	[Key] = 'Include_Link')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'False'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Weekly Early Subscription'
			AND	[Key] = 'Include_Link'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Weekly Early Subscription',
			'Include_Link',
			'False'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Weekly Early Subscription' AND	[Key] = 'Frequency')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'H'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Weekly Early Subscription'
			AND	[Key] = 'Frequency'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Weekly Early Subscription',
			'Frequency',
			'H'
		)
	END
GO
----*****************************************
---- Check Processing Completion Subscription
----*****************************************
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Check Processing Completion Subscription' AND	[Key] = 'Recipient_Address')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'datafactory@orangebank.com'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Check Processing Completion Subscription'
			AND	[Key] = 'Recipient_Address'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Check Processing Completion Subscription',
			'Recipient_Address',
			'datafactory@orangebank.com'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Check Processing Completion Subscription' AND	[Key] = 'Bcc_Recipient_Address')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'hamza.boukraa.ext@orangebank.com'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Check Processing Completion Subscription'
			AND	[Key] = 'Bcc_Recipient_Address'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Check Processing Completion Subscription',
			'Bcc_Recipient_Address',
			'hamza.boukraa.ext@orangebank.com'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Check Processing Completion Subscription' AND	[Key] = 'Include_Report')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'False'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Check Processing Completion Subscription'
			AND	[Key] = 'Include_Report'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Check Processing Completion Subscription',
			'Include_Report',
			'False'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Check Processing Completion Subscription' AND	[Key] = 'Email_Subject')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'Etat d''avancement des traitements d''alimentation #Frequency# de SIDP '
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Check Processing Completion Subscription'
			AND	[Key] = 'Email_Subject'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Check Processing Completion Subscription',
			'Email_Subject',
			'Etat d''avancement des traitements d''alimentation #Frequency# de SIDP '
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Check Processing Completion Subscription' AND	[Key] = 'Email_Body')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = '<html><body>Bonjour,<br/><br/>Les traitements d''alimentation de #Domain# sont terminés sur SIDP.<br/><br/>Cordialement,<br/>DataFactory Team</body></html>'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Check Processing Completion Subscription'
			AND	[Key] = 'Email_Body'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Check Processing Completion Subscription',
			'Email_Body',
			'<html><body>Bonjour,<br/><br/>Les traitements d''alimentation de #Domain# sont terminés sur SIDP.<br/><br/>Cordialement,<br/>DataFactory Team</body></html>'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Check Processing Completion Subscription' AND	[Key] = 'Include_Link')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'False'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Check Processing Completion Subscription'
			AND	[Key] = 'Include_Link'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Check Processing Completion Subscription',
			'Include_Link',
			'False'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Check Processing Completion Subscription' AND	[Key] = 'Frequency')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'Q'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Check Processing Completion Subscription'
			AND	[Key] = 'Frequency'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Check Processing Completion Subscription',
			'Frequency',
			'Q'
		)
	END
GO
--********************************************
GO
