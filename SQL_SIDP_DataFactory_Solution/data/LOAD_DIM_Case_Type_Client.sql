USE [$(DataFactoryDatabaseName)]
GO
TRUNCATE TABLE [dbo].[DIM_CASE_TYPE_CLI]
GO
INSERT INTO [dbo].[DIM_CASE_TYPE_CLI] ([CODE_SF], [LIBELLE]) VALUES (N'01', N'Particulier')
,(N'02', N'Professionnel Personne Physique')
,(N'03', N'Personne Morale')
GO