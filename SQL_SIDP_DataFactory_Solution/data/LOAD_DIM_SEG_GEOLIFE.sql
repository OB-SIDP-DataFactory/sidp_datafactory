USE [$(DataFactoryDatabaseName)]
GO
TRUNCATE TABLE [dbo].[DIM_SEG_GEOLIFE]
GO
  INSERT INTO [dbo].[DIM_SEG_GEOLIFE] ([LIB_SEG_GEO],[ID_CAT_SEG_GEO])
  VALUES 
  ('1 - urbain dynamique',1),
('urbain dynamique - 1',1),
('2 - urbain familial aise',1),
('urbain familial aise - 2',1),
('3 - urbain classe moyenne',1),
('urbain classe moyenne - 3',1),
('4 - populaire',1),
('populaire - 4',1),
('5 - urbain defavorise',1),
('urbain defavorise - 5',1),
('6 - periurbain en croissance',2),
('periurbain en croissance - 6',2),
('7 - pavillonnaire familial aise',2),
('pavillonnaire familial aise - 7',2),
('8 - rural dynamique',3),
('rural dynamique - 8',3),
('9 - rural ouvrier',3),
('rural ouvrier - 9',3),
('10 - rural traditionnel',3),
('rural traditionnel - 10',3),
('residence secondaire - 11',4),
(NULL,4)
GO
