USE [$(DataFactoryDatabaseName)]
GO
TRUNCATE TABLE [dbo].[DIM_MOYENS_PAIEMENT]
GO
INSERT INTO [dbo].[DIM_MOYENS_PAIEMENT] ([CODE_MOYENS_PAIEMENT], [MOYENS_PAIEMENT], [Validity_StartDate])
VALUES ('P', 'Carte bancaire exclusivement', '2017-01-01')
     , ('M', 'Paiement mobile exclusivement', '2017-01-01')
     , ('M P', 'Carte bancaire et paiement mobile', '2017-01-01')
     , ('NA', 'Aucun moyen de paiement', '2017-01-01')
GO
