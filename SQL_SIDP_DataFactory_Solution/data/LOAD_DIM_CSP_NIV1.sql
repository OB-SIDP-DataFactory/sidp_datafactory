USE [$(DataFactoryDatabaseName)]
GO
TRUNCATE TABLE [dbo].[DIM_CSP_NIV1]
GO
INSERT INTO [dbo].[DIM_CSP_NIV1] ([COD_CSP_NIV1], [LIB_CSP_NIV1]) VALUES 
(N'01', N'Agriculteurs exploitants')
,(N'02', N'Artisans, commerçants et chefs d''entreprise')
,(N'03', N'Cadres et professions intellectuelles supérieures')
,(N'04', N'Professions intermédiaires')
,(N'05', N'Employés')
,(N'06', N'Ouvriers')
,(N'07', N'Retraités')
,(N'08', N'Autres personnes sans activité professionnelle')
GO