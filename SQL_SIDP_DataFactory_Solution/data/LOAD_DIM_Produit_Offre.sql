USE [$(DataFactoryDatabaseName)]
GO
truncate table [dbo].[DIM_PRODUIT_OFFRE]
GO
INSERT INTO [dbo].[DIM_PRODUIT_OFFRE] ([CODE_FE], [LIBELLE], [PARENT_ID]) VALUES (N'OC80', N'Compte bancaire', NULL)

,(N'L80', N'Livret', NULL)

,(N'CB80', N'Carte bancaire', NULL)

,(N'C80', N'Compte bancaire Orange Bank', N'OC80')

,(N'OC1', N'Carte Visa Orange Bank', N'OC80')

,(N'OC1', N'Carte Visa Orange Bank', N'CB80')

,(N'L80', N'Compte sur livret Orange Bank', N'L80')
GO