USE [$(DataFactoryDatabaseName)]
GO
TRUNCATE TABLE [dbo].[DIM_USAGE_VIREMENT]
GO
INSERT INTO [dbo].[DIM_USAGE_VIREMENT] ([USAGE_VIREMENT], [NB_OP_MIN], [NB_OP_MAX])
VALUES ('0 opérations', 0, 0)
     , ('1 opération', 1, 1)
     , ('2 opérations', 2, 2)
     , ('3 opérations', 3, 3)
     , ('4 opérations',4, 4 )
     , ('5 opérations', 5, 5)
     , ('5+ opérations',6, NULL)
GO
