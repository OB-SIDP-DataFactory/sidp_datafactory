USE [$(DataFactoryDatabaseName)]
GO
TRUNCATE TABLE [dbo].[DIM_CASE_OBJET_SECONDAIRE]
GO
INSERT INTO [dbo].[DIM_CASE_OBJET_SECONDAIRE] ([CODE_SF], [LIBELLE]) VALUES (N'01', N'Information/conseil')
,(N'02', N'Qualité de l''offre/documentation contractuelle')
,(N'03', N'Dysfonctionnement site/mise à jour')
,(N'04', N'Accueil conseiller/méthode vente')
,(N'05', N'Remboursement cotisation')
,(N'06', N'Remboursement frais irrégularités/incidents')
,(N'07', N'Erreur de traitement')
,(N'08', N'Délai de traitement')
,(N'09', N'Inscription fichiers BDF (FCC, FICP, FNCI)')
,(N'10', N'Fraude/opération non autorisée')
,(N'11', N'Incidents sur compte/irrégularités')
,(N'12', N'Clôture du compte')
,(N'13', N'Mobilité bancaire')
,(N'14', N'Droit au compte/SBB/SAP')
,(N'15', N'Remboursement tarification')
,(N'16', N'Opposition')
,(N'17', N'Dysfonctionnements DAB')
,(N'18', N'Fiscalité/ transfert /clôture')
,(N'19', N'Mise en place/Déblocage des fonds/refus d''octroi de crédit')
,(N'20', N'Rupture de crédit ou d''autorisation de découvert')
,(N'21', N'Surendettement, demande de réaménagement de dettes')
GO