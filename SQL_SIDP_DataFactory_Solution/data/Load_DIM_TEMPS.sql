USE [$(DataFactoryDatabaseName)]
GO
TRUNCATE TABLE [dbo].[DIM_TEMPS]
GO
SET DATEFIRST 1;

WITH IDX_INIT AS
( SELECT '1' AS IDX UNION
  SELECT '2' AS IDX UNION
  SELECT '3' AS IDX UNION
  SELECT '4' AS IDX UNION
  SELECT '5' AS IDX UNION
  SELECT '6' AS IDX UNION
  SELECT '7' AS IDX UNION
  SELECT '8' AS IDX UNION
  SELECT '9' AS IDX UNION
  SELECT '0' AS IDX
)

INSERT INTO [dbo].[DIM_TEMPS]
( [PK_ID], --TO MAKE THE ID THE YYYYMMDD FORMAT UNCOMMENT THIS LINE... Comment for autoincrementing.
  [Date]
, [Day]
, [DaySuffix]
, [DayOfWeek]
, [DOWInMonth]
, [DayOfYear]
, [WeekOfYear]
, [WeekOfMonth] 
, [IsoWeek] 
, [IsoWeekYear] 
, [Month]
, [MonthName]
, [MonthYear]
, [Quarter]
, [QuarterName]
, [Year]
, [BusinessDay]
, [StandardDate]
)
SELECT CONVERT(VARCHAR, D.[Date], 112), --TO MAKE THE ID THE YYYYMMDD FORMAT UNCOMMENT THIS LINE COMMENT FOR AUTOINCREMENT
       D.[Date] AS [Date]
     , SUBSTRING(CONVERT(VARCHAR, D.[Date], 112), 7, 2) AS [Day]
     , CASE WHEN DATEPART(DAY, D.[Date]) IN (11,12,13) THEN CAST(DATEPART(DAY, D.[Date]) AS VARCHAR) + 'th'
            WHEN RIGHT(DATEPART(DAY, D.[Date]),1) = 1 THEN CAST(DATEPART(DAY, D.[Date]) AS VARCHAR) + 'st'
            WHEN RIGHT(DATEPART(DAY, D.[Date]),1) = 2 THEN CAST(DATEPART(DAY, D.[Date]) AS VARCHAR) + 'nd'
            WHEN RIGHT(DATEPART(DAY, D.[Date]),1) = 3 THEN CAST(DATEPART(DAY, D.[Date]) AS VARCHAR) + 'rd'
            ELSE CAST(DATEPART(DAY, D.[Date]) AS VARCHAR) + 'th' 
       END AS [DaySuffix]
     , DATENAME(WEEKDAY, D.[Date]) AS [DayOfWeek]
     , ROW_NUMBER() OVER (PARTITION BY SUBSTRING(CONVERT(VARCHAR, D.[Date], 112), 1, 6), DATEPART(DW,  D.[Date]) ORDER BY D.[Date]) AS [DOWInMonth]
     , DATEPART(dy, D.[Date]) AS [DayOfYear]               -- Day of the year. 0 - 365/366
     , DATEPART(ww, D.[Date]) AS [WeekOfYear]              -- 0-52/53
     , DATEPART(ww, D.[Date]) + 1 - DATEPART(ww,CAST(DATEPART(mm, D.[Date]) AS VARCHAR) + '/1/' + CAST(DATEPART(yy, D.[Date]) AS VARCHAR)) AS [WeekOfMonth]
     , DATEPART(isoww, D.[Date]) AS [IsoWeek]        -- 0-52/53
     , CASE WHEN DATEPART(isoww, D.[Date]) >= 52 AND DATEPART(mm, D.[Date]) = 1 THEN DATEPART(YEAR, D.[Date]) - 1
            WHEN DATEPART(isoww, D.[Date]) = 1 AND DATEPART(mm, D.[Date]) = 12 THEN DATEPART(YEAR, D.[Date]) + 1
            ELSE DATEPART(YEAR, D.[Date])
       END AS [IsoWeekYear]
     , SUBSTRING(CONVERT(VARCHAR, D.[Date], 112), 5, 2) AS [Month] 
     , DATENAME(MONTH, D.[Date]) AS [MonthName]
     , CONCAT(DATENAME(MONTH, D.[Date]), ' ', DATEPART(YEAR, D.[Date])) AS [MonthYear]
     , DATEPART(qq, D.[Date]) AS [Quarter]                 -- Calendar quarter
     , CASE DATEPART(qq, D.[Date]) 
            WHEN 1 THEN 'First'
            WHEN 2 THEN 'Second'
            WHEN 3 THEN 'Third'
            WHEN 4 THEN 'Fourth'
            END AS [QuarterName]
     , DATEPART(YEAR, D.[Date]) AS [Year]
     , CASE WHEN /* Sundays */         DATENAME(WEEKDAY, D.[Date]) = 'Sunday' OR
                 /* Fixed Holidays */  CONCAT(DATEPART(d, D.[Date]), '/', DATEPART(m, D.[Date])) IN ('1/1', '1/5', '8/5', '14/7', '15/8', '1/11', '11/11', '25/12') OR
                 /* Easter Holidays */ D.[Date] IN (DATEADD(d,  1, [dbo].[GetEasterSunday](DATEPART(YEAR, D.[Date]))),
                                                    DATEADD(d, 39, [dbo].[GetEasterSunday](DATEPART(YEAR, D.[Date]))),
                                                    DATEADD(d, 50, [dbo].[GetEasterSunday](DATEPART(YEAR, D.[Date]))))
            THEN 0 ELSE 1 END AS [BusinessDay] 
     , D.[Date] AS [StandardDate]
  FROM ( SELECT CAST('1/1/1900' AS DATETIME) + CAST(CONCAT(I1.IDX, I2.IDX, I3.IDX, I4.IDX, I5.IDX) AS INTEGER) AS [Date] 
           FROM IDX_INIT I1
           CROSS JOIN IDX_INIT I2
           CROSS JOIN IDX_INIT I3
           CROSS JOIN IDX_INIT I4
           CROSS JOIN IDX_INIT I5 ) AS D
 WHERE D.[Date] < CAST('1/1/2050' AS DATETIME)
GO
