USE [$(DataFactoryDatabaseName)]
GO
TRUNCATE TABLE [dbo].[DIM_FACTURATION_CPT]
GO
INSERT INTO [dbo].[DIM_FACTURATION_CPT] ([FACTURATION_CPT], [FACTURATION_CPT_MIN], [FACTURATION_CPT_MAX])
VALUES ('0 €', NULL, NULL)
     , ('5 €', 5, 5)
     , ('10 à 30 €', 10, 30)
     , ('35 à 55 €', 35, 55)
     , ('60 €', 60, NULL)
GO
