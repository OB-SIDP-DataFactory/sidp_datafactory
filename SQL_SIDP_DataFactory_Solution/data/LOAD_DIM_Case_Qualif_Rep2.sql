USE [$(DataFactoryDatabaseName)]
GO
TRUNCATE TABLE [dbo].[DIM_CASE_QUALIF_REP2]
GO
INSERT INTO [dbo].[DIM_CASE_QUALIF_REP2] ([CODE_SF], [LIBELLE]) VALUES (N'01', N'Réponse positive')
,(N'02', N'Réponse négative')
,(N'03', N'Accord transactionnel')
GO