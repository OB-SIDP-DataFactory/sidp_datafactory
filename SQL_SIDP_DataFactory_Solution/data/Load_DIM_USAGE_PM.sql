USE [$(DataFactoryDatabaseName)]
GO
TRUNCATE TABLE [dbo].[DIM_USAGE_PM]
GO
INSERT INTO [dbo].[DIM_USAGE_PM] ([USAGE_PM], [NB_OP_MIN], [NB_OP_MAX])
VALUES ('0 opérations', 0, 0)
     , ('1-5 opérations', 1, 5)
     , ('6-10 opérations', 6, 10)
     , ('11-15 opérations', 11, 15)
     , ('16- 20 opérations', 16, 20)
     , ('20+ opérations', 21, NULL)
GO
