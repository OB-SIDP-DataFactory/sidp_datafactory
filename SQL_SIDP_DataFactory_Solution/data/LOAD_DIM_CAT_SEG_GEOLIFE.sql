USE [$(DataFactoryDatabaseName)]
GO
TRUNCATE TABLE [dbo].[DIM_CAT_SEG_GEOLIFE]
GO
INSERT INTO [dbo].[DIM_CAT_SEG_GEOLIFE]
           ([LIB_CAT_SEG_GEO])
     VALUES
           ('Urbain')
		   ,('Péri-Urbain')
		   ,('Rural')
		   ,('Autres')
GO
