USE [$(DataFactoryDatabaseName)]
GO
TRUNCATE TABLE [dbo].[DIM_CASE_MOTIF]
GO
INSERT INTO [dbo].[DIM_CASE_MOTIF] ([CODE_SF], [LIBELLE]) VALUES 
(N'01', N'Favorable')
,(N'02', N'Défavorable')
GO