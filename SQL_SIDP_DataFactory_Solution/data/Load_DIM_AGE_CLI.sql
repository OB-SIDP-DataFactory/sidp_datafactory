USE [$(DataFactoryDatabaseName)]
GO
TRUNCATE TABLE [dbo].[DIM_AGE_CLI]
GO
INSERT INTO [dbo].[DIM_AGE_CLI] ([AGE_CLI], [AGE_CLI_MIN], [AGE_CLI_MAX])
VALUES ('16-17 ans', 16, 17)
     , ('18-24 ans', 18, 24)
     , ('25-39 ans', 25, 39)
     , ('40-59 ans', 40, 59)
     , ('60-69 ans', 60, 69)
     , ('70+ ans', 70, NULL )
GO
