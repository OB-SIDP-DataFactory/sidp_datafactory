USE [$(DataFactoryDatabaseName)]
GO
TRUNCATE TABLE [dbo].[DIM_AGE_OPPORT]
GO
INSERT INTO [dbo].[DIM_AGE_OPPORT] ([AGE_OPPORT], [AGE_OPPORT_MIN], [AGE_OPPORT_MAX])
VALUES ('1 jour', 0, 1)
     , ('2-3 jours', 2, 3)
     , ('4-5 jours', 4, 5)
     , ('6-10 jours', 6, 10)
     , ('11-20 jours', 11, 20)
     , ('21-30 jours', 21, 30)
     , ('31-60 jours', 31, 60)
     , ('60+ jours', 60, NULL )
GO
