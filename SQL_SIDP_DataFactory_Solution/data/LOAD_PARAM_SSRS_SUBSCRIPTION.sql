﻿USE [$(DataFactoryDatabaseName)]
GO
--********************************************
-- Report : R_Etat_Avancement_Chargement_SIDP
--********************************************
----********************
---- Daily Subscription
----********************
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Daily Subscription' AND	[Key] = 'Recipient_Address')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'meryem.guerrouani@orangebank.com;ariane.peyronne@orangebank.com;samir.aitidir@orangebank.com;guillaume.perrocheau@orangebank.com;n2incidentstechniques@orangebank.com;christophe.sol@orangebank.com;jessica.ndjiki@orangebank.com;anne.leoni@orangebank.com;manel.mahdaoui@orangebank.com'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Daily Subscription'
			AND	[Key] = 'Recipient_Address'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Daily Subscription',
			'Recipient_Address',
			'meryem.guerrouani@orangebank.com;ariane.peyronne@orangebank.com;samir.aitidir@orangebank.com;guillaume.perrocheau@orangebank.com;n2incidentstechniques@orangebank.com;christophe.sol@orangebank.com;jessica.ndjiki@orangebank.com;anne.leoni@orangebank.com;manel.mahdaoui@orangebank.com'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Daily Subscription' AND	[Key] = 'Bcc_Recipient_Address')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'datafactory@orangebank.com;datafactoryext@orangebank.com'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Daily Subscription'
			AND	[Key] = 'Bcc_Recipient_Address'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Daily Subscription',
			'Bcc_Recipient_Address',
			'datafactory@orangebank.com;datafactoryext@orangebank.com'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Daily Subscription' AND	[Key] = 'Include_Report')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'True'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Daily Subscription'
			AND	[Key] = 'Include_Report'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Daily Subscription',
			'Include_Report',
			'True'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Daily Subscription' AND	[Key] = 'Email_Subject')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'Etat d''avancement des traitements quotidiens de SIDP'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Daily Subscription'
			AND	[Key] = 'Email_Subject'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Daily Subscription',
			'Email_Subject',
			'Etat d''avancement des traitements quotidiens de SIDP'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Daily Subscription' AND	[Key] = 'Email_Body')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = '<html><body>Bonjour,<br/><br/>Veuillez trouver ci-dessous l''état d''avancement des traitements quotidiens de SIDP.<br/><br/>Ce rapport est consultable en suivant <a href=''https://obsidprs.intra.orangebank.com/ReportServer_INSTP00/Pages/ReportViewer.aspx?/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP&P_FRQN=Q''>ce lien</a>.<br/><br/>Cordialement,<br/>DataFactory Team</body></html>'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Daily Subscription'
			AND	[Key] = 'Email_Body'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Daily Subscription',
			'Email_Body',
			'<html><body>Bonjour,<br/><br/>Veuillez trouver ci-dessous l''état d''avancement des traitements quotidiens de SIDP.<br/><br/>Ce rapport est consultable en suivant <a href=''https://obsidprs.intra.orangebank.com/ReportServer_INSTP00/Pages/ReportViewer.aspx?/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP&P_FRQN=Q''>ce lien</a>.<br/><br/>Cordialement,<br/>DataFactory Team</body></html>'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Daily Subscription' AND	[Key] = 'Include_Link')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'False'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Daily Subscription'
			AND	[Key] = 'Include_Link'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Daily Subscription',
			'Include_Link',
			'False'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Daily Subscription' AND	[Key] = 'Frequency')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'Q'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Daily Subscription'
			AND	[Key] = 'Frequency'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Daily Subscription',
			'Frequency',
			'Q'
		)
	END
GO
----*************************
---- Daily Early Subscription
----*************************
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Daily Early Subscription' AND	[Key] = 'Recipient_Address')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'meryem.guerrouani@orangebank.com;ariane.peyronne@orangebank.com;samir.aitidir@orangebank.com;guillaume.perrocheau@orangebank.com;n2incidentstechniques@orangebank.com;christophe.sol@orangebank.com;jessica.ndjiki@orangebank.com;anne.leoni@orangebank.com;manel.mahdaoui@orangebank.com'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Daily Early Subscription'
			AND	[Key] = 'Recipient_Address'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Daily Early Subscription',
			'Recipient_Address',
			'meryem.guerrouani@orangebank.com;ariane.peyronne@orangebank.com;samir.aitidir@orangebank.com;guillaume.perrocheau@orangebank.com;n2incidentstechniques@orangebank.com;christophe.sol@orangebank.com;jessica.ndjiki@orangebank.com;anne.leoni@orangebank.com;manel.mahdaoui@orangebank.com'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Daily Early Subscription' AND	[Key] = 'Bcc_Recipient_Address')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'datafactory@orangebank.com;datafactoryext@orangebank.com'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Daily Early Subscription'
			AND	[Key] = 'Bcc_Recipient_Address'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Daily Early Subscription',
			'Bcc_Recipient_Address',
			'datafactory@orangebank.com;datafactoryext@orangebank.com'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Daily Early Subscription' AND	[Key] = 'Include_Report')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'True'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Daily Early Subscription'
			AND	[Key] = 'Include_Report'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Daily Early Subscription',
			'Include_Report',
			'True'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Daily Early Subscription' AND	[Key] = 'Email_Subject')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'Etat d''avancement des traitements quotidiens de SIDP'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Daily Early Subscription'
			AND	[Key] = 'Email_Subject'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Daily Early Subscription',
			'Email_Subject',
			'Etat d''avancement des traitements quotidiens de SIDP'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Daily Early Subscription' AND	[Key] = 'Email_Body')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = '<html><body>Bonjour,<br/><br/>Veuillez trouver ci-dessous l''état d''avancement des traitements quotidiens de SIDP.<br/><br/>Ce rapport est consultable en suivant <a href=''https://obsidprs.intra.orangebank.com/ReportServer_INSTP00/Pages/ReportViewer.aspx?/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP&P_FRQN=Q''>ce lien</a>.<br/><br/>Cordialement,<br/>DataFactory Team</body></html>'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Daily Early Subscription'
			AND	[Key] = 'Email_Body'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Daily Early Subscription',
			'Email_Body',
			'<html><body>Bonjour,<br/><br/>Veuillez trouver ci-dessous l''état d''avancement des traitements quotidiens de SIDP.<br/><br/>Ce rapport est consultable en suivant <a href=''https://obsidprs.intra.orangebank.com/ReportServer_INSTP00/Pages/ReportViewer.aspx?/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP&P_FRQN=Q''>ce lien</a>.<br/><br/>Cordialement,<br/>DataFactory Team</body></html>'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Daily Early Subscription' AND	[Key] = 'Include_Link')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'False'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Daily Early Subscription'
			AND	[Key] = 'Include_Link'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Daily Early Subscription',
			'Include_Link',
			'False'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Daily Early Subscription' AND	[Key] = 'Frequency')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'Q'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Daily Early Subscription'
			AND	[Key] = 'Frequency'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Daily Early Subscription',
			'Frequency',
			'Q'
		)
	END
GO
----********************
---- Weekly Subscription
----********************
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Weekly Subscription' AND	[Key] = 'Recipient_Address')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'meryem.guerrouani@orangebank.com;ariane.peyronne@orangebank.com;samir.aitidir@orangebank.com;guillaume.perrocheau@orangebank.com;n2incidentstechniques@orangebank.com;christophe.sol@orangebank.com;jessica.ndjiki@orangebank.com;anne.leoni@orangebank.com;manel.mahdaoui@orangebank.com'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Weekly Subscription'
			AND	[Key] = 'Recipient_Address'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Weekly Subscription',
			'Recipient_Address',
			'meryem.guerrouani@orangebank.com;ariane.peyronne@orangebank.com;samir.aitidir@orangebank.com;guillaume.perrocheau@orangebank.com;n2incidentstechniques@orangebank.com;christophe.sol@orangebank.com;jessica.ndjiki@orangebank.com;anne.leoni@orangebank.com;manel.mahdaoui@orangebank.com'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Weekly Subscription' AND	[Key] = 'Bcc_Recipient_Address')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'datafactory@orangebank.com;datafactoryext@orangebank.com'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Weekly Subscription'
			AND	[Key] = 'Bcc_Recipient_Address'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Weekly Subscription',
			'Bcc_Recipient_Address',
			'datafactory@orangebank.com;datafactoryext@orangebank.com'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Weekly Subscription' AND	[Key] = 'Include_Report')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'True'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Weekly Subscription'
			AND	[Key] = 'Include_Report'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Weekly Subscription',
			'Include_Report',
			'True'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Weekly Subscription' AND	[Key] = 'Email_Subject')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'Etat d''avancement des traitements hebdomadaires de SIDP'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Weekly Subscription'
			AND	[Key] = 'Email_Subject'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Weekly Subscription',
			'Email_Subject',
			'Etat d''avancement des traitements hebdomadaires de SIDP'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Weekly Subscription' AND	[Key] = 'Email_Body')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = '<html><body>Bonjour,<br/><br/>Veuillez trouver ci-dessous l''état d''avancement des traitements hebdomadaires de SIDP.<br/><br/>Ce rapport est consultable en suivant <a href=''https://obsidprs.intra.orangebank.com/ReportServer_INSTP00/Pages/ReportViewer.aspx?/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP&P_FRQN=H''>ce lien</a>.<br/><br/>Cordialement,<br/>DataFactory Team</body></html>'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Weekly Subscription'
			AND	[Key] = 'Email_Body'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Weekly Subscription',
			'Email_Body',
			'<html><body>Bonjour,<br/><br/>Veuillez trouver ci-dessous l''état d''avancement des traitements hebdomadaires de SIDP.<br/><br/>Ce rapport est consultable en suivant <a href=''https://obsidprs.intra.orangebank.com/ReportServer_INSTP00/Pages/ReportViewer.aspx?/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP&P_FRQN=H''>ce lien</a>.<br/><br/>Cordialement,<br/>DataFactory Team</body></html>'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Weekly Subscription' AND	[Key] = 'Include_Link')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'False'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Weekly Subscription'
			AND	[Key] = 'Include_Link'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Weekly Subscription',
			'Include_Link',
			'False'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Weekly Subscription' AND	[Key] = 'Frequency')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'H'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Weekly Subscription'
			AND	[Key] = 'Frequency'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Weekly Subscription',
			'Frequency',
			'H'
		)
	END
GO
----**************************
---- Weekly Early Subscription
----**************************
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Weekly Early Subscription' AND	[Key] = 'Recipient_Address')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'meryem.guerrouani@orangebank.com;ariane.peyronne@orangebank.com;samir.aitidir@orangebank.com;guillaume.perrocheau@orangebank.com;n2incidentstechniques@orangebank.com;christophe.sol@orangebank.com;jessica.ndjiki@orangebank.com;anne.leoni@orangebank.com;manel.mahdaoui@orangebank.com'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Weekly Early Subscription'
			AND	[Key] = 'Recipient_Address'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Weekly Early Subscription',
			'Recipient_Address',
			'meryem.guerrouani@orangebank.com;ariane.peyronne@orangebank.com;samir.aitidir@orangebank.com;guillaume.perrocheau@orangebank.com;n2incidentstechniques@orangebank.com;christophe.sol@orangebank.com;jessica.ndjiki@orangebank.com;anne.leoni@orangebank.com;manel.mahdaoui@orangebank.com'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Weekly Early Subscription' AND	[Key] = 'Bcc_Recipient_Address')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'datafactory@orangebank.com;datafactoryext@orangebank.com'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Weekly Early Subscription'
			AND	[Key] = 'Bcc_Recipient_Address'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Weekly Early Subscription',
			'Bcc_Recipient_Address',
			'datafactory@orangebank.com;datafactoryext@orangebank.com'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Weekly Early Subscription' AND	[Key] = 'Include_Report')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'True'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Weekly Early Subscription'
			AND	[Key] = 'Include_Report'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Weekly Early Subscription',
			'Include_Report',
			'True'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Weekly Early Subscription' AND	[Key] = 'Email_Subject')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'Etat d''avancement des traitements hebdomadaires de SIDP'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Weekly Early Subscription'
			AND	[Key] = 'Email_Subject'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Weekly Early Subscription',
			'Email_Subject',
			'Etat d''avancement des traitements hebdomadaires de SIDP'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Weekly Early Subscription' AND	[Key] = 'Email_Body')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = '<html><body>Bonjour,<br/><br/>Veuillez trouver ci-dessous l''état d''avancement des traitements hebdomadaires de SIDP.<br/><br/>Ce rapport est consultable en suivant <a href=''https://obsidprs.intra.orangebank.com/ReportServer_INSTP00/Pages/ReportViewer.aspx?/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP&P_FRQN=H''>ce lien</a>.<br/><br/>Cordialement,<br/>DataFactory Team</body></html>'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Weekly Early Subscription'
			AND	[Key] = 'Email_Body'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Weekly Early Subscription',
			'Email_Body',
			'<html><body>Bonjour,<br/><br/>Veuillez trouver ci-dessous l''état d''avancement des traitements hebdomadaires de SIDP.<br/><br/>Ce rapport est consultable en suivant <a href=''https://obsidprs.intra.orangebank.com/ReportServer_INSTP00/Pages/ReportViewer.aspx?/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP&P_FRQN=H''>ce lien</a>.<br/><br/>Cordialement,<br/>DataFactory Team</body></html>'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Weekly Early Subscription' AND	[Key] = 'Include_Link')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'False'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Weekly Early Subscription'
			AND	[Key] = 'Include_Link'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Weekly Early Subscription',
			'Include_Link',
			'False'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Weekly Early Subscription' AND	[Key] = 'Frequency')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'H'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Weekly Early Subscription'
			AND	[Key] = 'Frequency'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Weekly Early Subscription',
			'Frequency',
			'H'
		)
	END
GO
----*****************************************
---- Check Processing Completion Subscription
----*****************************************
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Check Processing Completion Subscription' AND	[Key] = 'Recipient_Address')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'datafactory@orangebank.com;ReportingEtPilotage@orangebank.com;DataRiskScience@orangebank.com;DataBusinessScience@orangebank.com;manel.mahdaoui@orangebank.com'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Check Processing Completion Subscription'
			AND	[Key] = 'Recipient_Address'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Check Processing Completion Subscription',
			'Recipient_Address',
			'datafactory@orangebank.com;ReportingEtPilotage@orangebank.com;DataRiskScience@orangebank.com;DataBusinessScience@orangebank.com;manel.mahdaoui@orangebank.com'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Check Processing Completion Subscription' AND	[Key] = 'Bcc_Recipient_Address')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'datafactory@orangebank.com;datafactoryext@orangebank.com'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Check Processing Completion Subscription'
			AND	[Key] = 'Bcc_Recipient_Address'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Check Processing Completion Subscription',
			'Bcc_Recipient_Address',
			'datafactory@orangebank.com;datafactoryext@orangebank.com'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Check Processing Completion Subscription' AND	[Key] = 'Include_Report')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'False'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Check Processing Completion Subscription'
			AND	[Key] = 'Include_Report'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Check Processing Completion Subscription',
			'Include_Report',
			'False'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Check Processing Completion Subscription' AND	[Key] = 'Email_Subject')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'Etat d''avancement des traitements d''alimentation #Frequency# de SIDP '
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Check Processing Completion Subscription'
			AND	[Key] = 'Email_Subject'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Check Processing Completion Subscription',
			'Email_Subject',
			'Etat d''avancement des traitements d''alimentation #Frequency# de SIDP '
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Check Processing Completion Subscription' AND	[Key] = 'Email_Body')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = '<html><body>Bonjour,<br/><br/>Les traitements d''alimentation de #Domain# sont terminés sur SIDP.<br/><br/>Cordialement,<br/>DataFactory Team</body></html>'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Check Processing Completion Subscription'
			AND	[Key] = 'Email_Body'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Check Processing Completion Subscription',
			'Email_Body',
			'<html><body>Bonjour,<br/><br/>Les traitements d''alimentation de #Domain# sont terminés sur SIDP.<br/><br/>Cordialement,<br/>DataFactory Team</body></html>'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Check Processing Completion Subscription' AND	[Key] = 'Include_Link')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'False'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Check Processing Completion Subscription'
			AND	[Key] = 'Include_Link'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Check Processing Completion Subscription',
			'Include_Link',
			'False'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Check Processing Completion Subscription' AND	[Key] = 'Frequency')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'Q'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Check Processing Completion Subscription'
			AND	[Key] = 'Frequency'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Check Processing Completion Subscription',
			'Frequency',
			'Q'
		)
	END
GO
----**************************
---- Monthly Subscription
----**************************
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Monthly Subscription' AND	[Key] = 'Recipient_Address')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'datafactory@orangebank.com;datafactoryext@orangebank.com'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Monthly Subscription'
			AND	[Key] = 'Recipient_Address'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Monthly Subscription',
			'Recipient_Address',
			'datafactory@orangebank.com;datafactoryext@orangebank.com'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Monthly Subscription' AND	[Key] = 'Bcc_Recipient_Address')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'datafactory@orangebank.com;datafactoryext@orangebank.com'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Monthly Subscription'
			AND	[Key] = 'Bcc_Recipient_Address'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Monthly Subscription',
			'Bcc_Recipient_Address',
			'datafactory@orangebank.com;datafactoryext@orangebank.com'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Monthly Subscription' AND	[Key] = 'Include_Report')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'True'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Monthly Subscription'
			AND	[Key] = 'Include_Report'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Monthly Subscription',
			'Include_Report',
			'True'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Monthly Subscription' AND	[Key] = 'Email_Subject')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'Etat d''avancement des traitements mensuels de SIDP'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Monthly Subscription'
			AND	[Key] = 'Email_Subject'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Monthly Subscription',
			'Email_Subject',
			'Etat d''avancement des traitements mensuels de SIDP'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Monthly Subscription' AND	[Key] = 'Email_Body')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = '<html><body>Bonjour,<br/><br/>Veuillez trouver ci-dessous l''état d''avancement des traitements mensuels de SIDP.<br/><br/>Ce rapport est consultable en suivant <a href=''https://obsidprs.intra.orangebank.com/ReportServer_INSTP00/Pages/ReportViewer.aspx?/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP&P_FRQN=M''>ce lien</a>.<br/><br/>Cordialement,<br/>DataFactory Team</body></html>'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Monthly Subscription'
			AND	[Key] = 'Email_Body'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Monthly Subscription',
			'Email_Body',
			'<html><body>Bonjour,<br/><br/>Veuillez trouver ci-dessous l''état d''avancement des traitements mensuels de SIDP.<br/><br/>Ce rapport est consultable en suivant <a href=''https://obsidprs.intra.orangebank.com/ReportServer_INSTP00/Pages/ReportViewer.aspx?/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP&P_FRQN=M''>ce lien</a>.<br/><br/>Cordialement,<br/>DataFactory Team</body></html>'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Monthly Subscription' AND	[Key] = 'Include_Link')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'False'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Monthly Subscription'
			AND	[Key] = 'Include_Link'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Monthly Subscription',
			'Include_Link',
			'False'
		)
	END
GO
IF EXISTS (SELECT 1 FROM dbo.PARAM_SSRS_SUBSCRIPTION WHERE ReportName = 'R_Etat_Avancement_Chargement_SIDP' AND SubscriptionName = 'Monthly Subscription' AND	[Key] = 'Frequency')
	BEGIN
		UPDATE dbo.PARAM_SSRS_SUBSCRIPTION
		SET [Value] = 'M'
		WHERE
			ReportName = 'R_Etat_Avancement_Chargement_SIDP'
			AND SubscriptionName = 'Monthly Subscription'
			AND	[Key] = 'Frequency'
	END
ELSE
	BEGIN
		INSERT INTO dbo.PARAM_SSRS_SUBSCRIPTION
		VALUES
		(
			'R_Etat_Avancement_Chargement_SIDP',
			'/SIDP/Etat%20Avancement%20SIDP/R_Etat_Avancement_Chargement_SIDP',
			'Monthly Subscription',
			'Frequency',
			'M'
		)
	END
GO
--********************************************