﻿USE [$(DataFactoryDatabaseName)]
GO
--------------------------------------------------------
--On vide la table SIDP_QOD_LISTE_CONTROLE
TRUNCATE TABLE [dbo].[SIDP_QOD_LISTE_CONTROLE]
GO
--------------------------------------------------------
--On désactive les ID_CONTROL automatique
SET IDENTITY_INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ON 
GO
/***********************************************************************************************************************************/
--AJOUTER ICI TOUS LES NOUVEAUX CONTROLES QUI SERONT APPEléS PAR par la procédure stockée suivante dans les packages:
--exec [dbo].[PKG_QOD_EXEC_FAMILLE_CONTROLE] 'FAMILLE_CONTROL'
--Le FAMILLE_CONTROL correspond à la variable @[User::NOM_FICH_OU_TABL] dans le package SSIS
--"EXEC dbo.PKG_QOD_EXEC_FAMILLE_CONTROLE '"+ @[User::NOM_FICH_OU_TABL] +"'"

--AJOUTER à la fin de ce script de NOUVEAUX CONTROLES: 
--!!Ne pas oublier de mettre en dur ID_CONTROL incrémenté de +1 par rapport au dernier contrôle ID_CONTROL de prod
/************************************************************************************************************************************/
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (1, N'DWH_CONTROL_LIEN_EQUIPEMENT_1.01', N'1 - Bloquant', N'Comparaison DATAHUB/DWH table DWH_LIEN_EQUIPEMENT', N'Ce contrôle permet de vérifier que le nombre d''enregistrements extraits des tables sources correspond au nombre d''enregistrements insérés dans la table cible', N'SELECT NB_LIGNES_SRC AS output_col1,
	   NB_LIGNES_TGT AS output_col2,
	   NULL AS output_col3,
	   NULL AS output_col4,
	   NULL AS output_col5,
	   NULL AS output_col6,
	   NULL AS output_col7	   
FROM
(
 SELECT MAX(NbRecordsCollected) AS NB_LIGNES_SRC,
 	   COUNT(NUMR_COMP) AS NB_LIGNES_TGT  
 FROM
 (
   SELECT PARAM_ALIM.DAT_VALD, REQ_DATA_LOG.NbRecordsCollected FROM [$(DataFactoryDatabaseName)].dbo.PARAM_ALIM PARAM_ALIM	WITH(NOLOCK)
   JOIN 
   (
     SELECT ETLName, ProcessingStartDate, NbRecordsCollected 
     FROM
     (
       SELECT ETLName, ProcessingStartDate, NbRecordsCollected, ROW_NUMBER() OVER(ORDER BY ProcessingStartDate DESC) as FLG_DER_TRT
       FROM [$(DataHubDatabaseName)].dbo.DATA_LOG	WITH(NOLOCK)
       WHERE FILEORTABLENAME = ''DWH_LIEN_EQUIPEMENT'' 
     ) DATA_LOG
     WHERE DATA_LOG.FLG_DER_TRT = 1
   ) REQ_DATA_LOG
   ON PARAM_ALIM.NOM_BATCH = REQ_DATA_LOG.ETLName
   AND PARAM_ALIM.DAT_DEBT_TRTM = REQ_DATA_LOG.ProcessingStartDate
 )REQ_DATES_VALID
 LEFT JOIN [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].dbo.DWH_LIEN_EQUIPEMENT LIEN_EQUI 	WITH(NOLOCK)
 ON LIEN_EQUI.DAT_OBSR = REQ_DATES_VALID.DAT_VALD
) REQ_FINAL
WHERE NB_LIGNES_SRC != NB_LIGNES_TGT', N'DWH', N'DWH_LIEN_EQUIPEMENT', N'Technique - Contrôle intégration', N'''Différence de lignes entre DATAHUB, ''+cast(output_col1 as char)+'', et DWH :''+cast(output_col2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, 290)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (2, N'DWH_CONTROL_CHEQUIER_1.01', N'1 - Bloquant', N'Comparaison DATAHUB/DWH table DWH_CHEQUIER', N'Ce contrôle permet de vérifier que le nombre d''enregistrements extraits des tables sources correspond au nombre d''enregistrements insérés dans la table cible', N'SELECT NB_LIGNES_SRC AS output_col1,
       NB_LIGNES_TGT AS output_col2,
       NULL AS output_col3,
       NULL AS output_col4,
       NULL AS output_col5,
       NULL AS output_col6,
       NULL AS output_col7
FROM  
(   
 SELECT MAX(NbRecordsCollected) AS NB_LIGNES_SRC,
 COUNT(NUMR_COMP) AS NB_LIGNES_TGT
 FROM
 (
  SELECT PARAM_ALIM.DAT_VALD, REQ_DATA_LOG.NbRecordsCollected 
  FROM [$(DataFactoryDatabaseName)].dbo.PARAM_ALIM PARAM_ALIM 	WITH(NOLOCK)    
  JOIN      
  (
   SELECT ETLName, ProcessingStartDate, NbRecordsCollected
   FROM
   (
    SELECT ETLName, ProcessingStartDate, NbRecordsCollected, ROW_NUMBER() OVER(ORDER BY ProcessingStartDate DESC) as FLG_DER_TRT
    FROM [$(DataHubDatabaseName)].dbo.DATA_LOG	WITH(NOLOCK)
    WHERE FILEORTABLENAME = ''DWH_CHEQUIER''
   ) DATA_LOG
   WHERE DATA_LOG.FLG_DER_TRT = 1     
  ) REQ_DATA_LOG
  ON PARAM_ALIM.NOM_BATCH = REQ_DATA_LOG.ETLName
  AND PARAM_ALIM.DAT_DEBT_TRTM = REQ_DATA_LOG.ProcessingStartDate
 )REQ_DATES_VALID   
LEFT JOIN [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].dbo.DWH_CHEQUIER CHEQUIER	WITH(NOLOCK)
ON CHEQUIER.DAT_OBSR = REQ_DATES_VALID.DAT_VALD) REQ_FINAL  
WHERE NB_LIGNES_SRC != NB_LIGNES_TGT', N'DWH', N'DWH_CHEQUIER', N'Technique - Contrôle intégration', N'''Différence de lignes entre DATAHUB, ''+cast(output_col1 as char)+'', et DWH :''+cast(output_col2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, 286)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (21, N'DWH_CONTROL_PERS_SCR_PRTT_1.0.0', N'1 - Bloquant', N'Comparaison Code / libellé du réseau', N'Ce contrôle compare la valeur du code réseau avec son libellé exemple le code 03 est Groupama', N'SELECT IDNT_SF_PERS as output_col1, COD_RES_DIST_PRNC as output_col2, LIBL_RES_DIST_PRNC as output_col3,  
      Null as output_col4 ,     Null as output_col5,         null as output_col6,     null as output_col7    
		 FROM [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].[dbo].[DWH_PERS_SCR_PRTT]     	WITH(NOLOCK)
		 Where  LIBL_RES_DIST is not NULL 
		 AND COD_RES_DIST  NOT IN (''01'',''02'',''03'')', N'DWH', N'DWH_PERS_SCR_PRTT', N'Technique - Contrôle intégration', N'''Le libellé ne correspond pas au code réseau''', N'Identifiant SF de la personne (IDNT_SF_PERS)', N'Code Réseau distributeur principal (COD_RES_DIST_PRNC)', N'Libellé Réseau distributeur principal (LIBL_RES_DIST_PRNC)', NULL, NULL, NULL, NULL, 1, NULL, 307)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (3, N'DWH_CONTROL_CARTE_1.02', N'1 - Bloquant', N'COMPARAISON FLG ACTIF et nombre de lignes dernière dat_vald', N'Ce contrôle compare FLG ACTIF et nombre de lignes dernière dat_vald', N'SELECT src_nb AS output_col1,
       cible_nb AS output_col2,
       NULL AS output_col3,
       NULL AS output_col4,
       NULL AS output_col5,
       NULL AS output_col6,
       NULL AS output_col7
FROM (
        (SELECT COUNT (*) AS src_nb
         FROM [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].[dbo].[DWH_CARTE]	WITH(NOLOCK)
         WHERE FLG_ENRG_COUR = 1) src
      JOIN
        (SELECT COUNT(*) cible_nb
         FROM [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].[dbo].[DWH_CARTE]	WITH(NOLOCK)
         WHERE DAT_OBSR =
             (SELECT max(DAT_VALD)
              FROM [$(DataFactoryDatabaseName)].dbo.PARAM_ALIM	WITH(NOLOCK)
              WHERE NOM_BATCH LIKE ''%CARTE%'')) cible ON src.src_nb != cible.cible_nb)', N'DWH', N'DWH_CARTE', N'Technique - Contrôle intégration', N'''Différence de lignes entre FLG_ACTIF, ''+cast(OUTPUT_COL1 as char)+'', et DERN_DAT_VALD:''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 0, NULL, 284)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (22, N'DWH_CONTROL_PERSONNE_1.0.75', N'1 - Bloquant', N'Test l''égalité ou non des valeurs du nom et prénom de différentes sources ', N'Ce contrôle teste les valeurs de sf_nom, fe_prnm et FE_Nom', N'SELECT DISTINCT [SF_IDNT_SF_PERS] as output_col1,
	[SA_NUMR_CLNT_SAB] as output_col2,
	[FE_IDNT_FE_CLNT] as output_col3,
	SF_NOM as output_col4,
	FE_NOM as output_col5 ,
	SF_PRNM as output_col6,  
	FE_PRNM as output_col7    
	FROM [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].[dbo].[DWH_PERSONNE]  
	Where fe_prnm  is not null 
	AND lower(sf_nom) <> lower(fe_prnm) + '' '' + lower(FE_Nom)', N'DWH', N'DWH_PERSONNE', N'Technique - Contrôle RG', N'''Les valeurs des noms et prénoms ne sont pas les memes pour ''', N'Identifiant SF de la personne (SF_IDNT_SF_PERS)', N'Numéro Client SAB (SA_NUMR_CLNT_SAB)', N'Identifiant FE Client (FE_IDNT_FE_CLNT)', N'Nom (SF_NOM)', N'Nom (FE_NOM)', N'Prénom (SF_PRNM)', N'Prénom (FE_PRNM)', 0, NULL, 297)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (5, N'DWH_CONTROL_CARTE_1.01', N'1 - Bloquant', N'Comparaison DATAHUB/DWH table DWH_CARTE', N'Ce contrôle permet de vérifier que le nombre d''enregistrements extraits des tables sources correspond au nombre d''enregistrements insérés dans la table cible', N'SELECT NB_LIGNES_SRC AS output_col1,
       NB_LIGNES_TGT AS output_col2,
       NULL AS output_col3,
       NULL AS output_col4,
       NULL AS output_col5,
       NULL AS output_col6,
       NULL AS output_col7
FROM  
(   
 SELECT MAX(NbRecordsCollected) AS NB_LIGNES_SRC,
 COUNT(NUMR_COMP) AS NB_LIGNES_TGT
 FROM
 (
  SELECT PARAM_ALIM.DAT_VALD, REQ_DATA_LOG.NbRecordsCollected 
  FROM [$(DataFactoryDatabaseName)].dbo.PARAM_ALIM PARAM_ALIM     
  JOIN      
  (
   SELECT ETLName, ProcessingStartDate, NbRecordsCollected
   FROM
   (
    SELECT ETLName, ProcessingStartDate, NbRecordsCollected, ROW_NUMBER() OVER(ORDER BY ProcessingStartDate DESC) as FLG_DER_TRT
    FROM [$(DataHubDatabaseName)].dbo.DATA_LOG	WITH(NOLOCK)
    WHERE FILEORTABLENAME = ''DWH_CARTE''
   ) DATA_LOG
   WHERE DATA_LOG.FLG_DER_TRT = 1     
  ) REQ_DATA_LOG
  ON PARAM_ALIM.NOM_BATCH = REQ_DATA_LOG.ETLName
  AND PARAM_ALIM.DAT_DEBT_TRTM = REQ_DATA_LOG.ProcessingStartDate
 )REQ_DATES_VALID   
LEFT JOIN [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].dbo.DWH_CARTE CARTE
ON CARTE.DAT_OBSR = REQ_DATES_VALID.DAT_VALD) REQ_FINAL  
WHERE NB_LIGNES_SRC != NB_LIGNES_TGT', N'DWH', N'DWH_CARTE', N'Technique - Contrôle intégration', N'''Différence de lignes entre DATAHUB, ''+cast(output_col1 as char)+'', et DWH :''+cast(output_col2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, 284)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (4, N'DWH_CONTROL_EQPM_CREDIT_CONSO_SL_1.0.1', N'1 - Bloquant', N'Comparaison du nombre de lignes entre [PV_FE_CONSUMERLOANEQUIPMENT] et [DWH_EQPM_CREDIT_CONSO_SL]', N'Ce contrôle vérifie que le nombre de lignes entre la source et la cible est le même', N'SELECT source.ID_FE_CONSUMERLOANEQUIPMENT as output_col1, 
source.nb  as output_col2, 
cible.nb  as output_col3,
null as output_col4,
null  as output_col5,
null  as output_col6,
null  as output_col7
from 
(
select ID_FE_CONSUMERLOANEQUIPMENT, COUNT(*) as nb from [$(DataHubDatabaseName)].[dbo].[PV_FE_CONSUMERLOANEQUIPMENT] LE	WITH(NOLOCK)
join ( 
select dat_vald from [$(DataFactoryDatabaseName)].dbo.PARAM_ALIM p	WITH(NOLOCK)
where p.nom_batch = ''PKGDWH_SSIS_EQPM_CREDIT_CONSO_SL'' 
and DAT_DEBT_TRTM = (select DAT_DEBT_TRTM from [$(DataFactoryDatabaseName)].dbo.PARAM_ALIM p 	WITH(NOLOCK) join [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].[dbo].DWH_PARAM_DATE_VALIDITE d	WITH(NOLOCK) on p.DAT_VALD = d.DAT_VALD and p.nom_batch = ''PKGDWH_SSIS_EQPM_CREDIT_CONSO_SL'' )
) param 
on param.Dat_VALD BETWEEN CAST(LE.Validity_StartDate AS DATE) AND DATEADD([day], - 1, CAST(LE.Validity_EndDate AS DATE))
group by ID_FE_CONSUMERLOANEQUIPMENT
) source
join 
(
select IDNT_EQPM_PRDT_CRDT, COUNT(*) as nb from [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].[dbo].[DWH_EQPM_CREDIT_CONSO_SL] 	WITH(NOLOCK)
WHERE DAT_OBSR in (
select dat_vald from [$(DataFactoryDatabaseName)].dbo.PARAM_ALIM p
where p.nom_batch = ''PKGDWH_SSIS_EQPM_CREDIT_CONSO_SL'' 
and DAT_DEBT_TRTM = (select DAT_DEBT_TRTM from [$(DataFactoryDatabaseName)].dbo.PARAM_ALIM p join [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].[dbo].DWH_PARAM_DATE_VALIDITE d on p.DAT_VALD = d.DAT_VALD and p.nom_batch = ''PKGDWH_SSIS_EQPM_CREDIT_CONSO_SL'' )
)
group by IDNT_EQPM_PRDT_CRDT
) cible on source.ID_FE_CONSUMERLOANEQUIPMENT = cible.IDNT_EQPM_PRDT_CRDT
where source.nb != cible.nb', N'DWH', N'DWH_EQPM_CREDIT_CONSO_SL', N'Technique - Contrôle intégration', N'''Différence de lignes entre PV_FE_CONSUMERLOANEQUIPMENT, ''+cast(OUTPUT_COL2 as char)+'', et DWH_EQPM_CREDIT_CONSO_SL :''+cast(OUTPUT_COL3 as char)+'' sur ID SL equipement :''+cast(OUTPUT_COL1 as char)', N'ID Service Layer équipement', N'Date Validité', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, 1, NULL, 302)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (24, N'DWH_CONTROL_PERSONNE_1.0.2', N'1 - Bloquant', N'Test l''existence ou non de la Date Valide', N'Ce contrôle teste la valeur de la DAT_OBSR', N'SELECT FLG_CLNT as output_col1, [SF_IDNT_SF_PERS] as output_col2, [SA_NUMR_CLNT_SAB] as output_col3, [FE_IDNT_FE_CLNT] as output_col4, FE_IDNT_SF_PERS as output_col5, 
  null as output_col6 , null as output_col7  FROM [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].[dbo].[DWH_PERSONNE]
  WHERE (FLG_CLNT is  NULL OR FLG_ANCN_CLNT IS NULL)', N'DWH', N'DWH_PERSONNE', N'Technique - Contrôle RG', N'''La valeur de la colonne FLG_CLNT est obligatoire''', N'Flag Client (FLG_CLNT)', N'Identifiant SF de la personne (SF_IDNT_SF_PERS)', N'Numéro Client SAB (SA_NUMR_CLNT_SAB)', N'Identifiant FE Client (FE_IDNT_FE_CLNT)', N'Identifiant SF de la personne (FE_IDNT_SF_PERS)', NULL, NULL, 0, NULL, 297)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (25, N'DWH_CONTROL_PERSONNE_1.0.3', N'1 - Bloquant', N'Test l''égalité ou non de la valeur de l''ancienneté OB', N'Ce contrôle teste la valeur de l''ancienneté OB', N'SELECT SF_ANCNT_OB as output_col1, DATEDIFF(month,  SF_DAT_ENTR_RELT_OB,DAT_OBSR) AS output_col2, [SF_IDNT_SF_PERS] as output_col3, 
  [SA_NUMR_CLNT_SAB] as output_col4, [FE_IDNT_FE_CLNT] as output_col5, DAT_OBSR as output_col6,  SF_DAT_ENTR_RELT_OB as output_col7 
  FROM [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].[dbo].[DWH_PERSONNE]
  WHERE (SF_ANCNT_OB<> DATEDIFF(month,  SF_DAT_ENTR_RELT_OB,DAT_OBSR) ) ', N'DWH', N'DWH_PERSONNE', N'Technique - Contrôle RG', N'''La valeur de la colonne ancienneté OB est erronée''', N'Ancienneté OB (SF_ANCNT_OB)', N'Différence de mois entre date ancienneté et obs', N'Identifiant SF de la personne (SF_IDNT_SF_PERS)', N'Numéro Client SAB (SA_NUMR_CLNT_SAB)', N'Identifiant FE Client (FE_IDNT_FE_CLNT)', N'Date d''''observation (DAT_OBSR)', N'Date d''''entrée en relation OB (SF_DAT_ENTR_RELT_OB)', 0, NULL, 297)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (6, N'DWH_CONTROL_M_EQPM_COMP_ACTV_1.01', N'1 - Bloquant', N'Comparaison DATAHUB/DWH table DWH_M_EQPM_COMP_ACTV', N'Ce contrôle permet de vérifier que le nombre d''enregistrements extraits des tables sources correspond au nombre d''enregistrements insérés dans la table cible', N'SELECT NB_LIGNES_SRC AS output_col1,
       NB_LIGNES_TGT AS output_col2,
       NULL AS output_col3,
       NULL AS output_col4,
       NULL AS output_col5,
       NULL AS output_col6,
       NULL AS output_col7
FROM  
(   
 SELECT MAX(NbRecordsCollected) AS NB_LIGNES_SRC,
 COUNT(NUMR_COMP) AS NB_LIGNES_TGT
 FROM
 (
  SELECT PARAM_ALIM.DAT_VALD, REQ_DATA_LOG.NbRecordsCollected 
  FROM [$(DataFactoryDatabaseName)].dbo.PARAM_ALIM PARAM_ALIM     
  JOIN      
  (
   SELECT ETLName, ProcessingStartDate, NbRecordsCollected
   FROM
   (
    SELECT ETLName, ProcessingStartDate, NbRecordsCollected, ROW_NUMBER() OVER(ORDER BY ProcessingStartDate DESC) as FLG_DER_TRT
    FROM [$(DataHubDatabaseName)].dbo.DATA_LOG	WITH(NOLOCK)
    WHERE FILEORTABLENAME = ''DWH_M_EQPM_COMP_ACTV''
   ) DATA_LOG
   WHERE DATA_LOG.FLG_DER_TRT = 1     
  ) REQ_DATA_LOG
  ON PARAM_ALIM.NOM_BATCH = REQ_DATA_LOG.ETLName
  AND PARAM_ALIM.DAT_DEBT_TRTM = REQ_DATA_LOG.ProcessingStartDate
 )REQ_DATES_VALID   
LEFT JOIN [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].dbo.DWH_M_EQPM_COMP_ACTV ACTV
ON ACTV.DAT_OBSR = REQ_DATES_VALID.DAT_VALD) REQ_FINAL  
WHERE NB_LIGNES_SRC != NB_LIGNES_TGT', N'DWH', N'DWH_M_EQPM_COMP_ACTV', N'Technique - Contrôle intégration', N'''Différence de lignes entre DATAHUB, ''+cast(output_col1 as char)+'', et DWH :''+cast(output_col2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 294)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (7, N'DWH_SOUSCRIPTEUR_1.0.1', N'1 - Bloquant', N'Comparaison DATAHUB/DWH table DWH_SOUSCRIPTEUR', N'Ce contrôle permet de vérifier que le nombre d''enregistrements extraits des tables sources correspond au nombre d''enregistrements insérés dans la table cible', N'SELECT NB_LIGNES_SRC AS output_col1,
	   NB_LIGNES_TGT AS output_col2,
	   NULL AS output_col3,
	   NULL AS output_col4,
	   NULL AS output_col5,
	   NULL AS output_col6,
	   NULL AS output_col7	   
FROM
(
 SELECT MAX(NbRecordsCollected) AS NB_LIGNES_SRC,
 	   COUNT(DWH_SOUSCRIPTEUR.DAT_OBSR) AS NB_LIGNES_TGT  
 FROM
 (
   SELECT PARAM_ALIM.DAT_VALD, REQ_DATA_LOG.NbRecordsCollected FROM [$(DataFactoryDatabaseName)].dbo.PARAM_ALIM PARAM_ALIM
   JOIN 
   (
     SELECT ETLName, ProcessingStartDate, NbRecordsCollected 
     FROM
     (
       SELECT ETLName, ProcessingStartDate, NbRecordsCollected, ROW_NUMBER() OVER(ORDER BY ProcessingStartDate DESC) as FLG_DER_TRT
       FROM [$(DataHubDatabaseName)].dbo.DATA_LOG	WITH(NOLOCK)
       WHERE FILEORTABLENAME = ''DWH_SOUSCRIPTEUR'' 
     ) DATA_LOG
     WHERE DATA_LOG.FLG_DER_TRT = 1
   ) REQ_DATA_LOG
   ON PARAM_ALIM.NOM_BATCH = REQ_DATA_LOG.ETLName
   AND PARAM_ALIM.DAT_DEBT_TRTM = REQ_DATA_LOG.ProcessingStartDate
 )REQ_DATES_VALID
 LEFT JOIN [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].dbo.DWH_SOUSCRIPTEUR 
 ON DWH_SOUSCRIPTEUR.DAT_OBSR = REQ_DATES_VALID.DAT_VALD
) REQ_FINAL
WHERE NB_LIGNES_SRC != NB_LIGNES_TGT', N'DWH', N'DWH_SOUSCRIPTEUR', N'Technique - Contrôle intégration', N'''Différence de lignes entre DATAHUB, ''+cast(output_col1 as char)+'', et DWH :''+cast(output_col2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, 292)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (26, N'DWH_CONTROL_PERS_SCR_PRTT_1.0.2', N'1 - Bloquant', N'Comparaison Code princ / libellé du réseau princ', N'Ce contrôle compare la valeur du code réseau principale avec son libellé exemple le code 03 est GroupAma', N'SELECT [IDNT_SF_PERS] as output_col1, [COD_RES_DIST_PRNC] as output_col2, [LIBL_RES_DIST_PRNC] as output_col3,  
     null as output_col4,     Null as output_col5,         null as output_col6,     null as output_col7    
		 FROM [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].[dbo].[DWH_PERS_SCR_PRTT]     
		 Where  [LIBL_RES_DIST_PRNC] is not NULL 
		 AND [COD_RES_DIST_PRNC]  NOT IN (''01'',''02'',''03'')', N'DWH', N'DWH_PERS_SCR_PRTT', N'Technique - Contrôle RG', N'''Le libellé ne correspond pas au code réseau''', N'Identifiant SF de la personne (IDNT_SF_PERS)', N'Code Réseau distributeur principal (COD_RES_DIST_PRNC)', N'Libellé Réseau distributeur principal (LIBL_RES_DIST_PRNC)', NULL, NULL, NULL, NULL, 1, NULL, 307)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (8, N'DWH_SOUSCRIPTEUR_1.0.2', N'2 - Warning', N'Vérification que DWH_SOUSCRIPTEUR contient que des souscripteurs', N'Ce contrôle permet de vérifier que la table DWH_SOUSCRIPTEUR contient uniquement des contacts de type Souscripteur', N'SELECT cible.DAT_OBSR as output_col1,
	cible.IDNT_SOUS as output_col2,
	cible.IDNT_TYP_ENRG as output_col3,
	ref.NAME_SF as output_col4,
	null as output_col5,
	null as output_col6,
	null as output_col7
	from [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].[dbo].DWH_SOUSCRIPTEUR cible
	inner join 
	 (
	   select PARAM_ALIM.DAT_VALD from [$(DataFactoryDatabaseName)].dbo.PARAM_ALIM PARAM_ALIM
	   inner join 
	   (
		 select ETLName, ProcessingStartDate 
		 from
		 (
		   select ETLName, ProcessingStartDate, row_number() over(order by ProcessingStartDate desc) as FLG_DER_TRT
		   from [$(DataHubDatabaseName)].dbo.DATA_LOG	WITH(NOLOCK)
		   where FILEORTABLENAME = ''DWH_SOUSCRIPTEUR'' 
		 ) DATA_LOG
		 where DATA_LOG.FLG_DER_TRT = 1
	   ) REQ_DATA_LOG
	   on PARAM_ALIM.NOM_BATCH = REQ_DATA_LOG.ETLName
	   and PARAM_ALIM.DAT_DEBT_TRTM = REQ_DATA_LOG.ProcessingStartDate
	 ) REQ_DATES_VALID
	on cible.DAT_OBSR = REQ_DATES_VALID.DAT_VALD
	INNER JOIN [$(DataHubDatabaseName)].[dbo].[REF_RECORDTYPE] ref
	ON cible.IDNT_TYP_ENRG = ref.ID_SF
	WHERE ref.NAME_SF != ''Souscripteur''', N'DWH', N'DWH_SOUSCRIPTEUR', N'Technique - Contrôle RG', N'''Le contact, de la table DWH_SOUSCRIPTEUR, ''+cast(OUTPUT_COL2 as char)+'', n''''est pas de type Souscripteur :''+cast(OUTPUT_COL4 as char)', N'Date Observation (DAT_OBSR)', N'Identifiant souscripteur (IDNT_SOUS)', N'Identifiant type enregistrement (IDNT_TYP_ENRG)', N'Nom (NAME_SF)', NULL, NULL, NULL, 1, NULL, 292)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (27, N'IWD_DATAHUB_CONTROL_1.0.8', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_DIALING_MODE', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_DIALING_MODE', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_DIALING_MODE'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_DIALING_MODE) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (28, N'IWD_DATAHUB_CONTROL_1.0.9', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_GIDB_GC_ACTION_CODE', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_GIDB_GC_ACTION_CODE', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_GIDB_GC_ACTION_CODE'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_GIDB_GC_ACTION_CODE) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (29, N'IWD_DATAHUB_CONTROL_1.0.1', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_ANCHOR_FLAGS', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_ANCHOR_FLAGS', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_ANCHOR_FLAGS'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_ANCHOR_FLAGS) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (9, N'DWH_CONTROL_OPPORTUNITE_1.0.2', N'2 - Warning', N'Vérification du canal de finalisation de l''opportunité', N'Ce contrôle vérifie que le canal de finalisation de l''opportunité n''est renseigné que si l''opportunité est clôturée', N'SELECT cible.DAT_OBSR as output_col1,
			cible.IDNT_OPPR as output_col2,
			cible.STD_VENT as output_col3,
			cible.CANL_INTR_FINL as output_col4,
			source.StageName as output_col5,
			source.canal_fin as output_col6,
			source.Validity_StartDate as output_col7
	from [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].[dbo].[DWH_OPPORTUNITE] cible
	inner join 
	 (
	   select PARAM_ALIM.DAT_VALD from [$(DataFactoryDatabaseName)].dbo.PARAM_ALIM PARAM_ALIM
	   inner join 
	   (
		 select ETLName, ProcessingStartDate 
		 from
		 (
		   select ETLName, ProcessingStartDate, row_number() over(order by ProcessingStartDate desc) as FLG_DER_TRT
		   from [$(DataHubDatabaseName)].dbo.DATA_LOG	WITH(NOLOCK)
		   where FILEORTABLENAME = ''DWH_OPPORTUNITE'' 
		 ) DATA_LOG
		 where DATA_LOG.FLG_DER_TRT = 1
	   ) REQ_DATA_LOG
	   on PARAM_ALIM.NOM_BATCH = REQ_DATA_LOG.ETLName
	   and PARAM_ALIM.DAT_DEBT_TRTM = REQ_DATA_LOG.ProcessingStartDate
	 ) REQ_DATES_VALID
	on cible.DAT_OBSR = REQ_DATES_VALID.DAT_VALD
	left join
	(
		select Id_SF, 
		StageName, 
		CommercialOfferName__c, 
		LeadSource, 
		CASE
           WHEN StageName IN (''10'',''11'',''12'',''15'',''16'')
                AND CommercialOfferCode__c  IN (''OPAP'',''OPTR'',''OPVN'',''OPVO'') THEN LeadSource
           WHEN StageName IN (''09'',''10'',''11'',''13'')
                AND CommercialOfferCode__c  not in (''OPAP'',''OPTR'',''OPVN'',''OPVO'')THEN LeadSource
           ELSE NULL
       END AS canal_fin, 
		Validity_StartDate, 
		Validity_EndDate,
		row_number() over(partition by id_sf, cast(Validity_StartDate as date) order by Validity_StartDate desc) as rang
		from [$(DataHubDatabaseName)].dbo.pv_sf_opportunity_history
	) source 
	on source.Id_SF = cible.IDNT_OPPR
	and cast(source.Validity_StartDate as date) = cible.DAT_OBSR 
	where source.rang = 1
	and cible.CANL_INTR_FINL != source.canal_fin', N'DWH', N'DWH_OPPORTUNITE', N'Technique - Contrôle RG', N'''L''''opportunité, de la table DWH_OPPORTUNITY, ''+cast(OUTPUT_COL2 as char)+'', présente un canal de finalisation :''+cast(OUTPUT_COL4 as char)+'', ne correspondant pas au canal de finalisation dans PV_SF_OPPORTUNITY_HISTORY''+cast(OUTPUT_COL6 as char)', N'Date Observation (DAT_OBSR)', N'Identifiant opportunite (IDNT_OPPR)', N'Stade de vente (STD_VENT)', N'Canal interaction de finalisation (CANL_INTR_FINL)', N'Stade de vente (StageName)', N'(canal_fin)', N'(Validity_StartDate)', 1, NULL, 291)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (30, N'IWD_DATAHUB_CONTROL_1.0.2', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_ATTEMPT_DISPOSITION', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_ATTEMPT_DISPOSITION', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_ATTEMPT_DISPOSITION'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_ATTEMPT_DISPOSITION) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (82, N'IWD_DATAHUB_CONTROL_1.0.3', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_CALL_RESULT', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_CALL_RESULT', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_CALL_RESULT'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_CALL_RESULT) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (10, N'DWH_PRODUIT_SOUSCRIT_1.0.1', N'1 - Bloquant', N'Comparaison DATAHUB/DWH table DWH_PRODUIT_SOUSCRIT', N'Ce contrôle permet de vérifier que le nombre d''enregistrements extraits des tables sources correspond au nombre d''enregistrements insérés dans la table cible', N'SELECT NB_LIGNES_SRC AS output_col1,
	   NB_LIGNES_TGT AS output_col2,
	   NULL AS output_col3,
	   NULL AS output_col4,
	   NULL AS output_col5,
	   NULL AS output_col6,
	   NULL AS output_col7	   
FROM
(
 SELECT MAX(NbRecordsCollected) AS NB_LIGNES_SRC,
 	   COUNT(DWH_PRODUIT_SOUSCRIT.DAT_OBSR) AS NB_LIGNES_TGT  
 FROM
 (
   SELECT PARAM_ALIM.DAT_VALD, REQ_DATA_LOG.NbRecordsCollected FROM [$(DataFactoryDatabaseName)].dbo.PARAM_ALIM PARAM_ALIM
   JOIN 
   (
     SELECT ETLName, ProcessingStartDate, NbRecordsCollected 
     FROM
     (
       SELECT ETLName, ProcessingStartDate, NbRecordsCollected, ROW_NUMBER() OVER(ORDER BY ProcessingStartDate DESC) as FLG_DER_TRT
       FROM [$(DataHubDatabaseName)].dbo.DATA_LOG	WITH(NOLOCK)
       WHERE FILEORTABLENAME = ''DWH_PRODUIT_SOUSCRIT'' 
     ) DATA_LOG
     WHERE DATA_LOG.FLG_DER_TRT = 1
   ) REQ_DATA_LOG
   ON PARAM_ALIM.NOM_BATCH = REQ_DATA_LOG.ETLName
   AND PARAM_ALIM.DAT_DEBT_TRTM = REQ_DATA_LOG.ProcessingStartDate
 )REQ_DATES_VALID
 LEFT JOIN [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].dbo.DWH_PRODUIT_SOUSCRIT 
 ON DWH_PRODUIT_SOUSCRIT.DAT_OBSR = REQ_DATES_VALID.DAT_VALD
) REQ_FINAL
WHERE NB_LIGNES_SRC != NB_LIGNES_TGT', N'DWH', N'DWH_PRODUIT_SOUSCRIT', N'Technique - Contrôle intégration', N'''Différence de lignes entre DATAHUB, ''+cast(output_col1 as char)+'', et DWH :''+cast(output_col2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, 299)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (11, N'DWH_CONTROL_OPPORTUNITE_1.0.1', N'2 - Warning', N'Vérification cohérence stade de vente entre les tables PV_SF_OPPORTUNITY et DWH_OPPORTUNITE', N'Ce contrôle compare le stade de vente entre DWH_OPPORTUNITE et pour le dernier stade de vente de l ''opportunité dans la table PV_SF_OPPORTUNITY du DATAHUB', N'SELECT cible.DAT_OBSR as output_col1,
		cible.IDNT_OPPR as output_col2,
		cible.STD_VENT as output_col3,
		source.StageName as output_col4,
		source.Validity_StartDate as output_col5,
		null as output_col6,
		null as output_col7
	from [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].[dbo].[DWH_OPPORTUNITE] cible
	inner join 
	 (
	   select PARAM_ALIM.DAT_VALD from [$(DataFactoryDatabaseName)].dbo.PARAM_ALIM PARAM_ALIM
	   inner join 
	   (
		 select ETLName, ProcessingStartDate 
		 from
		 (
		   select ETLName, ProcessingStartDate, row_number() over(order by ProcessingStartDate desc) as FLG_DER_TRT
		   from [$(DataHubDatabaseName)].dbo.DATA_LOG	WITH(NOLOCK)
		   where FILEORTABLENAME = ''DWH_OPPORTUNITE'' 
		 ) DATA_LOG
		 where DATA_LOG.FLG_DER_TRT = 1
	   ) REQ_DATA_LOG
	   on PARAM_ALIM.NOM_BATCH = REQ_DATA_LOG.ETLName
	   and PARAM_ALIM.DAT_DEBT_TRTM = REQ_DATA_LOG.ProcessingStartDate
	 ) REQ_DATES_VALID
	on cible.DAT_OBSR = REQ_DATES_VALID.DAT_VALD
	left join
	(
		select Id_SF, StageName, Validity_StartDate, Validity_EndDate,
		row_number() over(partition by id_sf, cast(Validity_StartDate as date) order by Validity_StartDate desc) as rang
		from [$(DataHubDatabaseName)].dbo.pv_sf_opportunity_history
	) source 
	on source.Id_SF = cible.IDNT_OPPR
	and cast(source.Validity_StartDate as date) = cible.DAT_OBSR 
	where source.rang = 1
	and cible.STD_VENT != source.StageName', N'DWH', N'DWH_OPPORTUNITE', N'Technique - Contrôle RG', N'''L''''opportunité, de la table DWH_OPPORTUNITY, ''+cast(OUTPUT_COL2 as char)+'', présente un stade de vente :''+cast(OUTPUT_COL3 as char)+'', ne correspondant pas au dernier stade de vente dans PV_SF_OPPORTUNITY_HISTORY''+cast(OUTPUT_COL4 as char)', N'Date Observation (DAT_OBSR)', N'Identifiant opportunite (IDNT_OPPR)', N'Stade de vente (STD_VENT)', N'Stade de vente (StageName)', N'(Validity_StartDate)', NULL, NULL, 1, NULL, 291)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (12, N'DWH_CONTROL_OPPORTUNITE_1.0.5', N'2 - Warning', N'Vérification du délai de déblocage de l''opportunite crédit', N'Ce contrôle vérifie que le délai de déblocage de l ''opportunité de type crédit est égal à la différence entre la date d accord et la date de déblocage', N'SELECT cible.DAT_OBSR as output_col1,
			cible.IDNT_OPPR as output_col2,
			cible.DAT_ACCR_CRDT as output_col3,
			cible.DAT_DEBL_CRDT as output_col4,
			cible.DEL_DEBL_CRDT as output_col5,
			source.delai_deblocage_credit as output_col6,
			source.Validity_StartDate as output_col7
	from [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].[dbo].[DWH_OPPORTUNITE] cible
	inner join 
	 (
	   select PARAM_ALIM.DAT_VALD from [$(DataFactoryDatabaseName)].dbo.PARAM_ALIM PARAM_ALIM
	   inner join 
	   (
		 select ETLName, ProcessingStartDate 
		 from
		 (
		   select ETLName, ProcessingStartDate, row_number() over(order by ProcessingStartDate desc) as FLG_DER_TRT
		   from [$(DataHubDatabaseName)].dbo.DATA_LOG	WITH(NOLOCK)
		   where FILEORTABLENAME = ''DWH_OPPORTUNITE'' 
		 ) DATA_LOG
		 where DATA_LOG.FLG_DER_TRT = 1
	   ) REQ_DATA_LOG
	   on PARAM_ALIM.NOM_BATCH = REQ_DATA_LOG.ETLName
	   and PARAM_ALIM.DAT_DEBT_TRTM = REQ_DATA_LOG.ProcessingStartDate
	 ) REQ_DATES_VALID
	on cible.DAT_OBSR = REQ_DATES_VALID.DAT_VALD
	left join
	(
		select Id_SF, 
		StageName, 
		CommercialOfferName__c, 
		LoanAcceptationDate__c, 
		ReleaseDate__c, 
		CASE
           WHEN StageName = ''15''
                AND CommercialOfferCode__c  in (''OPAP'',''OPTR'',''OPVN'',''OPVO'') THEN DATEDIFF(DD, LoanAcceptationDate__c, ReleaseDate__c)
			ELSE NULL
       END AS delai_deblocage_credit, 
		Validity_StartDate, 
		Validity_EndDate,
		row_number() over(partition by id_sf, cast(Validity_StartDate as date) order by Validity_StartDate desc) as rang
		from [$(DataHubDatabaseName)].dbo.pv_sf_opportunity_history
	) source 
	on source.Id_SF = cible.IDNT_OPPR
	and cast(source.Validity_StartDate as date) = cible.DAT_OBSR 
	where source.rang = 1
	and cible.DEL_DEBL_CRDT != source.delai_deblocage_credit', N'DWH', N'DWH_OPPORTUNITE', N'Technique - Contrôle RG', N'''L''''opportunité, de la table DWH_OPPORTUNITY, ''+cast(OUTPUT_COL2 as char)+'', présente un délai de déblocage :''+cast(OUTPUT_COL5 as char)+'', ne correspondant pas au délai de déblocage dans PV_SF_OPPORTUNITY_HISTORY''+cast(OUTPUT_COL6 as char)', N'Date Observation (DAT_OBSR)', N'Identifiant opportunite (IDNT_OPPR)', N'Date accord du credit (DAT_ACCR_CRDT)', N'Date deblocage du credit (DAT_DEBL_CRDT)', N'Delai deblocage (DEL_DEBL_CRDT)', N'Delai deblocage (delai_deblocage_credit)', N'(Validity_StartDate)', 1, NULL, 291)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (385, N'OAV_DATAHUB_CONTROL_1.0.35', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  PAYS', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table PAYS', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from PAYS'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_PAYS group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (386, N'OAV_DATAHUB_CONTROL_1.0.36', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  PERIODICITES', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table PERIODICITES', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from PERIODICITES'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_PERIODICITES group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (13, N'DWH_CONTROL_OPPORTUNITE_1.0.9', N'2 - Warning', N'Vérification de l''âge d''une opportunité', N'Ce contrôle vérifie que si l''opportunité est clôturé, l''âge correspond à la différence entre la date de création et de fermeture sinon différence entre la date de création et date de validité', N'SELECT cible.DAT_OBSR as output_col1,
			cible.IDNT_OPPR as output_col2,
			cible.STD_VENT as output_col3,
			cible.AGE_OPPR as output_col4,
			source.StageName as output_col5,
			source.age_opport as output_col6,
			source.Validity_StartDate as output_col7
	from [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].[dbo].[DWH_OPPORTUNITE] cible
	inner join 
	 (
	   select PARAM_ALIM.DAT_VALD from [$(DataFactoryDatabaseName)].dbo.PARAM_ALIM PARAM_ALIM
	   inner join 
	   (
		 select ETLName, ProcessingStartDate 
		 from
		 (
		   select ETLName, ProcessingStartDate, row_number() over(order by ProcessingStartDate desc) as FLG_DER_TRT
		   from [$(DataHubDatabaseName)].dbo.DATA_LOG	WITH(NOLOCK)
		   where FILEORTABLENAME = ''DWH_OPPORTUNITE'' 
		 ) DATA_LOG
		 where DATA_LOG.FLG_DER_TRT = 1
	   ) REQ_DATA_LOG
	   on PARAM_ALIM.NOM_BATCH = REQ_DATA_LOG.ETLName
	   and PARAM_ALIM.DAT_DEBT_TRTM = REQ_DATA_LOG.ProcessingStartDate
	 ) REQ_DATES_VALID
	on cible.DAT_OBSR = REQ_DATES_VALID.DAT_VALD
	left join
	(
		select tps.StandardDate,
		opp.Id_SF, 
		opp.StageName, 
		opp.CommercialOfferName__c,  
		opp.CreatedDate, 
		opp.CloseDate,
		CASE
           WHEN opp.CommercialOfferCode__c  IN (''OPAP'',''OPTR'',''OPVN'',''OPVO'') AND opp.StageName IN (''10'',''11'',''12'',''15'',''16'') 
		   THEN DATEDIFF(DD, opp.CreatedDate, opp.CloseDate)
           WHEN opp.CommercialOfferCode__c  NOT IN (''OPAP'',''OPTR'',''OPVN'',''OPVO'') AND opp.StageName IN (''09'',''10'',''11'',''13'') 
		   THEN DATEDIFF(DD, opp.CreatedDate, opp.CloseDate)
           ELSE DATEDIFF(DD, opp.CreatedDate, tps.StandardDate)
       END AS age_opport, 
		Validity_StartDate, 
		Validity_EndDate,
		row_number() over(partition by opp.id_sf, cast(opp.Validity_StartDate as date) order by opp.Validity_StartDate desc) as rang
		from [$(DataHubDatabaseName)].dbo.pv_sf_opportunity_history opp
		left join (SELECT StandardDate
					FROM [$(DataFactoryDatabaseName)].dbo.REF_TEMPS 
					) as tps
		on cast(opp.Validity_StartDate as date) <= tps.StandardDate 
			and tps.StandardDate < (case when cast(opp.Validity_EndDate as date)=''9999-12-31'' then GETDATE() 
									else cast(opp.Validity_EndDate as date) end)
	) source 
	on source.Id_SF = cible.IDNT_OPPR
	and source.StandardDate = cible.DAT_OBSR 
	where source.rang = 1
	and cible.AGE_OPPR != source.age_opport', N'DWH', N'DWH_OPPORTUNITE', N'Technique - Contrôle RG', N'''L''''opportunité, de la table DWH_OPPORTUNITY, ''+cast(OUTPUT_COL2 as char)+'', présente un âge :''+cast(OUTPUT_COL4 as char)+'', ne correspondant pas à l''''âge dans PV_SF_OPPORTUNITY_HISTORY''+cast(OUTPUT_COL6 as char)', N'Date Observation (DAT_OBSR)', N'Identifiant opportunite (IDNT_OPPR)', N'Stade de vente (STD_VENT)', N'Age opportunité (AGE_OPPR)', N'Stade de vente (StageName)', N'Age opportunité (age_opport)', N'(Validity_StartDate)', 1, NULL, 291)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (387, N'OAV_DATAHUB_CONTROL_1.0.37', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  PRE_ATTRIBUTION', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table PRE_ATTRIBUTION', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from PRE_ATTRIBUTION'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_PRE_ATTRIBUTION group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (71, N'IWD_DATAHUB_CONTROL_1.0.60', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de fait IWD_CALLING_LIST_METRIC_FACT', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_CALLING_LIST_METRIC_FACT', N'SELECT src_nb as output_col1,  
	 cible_nb as output_col2,  
	 null as output_col3,  
	 null as output_col4,  
	 null as output_col5,  
	 null as output_col6,  
	 null as output_col7  
	 from (select * 
 from (select count(*) as src_nb from openquery(INFOMART,''select CREATE_AUDIT_KEY,UPDATE_AUDIT_KEY from V_CALLING_LIST_METRIC_FACT'') f 
join [$(DataHubDatabaseName)].[dbo].IWD_CTL_AUDIT_LOG a on (F.UPDATE_AUDIT_KEY = A.AUDIT_KEY OR (F.CREATE_AUDIT_KEY = A.AUDIT_KEY AND F.UPDATE_AUDIT_KEY =0) )
join [$(DataHubDatabaseName)].[dbo].IWD_PILOTAGE_CHARGEMENT pil 
on (a.created between pil.dte_deb_periode and pil.dte_fin_periode)) a
	 join   (select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_CALLING_LIST_METRIC_FACT) b  on a.src_nb != b.cible_nb) t', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (290, N'VOL_COLLECT_30_RAW_SA_REFERENTIEL', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table RAW_SA_REFERENTIEL', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table RAW_SA_REFERENTIEL dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
DISTINCT TODAY.PROcessingType as output_col1,
''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
''RAW_SA_REFERENTIEL'' as output_col3,
TODAY.NBR_LIGNE as output_col4,
LastMonth.NBR_LIGNE as output_col5,
NULL as output_col6,
30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''RAW_SA_REFERENTIEL'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FI_XSIMCPO_00''
	AND FileOrTablename= ''RAW_SA_REFERENTIEL'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 	WITH(NOLOCK)
					WHERE ETLName = ''PKGCO_SSIS_FI_XSIMCPO_00''   AND FileOrTablename= ''RAW_SA_REFERENTIEL''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 121)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (388, N'OAV_DATAHUB_CONTROL_1.0.39', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  REGIME_MATRIMONIAL', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table REGIME_MATRIMONIAL', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from REGIME_MATRIMONIAL'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_REGIME_MATRIMONIAL group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (19, N'DWH_EQUIPEMENT_CREDIT_CONSO_1.0.2', N'2 - Warning', N'Vérification du délai de décaissement du crédit FI', N'Ce contrôle vérifie que le délai de décaissement du crédit FI est égal à la différence entre la date d accord tokos et la date de dernier déblocage', N'SELECT cible.DAT_OBSR as output_col1,
			cible.REFR_CONT as output_col2,
			cible.DEL_DECS as output_col3,
			source.DELAI_DECAISSEMENT as output_col4,
			source.Validity_StartDate as output_col5, 
			null as output_col6,
			null as output_col7
	from [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].[dbo].[DWH_EQUIPEMENT_CREDIT_CONSO] cible
	inner join 
	 (
	   select PARAM_ALIM.DAT_VALD from [$(DataFactoryDatabaseName)].dbo.PARAM_ALIM PARAM_ALIM
	   inner join 
	   (
		 select ETLName, ProcessingStartDate 
		 from
		 (
		   select ETLName, ProcessingStartDate, row_number() over(order by ProcessingStartDate desc) as FLG_DER_TRT
		   from [$(DataHubDatabaseName)].dbo.DATA_LOG	WITH(NOLOCK)
		   where FILEORTABLENAME = ''DWH_EQUIPEMENT_CREDIT_CONSO'' 
		 ) DATA_LOG
		 where DATA_LOG.FLG_DER_TRT = 1
	   ) REQ_DATA_LOG
	   on PARAM_ALIM.NOM_BATCH = REQ_DATA_LOG.ETLName
	   and PARAM_ALIM.DAT_DEBT_TRTM = REQ_DATA_LOG.ProcessingStartDate
	 ) REQ_DATES_VALID
	on cible.DAT_OBSR = REQ_DATES_VALID.DAT_VALD
	left join
	(
		select REF_CPT, 
		[DTE_ACC_TOKOS], 
		[DTE_DER_DEB], 
		DATEDIFF(DD,[DTE_ACC_TOKOS],[DTE_DER_DEB]) as DELAI_DECAISSEMENT, 
		Validity_StartDate, 
		Validity_EndDate
		from [$(DataHubDatabaseName)].dbo.VW_PV_FI_Q_DES
	) source 
	on source.REF_CPT = cible.REFR_CONT
	and  cible.DAT_OBSR between cast(source.Validity_StartDate as date) and cast(source.Validity_EndDate as date)
	where cible.DEL_DECS != source.DELAI_DECAISSEMENT', N'DWH', N'DWH_EQUIPEMENT_CREDIT_CONSO', N'Technique - Contrôle RG', N'''Le crédit, de la table DWH_EQUIPEMENT_CREDIT_CONSO, ''+cast(OUTPUT_COL2 as char)+'', présente un délai de décaissement :''+cast(OUTPUT_COL3 as char)+'', ne correspondant pas au délai de décaissement dans VW_PV_FI_Q_DES''+cast(OUTPUT_COL4 as char)', N'Date Observation (DAT_OBSR)', N'Référence contrat (REFR_CONT)', N'Delai decaissement (DEL_DECS)', N'Delai decaissement (DELAI_DECAISSEMENT)', N'(Validity_StartDate)', NULL, NULL, 1, NULL, 298)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (291, N'VOL_COLLECT_30_REF_CASE_PRIORITE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CASE_PRIORITE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CASE_PRIORITE dans le package PKGCO_SSIS_SF_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_00'' as output_col2,
	''REF_CASE_PRIORITE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_PRIORITE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_PRIORITE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_PRIORITE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 185)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (52, N'IWD_DATAHUB_CONTROL_1.0.68', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de fait IWD_IXN_RESOURCE_STATE_FACT', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_IXN_RESOURCE_STATE_FACT', N'SELECT src_nb as output_col1,  
	 cible_nb as output_col2,  
	 null as output_col3,  
	 null as output_col4,  
	 null as output_col5,  
	 null as output_col6,  
	 null as output_col7  
	 from (select * 
 from (select count(*) as src_nb from openquery(INFOMART,''select CREATE_AUDIT_KEY,UPDATE_AUDIT_KEY from V_IXN_RESOURCE_STATE_FACT'') f
join [$(DataHubDatabaseName)].[dbo].IWD_CTL_AUDIT_LOG a on (F.UPDATE_AUDIT_KEY = A.AUDIT_KEY OR (F.CREATE_AUDIT_KEY = A.AUDIT_KEY AND F.UPDATE_AUDIT_KEY =0) )
join [$(DataHubDatabaseName)].[dbo].IWD_PILOTAGE_CHARGEMENT pil 
on (a.created between pil.dte_deb_periode and pil.dte_fin_periode)) a
	 join   (select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_IXN_RESOURCE_STATE_FACT) b  on a.src_nb != b.cible_nb) t', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (424, N'VOL_COLLECT_5_PV_SA_M_FUSIONCPTOPE', N'2 - Warning', N'Contrôle de la volumétrie des données lors de la collecte de la table PV_SA_M_FUSIONCPTOPE', N'Contrôle si la variation en valeur absolue de la volumétrie des données par rapport à celle du mois précédent de la table PV_SA_M_FUSIONCPTOPE dans le package PKGCO_SSIS_SA_XSIMC010_00 est supérieure à 5%', N' SELECT 
 DISTINCT THIS_MONTH.PROcessingType as output_col1,   
 ''PKGCO_SSIS_SA_XSIMC010_00'' as output_col2,   
 ''PV_SA_M_FUSIONCPTOPE'' as output_col3,   
 THIS_MONTH.NBR_LIGNE as output_col4,   
 LAST_MONTH.NBR_LIGNE as output_col5,   
 NULL as output_col6,   
 5 AS output_col7  
FROM
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.Data_Log   	WITH(NOLOCK)
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMC010_00'' 
 AND FileOrTablename= ''PV_SA_M_FUSIONCPTOPE''   
 AND ProcessingStatus = ''OK'' 
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE() ) AS DATE )
) THIS_MONTH  
INNER JOIN   
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.Data_Log   	WITH(NOLOCK)
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMC010_00''   
 AND FileOrTablename= ''PV_SA_M_FUSIONCPTOPE''
 AND ProcessingStatus = ''OK''  
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE(), -1 ) AS DATE)
) LAST_MONTH   
on (ABS((THIS_MONTH.NBR_LIGNE - LAST_MONTH.NBR_LIGNE)) > (LAST_MONTH.NBR_LIGNE * 5 / 100))', N'DataHub', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport au mois précédent (5%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes mois courant', N'Nb lignes mois précédent', NULL, N'Pourcentage dépassé', 1, NULL, 449)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (69, N'IWD_DATAHUB_CONTROL_1.0.63', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de fait IWD_CAMPAIGN_GROUP_STATE_FACT', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_CAMPAIGN_GROUP_STATE_FACT', N'SELECT src_nb as output_col1,  
	 cible_nb as output_col2,  
	 null as output_col3,  
	 null as output_col4,  
	 null as output_col5,  
	 null as output_col6,  
	 null as output_col7  
	 from (select * 
 from (select count(*) as src_nb from openquery(INFOMART,''select CREATE_AUDIT_KEY,UPDATE_AUDIT_KEY from V_CAMPAIGN_GROUP_STATE_FACT '') f 
join [$(DataHubDatabaseName)].[dbo].IWD_CTL_AUDIT_LOG a on (F.UPDATE_AUDIT_KEY = A.AUDIT_KEY OR (F.CREATE_AUDIT_KEY = A.AUDIT_KEY AND F.UPDATE_AUDIT_KEY =0) )
join [$(DataHubDatabaseName)].[dbo].IWD_PILOTAGE_CHARGEMENT pil 
on (a.created between pil.dte_deb_periode and pil.dte_fin_periode)) a
	 join   (select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_CAMPAIGN_GROUP_STATE_FACT) b  on a.src_nb != b.cible_nb) t', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (425, N'VOL_COLLECT_10_PV_SA_M_CARTE', N'2 - Warning', N'Contrôle de la volumétrie des données lors de la collecte de la table PV_SA_M_CARTE', N'Contrôle si la variation en valeur absolue de la volumétrie des données par rapport à celle du mois précédent de la table PV_SA_M_CARTE dans le package PKGCO_SSIS_SA_XSIMCAR0_00 est supérieure à 10%', N' SELECT 
 DISTINCT THIS_MONTH.PROcessingType as output_col1,   
 ''PKGCO_SSIS_SA_XSIMCAR0_00'' as output_col2,   
 ''PV_SA_M_CARTE'' as output_col3,   
 THIS_MONTH.NBR_LIGNE as output_col4,   
 LAST_MONTH.NBR_LIGNE as output_col5,   
 NULL as output_col6,   
 10 AS output_col7  
FROM
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.Data_Log   	WITH(NOLOCK)
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMCAR0_00'' 
 AND FileOrTablename= ''PV_SA_M_CARTE''   
 AND ProcessingStatus = ''OK'' 
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE() ) AS DATE )
) THIS_MONTH  
INNER JOIN   
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.Data_Log   	WITH(NOLOCK)
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMCAR0_00''   
 AND FileOrTablename= ''PV_SA_M_CARTE''
 AND ProcessingStatus = ''OK''  
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE(), -1 ) AS DATE)
) LAST_MONTH   
on (ABS((THIS_MONTH.NBR_LIGNE - LAST_MONTH.NBR_LIGNE)) > (LAST_MONTH.NBR_LIGNE * 10 / 100))', N'DataHub', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport au mois précédent (10%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes mois courant', N'Nb lignes mois précédent', NULL, N'Pourcentage dépassé', 1, NULL, 450)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (53, N'IWD_DATAHUB_CONTROL_1.0.69', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de fait IWD_MEDIATION_SEGMENT_FACT', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_MEDIATION_SEGMENT_FACT', N'SELECT src_nb as output_col1,  
	 cible_nb as output_col2,  
	 null as output_col3,  
	 null as output_col4,  
	 null as output_col5,  
	 null as output_col6,  
	 null as output_col7  
	 from (select * 
 from (select count(*) as src_nb from openquery(INFOMART,''select CREATE_AUDIT_KEY,UPDATE_AUDIT_KEY from V_MEDIATION_SEGMENT_FACT'') f
join [$(DataHubDatabaseName)].[dbo].IWD_CTL_AUDIT_LOG a on (F.UPDATE_AUDIT_KEY = A.AUDIT_KEY OR (F.CREATE_AUDIT_KEY = A.AUDIT_KEY AND F.UPDATE_AUDIT_KEY =0) )
join [$(DataHubDatabaseName)].[dbo].IWD_PILOTAGE_CHARGEMENT pil 
on (a.created between pil.dte_deb_periode and pil.dte_fin_periode)) a
	 join   (select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_MEDIATION_SEGMENT_FACT) b  on a.src_nb != b.cible_nb) t', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (426, N'VOL_COLLECT_5_PV_SA_M_CHEQUIER', N'2 - Warning', N'Contrôle de la volumétrie des données lors de la collecte de la table PV_SA_M_CHEQUIER', N'Contrôle si la variation en valeur absolue de la volumétrie des données par rapport à celle du mois précédent de la table PV_SA_M_CHEQUIER dans le package PKGCO_SSIS_SA_XSIMCHQ0_00 est supérieure à 5%', N' SELECT 
 DISTINCT THIS_MONTH.PROcessingType as output_col1,   
 ''PKGCO_SSIS_SA_XSIMCHQ0_00'' as output_col2,   
 ''PV_SA_M_CHEQUIER'' as output_col3,   
 THIS_MONTH.NBR_LIGNE as output_col4,   
 LAST_MONTH.NBR_LIGNE as output_col5,   
 NULL as output_col6,   
 5 AS output_col7  
FROM
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.Data_Log   	WITH(NOLOCK)
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMCHQ0_00'' 
 AND FileOrTablename= ''PV_SA_M_CHEQUIER''   
 AND ProcessingStatus = ''OK'' 
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE() ) AS DATE )
) THIS_MONTH  
INNER JOIN   
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.Data_Log   	WITH(NOLOCK)
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMCHQ0_00''   
 AND FileOrTablename= ''PV_SA_M_CHEQUIER''
 AND ProcessingStatus = ''OK''  
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE(), -1 ) AS DATE)
) LAST_MONTH   
on (ABS((THIS_MONTH.NBR_LIGNE - LAST_MONTH.NBR_LIGNE)) > (LAST_MONTH.NBR_LIGNE * 5 / 100))', N'DataHub', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport au mois précédent (5%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes mois courant', N'Nb lignes mois précédent', NULL, N'Pourcentage dépassé', 1, NULL, 451)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (427, N'VOL_COLLECT_5_PV_SA_M_M_COMECHL', N'2 - Warning', N'Contrôle de la volumétrie des données lors de la collecte de la table PV_SA_M_M_COMECHL', N'Contrôle si la variation en valeur absolue de la volumétrie des données par rapport à celle du mois précédent de la table PV_SA_M_M_COMECHL dans le package PKGCO_SSIS_SA_XSIMCOM0_00 est supérieure à 5%', N' SELECT 
 DISTINCT THIS_MONTH.PROcessingType as output_col1,   
 ''PKGCO_SSIS_SA_XSIMCOM0_00'' as output_col2,   
 ''PV_SA_M_M_COMECHL'' as output_col3,   
 THIS_MONTH.NBR_LIGNE as output_col4,   
 LAST_MONTH.NBR_LIGNE as output_col5,   
 NULL as output_col6,   
 5 AS output_col7  
FROM
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.Data_Log   	WITH(NOLOCK)
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMCOM0_00'' 
 AND FileOrTablename= ''PV_SA_M_M_COMECHL''   
 AND ProcessingStatus = ''OK'' 
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE() ) AS DATE )
) THIS_MONTH  
INNER JOIN   
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.Data_Log   	WITH(NOLOCK)
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMCOM0_00''   
 AND FileOrTablename= ''PV_SA_M_M_COMECHL''
 AND ProcessingStatus = ''OK''  
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE(), -1 ) AS DATE)
) LAST_MONTH   
on (ABS((THIS_MONTH.NBR_LIGNE - LAST_MONTH.NBR_LIGNE)) > (LAST_MONTH.NBR_LIGNE * 5 / 100))', N'DataHub', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport au mois précédent (5%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes mois courant', N'Nb lignes mois précédent', NULL, N'Pourcentage dépassé', 1, NULL, 453)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (70, N'IWD_DATAHUB_CONTROL_1.0.64', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de fait IWD_CONTACT_ATTEMPT_FACT', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_CONTACT_ATTEMPT_FACT', N'SELECT src_nb as output_col1,  
	 cible_nb as output_col2,  
	 null as output_col3,  
	 null as output_col4,  
	 null as output_col5,  
	 null as output_col6,  
	 null as output_col7  
	 from (select * 
 from (select count(*) as src_nb from openquery(INFOMART,''select CREATE_AUDIT_KEY,UPDATE_AUDIT_KEY from V_CONTACT_ATTEMPT_FACT '') f 
join [$(DataHubDatabaseName)].[dbo].IWD_CTL_AUDIT_LOG a on (F.UPDATE_AUDIT_KEY = A.AUDIT_KEY OR (F.CREATE_AUDIT_KEY = A.AUDIT_KEY AND F.UPDATE_AUDIT_KEY =0) )
join [$(DataHubDatabaseName)].[dbo].IWD_PILOTAGE_CHARGEMENT pil 
on (a.created between pil.dte_deb_periode and pil.dte_fin_periode)) a
	 join   (select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_CONTACT_ATTEMPT_FACT) b  on a.src_nb != b.cible_nb) t', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (428, N'VOL_COLLECT_5_PV_SA_M_COMPTE', N'2 - Warning', N'Contrôle de la volumétrie des données lors de la collecte de la table PV_SA_M_COMPTE', N'Contrôle si la variation en valeur absolue de la volumétrie des données par rapport à celle du mois précédent de la table PV_SA_M_COMPTE dans le package PKGCO_SSIS_SA_XSIMCPT0_00 est supérieure à 5%', N' SELECT 
 DISTINCT THIS_MONTH.PROcessingType as output_col1,   
 ''PKGCO_SSIS_SA_XSIMCPT0_00'' as output_col2,   
 ''PV_SA_M_COMPTE'' as output_col3,   
 THIS_MONTH.NBR_LIGNE as output_col4,   
 LAST_MONTH.NBR_LIGNE as output_col5,   
 NULL as output_col6,   
 5 AS output_col7  
FROM
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.Data_Log   	WITH(NOLOCK)
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMCPT0_00'' 
 AND FileOrTablename= ''PV_SA_M_COMPTE''   
 AND ProcessingStatus = ''OK'' 
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE() ) AS DATE )
) THIS_MONTH  
INNER JOIN   
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.Data_Log   	WITH(NOLOCK)
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMCPT0_00''   
 AND FileOrTablename= ''PV_SA_M_COMPTE''
 AND ProcessingStatus = ''OK''  
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE(), -1 ) AS DATE)
) LAST_MONTH   
on (ABS((THIS_MONTH.NBR_LIGNE - LAST_MONTH.NBR_LIGNE)) > (LAST_MONTH.NBR_LIGNE * 5 / 100))', N'DataHub', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport au mois précédent (5%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes mois courant', N'Nb lignes mois précédent', NULL, N'Pourcentage dépassé', 1, NULL, 455)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (384, N'OAV_DATAHUB_CONTROL_1.0.33', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  PARAMETRES', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table PARAMETRES', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from PARAMETRES'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_PARAMETRES group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (62, N'IWD_DATAHUB_CONTROL_1.0.66', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de fait IWD_INTERACTION_RESOURCE_FACT', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_INTERACTION_RESOURCE_FACT', N'SELECT src_nb as output_col1,  
	 cible_nb as output_col2,  
	 null as output_col3,  
	 null as output_col4,  
	 null as output_col5,  
	 null as output_col6,  
	 null as output_col7  
	 from (select * 
 from (select count(*) as src_nb from openquery(INFOMART,''select CREATE_AUDIT_KEY,UPDATE_AUDIT_KEY from V_INTERACTION_RESOURCE_FACT'')  f
join [$(DataHubDatabaseName)].[dbo].IWD_CTL_AUDIT_LOG a on (F.UPDATE_AUDIT_KEY = A.AUDIT_KEY OR (F.CREATE_AUDIT_KEY = A.AUDIT_KEY AND F.UPDATE_AUDIT_KEY =0) )
join [$(DataHubDatabaseName)].[dbo].IWD_PILOTAGE_CHARGEMENT pil 
on (a.created between pil.dte_deb_periode and pil.dte_fin_periode)) a
	 join   (select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_INTERACTION_RESOURCE_FACT) b  on a.src_nb != b.cible_nb) t', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 1, NULL, 392)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (429, N'VOL_COLLECT_5_PV_SA_M_LKAUTENC', N'2 - Warning', N'Contrôle de la volumétrie des données lors de la collecte de la table PV_SA_M_LKAUTENC', N'Contrôle si la variation en valeur absolue de la volumétrie des données par rapport à celle du mois précédent de la table PV_SA_M_LKAUTENC dans le package PKGCO_SSIS_SA_XSIMENA0_00 est supérieure à 5%', N' SELECT 
 DISTINCT THIS_MONTH.PROcessingType as output_col1,   
 ''PKGCO_SSIS_SA_XSIMENA0_00'' as output_col2,   
 ''PV_SA_M_LKAUTENC'' as output_col3,   
 THIS_MONTH.NBR_LIGNE as output_col4,   
 LAST_MONTH.NBR_LIGNE as output_col5,   
 NULL as output_col6,   
 5 AS output_col7  
FROM
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.Data_Log   	WITH(NOLOCK)
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMENA0_00'' 
 AND FileOrTablename= ''PV_SA_M_LKAUTENC''   
 AND ProcessingStatus = ''OK'' 
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE() ) AS DATE )
) THIS_MONTH  
INNER JOIN   
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.Data_Log   	WITH(NOLOCK)
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMENA0_00''   
 AND FileOrTablename= ''PV_SA_M_LKAUTENC''
 AND ProcessingStatus = ''OK''  
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE(), -1 ) AS DATE)
) LAST_MONTH   
on (ABS((THIS_MONTH.NBR_LIGNE - LAST_MONTH.NBR_LIGNE)) > (LAST_MONTH.NBR_LIGNE * 5 / 100))', N'DataHub', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport au mois précédent (5%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes mois courant', N'Nb lignes mois précédent', NULL, N'Pourcentage dépassé', 1, NULL, 458)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (237, N'VOL_COLLECT_30_DIM_CASE_ORIGINE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table DIM_CASE_ORIGINE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table DIM_CASE_ORIGINE dans le package PKGCO_SSIS_SF_REFERENTIEL_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_01'' as output_col2,
	''DIM_CASE_ORIGINE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_ORIGINE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_ORIGINE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_ORIGINE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 210)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (238, N'VOL_COLLECT_30_DIM_CASE_PLAN_ACTION', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table DIM_CASE_PLAN_ACTION', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table DIM_CASE_PLAN_ACTION dans le package PKGCO_SSIS_SF_REFERENTIEL_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_01'' as output_col2,
	''DIM_CASE_PLAN_ACTION'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_PLAN_ACTION'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_PLAN_ACTION'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_PLAN_ACTION''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 211)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (239, N'VOL_COLLECT_30_DIM_CASE_QUALIF_REP2', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table DIM_CASE_QUALIF_REP2', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table DIM_CASE_QUALIF_REP2 dans le package PKGCO_SSIS_SF_REFERENTIEL_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_01'' as output_col2,
	''DIM_CASE_QUALIF_REP2'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_QUALIF_REP2'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_QUALIF_REP2'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_QUALIF_REP2''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 215)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (240, N'VOL_COLLECT_30_DIM_CASE_TYPE_CLI', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table DIM_CASE_TYPE_CLI', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table DIM_CASE_TYPE_CLI dans le package PKGCO_SSIS_SF_REFERENTIEL_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_01'' as output_col2,
	''DIM_CASE_TYPE_CLI'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_TYPE_CLI'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_TYPE_CLI'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_TYPE_CLI''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 216)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (241, N'VOL_COLLECT_30_IWD_CALLING_LIST_TO_CAMP_FACT_', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_CALLING_LIST_TO_CAMP_FACT_', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_CALLING_LIST_TO_CAMP_FACT_ dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_CALLING_LIST_TO_CAMP_FACT_'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (242, N'VOL_COLLECT_30_IWD_INTERACTION_RESOURCE_STATE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_INTERACTION_RESOURCE_STATE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_INTERACTION_RESOURCE_STATE dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_INTERACTION_RESOURCE_STATE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 365)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (243, N'VOL_COLLECT_30_DIM_MOTIF_REF', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table DIM_MOTIF_REF', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table DIM_MOTIF_REF dans le package PKGCO_SSIS_SF_REFERENTIEL_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_01'' as output_col2,
	''DIM_MOTIF_REF'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_MOTIF_REF'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_MOTIF_REF'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_MOTIF_REF''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 220)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (159, N'VOL_COLLECT_30_RAW_FE_ENROLMENTFOLDER', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table RAW_FE_ENROLMENTFOLDER', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table RAW_FE_ENROLMENTFOLDER dans le package PKGCO_SSIS_FE_ENROLMENTFOLDER_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_ENROLMENTFOLDER_00'' as output_col2,
	''RAW_FE_ENROLMENTFOLDER'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_ENROLMENTFOLDER_00''
	AND FileOrTablename= ''RAW_FE_ENROLMENTFOLDER'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_ENROLMENTFOLDER_00''
	AND FileOrTablename= ''RAW_FE_ENROLMENTFOLDER'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_ENROLMENTFOLDER_00''
	AND FileOrTablename= ''RAW_FE_ENROLMENTFOLDER''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 67)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (244, N'VOL_COLLECT_30_DIM_MOTIF_SS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table DIM_MOTIF_SS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table DIM_MOTIF_SS dans le package PKGCO_SSIS_SF_REFERENTIEL_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_01'' as output_col2,
	''DIM_MOTIF_SS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_MOTIF_SS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_MOTIF_SS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_MOTIF_SS''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 221)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (245, N'VOL_COLLECT_30_REF_CODE_SEGMENTS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CODE_SEGMENTS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CODE_SEGMENTS dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_CODE_SEGMENTS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CODE_SEGMENTS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CODE_SEGMENTS'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 	WITH(NOLOCK)
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_CODE_SEGMENTS''  
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
) LastMonth 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100) )
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 132)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (246, N'VOL_COLLECT_30_REF_CODE_SERVICES_CONVENTIONS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CODE_SERVICES_CONVENTIONS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CODE_SERVICES_CONVENTIONS dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_CODE_SERVICES_CONVENTIONS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CODE_SERVICES_CONVENTIONS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CODE_SERVICES_CONVENTIONS'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 	WITH(NOLOCK)
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_CODE_SERVICES_CONVENTIONS''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
) LastMonth 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100) )
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 133)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (247, N'VOL_COLLECT_30_DIM_STADE_VTE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table DIM_STADE_VTE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table DIM_STADE_VTE dans le package PKGCO_SSIS_SF_REFERENTIEL_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_01'' as output_col2,
	''DIM_STADE_VTE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_STADE_VTE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_STADE_VTE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_STADE_VTE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 224)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (248, N'VOL_COLLECT_30_DIM_STAT_MARITAL', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table DIM_STAT_MARITAL', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table DIM_STAT_MARITAL dans le package PKGCO_SSIS_SF_REFERENTIEL_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_01'' as output_col2,
	''DIM_STAT_MARITAL'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_STAT_MARITAL'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_STAT_MARITAL'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_STAT_MARITAL''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 225)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (249, N'VOL_COLLECT_30_REF_ETABLISSEMENT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_ETABLISSEMENT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_ETABLISSEMENT dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_ETABLISSEMENT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_ETABLISSEMENT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_ETABLISSEMENT'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 	WITH(NOLOCK)
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_ETABLISSEMENT''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 138)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (266, N'VOL_COLLECT_30_PV_SA_Q_SERVICE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SA_Q_SERVICE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SA_Q_SERVICE dans le package PKGCO_SSIS_SA_XSIQABS0_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_XSIQABS0_00'' as output_col2,
	''PV_SA_Q_SERVICE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQABS0_00''
	AND FileOrTablename= ''PV_SA_Q_SERVICE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQABS0_00''
	AND FileOrTablename= ''PV_SA_Q_SERVICE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQABS0_00''
	AND FileOrTablename= ''PV_SA_Q_SERVICE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 481)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (267, N'VOL_COLLECT_30_PV_SA_Q_MOUVEMENT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SA_Q_MOUVEMENT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SA_Q_MOUVEMENT dans le package PKGCO_SSIS_SA_XSIQMVT0_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_XSIQMVT0_00'' as output_col2,
	''PV_SA_Q_MOUVEMENT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQMVT0_00''
	AND FileOrTablename= ''PV_SA_Q_MOUVEMENT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQMVT0_00''
	AND FileOrTablename= ''PV_SA_Q_MOUVEMENT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQMVT0_00''
	AND FileOrTablename= ''PV_SA_Q_MOUVEMENT''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 488)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (268, N'VOL_COLLECT_30_PV_SA_Q_REF', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SA_Q_REF', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SA_Q_REF dans le package PKGCO_SSIS_SA_XSIQRCL0_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_XSIQRCL0_00'' as output_col2,
	''PV_SA_Q_REF'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQRCL0_00''
	AND FileOrTablename= ''PV_SA_Q_REF'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQRCL0_00''
	AND FileOrTablename= ''PV_SA_Q_REF'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQRCL0_00''
	AND FileOrTablename= ''PV_SA_Q_REF''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 489)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (269, N'VOL_COLLECT_30_REF_CANAL_DIS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CANAL_DIS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CANAL_DIS dans le package PKGCO_SSIS_SF_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_00'' as output_col2,
	''REF_CANAL_DIS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CANAL_DIS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CANAL_DIS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CANAL_DIS''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 175)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (270, N'VOL_COLLECT_30_REF_CANAL_INT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CANAL_INT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CANAL_INT dans le package PKGCO_SSIS_SF_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_00'' as output_col2,
	''REF_CANAL_INT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CANAL_INT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CANAL_INT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CANAL_INT''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 176)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (271, N'VOL_COLLECT_30_PV_SA_Q_ABONNEMENT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SA_Q_ABONNEMENT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SA_Q_ABONNEMENT dans le package PKGCO_SSIS_SA_XSIQABO0_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_XSIQABO0_00'' as output_col2,
	''PV_SA_Q_ABONNEMENT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQABO0_00''
	AND FileOrTablename= ''PV_SA_Q_ABONNEMENT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQABO0_00''
	AND FileOrTablename= ''PV_SA_Q_ABONNEMENT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQABO0_00''
	AND FileOrTablename= ''PV_SA_Q_ABONNEMENT''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 480)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (272, N'VOL_COLLECT_30_REF_CHANNEL', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CHANNEL', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CHANNEL dans le package PKGCO_SSIS_FE_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_REFERENTIEL_00'' as output_col2,
	''REF_CHANNEL'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CHANNEL'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CHANNEL'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CHANNEL''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 313)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (273, N'VOL_COLLECT_30_REF_PERSON_RELATIONSHIP', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_PERSON_RELATIONSHIP', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_PERSON_RELATIONSHIP dans le package PKGCO_SSIS_FE_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_REFERENTIEL_00'' as output_col2,
	''REF_PERSON_RELATIONSHIP'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_PERSON_RELATIONSHIP'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_PERSON_RELATIONSHIP'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_PERSON_RELATIONSHIP''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 87)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (274, N'VOL_COLLECT_30_RAW_FE_SBDCHKACCOUNT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table RAW_FE_SBDCHKACCOUNT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table RAW_FE_SBDCHKACCOUNT dans le package PKGCO_SSIS_FE_SBDCHKACCOUNT_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_SBDCHKACCOUNT_00'' as output_col2,
	''RAW_FE_SBDCHKACCOUNT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SBDCHKACCOUNT_00''
	AND FileOrTablename= ''RAW_FE_SBDCHKACCOUNT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SBDCHKACCOUNT_00''
	AND FileOrTablename= ''RAW_FE_SBDCHKACCOUNT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SBDCHKACCOUNT_00''
	AND FileOrTablename= ''RAW_FE_SBDCHKACCOUNT''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 88)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (275, N'VOL_COLLECT_30_RAW_FE_SUBSCRIBEDPRODUCT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table RAW_FE_SUBSCRIBEDPRODUCT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table RAW_FE_SUBSCRIBEDPRODUCT dans le package PKGCO_SSIS_FE_SUBSCRIBEDPRODUCT_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_SUBSCRIBEDPRODUCT_00'' as output_col2,
	''RAW_FE_SUBSCRIBEDPRODUCT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SUBSCRIBEDPRODUCT_00''
	AND FileOrTablename= ''RAW_FE_SUBSCRIBEDPRODUCT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SUBSCRIBEDPRODUCT_00''
	AND FileOrTablename= ''RAW_FE_SUBSCRIBEDPRODUCT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SUBSCRIBEDPRODUCT_00''
	AND FileOrTablename= ''RAW_FE_SUBSCRIBEDPRODUCT''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 96)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (276, N'VOL_COLLECT_30_PV_FE_SUBSCRIBEDPRODUCT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_FE_SUBSCRIBEDPRODUCT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_FE_SUBSCRIBEDPRODUCT dans le package PKGCO_SSIS_FE_SUBSCRIBEDPRODUCT_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_SUBSCRIBEDPRODUCT_01'' as output_col2,
	''PV_FE_SUBSCRIBEDPRODUCT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SUBSCRIBEDPRODUCT_01''
	AND FileOrTablename= ''PV_FE_SUBSCRIBEDPRODUCT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SUBSCRIBEDPRODUCT_01''
	AND FileOrTablename= ''PV_FE_SUBSCRIBEDPRODUCT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SUBSCRIBEDPRODUCT_01''
	AND FileOrTablename= ''PV_FE_SUBSCRIBEDPRODUCT''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 97)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (277, N'VOL_COLLECT_30_PV_FI_Q_COMPL_UP', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_FI_Q_COMPL_UP', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_FI_Q_COMPL_UP dans le package PKGCO_SSIS_FI_XSIQCPO_UP_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FI_XSIQCPO_UP_00'' as output_col2,
	''PV_FI_Q_COMPL_UP'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FI_XSIQCPO_UP_00''
	AND FileOrTablename= ''PV_FI_Q_COMPL_UP'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FI_XSIQCPO_UP_00''
	AND FileOrTablename= ''PV_FI_Q_COMPL_UP'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FI_XSIQCPO_UP_00''
	AND FileOrTablename= ''PV_FI_Q_COMPL_UP''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Franfinance', N'VOL_COLLECT_Franfinance', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 102)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (278, N'VOL_COLLECT_30_PV_FI_Q_DES', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_FI_Q_DES', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_FI_Q_DES dans le package PKGCO_SSIS_FI_XSIQDES_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FI_XSIQDES_00'' as output_col2,
	''PV_FI_Q_DES'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FI_XSIQDES_00''
	AND FileOrTablename= ''PV_FI_Q_DES'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FI_XSIQDES_00''
	AND FileOrTablename= ''PV_FI_Q_DES'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FI_XSIQDES_00''
	AND FileOrTablename= ''PV_FI_Q_DES''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Franfinance', N'VOL_COLLECT_Franfinance', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 103)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (279, N'VOL_COLLECT_30_PV_MC_CAMPAIGNMESSAGE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_MC_CAMPAIGNMESSAGE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_MC_CAMPAIGNMESSAGE dans le package PKGCO_SSIS_MC_XSIHCAME_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_MC_XSIHCAME_00'' as output_col2,
	''PV_MC_CAMPAIGNMESSAGE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	NULL as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHCAME_00''
	AND FileOrTablename= ''PV_MC_CAMPAIGNMESSAGE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHCAME_00''
	AND FileOrTablename= ''PV_MC_CAMPAIGNMESSAGE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Marketing Cloud', N'VOL_COLLECT_Marketing Cloud', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement de la semaine précédente''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 108)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (280, N'VOL_COLLECT_30_PV_MC_CAMPAIGNNOTCOMPLETED', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_MC_CAMPAIGNNOTCOMPLETED', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_MC_CAMPAIGNNOTCOMPLETED dans le package PKGCO_SSIS_MC_XSIHCANC_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_MC_XSIHCANC_00'' as output_col2,
	''PV_MC_CAMPAIGNNOTCOMPLETED'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	NULL as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHCANC_00''
	AND FileOrTablename= ''PV_MC_CAMPAIGNNOTCOMPLETED'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHCANC_00''
	AND FileOrTablename= ''PV_MC_CAMPAIGNNOTCOMPLETED''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Marketing Cloud', N'VOL_COLLECT_Marketing Cloud', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement de la semaine précédente''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 109)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (380, N'OAV_DATAHUB_CONTROL_1.0.29', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  OBJ_FIN_PERIODIC', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table OBJ_FIN_PERIODIC', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from OBJ_FIN_PERIODIC'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_OBJ_FIN_PERIODIC group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (381, N'OAV_DATAHUB_CONTROL_1.0.30', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  ORGANISME_DEROGATION', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table ORGANISME_DEROGATION', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from ORGANISME_DEROGATION'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_ORGANISME_DEROGATION group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (382, N'OAV_DATAHUB_CONTROL_1.0.31', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  ORGANISMES', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table ORGANISMES', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from ORGANISMES'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_ORGANISMES group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (281, N'VOL_COLLECT_30_PV_FE_COMMERCIALOFFER', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_FE_COMMERCIALOFFER', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_FE_COMMERCIALOFFER dans le package PKGCO_SSIS_FE_COMMERCIALOFFER_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_COMMERCIALOFFER_01'' as output_col2,
	''PV_FE_COMMERCIALOFFER'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_COMMERCIALOFFER_01''
	AND FileOrTablename= ''PV_FE_COMMERCIALOFFER'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_COMMERCIALOFFER_01''
	AND FileOrTablename= ''PV_FE_COMMERCIALOFFER'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_COMMERCIALOFFER_01''
	AND FileOrTablename= ''PV_FE_COMMERCIALOFFER''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 54)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (330, N'VOL_COLLECT_30_REF_STAT_MARITAL', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_STAT_MARITAL', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_STAT_MARITAL dans le package PKGCO_SSIS_SF_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_00'' as output_col2,
	''REF_STAT_MARITAL'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_STAT_MARITAL'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_STAT_MARITAL'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_STAT_MARITAL''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 198)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (331, N'VOL_COLLECT_30_REF_CONSUMER_LOAN_SITUATION', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CONSUMER_LOAN_SITUATION', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CONSUMER_LOAN_SITUATION dans le package PKGCO_SSIS_FE_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_REFERENTIEL_00'' as output_col2,
	''REF_CONSUMER_LOAN_SITUATION'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CONSUMER_LOAN_SITUATION'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CONSUMER_LOAN_SITUATION'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CONSUMER_LOAN_SITUATION''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 81)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (332, N'VOL_COLLECT_30_REF_CONSUMER_LOAN_STATUS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CONSUMER_LOAN_STATUS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CONSUMER_LOAN_STATUS dans le package PKGCO_SSIS_FE_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_REFERENTIEL_00'' as output_col2,
	''REF_CONSUMER_LOAN_STATUS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CONSUMER_LOAN_STATUS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CONSUMER_LOAN_STATUS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CONSUMER_LOAN_STATUS''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 82)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (333, N'VOL_COLLECT_30_RAW_FE_SBDSERVICESASSOC', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table RAW_FE_SBDSERVICESASSOC', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table RAW_FE_SBDSERVICESASSOC dans le package PKGCO_SSIS_FE_SBDSERVICESASSOC_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_SBDSERVICESASSOC_00'' as output_col2,
	''RAW_FE_SBDSERVICESASSOC'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SBDSERVICESASSOC_00''
	AND FileOrTablename= ''RAW_FE_SBDSERVICESASSOC'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SBDSERVICESASSOC_00''
	AND FileOrTablename= ''RAW_FE_SBDSERVICESASSOC'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log
	WHERE ETLName = ''PKGCO_SSIS_FE_SBDSERVICESASSOC_00''
	AND FileOrTablename= ''RAW_FE_SBDSERVICESASSOC''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 90)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (334, N'VOL_COLLECT_30_PV_FE_SBDSERVICESASSOC', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_FE_SBDSERVICESASSOC', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_FE_SBDSERVICESASSOC dans le package PKGCO_SSIS_FE_SBDSERVICESASSOC_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_SBDSERVICESASSOC_01'' as output_col2,
	''PV_FE_SBDSERVICESASSOC'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SBDSERVICESASSOC_01''
	AND FileOrTablename= ''PV_FE_SBDSERVICESASSOC'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SBDSERVICESASSOC_01''
	AND FileOrTablename= ''PV_FE_SBDSERVICESASSOC'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SBDSERVICESASSOC_01''
	AND FileOrTablename= ''PV_FE_SBDSERVICESASSOC''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 91)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (335, N'VOL_COLLECT_30_DIM_CANAL_DIS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table DIM_CANAL_DIS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table DIM_CANAL_DIS dans le package PKGCO_SSIS_SF_REFERENTIEL_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_01'' as output_col2,
	''DIM_CANAL_DIS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CANAL_DIS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CANAL_DIS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CANAL_DIS''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 202)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (336, N'VOL_COLLECT_30_DIM_CANAL_INT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table DIM_CANAL_INT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table DIM_CANAL_INT dans le package PKGCO_SSIS_SF_REFERENTIEL_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_01'' as output_col2,
	''DIM_CANAL_INT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CANAL_INT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CANAL_INT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CANAL_INT''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 203)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (337, N'VOL_COLLECT_30_PV_FI_M_COMPL', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_FI_M_COMPL', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_FI_M_COMPL dans le package PKGCO_SSIS_FI_XSIMCPO_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FI_XSIMCPO_00'' as output_col2,
	''PV_FI_M_COMPL'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FI_XSIMCPO_00''
	AND FileOrTablename= ''PV_FI_M_COMPL'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FI_XSIMCPO_00''
	AND FileOrTablename= ''PV_FI_M_COMPL'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log WITH(NOLOCK) 
					WHERE ETLName = ''PKGCO_SSIS_FI_XSIMCPO_00''   AND FileOrTablename= ''PV_FI_M_COMPL''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'Franfinance', N'VOL_COLLECT_Franfinance', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 99)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (338, N'VOL_COLLECT_30_PV_FI_M_DES', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_FI_M_DES', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_FI_M_DES dans le package PKGCO_SSIS_FI_XSIMDES_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FI_XSIMDES_00'' as output_col2,
	''PV_FI_M_DES'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FI_XSIMDES_00''
	AND FileOrTablename= ''PV_FI_M_DES'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FI_XSIMDES_00''
	AND FileOrTablename= ''PV_FI_M_DES'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log WITH(NOLOCK) 
					WHERE ETLName = ''PKGCO_SSIS_FI_XSIMDES_00''   AND FileOrTablename= ''PV_FI_M_DES''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'Franfinance', N'VOL_COLLECT_Franfinance', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 100)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (339, N'VOL_COLLECT_30_PV_MC_BOUNCES', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_MC_BOUNCES', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_MC_BOUNCES dans le package PKGCO_SSIS_MC_XSIHBOUN_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_MC_XSIHBOUN_00'' as output_col2,
	''PV_MC_BOUNCES'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	null as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHBOUN_00''
	AND FileOrTablename= ''PV_MC_BOUNCES'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHBOUN_00''
	AND FileOrTablename= ''PV_MC_BOUNCES''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Marketing Cloud', N'VOL_COLLECT_Marketing Cloud', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement de la semaine précédente''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 105)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (340, N'VOL_COLLECT_30_PV_MC_CAMPAIGNCANCELLED', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_MC_CAMPAIGNCANCELLED', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_MC_CAMPAIGNCANCELLED dans le package PKGCO_SSIS_MC_XSIHCACA_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_MC_XSIHCACA_00'' as output_col2,
	''PV_MC_CAMPAIGNCANCELLED'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	null as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHCACA_00''
	AND FileOrTablename= ''PV_MC_CAMPAIGNCANCELLED'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHCACA_00''
	AND FileOrTablename= ''PV_MC_CAMPAIGNCANCELLED''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Marketing Cloud', N'VOL_COLLECT_Marketing Cloud', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement de la semaine précédente''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 106)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (379, N'OAV_DATAHUB_CONTROL_1.0.28', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  OBJETS_FINANCEMENTS', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table OBJETS_FINANCEMENTS', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from OBJETS_FINANCEMENTS'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_OBJETS_FINANCEMENTS group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (341, N'VOL_COLLECT_30_DIM_CASE_METIER', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table DIM_CASE_METIER', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table DIM_CASE_METIER dans le package PKGCO_SSIS_SF_REFERENTIEL_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_01'' as output_col2,
	''DIM_CASE_METIER'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_METIER'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_METIER'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_METIER''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 207)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (342, N'VOL_COLLECT_30_DIM_CASE_MOTIF', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table DIM_CASE_MOTIF', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table DIM_CASE_MOTIF dans le package PKGCO_SSIS_SF_REFERENTIEL_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_01'' as output_col2,
	''DIM_CASE_MOTIF'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_MOTIF'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_MOTIF'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_MOTIF''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 208)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (343, N'VOL_COLLECT_30_PV_MC_CLICKS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_MC_CLICKS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_MC_CLICKS dans le package PKGCO_SSIS_MC_XSIHCLIK_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_MC_XSIHCLIK_00'' as output_col2,
	''PV_MC_CLICKS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	NULL as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHCLIK_00''
	AND FileOrTablename= ''PV_MC_CLICKS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHCLIK_00''
	AND FileOrTablename= ''PV_MC_CLICKS''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Marketing Cloud', N'VOL_COLLECT_Marketing Cloud', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement de la semaine précédente''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 111)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (344, N'VOL_COLLECT_30_PV_MC_COMPLAINTS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_MC_COMPLAINTS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_MC_COMPLAINTS dans le package PKGCO_SSIS_MC_XSIHCOMP_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_MC_XSIHCOMP_00'' as output_col2,
	''PV_MC_COMPLAINTS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	NULL as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHCOMP_00''
	AND FileOrTablename= ''PV_MC_COMPLAINTS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHCOMP_00''
	AND FileOrTablename= ''PV_MC_COMPLAINTS''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Marketing Cloud', N'VOL_COLLECT_Marketing Cloud', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement de la semaine précédente''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 112)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (383, N'OAV_DATAHUB_CONTROL_1.0.32', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  ORGA_OBJET', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table ORGA_OBJET', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from ORGA_OBJET'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_ORGA_OBJET group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (345, N'VOL_COLLECT_30_RAW_SF_RECORDTYPE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table RAW_SF_RECORDTYPE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table RAW_SF_RECORDTYPE dans le package PKGCO_SSIS_SF_RECORDTYPE_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_RECORDTYPE_00'' as output_col2,
	''RAW_SF_RECORDTYPE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_RECORDTYPE_00''
	AND FileOrTablename= ''RAW_SF_RECORDTYPE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_RECORDTYPE_00''
	AND FileOrTablename= ''RAW_SF_RECORDTYPE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_RECORDTYPE_00''
	AND FileOrTablename= ''RAW_SF_RECORDTYPE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 1)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (188, N'VOL_COLLECT_30_PV_SF_ACCOUNT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SF_ACCOUNT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SF_ACCOUNT dans le package PKGCO_SSIS_SF_ACCOUNT_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_ACCOUNT_00'' as output_col2,
	''PV_SF_ACCOUNT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_ACCOUNT_00''
	AND FileOrTablename= ''PV_SF_ACCOUNT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_ACCOUNT_00''
	AND FileOrTablename= ''PV_SF_ACCOUNT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_ACCOUNT_00''
	AND FileOrTablename= ''PV_SF_ACCOUNT''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 421)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (389, N'OAV_DATAHUB_CONTROL_1.0.40', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  RELEVES', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table RELEVES', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from RELEVES'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_RELEVES group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (390, N'OAV_DATAHUB_CONTROL_1.0.41', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  REPONSES_BDF', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table REPONSES_BDF', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from REPONSES_BDF'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_REPONSES_BDF group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (391, N'OAV_DATAHUB_CONTROL_1.0.42', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  REPONSES_FF', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table REPONSES_FF', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from REPONSES_FF'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_REPONSES_FF group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (392, N'OAV_DATAHUB_CONTROL_1.0.45', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  SIMULATION_FF', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table SIMULATION_FF', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from SIMULATION_FF'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_SIMULATION_FF group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (393, N'OAV_DATAHUB_CONTROL_1.0.46', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  SITUATIONS_MATRIMONIALES', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table SITUATIONS_MATRIMONIALES', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from SITUATIONS_MATRIMONIALES'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_SITUATIONS_MATRIMONIALES group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (394, N'OAV_DATAHUB_CONTROL_1.0.47', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  STATUTS', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table STATUTS', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from STATUTS'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_STATUTS group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (395, N'OAV_DATAHUB_CONTROL_1.0.48', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  TAUX', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table TAUX', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from TAUX'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_TAUX group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (396, N'OAV_DATAHUB_CONTROL_1.0.49', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  TAUX_DATES', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table TAUX_DATES', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from TAUX_DATES'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_TAUX_DATES group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (397, N'OAV_DATAHUB_CONTROL_1.0.50', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  TAUX_DUREES', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table TAUX_DUREES', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from TAUX_DUREES'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_TAUX_DUREES group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (398, N'OAV_DATAHUB_CONTROL_1.0.51', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  TAUX_MONTANTS', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table TAUX_MONTANTS', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from TAUX_MONTANTS'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_TAUX_MONTANTS group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (399, N'OAV_DATAHUB_CONTROL_1.0.52', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  TYPE_CONTRATS', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table TYPE_CONTRATS', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from TYPE_CONTRATS'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_TYPE_CONTRATS group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (400, N'OAV_DATAHUB_CONTROL_1.0.53', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  TYPE_OCCUPATIONS', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table TYPE_OCCUPATIONS', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from TYPE_OCCUPATIONS'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_TYPE_OCCUPATIONS group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (401, N'OAV_DATAHUB_CONTROL_1.0.54', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  UTILISATEURS', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table UTILISATEURS', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from UTILISATEURS'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_UTILISATEURS group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (402, N'OAV_DATAHUB_CONTROL_1.0.55', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  UTILISATEURS_PRODUITS', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table UTILISATEURS_PRODUITS', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from UTILISATEURS_PRODUITS'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_UTILISATEURS_PRODUITS group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (403, N'OAV_DATAHUB_CONTROL_1.0.56', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  UTILISATEURS_ROLES', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table UTILISATEURS_ROLES', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from UTILISATEURS_ROLES'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_UTILISATEURS_ROLES group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (367, N'OAV_DATAHUB_CONTROL_1.0.11', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  COMPTES', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table COMPTES', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from COMPTES'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_COMPTES group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (371, N'OAV_DATAHUB_CONTROL_1.0.15', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  DOSSIER_EMPRUNTEUR', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table DOSSIER_EMPRUNTEUR', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from DOSSIER_EMPRUNTEUR'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_DOSSIER_EMPRUNTEUR group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (127, N'VOL_COLLECT_30_REF_CASE_PLAN_ACTION', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CASE_PLAN_ACTION', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CASE_PLAN_ACTION dans le package PKGCO_SSIS_SF_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_00'' as output_col2,
	''REF_CASE_PLAN_ACTION'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_PLAN_ACTION'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_PLAN_ACTION'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_PLAN_ACTION''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 184)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (76, N'IWD_DATAHUB_CONTROL_1.0.54', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_ROUTING_TARGET', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_ROUTING_TARGET', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_ROUTING_TARGET'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_ROUTING_TARGET) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (128, N'VOL_COLLECT_30_REF_CASE_TYPE_CLI', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CASE_TYPE_CLI', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CASE_TYPE_CLI dans le package PKGCO_SSIS_SF_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_00'' as output_col2,
	''REF_CASE_TYPE_CLI'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_TYPE_CLI'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_TYPE_CLI'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_TYPE_CLI''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 189)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (31, N'IWD_DATAHUB_CONTROL_1.0.10', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_GIDB_GC_AGENT', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_GIDB_GC_AGENT', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_GIDB_GC_AGENT'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_GIDB_GC_AGENT) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (129, N'VOL_COLLECT_30_REF_MOTIF_SS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_MOTIF_SS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_MOTIF_SS dans le package PKGCO_SSIS_SF_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_00'' as output_col2,
	''REF_MOTIF_SS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_MOTIF_SS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_MOTIF_SS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_MOTIF_SS''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 194)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (130, N'VOL_COLLECT_30_REF_TYP_OCC_LOG_PPA', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_TYP_OCC_LOG_PPA', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_TYP_OCC_LOG_PPA dans le package PKGCO_SSIS_SF_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_00'' as output_col2,
	''REF_TYP_OCC_LOG_PPA'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_TYP_OCC_LOG_PPA'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_TYP_OCC_LOG_PPA'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_TYP_OCC_LOG_PPA''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 199)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (131, N'VOL_COLLECT_30_DIM_CASE_ATTENTE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table DIM_CASE_ATTENTE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table DIM_CASE_ATTENTE dans le package PKGCO_SSIS_SF_REFERENTIEL_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_01'' as output_col2,
	''DIM_CASE_ATTENTE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_ATTENTE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_ATTENTE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_ATTENTE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 204)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (132, N'VOL_COLLECT_30_DIM_CASE_NIVEAU', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table DIM_CASE_NIVEAU', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table DIM_CASE_NIVEAU dans le package PKGCO_SSIS_SF_REFERENTIEL_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_01'' as output_col2,
	''DIM_CASE_NIVEAU'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_NIVEAU'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_NIVEAU'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_NIVEAU''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 209)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (133, N'VOL_COLLECT_30_DIM_CASE_QUALIF_REP1', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table DIM_CASE_QUALIF_REP1', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table DIM_CASE_QUALIF_REP1 dans le package PKGCO_SSIS_SF_REFERENTIEL_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_01'' as output_col2,
	''DIM_CASE_QUALIF_REP1'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_QUALIF_REP1'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_QUALIF_REP1'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_QUALIF_REP1''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 214)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (134, N'VOL_COLLECT_30_DIM_CSP_NIV3', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table DIM_CSP_NIV3', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table DIM_CSP_NIV3 dans le package PKGCO_SSIS_SF_REFERENTIEL_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_01'' as output_col2,
	''DIM_CSP_NIV3'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CSP_NIV3'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CSP_NIV3'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CSP_NIV3''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 219)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (135, N'VOL_COLLECT_30_DIM_SS_RES', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table DIM_SS_RES', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table DIM_SS_RES dans le package PKGCO_SSIS_SF_REFERENTIEL_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_01'' as output_col2,
	''DIM_SS_RES'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_SS_RES'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_SS_RES'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_SS_RES''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 223)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (136, N'VOL_COLLECT_30_PV_FE_CUSTOMER', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_FE_CUSTOMER', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_FE_CUSTOMER dans le package PKGCO_SSIS_FE_CUSTOMER_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_CUSTOMER_01'' as output_col2,
	''PV_FE_CUSTOMER'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CUSTOMER_01''
	AND FileOrTablename= ''PV_FE_CUSTOMER'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CUSTOMER_01''
	AND FileOrTablename= ''PV_FE_CUSTOMER'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CUSTOMER_01''
	AND FileOrTablename= ''PV_FE_CUSTOMER''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 275)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (137, N'VOL_COLLECT_30_REF_OSDEVICE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_OSDEVICE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_OSDEVICE dans le package PKGCO_SSIS_FE_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_REFERENTIEL_00'' as output_col2,
	''REF_OSDEVICE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_OSDEVICE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.Data_Log	WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_OSDEVICE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_OSDEVICE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 315)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (187, N'VOL_COLLECT_30_PV_PC_SCORE_EER', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_PC_SCORE_EER', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_PC_SCORE_EER dans le package PKGCO_SSIS_PC_SCORE_EER', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_PC_SCORE_EER'' as output_col2,
	''PV_PC_SCORE_EER'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_PC_SCORE_EER''
	AND FileOrTablename= ''PV_PC_SCORE_EER'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_PC_SCORE_EER''
	AND FileOrTablename= ''PV_PC_SCORE_EER'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_PC_SCORE_EER''
	AND FileOrTablename= ''PV_PC_SCORE_EER''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Experian', N'VOL_COLLECT_Experian', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 418)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (186, N'VOL_COLLECT_30_WK_SF_OPPORTUNITYFIELDHISTORY', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table WK_SF_OPPORTUNITYFIELDHISTORY', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table WK_SF_OPPORTUNITYFIELDHISTORY dans le package PKGCO_SSIS_SF_OPPORTUNITYFIELDHISTORY_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_OPPORTUNITYFIELDHISTORY_00'' as output_col2,
	''WK_SF_OPPORTUNITYFIELDHISTORY'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_OPPORTUNITYFIELDHISTORY_00''
	AND FileOrTablename= ''WK_SF_OPPORTUNITYFIELDHISTORY'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_OPPORTUNITYFIELDHISTORY_00''
	AND FileOrTablename= ''WK_SF_OPPORTUNITYFIELDHISTORY'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_OPPORTUNITYFIELDHISTORY_00''
	AND FileOrTablename= ''WK_SF_OPPORTUNITYFIELDHISTORY''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 428)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (81, N'IWD_DATAHUB_CONTROL_1.0.23', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_GIDB_GC_IVR', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_GIDB_GC_IVR', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_GIDB_GC_IVR'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_GIDB_GC_IVR) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (83, N'IWD_DATAHUB_CONTROL_1.0.4', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_CAMPAIGN_GROUP_STATE', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_CAMPAIGN_GROUP_STATE', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_CAMPAIGN_GROUP_STATE'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_CAMPAIGN_GROUP_STATE) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (189, N'VOL_COLLECT_30_IWD_GROUP_TO_CAMPAIGN_FACT_', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_GROUP_TO_CAMPAIGN_FACT_', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_GROUP_TO_CAMPAIGN_FACT_ dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_GROUP_TO_CAMPAIGN_FACT_'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (190, N'VOL_COLLECT_30_IWD_INTERACTION_DESCRIPTOR', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_INTERACTION_DESCRIPTOR', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_INTERACTION_DESCRIPTOR dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_INTERACTION_DESCRIPTOR'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (191, N'VOL_COLLECT_30_IWD_ATTEMPT_DISPOSITION', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_ATTEMPT_DISPOSITION', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_ATTEMPT_DISPOSITION dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_ATTEMPT_DISPOSITION'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (192, N'VOL_COLLECT_30_IWD_CALL_RESULT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_CALL_RESULT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_CALL_RESULT dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_CALL_RESULT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (193, N'VOL_COLLECT_30_IWD_INTERACTION_TYPE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_INTERACTION_TYPE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_INTERACTION_TYPE dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_INTERACTION_TYPE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 366)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (194, N'VOL_COLLECT_30_IWD_RECORD_FIELD_GROUP_2', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_RECORD_FIELD_GROUP_2', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_RECORD_FIELD_GROUP_2 dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_RECORD_FIELD_GROUP_2'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (195, N'VOL_COLLECT_30_IWD_RECORD_STATUS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_RECORD_STATUS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_RECORD_STATUS dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_RECORD_STATUS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (196, N'VOL_COLLECT_30_IWD_RECORD_TYPE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_RECORD_TYPE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_RECORD_TYPE dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_RECORD_TYPE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (197, N'VOL_COLLECT_30_IWD_REQUESTED_SKILL', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_REQUESTED_SKILL', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_REQUESTED_SKILL dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_REQUESTED_SKILL'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (198, N'VOL_COLLECT_30_IWD_RESOURCE_GROUP_FACT_', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_RESOURCE_GROUP_FACT_', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_RESOURCE_GROUP_FACT_ dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_RESOURCE_GROUP_FACT_'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (231, N'VOL_COLLECT_30_REF_SCORE_EER', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_SCORE_EER', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_SCORE_EER dans le package PKGCO_SSIS_SF_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_00'' as output_col2,
	''REF_SCORE_EER'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_SCORE_EER'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_SCORE_EER'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_SCORE_EER''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 200)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (232, N'VOL_COLLECT_30_DIM_SCORE_EER', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table DIM_SCORE_EER', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table DIM_SCORE_EER dans le package PKGCO_SSIS_SF_REFERENTIEL_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_01'' as output_col2,
	''DIM_SCORE_EER'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_SCORE_EER'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_SCORE_EER'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_SCORE_EER''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 201)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (233, N'VOL_COLLECT_30_DIM_CASE_CANAL_ORIGINE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table DIM_CASE_CANAL_ORIGINE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table DIM_CASE_CANAL_ORIGINE dans le package PKGCO_SSIS_SF_REFERENTIEL_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_01'' as output_col2,
	''DIM_CASE_CANAL_ORIGINE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_CANAL_ORIGINE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_CANAL_ORIGINE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_CANAL_ORIGINE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 205)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (250, N'VOL_COLLECT_30_REF_GROUPES_AGENCE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_GROUPES_AGENCE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_GROUPES_AGENCE dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_GROUPES_AGENCE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_GROUPES_AGENCE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_GROUPES_AGENCE'' 
	  AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_GROUPES_AGENCE''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 139)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (251, N'VOL_COLLECT_30_REF_MOTIFS_DE_RESILIATION', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_MOTIFS_DE_RESILIATION', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_MOTIFS_DE_RESILIATION dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_MOTIFS_DE_RESILIATION'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_MOTIFS_DE_RESILIATION'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_MOTIFS_DE_RESILIATION'' 
	  AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_MOTIFS_DE_RESILIATION''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 144)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (252, N'VOL_COLLECT_30_REF_NATURES_OPERATIONS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_NATURES_OPERATIONS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_NATURES_OPERATIONS dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_NATURES_OPERATIONS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_NATURES_OPERATIONS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_NATURES_OPERATIONS'' 
	  AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_NATURES_OPERATIONS''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 145)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (253, N'VOL_COLLECT_30_DIM_RES_DIS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table DIM_RES_DIS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table DIM_RES_DIS dans le package PKGCO_SSIS_SF_REFERENTIEL_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_01'' as output_col2,
	''DIM_RES_DIS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_RES_DIS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_RES_DIS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_RES_DIS''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 222)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (254, N'VOL_COLLECT_30_DIM_TYP_OCC_LOG_PPA', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table DIM_TYP_OCC_LOG_PPA', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table DIM_TYP_OCC_LOG_PPA dans le package PKGCO_SSIS_SF_REFERENTIEL_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_01'' as output_col2,
	''DIM_TYP_OCC_LOG_PPA'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_TYP_OCC_LOG_PPA'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_TYP_OCC_LOG_PPA'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_TYP_OCC_LOG_PPA''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 226)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (255, N'VOL_COLLECT_30_REF_QUALITE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_QUALITE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_QUALITE dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_QUALITE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_QUALITE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_QUALITE'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_QUALITE''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 150)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (256, N'VOL_COLLECT_30_REF_SERVICES', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_SERVICES', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_SERVICES dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_SERVICES'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_SERVICES'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_SERVICES'' 
	  AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_SERVICES''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 151)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (257, N'VOL_COLLECT_30_PV_SA_M_ABONNEMENT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SA_M_ABONNEMENT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SA_M_ABONNEMENT dans le package PKGCO_SSIS_SA_XSIMABO0_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_XSIMABO0_00'' as output_col2,
	''PV_SA_M_ABONNEMENT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIMABO0_00''
	AND FileOrTablename= ''PV_SA_M_ABONNEMENT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIMABO0_00''
	AND FileOrTablename= ''PV_SA_M_ABONNEMENT'' 
	  AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_XSIMABO0_00''   AND FileOrTablename= ''PV_SA_M_ABONNEMENT''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 444)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (258, N'VOL_COLLECT_30_PV_SA_M_SERVICE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SA_M_SERVICE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SA_M_SERVICE dans le package PKGCO_SSIS_SA_XSIMABS0_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_XSIMABS0_00'' as output_col2,
	''PV_SA_M_SERVICE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIMABS0_00''
	AND FileOrTablename= ''PV_SA_M_SERVICE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIMABS0_00''
	AND FileOrTablename= ''PV_SA_M_SERVICE'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_XSIMABS0_00''   AND FileOrTablename= ''PV_SA_M_SERVICE''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 446)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (259, N'VOL_COLLECT_30_RAW_FE_MOBILE_PAYMENT_DEVICE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table RAW_FE_MOBILE_PAYMENT_DEVICE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table RAW_FE_MOBILE_PAYMENT_DEVICE dans le package PKGCO_SSIS_FE_MOBILE_PAYMENT_DEVICE_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_MOBILE_PAYMENT_DEVICE_00'' as output_col2,
	''RAW_FE_MOBILE_PAYMENT_DEVICE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_MOBILE_PAYMENT_DEVICE_00''
	AND FileOrTablename= ''RAW_FE_MOBILE_PAYMENT_DEVICE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_MOBILE_PAYMENT_DEVICE_00''
	AND FileOrTablename= ''RAW_FE_MOBILE_PAYMENT_DEVICE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_MOBILE_PAYMENT_DEVICE_00''
	AND FileOrTablename= ''RAW_FE_MOBILE_PAYMENT_DEVICE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 272)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (73, N'IWD_DATAHUB_CONTROL_1.0.52', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_RESOURCE_STATE', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_RESOURCE_STATE', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_RESOURCE_STATE'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_RESOURCE_STATE) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 1, NULL, 379)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (78, N'IWD_DATAHUB_CONTROL_1.0.56', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_TECHNICAL_DESCRIPTOR', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_TECHNICAL_DESCRIPTOR', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_TECHNICAL_DESCRIPTOR'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_TECHNICAL_DESCRIPTOR) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (260, N'VOL_COLLECT_30_PV_FE_MOBILE_PAYMENT_DEVICE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_FE_MOBILE_PAYMENT_DEVICE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_FE_MOBILE_PAYMENT_DEVICE dans le package PKGCO_SSIS_FE_MOBILE_PAYMENT_DEVICE_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_MOBILE_PAYMENT_DEVICE_01'' as output_col2,
	''PV_FE_MOBILE_PAYMENT_DEVICE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_MOBILE_PAYMENT_DEVICE_01''
	AND FileOrTablename= ''PV_FE_MOBILE_PAYMENT_DEVICE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_MOBILE_PAYMENT_DEVICE_01''
	AND FileOrTablename= ''PV_FE_MOBILE_PAYMENT_DEVICE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_MOBILE_PAYMENT_DEVICE_01''
	AND FileOrTablename= ''PV_FE_MOBILE_PAYMENT_DEVICE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 273)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (77, N'IWD_DATAHUB_CONTROL_1.0.55', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_STRATEGY', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_STRATEGY', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_STRATEGY'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_STRATEGY) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (80, N'IWD_DATAHUB_CONTROL_1.0.22', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_GIDB_GC_GROUP', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_GIDB_GC_GROUP', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_GIDB_GC_GROUP'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_GIDB_GC_GROUP) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (261, N'VOL_COLLECT_30_PV_SA_M_ENCRS_EPRGN', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SA_M_ENCRS_EPRGN', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SA_M_ENCRS_EPRGN dans le package PKGCO_SSIS_SA_XSIMERE0_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_XSIMERE0_00'' as output_col2,
	''PV_SA_M_ENCRS_EPRGN'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIMERE0_00''
	AND FileOrTablename= ''PV_SA_M_ENCRS_EPRGN'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIMERE0_00''
	AND FileOrTablename= ''PV_SA_M_ENCRS_EPRGN'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_XSIMERE0_00''   AND FileOrTablename= ''PV_SA_M_ENCRS_EPRGN''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 460)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (262, N'VOL_COLLECT_30_PV_SA_M_LKCPTCLI', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SA_M_LKCPTCLI', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SA_M_LKCPTCLI dans le package PKGCO_SSIS_SA_XSIMGRP0_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_XSIMGRP0_00'' as output_col2,
	''PV_SA_M_LKCPTCLI'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIMGRP0_00''
	AND FileOrTablename= ''PV_SA_M_LKCPTCLI'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIMGRP0_00''
	AND FileOrTablename= ''PV_SA_M_LKCPTCLI'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_XSIMGRP0_00''   AND FileOrTablename= ''PV_SA_M_LKCPTCLI''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 464)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (295, N'VOL_COLLECT_30_PV_FE_COMMERCIALPRODUCT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_FE_COMMERCIALPRODUCT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_FE_COMMERCIALPRODUCT dans le package PKGCO_SSIS_FE_COMMERCIALPRODUCT_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_COMMERCIALPRODUCT_01'' as output_col2,
	''PV_FE_COMMERCIALPRODUCT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_COMMERCIALPRODUCT_01''
	AND FileOrTablename= ''PV_FE_COMMERCIALPRODUCT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_COMMERCIALPRODUCT_01''
	AND FileOrTablename= ''PV_FE_COMMERCIALPRODUCT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_COMMERCIALPRODUCT_01''
	AND FileOrTablename= ''PV_FE_COMMERCIALPRODUCT''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 56)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (296, N'VOL_COLLECT_30_REF_CONSUMER_LOAN_SCORE_COLOR', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CONSUMER_LOAN_SCORE_COLOR', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CONSUMER_LOAN_SCORE_COLOR dans le package PKGCO_SSIS_FE_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_REFERENTIEL_00'' as output_col2,
	''REF_CONSUMER_LOAN_SCORE_COLOR'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CONSUMER_LOAN_SCORE_COLOR'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CONSUMER_LOAN_SCORE_COLOR'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CONSUMER_LOAN_SCORE_COLOR''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 80)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (297, N'VOL_COLLECT_30_PV_FE_SBDCHKACCOUNT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_FE_SBDCHKACCOUNT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_FE_SBDCHKACCOUNT dans le package PKGCO_SSIS_FE_SBDCHKACCOUNT_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_SBDCHKACCOUNT_01'' as output_col2,
	''PV_FE_SBDCHKACCOUNT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SBDCHKACCOUNT_01''
	AND FileOrTablename= ''PV_FE_SBDCHKACCOUNT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SBDCHKACCOUNT_01''
	AND FileOrTablename= ''PV_FE_SBDCHKACCOUNT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SBDCHKACCOUNT_01''
	AND FileOrTablename= ''PV_FE_SBDCHKACCOUNT''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 89)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (170, N'VOL_COLLECT_30_IWD_RESOURCE_', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_RESOURCE_', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_RESOURCE_ dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_RESOURCE_'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 375)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (171, N'VOL_COLLECT_30_IWD_RESOURCE_GROUP_COMBINATION', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_RESOURCE_GROUP_COMBINATION', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_RESOURCE_GROUP_COMBINATION dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_RESOURCE_GROUP_COMBINATION'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (172, N'VOL_COLLECT_30_IWD_PLACE_GROUP_FACT_', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_PLACE_GROUP_FACT_', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_PLACE_GROUP_FACT_ dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_PLACE_GROUP_FACT_'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (173, N'VOL_COLLECT_30_IWD_RECORD_FIELD_GROUP_1', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_RECORD_FIELD_GROUP_1', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_RECORD_FIELD_GROUP_1 dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_RECORD_FIELD_GROUP_1'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (174, N'VOL_COLLECT_30_IWD_CAMPAIGN_GROUP_STATE_FACT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_CAMPAIGN_GROUP_STATE_FACT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_CAMPAIGN_GROUP_STATE_FACT dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_CAMPAIGN_GROUP_STATE_FACT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (175, N'VOL_COLLECT_30_IWD_CONTACT_ATTEMPT_FACT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_CONTACT_ATTEMPT_FACT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_CONTACT_ATTEMPT_FACT dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_CONTACT_ATTEMPT_FACT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (176, N'VOL_COLLECT_30_IWD_STRATEGY', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_STRATEGY', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_STRATEGY dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_STRATEGY'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (177, N'VOL_COLLECT_30_IWD_TECHNICAL_DESCRIPTOR', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_TECHNICAL_DESCRIPTOR', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_TECHNICAL_DESCRIPTOR dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_TECHNICAL_DESCRIPTOR'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (178, N'VOL_COLLECT_30_RAW_FE_COMMERCIALOFFER', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table RAW_FE_COMMERCIALOFFER', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table RAW_FE_COMMERCIALOFFER dans le package PKGCO_SSIS_FE_COMMERCIALOFFER_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_COMMERCIALOFFER_00'' as output_col2,
	''RAW_FE_COMMERCIALOFFER'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_COMMERCIALOFFER_00''
	AND FileOrTablename= ''RAW_FE_COMMERCIALOFFER'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_COMMERCIALOFFER_00''
	AND FileOrTablename= ''RAW_FE_COMMERCIALOFFER'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_COMMERCIALOFFER_00''
	AND FileOrTablename= ''RAW_FE_COMMERCIALOFFER''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 53)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (179, N'VOL_COLLECT_30_REF_CONSUMER_LOAN_DEROGATION', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CONSUMER_LOAN_DEROGATION', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CONSUMER_LOAN_DEROGATION dans le package PKGCO_SSIS_FE_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_REFERENTIEL_00'' as output_col2,
	''REF_CONSUMER_LOAN_DEROGATION'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CONSUMER_LOAN_DEROGATION'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CONSUMER_LOAN_DEROGATION'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CONSUMER_LOAN_DEROGATION''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 77)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (180, N'VOL_COLLECT_30_IWD_IRF_USER_DATA_CUST_1', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_IRF_USER_DATA_CUST_1', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_IRF_USER_DATA_CUST_1 dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_IRF_USER_DATA_CUST_1'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (181, N'VOL_COLLECT_30_IWD_SM_RES_SESSION_FACT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_SM_RES_SESSION_FACT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_SM_RES_SESSION_FACT dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_SM_RES_SESSION_FACT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 396)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (182, N'VOL_COLLECT_30_IWD_SM_RES_STATE_FACT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_SM_RES_STATE_FACT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_SM_RES_STATE_FACT dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_SM_RES_STATE_FACT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (65, N'IWD_DATAHUB_CONTROL_1.0.67', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de fait IWD_IRF_USER_DATA_KEYS', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_IRF_USER_DATA_KEYS', N'SELECT src_nb as output_col1,  
	 cible_nb as output_col2,  
	 null as output_col3,  
	 null as output_col4,  
	 null as output_col5,  
	 null as output_col6,  
	 null as output_col7  
	 from (select * 
 from (select count(*) as src_nb from openquery(INFOMART,''select CREATE_AUDIT_KEY,UPDATE_AUDIT_KEY from V_IRF_USER_DATA_KEYS'') f
join [$(DataHubDatabaseName)].[dbo].IWD_CTL_AUDIT_LOG a on (F.UPDATE_AUDIT_KEY = A.AUDIT_KEY OR (F.CREATE_AUDIT_KEY = A.AUDIT_KEY AND F.UPDATE_AUDIT_KEY =0) )
join [$(DataHubDatabaseName)].[dbo].IWD_PILOTAGE_CHARGEMENT pil 
on (a.created between pil.dte_deb_periode and pil.dte_fin_periode)) a
	 join   (select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_IRF_USER_DATA_KEYS) b  on a.src_nb != b.cible_nb) t', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (79, N'IWD_DATAHUB_CONTROL_1.0.57', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_TIME_ZONE', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_TIME_ZONE', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_TIME_ZONE'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_TIME_ZONE) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (74, N'IWD_DATAHUB_CONTROL_1.0.42', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_RECORD_FIELD_GROUP_1', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_RECORD_FIELD_GROUP_1', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_RECORD_FIELD_GROUP_1'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_RECORD_FIELD_GROUP_1) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (75, N'IWD_DATAHUB_CONTROL_1.0.53', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_RESOURCE_STATE_REASON', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_RESOURCE_STATE_REASON', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_RESOURCE_STATE_REASON'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_RESOURCE_STATE_REASON) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 1, NULL, 380)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (84, N'IWD_DATAHUB_CONTROL_1.0.5', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_CONTACT_INFO_TYPE', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_CONTACT_INFO_TYPE', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_CONTACT_INFO_TYPE'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_CONTACT_INFO_TYPE) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (85, N'IWD_DATAHUB_CONTROL_1.0.6', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_CTL_UD_TO_UDE_MAPPING', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_CTL_UD_TO_UDE_MAPPING', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_CTL_UD_TO_UDE_MAPPING'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_CTL_UD_TO_UDE_MAPPING) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (86, N'IWD_DATAHUB_CONTROL_1.0.7', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_CTL_UDE_KEYS_TO_DIM_MAPPING', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_CTL_UDE_KEYS_TO_DIM_MAPPING', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_CTL_UDE_KEYS_TO_DIM_MAP'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_CTL_UDE_KEYS_TO_DIM_MAPPING) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (89, N'IWD_DATAHUB_CONTROL_1.0.26', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_GIDB_GC_OBJ_TABLE', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_GIDB_GC_OBJ_TABLE', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_GIDB_GC_OBJ_TABLE'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_GIDB_GC_OBJ_TABLE) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (90, N'IWD_DATAHUB_CONTROL_1.0.27', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_GIDB_GC_PLACE', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_GIDB_GC_PLACE', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_GIDB_GC_PLACE'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_GIDB_GC_PLACE) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (15, N'DWH_CONTROL_OPPORTUNITE_1.0.3', N'2 - Warning', N'Vérification du sous réseau de finalisation de l''opportunité', N'Ce contrôle vérifie que le sous réseau de finalisation de l''opportunité n''est renseigné que si l''opportunité est clôturée', N'SELECT cible.DAT_OBSR as output_col1,
			cible.IDNT_OPPR as output_col2,
			cible.STD_VENT as output_col3,
			cible.ENTT_DIST_FINL as output_col4,
			source.StageName as output_col5,
			source.sous_res_fin as output_col6,
			source.Validity_StartDate as output_col7
	from [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].[dbo].[DWH_OPPORTUNITE] cible
	inner join 
	 (
	   select PARAM_ALIM.DAT_VALD from [$(DataFactoryDatabaseName)].dbo.PARAM_ALIM PARAM_ALIM
	   inner join 
	   (
		 select ETLName, ProcessingStartDate 
		 from
		 (
		   select ETLName, ProcessingStartDate, row_number() over(order by ProcessingStartDate desc) as FLG_DER_TRT
		   from [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
		   where FILEORTABLENAME = ''DWH_OPPORTUNITE'' 
		 ) DATA_LOG
		 where DATA_LOG.FLG_DER_TRT = 1
	   ) REQ_DATA_LOG
	   on PARAM_ALIM.NOM_BATCH = REQ_DATA_LOG.ETLName
	   and PARAM_ALIM.DAT_DEBT_TRTM = REQ_DATA_LOG.ProcessingStartDate
	 ) REQ_DATES_VALID
	on cible.DAT_OBSR = REQ_DATES_VALID.DAT_VALD
	left join
	(
		select Id_SF, 
		StageName, 
		CommercialOfferName__c, 
		DistributorEntity__c, 
		CASE
           WHEN StageName IN (''10'',''11'',''12'',''15'',''16'')
                AND CommercialOfferCode__c  IN (''OPAP'',''OPTR'',''OPVN'',''OPVO'') THEN LeadSource
           WHEN StageName IN (''09'',''10'',''11'',''13'')
                AND CommercialOfferCode__c  not in (''OPAP'',''OPTR'',''OPVN'',''OPVO'') THEN LeadSource
           ELSE NULL
       END AS sous_res_fin, 
		Validity_StartDate, 
		Validity_EndDate,
		row_number() over(partition by id_sf, cast(Validity_StartDate as date) order by Validity_StartDate desc) as rang
		from [$(DataHubDatabaseName)].dbo.pv_sf_opportunity_history
	) source 
	on source.Id_SF = cible.IDNT_OPPR
	and cast(source.Validity_StartDate as date) = cible.DAT_OBSR 
	where source.rang = 1
	and cible.ENTT_DIST_FINL != source.sous_res_fin', N'DWH', N'DWH_OPPORTUNITE', N'Technique - Contrôle RG', N'''L''''opportunité, de la table DWH_OPPORTUNITY, ''+cast(OUTPUT_COL2 as char)+'', présente un sous réseau de finalisation :''+cast(OUTPUT_COL4 as char)+'', ne correspondant pas au sous réseau de finalisation dans PV_SF_OPPORTUNITY_HISTORY''+cast(OUTPUT_COL6 as char)', N'Date Observation (DAT_OBSR)', N'Identifiant opportunite (IDNT_OPPR)', N'Stade de vente (STD_VENT)', N'Entite distributrice de finalisation (ENTT_DIST_FINL)', N'Stade de vente (StageName)', N'(sous_res_fin)', N'(Validity_StartDate)', 1, NULL, 291)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (16, N'DWH_CONTROL_OPPORTUNITE_1.0.4', N'2 - Warning', N'Vérification du délai accordé de l''opportunite crédit', N'Ce contrôle vérifie que le délai accordé de l''opportunité de type crédit est égal à la différence entre la date de création et la date d accord', N'SELECT cible.DAT_OBSR as output_col1,
			cible.IDNT_OPPR as output_col2,
			cible.DAT_CRTN_OPPR as output_col3,
			cible.DAT_ACCR_CRDT as output_col4,
			cible.DEL_ACCR_CRDT as output_col5,
			source.delai_accord_credit as output_col6,
			source.Validity_StartDate as output_col7
	from [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].[dbo].[DWH_OPPORTUNITE] cible
	inner join 
	 (
	   select PARAM_ALIM.DAT_VALD from [$(DataFactoryDatabaseName)].dbo.PARAM_ALIM PARAM_ALIM
	   inner join 
	   (
		 select ETLName, ProcessingStartDate 
		 from
		 (
		   select ETLName, ProcessingStartDate, row_number() over(order by ProcessingStartDate desc) as FLG_DER_TRT
		   from [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
		   where FILEORTABLENAME = ''DWH_OPPORTUNITE'' 
		 ) DATA_LOG
		 where DATA_LOG.FLG_DER_TRT = 1
	   ) REQ_DATA_LOG
	   on PARAM_ALIM.NOM_BATCH = REQ_DATA_LOG.ETLName
	   and PARAM_ALIM.DAT_DEBT_TRTM = REQ_DATA_LOG.ProcessingStartDate
	 ) REQ_DATES_VALID
	on cible.DAT_OBSR = REQ_DATES_VALID.DAT_VALD
	left join
	(
		select Id_SF, 
		StageName, 
		CommercialOfferName__c, 
		CreatedDate, 
		LoanAcceptationDate__c, 
		CASE
           WHEN StageName in (''12'',''14'',''15'',''16'')
                AND CommercialOfferCode__c  in (''OPAP'',''OPTR'',''OPVN'',''OPVO'') THEN DATEDIFF(DD, CreatedDate, LoanAcceptationDate__c)
			ELSE NULL
       END AS delai_accord_credit, 
		Validity_StartDate, 
		Validity_EndDate,
		row_number() over(partition by id_sf, cast(Validity_StartDate as date) order by Validity_StartDate desc) as rang
		from [$(DataHubDatabaseName)].dbo.pv_sf_opportunity_history
	) source 
	on source.Id_SF = cible.IDNT_OPPR
	and cast(source.Validity_StartDate as date) = cible.DAT_OBSR 
	where source.rang = 1
	and cible.DEL_ACCR_CRDT != source.delai_accord_credit', N'DWH', N'DWH_OPPORTUNITE', N'Technique - Contrôle RG', N'''L''''opportunité, de la table DWH_OPPORTUNITY, ''+cast(OUTPUT_COL2 as char)+'', présente un délai accordé :''+cast(OUTPUT_COL5 as char)+'', ne correspondant pas au délai accordé dans PV_SF_OPPORTUNITY_HISTORY''+cast(OUTPUT_COL6 as char)', N'Date Observation (DAT_OBSR)', N'Identifiant opportunité (IDNT_OPPR)', N'Date de création opportunité (DAT_CRTN_OPPR)', N'Date accord du crédit (DAT_ACCR_CRDT)', N'Delai accord (DEL_ACCR_CRDT)', N'Delai accord (delai_accord_credit)', N'(Validity_StartDate)', 1, NULL, 291)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (87, N'IWD_DATAHUB_CONTROL_1.0.24', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_GIDB_GC_IVRPORT', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_GIDB_GC_IVRPORT', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_GIDB_GC_IVRPORT'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_GIDB_GC_IVRPORT) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (88, N'IWD_DATAHUB_CONTROL_1.0.25', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_GIDB_GC_LOGIN', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_GIDB_GC_LOGIN', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_GIDB_GC_LOGIN'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_GIDB_GC_LOGIN) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (91, N'IWD_DATAHUB_CONTROL_1.0.28', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_GIDB_GC_SCRIPT', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_GIDB_GC_SCRIPT', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_GIDB_GC_SCRIPT'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_GIDB_GC_SCRIPT) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (92, N'IWD_DATAHUB_CONTROL_1.0.34', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_GIDB_GC_TREATMENT', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_GIDB_GC_TREATMENT', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_GIDB_GC_TREATMENT'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_GIDB_GC_TREATMENT) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (93, N'IWD_DATAHUB_CONTROL_1.0.35', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_GIDB_GC_VOICE_PROMPT', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_GIDB_GC_VOICE_PROMPT', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_GIDB_GC_VOICE_PROMPT'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_GIDB_GC_VOICE_PROMPT) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (94, N'IWD_DATAHUB_CONTROL_1.0.36', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_GROUP_TO_CAMPAIGN_FACT_', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_GROUP_TO_CAMPAIGN_FACT_', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_GROUP_TO_CAMPAIGN_FACT_'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_GROUP_TO_CAMPAIGN_FACT_) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (95, N'IWD_DATAHUB_CONTROL_1.0.37', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_INTERACTION_DESCRIPTOR', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_INTERACTION_DESCRIPTOR', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_INTERACTION_DESCRIPTOR'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_INTERACTION_DESCRIPTOR) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (17, N'DWH_CONTROL_EQUIPEMENT_SL_1.0.1', N'1 - Bloquant', N'Comparaison du nombre de lignes entre EQUIPMENT,ENROLMENTFOLDER,OPPORTUNITY et [DWH_EQUIPEMENT_SL]', N'Ce contrôle vérifie que le nombre de lignes entre la source et la cible est le même', N'SELECT source.ID_FE_EQUIPMENT as output_col1, 
source.nb  as output_col2, 
cible.nb as output_col3,
null as output_col4,
null as output_col5,
null as  output_col6,
null as  output_col7
from 
(
select  ID_FE_EQUIPMENT, COUNT(*) as nb from (SELECT  EQP.ID_FE_EQUIPMENT, EQP.CREATION_DATE, EQP.LAST_UPDATE_DATE, EQP.ENROLMENT_FOLDER_ID, EQP.EQUIPMENT_NUMBER, EQP.DISCRIMINANT, EQP.PRODUCT_ID, OPP.Id_SF,
                                                     EQP.Validity_StartDate,EQP.Validity_EndDate
                                             FROM  [$(DataHubDatabaseName)].dbo.PV_FE_EQUIPMENT AS EQP WITH(NOLOCK) LEFT  JOIN
                                             [$(DataHubDatabaseName)].dbo.PV_FE_ENROLMENTFOLDER AS EF WITH(NOLOCK) ON EQP.ENROLMENT_FOLDER_ID = EF.ID_FE_ENROLMENT_FOLDER LEFT  JOIN
                                             [$(DataHubDatabaseName)].dbo.PV_SF_OPPORTUNITY AS OPP WITH(NOLOCK) ON EF.SALESFORCE_OPPORTUNITY_ID = OPP.Id_SF) req
join ( 
select dat_vald from [$(DataFactoryDatabaseName)].dbo.PARAM_ALIM p WITH(NOLOCK)
where p.nom_batch = ''PKGDWH_SSIS_EQUIPEMENT_SL'' 
and DAT_DEBT_TRTM = (select DAT_DEBT_TRTM from [$(DataFactoryDatabaseName)].dbo.PARAM_ALIM p WITH(NOLOCK) join [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].[dbo].DWH_PARAM_DATE_VALIDITE d WITH(NOLOCK) on p.DAT_VALD = d.DAT_VALD and p.nom_batch = ''PKGDWH_SSIS_EQUIPEMENT_SL'' )
) param 
on param.Dat_VALD BETWEEN CAST(req.Validity_StartDate AS DATE) AND DATEADD([day], - 1, CAST(req.Validity_EndDate AS DATE))
group by  ID_FE_EQUIPMENT ) source
join 
(
select IDNT_EQPM, COUNT(*) as nb from [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].[dbo].[DWH_EQUIPEMENT_SL] WITH(NOLOCK)
WHERE DAt_OBSR in (
select dat_vald from [$(DataFactoryDatabaseName)].dbo.PARAM_ALIM p WITH(NOLOCK)
where p.nom_batch = ''PKGDWH_SSIS_EQUIPEMENT_SL'' 
and DAT_DEBT_TRTM = (select DAT_DEBT_TRTM from [$(DataFactoryDatabaseName)].dbo.PARAM_ALIM p WITH(NOLOCK) join [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].[dbo].DWH_PARAM_DATE_VALIDITE d WITH(NOLOCK) on p.DAT_VALD = d.DAT_VALD and p.nom_batch = ''PKGDWH_SSIS_EQUIPEMENT_SL'')
)
group by IDNT_EQPM
) cible on source.ID_FE_EQUIPMENT = cible.IDNT_EQPM
where source.nb != cible.nb', N'DWH', N'DWH_EQUIPEMENT_SL', N'Technique - Contrôle intégration', N'''Différence de lignes entre PV_FE_EQUIPMENT JOIN PV_FE_ENROLMENTFOLDER JOIN PV_SF_OPPORTUNITY, ''+cast(OUTPUT_COL2 as char)+'', et DWH_EQUIPMENT_SL :''+cast(OUTPUT_COL3 as char)+'' sur ID SL equipement :''+cast(OUTPUT_COL1 as char)', N'ID Service Layer équipement (ID_FE_EQUIPMENT)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, 1, NULL, 301)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (96, N'IWD_DATAHUB_CONTROL_1.0.38', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_INTERACTION_RESOURCE_STATE', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_INTERACTION_RESOURCE_STATE', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_INTERACTION_RESOURCE_STATE'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_INTERACTION_RESOURCE_STATE) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (32, N'IWD_DATAHUB_CONTROL_1.0.11', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_GIDB_GC_ANNEX', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_GIDB_GC_ANNEX', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_GIDB_GC_ANNEX'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_GIDB_GC_ANNEX) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (33, N'IWD_DATAHUB_CONTROL_1.0.12', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_GIDB_GC_APPLICATION', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_GIDB_GC_APPLICATION', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_GIDB_GC_APPLICATION'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_GIDB_GC_APPLICATION) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (34, N'IWD_DATAHUB_CONTROL_1.0.13', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_GIDB_GC_ATTR_VALUE', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_GIDB_GC_ATTR_VALUE', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_GIDB_GC_ATTR_VALUE'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_GIDB_GC_ATTR_VALUE) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (363, N'OAV_DATAHUB_CONTROL_1.0.5', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  CAPACITE_JURIDIQUE', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table CAPACITE_JURIDIQUE', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from CAPACITE_JURIDIQUE'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_CAPACITE_JURIDIQUE group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (35, N'IWD_DATAHUB_CONTROL_1.0.14', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_GIDB_GC_BUS_ATTRIBUTE', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_GIDB_GC_BUS_ATTRIBUTE', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_GIDB_GC_BUS_ATTRIBUTE'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_GIDB_GC_BUS_ATTRIBUTE) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (36, N'IWD_DATAHUB_CONTROL_1.0.29', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_GIDB_GC_SKILL', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_GIDB_GC_SKILL', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_GIDB_GC_SKILL'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_GIDB_GC_SKILL) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (97, N'IWD_DATAHUB_CONTROL_1.0.71', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de fait IWD_SM_RES_STATE_FACT', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_SM_RES_STATE_FACT', N'SELECT src_nb as output_col1,  
	 cible_nb as output_col2,  
	 null as output_col3,  
	 null as output_col4,  
	 null as output_col5,  
	 null as output_col6,  
	 null as output_col7  
	 from (select * 
 from (select count(*) as src_nb from openquery(INFOMART,''select CREATE_AUDIT_KEY,UPDATE_AUDIT_KEY from V_SM_RES_STATE_FACT'') f
join [$(DataHubDatabaseName)].[dbo].IWD_CTL_AUDIT_LOG a on (F.UPDATE_AUDIT_KEY = A.AUDIT_KEY OR (F.CREATE_AUDIT_KEY = A.AUDIT_KEY AND F.UPDATE_AUDIT_KEY =0) )
join [$(DataHubDatabaseName)].[dbo].IWD_PILOTAGE_CHARGEMENT pil 
on (a.created between pil.dte_deb_periode and pil.dte_fin_periode)) a
	 join   (select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_SM_RES_STATE_FACT) b  on a.src_nb != b.cible_nb) t', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (37, N'IWD_DATAHUB_CONTROL_1.0.30', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_GIDB_GC_SWITCH', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_GIDB_GC_SWITCH', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_GIDB_GC_SWITCH'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_GIDB_GC_SWITCH) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (38, N'IWD_DATAHUB_CONTROL_1.0.31', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_GIDB_GC_TABLE_ACCESS', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_GIDB_GC_TABLE_ACCESS', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_GIDB_GC_TABLE_ACCESS'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_GIDB_GC_TABLE_ACCESS) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (18, N'DWH_EQUIPEMENT_CREDIT_CONSO_1.0.1', N'1 - Bloquant', N'Comparaison DATAHUB/DWH table DWH_EQUIPEMENT_CREDIT_CONSO', N'Ce contrôle permet de vérifier que le nombre d''enregistrements extraits des tables sources correspond au nombre d''enregistrements insérés dans la table cible', N'SELECT NB_LIGNES_SRC AS output_col1,
	   NB_LIGNES_TGT AS output_col2,
	   NULL AS output_col3,
	   NULL AS output_col4,
	   NULL AS output_col5,
	   NULL AS output_col6,
	   NULL AS output_col7	   
FROM
(
 SELECT MAX(NbRecordsCollected) AS NB_LIGNES_SRC,
 	   COUNT(DWH_EQUIPEMENT_CREDIT_CONSO.DAT_OBSR) AS NB_LIGNES_TGT  
 FROM
 (
   SELECT PARAM_ALIM.DAT_VALD, REQ_DATA_LOG.NbRecordsCollected FROM [$(DataFactoryDatabaseName)].dbo.PARAM_ALIM PARAM_ALIM
   JOIN 
   (
     SELECT ETLName, ProcessingStartDate, NbRecordsCollected 
     FROM
     (
       SELECT ETLName, ProcessingStartDate, NbRecordsCollected, ROW_NUMBER() OVER(ORDER BY ProcessingStartDate DESC) as FLG_DER_TRT
       FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
       WHERE FILEORTABLENAME = ''DWH_EQUIPEMENT_CREDIT_CONSO'' 
     ) DATA_LOG
     WHERE DATA_LOG.FLG_DER_TRT = 1
   ) REQ_DATA_LOG
   ON PARAM_ALIM.NOM_BATCH = REQ_DATA_LOG.ETLName
   AND PARAM_ALIM.DAT_DEBT_TRTM = REQ_DATA_LOG.ProcessingStartDate
 )REQ_DATES_VALID
 LEFT JOIN [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].dbo.DWH_EQUIPEMENT_CREDIT_CONSO 
 ON DWH_EQUIPEMENT_CREDIT_CONSO.DAT_OBSR = REQ_DATES_VALID.DAT_VALD
) REQ_FINAL
WHERE NB_LIGNES_SRC != NB_LIGNES_TGT', N'DWH', N'DWH_EQUIPEMENT_CREDIT_CONSO', N'Technique - Contrôle intégration', N'''Différence de lignes entre DATAHUB, ''+cast(output_col1 as char)+'', et DWH :''+cast(output_col2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, 298)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (39, N'IWD_DATAHUB_CONTROL_1.0.32', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_GIDB_GC_TENANT', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_GIDB_GC_TENANT', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_GIDB_GC_TENANT'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_GIDB_GC_TENANT) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (40, N'IWD_DATAHUB_CONTROL_1.0.33', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_GIDB_GC_TIME_ZONE', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_GIDB_GC_TIME_ZONE', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_GIDB_GC_TIME_ZONE'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_GIDB_GC_TIME_ZONE) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (373, N'OAV_DATAHUB_CONTROL_1.0.20', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  EMPRUNTEURS', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table EMPRUNTEURS', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from EMPRUNTEURS'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_EMPRUNTEURS group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (72, N'IWD_DATAHUB_CONTROL_1.0.61', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de fait IWD_CALLING_LIST_TO_CAMP_FACT_', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_CALLING_LIST_TO_CAMP_FACT_', N'SELECT src_nb as output_col1,  
	 cible_nb as output_col2,  
	 null as output_col3,  
	 null as output_col4,  
	 null as output_col5,  
	 null as output_col6,  
	 null as output_col7  
	 from (select * 
 from (select count(*) as src_nb from openquery(INFOMART,''select CREATE_AUDIT_KEY,UPDATE_AUDIT_KEY from V_CALLING_LIST_TO_CAMP_FACT_ '') f join [$(DataHubDatabaseName)].[dbo].IWD_CTL_AUDIT_LOG a on (F.UPDATE_AUDIT_KEY = A.AUDIT_KEY OR (F.CREATE_AUDIT_KEY = A.AUDIT_KEY AND F.UPDATE_AUDIT_KEY =0) )
join [$(DataHubDatabaseName)].[dbo].IWD_PILOTAGE_CHARGEMENT pil 
on (a.created between pil.dte_deb_periode and pil.dte_fin_periode)) a
	 join   (select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_CALLING_LIST_TO_CAMP_FACT_) b  on a.src_nb != b.cible_nb) t', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (374, N'OAV_DATAHUB_CONTROL_1.0.21', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  EXTRAITS_COMPTES', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table EXTRAITS_COMPTES', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from EXTRAITS_COMPTES'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_EXTRAITS_COMPTES group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (50, N'IWD_DATAHUB_CONTROL_1.0.15', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_GIDB_GC_CALLING_LIST', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_GIDB_GC_CALLING_LIST', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_GIDB_GC_CALLING_LIST'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_GIDB_GC_CALLING_LIST) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (51, N'IWD_DATAHUB_CONTROL_1.0.16', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_GIDB_GC_CAMPAIGN', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_GIDB_GC_CAMPAIGN', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_GIDB_GC_CAMPAIGN'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_GIDB_GC_CAMPAIGN) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (20, N'DWH_PRDT_SOUS_CREDIT_CONSO_1.0.1', N'1 - Bloquant', N'Comparaison DATAHUB/DWH table DWH_PRODUIT_SOUSCRIT', N'Ce contrôle permet de vérifier que le nombre d''enregistrements extraits des tables sources correspond au nombre d''enregistrements insérés dans la table cible', N'SELECT NB_LIGNES_SRC AS output_col1,
	   NB_LIGNES_TGT AS output_col2,
	   NULL AS output_col3,
	   NULL AS output_col4,
	   NULL AS output_col5,
	   NULL AS output_col6,
	   NULL AS output_col7	   
FROM
(
 SELECT MAX(NbRecordsCollected) AS NB_LIGNES_SRC,
 	   COUNT(DWH_PRDT_SOUS_CREDIT_CONSO.DAT_OBSR) AS NB_LIGNES_TGT  
 FROM
 (
   SELECT PARAM_ALIM.DAT_VALD, REQ_DATA_LOG.NbRecordsCollected FROM [$(DataFactoryDatabaseName)].dbo.PARAM_ALIM PARAM_ALIM
   JOIN 
   (
     SELECT ETLName, ProcessingStartDate, NbRecordsCollected 
     FROM
     (
       SELECT ETLName, ProcessingStartDate, NbRecordsCollected, ROW_NUMBER() OVER(ORDER BY ProcessingStartDate DESC) as FLG_DER_TRT
       FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
       WHERE FILEORTABLENAME = ''DWH_PRDT_SOUS_CREDIT_CONSO'' 
     ) DATA_LOG
     WHERE DATA_LOG.FLG_DER_TRT = 1
   ) REQ_DATA_LOG
   ON PARAM_ALIM.NOM_BATCH = REQ_DATA_LOG.ETLName
   AND PARAM_ALIM.DAT_DEBT_TRTM = REQ_DATA_LOG.ProcessingStartDate
 )REQ_DATES_VALID
 LEFT JOIN [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].dbo.DWH_PRDT_SOUS_CREDIT_CONSO 
 ON DWH_PRDT_SOUS_CREDIT_CONSO.DAT_OBSR = REQ_DATES_VALID.DAT_VALD
) REQ_FINAL
WHERE NB_LIGNES_SRC != NB_LIGNES_TGT', N'DWH', N'DWH_PRDT_SOUS_CREDIT_CONSO', N'Technique - Contrôle intégration', N'''Différence de lignes entre DATAHUB, ''+cast(output_col1 as char)+'', et DWH :''+cast(output_col2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, 300)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (41, N'IWD_DATAHUB_CONTROL_1.0.39', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_INTERACTION_TYPE', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_INTERACTION_TYPE', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_INTERACTION_TYPE'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_INTERACTION_TYPE) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 1, NULL, 366)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (375, N'OAV_DATAHUB_CONTROL_1.0.22', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  FORMES_JURIDIQUES', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table FORMES_JURIDIQUES', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from FORMES_JURIDIQUES'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_FORMES_JURIDIQUES group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (42, N'IWD_DATAHUB_CONTROL_1.0.40', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_MEDIA_TYPE', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_MEDIA_TYPE', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_MEDIA_TYPE'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_MEDIA_TYPE) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 1, NULL, 367)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (109, N'VOL_COLLECT_30_RAW_FE_CONSUMERLOANFINANCIALPLAN', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table RAW_FE_CONSUMERLOANFINANCIALPLAN', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table RAW_FE_CONSUMERLOANFINANCIALPLAN dans le package PKGCO_SSIS_FE_CONSUMERLOANFINANCIALPLAN_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_CONSUMERLOANFINANCIALPLAN_00'' as output_col2,
	''RAW_FE_CONSUMERLOANFINANCIALPLAN'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CONSUMERLOANFINANCIALPLAN_00''
	AND FileOrTablename= ''RAW_FE_CONSUMERLOANFINANCIALPLAN'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CONSUMERLOANFINANCIALPLAN_00''
	AND FileOrTablename= ''RAW_FE_CONSUMERLOANFINANCIALPLAN'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CONSUMERLOANFINANCIALPLAN_00''
	AND FileOrTablename= ''RAW_FE_CONSUMERLOANFINANCIALPLAN''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 59)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (370, N'OAV_DATAHUB_CONTROL_1.0.14', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  DOSSIER_COMMENTS', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table DOSSIER_COMMENTS', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from DOSSIER_COMMENTS'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_DOSSIER_COMMENTS group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (43, N'IWD_DATAHUB_CONTROL_1.0.41', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_PLACE_GROUP_FACT_', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_PLACE_GROUP_FACT_', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_PLACE_GROUP_FACT_'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_PLACE_GROUP_FACT_) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (111, N'VOL_COLLECT_30_RAW_FE_SERVICEPRODUCT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table RAW_FE_SERVICEPRODUCT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table RAW_FE_SERVICEPRODUCT dans le package PKGCO_SSIS_FE_SERVICEPRODUCT_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_SERVICEPRODUCT_00'' as output_col2,
	''RAW_FE_SERVICEPRODUCT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SERVICEPRODUCT_00''
	AND FileOrTablename= ''RAW_FE_SERVICEPRODUCT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SERVICEPRODUCT_00''
	AND FileOrTablename= ''RAW_FE_SERVICEPRODUCT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SERVICEPRODUCT_00''
	AND FileOrTablename= ''RAW_FE_SERVICEPRODUCT''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 92)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (362, N'OAV_DATAHUB_CONTROL_1.0.4', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  BACKUPS', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table BACKUPS', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from BACKUPS'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_BACKUPS group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (44, N'IWD_DATAHUB_CONTROL_1.0.43', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_RECORD_FIELD_GROUP_2', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_RECORD_FIELD_GROUP_2', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_RECORD_FIELD_GROUP_2'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_RECORD_FIELD_GROUP_2) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (112, N'VOL_COLLECT_30_PV_FI_Q_COMPL', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_FI_Q_COMPL', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_FI_Q_COMPL dans le package PKGCO_SSIS_FI_XSIQCPO_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FI_XSIQCPO_00'' as output_col2,
	''PV_FI_Q_COMPL'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FI_XSIQCPO_00''
	AND FileOrTablename= ''PV_FI_Q_COMPL'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FI_XSIQCPO_00''
	AND FileOrTablename= ''PV_FI_Q_COMPL'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FI_XSIQCPO_00''
	AND FileOrTablename= ''PV_FI_Q_COMPL''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Franfinance', N'VOL_COLLECT_Franfinance', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 101)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (369, N'OAV_DATAHUB_CONTROL_1.0.13', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  DEPARTEMENTS', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table DEPARTEMENTS', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from DEPARTEMENTS'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_DEPARTEMENTS group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (45, N'IWD_DATAHUB_CONTROL_1.0.44', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_RECORD_STATUS', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_RECORD_STATUS', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_RECORD_STATUS'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_RECORD_STATUS) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (361, N'OAV_DATAHUB_CONTROL_1.0.3', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  ASSURANCES', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table ASSURANCES', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from ASSURANCES'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_ASSURANCES group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (110, N'VOL_COLLECT_30_REF_CONSUMER_LOAN_TERM_REASON', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CONSUMER_LOAN_TERM_REASON', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CONSUMER_LOAN_TERM_REASON dans le package PKGCO_SSIS_FE_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_REFERENTIEL_00'' as output_col2,
	''REF_CONSUMER_LOAN_TERM_REASON'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CONSUMER_LOAN_TERM_REASON'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CONSUMER_LOAN_TERM_REASON'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CONSUMER_LOAN_TERM_REASON''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 83)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (372, N'OAV_DATAHUB_CONTROL_1.0.16', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  DOSSIERS', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table DOSSIERS', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from DOSSIERS'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_DOSSIERS group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (359, N'VOL_COLLECT_30_REF_HISTORIQUE_MENSUEL_TAUX', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_HISTORIQUE_MENSUEL_TAUX', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_HISTORIQUE_MENSUEL_TAUX dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_HISTORIQUE_MENSUEL_TAUX'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_HISTORIQUE_MENSUEL_TAUX'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_HISTORIQUE_MENSUEL_TAUX'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_HISTORIQUE_MENSUEL_TAUX''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 141)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (360, N'VOL_COLLECT_30_REF_LIBELLES_DES_CONVENTIONS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_LIBELLES_DES_CONVENTIONS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_LIBELLES_DES_CONVENTIONS dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_LIBELLES_DES_CONVENTIONS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_LIBELLES_DES_CONVENTIONS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_LIBELLES_DES_CONVENTIONS'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_LIBELLES_DES_CONVENTIONS''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 142)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (154, N'VOL_COLLECT_30_REF_FINANCING_TYPE_FAMILY', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_FINANCING_TYPE_FAMILY', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_FINANCING_TYPE_FAMILY dans le package PKGCO_SSIS_FE_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_REFERENTIEL_00'' as output_col2,
	''REF_FINANCING_TYPE_FAMILY'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_FINANCING_TYPE_FAMILY'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_FINANCING_TYPE_FAMILY'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_FINANCING_TYPE_FAMILY''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 86)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (46, N'IWD_DATAHUB_CONTROL_1.0.45', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_RECORD_TYPE', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_RECORD_TYPE', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_RECORD_TYPE'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_RECORD_TYPE) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (55, N'IWD_DATAHUB_CONTROL_1.0.58', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_WORKBIN', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_WORKBIN', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_WORKBIN'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_WORKBIN) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (155, N'VOL_COLLECT_30_RAW_FE_CUSTOMERENRFOLDERASSOC', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table RAW_FE_CUSTOMERENRFOLDERASSOC', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table RAW_FE_CUSTOMERENRFOLDERASSOC dans le package PKGCO_SSIS_FE_CUSTOMERENRFOLDERASSOC_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_CUSTOMERENRFOLDERASSOC_00'' as output_col2,
	''RAW_FE_CUSTOMERENRFOLDERASSOC'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CUSTOMERENRFOLDERASSOC_00''
	AND FileOrTablename= ''RAW_FE_CUSTOMERENRFOLDERASSOC'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CUSTOMERENRFOLDERASSOC_00''
	AND FileOrTablename= ''RAW_FE_CUSTOMERENRFOLDERASSOC'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CUSTOMERENRFOLDERASSOC_00''
	AND FileOrTablename= ''RAW_FE_CUSTOMERENRFOLDERASSOC''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 63)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (63, N'IWD_DATAHUB_CONTROL_1.0.48', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_RESOURCE_', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_RESOURCE_', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_RESOURCE_'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_RESOURCE_) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 1, NULL, 375)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (64, N'IWD_DATAHUB_CONTROL_1.0.49', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_RESOURCE_GROUP_COMBINATION', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_RESOURCE_GROUP_COMBINATION', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_RESOURCE_GROUP_COMBINATION'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_RESOURCE_GROUP_COMBINATION) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (156, N'VOL_COLLECT_30_PV_FE_CUSTOMERENRFOLDERASSOC', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_FE_CUSTOMERENRFOLDERASSOC', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_FE_CUSTOMERENRFOLDERASSOC dans le package PKGCO_SSIS_FE_CUSTOMERENRFOLDERASSOC_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_CUSTOMERENRFOLDERASSOC_01'' as output_col2,
	''PV_FE_CUSTOMERENRFOLDERASSOC'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CUSTOMERENRFOLDERASSOC_01''
	AND FileOrTablename= ''PV_FE_CUSTOMERENRFOLDERASSOC'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CUSTOMERENRFOLDERASSOC_01''
	AND FileOrTablename= ''PV_FE_CUSTOMERENRFOLDERASSOC'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CUSTOMERENRFOLDERASSOC_01''
	AND FileOrTablename= ''PV_FE_CUSTOMERENRFOLDERASSOC''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 64)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (59, N'IWD_DATAHUB_CONTROL_1.0.19', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_GIDB_GC_FILTER', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_GIDB_GC_FILTER', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_GIDB_GC_FILTER'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_GIDB_GC_FILTER) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (60, N'IWD_DATAHUB_CONTROL_1.0.20', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_GIDB_GC_FOLDER', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_GIDB_GC_FOLDER', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_GIDB_GC_FOLDER'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_GIDB_GC_FOLDER) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (157, N'VOL_COLLECT_30_RAW_FE_CUSTOMERSUBPRODUCTASSOC', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table RAW_FE_CUSTOMERSUBPRODUCTASSOC', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table RAW_FE_CUSTOMERSUBPRODUCTASSOC dans le package PKGCO_SSIS_FE_CUSTOMERSUBPRODUCTASSOC_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_CUSTOMERSUBPRODUCTASSOC_00'' as output_col2,
	''RAW_FE_CUSTOMERSUBPRODUCTASSOC'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CUSTOMERSUBPRODUCTASSOC_00''
	AND FileOrTablename= ''RAW_FE_CUSTOMERSUBPRODUCTASSOC'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CUSTOMERSUBPRODUCTASSOC_00''
	AND FileOrTablename= ''RAW_FE_CUSTOMERSUBPRODUCTASSOC'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CUSTOMERSUBPRODUCTASSOC_00''
	AND FileOrTablename= ''RAW_FE_CUSTOMERSUBPRODUCTASSOC''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 65)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (57, N'IWD_DATAHUB_CONTROL_1.0.17', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_GIDB_GC_ENDPOINT', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_GIDB_GC_ENDPOINT', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_GIDB_GC_ENDPOINT'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_GIDB_GC_ENDPOINT) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (58, N'IWD_DATAHUB_CONTROL_1.0.18', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_GIDB_GC_FIELD', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_GIDB_GC_FIELD', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_GIDB_GC_FIELD'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_GIDB_GC_FIELD) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (158, N'VOL_COLLECT_30_PV_FE_CUSTOMERSUBPRODUCTASSOC', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_FE_CUSTOMERSUBPRODUCTASSOC', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_FE_CUSTOMERSUBPRODUCTASSOC dans le package PKGCO_SSIS_FE_CUSTOMERSUBPRODUCTASSOC_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_CUSTOMERSUBPRODUCTASSOC_01'' as output_col2,
	''PV_FE_CUSTOMERSUBPRODUCTASSOC'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CUSTOMERSUBPRODUCTASSOC_01''
	AND FileOrTablename= ''PV_FE_CUSTOMERSUBPRODUCTASSOC'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CUSTOMERSUBPRODUCTASSOC_01''
	AND FileOrTablename= ''PV_FE_CUSTOMERSUBPRODUCTASSOC'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CUSTOMERSUBPRODUCTASSOC_01''
	AND FileOrTablename= ''PV_FE_CUSTOMERSUBPRODUCTASSOC''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 66)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (49, N'IWD_DATAHUB_CONTROL_1.0.47', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_REQUESTED_SKILL_COMBINATION', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_REQUESTED_SKILL_COMBINATION', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_REQUESTED_SKILL_COMBINATION'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_REQUESTED_SKILL_COMBINATION) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (364, N'OAV_DATAHUB_CONTROL_1.0.8', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  CIVILITE', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table CIVILITE', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from CIVILITE'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_CIVILITE group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (160, N'VOL_COLLECT_30_PV_FE_ENROLMENTFOLDER', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_FE_ENROLMENTFOLDER', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_FE_ENROLMENTFOLDER dans le package PKGCO_SSIS_FE_ENROLMENTFOLDER_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_ENROLMENTFOLDER_01'' as output_col2,
	''PV_FE_ENROLMENTFOLDER'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_ENROLMENTFOLDER_01''
	AND FileOrTablename= ''PV_FE_ENROLMENTFOLDER'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_ENROLMENTFOLDER_01''
	AND FileOrTablename= ''PV_FE_ENROLMENTFOLDER'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_ENROLMENTFOLDER_01''
	AND FileOrTablename= ''PV_FE_ENROLMENTFOLDER''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 68)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (162, N'VOL_COLLECT_30_RAW_FE_EQUIPMENT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table RAW_FE_EQUIPMENT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table RAW_FE_EQUIPMENT dans le package PKGCO_SSIS_FE_EQUIPMENT_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_EQUIPMENT_00'' as output_col2,
	''RAW_FE_EQUIPMENT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_EQUIPMENT_00''
	AND FileOrTablename= ''RAW_FE_EQUIPMENT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_EQUIPMENT_00''
	AND FileOrTablename= ''RAW_FE_EQUIPMENT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_EQUIPMENT_00''
	AND FileOrTablename= ''RAW_FE_EQUIPMENT''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 69)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (161, N'VOL_COLLECT_30_PV_FE_SUBSCRIBEDCONSUMERLOAN', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_FE_SUBSCRIBEDCONSUMERLOAN', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_FE_SUBSCRIBEDCONSUMERLOAN dans le package PKGCO_SSIS_FE_SUBSCRIBEDCONSUMERLOAN_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_SUBSCRIBEDCONSUMERLOAN_01'' as output_col2,
	''PV_FE_SUBSCRIBEDCONSUMERLOAN'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SUBSCRIBEDCONSUMERLOAN_01''
	AND FileOrTablename= ''PV_FE_SUBSCRIBEDCONSUMERLOAN'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SUBSCRIBEDCONSUMERLOAN_01''
	AND FileOrTablename= ''PV_FE_SUBSCRIBEDCONSUMERLOAN'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SUBSCRIBEDCONSUMERLOAN_01''
	AND FileOrTablename= ''PV_FE_SUBSCRIBEDCONSUMERLOAN''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 95)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (48, N'IWD_DATAHUB_CONTROL_1.0.46', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_REQUESTED_SKILL', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_REQUESTED_SKILL', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_REQUESTED_SKILL'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_REQUESTED_SKILL) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (54, N'IWD_DATAHUB_CONTROL_1.0.21', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_GIDB_GC_FORMAT', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_GIDB_GC_FORMAT', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_GIDB_GC_FORMAT'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_GIDB_GC_FORMAT) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (163, N'VOL_COLLECT_30_PV_FE_EQUIPMENT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_FE_EQUIPMENT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_FE_EQUIPMENT dans le package PKGCO_SSIS_FE_EQUIPMENT_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_EQUIPMENT_01'' as output_col2,
	''PV_FE_EQUIPMENT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_EQUIPMENT_01''
	AND FileOrTablename= ''PV_FE_EQUIPMENT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_EQUIPMENT_01''
	AND FileOrTablename= ''PV_FE_EQUIPMENT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_EQUIPMENT_01''
	AND FileOrTablename= ''PV_FE_EQUIPMENT''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 70)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (164, N'VOL_COLLECT_30_RAW_FE_EQUIPMENTSVCASSOC', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table RAW_FE_EQUIPMENTSVCASSOC', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table RAW_FE_EQUIPMENTSVCASSOC dans le package PKGCO_SSIS_FE_EQUIPMENTSVCASSOC_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_EQUIPMENTSVCASSOC_00'' as output_col2,
	''RAW_FE_EQUIPMENTSVCASSOC'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_EQUIPMENTSVCASSOC_00''
	AND FileOrTablename= ''RAW_FE_EQUIPMENTSVCASSOC'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_EQUIPMENTSVCASSOC_00''
	AND FileOrTablename= ''RAW_FE_EQUIPMENTSVCASSOC'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_EQUIPMENTSVCASSOC_00''
	AND FileOrTablename= ''RAW_FE_EQUIPMENTSVCASSOC''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 71)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (165, N'VOL_COLLECT_30_PV_FE_EQUIPMENTSVCASSOC', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_FE_EQUIPMENTSVCASSOC', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_FE_EQUIPMENTSVCASSOC dans le package PKGCO_SSIS_FE_EQUIPMENTSVCASSOC_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_EQUIPMENTSVCASSOC_01'' as output_col2,
	''PV_FE_EQUIPMENTSVCASSOC'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_EQUIPMENTSVCASSOC_01''
	AND FileOrTablename= ''PV_FE_EQUIPMENTSVCASSOC'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_EQUIPMENTSVCASSOC_01''
	AND FileOrTablename= ''PV_FE_EQUIPMENTSVCASSOC'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_EQUIPMENTSVCASSOC_01''
	AND FileOrTablename= ''PV_FE_EQUIPMENTSVCASSOC''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 72)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (166, N'VOL_COLLECT_30_PV_FE_PRODUCTOFFERASSOC', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_FE_PRODUCTOFFERASSOC', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_FE_PRODUCTOFFERASSOC dans le package PKGCO_SSIS_FE_PRODUCTOFFERASSOC_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_PRODUCTOFFERASSOC_00'' as output_col2,
	''PV_FE_PRODUCTOFFERASSOC'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_PRODUCTOFFERASSOC_00''
	AND FileOrTablename= ''PV_FE_PRODUCTOFFERASSOC'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_PRODUCTOFFERASSOC_00''
	AND FileOrTablename= ''PV_FE_PRODUCTOFFERASSOC'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_PRODUCTOFFERASSOC_00''
	AND FileOrTablename= ''PV_FE_PRODUCTOFFERASSOC''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 73)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (113, N'VOL_COLLECT_30_PV_MC_CAMPAIGNCOMPLETED', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_MC_CAMPAIGNCOMPLETED', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_MC_CAMPAIGNCOMPLETED dans le package PKGCO_SSIS_MC_XSIHCACO_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_MC_XSIHCACO_00'' as output_col2,
	''PV_MC_CAMPAIGNCOMPLETED'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	NULL as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHCACO_00''
	AND FileOrTablename= ''PV_MC_CAMPAIGNCOMPLETED'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHCACO_00''
	AND FileOrTablename= ''PV_MC_CAMPAIGNCOMPLETED''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Marketing Cloud', N'VOL_COLLECT_Marketing Cloud', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement de la semaine précédente''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 107)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (47, N'IWD_DATAHUB_CONTROL_1.0.72', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de fait IWD_SM_RES_STATE_REASON_FACT', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_SM_RES_STATE_REASON_FACT', N'SELECT src_nb as output_col1,  
	 cible_nb as output_col2,  
	 null as output_col3,  
	 null as output_col4,  
	 null as output_col5,  
	 null as output_col6,  
	 null as output_col7  
	 from (select * 
 from (select count(*) as src_nb from openquery(INFOMART,''select CREATE_AUDIT_KEY,UPDATE_AUDIT_KEY from V_SM_RES_STATE_REASON_FACT'') f
join [$(DataHubDatabaseName)].[dbo].IWD_CTL_AUDIT_LOG a on (F.UPDATE_AUDIT_KEY = A.AUDIT_KEY OR (F.CREATE_AUDIT_KEY = A.AUDIT_KEY AND F.UPDATE_AUDIT_KEY =0) )
join [$(DataHubDatabaseName)].[dbo].IWD_PILOTAGE_CHARGEMENT pil 
on (a.created between pil.dte_deb_periode and pil.dte_fin_periode)) a
	 join   (select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_SM_RES_STATE_REASON_FACT) b  on a.src_nb != b.cible_nb) t', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 1, NULL, 398)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (56, N'IWD_DATAHUB_CONTROL_1.0.70', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de fait IWD_SM_RES_SESSION_FACT', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_SM_RES_SESSION_FACT', N'SELECT src_nb as output_col1,  
	 cible_nb as output_col2,  
	 null as output_col3,  
	 null as output_col4,  
	 null as output_col5,  
	 null as output_col6,  
	 null as output_col7  
	 from (select * 
 from (select count(*) as src_nb from openquery(INFOMART,''select CREATE_AUDIT_KEY,UPDATE_AUDIT_KEY from V_SM_RES_SESSION_FACT'') f
join [$(DataHubDatabaseName)].[dbo].IWD_CTL_AUDIT_LOG a on (F.UPDATE_AUDIT_KEY = A.AUDIT_KEY OR (F.CREATE_AUDIT_KEY = A.AUDIT_KEY AND F.UPDATE_AUDIT_KEY =0) )
join [$(DataHubDatabaseName)].[dbo].IWD_PILOTAGE_CHARGEMENT pil 
on (a.created between pil.dte_deb_periode and pil.dte_fin_periode)) a
	 join   (select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_SM_RES_SESSION_FACT) b  on a.src_nb != b.cible_nb) t', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 1, NULL, 396)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (67, N'IWD_DATAHUB_CONTROL_1.0.50', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_RESOURCE_GROUP_FACT_', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_RESOURCE_GROUP_FACT_', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_RESOURCE_GROUP_FACT_'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_RESOURCE_GROUP_FACT_) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (61, N'IWD_DATAHUB_CONTROL_1.0.65', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de fait IWD_INTERACTION_FACT', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_INTERACTION_FACT', N'SELECT src_nb as output_col1,  
	 cible_nb as output_col2,  
	 null as output_col3,  
	 null as output_col4,  
	 null as output_col5,  
	 null as output_col6,  
	 null as output_col7  
	 from (select * 
 from (select count(*) as src_nb from openquery(INFOMART,''select CREATE_AUDIT_KEY,UPDATE_AUDIT_KEY from V_INTERACTION_FACT'') f 
join [$(DataHubDatabaseName)].[dbo].IWD_CTL_AUDIT_LOG a on (F.UPDATE_AUDIT_KEY = A.AUDIT_KEY OR (F.CREATE_AUDIT_KEY = A.AUDIT_KEY AND F.UPDATE_AUDIT_KEY =0) )
join [$(DataHubDatabaseName)].[dbo].IWD_PILOTAGE_CHARGEMENT pil 
on (a.created between pil.dte_deb_periode and pil.dte_fin_periode)) a
	 join   (select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_INTERACTION_FACT) b  on a.src_nb != b.cible_nb) t', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (68, N'IWD_DATAHUB_CONTROL_1.0.51', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de dimension IWD_RESOURCE_SKILL_FACT_', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_RESOURCE_SKILL_FACT_', N'SELECT src_nb as output_col1, 
cible_nb as output_col2,
null as output_col3,
null as output_col4,
null as output_col5,
null as output_col6,
null as output_col7
from openquery(INFOMART, ''select count(*) as src_nb from V_RESOURCE_SKILL_FACT_'') a
join 
(select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_RESOURCE_SKILL_FACT_) b
on a.src_nb != b.cible_nb', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (66, N'IWD_DATAHUB_CONTROL_1.0.62', N'2 - Warning', N'Comparaison INFOMART/DATAHUB table de fait IWD_CAMPAIGN_GROUP_SESSION_FACT', N'Ce contrôle compare le nombre de lignes entre INFOMART (Oracle) et DATAHUB pour la table IWD_CAMPAIGN_GROUP_SESSION_FACT', N'SELECT src_nb as output_col1,  
	 cible_nb as output_col2,  
	 null as output_col3,  
	 null as output_col4,  
	 null as output_col5,  
	 null as output_col6,  
	 null as output_col7  
	 from (select * 
 from (select count(*) as src_nb from openquery(INFOMART,''select CREATE_AUDIT_KEY,UPDATE_AUDIT_KEY from V_CAMPAIGN_GROUP_SESSION_FACT '') f 
join [$(DataHubDatabaseName)].[dbo].IWD_CTL_AUDIT_LOG a on (F.UPDATE_AUDIT_KEY = A.AUDIT_KEY OR (F.CREATE_AUDIT_KEY = A.AUDIT_KEY AND F.UPDATE_AUDIT_KEY =0) )
join [$(DataHubDatabaseName)].[dbo].IWD_PILOTAGE_CHARGEMENT pil 
on (a.created between pil.dte_deb_periode and pil.dte_fin_periode)) a
	 join   (select count(*) as cible_nb from [$(DataHubDatabaseName)].[dbo].IWD_CAMPAIGN_GROUP_SESSION_FACT) b  on a.src_nb != b.cible_nb) t', N'IWD', N'IWD - CONTROLE DATAHUB', N'Technique - Contrôle intégration', N'''Différence de lignes entre INFOMART, ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre lignes en source (INFOMART)', N'Nombre de lignes en cible (DataHub)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (23, N'DWH_CONTROL_PERSONNE_1.0.1', N'1 - Bloquant', N'Test l''existence ou non de la date observation', N'Ce contrôle teste la valeur de la DAT_OBSR', N'SELECT [SF_IDNT_SF_PERS] as output_col1,      
 [SA_NUMR_CLNT_SAB] as output_col2,   
 [FE_IDNT_FE_CLNT] as output_col3,   
 FE_IDNT_SF_PERS as output_col4,  
  null as output_col5,  null as output_col6,   null as output_col7         FROM [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].[dbo].[DWH_PERSONNE]
  WHERE(DAT_OBSR is  Null)', N'DWH', N'DWH_PERSONNE', N'Technique - Contrôle RG', N'''La valeur de la colonne Date_Obsr est obligatoire''', N'Identifiant SF de la personne (SF_IDNT_SF_PERS)', N'Numéro Client SAB (SA_NUMR_CLNT_SAB)', N'Identifiant FE Client (FE_IDNT_FE_CLNT)', N'Identifiant SF de la personne (FE_IDNT_SF_PERS)', NULL, NULL, NULL, 0, NULL, 297)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (14, N'DWH_OPPORTUNITE_1.0.10', N'1 - Bloquant', N'Comparaison DATAHUB/DWH table DWH_OPPORTUNITE', N'Ce contrôle permet de vérifier que le nombre d''enregistrements extraits des tables sources correspond au nombre d''enregistrements insérés dans la table cible', N'SELECT NB_LIGNES_SRC AS output_col1,
	   NB_LIGNES_TGT AS output_col2,
	   NULL AS output_col3,
	   NULL AS output_col4,
	   NULL AS output_col5,
	   NULL AS output_col6,
	   NULL AS output_col7	   
FROM
(
 SELECT MAX(NbRecordsCollected) AS NB_LIGNES_SRC,
 	   COUNT(DWH_OPPORTUNITE.DAT_OBSR) AS NB_LIGNES_TGT  
 FROM
 (
   SELECT PARAM_ALIM.DAT_VALD, REQ_DATA_LOG.NbRecordsCollected FROM [$(DataFactoryDatabaseName)].dbo.PARAM_ALIM PARAM_ALIM
   JOIN 
   (
     SELECT ETLName, ProcessingStartDate, NbRecordsCollected 
     FROM
     (
       SELECT ETLName, ProcessingStartDate, NbRecordsCollected, ROW_NUMBER() OVER(ORDER BY ProcessingStartDate DESC) as FLG_DER_TRT
       FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
       WHERE FILEORTABLENAME = ''DWH_OPPORTUNITE'' 
     ) DATA_LOG
     WHERE DATA_LOG.FLG_DER_TRT = 1
   ) REQ_DATA_LOG
   ON PARAM_ALIM.NOM_BATCH = REQ_DATA_LOG.ETLName
   AND PARAM_ALIM.DAT_DEBT_TRTM = REQ_DATA_LOG.ProcessingStartDate
 )REQ_DATES_VALID
 LEFT JOIN [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].dbo.DWH_OPPORTUNITE  
 ON DWH_OPPORTUNITE.DAT_OBSR = REQ_DATES_VALID.DAT_VALD
) REQ_FINAL
WHERE NB_LIGNES_SRC != NB_LIGNES_TGT', N'DWH', N'DWH_OPPORTUNITE', N'Technique - Contrôle intégration', N'''Différence de lignes entre DATAHUB, ''+cast(output_col1 as char)+'', et DWH :''+cast(output_col2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, 291)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (365, N'OAV_DATAHUB_CONTROL_1.0.9', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  CODE_INSEE', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table CODE_INSEE', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from CODE_INSEE'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_CODE_INSEE group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (377, N'OAV_DATAHUB_CONTROL_1.0.24', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  LIEN_EMPRUNTEURS', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table LIEN_EMPRUNTEURS', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from LIEN_EMPRUNTEURS'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_LIEN_EMPRUNTEURS group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (378, N'OAV_DATAHUB_CONTROL_1.0.25', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  MANDATS_PRELEVEMENTS', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table MANDATS_PRELEVEMENTS', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from MANDATS_PRELEVEMENTS'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_MANDATS_PRELEVEMENTS group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (366, N'OAV_DATAHUB_CONTROL_1.0.10', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  CODE_NAF', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table CODE_NAF', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from CODE_NAF'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_CODE_NAF group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (376, N'OAV_DATAHUB_CONTROL_1.0.23', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  JUSTIFICATIFS_SANTE', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table JUSTIFICATIFS_SANTE', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from JUSTIFICATIFS_SANTE'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_JUSTIFICATIFS_SANTE group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (404, N'BFR_CONTROL_CLIENT_1', N'1 - Bloquant', N'Détection du champ DWHCLIRU2 non renseigné', N'Ce contrôle génère une alerte si le champ DWHCLIRU2 n''''est pas renseigné', N'select DWHCLICLI as output_col1, isnull(nullif(LTRIM(RTRIM(DWHCLIRU2)),''''),''valeur NR'') as output_col2, NULL AS output_col3, NULL AS output_col4, NULL AS output_col5,   
NULL AS output_col6, NULL AS output_col7  from [$(DataHubDatabaseName)].dbo.[PV_SA_M_CLIENT]  where DWHCLICOL=0 and DWHCLIDTX= (select max(DWHCLIDTX) from [$(DataHubDatabaseName)].dbo.[PV_SA_M_CLIENT]) 
and ( DWHCLIRU2 is null OR LTRIM(RTRIM(DWHCLIRU2))='''' ) and DWHCLIAGE not in (51,52)', N'DataHub', N'DATAHUB_BFR', N'Fonctionnel', N'''Le champ ( DWHCLIRU2 ) de la table PV_SA_M_CLIENT n''''est pas renseigné pour le client :''+Cast(output_col1 as char)', N'ID CLIENT', N'DWHCLIRU2', NULL, NULL, NULL, NULL, NULL, 1, NULL, 559)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (108, N'BFR_CONTROL_CLIENT_3', N'1 - Bloquant', N'vérification des valeurs du champ DWHCLIRU2 dans la table de référence SIDP REF_TRANSCO_MARQUE', N'ce contrôle génère une alerte si la valeur du champ DWHCLIRU2 de la table PV_SA_M_CLIENT n''existe pas dans la table REF_TRANSCO_MARQUE ', N'select DWHCLICLI as output_col1, DWHCLIRU2 as output_col2, NULL AS output_col3, NULL AS output_col4, NULL AS output_col5, NULL AS output_col6, NULL AS output_col7       
from [$(DataHubDatabaseName)].dbo.[PV_SA_M_CLIENT] where  DWHCLIDTX= (select max(DWHCLIDTX) from [$(DataHubDatabaseName)].dbo.[PV_SA_M_CLIENT]) 
and DWHCLIRU2 not in (select CODE_APPORTEUR from [$(DataHubDatabaseName)].dbo.REF_RESEAU) and LTRIM(RTRIM(isnull(DWHCLIRU2,''''))) not in ('''')
and DWHCLIAGE not in (51,52)', N'DataHub', N'DATAHUB_BFR', N'Fonctionnel', N'''la valeur ( ''+Cast(output_col2 as varchar(50))+'' ) de la colonne ( DWHCLIRU2 ) de la table PV_SA_M_CLIENT n''''existe pas dans la table ( REF_TRANSCO_MARQUE )''', N'ID CLIENT', N'DWHCLIRU2', NULL, NULL, NULL, NULL, NULL, 1, NULL, 559)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (406, N'BFR_CONTROL_CLIENT_4', N'1 - Bloquant', N'détection du champ DWHCLIRES non renseigné', N'ce contrôle génère une alerte si le champ DWHCLIRES de la table PV_SA_M_CLIENT n''est pas renseigné ', N'select DWHCLICLI as output_col1, isnull(nullif(LTRIM(RTRIM(Cast(DWHCLIRES as varchar(15)))),''''),''NR'') as output_col2, NULL AS output_col3, NULL AS output_col4, NULL AS output_col5, NULL AS output_col6,
NULL AS output_col7  from [$(DataHubDatabaseName)].dbo.[PV_SA_M_CLIENT] where DWHCLIDTX= (select max(DWHCLIDTX)   from [$(DataHubDatabaseName)].dbo.[PV_SA_M_CLIENT]) AND ( DWHCLIRES is null OR LTRIM(RTRIM(DWHCLIRES))='''' )      
and DWHCLIAGE not in (51,52)', N'DataHub', N'DATAHUB_BFR', N'Fonctionnel', N'''Le champ ( DWHCLIRES ) de la table PV_SA_M_CLIENT n''''est pas renseigné pour le client : ''+Cast(output_col1 as char)', N'ID CLIENT', N'DWHCLIRU2', NULL, NULL, NULL, NULL, NULL, 1, NULL, 559)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (407, N'BFR_CONTROL_COMPTE_1', N'1 - Bloquant', N'détection du champ DWHCLIRUB non renseigné', N'ce contrôle génère une alerte si le champ DWHCLIRUB de la table PV_SA_M_COMPTE n''est pas renseigné', N'select DWHCPTCOM as output_col1, isnull(nullif(LTRIM(RTRIM(Cast(DWHCPTRUB as varchar(15)))),''''),''NR'') as output_col2, NULL AS output_col3, NULL AS output_col4, NULL AS output_col5, NULL AS output_col6,
NULL AS output_col7 from [$(DataHubDatabaseName)].dbo.[PV_SA_M_COMPTE] where DWHCPTDTX= (select max(DWHCPTDTX) from [$(DataHubDatabaseName)].dbo.[PV_SA_M_COMPTE] ) AND (DWHCPTRUB is null OR LTRIM(RTRIM(DWHCPTRUB))='''' )
and DWHCPTAGE not in (51,52)', N'DataHub', N'DATAHUB_BFR', N'Fonctionnel', N'''Le champ ( DWHCPTRUB ) de la table PV_SA_M_COMPTE n''''est pas renseigné pour le compte : ''+Cast(output_col1 as char)', N'DWHCPTCOM', N'DWHCPTRUB', NULL, NULL, NULL, NULL, NULL, 1, NULL, 559)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (99, N'DWH_CONTROL_LIEN_PERSONNE_1.01', N'1 - Bloquant', N'Contrôle d''intégrité référentielle', N'Ce contrôle consiste à vérifier que tous les identifiants SF Personne de la table DWH_LIEN_PERSONNE sont présents dans la table DWH_PERSONNE', N'SELECT IDNT_PERS_SF AS output_col1, DAT_OBSR AS output_col2, NULL AS output_col3, NULL AS output_col4, NULL AS output_col5, NULL AS output_col6, NULL AS output_col7
FROM (
SELECT DISTINCT IDNT_SF_PERS1 AS IDNT_PERS_SF, DAT_OBSR
FROM [OBKSVWCP24\INSTP00].[SIDP_DWH].[dbo].DWH_LIEN_PERSONNE
WHERE DAT_OBSR IN (SELECT PARAM_ALIM.DAT_VALD FROM [SIDP_DataFactory].dbo.PARAM_ALIM PARAM_ALIM
                   JOIN (SELECT ETLName, ProcessingStartDate, ROW_NUMBER() OVER(ORDER BY ProcessingStartDate DESC) as FLG_DER_TRT
                         FROM [SIDP_DataHub].dbo.DATA_LOG WITH(NOLOCK)
                         WHERE FILEORTABLENAME = ''DWH_LIEN_PERSONNE'') DATA_LOG
                   ON PARAM_ALIM.NOM_BATCH = DATA_LOG.ETLName AND PARAM_ALIM.DAT_DEBT_TRTM = DATA_LOG.ProcessingStartDate AND DATA_LOG.FLG_DER_TRT = 1)
UNION 
SELECT DISTINCT IDNT_SF_PERS2 AS IDNT_PERS_SF, DAT_OBSR
FROM [OBKSVWCP24\INSTP00].[SIDP_DWH].[dbo].DWH_LIEN_PERSONNE
WHERE DAT_OBSR IN (SELECT PARAM_ALIM.DAT_VALD FROM [SIDP_DataFactory].dbo.PARAM_ALIM PARAM_ALIM
                   JOIN (SELECT ETLName, ProcessingStartDate, ROW_NUMBER() OVER(ORDER BY ProcessingStartDate DESC) as FLG_DER_TRT
                         FROM [SIDP_DataHub].dbo.DATA_LOG WITH(NOLOCK)
                         WHERE FILEORTABLENAME = ''DWH_LIEN_PERSONNE'') DATA_LOG
                   ON PARAM_ALIM.NOM_BATCH = DATA_LOG.ETLName AND PARAM_ALIM.DAT_DEBT_TRTM = DATA_LOG.ProcessingStartDate AND DATA_LOG.FLG_DER_TRT = 1)
EXCEPT
SELECT DISTINCT IDNT_PERS_SF AS IDNT_PERS_SF, DAT_OBSR
FROM [OBKSVWCP24\INSTP00].[SIDP_DWH].[dbo].DWH_PERSONNE
WHERE DAT_OBSR IN (SELECT PARAM_ALIM.DAT_VALD FROM [SIDP_DataFactory].dbo.PARAM_ALIM PARAM_ALIM
                   JOIN (SELECT ETLName, ProcessingStartDate, ROW_NUMBER() OVER(ORDER BY ProcessingStartDate DESC) as FLG_DER_TRT
                         FROM [SIDP_DataHub].dbo.DATA_LOG WITH(NOLOCK)
                         WHERE FILEORTABLENAME = ''DWH_LIEN_PERSONNE'') DATA_LOG
                   ON PARAM_ALIM.NOM_BATCH = DATA_LOG.ETLName AND PARAM_ALIM.DAT_DEBT_TRTM = DATA_LOG.ProcessingStartDate AND DATA_LOG.FLG_DER_TRT = 1)
) REQ WHERE IDNT_PERS_SF IS NOT NULL', N'DWH', N'DWH_LIEN_PERSONNE', N'Technique - Contrôle intégration', N'''L''+char(39)+''identifiant SF Personne "''+LTRIM(RTRIM(CAST(OUTPUT_COL1 AS CHAR)))+''" n''+char(39)+''est pas présent dans DWH_PERSONNE pour la date d''+char(39)+''observation "''+LTRIM(RTRIM(CAST(OUTPUT_COL2 AS CHAR)))+''"''', N'Identifiant SF Personne (IDNT_SF_PERS)', N'Date d''observation (DAT_OBSR)', NULL, NULL, NULL, NULL, NULL, 1, NULL, 318)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (412, N'BFR_CONTROL_FFCR_2', N'1 - Bloquant', N'vérification des valeurs du champ NUM_PRE dans la table des références externes PV_SA_Q_COM', N'ce contrôle génère une alerte si la valeur du champ NUM_PRE de la table PV_FI_M_COMPL n''existe pas dans la table PV_SA_Q_COM', N'select NUM_PRE as output_col1, NULL as output_col2, NULL AS output_col3, NULL AS output_col4, NULL AS output_col5, NULL AS output_col6, NULL AS output_col7 
from [$(DataHubDatabaseName)].dbo.[PV_FI_M_COMPL] 
where NUM_PRE not in (select COMREFREF from [$(DataHubDatabaseName)].dbo.PV_SA_Q_COM)', N'DataHub', N'DATAHUB_BFR', N'Fonctionnel', N'''la valeur ( ''+Cast(output_col1 as varchar(10))+'' ) du champ ( NUM_PRE ) de la table PV_FI_M_COMPL n''''existe pas dans la table ( PV_SA_Q_COM )''', N'NUM_PRE', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 559)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (106, N'BFR_CONTROL_CLIENT_5', N'1 - Bloquant', N'vérification des valeurs du champ DWHCLIRES dans la table de référence SIDP REF_TRANSCO_MARCHE', N'ce contrôle génère une alerte si la valeur du champ DWHCLIRES de la table PV_SA_M_CLIENT n''existe pas dans la table REF_TRANSCO_MARCHE', N'select DWHCLICLI as output_col1, DWHCLIRES as output_col2, NULL AS output_col3, NULL AS output_col4, NULL AS output_col5, NULL AS output_col6, NULL AS output_col7
from [$(DataHubDatabaseName)].dbo.[PV_SA_M_CLIENT] where DWHCLIDTX= (select max(DWHCLIDTX) from [$(DataHubDatabaseName)].dbo.[PV_SA_M_CLIENT]) AND DWHCLIRES not in (select code_responsable from [$(DataHubDatabaseName)].dbo.REF_MARCHE)
and LTRIM(RTRIM(isnull(DWHCLIRES,'''')))<>'''' and DWHCLIAGE not in (51,52)', N'DataHub', N'DATAHUB_BFR', N'Fonctionnel', N'''la valeur ( ''+Cast(output_col2 as varchar(50))+'' ) de la colonne ( DWHCLIRES ) de la table PV_SA_M_CLIENT n''''existe pas dans la table ( REF_MARCHE )''', N'ID CLIENT', N'DWHCLIRU2', NULL, NULL, NULL, NULL, NULL, 1, NULL, 559)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (107, N'BFR_CONTROL_COMPTE_2', N'1 - Bloquant', N'vérification des valeurs du champ DWHCLIRUB dans la table de référence SAB REF_PLAN_DE_COMPTES', N'ce contrôle génère une alerte si la valeur du champ DWHCLIRUB de la table PV_SA_M_COMPTE n''existe pas dans la table REF_PLAN_DE_COMPTES', N'select DWHCPTCOM as output_col1, DWHCPTRUB as output_col2, NULL AS output_col3, NULL AS output_col4, NULL AS output_col5, NULL AS output_col6, NULL AS output_col7 
from [$(DataHubDatabaseName)].dbo.[PV_SA_M_COMPTE] where DWHCPTDTX= (select max(DWHCPTDTX) from [$(DataHubDatabaseName)].dbo.[PV_SA_M_COMPTE] ) 
AND DWHCPTRUB not in (select plan_de_comptes_compte_obligatoire from [$(DataHubDatabaseName)].dbo.REF_PLAN_DE_COMPTES ) and LTRIM(RTRIM(isnull(DWHCPTRUB,'''')))<>''''
and DWHCPTAGE not in (51,52)', N'DataHub', N'DATAHUB_BFR', N'Fonctionnel', N'''la valeur ( ''+Cast(output_col2 as varchar(50))+'' ) de la colonne ( DWHCPTRUB ) ( DWHCPTCOM : ''+Cast(output_col1 as varchar(10))+'' ) de la table PV_SA_M_COMPTE n''''existe pas dans la table ( REF_PLAN_DE_COMPTES )''', N'DWHCPTCOM', N'DWHCPTRUB', NULL, NULL, NULL, NULL, NULL, 1, NULL, 559)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (408, N'BFR_CONTROL_COMPTE_3', N'1 - Bloquant', N'vérification des valeurs du champ DWHCLIRUB dans la table de référence SIDP REF_PRODUIT', N'ce contrôle génère une alerte si la valeur du champ DWHCLIRUB de la table PV_SA_M_COMPTE n''existe pas dans la table REF_PRODUIT', N'select DWHCPTCOM as output_col1, DWHCPTRUB as output_col2, NULL AS output_col3, NULL AS output_col4, NULL AS output_col5, NULL AS output_col6, NULL AS output_col7 
from [$(DataHubDatabaseName)].dbo.[PV_SA_M_COMPTE] where DWHCPTDTX= (select max(DWHCPTDTX) from [$(DataHubDatabaseName)].dbo.[PV_SA_M_COMPTE] ) 
AND DWHCPTRUB not in (select code_rubrique_comptable_SAB from [$(DataHubDatabaseName)].dbo.REF_PRODUIT ) and LTRIM(RTRIM(isnull(DWHCPTRUB,'''')))<>''''
and DWHCPTAGE not in (51,52)', N'DataHub', N'DATAHUB_BFR', N'Fonctionnel', N'''la valeur ( ''+Cast(output_col2 as varchar(50))+'' ) de la colonne ( DWHCPTRUB ) ( DWHCPTCOM : ''+Cast(output_col1 as varchar(10))+'' ) de la table PV_SA_M_COMPTE n''''existe pas dans la table ( REF_PRODUIT )''', N'DWHCPTCOM', N'DWHCPTRUB', NULL, NULL, NULL, NULL, NULL, 1, NULL, 559)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (409, N'BFR_CONTROL_FFCC_1', N'1 - Bloquant', N'vérification des valeurs du champ REF_CPT dans la table des références externes PV_SA_Q_COM', N'ce contrôle génère une alerte si la valeur du champ REF_CPT de la table PV_FI_M_DES_Q n''existe pas dans la table PV_SA_Q_COM', N'select REF_CPT as output_col1, NULL as output_col2, NULL AS output_col3, NULL AS output_col4, NULL AS output_col5, NULL AS output_col6, NULL AS output_col7 
from [$(DataHubDatabaseName)].dbo.[PV_FI_M_DES_Q] 
where REF_CPT not in (select COMREFREF from [$(DataHubDatabaseName)].dbo.PV_SA_Q_COM )', N'DataHub', N'DATAHUB_BFR', N'Fonctionnel', N'''la valeur ( ''+Cast(output_col1 as varchar(10))+'' ) du champ ( REF_CPT ) de la table PV_FI_M_DES_Q n''''existe pas dans la table ( PV_SA_Q_COM )''', N'REF_CPT', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 559)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (410, N'BFR_CONTROL_FFCC_2', N'1 - Bloquant', N'vérification des valeurs du champ NUM_PRE dans la table des références externes PV_SA_Q_COM', N'ce contrôle génère une alerte si la valeur du champ NUM_PRE de la table PV_FI_M_DES n''existe pas dans la table PV_SA_Q_COM', N'select NUM_PRE as output_col1, NULL as output_col2, NULL AS output_col3, NULL AS output_col4, NULL AS output_col5, NULL AS output_col6, NULL AS output_col7 
from [$(DataHubDatabaseName)].dbo.[PV_FI_M_DES] 
where NUM_PRE not in (select COMREFREF from [$(DataHubDatabaseName)].dbo.PV_SA_Q_COM )', N'DataHub', N'DATAHUB_BFR', N'Fonctionnel', N'''la valeur ( ''+Cast(output_col1 as varchar(10))+'' ) du champ ( NUM_PRE ) de la table PV_FI_M_DES n''''existe pas dans la table ( PV_SA_Q_COM )''', N'NUM_PRE', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 559)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (411, N'BFR_CONTROL_FFCR_1', N'1 - Bloquant', N'vérification des valeurs du champ NUM_PRE dans la table des références externes PV_SA_Q_COM', N'ce contrôle génère une alerte si la valeur du champ NUM_PRE de la table PV_FI_M_COMPL_Q n''existe pas dans la table PV_SA_Q_COM', N'select NUM_PRE as output_col1, NULL as output_col2, NULL AS output_col3, NULL AS output_col4, NULL AS output_col5, NULL AS output_col6, NULL AS output_col7 
from [$(DataHubDatabaseName)].dbo.[PV_FI_M_COMPL_Q] 
where NUM_PRE not in (select COMREFREF from [$(DataHubDatabaseName)].dbo.PV_SA_Q_COM)', N'DataHub', N'DATAHUB_BFR', N'Fonctionnel', N'''la valeur ( ''+Cast(output_col1 as varchar(10))+'' ) du champ ( NUM_PRE ) de la table PV_FI_M_COMPL_Q n''''existe pas dans la table ( PV_SA_Q_COM )''', N'NUM_PRE', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 559)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (413, N'BFR_CONTROL_FFCTX_1', N'1 - Bloquant', N'vérification des valeurs du champ NUM_PRE dans la table des références externes PV_SA_Q_COM', N'ce contrôle génère une alerte si la valeur du champ NUM_PRE de la table PV_FI_M_CTX n''existe pas dans la table PV_SA_Q_COM', N'select NUM_PRE as output_col1, NULL as output_col2, NULL AS output_col3, NULL AS output_col4, NULL AS output_col5, NULL AS output_col6, NULL AS output_col7 
from [$(DataHubDatabaseName)].dbo.[PV_FI_M_CTX] 
where NUM_PRE not in (select COMREFREF from [$(DataHubDatabaseName)].dbo.PV_SA_Q_COM)', N'DataHub', N'DATAHUB_BFR', N'Fonctionnel', N'''la valeur ( ''+Cast(output_col1 as varchar(10))+'' ) du champ ( NUM_PRE ) de la table PV_FI_M_CTX n''''existe pas dans la table ( PV_SA_Q_COM )''', N'NUM_PRE', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 559)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (368, N'OAV_DATAHUB_CONTROL_1.0.12', N'1 - Bloquant', N'Comparaison OAV/DATAHUB table  CSPS', N'Ce contrôle compare le nombre de lignes entre OAV (Oracle) et DATAHUB pour la table CSPS', N'select src_nb as output_col1,cible_nb as output_col2,b.dat_obsr as output_col3,null as output_col4,null as output_col5,null as output_col6,null as output_col7 from openquery(OAV, ''select count(*) as src_nb from CSPS'') a join (select  dat_obsr,count(*) as cible_nb FROM [$(DataHubDatabaseName)].dbo.OAV_CSPS group by dat_obsr) b on a.src_nb != b.cible_nb', N'OAV-OCC', N'OAV - Contrôle DataHUB', N'Technique - Contrôle intégration', N' ''Différence de lignes entre OAV ''+cast(OUTPUT_COL1 as char)+'', et DataHUB :''+cast(OUTPUT_COL2 as char)', N'Nombre de lignes en source', N'Nombre de lignes en cible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (114, N'VOL_COLLECT_30_PV_MC_EMAILLOG', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_MC_EMAILLOG', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_MC_EMAILLOG dans le package PKGCO_SSIS_MC_XSIHMAIL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_MC_XSIHMAIL_00'' as output_col2,
	''PV_MC_EMAILLOG'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	NULL as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHMAIL_00''
	AND FileOrTablename= ''PV_MC_EMAILLOG'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHMAIL_00''
	AND FileOrTablename= ''PV_MC_EMAILLOG''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Marketing Cloud', N'VOL_COLLECT_Marketing Cloud', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement de la semaine précédente''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 113)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (115, N'VOL_COLLECT_30_PV_MC_STATUSCHANGE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_MC_STATUSCHANGE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_MC_STATUSCHANGE dans le package PKGCO_SSIS_MC_XSIHSTAT_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_MC_XSIHSTAT_00'' as output_col2,
	''PV_MC_STATUSCHANGE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	NULL as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHSTAT_00''
	AND FileOrTablename= ''PV_MC_STATUSCHANGE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHSTAT_00''
	AND FileOrTablename= ''PV_MC_STATUSCHANGE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Marketing Cloud', N'VOL_COLLECT_Marketing Cloud', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement de la semaine précédente''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 119)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (116, N'VOL_COLLECT_30_REF_CATEGORIE_PROFESSIONNELLES', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CATEGORIE_PROFESSIONNELLES', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CATEGORIE_PROFESSIONNELLES dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
DISTINCT TODAY.PROcessingType as output_col1,
''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
''REF_CATEGORIE_PROFESSIONNELLES'' as output_col3,
TODAY.NBR_LIGNE as output_col4,
LastMonth.NBR_LIGNE as output_col5,
Null as output_col6,
30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CATEGORIE_PROFESSIONNELLES'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CATEGORIE_PROFESSIONNELLES'' 
	 AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_CATEGORIE_PROFESSIONNELLES''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
) LastMonth 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100) )
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 125)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (117, N'VOL_COLLECT_30_REF_CODE_RESPONSABLES_COMMERCIAUX', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CODE_RESPONSABLES_COMMERCIAUX', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CODE_RESPONSABLES_COMMERCIAUX dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_CODE_RESPONSABLES_COMMERCIAUX'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	Null as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CODE_RESPONSABLES_COMMERCIAUX'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CODE_RESPONSABLES_COMMERCIAUX'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_CODE_RESPONSABLES_COMMERCIAUX''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
) LastMonth 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100) )
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 131)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (118, N'VOL_COLLECT_30_REF_COTATIONS_INTERNES', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_COTATIONS_INTERNES', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_COTATIONS_INTERNES dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_COTATIONS_INTERNES'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	Null as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_COTATIONS_INTERNES'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_COTATIONS_INTERNES'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_COTATIONS_INTERNES''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
) LastMonth 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100) )
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 137)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (119, N'VOL_COLLECT_30_REF_MOTIFS_DE_CLOTURE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_MOTIFS_DE_CLOTURE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_MOTIFS_DE_CLOTURE dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_MOTIFS_DE_CLOTURE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	lastMonth.NBR_LIGNE as output_col5,
	Null as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_MOTIFS_DE_CLOTURE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_MOTIFS_DE_CLOTURE'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_MOTIFS_DE_CLOTURE''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
) lastMonth 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(lastMonth.NBR_LIGNE,0)-(30*ISNULL(lastMonth.NBR_LIGNE,0)/100) )
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 143)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (120, N'VOL_COLLECT_30_REF_PROFESSIONS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_PROFESSIONS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_PROFESSIONS dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_PROFESSIONS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_PROFESSIONS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_PROFESSIONS'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_PROFESSIONS''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
) LastMonth 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100) )
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 149)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (121, N'VOL_COLLECT_30_REF_UTILISATION', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_UTILISATION', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_UTILISATION dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_UTILISATION'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	Null as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_UTILISATION'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_UTILISATION'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_XSIMENC0_00''   AND FileOrTablename= ''PV_SA_M_ENCRS_ECHL''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
) LastMonth 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100) )
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 154)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (122, N'VOL_COLLECT_30_PV_SA_M_ENCRS_ECHL', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SA_M_ENCRS_ECHL', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SA_M_ENCRS_ECHL dans le package PKGCO_SSIS_SA_XSIMENC0_00', N'	
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_XSIMENC0_00'' as output_col2,
	''PV_SA_M_ENCRS_ECHL'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	Null as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIMENC0_00''
	AND FileOrTablename= ''PV_SA_M_ENCRS_ECHL'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIMENC0_00''
	AND FileOrTablename= ''PV_SA_M_ENCRS_ECHL'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_XSIMENC0_00''   AND FileOrTablename= ''PV_SA_M_ENCRS_ECHL''  
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
) LastMonth 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100) )

', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 459)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (123, N'VOL_COLLECT_30_PV_SA_M_OPCREDIT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SA_M_OPCREDIT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SA_M_OPCREDIT dans le package PKGCO_SSIS_SA_XSIMOPE0_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_XSIMOPE0_00'' as output_col2,
	''PV_SA_M_OPCREDIT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIMOPE0_00''
	AND FileOrTablename= ''PV_SA_M_OPCREDIT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIMOPE0_00''
	AND FileOrTablename= ''PV_SA_M_OPCREDIT'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_XSIMOPE0_00''   AND FileOrTablename= ''PV_SA_M_OPCREDIT''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
) LastMonth 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100) )
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 472)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (124, N'VOL_COLLECT_30_PV_SA_Q_LKCPTCLI', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SA_Q_LKCPTCLI', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SA_Q_LKCPTCLI dans le package PKGCO_SSIS_SA_XSIQGRP0_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_XSIQGRP0_00'' as output_col2,
	''PV_SA_Q_LKCPTCLI'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQGRP0_00''
	AND FileOrTablename= ''PV_SA_Q_LKCPTCLI'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQGRP0_00''
	AND FileOrTablename= ''PV_SA_Q_LKCPTCLI'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQGRP0_00''
	AND FileOrTablename= ''PV_SA_Q_LKCPTCLI''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 487)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (125, N'VOL_COLLECT_30_RAW_SF_REFERENTIEL', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table RAW_SF_REFERENTIEL', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table RAW_SF_REFERENTIEL dans le package PKGCO_SSIS_SF_PICKLISTVALUES_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_PICKLISTVALUES_00'' as output_col2,
	''RAW_SF_REFERENTIEL'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_PICKLISTVALUES_00''
	AND FileOrTablename= ''RAW_SF_REFERENTIEL'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_PICKLISTVALUES_00''
	AND FileOrTablename= ''RAW_SF_REFERENTIEL'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_PICKLISTVALUES_00''
	AND FileOrTablename= ''RAW_SF_REFERENTIEL''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 174)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (126, N'VOL_COLLECT_30_REF_CASE_CANAL_REPONSE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CASE_CANAL_REPONSE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CASE_CANAL_REPONSE dans le package PKGCO_SSIS_SF_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_00'' as output_col2,
	''REF_CASE_CANAL_REPONSE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_CANAL_REPONSE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_CANAL_REPONSE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_CANAL_REPONSE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 179)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (138, N'VOL_COLLECT_30_PV_MXT_PORTEUR', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_MXT_PORTEUR', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_MXT_PORTEUR dans le package PKGCO_SSIS_MXT_PORTEUR_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_MXT_PORTEUR_00'' as output_col2,
	''PV_MXT_PORTEUR'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MXT_PORTEUR_00''
	AND FileOrTablename= ''PV_MXT_PORTEUR'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MXT_PORTEUR_00''
	AND FileOrTablename= ''PV_MXT_PORTEUR'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MXT_PORTEUR_00''
	AND FileOrTablename= ''PV_MXT_PORTEUR''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Monext', N'VOL_COLLECT_Monext', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 417)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (139, N'VOL_COLLECT_30_PV_SF_LIVECHATTRANSCRIPT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SF_LIVECHATTRANSCRIPT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SF_LIVECHATTRANSCRIPT dans le package PKGCO_SSIS_SF_LIVECHATTRANSCRIPT_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_LIVECHATTRANSCRIPT_00'' as output_col2,
	''PV_SF_LIVECHATTRANSCRIPT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_LIVECHATTRANSCRIPT_00''
	AND FileOrTablename= ''PV_SF_LIVECHATTRANSCRIPT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_LIVECHATTRANSCRIPT_00''
	AND FileOrTablename= ''PV_SF_LIVECHATTRANSCRIPT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_LIVECHATTRANSCRIPT_00''
	AND FileOrTablename= ''PV_SF_LIVECHATTRANSCRIPT''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 426)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (140, N'VOL_COLLECT_30_IWD_ANCHOR_FLAGS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_ANCHOR_FLAGS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_ANCHOR_FLAGS dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_ANCHOR_FLAGS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (141, N'VOL_COLLECT_30_IWD_DIALING_MODE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_DIALING_MODE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_DIALING_MODE dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_DIALING_MODE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (142, N'VOL_COLLECT_30_IWD_MEDIA_TYPE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_MEDIA_TYPE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_MEDIA_TYPE dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_MEDIA_TYPE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 367)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (143, N'VOL_COLLECT_30_IWD_REQUESTED_SKILL_COMBINATION', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_REQUESTED_SKILL_COMBINATION', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_REQUESTED_SKILL_COMBINATION dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_REQUESTED_SKILL_COMBINATION'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (144, N'VOL_COLLECT_30_IWD_ROUTING_TARGET', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_ROUTING_TARGET', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_ROUTING_TARGET dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_ROUTING_TARGET'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (145, N'VOL_COLLECT_30_IWD_CAMPAIGN_GROUP_SESSION_FACT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_CAMPAIGN_GROUP_SESSION_FACT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_CAMPAIGN_GROUP_SESSION_FACT dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_CAMPAIGN_GROUP_SESSION_FACT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (146, N'VOL_COLLECT_30_IWD_MEDIATION_SEGMENT_FACT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_MEDIATION_SEGMENT_FACT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_MEDIATION_SEGMENT_FACT dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_MEDIATION_SEGMENT_FACT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (147, N'VOL_COLLECT_30_PV_FE_CONSUMERLOANFINANCIALPLAN', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_FE_CONSUMERLOANFINANCIALPLAN', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_FE_CONSUMERLOANFINANCIALPLAN dans le package PKGCO_SSIS_FE_CONSUMERLOANFINANCIALPLAN_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_CONSUMERLOANFINANCIALPLAN_01'' as output_col2,
	''PV_FE_CONSUMERLOANFINANCIALPLAN'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CONSUMERLOANFINANCIALPLAN_01''
	AND FileOrTablename= ''PV_FE_CONSUMERLOANFINANCIALPLAN'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CONSUMERLOANFINANCIALPLAN_01''
	AND FileOrTablename= ''PV_FE_CONSUMERLOANFINANCIALPLAN'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CONSUMERLOANFINANCIALPLAN_01''
	AND FileOrTablename= ''PV_FE_CONSUMERLOANFINANCIALPLAN''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 60)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (148, N'VOL_COLLECT_30_RAW_FE_CONSUMERLOANPRODUCT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table RAW_FE_CONSUMERLOANPRODUCT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table RAW_FE_CONSUMERLOANPRODUCT dans le package PKGCO_SSIS_FE_CONSUMERLOANPRODUCT_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_CONSUMERLOANPRODUCT_00'' as output_col2,
	''RAW_FE_CONSUMERLOANPRODUCT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CONSUMERLOANPRODUCT_00''
	AND FileOrTablename= ''RAW_FE_CONSUMERLOANPRODUCT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CONSUMERLOANPRODUCT_00''
	AND FileOrTablename= ''RAW_FE_CONSUMERLOANPRODUCT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CONSUMERLOANPRODUCT_00''
	AND FileOrTablename= ''RAW_FE_CONSUMERLOANPRODUCT''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 61)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (149, N'VOL_COLLECT_30_REF_DAY_OF_MONTH', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_DAY_OF_MONTH', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_DAY_OF_MONTH dans le package PKGCO_SSIS_FE_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_REFERENTIEL_00'' as output_col2,
	''REF_DAY_OF_MONTH'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_DAY_OF_MONTH'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_DAY_OF_MONTH'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_DAY_OF_MONTH''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 84)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (150, N'VOL_COLLECT_30_REF_FINANCING_TYPE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_FINANCING_TYPE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_FINANCING_TYPE dans le package PKGCO_SSIS_FE_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_REFERENTIEL_00'' as output_col2,
	''REF_FINANCING_TYPE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_FINANCING_TYPE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_FINANCING_TYPE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_FINANCING_TYPE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 85)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (151, N'VOL_COLLECT_30_PV_FE_SERVICEPRODUCT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_FE_SERVICEPRODUCT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_FE_SERVICEPRODUCT dans le package PKGCO_SSIS_FE_SERVICEPRODUCT_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_SERVICEPRODUCT_01'' as output_col2,
	''PV_FE_SERVICEPRODUCT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SERVICEPRODUCT_01''
	AND FileOrTablename= ''PV_FE_SERVICEPRODUCT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SERVICEPRODUCT_01''
	AND FileOrTablename= ''PV_FE_SERVICEPRODUCT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SERVICEPRODUCT_01''
	AND FileOrTablename= ''PV_FE_SERVICEPRODUCT''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 93)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (153, N'VOL_COLLECT_30_PV_FE_CONSUMERLOANPRODUCT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_FE_CONSUMERLOANPRODUCT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_FE_CONSUMERLOANPRODUCT dans le package PKGCO_SSIS_FE_CONSUMERLOANPRODUCT_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_CONSUMERLOANPRODUCT_01'' as output_col2,
	''PV_FE_CONSUMERLOANPRODUCT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CONSUMERLOANPRODUCT_01''
	AND FileOrTablename= ''PV_FE_CONSUMERLOANPRODUCT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CONSUMERLOANPRODUCT_01''
	AND FileOrTablename= ''PV_FE_CONSUMERLOANPRODUCT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CONSUMERLOANPRODUCT_01''
	AND FileOrTablename= ''PV_FE_CONSUMERLOANPRODUCT''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 62)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (152, N'VOL_COLLECT_30_RAW_FE_SUBSCRIBEDCONSUMERLOAN', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table RAW_FE_SUBSCRIBEDCONSUMERLOAN', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table RAW_FE_SUBSCRIBEDCONSUMERLOAN dans le package PKGCO_SSIS_FE_SUBSCRIBEDCONSUMERLOAN_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_SUBSCRIBEDCONSUMERLOAN_00'' as output_col2,
	''RAW_FE_SUBSCRIBEDCONSUMERLOAN'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SUBSCRIBEDCONSUMERLOAN_00''
	AND FileOrTablename= ''RAW_FE_SUBSCRIBEDCONSUMERLOAN'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SUBSCRIBEDCONSUMERLOAN_00''
	AND FileOrTablename= ''RAW_FE_SUBSCRIBEDCONSUMERLOAN'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SUBSCRIBEDCONSUMERLOAN_00''
	AND FileOrTablename= ''RAW_FE_SUBSCRIBEDCONSUMERLOAN''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 94)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (202, N'VOL_COLLECT_30_IWD_TIME_ZONE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_TIME_ZONE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_TIME_ZONE dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_TIME_ZONE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (203, N'VOL_COLLECT_30_IWD_USER_DATA_CUST_DIM_1', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_USER_DATA_CUST_DIM_1', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_USER_DATA_CUST_DIM_1 dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_USER_DATA_CUST_DIM_1'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (204, N'VOL_COLLECT_30_IWD_USER_DATA_CUST_DIM_2', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_USER_DATA_CUST_DIM_2', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_USER_DATA_CUST_DIM_2 dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_USER_DATA_CUST_DIM_2'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (205, N'VOL_COLLECT_30_REF_CODE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CODE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CODE dans le package PKGCO_SSIS_FE_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_REFERENTIEL_00'' as output_col2,
	''REF_CODE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CODE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CODE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CODE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 314)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (206, N'VOL_COLLECT_30_PV_ELQ_ENQUETESAT_POSTENR', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_ELQ_ENQUETESAT_POSTENR', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_ELQ_ENQUETESAT_POSTENR dans le package PKGCO_SSIS_ELQ_ENQUETESAT_POSTENR', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_ELQ_ENQUETESAT_POSTENR'' as output_col2,
	''PV_ELQ_ENQUETESAT_POSTENR'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_ELQ_ENQUETESAT_POSTENR''
	AND FileOrTablename= ''PV_ELQ_ENQUETESAT_POSTENR'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_ELQ_ENQUETESAT_POSTENR''
	AND FileOrTablename= ''PV_ELQ_ENQUETESAT_POSTENR'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_ELQ_ENQUETESAT_POSTENR''
	AND FileOrTablename= ''PV_ELQ_ENQUETESAT_POSTENR''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Eloquant', N'VOL_COLLECT_Eloquant', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 413)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (207, N'VOL_COLLECT_30_PV_FI_Q_BDE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_FI_Q_BDE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_FI_Q_BDE dans le package PKGCO_SSIS_FI_XSIQBDE_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FI_XSIQBDE_00'' as output_col2,
	''PV_FI_Q_BDE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FI_XSIQBDE_00''
	AND FileOrTablename= ''PV_FI_Q_BDE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FI_XSIQBDE_00''
	AND FileOrTablename= ''PV_FI_Q_BDE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FI_XSIQBDE_00''
	AND FileOrTablename= ''PV_FI_Q_BDE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Franfinance', N'VOL_COLLECT_Franfinance', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 276)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (208, N'VOL_COLLECT_30_PV_SA_Q_CHEQUIER', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SA_Q_CHEQUIER', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SA_Q_CHEQUIER dans le package PKGCO_SSIS_SA_XSIQCHQ0_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_XSIQCHQ0_00'' as output_col2,
	''PV_SA_Q_CHEQUIER'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQCHQ0_00''
	AND FileOrTablename= ''PV_SA_Q_CHEQUIER'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQCHQ0_00''
	AND FileOrTablename= ''PV_SA_Q_CHEQUIER'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQCHQ0_00''
	AND FileOrTablename= ''PV_SA_Q_CHEQUIER''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 483)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (209, N'VOL_COLLECT_30_PV_WRC_CREDITS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_WRC_CREDITS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_WRC_CREDITS dans le package PKGCO_SSIS_WRC_CREDITS_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_WRC_CREDITS_00'' as output_col2,
	''PV_WRC_CREDITS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_WRC_CREDITS_00''
	AND FileOrTablename= ''PV_WRC_CREDITS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_WRC_CREDITS_00''
	AND FileOrTablename= ''PV_WRC_CREDITS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_WRC_CREDITS_00''
	AND FileOrTablename= ''PV_WRC_CREDITS''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Wirecard', N'VOL_COLLECT_Wirecard', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 414)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (210, N'VOL_COLLECT_30_PV_WRC_DEBITS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_WRC_DEBITS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_WRC_DEBITS dans le package PKGCO_SSIS_WRC_DEBITS_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_WRC_DEBITS_00'' as output_col2,
	''PV_WRC_DEBITS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_WRC_DEBITS_00''
	AND FileOrTablename= ''PV_WRC_DEBITS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_WRC_DEBITS_00''
	AND FileOrTablename= ''PV_WRC_DEBITS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_WRC_DEBITS_00''
	AND FileOrTablename= ''PV_WRC_DEBITS''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Wirecard', N'VOL_COLLECT_Wirecard', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 415)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (211, N'VOL_COLLECT_30_PV_MXT_COMPENSATION', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_MXT_COMPENSATION', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_MXT_COMPENSATION dans le package PKGCO_SSIS_MXT_COMPENSATION_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_MXT_COMPENSATION_00'' as output_col2,
	''PV_MXT_COMPENSATION'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MXT_COMPENSATION_00''
	AND FileOrTablename= ''PV_MXT_COMPENSATION'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MXT_COMPENSATION_00''
	AND FileOrTablename= ''PV_MXT_COMPENSATION'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MXT_COMPENSATION_00''
	AND FileOrTablename= ''PV_MXT_COMPENSATION''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Monext', N'VOL_COLLECT_Monext', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 416)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (212, N'VOL_COLLECT_30_PV_SF_ACCOUNTLINK', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SF_ACCOUNTLINK', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SF_ACCOUNTLINK dans le package PKGCO_SSIS_SF_ACCOUNTLINK_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_ACCOUNTLINK_00'' as output_col2,
	''PV_SF_ACCOUNTLINK'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_ACCOUNTLINK_00''
	AND FileOrTablename= ''PV_SF_ACCOUNTLINK'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_ACCOUNTLINK_00''
	AND FileOrTablename= ''PV_SF_ACCOUNTLINK'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_ACCOUNTLINK_00''
	AND FileOrTablename= ''PV_SF_ACCOUNTLINK''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 422)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (213, N'VOL_COLLECT_30_PV_SF_CASE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SF_CASE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SF_CASE dans le package PKGCO_SSIS_SF_CASE_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_CASE_00'' as output_col2,
	''PV_SF_CASE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_CASE_00''
	AND FileOrTablename= ''PV_SF_CASE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_CASE_00''
	AND FileOrTablename= ''PV_SF_CASE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_CASE_00''
	AND FileOrTablename= ''PV_SF_CASE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 423)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (214, N'VOL_COLLECT_30_PV_SF_CONTACT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SF_CONTACT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SF_CONTACT dans le package PKGCO_SSIS_SF_CONTACT_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_CONTACT_00'' as output_col2,
	''PV_SF_CONTACT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_CONTACT_00''
	AND FileOrTablename= ''PV_SF_CONTACT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_CONTACT_00''
	AND FileOrTablename= ''PV_SF_CONTACT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_CONTACT_00''
	AND FileOrTablename= ''PV_SF_CONTACT''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 424)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (215, N'VOL_COLLECT_30_IWD_WORKBIN', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_WORKBIN', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_WORKBIN dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_WORKBIN'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (216, N'VOL_COLLECT_30_IWD_INTERACTION_FACT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_INTERACTION_FACT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_INTERACTION_FACT dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_INTERACTION_FACT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 391)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (217, N'VOL_COLLECT_30_PV_SF_EMAILMESSAGE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SF_EMAILMESSAGE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SF_EMAILMESSAGE dans le package PKGCO_SSIS_SF_EMAILMESSAGE_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_EMAILMESSAGE_00'' as output_col2,
	''PV_SF_EMAILMESSAGE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_EMAILMESSAGE_00''
	AND FileOrTablename= ''PV_SF_EMAILMESSAGE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_EMAILMESSAGE_00''
	AND FileOrTablename= ''PV_SF_EMAILMESSAGE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_EMAILMESSAGE_00''
	AND FileOrTablename= ''PV_SF_EMAILMESSAGE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 425)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (234, N'VOL_COLLECT_30_DIM_CASE_CANAL_REPONSE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table DIM_CASE_CANAL_REPONSE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table DIM_CASE_CANAL_REPONSE dans le package PKGCO_SSIS_SF_REFERENTIEL_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_01'' as output_col2,
	''DIM_CASE_CANAL_REPONSE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_CANAL_REPONSE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_CANAL_REPONSE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_CANAL_REPONSE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 206)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (235, N'VOL_COLLECT_30_IWD_IXN_RESOURCE_STATE_FACT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_IXN_RESOURCE_STATE_FACT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_IXN_RESOURCE_STATE_FACT dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_IXN_RESOURCE_STATE_FACT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (236, N'VOL_COLLECT_30_IWD_SM_RES_STATE_REASON_FACT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_SM_RES_STATE_REASON_FACT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_SM_RES_STATE_REASON_FACT dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_SM_RES_STATE_REASON_FACT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 398)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (199, N'VOL_COLLECT_30_IWD_RESOURCE_SKILL_FACT_', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_RESOURCE_SKILL_FACT_', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_RESOURCE_SKILL_FACT_ dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_RESOURCE_SKILL_FACT_'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (200, N'VOL_COLLECT_30_IWD_RESOURCE_STATE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_RESOURCE_STATE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_RESOURCE_STATE dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_RESOURCE_STATE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 379)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (201, N'VOL_COLLECT_30_IWD_RESOURCE_STATE_REASON', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_RESOURCE_STATE_REASON', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_RESOURCE_STATE_REASON dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_RESOURCE_STATE_REASON'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 380)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (218, N'VOL_COLLECT_30_PV_SF_OPPORTUNITYCONTACTROLE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SF_OPPORTUNITYCONTACTROLE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SF_OPPORTUNITYCONTACTROLE dans le package PKGCO_SSIS_SF_OPPORTUNITYCONTACTROLE_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_OPPORTUNITYCONTACTROLE_00'' as output_col2,
	''PV_SF_OPPORTUNITYCONTACTROLE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_OPPORTUNITYCONTACTROLE_00''
	AND FileOrTablename= ''PV_SF_OPPORTUNITYCONTACTROLE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_OPPORTUNITYCONTACTROLE_00''
	AND FileOrTablename= ''PV_SF_OPPORTUNITYCONTACTROLE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_OPPORTUNITYCONTACTROLE_00''
	AND FileOrTablename= ''PV_SF_OPPORTUNITYCONTACTROLE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 429)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (219, N'VOL_COLLECT_30_PV_SF_PROFILE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SF_PROFILE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SF_PROFILE dans le package PKGCO_SSIS_SF_PROFILE_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_PROFILE_00'' as output_col2,
	''PV_SF_PROFILE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_PROFILE_00''
	AND FileOrTablename= ''PV_SF_PROFILE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_PROFILE_00''
	AND FileOrTablename= ''PV_SF_PROFILE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_PROFILE_00''
	AND FileOrTablename= ''PV_SF_PROFILE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 430)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (220, N'VOL_COLLECT_30_PV_SF_TASK', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SF_TASK', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SF_TASK dans le package PKGCO_SSIS_SF_TASK_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_TASK_00'' as output_col2,
	''PV_SF_TASK'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_TASK_00''
	AND FileOrTablename= ''PV_SF_TASK'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_TASK_00''
	AND FileOrTablename= ''PV_SF_TASK'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_TASK_00''
	AND FileOrTablename= ''PV_SF_TASK''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 431)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (221, N'VOL_COLLECT_30_IWD_INTERACTION_RESOURCE_FACT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_INTERACTION_RESOURCE_FACT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_INTERACTION_RESOURCE_FACT dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_INTERACTION_RESOURCE_FACT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 392)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (222, N'VOL_COLLECT_30_IWD_IRF_USER_DATA_KEYS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_IRF_USER_DATA_KEYS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_IRF_USER_DATA_KEYS dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_IRF_USER_DATA_KEYS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (223, N'VOL_COLLECT_30_PV_SF_USER', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SF_USER', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SF_USER dans le package PKGCO_SSIS_SF_USER_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_USER_00'' as output_col2,
	''PV_SF_USER'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_USER_00''
	AND FileOrTablename= ''PV_SF_USER'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_USER_00''
	AND FileOrTablename= ''PV_SF_USER'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_USER_00''
	AND FileOrTablename= ''PV_SF_USER''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 432)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (224, N'VOL_COLLECT_30_IWD_CALLING_LIST_METRIC_FACT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_CALLING_LIST_METRIC_FACT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_CALLING_LIST_METRIC_FACT dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_CALLING_LIST_METRIC_FACT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (225, N'VOL_COLLECT_30_IWD_CAMPAIGN_GROUP_STATE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_CAMPAIGN_GROUP_STATE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_CAMPAIGN_GROUP_STATE dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_CAMPAIGN_GROUP_STATE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (226, N'VOL_COLLECT_30_IWD_CONTACT_INFO_TYPE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table IWD_CONTACT_INFO_TYPE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table IWD_CONTACT_INFO_TYPE dans le package PKGIWD_SSIS_Alimentation', N'
SELECT 
	''COLLECT'' as output_col1,
	''PKGIWD_SSIS_Alimentation'' as output_col2,
	''IWD_CONTACT_INFO_TYPE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT commentaire AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.iwd_pilotage_historique
	WHERE table_cible = ''PKGIWD_SSIS_Alimentation''
	AND CAST(DTE_DEB_EXEC AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'IWD', N'VOL_COLLECT_IWD', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 411)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (227, N'VOL_COLLECT_30_REF_CIVILITE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CIVILITE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CIVILITE dans le package PKGCO_SSIS_SF_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_00'' as output_col2,
	''REF_CIVILITE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CIVILITE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CIVILITE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CIVILITE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 190)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (228, N'VOL_COLLECT_30_REF_CSP_NIV1', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CSP_NIV1', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CSP_NIV1 dans le package PKGCO_SSIS_SF_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_00'' as output_col2,
	''REF_CSP_NIV1'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CSP_NIV1'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CSP_NIV1'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CSP_NIV1''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 191)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (229, N'VOL_COLLECT_30_REF_RES_DIS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_RES_DIS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_RES_DIS dans le package PKGCO_SSIS_SF_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_00'' as output_col2,
	''REF_RES_DIS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_RES_DIS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_RES_DIS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_RES_DIS''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 195)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (230, N'VOL_COLLECT_30_REF_SS_RES', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_SS_RES', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_SS_RES dans le package PKGCO_SSIS_SF_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_00'' as output_col2,
	''REF_SS_RES'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_SS_RES'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_SS_RES'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_SS_RES''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 196)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (263, N'VOL_COLLECT_30_PV_SA_Q_CARTE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SA_Q_CARTE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SA_Q_CARTE dans le package PKGCO_SSIS_SA_XSIQCAR0_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_XSIQCAR0_00'' as output_col2,
	''PV_SA_Q_CARTE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQCAR0_00''
	AND FileOrTablename= ''PV_SA_Q_CARTE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQCAR0_00''
	AND FileOrTablename= ''PV_SA_Q_CARTE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQCAR0_00''
	AND FileOrTablename= ''PV_SA_Q_CARTE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 482)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (264, N'VOL_COLLECT_30_PV_SA_Q_CPTBIS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SA_Q_CPTBIS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SA_Q_CPTBIS dans le package PKGCO_SSIS_SA_XSIQCLB0_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_XSIQCLB0_00'' as output_col2,
	''PV_SA_Q_CPTBIS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQCLB0_00''
	AND FileOrTablename= ''PV_SA_Q_CPTBIS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQCLB0_00''
	AND FileOrTablename= ''PV_SA_Q_CPTBIS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQCLB0_00''
	AND FileOrTablename= ''PV_SA_Q_CPTBIS''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 484)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (265, N'VOL_COLLECT_30_RAW_FE_CUSTOMER', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table RAW_FE_CUSTOMER', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table RAW_FE_CUSTOMER dans le package PKGCO_SSIS_FE_CUSTOMER_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_CUSTOMER_00'' as output_col2,
	''RAW_FE_CUSTOMER'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CUSTOMER_00''
	AND FileOrTablename= ''RAW_FE_CUSTOMER'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CUSTOMER_00''
	AND FileOrTablename= ''RAW_FE_CUSTOMER'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CUSTOMER_00''
	AND FileOrTablename= ''RAW_FE_CUSTOMER''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 274)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (314, N'VOL_COLLECT_30_PV_SA_M_INT_MARGE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SA_M_INT_MARGE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SA_M_INT_MARGE dans le package PKGCO_SSIS_SA_XSIMMM30_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_XSIMMM30_00'' as output_col2,
	''PV_SA_M_INT_MARGE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIMMM30_00''
	AND FileOrTablename= ''PV_SA_M_INT_MARGE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIMMM30_00''
	AND FileOrTablename= ''PV_SA_M_INT_MARGE'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_XSIMMM30_00''   AND FileOrTablename= ''PV_SA_M_INT_MARGE''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 469)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (315, N'VOL_COLLECT_30_PV_SA_Q_CLIENT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SA_Q_CLIENT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SA_Q_CLIENT dans le package PKGCO_SSIS_SA_XSIQCLI0_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_XSIQCLI0_00'' as output_col2,
	''PV_SA_Q_CLIENT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQCLI0_00''
	AND FileOrTablename= ''PV_SA_Q_CLIENT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQCLI0_00''
	AND FileOrTablename= ''PV_SA_Q_CLIENT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQCLI0_00''
	AND FileOrTablename= ''PV_SA_Q_CLIENT''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 485)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (316, N'VOL_COLLECT_30_PV_SA_Q_COMPTE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SA_Q_COMPTE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SA_Q_COMPTE dans le package PKGCO_SSIS_SA_XSIQCPT0_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_XSIQCPT0_00'' as output_col2,
	''PV_SA_Q_COMPTE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQCPT0_00''
	AND FileOrTablename= ''PV_SA_Q_COMPTE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQCPT0_00''
	AND FileOrTablename= ''PV_SA_Q_COMPTE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQCPT0_00''
	AND FileOrTablename= ''PV_SA_Q_COMPTE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 486)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (317, N'VOL_COLLECT_30_PV_SA_Q_COM', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SA_Q_COM', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SA_Q_COM dans le package PKGCO_SSIS_SA_XSIQRCP0_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_XSIQRCP0_00'' as output_col2,
	''PV_SA_Q_COM'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQRCP0_00''
	AND FileOrTablename= ''PV_SA_Q_COM'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQRCP0_00''
	AND FileOrTablename= ''PV_SA_Q_COM'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIQRCP0_00''
	AND FileOrTablename= ''PV_SA_Q_COM''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 490)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (318, N'VOL_COLLECT_30_PV_SF_OPPORTUNITY_HISTORY', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SF_OPPORTUNITY_HISTORY', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SF_OPPORTUNITY_HISTORY dans le package PKGCO_SSIS_SF_OPPORTUNITY_HISTORY_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_OPPORTUNITY_HISTORY_01'' as output_col2,
	''PV_SF_OPPORTUNITY_HISTORY'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_OPPORTUNITY_HISTORY_01''
	AND FileOrTablename= ''PV_SF_OPPORTUNITY_HISTORY'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_OPPORTUNITY_HISTORY_01''
	AND FileOrTablename= ''PV_SF_OPPORTUNITY_HISTORY'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_OPPORTUNITY_HISTORY_01''
	AND FileOrTablename= ''PV_SF_OPPORTUNITY_HISTORY''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 173)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (319, N'VOL_COLLECT_30_REF_CASE_ATTENTE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CASE_ATTENTE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CASE_ATTENTE dans le package PKGCO_SSIS_SF_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_00'' as output_col2,
	''REF_CASE_ATTENTE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_ATTENTE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_ATTENTE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_ATTENTE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 177)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (320, N'VOL_COLLECT_30_REF_CASE_CANAL_ORIGINE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CASE_CANAL_ORIGINE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CASE_CANAL_ORIGINE dans le package PKGCO_SSIS_SF_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_00'' as output_col2,
	''REF_CASE_CANAL_ORIGINE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_CANAL_ORIGINE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_CANAL_ORIGINE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_CANAL_ORIGINE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 178)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (321, N'VOL_COLLECT_30_REF_CASE_NIVEAU', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CASE_NIVEAU', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CASE_NIVEAU dans le package PKGCO_SSIS_SF_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_00'' as output_col2,
	''REF_CASE_NIVEAU'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_NIVEAU'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_NIVEAU'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_NIVEAU''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 182)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (322, N'VOL_COLLECT_30_REF_CASE_ORIGINE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CASE_ORIGINE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CASE_ORIGINE dans le package PKGCO_SSIS_SF_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_00'' as output_col2,
	''REF_CASE_ORIGINE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_ORIGINE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_ORIGINE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_ORIGINE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 183)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (323, N'VOL_COLLECT_30_REF_CASE_QUALIF_REP1', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CASE_QUALIF_REP1', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CASE_QUALIF_REP1 dans le package PKGCO_SSIS_SF_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_00'' as output_col2,
	''REF_CASE_QUALIF_REP1'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_QUALIF_REP1'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_QUALIF_REP1'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_QUALIF_REP1''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 187)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (324, N'VOL_COLLECT_30_REF_CASE_QUALIF_REP2', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CASE_QUALIF_REP2', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CASE_QUALIF_REP2 dans le package PKGCO_SSIS_SF_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_00'' as output_col2,
	''REF_CASE_QUALIF_REP2'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_QUALIF_REP2'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_QUALIF_REP2'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_QUALIF_REP2''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 188)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (325, N'VOL_COLLECT_30_REF_CSP_NIV3', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CSP_NIV3', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CSP_NIV3 dans le package PKGCO_SSIS_SF_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_00'' as output_col2,
	''REF_CSP_NIV3'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CSP_NIV3'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CSP_NIV3'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CSP_NIV3''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 192)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (326, N'VOL_COLLECT_30_REF_MOTIF_REF', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_MOTIF_REF', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_MOTIF_REF dans le package PKGCO_SSIS_SF_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_00'' as output_col2,
	''REF_MOTIF_REF'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_MOTIF_REF'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_MOTIF_REF'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_MOTIF_REF''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 193)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (327, N'VOL_COLLECT_30_RAW_FE_CONSUMERLOANEQUIPMENT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table RAW_FE_CONSUMERLOANEQUIPMENT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table RAW_FE_CONSUMERLOANEQUIPMENT dans le package PKGCO_SSIS_FE_CONSUMERLOANEQUIPMENT_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_CONSUMERLOANEQUIPMENT_00'' as output_col2,
	''RAW_FE_CONSUMERLOANEQUIPMENT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CONSUMERLOANEQUIPMENT_00''
	AND FileOrTablename= ''RAW_FE_CONSUMERLOANEQUIPMENT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CONSUMERLOANEQUIPMENT_00''
	AND FileOrTablename= ''RAW_FE_CONSUMERLOANEQUIPMENT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CONSUMERLOANEQUIPMENT_00''
	AND FileOrTablename= ''RAW_FE_CONSUMERLOANEQUIPMENT''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 57)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (328, N'VOL_COLLECT_30_PV_FE_CONSUMERLOANEQUIPMENT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_FE_CONSUMERLOANEQUIPMENT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_FE_CONSUMERLOANEQUIPMENT dans le package PKGCO_SSIS_FE_CONSUMERLOANEQUIPMENT_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_CONSUMERLOANEQUIPMENT_01'' as output_col2,
	''PV_FE_CONSUMERLOANEQUIPMENT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CONSUMERLOANEQUIPMENT_01''
	AND FileOrTablename= ''PV_FE_CONSUMERLOANEQUIPMENT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CONSUMERLOANEQUIPMENT_01''
	AND FileOrTablename= ''PV_FE_CONSUMERLOANEQUIPMENT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_CONSUMERLOANEQUIPMENT_01''
	AND FileOrTablename= ''PV_FE_CONSUMERLOANEQUIPMENT''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 58)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (329, N'VOL_COLLECT_30_REF_STADE_VTE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_STADE_VTE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_STADE_VTE dans le package PKGCO_SSIS_SF_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_00'' as output_col2,
	''REF_STADE_VTE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_STADE_VTE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_STADE_VTE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_STADE_VTE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 197)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (346, N'VOL_COLLECT_30_REF_RECORDTYPE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_RECORDTYPE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_RECORDTYPE dans le package PKGCO_SSIS_SF_RECORDTYPE_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_RECORDTYPE_01'' as output_col2,
	''REF_RECORDTYPE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_RECORDTYPE_01''
	AND FileOrTablename= ''REF_RECORDTYPE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_RECORDTYPE_01''
	AND FileOrTablename= ''REF_RECORDTYPE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_RECORDTYPE_01''
	AND FileOrTablename= ''REF_RECORDTYPE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 2)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (347, N'VOL_COLLECT_30_PV_MC_SENT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_MC_SENT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_MC_SENT dans le package PKGCO_SSIS_MC_XSIHSENT_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_MC_XSIHSENT_00'' as output_col2,
	''PV_MC_SENT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	NULL as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHSENT_00''
	AND FileOrTablename= ''PV_MC_SENT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHSENT_00''
	AND FileOrTablename= ''PV_MC_SENT''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Marketing Cloud', N'VOL_COLLECT_Marketing Cloud', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement de la semaine précédente''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 117)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (348, N'VOL_COLLECT_30_PV_MC_SMSLOG', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_MC_SMSLOG', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_MC_SMSLOG dans le package PKGCO_SSIS_MC_XSIHSMSL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_MC_XSIHSMSL_00'' as output_col2,
	''PV_MC_SMSLOG'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	NULL as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHSMSL_00''
	AND FileOrTablename= ''PV_MC_SMSLOG'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHSMSL_00''
	AND FileOrTablename= ''PV_MC_SMSLOG''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Marketing Cloud', N'VOL_COLLECT_Marketing Cloud', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement de la semaine précédente''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 118)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (349, N'VOL_COLLECT_30_DIM_CASE_PRIORITE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table DIM_CASE_PRIORITE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table DIM_CASE_PRIORITE dans le package PKGCO_SSIS_SF_REFERENTIEL_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_01'' as output_col2,
	''DIM_CASE_PRIORITE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_PRIORITE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_PRIORITE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_PRIORITE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 212)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (350, N'VOL_COLLECT_30_DIM_CASE_PROCESSUS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table DIM_CASE_PROCESSUS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table DIM_CASE_PROCESSUS dans le package PKGCO_SSIS_SF_REFERENTIEL_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_01'' as output_col2,
	''DIM_CASE_PROCESSUS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_PROCESSUS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_PROCESSUS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CASE_PROCESSUS''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 213)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (351, N'VOL_COLLECT_30_REF_TYPE_DE_PORTEUR_CB', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_TYPE_DE_PORTEUR_CB', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_TYPE_DE_PORTEUR_CB dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_TYPE_DE_PORTEUR_CB'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_TYPE_DE_PORTEUR_CB'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_TYPE_DE_PORTEUR_CB'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_TYPE_DE_PORTEUR_CB''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 0, NULL, 123)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (352, N'VOL_COLLECT_30_REF_APE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_APE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_APE dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_APE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_APE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_APE'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_APE''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 124)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (353, N'VOL_COLLECT_30_REF_CODE_LIENS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CODE_LIENS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CODE_LIENS dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_CODE_LIENS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CODE_LIENS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CODE_LIENS'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_CODE_LIENS''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 129)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (354, N'VOL_COLLECT_30_REF_CODE_PRESTATIONS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CODE_PRESTATIONS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CODE_PRESTATIONS dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_CODE_PRESTATIONS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CODE_PRESTATIONS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CODE_PRESTATIONS'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_CODE_PRESTATIONS''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 130)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (355, N'VOL_COLLECT_30_DIM_CIVILITE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table DIM_CIVILITE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table DIM_CIVILITE dans le package PKGCO_SSIS_SF_REFERENTIEL_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_01'' as output_col2,
	''DIM_CIVILITE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CIVILITE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CIVILITE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CIVILITE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 217)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (356, N'VOL_COLLECT_30_DIM_CSP_NIV1', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table DIM_CSP_NIV1', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table DIM_CSP_NIV1 dans le package PKGCO_SSIS_SF_REFERENTIEL_01', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_01'' as output_col2,
	''DIM_CSP_NIV1'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CSP_NIV1'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CSP_NIV1'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_01''
	AND FileOrTablename= ''DIM_CSP_NIV1''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 218)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (357, N'VOL_COLLECT_30_REF_CODES_RESIDENTS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CODES_RESIDENTS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CODES_RESIDENTS dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_CODES_RESIDENTS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CODES_RESIDENTS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CODES_RESIDENTS'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_CODES_RESIDENTS''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 135)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (358, N'VOL_COLLECT_30_REF_COMMISSIONS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_COMMISSIONS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_COMMISSIONS dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_COMMISSIONS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_COMMISSIONS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_COMMISSIONS'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_COMMISSIONS''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 136)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (167, N'VOL_COLLECT_30_REF_CHECKING_ACCOUNT_TYPE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CHECKING_ACCOUNT_TYPE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CHECKING_ACCOUNT_TYPE dans le package PKGCO_SSIS_FE_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_REFERENTIEL_00'' as output_col2,
	''REF_CHECKING_ACCOUNT_TYPE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CHECKING_ACCOUNT_TYPE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CHECKING_ACCOUNT_TYPE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CHECKING_ACCOUNT_TYPE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 74)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (168, N'VOL_COLLECT_30_REF_CONS_LOAN_REJECT_REASON', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CONS_LOAN_REJECT_REASON', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CONS_LOAN_REJECT_REASON dans le package PKGCO_SSIS_FE_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_REFERENTIEL_00'' as output_col2,
	''REF_CONS_LOAN_REJECT_REASON'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CONS_LOAN_REJECT_REASON'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CONS_LOAN_REJECT_REASON'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CONS_LOAN_REJECT_REASON''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 75)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (169, N'VOL_COLLECT_30_REF_CONSU_LOAN_FIN_PLAN_STATUS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CONSU_LOAN_FIN_PLAN_STATUS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CONSU_LOAN_FIN_PLAN_STATUS dans le package PKGCO_SSIS_FE_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_REFERENTIEL_00'' as output_col2,
	''REF_CONSU_LOAN_FIN_PLAN_STATUS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CONSU_LOAN_FIN_PLAN_STATUS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CONSU_LOAN_FIN_PLAN_STATUS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CONSU_LOAN_FIN_PLAN_STATUS''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 76)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (282, N'VOL_COLLECT_30_RAW_FE_COMMERCIALPRODUCT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table RAW_FE_COMMERCIALPRODUCT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table RAW_FE_COMMERCIALPRODUCT dans le package PKGCO_SSIS_FE_COMMERCIALPRODUCT_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_COMMERCIALPRODUCT_00'' as output_col2,
	''RAW_FE_COMMERCIALPRODUCT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_COMMERCIALPRODUCT_00''
	AND FileOrTablename= ''RAW_FE_COMMERCIALPRODUCT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_COMMERCIALPRODUCT_00''
	AND FileOrTablename= ''RAW_FE_COMMERCIALPRODUCT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_COMMERCIALPRODUCT_00''
	AND FileOrTablename= ''RAW_FE_COMMERCIALPRODUCT''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 55)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (283, N'VOL_COLLECT_30_REF_CASE_METIER', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CASE_METIER', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CASE_METIER dans le package PKGCO_SSIS_SF_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_00'' as output_col2,
	''REF_CASE_METIER'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_METIER'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_METIER'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_METIER''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 180)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (284, N'VOL_COLLECT_30_REF_CASE_MOTIF', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CASE_MOTIF', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CASE_MOTIF dans le package PKGCO_SSIS_SF_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_00'' as output_col2,
	''REF_CASE_MOTIF'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_MOTIF'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_MOTIF'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_MOTIF''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 181)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (285, N'VOL_COLLECT_30_REF_CONSUMER_LOAN_MGT_DECISION', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CONSUMER_LOAN_MGT_DECISION', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CONSUMER_LOAN_MGT_DECISION dans le package PKGCO_SSIS_FE_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_REFERENTIEL_00'' as output_col2,
	''REF_CONSUMER_LOAN_MGT_DECISION'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CONSUMER_LOAN_MGT_DECISION'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CONSUMER_LOAN_MGT_DECISION'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CONSUMER_LOAN_MGT_DECISION''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 78)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (286, N'VOL_COLLECT_30_REF_CONSUMER_LOAN_MGT_RT_ST', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CONSUMER_LOAN_MGT_RT_ST', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CONSUMER_LOAN_MGT_RT_ST dans le package PKGCO_SSIS_FE_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_REFERENTIEL_00'' as output_col2,
	''REF_CONSUMER_LOAN_MGT_RT_ST'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CONSUMER_LOAN_MGT_RT_ST'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CONSUMER_LOAN_MGT_RT_ST'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CONSUMER_LOAN_MGT_RT_ST''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 79)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (287, N'VOL_COLLECT_30_PV_MC_OPENS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_MC_OPENS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_MC_OPENS dans le package PKGCO_SSIS_MC_XSIHOPEN_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_MC_XSIHOPEN_00'' as output_col2,
	''PV_MC_OPENS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	NULL as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHOPEN_00''
	AND FileOrTablename= ''PV_MC_OPENS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHOPEN_00''
	AND FileOrTablename= ''PV_MC_OPENS''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Marketing Cloud', N'VOL_COLLECT_Marketing Cloud', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement de la semaine précédente''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 114)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (288, N'VOL_COLLECT_30_PV_MC_PUSHLOG', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_MC_PUSHLOG', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_MC_PUSHLOG dans le package PKGCO_SSIS_MC_XSIHPUSH_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_MC_XSIHPUSH_00'' as output_col2,
	''PV_MC_PUSHLOG'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	NULL as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHPUSH_00''
	AND FileOrTablename= ''PV_MC_PUSHLOG'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHPUSH_00''
	AND FileOrTablename= ''PV_MC_PUSHLOG''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Marketing Cloud', N'VOL_COLLECT_Marketing Cloud', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement de la semaine précédente''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 115)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (289, N'VOL_COLLECT_30_PV_MC_UNSUBS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_MC_UNSUBS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_MC_UNSUBS dans le package PKGCO_SSIS_MC_XSIHUNSU_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_MC_XSIHUNSU_00'' as output_col2,
	''PV_MC_UNSUBS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	NULL as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHUNSU_00''
	AND FileOrTablename= ''PV_MC_UNSUBS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHUNSU_00''
	AND FileOrTablename= ''PV_MC_UNSUBS''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Marketing Cloud', N'VOL_COLLECT_Marketing Cloud', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement de la semaine précédente''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 120)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (292, N'VOL_COLLECT_30_REF_CASE_PROCESSUS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CASE_PROCESSUS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CASE_PROCESSUS dans le package PKGCO_SSIS_SF_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_REFERENTIEL_00'' as output_col2,
	''REF_CASE_PROCESSUS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_PROCESSUS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_PROCESSUS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CASE_PROCESSUS''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 186)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (293, N'VOL_COLLECT_30_REF_CODE_CATEGORIES_CLIENTS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CODE_CATEGORIES_CLIENTS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CODE_CATEGORIES_CLIENTS dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_CODE_CATEGORIES_CLIENTS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CODE_CATEGORIES_CLIENTS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CODE_CATEGORIES_CLIENTS'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_CODE_CATEGORIES_CLIENTS''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 126)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (294, N'VOL_COLLECT_30_REF_CODE_ENTREES_EN_LITIGIEUX', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CODE_ENTREES_EN_LITIGIEUX', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CODE_ENTREES_EN_LITIGIEUX dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_CODE_ENTREES_EN_LITIGIEUX'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CODE_ENTREES_EN_LITIGIEUX'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CODE_ENTREES_EN_LITIGIEUX'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_CODE_ENTREES_EN_LITIGIEUX''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 127)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (183, N'VOL_COLLECT_30_REF_SALUTATION', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_SALUTATION', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_SALUTATION dans le package PKGCO_SSIS_FE_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_REFERENTIEL_00'' as output_col2,
	''REF_SALUTATION'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_SALUTATION'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_SALUTATION'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_REFERENTIEL_00''
	AND FileOrTablename= ''REF_SALUTATION''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 316)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (184, N'VOL_COLLECT_30_PV_ELQ_ENQUETESAT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_ELQ_ENQUETESAT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_ELQ_ENQUETESAT dans le package PKGCO_SSIS_ELQ_ENQUETESAT', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_ELQ_ENQUETESAT'' as output_col2,
	''PV_ELQ_ENQUETESAT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_ELQ_ENQUETESAT''
	AND FileOrTablename= ''PV_ELQ_ENQUETESAT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_ELQ_ENQUETESAT''
	AND FileOrTablename= ''PV_ELQ_ENQUETESAT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_ELQ_ENQUETESAT''
	AND FileOrTablename= ''PV_ELQ_ENQUETESAT''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Eloquant', N'VOL_COLLECT_Eloquant', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 412)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (185, N'VOL_COLLECT_30_PV_SF_OPPORTUNITY', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SF_OPPORTUNITY', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SF_OPPORTUNITY dans le package PKGCO_SSIS_SF_OPPORTUNITY_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SF_OPPORTUNITY_00'' as output_col2,
	''PV_SF_OPPORTUNITY'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_OPPORTUNITY_00''
	AND FileOrTablename= ''PV_SF_OPPORTUNITY'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_OPPORTUNITY_00''
	AND FileOrTablename= ''PV_SF_OPPORTUNITY'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SF_OPPORTUNITY_00''
	AND FileOrTablename= ''PV_SF_OPPORTUNITY''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Salesforce', N'VOL_COLLECT_Salesforce', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 427)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (298, N'VOL_COLLECT_30_PV_FE_SVCPRODUCTASSOC', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_FE_SVCPRODUCTASSOC', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_FE_SVCPRODUCTASSOC dans le package PKGCO_SSIS_FE_SVCPRODUCTASSOC_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_FE_SVCPRODUCTASSOC_00'' as output_col2,
	''PV_FE_SVCPRODUCTASSOC'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	YESTERDAY.NBR_LIGNE as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SVCPRODUCTASSOC_00''
	AND FileOrTablename= ''PV_FE_SVCPRODUCTASSOC'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SVCPRODUCTASSOC_00''
	AND FileOrTablename= ''PV_FE_SVCPRODUCTASSOC'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-1,GETDATE()) AS DATE)
) YESTERDAY 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(YESTERDAY.NBR_LIGNE,0)-(30*ISNULL(YESTERDAY.NBR_LIGNE,0)/100) )
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_FE_SVCPRODUCTASSOC_00''
	AND FileOrTablename= ''PV_FE_SVCPRODUCTASSOC''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Front End', N'VOL_COLLECT_Front End', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport à la veille et à la semaine précédente (30%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 98)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (299, N'VOL_COLLECT_30_PV_MC_PARRAINAGE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_MC_PARRAINAGE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_MC_PARRAINAGE dans le package PKGCO_SSIS_MC_XSIHPAR_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_MC_XSIHPAR_00'' as output_col2,
	''PV_MC_PARRAINAGE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	NULL as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHPAR_00''
	AND FileOrTablename= ''PV_MC_PARRAINAGE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHPAR_00''
	AND FileOrTablename= ''PV_MC_PARRAINAGE''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Marketing Cloud', N'VOL_COLLECT_Marketing Cloud', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement de la semaine précédente''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 104)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (300, N'VOL_COLLECT_30_PV_MC_CGLOG', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_MC_CGLOG', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_MC_CGLOG dans le package PKGCO_SSIS_MC_XSIHCGLO_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_MC_XSIHCGLO_00'' as output_col2,
	''PV_MC_CGLOG'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	NULL as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHCGLO_00''
	AND FileOrTablename= ''PV_MC_CGLOG'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHCGLO_00''
	AND FileOrTablename= ''PV_MC_CGLOG''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Marketing Cloud', N'VOL_COLLECT_Marketing Cloud', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement de la semaine précédente''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 110)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (301, N'VOL_COLLECT_30_PV_MC_SENDJOBS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_MC_SENDJOBS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_MC_SENDJOBS dans le package PKGCO_SSIS_MC_XSIHSEJO_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_MC_XSIHSEJO_00'' as output_col2,
	''PV_MC_SENDJOBS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	NULL as output_col5,
	LASTWEEK.NBR_LIGNE as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHSEJO_00''
	AND FileOrTablename= ''PV_MC_SENDJOBS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_MC_XSIHSEJO_00''
	AND FileOrTablename= ''PV_MC_SENDJOBS''  
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(DATEADD(DAY,-7,GETDATE()) AS DATE)
) LASTWEEK 
ON ( ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LASTWEEK.NBR_LIGNE,0)-(30*ISNULL(LASTWEEK.NBR_LIGNE,0)/100) )
', N'Marketing Cloud', N'VOL_COLLECT_Marketing Cloud', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement de la semaine précédente''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 116)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (302, N'VOL_COLLECT_30_REF_AGENCE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_AGENCE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_AGENCE dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_AGENCE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	NULL as output_col5,
	NULL  as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_AGENCE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_AGENCE'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_AGENCE''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 122)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (303, N'VOL_COLLECT_30_REF_CODE_ETAT_CLIENT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CODE_ETAT_CLIENT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CODE_ETAT_CLIENT dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_CODE_ETAT_CLIENT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CODE_ETAT_CLIENT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CODE_ETAT_CLIENT'' 
	  AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_CODE_ETAT_CLIENT''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 128)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (304, N'VOL_COLLECT_30_REF_CODES_ANALYTIQUES', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_CODES_ANALYTIQUES', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_CODES_ANALYTIQUES dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_CODES_ANALYTIQUES'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CODES_ANALYTIQUES'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_CODES_ANALYTIQUES'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_CODES_ANALYTIQUES''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 134)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (305, N'VOL_COLLECT_30_REF_HISTORIQUE_MENSUEL_DEVISES', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_HISTORIQUE_MENSUEL_DEVISES', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_HISTORIQUE_MENSUEL_DEVISES dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_HISTORIQUE_MENSUEL_DEVISES'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_HISTORIQUE_MENSUEL_DEVISES'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_HISTORIQUE_MENSUEL_DEVISES'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_HISTORIQUE_MENSUEL_DEVISES''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 140)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (306, N'VOL_COLLECT_30_REF_OPERATIONS', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_OPERATIONS', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_OPERATIONS dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_OPERATIONS'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_OPERATIONS'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_OPERATIONS'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_OPERATIONS''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 146)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (307, N'VOL_COLLECT_30_REF_OPTION_DE_DEBIT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_OPTION_DE_DEBIT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_OPTION_DE_DEBIT dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_OPTION_DE_DEBIT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_OPTION_DE_DEBIT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_OPTION_DE_DEBIT'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_OPTION_DE_DEBIT''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 147)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (308, N'VOL_COLLECT_30_REF_PLAN_DE_COMPTES', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_PLAN_DE_COMPTES', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_PLAN_DE_COMPTES dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_PLAN_DE_COMPTES'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_PLAN_DE_COMPTES'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_PLAN_DE_COMPTES'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_PLAN_DE_COMPTES''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 148)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (309, N'VOL_COLLECT_30_REF_SOUS_SERVICES', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_SOUS_SERVICES', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_SOUS_SERVICES dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_SOUS_SERVICES'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_SOUS_SERVICES'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_SOUS_SERVICES'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_SOUS_SERVICES''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 152)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (310, N'VOL_COLLECT_30_REF_TYPES_DE_COMPTE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table REF_TYPES_DE_COMPTE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table REF_TYPES_DE_COMPTE dans le package PKGCO_SSIS_SA_REFERENTIEL_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_REFERENTIEL_00'' as output_col2,
	''REF_TYPES_DE_COMPTE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_TYPES_DE_COMPTE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''
	AND FileOrTablename= ''REF_TYPES_DE_COMPTE'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_REFERENTIEL_00''   AND FileOrTablename= ''REF_TYPES_DE_COMPTE''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 153)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (311, N'VOL_COLLECT_30_PV_SA_M_CLIENT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SA_M_CLIENT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SA_M_CLIENT dans le package PKGCO_SSIS_SA_XSIMCLI0_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_XSIMCLI0_00'' as output_col2,
	''PV_SA_M_CLIENT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIMCLI0_00''
	AND FileOrTablename= ''PV_SA_M_CLIENT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIMCLI0_00''
	AND FileOrTablename= ''PV_SA_M_CLIENT'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_XSIMCLI0_00''   AND FileOrTablename= ''PV_SA_M_CLIENT''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 452)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (312, N'VOL_COLLECT_30_PV_SA_M_COMPTE', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SA_M_COMPTE', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SA_M_COMPTE dans le package PKGCO_SSIS_SA_XSIMCPT0_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_XSIMCPT0_00'' as output_col2,
	''PV_SA_M_COMPTE'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIMCPT0_00''
	AND FileOrTablename= ''PV_SA_M_COMPTE'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIMCPT0_00''
	AND FileOrTablename= ''PV_SA_M_COMPTE'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_XSIMCPT0_00''   AND FileOrTablename= ''PV_SA_M_COMPTE''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 455)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (313, N'VOL_COLLECT_30_PV_SA_M_LKCPTINT', N'2 - Warning', N'Contrôle de la volumétrie des données pour la collecte de la table PV_SA_M_LKCPTINT', N'Ce contrôle compare le nombre de données chargées par rapport aux données chargées la veille et au même jour de la semaine précédente si c''est inférieur à 30% pour la table PV_SA_M_LKCPTINT dans le package PKGCO_SSIS_SA_XSIMMM10_00', N'
SELECT 
	DISTINCT TODAY.PROcessingType as output_col1,
	''PKGCO_SSIS_SA_XSIMMM10_00'' as output_col2,
	''PV_SA_M_LKCPTINT'' as output_col3,
	TODAY.NBR_LIGNE as output_col4,
	LastMonth.NBR_LIGNE as output_col5,
	NULL as output_col6,
	30 AS output_col7
FROM
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIMMM10_00''
	AND FileOrTablename= ''PV_SA_M_LKCPTINT'' 
	AND CAST(PROCESSINGSTARTDATE AS DATE) = CAST(GETDATE() AS DATE)
) TODAY
INNER JOIN 
(
	SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE
	FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)
	WHERE ETLName = ''PKGCO_SSIS_SA_XSIMMM10_00''
	AND FileOrTablename= ''PV_SA_M_LKCPTINT'' 
	AND id in ( 
					select MAX(id) 
					from [$(DataHubDatabaseName)].[dbo].Data_Log 
					WHERE ETLName = ''PKGCO_SSIS_SA_XSIMMM10_00''   AND FileOrTablename= ''PV_SA_M_LKCPTINT''   
					and  convert(varchar(7), processingStartDate, 126) =CONVERT(varchar(7),DATEADD(m,-1,getdate()),126)
				)
	AND ProcessingStatus=''OK''
	) LastMonth 
	ON ISNULL(TODAY.NBR_LIGNE,0)<ISNULL(LastMonth.NBR_LIGNE,0)-(30*ISNULL(LastMonth.NBR_LIGNE,0)/100)
', N'SAB', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie de 30% par rapport au chargement du mois précédent''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes aujourdhui', N'Nb lignes la veille ', N'Nb ligne la semaine précédente', N'Pourcentage dépassé', 1, NULL, 468)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (414, N'VOL_COLLECT_20_PV_FI_M_DES', N'2 - Warning', N'Contrôle de la volumétrie des données lors de la collecte de la table PV_FI_M_DES', N'Contrôle si la variation en valeur absolue de la volumétrie des données par rapport à celle du mois précédent de la table PV_FI_M_DES dans le package PKGCO_SSIS_FI_XSIMDES_00 est supérieure à 20%', N' SELECT 
 DISTINCT THIS_MONTH.PROcessingType as output_col1,   
 ''PKGCO_SSIS_FI_XSIMDES_00'' as output_col2,   
 ''PV_FI_M_DES'' as output_col3,   
 THIS_MONTH.NBR_LIGNE as output_col4,   
 LAST_MONTH.NBR_LIGNE as output_col5,   
 NULL as output_col6,   
 20 AS output_col7  
FROM
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_FI_XSIMDES_00'' 
 AND FileOrTablename= ''PV_FI_M_DES''   
 AND ProcessingStatus = ''OK'' 
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE() ) AS DATE )
) THIS_MONTH  
INNER JOIN   
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_FI_XSIMDES_00''   
 AND FileOrTablename= ''PV_FI_M_DES''
 AND ProcessingStatus = ''OK''  
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE(), -1 ) AS DATE)
) LAST_MONTH   
on (ABS((THIS_MONTH.NBR_LIGNE - LAST_MONTH.NBR_LIGNE)) > (LAST_MONTH.NBR_LIGNE * 20 / 100))', N'DataHub', N'VOL_COLLECT_FF', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport au mois précédent (20%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes mois courant', N'Nb lignes mois précédent', N'NULL', N'Pourcentage dépassé', 1, N'NULL', 100)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (415, N'VOL_COLLECT_20_PV_FI_M_COMPL', N'2 - Warning', N'Contrôle de la volumétrie des données lors de la collecte de la table PV_FI_M_COMPL', N'Contrôle si la variation en valeur absolue de la volumétrie des données par rapport à celle du mois précédent de la table PV_FI_M_COMPL dans le package PKGCO_SSIS_FI_XSIMCPO_00 est supérieure à 20%', N' SELECT 
 DISTINCT THIS_MONTH.PROcessingType as output_col1,   
 ''PKGCO_SSIS_FI_XSIMCPO_00'' as output_col2,   
 ''PV_FI_M_COMPL'' as output_col3,   
 THIS_MONTH.NBR_LIGNE as output_col4,   
 LAST_MONTH.NBR_LIGNE as output_col5,   
 NULL as output_col6,   
 20 AS output_col7  
FROM
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_FI_XSIMCPO_00'' 
 AND FileOrTablename= ''PV_FI_M_COMPL''   
 AND ProcessingStatus = ''OK'' 
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE() ) AS DATE )
) THIS_MONTH  
INNER JOIN   
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_FI_XSIMCPO_00''   
 AND FileOrTablename= ''PV_FI_M_COMPL''
 AND ProcessingStatus = ''OK''  
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE(), -1 ) AS DATE)
) LAST_MONTH   
on (ABS((THIS_MONTH.NBR_LIGNE - LAST_MONTH.NBR_LIGNE)) > (LAST_MONTH.NBR_LIGNE * 20 / 100))
', N'DataHub', N'VOL_COLLECT_FF', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport au mois précédent (20%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes mois courant', N'Nb lignes mois précédent', N'NULL', N'Pourcentage dépassé', 1, N'NULL', 99)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (416, N'VOL_COLLECT_5_PV_FI_M_CTX', N'2 - Warning', N'Contrôle de la volumétrie des données lors de la collecte de la table PV_FI_M_CTX', N'Contrôle si la variation en valeur absolue de la volumétrie des données par rapport à celle du mois précédent de la table PV_FI_M_CTX dans le package PKGCO_SSIS_FI_XSIMCTX_00 est supérieure à 5%', N' SELECT 
 DISTINCT THIS_MONTH.PROcessingType as output_col1,   
 ''PKGCO_SSIS_FI_XSIMCTX_00'' as output_col2,   
 ''PV_FI_M_CTX'' as output_col3,   
 THIS_MONTH.NBR_LIGNE as output_col4,   
 LAST_MONTH.NBR_LIGNE as output_col5,   
 NULL as output_col6,   
 5 AS output_col7  
FROM
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_FI_XSIMCTX_00'' 
 AND FileOrTablename= ''PV_FI_M_CTX''   
 AND ProcessingStatus = ''OK'' 
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE() ) AS DATE )
) THIS_MONTH  
INNER JOIN   
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_FI_XSIMCTX_00''   
 AND FileOrTablename= ''PV_FI_M_CTX''
 AND ProcessingStatus = ''OK''  
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE(), -1 ) AS DATE)
) LAST_MONTH   
on (ABS((THIS_MONTH.NBR_LIGNE - LAST_MONTH.NBR_LIGNE)) > (LAST_MONTH.NBR_LIGNE * 5 / 100))
', N'DataHub', N'VOL_COLLECT_FF', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport au mois précédent (5%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes mois courant', N'Nb lignes mois précédent', N'NULL', N'Pourcentage dépassé', 1, N'NULL', NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (417, N'VOL_COLLECT_5_PV_SA_M_CLIENT', N'2 - Warning', N'Contrôle de la volumétrie des données lors de la collecte de la table PV_SA_M_CLIENT', N'Contrôle si la variation en valeur absolue de la volumétrie des données par rapport à celle du mois précédent de la table PV_SA_M_CLIENT dans le package PKGCO_SSIS_SA_XSIMCLI0_00 est supérieure à 5%', N'SELECT 
 DISTINCT THIS_MONTH.PROcessingType as output_col1,   
 ''PKGCO_SSIS_SA_XSIMCLI0_00'' as output_col2,   
 ''PV_SA_M_CLIENT'' as output_col3,   
 THIS_MONTH.NBR_LIGNE as output_col4,   
 LAST_MONTH.NBR_LIGNE as output_col5,   
 NULL as output_col6,   
 5 AS output_col7  
FROM
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMCLI0_00'' 
 AND FileOrTablename= ''PV_SA_M_CLIENT''   
 AND ProcessingStatus = ''OK'' 
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE() ) AS DATE )
) THIS_MONTH  
INNER JOIN   
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMCLI0_00''   
 AND FileOrTablename= ''PV_SA_M_CLIENT''
 AND ProcessingStatus = ''OK''  
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE(), -1 ) AS DATE)
) LAST_MONTH   
on (ABS((THIS_MONTH.NBR_LIGNE - LAST_MONTH.NBR_LIGNE)) > (LAST_MONTH.NBR_LIGNE * 5 / 100))', N'DataHub', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport au mois précédent (5%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes mois courant', N'Nb lignes mois précédent', NULL, N'Pourcentage dépassé', 1, NULL, 452)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (418, N'VOL_COLLECT_5_PV_SA_M_LKCPTCLI', N'2 - Warning', N'Contrôle de la volumétrie des données lors de la collecte de la table PV_SA_M_LKCPTCLI', N'Contrôle si la variation en valeur absolue de la volumétrie des données par rapport à celle du mois précédent de la table PV_SA_M_LKCPTCLI dans le package PKGCO_SSIS_SA_XSIMGRP0_00 est supérieure à 5%', N' SELECT 
 DISTINCT THIS_MONTH.PROcessingType as output_col1,   
 ''PKGCO_SSIS_SA_XSIMGRP0_00'' as output_col2,   
 ''PV_SA_M_LKCPTCLI'' as output_col3,   
 THIS_MONTH.NBR_LIGNE as output_col4,   
 LAST_MONTH.NBR_LIGNE as output_col5,   
 NULL as output_col6,   
 5 AS output_col7  
FROM
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMGRP0_00'' 
 AND FileOrTablename= ''PV_SA_M_LKCPTCLI''   
 AND ProcessingStatus = ''OK'' 
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE() ) AS DATE )
) THIS_MONTH  
INNER JOIN   
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMGRP0_00''   
 AND FileOrTablename= ''PV_SA_M_LKCPTCLI''
 AND ProcessingStatus = ''OK''  
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE(), -1 ) AS DATE)
) LAST_MONTH   
on (ABS((THIS_MONTH.NBR_LIGNE - LAST_MONTH.NBR_LIGNE)) > (LAST_MONTH.NBR_LIGNE * 5 / 100))', N'DataHub', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport au mois précédent (5%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes mois courant', N'Nb lignes mois précédent', NULL, N'Pourcentage dépassé', 1, NULL, 464)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (419, N'VOL_COLLECT_5_PV_SA_M_ABONNEMENT', N'2 - Warning', N'Contrôle de la volumétrie des données lors de la collecte de la table PV_SA_M_ABONNEMENT', N'Contrôle si la variation en valeur absolue de la volumétrie des données par rapport à celle du mois précédent de la table PV_SA_M_ABONNEMENT dans le package PKGCO_SSIS_SA_XSIMABO0_00 est supérieure à 5%', N' SELECT 
 DISTINCT THIS_MONTH.PROcessingType as output_col1,   
 ''PKGCO_SSIS_SA_XSIMABO0_00'' as output_col2,   
 ''PV_SA_M_ABONNEMENT'' as output_col3,   
 THIS_MONTH.NBR_LIGNE as output_col4,   
 LAST_MONTH.NBR_LIGNE as output_col5,   
 NULL as output_col6,   
 5 AS output_col7  
FROM
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMABO0_00'' 
 AND FileOrTablename= ''PV_SA_M_ABONNEMENT''   
 AND ProcessingStatus = ''OK'' 
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE() ) AS DATE )
) THIS_MONTH  
INNER JOIN   
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMABO0_00''   
 AND FileOrTablename= ''PV_SA_M_ABONNEMENT''
 AND ProcessingStatus = ''OK''  
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE(), -1 ) AS DATE)
) LAST_MONTH   
on (ABS((THIS_MONTH.NBR_LIGNE - LAST_MONTH.NBR_LIGNE)) > (LAST_MONTH.NBR_LIGNE * 5 / 100))', N'DataHub', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport au mois précédent (5%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes mois courant', N'Nb lignes mois précédent', NULL, N'Pourcentage dépassé', 1, NULL, 444)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (420, N'VOL_COLLECT_5_PV_SA_M_PRESTATION', N'2 - Warning', N'Contrôle de la volumétrie des données lors de la collecte de la table PV_SA_M_PRESTATION', N'Contrôle si la variation en valeur absolue de la volumétrie des données par rapport à celle du mois précédent de la table PV_SA_M_PRESTATION dans le package PKGCO_SSIS_SA_XSIMABP0_00 est supérieure à 5%', N' SELECT 
 DISTINCT THIS_MONTH.PROcessingType as output_col1,   
 ''PKGCO_SSIS_SA_XSIMABP0_00'' as output_col2,   
 ''PV_SA_M_PRESTATION'' as output_col3,   
 THIS_MONTH.NBR_LIGNE as output_col4,   
 LAST_MONTH.NBR_LIGNE as output_col5,   
 NULL as output_col6,   
 5 AS output_col7  
FROM
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMABP0_00'' 
 AND FileOrTablename= ''PV_SA_M_PRESTATION''   
 AND ProcessingStatus = ''OK'' 
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE() ) AS DATE )
) THIS_MONTH  
INNER JOIN   
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMABP0_00''   
 AND FileOrTablename= ''PV_SA_M_PRESTATION''
 AND ProcessingStatus = ''OK''  
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE(), -1 ) AS DATE)
) LAST_MONTH   
on (ABS((THIS_MONTH.NBR_LIGNE - LAST_MONTH.NBR_LIGNE)) > (LAST_MONTH.NBR_LIGNE * 5 / 100))', N'DataHub', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport au mois précédent (5%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes mois courant', N'Nb lignes mois précédent', NULL, N'Pourcentage dépassé', 1, NULL, 445)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (421, N'VOL_COLLECT_5_PV_SA_M_SERVICE', N'2 - Warning', N'Contrôle de la volumétrie des données lors de la collecte de la table PV_SA_M_SERVICE', N'Contrôle si la variation en valeur absolue de la volumétrie des données par rapport à celle du mois précédent de la tablePV_SA_M_SERVICE dans le package PKGCO_SSIS_SA_XSIMABS0_00 est supérieure à 5%', N' SELECT 
 DISTINCT THIS_MONTH.PROcessingType as output_col1,   
 ''PKGCO_SSIS_SA_XSIMABS0_00'' as output_col2,   
 ''PV_SA_M_SERVICE'' as output_col3,   
 THIS_MONTH.NBR_LIGNE as output_col4,   
 LAST_MONTH.NBR_LIGNE as output_col5,   
 NULL as output_col6,   
 5 AS output_col7  
FROM
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMABS0_00'' 
 AND FileOrTablename= ''PV_SA_M_SERVICE''   
 AND ProcessingStatus = ''OK'' 
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE() ) AS DATE )
) THIS_MONTH  
INNER JOIN   
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMABS0_00''   
 AND FileOrTablename= ''PV_SA_M_SERVICE''
 AND ProcessingStatus = ''OK''  
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE(), -1 ) AS DATE)
) LAST_MONTH   
on (ABS((THIS_MONTH.NBR_LIGNE - LAST_MONTH.NBR_LIGNE)) > (LAST_MONTH.NBR_LIGNE * 5 / 100))', N'DataHub', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport au mois précédent (5%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes mois courant', N'Nb lignes mois précédent', NULL, N'Pourcentage dépassé', 1, NULL, 445)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (422, N'VOL_COLLECT_5_PV_SA_M_ASSCRE', N'2 - Warning', N'Contrôle de la volumétrie des données lors de la collecte de la table PV_SA_M_ASSCRE', N'Contrôle si la variation en valeur absolue de la volumétrie des données par rapport à celle du mois précédent de la table PV_SA_M_ASSCRE dans le package PKGCO_SSIS_SA_XSIMASS0_00 est supérieure à 5%', N' SELECT 
 DISTINCT THIS_MONTH.PROcessingType as output_col1,   
 ''PKGCO_SSIS_SA_XSIMASS0_00'' as output_col2,   
 ''PV_SA_M_ASSCRE'' as output_col3,   
 THIS_MONTH.NBR_LIGNE as output_col4,   
 LAST_MONTH.NBR_LIGNE as output_col5,   
 NULL as output_col6,   
 5 AS output_col7  
FROM
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMASS0_00'' 
 AND FileOrTablename= ''PV_SA_M_ASSCRE''   
 AND ProcessingStatus = ''OK'' 
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE() ) AS DATE )
) THIS_MONTH  
INNER JOIN   
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMASS0_00''   
 AND FileOrTablename= ''PV_SA_M_ASSCRE''
 AND ProcessingStatus = ''OK''  
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE(), -1 ) AS DATE)
) LAST_MONTH   
on (ABS((THIS_MONTH.NBR_LIGNE - LAST_MONTH.NBR_LIGNE)) > (LAST_MONTH.NBR_LIGNE * 5 / 100))', N'DataHub', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport au mois précédent (5%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes mois courant', N'Nb lignes mois précédent', NULL, N'Pourcentage dépassé', 1, NULL, 447)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (423, N'VOL_COLLECT_5_PV_SA_M_AUTORISATION', N'2 - Warning', N'Contrôle de la volumétrie des données lors de la collecte de la table PV_SA_M_AUTORISATION', N'Contrôle si la variation en valeur absolue de la volumétrie des données par rapport à celle du mois précédent de la table PV_SA_M_AUTORISATION dans le package PKGCO_SSIS_SA_XSIMAUT0_00 est supérieure à 5%', N' SELECT 
 DISTINCT THIS_MONTH.PROcessingType as output_col1,   
 ''PKGCO_SSIS_SA_XSIMAUT0_00'' as output_col2,   
 ''PV_SA_M_AUTORISATION'' as output_col3,   
 THIS_MONTH.NBR_LIGNE as output_col4,   
 LAST_MONTH.NBR_LIGNE as output_col5,   
 NULL as output_col6,   
 5 AS output_col7  
FROM
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMAUT0_00'' 
 AND FileOrTablename= ''PV_SA_M_AUTORISATION''   
 AND ProcessingStatus = ''OK'' 
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE() ) AS DATE )
) THIS_MONTH  
INNER JOIN   
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMAUT0_00''   
 AND FileOrTablename= ''PV_SA_M_AUTORISATION''
 AND ProcessingStatus = ''OK''  
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE(), -1 ) AS DATE)
) LAST_MONTH   
on (ABS((THIS_MONTH.NBR_LIGNE - LAST_MONTH.NBR_LIGNE)) > (LAST_MONTH.NBR_LIGNE * 5 / 100))', N'DataHub', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport au mois précédent (5%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes mois courant', N'Nb lignes mois précédent', NULL, N'Pourcentage dépassé', 1, NULL, 448)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (430, N'VOL_COLLECT_10_PV_SA_M_ENCRS_ECHL', N'2 - Warning', N'Contrôle de la volumétrie des données lors de la collecte de la table PV_SA_M_ENCRS_ECHL', N'Contrôle si la variation en valeur absolue de la volumétrie des données par rapport à celle du mois précédent de la table PV_SA_M_ENCRS_ECHL dans le package PKGCO_SSIS_SA_XSIMENC0_00 est supérieure à 10%', N' SELECT 
 DISTINCT THIS_MONTH.PROcessingType as output_col1,   
 ''PKGCO_SSIS_SA_XSIMENC0_00'' as output_col2,   
 ''PV_SA_M_ENCRS_ECHL'' as output_col3,   
 THIS_MONTH.NBR_LIGNE as output_col4,   
 LAST_MONTH.NBR_LIGNE as output_col5,   
 NULL as output_col6,   
 10 AS output_col7  
FROM
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMENC0_00'' 
 AND FileOrTablename= ''PV_SA_M_ENCRS_ECHL''   
 AND ProcessingStatus = ''OK'' 
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE() ) AS DATE )
) THIS_MONTH  
INNER JOIN   
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMENC0_00''   
 AND FileOrTablename= ''PV_SA_M_ENCRS_ECHL''
 AND ProcessingStatus = ''OK''  
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE(), -1 ) AS DATE)
) LAST_MONTH   
on (ABS((THIS_MONTH.NBR_LIGNE - LAST_MONTH.NBR_LIGNE)) > (LAST_MONTH.NBR_LIGNE * 10 / 100))', N'DataHub', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport au mois précédent (10%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes mois courant', N'Nb lignes mois précédent', NULL, N'Pourcentage dépassé', 1, NULL, 459)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (431, N'VOL_COLLECT_5_PV_SA_M_ENCRS_EPRGN', N'2 - Warning', N'Contrôle de la volumétrie des données lors de la collecte de la table PV_SA_M_ENCRS_EPRGN', N'Contrôle si la variation en valeur absolue de la volumétrie des données par rapport à celle du mois précédent de la table PV_SA_M_ENCRS_EPRGN dans le package PKGCO_SSIS_SA_XSIMERE0_00 est supérieure à 5%', N' SELECT 
 DISTINCT THIS_MONTH.PROcessingType as output_col1,   
 ''PKGCO_SSIS_SA_XSIMERE0_00'' as output_col2,   
 ''PV_SA_M_ENCRS_EPRGN'' as output_col3,   
 THIS_MONTH.NBR_LIGNE as output_col4,   
 LAST_MONTH.NBR_LIGNE as output_col5,   
 NULL as output_col6,   
 5 AS output_col7  
FROM
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMERE0_00'' 
 AND FileOrTablename= ''PV_SA_M_ENCRS_EPRGN''   
 AND ProcessingStatus = ''OK'' 
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE() ) AS DATE )
) THIS_MONTH  
INNER JOIN   
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMERE0_00''   
 AND FileOrTablename= ''PV_SA_M_ENCRS_EPRGN''
 AND ProcessingStatus = ''OK''  
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE(), -1 ) AS DATE)
) LAST_MONTH   
on (ABS((THIS_MONTH.NBR_LIGNE - LAST_MONTH.NBR_LIGNE)) > (LAST_MONTH.NBR_LIGNE * 5 / 100))', N'DataHub', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport au mois précédent (5%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes mois courant', N'Nb lignes mois précédent', NULL, N'Pourcentage dépassé', 1, NULL, 460)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (432, N'VOL_COLLECT_5_PV_SA_M_EXPRISK', N'2 - Warning', N'Contrôle de la volumétrie des données lors de la collecte de la table PV_SA_M_EXPRISK', N'Contrôle si la variation en valeur absolue de la volumétrie des données par rapport à celle du mois précédent de la table PV_SA_M_EXPRISK dans le package PKGCO_SSIS_SA_XSIMEXP0_00 est supérieure à 5%', N' SELECT 
 DISTINCT THIS_MONTH.PROcessingType as output_col1,   
 ''PKGCO_SSIS_SA_XSIMEXP0_00'' as output_col2,   
 ''PV_SA_M_EXPRISK'' as output_col3,   
 THIS_MONTH.NBR_LIGNE as output_col4,   
 LAST_MONTH.NBR_LIGNE as output_col5,   
 NULL as output_col6,   
 5 AS output_col7  
FROM
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMEXP0_00'' 
 AND FileOrTablename= ''PV_SA_M_EXPRISK''   
 AND ProcessingStatus = ''OK'' 
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE() ) AS DATE )
) THIS_MONTH  
INNER JOIN   
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMEXP0_00''   
 AND FileOrTablename= ''PV_SA_M_EXPRISK''
 AND ProcessingStatus = ''OK''  
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE(), -1 ) AS DATE)
) LAST_MONTH   
on (ABS((THIS_MONTH.NBR_LIGNE - LAST_MONTH.NBR_LIGNE)) > (LAST_MONTH.NBR_LIGNE * 5 / 100))', N'DataHub', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport au mois précédent (5%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes mois courant', N'Nb lignes mois précédent', NULL, N'Pourcentage dépassé', 1, NULL, 461)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (433, N'VOL_COLLECT_10_PV_SA_M_INCIDENT', N'2 - Warning', N'Contrôle de la volumétrie des données lors de la collecte de la table PV_SA_M_INCIDENT', N'Contrôle si la variation en valeur absolue de la volumétrie des données par rapport à celle du mois précédent de la table PV_SA_M_INCIDENT dans le package PKGCO_SSIS_SA_XSIMFIM0_00 est supérieure à 10%', N' SELECT 
 DISTINCT THIS_MONTH.PROcessingType as output_col1,   
 ''PKGCO_SSIS_SA_XSIMFIM0_00'' as output_col2,   
 ''PV_SA_M_INCIDENT'' as output_col3,   
 THIS_MONTH.NBR_LIGNE as output_col4,   
 LAST_MONTH.NBR_LIGNE as output_col5,   
 NULL as output_col6,   
 10 AS output_col7  
FROM
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMFIM0_00'' 
 AND FileOrTablename= ''PV_SA_M_INCIDENT''   
 AND ProcessingStatus = ''OK'' 
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE() ) AS DATE )
) THIS_MONTH  
INNER JOIN   
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMFIM0_00''   
 AND FileOrTablename= ''PV_SA_M_INCIDENT''
 AND ProcessingStatus = ''OK''  
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE(), -1 ) AS DATE)
) LAST_MONTH   
on (ABS((THIS_MONTH.NBR_LIGNE - LAST_MONTH.NBR_LIGNE)) > (LAST_MONTH.NBR_LIGNE * 10 / 100))', N'DataHub', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport au mois précédent (10%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes mois courant', N'Nb lignes mois précédent', NULL, N'Pourcentage dépassé', 1, NULL, 462)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (434, N'VOL_COLLECT_5_PV_SA_M_GARANTIE', N'2 - Warning', N'Contrôle de la volumétrie des données lors de la collecte de la table PV_SA_M_GARANTIE', N'Contrôle si la variation en valeur absolue de la volumétrie des données par rapport à celle du mois précédent de la table PV_SA_M_GARANTIE dans le package PKGCO_SSIS_SA_XSIMGAR0_00 est supérieure à 5%', N' SELECT 
 DISTINCT THIS_MONTH.PROcessingType as output_col1,   
 ''PKGCO_SSIS_SA_XSIMGAR0_00'' as output_col2,   
 ''PV_SA_M_GARANTIE'' as output_col3,   
 THIS_MONTH.NBR_LIGNE as output_col4,   
 LAST_MONTH.NBR_LIGNE as output_col5,   
 NULL as output_col6,   
 5 AS output_col7  
FROM
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMGAR0_00'' 
 AND FileOrTablename= ''PV_SA_M_GARANTIE''   
 AND ProcessingStatus = ''OK'' 
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE() ) AS DATE )
) THIS_MONTH  
INNER JOIN   
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMGAR0_00''   
 AND FileOrTablename= ''PV_SA_M_GARANTIE''
 AND ProcessingStatus = ''OK''  
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE(), -1 ) AS DATE)
) LAST_MONTH   
on (ABS((THIS_MONTH.NBR_LIGNE - LAST_MONTH.NBR_LIGNE)) > (LAST_MONTH.NBR_LIGNE * 5 / 100))', N'DataHub', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport au mois précédent (5%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes mois courant', N'Nb lignes mois précédent', NULL, N'Pourcentage dépassé', 1, NULL, 463)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (435, N'VOL_COLLECT_5_PV_SA_M_LKAUT', N'2 - Warning', N'Contrôle de la volumétrie des données lors de la collecte de la table PV_SA_M_LKAUT', N'Contrôle si la variation en valeur absolue de la volumétrie des données par rapport à celle du mois précédent de la table PV_SA_M_LKAUT dans le package PKGCO_SSIS_SA_XSIMLIA0_00 est supérieure à 5%', N' SELECT 
 DISTINCT THIS_MONTH.PROcessingType as output_col1,   
 ''PKGCO_SSIS_SA_XSIMLIA0_00'' as output_col2,   
 ''PV_SA_M_LKAUT'' as output_col3,   
 THIS_MONTH.NBR_LIGNE as output_col4,   
 LAST_MONTH.NBR_LIGNE as output_col5,   
 NULL as output_col6,   
 5 AS output_col7  
FROM
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMLIA0_00'' 
 AND FileOrTablename= ''PV_SA_M_LKAUT''   
 AND ProcessingStatus = ''OK'' 
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE() ) AS DATE )
) THIS_MONTH  
INNER JOIN   
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMLIA0_00''   
 AND FileOrTablename= ''PV_SA_M_LKAUT''
 AND ProcessingStatus = ''OK''  
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE(), -1 ) AS DATE)
) LAST_MONTH   
on (ABS((THIS_MONTH.NBR_LIGNE - LAST_MONTH.NBR_LIGNE)) > (LAST_MONTH.NBR_LIGNE * 5 / 100))', N'DataHub', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport au mois précédent (5%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes mois courant', N'Nb lignes mois précédent', NULL, N'Pourcentage dépassé', 1, NULL, 466)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (436, N'VOL_COLLECT_5_PV_SA_M_LKGAR', N'2 - Warning', N'Contrôle de la volumétrie des données lors de la collecte de la table PV_SA_M_LKGAR', N'Contrôle si la variation en valeur absolue de la volumétrie des données par rapport à celle du mois précédent de la table PV_SA_M_LKGAR dans le package PKGCO_SSIS_SA_XSIMLIE0_00 est supérieure à 5%', N' SELECT 
 DISTINCT THIS_MONTH.PROcessingType as output_col1,   
 ''PKGCO_SSIS_SA_XSIMLIE0_00'' as output_col2,   
 ''PV_SA_M_LKGAR'' as output_col3,   
 THIS_MONTH.NBR_LIGNE as output_col4,   
 LAST_MONTH.NBR_LIGNE as output_col5,   
 NULL as output_col6,   
 5 AS output_col7  
FROM
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMLIE0_00'' 
 AND FileOrTablename= ''PV_SA_M_LKGAR''   
 AND ProcessingStatus = ''OK'' 
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE() ) AS DATE )
) THIS_MONTH  
INNER JOIN   
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMLIE0_00''   
 AND FileOrTablename= ''PV_SA_M_LKGAR''
 AND ProcessingStatus = ''OK''  
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE(), -1 ) AS DATE)
) LAST_MONTH   
on (ABS((THIS_MONTH.NBR_LIGNE - LAST_MONTH.NBR_LIGNE)) > (LAST_MONTH.NBR_LIGNE * 5 / 100))', N'DataHub', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport au mois précédent (5%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes mois courant', N'Nb lignes mois précédent', NULL, N'Pourcentage dépassé', 1, NULL, 467)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (437, N'VOL_COLLECT_15_PV_SA_M_INT_MARGE', N'2 - Warning', N'Contrôle de la volumétrie des données lors de la collecte de la table PV_SA_M_LKCPTINT', N'Contrôle si la variation en valeur absolue de la volumétrie des données par rapport à celle du mois précédent de la table PV_SA_M_INT_MARGE dans le package PKGCO_SSIS_SA_XSIMMM10_00 est supérieure à 15%', N' SELECT 
 DISTINCT THIS_MONTH.PROcessingType as output_col1,   
 ''PKGCO_SSIS_SA_XSIMMM10_00'' as output_col2,   
 ''PV_SA_M_LKCPTINT'' as output_col3,   
 THIS_MONTH.NBR_LIGNE as output_col4,   
 LAST_MONTH.NBR_LIGNE as output_col5,   
 NULL as output_col6,   
 15 AS output_col7  
FROM
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMMM10_00'' 
 AND FileOrTablename= ''PV_SA_M_LKCPTINT''   
 AND ProcessingStatus = ''OK'' 
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE() ) AS DATE )
) THIS_MONTH  
INNER JOIN   
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMMM10_00''   
 AND FileOrTablename= ''PV_SA_M_LKCPTINT''
 AND ProcessingStatus = ''OK''  
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE(), -1 ) AS DATE)
) LAST_MONTH   
on (ABS((THIS_MONTH.NBR_LIGNE - LAST_MONTH.NBR_LIGNE)) > (LAST_MONTH.NBR_LIGNE * 15 / 100))', N'DataHub', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport au mois précédent (15%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes mois courant', N'Nb lignes mois précédent', NULL, N'Pourcentage dépassé', 1, NULL, 469)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (439, N'VOL_COLLECT_10_PV_SA_M_COMOPE', N'2 - Warning', N'Contrôle de la volumétrie des données lors de la collecte de la table PV_SA_M_COMOPE', N'Contrôle si la variation en valeur absolue de la volumétrie des données par rapport à celle du mois précédent de la table PV_SA_M_COMOPE dans le package PKGCO_SSIS_SA_XSIMMM40_00 est supérieure à 10%', N' SELECT 
 DISTINCT THIS_MONTH.PROcessingType as output_col1,   
 ''PKGCO_SSIS_SA_XSIMMM40_00'' as output_col2,   
 ''PV_SA_M_COMOPE'' as output_col3,   
 THIS_MONTH.NBR_LIGNE as output_col4,   
 LAST_MONTH.NBR_LIGNE as output_col5,   
 NULL as output_col6,   
 10 AS output_col7  
FROM
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMMM40_00'' 
 AND FileOrTablename= ''PV_SA_M_COMOPE''   
 AND ProcessingStatus = ''OK'' 
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE() ) AS DATE )
) THIS_MONTH  
INNER JOIN   
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMMM40_00''   
 AND FileOrTablename= ''PV_SA_M_COMOPE''
 AND ProcessingStatus = ''OK''  
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE(), -1 ) AS DATE)
) LAST_MONTH   
on (ABS((THIS_MONTH.NBR_LIGNE - LAST_MONTH.NBR_LIGNE)) > (LAST_MONTH.NBR_LIGNE * 10 / 100))', N'DataHub', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport au mois précédent (10%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes mois courant', N'Nb lignes mois précédent', NULL, N'Pourcentage dépassé', 1, NULL, 470)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (438, N'VOL_COLLECT_5_PV_SA_M_INT_MARGE', N'2 - Warning', N'Contrôle de la volumétrie des données lors de la collecte de la table PV_SA_M_INT_MARGE', N'Contrôle si la variation en valeur absolue de la volumétrie des données par rapport à celle du mois précédent de la table PV_SA_M_INT_MARGE dans le package PKGCO_SSIS_SA_XSIMMM30_00 est supérieure à 5%', N' SELECT 
 DISTINCT THIS_MONTH.PROcessingType as output_col1,   
 ''PKGCO_SSIS_SA_XSIMMM30_00'' as output_col2,   
 ''PV_SA_M_INT_MARGE'' as output_col3,   
 THIS_MONTH.NBR_LIGNE as output_col4,   
 LAST_MONTH.NBR_LIGNE as output_col5,   
 NULL as output_col6,   
 5 AS output_col7  
FROM
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMMM30_00'' 
 AND FileOrTablename= ''PV_SA_M_INT_MARGE''   
 AND ProcessingStatus = ''OK'' 
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE() ) AS DATE )
) THIS_MONTH  
INNER JOIN   
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMMM30_00''   
 AND FileOrTablename= ''PV_SA_M_INT_MARGE''
 AND ProcessingStatus = ''OK''  
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE(), -1 ) AS DATE)
) LAST_MONTH   
on (ABS((THIS_MONTH.NBR_LIGNE - LAST_MONTH.NBR_LIGNE)) > (LAST_MONTH.NBR_LIGNE * 5 / 100))', N'DataHub', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport au mois précédent (5%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes mois courant', N'Nb lignes mois précédent', NULL, N'Pourcentage dépassé', 1, NULL, 469)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (440, N'VOL_COLLECT_5_PV_SA_M_NOTEXTCLI', N'2 - Warning', N'Contrôle de la volumétrie des données lors de la collecte de la table PV_SA_M_NOTEXTCLI', N'Contrôle si la variation en valeur absolue de la volumétrie des données par rapport à celle du mois précédent de la table PV_SA_M_NOTEXTCLI dans le package PKGCO_SSIS_SA_XSIMNEX0_00 est supérieure à 5%', N' SELECT 
 DISTINCT THIS_MONTH.PROcessingType as output_col1,   
 ''PKGCO_SSIS_SA_XSIMNEX0_00'' as output_col2,   
 ''PV_SA_M_NOTEXTCLI'' as output_col3,   
 THIS_MONTH.NBR_LIGNE as output_col4,   
 LAST_MONTH.NBR_LIGNE as output_col5,   
 NULL as output_col6,   
 5 AS output_col7  
FROM
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMNEX0_00'' 
 AND FileOrTablename= ''PV_SA_M_NOTEXTCLI''   
 AND ProcessingStatus = ''OK'' 
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE() ) AS DATE )
) THIS_MONTH  
INNER JOIN   
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMNEX0_00''   
 AND FileOrTablename= ''PV_SA_M_NOTEXTCLI''
 AND ProcessingStatus = ''OK''  
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE(), -1 ) AS DATE)
) LAST_MONTH   
on (ABS((THIS_MONTH.NBR_LIGNE - LAST_MONTH.NBR_LIGNE)) > (LAST_MONTH.NBR_LIGNE * 5 / 100))', N'DataHub', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport au mois précédent (5%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes mois courant', N'Nb lignes mois précédent', NULL, N'Pourcentage dépassé', 1, NULL, 471)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (441, N'VOL_COLLECT_5_PV_SA_M_OPCREDIT', N'2 - Warning', N'Contrôle de la volumétrie des données lors de la collecte de la table PV_SA_M_OPCREDIT', N'Contrôle si la variation en valeur absolue de la volumétrie des données par rapport à celle du mois précédent de la table PV_SA_M_OPCREDIT dans le package PKGCO_SSIS_SA_XSIMOPE0_00 est supérieure à 5%', N' SELECT 
 DISTINCT THIS_MONTH.PROcessingType as output_col1,   
 ''PKGCO_SSIS_SA_XSIMOPE0_00'' as output_col2,   
 ''PV_SA_M_OPCREDIT'' as output_col3,   
 THIS_MONTH.NBR_LIGNE as output_col4,   
 LAST_MONTH.NBR_LIGNE as output_col5,   
 NULL as output_col6,   
 5 AS output_col7  
FROM
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMOPE0_00'' 
 AND FileOrTablename= ''PV_SA_M_OPCREDIT''   
 AND ProcessingStatus = ''OK'' 
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE() ) AS DATE )
) THIS_MONTH  
INNER JOIN   
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMOPE0_00''   
 AND FileOrTablename= ''PV_SA_M_OPCREDIT''
 AND ProcessingStatus = ''OK''  
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE(), -1 ) AS DATE)
) LAST_MONTH   
on (ABS((THIS_MONTH.NBR_LIGNE - LAST_MONTH.NBR_LIGNE)) > (LAST_MONTH.NBR_LIGNE * 5 / 100))', N'DataHub', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport au mois précédent (5%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes mois courant', N'Nb lignes mois précédent', NULL, N'Pourcentage dépassé', 1, NULL, 472)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (442, N'VOL_COLLECT_5_PV_SA_M_PROCPT', N'2 - Warning', N'Contrôle de la volumétrie des données lors de la collecte de la table PV_SA_M_PROCPT', N'Contrôle si la variation en valeur absolue de la volumétrie des données par rapport à celle du mois précédent de la table PV_SA_M_PROCPT dans le package PKGCO_SSIS_SA_XSIMPRC0_00 est supérieure à 5%', N' SELECT 
 DISTINCT THIS_MONTH.PROcessingType as output_col1,   
 ''PKGCO_SSIS_SA_XSIMPRC0_00'' as output_col2,   
 ''PV_SA_M_PROCPT'' as output_col3,   
 THIS_MONTH.NBR_LIGNE as output_col4,   
 LAST_MONTH.NBR_LIGNE as output_col5,   
 NULL as output_col6,   
 5 AS output_col7  
FROM
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMPRC0_00'' 
 AND FileOrTablename= ''PV_SA_M_PROCPT''   
 AND ProcessingStatus = ''OK'' 
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE() ) AS DATE )
) THIS_MONTH  
INNER JOIN   
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)    
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMPRC0_00''   
 AND FileOrTablename= ''PV_SA_M_PROCPT''
 AND ProcessingStatus = ''OK''  
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE(), -1 ) AS DATE)
) LAST_MONTH   
on (ABS((THIS_MONTH.NBR_LIGNE - LAST_MONTH.NBR_LIGNE)) > (LAST_MONTH.NBR_LIGNE * 5 / 100))', N'DataHub', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport au mois précédent (5%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes mois courant', N'Nb lignes mois précédent', NULL, N'Pourcentage dépassé', 1, NULL, 475)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (443, N'VOL_COLLECT_20_PV_SA_M_PRDCOMPTA', N'2 - Warning', N'Contrôle de la volumétrie des données lors de la collecte de la table PV_SA_M_PRDCOMPTA', N'Contrôle si la variation en valeur absolue de la volumétrie des données par rapport à celle du mois précédent de la table PV_SA_M_PRDCOMPTA dans le package PKGCO_SSIS_SA_XSIMPRD0_00 est supérieure à 20%', N' SELECT 
 DISTINCT THIS_MONTH.PROcessingType as output_col1,   
 ''PKGCO_SSIS_SA_XSIMPRD0_00'' as output_col2,   
 ''PV_SA_M_PRDCOMPTA'' as output_col3,   
 THIS_MONTH.NBR_LIGNE as output_col4,   
 LAST_MONTH.NBR_LIGNE as output_col5,   

 NULL as output_col6,   
 20 AS output_col7  
FROM
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)    
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMPRD0_00'' 
 AND FileOrTablename= ''PV_SA_M_PRDCOMPTA''   
 AND ProcessingStatus = ''OK'' 
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE() ) AS DATE )
) THIS_MONTH  
INNER JOIN   
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)    
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMPRD0_00''   
 AND FileOrTablename= ''PV_SA_M_PRDCOMPTA''
 AND ProcessingStatus = ''OK''  
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE(), -1 ) AS DATE)
) LAST_MONTH   
on (ABS((THIS_MONTH.NBR_LIGNE - LAST_MONTH.NBR_LIGNE)) > (LAST_MONTH.NBR_LIGNE * 20 / 100))', N'DataHub', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport au mois précédent (20%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes mois courant', N'Nb lignes mois précédent', NULL, N'Pourcentage dépassé', 1, NULL, 476)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (444, N'VOL_COLLECT_5_PV_SA_M_PROOP', N'2 - Warning', N'Contrôle de la volumétrie des données lors de la collecte de la table PV_SA_M_PROOP', N'Contrôle si la variation en valeur absolue de la volumétrie des données par rapport à celle du mois précédent de la table PV_SA_M_PROOP dans le packagePKGCO_SSIS_SA_XSIMPRO0_00 est supérieure à 5%', N' SELECT 
 DISTINCT THIS_MONTH.PROcessingType as output_col1,   
 ''PKGCO_SSIS_SA_XSIMPRO0_00'' as output_col2,   
 ''PV_SA_M_PROOP'' as output_col3,   
 THIS_MONTH.NBR_LIGNE as output_col4,   
 LAST_MONTH.NBR_LIGNE as output_col5,   
 NULL as output_col6,   
 5 AS output_col7  
FROM
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)    
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMPRO0_00'' 
 AND FileOrTablename= ''PV_SA_M_PROOP''   
 AND ProcessingStatus = ''OK'' 
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE() ) AS DATE )
) THIS_MONTH  
INNER JOIN   
(
 SELECT ProcessingType,NbRecordsCollected AS NBR_LIGNE   
 FROM [$(DataHubDatabaseName)].dbo.DATA_LOG WITH(NOLOCK)   
 WHERE ETLName = ''PKGCO_SSIS_SA_XSIMPRO0_00''   
 AND FileOrTablename= ''PV_SA_M_PROOP''
 AND ProcessingStatus = ''OK''  
 AND CAST ( EOMONTH (PROCESSINGSTARTDATE) AS DATE ) = CAST ( EOMONTH ( GETDATE(), -1 ) AS DATE)
) LAST_MONTH   
on (ABS((THIS_MONTH.NBR_LIGNE - LAST_MONTH.NBR_LIGNE)) > (LAST_MONTH.NBR_LIGNE * 5 / 100))', N'DataHub', N'VOL_COLLECT_SAB', N'Technique - Contrôle volumétrie', N'''Variation de la volumétrie par rapport au mois précédent (5%)''', N'Processing Type', N'Nom du package', N'Nom de la table', N'Nb lignes mois courant', N'Nb lignes mois précédent', NULL, N'Pourcentage dépassé', 1, NULL, 477)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (445, N'DWH_CONTROL_REFERENTIEL_SA_1.0.1', N'2 - Warning', N'Comparaison nombre de référentiel DataFactory/ DWH table REFERENTIEL_SA', N'Ce contrôle permet de vérifier que le nombre d''enregistrements extraits de la table REFERENTIEL_SA de DataFactory sur le serveur 12 est égale au nombre extraits de DWH sur le serveur 24, et ce nombre d''enregistrements est différent que 0', N'SELECT NB_REF_SA_12 AS output_col1,
    NB_REF_SA_24 AS output_col2,
    NULL AS output_col3,
    NULL AS output_col4,
    NULL AS output_col5,
    NULL AS output_col6,
    NULL AS output_col7
FROM (
 SELECT COUNT(*) AS NB_REF_SA_12
 FROM [dbo].[REFERENTIEL_SA] WITH(NOLOCK)) A
JOIN (
 SELECT COUNT(*) AS NB_REF_SA_24
 FROM [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].[dbo].[REFERENTIEL_SA] WITH(NOLOCK)) B ON 1=1
WHERE NB_REF_SA_12 != NB_REF_SA_24 
   OR NB_REF_SA_12 = 0 
   OR NB_REF_SA_24 = 0', N'DWH', N'REFERENTIEL', N'Technique - Contrôle intégration', N'''Différence de lignes entre DataFactory sur le 12, ''+cast(output_col1 as char)+'', et DWH sur le 24 :''+cast(output_col2 as char)', N'Nombre de lignes en DataFactory sur 12', N'Nombre de lignes en DWH sur 24', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (446, N'DWH_CONTROL_REFERENTIEL_SF_1.0.1', N'2 - Warning', N'Comparaison nombre de référentiel DataFactory/ DWH table REFERENTIEL_SF', N'Ce contrôle permet de vérifier que le nombre d''enregistrements extraits de la table REFERENTIEL_SF de DataFactory sur le serveur 12 est égale au nombre extraits de DWH sur le serveur 24, et ce nombre d''enregistrements est différent que 0', N'SELECT NB_REF_SF_12 AS output_col1,
    NB_REF_SF_24 AS output_col2,
    NULL AS output_col3,
    NULL AS output_col4,
    NULL AS output_col5,
    NULL AS output_col6,
    NULL AS output_col7
FROM (
 SELECT COUNT(*) AS NB_REF_SF_12
 FROM [dbo].[REFERENTIEL_SF] WITH(NOLOCK)) A
JOIN (
 SELECT COUNT(*) AS NB_REF_SF_24
 FROM [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].[dbo].[REFERENTIEL_SF] WITH(NOLOCK)) B ON 1=1
WHERE NB_REF_SF_12 != NB_REF_SF_24 
   OR NB_REF_SF_12 = 0 
   OR NB_REF_SF_24 = 0
', N'DWH', N'REFERENTIEL', N'Technique - Contrôle intégration', N'''Différence de lignes entre DataFactory sur le 12, ''+cast(output_col1 as char)+'', et DWH sur le 24 :''+cast(output_col2 as char)', N'Nombre de lignes en DataFactory sur 12', N'Nombre de lignes en DWH sur 24', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (447, N'DWH_CONTROL_REFERENTIEL_SA_1.0.2', N'2 - Warning', N'Comparaison le détail des référentiels DataFactory/ DWH table REFERENTIEL_SA', N'Ce contrôle permet de comparer les détails de la table REFERENTIEL_SA de DataFactory sur le serveur 12 avec cette table de DWH sur le serveur 24', N'SELECT ID_REFERENTIEL_SA as output_col1,
       SOUR_DONN as output_col2,
    Col1 as output_col3,
    Col2 as output_col4,
    Col3 as output_col5,
    Col4 as output_col6,
    Col5 as output_col7
FROM (
 (SELECT *
  FROM [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].[dbo].[REFERENTIEL_SA] WITH(NOLOCK)
  EXCEPT
  SELECT *
  FROM [dbo].[REFERENTIEL_SA] WITH(NOLOCK))
 UNION ALL
 (SELECT *
  FROM [dbo].[REFERENTIEL_SA] WITH(NOLOCK)
  EXCEPT
  SELECT *
  FROM [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].[dbo].[REFERENTIEL_SA] WITH(NOLOCK))
 ) Detail', N'DWH', N'REFERENTIEL', N'Technique - Contrôle intégration', N'''Les valeurs ne sont pas les mêmes pour la ligne : ''', N'ID_REFERENTIEL_SA', N'SOUR_DONN', N'Col1', N'Col2', N'Col3', N'Col4', N'Col5', 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (448, N'DWH_CONTROL_REFERENTIEL_SF_1.0.2', N'2 - Warning', N'Comparaison le détail des référentiels DataFactory/ DWH table REFERENTIEL_SF', N'Ce contrôle permet de comparer les détails de la table REFERENTIEL_SF de DataFactory sur le serveur 12 avec cette table de DWH sur le serveur 24', N'SELECT ID_REFERENTIEL_SF as output_col1,
       COD_OBJ_SF as output_col2,
    COD_CHMP_SF as output_col3,
    COD_SF as output_col4,
    COD_CHMP_PARN_SF as output_col5,
    COD_PARN_SF as output_col6,
    NULL as output_col7
FROM (
 (SELECT *
  FROM [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].[dbo].[REFERENTIEL_SF] WITH(NOLOCK)
  EXCEPT
  SELECT *
  FROM [dbo].[REFERENTIEL_SF] WITH(NOLOCK))
 UNION ALL
 (SELECT *
  FROM [dbo].[REFERENTIEL_SF] WITH(NOLOCK)
  EXCEPT
  SELECT *
  FROM [$(DwhDatabaseInstanceName)].[$(DwhDatabaseName)].[dbo].[REFERENTIEL_SF] WITH(NOLOCK))
 ) Detail', N'DWH', N'REFERENTIEL', N'Technique - Contrôle intégration', N'''Les valeurs ne sont pas les mêmes pour la ligne : ''', N'ID_REFERENTIEL_SF', N'COD_OBJ_SF', N'COD_CHMP_SF', N'COD_SF', N'COD_CHMP_PARN_SF', N'COD_PARN_SF', NULL, 1, NULL, NULL)
GO
INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (449, N'DF_CONTROL_REFERENTIEL_SF_1', N'2 - Warning', N'Identification des doublons dans la table REFERENTIEL_SF', N'Ce contrôle vise à identifier les doublons contenus dans la table REFERENTIEL_SF', N'SELECT COD_OBJ_SF AS OUTPUT_COL1,COD_CHMP_SF AS OUTPUT_COL2,COD_SF AS OUTPUT_COL3,COD_PARN_SF AS OUTPUT_COL4, NULL AS OUTPUT_COL5, NULL AS OUTPUT_COL6, NULL AS OUTPUT_COL7
FROM 
(SELECT COD_OBJ_SF,COD_CHMP_SF,COD_SF,COD_PARN_SF,DAT_FIN_VALD, LEAD(DAT_FIN_VALD)  OVER (PARTITION BY COD_OBJ_SF,COD_CHMP_SF,COD_SF,COD_PARN_SF ORDER BY ID_REFERENTIEL_SF DESC) AS LEAD_DAT_FIN_VALD
 FROM [dbo].[REFERENTIEL_SF] WITH(NOLOCK)) REQ
WHERE DAT_FIN_VALD =''9999-12-31'' and LEAD_DAT_FIN_VALD = ''9999-12-31''', N'DataFactory', N'REFERENTIEL', N'Technique - Contrôle Doublons', N'''Présence de doublons pour l''+char(39)+''identifiant suivant : COD_OBJ_SF = ''+LTRIM(RTRIM(CAST(OUTPUT_COL1 AS CHAR)))+'', COD_CHMP_SF = ''+LTRIM(RTRIM(CAST(OUTPUT_COL2 AS CHAR)))+'', COD_SF = ''+LTRIM(RTRIM(CAST(OUTPUT_COL3 AS CHAR)))+'', COD_PARN_SF = ''+LTRIM(RTRIM(CAST(OUTPUT_COL4 AS CHAR)))', N'COD_OBJ_SF', N'COD_CHMP_SF', N'COD_SF', N'COD_PARN_SF', NULL, NULL, NULL, 1, NULL, 308)
GO

INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (450, N'DH_PV_SA_Q_CLIENT_1', N'2 - Warning', N'Identification des numéros de client groupe ayant l''indicateur tiers collectif valorisé à 0', N'Ce contrôle vise à identifier les numéros de client groupe ayant l''indicateur tiers collectif valorisé à 0', N'SELECT DWHGRPREG AS OUTPUT_COL1,NULL AS OUTPUT_COL2,NULL AS OUTPUT_COL3,NULL AS OUTPUT_COL4, NULL AS OUTPUT_COL5, NULL AS OUTPUT_COL6, NULL AS OUTPUT_COL7
FROM 
(SELECT DISTINCT DWHGRPREG FROM [$(DataHubDatabaseName)].[dbo].PV_SA_Q_LKCPTCLI
JOIN [$(DataHubDatabaseName)].[dbo].PV_SA_Q_CLIENT
ON DWHGRPREG = DWHCLICLI AND DWHCLICOL = 0
WHERE DWHGRPREL IN (''EPX'',''COL'')) REQ', N'DataHub', N'SAB - Contrôle DataHUB', N'Fonctionnel', N'''Le numéro de client groupe "''+LTRIM(RTRIM(CAST(OUTPUT_COL1 AS CHAR)))+''" a l''+char(39)+''indicateur tiers collectif valorisé à 0 dans la table Client''', N'DWHGRPREG', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 487)
GO

INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (451, N'BFR_CONTROL_RENTA_1', N'1 - Bloquant', N'vérification de la somme du champ DWHENCBA1 dans la table PV_SA_M_ENCRS_ECHL', N'ce contrôle génère une alerte si la somme du champ DWHENCBA1 de la table PV_SA_M_ENCRS_ECHL du mois courant est égale à celle du mois précédent', N'select sum(DWHENCBA1) as output_col1, NULL as output_col2, NULL AS output_col3, NULL AS output_col4, NULL AS output_col5, NULL AS output_col6, NULL AS output_col7   
from [$(DataHubDatabaseName)].dbo.PV_SA_M_ENCRS_ECHL WHERE DWHENCDTX=((select MAX(DWHENCDTX) from [$(DataHubDatabaseName)].[dbo].PV_SA_M_ENCRS_ECHL))   
having sum(DWHENCBA1) = ( select sum(DWHENCBA1) from  [$(DataHubDatabaseName)].dbo.[PV_SA_M_ENCRS_ECHL_AS_OF_DATE](getdate()) WHERE DWHENCDTX=(EOMONTH((select MAX(DWHENCDTX) from [$(DataHubDatabaseName)].[dbo].PV_SA_M_ENCRS_ECHL),-1))) ',
 N'DataHub', N'DATAHUB_BFR', N'Fonctionnel', N'''la somme du champ DWHENCBA1 de la table PV_SA_M_ENCRS_ECHL du mois courant est égale à celle du mois précédent :''+Cast(output_col1 as char)', N'Somme DWHENCBA1', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 559)
 GO

INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (452, N'BFR_CONTROL_RENTA_2', N'1 - Bloquant', N'vérification de la somme du champ DWHEREBA1 dans la table PV_SA_M_ENCRS_EPRGN', N'ce contrôle génère une alerte si la somme du champ DWHEREBA1 de la table PV_SA_M_ENCRS_EPRGN du mois courant est égale à celle du mois précédent', N'select 
sum(DWHEREBA1) as output_col1, 
NULL as output_col2, 
NULL AS output_col3, 
NULL AS output_col4, 
NULL AS output_col5, 
NULL AS output_col6, 
NULL AS output_col7   
from [$(DataHubDatabaseName)].dbo.PV_SA_M_ENCRS_EPRGN WHERE DWHEREDTX=((select MAX(DWHEREDTX) from [$(DataHubDatabaseName)].[dbo].PV_SA_M_ENCRS_EPRGN))   
having sum(DWHEREBA1) = ( select sum(DWHEREBA1) from  [$(DataHubDatabaseName)].dbo.[PV_SA_M_ENCRS_EPRGN_AS_OF_DATE](getdate()) WHERE DWHEREDTX=(EOMONTH((select MAX(DWHEREDTX) from [$(DataHubDatabaseName)].[dbo].PV_SA_M_ENCRS_EPRGN),-1))) ',
 N'DataHub', N'DATAHUB_BFR', N'Fonctionnel', N'''la somme du champ DWHEREBA1 de la table PV_SA_M_ENCRS_EPRGN du mois courant est égale à celle du mois précédent :''+Cast(output_col1 as char)', N'Somme DWHEREBA1', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 559)
 GO 

 INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (453, N'BFR_CONTROL_RENTA_3', N'1 - Bloquant', N'vérification de la somme du champ DWHMM1CVL dans la table PV_SA_M_LKCPTINT', N'ce contrôle génère une alerte si la somme du champ DWHMM1CVL de la table PV_SA_M_LKCPTINT du mois courant est égale à celle du mois précédent', N'select 
sum(DWHMM1CVL) as output_col1, 
NULL as output_col2, 
NULL AS output_col3, 
NULL AS output_col4, 
NULL AS output_col5, 
NULL AS output_col6, 
NULL AS output_col7   
from [$(DataHubDatabaseName)].dbo.PV_SA_M_LKCPTINT WHERE DWHMM1DTX=((select MAX(DWHMM1DTX) from [$(DataHubDatabaseName)].[dbo].PV_SA_M_LKCPTINT))   
having sum(DWHMM1CVL) = ( select sum(DWHMM1CVL) from  [$(DataHubDatabaseName)].dbo.PV_SA_M_LKCPTINT_AS_OF_DATE(getdate()) WHERE DWHMM1DTX=(EOMONTH((select MAX(DWHMM1DTX) from [$(DataHubDatabaseName)].[dbo].PV_SA_M_LKCPTINT),-1))) ',
 N'DataHub', N'DATAHUB_BFR', N'Fonctionnel', N'''la somme du champ DWHMM1CVL de la table PV_SA_M_LKCPTINT du mois courant est égale à celle du mois précédent :''+Cast(output_col1 as char)', N'Somme DWHMM1CVL', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 559)
GO 

INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (454, N'BFR_CONTROL_RENTA_4', N'1 - Bloquant', N'vérification de la somme du champ DWHMM3INC dans la table PV_SA_M_INT_MARGE', N'ce contrôle génère une alerte si la somme du champ DWHMM3INC de la table PV_SA_M_INT_MARGE du mois courant est égale à celle du mois précédent', N'select 
sum(DWHMM3INC) as output_col1, 
NULL as output_col2, 
NULL AS output_col3, 
NULL AS output_col4, 
NULL AS output_col5, 
NULL AS output_col6, 
NULL AS output_col7   
from [$(DataHubDatabaseName)].dbo.PV_SA_M_INT_MARGE WHERE DWHMM3DTX=((select MAX(DWHMM3DTX) from [$(DataHubDatabaseName)].[dbo].PV_SA_M_INT_MARGE))   
having sum(DWHMM3INC) = ( select sum(DWHMM3INC) from  [$(DataHubDatabaseName)].dbo.PV_SA_M_INT_MARGE_AS_OF_DATE(getdate()) WHERE DWHMM3DTX=(EOMONTH((select MAX(DWHMM3DTX) from [$(DataHubDatabaseName)].[dbo].PV_SA_M_INT_MARGE),-1))) ',
 N'DataHub', N'DATAHUB_BFR', N'Fonctionnel', N'''la somme du champ DWHMM3INC de la table PV_SA_M_INT_MARGE du mois courant est égale à celle du mois précédent :''+Cast(output_col1 as char)', N'Somme DWHMM3INC', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 559)
 GO 

 INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (455, N'BFR_CONTROL_RENTA_5', N'1 - Bloquant', N'vérification de la somme du champ DWHMM4MON dans la table PV_SA_M_COM_OPE', N'ce contrôle génère une alerte si la somme du champ DWHMM4MON de la table PV_SA_M_COMOPE du mois courant est égale à celle du mois précédent', N'select 
sum(DWHMM4MON) as output_col1, 
NULL as output_col2, 
NULL AS output_col3, 
NULL AS output_col4, 
NULL AS output_col5, 
NULL AS output_col6, 
NULL AS output_col7   
from [$(DataHubDatabaseName)].dbo.PV_SA_M_COMOPE 
where DWHMM4DTX =((select MAX(DWHMM4DTX) from [$(DataHubDatabaseName)].[dbo].PV_SA_M_COMOPE))   
having sum(DWHMM4MON) = ( select sum(DWHMM4MON) from  [$(DataHubDatabaseName)].dbo.[PV_SA_M_COMOPE_AS_OF_DATE](GETDATE())
where DWHMM4DTX =(EOMONTH((select MAX(DWHMM4DTX) from [$(DataHubDatabaseName)].[dbo].PV_SA_M_COMOPE),-1))) ',
 N'DataHub', N'DATAHUB_BFR', N'Fonctionnel', N'''la somme du champ DWHMM4MON de la table PV_SA_M_COMOPE du mois courant est égale à celle du mois précédent :''+Cast(output_col1 as char)', N'Somme DWHMM4MON', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 559)
GO 

INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (456, N'BFR_CONTROL_RENTA_6', N'1 - Bloquant', N'vérification de la somme du champ DWHPRDMON dans la table PV_SA_M_PRDCOMPTA', N'ce contrôle génère une alerte si la somme du champ DWHPRDMON de la table PV_SA_M_PRDCOMPTA du mois courant est égale à celle du mois précédent', N'select 
sum(DWHPRDMON) as output_col1, 
NULL as output_col2, 
NULL AS output_col3, 
NULL AS output_col4, 
NULL AS output_col5, 
NULL AS output_col6, 
NULL AS output_col7   
from [$(DataHubDatabaseName)].dbo.[PV_SA_M_PRDCOMPTA] WHERE DWHPRDDTX=((select MAX(DWHPRDDTX) from [$(DataHubDatabaseName)].[dbo].[PV_SA_M_PRDCOMPTA]))   
having sum(DWHPRDMON) = ( select sum(DWHPRDMON) from  [$(DataHubDatabaseName)].dbo.[PV_SA_M_PRDCOMPTA_AS_OF_DATE](getdate()) WHERE DWHPRDDTX=(EOMONTH((select MAX(DWHPRDDTX) from [$(DataHubDatabaseName)].[dbo].[PV_SA_M_PRDCOMPTA]),-1)))  ',
 N'DataHub', N'DATAHUB_BFR', N'Fonctionnel', N'''la somme du champ DWHPRDMON de la table PV_SA_M_PRDCOMPTA du mois courant est égale à celle du mois précédent :''+Cast(output_col1 as char)', N'Somme DWHPRDMON', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 559)

 GO

 INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (457, N'DATAHUB_MXT_PORTEUR_1', N'2 - Warning', N'Contrôle nombre de lignes decalées MXT_PORTEUR', N'Ce contrôle vérifie la présence de lignes décalées parmis les lignes processées', N'select nb_lignes_decal as output_col1 
, insrt_ts as output_col2
, nom_fichier as output_col3
, NULL as output_col4
, NULL as output_col5
, NULL as output_col6
, NULL as output_col7  
FROM (
select min(INSRT_TS) as insrt_ts, count(INSRT_TS) as nb_lignes_decal, MIN(NOM_FICHIER) as nom_fichier
from [$(DataHubDatabaseName)].[dbo].WK_MXT_PORTEUR_DECAL
where cast(INSRT_TS as date) = cast(GETDATE() as date)
group by cast(INSRT_TS as date)
having count(*) > 1   
) tab1',
 N'DataHub', N'DATAHUB_MXT', N'Technique - lignes décalées MXT_PORTEUR', N'''Il y a une ou plusieurs lignes décalées parmi les lignes processées.''', N'Nombre de lignes décalées', 'Numéro premiere ligne decal', 'Nom fichier', NULL, NULL, NULL, NULL, 1, NULL, 417)

 GO
 
 INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] ([ID_CONTROL], [COD_CONTROL], [CRITICITE_CONTROL], [LIBELLE_CONTROL], [DESC_CONTROL], [SQL_CONTROL], [PERIMETRE_CONTROL], [FAMILLE_CONTROL], [CATEGORIE_CONTROL], [OUTPUT_message], [OUTPUT_LIB1], [OUTPUT_LIB2], [OUTPUT_LIB3], [OUTPUT_LIB4], [OUTPUT_LIB5], [OUTPUT_LIB6], [OUTPUT_LIB7], [FLG_ACTIF], [ACTION_A_FAIRE], [ID_REF_TRAITEMENT]) VALUES (458, N'DATAHUB_MXT_COMPENSATION_1', N'2 - Warning', N'Contrôle nombre de lignes decalées MXT_COMPENSATION', N'Ce contrôle vérifie la présence de lignes décalées parmis les lignes processées', N'select nb_lignes_decal as output_col1 
, insrt_ts as output_col2
, nom_fichier as output_col3
, NULL as output_col4
, NULL as output_col5
, NULL as output_col6
, NULL as output_col7  
FROM (
select min(INSRT_TS) as insrt_ts, count(INSRT_TS) as nb_lignes_decal, MIN(NOM_FICHIER) as nom_fichier
from [$(DataHubDatabaseName)].[dbo].WK_MXT_COMPENSATION_DECAL
where cast(INSRT_TS as date) = cast(GETDATE() as date)
group by cast(INSRT_TS as date)

) tab1',
 N'DataHub', N'DATAHUB_MXT', N'Technique - lignes décalées MXT_COMPENSATION', N'''Il y a une ou plusieurs lignes décalées parmi les lignes processées.''', N'Nombre de lignes décalées', 'Numéro premiere ligne decal', 'Nom fichier', NULL, NULL, NULL, NULL, 1, NULL, 416)

 GO
--------------------------------------------------------
--AJOUTER LES NOUVEAUX CONTROLES au dessus de cette ligne: 
--!!Ne pas oublier de mettre en dur ID_CONTROL incrémenté de +1 par rapport au dernier contrôle ID_CONTROL de prod
--------------------------------------------------------
--On réactive les ID_CONTROL automatique
SET IDENTITY_INSERT [dbo].[SIDP_QOD_LISTE_CONTROLE] OFF
GO
