USE [$(DataFactoryDatabaseName)]
GO
TRUNCATE TABLE [dbo].[DIM_CASE_CANAL_ORIGINE]
GO
INSERT INTO [dbo].[DIM_CASE_CANAL_ORIGINE] ([CODE_SF], [LIBELLE]) VALUES 
(N'01', N'Mobile/SMS')
,(N'02', N'Internet / Espace Client')
,(N'03', N'Courriers')
,(N'04', N'Appel entrant')
,(N'05', N'Appel sortant')
,(N'06', N'Fax')
,(N'07', N'Chat')
,(N'08', N'Réseaux sociaux')
,(N'09', N'Appel entrant transfert Telco')
,(N'10', N'Appel entrant (boutiques)')
,(N'11', N'Email')
,(N'12', N'Formulaire de demandes')
,(N'13', N'SFMC')
GO