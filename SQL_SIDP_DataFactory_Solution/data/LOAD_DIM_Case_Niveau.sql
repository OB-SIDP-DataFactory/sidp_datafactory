USE [$(DataFactoryDatabaseName)]
GO
TRUNCATE TABLE [dbo].[DIM_CASE_NIVEAU]
GO
INSERT INTO [dbo].[DIM_CASE_NIVEAU] ([CODE_SF], [LIBELLE]) VALUES 
(N'01', N'1')
,(N'02', N'2')
,(N'03', N'2 DG')
,(N'04', N'3')
GO