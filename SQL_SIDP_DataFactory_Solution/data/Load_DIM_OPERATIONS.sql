USE [$(DataFactoryDatabaseName)]
GO
TRUNCATE TABLE [dbo].[DIM_OPERATIONS]
GO
INSERT INTO [dbo].[DIM_OPERATIONS] ([COD_OPE], [DON_ANA], [COD_EVE], [LIB_OPE], [TYPE])
VALUES 
		('AV0',NULL,NULL,'Virement sortant','virement')
		,('A0V',NULL,NULL,'Virement sortant','virement')
		,('R0V',NULL,NULL,'Virement entrant','virement')
		,('R0P',NULL,NULL,'Prélèvement Sortant','prélèvement')
		,('A0P',NULL,NULL,'Prélèvement Entrant','prélèvement')
		,('CTB','','IMM','Paiement carte bancaire','CB')
		,('CTB','1','IMM','Paiement carte bancaire','CB')
		,('CTB','2','IMM','Paiement mobile','PM')
		,('FAC','FBAG80','FAC','Facturation pour inactivité','facturation')
		,('ERE',NULL,'VER','Mouvement sur compte epargne','dep_ret')
GO
