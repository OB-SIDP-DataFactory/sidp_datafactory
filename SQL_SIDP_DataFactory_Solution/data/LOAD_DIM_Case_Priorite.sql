USE [$(DataFactoryDatabaseName)]
GO
TRUNCATE TABLE [dbo].[DIM_CASE_PRIORITE]
GO
INSERT INTO [dbo].[DIM_CASE_PRIORITE] ([CODE_SF], [LIBELLE]) VALUES (N'01', N'Normal')
,(N'02', N'Urgent')
,(N'03', N'Très urgent')
GO