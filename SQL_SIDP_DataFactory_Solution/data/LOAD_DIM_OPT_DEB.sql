USE [$(DataFactoryDatabaseName)]
GO
TRUNCATE TABLE [dbo].[DIM_OPT_DEB]
GO
INSERT INTO [dbo].[DIM_OPT_DEB]
           ([COD_OPT_DEB]
           ,[ABR_OPT_DEB]
           ,[LIB_OPT_DEB])
     VALUES
           ('D','DIFFERE','DEBIT DIFFERE')
		   ,('I','IMMEDIAT','DEBIT IMMEDIAT')
		   ,('1','SUR DIFF M+1','SUR DIFFÉRÉ M+1')
		   ,('2','SUR DIFF M+2','SUR DIFFÉRÉ M+2')
		   ,('3','SUR DIFF M+3','SUR DIFFÉRÉ M+3')
GO