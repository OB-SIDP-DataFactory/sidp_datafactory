USE [$(DataFactoryDatabaseName)]
GO
TRUNCATE TABLE [dbo].[DIM_RESEAU]
GO
INSERT INTO DIM_RESEAU ([CODE_SF], [LIBELLE], [PARENT_ID], [Validity_StartDate])
VALUES ('01', 'Orange France', NULL, '2017-01-01')
     , ('02', 'Orange Bank', NULL, '2017-01-01')
     , ('03', 'Groupama', NULL, '2017-01-01')
     , ('01', 'AD', '01', '2017-01-01')
     , ('02', 'GDT', '01', '2017-01-01')
     , ('03', 'Orange Bank', '02', '2017-01-01')
     , ('04', 'OF-WEB', '01', '2017-01-01')
     , ('05', 'Caisses régionales', '03', '2017-01-01')
     , ('06', 'GAN Patrimoine', '03', '2017-01-01')
     , ('07', 'GAN Prévoyance', '03', '2017-01-01')
     , ('08', 'GAN Assurance', '03', '2017-01-01')
     , ('09', 'DO', '04', '2017-01-01')
GO
