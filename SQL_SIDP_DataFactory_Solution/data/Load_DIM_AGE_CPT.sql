USE [$(DataFactoryDatabaseName)]
GO
TRUNCATE TABLE [dbo].[DIM_AGE_CPT]
GO
INSERT INTO [dbo].[DIM_AGE_CPT] ([AGE_CPT], [AGE_CPT_MIN], [AGE_CPT_MAX])
VALUES ('<3 mois', NULL, 2)
     , ('3-12 mois', 3, 12)
     , ('>12 mois', 13, NULL)
GO
