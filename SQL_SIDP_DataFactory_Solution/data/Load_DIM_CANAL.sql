USE [$(DataFactoryDatabaseName)]
GO
TRUNCATE TABLE [dbo].[DIM_CANAL]
GO
INSERT INTO [dbo].[DIM_CANAL] ([CODE_SF], [LIBELLE], [PARENT_ID], [Validity_StartDate])
VALUES ('01', 'Digital', NULL, '2017-01-01')
     , ('02', 'Agence', NULL, '2017-01-01')
     , ('03', 'CRC', NULL, '2017-01-01')
     , ('04', 'Autre', NULL, '2017-01-01')
     , ('04', 'E-shop', '01', '2017-01-01')
     , ('05', 'Orange&moi web', '01', '2017-01-01')
     , ('06', 'Orange&moi appli', '01', '2017-01-01')
     , ('07', 'Orange Cash', '01', '2017-01-01')
     , ('08', 'Portail OF', '01', '2017-01-01')
     , ('10', 'Appli OB', '01', '2017-01-01')
     , ('11', 'Web OB', '01', '2017-01-01')
     , ('02', 'Boutique IOBSP', '02', '2017-01-01')
     , ('03', 'Boutique non IOBSP', '02', '2017-01-01')
     , ('12', 'Appel entrant', '03', '2017-01-01')
     , ('13', 'Appel sortant', '03', '2017-01-01')
     , ('14', 'Chat', '03', '2017-01-01')
     , ('15', 'E-mail', '03', '2017-01-01')
     , ('01', 'A préciser', '04', '2017-01-01')
     , ('09', 'TV', '04', '2017-01-01')
     , ('16', 'Courrier/fax', '04', '2017-01-01')
     , ('-1', 'Non renseigné', '-1', '2017-01-01')
GO
