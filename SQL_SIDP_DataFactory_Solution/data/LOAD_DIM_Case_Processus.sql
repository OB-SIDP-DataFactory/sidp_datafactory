USE [$(DataFactoryDatabaseName)]
GO
TRUNCATE TABLE [dbo].[DIM_CASE_PROCESSUS]
GO
INSERT INTO [dbo].[DIM_CASE_PROCESSUS] ([CODE_SF], [LIBELLE]) VALUES (N'01', N'Souscription')
,(N'02', N'Gestion')
,(N'03', N'Clôture')
GO