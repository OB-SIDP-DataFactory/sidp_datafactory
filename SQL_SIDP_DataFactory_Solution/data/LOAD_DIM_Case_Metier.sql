USE [$(DataFactoryDatabaseName)]
GO
TRUNCATE TABLE [dbo].[DIM_CASE_METIER]
GO
INSERT INTO [dbo].[DIM_CASE_METIER] ([CODE_SF], [LIBELLE]) VALUES 
(N'01', N'Gestion des comptes')
,(N'02', N'Moyens de paiement')
,(N'03', N'Engagement')
,(N'04', N'Back office Crédit')
,(N'05', N'Relations Client')
GO
