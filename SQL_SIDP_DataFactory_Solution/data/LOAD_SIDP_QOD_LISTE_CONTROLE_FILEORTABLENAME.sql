﻿USE [$(DataFactoryDatabaseName)]
GO
/***********************************************************************************************************************************/
--AJOUTER ICI TOUS LES NOUVEAUX CONTROLES QUI SERONT APPEléS PAR LE TRIGGER [dbo].[TR_UPDATE_DATA_LOG] sur la table [dbo].[Data_Log]
--!! Ce sont les COPIE des contrôles déjà présents dans la table SIDP_QOD_LISTE_CONTROLE!!
/************************************************************************************************************************************/

--On vide la table SIDP_QOD_LISTE_CONTROLE_FILEORTABLENAME
TRUNCATE TABLE [dbo].[SIDP_QOD_LISTE_CONTROLE_FILEORTABLENAME]
GO
print('Insertion des contrôles de volumétrie dans la table SIDP_QOD_LISTE_CONTROLE_FILEORTABLENAME');
INSERT INTO dbo.SIDP_QOD_LISTE_CONTROLE_FILEORTABLENAME ( ID_CONTROL,COD_CONTROL,FAMILLE_CONTROL,FileOrTableName,FLG_ACTIF)
SELECT S.ID_CONTROL
,S.COD_CONTROL
,S.FAMILLE_CONTROL
,SUBSTRING(S.COD_CONTROL, 16, len(S.COD_CONTROL) -15) AS FileOrTableName
,S.FLG_ACTIF
FROM dbo.SIDP_QOD_LISTE_CONTROLE S
INNER JOIN dbo.REF_TRAITEMENT R ON  R.NOM_TABL = SUBSTRING(S.COD_CONTROL, 16, len(S.COD_CONTROL) -15)
WHERE S.COD_CONTROL LIKE 'VOL_COLLECT%' AND R.PERM <> 'IWD' AND R.DOMN ='DataHub'
--AND R.FLG_ACTIF = 1 --Le trigger n'exécute que les contrôles ACTIF
AND S.COD_CONTROL NOT IN (SELECT COD_CONTROL FROM dbo.SIDP_QOD_LISTE_CONTROLE_FILEORTABLENAME) 
GO
-----------------------------------------------------------------------------------------------------
--AJOUTER LES NOUVEAUX CONTROLES au dessus de cette ligne: 
--METTRE LES AUTRES CONTROLES PRESENTS DANS LA TABLE SIDP_QOD_LISTE_CONTROLE que l'on veut récupérer
-----------------------------------------------------------------------------------------------------
