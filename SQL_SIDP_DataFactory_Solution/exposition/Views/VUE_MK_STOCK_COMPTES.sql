﻿CREATE VIEW [exposition].[VUE_MK_STOCK_COMPTES] AS
SELECT jour_alim AS DTE_ALIM
     , code_sab_type_compte AS RUB_CPT
     , ouvert_jour AS NBE_CPT_OUV
     , ferm_jour AS NBE_CPT_CLO
     , ouvert_cumul AS NBE_CPT_OUV_CUM
     , ferm_cumul AS NBE_CPT_CLO_CUM
     , stock_calcul AS NBE_CPT_STOCK
  FROM dbo.sas_vue_synthese_prod