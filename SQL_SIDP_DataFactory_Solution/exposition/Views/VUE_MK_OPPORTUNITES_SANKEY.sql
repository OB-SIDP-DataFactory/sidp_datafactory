﻿CREATE VIEW [exposition].[VUE_MK_OPPORTUNITES_SANKEY] AS
SELECT Id_SF AS IDE_OPPRT_SF
     , code_stade_vente AS CDE_STADE_VENTE
     , stade_vente AS LIB_STADE_VENTE
     , code_canal_interact_orig AS CDE_CAN_INT_ORI
     , canal_interact_orig AS LIB_CAN_INT_ORI
     , code_canal_interact_fin AS CDE_CAN_INT
     , canal_interact_fin AS LIB_CAN_INT
     , date_changement AS DTE_MOD_OPPRT
     , date_creation_opp AS DTE_CREA_OPPRT
  FROM dbo.sas_vue_opport_sankey