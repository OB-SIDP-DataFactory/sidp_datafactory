﻿
CREATE VIEW [exposition].[VUE_RC_INTERACTIONS] AS
SELECT  
 PK_ID                  AS  IDE_INTERACTION
,interaction_type       AS  TYP_INTER
,interaction_sous_type  AS  SS_TYP_INTER
,MONTHYEAR              AS  AM_INTER
,WeekOfYear             AS  SEM_INTER
,StandardDate           AS  DTE_INTER
,CODE_GENESYS_USER      AS  IDE_LOG_GEN
,PROFIL_USER            AS  PRO_USER
,LOGIN_USER             AS  LOG_USER
,CASE_ID                AS  IDE_SF_CASE
,RECORDTYPE_LABEL       AS  TYP_ENR_REQ
,TYPE_CASE_LABEL        AS  TYP_REQ
,SUBTYPE_CASE_LABEL     AS  SS_TYP_REQ
,STATUS_CASE_LABEL      AS  STAT_REQ
,DMC                    AS  DMC               
             
FROM dbo.SAS_Vue_RC_Interactions